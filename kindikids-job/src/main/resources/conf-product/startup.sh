#!/bin/sh
now=$(date +%Y%m%d)
JAVA_HOME=/usr/local/jdk/jdk1.7.0_79
export PATH=$JAVA_HOME/bin:$PATH

command='java -Xms512m -Xmx1024m -jar /home/ubuntu/project/kindikids/jobs/kindikids-job/kindikids-job.jar'
log_file_url="/home/ubuntu/project/kindikids/jobs/kindikids-job/kindikids-job.log"

start(){
    if [ "$log_file_url" != "" ]; then
        exec $command  > "$log_file_url" &
    else
        exec $command &
    fi
}

stop(){
 ps -ef | grep "$command" | awk '{print $2}' | while read pid  
 do
    C_PID=$(ps --no-heading $pid | wc -l)
    echo "µ±Ç°PID=$pid"
    if [ "$C_PID" == "1" ]; then
        echo "PID=$pid ×¼±¸½áÊø"
        kill -9 $pid
        echo "PID=$pid ÒÑ¾­½áÊø"
    else
        echo "PID=$pid ²»´æÔÚ"
    fi
 done
}

case "$1" in 
    start)
		start
		;;
	stop) 
		stop 
		;;
	restart) 
		stop start 
		;; 
	*)
		printf 'Usage: %s {start|stop|restart}\n' "$prog"
		exit 1
		;;
esac
