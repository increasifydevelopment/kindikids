package com.aoyuntek.kindkids.job;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.service.IJobService;

/**
 * 
 * @description 兼職排班角色變化
 * @author gfwang
 * @create 2016年12月27日下午1:25:19
 * @version 1.0
 */
@Service
public class CasualJob extends BaseJob {

    public static Logger logger = Logger.getLogger("CasualJob");

    @Autowired
    private IJobService jobService;

    @Scheduled(cron = "0/20 * 05-21 * * ? ")
    public void updateJob() {
        logger.info("------------updateJob start-----------");
        jobService.updateCaualsRole();
        logger.info("------------updateJob end-----------");
    }

}
