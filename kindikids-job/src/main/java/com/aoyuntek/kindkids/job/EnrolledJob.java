package com.aoyuntek.kindkids.job;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.service.IJobService;
import com.theone.date.util.DateUtil;

@Service
public class EnrolledJob extends BaseJob {

    public static Logger logger = Logger.getLogger("EnrolledJob");

    @Autowired
    private IJobService jobService;

    /**
     * 
     * @author dlli5 at 2016年6月25日下午2:04:58
     */
    @Scheduled(cron = "00 50 01 * * ?")
    public void updateJob() {
        logger.info("+++++++++++++++++++++++++++++++EnrolledJob begin+++++++++++++++++++++++++++++++");
        Date now = DateUtil.parse(DateUtil.format(new Date(), DateUtil.yyyyMMdd), DateUtil.yyyyMMdd);
        try {
            jobService.dealEnrolledJob(logger, now);
        } catch (Exception e) {
            sendErrorMsg("EnrolledJob Error");
            logger.error("EnrolledJob Error", e);
        }

        try {
            jobService.dealEnrolledJobOneWeekAgo(now);
        } catch (Exception e) {
            sendErrorMsg("dealEnrolledJobOneWeekAgo Error");
            logger.error("dealEnrolledJobOneWeekAgo Error", e);
        }

        try {
            jobService.dealChangeCenterJobOneWeekAgo(now);
        } catch (Exception e) {
            sendErrorMsg("dealChangeCenterJobOneWeekAgo Error");
            logger.error("dealChangeCenterJobOneWeekAgo Error", e);
        }
        logger.info("+++++++++++++++++++++++++++++++EnrolledJob end +++++++++++++++++++++++++++++++");
        sendSuccessEmail();
    }

    public static void main(String[] args) {
        Date now = DateUtil.parse(DateUtil.format(new Date(), DateUtil.yyyyMMdd), DateUtil.yyyyMMdd);
        int dayOfWeek = DateUtil.getDayOfWeek(now);
        System.out.println(dayOfWeek);
    }
}
