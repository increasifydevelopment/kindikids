package com.aoyuntek.kindkids.job;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.constants.SystemConstants;
import com.aoyuntek.aoyun.entity.po.AccountInfo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.service.IJobService;
import com.theone.aws.util.SmsUtil;
import com.theone.date.util.DateUtil;

@Service
public class RosterJob extends BaseJob {

    public static Logger logger = Logger.getLogger("RosterJob");

    @Autowired
    private IJobService jobService;

    @Scheduled(cron = "00 30 01 * * ?")
    public void dealRosterJob() {
        logger.info("+++++++++++++++++++++++++++++++RosterJob begin+++++++++++++++++++++++++++++++");
        try {
            Date now = DateUtil.parse(DateUtil.format(new Date(), DateUtil.yyyyMMdd), DateUtil.yyyyMMdd);
            UserInfoVo userInfoVo = new UserInfoVo();
            userInfoVo.setAccountInfo(new AccountInfo());
            userInfoVo.getAccountInfo().setId(SystemConstants.JobCreateAccountId);
            jobService.dealRosterJob(logger, now, userInfoVo);
        } catch (Exception e) {
            sendErrorMsg("RosterJob Error");
            logger.error("RosterJob Error", e);
        }
        logger.info("+++++++++++++++++++++++++++++++RosterJob end +++++++++++++++++++++++++++++++");
    }
}
