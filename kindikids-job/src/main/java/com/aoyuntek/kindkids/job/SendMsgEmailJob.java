package com.aoyuntek.kindkids.job;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.aoyuntek.aoyun.service.IMsgEmailInfoService;
import com.theone.aws.util.SmsUtil;

/**
 * 
 * @description 发送邮件，短信
 * @author gfwang
 * @create 2016年10月10日上午9:49:57
 * @version 1.0
 */
@Service
public class SendMsgEmailJob extends BaseJob {
    /**
     * 日志对象
     */
    public static Logger log = Logger.getLogger("SendMsgEmailJob");

    /**
     * msgEmailService
     */
    @Autowired
    private IMsgEmailInfoService msgEmailService;

    public static void main(String[] args) {
        new SmsUtil(Region.getRegion(Regions.AP_SOUTHEAST_2)).sendSms("TaskJobOfCustmer Error", "+8615056951623");
    }

    @Scheduled(cron = "00 00 08 * * ?")
    public void sendJobs() {
        log.info("+++++++++++++++++++++++++++++++SendMsgEmailJob begin+++++++++++++++++++++++++++++++");
        try {
            msgEmailService.sendMsg();
        } catch (Exception e) {
            sendErrorMsg("SendMsgEmailJob Error");
            log.error("SendMsgEmailJob Error", e);
        }

        log.info("+++++++++++++++++++++++++++++++SendMsgEmailJob end+++++++++++++++++++++++++++++++");
    }

}
