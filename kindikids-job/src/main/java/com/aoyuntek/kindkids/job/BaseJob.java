package com.aoyuntek.kindkids.job;

import java.util.Date;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.aoyuntek.aoyun.uitl.DateUtil;
import com.aoyuntek.framework.constants.PropertieNameConts;
import com.sendgrid.SendGridException;
import com.theone.aws.util.AwsEmailUtil;
import com.theone.aws.util.SmsUtil;
import com.theone.resource.util.PropertiesCacheUtil;
import com.theone.string.util.StringUtil;

public class BaseJob {
    public static Logger log = Logger.getLogger("BaseJob");
    @Value("#{sys}")
    public Properties sysProperties;

    public String getSystem(String key) {
        return sysProperties.getProperty(key);
    }

    /**
     * 
     * @description 发送短信
     * @author gfwang
     * @create 2016年11月24日下午1:50:09
     * @version 1.0
     * @param msg
     */
    public void sendErrorMsg(String msg) {
        try {
            // 获取 监控着 电话
            String[] phones = getSystem("moniter_phone").split(",");
            SmsUtil sms = new SmsUtil(Region.getRegion(Regions.AP_SOUTHEAST_2));
            String server = getSystem("rootUrl");
            for (String phone : phones) {
                if (StringUtil.isEmpty(phone)) {
                    return;
                }
                sms.sendSms(server + "->" + msg, phone);
            }
        } catch (Exception e) {
            log.error("sendErrorMsg=====", e);
        }
    }

    public static void main(String[] args) {
        System.err.println(PropertiesCacheUtil.getValue(PropertieNameConts.SYSTEM, "moniter_email"));
    }

    public void sendSuccessEmail() {
        try {
            String[] emails = getSystem("moniter_email").split(",");
            String server = getSystem("rootUrl");
            AwsEmailUtil emailUtil = new AwsEmailUtil();
            String senderAddress = getSystem("mailHostAccount");
            String senderName = getSystem("system");
            String sub = getSystem("monitor_sub");
            String msg = getSystem("monitor_msg") + "--" + server + "---" + DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss");
            String key = getSystem("email_key");
            for (String email : emails) {
                if (StringUtil.isEmpty(email)) {
                    return;
                }
                emailUtil.sendgrid(senderAddress, senderName, email, email, sub, msg, true, null, null, key);
            }
        } catch (SendGridException e) {
            log.error("sendSuccessEmail=====", e);
        }
    }
}
