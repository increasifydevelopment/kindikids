package com.aoyuntek.kindkids.job;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.enums.TaskType;
import com.aoyuntek.aoyun.service.IJobService;
import com.aoyuntek.aoyun.service.meeting.IMeetingTempService;
import com.aoyuntek.aoyun.service.program.IProgramRegisterTaskService;
import com.aoyuntek.aoyun.service.program.IProgramTaskExgrscService;
import com.aoyuntek.aoyun.service.program.ITaskProgramFollowUpService;
import com.aoyuntek.aoyun.service.program.IWeeklyEvaluationService;
import com.theone.date.util.DateUtil;

@Service
public class TaskJob extends BaseJob {

	public static Logger logger = Logger.getLogger("TaskJobOfCustmer");

	@Autowired
	private IJobService jobService;
	@Autowired
	private IProgramTaskExgrscService programTaskExgrscService;
	@Autowired
	private ITaskProgramFollowUpService taskProgramFollowUpService;
	@Autowired
	private IWeeklyEvaluationService weeklyEvaluationService;
	@Autowired
	private IProgramRegisterTaskService programRegisterTaskService;
	@Autowired
	private IMeetingTempService meetingTempService;

	@Scheduled(cron = "00 30 01 * * ?")
	public void doJob() {
		logger.info("+++++++++++++++++++++++++++++++TaskJobOfCustmer begin+++++++++++++++++++++++++++++++");
		Date now = DateUtil.parse(DateUtil.format(new Date(), DateUtil.yyyyMMdd), DateUtil.yyyyMMdd);
		try {
			// 每天生成Explore Curriculum,Grow Curriculum,School Readiness
			programTaskExgrscService.createProgramTaskExgrscTimedTask(now);
		} catch (Exception e) {
			sendErrorMsg("createProgramTaskExgrscTimedTask Error");
			logger.error("createProgramTaskExgrscTimedTask Error", e);
		}

		try {
			// 每天生成Followup
			taskProgramFollowUpService.createProgramFollowupTimedTask(now);
		} catch (Exception e) {
			sendErrorMsg("createProgramFollowupTimedTask Error");
			logger.error("createProgramFollowupTimedTask Error", e);
		}

		try {
			// 周五生成weeklyEvaluation
			weeklyEvaluationService.createWeeklyEvaluationOnFridayTimedTask(now);
		} catch (Exception e) {
			sendErrorMsg("createWeeklyEvaluationOnFridayTimedTask Error");
			logger.error("createWeeklyEvaluationOnFridayTimedTask Error", e);
		}

		try {
			jobService.dealTaskJobOfCustmer(logger, now);
		} catch (Exception e) {
			sendErrorMsg("dealTaskJobOfCustmer Error");
			logger.error("dealTaskJobOfCustmer Error", e);
		}
		try {
			meetingTempService.createMeetingTempForMeetingTimeTask(now);
		} catch (Exception e) {
			sendErrorMsg("createMeetingTempForMeetingTimeTask Error");
			logger.error("createMeetingTempForMeetingTimeTask Error", e);
		}
		logger.info("+++++++++++++++++++++++++++++++TaskJobOfCustmer end +++++++++++++++++++++++++++++++");
	}

	@Scheduled(cron = "00 55 01 * * ?")
	public void doJob1() {
		logger.info("+++++++++++++++++++++++++++++++TaskJobOfRegister begin+++++++++++++++++++++++++++++++");
		Date now = DateUtil.parse(DateUtil.format(new Date(), DateUtil.yyyyMMdd), DateUtil.yyyyMMdd);
		try {
			// 定时生成RegisterTaskInfo（MealRegister，SleepRegister，SunscreenRegister，NappyRegister，ToiletRegister）
			programRegisterTaskService.createTimingTask(now, TaskType.MealRegister.getValue());
			programRegisterTaskService.createTimingTask(now, TaskType.SleepRegister.getValue());
			programRegisterTaskService.createTimingTask(now, TaskType.SunscreenRegister.getValue());
			programRegisterTaskService.createTimingTask(now, TaskType.NappyRegister.getValue());
			programRegisterTaskService.createTimingTask(now, TaskType.ToiletRegister.getValue());
		} catch (Exception e) {
			sendErrorMsg("createTimingTask Error");
			logger.error("createTimingTask Error", e);
		}
		logger.info("+++++++++++++++++++++++++++++++TaskJobOfRegister end +++++++++++++++++++++++++++++++");
	}

}
