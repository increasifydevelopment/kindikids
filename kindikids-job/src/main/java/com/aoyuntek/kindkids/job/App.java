package com.aoyuntek.kindkids.job;

import java.io.File;

import org.apache.log4j.PropertyConfigurator;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {  
        PropertyConfigurator.configure(System.getProperty("user.dir") + File.separator + "log4j.properties");
        new ClassPathXmlApplicationContext("conf/spring/spring-base.xml").start();
    }
}
