package com.aoyuntek.kindkids.job;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.service.IJobService;
import com.theone.date.util.DateUtil;

@Service
public class AttendanceRequestJob extends BaseJob {

    public static Logger logger = Logger.getLogger("AttendanceRequestJob");

    @Autowired
    private IJobService jobService;

    /**
     * 
     * @author dlli5 at 2016年6月25日下午2:04:58
     */
    @Scheduled(cron = "00 20 01 * * ?")
    public void updateJob() {
        logger.info("+++++++++++++++++++++++++++++++AttendanceRequestJob begin+++++++++++++++++++++++++++++++");
        try {
            Date now = DateUtil.parse(DateUtil.format(new Date(), DateUtil.yyyyMMdd), DateUtil.yyyyMMdd);
            jobService.dealAttendanceRequestJob(logger, now, null);
        } catch (Exception e) {
            sendErrorMsg("AttendanceRequestJob Error");
            logger.error("AttendanceRequestJob", e);
        }

        logger.info("+++++++++++++++++++++++++++++++AttendanceRequestJob end +++++++++++++++++++++++++++++++");
    }

    public static void main(String[] args) {
        Date now = DateUtil.parse(DateUtil.format(new Date(), DateUtil.yyyyMMdd), DateUtil.yyyyMMdd);
        int dayOfWeek = DateUtil.getDayOfWeek(now);
        System.out.println(dayOfWeek);
    }
}
