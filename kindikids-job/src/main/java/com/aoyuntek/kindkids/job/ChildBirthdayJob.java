package com.aoyuntek.kindkids.job;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.service.IUserInfoService;

@Service
public class ChildBirthdayJob extends BaseJob {

    public static Logger logger = Logger.getLogger("ChildBirthdayJob");

    @Autowired
    private IUserInfoService userService;


    /**
     * 
     * @author gfwang at 2016年6月25日下午2:04:58
     */
    @Scheduled(cron = "00 40 01 * * ?")
    public void updateBirthdayJob() {
        logger.info("+++++++++++++++++++++++++++++++updateBirthdayJob begin+++++++++++++++++++++++++++++++");
        try {
            userService.sendUserNewsFeedByBirthday(new Date());
        } catch (Exception e) {
            sendErrorMsg("updateBirthdayJob Error");
            logger.error("updateBirthdayJob", e);
        }
        logger.info("+++++++++++++++++++++++++++++++updateBirthdayJob end +++++++++++++++++++++++++++++++");
    }

}
