package com.aoyuntek.kindkids.job;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.service.IHubworksService;
import com.aoyuntek.aoyun.service.IJobService;
import com.theone.date.util.DateUtil;

@Service
public class ChildSigninJob extends BaseJob {

	public static Logger logger = Logger.getLogger("SigninJob");

	@Autowired
	private IJobService jobService;
	@Autowired
	private IHubworksService hubworksService;

	/**
	 * 
	 * @author dlli5 at 2016年6月25日下午2:04:58
	 */
	@Scheduled(cron = "00 20 01 * * ?")
	public void updateJob() {
		logger.info("+++++++++++++++++++++++++++++++ChildSigninJob begin+++++++++++++++++++++++++++++++");
		try {
			Date now = DateUtil.addDay(DateUtil.parse(DateUtil.format(new Date(), DateUtil.yyyyMMdd), DateUtil.yyyyMMdd), -1);
			int dayOfWeek = DateUtil.getDayOfWeek(now);
			if (dayOfWeek == 1 || dayOfWeek == 7) {
				logger.info("today is 1 or 7");
				return;
			}
			jobService.dealChildSigninJob(logger, now);
		} catch (Exception e) {
			sendErrorMsg("ChildSigninJob Error");
			logger.error("ChildSigninJob Error", e);
		}
		logger.info("+++++++++++++++++++++++++++++++ChildSigninJob end +++++++++++++++++++++++++++++++");
	}

	/**
	 * 同步hubworks信息
	 * 
	 * @author hxzhang 2019年1月2日
	 */
	@Scheduled(cron = "00 00 01 * * ?")
	public void dealSyncHubworks() {
		logger.info("++++++++++++++++++++++++++++++++dealSyncHubworks begin+++++++++++++++++++++++++++++++");
		try {
			hubworksService.dealChildSync(logger);
		} catch (Exception e) {
			sendErrorMsg("dealSyncHubworks error");
			logger.error("dealSyncHubworks error", e);
		}
		logger.info("++++++++++++++++++++++++++++++++dealSyncHubworks end+++++++++++++++++++++++++++++++");
	}

	/**
	 * 同步Attendance信息
	 * 
	 * @author hxzhang 2019年1月3日
	 */
	@Scheduled(cron = "00 0/2 05-21 * * ?")
	public void dealSyncAttendance() {
		logger.info("----------------dealSyncAttendance begin----------------");
		try {
			hubworksService.dealChildAttend(new Date(), logger);
		} catch (Exception e) {
			sendErrorMsg("dealSyncAttendance error");
			logger.error("dealSyncAttendance error", e);
		}
		logger.info("----------------dealSyncAttendance end----------------");
	}

	public static void main(String[] args) {
		Date now = DateUtil.parse(DateUtil.format(new Date(), DateUtil.yyyyMMdd), DateUtil.yyyyMMdd);
		int dayOfWeek = DateUtil.getDayOfWeek(now);
		System.out.println(dayOfWeek);
	}
}
