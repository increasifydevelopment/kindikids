package com.aoyuntek.kindkids.job;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.enums.TaskType;
import com.aoyuntek.aoyun.service.IJobService;
import com.aoyuntek.aoyun.service.program.IProgramIntobsleaService;
import com.aoyuntek.aoyun.service.program.IProgramLessonService;
import com.aoyuntek.aoyun.service.program.IProgramMedicationService;
import com.aoyuntek.aoyun.service.program.IProgramRegisterTaskService;
import com.aoyuntek.aoyun.service.program.IProgramTaskExgrscService;
import com.aoyuntek.aoyun.service.program.ITaskProgramFollowUpService;
import com.aoyuntek.aoyun.service.program.IWeeklyEvaluationService;
import com.theone.aws.util.SmsUtil;
import com.theone.date.util.DateUtil;

@Service
public class TaskJobOverdue extends BaseJob {

    public static Logger logger = Logger.getLogger("TaskJobDueOfCustmer");

    @Autowired
    private IJobService jobService;
    @Autowired
    private IProgramTaskExgrscService programTaskExgrscService;
    @Autowired
    private ITaskProgramFollowUpService taskProgramFollowUpService;
    @Autowired
    private IWeeklyEvaluationService weeklyEvaluationService;
    @Autowired
    private IProgramLessonService programLessonService;
    @Autowired
    private IProgramIntobsleaService programIntobsleaService;
    @Autowired
    private IProgramRegisterTaskService programRegisterTaskService;
    @Autowired
    private IProgramMedicationService programMedicationService;

    @Scheduled(cron = "00 00 01 * * ?")
    public void doJob() {
        logger.info("+++++++++++++++++++++++++++++++TaskJobDueOfCustmer begin+++++++++++++++++++++++++++++++");
        Date now = DateUtil.addDay(DateUtil.parse(DateUtil.format(new Date(), DateUtil.yyyyMMdd), DateUtil.yyyyMMdd), -1);

        try {
            // 将过期Explore Curriculum,Grow Curriculum,School
            // Readiness状态更新为overDue
            programTaskExgrscService.updateOverDueProgramTaskExgrscTimedTask(new Date());
        } catch (Exception e) {
            sendErrorMsg("updateOverDueProgramTaskExgrscTimedTask Error");
            logger.error("updateOverDueProgramTaskExgrscTimedTask Error", e);
        }

        try {
            // 将过期Followup状态更新为overDue
            taskProgramFollowUpService.updateOverDueProgramFollowupTimedTask(new Date());
        } catch (Exception e) {
            sendErrorMsg("updateOverDueProgramFollowupTimedTask Error");
            logger.error("updateOverDueProgramFollowupTimedTask Error", e);
        }

        try {
            // 将过期weeklyEvaluation状态更新为overDue
            weeklyEvaluationService.updateOverDueWeeklyEvaluationTimedTask(new Date());
        } catch (Exception e) {
            sendErrorMsg("updateOverDueWeeklyEvaluationTimedTask Error");
            logger.error("updateOverDueWeeklyEvaluationTimedTask Error", e);
        }

        try {
            // 将过期的lesson状态更新为overDue
            programLessonService.updateOverDueLessonTimedTask(new Date());
        } catch (Exception e) {
            sendErrorMsg("updateOverDueLessonTimedTask Error");
            logger.error("updateOverDueLessonTimedTask Error", e);
        }

        try {
            // 将过期的intobslea状态更新为overDue
            programIntobsleaService.updateOverDueIntobsleaTimedTask(new Date());
        } catch (Exception e) {
            sendErrorMsg("updateOverDueIntobsleaTimedTask Error");
            logger.error("updateOverDueIntobsleaTimedTask Error", e);
        }

        try {
            // 将过期的Register状态更新为overDue（MealRegister，SleepRegister，SunscreenRegister，NappyRegister,ToiletRegister）
            programRegisterTaskService.updateOverDueRegisterTimedTask(new Date(), TaskType.MealRegister.getValue());
            programRegisterTaskService.updateOverDueRegisterTimedTask(new Date(), TaskType.SleepRegister.getValue());
            programRegisterTaskService.updateOverDueRegisterTimedTask(new Date(), TaskType.SunscreenRegister.getValue());
            programRegisterTaskService.updateOverDueRegisterTimedTask(new Date(), TaskType.NappyRegister.getValue());
            programRegisterTaskService.updateOverDueRegisterTimedTask(new Date(), TaskType.ToiletRegister.getValue());
        } catch (Exception e) {
            sendErrorMsg("updateOverDueRegisterTimedTask Error");
            logger.error("updateOverDueRegisterTimedTask Error", e);
        }

        try {
            // 将过期的medication状态更新为overDue
            programMedicationService.updateOverDueMedicationTimedTask(new Date());
        } catch (Exception e) {
            sendErrorMsg("updateOverDueMedicationTimedTask Error");
            logger.error("updateOverDueMedicationTimedTask Error", e);
        }

        try {
            jobService.dealOverDueTaskJobOfCustmer(logger, now);
        } catch (Exception e) {
            sendErrorMsg("dealOverDueTaskJobOfCustmer Error");
            logger.error("dealOverDueTaskJobOfCustmer Error", e);
        }
        logger.info("+++++++++++++++++++++++++++++++TaskJobDueOfCustmer end +++++++++++++++++++++++++++++++");
    }
}
