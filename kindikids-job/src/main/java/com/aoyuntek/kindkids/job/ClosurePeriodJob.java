package com.aoyuntek.kindkids.job;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.constants.SystemConstants;
import com.aoyuntek.aoyun.entity.po.AccountInfo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.service.IPayrollService;
import com.theone.date.util.DateUtil;

@Service
public class ClosurePeriodJob extends BaseJob {
	public static Logger logger = Logger.getLogger("ClosurePeriodJob");
	@Autowired
	private IPayrollService payrollService;

	@Scheduled(cron = "00 00 02 * * ?")
	public void dealRosterJob() {
		logger.info("+++++++++++++++++++++++++++++++ClosurePeriodJob begin+++++++++++++++++++++++++++++++");
		try {
			Date now = DateUtil.parse(DateUtil.format(new Date(), DateUtil.yyyyMMdd), DateUtil.yyyyMMdd);
			UserInfoVo userInfoVo = new UserInfoVo();
			userInfoVo.setAccountInfo(new AccountInfo());
			userInfoVo.getAccountInfo().setId(SystemConstants.JobCreateAccountId);
			payrollService.dealClosurePeriodData(now);
		} catch (Exception e) {
			sendErrorMsg("RosterJob Error");
			logger.error("RosterJob Error", e);
		}
		logger.info("+++++++++++++++++++++++++++++++ClosurePeriodJob end +++++++++++++++++++++++++++++++");
	}
}
