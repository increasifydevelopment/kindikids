package com.aoyuntek.kindkids.job;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.constants.SystemConstants;
import com.aoyuntek.aoyun.entity.po.AccountInfo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.service.IJobService;
import com.theone.date.util.DateUtil;

@Service
public class StaffSigninJob extends BaseJob {

    public static Logger logger = Logger.getLogger("StaffSigninJob");

    @Autowired
    private IJobService jobService;

    /**
     * 
     * @author dlli5 at 2016年6月25日下午2:04:58
     */
    @Scheduled(cron = "00 20 01 * * ?")
    public void updateJob() {
        logger.info("+++++++++++++++++++++++++++++++StaffSigninJob begin+++++++++++++++++++++++++++++++");
        try {
            Date now = DateUtil.addDay(DateUtil.parse(DateUtil.format(new Date(), DateUtil.yyyyMMdd), DateUtil.yyyyMMdd), -1);
            int dayOfWeek = DateUtil.getDayOfWeek(now);
            if (dayOfWeek == 1 || dayOfWeek == 7) {
                logger.info("today is 1 or 7");
                return;
            }
            UserInfoVo userInfoVo = new UserInfoVo();
            userInfoVo.setAccountInfo(new AccountInfo());
            userInfoVo.getAccountInfo().setId(SystemConstants.JobCreateAccountId);
            jobService.dealStaffSigninJob(logger, now, userInfoVo);
        } catch (Exception e) {
            sendErrorMsg("StaffSigninJob Error");
            logger.error("StaffSigninJob Error", e);
        }
        logger.info("+++++++++++++++++++++++++++++++StaffSigninJob end +++++++++++++++++++++++++++++++");
    }

    public static void main(String[] args) {
        Date now = DateUtil.parse(DateUtil.format(new Date(), DateUtil.yyyyMMdd), DateUtil.yyyyMMdd);
        int dayOfWeek = DateUtil.getDayOfWeek(now);
        System.out.println(dayOfWeek);
    }
}
