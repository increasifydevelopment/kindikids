package com.aoyuntek.kindkids.job;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.service.IJobService;
import com.theone.date.util.DateUtil;

@Service
public class SubtractedAttendanceJob extends BaseJob {

    public static Logger logger = Logger.getLogger("SubtractedAttendanceJob");

    @Autowired
    private IJobService jobService;

    @Scheduled(cron = "00 30 01 * * ?")
    public void updateJob() {
        logger.info("+++++++++++++++++++++++++++++++SubtractedAttendanceJob begin+++++++++++++++++++++++++++++++");
        try {
            Date now = DateUtil.parse(DateUtil.format(new Date(), DateUtil.yyyyMMdd), DateUtil.yyyyMMdd);
            jobService.dealSubtractedAttendanceJob(logger, now);
        } catch (Exception e) {
            sendErrorMsg("SubtractedAttendanceJob Error");
            logger.error("SubtractedAttendanceJob Error", e);
        }
        logger.info("+++++++++++++++++++++++++++++++SubtractedAttendanceJob end +++++++++++++++++++++++++++++++");
    }
}
