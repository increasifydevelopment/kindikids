package com.aoyuntek.kindkids.job;

import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.service.ICenterService;
import com.aoyuntek.aoyun.service.IFamilyService;
import com.aoyuntek.aoyun.service.IStaffService;
import com.aoyuntek.aoyun.service.IWeekNutrionalMenuService;
import com.aoyuntek.aoyun.service.attendance.IAbsenteeService;
import com.aoyuntek.aoyun.service.attendance.IGivingNoticeInfoService;
import com.aoyuntek.aoyun.service.attendance.ILeaveService;
import com.aoyuntek.aoyun.service.attendance.ISignService;
import com.aoyuntek.aoyun.service.program.IProgramIntobsleaService;
import com.aoyuntek.aoyun.service.program.IProgramLessonService;
import com.aoyuntek.aoyun.service.program.IProgramMedicationService;
import com.aoyuntek.aoyun.service.program.IProgramRegisterTaskService;
import com.aoyuntek.aoyun.service.program.IProgramTaskExgrscService;
import com.aoyuntek.aoyun.service.program.ITaskProgramFollowUpService;
import com.aoyuntek.aoyun.service.program.IWeeklyEvaluationService;
import com.theone.aws.util.SmsUtil;

/**
 * 
 * @author dlli5 at 2016年6月25日下午2:05:11
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
@Service
public class TimedTaskJob extends BaseJob {
	@Autowired
	private ILeaveService leaveService;
	@Autowired
	private IFamilyService familyService;
	@Autowired
	private IAbsenteeService absenteeService;
	@Autowired
	private IStaffService staffService;
	@Autowired
	private ISignService signService;
	@Autowired
	private IGivingNoticeInfoService gnService;
	@Autowired
	private IWeekNutrionalMenuService weekNutrionalMenuService;
	/**
	 * 日志对象
	 */
	public static Logger logger = Logger.getLogger("TimedTaskJob");

	/**
	 * 
	 * @author dlli5 at 2016年6月25日下午2:04:58
	 */
	@Scheduled(cron = "00 10 01 * * ?")
	public void updateJob() {
		logger.info("+++++++++++++++++++++++++++++++LeaveTimedTaskJob begin+++++++++++++++++++++++++++++++");
		try {
			// leave模块定时任务
			leaveService.updateLeaveTimedTask(new Date());
		} catch (Exception e) {
			sendErrorMsg("updateLeaveTimedTask Error");
			logger.error("updateLeaveTimedTask Error", e);
		}

		try {
			// family模块定时任务
			familyService.updateFamilyTimedTask(new Date());
		} catch (Exception e) {
			sendErrorMsg("updateFamilyTimedTask Error");
			logger.error("updateFamilyTimedTask Error", e);
		}

		try {
			// center管理模块 发送菜单
			// centerService.sendTodaysMenu(new Date());
			weekNutrionalMenuService.sendMenu(new Date());
		} catch (Exception e) {
			sendErrorMsg("sendTodaysMenu Error");
			logger.error("sendTodaysMenu Error", e);
		}

		try {
			// 请假申请过期未批准变为失效
			absenteeService.updateAbsenteeStatusTimedTask(new Date());
		} catch (Exception e) {
			sendErrorMsg("updateAbsenteeStatusTimedTask Error");
			logger.error("updateAbsenteeStatusTimedTask Error", e);
		}
		try {
			// 离园申请过期未批准变为失效
			gnService.updateGivingNoticeStatusTimedTask(new Date());
		} catch (Exception e) {
			sendErrorMsg("updateAbsenteeStatusTimedTask Error");
			logger.error("updateAbsenteeStatusTimedTask Error", e);
		}

		try {
			// 给未激活的家长每七天发送激活邮件
			familyService.sendActiveEmailTimedTask(new Date());
		} catch (Exception e) {
			sendErrorMsg("sendActiveEmailTimedTask Error");
			logger.error("sendActiveEmailTimedTask Error", e);
		}

		try {
			// 给未激活的员工每七天发送激活邮件
			staffService.sendActiveEmailTimedTask(new Date());
		} catch (Exception e) {
			sendErrorMsg("sendActiveEmailTimedTask Error");
			logger.error("sendActiveEmailTimedTask Error", e);
		}
		try {
			// 处理Staff Employment
			staffService.dealEmployment(new Date());
		} catch (Exception e) {
			sendErrorMsg("dealEmployment Error");
			logger.error("dealEmployment Error", e);
		}
		try {
			staffService.dealAchiveStaff(new Date());
		} catch (Exception e) {
			sendErrorMsg("dealAchiveStaff Error");
			logger.error("dealAchiveStaff Error", e);
		}
		logger.info("+++++++++++++++++++++++++++++++LeaveTimedTaskJob end +++++++++++++++++++++++++++++++");
	}

	@Scheduled(cron = "00 00 18 * * ?")
	public void updateJob5() {
		logger.info("+++++++++++++++++++++++++++++++sendMsgByChildUnSignoutTimedTask begin+++++++++++++++++++++++++++++++");
		try {
			// 在18:00还有未签到的小孩,给管理员发送短信
			signService.sendMsgByChildUnSignoutTimedTask(new Date());
		} catch (Exception e) {
			sendErrorMsg("sendMsgByChildUnSignoutTimedTask Error");
			logger.error("sendMsgByChildUnSignoutTimedTask Error", e);
		}
		logger.info("+++++++++++++++++++++++++++++++sendMsgByChildUnSignoutTimedTask end+++++++++++++++++++++++++++++++");
	}
}
