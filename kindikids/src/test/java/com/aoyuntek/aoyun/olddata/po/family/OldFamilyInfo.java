package com.aoyuntek.aoyun.olddata.po.family;

import java.util.List;

import com.aoyuntek.aoyun.olddata.po.family.parents.OldParentsPo;

public class OldFamilyInfo {
    private List<OldSiblingsInfo> siblings;

    private List<OldParentsPo> parents;

    public List<OldSiblingsInfo> getSiblings() {
        return siblings;
    }

    public void setSiblings(List<OldSiblingsInfo> siblings) {
        this.siblings = siblings;
    }

    public List<OldParentsPo> getParents() {
        return parents;
    }

    public void setParents(List<OldParentsPo> parents) {
        this.parents = parents;
    }

}
