package com.aoyuntek.aoyun.olddata.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.aoyuntek.aoyun.condtion.UserDetailCondition;
import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.dao.program.EylfCheckInfoMapper;
import com.aoyuntek.aoyun.entity.po.AccountInfo;
import com.aoyuntek.aoyun.entity.po.AccountRoleInfo;
import com.aoyuntek.aoyun.entity.po.AttendanceHistoryInfo;
import com.aoyuntek.aoyun.entity.po.BackgroundPersonInfo;
import com.aoyuntek.aoyun.entity.po.BackgroundPetsInfo;
import com.aoyuntek.aoyun.entity.po.ChildAttendanceInfo;
import com.aoyuntek.aoyun.entity.po.ChildBackgroundInfo;
import com.aoyuntek.aoyun.entity.po.ChildCustodyArrangeInfo;
import com.aoyuntek.aoyun.entity.po.ChildDietaryRequireInfo;
import com.aoyuntek.aoyun.entity.po.ChildHealthMedicalDetailInfo;
import com.aoyuntek.aoyun.entity.po.ChildHealthMedicalInfo;
import com.aoyuntek.aoyun.entity.po.ChildMedicalInfo;
import com.aoyuntek.aoyun.entity.po.EmergencyContactInfo;
import com.aoyuntek.aoyun.entity.po.FamilyInfo;
import com.aoyuntek.aoyun.entity.po.RelationKidParentInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.po.program.ChildEylfInfo;
import com.aoyuntek.aoyun.entity.vo.old.OldFamilyInfoVo;
import com.aoyuntek.aoyun.enums.ArchivedStatus;
import com.aoyuntek.aoyun.enums.ChildType;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.EnrolledState;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.enums.UserType;
import com.aoyuntek.aoyun.olddata.constants.HttpUrlConstants;
import com.aoyuntek.aoyun.olddata.po.family.OldEylfPo;
import com.aoyuntek.aoyun.olddata.po.family.OldFamilyInfo;
import com.aoyuntek.aoyun.olddata.po.family.OldSiblingsInfo;
import com.aoyuntek.aoyun.olddata.po.family.child.OldAttendancePo;
import com.aoyuntek.aoyun.olddata.po.family.child.OldBackPo;
import com.aoyuntek.aoyun.olddata.po.family.child.OldBasePo;
import com.aoyuntek.aoyun.olddata.po.family.child.OldChildPo;
import com.aoyuntek.aoyun.olddata.po.family.child.OldConcatPo;
import com.aoyuntek.aoyun.olddata.po.family.child.OldDietaryPo;
import com.aoyuntek.aoyun.olddata.po.family.child.OldDocumentPo;
import com.aoyuntek.aoyun.olddata.po.family.child.OldMedicalPo;
import com.aoyuntek.aoyun.olddata.po.family.child.OldStatusInfoPo;
import com.aoyuntek.aoyun.olddata.po.family.child.attendance.OldCurrentSchedulePo;
import com.aoyuntek.aoyun.olddata.po.family.child.back.OldBackPeoplePo;
import com.aoyuntek.aoyun.olddata.po.family.child.back.OldBackPetsPo;
import com.aoyuntek.aoyun.olddata.po.family.child.back.OldNurseryRoutinePo;
import com.aoyuntek.aoyun.olddata.po.family.child.base.OldCustodyArrangementPo;
import com.aoyuntek.aoyun.olddata.po.family.child.base.OldHealthPo;
import com.aoyuntek.aoyun.olddata.po.family.child.dietary.OldDairyPo;
import com.aoyuntek.aoyun.olddata.po.family.child.dietary.OldDietaryEggsPo;
import com.aoyuntek.aoyun.olddata.po.family.child.dietary.OldDietaryMeatPo;
import com.aoyuntek.aoyun.olddata.po.family.child.dietary.OldDietaryNutsPo;
import com.aoyuntek.aoyun.olddata.po.family.parents.OldParentsPo;
import com.aoyuntek.aoyun.olddata.po.family.parents.OldPreferredContactPo;
import com.aoyuntek.aoyun.service.old.IOldDataService;
import com.aoyuntek.aoyun.uitl.FilePathUtil;
import com.google.gson.reflect.TypeToken;
import com.hp.hpl.sparta.xpath.ThisNodeTest;
import com.theone.common.util.ServiceResult;
import com.theone.date.util.DateUtil;
import com.theone.json.util.JsonUtil;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

public class FamilyService extends BaseService {
    private static Logger log = Logger.getLogger(FamilyService.class);
    @Autowired
    private IOldDataService oldDataService;
    @Autowired
    private UserInfoMapper userInfoMapper;
    @Autowired
    private CentersInfoMapper centersInfoMapper;
    @Autowired
    private EylfCheckInfoMapper checkInfoMapper;
    OldFamilyInfoVo familyVo = new OldFamilyInfoVo();
    private List<String> eylfIds = new ArrayList<String>();

    @Test
    public void importFamily() {
        eylfIds = checkInfoMapper.getIds();
        List<OldFamilyInfo> oldList = null;
        int familySuccessCount = 0;
        int familuErrorCount = 0;
        try {
            String json = getHttpJson(HttpUrlConstants.FAMILY_URL);
            oldList = JsonUtil.jsonToList(json, new TypeToken<List<OldFamilyInfo>>() {
            }.getType());

            for (OldFamilyInfo oldFamilyInfo : oldList) {
                // 验证
                ServiceResult<Object> result = validateData(oldFamilyInfo);
                if (!result.isSuccess()) {
                    familuErrorCount++;
                    super.exceptionList.add(result.getMsg());
                    continue;
                }
                String familyJson = JsonUtil.objectToJson(oldFamilyInfo);
                log.error("-->" + familyJson);
                List<OldParentsPo> oldParents = oldFamilyInfo.getParents();
                String familyId = getUUID();
                String familyName = "";
                Date createDate = null;
                // 存放accountId,用来建立关系
                List<String> parentIds = new ArrayList<String>();
                List<String> childIds = new ArrayList<String>();
                // 初始化家长
                for (OldParentsPo parent : oldParents) {
                    String accountId = getUUID();
                    String userId = getUUID();
                    initParentInfo(parent, familyId, userId, accountId, familyJson, parentIds);
                    parentIds.add(accountId);
                }
                // 初始化小孩
                List<OldSiblingsInfo> oldSiblings = oldFamilyInfo.getSiblings();
                for (OldSiblingsInfo siblings : oldSiblings) {
                    if (siblings == null) {
                        continue;
                    }
                    String accountId = getUUID();
                    String userId = getUUID();
                    OldChildPo oldChild = siblings.getChild();
                    // 初始化
                    Object[] backData = initChildInfo(oldChild, familyId, userId, accountId, childIds, parentIds, familyJson);
                    familyName = (String) backData[0];
                    createDate = (Date) backData[1];
                    childIds.add(accountId);
                }

                // 建立家庭关系
                buildChildKindRelation(parentIds, childIds, familyId);
                if (StringUtil.isEmpty(familyName)) {
                    OldParentsPo firstParent = oldParents.get(0);
                    if (firstParent == null) {
                        this.exceptionList.add("-----familyerror---name-->" + familyJson);
                    } else {
                        familyName = firstParent.getSurname();
                    }
                }
                initFamily(familyName, familyId, createDate);
                familySuccessCount++;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        log.info("====================================end insert=====================");

        try {
            // Collections.sort(this.totals);
            // System.err.println(this.totals.get(this.totals.size() - 1));
            oldDataService.importFamily(familyVo);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        printLog(this.log);
        log.info("oldFamily count=" + oldList.size() + ",success count=" + familySuccessCount + ",error count=" + familuErrorCount);
    }

    private ServiceResult<Object> validateData(OldFamilyInfo oldFamilyInfo) {
        ServiceResult<Object> result = new ServiceResult<Object>();
        result.setSuccess(false);
        String familyJson = JsonUtil.objectToJson(oldFamilyInfo);
        List<OldParentsPo> oldParents = oldFamilyInfo.getParents();
        String familyId = getUUID();
        List<String> parentIds = new ArrayList<String>();
        List<String> childIds = new ArrayList<String>();
        // 初始化家长
        for (OldParentsPo parent : oldParents) {
            String accountId = getUUID();
            result = validateParent(familyJson, parent);
            if (!result.isSuccess()) {
                // 数据异常加日志
                return result;
            }
            parentIds.add(accountId);
        }
        // 初始化小孩
        List<OldSiblingsInfo> oldSiblings = oldFamilyInfo.getSiblings();
        for (OldSiblingsInfo siblings : oldSiblings) {
            if (siblings == null) {
                continue;
            }
            String accountId = getUUID();
            OldChildPo oldChild = siblings.getChild();
            // 初始化
            result = validateChild(familyJson, oldChild);
            if (!result.isSuccess()) {
                // 数据异常加日志
                return result;
            }
            childIds.add(accountId);
        }
        if (ListUtil.isEmpty(parentIds) || ListUtil.isEmpty(childIds)) {
            result.setMsg("-----buildChildKindRelation--familyerror---->familyId=" + familyId);
            return result;
        }
        result.setSuccess(true);
        return result;
    }

    private ServiceResult<Object> validateParent(String familyJson, OldParentsPo parent) {
        ServiceResult<Object> result = new ServiceResult<Object>();
        result.setSuccess(false);
        String email = parent.getEmail();
        if (StringUtil.isEmpty(email)) {
            result.setMsg("error parent email is require---> " + familyJson);
            return result;
        }
        UserDetailCondition condition = new UserDetailCondition();
        condition.setEmail(email);
        if (userInfoMapper.getByCondition(condition).size() > 0) {
            result.setMsg("error parent email is repeat--->" + familyJson);
            return result;
        }
        result.setSuccess(true);
        return result;
    }

    private ServiceResult<Object> validateChild(String familyJson, OldChildPo oldChild) {
        ServiceResult<Object> result = new ServiceResult<Object>();
        result.setSuccess(false);
        OldBasePo oldBase = oldChild.getBasic_info();
        String oldId = oldBase.getChildId_oldId();
        String oldCenterId = StringUtil.isEmpty(oldBase.getCentres_oldId()) ? "" : oldBase.getCentres_oldId();
        String oldRoomId = StringUtil.isEmpty(oldBase.getRoom_oldId()) ? "" : oldBase.getRoom_oldId();
        String oldGroupId = StringUtil.isEmpty(oldBase.getGroup_oldId()) ? "" : oldBase.getGroup_oldId();

        if (super.repeatFactory.containsKey(oldId)) {
            result.setMsg(" error child already exit----old_id=" + oldId + "--->" + familyJson);
            return result;
        }
        String centerId = centersInfoMapper.getIdByOldId(oldCenterId);
        String roomId = centersInfoMapper.getIdByOldId(oldRoomId);
        String groupId = centersInfoMapper.getIdByOldId(oldGroupId);

        if (StringUtil.isEmpty(centerId)) {
            result.setMsg(" error child no center id ----> " + familyJson);
            return result;
        }
        if (StringUtil.isEmpty(roomId)) {
            result.setMsg(" error child no room id ----> " + familyJson);
            return result;
        }
        /*
         * if (StringUtil.isEmpty(groupId)) {
         * result.setMsg(" error child no group id ----> " + familyJson); return
         * result; }
         */
        result.setSuccess(true);
        return result;
    }

    private void initFamily(String familyName, String familyId, Date createDate) {
        familyVo.getFamilyList().add(new FamilyInfo(familyId, familyName, DeleteFlag.Default.getValue(), createDate, createDate, null));
    }

    // 建立家庭关系
    private void buildChildKindRelation(List<String> parentIds, List<String> childIds, String familyId) {
        for (String parentAccountId : parentIds) {
            for (String kidAccountId : childIds) {
                familyVo.getParentChildRelationList().add(
                        new RelationKidParentInfo(getUUID(), kidAccountId, parentAccountId, DeleteFlag.Default.getValue()));
            }
        }
    }

    /**
     * 
     * @description 初始化小孩信息
     * @author gfwang
     * @create 2016年12月14日下午3:22:45
     * @version 1.0
     * @param oldChild
     * @param familyId
     * @param userId
     * @param accountId
     * @param childIds
     * @param parentIds
     * @param familyJson
     * @return
     */
    private Object[] initChildInfo(OldChildPo oldChild, String familyId, String userId, String accountId, List<String> childIds,
            List<String> parentIds, String familyJson) {

        OldBasePo oldBase = oldChild.getBasic_info();
        // 获取创建日期
        String oldId = oldBase.getChildId_oldId();
        Date createDate = getDateByOldId(oldId);

        UserInfo childInfo = new UserInfo();
        childInfo.setFamilyId(familyId);
        // 初始化 tbl_user,tbl_account,tbl_account_role
        String familyName = initChildBaseInfo(oldBase, childInfo, oldChild.getStatus_info(), familyId, userId, accountId, parentIds, childIds,
                familyJson, createDate);
        // 处理背景信息
        initBackGroudInfo(oldChild.getBackground(), userId, createDate);

        // 小孩药物
        initChildMedical(oldChild.getMedical(), userId);

        initDietary(oldChild.getDietary(), userId);

        initAttendance(oldChild.getAttendance(), createDate, accountId, childInfo);

        // EYLF
        initEylf(oldChild.getEylf_check(), accountId);

        return new Object[] { familyName, createDate };

    }

    // 小孩
    private void initAttendance(OldAttendancePo attendance, Date createDate, String accountId, UserInfo user) {
        OldCurrentSchedulePo current_schedule = attendance.getCurrent_schedule();
        ChildAttendanceInfo childAttendanceInfo = new ChildAttendanceInfo();
        childAttendanceInfo.setId(getUUID());
        childAttendanceInfo.setUserId(user.getId());
        childAttendanceInfo.setDeleteFlag(DeleteFlag.Default.getValue());
        childAttendanceInfo.setCreateTime(createDate);
        childAttendanceInfo.setUpdateTime(createDate);
        childAttendanceInfo.setMonday(current_schedule.getMonday());
        childAttendanceInfo.setTuesday(current_schedule.getTuesday());
        childAttendanceInfo.setWednesday(current_schedule.getWednesday());
        childAttendanceInfo.setThursday(current_schedule.getThursday());
        childAttendanceInfo.setFriday(current_schedule.getFriday());
        childAttendanceInfo.setEnrolled(EnrolledState.Enrolled.getValue());
        childAttendanceInfo.setType(ChildType.Inside.getValue());
        childAttendanceInfo.setEnrolmentDate(createDate);
        childAttendanceInfo.setChangeDate(createDate);
        // attend表
        familyVo.getChildAttendList().add(childAttendanceInfo);

        // history表
        AttendanceHistoryInfo attendanceHistoryInfo = new AttendanceHistoryInfo();
        attendanceHistoryInfo.setId(getUUID());
        attendanceHistoryInfo.setChildId(accountId);
        attendanceHistoryInfo.setCenterId(user.getCentersId());
        attendanceHistoryInfo.setRoomId(user.getRoomId());
        attendanceHistoryInfo.setMonday(current_schedule.getMonday());
        attendanceHistoryInfo.setTuesday(current_schedule.getTuesday());
        attendanceHistoryInfo.setWednesday(current_schedule.getWednesday());
        attendanceHistoryInfo.setThursday(current_schedule.getThursday());
        attendanceHistoryInfo.setFriday(current_schedule.getFriday());
        attendanceHistoryInfo.setCreateTime(createDate);
        attendanceHistoryInfo.setCreateAccountId(this.defaultAccountId);
        attendanceHistoryInfo.setUpdateTime(createDate);
        attendanceHistoryInfo.setUpdateAccountId(this.defaultAccountId);
        attendanceHistoryInfo.setDeleteFlag(DeleteFlag.Default.getValue());
        attendanceHistoryInfo.setBeginTime(createDate);
        attendanceHistoryInfo.setGroup(1);
        familyVo.getChildAttendHistoryList().add(attendanceHistoryInfo);
    }

    // 小孩
    private void initDietary(OldDietaryPo dietary, String userId) {
        ChildDietaryRequireInfo dietaryRequire = new ChildDietaryRequireInfo();
        dietaryRequire.setId(getUUID());
        dietaryRequire.setUserId(userId);
        dietaryRequire.setDeleteFlag(DeleteFlag.Default.getValue());
        // Dairy
        OldDairyPo dairy = dietary.getDairy();
        dietaryRequire.setDairy1(dairy.getFood_containing_dairy());
        dietaryRequire.setDairy2(dairy.getStreight_milk());
        dietaryRequire.setDairy3(dairy.getFood_containing_dairy_powder());
        if (StringUtil.isNotEmpty(dairy.getOther())) {
            dietaryRequire.setDairyOther(true);
            dietaryRequire.setDairyOherSpecify(dairy.getOther());
        }
        // Nuts
        OldDietaryNutsPo nuts = dietary.getNuts();
        dietaryRequire.setNuts1(nuts.getTrace_of_nuts());
        dietaryRequire.setNuts2(nuts.getPeanuts());
        dietaryRequire.setNuts3(nuts.getCashews());
        dietaryRequire.setNuts4(nuts.getAlmonds());
        if (StringUtil.isNotEmpty(nuts.getOther())) {
            dietaryRequire.setNutsOther(true);
            dietaryRequire.setNutsOtherSpecify(nuts.getOther());
        }
        // Eggs
        OldDietaryEggsPo eggs = dietary.getEggs();
        dietaryRequire.setEggs1(eggs.getOnly_whole_egg());
        dietaryRequire.setEggs2(eggs.getOnly_egg_yolks());
        dietaryRequire.setEggs3(eggs.getFood_containing_eggs());
        if (StringUtil.isNotEmpty(eggs.getOther())) {
            dietaryRequire.setEggsOther(true);
            dietaryRequire.setEggsOtherSpecify(eggs.getOther());
        }
        // meat
        OldDietaryMeatPo meat = dietary.getMeat();
        dietaryRequire.setMeat1(meat.getLamb());
        dietaryRequire.setMeat2(meat.getBeef());
        dietaryRequire.setMeat3(meat.getChicken());
        dietaryRequire.setMeat4(meat.getPork());
        dietaryRequire.setMeat5(meat.getFish_fillet());
        if (StringUtil.isNotEmpty(meat.getOther())) {
            dietaryRequire.setMeatOther(true);
            dietaryRequire.setMeatOtherSpecify(meat.getOther());
        }

        dietaryRequire.setEpiPen(getChoosePen(dietary.getRequire_api_pen()));
        dietaryRequire.setTermsConditions(dietary.getTerms_agreed());
        String sourceId = getUUID();
        dietaryRequire.setEpiPenAttachId(sourceId);
        initFamilyFile(sourceId, dietary.getFiles());
        familyVo.getDietaryList().add(dietaryRequire);
    }

    private Short getChoosePen(Short oldDate) {
        if (oldDate == null || oldDate == -1) {
            return oldDate;
        }
        if (oldDate == 1) {
            return (short) 0;
        }
        return (short) 1;
    }

    // 小孩
    private void initChildMedical(OldMedicalPo medical, String userId) {
        ChildMedicalInfo medicalInfo = new ChildMedicalInfo();
        medicalInfo.setId(getUUID());
        medicalInfo.setUserId(userId);
        medicalInfo.setDeleteFlag(DeleteFlag.Default.getValue());
        medicalInfo.setApplicableEczema(medical.getEczema());
        medicalInfo.setApplicableAsthma(medical.getAsthma());
        medicalInfo.setApplicableEpilepsy(medical.getEpilepsy());
        if (!StringUtil.isEmpty(medical.getOther())) {
            medicalInfo.setApplicableOther(true);
            medicalInfo.setApplicableOtherSpecify(medical.getOther());
        }
        medicalInfo.setRequireLongMedic(getMedicationRequire(medical.getMedication_required()));
        medicalInfo.setTermsConditions(medical.getTerms_conditions());
        String sourceId = getUUID();
        medicalInfo.setRequireLongMedicAttachId(sourceId);
        initFamilyFile(sourceId, medical.getAttachments());
        familyVo.getMedicalList().add(medicalInfo);
    }

    private Short getMedicationRequire(Short value) {
        if (null == value || -1 == value) {
            return value;
        }
        return value == 0 ? (short) 1 : (short) 0;
    }

    // 小孩
    private void initFamilyFile(String sourceId, List<OldDocumentPo> files) {
        if (!ListUtil.isEmpty(files)) {
            for (OldDocumentPo file : files) {
                if (StringUtil.isEmpty(file.getPath())) {
                    System.err.println("------------------empty");
                }
                familyVo.getFileList().add(initAttachInfo(sourceId, file.getPath(), FilePathUtil.FAMILY_PATH, file.getName()));
            }
        }
    }

    private List<Integer> totals = new ArrayList<Integer>();

    // 小孩
    private void initBackGroudInfo(OldBackPo background, String userId, Date date) {
        ChildBackgroundInfo childBackGroud = new ChildBackgroundInfo();
        String backgroundId = getUUID();
        childBackGroud.setId(backgroundId);
        childBackGroud.setUserId(userId);
        childBackGroud.setLanguageHome(background.getLanguage_at_home());
        childBackGroud.setCulturalBackground(background.getCultural_background());
        childBackGroud.setSameHousehold(background.getOther_people_same_household());
        childBackGroud.setChildHavePets(background.getHas_pets());
        childBackGroud.setSameHousehold(background.getOther_people_same_household());
        childBackGroud.setUnderTwoYears(background.getUnder_two_years());

        OldNurseryRoutinePo nurseryRoutine = background.getNursery_routine();
        if (background.getUnder_two_years() && nurseryRoutine != null) {
            childBackGroud.setSleepTimes(nurseryRoutine.getSleep_times());
            childBackGroud.setMealTimes(nurseryRoutine.getMeal_times());
            childBackGroud.setBottleTimes(nurseryRoutine.getBottle_times());
        }

        childBackGroud.setWayToilet(getBacgroundToilet(background.getToilet_training_stage()));
        childBackGroud.setBottleCup(background.getDrink_from_bottle());
        childBackGroud.setBottleCupSpecify(background.getBottle_info());
        childBackGroud.setSpecialRequirementsSleep(background.getSpecial_requirements_for_sleep_time());

        childBackGroud.setSleepSpecify(background.getSleep_time_info());

        childBackGroud.setClothingNeeds(background.getIndividual_clothing_needs());
        childBackGroud.setClothingSpecify(background.getClothing_info());
        childBackGroud.setAssistanceDuringFeed(background.getRequire_feeding());
        childBackGroud.setFeedSpecify(background.getFeeding_info());

        if (!StringUtil.isEmpty(background.getFeeding_info())) {
            this.totals.add(background.getFeeding_info().length());
        }

        childBackGroud.setHaveCulturalReligious(background.getAdditional_needs());
        childBackGroud.setReligiousSpecify(background.getAdditional_need_info());
        childBackGroud.setExperience(background.getPreferred_experience());
        childBackGroud.setChildInterests(background.getInterest_info());
        childBackGroud.setDeleteFlag(DeleteFlag.Default.getValue());

        // TODO 缺少hasPeople
        initChildBackPeoplePets(backgroundId, background, background.getHas_pets(), background.getOther_people_same_household(), date);
        familyVo.getBackgroundList().add(childBackGroud);
    }

    // 小孩
    private void initChildBackPeoplePets(String backgroundId, OldBackPo background, boolean hasPets, boolean hasPeople, Date date) {
        List<OldBackPeoplePo> oldPeoples = background.getPeople();
        List<OldBackPetsPo> oldPets = background.getPets();

        int count = 0;
        for (OldBackPeoplePo item : oldPeoples) {
            if (!hasPeople) {
                continue;
            }
            familyVo.getPersonList().add(
                    new BackgroundPersonInfo(getUUID(), item.getName(), item.getSurname(), item.getRelationship(), backgroundId, DateUtil.addSeconds(
                            date, count), DeleteFlag.Default.getValue()));
            count++;
        }
        count = 0;
        for (OldBackPetsPo item : oldPets) {
            if (!hasPets) {
                continue;
            }
            familyVo.getPetList().add(
                    new BackgroundPetsInfo(getUUID(), item.getName(), item.getType(), backgroundId, DateUtil.addSeconds(date, count),
                            DeleteFlag.Default.getValue()));
        }
    }

    // 小孩
    private Short getBacgroundToilet(Short value) {
        if (value == null || value == -1) {
            return null;
        }
        return value;
    }

    /**
     * 
     * @description 初始化基本信息
     * @author gfwang
     * @create 2016年12月14日下午3:20:07
     * @version 1.0
     * @param oldBase
     *            老数据
     * @param childInfo
     *            新对象
     * @param status_info
     * @param familyId
     * @param userId
     * @param accountId
     * @param parentIds
     *            当前family中的家长
     * @param childIds
     *            当前family中的小孩
     * @param familyJson
     *            familyJson数据
     * @return
     */
    private String initChildBaseInfo(OldBasePo oldBase, UserInfo childInfo, OldStatusInfoPo status_info, String familyId, String userId,
            String accountId, List<String> parentIds, List<String> childIds, String familyJson, Date createDate) {
        String oldId = oldBase.getChildId_oldId();
        String oldCenterId = StringUtil.isEmpty(oldBase.getCentres_oldId()) ? "" : oldBase.getCentres_oldId();
        String oldRoomId = StringUtil.isEmpty(oldBase.getRoom_oldId()) ? "" : oldBase.getRoom_oldId();
        String oldGroupId = StringUtil.isEmpty(oldBase.getGroup_oldId()) ? "" : oldBase.getGroup_oldId();

        String centerId = centersInfoMapper.getIdByOldId(oldCenterId);
        String roomId = centersInfoMapper.getIdByOldId(oldRoomId);
        String groupId = centersInfoMapper.getIdByOldId(oldGroupId);

        childInfo.setId(userId);
        childInfo.setUserType(UserType.Child.getValue());
        childInfo.setFirstName(oldBase.getChild_first_name());
        childInfo.setMiddleName(oldBase.getChild_middle_name());
        childInfo.setLastName(StringUtil.isEmpty(oldBase.getChild_surname()) ? "" : oldBase.getChild_surname());
        childInfo.setOldId(oldId);
        String gender = oldBase.getChild_gender();
        if (StringUtil.isNotEmpty(gender)) {
            childInfo.setGender("male".equals(gender.toLowerCase()) ? (short) 1 : (short) 0);
        }
        childInfo.setAddress(oldBase.getAddress());
        childInfo.setBirthday(parseDate(oldBase.getDate_of_birth()));
        childInfo.setSuburb(oldBase.getSuburb());
        childInfo.setState(oldBase.getState());
        childInfo.setPostcode(oldBase.getPostcode());
        childInfo.setCentersId(centerId);
        childInfo.setRoomId(roomId);
        childInfo.setGroupId(groupId);
        childInfo.setFamilyId(familyId);
        childInfo.setCreateTime(createDate);
        childInfo.setUpdateTime(createDate);
        childInfo.setCreateAccountId(this.defaultAccountId);
        childInfo.setUpdateAccountId(this.defaultAccountId);
        childInfo.setDeleteFlag(DeleteFlag.Default.getValue());

        // family Stat us
        childInfo.setFamilyStatus(oldBase.getFamily_status().getChoice());
        childInfo.setFamilyStatusOther(oldBase.getFamily_status().getOther());

        String personColor = getColor(childIds.size() + 1);
        childInfo.setPersonColor(personColor);

        // 处理头像
        childInfo.setAvatar(initAvatar(oldBase.getChild_image()));

        if (StringUtil.isNotEmpty(oldBase.getBirth_certificate())) {
            String birthId = getUUID();
            childInfo.setBirthCert(birthId);
            familyVo.getFileList().add(initAttachInfo(birthId, oldBase.getBirth_certificate(), FilePathUtil.FAMILY_PATH, null));
        }

        familyVo.getUserList().add(childInfo);
        // account信息
        familyVo.getAccountList().add(new AccountInfo(accountId, null, null, status_info.getStatus(), createDate, this.defaultAccountId, userId));
        // 角色人
        familyVo.getRoleRelationList().add(
                new AccountRoleInfo(getUUID(), accountId, super.roleMap.get(Role.Children.getValue()), createDate, this.defaultAccountId, createDate,
                        this.defaultAccountId));
        super.repeatFactory.put(oldId, oldId);
        // 紧急联系人
        initChildConcat(userId, oldBase.getEmergency_contact(), createDate);

        initChildCustodyArrange(oldBase.getCustody_arrangement(), userId, parentIds, familyJson);
        // 疫苗
        initChildhealthMedical(userId, oldBase.getHealth_medical_info(), createDate);
        return oldBase.getFamily_name();
    }

    private void initEylf(List<OldEylfPo> eylfList, String accountId) {
        // TODO eylf
        for (OldEylfPo item : eylfList) {
            String id = item.getEylf_id();
            if (!eylfIds.contains(id)) {
                super.logList.add("waring can't found eylf--->" + formatJson(item));
                continue;
            }
            ChildEylfInfo childEylfInfo = new ChildEylfInfo();
            childEylfInfo.setId(getUUID());
            childEylfInfo.setChildId(accountId);
            childEylfInfo.setEylfCheckId(id);
            childEylfInfo.setValue(StringUtil.isEmpty(item.getValue()) ? null : Short.parseShort(item.getValue()));
            childEylfInfo.setDeleteFlag(DeleteFlag.Default.getValue());
            childEylfInfo.setReason(item.getReason());
            childEylfInfo.setAgeScope(Integer.parseInt(item.getAge_scope()));
            familyVo.getEylfList().add(childEylfInfo);
        }
    }

    // 小孩
    private void initChildConcat(String userId, List<OldConcatPo> emergency_contact, Date createDate) {
        if (ListUtil.isEmpty(emergency_contact)) {
            return;
        }
        int count = 0;
        for (OldConcatPo oldConcat : emergency_contact) {
            EmergencyContactInfo concat = new EmergencyContactInfo();
            concat.setId(getUUID());
            concat.setUserId(userId);
            concat.setCreateTime(DateUtil.addSeconds(createDate, count));
            concat.setDeleteFlag(DeleteFlag.Default.getValue());
            concat.setName(oldConcat.getName());
            concat.setWorkPhoneNumChild(oldConcat.getWork_phone_number());
            concat.setState(oldConcat.getState());
            concat.setPhoneNum(oldConcat.getMobile_phone_number());
            concat.setSuburb(oldConcat.getSuburb());
            concat.setPostcode(oldConcat.getPostcode());
            concat.setHomePhoneNumChild(oldConcat.getHome_phone_number());
            concat.setRelationshipChild(oldConcat.getRelationship());
            concat.setAddress(oldConcat.getAddress());

            concat.setPersonAuthority1(oldConcat.getCollect());
            if (StringUtil.isNotEmpty(oldConcat.getExcursion_out())) {
                concat.setPersonAuthority2(Boolean.parseBoolean(oldConcat.getExcursion_out()));
            }
            if (StringUtil.isNotEmpty(oldConcat.getMedical_treatment())) {
                concat.setPersonAuthority3(Boolean.parseBoolean(oldConcat.getMedical_treatment()));
            }
            concat.setPersonAuthority4(oldConcat.getTransportation_of_ambulance_service());
            concat.setPersonAuthority5(oldConcat.getRequest_permit_medication());
            concat.setPersonAuthority6(oldConcat.getPerson_to_be_notified());
            familyVo.getEmergencyContactList().add(concat);
            count++;
        }
    }

    private void initChildCustodyArrange(OldCustodyArrangementPo custody_arrangement, String userId, List<String> parentIds, String familyJson) {
        String parentId = StringUtil.isEmpty(custody_arrangement.getLegal_custody()) ? "" : custody_arrangement.getLegal_custody();
        if ("both".equals(parentId.toLowerCase()) || StringUtil.isEmpty(parentId)) {
            parentId = "";
        } else {
            parentId = getParentId(parentIds, parentId, familyJson);
        }
        String sourceId = getUUID();
        initFamilyFile(sourceId, custody_arrangement.getCourt_orders_attachments());
        familyVo.getCustodyArrangeList().add(
                new ChildCustodyArrangeInfo(getUUID(), userId, parentId, custody_arrangement.getCourt_orders(), sourceId, DeleteFlag.Default
                        .getValue()));
    }

    private String getParentId(List<String> parentIds, String key, String familyJson) {
        String id = "";
        try {
            int index = Integer.parseInt(key.split(" ")[1]);
            id = parentIds.get(index);
        } catch (Exception e) {
            System.err.println(familyJson);
        }
        return id;
    }

    private void initChildhealthMedical(String userId, OldHealthPo health_medical_info, Date date) {
        ChildHealthMedicalInfo healthMedical = new ChildHealthMedicalInfo();
        healthMedical.setId(getUUID());
        String sourceId = getUUID();
        healthMedical.setUserId(userId);
        healthMedical.setDeleteFlag(DeleteFlag.Default.getValue());
        healthMedical.setImmunisationFileId(sourceId);
        healthMedical.setMedicareNumber(health_medical_info.getMedicare_number());
        healthMedical.setMedicalCentreName(health_medical_info.getMedical_centre_number());
        healthMedical.setPractitionerName(health_medical_info.getMedical_practitioner_name());
        healthMedical.setPractitionerPhone(health_medical_info.getPractitioner_phone());
        healthMedical.setPractitionerAddress(health_medical_info.getPractitioner_address());
        healthMedical.setDentistName(health_medical_info.getDentist_name());
        familyVo.getHealthMedicalList().add(healthMedical);
        // 导入详情
        initHealthDetail(userId, health_medical_info, date);

        // 添加详情
        initFamilyFile(sourceId, health_medical_info.getDocuments());

    }

    private void initHealthDetail(String userId, OldHealthPo health_medical_info, Date date) {
        for (int i = 0; i < 13; i++) {
            ChildHealthMedicalDetailInfo detail = getBaseDetail(userId, i, date);
            switch (i) {
            case 0:// Hepatitis B
                initDatailValue(detail, health_medical_info.getHepatitis_birth(), health_medical_info.getHepatitis_2mo(),
                        health_medical_info.getHepatitis_4mo(), health_medical_info.getHepatitis_6mo(), health_medical_info.getHepatitis_12mo(),
                        null, null);
                break;
            case 1:// Diphtheria
                initDatailValue(detail, null, health_medical_info.getDiptheria_2mo(), health_medical_info.getDiptheria_4mo(),
                        health_medical_info.getDiptheria_6mo(), null, null, health_medical_info.getDiptheria_4yr());
                break;
            case 2:// Tetanus
                initDatailValue(detail, null, health_medical_info.getTetanus_2mo(), health_medical_info.getTetanus_4mo(),
                        health_medical_info.getTetanus_6mo(), null, null, health_medical_info.getTetanus_4yr());

                break;
            case 3:// Pertussis
                initDatailValue(detail, null, health_medical_info.getPertussis_2mo(), health_medical_info.getPertussis_4mo(),
                        health_medical_info.getPertussis_6mo(), null, null, health_medical_info.getPertussis_4yr());
                break;
            case 4:// Polio
                initDatailValue(detail, null, health_medical_info.getPolio_2mo(), health_medical_info.getPolio_4mo(),
                        health_medical_info.getPolio_6mo(), null, null, health_medical_info.getPolio_4yr());
                break;
            case 5:// Hib
                initDatailValue(detail, null, health_medical_info.getHib_2mo(), health_medical_info.getHib_4mo(), health_medical_info.getHib_6mo(),
                        health_medical_info.getHib_12mo(), null, null);
                break;
            case 6:// Pneumococcal
                initDatailValue(detail, null, health_medical_info.getPneumococcal_2mo(), health_medical_info.getPneumococcal_4mo(),
                        health_medical_info.getPneumococcal_6mo(), null, null, null);
                break;
            case 7:// Rotavirus
                initDatailValue(detail, null, health_medical_info.getRotavirus_2mo(), health_medical_info.getRotavirus_4mo(),
                        health_medical_info.getRotavirus_6mo(), null, null, null);
                break;
            case 8:// Measles
                initDatailValue(detail, null, null, null, null, health_medical_info.getMeasles(), null, null);
                break;
            case 9:// Mumps
                initDatailValue(detail, null, null, null, null, health_medical_info.getMumps_12mo(), null, null);
                break;
            case 10:// Rubella
                initDatailValue(detail, null, null, null, null, health_medical_info.getRubella_12mo(), null, null);
                break;
            case 11:// Menigococcal
                initDatailValue(detail, null, null, null, null, health_medical_info.getMeningococcal_12mo(), null, null);
                break;
            case 12:// Varicella
                initDatailValue(detail, null, null, null, null, null, health_medical_info.getVaricella_18mo(), null);
                break;
            default:
                break;
            }
            familyVo.getHealthMedicalDetailList().add(detail);
        }
    }

    private void initDatailValue(ChildHealthMedicalDetailInfo detail, Boolean birth, Boolean mo2, Boolean mo4, Boolean mo6, Boolean mo12,
            Boolean mo18, Boolean yrs4) {
        detail.setBirthCheck(birth);
        detail.setMo2Check(mo2);
        detail.setMo4Check(mo4);
        detail.setMo6Check(mo6);
        detail.setMo12Check(mo12);
        detail.setMo18Check(mo18);
        detail.setYrs4Check(yrs4);
    }

    private ChildHealthMedicalDetailInfo getBaseDetail(String userId, int index, Date date) {
        ChildHealthMedicalDetailInfo detail = new ChildHealthMedicalDetailInfo();
        detail.setId(getUUID());
        detail.setUserId(userId);
        detail.setNameIndex(index);
        detail.setCreateTime(date);
        detail.setDeleteFlag(DeleteFlag.Default.getValue());
        return detail;
    }

    // 处理家长信息
    private void initParentInfo(OldParentsPo parent, String familyId, String userId, String accountId, String familyJson, List<String> parentIds) {
        String email = parent.getEmail();
        String oldParentId = parent.getParent_id();

        // 判断邮箱是否有重复
        if (repeatFactory.containsKey(email)) {
            email += "**";
            super.logList.add("error parent email is repeat--->" + familyJson);
        }
        repeatFactory.put(email, parent.getParent_id());

        // 处理user信息
        UserInfo user = new UserInfo();
        user.setId(userId);
        user.setOldId(oldParentId);
        user.setEmail(email);
        user.setFirstName(parent.getGiven_name());
        user.setMiddleName(parent.getMiddle_name());
        user.setLastName(parent.getSurname());
        String gender = parent.getGender();
        if (StringUtil.isNotEmpty(gender)) {
            user.setGender("female".equals(parent.getGender().toLowerCase()) ? (short) 0 : (short) 1);
        }
        user.setBirthday(parseDate(parent.getDate_of_birth()));
        user.setAddress(parent.getAddress());
        user.setOccupation(parent.getOccupation());
        user.setSuburb(parent.getSuburb());
        user.setState(parent.getState());
        user.setPostcode(parent.getPostcode());
        user.setPhoneNumber(parent.getHome_phone());
        user.setWorkPhoneNumber(parent.getWork_phone());
        user.setMobileNumber(parent.getMobile_phone());
        user.setPlaceOfWork(parent.getPlace_of_work());
        user.setCreateAccountId(this.defaultAccountId);

        Date date = DateUtil.addSeconds(getDateByOldId(parent.getParent_id()), parentIds.size());

        user.setCreateTime(date);
        user.setDeleteFlag(DeleteFlag.Default.getValue());
        user.setUpdateTime(date);
        user.setUpdateAccountId(this.defaultAccountId);
        OldPreferredContactPo concat = parent.getPreferred_method_of_contact();
        if (concat != null) {
            user.setContactEmail(concat.getVia_email());
            user.setContactLetter(concat.getVia_letter());
            user.setContactPerson(concat.getIn_person());
            user.setContactPhone(concat.getVia_phone_call());
        }
        user.setUserType(UserType.Parent.getValue());
        user.setFamilyId(familyId);
        String personColor = getColor(parentIds.size() + 1);
        user.setPersonColor(personColor);

        // 出来account信息
        AccountInfo accountInfo = new AccountInfo();
        accountInfo.setId(accountId);
        accountInfo.setAccount(email);
        accountInfo.setPsw(parent.getPassword());
        accountInfo.setStatus(ArchivedStatus.UnArchived.getValue());
        accountInfo.setDeleteFlag(DeleteFlag.Default.getValue());
        accountInfo.setCreateAccountId(this.defaultAccountId);
        accountInfo.setUpdateAccountId(this.defaultAccountId);
        accountInfo.setCreateTime(date);
        accountInfo.setUpdateTime(date);
        accountInfo.setUserId(userId);

        // 处理角色信息
        AccountRoleInfo accountRoleInfo = new AccountRoleInfo();
        accountRoleInfo.setId(getUUID());
        accountRoleInfo.setAccountId(accountId);
        accountRoleInfo.setRoleId(super.roleMap.get(Role.Parent.getValue()));
        accountRoleInfo.setCreateAccountId(this.defaultAccountId);
        accountRoleInfo.setUpdateAccountId(this.defaultAccountId);
        accountRoleInfo.setCreateTime(date);
        accountRoleInfo.setUpdateTime(date);
        accountRoleInfo.setDeleteFlag(DeleteFlag.Default.getValue());

        familyVo.getUserList().add(user);
        familyVo.getAccountList().add(accountInfo);
        familyVo.getRoleRelationList().add(accountRoleInfo);
    }

    public static void main(String[] args) {
        String id = "56fa5bb10ea7b004f49f6311";
    }
}
