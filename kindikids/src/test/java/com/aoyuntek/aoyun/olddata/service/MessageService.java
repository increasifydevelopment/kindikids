package com.aoyuntek.aoyun.olddata.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.entity.po.AttachmentInfo;
import com.aoyuntek.aoyun.entity.po.MessageContentInfo;
import com.aoyuntek.aoyun.entity.po.MessageInfo;
import com.aoyuntek.aoyun.entity.po.MessageLookInfo;
import com.aoyuntek.aoyun.entity.po.NqsRelationInfo;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.ReadFlag;
import com.aoyuntek.aoyun.olddata.constants.HttpUrlConstants;
import com.aoyuntek.aoyun.olddata.po.massage.OldAttachment;
import com.aoyuntek.aoyun.olddata.po.massage.OldMessagePo;
import com.aoyuntek.aoyun.olddata.po.massage.OldRecipient;
import com.aoyuntek.aoyun.service.old.IOldDataService;
import com.aoyuntek.aoyun.uitl.FilePathUtil;
import com.aoyuntek.aoyun.uitl.ListSplit;
import com.google.gson.reflect.TypeToken;
import com.theone.json.util.JsonUtil;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

public class MessageService extends BaseService {
    private static Logger log = Logger.getLogger(MessageService.class);
    private List<NqsRelationInfo> nqs = new ArrayList<NqsRelationInfo>();
    private List<AttachmentInfo> attachmentInfos = new ArrayList<AttachmentInfo>();
    private List<MessageLookInfo> messageLookInfos = new ArrayList<MessageLookInfo>();
    private List<MessageInfo> messageInfos = new ArrayList<MessageInfo>();
    private List<MessageContentInfo> messageContentInfos = new ArrayList<MessageContentInfo>();
    List<OldMessagePo> oldmessageListR = new ArrayList<OldMessagePo>();
    HashMap<String, String> idMAp = new HashMap<String, String>();

    HashMap<String, String> acountMsgMap = new HashMap<String, String>();
    HashSet<String> set = new HashSet<String>();
    HashMap<String, String> parentMap = new HashMap<String, String>();

    int count = 0;
    boolean flag = true;
    Date createTime = null;
    int countR;
    int totalCountR;
    @Autowired
    private CentersInfoMapper centersInfoMapper1;
    @Autowired
    private IOldDataService oldDataService;
    @Autowired
    private UserInfoMapper userInfoMapper;

    @Test
    public void getList() {
        String error = "q";
        HashSet<String> arr = new HashSet<String>();
        getOrgsMap(centersInfoMapper1);
        getUserMap(userInfoMapper);
        try {
            List<OldMessagePo> oldmessageListOrical = new ArrayList<OldMessagePo>();
            List<OldMessagePo> oldmessageListO = new ArrayList<OldMessagePo>();

            log.info("importMessage start");
            String json = getHttpJson(HttpUrlConstants.MESSAGE_URL);
            oldmessageListOrical = JsonUtil.jsonToList(json, new TypeToken<List<OldMessagePo>>() {
            }.getType());
            chuliOldData(oldmessageListOrical);
            int counttt = oldmessageListR.size();

            for (OldMessagePo oldMessagePo : oldmessageListR) {
                String messageOldId = oldMessagePo.getMessage_oldId();
                error = oldMessagePo.getSend_account_id();

                if (!idMAp.containsKey(oldMessagePo.getMessage_oldId())) {
                    String id = getUUID();
                    idMAp.put(oldMessagePo.getMessage_oldId(), id);
                    if (oldMessagePo.getMessage_type() == 0) {
                        parentMap.put(oldMessagePo.getMessage_oldId(), id);
                    }
                    oldmessageListO.add(oldMessagePo);
                } else {
                    arr.add(oldMessagePo.getMessage_oldId());

                }
                if (oldMessagePo.getMessage_oldId().equals("56554e8e0ea7b016960631ce")) {
                    System.out.println("fghfhg");
                }

            }
            // Iterator<String> iterator=arr.iterator();
            // while(iterator.hasNext()){
            // log.info(iterator.next());
            // }
            importMessage(oldmessageListO, idMAp);
        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    // 初次处理OldData
    private void chuliOldData(List<OldMessagePo> oldmessageListOrical) {
        OldMessagePo oldMessagePo1 = null;
        for (OldMessagePo oldMessagePo : oldmessageListOrical) {
            if (!isHaveSendAccount(oldMessagePo)) {
                super.exceptionList.add(" ----------------- no have sendAccount   >" + formatJson(oldMessagePo));
                count++;
                continue;
            }
            if (oldMessagePo.getMessage_type() == 0) {

                acountMsgMap.put(oldMessagePo.getMessage_oldId(), oldMessagePo.getSend_account_id());
            } else {
                oldMessagePo.setMessage_oldId(getUUID());
            }
            oldmessageListR.add(oldMessagePo);
        }
    }

    // 验证发送人是否有用
    private boolean isHaveSendAccount(OldMessagePo oldMessagePo) {

        if (StringUtil.isEmpty(oldMessagePo.getSend_account_id())) {
            return false;
        }
        if (StringUtil.isEmpty(super.userMap.get(oldMessagePo.getSend_account_id()))) {
            return false;
        }
        return true;
    }

    // 导入数据库
    private void importDataBaseAndprintLog() {
        try {
            log.info("====================================start insert=====================");
            getmessageLookInfos();
             oldDataService.importMessage(this.nqs, this.attachmentInfos,
             null, this.messageInfos, this.messageContentInfos);
            ListSplit<MessageLookInfo> listSplit = new ListSplit<MessageLookInfo>();
            // 切割
            List<List<MessageLookInfo>> MessageLookInfosList = listSplit.SplitList(messageLookInfos, 10);
            for (List<MessageLookInfo> list : MessageLookInfosList) {
                System.err.println(list.size());
                 oldDataService.importMessage(null, null, list, null, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        log.info("====================================end insert=====================");

        printLog(this.log);
        log.info("oldmessageList count=" + oldmessageListR.size());
        log.info("oldmessageList no import count=" + count);
        log.info("OldRecipient count=" + totalCountR);
        log.info("OldRecipient no import count=" + countR);
        log.info(formatLog("importMessage end"));
    }

    public void importMessage(List<OldMessagePo> oldmessageList, HashMap<String, String> idMap) {

        try {

            for (OldMessagePo oldMessagePo : oldmessageList) {

                String id = idMap.get(oldMessagePo.getMessage_oldId());

                importMessageInfo(id, oldMessagePo, idMap);
                if (!flag) {
                    flag = true;
                    continue;
                }
                importNqs(id, oldMessagePo);
                importMessageLookInfos(id, oldMessagePo);
                imporstMessageContentInfos(id, oldMessagePo);
            }

            importDataBaseAndprintLog();

        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
            return;
        }

    }

    // 导入messageInfo
    private void importMessageInfo(String messageId, OldMessagePo oldMessagePo, HashMap<String, String> idMap) {
        MessageInfo messageInfo = new MessageInfo();
        String messageOldId = oldMessagePo.getMessage_oldId();

        if (oldMessagePo.getAttachments().toString().contains("filePath")) {
            String fileId = getUUID();
            messageInfo.setFileId(fileId);
            importAttachmentInfos(fileId, oldMessagePo);
        }

        messageInfo.setId(messageId);
        messageInfo.setReadFlag(oldMessagePo.getRead_flag());

        messageInfo.setSendAccountId(super.userMap.get(oldMessagePo.getSend_account_id()));
        messageInfo.setDeleteFlag(oldMessagePo.getDelete_flag());
        messageInfo.setMessageType(oldMessagePo.getMessage_type());
        if (oldMessagePo.getMessage_type() != 0) {
            if (StringUtil.isEmpty(idMap.get(oldMessagePo.getParent_message_id()))) {
                super.exceptionList.add("error can't find parentMessage" + messageOldId + "---->" + formatJson(oldMessagePo));
                count++;
                flag = false;
                return;
            }
        }

        messageInfo.setParentMessageId(idMap.get(oldMessagePo.getParent_message_id()));

        if (StringUtil.isEmpty(oldMessagePo.getCreate_time())) {
            getDateByOldId(messageOldId);
        } else {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            try {
                createTime = format.parse((oldMessagePo.getCreate_time()));
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                log.info("Date parse  error");
            }
        }

        messageInfo.setCreateTime(createTime);

        messageInfo.setSenderTime(createTime);

        messageInfo.setSubject(oldMessagePo.getSubject());
        if (StringUtil.isEmpty(super.orgMap.get(oldMessagePo.getCentres_oldid()))) {

        } else {
            messageInfo.setCentersId(super.orgMap.get(oldMessagePo.getCentres_oldid()));
        }

        messageInfo.setOldId(oldMessagePo.getMessage_oldId());

        messageInfos.add(messageInfo);

    }

    private void importNqs(String id, OldMessagePo oldMessagePo) {
        List<String> oldNqsS = null;
        Object oldNqs = oldMessagePo.getNqs();
        if (!oldMessagePo.getNqs().toString().contains("[")) {
            return;
        }
        String json = JsonUtil.objectToJson(oldMessagePo.getNqs());
        oldNqsS = JsonUtil.jsonToList(json, new TypeToken<List<String>>() {
        }.getType());
        if (ListUtil.isEmpty(oldNqsS)) {
            return;
        }

        // 转化数据类型
        if (ListUtil.isNotEmpty(oldNqsS)) {
            for (String nqc : oldNqsS) {
                NqsRelationInfo nqsRelationInfo = new NqsRelationInfo();
                String nqsId = getUUID();
                nqsRelationInfo.setId(nqsId);
                nqsRelationInfo.setObjId(id);
                nqsRelationInfo.setNqsVersion(nqc);
                nqsRelationInfo.setDeleteFlag(DeleteFlag.Default.getValue());
                nqs.add(nqsRelationInfo);

            }
        }
    }

    // 组装MessageLookInfo信息
    private void importMessageLookInfos(String messageId, OldMessagePo oldMessagePo) {
        List<OldRecipient> OldRecipients = oldMessagePo.getMessage_recipients();
        String parentMessageId = oldMessagePo.getParent_message_id();
        Date date = null;

        for (OldRecipient oldRecipient : OldRecipients) {
            totalCountR++;
            if (acountMsgMap.get(parentMessageId).equals(oldRecipient.getLook_account_id())) {
                continue;
            }

            // MessageLookInfo messageLookInfo = new MessageLookInfo();
            // String id = getUUID();
            // messageLookInfo.setMessageId(messageId);
            // messageLookInfo.setId(id);
            if (StringUtil.isEmpty(super.userMap.get(oldRecipient.getLook_account_id()))) {
                countR++;
                log.info("-------------->Recipient is not find   " + oldRecipient.getLook_account_id() + "------------->"
                        + idMAp.get(oldMessagePo.getMessage_oldId()));
                continue;

            }
            set.add(oldMessagePo.getParent_message_id() + "&&" + oldRecipient.getLook_account_id());

            // messageLookInfo.setLookReceiveTime(createTime);
            // messageLookInfo.setLookTime(createTime);
            // messageLookInfo.setReadFlag(oldMessagePo.getRead_flag());
            // messageLookInfo.setLookAccountId(super.userMap.get(oldRecipient.getLook_account_id()));
            // messageLookInfo.setDeleteFlag(DeleteFlag.Default.getValue());
            // messageLookInfo.setReadFlag(ReadFlag.Read.getValue());
            // messageLookInfos.add(messageLookInfo);

        }

    }

    private void getmessageLookInfos() {
        List<MessageLookInfo> list = new ArrayList<MessageLookInfo>();
        String[] arr = null;
        MessageLookInfo messageLookInfo = null;
        for (String str : set) {
            arr = str.split("&&");
            messageLookInfo = new MessageLookInfo();
            String id = getUUID();

            messageLookInfo.setMessageId(parentMap.get(arr[0]));
            messageLookInfo.setId(id);
            messageLookInfo.setLookReceiveTime(createTime);
            messageLookInfo.setLookTime(createTime);
            messageLookInfo.setLookAccountId(super.userMap.get(arr[1]));
            messageLookInfo.setDeleteFlag(DeleteFlag.Default.getValue());
            messageLookInfo.setReadFlag(ReadFlag.Read.getValue());
            messageLookInfos.add(messageLookInfo);

        }

    }

    // 组装附件信息
    private void importAttachmentInfos(String fileId, OldMessagePo oldMessagePo) {
        Object oldAttachments = oldMessagePo.getAttachments();
        String json = JsonUtil.objectToJson(oldAttachments);
        List<OldAttachment> oldAttachmentList = JsonUtil.jsonToList(json, new TypeToken<List<OldAttachment>>() {
        }.getType());
        for (OldAttachment oldAttachment : oldAttachmentList) {
            AttachmentInfo attachmentInfo = initAttachInfo(fileId, oldAttachment.getFilePath(), FilePathUtil.MESSAGE_PATH, null);

            attachmentInfos.add(attachmentInfo);
        }

    }

    // 组装邮件内容信息
    private void imporstMessageContentInfos(String messageId, OldMessagePo oldMessagePo) {
        String content = oldMessagePo.getContent();
        MessageContentInfo messageContentInfo = new MessageContentInfo();
        String id = getUUID();
        messageContentInfo.setId(id);
        messageContentInfo.setContent(content);
        messageContentInfo.setMessageId(messageId);
        messageContentInfos.add(messageContentInfo);

    }

}
