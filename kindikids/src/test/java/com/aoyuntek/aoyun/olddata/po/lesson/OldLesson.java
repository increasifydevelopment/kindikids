package com.aoyuntek.aoyun.olddata.po.lesson;

import java.util.List;

public class OldLesson {
    String[] photos;
    short state;
    String update_account_oldid;
    List<OldLessonInfo> lessons;
    String day;
    String center_oldid;
    boolean delete_flag;
    String newsfeed_oldid;
    String eylf_reflection;
    String lesson_oldId;
    String room_oldid;
    String update_time;
    String create_account_oldid;

    public String[] getPhotos() {
        return photos;
    }

    public void setPhotos(String[] photos) {
        this.photos = photos;
    }

    public short getState() {
        return state;
    }

    public void setState(short state) {
        this.state = state;
    }

    public String getUpdate_account_oldid() {
        return update_account_oldid;
    }

    public void setUpdate_account_oldid(String update_account_oldid) {
        this.update_account_oldid = update_account_oldid;
    }

    public List<OldLessonInfo> getLessons() {
        return lessons;
    }

    public void setLessons(List<OldLessonInfo> lessons) {
        this.lessons = lessons;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getCenter_oldid() {
        return center_oldid;
    }

    public void setCenter_oldid(String center_oldid) {
        this.center_oldid = center_oldid;
    }

    public boolean isDelete_flag() {
        return delete_flag;
    }

    public void setDelete_flag(boolean delete_flag) {
        this.delete_flag = delete_flag;
    }

    public String getNewsfeed_oldid() {
        return newsfeed_oldid;
    }

    public void setNewsfeed_oldid(String newsfeed_oldid) {
        this.newsfeed_oldid = newsfeed_oldid;
    }

    public String getEylf_reflection() {
        return eylf_reflection;
    }

    public void setEylf_reflection(String eylf_reflection) {
        this.eylf_reflection = eylf_reflection;
    }

    public String getLesson_oldId() {
        return lesson_oldId;
    }

    public void setLesson_oldId(String lesson_oldId) {
        this.lesson_oldId = lesson_oldId;
    }

    public String getRoom_oldid() {
        return room_oldid;
    }

    public void setRoom_oldid(String room_oldid) {
        this.room_oldid = room_oldid;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public String getCreate_account_oldid() {
        return create_account_oldid;
    }

    public void setCreate_account_oldid(String create_account_oldid) {
        this.create_account_oldid = create_account_oldid;
    }

}
