package com.aoyuntek.aoyun.olddata.po.programIntobslea;

import java.util.List;

public class ProgramIntobslea {

    private String evaluation_date;
    private List<String> photos;
    private Short category;
    private String child_oldid;
    private String educator_experience;
    private String update_account_oldid;
    private String day;
    private String observation;
    private String learning;
    private Short type;
    private String objective;
    private String centre_oldid;
    private String newsfeed_oldid;
    private Short state;
    private String room_oldid;
    private String update_time;
    private String create_account_oldid;
    private String oldid;
    private Boolean delete_flag;
    private String interest_name;
    private String interest_evolved;

    private String childNewId;
    private String centerNewId;
    private String roomNewId;
    private String groupNewId;
    private String createNewAccountId;
    private String updateNewAccountId;
    private Short newType;
    private String inob;
    private String inobstr;

    public String getInob() {
        return inob;
    }

    public void setInob(String inob) {
        this.inob = inob;
    }

    public String getInobstr() {
        return inobstr;
    }

    public void setInobstr(String inobstr) {
        this.inobstr = inobstr;
    }

    public String getInterest_name() {
        return interest_name;
    }

    public void setInterest_name(String interest_name) {
        this.interest_name = interest_name;
    }

    public String getInterest_evolved() {
        return interest_evolved;
    }

    public void setInterest_evolved(String interest_evolved) {
        this.interest_evolved = interest_evolved;
    }

    public Short getNewType() {
        return newType;
    }

    public void setNewType(Short newType) {
        this.newType = newType;
    }

    public String getGroupNewId() {
        return groupNewId;
    }

    public void setGroupNewId(String groupNewId) {
        this.groupNewId = groupNewId;
    }

    public String getCreateNewAccountId() {
        return createNewAccountId;
    }

    public void setCreateNewAccountId(String createNewAccountId) {
        this.createNewAccountId = createNewAccountId;
    }

    public String getUpdateNewAccountId() {
        return updateNewAccountId;
    }

    public void setUpdateNewAccountId(String updateNewAccountId) {
        this.updateNewAccountId = updateNewAccountId;
    }

    public String getChildNewId() {
        return childNewId;
    }

    public void setChildNewId(String childNewId) {
        this.childNewId = childNewId;
    }

    public String getCenterNewId() {
        return centerNewId;
    }

    public void setCenterNewId(String centerNewId) {
        this.centerNewId = centerNewId;
    }

    public String getRoomNewId() {
        return roomNewId;
    }

    public void setRoomNewId(String roomNewId) {
        this.roomNewId = roomNewId;
    }

    public String getEvaluation_date() {
        return evaluation_date;
    }

    public void setEvaluation_date(String evaluation_date) {
        this.evaluation_date = evaluation_date;
    }

    public List<String> getPhotos() {
        return photos;
    }

    public void setPhotos(List<String> photos) {
        this.photos = photos;
    }

    public Short getCategory() {
        return category;
    }

    public void setCategory(Short category) {
        this.category = category;
    }

    public String getChild_oldid() {
        return child_oldid;
    }

    public void setChild_oldid(String child_oldid) {
        this.child_oldid = child_oldid;
    }

    public String getEducator_experience() {
        return educator_experience;
    }

    public void setEducator_experience(String educator_experience) {
        this.educator_experience = educator_experience;
    }

    public String getUpdate_account_oldid() {
        return update_account_oldid;
    }

    public void setUpdate_account_oldid(String update_account_oldid) {
        this.update_account_oldid = update_account_oldid;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getLearning() {
        return learning;
    }

    public void setLearning(String learning) {
        this.learning = learning;
    }

    public Short getType() {
        return type;
    }

    public void setType(Short type) {
        this.type = type;
    }

    public String getObjective() {
        return objective;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }

    public String getCentre_oldid() {
        return centre_oldid;
    }

    public void setCentre_oldid(String centre_oldid) {
        this.centre_oldid = centre_oldid;
    }

    public String getNewsfeed_oldid() {
        return newsfeed_oldid;
    }

    public void setNewsfeed_oldid(String newsfeed_oldid) {
        this.newsfeed_oldid = newsfeed_oldid;
    }

    public Short getState() {
        return state;
    }

    public void setState(Short state) {
        this.state = state;
    }

    public String getRoom_oldid() {
        return room_oldid;
    }

    public void setRoom_oldid(String room_oldid) {
        this.room_oldid = room_oldid;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public String getCreate_account_oldid() {
        return create_account_oldid;
    }

    public void setCreate_account_oldid(String create_account_oldid) {
        this.create_account_oldid = create_account_oldid;
    }

    public String getOldid() {
        return oldid;
    }

    public void setOldid(String oldid) {
        this.oldid = oldid;
    }

    public Boolean getDelete_flag() {
        return delete_flag;
    }

    public void setDelete_flag(Boolean delete_flag) {
        this.delete_flag = delete_flag;
    }

}
