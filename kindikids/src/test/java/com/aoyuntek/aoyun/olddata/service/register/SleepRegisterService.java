package com.aoyuntek.aoyun.olddata.service.register;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.aoyuntek.aoyun.constants.SystemConstants;
import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.entity.po.program.ProgramRegisterItemInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramRegisterSleepInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramRegisterTaskInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskInstanceInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskResponsibleLogInfo;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.ProgramState;
import com.aoyuntek.aoyun.enums.RegisterServings;
import com.aoyuntek.aoyun.enums.TaskCategory;
import com.aoyuntek.aoyun.enums.TaskType;
import com.aoyuntek.aoyun.factory.TaskFactory;
import com.aoyuntek.aoyun.olddata.constants.HttpUrlConstants;
import com.aoyuntek.aoyun.olddata.po.registers.OldRegisterPo;
import com.aoyuntek.aoyun.service.old.IOldDataService;
import com.google.gson.reflect.TypeToken;
import com.theone.common.util.ServiceResult;
import com.theone.date.util.DateUtil;
import com.theone.json.util.JsonUtil;
import com.theone.string.util.StringUtil;

public class SleepRegisterService extends BaseRegisterService {
    private static Logger log = Logger.getLogger(SleepRegisterService.class);
    @Autowired
    private IOldDataService oldDataService;
    @Autowired
    private UserInfoMapper userInfoMapper;
    @Autowired
    private CentersInfoMapper centersInfoMapper;
    @Autowired
    private TaskFactory taskFactory;

    private List<ProgramRegisterTaskInfo> taskInfoList = new ArrayList<ProgramRegisterTaskInfo>();

    private List<ProgramRegisterItemInfo> registerItemList = new ArrayList<ProgramRegisterItemInfo>();

    private List<ProgramRegisterSleepInfo> sleepItemList = new ArrayList<ProgramRegisterSleepInfo>();

    private List<TaskInstanceInfo> taskInstanceList = new ArrayList<TaskInstanceInfo>();

    private int errorCount = 0;

    private List<TaskResponsibleLogInfo> responsibleList = new ArrayList<TaskResponsibleLogInfo>();

    @Test
    public void importRegisterDetail() {
        getOrgsMap(centersInfoMapper);
        getUserMap(userInfoMapper);
        List<OldRegisterPo> oldSleepList = new ArrayList<OldRegisterPo>();
        List<OldRegisterPo> oldMealList = new ArrayList<OldRegisterPo>();
        Map<String, List<OldRegisterPo>> map = new HashMap<String, List<OldRegisterPo>>();
        Map<String, List<OldRegisterPo>> mapSleep = new HashMap<String, List<OldRegisterPo>>();
        Map<String, List<OldRegisterPo>> mapMeal = new HashMap<String, List<OldRegisterPo>>();
        try {
            String jsonStrSleep = getHttpJson(HttpUrlConstants.SLEEP_DATA_URL);
            System.err.println("json ok");
            oldSleepList = JsonUtil.jsonToList(jsonStrSleep, new TypeToken<List<OldRegisterPo>>() {
            }.getType());
            mapSleep = initRegisterList(oldSleepList, TaskType.SleepRegister);

            String jsonStrMeal = getHttpJson(HttpUrlConstants.MEAL_DATA_URL);
            oldMealList = JsonUtil.jsonToList(jsonStrMeal, new TypeToken<List<OldRegisterPo>>() {
            }.getType());
            mapMeal = initRegisterList(oldMealList, TaskType.MealRegister);

            map.putAll(mapSleep);
            map.putAll(mapMeal);

            for (Map.Entry<String, List<OldRegisterPo>> entry : map.entrySet()) {
                ServiceResult<Object> result = initData(entry.getKey(), entry.getValue(), 0);
                if (!result.isSuccess()) {
                    super.exceptionList.add("list error-->" + formatJson(entry.getValue()));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        try {
          //  oldDataService.importRegister(taskInfoList, registerItemList, taskInstanceList, sleepItemList, this.responsibleList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        printLog(log);
        log.info("old oldSleepList count=" + mapSleep.size() + ",old item count=" + oldSleepList.size());
        log.info("old oldMealList count=" + mapMeal.size() + ",old item count=" + oldMealList.size());
        log.info("error count=" + errorCount);
        log.info("success count=" + taskInfoList.size() + ",success item count=" + registerItemList.size() + ",sleep count=" + sleepItemList.size());
    }

    public ServiceResult<Object> initData(String oldId, List<OldRegisterPo> list, int index) {
        ServiceResult<Object> result = new ServiceResult<Object>();
        result.setSuccess(false);
        if (index == list.size()) {
            return result;
        }
        OldRegisterPo item = list.get(index);
        TaskType taskType = TaskType.getType(item.getTaskType());
        String id = getUUID();
        result = validateData(item, true);
        if (!result.isSuccess()) {
            index++;
            initData(oldId, list, index);
            return result;
        }
        if("58933b600ea7b01c2ef79030".equals(oldId)){
            System.err.println(item.getDay());
        }
        String newCentreId = super.orgMap.get(item.getCentre_oldid());
        String newRoomId = super.orgMap.get(item.getRoom_oldid());
        String newAccountId = super.userMap.get(item.getCreate_account_oldid());
        String updateAccountId = super.userMap.get(item.getUpdate_account_oldid());
        ProgramRegisterTaskInfo taskInfo = new ProgramRegisterTaskInfo();
        taskInfo.setId(id);
        taskInfo.setCenterId(newCentreId);
        taskInfo.setRoomId(newRoomId);
        taskInfo.setType(taskType.getValue());
        taskInfo.setState(item.getState());
        Date day = parseDate(item.getDay());
        taskInfo.setDay(day);
        Date createDate = getDateByOldId(oldId);
        taskInfo.setCreateAccountId(newAccountId);
        taskInfo.setUpdateAccountId(updateAccountId);
        taskInfo.setUpdateTime(createDate);
        taskInfo.setOldId(oldId);
        taskInfo.setDeleteFlag(DeleteFlag.Default.getValue());
        taskInfoList.add(taskInfo);
        getTaskInstance(taskType, newCentreId, newRoomId, id, newAccountId, day);
        initItemsList(id, list, day);
        result.setSuccess(true);
        return result;
    }

    public ServiceResult<Object> validateData(OldRegisterPo item, boolean baseValidate) {
        ServiceResult<Object> result = new ServiceResult<Object>();
        result.setSuccess(false);
        if (!super.orgMap.containsKey(item.getCentre_oldid())) {
            result.setMsg("error can't find centreId-->" + formatJson(item));
            return result;
        }
        if (!super.orgMap.containsKey(item.getRoom_oldid())) {
            result.setMsg("error can't find roomId-->" + formatJson(item));
            return result;
        }
        if (!super.userMap.containsKey(item.getCreate_account_oldid())) {
            super.logList.add("warning can't found createAccountId-->" + formatJson(item));
        }
        if (!super.userMap.containsKey(item.getChild_oldid()) && !baseValidate) {
            result.setMsg("error can't found child-->" + formatJson(item));
            return result;
        }
        result.setSuccess(true);
        return result;
    }

    private void initItemsList(String newId, List<OldRegisterPo> list, Date day) {
        ServiceResult<Object> result = new ServiceResult<Object>();
        result.setSuccess(false);
        for (OldRegisterPo oldItem : list) {
            result = validateData(oldItem, false);
            if (!result.isSuccess()) {
                errorCount++;
                super.exceptionList.add(result.getMsg());
                continue;
            }
            String oldChildId = oldItem.getChild_oldid();
            String oldCreateAccountId = oldItem.getCreate_account_oldid();
            ProgramRegisterItemInfo newitem = new ProgramRegisterItemInfo();
            newitem.setId(getUUID());
            newitem.setChildId(super.userMap.get(oldChildId));
            newitem.setTaskId(newId);
            newitem.setServings(oldItem.getServings());
            newitem.setCreateAccountId(super.userMap.get(oldCreateAccountId));
            Date createDate = getDateByOldId(oldItem.getOldid());
            newitem.setUpdateTime(createDate);
            newitem.setDeleteFlag(DeleteFlag.Default.getValue());
            this.registerItemList.add(newitem);
            if (oldItem.getServings() == RegisterServings.Sleep.getValue()) {
                ProgramRegisterSleepInfo programRegisterSleepInfo = new ProgramRegisterSleepInfo();
                programRegisterSleepInfo.setRegisterId(newitem.getId());
                programRegisterSleepInfo.setSleepTime(comboDate(day, oldItem.getSleep_time(), createDate));
                programRegisterSleepInfo.setWokeupTime(comboDate(day, oldItem.getWokeup_time(), createDate));
                this.sleepItemList.add(programRegisterSleepInfo);
            }
        }
    }

    private void getTaskInstance(TaskType taskType, String centreId, String roomId, String valueId, String createAccountId, Date day) {
        Date[] dates = taskFactory.dealDate(taskType, centreId, day);
        createAccountId = StringUtil.isEmpty(createAccountId) ? SystemConstants.JobCreateAccountId : createAccountId;
        TaskInstanceInfo model = new TaskInstanceInfo();
        String taskInstanceId = getUUID();
        model.setId(taskInstanceId);
        model.setCentreId(centreId);
        model.setRoomId(roomId);
        model.setObjId(null);
        model.setObjType(TaskCategory.ROOM.getValue());
        model.setCreateAccountId(createAccountId);
        model.setCreateTime(new Date());
        model.setDeleteFlag(DeleteFlag.Default.getValue());
        model.setStatu(ProgramState.Complete.getValue());
        model.setTaskModelId(centreId);
        model.setTaskType(taskType.getValue());
        model.setValueId(valueId);
        model.setBeginDate(dates[0]);
        model.setEndDate(dates[1]);
        this.taskInstanceList.add(model);
        if (StringUtil.isNotEmpty(createAccountId)) {
            this.responsibleList.add(getResponsiblePerson(taskInstanceId, createAccountId));
            return;
        }
        super.logList.add("can't find update accountId taskid=" + valueId);
    }

    private Date comboDate(Date date, String time, Date baseDate) {
        if (StringUtil.isEmpty(time)) {
            return null;
        }
        if (time.contains(":") && !time.contains("pm")) {
            String[] arr = time.split(":");
            time = arr[0] + ":" + arr[arr.length - 1];
        } else if (time.contains(".")) {
            String[] arr = time.split("\\.");
            time = arr[0] + ":" + arr[arr.length - 1];
        } else if (time.contains(";")) {
            String[] arr = time.split(";");
            time = arr[0] + ":" + arr[arr.length - 1];
        } else if (time.contains("pm")) {
            time = time.replace("pm", "");
            time = time.trim();
        }
        String dateStr = DateUtil.format("yyyy-MM-dd");
        dateStr = dateStr + time + "PM";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-ddhh:mma", Locale.ENGLISH);
        try {
            return sdf.parse(dateStr);
        } catch (ParseException e) {
            System.err.println(time);
        }
        return baseDate;
    }

    public static void main(String[] args) {
        String a = "1:40 pm";
        System.err.println(a.contains("pm"));
    }
}
