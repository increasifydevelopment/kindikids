package com.aoyuntek.aoyun.olddata.po.user.staff;

import com.aoyuntek.aoyun.olddata.po.user.OldStatusPo;
import com.aoyuntek.aoyun.olddata.po.user.OldUserPo;

public class OldStaffPo {
    private OldEmploymentPo employment;
    private OldUserPo basic_info;
    private OldStaffCredentialsPo credentials;
    private OldStatusPo status;
    private OldMedicalPo medical;

    public OldStaffPo(OldEmploymentPo employment, OldUserPo basic_info, OldStaffCredentialsPo credentials, OldStatusPo status, OldMedicalPo medical) {
        super();
        this.employment = employment;
        this.basic_info = basic_info;
        this.credentials = credentials;
        this.status = status;
        this.medical = medical;
    }
    public OldStaffPo(){
        
    }

    public OldEmploymentPo getEmployment() {
        return employment;
    }

    public void setEmployment(OldEmploymentPo employment) {
        this.employment = employment;
    }

    public OldUserPo getBasic_info() {
        return basic_info;
    }

    public void setBasic_info(OldUserPo basic_info) {
        this.basic_info = basic_info;
    }

    public OldStaffCredentialsPo getCredentials() {
        return credentials;
    }

    public void setCredentials(OldStaffCredentialsPo credentials) {
        this.credentials = credentials;
    }

    public OldStatusPo getStatus() {
        return status;
    }

    public void setStatus(OldStatusPo status) {
        this.status = status;
    }

    public OldMedicalPo getMedical() {
        return medical;
    }

    public void setMedical(OldMedicalPo medical) {
        this.medical = medical;
    }

}
