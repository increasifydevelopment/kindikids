package com.aoyuntek.aoyun.olddata.po.weekly;

public class Oldweekly {
private String weekly_evaluation;
private Short  state;

private String centre_oldid;
private String create_account_oldid;
private String day;
private String room_oldid;
private String update_time;
private String oldid;
private String update_account_oldid;
public Oldweekly() {
    super();
}
public Oldweekly(String weekly_evaluation, Short state, String centre_oldid, String create_account_oldid, String day, String room_oldid, String update_time, String oldid, String update_account_oldid) {
    super();
    this.weekly_evaluation = weekly_evaluation;
    this.state = state;
    this.centre_oldid = centre_oldid;
    this.create_account_oldid = create_account_oldid;
    this.day = day;
    this.room_oldid = room_oldid;
    this.update_time = update_time;
    this.oldid = oldid;
    this.update_account_oldid = update_account_oldid;
}
public String getWeekly_evaluation() {
    return weekly_evaluation;
}
public void setWeekly_evaluation(String weekly_evaluation) {
    this.weekly_evaluation = weekly_evaluation;
}
public Short getState() {
    return state;
}
public void setState(Short state) {
    this.state = state;
}
public String getCentre_oldid() {
    return centre_oldid;
}
public void setCentre_oldid(String centre_oldid) {
    this.centre_oldid = centre_oldid;
}
public String getCreate_account_oldid() {
    return create_account_oldid;
}
public void setCreate_account_oldid(String create_account_oldid) {
    this.create_account_oldid = create_account_oldid;
}
public String getDay() {
    return day;
}
public void setDay(String day) {
    this.day = day;
}
public String getRoom_oldid() {
    return room_oldid;
}
public void setRoom_oldid(String room_oldid) {
    this.room_oldid = room_oldid;
}
public String getUpdate_time() {
    return update_time;
}
public void setUpdate_time(String update_time) {
    this.update_time = update_time;
}
public String getOldid() {
    return oldid;
}
public void setOldid(String oldid) {
    this.oldid = oldid;
}
public String getUpdate_account_oldid() {
    return update_account_oldid;
}
public void setUpdate_account_oldid(String update_account_oldid) {
    this.update_account_oldid = update_account_oldid;
}

}

