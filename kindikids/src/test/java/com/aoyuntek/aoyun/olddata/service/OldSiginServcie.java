package com.aoyuntek.aoyun.olddata.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.aoyuntek.aoyun.dao.AccountInfoMapper;
import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.RoomInfoMapper;
import com.aoyuntek.aoyun.dao.SignChildInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.entity.po.SignChildInfoWithBLOBs;
import com.aoyuntek.aoyun.entity.po.SignSelationInfo;
import com.aoyuntek.aoyun.entity.po.SignVisitorInfoWithBLOBs;
import com.aoyuntek.aoyun.entity.po.SigninInfo;
import com.aoyuntek.aoyun.entity.po.old.ChildSignin;
import com.aoyuntek.aoyun.enums.SigninType;
import com.aoyuntek.aoyun.olddata.constants.HttpUrlConstants;
import com.aoyuntek.aoyun.olddata.po.signin.StaffSignin;
import com.aoyuntek.aoyun.olddata.po.signin.VisitorSignin;
import com.aoyuntek.aoyun.service.old.IOldDataService;
import com.google.gson.reflect.TypeToken;
import com.theone.common.util.ServiceResult;
import com.theone.date.util.DateUtil;
import com.theone.json.util.JsonUtil;
import com.theone.string.util.StringUtil;

/**
 * @description 小孩签到,员工签到,访客签到
 * @author Hxzhang 2016年12月13日上午10:09:44
 */
public class OldSiginServcie extends BaseService {
    private static Logger log = Logger.getLogger(OldSiginServcie.class);

    private List<ChildSignin> childSignins = new ArrayList<ChildSignin>();
    private List<StaffSignin> staffSignins;
    private List<VisitorSignin> visitorSignins;

    @Autowired
    private IOldDataService oldDataService;
    @Autowired
    private UserInfoMapper userInfoMapper;
    @Autowired
    private CentersInfoMapper centersInfoMapper;
    @Autowired
    private RoomInfoMapper roomInfoMapper;
    @Autowired
    private AccountInfoMapper accountInfoMapper;
    @Autowired
    private SignChildInfoMapper signChildInfoMapper;

    @Test
    public void importSigninData() {
        String beginDate = DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss");
        getOrgsMap(centersInfoMapper);
        getUserMap(userInfoMapper);

        System.err.println("child start");
        Date startDate = new Date();
        log.info("CHILD SIGN START:" + DateUtil.format(startDate, "HH:mm:ss"));
        int i = 0;
        childSignins = signChildInfoMapper.getOldList();
        if (childSignins.size() <= 0) {
            boolean flag = true;
            while (flag) {
                String str = HttpUrlConstants.CHILD_SIGN_URL + i;
                String childJosn = getHttpJson(str);
                if ("[]".equals(childJosn)) {
                    flag = false;
                    continue;
                }
                List<ChildSignin> list = JsonUtil.jsonToList(childJosn, new TypeToken<List<ChildSignin>>() {
                }.getType());

                // childSignins.addAll(list);
                i++;
                System.err.println(i);
                signChildInfoMapper.insertOldBatch(list);
            }
            childSignins = signChildInfoMapper.getOldList();
        }

        operateChildSigninData(childSignins);
        System.err.println("childSignins" + childSignins.size());
        log.info("CHILD SIGN END:" + DateUtil.format(startDate, "HH:mm:ss") + "=>" + DateUtil.format(new Date(), "HH:mm:ss"));
        System.err.println("child end");
        // ===========================================================================================================================
        System.err.println("visitor start");
        String visitorSigninsJson = getHttpJson(HttpUrlConstants.VISITOR_SIGN_URL);
        operateVisitorSigninDate(visitorSigninsJson);
        System.err.println("visitor end");
        // // ===========================================================================================================================
        System.err.println("staff start");
        String staffSigninsJson = getHttpJson(HttpUrlConstants.STAFF_SIGN_URL);
        operateStaffSigninDate(staffSigninsJson);
        System.err.println("staff end");
        String endDate = DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss");
        System.err.println("^^^^^^^^^^^^^^^^^^^^" + beginDate + "=>" + endDate + "^^^^^^^^^^^^^^^^^^^^^");
    }

    private void operateChildSigninData(List<ChildSignin> childSignins) {
        log.info("==============================getChildSignins start==================================");
        Map<String, ChildSignin> in = new HashMap<String, ChildSignin>();
        Map<String, ChildSignin> out = new HashMap<String, ChildSignin>();

        int count = 0;
        for (ChildSignin c : childSignins) {
            ServiceResult<Object> result = validateChildSignData(c);
            if (!result.isSuccess()) {
                count++;
                super.exceptionList.add(result.getMsg());
                continue;
            }
            String key = c.getChild_oldId() + c.getSign_date().substring(0, 10);
            if (c.getType() == SigninType.IN.getValue()) {
                if (in.containsKey(key)) {
                    ChildSignin obj = in.get(key);
                    if (StringUtil.isEmpty(obj.getSign_in_name()) && StringUtil.isNotEmpty(c.getSign_in_name())) {
                        in.remove(key);
                        count++;
                        super.exceptionList.add("Sign in repetition :" + obj.getLogChildOldId() + " " + obj.getSign_date());
                        in.put(key, c);
                        continue;
                    }
                    if ((StringUtil.isEmpty(obj.getSign_in_name()) && StringUtil.isEmpty(c.getSign_in_name()))
                            || (StringUtil.isNotEmpty(obj.getSign_in_name()) && StringUtil.isNotEmpty(c.getSign_in_name()))) {
                        if (DateUtil.parseDate(obj.getSign_date()).after(DateUtil.parseDate(c.getSign_date()))) {
                            in.remove(key);
                            count++;
                            super.exceptionList.add("Sign in repetition :" + obj.getLogChildOldId() + " " + obj.getSign_date());
                            in.put(key, c);
                            continue;
                        }
                    }
                    count++;
                    super.exceptionList.add("Sign in repetition :" + c.getLogChildOldId() + " " + c.getSign_date());
                    continue;
                }
                in.put(key, c);
            } else {
                if (out.containsKey(key)) {
                    ChildSignin obj = out.get(key);
                    if (StringUtil.isEmpty(obj.getSign_out_name()) && StringUtil.isNotEmpty(c.getSign_out_name())) {
                        out.remove(key);
                        count++;
                        super.exceptionList.add("Sign in repetition :" + obj.getLogChildOldId() + " " + obj.getSign_date());
                        out.put(key, c);
                        continue;
                    }
                    if ((StringUtil.isEmpty(obj.getSign_out_name()) && StringUtil.isEmpty(c.getSign_out_name()))
                            || (StringUtil.isNotEmpty(obj.getSign_out_name()) && StringUtil.isNotEmpty(c.getSign_out_name()))) {
                        if (DateUtil.parseDate(obj.getSign_date()).before(DateUtil.parseDate(c.getSign_date()))) {
                            out.remove(key);
                            count++;
                            super.exceptionList.add("Sign in repetition :" + obj.getLogChildOldId() + " " + obj.getSign_date());
                            out.put(key, c);
                            continue;
                        }
                    }
                    count++;
                    super.exceptionList.add("Sign out repetition :" + c.getLogChildOldId() + " " + c.getSign_date());
                    continue;
                }
                out.put(key, c);
            }
        }
        int signinInfosCount = 0;
        int childInfoWithBLOBsCount = 0;
        int signSelationInfosCount = 0;
        for (Map.Entry<String, ChildSignin> entry : in.entrySet()) {
            List<SigninInfo> signinInfos = new ArrayList<SigninInfo>();
            List<SignChildInfoWithBLOBs> childInfoWithBLOBs = new ArrayList<SignChildInfoWithBLOBs>();
            List<SignSelationInfo> signSelationInfos = new ArrayList<SignSelationInfo>();
            ChildSignin c = entry.getValue();
            if (c.getSign_date().equals("2016-05-10 07:30")) {
                System.err.println("error");
            }

            SigninInfo signin = new SigninInfo(c.getChild_oldId(), conDate(c.getSign_date()), c.getType(), c.getCentre_oldid(), c.getRoom_oldid(),
                    conDate(c.getCreate_time()), StringUtil.isEmpty(c.getCreate_account_id()) ? null : c.getCreate_account_id(),
                    conDate(c.getSign_date()));
            signin.setId(UUID.randomUUID().toString());
            signin.setAccountId(c.getChild_oldId());
            signin.setSignDate(conDate(c.getSign_date()));
            signin.setType(c.getType());

            signinInfos.add(signin);
            ChildSignin cc = out.get(entry.getKey());
            SignChildInfoWithBLOBs bloB = new SignChildInfoWithBLOBs(c.getSunscreen_app(), conDate(c.getCreate_time()), c.getCreate_account_id(),
                    c.getSign_in_name(), cc != null ? cc.getSign_out_name() : null);
            childInfoWithBLOBs.add(bloB);
            SignSelationInfo selationInfo = new SignSelationInfo(bloB.getId(), signin.getId());
            signSelationInfos.add(selationInfo);
            if (cc != null) {
                SigninInfo signout = new SigninInfo(cc.getChild_oldId(), conDate(cc.getSign_date()), cc.getType(), cc.getCentre_oldid(),
                        cc.getRoom_oldid(), conDate(cc.getCreate_time()), StringUtil.isEmpty(cc.getCreate_account_id()) ? null
                                : cc.getCreate_account_id(), conDate(cc.getSign_date()));
                signinInfos.add(signout);
                SignSelationInfo selationInfo2 = new SignSelationInfo(bloB.getId(), signout.getId());
                signSelationInfos.add(selationInfo2);
            }
            oldDataService.importChildSign(signinInfos, childInfoWithBLOBs, signSelationInfos);
            signinInfosCount += signinInfos.size();
            childInfoWithBLOBsCount += childInfoWithBLOBs.size();
            signSelationInfosCount += signSelationInfos.size();
        }
        log.error("================ErrorChildSignStart=================");
        printLog(this.log);
        log.info("childSignins=" + childSignins.size());
        log.info("in =" + in.size());
        log.info("out =" + out.size());
        log.info("Error Data=" + count);
        log.info("signinInfosCount=" + signinInfosCount);
        log.info("childInfoWithBLOBsCount=" + childInfoWithBLOBsCount);
        log.info("signSelationInfosCount=" + signSelationInfosCount);
        log.error("childSignins=" + childSignins.size());
        log.error("in =" + in.size());
        log.error("out =" + out.size());
        log.error("Error Data=" + count);
        log.error("signinInfosCount=" + signinInfosCount);
        log.error("childInfoWithBLOBsCount=" + childInfoWithBLOBsCount);
        log.error("signSelationInfosCount=" + signSelationInfosCount);
        log.error("================ErrorChildSignEnd=================");
        log.info("==============================getChildSignins end==================================");
    }

    private ServiceResult<Object> validateChildSignData(ChildSignin c) {
        c.setLogChildOldId(c.getChild_oldId());
        ServiceResult<Object> result = new ServiceResult<Object>();
        if (StringUtil.isEmpty(c.getChild_oldId())) {
            result.setSuccess(false);
            result.setMsg("Child old id is empty==>" + c.getChild_oldId());
            return result;
        }
        String childAccountId = super.userMap.get(c.getChild_oldId());
        if (StringUtil.isEmpty(childAccountId)) {
            result.setSuccess(false);
            result.setMsg("Child new id is empty==>" + c.getChild_oldId());
            return result;
        }
        if (StringUtil.isEmpty(c.getSign_date())) {
            result.setSuccess(false);
            result.setMsg("Child sign date is empty==>" + c.getChild_oldId());
            return result;
        }
        if (StringUtil.isEmpty(c.getRoom_oldid())) {
            result.setSuccess(false);
            result.setMsg("Child old room id is empty==>" + c.getChild_oldId());
            return result;
        }
        String roomId = super.orgMap.get(c.getRoom_oldid());
        if (StringUtil.isEmpty(roomId)) {
            result.setSuccess(false);
            result.setMsg("Child new room id is empty==>" + c.getChild_oldId());
            return result;
        }
        if (StringUtil.isEmpty(c.getCentre_oldid())) {
            result.setSuccess(false);
            result.setMsg("Child old center id is empty==>" + c.getChild_oldId());
            return result;
        }
        String centerId = super.orgMap.get(c.getCentre_oldid());
        if (StringUtil.isEmpty(centerId)) {
            result.setSuccess(false);
            result.setMsg("Child new center id is empty==>" + c.getChild_oldId());
            return result;
        }
        String createUserAccountId = super.userMap.get(c.getCreate_account_id());
        if (StringUtil.isNotEmpty(c.getCreate_account_id()) && !c.getCreate_account_id().equals("System")) {
            if (StringUtil.isNotEmpty(c.getCreate_account_id()) && StringUtil.isEmpty(createUserAccountId)) {
                result.setSuccess(false);
                result.setMsg("Child create user not found==>" + c.getCreate_account_id());
                return result;
            }
        }
        c.setChild_oldId(childAccountId);
        c.setRoom_oldid(roomId);
        c.setCentre_oldid(centerId);
        c.setCreate_account_id(createUserAccountId);
        result.setSuccess(true);
        return result;
    }

    private void operateStaffSigninDate(String staffSigninsJson) {
        log.info("==============================getStaffSignins start=================================");
        Map<String, StaffSignin> in = new HashMap<String, StaffSignin>();
        Map<String, StaffSignin> out = new HashMap<String, StaffSignin>();

        staffSignins = JsonUtil.jsonToList(staffSigninsJson, new TypeToken<List<StaffSignin>>() {
        }.getType());
        int errorCount = 0;
        int successCount = 0;
        for (StaffSignin s : staffSignins) {
            ServiceResult<Object> result = validateStaffSignins(s);
            if (!result.isSuccess()) {
                errorCount++;
                super.exceptionList.add(result.getMsg());
                continue;
            }
            String key = s.getStaffId_oldid() + s.getSign_date().substring(0, 10);
            if (s.getType() == SigninType.IN.getValue()) {
                if (in.containsKey(key)) {
                    StaffSignin obj = in.get(key);
                    if (DateUtil.parseDate(obj.getSign_date()).after(DateUtil.parseDate(s.getSign_date()))) {
                        in.remove(key);
                        errorCount++;
                        super.exceptionList.add("Sign in repetition :" + obj.getStaffId_oldid() + " " + obj.getSign_date());
                        in.put(key, s);
                        continue;
                    }
                    errorCount++;
                    super.exceptionList.add("Sign in repetition :" + s.getStaffId_oldid() + " " + s.getSign_date());
                    continue;
                }
                in.put(key, s);
            } else {
                if (out.containsKey(key)) {
                    StaffSignin obj = out.get(key);

                    if (DateUtil.parseDate(obj.getSign_date()).before(DateUtil.parseDate(s.getSign_date()))) {
                        out.remove(key);
                        errorCount++;
                        super.exceptionList.add("Sign in repetition :" + obj.getStaffId_oldid() + " " + obj.getSign_date());
                        out.put(key, s);
                        continue;
                    }
                    errorCount++;
                    super.exceptionList.add("Sign out repetition :" + s.getStaffId_oldid() + " " + s.getSign_date());
                    continue;
                }
                out.put(key, s);
            }
            // if (s.getType() == SigninType.IN.getValue()) {
            // if (in.containsKey(s.getStaffId_oldid() + s.getSign_date().substring(0, 10))) {
            // errorCount++;
            // super.exceptionList.add("Sign in repetition :" + s.getStaffId_oldid() + " " + s.getSign_date());
            // continue;
            // }
            // in.put(s.getStaffId_oldid() + s.getSign_date().substring(0, 10), s);
            // } else {
            // if (out.containsKey(s.getStaffId_oldid() + s.getSign_date().substring(0, 10))) {
            // errorCount++;
            // super.exceptionList.add("Sign out repetition :" + s.getStaffId_oldid() + " " + s.getSign_date());
            // continue;
            // }
            // out.put(s.getStaffId_oldid() + s.getSign_date().substring(0, 10), s);
            // }
        }
        for (Map.Entry<String, StaffSignin> entry : in.entrySet()) {
            List<SigninInfo> signinInfos = new ArrayList<SigninInfo>();

            StaffSignin staffSignin = entry.getValue();
            StaffSignin staffSignin2 = out.get(entry.getKey());
            String accountId = super.userMap.get(staffSignin.getStaffId_oldid());
            String centerId = super.orgMap.get(staffSignin.getCentre_oldid());
            String roomId = StringUtil.isNotEmpty(staffSignin.getCentre_oldid()) ? super.orgMap.get(staffSignin.getCentre_oldid()) : null;
            signinInfos.add(new SigninInfo(accountId, conDate(staffSignin.getSign_date()), staffSignin.getType(), centerId, roomId,
                    conDate(staffSignin.getSign_date()), accountId, conDate(staffSignin.getSign_date())));

            if (staffSignin2 != null) {
                signinInfos.add(new SigninInfo(accountId, conDate(staffSignin2.getSign_date()), staffSignin2.getType(), centerId, roomId,
                        conDate(staffSignin2.getSign_date()), accountId, conDate(staffSignin2.getSign_date())));
            }

            oldDataService.importStaffSign(signinInfos);
            successCount += signinInfos.size();
        }
        log.error("========================ErrorStaffSignStart=========================");
        printLog(this.log);
        log.info("staffSignins = " + staffSignins.size());
        log.info("successCount = " + successCount);
        log.info("errorCount = " + errorCount);
        log.error("staffSignins = " + staffSignins.size());
        log.error("successCount = " + successCount);
        log.error("errorCount = " + errorCount);
        log.error("========================ErrorStaffSignEnd=========================");
        log.info("==============================getStaffSignins end==================================");
    }

    private ServiceResult<Object> validateStaffSignins(StaffSignin s) {
        if (StringUtil.isNotEmpty(s.getCentre_oldid()) && s.getCentre_oldid().equals("NA")) {
            s.setCentre_oldid(null);
        }
        if (StringUtil.isNotEmpty(s.getRoom_oldid()) && s.getRoom_oldid().equals("NA")) {
            s.setRoom_oldid(null);
        }
        ServiceResult<Object> result = new ServiceResult<Object>();
        if (StringUtil.isEmpty(s.getStaffId_oldid())) {
            result.setSuccess(false);
            result.setMsg("Staff Old Id is empty==>" + formatJson(s));
            return result;
        }
        if (StringUtil.isEmpty(s.getSign_date())) {
            result.setSuccess(false);
            result.setMsg("Staff Sign date is empty==>" + formatJson(s));
            return result;
        }
        if (StringUtil.isNotEmpty(s.getCentre_oldid()) && StringUtil.isEmpty(super.orgMap.get(s.getCentre_oldid()))) {
            result.setSuccess(false);
            result.setMsg("Staff Center New Id is empty==>" + formatJson(s));
            return result;
        }
        if (StringUtil.isNotEmpty(s.getRoom_oldid()) && StringUtil.isEmpty(super.orgMap.get(s.getRoom_oldid().trim()))) {
            result.setSuccess(false);
            result.setMsg("Staff New Room Id is empty==>" + formatJson(s));
            return result;
        }
        if (StringUtil.isEmpty(s.getStaffId_oldid())) {
            result.setSuccess(false);
            result.setMsg("Staff old account id is empty==>" + formatJson(s));
            return result;
        }
        if (StringUtil.isEmpty(super.userMap.get(s.getStaffId_oldid()))) {
            result.setSuccess(false);
            result.setMsg("Staff new account id is empty==>" + formatJson(s));
            return result;
        }

        result.setSuccess(true);
        return result;
    }

    private void operateVisitorSigninDate(String visitorSigninsJson) {
        Map<String, VisitorSignin> in = new HashMap<String, VisitorSignin>();
        Map<String, VisitorSignin> out = new HashMap<String, VisitorSignin>();
        log.info("==============================getvisitorSignins start=================================");
        visitorSignins = JsonUtil.jsonToList(visitorSigninsJson, new TypeToken<List<VisitorSignin>>() {
        }.getType());
        int errorCount = 0;
        for (VisitorSignin v : visitorSignins) {
            ServiceResult<Object> result = validateVisitorSignins(v);
            if (!result.isSuccess()) {
                errorCount++;
                super.exceptionList.add(result.getMsg());
                continue;
            }
            if (v.getType() == SigninType.IN.getValue()) {
                in.put(v.getVisitor_oldid(), v);
            } else {
                out.put(v.getVisitor_oldid(), v);
            }
        }
        for (Map.Entry<String, VisitorSignin> entry : in.entrySet()) {
            List<SigninInfo> signinInfos = new ArrayList<SigninInfo>();
            List<SignVisitorInfoWithBLOBs> signVisitorInfosInfoWithBLOBs = new ArrayList<SignVisitorInfoWithBLOBs>();
            List<SignSelationInfo> signSelationInfos = new ArrayList<SignSelationInfo>();

            VisitorSignin v = entry.getValue();
            SigninInfo signin = new SigninInfo(null, conDate(v.getSign_date()), v.getType(), v.getCentre_oldid(), null, conDate(v.getSign_date()),
                    null, conDate(v.getSign_date()));
            signinInfos.add(signin);
            VisitorSignin vv = out.get(entry.getKey());
            SignVisitorInfoWithBLOBs signVisitorInfoWithBLOBs = new SignVisitorInfoWithBLOBs(v.getFirst_name(), v.getSurname(), v.getVisit_reason(),
                    v.getOrganisation(), v.getContact_number(), conDate(v.getSign_date()), null, getColor(visitorSignins.indexOf(v)),
                    v.getSign_in_name(), vv != null ? vv.getSign_out_name() : null, v.getVisitor_oldid());
            if (vv != null) {
                SigninInfo signout = new SigninInfo(null, conDate(vv.getSign_date()), vv.getType(), vv.getCentre_oldid(), null,
                        conDate(vv.getSign_date()), null, conDate(vv.getSign_date()));
                signinInfos.add(signout);
                signSelationInfos.add(new SignSelationInfo(signVisitorInfoWithBLOBs.getId(), signout.getId()));

            }
            signVisitorInfosInfoWithBLOBs.add(signVisitorInfoWithBLOBs);
            signSelationInfos.add(new SignSelationInfo(signVisitorInfoWithBLOBs.getId(), signin.getId()));

            oldDataService.importVisitorSign(signinInfos, signVisitorInfosInfoWithBLOBs, signSelationInfos);
        }
        log.error("===================ErrorVisitorSignStart====================");
        printLog(this.log);
        log.info("visitorSignins = " + visitorSignins.size());
        log.info("inMap = " + in.size());
        log.info("outMap = " + out.size());
        log.info("errorCount = " + errorCount);
        log.info("exceptionList = " + exceptionList.size());
        log.error("visitorSignins = " + visitorSignins.size());
        log.error("inMap = " + in.size());
        log.error("outMap = " + out.size());
        log.error("errorCount = " + errorCount);
        log.error("exceptionList = " + exceptionList.size());
        log.error("===================ErrorVisitorSignEnd====================");
        log.info("==============================getvisitorSignins end==================================");
    }

    private ServiceResult<Object> validateVisitorSignins(VisitorSignin v) {
        ServiceResult<Object> result = new ServiceResult<Object>();
        if (StringUtil.isEmpty(v.getFirst_name())) {
            result.setSuccess(false);
            result.setMsg("First Name is empty==>" + formatJson(v));
            return result;
        }
        if (v.getType() == SigninType.IN.getValue() && StringUtil.isEmpty(v.getSign_date())) {
            result.setSuccess(false);
            result.setMsg("Sign In Date is empty==>" + formatJson(v));
            return result;
        }
        if (v.getType() == SigninType.OUT.getValue() && StringUtil.isEmpty(v.getSign_date())) {
            result.setSuccess(false);
            result.setMsg("Sign Out Date is empty==>" + formatJson(v));
            return result;
        }
        if (StringUtil.isEmpty(v.getCentre_oldid())) {
            result.setSuccess(false);
            result.setMsg("Old Center is empty==>" + formatJson(v));
            return result;
        }
        if (StringUtil.isEmpty(super.orgMap.get(v.getCentre_oldid()))) {
            result.setSuccess(false);
            result.setMsg("New Center is empty==>" + formatJson(v));
            return result;
        } else {
            v.setCentre_oldid(super.orgMap.get(v.getCentre_oldid()));
        }

        result.setSuccess(true);
        return result;
    }

    public static void main(String[] args) {
        String str = "2016-11-01 08:03";
        System.err.println(str.substring(0, 10));
    }

}
