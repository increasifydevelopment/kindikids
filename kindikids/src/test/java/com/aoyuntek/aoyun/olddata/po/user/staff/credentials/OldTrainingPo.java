package com.aoyuntek.aoyun.olddata.po.user.staff.credentials;

public class OldTrainingPo {
    private String date_completed;

    private String upload_certificate;

    private String course_name;

    public String getDate_completed() {
        return date_completed;
    }

    public void setDate_completed(String date_completed) {
        this.date_completed = date_completed;
    }

    public String getUpload_certificate() {
        return upload_certificate;
    }

    public void setUpload_certificate(String upload_certificate) {
        this.upload_certificate = upload_certificate;
    }

    public String getCourse_name() {
        return course_name;
    }

    public void setCourse_name(String course_name) {
        this.course_name = course_name;
    }

}
