package com.aoyuntek.aoyun.olddata.po.family.child;

import com.aoyuntek.aoyun.olddata.po.family.child.attendance.OldCentreInfoPo;
import com.aoyuntek.aoyun.olddata.po.family.child.attendance.OldCurrentSchedulePo;

public class OldAttendancePo {
    private OldCurrentSchedulePo current_schedule;

    private OldCentreInfoPo centre_info;

    public OldCurrentSchedulePo getCurrent_schedule() {
        return current_schedule;
    }

    public void setCurrent_schedule(OldCurrentSchedulePo current_schedule) {
        this.current_schedule = current_schedule;
    }

    public OldCentreInfoPo getCentre_info() {
        return centre_info;
    }

    public void setCentre_info(OldCentreInfoPo centre_info) {
        this.centre_info = centre_info;
    }

}
