package com.aoyuntek.aoyun.olddata.po.lesson;

import java.util.List;

public class OldLessonInfo {
    String lessonName;
    String resourcesNeeded;
    String objective;
    String unitName;
    String instructedBy;
    String curriculum;
    List<NqsText> nqs_text;
    String extension;
    String reflection;

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getReflection() {
        return reflection;
    }

    public void setReflection(String reflection) {
        this.reflection = reflection;
    }

    public String getLessonName() {
        return lessonName;
    }

    public void setLessonName(String lessonName) {
        this.lessonName = lessonName;
    }


    public String getResourcesNeeded() {
        return resourcesNeeded;
    }

    public void setResourcesNeeded(String resourcesNeeded) {
        this.resourcesNeeded = resourcesNeeded;
    }

    public String getObjective() {
        return objective;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getInstructedBy() {
        return instructedBy;
    }

    public void setInstructedBy(String instructedBy) {
        this.instructedBy = instructedBy;
    }

    public String getCurriculum() {
        return curriculum;
    }

    public void setCurriculum(String curriculum) {
        this.curriculum = curriculum;
    }

    public List<NqsText> getNqs_text() {
        return nqs_text;
    }

    public void setNqs_text(List<NqsText> nqs_text) {
        this.nqs_text = nqs_text;
    }

}
