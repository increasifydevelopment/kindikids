package com.aoyuntek.aoyun.olddata.po.user.staff.credentials;

public class OldSupervisorPo {
    private String upload_certificate;

    public String getUpload_certificate() {
        return upload_certificate;
    }

    public void setUpload_certificate(String upload_certificate) {
        this.upload_certificate = upload_certificate;
    }
    
}
