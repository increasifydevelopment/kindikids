package com.aoyuntek.aoyun.olddata.po.family.child.dietary;

public class OldDietaryEggsPo {
    private Boolean food_containing_eggs;
    private String other;
    private Boolean only_whole_egg;
    private Boolean only_egg_yolks;

    public String getOther() {
        return other;
    }

    public Boolean getFood_containing_eggs() {
        return food_containing_eggs;
    }

    public void setFood_containing_eggs(Boolean food_containing_eggs) {
        this.food_containing_eggs = food_containing_eggs;
    }

    public Boolean getOnly_whole_egg() {
        return only_whole_egg;
    }

    public void setOnly_whole_egg(Boolean only_whole_egg) {
        this.only_whole_egg = only_whole_egg;
    }

    public Boolean getOnly_egg_yolks() {
        return only_egg_yolks;
    }

    public void setOnly_egg_yolks(Boolean only_egg_yolks) {
        this.only_egg_yolks = only_egg_yolks;
    }

    public void setOther(String other) {
        this.other = other;
    }

}
