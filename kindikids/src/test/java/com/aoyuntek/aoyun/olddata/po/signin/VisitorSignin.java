package com.aoyuntek.aoyun.olddata.po.signin;

public class VisitorSignin {
    private Short type;
    private String visit_reason;
    private String sign_date;
    private String contact_number;
    private String sign_out_name;
    private Short state;
    private String centre_oldid;
    private String visitor_oldid;
    private String first_name;
    private String sign_in_name;
    private String organisation;
    private String surname;

    public Short getType() {
        return type;
    }

    public void setType(Short type) {
        this.type = type;
    }

    public String getVisit_reason() {
        return visit_reason;
    }

    public void setVisit_reason(String visit_reason) {
        this.visit_reason = visit_reason;
    }

    public String getSign_date() {
        return sign_date;
    }

    public void setSign_date(String sign_date) {
        this.sign_date = sign_date;
    }

    public String getContact_number() {
        return contact_number;
    }

    public void setContact_number(String contact_number) {
        this.contact_number = contact_number;
    }

    public String getSign_out_name() {
        return sign_out_name;
    }

    public void setSign_out_name(String sign_out_name) {
        this.sign_out_name = sign_out_name;
    }

    public Short getState() {
        return state;
    }

    public void setState(Short state) {
        this.state = state;
    }

    public String getCentre_oldid() {
        return centre_oldid;
    }

    public void setCentre_oldid(String centre_oldid) {
        this.centre_oldid = centre_oldid;
    }

    public String getVisitor_oldid() {
        return visitor_oldid;
    }

    public void setVisitor_oldid(String visitor_oldid) {
        this.visitor_oldid = visitor_oldid;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getSign_in_name() {
        return sign_in_name;
    }

    public void setSign_in_name(String sign_in_name) {
        this.sign_in_name = sign_in_name;
    }

    public String getOrganisation() {
        return organisation;
    }

    public void setOrganisation(String organisation) {
        this.organisation = organisation;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

}
