package com.aoyuntek.aoyun.olddata.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.aoyuntek.aoyun.dao.AccountInfoMapper;
import com.aoyuntek.aoyun.dao.AuthorityGroupInfoMapper;
import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.dao.UserStaffMapper;
import com.aoyuntek.aoyun.entity.po.AttachmentInfo;
import com.aoyuntek.aoyun.entity.po.NewsCommentInfo;
import com.aoyuntek.aoyun.entity.po.NewsContentInfo;
import com.aoyuntek.aoyun.entity.po.NewsGroupInfo;
import com.aoyuntek.aoyun.entity.po.NewsInfo;
import com.aoyuntek.aoyun.entity.po.NewsReceiveInfo;
import com.aoyuntek.aoyun.entity.po.NqsRelationInfo;
import com.aoyuntek.aoyun.entity.po.YelfNewsInfo;
import com.aoyuntek.aoyun.entity.vo.ReceiverVo;
import com.aoyuntek.aoyun.entity.vo.UserInGroupInfo;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.GroupFlag;
import com.aoyuntek.aoyun.enums.NewsStatus;
import com.aoyuntek.aoyun.olddata.constants.HttpUrlConstants;
import com.aoyuntek.aoyun.olddata.po.newsfeed.NewsGroup;
import com.aoyuntek.aoyun.olddata.po.newsfeed.NewsImage;
import com.aoyuntek.aoyun.olddata.po.newsfeed.OldNewsComment;
import com.aoyuntek.aoyun.olddata.po.newsfeed.OldNewsMain;
import com.aoyuntek.aoyun.olddata.po.newsfeed.OldNewsTag;
import com.aoyuntek.aoyun.olddata.po.newsfeed.OldNewsVo;
import com.aoyuntek.aoyun.service.IAuthGroupService;
import com.aoyuntek.aoyun.service.old.IOldDataService;
import com.aoyuntek.aoyun.uitl.FilePathUtil;
import com.google.gson.reflect.TypeToken;
import com.theone.common.util.ServiceResult;
import com.theone.json.util.JsonUtil;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

public class NewsfeedService extends BaseService {

    @Autowired
    private IOldDataService oldDataService;

    @Autowired
    private CentersInfoMapper centersInfoMapper;
    @Autowired
    private UserStaffMapper userStaffMapper;
    @Autowired
    private AuthorityGroupInfoMapper authorityGroupInfoMapper;
    @Autowired
    private IAuthGroupService authGroupService;
    @Autowired
    private AccountInfoMapper accountInfoMapper;
    @Autowired
    private UserInfoMapper userInfoMapper;

    private static Logger log = Logger.getLogger(NewsfeedService.class);

    private List<NewsInfo> NewsList = new ArrayList<NewsInfo>();
    private List<NqsRelationInfo> nqsList = new ArrayList<NqsRelationInfo>();
    private List<NewsContentInfo> contentList = new ArrayList<NewsContentInfo>();
    private List<NewsCommentInfo> commentList = new ArrayList<NewsCommentInfo>();
    private List<NewsGroupInfo> newsGroupList = new ArrayList<NewsGroupInfo>();
    private List<NewsReceiveInfo> newsReceiveInfoList = new ArrayList<NewsReceiveInfo>();
    private List<AttachmentInfo> attachmentInfos = new ArrayList<AttachmentInfo>();
    private List<YelfNewsInfo> eylfList = new ArrayList<YelfNewsInfo>();

    @Test
    public void importNewsfeed() {
        System.err.println("mystart--->" + System.currentTimeMillis());
        log.info(formatLog("importNewsfeed start"));
        int count = 0;
        List<OldNewsVo> oldNewsList = new ArrayList<OldNewsVo>();
        try {
            // 从网页获取历史数据
            String json = getHttpJson(HttpUrlConstants.NEWSFEED_URL);
            // 格式化数据
            json = formatJson(json);
            // json字符串转换成对象集合
            oldNewsList = JsonUtil.jsonToList(json, new TypeToken<List<OldNewsVo>>() {
            }.getType());
            System.out.println(oldNewsList.size());
            // 循环集合，处理数据
            for (OldNewsVo oldNewsVo : oldNewsList) {
                // 初始化新的newsId
                String newsId = getUUID();
                // newsInfo数据处理
                ServiceResult<Object> result = importNewsInfo(newsId, oldNewsVo);
                // 返回结果
                if (!result.isSuccess()) {
                    count++;
                    super.exceptionList.add(result.getMsg());
                    continue;
                }
                // 评论数据处理
                importNewsComment(newsId, oldNewsVo);
                // newsGroup数据处理
                importNewsGroup(newsId, oldNewsVo);
                // tagChildStaff数据处理
                importNewsTagChildStaff(newsId, oldNewsVo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        log.info("====================================start insert=============================");
        try {
            // 插入数据
            oldDataService.importNewsFeed(this.NewsList, this.nqsList, this.contentList, this.commentList, this.newsGroupList,
                    this.newsReceiveInfoList, this.attachmentInfos, this.eylfList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.info("====================================end insert===============================");
        
        // 打印日志
        printLog(this.log);
        // newsfeed总个数
        log.info("oldNewsList count=" + oldNewsList.size());
        // 插入未成功的newsfeed个数
        log.info("oldNewsList no import count=" + count);
        System.err.println("myend--->" + System.currentTimeMillis());
    }

    /**
     * 
     * @description importNewsTagChildStaff
     * @author mingwang
     * @create 2016年12月19日上午9:24:03
     * @param newsId
     * @param oldNewsVo
     */
    private void importNewsTagChildStaff(String newsId, OldNewsVo oldNewsVo) {
        List<OldNewsTag> newsfeed_tag_child_staff = oldNewsVo.getNewsfeed_tag_child_staff();
        if (ListUtil.isNotEmpty(newsfeed_tag_child_staff)) {
            super.logList.add("tag------------oldId=" + oldNewsVo.getNewsfeed_main().getNews_oldid());
        }
    }

    /**
     * 
     * @description importNewsGroup
     * @author mingwang
     * @create 2016年12月14日上午11:23:35
     * @param newsId
     * @param oldNewsVo
     */
    private void importNewsGroup(String newsId, OldNewsVo oldNewsVo) {
        List<NewsGroup> newsfeed_group = oldNewsVo.getNewsfeed_group();
        // 如果集合为空，记录日志
        if (ListUtil.isEmpty(newsfeed_group)) {
            return;
        }
        List<String> groupNames = new ArrayList<String>();
        // 循环集合，初始化数据
        for (NewsGroup newsGroup : newsfeed_group) {
            NewsGroupInfo newsGroupInfo = new NewsGroupInfo();
            newsGroupInfo.setId(getUUID());
            newsGroupInfo.setNewsId(newsId);
            String groupName = authorityGroupInfoMapper.getIdByGroupName(newsGroup.getGroup_name());
            if (StringUtil.isEmpty(groupName)) {
                super.logList.add("error can't find groupName----oldNewsId=" + oldNewsVo.getNewsfeed_main().getNews_oldid() + "-> ");
                continue;
            }
            newsGroupInfo.setGroupName(groupName);
            newsGroupInfo.setDeleteFlag(newsGroup.isDelete_flag() ? DeleteFlag.Delete.getValue() : DeleteFlag.Default.getValue());
            this.newsGroupList.add(newsGroupInfo);
            groupNames.add(newsGroup.getGroup_name());
        }
        List<ReceiverVo> receiverVos = getAllAccountInfo(groupNames);
        // ReceiveInfo数据处理
        importReceiveInfo(newsId, receiverVos);
    }

    private void importReceiveInfo(String newsId, List<ReceiverVo> receiverVos) {
        if (ListUtil.isEmpty(receiverVos)) {
            super.logList.add("error can't find receiverVos----oldNewsId=" + newsId + "-> ");
            return;
        }
        for (ReceiverVo receiverVo : receiverVos) {
            NewsReceiveInfo newsReceiveInfo = new NewsReceiveInfo();
            newsReceiveInfo.setId(getUUID());
            newsReceiveInfo.setNewsId(newsId);
            newsReceiveInfo.setReceiveAccountId(receiverVo.getId());
            newsReceiveInfo.setGroupFlag((short) 1);
            newsReceiveInfo.setReceiveUserName(receiverVo.getName());
            newsReceiveInfo.setDeleteFlag((short) 0);
            this.newsReceiveInfoList.add(newsReceiveInfo);
        }

    }

    /**
     * @author hxzhang copy by mingwang from NewsInfoServiceImpl
     */
    private List<ReceiverVo> getAllAccountInfo(List<String> groupName) {
        List<String> ids = new ArrayList<String>();
        // 获取所有分组
        List<String> groupNames = authGroupService.getAllGroups();
        // 获取分组及分组以外的接收对象
        List<String> receiveObjects = new ArrayList<String>(groupName);
        for (int i = receiveObjects.size() - 1; i >= 0; i--) {
            if (groupNames.contains(receiveObjects.get(i))) {
                // 通过分组获取List<AccountInfo>
                List<UserInGroupInfo> accounts = authGroupService.dealAccountListByGroup(receiveObjects.get(i));
                if (!ListUtil.isEmpty(accounts)) {
                    List<String> accountIds = new ArrayList<String>();
                    for (int j = 0; j < accounts.size(); j++) {
                        accountIds.add(accounts.get(j).getAccountId());
                        ids.addAll(accountIds);
                    }
                }
                // 删除接收对象中分组信息
                receiveObjects.remove(i);
            }
        }
        // 通过分组以外的接收对象获取List<AccountInfo>
        List<ReceiverVo> idsAndNamesOutGroup = new ArrayList<ReceiverVo>();
        if (!ListUtil.isEmpty(receiveObjects)) {
            idsAndNamesOutGroup = accountInfoMapper.getNamesByIds(receiveObjects);
        }
        List<ReceiverVo> idsAndNamesInGroup = new ArrayList<ReceiverVo>();
        if (ids.size() != 0) {
            idsAndNamesInGroup = accountInfoMapper.getNamesByIds(ids);
            for (int i = 0; i < idsAndNamesInGroup.size(); i++) {
                idsAndNamesInGroup.get(i).setGroupFlag(GroupFlag.In.getValue());
            }
        }
        List<ReceiverVo> idsAndNames = new ArrayList<ReceiverVo>();
        idsAndNames.addAll(idsAndNamesOutGroup);
        idsAndNames.addAll(idsAndNamesInGroup);
        return idsAndNames;
    }

    /**
     * 
     * @description importNewsComment
     * @author mingwang
     * @create 2016年12月14日上午11:03:12
     * @param newsId
     * @param oldNewsVo
     */
    private void importNewsComment(String newsId, OldNewsVo oldNewsVo) {
        List<OldNewsComment> newsfeed_comment = oldNewsVo.getNewsfeed_comment();
        if (ListUtil.isEmpty(newsfeed_comment)) {
            super.logList.add("error can't find newsfeed_comment----oldNewsId=" + newsId + "-> ");
            return;
        }
        // 循环初始化comment数据
        for (OldNewsComment oldNewsComment : newsfeed_comment) {
            NewsCommentInfo commentInfo = new NewsCommentInfo();
            commentInfo.setId(getUUID());
            commentInfo.setContent(oldNewsComment.getContent());
            commentInfo.setCommentAccountId(userStaffMapper.getIdByOldId(oldNewsComment.getComment_account_oldid()));
            Date newCreateTime = null;
            if (StringUtil.isEmpty(oldNewsComment.getCreate_time())) {
                newCreateTime = new Date();
            } else {
                newCreateTime = parseDateFormat(oldNewsComment.getCreate_time());
            }
            commentInfo.setCreateTime(newCreateTime);
            commentInfo.setNewsId(newsId);
            if (StringUtil.isEmpty(oldNewsComment.getUpdate_time())) {
                commentInfo.setUpdateTime(newCreateTime);
            } else {
                commentInfo.setUpdateTime(parseDate(oldNewsComment.getUpdate_time()));
            }
            commentInfo.setUpdateAccountId(userStaffMapper.getIdByOldId(oldNewsComment.getUpdate_account_oldid()));
            commentInfo.setDeleteFlag(oldNewsComment.isDelete_flag() ? DeleteFlag.Delete.getValue() : DeleteFlag.Default.getValue());
            this.commentList.add(commentInfo);
        }
    }

    /**
     * 
     * @description importNewsInfo
     * @author mingwang
     * @create 2016年12月14日上午11:03:01
     * @param newsId
     * @param oldNewsVo
     */
    private ServiceResult<Object> importNewsInfo(String newsId, OldNewsVo oldNewsVo) {
        OldNewsMain oldNewsInfo = oldNewsVo.getNewsfeed_main();
        NewsInfo newsInfo = new NewsInfo();
        ServiceResult<Object> result = new ServiceResult<Object>();
        result.setSuccess(false);
        newsInfo.setId(newsId);

        String newCenterId = "";
        // 老数据centerId不为空或者不为0，从数据库查询新的centerid
        if (StringUtil.isNotEmpty(oldNewsInfo.getCentres_oldid()) && !"0".equals(oldNewsInfo.getCentres_oldid())) {
            newCenterId = centersInfoMapper.getIdByOldId(oldNewsInfo.getCentres_oldid());
            // 数据库没有记录，返回错误信息
            if (StringUtil.isEmpty(newCenterId)) {
                result.setMsg("error can't find centresId----oldId=" + oldNewsInfo.getCentres_oldid() + "-> ");
                return result;
            }
        }
        // 老数据centerId为空或者为0，直接插入
        newsInfo.setCentersId(newCenterId);

        String newRoomId = "";
        // 老数据roomId不为空或者不为0，从数据库查询新的RoomID
        if (StringUtil.isNotEmpty(oldNewsInfo.getRoom_oldid()) && !"0".equals(oldNewsInfo.getRoom_oldid())) {
            newRoomId = centersInfoMapper.getIdByOldId(oldNewsInfo.getRoom_oldid());
            // 数据库没有记录，返回错误信息
            if (StringUtil.isEmpty(newRoomId)) {
                result.setMsg("error can't find roomId----oldId=" + oldNewsInfo.getRoom_oldid() + "-> ");
                return result;
            }
        }
        // 老数据RoomId为空或者为0，直接插入
        newsInfo.setRoomId(newRoomId);

        String createOldId = oldNewsInfo.getCreate_Account_oldid();
        // 老数据createUserId不为空，从数据库查询新的accountId
        if (StringUtil.isNotEmpty(createOldId)) {
            createOldId = userInfoMapper.getAccountIdByUserOldId(createOldId);
            // 数据库中没有记录，返回错误信息
            if (StringUtil.isEmpty(createOldId)) {
                result.setMsg("error can't find createAccountId----oldId=" + oldNewsInfo.getCreate_Account_oldid() + "-> ");
                return result;
            }
        }
        newsInfo.setCreateAccountId(createOldId);

        Date oldCreateTime = null;
        // createTime为空的话，设置为当前时间
        if (StringUtil.isEmpty(oldNewsInfo.getCreate_time())) {
            oldCreateTime = getDateByOldId(oldNewsInfo.getNews_oldid());
        } else {
            oldCreateTime = parseDateFormat(oldNewsInfo.getCreate_time());
        }

        newsInfo.setApproveAccountId(userStaffMapper.getIdByOldId(oldNewsInfo.getApprove_Account_oldid()));
        newsInfo.setUpdateAccountId(this.defaultAccountId);
        newsInfo.setNewsTag(getOldNewsTag(oldNewsInfo));
        // approveTime为空时，将createTime设置为approveTime
        if (StringUtil.isEmpty(oldNewsInfo.getApporve_time())) {
            newsInfo.setApproveTime(oldCreateTime);
        } else {
            newsInfo.setApproveTime(parseDate(oldNewsInfo.getApporve_time()));
        }
        newsInfo.setStatus(getNewsStatus(oldNewsInfo.getStatus()));
        newsInfo.setScore(oldNewsInfo.getScore());
        newsInfo.setCreateTime(oldCreateTime);
        // updateTime为空时，将createTime设置为updateTime
        if (StringUtil.isEmpty(oldNewsInfo.getUpdate_time())) {
            newsInfo.setUpdateTime(oldCreateTime);
        } else {
            newsInfo.setUpdateTime(parseDate(oldNewsInfo.getUpdate_time()));
        }
        newsInfo.setNewsType(oldNewsInfo.getNews_type() == null || "".equals(oldNewsInfo.getNews_type()) ? null : (short) Integer
                .parseInt(oldNewsInfo.getNews_type()));
        String imgId = getUUID();
        newsInfo.setImgId(imgId);
        newsInfo.setDeleteFlag(oldNewsInfo.isDelete_flag() ? DeleteFlag.Delete.getValue() : DeleteFlag.Default.getValue());
        newsInfo.setOldId(oldNewsInfo.getNews_oldid());
        this.NewsList.add(newsInfo);

        importNqs(newsId, oldNewsInfo);
        importContent(newsId, oldNewsInfo);
        importAttachment(imgId, newsId, oldNewsInfo);
        importEylf(newsId, oldNewsInfo);

        result.setSuccess(true);
        return result;
    }

    private void importEylf(String newsId, OldNewsMain oldNewsInfo) {
        Object eylf = oldNewsInfo.getEylf();
        if (eylf == null || "".equals(eylf)) {
            return;
        }
//        System.err.println(JsonUtil.objectToJson(oldNewsInfo));
        List<String> eylfs = (List<String>) eylf;
        if (ListUtil.isEmpty(eylfs)) {
            return;
        }
        for (String version : eylfs) {
            YelfNewsInfo yelfNewsInfo = new YelfNewsInfo();
            yelfNewsInfo.setId(getUUID());
            yelfNewsInfo.setNewsId(newsId);
            yelfNewsInfo.setYelfVersion(version);
            yelfNewsInfo.setDeleteFlag(DeleteFlag.Default.getValue());
            this.eylfList.add(yelfNewsInfo);
        }
    }

    /**
     * 
     * @description import newsfeed image
     * @author mingwang
     * @create 2016年12月14日上午10:49:14
     * @param imgId
     * @param newsId
     * @param oldNewsInfo
     */
    private void importAttachment(String imgId, String newsId, OldNewsMain oldNewsInfo) {
        List<NewsImage> newsfeed_image = oldNewsInfo.getNewsfeed_image();
        if (ListUtil.isEmpty(newsfeed_image)) {
            return;
        }
        for (NewsImage newsImage : newsfeed_image) {
            AttachmentInfo attachmentInfo = initAttachInfo(imgId, newsImage.getFile(), FilePathUtil.NewsFeed_PATH, null);
            this.attachmentInfos.add(attachmentInfo);
        }
    }

    /**
     * 
     * @description import news content
     * @author mingwang
     * @create 2016年12月14日上午10:36:54
     * @param newsId
     * @param oldNewsInfo
     */
    private void importContent(String newsId, OldNewsMain oldNewsInfo) {
        NewsContentInfo newsContentInfo = new NewsContentInfo();
        newsContentInfo.setId(getUUID());
        newsContentInfo.setNewsId(newsId);
        if (StringUtil.isEmpty(oldNewsInfo.getTitle()) && StringUtil.isEmpty(oldNewsInfo.getContent())) {
            super.logList.add("content is null------newsId=" + oldNewsInfo.getNews_oldid());
        }
        String content = oldNewsInfo.getTitle() + "<br/>" + oldNewsInfo.getContent();
        newsContentInfo.setContent(content);
        this.contentList.add(newsContentInfo);
    }

    /**
     * 
     * @description 处理news nqs
     * @author mingwang
     * @create 2016年12月14日上午10:32:19
     * @param newsId
     * @param oldNewsInfo
     */
    private void importNqs(String newsId, OldNewsMain oldNewsInfo) {
        String[] nqs = oldNewsInfo.getNqs();
        if (nqs.length == 0) {
            super.logList.add("error can't find nqs----oldNewsId=" + oldNewsInfo.getNews_oldid() + "-> ");
            return;
        }
        for (String version : nqs) {
            NqsRelationInfo nqsNewsInfo = new NqsRelationInfo();
            nqsNewsInfo.setId(getUUID());
            nqsNewsInfo.setObjId(newsId);
            nqsNewsInfo.setNqsVersion(version);
            nqsNewsInfo.setDeleteFlag((short) 0);
            this.nqsList.add(nqsNewsInfo);
        }
    }

    /**
     * 
     * @description 获取老数据的NewsTag
     * @author mingwang
     * @create 2016年12月14日上午9:51:22
     * @param oldNewsInfo
     * @return
     */
    private String getOldNewsTag(OldNewsMain oldNewsInfo) {
        // lesson的newsfeed中的newsTag返回的是lesson_name
        if ("3".equals(oldNewsInfo.getNews_type())) {
            // 如果此时newsTag同时存在，增加日志到logList
            if (oldNewsInfo.getNews_tag() != null) {
                super.logList.add("error: lesson_name and news_tag exist at the same time" + formatJson(oldNewsInfo) + "-> ");
            }
            String[] lessons = oldNewsInfo.getLesson_name();
            // 如果lessonName为空，记录日志
            if (lessons == null || lessons.length <= 0) {
                super.logList.add("error: lesson_name null || lesson_name's length is 0" + formatJson(oldNewsInfo) + "-> ");
                return null;
            }
            return lessons[0];
        }
        // 非lesson的newsTag直接从newsTag中获取
        String[] news_tag = oldNewsInfo.getNews_tag();
        if (news_tag == null || news_tag.length == 0) {
            return null;
        }
        String oldNewsTag = "";
        for (String string : news_tag) {
            oldNewsTag += ";" + string;
        }
        return oldNewsTag.substring(1);
    }

    /**
     * 
     * @description 将字符串中的'-'转换成'_'
     * @author mingwang
     * @create 2016年12月14日上午9:52:03
     * @param json
     * @return
     */
    private String formatJson(String json) {
        json = json.replace("newsfeed-comment", "newsfeed_comment");
        json = json.replace("newsfeed-group", "newsfeed_group");
        json = json.replace("newsfeed-tag-child-staff", "newsfeed_tag_child_staff");
        json = json.replace("\"eylf\": []", "\"eylf\": \"\"");
        json = json.replace("</br>", "<br/>");
        json = json.replace("<br>", "<br/>");
        json = json.replace("<hr>", "<hr/>");
        json = json.replace("</hr>", "<hr/>");
        return json;
    }

    // Draft and Approved
    private Short getNewsStatus(String status) {
        if (StringUtil.isEmpty(status)) {
            return null;
        }
        if (status.equals("Draft")) {
            return NewsStatus.Draft.getValue();
        }
        if (status.equals("Approved")) {
            return NewsStatus.Appreoved.getValue();
        }
        return null;
    }

    public Date parseDateFormat(String str) {

        String str1 = str.trim();
        StringBuffer sb = new StringBuffer(str1);
        int index = sb.indexOf(" ");
        str = sb.delete(index - 2, index).toString();

        SimpleDateFormat format = new SimpleDateFormat("d MMM, yyyy", Locale.ENGLISH);
        Date d1 = null;
        try {
            d1 = format.parse(str);
        } catch (ParseException e) {
            log.info("日期转换错误");
        }
        return d1;

    }
}
