package com.aoyuntek.aoyun.olddata.po.centre;

import java.util.Date;

import com.aoyuntek.framework.model.BaseModel;

public class OldCentreVo extends BaseModel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String centre_address;
    private String centre_name;
    private Short status;
    private String create_time;
    private String centre_image;
    private String centre_contact;
    private Short delete_flag;
    private String centre_id;
    private String email_address;

    public String getCentre_address() {
        return centre_address;
    }

    public void setCentre_address(String centre_address) {
        this.centre_address = centre_address;
    }

    public String getCentre_name() {
        return centre_name;
    }

    public void setCentre_name(String centre_name) {
        this.centre_name = centre_name;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getCentre_image() {
        return centre_image;
    }

    public void setCentre_image(String centre_image) {
        this.centre_image = centre_image;
    }

    public String getCentre_contact() {
        return centre_contact;
    }

    public void setCentre_contact(String centre_contact) {
        this.centre_contact = centre_contact;
    }

    public Short getDelete_flag() {
        return delete_flag;
    }

    public void setDelete_flag(Short delete_flag) {
        this.delete_flag = delete_flag;
    }

    public String getCentre_id() {
        return centre_id;
    }

    public void setCentre_id(String centre_id) {
        this.centre_id = centre_id;
    }

    public String getEmail_address() {
        return email_address;
    }

    public void setEmail_address(String email_address) {
        this.email_address = email_address;
    }

}
