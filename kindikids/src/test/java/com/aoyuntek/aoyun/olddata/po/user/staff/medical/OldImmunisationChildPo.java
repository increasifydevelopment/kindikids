package com.aoyuntek.aoyun.olddata.po.user.staff.medical;

public class OldImmunisationChildPo {
    private Boolean had_vaccine;
    private Boolean confirmed_infection;
    private String date_of_confirmed_infection;
    private String date_of_vaccine;

    public Boolean getHad_vaccine() {
        return had_vaccine;
    }

    public void setHad_vaccine(Boolean had_vaccine) {
        this.had_vaccine = had_vaccine;
    }

    public Boolean getConfirmed_infection() {
        return confirmed_infection;
    }

    public void setConfirmed_infection(Boolean confirmed_infection) {
        this.confirmed_infection = confirmed_infection;
    }

    public String getDate_of_confirmed_infection() {
        return date_of_confirmed_infection;
    }

    public void setDate_of_confirmed_infection(String date_of_confirmed_infection) {
        this.date_of_confirmed_infection = date_of_confirmed_infection;
    }

    public String getDate_of_vaccine() {
        return date_of_vaccine;
    }

    public void setDate_of_vaccine(String date_of_vaccine) {
        this.date_of_vaccine = date_of_vaccine;
    }

}
