package com.aoyuntek.aoyun.olddata.po.family.child;

public class OldConcatPo {
    private String name;

    private String work_phone_number;

    private Boolean collect;

    private Boolean request_permit_medication;

    private String state;

    private String mobile_phone_number;

    private String suburb;

    private String excursion_out;

    private String postcode;

    private String home_phone_number;

    private Short relationship;

    private String medical_treatment;

    private String address;

    private Boolean transportation_of_ambulance_service;

    private Boolean person_to_be_notified;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWork_phone_number() {
        return work_phone_number;
    }

    public void setWork_phone_number(String work_phone_number) {
        this.work_phone_number = work_phone_number;
    }

    public Boolean getCollect() {
        return collect;
    }

    public void setCollect(Boolean collect) {
        this.collect = collect;
    }

    public Boolean getRequest_permit_medication() {
        return request_permit_medication;
    }

    public void setRequest_permit_medication(Boolean request_permit_medication) {
        this.request_permit_medication = request_permit_medication;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getMobile_phone_number() {
        return mobile_phone_number;
    }

    public void setMobile_phone_number(String mobile_phone_number) {
        this.mobile_phone_number = mobile_phone_number;
    }

    public String getSuburb() {
        return suburb;
    }

    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }

    public String getExcursion_out() {
        return excursion_out;
    }

    public void setExcursion_out(String excursion_out) {
        this.excursion_out = excursion_out;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getHome_phone_number() {
        return home_phone_number;
    }

    public void setHome_phone_number(String home_phone_number) {
        this.home_phone_number = home_phone_number;
    }

    public Short getRelationship() {
        return relationship;
    }

    public void setRelationship(Short relationship) {
        this.relationship = relationship;
    }

    public String getMedical_treatment() {
        return medical_treatment;
    }

    public void setMedical_treatment(String medical_treatment) {
        this.medical_treatment = medical_treatment;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Boolean getTransportation_of_ambulance_service() {
        return transportation_of_ambulance_service;
    }

    public void setTransportation_of_ambulance_service(Boolean transportation_of_ambulance_service) {
        this.transportation_of_ambulance_service = transportation_of_ambulance_service;
    }

    public Boolean getPerson_to_be_notified() {
        return person_to_be_notified;
    }

    public void setPerson_to_be_notified(Boolean person_to_be_notified) {
        this.person_to_be_notified = person_to_be_notified;
    }

}
