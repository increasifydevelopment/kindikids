package com.aoyuntek.aoyun.olddata.po.family.parents;

public class OldPreferredContactPo {
    private Boolean in_person;

    private Boolean via_email;

    private Boolean via_phone_call;

    private Boolean via_letter;

    public Boolean getVia_letter() {
        return via_letter;
    }

    public void setVia_letter(Boolean via_letter) {
        this.via_letter = via_letter;
    }

    public Boolean getIn_person() {
        return in_person;
    }

    public void setIn_person(Boolean in_person) {
        this.in_person = in_person;
    }

    public Boolean getVia_email() {
        return via_email;
    }

    public void setVia_email(Boolean via_email) {
        this.via_email = via_email;
    }

    public Boolean getVia_phone_call() {
        return via_phone_call;
    }

    public void setVia_phone_call(Boolean via_phone_call) {
        this.via_phone_call = via_phone_call;
    }

}
