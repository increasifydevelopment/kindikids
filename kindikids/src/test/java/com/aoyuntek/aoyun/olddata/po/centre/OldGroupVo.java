package com.aoyuntek.aoyun.olddata.po.centre;

import java.util.Date;

import com.aoyuntek.framework.model.BaseModel;

public class OldGroupVo extends BaseModel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private String room_id;
    private String group_id;
    private Short status;
    private String create_time;
    private String group_name;
    private Short delete_flag;
    private String centre_id;

    public String getRoom_id() {
        return room_id;
    }

    public void setRoom_id(String room_id) {
        this.room_id = room_id;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public Short getDelete_flag() {
        return delete_flag;
    }

    public void setDelete_flag(Short delete_flag) {
        this.delete_flag = delete_flag;
    }

    public String getCentre_id() {
        return centre_id;
    }

    public void setCentre_id(String centre_id) {
        this.centre_id = centre_id;
    }

}
