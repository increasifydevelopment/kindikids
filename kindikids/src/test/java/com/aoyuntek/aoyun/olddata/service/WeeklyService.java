package com.aoyuntek.aoyun.olddata.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.aoyuntek.aoyun.constants.SystemConstants;
import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.RoomInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.dao.program.ProgramWeeklyEvalutionInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskInstanceInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskResponsibleLogInfoMapper;
import com.aoyuntek.aoyun.entity.po.program.ProgramWeeklyEvalutionInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskInstanceInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskResponsibleLogInfo;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.ProgramState;
import com.aoyuntek.aoyun.enums.ProgramWeeklyType;
import com.aoyuntek.aoyun.enums.TaskCategory;
import com.aoyuntek.aoyun.enums.TaskType;
import com.aoyuntek.aoyun.factory.TaskFactory;
import com.aoyuntek.aoyun.olddata.constants.HttpUrlConstants;
import com.aoyuntek.aoyun.olddata.po.weekly.Oldweekly;
import com.aoyuntek.aoyun.service.old.IOldDataService;
import com.aoyuntek.aoyun.service.task.ITaskInstance;
import com.google.gson.reflect.TypeToken;
import com.theone.common.util.ServiceResult;
import com.theone.json.util.JsonUtil;
import com.theone.string.util.StringUtil;

public class WeeklyService extends BaseService {
    private static Logger log = Logger.getLogger(WeeklyService.class);
    private List<ProgramWeeklyEvalutionInfo> programWeeklyEvalutionInfos = new ArrayList<ProgramWeeklyEvalutionInfo>();

    private List<TaskInstanceInfo> TaskInstanceList = new ArrayList<TaskInstanceInfo>();
    private List<TaskResponsibleLogInfo> taskResponsibleLogInfos =new ArrayList<TaskResponsibleLogInfo>();
    @Autowired
    private ProgramWeeklyEvalutionInfoMapper programWeeklyEvalutionInfoMapper;
    @Autowired
    private RoomInfoMapper roomInfoMapper;
    @Autowired
    private ITaskInstance taskInstance;
    @Autowired
    private TaskInstanceInfoMapper taskInstanceInfoMapper;
    @Autowired
    private CentersInfoMapper centersInfoMapper;
    @Autowired
    private UserInfoMapper UserInfoMapper;
    @Autowired
    private IOldDataService oldDataService;
    @Autowired
    private TaskFactory taskFactory;
    
    @Test
    public void importWeekly() {
        List<Oldweekly> oldWeeklys = new ArrayList<Oldweekly>();
        int count = 0;
        try {
            log.info("importWeekly start");
            String json = getHttpJson(HttpUrlConstants.PROGRAM_WEEKLY_URL);
            System.err.println(json);
            oldWeeklys = JsonUtil.jsonToList(json, new TypeToken<List<Oldweekly>>() {
            }.getType());
            for (Oldweekly oldweekly : oldWeeklys) {
                String id = getUUID();
                ServiceResult<Object> result = importProgramWeeklyEvalutionInfos(id, oldweekly);

                if (!result.isSuccess()) {
                    count++;
                    super.exceptionList.add(result.getMsg());
                    continue;
                }

            }
        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
            return;
        }

        try {
            log.info("====================================start insert=====================");
            oldDataService.importWeekly(this.programWeeklyEvalutionInfos, this.TaskInstanceList,this.taskResponsibleLogInfos);
            

        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        log.info("====================================end insert=====================");

        printLog(this.log);
        log.info("oldWeeklys count=" + oldWeeklys.size());
        log.info("oldWeeklys no import count=" + count);
        log.info(formatLog("importWeekly end"));

    }

    // 获取ProgramWeeklyEvalutionInfo
    private ServiceResult<Object> importProgramWeeklyEvalutionInfos(String id, Oldweekly oldweekly) {
        ProgramWeeklyEvalutionInfo programWeeklyEvalutionInfo = new ProgramWeeklyEvalutionInfo();
        ServiceResult<Object> result = new ServiceResult<Object>();
        String oldId = oldweekly.getOldid();
        result.setSuccess(false);
        String centerid = centersInfoMapper.getCenterIdByOldId(oldweekly.getCentre_oldid());
        String roomid = roomInfoMapper.getRoomIdByOldId(oldweekly.getRoom_oldid());
        String str = oldweekly.getUpdate_account_oldid();
        String updateAccountId = UserInfoMapper.getAccountIdByUserOldId(oldweekly.getUpdate_account_oldid());
        String createAccountId = UserInfoMapper.getAccountIdByUserOldId(oldweekly.getCreate_account_oldid());
        if (StringUtil.isEmpty(centerid)) {
            result.setMsg("error center is  find  " + oldId + "----------->" + formatJson(oldweekly));
            return result;
        }
        if (StringUtil.isEmpty(roomid)) {
            result.setMsg("error room is  find  " + oldId + "----------->" + formatJson(oldweekly));
            return result;
        }
        if (StringUtil.isEmpty(oldweekly.getDay())) {
            result.setMsg("error day is  null  " + oldId + "----------->" + formatJson(oldweekly));
            return result;
        }

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        Date day = null;
        try {
            day = format.parse(oldweekly.getDay());
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            log.info("日期转换错误");
        }
        programWeeklyEvalutionInfo.setId(id);
        programWeeklyEvalutionInfo.setCenterId(centerid);
        programWeeklyEvalutionInfo.setRoomId(roomid);
        programWeeklyEvalutionInfo.setDay(day);
        programWeeklyEvalutionInfo.setWeeklyEvalution(oldweekly.getWeekly_evaluation());
        if (oldweekly.getState() == null) {
            programWeeklyEvalutionInfo.setState((short) (1));
        }

        programWeeklyEvalutionInfo.setState(oldweekly.getState());

        programWeeklyEvalutionInfo.setCreateAccountId(createAccountId);
        Date update = null;
        if (StringUtil.isEmpty(oldweekly.getUpdate_time())) {
            update = getDateByOldId(oldweekly.getOldid());
        } else {
            try {
                update = format.parse(oldweekly.getUpdate_time());
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            }
        }
        programWeeklyEvalutionInfo.setUpdateTime(update);

        programWeeklyEvalutionInfo.setUpdateAccountId(updateAccountId);

        programWeeklyEvalutionInfo.setOldId(oldweekly.getOldid());
        
        programWeeklyEvalutionInfo.setDeleteFlag(DeleteFlag.Default.getValue());
        programWeeklyEvalutionInfo.setType(ProgramWeeklyType.ExploreCurriculum.getValue());
        programWeeklyEvalutionInfos.add(programWeeklyEvalutionInfo);
        importTaskInstance(TaskType.WeeklyEvalution, centerid, roomid, null, id, createAccountId, day,updateAccountId);

        result.setSuccess(true);
        ;
        return result;
    }

    // 获取TaskInstance
    private void importTaskInstance(TaskType taskType, String centreId, String roomId, String groupId, String valueId, String createAccountId, Date day,String updateAccountId) {
        addTaskInstance(taskType, centreId, roomId, groupId, null, TaskCategory.ROOM, centreId, valueId, createAccountId, day,updateAccountId);
    }

    private void addTaskInstance(TaskType taskType, String centreId, String roomId, String groupId, String obj_id, TaskCategory taskCategory, String taskModelId, String valueId, String createAccountId, Date day,String updateAccountId) {
        Date[] dates = taskFactory.dealDate(taskType, taskModelId, day);
        createTaskInstance(taskType, centreId, roomId, groupId, obj_id, taskCategory, taskModelId, valueId, createAccountId, dates[0], dates[1] , updateAccountId);
    }

    private void createTaskInstance(TaskType taskType, String centreId, String roomId, String groupId, String obj_id, TaskCategory taskCategory, String taskModelId, String valueId, String createAccountId, Date startDate, Date endDate,String updateAccountId) {
        createAccountId = StringUtil.isEmpty(createAccountId) ? SystemConstants.JobCreateAccountId : createAccountId;
        TaskInstanceInfo model = new TaskInstanceInfo();
        String taskInstanceId =getUUID();
        model.setId(taskInstanceId);
        model.setCentreId(centreId);
        model.setRoomId(roomId);
        model.setGroupId(groupId);
        model.setObjId(obj_id);
        model.setObjType(taskCategory.getValue());
        model.setCreateAccountId(createAccountId);
        model.setCreateTime(new Date());
        model.setDeleteFlag(DeleteFlag.Default.getValue());
        model.setStatu(ProgramState.Pending.getValue());
        model.setTaskModelId(taskModelId);
        model.setTaskType(taskType.getValue());
        model.setValueId(valueId);
        model.setBeginDate(startDate);
        model.setEndDate(endDate);
        TaskInstanceList.add(model);
        createTaskResponsibleLogInfo(taskInstanceId,  updateAccountId);
       

    }
    
    private void createTaskResponsibleLogInfo(String taskInstanceId, String accountId){
        if(StringUtil.isNotEmpty(accountId)){
            TaskResponsibleLogInfo logInfo = getResponsiblePerson( taskInstanceId,  accountId);
            taskResponsibleLogInfos.add(logInfo); 
        }
        else{
            log.info("---------> updateAccountId is null "+taskInstanceId);
        }
       
        
    } 

}
