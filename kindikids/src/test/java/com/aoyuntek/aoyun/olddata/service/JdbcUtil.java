package com.aoyuntek.aoyun.olddata.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.UUID;

public class JdbcUtil {
    public static Connection getConnection() {

        String username = "root";

        String password = "aoyun@2015";

        String driverClassName = "com.mysql.jdbc.Driver";

        String url = "jdbc:mysql://192.168.1.3:3306/kk_old_test?useUnicode=true&characterEncoding=UTF-8";

        Connection conn = null;

        try {

            Class.forName(driverClassName);

            conn = DriverManager.getConnection(url, username, password);

        } catch (ClassNotFoundException e) {

            e.printStackTrace();

        } catch (SQLException e) {

            e.printStackTrace();

        }
        return conn;
    }

    public static void main(String[] args) throws SQLException {
        String sql = "insert into tbl_news_content (id,news_id,content) values(?,?,?)";
        Connection conn = getConnection();
        PreparedStatement pre = conn.prepareStatement(sql);
        pre.setString(1, UUID.randomUUID().randomUUID().toString());
        pre.setString(2, "wgf");
        pre.setString(3, "o came to visit! 🐢");
        pre.execute();
    }

}
