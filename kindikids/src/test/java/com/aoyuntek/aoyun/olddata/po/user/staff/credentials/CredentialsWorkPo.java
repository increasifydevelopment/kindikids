package com.aoyuntek.aoyun.olddata.po.user.staff.credentials;

public class CredentialsWorkPo {
    private Object employer_verification;

    private String date_of_Expiry;

    private String check_number;

    private Object check_clearance_letter;

    public Object getEmployer_verification() {
        return employer_verification;
    }

    public void setEmployer_verification(String employer_verification) {
        this.employer_verification = employer_verification;
    }

    public String getDate_of_Expiry() {
        return date_of_Expiry;
    }

    public void setDate_of_Expiry(String date_of_Expiry) {
        this.date_of_Expiry = date_of_Expiry;
    }

    public String getCheck_number() {
        return check_number;
    }

    public void setCheck_number(String check_number) {
        this.check_number = check_number;
    }

    public Object getCheck_clearance_letter() {
        return check_clearance_letter;
    }

    public void setCheck_clearance_letter(String check_clearance_letter) {
        this.check_clearance_letter = check_clearance_letter;
    }

}
