package com.aoyuntek.aoyun.olddata.po.family.child.base;

import java.util.List;

import com.aoyuntek.aoyun.olddata.po.family.child.OldDocumentPo;

public class OldHealthPo {
    private Boolean polio_4mo;
    private Boolean polio_6mo;
    private Boolean tetanus_6mo;

    private Boolean rubella_12mo;
    private Boolean hepatitis_12mo;
    private Boolean hib_2mo;
    private Boolean rotavirus_4mo;
    private Boolean hib_4mo;

    private Boolean hepatitis_6mo;
    private Boolean hepatitis_4mo;

    private Boolean measles;
    private Boolean diptheria_4mo;

    private Boolean tetanus_4mo;
    private Boolean diptheria_4yr;
    private Boolean mumps_12mo;

    private Boolean polio_2mo;
    private String dentist_name;

    private Boolean pneumococcal_4mo;

    private Boolean rotavirus_6mo;
    private Boolean rotavirus_2mo;

    private Boolean pneumococcal_6mo;
    private Boolean diptheria_2mo;
    private Boolean pertussis_2mo;
    private Boolean hepatitis_2mo;
    private Boolean tetanus_2mo;

    public Boolean getTetanus_2mo() {
        return tetanus_2mo;
    }

    public void setTetanus_2mo(Boolean tetanus_2mo) {
        this.tetanus_2mo = tetanus_2mo;
    }

    private Boolean pneumococcal_2mo;

    private Boolean tetanus_4yr;
    private Boolean varicella_18mo;
    private String medical_practitioner_name;
    private String practitioner_address;
    private String medicare_number;
    private Boolean hib_12mo;
    private String medical_centre_number;
    private Boolean pertussis_6mo;
    private Boolean polio_4yr;
    private Boolean diptheria_6mo;

    private String practitioner_phone;
    private Boolean pertussis_4mo;

    private Boolean meningococcal_12mo;
    private Boolean hepatitis_birth;
    private List<OldDocumentPo> documents;
    private Boolean pertussis_4yr;
    private Boolean hib_6mo;
    private String dentist_address;

    public Boolean getPolio_4mo() {
        return polio_4mo;
    }

    public void setPolio_4mo(Boolean polio_4mo) {
        this.polio_4mo = polio_4mo;
    }

    public Boolean getPolio_6mo() {
        return polio_6mo;
    }

    public void setPolio_6mo(Boolean polio_6mo) {
        this.polio_6mo = polio_6mo;
    }

    public Boolean getTetanus_6mo() {
        return tetanus_6mo;
    }

    public void setTetanus_6mo(Boolean tetanus_6mo) {
        this.tetanus_6mo = tetanus_6mo;
    }

    public Boolean getRubella_12mo() {
        return rubella_12mo;
    }

    public void setRubella_12mo(Boolean rubella_12mo) {
        this.rubella_12mo = rubella_12mo;
    }

    public Boolean getHepatitis_12mo() {
        return hepatitis_12mo;
    }

    public void setHepatitis_12mo(Boolean hepatitis_12mo) {
        this.hepatitis_12mo = hepatitis_12mo;
    }

    public Boolean getHib_2mo() {
        return hib_2mo;
    }

    public void setHib_2mo(Boolean hib_2mo) {
        this.hib_2mo = hib_2mo;
    }

    public Boolean getRotavirus_4mo() {
        return rotavirus_4mo;
    }

    public void setRotavirus_4mo(Boolean rotavirus_4mo) {
        this.rotavirus_4mo = rotavirus_4mo;
    }

    public Boolean getHib_4mo() {
        return hib_4mo;
    }

    public void setHib_4mo(Boolean hib_4mo) {
        this.hib_4mo = hib_4mo;
    }

    public Boolean getHepatitis_6mo() {
        return hepatitis_6mo;
    }

    public void setHepatitis_6mo(Boolean hepatitis_6mo) {
        this.hepatitis_6mo = hepatitis_6mo;
    }

    public Boolean getHepatitis_4mo() {
        return hepatitis_4mo;
    }

    public void setHepatitis_4mo(Boolean hepatitis_4mo) {
        this.hepatitis_4mo = hepatitis_4mo;
    }

    public Boolean getMeasles() {
        return measles;
    }

    public void setMeasles(Boolean measles) {
        this.measles = measles;
    }

    public Boolean getDiptheria_4mo() {
        return diptheria_4mo;
    }

    public void setDiptheria_4mo(Boolean diptheria_4mo) {
        this.diptheria_4mo = diptheria_4mo;
    }

    public Boolean getTetanus_4mo() {
        return tetanus_4mo;
    }

    public void setTetanus_4mo(Boolean tetanus_4mo) {
        this.tetanus_4mo = tetanus_4mo;
    }

    public Boolean getDiptheria_4yr() {
        return diptheria_4yr;
    }

    public void setDiptheria_4yr(Boolean diptheria_4yr) {
        this.diptheria_4yr = diptheria_4yr;
    }

    public Boolean getMumps_12mo() {
        return mumps_12mo;
    }

    public void setMumps_12mo(Boolean mumps_12mo) {
        this.mumps_12mo = mumps_12mo;
    }

    public Boolean getPolio_2mo() {
        return polio_2mo;
    }

    public void setPolio_2mo(Boolean polio_2mo) {
        this.polio_2mo = polio_2mo;
    }

    public String getDentist_name() {
        return dentist_name;
    }

    public void setDentist_name(String dentist_name) {
        this.dentist_name = dentist_name;
    }

    public Boolean getPneumococcal_4mo() {
        return pneumococcal_4mo;
    }

    public void setPneumococcal_4mo(Boolean pneumococcal_4mo) {
        this.pneumococcal_4mo = pneumococcal_4mo;
    }

    public Boolean getRotavirus_6mo() {
        return rotavirus_6mo;
    }

    public void setRotavirus_6mo(Boolean rotavirus_6mo) {
        this.rotavirus_6mo = rotavirus_6mo;
    }

    public Boolean getRotavirus_2mo() {
        return rotavirus_2mo;
    }

    public void setRotavirus_2mo(Boolean rotavirus_2mo) {
        this.rotavirus_2mo = rotavirus_2mo;
    }

    public Boolean getPneumococcal_6mo() {
        return pneumococcal_6mo;
    }

    public void setPneumococcal_6mo(Boolean pneumococcal_6mo) {
        this.pneumococcal_6mo = pneumococcal_6mo;
    }

    public Boolean getDiptheria_2mo() {
        return diptheria_2mo;
    }

    public void setDiptheria_2mo(Boolean diptheria_2mo) {
        this.diptheria_2mo = diptheria_2mo;
    }

    public Boolean getPertussis_2mo() {
        return pertussis_2mo;
    }

    public void setPertussis_2mo(Boolean pertussis_2mo) {
        this.pertussis_2mo = pertussis_2mo;
    }

    public Boolean getHepatitis_2mo() {
        return hepatitis_2mo;
    }

    public void setHepatitis_2mo(Boolean hepatitis_2mo) {
        this.hepatitis_2mo = hepatitis_2mo;
    }

    public Boolean getPneumococcal_2mo() {
        return pneumococcal_2mo;
    }

    public void setPneumococcal_2mo(Boolean pneumococcal_2mo) {
        this.pneumococcal_2mo = pneumococcal_2mo;
    }

    public Boolean getTetanus_4yr() {
        return tetanus_4yr;
    }

    public void setTetanus_4yr(Boolean tetanus_4yr) {
        this.tetanus_4yr = tetanus_4yr;
    }

    public Boolean getVaricella_18mo() {
        return varicella_18mo;
    }

    public void setVaricella_18mo(Boolean varicella_18mo) {
        this.varicella_18mo = varicella_18mo;
    }

    public String getMedical_practitioner_name() {
        return medical_practitioner_name;
    }

    public void setMedical_practitioner_name(String medical_practitioner_name) {
        this.medical_practitioner_name = medical_practitioner_name;
    }

    public String getPractitioner_address() {
        return practitioner_address;
    }

    public void setPractitioner_address(String practitioner_address) {
        this.practitioner_address = practitioner_address;
    }

    public String getMedicare_number() {
        return medicare_number;
    }

    public void setMedicare_number(String medicare_number) {
        this.medicare_number = medicare_number;
    }

    public Boolean getHib_12mo() {
        return hib_12mo;
    }

    public void setHib_12mo(Boolean hib_12mo) {
        this.hib_12mo = hib_12mo;
    }

    public String getMedical_centre_number() {
        return medical_centre_number;
    }

    public void setMedical_centre_number(String medical_centre_number) {
        this.medical_centre_number = medical_centre_number;
    }

    public Boolean getPertussis_6mo() {
        return pertussis_6mo;
    }

    public void setPertussis_6mo(Boolean pertussis_6mo) {
        this.pertussis_6mo = pertussis_6mo;
    }

    public Boolean getPolio_4yr() {
        return polio_4yr;
    }

    public void setPolio_4yr(Boolean polio_4yr) {
        this.polio_4yr = polio_4yr;
    }

    public Boolean getDiptheria_6mo() {
        return diptheria_6mo;
    }

    public void setDiptheria_6mo(Boolean diptheria_6mo) {
        this.diptheria_6mo = diptheria_6mo;
    }

    public String getPractitioner_phone() {
        return practitioner_phone;
    }

    public void setPractitioner_phone(String practitioner_phone) {
        this.practitioner_phone = practitioner_phone;
    }

    public Boolean getPertussis_4mo() {
        return pertussis_4mo;
    }

    public void setPertussis_4mo(Boolean pertussis_4mo) {
        this.pertussis_4mo = pertussis_4mo;
    }

    public Boolean getMeningococcal_12mo() {
        return meningococcal_12mo;
    }

    public void setMeningococcal_12mo(Boolean meningococcal_12mo) {
        this.meningococcal_12mo = meningococcal_12mo;
    }

    public Boolean getHepatitis_birth() {
        return hepatitis_birth;
    }

    public void setHepatitis_birth(Boolean hepatitis_birth) {
        this.hepatitis_birth = hepatitis_birth;
    }

    public List<OldDocumentPo> getDocuments() {
        return documents;
    }

    public void setDocuments(List<OldDocumentPo> documents) {
        this.documents = documents;
    }

    public Boolean getPertussis_4yr() {
        return pertussis_4yr;
    }

    public void setPertussis_4yr(Boolean pertussis_4yr) {
        this.pertussis_4yr = pertussis_4yr;
    }

    public Boolean getHib_6mo() {
        return hib_6mo;
    }

    public void setHib_6mo(Boolean hib_6mo) {
        this.hib_6mo = hib_6mo;
    }

    public String getDentist_address() {
        return dentist_address;
    }

    public void setDentist_address(String dentist_address) {
        this.dentist_address = dentist_address;
    }

}
