package com.aoyuntek.aoyun.olddata.service.register;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import com.aoyuntek.aoyun.constants.SystemConstants;
import com.aoyuntek.aoyun.entity.po.program.ProgramRegisterSleepInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramRegisterTaskInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskInstanceInfo;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.ProgramState;
import com.aoyuntek.aoyun.enums.TaskCategory;
import com.aoyuntek.aoyun.enums.TaskType;
import com.aoyuntek.aoyun.olddata.po.registers.OldRegisterPo;
import com.aoyuntek.aoyun.olddata.service.BaseService;
import com.theone.string.util.StringUtil;

public class BaseRegisterService extends BaseService {

    /**
     * 
     * @description
     * @author gfwang
     * @create 2016年12月23日下午12:43:36
     * @version 1.0
     * @param oldList
     * @return
     */
    public Map<String, List<OldRegisterPo>> initRegisterList(List<OldRegisterPo> oldList, TaskType taskType) {
        Map<String, List<OldRegisterPo>> map = new HashMap<String, List<OldRegisterPo>>();
        for (OldRegisterPo item : oldList) {
            if (taskType != null) {
                item.setTaskType(taskType.getValue());
            }
            String oldId = item.getOldid();
            if (!map.containsKey(oldId)) {
                List<OldRegisterPo> temp = new ArrayList<OldRegisterPo>();
                temp.add(item);
                map.put(oldId, temp);
                continue;
            }
            List<OldRegisterPo> temp = map.get(oldId);
            temp.add(item);
            map.put(oldId, temp);
        }
        return map;
    }

    public Date parseDate(String dateStr) {
        String[] freakyFormat = { "dd'st' MMM,yyyy", "dd'nd' MMM, yyyy", "dd'th' MMM, yyyy", "dd'rd' MMM, yyyy", "yyyy-MM-dd HH:mm" };
        Date date = null;
        for (String formatStr : freakyFormat) {
            boolean isOk = true;
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formatStr, Locale.ENGLISH);
            try {
                date = simpleDateFormat.parse(dateStr);
            } catch (ParseException e) {
                isOk = false;
            }
            if (isOk) {
                break;
            }
        }
        return date;
    }

}
