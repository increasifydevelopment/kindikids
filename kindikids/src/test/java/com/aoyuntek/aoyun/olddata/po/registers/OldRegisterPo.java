package com.aoyuntek.aoyun.olddata.po.registers;

public class OldRegisterPo {
    private String wokeup_time;

    private String centre_oldid;

    private String update_time;

    private String update_account_oldid;

    private String newsfeed_oldid;

    private String create_account_oldid;

    private String room_oldid;

    private Short state;

    private Short servings;

    private Boolean delete_flag;

    private String oldid;

    private String day;

    private String child_oldid;

    private String sleep_time;

    private short taskType;

    public short getTaskType() {
        return taskType;
    }

    public void setTaskType(short taskType) {
        this.taskType = taskType;
    }

    public String getWokeup_time() {
        return wokeup_time;
    }

    public void setWokeup_time(String wokeup_time) {
        this.wokeup_time = wokeup_time;
    }

    public String getCentre_oldid() {
        return centre_oldid;
    }

    public void setCentre_oldid(String centre_oldid) {
        this.centre_oldid = centre_oldid;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public String getUpdate_account_oldid() {
        return update_account_oldid;
    }

    public void setUpdate_account_oldid(String update_account_oldid) {
        this.update_account_oldid = update_account_oldid;
    }

    public String getNewsfeed_oldid() {
        return newsfeed_oldid;
    }

    public void setNewsfeed_oldid(String newsfeed_oldid) {
        this.newsfeed_oldid = newsfeed_oldid;
    }

    public String getCreate_account_oldid() {
        return create_account_oldid;
    }

    public void setCreate_account_oldid(String create_account_oldid) {
        this.create_account_oldid = create_account_oldid;
    }

    public String getRoom_oldid() {
        return room_oldid;
    }

    public void setRoom_oldid(String room_oldid) {
        this.room_oldid = room_oldid;
    }

    public Short getState() {
        return state;
    }

    public void setState(Short state) {
        this.state = state;
    }

    public Short getServings() {
        return servings;
    }

    public void setServings(Short servings) {
        this.servings = servings;
    }

    public Boolean getDelete_flag() {
        return delete_flag;
    }

    public void setDelete_flag(Boolean delete_flag) {
        this.delete_flag = delete_flag;
    }

    public String getOldid() {
        return oldid;
    }

    public void setOldid(String oldid) {
        this.oldid = oldid;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getChild_oldid() {
        return child_oldid;
    }

    public void setChild_oldid(String child_oldid) {
        this.child_oldid = child_oldid;
    }

    public String getSleep_time() {
        return sleep_time;
    }

    public void setSleep_time(String sleep_time) {
        this.sleep_time = sleep_time;
    }

}
