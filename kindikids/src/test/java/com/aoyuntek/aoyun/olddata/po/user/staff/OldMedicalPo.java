package com.aoyuntek.aoyun.olddata.po.user.staff;

import com.aoyuntek.aoyun.olddata.po.user.staff.medical.OldImmunisationPo;
import com.aoyuntek.aoyun.olddata.po.user.staff.medical.OldMedicalChildPo;

public class OldMedicalPo {
    private OldImmunisationPo immunisation;
    private OldMedicalChildPo medical;

    public OldImmunisationPo getImmunisation() {
        return immunisation;
    }

    public void setImmunisation(OldImmunisationPo immunisation) {
        this.immunisation = immunisation;
    }

    public OldMedicalChildPo getMedical() {
        return medical;
    }

    public void setMedical(OldMedicalChildPo medical) {
        this.medical = medical;
    }

}
