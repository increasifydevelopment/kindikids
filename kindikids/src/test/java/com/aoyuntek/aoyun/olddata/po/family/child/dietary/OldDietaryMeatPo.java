package com.aoyuntek.aoyun.olddata.po.family.child.dietary;

public class OldDietaryMeatPo {
    private String other;
    private Boolean lamb;
    private Boolean chicken;
    private Boolean pork;
    private Boolean fish_fillet;
    private Boolean beef;

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public Boolean getLamb() {
        return lamb;
    }

    public void setLamb(Boolean lamb) {
        this.lamb = lamb;
    }

    public Boolean getChicken() {
        return chicken;
    }

    public void setChicken(Boolean chicken) {
        this.chicken = chicken;
    }

    public Boolean getPork() {
        return pork;
    }

    public void setPork(Boolean pork) {
        this.pork = pork;
    }

    public Boolean getFish_fillet() {
        return fish_fillet;
    }

    public void setFish_fillet(Boolean fish_fillet) {
        this.fish_fillet = fish_fillet;
    }

    public Boolean getBeef() {
        return beef;
    }

    public void setBeef(Boolean beef) {
        this.beef = beef;
    }

}
