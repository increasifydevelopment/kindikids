package com.aoyuntek.aoyun.olddata.po.newsfeed;

public class NewsImage {

    private String attach_name;
    private String file;
    
    public String getAttach_name() {
        return attach_name;
    }
    public void setAttach_name(String attach_name) {
        this.attach_name = attach_name;
    }
    public String getFile() {
        return file;
    }
    public void setFile(String file) {
        this.file = file;
    }
    
}
