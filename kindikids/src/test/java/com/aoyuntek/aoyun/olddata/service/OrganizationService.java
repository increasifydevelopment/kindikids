package com.aoyuntek.aoyun.olddata.service;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.RoomGroupInfoMapper;
import com.aoyuntek.aoyun.dao.RoomInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.entity.po.CentersInfo;
import com.aoyuntek.aoyun.entity.po.RoomGroupInfo;
import com.aoyuntek.aoyun.entity.po.RoomInfo;
import com.aoyuntek.aoyun.olddata.constants.HttpUrlConstants;
import com.aoyuntek.aoyun.olddata.po.centre.OldCentreVo;
import com.aoyuntek.aoyun.olddata.po.centre.OldGroupVo;
import com.aoyuntek.aoyun.olddata.po.centre.OldRoomVo;
import com.aoyuntek.aoyun.service.IAuthGroupService;
import com.aoyuntek.aoyun.service.ICenterService;
import com.aoyuntek.aoyun.uitl.FilePathUtil;
import com.google.gson.reflect.TypeToken;
import com.theone.json.util.JsonUtil;
import com.theone.string.util.StringUtil;

/**
 * 
 * @description 导入 centre,room,group信息
 * @author gfwang
 * @create 2016年12月5日上午9:58:19
 * @version 1.0
 */
public class OrganizationService extends BaseService {
    private static Logger log = Logger.getLogger(OrganizationService.class);

    private List<OldCentreVo> oldCentreList;

    private List<OldRoomVo> oldRoomList;

    private List<OldGroupVo> oldGroupList;

    @Autowired
    private CentersInfoMapper centersInfoMapper;

    @Autowired
    private RoomInfoMapper roomInfoMapper;

    @Autowired
    private RoomGroupInfoMapper groupInfoMapper;

    private Map<String, String> ids = new HashMap<String, String>();

    @Autowired
    private IAuthGroupService authGroupService;

    @Autowired
    private ICenterService centerService;

    @Autowired
    private UserInfoMapper userInfoMapper;
    
    @Test
    public void importCentre() {
        try {
            getOrgsMap(centersInfoMapper);
            getUserMap(userInfoMapper);
            String centreJson = getHttpJson(HttpUrlConstants.CENTRE_URL);
            String roomJson = getHttpJson(HttpUrlConstants.ROOM_URL);
            String groupJson = getHttpJson(HttpUrlConstants.GROUP_URL);
            oldCentreList = JsonUtil.jsonToList(centreJson, new TypeToken<List<OldCentreVo>>() {
            }.getType());
            log.info("old centreList count=" + oldCentreList.size());
            oldRoomList = JsonUtil.jsonToList(roomJson, new TypeToken<List<OldRoomVo>>() {
            }.getType());
            log.info("old roomList count=" + oldRoomList.size());
            oldGroupList = JsonUtil.jsonToList(groupJson, new TypeToken<List<OldGroupVo>>() {
            }.getType());
            log.info("old groupList count=" + oldGroupList.size());

            log.info("----------------import centre start----------------");
            int count = 0;
            for (OldCentreVo oldCentre : oldCentreList) {
                CentersInfo centerInfo = new CentersInfo();
                handleCenterInfo(oldCentre, centerInfo);
                // 插入园区

               // centersInfoMapper.insert(centerInfo);
                // authGroupService.addOrUpdateGroup(centerInfo.getId(),
                // centerInfo.getName(), "", true);
                String fileSourceId = centerInfo.getCentreImageId();
                String filePath = oldCentre.getCentre_image();
                // 插入园区图片
                insertAttach(fileSourceId, filePath, FilePathUtil.CENTER_MANAGER);
             //   centerService.saveWeekNutrionalMenuInfo(centerInfo.getId(), handleCurrtenUser());
                count++;
            }
            log.info("=====import centre result====" + count);
            log.info("----------------import centre end----------------");

            log.info("----------------import room start----------------");
            importRoom();
            log.info("----------------import room end----------------");
            log.info("----------------import group start----------------");
            importGroup();
            log.info("----------------import group end----------------");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void importRoom() {
        int count = 0;
        for (OldRoomVo oldRoom : oldRoomList) {
            String centreId = ids.get(oldRoom.getCentre_id());
            // 没有找到对应关系
            if (StringUtil.isEmpty(centreId)) {
                log.info("++++++++++++room not find centre,roomId=" + oldRoom.getRoom_id());
                continue;
            }
            RoomInfo roomInfo = new RoomInfo();
            handleRoomInfo(oldRoom, roomInfo);
       //     roomInfoMapper.insert(roomInfo);
            count++;
        }
        log.info("=====import room result====" + count);
    }

    private void importGroup() {
        int count = 0;
        for (OldGroupVo oldGroup : oldGroupList) {
            RoomGroupInfo groupInfo = new RoomGroupInfo();
            String centreId = ids.get(oldGroup.getCentre_id());
            String roomId = ids.get(oldGroup.getRoom_id());
            // 没有找到对应关系
            if (StringUtil.isEmpty(centreId)) {
                log.info("++++++++++++group not find centre,groupId=" + oldGroup.getGroup_id());
                continue;
            }
            // 没有找到对应关系
            if (StringUtil.isEmpty(roomId)) {
                log.info("++++++++++++group not find room,groupId=" + oldGroup.getGroup_id());
                continue;
            }
            handleGroupInfo(oldGroup, groupInfo);
        //    groupInfoMapper.insert(groupInfo);
            count++;
        }
        log.info("=====import group result====" + count);
    }

    private void handleRoomInfo(OldRoomVo oldRoom, RoomInfo roomInfo) {
        String centreId = ids.get(oldRoom.getCentre_id());
        Date createTime = getDateByOldId(oldRoom.getRoom_id());
        String baseId = getBaseVersionId(oldRoom.getRoom_id(), 1);
        String newId = StringUtil.isEmpty(baseId) ? getUUID() : baseId;
        roomInfo.setId(newId);
        roomInfo.setOldId(oldRoom.getRoom_id());
        roomInfo.setAgeGroup(oldRoom.getAge_group());

        roomInfo.setCentersId(centreId);
        roomInfo.setChildCapacity(oldRoom.getChild_capacity());
        roomInfo.setName(oldRoom.getRoom_name());

        roomInfo.setDeleteFlag(this.getDeleteFlag(oldRoom.getDelete_flag()));
        roomInfo.setStatus(this.getState(oldRoom.getStatus()));
        roomInfo.setCreateAccountId(this.defaultAccountId);
        roomInfo.setUpdateAccountId(this.defaultAccountId);
        roomInfo.setCreateTime(createTime);
        roomInfo.setUpdateTime(createTime);
        ids.put(oldRoom.getRoom_id(), newId);
    }

    private void handleGroupInfo(OldGroupVo oldGroup, RoomGroupInfo groupInfo) {
        String centreId = ids.get(oldGroup.getCentre_id());
        String roomId = ids.get(oldGroup.getRoom_id());

        String old_id = oldGroup.getGroup_id();
        Date createTime = getDateByOldId(old_id);
        String newId = getUUID();
        groupInfo.setId(newId);
        groupInfo.setOldId(oldGroup.getGroup_id());

        groupInfo.setCenterId(centreId);
        groupInfo.setRoomId(roomId);
        groupInfo.setName(oldGroup.getGroup_name());

        groupInfo.setCreateTime(createTime);
        groupInfo.setUpdateTime(createTime);
        groupInfo.setDeleteFlag(this.getDeleteFlag(oldGroup.getDelete_flag()));
        groupInfo.setStatus(this.getState(oldGroup.getStatus()));
        groupInfo.setCreateAccountId(this.defaultAccountId);
        groupInfo.setUpdateAccountId(this.defaultAccountId);
        ids.put(oldGroup.getGroup_id(), newId);
    }

    private String getBaseVersionId(String old_id, int index) {
        String baseVersionId = null;
        switch (index) {
        case 0:
            baseVersionId = centersInfoMapper.getCenterIdByOldId(old_id);
            if (StringUtil.isNotEmpty(baseVersionId)) {
                centersInfoMapper.deleteByPrimaryKey(baseVersionId);
                return baseVersionId;
            }
            break;
        case 1:
            baseVersionId = roomInfoMapper.getRoomIdByOldId(old_id);
            if (StringUtil.isNotEmpty(baseVersionId)) {
                roomInfoMapper.deleteByPrimaryKey(baseVersionId);
                return baseVersionId;
            }
            break;
        default:
            break;
        }

        return baseVersionId;
    }

    private void handleCenterInfo(OldCentreVo oldCenter, CentersInfo centreInfo) {
        Date createTime = getDateByOldId(oldCenter.getCentre_id());
        if ("Centre Two".equals(oldCenter.getCentre_name())) {
            createTime = addOneSecond(createTime, 1);
        }
        if ("Centre Three".equals(oldCenter.getCentre_name())) {
            createTime = addOneSecond(createTime, 2);
        }
        String baseId = getBaseVersionId(oldCenter.getCentre_id(), 0);
        String newId = StringUtil.isEmpty(baseId) ? getUUID() : baseId;
        centreInfo.setId(newId);
        centreInfo.setOldId(oldCenter.getCentre_id());
        centreInfo.setCentreAddress(oldCenter.getCentre_address());
        centreInfo.setName(oldCenter.getCentre_name());
        centreInfo.setCentreContact(oldCenter.getCentre_contact());
        centreInfo.setCentreAddress(oldCenter.getCentre_address());
        centreInfo.setEmailAddress(oldCenter.getEmail_address());
        centreInfo.setCentreImageId(getUUID());
        centreInfo.setCreateTime(createTime);
        centreInfo.setStatus(this.getState(oldCenter.getStatus()));
        centreInfo.setDeleteFlag(this.getDeleteFlag(oldCenter.getDelete_flag()));
        centreInfo.setCreateAccountId(this.defaultAccountId);
        centreInfo.setUpdateTaccountId(this.defaultAccountId);
        centreInfo.setUpdateTime(createTime);
        ids.put(oldCenter.getCentre_id(), newId);
    }

    public Date addOneSecond(Date date, int amount) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.SECOND, amount);
        return calendar.getTime();
    }
}
