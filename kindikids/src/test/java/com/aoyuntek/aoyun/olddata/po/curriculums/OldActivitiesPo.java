package com.aoyuntek.aoyun.olddata.po.curriculums;

public class OldActivitiesPo {
    private String detaill;

    private String text;

    private Boolean activity_tag;

    public String getDetaill() {
        return detaill;
    }

    public void setDetaill(String detaill) {
        this.detaill = detaill;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Boolean getActivity_tag() {
        return activity_tag;
    }

    public void setActivity_tag(Boolean activity_tag) {
        this.activity_tag = activity_tag;
    }

}
