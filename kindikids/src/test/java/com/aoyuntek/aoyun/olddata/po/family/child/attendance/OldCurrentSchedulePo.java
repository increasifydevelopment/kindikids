package com.aoyuntek.aoyun.olddata.po.family.child.attendance;

public class OldCurrentSchedulePo {
    private Boolean wednesday;
    private Boolean thursday;
    private String date_of_change;
    private Boolean tuesday;
    private Boolean friday;
    private Boolean monday;

    public Boolean getWednesday() {
        return wednesday;
    }

    public void setWednesday(Boolean wednesday) {
        this.wednesday = wednesday;
    }

    public Boolean getThursday() {
        return thursday;
    }

    public void setThursday(Boolean thursday) {
        this.thursday = thursday;
    }

    public String getDate_of_change() {
        return date_of_change;
    }

    public void setDate_of_change(String date_of_change) {
        this.date_of_change = date_of_change;
    }

    public Boolean getTuesday() {
        return tuesday;
    }

    public void setTuesday(Boolean tuesday) {
        this.tuesday = tuesday;
    }

    public Boolean getFriday() {
        return friday;
    }

    public void setFriday(Boolean friday) {
        this.friday = friday;
    }

    public Boolean getMonday() {
        return monday;
    }

    public void setMonday(Boolean monday) {
        this.monday = monday;
    }

}
