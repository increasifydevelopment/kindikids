package com.aoyuntek.aoyun.olddata.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.aoyuntek.aoyun.constants.DateFormatterConstants;
import com.aoyuntek.aoyun.constants.SystemConstants;
import com.aoyuntek.aoyun.dao.AttachmentInfoMapper;
import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.entity.po.AccountInfo;
import com.aoyuntek.aoyun.entity.po.AttachmentInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskResponsibleLogInfo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.enums.AccountActiveStatus;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.test.BaseTest;
import com.theone.date.util.DateUtil;
import com.theone.http.util.HttpclientUtil;
import com.theone.json.util.JsonUtil;
import com.theone.string.util.StringUtil;

public class BaseService extends BaseTest {

    /**
     * log
     */
    private static Logger log = Logger.getLogger(BaseService.class);
    @Autowired
    private AttachmentInfoMapper attachmentInfoMapper;

    public final String defaultAccountId = "9549abe4-530a-11e6-9746-00e0703507b4";
    public List<String> logList = new ArrayList<String>();

    public List<String> exceptionList = new ArrayList<String>();

    public Map<Short, String> roleMap = new HashedMap();

    public Map<String, String> repeatFactory = new HashedMap();

    @Value("#{sys}")
    private Properties sysProperties;

    public String getSystemMsg(String tipKey) {
        return sysProperties.getProperty(tipKey);
    }

    public BaseService() {
        roleMap.put((short) 0, "2e083440-49a3-11e6-b19e-00e0703507b4");
        roleMap.put((short) 1, "4586aa2b-49a3-11e6-b19e-00e0703507b4");
        roleMap.put((short) 2, "51c32196-49a3-11e6-b19e-00e0703507b4");
        roleMap.put((short) 9, "74ba21a6-49a3-11e6-b19e-00e0703507b4");
        roleMap.put((short) 10, "8090ce81-49a3-11e6-b19e-00e0703507b4");
        roleMap.put((short) 11, "8090ce81-49a3-11e6-b19e-00e0703507b5kid");

        roleMap.put((short) 3, "575490f2-49a3-11e6-b19e-00e0703507b4");
        roleMap.put((short) 4, "5bce6bab-49a3-11e6-b19e-00e0703507b4");
        roleMap.put((short) 5, "60a193d5-49a3-11e6-b19e-00e0703507b4");
        roleMap.put((short) 6, "66090736-49a3-11e6-b19e-00e0703507b4");
        roleMap.put((short) 7, "6aec2330-49a3-11e6-b19e-00e0703507b4");
        roleMap.put((short) 8, "6feff4ba-49a3-11e6-b19e-00e0703507b4");
    }

    public Map<String, String> orgMap = new HashMap<String, String>();

    public void getOrgsMap(CentersInfoMapper centersInfoMapper) {
        List<SelecterPo> list = centersInfoMapper.getMapId();
        for (SelecterPo item : list) {
            orgMap.put(item.getText(), item.getId());
        }
    }

    public Map<String, String> userMap = new HashMap<String, String>();

    public void getUserMap(UserInfoMapper userInfoMapper) {
        List<SelecterPo> list = userInfoMapper.getMapId();
        for (SelecterPo item : list) {
            userMap.put(item.getText(), item.getId());
        }
    }

    /**
     * 
     * @description 初始化附件，直接插入数据库
     * @author gfwang
     * @create 2016年12月13日上午9:24:21
     * @version 1.0
     * @param sourceId
     * @param path
     * @param key
     */
    public void insertAttach(String sourceId, String path, String key) {
        AttachmentInfo attachmentInfo = initAttachInfo(sourceId, path, key, null);
        attachmentInfoMapper.insert(attachmentInfo);
    }

    public AttachmentInfo initAttachInfo(String sourceId, String path, String dir, String fileName) {
        AttachmentInfo attachmentInfo = new AttachmentInfo();
        attachmentInfo.setId(UUID.randomUUID().toString());
        attachmentInfo.setSourceId(sourceId);
        attachmentInfo.setAttachId(SystemConstants.defaultFile);
        attachmentInfo.setKeyName(dir);
        if (StringUtil.isEmpty(fileName)) {
            attachmentInfo.setAttachName(getFileName(path));
        } else {
            attachmentInfo.setAttachName(fileName);
        }
        attachmentInfo.setDeleteFlag(DeleteFlag.Default.getValue());
        attachmentInfo.setVisitUrl(path);
        return attachmentInfo;
    }

    private String getFileName(String filePath) {
        if (StringUtil.isEmpty(filePath)) {
            return null;
        }
        if (filePath.contains("base64")) {
            return "base64";
        }
        String[] arr = filePath.split("/");
        return arr[arr.length - 1];
    }

    public String getHttpJson(String url) {
        try {
            return HttpclientUtil.get(sysProperties.getProperty("old_system_url") + url);
        } catch (Exception e) {
            log.error("visite url=" + url + ",fail", e);
            e.printStackTrace();
        }
        return null;
    }

    public String getUUID() {
        return UUID.randomUUID().toString();
    }

    public short getDeleteFlag(Short deleteFlag) {
        return deleteFlag == null ? DeleteFlag.Default.getValue() : deleteFlag;
    }

    public short getState(Short state) {
        return state == null ? AccountActiveStatus.Enabled.getValue() : state;
    }

    public Date parseDate(String date) {
        try {
            return StringUtil.isEmpty(date) ? null : DateUtil.parseDate(date, DateFormatterConstants.SYS_FORMAT, DateFormatterConstants.SYS_FORMAT2,
                    "yyyy-MM-dd");
        } catch (ParseException e) {
            log.info("××××××date error××××××," + date);
            e.printStackTrace();
            return null;
        }

    }

    public void addSuccessLog(String msg) {
        logList.add(formatLog(msg));
    }

    public void addExceptionLog(String msg) {
        exceptionList.add(formatLog(msg));
    }

    private void printSuccessLog(Logger classLog) {
        classLog.info("-----------------------------------printSuccessLog start---------------------------------");
        for (String item : this.logList) {
            classLog.info("printSuccessLog-kk: " + item);
        }
        classLog.info("-----------------------------------printSuccessLog end---------------------------------");
    }

    private void printExceptionLog(Logger classLog) {
        classLog.info("-----------------------------------printExceptionLog start---------------------------------");
        for (String item : this.exceptionList) {
            classLog.error("printExceptionLog-kk: " + item);
        }
        classLog.info("-----------------------------------printExceptionLog end---------------------------------");
    }

    public void printLog(Logger classLog) {
        printSuccessLog(classLog);
        printExceptionLog(classLog);
    }

    public String formatLog(String msg) {
        return "-----------------------------> " + msg;
    }

    public Short getGender(String oldGender) {
        if (StringUtil.isEmpty(oldGender)) {
            return null;
        }
        if ("female".equals(oldGender.toLowerCase())) {
            return 0;
        } else {
            return 1;
        }
    }

    public String formatJson(Object obj) {
        return JsonUtil.objectToJson(obj);
    }

    public String getColor(int index) {
        String[] colors = getSystemMsg("user.color").split(",");
        int totalCount = colors.length;
        if (index % totalCount == 0) {
            return colors[0];
        }
        return colors[(index % totalCount) - 1];
    }

    /**
     * 
     * @description 获取一个假的当前用户对象，为了通用一些方法
     * @author gfwang
     * @create 2016年12月9日下午1:24:59
     * @version 1.0
     * @return
     */
    public UserInfoVo handleCurrtenUser() {
        UserInfoVo user = new UserInfoVo();
        AccountInfo account = new AccountInfo();
        account.setId(this.defaultAccountId);
        user.setAccountInfo(account);
        return user;
    }

    public Date conDate(String dateStr) {
        String[] freakyFormat = { "dd'st' MMM,yyyy", "dd'nd' MMM, yyyy", "dd'th' MMM, yyyy", "dd'rd' MMM, yyyy", "yyyy-MM-dd HH:mm" };
        Date date = null;
        for (String formatStr : freakyFormat) {
            boolean isOk = true;
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formatStr, Locale.ENGLISH);
            try {
                date = simpleDateFormat.parse(dateStr);
            } catch (ParseException e) {
                isOk = false;
            }
            if (isOk) {
                break;
            }
        }
        return date;
    }

    /**
     * 
     * @description id轉換時間
     * @author gfwang
     * @create 2016年12月21日上午8:56:36
     * @version 1.0
     * @param id
     * @return
     */
    public Date getDateByOldId(String oldId) {
        if (StringUtil.isEmpty(oldId)) {
            return null;
        }
        return new ObjectId(oldId).getDate();
    }

    public String initAvatar(String path) {
        if (StringUtil.isEmpty(path)) {
            return null;
        }
        if (path.contains("base64")) {
            return path;
        }
        return SystemConstants.defaultFile + "aoyun" + path;
    }

    /**
     * 
     * @description 獲取責任人
     * @author gfwang
     * @create 2017年1月16日下午3:23:20
     * @version 1.0
     * @param taskInstanceId
     * @param accountId
     * @return
     */
    public TaskResponsibleLogInfo getResponsiblePerson(String taskInstanceId, String accountId) {
        TaskResponsibleLogInfo logInfo = new TaskResponsibleLogInfo();
        logInfo.setAcountId(accountId);
        logInfo.setTaskInstanceId(taskInstanceId);
        return logInfo;
    }
}
