package com.aoyuntek.aoyun.olddata.po.family.child.base;

public class OldFamilyStatusPo {
    private String other;

    private Short choice;

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public Short getChoice() {
        return choice;
    }

    public void setChoice(Short choice) {
        this.choice = choice;
    }

}
