package com.aoyuntek.aoyun.olddata.po.user.staff;

import java.util.List;

import com.aoyuntek.aoyun.olddata.po.user.staff.credentials.CredentialsWorkPo;
import com.aoyuntek.aoyun.olddata.po.user.staff.credentials.OldAidPo;
import com.aoyuntek.aoyun.olddata.po.user.staff.credentials.OldQualificationPo;
import com.aoyuntek.aoyun.olddata.po.user.staff.credentials.OldSupervisorPo;
import com.aoyuntek.aoyun.olddata.po.user.staff.credentials.OldTrainingPo;

public class OldStaffCredentialsPo {
    private List<CredentialsWorkPo> working_with_children_check;
    private List<OldAidPo> first_aid;
    private List<OldTrainingPo> training;
    private List<OldSupervisorPo> certified_supervisor;
    private List<OldQualificationPo> qualification;
    public List<CredentialsWorkPo> getWorking_with_children_check() {
        return working_with_children_check;
    }
    public void setWorking_with_children_check(List<CredentialsWorkPo> working_with_children_check) {
        this.working_with_children_check = working_with_children_check;
    }
    public List<OldAidPo> getFirst_aid() {
        return first_aid;
    }
    public void setFirst_aid(List<OldAidPo> first_aid) {
        this.first_aid = first_aid;
    }
    public List<OldTrainingPo> getTraining() {
        return training;
    }
    public void setTraining(List<OldTrainingPo> training) {
        this.training = training;
    }
    public List<OldSupervisorPo> getCertified_supervisor() {
        return certified_supervisor;
    }
    public void setCertified_supervisor(List<OldSupervisorPo> certified_supervisor) {
        this.certified_supervisor = certified_supervisor;
    }
    public List<OldQualificationPo> getQualification() {
        return qualification;
    }
    public void setQualification(List<OldQualificationPo> qualification) {
        this.qualification = qualification;
    }

}
