package com.aoyuntek.aoyun.olddata.po.family.child;

import java.util.List;

import com.aoyuntek.aoyun.olddata.po.family.child.back.OldBackPeoplePo;
import com.aoyuntek.aoyun.olddata.po.family.child.back.OldBackPetsPo;
import com.aoyuntek.aoyun.olddata.po.family.child.back.OldNurseryRoutinePo;

public class OldBackPo {
    private Boolean require_feeding;

    private Boolean other_people_same_household;

    private List<OldBackPeoplePo> people;

    private Boolean additional_needs;

    private Boolean under_two_years;

    private String clothing_info;

    private String language_at_home;
    private String interest_info;

    private String bottle_info;

    private String preferred_experience;
    private Boolean has_pets;

    private Boolean special_requirements_for_sleep_time;
    private List<OldBackPetsPo> pets;
    private Boolean individual_clothing_needs;
    private String cultural_background;
    private String feeding_info;
    private Short toilet_training_stage;

    private Boolean drink_from_bottle;
    private String sleep_time_info;
    private String additional_need_info;
    private OldNurseryRoutinePo nursery_routine;

    public Boolean getRequire_feeding() {
        return require_feeding;
    }

    public void setRequire_feeding(Boolean require_feeding) {
        this.require_feeding = require_feeding;
    }

    public Boolean getOther_people_same_household() {
        return other_people_same_household;
    }

    public void setOther_people_same_household(Boolean other_people_same_household) {
        this.other_people_same_household = other_people_same_household;
    }

    public Boolean getAdditional_needs() {
        return additional_needs;
    }

    public void setAdditional_needs(Boolean additional_needs) {
        this.additional_needs = additional_needs;
    }

    public Boolean getUnder_two_years() {
        return under_two_years;
    }

    public void setUnder_two_years(Boolean under_two_years) {
        this.under_two_years = under_two_years;
    }

    public String getClothing_info() {
        return clothing_info;
    }

    public void setClothing_info(String clothing_info) {
        this.clothing_info = clothing_info;
    }

    public String getLanguage_at_home() {
        return language_at_home;
    }

    public void setLanguage_at_home(String language_at_home) {
        this.language_at_home = language_at_home;
    }

    public String getInterest_info() {
        return interest_info;
    }

    public void setInterest_info(String interest_info) {
        this.interest_info = interest_info;
    }

    public String getBottle_info() {
        return bottle_info;
    }

    public void setBottle_info(String bottle_info) {
        this.bottle_info = bottle_info;
    }

    public String getPreferred_experience() {
        return preferred_experience;
    }

    public void setPreferred_experience(String preferred_experience) {
        this.preferred_experience = preferred_experience;
    }

    public Boolean getHas_pets() {
        return has_pets;
    }

    public void setHas_pets(Boolean has_pets) {
        this.has_pets = has_pets;
    }

    public Boolean getSpecial_requirements_for_sleep_time() {
        return special_requirements_for_sleep_time;
    }

    public void setSpecial_requirements_for_sleep_time(Boolean special_requirements_for_sleep_time) {
        this.special_requirements_for_sleep_time = special_requirements_for_sleep_time;
    }

    public List<OldBackPeoplePo> getPeople() {
        return people;
    }

    public void setPeople(List<OldBackPeoplePo> people) {
        this.people = people;
    }

    public List<OldBackPetsPo> getPets() {
        return pets;
    }

    public void setPets(List<OldBackPetsPo> pets) {
        this.pets = pets;
    }

    public Boolean getIndividual_clothing_needs() {
        return individual_clothing_needs;
    }

    public void setIndividual_clothing_needs(Boolean individual_clothing_needs) {
        this.individual_clothing_needs = individual_clothing_needs;
    }

    public String getCultural_background() {
        return cultural_background;
    }

    public void setCultural_background(String cultural_background) {
        this.cultural_background = cultural_background;
    }

    public String getFeeding_info() {
        return feeding_info;
    }

    public void setFeeding_info(String feeding_info) {
        this.feeding_info = feeding_info;
    }

    public Short getToilet_training_stage() {
        return toilet_training_stage;
    }

    public void setToilet_training_stage(Short toilet_training_stage) {
        this.toilet_training_stage = toilet_training_stage;
    }

    public Boolean getDrink_from_bottle() {
        return drink_from_bottle;
    }

    public void setDrink_from_bottle(Boolean drink_from_bottle) {
        this.drink_from_bottle = drink_from_bottle;
    }

    public String getSleep_time_info() {
        return sleep_time_info;
    }

    public void setSleep_time_info(String sleep_time_info) {
        this.sleep_time_info = sleep_time_info;
    }

    public String getAdditional_need_info() {
        return additional_need_info;
    }

    public void setAdditional_need_info(String additional_need_info) {
        this.additional_need_info = additional_need_info;
    }

    public OldNurseryRoutinePo getNursery_routine() {
        return nursery_routine;
    }

    public void setNursery_routine(OldNurseryRoutinePo nursery_routine) {
        this.nursery_routine = nursery_routine;
    }

}
