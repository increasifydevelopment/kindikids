package com.aoyuntek.aoyun.olddata.po.family.child;

import java.util.List;

import com.aoyuntek.aoyun.olddata.po.family.OldEylfPo;

public class OldChildPo {
    private OldBackPo background;

    private OldMedicalPo medical;

    private OldDietaryPo dietary;

    private OldBasePo basic_info;

    private List<OldEylfPo> eylf_check;
    
    private OldAttendancePo attendance;

    private OldStatusInfoPo status_info;

    public List<OldEylfPo> getEylf_check() {
        return eylf_check;
    }

    public void setEylf_check(List<OldEylfPo> eylf_check) {
        this.eylf_check = eylf_check;
    }

    public OldBackPo getBackground() {
        return background;
    }

    public void setBackground(OldBackPo background) {
        this.background = background;
    }

    public OldMedicalPo getMedical() {
        return medical;
    }

    public void setMedical(OldMedicalPo medical) {
        this.medical = medical;
    }

    public OldDietaryPo getDietary() {
        return dietary;
    }

    public void setDietary(OldDietaryPo dietary) {
        this.dietary = dietary;
    }

    public OldBasePo getBasic_info() {
        return basic_info;
    }

    public void setBasic_info(OldBasePo basic_info) {
        this.basic_info = basic_info;
    }

    public OldAttendancePo getAttendance() {
        return attendance;
    }

    public void setAttendance(OldAttendancePo attendance) {
        this.attendance = attendance;
    }

    public OldStatusInfoPo getStatus_info() {
        return status_info;
    }

    public void setStatus_info(OldStatusInfoPo status_info) {
        this.status_info = status_info;
    }

}
