package com.aoyuntek.aoyun.olddata.po.massage;

import com.aoyuntek.framework.model.BaseModel;

public class OldAttachment extends BaseModel {
private String filePath;
private String fileName;
public OldAttachment() {
    super();
}
public OldAttachment(String filePath, String fileName) {
    super();
    this.filePath = filePath;
    this.fileName = fileName;
}
public String getFilePath() {
    return filePath;
}
public void setFilePath(String filePath) {
    this.filePath = filePath;
}
public String getFileName() {
    return fileName;
}
public void setFileName(String fileName) {
    this.fileName = fileName;
}

}
