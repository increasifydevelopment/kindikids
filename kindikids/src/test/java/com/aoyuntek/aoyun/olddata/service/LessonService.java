package com.aoyuntek.aoyun.olddata.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.NewsInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.dao.UserStaffMapper;
import com.aoyuntek.aoyun.entity.po.AttachmentInfo;
import com.aoyuntek.aoyun.entity.po.NqsRelationInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramLessonInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskInstanceInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskResponsibleLogInfo;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.TaskType;
import com.aoyuntek.aoyun.olddata.constants.HttpUrlConstants;
import com.aoyuntek.aoyun.olddata.po.lesson.NqsText;
import com.aoyuntek.aoyun.olddata.po.lesson.OldLesson;
import com.aoyuntek.aoyun.olddata.po.lesson.OldLessonInfo;
import com.aoyuntek.aoyun.service.old.IOldDataService;
import com.aoyuntek.aoyun.service.task.ITaskInstance;
import com.aoyuntek.aoyun.uitl.FilePathUtil;
import com.google.gson.reflect.TypeToken;
import com.theone.common.util.ServiceResult;
import com.theone.json.util.JsonUtil;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

public class LessonService extends BaseService {
    private static Logger log = Logger.getLogger(LessonService.class);

    @Autowired
    private CentersInfoMapper centersInfoMapper;
    @Autowired
    private NewsInfoMapper newsInfoMapper;
    @Autowired
    private UserStaffMapper userStaffMapper;
    @Autowired
    private IOldDataService oldDataService;
    @Autowired
    private ITaskInstance taskInstance;
    @Autowired
    private UserInfoMapper userInfoMapper;

    private List<ProgramLessonInfo> lessonList = new ArrayList<ProgramLessonInfo>();
    private List<NqsRelationInfo> nqsList = new ArrayList<NqsRelationInfo>();
    private List<AttachmentInfo> attachmentInfos = new ArrayList<AttachmentInfo>();
    private List<TaskResponsibleLogInfo> responsibleList = new ArrayList<TaskResponsibleLogInfo>();

    @Test
    public void importLesson() {
        getOrgsMap(centersInfoMapper);
        getUserMap(userInfoMapper);
        
        List<OldLesson> oldLessonList = new ArrayList<OldLesson>();
        int count = 0;
        try {
            // 从网页获取历史数据
            String json = getHttpJson(HttpUrlConstants.PROGRAM_LESSONS_URL);
            // json字符串转换成对象集合
            oldLessonList = JsonUtil.jsonToList(json, new TypeToken<List<OldLesson>>() {
            }.getType());

            // 循环集合，处理数据
            for (OldLesson oldLesson : oldLessonList) {
                ServiceResult<Object> result = importLesson(oldLesson);
                count += (int) result.getReturnObj();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        log.info("====================================start insert=============================");
        try {
            // 插入数据
            oldDataService.importLesson(this.lessonList, this.nqsList, this.attachmentInfos, this.responsibleList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.info("====================================end insert===============================");

        // 打印日志
        printLog(this.log);
        // programLesson总个数
        log.info("oldLessonList count=" + count);
        // 插入未成功的lesson个数
        log.info("oldLessonList no import count=" + super.exceptionList.size());
    }

    /**
     * 
     * @description importLesson
     * @author mingwang
     * @create 2016年12月20日下午2:12:02
     * @param lessonId
     * @param oldlesson
     * @return
     */
    private ServiceResult<Object> importLesson(OldLesson oldLesson) {
        List<OldLessonInfo> lessons = oldLesson.getLessons();
        ServiceResult<Object> result = new ServiceResult<Object>();
        if (ListUtil.isEmpty(lessons)) {
            result.setReturnObj(1);
            super.exceptionList.add("lesson error null" + formatJson(oldLesson));
            return result;
        }
        String newCentreId = super.orgMap.get(oldLesson.getCenter_oldid());
        String newRoomId = super.orgMap.get(oldLesson.getRoom_oldid());
        String newAccountId = super.userMap.get(oldLesson.getCreate_account_oldid());
        String updateAccountId = super.userMap.get(oldLesson.getUpdate_account_oldid());
        for (OldLessonInfo oldLessonInfo : lessons) {
            String lessonId = getUUID();
            // nqs导入
            List<NqsText> nqs_text = oldLessonInfo.getNqs_text();
            importNqsText(lessonId, nqs_text);

            ProgramLessonInfo lessonInfo = new ProgramLessonInfo();
            lessonInfo.setId(lessonId);
            lessonInfo.setCenterId(newCentreId);
            lessonInfo.setRoomId(newRoomId);
            lessonInfo.setState(oldLesson.getState());
            String lessonName = oldLessonInfo.getLessonName();
            // lessonName必填
            if (StringUtil.isEmpty(lessonName)) {
                super.exceptionList.add("lessonName error null");
                continue;
            }
            lessonInfo.setLessonName(lessonName);
            lessonInfo.setCurriculum(oldLessonInfo.getCurriculum());
            lessonInfo.setUnitName(oldLessonInfo.getUnitName());
            lessonInfo.setInstrctedBy(oldLessonInfo.getInstructedBy());
            lessonInfo.setResources(oldLessonInfo.getResourcesNeeded());
            lessonInfo.setLearning(oldLessonInfo.getObjective());
            lessonInfo.setExtension(oldLessonInfo.getExtension());
            lessonInfo.setReflection(oldLesson.getEylf_reflection());
            // String newsfeedId =
            // newsInfoMapper.getNewIdByOldId(oldLesson.getNewsfeed_oldid());
            // lessonInfo.setNewsfeedId(newsfeedId);
            Date newDay = null;
            if (StringUtil.isEmpty(oldLesson.getDay())) {
                newDay = getDateByOldId(oldLesson.getLesson_oldId());
            } else {
                newDay = parseDateFormat(oldLesson.getDay());
            }
            lessonInfo.setDay(newDay);
            lessonInfo.setCreateAccountId(newAccountId);
            if (StringUtil.isEmpty(oldLesson.getUpdate_time())) {
                lessonInfo.setUpdateTime(newDay);
            } else {
                lessonInfo.setUpdateTime(parseDate(oldLesson.getUpdate_time()));
            }
            lessonInfo.setUpdateAccountId(updateAccountId);
            lessonInfo.setDeleteFlag(oldLesson.isDelete_flag() ? DeleteFlag.Delete.getValue() : DeleteFlag.Default.getValue());
            lessonInfo.setOldId(oldLesson.getLesson_oldId());
            this.lessonList.add(lessonInfo);
            ServiceResult<TaskInstanceInfo> addTaskInstance = taskInstance.addTaskInstance(TaskType.Lesson, newCentreId, newRoomId, null, lessonId, newAccountId, newDay);
            if (StringUtil.isNotEmpty(updateAccountId)) {
                this.responsibleList.add(getResponsiblePerson(addTaskInstance.getReturnObj().getId(), updateAccountId));
            } else {
                super.logList.add("can't find update accountId taskid=" + lessonId);
            }
            
            // 附件导入
            importPhotos(lessonId, oldLesson);
        }
        result.setReturnObj(lessons.size());
        return result;
    }

    private void importNqsText(String lessonId, List<NqsText> nqs_text) {
        if (ListUtil.isNotEmpty(nqs_text)) {
            for (NqsText nqsText : nqs_text) {
                NqsRelationInfo nqsRelationInfo = new NqsRelationInfo();
                nqsRelationInfo.setId(getUUID());
                nqsRelationInfo.setObjId(lessonId);
                nqsRelationInfo.setNqsVersion(nqsText.getLabel());
                nqsRelationInfo.setDeleteFlag(DeleteFlag.Default.getValue());
                this.nqsList.add(nqsRelationInfo);
            }
        }
    }

    private void importPhotos(String lessonId, OldLesson oldLesson) {
        String[] photos = oldLesson.getPhotos();
        if (photos.length == 0) {
            return;
        }
        for (String string : photos) {
            AttachmentInfo attachmentInfo = initAttachInfo(lessonId, string, FilePathUtil.PROGRAM_PATH, null);
            this.attachmentInfos.add(attachmentInfo);
        }
    }

    public Date parseDateFormat(String str) {

        String str1 = str.trim();
        StringBuffer sb = new StringBuffer(str1);
        int index = sb.indexOf(" ");
        str = sb.delete(index - 2, index).toString();

        SimpleDateFormat format = new SimpleDateFormat("d MMM, yyyy", Locale.ENGLISH);
        Date d1 = null;
        try {
            d1 = format.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
            log.info("日期转换错误");
        }
        return d1;

    }
}
