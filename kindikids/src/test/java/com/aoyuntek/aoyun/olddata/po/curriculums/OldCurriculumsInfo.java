package com.aoyuntek.aoyun.olddata.po.curriculums;

import java.util.List;

public class OldCurriculumsInfo {
    private String update_account_oldid;
    private String centre_oldid;
    private Short category;
    private Short state;
    private String eylf_reflection;
    private String create_account_oldid;
    private String day;
    private Boolean delete_flag;

    private String update_time;

    private List<String> photos;

    private List<OldActivitiesPo> activities;
    private String program_oldId;

    private String newsfeed_oldid;

    private String room_oldid;

    public String getUpdate_account_oldid() {
        return update_account_oldid;
    }

    public void setUpdate_account_oldid(String update_account_oldid) {
        this.update_account_oldid = update_account_oldid;
    }

    public String getCentre_oldid() {
        return centre_oldid;
    }

    public void setCentre_oldid(String centre_oldid) {
        this.centre_oldid = centre_oldid;
    }

    public Short getCategory() {
        return category;
    }

    public void setCategory(Short category) {
        this.category = category;
    }

    public Short getState() {
        return state;
    }

    public void setState(Short state) {
        this.state = state;
    }

    public String getEylf_reflection() {
        return eylf_reflection;
    }

    public void setEylf_reflection(String eylf_reflection) {
        this.eylf_reflection = eylf_reflection;
    }

    public String getCreate_account_oldid() {
        return create_account_oldid;
    }

    public void setCreate_account_oldid(String create_account_oldid) {
        this.create_account_oldid = create_account_oldid;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public Boolean getDelete_flag() {
        return delete_flag;
    }

    public void setDelete_flag(Boolean delete_flag) {
        this.delete_flag = delete_flag;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public List<String> getPhotos() {
        return photos;
    }

    public void setPhotos(List<String> photos) {
        this.photos = photos;
    }

    public List<OldActivitiesPo> getActivities() {
        return activities;
    }

    public void setActivities(List<OldActivitiesPo> activities) {
        this.activities = activities;
    }

    public String getProgram_oldId() {
        return program_oldId;
    }

    public void setProgram_oldId(String program_oldId) {
        this.program_oldId = program_oldId;
    }

    public String getNewsfeed_oldid() {
        return newsfeed_oldid;
    }

    public void setNewsfeed_oldid(String newsfeed_oldid) {
        this.newsfeed_oldid = newsfeed_oldid;
    }

    public String getRoom_oldid() {
        return room_oldid;
    }

    public void setRoom_oldid(String room_oldid) {
        this.room_oldid = room_oldid;
    }
}
