package com.aoyuntek.aoyun.olddata.po.newsfeed;

public class NewsGroup {

    private String group_name;
    private String news_id;
    private boolean delete_flag;
    public String getGroup_name() {
        return group_name;
    }
    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }
    public String getNews_id() {
        return news_id;
    }
    public void setNews_id(String news_id) {
        this.news_id = news_id;
    }
    public boolean isDelete_flag() {
        return delete_flag;
    }
    public void setDelete_flag(boolean delete_flag) {
        this.delete_flag = delete_flag;
    }
    
}
