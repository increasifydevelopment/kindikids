package com.aoyuntek.aoyun.olddata.po.newsfeed;

public class OldNewsComment {
    private String comment_account_oldid;
    private String news_oldid;
    private boolean delete_flag;
    private String create_time;
    private String update_time;
    private String update_account_oldid;
    private String content;
    public String getComment_account_oldid() {
        return comment_account_oldid;
    }
    public void setComment_account_oldid(String comment_account_oldid) {
        this.comment_account_oldid = comment_account_oldid;
    }
    public String getNews_oldid() {
        return news_oldid;
    }
    public void setNews_oldid(String news_oldid) {
        this.news_oldid = news_oldid;
    }
    public boolean isDelete_flag() {
        return delete_flag;
    }
    public void setDelete_flag(boolean delete_flag) {
        this.delete_flag = delete_flag;
    }
    public String getCreate_time() {
        return create_time;
    }
    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }
    public String getUpdate_time() {
        return update_time;
    }
    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }
    public String getUpdate_account_oldid() {
        return update_account_oldid;
    }
    public void setUpdate_account_oldid(String update_account_oldid) {
        this.update_account_oldid = update_account_oldid;
    }
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }

    
}
