package com.aoyuntek.aoyun.olddata.po.family.child;

import java.util.List;

import com.aoyuntek.aoyun.olddata.po.family.child.base.OldCustodyArrangementPo;
import com.aoyuntek.aoyun.olddata.po.family.child.base.OldFamilyStatusPo;
import com.aoyuntek.aoyun.olddata.po.family.child.base.OldHealthPo;

public class OldBasePo {
    private String child_surname;

    private OldFamilyStatusPo family_status;

    private String child_middle_name;

    private OldHealthPo health_medical_info;

    private OldCustodyArrangementPo custody_arrangement;

    private String address;

    private String date_of_birth;

    private String centres_oldId;
    private String suburb;
    private String group_oldId;
    private String child_gender;
    private List<OldConcatPo> emergency_contact;

    private String child_first_name;

    private String room_oldId;
    private String childId_oldId;
    private String state;

    private String family_name;

    private String birth_certificate;
    private String child_image;
    
    public String getChild_image() {
        return child_image;
    }

    public void setChild_image(String child_image) {
        this.child_image = child_image;
    }

    private String postcode;

    public String getChild_surname() {
        return child_surname;
    }

    public void setChild_surname(String child_surname) {
        this.child_surname = child_surname;
    }

    public OldFamilyStatusPo getFamily_status() {
        return family_status;
    }

    public void setFamily_status(OldFamilyStatusPo family_status) {
        this.family_status = family_status;
    }

    public String getChild_middle_name() {
        return child_middle_name;
    }

    public void setChild_middle_name(String child_middle_name) {
        this.child_middle_name = child_middle_name;
    }

    public OldHealthPo getHealth_medical_info() {
        return health_medical_info;
    }

    public void setHealth_medical_info(OldHealthPo health_medical_info) {
        this.health_medical_info = health_medical_info;
    }

    public OldCustodyArrangementPo getCustody_arrangement() {
        return custody_arrangement;
    }

    public void setCustody_arrangement(OldCustodyArrangementPo custody_arrangement) {
        this.custody_arrangement = custody_arrangement;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(String date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getCentres_oldId() {
        return centres_oldId;
    }

    public void setCentres_oldId(String centres_oldId) {
        this.centres_oldId = centres_oldId;
    }

    public String getSuburb() {
        return suburb;
    }

    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }

    public String getGroup_oldId() {
        return group_oldId;
    }

    public void setGroup_oldId(String group_oldId) {
        this.group_oldId = group_oldId;
    }

    public String getChild_gender() {
        return child_gender;
    }

    public void setChild_gender(String child_gender) {
        this.child_gender = child_gender;
    }

    public List<OldConcatPo> getEmergency_contact() {
        return emergency_contact;
    }

    public void setEmergency_contact(List<OldConcatPo> emergency_contact) {
        this.emergency_contact = emergency_contact;
    }

    public String getChild_first_name() {
        return child_first_name;
    }

    public void setChild_first_name(String child_first_name) {
        this.child_first_name = child_first_name;
    }

    public String getRoom_oldId() {
        return room_oldId;
    }

    public void setRoom_oldId(String room_oldId) {
        this.room_oldId = room_oldId;
    }

    public String getChildId_oldId() {
        return childId_oldId;
    }

    public void setChildId_oldId(String childId_oldId) {
        this.childId_oldId = childId_oldId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getFamily_name() {
        return family_name;
    }

    public void setFamily_name(String family_name) {
        this.family_name = family_name;
    }

    public String getBirth_certificate() {
        return birth_certificate;
    }

    public void setBirth_certificate(String birth_certificate) {
        this.birth_certificate = birth_certificate;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

}
