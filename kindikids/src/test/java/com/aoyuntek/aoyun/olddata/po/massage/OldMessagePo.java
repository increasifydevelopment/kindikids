package com.aoyuntek.aoyun.olddata.po.massage;

import java.util.List;

import com.aoyuntek.framework.model.BaseModel;

public class OldMessagePo extends BaseModel{
private Short read_flag;
private String content;
private String send_account_id;
private Short delete_flag;
private String create_time;
private String centres_oldid;
private String subject;
private String message_oldId;
private Short message_type;
private Object  attachments;
private String parent_message_id;
private Object nqs;
private List<OldRecipient> message_recipients;
public OldMessagePo() {
    super();
}
public OldMessagePo(Short read_flag, String content, String send_account_id, Short delete_flag, String create_time, String centres_oldid, String subject, String message_oldId, Short message_type, List<OldAttachment> attachments,
        String parent_message_id, List<String> nqs, List message_recipients) {
    super();
    this.read_flag = read_flag;
    this.content = content;
    this.send_account_id = send_account_id;
    this.delete_flag = delete_flag;
    this.create_time = create_time;
    this.centres_oldid = centres_oldid;
    this.subject = subject;
    this.message_oldId = message_oldId;
    this.message_type = message_type;
    this.attachments = attachments;
    this.parent_message_id = parent_message_id;
    this.nqs = nqs;
    this.message_recipients = message_recipients;
}
public Short getRead_flag() {
    return read_flag;
}
public void setRead_flag(Short read_flag) {
    this.read_flag = read_flag;
}
public String getContent() {
    return content;
}
public void setContent(String content) {
    this.content = content;
}
public String getSend_account_id() {
    return send_account_id;
}
public void setSend_account_id(String send_account_id) {
    this.send_account_id = send_account_id;
}
public Short getDelete_flag() {
    return delete_flag;
}
public void setDelete_flag(Short delete_flag) {
    this.delete_flag = delete_flag;
}
public String getCreate_time() {
    return create_time;
}
public void setCreate_time(String create_time) {
    this.create_time = create_time;
}
public String getCentres_oldid() {
    return centres_oldid;
}
public void setCentres_oldid(String centres_oldid) {
    this.centres_oldid = centres_oldid;
}
public String getSubject() {
    return subject;
}
public void setSubject(String subject) {
    this.subject = subject;
}
public String getMessage_oldId() {
    return message_oldId;
}
public void setMessage_oldId(String message_oldId) {
    this.message_oldId = message_oldId;
}
public Short getMessage_type() {
    return message_type;
}
public void setMessage_type(Short message_type) {
    this.message_type = message_type;
}
public Object getAttachments() {
    return attachments;
}
public void setAttachments(List<OldAttachment> attachments) {
    this.attachments = attachments;
}
public String getParent_message_id() {
    return parent_message_id;
}
public void setParent_message_id(String parent_message_id) {
    this.parent_message_id = parent_message_id;
}
public Object getNqs() {
    return nqs;
}
public void setNqs(List<String> nqs) {
    this.nqs = nqs;
}
public List getMessage_recipients() {
    return message_recipients;
}
public void setMessage_recipients(List message_recipients) {
    this.message_recipients = message_recipients;
}



}
