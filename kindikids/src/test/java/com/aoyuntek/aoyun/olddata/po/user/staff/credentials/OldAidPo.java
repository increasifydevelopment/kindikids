package com.aoyuntek.aoyun.olddata.po.user.staff.credentials;

public class OldAidPo {
    private String first_aid_course_name;
    private String date_completed;
    private String upload_certificate;
    private String date_of_Expiry;

    public String getFirst_aid_course_name() {
        return first_aid_course_name;
    }

    public void setFirst_aid_course_name(String first_aid_course_name) {
        this.first_aid_course_name = first_aid_course_name;
    }

    public String getDate_completed() {
        return date_completed;
    }

    public void setDate_completed(String date_completed) {
        this.date_completed = date_completed;
    }

    public String getUpload_certificate() {
        return upload_certificate;
    }

    public void setUpload_certificate(String upload_certificate) {
        this.upload_certificate = upload_certificate;
    }

    public String getDate_of_Expiry() {
        return date_of_Expiry;
    }

    public void setDate_of_Expiry(String date_of_Expiry) {
        this.date_of_Expiry = date_of_Expiry;
    }

}
