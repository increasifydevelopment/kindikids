package com.aoyuntek.aoyun.olddata.po.newsfeed;

import java.util.List;

public class OldNewsVo {
    private List<OldNewsComment> newsfeed_comment;
    private List<NewsGroup> newsfeed_group;
    private OldNewsMain newsfeed_main;
    private List<OldNewsTag> newsfeed_tag_child_staff;

    public OldNewsVo(List<OldNewsComment> newsfeed_comment, List<NewsGroup> newsfeed_group, OldNewsMain newsfeed_main, List<OldNewsTag> newsfeed_tag_child_staff) {
        this.newsfeed_comment = newsfeed_comment;
        this.newsfeed_group = newsfeed_group;
        this.newsfeed_main = newsfeed_main;
        this.newsfeed_tag_child_staff = newsfeed_tag_child_staff;
    }

    public OldNewsVo() {

    }

    public List<OldNewsComment> getNewsfeed_comment() {
        return newsfeed_comment;
    }

    public void setNewsfeed_comment(List<OldNewsComment> newsfeed_comment) {
        this.newsfeed_comment = newsfeed_comment;
    }

     public List<NewsGroup> getNewsfeed_group() {
     return newsfeed_group;
     }
    
     public void setNewsfeed_group(List<NewsGroup> newsfeed_group) {
     this.newsfeed_group = newsfeed_group;
     }

    public OldNewsMain getNewsfeed_main() {
        return newsfeed_main;
    }

    public void setNewsfeed_main(OldNewsMain newsfeed_main) {
        this.newsfeed_main = newsfeed_main;
    }

    public List<OldNewsTag> getNewsfeed_tag_child_staff() {
        return newsfeed_tag_child_staff;
    }

    public void setNewsfeed_tag_child_staff(List<OldNewsTag> newsfeed_tag_child_staff) {
        this.newsfeed_tag_child_staff = newsfeed_tag_child_staff;
    }
}
