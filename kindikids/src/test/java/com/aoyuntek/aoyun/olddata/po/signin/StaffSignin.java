package com.aoyuntek.aoyun.olddata.po.signin;

public class StaffSignin {
    private Short type;
    private String StaffId_oldid;
    private String sign_date;
    private Short state;
    private String centre_oldid;
    private String room_oldid;
    private String accountId;

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public Short getType() {
        return type;
    }

    public void setType(Short type) {
        this.type = type;
    }

    public String getStaffId_oldid() {
        return StaffId_oldid;
    }

    public void setStaffId_oldid(String staffId_oldid) {
        StaffId_oldid = staffId_oldid;
    }

    public String getSign_date() {
        return sign_date;
    }

    public void setSign_date(String sign_date) {
        this.sign_date = sign_date;
    }

    public Short getState() {
        return state;
    }

    public void setState(Short state) {
        this.state = state;
    }

    public String getCentre_oldid() {
        return centre_oldid;
    }

    public void setCentre_oldid(String centre_oldid) {
        this.centre_oldid = centre_oldid;
    }

    public String getRoom_oldid() {
        return room_oldid;
    }

    public void setRoom_oldid(String room_oldid) {
        this.room_oldid = room_oldid;
    }

}
