package com.aoyuntek.aoyun.olddata.po.family.child.base;

import java.util.List;

import com.aoyuntek.aoyun.olddata.po.family.child.OldDocumentPo;

public class OldCustodyArrangementPo {
    private String court_orders;

    private String legal_custody;

    private List<OldDocumentPo> court_orders_attachments;

    public String getCourt_orders() {
        return court_orders;
    }

    public void setCourt_orders(String court_orders) {
        this.court_orders = court_orders;
    }

    public String getLegal_custody() {
        return legal_custody;
    }

    public void setLegal_custody(String legal_custody) {
        this.legal_custody = legal_custody;
    }

    public List<OldDocumentPo> getCourt_orders_attachments() {
        return court_orders_attachments;
    }

    public void setCourt_orders_attachments(List<OldDocumentPo> court_orders_attachments) {
        this.court_orders_attachments = court_orders_attachments;
    }

}
