package com.aoyuntek.aoyun.olddata.po.family.child;

import java.util.List;

import com.aoyuntek.aoyun.olddata.po.family.child.dietary.OldDairyPo;
import com.aoyuntek.aoyun.olddata.po.family.child.dietary.OldDietaryEggsPo;
import com.aoyuntek.aoyun.olddata.po.family.child.dietary.OldDietaryFilesPo;
import com.aoyuntek.aoyun.olddata.po.family.child.dietary.OldDietaryMeatPo;
import com.aoyuntek.aoyun.olddata.po.family.child.dietary.OldDietaryNutsPo;

public class OldDietaryPo {
    private OldDairyPo dairy;

    private OldDietaryEggsPo eggs;
    private List<OldDocumentPo> files;
    private OldDietaryMeatPo meat;
    private OldDietaryNutsPo nuts;

    private Short require_api_pen;

    private Boolean terms_agreed;

    public OldDairyPo getDairy() {
        return dairy;
    }

    public void setDairy(OldDairyPo dairy) {
        this.dairy = dairy;
    }

    public OldDietaryEggsPo getEggs() {
        return eggs;
    }

    public void setEggs(OldDietaryEggsPo eggs) {
        this.eggs = eggs;
    }

    public List<OldDocumentPo> getFiles() {
        return files;
    }

    public void setFiles(List<OldDocumentPo> files) {
        this.files = files;
    }

    public OldDietaryMeatPo getMeat() {
        return meat;
    }

    public void setMeat(OldDietaryMeatPo meat) {
        this.meat = meat;
    }

    public OldDietaryNutsPo getNuts() {
        return nuts;
    }

    public void setNuts(OldDietaryNutsPo nuts) {
        this.nuts = nuts;
    }

    public Short getRequire_api_pen() {
        return require_api_pen;
    }

    public void setRequire_api_pen(Short require_api_pen) {
        this.require_api_pen = require_api_pen;
    }

    public Boolean getTerms_agreed() {
        return terms_agreed;
    }

    public void setTerms_agreed(Boolean terms_agreed) {
        this.terms_agreed = terms_agreed;
    }

}
