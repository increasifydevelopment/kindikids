package com.aoyuntek.aoyun.olddata.po.centre;

import java.util.Date;

import com.aoyuntek.framework.model.BaseModel;

public class OldRoomVo extends BaseModel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String room_id;
    private String create_time;
    private Integer child_capacity;
    private String age_group;
    private String room_name;
    private Short delete_flag;
    private String centre_id;
    private Short status;

    public String getRoom_id() {
        return room_id;
    }

    public void setRoom_id(String room_id) {
        this.room_id = room_id;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public Integer getChild_capacity() {
        return child_capacity;
    }

    public void setChild_capacity(Integer child_capacity) {
        this.child_capacity = child_capacity;
    }

    public String getAge_group() {
        return age_group;
    }

    public void setAge_group(String age_group) {
        this.age_group = age_group;
    }

    public String getRoom_name() {
        return room_name;
    }

    public void setRoom_name(String room_name) {
        this.room_name = room_name;
    }

    public Short getDelete_flag() {
        return delete_flag;
    }

    public void setDelete_flag(Short delete_flag) {
        this.delete_flag = delete_flag;
    }

    public String getCentre_id() {
        return centre_id;
    }

    public void setCentre_id(String centre_id) {
        this.centre_id = centre_id;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }
}
