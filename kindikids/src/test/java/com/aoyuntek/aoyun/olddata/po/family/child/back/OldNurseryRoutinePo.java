package com.aoyuntek.aoyun.olddata.po.family.child.back;

public class OldNurseryRoutinePo {
    private String meal_times;

    private String sleep_times;

    private String bottle_times;

    public String getMeal_times() {
        return meal_times;
    }

    public void setMeal_times(String meal_times) {
        this.meal_times = meal_times;
    }

    public String getSleep_times() {
        return sleep_times;
    }

    public void setSleep_times(String sleep_times) {
        this.sleep_times = sleep_times;
    }

    public String getBottle_times() {
        return bottle_times;
    }

    public void setBottle_times(String bottle_times) {
        this.bottle_times = bottle_times;
    }

}
