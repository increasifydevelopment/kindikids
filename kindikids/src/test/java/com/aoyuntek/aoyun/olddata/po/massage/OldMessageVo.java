package com.aoyuntek.aoyun.olddata.po.massage;

public class OldMessageVo {
   private OldMessagePo  oldMassagePo;

public OldMessageVo() {
    super();
}

public OldMessageVo(OldMessagePo oldMassagePo) {
    super();
    this.oldMassagePo = oldMassagePo;
}

public OldMessagePo getOldMassagePo() {
    return oldMassagePo;
}

public void setOldMassagePo(OldMessagePo oldMassagePo) {
    this.oldMassagePo = oldMassagePo;
}
   

}
