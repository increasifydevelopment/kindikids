package com.aoyuntek.aoyun.olddata.po.family.child.dietary;

public class OldDairyPo {
    private String other;

    private Boolean food_containing_dairy_powder;

    private Boolean food_containing_dairy;

    private Boolean streight_milk;

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public Boolean getFood_containing_dairy_powder() {
        return food_containing_dairy_powder;
    }

    public void setFood_containing_dairy_powder(Boolean food_containing_dairy_powder) {
        this.food_containing_dairy_powder = food_containing_dairy_powder;
    }

    public Boolean getFood_containing_dairy() {
        return food_containing_dairy;
    }

    public void setFood_containing_dairy(Boolean food_containing_dairy) {
        this.food_containing_dairy = food_containing_dairy;
    }

    public Boolean getStreight_milk() {
        return streight_milk;
    }

    public void setStreight_milk(Boolean streight_milk) {
        this.streight_milk = streight_milk;
    }

}
