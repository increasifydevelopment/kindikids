package com.aoyuntek.aoyun.olddata.po.user.staff.medical;

public class OldMedicalChildPo {
    private String medical_condition;
    private String certificate_provided;
    private String taking_regular_medication;
    private String allergies;

    public String getMedical_condition() {
        return medical_condition;
    }

    public void setMedical_condition(String medical_condition) {
        this.medical_condition = medical_condition;
    }

    public String getCertificate_provided() {
        return certificate_provided;
    }

    public void setCertificate_provided(String certificate_provided) {
        this.certificate_provided = certificate_provided;
    }

    public String getTaking_regular_medication() {
        return taking_regular_medication;
    }

    public void setTaking_regular_medication(String taking_regular_medication) {
        this.taking_regular_medication = taking_regular_medication;
    }

    public String getAllergies() {
        return allergies;
    }

    public void setAllergies(String allergies) {
        this.allergies = allergies;
    }

}
