package com.aoyuntek.aoyun.olddata.po.user;

import com.aoyuntek.aoyun.olddata.po.user.staff.OldStaffPo;

public class OldStaffVo {
    private OldStaffPo staff;

    public OldStaffVo() {

    }

    public OldStaffVo(OldStaffPo staff) {
        this.staff = staff;
    }

    public OldStaffPo getStaff() {
        return staff;
    }

    public void setStaff(OldStaffPo staff) {
        this.staff = staff;
    }

}
