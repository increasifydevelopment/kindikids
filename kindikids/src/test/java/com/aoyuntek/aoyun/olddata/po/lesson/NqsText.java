package com.aoyuntek.aoyun.olddata.po.lesson;

import java.util.List;

import com.aoyuntek.aoyun.olddata.po.family.child.OldChildPo;

public class NqsText {
    String parent;
    String label;
    String text;
    List<OldChildPo> child;

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<OldChildPo> getChild() {
        return child;
    }

    public void setChild(List<OldChildPo> child) {
        this.child = child;
    }

}
