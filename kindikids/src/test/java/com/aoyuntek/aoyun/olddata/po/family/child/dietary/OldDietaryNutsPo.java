package com.aoyuntek.aoyun.olddata.po.family.child.dietary;

public class OldDietaryNutsPo {
    private Boolean trace_of_nuts;

    private String other;

    private Boolean peanuts;

    private Boolean cashews;

    private Boolean almonds;

    public Boolean getTrace_of_nuts() {
        return trace_of_nuts;
    }

    public void setTrace_of_nuts(Boolean trace_of_nuts) {
        this.trace_of_nuts = trace_of_nuts;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public Boolean getPeanuts() {
        return peanuts;
    }

    public void setPeanuts(Boolean peanuts) {
        this.peanuts = peanuts;
    }

    public Boolean getCashews() {
        return cashews;
    }

    public void setCashews(Boolean cashews) {
        this.cashews = cashews;
    }

    public Boolean getAlmonds() {
        return almonds;
    }

    public void setAlmonds(Boolean almonds) {
        this.almonds = almonds;
    }

}
