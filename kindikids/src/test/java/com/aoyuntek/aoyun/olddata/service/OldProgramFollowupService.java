package com.aoyuntek.aoyun.olddata.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.dao.YelfInfoMapper;
import com.aoyuntek.aoyun.dao.program.ProgramIntobsleaInfoMapper;
import com.aoyuntek.aoyun.dao.program.TaskFollowupEylfInfoMapper;
import com.aoyuntek.aoyun.entity.po.program.ProgramIntobsleaInfo;
import com.aoyuntek.aoyun.entity.po.program.TaskFollowupEylfInfo;
import com.aoyuntek.aoyun.entity.po.program.TaskProgramFollowUpInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskInstanceInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskResponsibleLogInfo;
import com.aoyuntek.aoyun.entity.vo.FollowupEylfVo;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.ProgramState;
import com.aoyuntek.aoyun.enums.TaskCategory;
import com.aoyuntek.aoyun.enums.TaskType;
import com.aoyuntek.aoyun.factory.TaskFactory;
import com.aoyuntek.aoyun.olddata.constants.HttpUrlConstants;
import com.aoyuntek.aoyun.olddata.po.programIntobslea.followup;
import com.aoyuntek.aoyun.service.old.IOldDataService;
import com.aoyuntek.aoyun.service.task.ITaskInstance;
import com.google.gson.reflect.TypeToken;
import com.theone.common.util.ServiceResult;
import com.theone.json.util.JsonUtil;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

public class OldProgramFollowupService extends BaseService {
    private static Logger log = Logger.getLogger(OldProgramFollowupService.class);
    @Autowired
    private ProgramIntobsleaInfoMapper programIntobsleaInfoMapper;
    @Autowired
    private UserInfoMapper userInfoMapper;
    @Autowired
    private IOldDataService oldDataService;
    @Autowired
    private ITaskInstance taskInstance;
    @Autowired
    private YelfInfoMapper yelfInfoMapper;
    @Autowired
    private TaskFollowupEylfInfoMapper taskFollowupEylfInfoMapper;
    @Autowired
    private TaskFactory taskFactory;

    private List<TaskProgramFollowUpInfo> followUpInfos = new ArrayList<TaskProgramFollowUpInfo>();
    private List<TaskInstanceInfo> taskInstanceInfos = new ArrayList<TaskInstanceInfo>();
    private List<TaskResponsibleLogInfo> responsibleList = new ArrayList<TaskResponsibleLogInfo>();

    @Test
    public void importFollowup() {
        getUserMap(userInfoMapper);

        String followupJson = getHttpJson(HttpUrlConstants.PROGRAM_FOLLOWUPS_URL);
        operateFollowup(followupJson);

    }

    private void operateFollowup(String followupJson) {
        List<followup> followups = JsonUtil.jsonToList(followupJson, new TypeToken<List<followup>>() {
        }.getType());
        int count = 0;
        for (followup f : followups) {
            ServiceResult<Object> result = validateFollwup(f);
            if (!result.isSuccess()) {
                count++;
                super.exceptionList.add(result.getMsg());
                continue;
            }
            TaskProgramFollowUpInfo followup = new TaskProgramFollowUpInfo(f.getTaskId(), null, f.getObservation(), f.getCreateAccountId(),
                    f.getState(), conDate(f.getUpdate_time()), f.getUpdate_account_oldid());
            followUpInfos.add(followup);

            ProgramIntobsleaInfo intobsleaInfo = programIntobsleaInfoMapper.selectByPrimaryKey(f.getTaskId());
            // taskInstance.addTaskInstance(getFollowupType(intobsleaInfo.getType()), intobsleaInfo.getCenterId(), intobsleaInfo.getRoomId(),
            // userInfoMapper.getUserInfoByAccountId(intobsleaInfo.getChildId()).getGroupId(), followup.getId(), followup.getCreateAccountId(),
            // intobsleaInfo.getEvaluationDate());

            Date[] dates = taskFactory.dealDate(getTaskType(intobsleaInfo.getType()), intobsleaInfo.getCenterId(), intobsleaInfo.getEvaluationDate());
            TaskInstanceInfo model = new TaskInstanceInfo();
            String taskInstanceId = getUUID();
            model.setId(taskInstanceId);
            model.setCentreId(intobsleaInfo.getCenterId());
            model.setRoomId(intobsleaInfo.getRoomId());
            model.setObjId(null);
            model.setObjType(TaskCategory.ROOM.getValue());
            model.setCreateAccountId(followup.getCreateAccountId());
            model.setCreateTime(new Date());
            model.setDeleteFlag(DeleteFlag.Default.getValue());
            model.setStatu(ProgramState.Complete.getValue());
            model.setTaskModelId(intobsleaInfo.getCenterId());
            model.setTaskType(getFollowupType(intobsleaInfo.getType()).getValue());
            model.setValueId(followup.getId());
            model.setBeginDate(dates[0]);
            model.setEndDate(dates[1]);
            taskInstanceInfos.add(model);

            saveFollowupEylf(followup.getId(), f.getEylf_click());

            String newUpdateAccountId = userMap.get(f.getUpdate_account_oldid());
            if (StringUtil.isNotEmpty(newUpdateAccountId)) {
                responsibleList.add(getResponsiblePerson(taskInstanceId, newUpdateAccountId));
            } else {
                super.exceptionList.add("can't find update accountId taskid=" + intobsleaInfo.getId());
            }

        }
        try {
            oldDataService.importFollowup(followUpInfos, taskInstanceInfos, responsibleList);
        } catch (Exception e) {
            log.error("importFollowup error", e);
        }
        printLog(this.log);
        log.info("Normal Data=" + followUpInfos.size());
        log.info("Error Data=" + count);
        log.error("Total Data=" + followups.size());
        log.error("Normal Data=" + followUpInfos.size());
        log.error("Error Data=" + count);
    }

    private ServiceResult<Object> validateFollwup(followup f) {
        ServiceResult<Object> result = new ServiceResult<Object>();
        if (StringUtil.isEmpty(f.getInterest_oldid()) && StringUtil.isEmpty(f.getObservation_oldid())) {
            result.setSuccess(false);
            result.setMsg("Task old id is empty=>" + formatJson(f));
            return result;
        }
        String taskId = programIntobsleaInfoMapper.getTaskIdByOldId(f.getInterest_oldid());
        String taskId2 = programIntobsleaInfoMapper.getTaskIdByOldId(f.getObservation_oldid());
        if (StringUtil.isEmpty(taskId) && StringUtil.isEmpty(taskId2)) {
            result.setSuccess(false);
            result.setMsg("Task new id is empty=>" + formatJson(f));
            return result;
        } else {
            if (StringUtil.isNotEmpty(taskId)) {
                f.setTaskId(taskId);
            }
            if (StringUtil.isNotEmpty(taskId2)) {
                f.setTaskId(taskId2);
            }
        }
        if (StringUtil.isNotEmpty(f.getCreate_account_oldid())) {
            String createAccountId = userInfoMapper.getAccountIdByUserOldId(f.getCreate_account_oldid());
            if (StringUtil.isEmpty(createAccountId)) {
                result.setSuccess(false);
                result.setMsg("Creater new id is empty=>" + formatJson(f));
                return result;
            } else {
                f.setCreateAccountId(createAccountId);
            }
        }
        if (StringUtil.isNotEmpty(f.getUpdate_account_oldid())) {
            String updateAccountId = userInfoMapper.getAccountIdByUserOldId(f.getUpdate_account_oldid());
            if (StringUtil.isEmpty(updateAccountId)) {
                result.setSuccess(false);
                result.setMsg("Updater new id is empty=>" + formatJson(f));
                return result;
            } else {
                f.setUpdateAccountId(updateAccountId);
            }
        }
        result.setSuccess(true);
        return result;
    }

    private TaskType getFollowupType(short type) {
        switch (type) {
        case 4:
            return TaskType.InterestFollowup;
        case 6:
            return TaskType.ObservationFollowup;
        case 8:
            return TaskType.LearningStoryFollowup;
        default:
            break;
        }
        return null;
    }

    private void saveFollowupEylf(String followUpId, List<String> eylfs) {
        List<FollowupEylfVo> followupEylfVoList = yelfInfoMapper.getAllEylf();
        List<TaskFollowupEylfInfo> tfeList = new ArrayList<TaskFollowupEylfInfo>();
        for (FollowupEylfVo fv : followupEylfVoList) {
            TaskFollowupEylfInfo tfe = new TaskFollowupEylfInfo();
            if (eylfs.contains(fv.getVersion())) {
                tfe.setCheckFlag(true);
            } else {
                tfe.setCheckFlag(false);
            }
            tfe.setId(UUID.randomUUID().toString());
            tfe.setEylfVersion(fv.getVersion());
            tfe.setFollowId(followUpId);

            tfeList.add(tfe);
        }
        if (ListUtil.isNotEmpty(tfeList)) {
            taskFollowupEylfInfoMapper.batchInsertTaskFollowupEylfInfo(tfeList);
        }
    }

    private TaskType getTaskType(short type) {
        for (TaskType t : TaskType.values()) {
            if (t.getValue() == type) {
                return t;
            }
        }
        return null;
    }
}
