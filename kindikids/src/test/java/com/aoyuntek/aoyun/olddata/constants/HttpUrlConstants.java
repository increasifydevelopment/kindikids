package com.aoyuntek.aoyun.olddata.constants;


public class HttpUrlConstants {

    public static final String CENTRE_URL = "all-centres-data";

    public static final String ROOM_URL = "all-rooms-data";

    public static final String GROUP_URL = "all-groups-data";

    public static final String FAMILY_URL = "all-children-data";

    public static final String STAFF_URL = "all-staff-data";

    public static final String CHILD_SIGN_URL = "all-children-signin?page=";

    public static final String STAFF_SIGN_URL = "all-staff-signin";

    public static final String VISITOR_SIGN_URL = "all-visitor-signin";

    public static final String MESSAGE_URL = "all-message-data";

    public static final String PROGRAM_LESSONS_URL = "all-lessons-data";

    public static final String PROGRAM_OBSERVATION_INTERESTS_URL = "all-observations-data";

    public static final String PROGRAM_FOLLOWUPS_URL = "all-followups-data";

    public static final String PROGRAM_WEEKLY_URL = "all-weekly-evaluations";

    public static final String NEWSFEED_URL = "all-newsfeed-data";

    public static final String MEAL_DATA_URL = "all-meal-data";

    public static final String SLEEP_DATA_URL = "all-sleep-data";

    public static final String NAPPY_DATA_URL8 = "all-nappy-data?time=8";

    public static final String NAPPY_DATA_URL10 = "all-nappy-data?time=10";

    public static final String NAPPY_DATA_URL13 = "all-nappy-data?time=13";

    public static final String NAPPY_DATA_URL15 = "all-nappy-data?time=15";

    public static final String PROGRAM_CURRICULUMS_URL = "all-programs-data";

}
