package com.aoyuntek.aoyun.olddata.po.user.staff;

public class OldEmploymentPo {
    private String[] role;

    private Short employed_as;

    private String centre_oldId;

    private String enrolment_date;

    private String[] position;

    private String room_oldId;

    private String group_oldId;

    public String[] getRole() {
        return role;
    }

    public void setRole(String[] role) {
        this.role = role;
    }

    public Short getEmployed_as() {
        return employed_as;
    }

    public void setEmployed_as(Short employed_as) {
        this.employed_as = employed_as;
    }

    public String getCentre_oldId() {
        return centre_oldId;
    }

    public void setCentre_oldId(String centre_oldId) {
        this.centre_oldId = centre_oldId;
    }

    public String getEnrolment_date() {
        return enrolment_date;
    }

    public void setEnrolment_date(String enrolment_date) {
        this.enrolment_date = enrolment_date;
    }

    public String[] getPosition() {
        return position;
    }

    public void setPosition(String[] position) {
        this.position = position;
    }

    public String getRoom_oldId() {
        return room_oldId;
    }

    public void setRoom_oldId(String room_oldId) {
        this.room_oldId = room_oldId;
    }

    public String getGroup_oldId() {
        return group_oldId;
    }

    public void setGroup_oldId(String group_oldId) {
        this.group_oldId = group_oldId;
    }

}
