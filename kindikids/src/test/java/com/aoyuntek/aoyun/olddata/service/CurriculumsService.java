package com.aoyuntek.aoyun.olddata.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.aoyuntek.aoyun.constants.SystemConstants;
import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.NewsInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.entity.po.AttachmentInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramTaskExgrscInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskInstanceInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskResponsibleLogInfo;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.ProgramState;
import com.aoyuntek.aoyun.enums.ProgramWeeklyType;
import com.aoyuntek.aoyun.enums.TaskCategory;
import com.aoyuntek.aoyun.enums.TaskType;
import com.aoyuntek.aoyun.factory.TaskFactory;
import com.aoyuntek.aoyun.olddata.constants.HttpUrlConstants;
import com.aoyuntek.aoyun.olddata.po.curriculums.OldActivitiesPo;
import com.aoyuntek.aoyun.olddata.po.curriculums.OldCurriculumsInfo;
import com.aoyuntek.aoyun.service.old.IOldDataService;
import com.aoyuntek.aoyun.uitl.FilePathUtil;
import com.google.gson.reflect.TypeToken;
import com.theone.date.util.DateUtil;
import com.theone.json.util.JsonUtil;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

public class CurriculumsService extends BaseService {
    private static Logger log = Logger.getLogger(CurriculumsService.class);

    @Autowired
    private TaskFactory taskFactory;
    @Autowired
    private CentersInfoMapper centersInfoMapper;
    @Autowired
    private NewsInfoMapper newsInfoMapper;
    @Autowired
    private UserInfoMapper userInfoMapper;

    private List<TaskInstanceInfo> taskInstanceList = new ArrayList<TaskInstanceInfo>();

    private List<ProgramTaskExgrscInfo> taskList = new ArrayList<ProgramTaskExgrscInfo>();

    private List<AttachmentInfo> fileList = new ArrayList<AttachmentInfo>();

    private List<TaskResponsibleLogInfo> responsibleList = new ArrayList<TaskResponsibleLogInfo>();

    @Autowired
    private IOldDataService dataService;

    @Test
    public void importCurriculums() {
        List<OldCurriculumsInfo> oldList = new ArrayList<OldCurriculumsInfo>();
        try {
            System.err.println(System.currentTimeMillis());
            for (int i = 1; i <= 5; i++) {
                System.err.println(i);
                String json = getHttpJson(HttpUrlConstants.PROGRAM_CURRICULUMS_URL + "?page=" + i);
                List<OldCurriculumsInfo> tempList = JsonUtil.jsonToList(json, new TypeToken<List<OldCurriculumsInfo>>() {
                }.getType());
                oldList.addAll(tempList);
            }
            System.err.println(oldList.size());
            System.err.println(System.currentTimeMillis());
            initList(oldList);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        try {
            dataService.importProgramCurriculums(taskInstanceList, taskList, fileList, responsibleList);
        } catch (Exception e) {
            log.error("importProgramCurriculums error", e);
        }
        printLog(log);
        log.info("-------------import end------------------");
        log.info("old count:" + oldList.size());
        log.info("success count:" + taskList.size());
        log.info("success instance count:" + taskInstanceList.size());
    }

    private void initList(List<OldCurriculumsInfo> oldList) {
        for (OldCurriculumsInfo curriculumsInfo : oldList) {
            String oldCentreId = StringUtil.isEmpty(curriculumsInfo.getCentre_oldid()) ? "" : curriculumsInfo.getCentre_oldid();
            String oldRoomId = StringUtil.isEmpty(curriculumsInfo.getRoom_oldid()) ? "" : curriculumsInfo.getRoom_oldid();
            // 判斷園區和room是否存在
            String newCentreId = centersInfoMapper.getIdByOldId(oldCentreId);
            String newRoomId = centersInfoMapper.getIdByOldId(oldRoomId);
            if (StringUtil.isEmpty(newCentreId)) {
                super.exceptionList.add("error program centreId is empty---->" + super.formatJson(curriculumsInfo));
                continue;
            }
            if (StringUtil.isEmpty(newRoomId)) {
                super.exceptionList.add("error program roomId is empty--->" + super.formatJson(curriculumsInfo));
                continue;
            }
            Date day = parseDate(curriculumsInfo.getDay());

            if (day == null) {
                super.exceptionList.add("error program day is empty---->" + super.formatJson(curriculumsInfo));
                continue;
            }
            if ("2016-07-31".equals(DateUtil.format(day, "yyyy-MM-dd"))) {
                super.exceptionList.add("------------------------>" + super.formatJson(curriculumsInfo));
            }
            initTaskInfo(curriculumsInfo, newCentreId, newRoomId, day);
        }
    }

    private void initTaskInfo(OldCurriculumsInfo curriculumsInfo, String centreId, String roomId, Date day) {
        ProgramTaskExgrscInfo exgrscInfo = new ProgramTaskExgrscInfo();
        String id = getUUID();
        exgrscInfo.setId(id);
        exgrscInfo.setCenterId(centreId);
        exgrscInfo.setRoomId(roomId);
        exgrscInfo.setDay(day);

        String oldCreateAccountId = StringUtil.isEmpty(curriculumsInfo.getCreate_account_oldid()) ? "" : curriculumsInfo.getCreate_account_oldid();
        String oldUpdateAccountId = StringUtil.isEmpty(curriculumsInfo.getUpdate_account_oldid()) ? "" : curriculumsInfo.getUpdate_account_oldid();
        String newCreateAccountId = userInfoMapper.getAccountIdByUserOldId(oldCreateAccountId);
        String newUpdateAccountId = userInfoMapper.getAccountIdByUserOldId(oldUpdateAccountId);
        exgrscInfo.setCreateAccountId(newCreateAccountId);
        exgrscInfo.setUpdateAccountId(newUpdateAccountId);
        Date updateTime = parseDate(curriculumsInfo.getUpdate_time());
        exgrscInfo.setUpdateTime(updateTime);
        exgrscInfo.setCategory(curriculumsInfo.getCategory());
        exgrscInfo.setDeleteFlag(getDeleteFlag(curriculumsInfo.getDelete_flag()));
        exgrscInfo.setState(curriculumsInfo.getState());
        if("5604b8b00ea7b04332c4aef2".equals(curriculumsInfo.getProgram_oldId())){
            System.err.println(formatJson(curriculumsInfo));
        }
        exgrscInfo.setOldId(curriculumsInfo.getProgram_oldId());
        exgrscInfo.setReflection(curriculumsInfo.getEylf_reflection());
        exgrscInfo.setActivities(getActives(curriculumsInfo.getActivities()));
        taskList.add(exgrscInfo);
        initImages(curriculumsInfo.getPhotos(), id);
        if (curriculumsInfo.getCategory() == ProgramWeeklyType.ExploreCurriculum.getValue()) {
            getTaskInstance(TaskType.ExploreCurriculum, centreId, roomId, id, newCreateAccountId, day);
        } else {
            getTaskInstance(TaskType.GrowCurriculum, centreId, roomId, id, newCreateAccountId, day);
        }
        // TODO 都是1 類別
    }

    private short getDeleteFlag(Boolean deleteFlag) {
        if (deleteFlag == null || !deleteFlag) {
            return (short) 0;
        }
        return (short) 1;
    }

    private void initImages(List<String> photos, String sourceId) {
        for (String path : photos) {
            this.fileList.add(initAttachInfo(sourceId, path, FilePathUtil.PROGRAM_PATH, null));
        }
    }

    private String getActives(List<OldActivitiesPo> list) {
        if (ListUtil.isEmpty(list)) {
            return "";
        }
        String actives = "";
        for (OldActivitiesPo item : list) {
            if(!actives.contains(item.getText()+",")){
                actives += item.getText() + ",";
            }
        }
        return actives;
    }

    private void getTaskInstance(TaskType taskType, String centreId, String roomId, String valueId, String createAccountId, Date day) {
        Date[] dates = taskFactory.dealDate(taskType, centreId, day);
        //createAccountId = StringUtil.isEmpty(createAccountId) ? SystemConstants.JobCreateAccountId : createAccountId;
        TaskInstanceInfo model = new TaskInstanceInfo();
        String taskInstanceId = getUUID();
        model.setId(taskInstanceId);
        model.setCentreId(centreId);
        model.setRoomId(roomId);
        model.setObjId(null);
        model.setObjType(TaskCategory.ROOM.getValue());
        model.setCreateAccountId(createAccountId);
        model.setCreateTime(new Date());
        model.setDeleteFlag(DeleteFlag.Default.getValue());
        model.setStatu(ProgramState.Complete.getValue());
        model.setTaskModelId(centreId);
        model.setTaskType(taskType.getValue());
        model.setValueId(valueId);
        model.setBeginDate(dates[0]);
        model.setEndDate(dates[1]);
        this.taskInstanceList.add(model);

        if (StringUtil.isNotEmpty(createAccountId)) {
            this.responsibleList.add(getResponsiblePerson(taskInstanceId, createAccountId));
            return;
        }
        super.logList.add("can't find update accountId taskid=" + valueId);
    }

    public Date parseDate(String dateStr) {
        String[] freakyFormat = { "dd'st' MMM,yyyy", "dd'nd' MMM, yyyy", "dd'th' MMM, yyyy", "dd'rd' MMM, yyyy", "yyyy-MM-dd HH:mm" };
        Date date = null;
        for (String formatStr : freakyFormat) {
            boolean isOk = true;
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formatStr, Locale.ENGLISH);
            try {
                date = simpleDateFormat.parse(dateStr);
            } catch (ParseException e) {
                isOk = false;
            }
            if (isOk) {
                break;
            }
        }
        return date;
    }
}
