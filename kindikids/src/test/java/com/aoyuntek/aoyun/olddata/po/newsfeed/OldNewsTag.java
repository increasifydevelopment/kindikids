package com.aoyuntek.aoyun.olddata.po.newsfeed;

public class OldNewsTag {
    private String account_oldid;
    private String news_oldid;
    private boolean delete_flag;
    private String type;
    public String getAccount_oldid() {
        return account_oldid;
    }
    public void setAccount_oldid(String account_oldid) {
        this.account_oldid = account_oldid;
    }
    public String getNews_oldid() {
        return news_oldid;
    }
    public void setNews_oldid(String news_oldid) {
        this.news_oldid = news_oldid;
    }
    public boolean isDelete_flag() {
        return delete_flag;
    }
    public void setDelete_flag(boolean delete_flag) {
        this.delete_flag = delete_flag;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
}
