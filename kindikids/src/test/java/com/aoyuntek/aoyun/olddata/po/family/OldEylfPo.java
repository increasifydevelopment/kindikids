package com.aoyuntek.aoyun.olddata.po.family;

public class OldEylfPo {
    private String reason;
    
    private String age_scope;
    
    private String eylf_content;
    
    private String eylf_id;
    
    private String value;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getAge_scope() {
        return age_scope;
    }

    public void setAge_scope(String age_scope) {
        this.age_scope = age_scope;
    }

    public String getEylf_content() {
        return eylf_content;
    }

    public void setEylf_content(String eylf_content) {
        this.eylf_content = eylf_content;
    }

    public String getEylf_id() {
        return eylf_id;
    }

    public void setEylf_id(String eylf_id) {
        this.eylf_id = eylf_id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    
    
}
