package com.aoyuntek.aoyun.olddata.po.massage;

import com.aoyuntek.framework.model.BaseModel;

public class OldRecipient extends BaseModel {
private String look_account_id;
private String group_name_id;
public OldRecipient() {
    super();
}
public OldRecipient(String look_account_id, String group_name_id) {
    super();
    this.look_account_id = look_account_id;
    this.group_name_id = group_name_id;
}
public String getLook_account_id() {
    return look_account_id;
}
public void setLook_account_id(String look_account_id) {
    this.look_account_id = look_account_id;
}
public String getGroup_name_id() {
    return group_name_id;
}
public void setGroup_name_id(String group_name_id) {
    this.group_name_id = group_name_id;
}

}
