package com.aoyuntek.aoyun.olddata.po.family.child;

import java.util.List;

public class OldMedicalPo {
    private String other;
    private Boolean eczema;
    // TODO
    private List<OldDocumentPo> attachments;
    private Boolean terms_conditions;

    private Boolean epilepsy;

    private Boolean asthma;

    private Short medication_required;

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public List<OldDocumentPo> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<OldDocumentPo> attachments) {
        this.attachments = attachments;
    }

    public Short getMedication_required() {
        return medication_required;
    }

    public void setMedication_required(Short medication_required) {
        this.medication_required = medication_required;
    }

    public Boolean getEczema() {
        return eczema;
    }

    public void setEczema(Boolean eczema) {
        this.eczema = eczema;
    }

    public Boolean getTerms_conditions() {
        return terms_conditions;
    }

    public void setTerms_conditions(Boolean terms_conditions) {
        this.terms_conditions = terms_conditions;
    }

    public Boolean getEpilepsy() {
        return epilepsy;
    }

    public void setEpilepsy(Boolean epilepsy) {
        this.epilepsy = epilepsy;
    }

    public Boolean getAsthma() {
        return asthma;
    }

    public void setAsthma(Boolean asthma) {
        this.asthma = asthma;
    }

}
