package com.aoyuntek.aoyun.olddata.po.newsfeed;

import java.util.List;

public class OldNewsMain {

    private String create_Account_oldid;
    private String news_oldid;
    private boolean delete_flag;
    private String create_time;
    private String[] news_tag;
    private String title;
    private String update_time;
    private String content;
    private String status;
    private String[] nqs;
    private List<NewsImage> newsfeed_image;
    private String news_type;
    private int score;
    private String approve_Account_oldid;
    private String room_oldid;
    private String centres_oldid;
    private String apporve_time;
    private Object eylf;
    private String[] lesson_name;

    public String[] getLesson_name() {
        return lesson_name;
    }

    public void setLesson_name(String[] lesson_name) {
        this.lesson_name = lesson_name;
    }

    public Object getEylf() {
        return eylf;
    }

    public void setEylf(Object eylf) {
        this.eylf = eylf;
    }

    public String getCreate_Account_oldid() {
        return create_Account_oldid;
    }

    public void setCreate_Account_oldid(String create_Account_oldid) {
        this.create_Account_oldid = create_Account_oldid;
    }

    public String getNews_oldid() {
        return news_oldid;
    }

    public void setNews_oldid(String news_oldid) {
        this.news_oldid = news_oldid;
    }

    public boolean isDelete_flag() {
        return delete_flag;
    }

    public void setDelete_flag(boolean delete_flag) {
        this.delete_flag = delete_flag;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String[] getNews_tag() {
        return news_tag;
    }

    public void setNews_tag(String[] news_tag) {
        this.news_tag = news_tag;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String[] getNqs() {
        return nqs;
    }

    public void setNqs(String[] nqs) {
        this.nqs = nqs;
    }

    public List<NewsImage> getNewsfeed_image() {
        return newsfeed_image;
    }

    public void setNewsfeed_image(List<NewsImage> newsfeed_image) {
        this.newsfeed_image = newsfeed_image;
    }

    public String getNews_type() {
        return news_type;
    }

    public void setNews_type(String news_type) {
        this.news_type = news_type;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getApprove_Account_oldid() {
        return approve_Account_oldid;
    }

    public void setApprove_Account_oldid(String approve_Account_oldid) {
        this.approve_Account_oldid = approve_Account_oldid;
    }

    public String getRoom_oldid() {
        return room_oldid;
    }

    public void setRoom_oldid(String room_oldid) {
        this.room_oldid = room_oldid;
    }

    public String getCentres_oldid() {
        return centres_oldid;
    }

    public void setCentres_oldid(String centres_oldid) {
        this.centres_oldid = centres_oldid;
    }

    public String getApporve_time() {
        return apporve_time;
    }

    public void setApporve_time(String apporve_time) {
        this.apporve_time = apporve_time;
    }

}
