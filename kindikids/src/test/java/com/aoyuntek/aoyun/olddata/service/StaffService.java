package com.aoyuntek.aoyun.olddata.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.aoyuntek.aoyun.condtion.UserDetailCondition;
import com.aoyuntek.aoyun.dao.AccountInfoMapper;
import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.entity.po.AccountInfo;
import com.aoyuntek.aoyun.entity.po.AccountRoleInfo;
import com.aoyuntek.aoyun.entity.po.AttachmentInfo;
import com.aoyuntek.aoyun.entity.po.CredentialsInfo;
import com.aoyuntek.aoyun.entity.po.EmergencyContactInfo;
import com.aoyuntek.aoyun.entity.po.MedicalImmunisationInfo;
import com.aoyuntek.aoyun.entity.po.MedicalInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.enums.Credential;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.enums.StaffType;
import com.aoyuntek.aoyun.enums.UserType;
import com.aoyuntek.aoyun.olddata.constants.HttpUrlConstants;
import com.aoyuntek.aoyun.olddata.po.user.OldEmergencyContactPo;
import com.aoyuntek.aoyun.olddata.po.user.OldStaffVo;
import com.aoyuntek.aoyun.olddata.po.user.OldStatusPo;
import com.aoyuntek.aoyun.olddata.po.user.OldUserPo;
import com.aoyuntek.aoyun.olddata.po.user.staff.OldEmploymentPo;
import com.aoyuntek.aoyun.olddata.po.user.staff.OldPreferredMethodPo;
import com.aoyuntek.aoyun.olddata.po.user.staff.credentials.CredentialsWorkPo;
import com.aoyuntek.aoyun.olddata.po.user.staff.credentials.OldAidPo;
import com.aoyuntek.aoyun.olddata.po.user.staff.credentials.OldQualificationPo;
import com.aoyuntek.aoyun.olddata.po.user.staff.credentials.OldSupervisorPo;
import com.aoyuntek.aoyun.olddata.po.user.staff.credentials.OldTrainingPo;
import com.aoyuntek.aoyun.olddata.po.user.staff.medical.OldImmunisationPo;
import com.aoyuntek.aoyun.olddata.po.user.staff.medical.OldMedicalChildPo;
import com.aoyuntek.aoyun.service.old.IOldDataService;
import com.aoyuntek.aoyun.uitl.FilePathUtil;
import com.google.gson.reflect.TypeToken;
import com.theone.common.util.ServiceResult;
import com.theone.date.util.DateUtil;
import com.theone.json.util.JsonUtil;
import com.theone.string.util.StringUtil;

/**
 * 
 * @description 导入 centre,room,group信息
 * @author gfwang
 * @create 2016年12月5日上午9:58:19
 * @version 1.0
 */
public class StaffService extends BaseService {
    private static Logger log = Logger.getLogger(StaffService.class);

    @Autowired
    private CentersInfoMapper centersInfoMapper;
    @Autowired
    private UserInfoMapper userInfoMapper;
    private List<UserInfo> userList = new ArrayList<UserInfo>();
    private List<AccountInfo> accountList = new ArrayList<AccountInfo>();
    private List<AccountRoleInfo> accountRoleList = new ArrayList<AccountRoleInfo>();
    private List<EmergencyContactInfo> contactList = new ArrayList<EmergencyContactInfo>();
    private List<CredentialsInfo> credentialsList = new ArrayList<CredentialsInfo>();
    private List<MedicalInfo> medicalList = new ArrayList<MedicalInfo>();
    private List<MedicalImmunisationInfo> immunisationList = new ArrayList<MedicalImmunisationInfo>();
    private List<AttachmentInfo> attachList = new ArrayList<AttachmentInfo>();

    private List<String> centerMangerList = new ArrayList<String>();
    private Map<String, String> groupMangerMap = new HashMap<String, String>();

    @Autowired
    private IOldDataService oldDataService;

    @Autowired
    private AccountInfoMapper accountInfoMapper;

    @Test
    public void importStaff() {
        log.info(formatLog("importStaff start"));
        List<OldStaffVo> oldStaffList = new ArrayList<OldStaffVo>();
        int count = 0;
        StringBuffer sb = new StringBuffer();
        int foundCount = 0;
        try {
            String json = getHttpJson(HttpUrlConstants.STAFF_URL);
            oldStaffList = JsonUtil.jsonToList(json, new TypeToken<List<OldStaffVo>>() {
            }.getType());
            for (OldStaffVo oldStaff : oldStaffList) {
                String oldUserId = oldStaff.getStaff().getBasic_info().getStaff_oldId();
                if ("5548096400b5ca45297ecea0".equals(oldUserId)) {
                    System.err.println("-->" + formatJson(oldStaff));
                }
                UserInfo user = userInfoMapper.getUserInfoByOldId(oldUserId);
                String userId = getUUID();
                String accountId = getUUID();
                if (null != user) {
                    foundCount++;
                    userId = user.getId();
                    accountId = accountInfoMapper.getAccountInfoByUserId(userId).getId();
                    userInfoMapper.deleteByPrimaryKey(userId);
                    accountInfoMapper.deleteByPrimaryKey(accountId);
                }
                ServiceResult<Object> result = importUserAccountInfo(oldStaff, accountId, userId);
                if (!result.isSuccess()) {
                    count++;
                    super.exceptionList.add(result.getMsg());
                    continue;
                }
                Date createTime = (Date) result.getReturnObj();
                importConcat(userId, oldStaff, createTime);
                importCredentials(userId, oldStaff);
                importMedical(userId, oldStaff);
                importImmunisation(userId, oldStaff);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        System.err.println(foundCount);
        try {
            log.info("====================================start insert=====================");
            oldDataService.importStaff(this.userList, this.accountList, this.accountRoleList, this.contactList, this.credentialsList,
                    this.medicalList, this.immunisationList, this.attachList, this.centerMangerList, this.groupMangerMap);
        } catch (Exception e) {
            e.printStackTrace();
        }

        log.info("====================================end insert=====================");
        printLog(this.log);
        log.info("oldStaffList count=" + oldStaffList.size());
        log.info("oldStaffList no import count=" + count);
    }

    private void importImmunisation(String userId, OldStaffVo oldStaff) {
        OldImmunisationPo oldImmunisation = oldStaff.getStaff().getMedical().getImmunisation();
        initImmunisation(oldImmunisation.getPertussis(), "1", userId);
        initImmunisation(oldImmunisation.getMeasles_mumps_rubella(), "2", userId);
        initImmunisation(oldImmunisation.getVaricella(), "3", userId);
        initImmunisation(oldImmunisation.getHepatitis_a(), "4", userId);
        initImmunisation(oldImmunisation.getHepatitis_b(), "5", userId);
        initImmunisation(oldImmunisation.getInfluenza(), "6", userId);
    }

    private void initImmunisation(Object obj, String name, String userId) {
        MedicalImmunisationInfo medicalImmunisationInfo = new MedicalImmunisationInfo();
        medicalImmunisationInfo.setDeleteFlag(DeleteFlag.Default.getValue());
        medicalImmunisationInfo.setUserId(userId);
        medicalImmunisationInfo.setName(name);
        medicalImmunisationInfo.setId(getUUID());
        String json = JsonUtil.objectToJson(obj);
        Map<String, Object> map = (Map<String, Object>) JsonUtil.jsonToMap(json);
        for (String key : map.keySet()) {
            if ("confirmed_infection".equals(key)) {
                medicalImmunisationInfo.setConfirmed((Boolean) map.get(key));
            } else if ("date_of_confirmed_infection".equals(key)) {
                if ((Boolean) map.get("confirmed_infection")) {
                    medicalImmunisationInfo.setConfirmedDate(parseDate(map.get(key).toString()));
                }
            } else if ("had_vaccine".equals(key)) {
                medicalImmunisationInfo.setHadVaccine((Boolean) map.get(key));
            } else if ("date_of_vaccine".equals(key)) {
                if ((Boolean) map.get("had_vaccine")) {
                    medicalImmunisationInfo.setVaccineDate(parseDate(map.get(key).toString()));
                }
            }
        }
        this.immunisationList.add(medicalImmunisationInfo);
    }

    private void importMedical(String userId, OldStaffVo oldStaff) {
        OldMedicalChildPo oldMedicalPo = oldStaff.getStaff().getMedical().getMedical();
        MedicalInfo medicalInfo = new MedicalInfo();
        medicalInfo.setDeleteFlag(DeleteFlag.Default.getValue());
        medicalInfo.setId(getUUID());
        medicalInfo.setUserId(userId);
        medicalInfo.setCondition(oldMedicalPo.getMedical_condition());
        medicalInfo.setAllergies(oldMedicalPo.getAllergies());
        medicalInfo.setMedication(oldMedicalPo.getTaking_regular_medication());
        medicalInfo.setCertificate(oldMedicalPo.getCertificate_provided());
        this.medicalList.add(medicalInfo);
    }

    // 初始化证书
    private void importCredentials(String userId, OldStaffVo oldStaff) {
        List<OldQualificationPo> oldQualifications = oldStaff.getStaff().getCredentials().getQualification();
        List<CredentialsWorkPo> working_with_children_check = oldStaff.getStaff().getCredentials().getWorking_with_children_check();
        List<OldAidPo> first_aid = oldStaff.getStaff().getCredentials().getFirst_aid();
        List<OldTrainingPo> training = oldStaff.getStaff().getCredentials().getTraining();
        List<OldSupervisorPo> certified_supervisor = oldStaff.getStaff().getCredentials().getCertified_supervisor();

        // Qualification
        for (OldQualificationPo oldItem : oldQualifications) {
            CredentialsInfo credentialsInfo = new CredentialsInfo();
            initCredentialsInfo(credentialsInfo, Credential.Qualification, userId);

            credentialsInfo.setQualObtained(oldItem.getQualification_obtained());
            credentialsInfo.setCompletedDate(parseDate(oldItem.getDate_completed()));
            String path = oldItem.getUpload_certificate();
            addCredentialsFile(path, credentialsInfo);
            this.credentialsList.add(credentialsInfo);
        }
        // Working with Children's Check
        for (CredentialsWorkPo oldItem : working_with_children_check) {
            CredentialsInfo credentialsInfo = new CredentialsInfo();
            initCredentialsInfo(credentialsInfo, Credential.ChildrenCheck, userId);

            credentialsInfo.setCheckNum(oldItem.getCheck_number());
            credentialsInfo.setExpiryDate(parseDate(oldItem.getDate_of_Expiry()));

            String pathVer = "";
            if (oldItem.getEmployer_verification() instanceof java.lang.String) {
                pathVer = (String) oldItem.getEmployer_verification();
                if (StringUtil.isNotEmpty(pathVer) && !"path".equals(pathVer)) {
                    String employVerifyAttachId = getUUID();
                    credentialsInfo.setEmployVerifyAttachId(employVerifyAttachId);
                    this.attachList.add(initAttachInfo(employVerifyAttachId, pathVer, FilePathUtil.STAFF_PATH, null));
                }
            } else {
                super.logList.add("mytest------------>" + pathVer.toString());
            }

            if (oldItem.getCheck_clearance_letter() instanceof java.lang.String) {
                String pathLetter = (String) oldItem.getCheck_clearance_letter();
                if (StringUtil.isNotEmpty(pathLetter) && !"path".equals(pathVer)) {
                    String letterId = getUUID();
                    credentialsInfo.setLetterImgId(letterId);
                    this.attachList.add(initAttachInfo(letterId, pathLetter, FilePathUtil.STAFF_PATH, null));
                }
            } else {
                super.logList.add("mytest------------>" + pathVer.toString());
            }
            this.credentialsList.add(credentialsInfo);
        }
        // First Aid
        for (OldAidPo oldItem : first_aid) {
            CredentialsInfo credentialsInfo = new CredentialsInfo();
            initCredentialsInfo(credentialsInfo, Credential.FirstAids, userId);
            credentialsInfo.setCourseName(oldItem.getFirst_aid_course_name());
            credentialsInfo.setExpiryDate(parseDate(oldItem.getDate_of_Expiry()));
            credentialsInfo.setCompletedDate(parseDate(oldItem.getDate_completed()));

            String path = oldItem.getUpload_certificate();
            addCredentialsFile(path, credentialsInfo);
            this.credentialsList.add(credentialsInfo);
        }
        // Professional Development Training
        for (OldTrainingPo oldItem : training) {
            CredentialsInfo credentialsInfo = new CredentialsInfo();
            initCredentialsInfo(credentialsInfo, Credential.Trainings, userId);
            credentialsInfo.setCourseName(oldItem.getCourse_name());
            credentialsInfo.setCompletedDate(parseDate(oldItem.getDate_completed()));

            String path = oldItem.getUpload_certificate();
            addCredentialsFile(path, credentialsInfo);
            this.credentialsList.add(credentialsInfo);
        }
        // Certified Supervisors
        for (OldSupervisorPo oldItem : certified_supervisor) {
            CredentialsInfo credentialsInfo = new CredentialsInfo();
            initCredentialsInfo(credentialsInfo, Credential.Supervisors, userId);
            String path = oldItem.getUpload_certificate();
            addCredentialsFile(path, credentialsInfo);
            this.credentialsList.add(credentialsInfo);
        }

    }

    private void addCredentialsFile(String path, CredentialsInfo credentialsInfo) {
        if (!StringUtil.isEmpty(path) && !"path".equals(path)) {
            String uploadCertificateId = getUUID();
            credentialsInfo.setCertificateImgId(uploadCertificateId);
            this.attachList.add(initAttachInfo(uploadCertificateId, path, FilePathUtil.STAFF_PATH, null));
        }
    }

    private void initCredentialsInfo(CredentialsInfo credentialsInfo, Credential type, String userId) {
        credentialsInfo.setId(getUUID());
        credentialsInfo.setUserId(userId);
        credentialsInfo.setDeleteFlag(DeleteFlag.Default.getValue());
        credentialsInfo.setType(type.getValue());
    }

    private void importConcat(String userId, OldStaffVo oldStaff, Date date) {
        ServiceResult<Object> result = new ServiceResult<Object>();
        result.setSuccess(false);
        List<OldEmergencyContactPo> oldContact = oldStaff.getStaff().getBasic_info().getEmergency_contact();
        int count = 0;
        for (OldEmergencyContactPo oldItem : oldContact) {
            String concatName = oldItem.getName();
            EmergencyContactInfo contactInfo = new EmergencyContactInfo();
            contactInfo.setId(getUUID());
            contactInfo.setAddress(oldItem.getAddress());
            contactInfo.setSuburb(oldItem.getCity());
            contactInfo.setName(concatName);
            contactInfo.setPhoneNum(oldItem.getPhone_number());
            contactInfo.setPostcode(oldItem.getPostcode());
            contactInfo.setRelation(oldItem.getRelation());
            contactInfo.setState(oldItem.getState());
            contactInfo.setUserId(userId);
            try {
                contactInfo.setCreateTime(DateUtil.addSeconds(date, count));
            } catch (Exception e) {
                System.err.println(formatJson(oldStaff));
            }

            contactInfo.setDeleteFlag(DeleteFlag.Default.getValue());
            count++;
            contactList.add(contactInfo);
        }
    }

    private ServiceResult<Object> importUserAccountInfo(OldStaffVo oldStaff, String accountId, String userId) {
        UserInfo userInfo = new UserInfo();
        OldUserPo oldBaseInfo = oldStaff.getStaff().getBasic_info();
        ServiceResult<Object> result = new ServiceResult<Object>();
        result.setSuccess(false);
        OldEmploymentPo oldEmploymentPo = oldStaff.getStaff().getEmployment();

        String oldId = oldBaseInfo.getStaff_oldId();
        String email = oldBaseInfo.getEmail();
        result.setMsg(email);
        if ("admin@kindikids.com.au".equals(email)) {
            result.setMsg("error admin@kindikids.com.au" + formatJson(oldStaff));
            return result;
        }
        if (StringUtil.isEmpty(oldBaseInfo.getFirst_name())) {
            result.setMsg("error can't find first name--" + email + "--oldId=" + oldId + "-> " + formatJson(oldStaff));
            return result;
        }
        if (StringUtil.isEmpty(email)) {
            result.setMsg("error can't find email------" + email + "--oldId=" + oldId + "-> " + formatJson(oldStaff));
            return result;
        }
        // 判断邮箱是否有重复
        UserDetailCondition condition = new UserDetailCondition();
        condition.setEmail(email);
        if (userInfoMapper.getByCondition(condition).size() > 0 || repeatFactory.containsKey(email)) {
            result.setMsg("error email is repeat------" + email + "--oldId=" + oldId + "-> " + formatJson(oldStaff));
            return result;
        }

        String newCenterId = centersInfoMapper.getIdByOldId(oldBaseInfo.getCentre_oldId());
        String newRoomId = centersInfoMapper.getIdByOldId(oldBaseInfo.getRoom_oldId());
        String newGroupId = centersInfoMapper.getIdByOldId(oldBaseInfo.getGroup_oldId());

        Date createTime = getDateByOldId(oldId);
        userInfo.setId(userId);
        userInfo.setOldId(oldId);
        userInfo.setLastName(oldBaseInfo.getLast_name());
        userInfo.setAddress(oldBaseInfo.getAddress());
        userInfo.setSuburb(oldBaseInfo.getSuburb());
        userInfo.setChildrenNum(oldBaseInfo.getNum_of_children());
        userInfo.setMiddleName(oldBaseInfo.getMiddle_name());
        // userInfo.oldBaseInfo.getPreferred_method_of_contact();
        userInfo.setBirthday(parseDate(oldBaseInfo.getDate_of_birth()));
        userInfo.setFirstName(oldBaseInfo.getFirst_name());
        userInfo.setState(oldBaseInfo.getState());
        userInfo.setPhoneNumber(oldBaseInfo.getHome_phone_number());
        userInfo.setEmail(oldBaseInfo.getEmail());
        userInfo.setEthnicity(oldBaseInfo.getEthnicity());
        userInfo.setPostcode(oldBaseInfo.getPostcode());

        // 处理头像
        userInfo.setAvatar(initAvatar(oldBaseInfo.getUser_image()));

        userInfo.setGender(getGender(oldBaseInfo.getGender()));
        userInfo.setMaritalStatus(getMaritalStatus(oldBaseInfo.getMarital_status()));
        userInfo.setChildrenAge(oldBaseInfo.getAges_of_children());
        userInfo.setMobileNumber(oldBaseInfo.getMobile_number());
        userInfo.setCentersId(newCenterId);
        userInfo.setRoomId(newRoomId);
        userInfo.setGroupId(newGroupId);
        userInfo.setCreateTime(createTime);
        userInfo.setCreateAccountId(this.defaultAccountId);
        userInfo.setUpdateAccountId(this.defaultAccountId);
        userInfo.setUpdateTime(createTime);
        userInfo.setUserType(UserType.Staff.getValue());
        OldStatusPo oldState = oldStaff.getStaff().getStatus();
        userInfo.setDeleteFlag(oldState.getDelete_flag());
        userInfo.setStaffInDate(parseDate(oldEmploymentPo.getEnrolment_date()));

        OldPreferredMethodPo preferredMethod = oldBaseInfo.getPreferred_method_of_contact();
        userInfo.setContactEmail(preferredMethod.getVia_email());
        userInfo.setContactLetter(preferredMethod.getVia_letter());
        userInfo.setContactPhone(preferredMethod.getVia_phone_call());
        userInfo.setContactPerson(preferredMethod.getIn_person());

        String color = getColor(this.userList.size() + 1);
        userInfo.setPersonColor(color);

        Short staffType = oldEmploymentPo.getEmployed_as();
        if (staffType != null && 2 != staffType) {
            userInfo.setStaffType(staffType);
        }

        AccountInfo accountInfo = new AccountInfo();
        accountInfo.setId(accountId);
        accountInfo.setUserId(userId);
        accountInfo.setAccount(oldBaseInfo.getEmail());
        accountInfo.setDeleteFlag(oldState.getDelete_flag());
        accountInfo.setStatus(oldState.getStatus());
        accountInfo.setUpdateAccountId(this.defaultAccountId);
        accountInfo.setUpdateTime(createTime);
        accountInfo.setCreateAccountId(this.defaultAccountId);
        accountInfo.setCreateTime(createTime);
        accountInfo.setPsw(oldBaseInfo.getPassword());

        // 角色id
        Set<String> roleIds = new HashSet<String>();

        result = initRoleFunction(oldStaff, roleIds, userInfo, email, oldId);
        if (!result.isSuccess()) {
            return result;
        }
        repeatFactory.put(email, email);
        // 整合数据
        this.userList.add(userInfo);
        this.accountList.add(accountInfo);
        for (String roleId : roleIds) {
            this.accountRoleList.add(new AccountRoleInfo(getUUID(), accountId, roleId, createTime, this.defaultAccountId, createTime,
                    this.defaultAccountId));
        }

        result.setSuccess(true);
        result.setReturnObj(createTime);
        return result;
    }

    private ServiceResult<Object> initRoleFunction(OldStaffVo oldStaff, Set<String> roleIds, UserInfo user, String email, String oldId) {
        OldEmploymentPo oldEmploymentPo = oldStaff.getStaff().getEmployment();
        ServiceResult<Object> result = new ServiceResult<Object>();
        result.setSuccess(false);
        String centerId = user.getCentersId();
        String roomId = user.getRoomId();
        String groupId = user.getGroupId();
        String userId = user.getId();

        // 例外处理
        if ("k.iwassi@hotmail.com".equals(email) || "cgallo91@hotmail.com".equals(email) || "sandrinebaps@hotmail.com".equals(email)
                || "annamariagago@gmail.com".equals(email) || "kemiadio@hotmail.com".equals(email)) {
            // 设置为园长，二级园长
            oldEmploymentPo.setRole(new String[] { "4", "7" });
        }

        // 老师例外
        /*
         * if("***brittneyfitzpatrick@hotmail.com***".equals(email)||
         * "danika.susino@y7mail.com**".equals(email)){
         * oldEmploymentPo.setRole(new String[] { "4"}); }
         */

        for (String key : oldEmploymentPo.getRole()) {
            Role itemRole = getNewRole(key);
            boolean centreEmpty = StringUtil.isEmpty(centerId) || "0".equals(centerId);
            boolean roomEmpty = StringUtil.isEmpty(roomId) || "0".equals(centerId);
            boolean groupEmpty = StringUtil.isEmpty(groupId) || "0".equals(centerId);

            if ("vincent@increasify.com.au".equals(email)) {
                itemRole = Role.CEO;
            }

            if (itemRole == null) {
                result.setMsg("error can't find role-----" + email + "--oldId=" + oldId + "-> " + formatJson(oldStaff));
                itemRole = Role.Educator;
            }
            switch (itemRole) {
            case CEO:
                if (!centreEmpty) {
                    result.setMsg("warning ceo not allow have centre-----" + email + "--oldId=" + oldId + "--> " + formatJson(oldStaff));
                    user.setCentersId(null);
                    user.setRoomId(null);
                    user.setGroupId(null);
                }
                break;
            case CentreManager:
                if (centreEmpty) {
                    result.setMsg("error CentreManager must be have centre-----" + email + "--oldId=" + oldId + "-> " + formatJson(oldStaff));
                    user.setRoomId(null);
                    user.setGroupId(null);
                    user.setStaffType((short) 2);
                    // 所有有问题的人都导入成兼职
                    setEduCaual(roleIds);
                    return successResult();
                }
                // 判断是否已经有园长
                if (isCenterManager(email)) {
                    this.centerMangerList.add(userId);
                } else {
                    result.setMsg("error center alerdy have centerManager----" + email + "--centerid=" + centerId + "--oldId=" + oldId + "-->"
                            + formatJson(oldStaff));
                    // 所有有问题的人都导入成兼职
                    setEduCaual(roleIds);
                    user.setStaffType((short) 2);
                    return successResult();
                }
                break;
            case Educator:
                // 老师是否有园区
                if (centreEmpty) {
                    result.setMsg("error Educator must be have centre-----" + email + "--oldId=" + oldId + "---> " + formatJson(oldStaff));
                    user.setCentersId(null);
                    // 所有有问题的人都导入成兼职
                    setEduCaual(roleIds);
                    user.setStaffType((short) 2);
                    return successResult();
                } else if (roomEmpty) {// 老师是否有room
                    result.setMsg("error Educator must be have room-----" + email + "--oldId=" + oldId + "---> " + formatJson(oldStaff));
                    user.setCentersId(null);
                    user.setRoomId(null);
                    // 所有有问题的人都导入成兼职
                    setEduCaual(roleIds);
                    user.setStaffType((short) 2);
                    return successResult();
                } else if (groupEmpty) {// 老师是否有group
                    result.setMsg("error Educator must be have group-----" + email + "--oldId=" + oldId + "--> " + formatJson(oldStaff));
                    user.setCentersId(null);
                    user.setRoomId(null);
                    user.setGroupId(null);
                    // 所有有问题的人都导入成兼职
                    setEduCaual(roleIds);
                    user.setStaffType((short) 2);
                    return successResult();
                }
                // 判断组里是否有老师

                if (!groupMangerMap.containsKey(groupId)) {
                    groupMangerMap.put(groupId, userId);
                } else {
                    result.setMsg("error group already have Educator----" + email + "--oldId=" + oldId + "--groupid=" + groupId + "-->"
                            + formatJson(oldStaff));
                    user.setCentersId(null);
                    user.setRoomId(null);
                    user.setGroupId(null);
                    // 所有有问题的人都导入成兼职
                    setEduCaual(roleIds);
                    user.setStaffType((short) 2);
                    return successResult();
                }

                break;
            case Cook:
                // 厨师是否有园区
                if (centreEmpty) {
                    result.setMsg("error Cook must be have centre-----" + email + "--oldId=" + oldId + "---> " + formatJson(oldStaff));
                    return result;
                }
                user.setRoomId(null);
                user.setGroupId(null);
                break;

            case Casual:
                result.setMsg("warning Casual not allow have centre-----" + email + "--oldId=" + oldId + "----> " + formatJson(oldStaff));
                user.setCentersId(null);
                user.setRoomId(null);
                user.setGroupId(null);
                user.setStaffType(StaffType.Casual.getValue());
                roleIds.add(super.roleMap.get(Role.Educator.getValue()));
                break;
            default:
                break;
            }

            roleIds.add(super.roleMap.get(itemRole.getValue()));
        }

        /*
         * if ("2".equals(oldEmploymentPo.getEmployed_as() == null ? null :
         * oldEmploymentPo.getEmployed_as().toString()) &&
         * StringUtil.isNotEmpty(centerId)) {
         * result.setMsg("warning cauals have centre--" + email + "--> " +
         * formatJson(oldStaff)); user.setCentersId(null); // 为兼职，加一个角色
         * roleIds.add(super.roleMap.get(Role.Casual.getValue())); }
         */
        result.setSuccess(true);
        return result;
    }

    private void setEduCaual(Set<String> roleIds) {
        roleIds.clear();
        roleIds.add(super.roleMap.get(Role.Educator.getValue()));
        roleIds.add(super.roleMap.get(Role.Casual.getValue()));
    }

    private ServiceResult<Object> successResult() {
        ServiceResult<Object> result = new ServiceResult<Object>();
        result.setSuccess(true);
        return result;
    }

    // 判断是否为园长
    private boolean isCenterManager(String email) {
        // 园1的园长
        if ("e700_5@hotmail.com".equals(email)) {
            return true;
        } else if ("centre2@kindikids.com.au".equals(email)) {
            // 园二的园长
            return true;
        } else if ("cgallo91@hotmail.com".equals(email)) {
            // 园3的园长
            return true;
        }
        return false;
    }

    private Short getMaritalStatus(String name) {
        Short result = null;
        name = name.toLowerCase();
        switch (name) {
        case "never married":
            result = 0;
            break;
        case "married":
            result = 1;
            break;
        case "de facto":
            result = 2;
            break;
        case "widowed":
            result = 3;
            break;
        default:
            break;
        }
        return result;
    }

    private Role getNewRole(String role) {
        switch (role) {
        case "1":
            return Role.Parent;
        case "2":
            return Role.CEO;
        case "3":
            return Role.CentreManager;
        case "4":
            return Role.Educator;
        case "5":
            return Role.Cook;
        case "6":
            return Role.Casual;
        case "7":
            return Role.EducatorSecondInCharge;
        default:
            break;
        }
        return null;
    }

}
