package com.aoyuntek.aoyun.olddata.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.RoomInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.entity.po.AttachmentInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramIntobsleaInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskInstanceInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskResponsibleLogInfo;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.ProgramState;
import com.aoyuntek.aoyun.enums.TaskCategory;
import com.aoyuntek.aoyun.enums.TaskType;
import com.aoyuntek.aoyun.factory.TaskFactory;
import com.aoyuntek.aoyun.olddata.constants.HttpUrlConstants;
import com.aoyuntek.aoyun.olddata.po.programIntobslea.ProgramIntobslea;
import com.aoyuntek.aoyun.service.old.IOldDataService;
import com.aoyuntek.aoyun.service.task.ITaskInstance;
import com.aoyuntek.aoyun.uitl.FilePathUtil;
import com.google.gson.reflect.TypeToken;
import com.theone.common.util.ServiceResult;
import com.theone.json.util.JsonUtil;
import com.theone.string.util.StringUtil;

/**
 * @description 导入Intobslea
 * @author Hxzhang 2016年12月20日下午1:58:56
 */
public class OldProgramIntobsleaService extends BaseService {
    private static Logger log = Logger.getLogger(OldProgramIntobsleaService.class);
    @Autowired
    private CentersInfoMapper centersInfoMapper;
    @Autowired
    private RoomInfoMapper roomInfoMapper;
    @Autowired
    private UserInfoMapper userInfoMapper;
    @Autowired
    private IOldDataService oldDataService;
    @Autowired
    private ITaskInstance taskInstance;
    @Autowired
    private TaskFactory taskFactory;

    private List<AttachmentInfo> fileList = new ArrayList<AttachmentInfo>();
    private List<ProgramIntobsleaInfo> programIntobsleaInfos = new ArrayList<ProgramIntobsleaInfo>();
    private List<TaskInstanceInfo> taskInstanceInfos = new ArrayList<TaskInstanceInfo>();
    private List<TaskResponsibleLogInfo> responsibleList = new ArrayList<TaskResponsibleLogInfo>();

    @Test
    public void importIntobslea() {
        getOrgsMap(centersInfoMapper);
        getUserMap(userInfoMapper);

        String intobsleaJson = getHttpJson(HttpUrlConstants.PROGRAM_OBSERVATION_INTERESTS_URL);
        operateIntobslea(intobsleaJson);
    }

    private void operateIntobslea(String intobsleaJson) {
        List<ProgramIntobslea> intobsleas = JsonUtil.jsonToList(intobsleaJson, new TypeToken<List<ProgramIntobslea>>() {
        }.getType());
        int count = 0;
        for (ProgramIntobslea i : intobsleas) {
            ServiceResult<Object> result = validateIntobslea(i);
            if (!result.isSuccess()) {
                count++;
                super.exceptionList.add(result.getMsg());
                continue;
            }
            // Explore Curriculum:0 Grow Curriculum:1 School Readiness:2
            switch (i.getCategory()) {
            case 0:
                i.setCategory(TaskType.ExploreCurriculum.getValue());
                break;
            case 1:
                i.setCategory(TaskType.GrowCurriculum.getValue());
                break;
            case 2:
                i.setCategory(TaskType.SchoolReadiness.getValue());
                break;
            default:
                break;
            }
            // interest:0 observation:1 learning story:2
            switch (i.getType()) {
            case 0:
                i.setNewType(TaskType.Interest.getValue());
                i.setInob(i.getInterest_name());
                i.setInobstr(i.getInterest_evolved());
                break;
            case 1:
                i.setNewType(TaskType.Observation.getValue());
                i.setInob(i.getObservation());
                i.setInobstr(i.getLearning());
                break;
            case 2:
                i.setNewType(TaskType.LearningStory.getValue());
                break;
            default:
                break;
            }

            ProgramIntobsleaInfo intobsleaInfo = new ProgramIntobsleaInfo(i.getNewType(), i.getCenterNewId(), i.getRoomNewId(), i.getChildNewId(),
                    i.getCategory(), i.getInob(), conDate(i.getEvaluation_date()), i.getInobstr(), i.getObjective(), i.getEducator_experience(),
                    conDate(i.getDay()), i.getNewsfeed_oldid(), i.getState(), i.getCreateNewAccountId(), conDate(i.getUpdate_time()),
                    i.getUpdateNewAccountId(), i.getOldid());

            programIntobsleaInfos.add(intobsleaInfo);

            Date[] dates = taskFactory.dealDate(getTaskType(intobsleaInfo.getType()), intobsleaInfo.getCenterId(), intobsleaInfo.getDay());
            TaskInstanceInfo model = new TaskInstanceInfo();
            String taskInstanceId = getUUID();
            model.setId(taskInstanceId);
            model.setCentreId(intobsleaInfo.getCenterId());
            model.setRoomId(intobsleaInfo.getRoomId());
            model.setObjId(null);
            model.setObjType(TaskCategory.ROOM.getValue());
            model.setCreateAccountId(intobsleaInfo.getCreateAccountId());
            model.setCreateTime(new Date());
            model.setDeleteFlag(DeleteFlag.Default.getValue());
            model.setStatu(ProgramState.Complete.getValue());
            model.setTaskModelId(intobsleaInfo.getCenterId());
            model.setTaskType(intobsleaInfo.getType());
            model.setValueId(intobsleaInfo.getId());
            model.setBeginDate(dates[0]);
            model.setEndDate(dates[1]);
            taskInstanceInfos.add(model);

            initImages(i.getPhotos(), intobsleaInfo.getId());

            String newUpdateAccountId = userMap.get(i.getUpdate_account_oldid());
            if (StringUtil.isNotEmpty(newUpdateAccountId)) {
                responsibleList.add(getResponsiblePerson(taskInstanceId, newUpdateAccountId));
            } else {
                count++;
                super.exceptionList.add("can't find update accountId taskid=" + intobsleaInfo.getId());
            }
        }
        try {
            oldDataService.importProgramIntobslea(programIntobsleaInfos, taskInstanceInfos, responsibleList, fileList);
        } catch (Exception e) {
            log.error("importProgramIntobslea error", e);
        }
        printLog(this.log);
        log.info("Normal Data=" + programIntobsleaInfos.size());
        log.info("Error Data=" + count);
        log.error("Total Data=" + intobsleas.size());
        log.error("Normal Data=" + programIntobsleaInfos.size());
        log.error("Error Data=" + count);
    }

    private ServiceResult<Object> validateIntobslea(ProgramIntobslea i) {
        ServiceResult<Object> result = new ServiceResult<Object>();
        if (StringUtil.isEmpty(i.getOldid())) {
            result.setSuccess(false);
            result.setMsg("Old Id Is Empty=>" + formatJson(i));
            return result;
        }
        if (StringUtil.isEmpty(i.getChild_oldid())) {
            result.setSuccess(false);
            result.setMsg("Old Child Id Is Empty=>" + formatJson(i));
            return result;
        }
        UserInfo child = userInfoMapper.getUserInfoByOldId(i.getChild_oldid());
        if (null == child) {
            result.setSuccess(false);
            result.setMsg("New Child Id Is Empty=>" + formatJson(i));
            return result;
        } else {
            i.setChildNewId(child.getAccountId());
            i.setGroupNewId(child.getGroupId());
        }
        // if (StringUtil.isEmpty(i.getCentre_oldid())) {
        // result.setSuccess(false);
        // result.setMsg("Old Center Id Is Empty=>" + formatJson(i));
        // return result;
        // }
        if (StringUtil.isEmpty(i.getRoom_oldid())) {
            result.setSuccess(false);
            result.setMsg("Old Room Id Is Empty=>" + formatJson(i));
            return result;
        }
        // String centerId = centersInfoMapper.getCenterIdByOldId(i.getCentre_oldid());
        // if (StringUtil.isEmpty(centerId)) {
        // result.setSuccess(false);
        // result.setMsg("New Center Id Is Empty=>" + formatJson(i));
        // return result;
        // } else {
        // i.setCenterNewId(centerId);
        // }
        String roomId = roomInfoMapper.getRoomIdByOldId(i.getRoom_oldid());
        if (StringUtil.isEmpty(roomId)) {
            result.setSuccess(false);
            result.setMsg("New Room Id Is Empty=>" + formatJson(i));
            return result;
        } else {
            i.setRoomNewId(roomId);
            i.setCenterNewId(roomInfoMapper.getCenterIdByRoomOldId(i.getRoom_oldid()));
        }
        // if (StringUtil.isEmpty(i.getCreate_account_oldid())) {
        // result.setSuccess(false);
        // result.setMsg("Old Create Id Is Empty=>" + formatJson(i));
        // return result;
        // }
        // String createNewAccountId = userInfoMapper.getAccountIdByUserOldId(i.getCentre_oldid());
        // if (StringUtil.isEmpty(createNewAccountId)) {
        // result.setSuccess(false);
        // result.setMsg("New Create Id Is Empty=>" + formatJson(i));
        // return result;
        // } else {
        // i.setCreateNewAccountId(createNewAccountId);
        // }
        // if (StringUtil.isNotEmpty(i.getUpdate_account_oldid())) {
        // String updateNewAccountId = userInfoMapper.getAccountIdByUserOldId(i.getUpdate_account_oldid());
        // if (StringUtil.isEmpty(createNewAccountId)) {
        // result.setSuccess(false);
        // result.setMsg("New Update Id Is Empty=>" + formatJson(i));
        // return result;
        // } else {
        // i.setUpdateNewAccountId(updateNewAccountId);
        // }
        // }
        result.setSuccess(true);
        return result;
    }

    private TaskType getTaskType(short type) {
        for (TaskType t : TaskType.values()) {
            if (t.getValue() == type) {
                return t;
            }
        }
        return null;
    }

    private void initImages(List<String> photos, String sourceId) {
        if (photos == null) {
            return;
        }
        for (String path : photos) {
            this.fileList.add(initAttachInfo(sourceId, path, FilePathUtil.PROGRAM_PATH, null));
        }
    }

}
