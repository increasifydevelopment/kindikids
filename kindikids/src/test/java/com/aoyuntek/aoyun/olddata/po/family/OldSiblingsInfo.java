package com.aoyuntek.aoyun.olddata.po.family;

import com.aoyuntek.aoyun.olddata.po.family.child.OldChildPo;

public class OldSiblingsInfo {
    private OldChildPo child;

    public OldChildPo getChild() {
        return child;
    }

    public void setChild(OldChildPo child) {
        this.child = child;
    }

}
