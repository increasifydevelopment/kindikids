package com.aoyuntek.aoyun.olddata.po.user;

public class OldStatusPo {
    private Short delete_flag;
    private Short status;

    public Short getDelete_flag() {
        return delete_flag;
    }

    public void setDelete_flag(Short delete_flag) {
        this.delete_flag = delete_flag;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

}
