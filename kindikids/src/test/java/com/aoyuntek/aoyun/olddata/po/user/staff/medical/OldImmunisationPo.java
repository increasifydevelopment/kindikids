package com.aoyuntek.aoyun.olddata.po.user.staff.medical;

public class OldImmunisationPo {

    private OldImmunisationChildPo pertussis;
    private OldImmunisationChildPo hepatitis_a;
    private OldImmunisationChildPo hepatitis_b;
    private OldImmunisationChildPo measles_mumps_rubella;
    private OldImmunisationChildPo varicella;
    private OldImmunisationChildPo influenza;

    public OldImmunisationChildPo getPertussis() {
        return pertussis;
    }

    public void setPertussis(OldImmunisationChildPo pertussis) {
        this.pertussis = pertussis;
    }

    public OldImmunisationChildPo getHepatitis_a() {
        return hepatitis_a;
    }

    public void setHepatitis_a(OldImmunisationChildPo hepatitis_a) {
        this.hepatitis_a = hepatitis_a;
    }

    public OldImmunisationChildPo getHepatitis_b() {
        return hepatitis_b;
    }

    public void setHepatitis_b(OldImmunisationChildPo hepatitis_b) {
        this.hepatitis_b = hepatitis_b;
    }

    public OldImmunisationChildPo getMeasles_mumps_rubella() {
        return measles_mumps_rubella;
    }

    public void setMeasles_mumps_rubella(OldImmunisationChildPo measles_mumps_rubella) {
        this.measles_mumps_rubella = measles_mumps_rubella;
    }

    public OldImmunisationChildPo getVaricella() {
        return varicella;
    }

    public void setVaricella(OldImmunisationChildPo varicella) {
        this.varicella = varicella;
    }

    public OldImmunisationChildPo getInfluenza() {
        return influenza;
    }

    public void setInfluenza(OldImmunisationChildPo influenza) {
        this.influenza = influenza;
    }

}
