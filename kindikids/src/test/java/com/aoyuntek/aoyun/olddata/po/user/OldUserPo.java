package com.aoyuntek.aoyun.olddata.po.user;

import java.util.List;

import com.aoyuntek.aoyun.olddata.po.user.staff.OldPreferredMethodPo;

public class OldUserPo {
    private String last_name;
    private String address;
    private String suburb;

    private String group_oldId;
    private String num_of_children;
    private String middle_name;
    private String centre_oldId;

    private OldPreferredMethodPo preferred_method_of_contact;
    private String date_of_birth;
    private String first_name;
    private String state;

    private String password;
    private String home_phone_number;
    private String email;
    private String ethnicity;
    private String postcode;

    private String user_image;

    private List<OldEmergencyContactPo> emergency_contact;
    private String gender;
    private String marital_status;
    private String staff_oldId;
    private String ages_of_children;
    private String mobile_number;
    private String room_oldId;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<OldEmergencyContactPo> getEmergency_contact() {
        return emergency_contact;
    }

    public void setEmergency_contact(List<OldEmergencyContactPo> emergency_contact) {
        this.emergency_contact = emergency_contact;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSuburb() {
        return suburb;
    }

    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }

    public String getGroup_oldId() {
        return group_oldId;
    }

    public void setGroup_oldId(String group_oldId) {
        this.group_oldId = group_oldId;
    }

    public String getNum_of_children() {
        return num_of_children;
    }

    public void setNum_of_children(String num_of_children) {
        this.num_of_children = num_of_children;
    }

    public String getMiddle_name() {
        return middle_name;
    }

    public void setMiddle_name(String middle_name) {
        this.middle_name = middle_name;
    }

    public String getCentre_oldId() {
        return centre_oldId;
    }

    public void setCentre_oldId(String centre_oldId) {
        this.centre_oldId = centre_oldId;
    }

    public OldPreferredMethodPo getPreferred_method_of_contact() {
        return preferred_method_of_contact;
    }

    public void setPreferred_method_of_contact(OldPreferredMethodPo preferred_method_of_contact) {
        this.preferred_method_of_contact = preferred_method_of_contact;
    }

    public String getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(String date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getHome_phone_number() {
        return home_phone_number;
    }

    public void setHome_phone_number(String home_phone_number) {
        this.home_phone_number = home_phone_number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMarital_status() {
        return marital_status;
    }

    public void setMarital_status(String marital_status) {
        this.marital_status = marital_status;
    }

    public String getStaff_oldId() {
        return staff_oldId;
    }

    public void setStaff_oldId(String staff_oldId) {
        this.staff_oldId = staff_oldId;
    }

    public String getAges_of_children() {
        return ages_of_children;
    }

    public void setAges_of_children(String ages_of_children) {
        this.ages_of_children = ages_of_children;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getRoom_oldId() {
        return room_oldId;
    }

    public void setRoom_oldId(String room_oldId) {
        this.room_oldId = room_oldId;
    }

}
