package com.aoyuntek.aoyun.olddata.service.register;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.entity.po.program.ProgramRegisterItemInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramRegisterTaskInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskInstanceInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskResponsibleLogInfo;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.TaskType;
import com.aoyuntek.aoyun.olddata.constants.HttpUrlConstants;
import com.aoyuntek.aoyun.olddata.po.registers.OldRegisterPo;
import com.aoyuntek.aoyun.service.old.IOldDataService;
import com.aoyuntek.aoyun.service.task.ITaskInstance;
import com.google.gson.reflect.TypeToken;
import com.theone.common.util.ServiceResult;
import com.theone.json.util.JsonUtil;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

public class NappyRegisterService extends BaseRegisterService {
    private static Logger log = Logger.getLogger(NappyRegisterService.class);
    @Autowired
    private IOldDataService oldDataService;
    @Autowired
    private UserInfoMapper userInfoMapper;
    @Autowired
    private CentersInfoMapper centersInfoMapper;
    @Autowired
    private ITaskInstance taskInstance;

    private List<ProgramRegisterTaskInfo> taskInfoList = new ArrayList<ProgramRegisterTaskInfo>();

    private List<ProgramRegisterItemInfo> taskItemList = new ArrayList<ProgramRegisterItemInfo>();

    private List<TaskResponsibleLogInfo> responsibleList = new ArrayList<TaskResponsibleLogInfo>();

    @Test
    public void importRegister() {
        getOrgsMap(centersInfoMapper);
        getUserMap(userInfoMapper);

        Map<String, List<OldRegisterPo>> mapNappy8 = getMapNappy(HttpUrlConstants.NAPPY_DATA_URL8);
        Map<String, List<OldRegisterPo>> mapNappy10 = getMapNappy(HttpUrlConstants.NAPPY_DATA_URL10);
        Map<String, List<OldRegisterPo>> mapNappy13 = getMapNappy(HttpUrlConstants.NAPPY_DATA_URL13);
        Map<String, List<OldRegisterPo>> mapNappy15 = getMapNappy(HttpUrlConstants.NAPPY_DATA_URL15);

        Map<String, List<OldRegisterPo>> map = new HashMap<String, List<OldRegisterPo>>();
        map.putAll(mapNappy8);
        map.putAll(mapNappy10);
        map.putAll(mapNappy13);
        map.putAll(mapNappy15);

        int count = 0;
        for (Map.Entry<String, List<OldRegisterPo>> entry : map.entrySet()) {
            count += entry.getValue().size();
            ServiceResult<Object> result = importNappyRegister(entry.getKey(), entry.getValue(), TaskType.NappyRegister, 0);
            if (!result.isSuccess()) {
                super.exceptionList.add("list error-->" + formatJson(entry.getValue()));
            }
        }
        oldDataService.importNappyRegister(this.taskInfoList, this.taskItemList, this.responsibleList);
        // 打印日志
        printLog(this.log);
        // nappyItem总个数
        log.info("oldNappy item count=" + count);
        // 插入未成功的nappyItem个数
        log.info("oldNappy item no import count=" + super.exceptionList.size());

    }

    private Map<String, List<OldRegisterPo>> getMapNappy(String nappyDataUrl) {
        String jsonNappy = getHttpJson(nappyDataUrl);
        List<OldRegisterPo> oldListNappy = new ArrayList<OldRegisterPo>();
        oldListNappy = JsonUtil.jsonToList(jsonNappy, new TypeToken<List<OldRegisterPo>>() {
        }.getType());
        Map<String, List<OldRegisterPo>> mapNappy = initRegisterList(oldListNappy, TaskType.NappyRegister);
        return mapNappy;
    }

    private ServiceResult<Object> importNappyRegister(String oldId, List<OldRegisterPo> nappyList, TaskType nappyregister, int index) {
        ServiceResult<Object> result = new ServiceResult<Object>();
        result.setSuccess(false);
        if (index == nappyList.size()) {
            return result;
        }
        OldRegisterPo item = nappyList.get(index);
        String id = getUUID();
        result = validateData(item, true);
        if (!result.isSuccess()) {
            index++;
            importNappyRegister(oldId, nappyList, nappyregister, index);
            return result;
        }

        String newCentreId = super.orgMap.get(item.getCentre_oldid());
        String newRoomId = super.orgMap.get(item.getRoom_oldid());
        String newAccountId = super.userMap.get(item.getCreate_account_oldid());
        String updateAccountId = super.userMap.get(item.getUpdate_account_oldid());
        ProgramRegisterTaskInfo taskInfo = new ProgramRegisterTaskInfo();
        taskInfo.setId(id);
        taskInfo.setCenterId(newCentreId);
        taskInfo.setRoomId(newRoomId);
        taskInfo.setType(nappyregister.getValue());
        taskInfo.setState(item.getState());

        Date day = getDay(item.getDay());
        taskInfo.setDay(day);
        taskInfo.setCreateAccountId(newAccountId);
        taskInfo.setUpdateAccountId(newAccountId);
        Date updateTime = null;
        if (StringUtil.isEmpty(item.getDay())) {
            updateTime = day;
        } else {
            updateTime = parseDate(item.getUpdate_time());
        }
        taskInfo.setUpdateTime(updateTime);
        taskInfo.setDeleteFlag(DeleteFlag.Default.getValue());
        taskInfo.setOldId(item.getOldid());
        this.taskInfoList.add(taskInfo);
        ServiceResult<TaskInstanceInfo> addTaskInstance = taskInstance.addTaskInstance(TaskType.NappyRegister, newCentreId, newRoomId, null, id,
                newAccountId, day);
        if (StringUtil.isNotEmpty(newAccountId)) {
            this.responsibleList.add(getResponsiblePerson(addTaskInstance.getReturnObj().getId(), newAccountId));
        } else {
            super.logList.add("can't find create accountId taskid=" + id);
        }

        importRegisterItem(id, nappyList);

        result.setSuccess(true);
        return result;
    }

    private Date getDay(String day) {
        String[] str = day.split(" ");
        if ("15:00".equals(str[1])) {
            day = str[0] + " 15:30";
        }
        return parseDate(day);
    }

    private void importRegisterItem(String id, List<OldRegisterPo> nappyList) {
        if (ListUtil.isEmpty(nappyList)) {
            this.exceptionList.add("nappyList error null");
            return;
        }
        ServiceResult<Object> result = new ServiceResult<Object>();
        result.setSuccess(false);
        for (OldRegisterPo oldRegisterPo : nappyList) {
            result = validateData(oldRegisterPo, false);
            if (!result.isSuccess()) {
                super.exceptionList.add(result.getMsg());
                continue;
            }
            ProgramRegisterItemInfo itemInfo = new ProgramRegisterItemInfo();
            itemInfo.setId(getUUID());
            itemInfo.setTaskId(id);
            itemInfo.setChildId(super.userMap.get(oldRegisterPo.getChild_oldid()));
            itemInfo.setServings(oldRegisterPo.getServings());
            // itemInfo.setNewsfeedId(oldRegisterPo.getNewsfeed_oldid());
            itemInfo.setCreateAccountId(super.userMap.get(oldRegisterPo.getCreate_account_oldid()));
            itemInfo.setUpdateTime(getDateByOldId(oldRegisterPo.getOldid()));
            itemInfo.setUpdateAccountId(super.userMap.get(oldRegisterPo.getUpdate_account_oldid()));
            itemInfo.setDeleteFlag(DeleteFlag.Default.getValue());
            this.taskItemList.add(itemInfo);
        }
    }

    public ServiceResult<Object> validateData(OldRegisterPo item, boolean baseValidate) {
        ServiceResult<Object> result = new ServiceResult<Object>();
        result.setSuccess(false);
        if (!super.orgMap.containsKey(item.getCentre_oldid())) {
            result.setMsg("error can't find centreId-->" + formatJson(item));
            return result;
        }
        if (!super.orgMap.containsKey(item.getRoom_oldid())) {
            result.setMsg("error can't find roomId-->" + formatJson(item));
            return result;
        }
        if (!super.userMap.containsKey(item.getCreate_account_oldid())) {
            super.logList.add("warning can't found createAccountId-->" + formatJson(item));
        }
        if (!super.userMap.containsKey(item.getChild_oldid()) && !baseValidate) {
            result.setMsg("error can't found child-->" + formatJson(item));
            return result;
        }
        result.setSuccess(true);
        return result;
    }
}
