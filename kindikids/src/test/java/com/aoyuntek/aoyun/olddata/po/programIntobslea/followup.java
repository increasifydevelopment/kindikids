package com.aoyuntek.aoyun.olddata.po.programIntobslea;

import java.util.List;

import com.aoyuntek.framework.model.BaseModel;

public class followup extends BaseModel {
    private static final long serialVersionUID = 1L;
    private String oldid;
    private String interest_oldid;
    private String observation_oldid;
    private String observation;
    private Short state;
    private Object newsfeed_oldid;
    private String create_account_oldid;
    private String update_account_oldid;
    private String update_time;
    private Boolean delete_flag;
    private List<String> eylf_click;

    private String taskId;
    private String createAccountId;
    private String updateAccountId;

    public String getObservation_oldid() {
        return observation_oldid;
    }

    public void setObservation_oldid(String observation_oldid) {
        this.observation_oldid = observation_oldid;
    }

    public List<String> getEylf_click() {
        return eylf_click;
    }

    public void setEylf_click(List<String> eylf_click) {
        this.eylf_click = eylf_click;
    }

    public String getOldid() {
        return oldid;
    }

    public void setOldid(String oldid) {
        this.oldid = oldid;
    }

    public String getInterest_oldid() {
        return interest_oldid;
    }

    public void setInterest_oldid(String interest_oldid) {
        this.interest_oldid = interest_oldid;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public Short getState() {
        return state;
    }

    public void setState(Short state) {
        this.state = state;
    }

    public Object getNewsfeed_oldid() {
        return newsfeed_oldid;
    }

    public void setNewsfeed_oldid(Object newsfeed_oldid) {
        this.newsfeed_oldid = newsfeed_oldid;
    }

    public String getCreate_account_oldid() {
        return create_account_oldid;
    }

    public void setCreate_account_oldid(String create_account_oldid) {
        this.create_account_oldid = create_account_oldid;
    }

    public String getUpdate_account_oldid() {
        return update_account_oldid;
    }

    public void setUpdate_account_oldid(String update_account_oldid) {
        this.update_account_oldid = update_account_oldid;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public Boolean getDelete_flag() {
        return delete_flag;
    }

    public void setDelete_flag(Boolean delete_flag) {
        this.delete_flag = delete_flag;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getCreateAccountId() {
        return createAccountId;
    }

    public void setCreateAccountId(String createAccountId) {
        this.createAccountId = createAccountId;
    }

    public String getUpdateAccountId() {
        return updateAccountId;
    }

    public void setUpdateAccountId(String updateAccountId) {
        this.updateAccountId = updateAccountId;
    }

}
