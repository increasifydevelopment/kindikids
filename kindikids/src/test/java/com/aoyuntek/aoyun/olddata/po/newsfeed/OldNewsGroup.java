package com.aoyuntek.aoyun.olddata.po.newsfeed;

import java.util.List;

import com.theone.json.util.JsonUtil;

public class OldNewsGroup {
    
    private List<String> list;
    private String newsGroup;
    
    public List<String> getList() {
        return list;
    }

    public void setList(List<String> list) {
        this.list = list;
    }

    public String getNewsGroup() {
        return newsGroup;
    }

    public void setNewsGroup(String newsGroup) {
        this.newsGroup = newsGroup;
    }

    public static void main(String[] args) {
        String json = "{\"newsfeed_group\": [[{\"group_name\": \"All parents in centre one\", \"news_id\": \"5576aa3ee0eefd1e84acc01c\", \"delete_flag\": false}, {\"group_name\": \"All parents in centre two\", \"news_id\": \"5576aa3ee0eefd1e84acc01c\", \"delete_flag\": false}, {\"group_name\": \"All parents in centre three\", \"news_id\": \"5576aa3ee0eefd1e84acc01c\", \"delete_flag\": false}], {\"group_name\": \"Directors\", \"news_id\": \"5576aa3ee0eefd1e84acc01c\", \"delete_flag\": false}]}";
        OldNewsGroup group = (OldNewsGroup) JsonUtil.jsonToBean(json, OldNewsGroup.class);
        System.out.println(group.getNewsGroup());
    }


}
