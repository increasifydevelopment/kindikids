package com.aoyuntek.aoyun.olddata.po.user.staff.credentials;

public class OldQualificationPo {
    private String date_completed;
    private String upload_certificate;
    private String qualification_obtained;
    public String getDate_completed() {
        return date_completed;
    }
    public void setDate_completed(String date_completed) {
        this.date_completed = date_completed;
    }
    public String getUpload_certificate() {
        return upload_certificate;
    }
    public void setUpload_certificate(String upload_certificate) {
        this.upload_certificate = upload_certificate;
    }
    public String getQualification_obtained() {
        return qualification_obtained;
    }
    public void setQualification_obtained(String qualification_obtained) {
        this.qualification_obtained = qualification_obtained;
    }
    
    
}
