package com.aoyuntek.aoyun.test.vo;

import com.aoyuntek.framework.model.BaseModel;

public class YelfInfoVo extends BaseModel {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String _id;

    private String parent_id;

    private Integer group;

    private String title;

    public YelfInfoVo() {
    }

    public YelfInfoVo(String _id, String parent_id, Integer group, String title) {
        super();
        this._id = _id;
        this.parent_id = parent_id;
        this.group = group;
        this.title = title;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public Integer getGroup() {
        return group;
    }

    public void setGroup(Integer group) {
        this.group = group;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
