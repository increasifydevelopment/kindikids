package com.aoyuntek.aoyun.test;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

@RunWith(JUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:conf/spring/spring-base.xml" })
public class BaseTest extends AbstractJUnit4SpringContextTests{

}