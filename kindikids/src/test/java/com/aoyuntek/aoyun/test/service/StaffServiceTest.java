package com.aoyuntek.aoyun.test.service;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.aoyuntek.aoyun.condtion.StaffCondition;
import com.aoyuntek.aoyun.dao.UserStaffMapper;
import com.aoyuntek.aoyun.entity.vo.StaffInfoVo;
import com.aoyuntek.aoyun.entity.vo.StaffListVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.service.IStaffService;
import com.aoyuntek.aoyun.test.BaseTest;
import com.aoyuntek.framework.model.Pager;
import com.theone.common.util.ServiceResult;
import com.theone.json.util.JsonUtil;
import com.theone.web.util.ResponseUtils;
import com.theone.web.util.ResponseVo;

public class StaffServiceTest extends BaseTest {

	@Autowired
	private IStaffService service;
	@Autowired
	private UserStaffMapper mapper;

	@Test
	public void test01() {
		StaffCondition condition = new StaffCondition();
		condition.setPageIndex(1);
		ServiceResult<Pager<StaffListVo, StaffCondition>> result = service.getPagerStaffs(condition);
		ResponseVo res = ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
		System.out.println(JsonUtil.objectToJson(res));
	}

	@Test
	public void test02() {
		ServiceResult<StaffInfoVo> result = service.saveGetStaffInfo("45f675b6-530a-11e6-9746-00e0703507b4");
		// ResponseVo res = ResponseUtils.sendMsg(result.getCode(),
		// result.getReturnObj());
		// System.out.println(JsonUtil.objectToJson(res));

		StaffInfoVo staffInfo = result.getReturnObj();
		staffInfo.getMedicalInfo().setMedication("hahahah");
		// ServiceResult<Object> result2 = service.updateStaffInfo(staffInfo,
		// "e9f0ce79-6e7b-4cd9-8be2-863d9b7a0758");
		// System.out.println(result2.getMsg());
	}

	@Test
	public void test03() {
		int result = mapper.selectAccount("abc@qq.com", "e9f0ce79-6e7b-4cd9-8be2-863d9b7a0758");
		System.out.println(result);
	}

	@Test
	public void test04() {
		service.updateAchive("e9f0ce79-6e7b-4cd9-8be2-863d9b7a0758", 1, new UserInfoVo());
	}

}
