package com.aoyuntek.aoyun.test.service;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.vo.SignatureVo;
import com.aoyuntek.aoyun.test.BaseTest;
import com.theone.json.util.JsonUtil;
import com.theone.string.util.StringUtil;

public class Signature extends BaseTest {
    @Autowired
    private UserInfoMapper userInfoMapper;

    @Test
    public void test() {
        String tempFilePath = "D:\\javaSource\\Kindikids\\kindikids-parent\\kindikids\\src\\main\\webapp\\templet\\dat\\myfile.dat";

        BufferedReader reader;
        int count = 0;
        try {
            reader = new BufferedReader(new FileReader(tempFilePath));
            String line = "";
            while ((line = reader.readLine()) != null) {
                SignatureVo signatureVo = (SignatureVo) JsonUtil.jsonToBean(line, SignatureVo.class);
                // getImg(signatureVo.getParent_signature(), signatureVo.get_id());
                String imgFile = "D:\\Desktop\\importImg\\" + signatureVo.get_id() + ".png";
                userInfoMapper.saveSignature(UUID.randomUUID().toString(), signatureVo.get_id(), getImageStr(imgFile));
                System.err.println(count++);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void getImg(String str, String name) {
        if (StringUtil.isNotEmpty(str) && str.contains(";base64,")) {
            str = str.split(";base64,")[1];
        }
        BASE64Decoder decoder = new BASE64Decoder();
        try {
            // Base64解码
            byte[] b = decoder.decodeBuffer(str);
            for (int i = 0; i < b.length; ++i) {
                if (b[i] < 0) {// 调整异常数据
                    b[i] += 256;
                }
            }
            // 生成jpeg图片
            String imgFilePath = "D://Desktop//img//" + name + ".png";// 新生成的图片
            OutputStream out = new FileOutputStream(imgFilePath);
            out.write(b);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getImageStr(String imgFile) {
        InputStream inputStream = null;
        byte[] data = null;
        try {
            inputStream = new FileInputStream(imgFile);
            data = new byte[inputStream.available()];
            inputStream.read(data);
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // 加密
        BASE64Encoder encoder = new BASE64Encoder();
        return "data:image/png;base64," + encoder.encode(data);
    }
}
