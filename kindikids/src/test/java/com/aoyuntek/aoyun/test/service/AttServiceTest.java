package com.aoyuntek.aoyun.test.service;

import java.util.UUID;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;

import com.aoyuntek.aoyun.condtion.AttendanceCondtion;
import com.aoyuntek.aoyun.condtion.StaffCondition;
import com.aoyuntek.aoyun.dao.SigninInfoMapper;
import com.aoyuntek.aoyun.entity.po.SigninInfo;
import com.aoyuntek.aoyun.entity.vo.StaffListVo;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.SigninState;
import com.aoyuntek.aoyun.service.IFamilyService;
import com.aoyuntek.aoyun.service.attendance.IAttendanceService;
import com.aoyuntek.aoyun.test.BaseTest;
import com.aoyuntek.framework.model.Pager;
import com.theone.common.util.ServiceResult;
import com.theone.date.util.DateUtil;
import com.theone.json.util.JsonUtil;
import com.theone.web.util.ResponseUtils;
import com.theone.web.util.ResponseVo;

public class AttServiceTest extends BaseTest {

	@Autowired
	private IFamilyService familyService;
	;
	@Autowired
	private SigninInfoMapper signinInfoMapper;
   
	@Test
	public void test01() {
		familyService.getAgeStage();
	}
	
	@Test
	public void addSigninInfo() {
		SigninInfo _SigninInfo=new SigninInfo();
		_SigninInfo.setAccountId("4f92f90d-3cc0-4d75-8771-9da2c94678f0");
		_SigninInfo.setCentreId("aa8419ef-f791-4ced-afdd-149a6ff02f22");
		_SigninInfo.setDeleteFlag(DeleteFlag.Default.getValue());
		_SigninInfo.setId(UUID.randomUUID().toString());
		_SigninInfo.setRoomId("c255d38e-aab7-445c-9bc3-7c0471dba2f0");
		_SigninInfo.setSignDate(DateUtil.parse("2016-08-23", "yyyy-MM-dd"));
		_SigninInfo.setState(SigninState.Signin.getValue());
		
		SigninInfo _SigninInfo1=new SigninInfo();
		_SigninInfo1.setAccountId("9bf3f2ee-5637-4366-bcc1-a2886045d7ab");
		_SigninInfo1.setCentreId("aa8419ef-f791-4ced-afdd-149a6ff02f22");
		_SigninInfo1.setDeleteFlag(DeleteFlag.Default.getValue());
		_SigninInfo1.setId(UUID.randomUUID().toString());
		_SigninInfo1.setRoomId("c255d38e-aab7-445c-9bc3-7c0471dba2f0");
		_SigninInfo1.setSignDate(DateUtil.parse("2016-08-23", "yyyy-MM-dd"));
		_SigninInfo1.setState(SigninState.NoSignin.getValue());
		
		signinInfoMapper.insert(_SigninInfo);
		signinInfoMapper.insert(_SigninInfo1);
	}

}
