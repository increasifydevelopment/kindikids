package com.aoyuntek.aoyun.test.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.UUID;

import org.apache.commons.collections.map.HashedMap;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.aoyuntek.aoyun.dao.YelfInfoMapper;
import com.aoyuntek.aoyun.dao.program.EylfCheckInfoMapper;
import com.aoyuntek.aoyun.entity.po.YelfInfo;
import com.aoyuntek.aoyun.entity.po.program.EylfCheckInfo;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.test.BaseTest;
import com.aoyuntek.aoyun.test.vo.YelfCheckVo;
import com.aoyuntek.aoyun.test.vo.YelfInfoVo;
import com.google.gson.reflect.TypeToken;
import com.theone.json.util.JsonUtil;

public class YelfServiceTest extends BaseTest {

    @Autowired
    YelfInfoMapper yelfInfoMapper;

    @Autowired
    EylfCheckInfoMapper eylfCheckInfoMapper;

    private Map<String, String> map = new HashMap<String, String>();

    public YelfServiceTest() {
        map.put("54d9aeac87431582091adaf7", "OUTCOME 1");
        map.put("54d9b0e687431582091adaf8", "OUTCOME 2");
        map.put("54d9b5ea87431582091adaf9", "OUTCOME 3");
        map.put("54d9b63987431582091adafa", "OUTCOME 4");
        map.put("54d9b66787431582091adafb", "OUTCOME 5");
    }

    @Test
    public void importData() {
        try {
            String yelfPath = "E:/eylf.json";
            String yelfCheckPath = "E:/EYLF_Check_List.json";

            List<YelfInfoVo> yelfList = getYelfList(yelfPath);
            List<YelfCheckVo> yelfCheckList = getYelfCheckList(yelfCheckPath);
            System.err.println(yelfList.size());
            System.err.println(yelfCheckList.size());

            for (YelfInfoVo yelf : yelfList) {
                String id = dealId(yelf.get_id());
                if (map.containsKey(id)) {
                    YelfInfo dbYelf = new YelfInfo();
                    dbYelf.setId(id);
                    dbYelf.setVersion(map.get(id));
                    String[] version = getVersion(yelf.getTitle());
                    dbYelf.setContent(version[1]);
                    dbYelf.setDeleteFlag(DeleteFlag.Default.getValue());
                    yelfInfoMapper.insert(dbYelf);
                    continue;
                }

                EylfCheckInfo dbCheck = new EylfCheckInfo();
                dbCheck.setId(id);
                dbCheck.setAgeScope(yelf.getGroup());
                dbCheck.setDeleteFlag(DeleteFlag.Default.getValue());
                dbCheck.setParentNode(map.get(yelf.getParent_id()));
                dbCheck.setContent(yelf.getTitle());
                eylfCheckInfoMapper.insert(dbCheck);
            }

            for (YelfCheckVo yelfCheck : yelfCheckList) {
                YelfInfo dbYelf = new YelfInfo();
                dbYelf.setVersion(yelfCheck.getLabel());
                dbYelf.setContent(yelfCheck.getText());
                dbYelf.setId(dealId(yelfCheck.get_id()));
                dbYelf.setParentNode(map.get(yelfCheck.getEylf_id()));
                dbYelf.setDeleteFlag(DeleteFlag.Default.getValue());
                yelfInfoMapper.insert(dbYelf);
            }
            System.err.println(yelfList.size());
            System.err.println(yelfCheckList.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String dealId(String id) {
        return id.split("ObjectId\\(\"")[1].split("\"\\)")[0];
    }

    public String[] getVersion(String title) {
        return title.split("##");
    }

    private List<YelfInfoVo> getYelfList(String path) {
        File file = new File(path);
        Scanner scanner = null;
        StringBuilder buffer = new StringBuilder();
        try {
            scanner = new Scanner(file, "utf-8");
            while (scanner.hasNextLine()) {
                buffer.append(scanner.nextLine());
            }
        } catch (FileNotFoundException e) {

        } finally {
            if (scanner != null) {
                scanner.close();
            }
        }
        System.err.println(buffer.toString());
        List<YelfInfoVo> list = JsonUtil.jsonToList(buffer.toString(), new TypeToken<List<YelfInfoVo>>() {
        }.getType());
        return list;
    }

    private List<YelfCheckVo> getYelfCheckList(String path) {
        File file = new File(path);
        Scanner scanner = null;
        StringBuilder buffer = new StringBuilder();
        try {
            scanner = new Scanner(file, "utf-8");
            while (scanner.hasNextLine()) {
                buffer.append(scanner.nextLine());
            }
        } catch (FileNotFoundException e) {

        } finally {
            if (scanner != null) {
                scanner.close();
            }
        }
        System.err.println(buffer.toString());
        List<YelfCheckVo> list = JsonUtil.jsonToList(buffer.toString(), new TypeToken<List<YelfCheckVo>>() {
        }.getType());
        return list;
    }
}
