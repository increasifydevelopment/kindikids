/**
 * <b>项目名：</b>kindkids<br/>
 * <b>包   名：</b>com.aoyuntek.aoyun.test.service<br/>
 * <b>文件名：</b>TaskServiceTest.java<br/>
 * <b>版本信息：</b>1.0<br/>
 * <b>日期：</b>2016年5月11日-下午6:08:24<br/>
 * 
 *//*
package com.aoyuntek.aoyun.test.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.aoyuntek.aoyun.entity.po.AccountModel;
import com.aoyuntek.aoyun.entity.po.LevelUnitModel;
import com.aoyuntek.aoyun.entity.po.ProjectModel;
import com.aoyuntek.aoyun.entity.po.TaskGroupModel;
import com.aoyuntek.aoyun.entity.po.TaskItemModel;
import com.aoyuntek.aoyun.entity.vo.LevelFinishesMatrix;
import com.aoyuntek.aoyun.entity.vo.TaskItemModelVo;
import com.aoyuntek.aoyun.entity.vo.TaskModelVo;
import com.aoyuntek.aoyun.entity.vo.UserInfo;
import com.aoyuntek.aoyun.service.IProjectService;
import com.aoyuntek.aoyun.service.IProjectTaskGroupService;
import com.aoyuntek.aoyun.service.ITaskGroupService;
import com.aoyuntek.aoyun.service.ITaskItemService;
import com.aoyuntek.aoyun.service.ITaskProgressService;
import com.aoyuntek.aoyun.test.BaseTest;
import com.aoyuntek.framework.model.ServiceResult;
import com.theone.json.util.JsonUtil;

*//**
 * <b>类   名：</b>TaskServiceTest<br/>
 * <b>类描述：</b>描述这个类的功能<br/>
 * <b>创建人：</b>weiyang<br/>
 * <b>创建时间：</b>2016年5月11日 下午6:08:24<br/>
 * <b>修改人：</b>weiyang<br/>
 * <b>修改时间：</b>2016年5月11日 下午6:08:24<br/>
 * <b>修改备注：</b><br/>
 *
 * @version 1.0<br/>
 * 
 *//*
public class TaskServiceTest extends BaseTest{
	@Autowired
	private ITaskGroupService taskGroupService;
	@Autowired
	private ITaskProgressService taskProgressService;
	@Autowired
	private IProjectService projectService;
	@Autowired
	private ITaskItemService taskItemService;
	@Autowired
	private IProjectTaskGroupService projectTaskGroupService;
	
	@Test
	public void testGetTask(){
		TaskItemModelVo TaskItemModelVo = new TaskItemModelVo();
		TaskItemModelVo.setName("Atlanta Falcons");
		TaskItemModelVo.setTaskGroupId(688);
		TaskItemModelVo.setId(1522);
		AccountModel accountModel = new AccountModel();
		accountModel.setId(1);
		ProjectModel projectModel = new ProjectModel();
		projectModel.setId(41);
		UserInfo userInfo = new UserInfo();
		userInfo.setAccount(accountModel);
		userInfo.setProject(projectModel);
		taskItemService.insertOrUpdate(TaskItemModelVo, userInfo, 688, 309);
//		System.out.println(JsonUtil.objectToJson(projectService.getProjectFinishesMatrix(41)));
//		ServiceResult<Map<String, Object>> result = taskProgressService.getMatrixUnit(309);
//		System.out.println(JsonUtil.objectToJson(result));
//		Map<String, Object> resu = result.getReturnObj();
//		List<TaskModelVo> categoryList = (List<TaskModelVo>) resu.get("categoryList");
//		List<TaskItemModelVo> taskItemModelList = new ArrayList<TaskItemModelVo>();
//		for (TaskModelVo ele : categoryList) {
//			taskItemModelList.addAll(ele.getTaskItemModelList());
//		}
//		List<LevelUnitModel> unitList = (List<LevelUnitModel>) resu.get("unitList");
//		List<LevelFinishesMatrix> levelFinishesMatrixs = new ArrayList<LevelFinishesMatrix>();
//		for (LevelUnitModel unit : unitList) {
//			for (TaskItemModelVo item : taskItemModelList) {
//				LevelFinishesMatrix element = new LevelFinishesMatrix();
//				element.setCompletionDate(new Date());
//				element.setProgress(75D);
//				element.setTaskItemId(item.getId());
//				element.setUnitId(unit.getId());
//				levelFinishesMatrixs.add(element);
//			}
//		}
//		taskProgressService.saveMatrixUnit(309, levelFinishesMatrixs, 1);
	}
}
*/