package com.aoyuntek.aoyun.test.vo;

import com.aoyuntek.framework.model.BaseModel;

public class YelfCheckVo extends BaseModel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String _id;

    private String eylf_id;

    private String label;

    private String text;

    public YelfCheckVo(String _id, String eylf_id, String label, String text) {
        super();
        this._id = _id;
        this.eylf_id = eylf_id;
        this.label = label;
        this.text = text;
    }

    public YelfCheckVo() {
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getEylf_id() {
        return eylf_id;
    }

    public void setEylf_id(String eylf_id) {
        this.eylf_id = eylf_id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
