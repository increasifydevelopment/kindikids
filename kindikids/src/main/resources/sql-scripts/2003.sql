create view v_news_user as  SELECT receive_account_id as account_id,news_id from tbl_news_receive where delete_flag=0
UNION ALL
SELECT account_id,news_id  from tbl_news_accounts  where delete_flag=0;


CREATE INDEX Idx_news_accounts_delete_flag ON tbl_news_accounts (delete_flag); 

CREATE INDEX Idx_news_receive_delete_flag ON tbl_news_receive (delete_flag); 



	CREATE VIEW v_news_visibility2 as  SELECT
			`n`.`id` AS `id`,
	`n`.`create_account_id` AS `create_account_id`,
	`n`.`centers_id` AS `centers_id`,
	`n`.`status` AS `status`,
	`n`.`delete_flag` AS `delete_flag`,
	`n`.`approve_time` AS `approve_time`,
	`n`.`update_time` AS `update_time`,
	`n`.`news_type` AS `news_type`,
	`n`.`score` AS `score`,
	`n`.`create_time` AS `create_time`,
	`n`.`room_id` AS `room_id`,
	`r`.`account_id` AS `account_id`
	FROM
		tbl_news n LEFT JOIN  v_news_user r on n.id=r.news_id;
		
		
CREATE INDEX Idx_news_status ON tbl_news (`status`);
CREATE VIEW v_news_tag2 AS SELECT
	`n`.`id` AS `id`,
	`n`.`create_account_id` AS `create_account_id`,
	`n`.`centers_id` AS `centers_id`,
	`n`.`status` AS `status`,
	`n`.`delete_flag` AS `delete_flag`,
	`n`.`approve_time` AS `approve_time`,
	`n`.`update_time` AS `update_time`,
	`n`.`news_type` AS `news_type`,
	`n`.`score` AS `score`,
	`n`.`create_time` AS `create_time`,
	`n`.`room_id` AS `room_id`,
	`a`.`account_id` AS `account_id`
FROM
	`tbl_news` `n`
LEFT JOIN `tbl_news_accounts` `a` ON `n`.`id` = `a`.`news_id`
AND `a`.`delete_flag` = 0;


DELETE from tbl_news_accounts where delete_flag=1;


DELETE  FROM tbl_news_receive where delete_flag=1;
		