alter table tbl_nqs drop primary key;

ALTER TABLE tbl_nqs add COLUMN tag SMALLINT(1) DEFAULT 1;
ALTER TABLE tbl_nqs_relation add COLUMN tag SMALLINT(1) DEFAULT 1;

update tbl_nqs set tag=1;
update tbl_nqs_relation set tag=1;

CREATE INDEX Idx_nqs_r_tag ON tbl_nqs_relation (tag);

CREATE INDEX Idx_nqs_tag ON tbl_nqs (tag);
CREATE INDEX Idx_nqs_version ON tbl_nqs (version);


INSERT INTO `tbl_nqs` VALUES ('1.1', 'The educational program enhances each child\'s learning and development', 'QA1', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('1.1.1', 'Curriculum decision-making contributes to each child\'s learning and development outcomes in relation to their identity, connection with community, wellbeing, confidence as learners and effectiveness as communicators', '1.1', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('1.1.2', 'Each child\'s current knowledge, strengths, ideas, culture, abilities and interests are the foundation of the program', '1.1', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('1.1.3', 'All aspects of the program, including routines, are organised in ways that maximise opportunities for each child\'s learning', '1.1', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('1.2', 'Educators facilitate and extend each child\'s learning and development', 'QA1', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('1.2.1', 'Educators are deliberate, purposeful, and thoughtful in their decisions and actions', '1.2', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('1.2.2', 'Educators respond to children\'s ideas and play and extend children\'s learning through open-ended questions, interactions and feedback', '1.2', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('1.2.3', 'Each child\'s agency is promoted, enabling them to make choices and decisions that influence events and their world', '1.2', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('1.3', 'Educators and co-ordinators take a planned and reflective approach to implementing the program for each child', 'QA1', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('1.3.1', 'Each child\'s learning and development is assessed or evaluated as part of an ongoing cycle of observation, analysing learning, documentation, planning, implementation and reflection', '1.3', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('1.3.2', 'Critical reflection on children\'s learning and development, both as individuals and in groups, drives program planning and implementation', '1.3', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('1.3.3', 'Families are informed about the program and their child\'s progress', '1.3', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('2.1', 'Each child\'s health and physical activity is supported and promoted', 'QA2', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('2.1.1', 'Each child\'s wellbeing and comfort is provided for, including appropriate opportunities to meet each child\'s need for sleep, rest and relaxation', '2.1', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('2.1.2', 'Effective illness and injury management and hygiene practices are promoted and implemented', '2.1', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('2.1.3', 'Healthy eating and physical activity are promoted and appropriate for each child', '2.1', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('2.2', 'Each child is protected', 'QA2', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('2.2.1', 'At all times, reasonable precautions and adequate supervision ensure children are protected from harm and hazard', '2.2', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('2.2.2', 'Plans to effectively manage incidents and emergencies are developed in consultation with relevant authorities, practised and implemented', '2.2', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('2.2.3', 'Management, educators and staff are aware of their roles and responsibilities to identify and respond to every child at risk of abuse or neglect', '2.2', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('3.1', 'The design of the facilities is appropriate for the operation of a service', 'QA3', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('3.1.1', 'Outdoor and indoor spaces, buildings, fixtures and fittings are suitable for their purpose, including supporting the access of every child', '3.1', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('3.1.2', 'Premises, furniture and equipment are safe, clean and well maintained', '3.1', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('3.2', 'The service environment is inclusive, promotes competence and supports exploration and play-based learning', 'QA3', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('3.2.1', 'Outdoor and indoor spaces are organised and adapted to support every child\'s participation and to engage every child in quality experiences in both built and natural environments', '3.2', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('3.2.2', 'Resources, materials and equipment allow for multiple uses, are sufficient in number, and enable every child to engage in play-based learning', '3.2', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('3.2.3', 'The service cares for the environment and supports children to become environmentally responsible', '3.2', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('4.1', 'Staffing arrangements enhance children\'s learning and development', 'QA4', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('4.1.1', 'The organisation of educators across the service supports children\'s learning and development', '4.1', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('4.1.2', 'Every effort is made for children to experience continuity of educators at the service', '4.1', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('4.2', 'Management, educators and staff are collaborative, respectful and ethical', 'QA4', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('4.2.1', 'Management, educators and staff work with mutual respect and collaboratively, and challenge and learn from each other, recognising each other\'s strengths and skills', '4.2', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('4.2.2', 'Professional standards guide practice, interactions and relationships', '4.2', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('5.1', 'Respectful and equitable relationships are maintained with each child', 'QA5', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('5.1.1', 'Responsive and meaningful interactions build trusting relationships which engage and support each child to feel secure, confident and included', '5.1', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('5.1.2', 'The dignity and rights of every child are maintained', '5.1', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('5.2', 'Each child is supported to build and maintain sensitive and responsive relationships', 'QA5', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('5.2.1', 'Children are supported to collaborate, learn from and help each other', '5.2', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('5.2.2', 'Each child is supported to regulate their own behaviour, respond appropriately to the behaviour of others and communicate effectively to resolve conflicts', '5.2', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('6.1', 'Respectful relationships with families are developed and maintained and families are supported in their parenting role', 'QA6', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('6.1.1', 'Families are supported from enrolment to be involved in the service and contribute to service decisions', '6.1', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('6.1.2', 'The expertise, culture, values and beliefs of families are respected and families share in decision-making about their child\'s learning and wellbeing', '6.1', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('6.1.3', 'Current information is available to families about the service and relevant community services and resources to support parenting and family wellbeing', '6.1', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('6.2', 'Collaborative partnerships enhance children\'s inclusion, learning and wellbeing', 'QA6', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('6.2.1', 'Continuity of learning and transitions for each child are supported by sharing information and clarifying responsibilities', '6.2', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('6.2.2', 'Effective partnerships support children\'s access, inclusion and participation in the program', '6.2', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('6.2.3', 'The service builds relationships and engages with its community', '6.2', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('7.1', 'Governance supports the operation of a quality service', 'QA7', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('7.1.1', 'A statement of philosophy guides all aspects of the service\'s operations', '7.1', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('7.1.2', 'Systems are in place to manage risk and enable the effective management and operation of a quality service', '7.1', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('7.1.3', 'Roles and responsibilities are clearly defined, and understood, and support effective decision-making and operation of the service', '7.1', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('7.2', 'Effective leadership builds and promotes a positive organisational culture and professional learning community', 'QA7', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('7.2.1', 'There is an effective self-assessment and quality improvement process in place', '7.2', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('7.2.2', 'The educational leader is supported and leads the development and implementation of the educational program and assessment and planning cycle', '7.2', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('7.2.3', 'Educators, co-ordinators and staff members\' performance is regularly evaluated and individual plans are in place to support learning and development', '7.2', null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('QA1', 'Educational program and practice', null, null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('QA2', 'Children\'s health and safety', null, null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('QA3', 'Physical environment', null, null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('QA4', 'Staffing arrangements', null, null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('QA5', 'Relationships with children', null, null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('QA6', 'Collaborative partnerships with families and communities', null, null, '0', '2');
INSERT INTO `tbl_nqs` VALUES ('QA7', 'Governance and Leadership', null, null, '0', '2');



ALTER VIEW v_nqs_query as SELECT
	`a`.`version` AS `root_node`,
	`a`.`content` AS `content`,
	`c`.`version` AS `version`,
   a.tag as tag
FROM
	`tbl_nqs` `a`
LEFT JOIN `tbl_nqs` `b` ON `a`.`version` = `b`.`parent_node` and a.tag=b.tag
LEFT  JOIN `tbl_nqs` `c` ON `b`.`version` = `c`.`parent_node` and b.tag=c.tag
WHERE
	isnull(`a`.`parent_node`);
	
	
	
ALTER view v_nqs_relation as SELECT
	`n`.`id` AS `id`,
	`n`.`obj_id` AS `obj_id`,
	`n`.`nqs_version` AS `nqs_version`,
	`n`.`delete_flag` AS `delete_flag`,
	`v`.`root_node` AS `root_node`,
	`v`.`content` AS `content`
FROM
	`v_nqs_query` `v`
JOIN `tbl_nqs_relation` `n` ON `v`.`version` = `n`.`nqs_version` and v.tag=n.tag
WHERE
	`n`.`delete_flag` = 0
ORDER BY
	`n`.`nqs_version`;