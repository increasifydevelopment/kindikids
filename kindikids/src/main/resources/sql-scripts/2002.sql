alter view v_news_query as  (
	SELECT
		`n`.`id` AS `n_id`,
		`n`.`centers_id` AS `n_centers_id`,
		`cc`.`name` AS `centersName`,
		`rr`.`name` AS `roomName`,
		`n`.`room_id` AS `n_room_id`,
		`n`.`create_account_id` AS `n_create_account_id`,
		`n`.`approve_account_id` AS `n_approve_account_id`,
		concat(

			IF (
				(
					ifnull(`uu`.`first_name`, '') = ''
				),
				'',
				concat(`uu`.`first_name`, ' ')
			),

		IF (
			(
				ifnull(`uu`.`middle_name`, '') = ''
			),
			'',
			concat(`uu`.`middle_name`, ' ')
		),

	IF (
		(
			ifnull(`uu`.`last_name`, '') = ''
		),
		'',
		concat(`uu`.`last_name`, ' ')
	)
		) AS `n_create_user_name`,
		concat(

			IF (
				(
					ifnull(`u`.`first_name`, '') = ''
				),
				'',
				concat(`u`.`first_name`, ' ')
			),

		IF (
			(
				ifnull(`u`.`middle_name`, '') = ''
			),
			'',
			concat(`u`.`middle_name`, ' ')
		),

	IF (
		(
			ifnull(`u`.`last_name`, '') = ''
		),
		'',
		concat(`u`.`last_name`, ' ')
	)
		) AS `u_user_name`,
		concat(

			IF (
				(
					ifnull(`uuuuu`.`first_name`, '') = ''
				),
				'',
				concat(`uuuuu`.`first_name`, ' ')
			),

		IF (
			(
				ifnull(`uuuuu`.`middle_name`, '') = ''
			),
			'',
			concat(`uuuuu`.`middle_name`, ' ')
		),

	IF (
		(
			ifnull(`uuuuu`.`last_name`, '') = ''
		),
		'',
		concat(`uuuuu`.`last_name`, ' ')
	)
		) AS `n_update_user_name`,
		concat(

			IF (
				(
					ifnull(`uuuu`.`first_name`, '') = ''
				),
				'',
				concat(`uuuu`.`first_name`, ' ')
			),

		IF (
			(
				ifnull(`uuuu`.`middle_name`, '') = ''
			),
			'',
			concat(`uuuu`.`middle_name`, ' ')
		),

	IF (
		(
			ifnull(`uuuu`.`last_name`, '') = ''
		),
		'',
		concat(`uuuu`.`last_name`, ' ')
	)
		) AS `n_approve_user_name`,
		`uu`.`avatar` AS `n_avatar`,
		`uu`.`person_color` AS `n_person_color`,
		`n`.`update_account_id` AS `n_update_account_id`,
		`n`.`news_tag` AS `n_news_tag`,
		`n`.`approve_time` AS `n_approve_time`,
		`n`.`status` AS `n_status`,
		`n`.`score` AS `n_score`,
		`n`.`create_time` AS `n_create_time`,
		`n`.`update_time` AS `n_update_time`,
		`n`.`news_type` AS `n_news_type`,
		`n`.`img_id` AS `n_img_id`,
		`n`.`delete_flag` AS `n_delete_flag`,
		`c`.`id` AS `c_id`,
		`c`.`content` AS `c_content`,
		`u`.`id` AS `u_id`,
		`u`.`first_name` AS `u_first_name`,
		`u`.`middle_name` AS `u_middle_name`,
		`u`.`last_name` AS `u_last_name`,
		`u`.`email` AS `u_email`,
		`u`.`avatar` AS `u_avatar`,
		`uo`.`id` AS `uo_id`,
		`uo`.`first_name` AS `uo_first_name`,
		`uo`.`middle_name` AS `uo_middle_name`,
		`uo`.`last_name` AS `uo_last_name`,
		`uo`.`avatar` AS `uo_avatar`,
		`uo`.`person_color` AS `uo_person_color`,
		`ag`.`id` AS `g_id`,
		`ag`.`group_name` AS `g_group_name`,
		`ag`.`create_time` AS `g_group_create_time`,
		`ng`.`news_id` AS `g_news_id`,
		`ng`.`delete_flag` AS `g_delete_flag`,
		`nq`.`version` AS `nq_version`,
		`nq`.`content` AS `nq_content`,
     nq.tag as nq_tag,
		`at`.`id` AS `f_id`,
		`at`.`source_id` AS `f_source_id`,
		`at`.`attach_name` AS `f_attach_name`,
		`at`.`attach_id` AS `f_attach_id`,
		`at`.`visit_url` AS `f_visit_url`,
		`at`.`attach_type` AS `f_attach_type`,
		`at`.`key_name` AS `f_key_name`,
		`nc`.`id` AS `nc_id`,
		`nc`.`content` AS `nc_content`,
		`nc`.`comment_account_id` AS `nc_comment_account_id`,
		concat(

			IF (
				(
					ifnull(`unc`.`first_name`, '') = ''
				),
				'',
				concat(`unc`.`first_name`, ' ')
			),

		IF (
			(
				ifnull(`unc`.`middle_name`, '') = ''
			),
			'',
			concat(`unc`.`middle_name`, ' ')
		),

	IF (
		(
			ifnull(`unc`.`last_name`, '') = ''
		),
		'',
		concat(`unc`.`last_name`, ' ')
	)
		) AS `nc_comment_user_name`,
		`unc`.`email` AS `nc_email`,
		`unc`.`avatar` AS `nc_avatar`,
		`unc`.`person_color` AS `nc_person_color`,
		`nc`.`create_time` AS `nc_create_time`,
		`nc`.`news_id` AS `nc_news_id`,
		`nc`.`update_time` AS `nc_update_time`,
		`nc`.`update_account_id` AS `nc_update_account_id`,
		`nc`.`delete_flag` AS `nc_delete_flag`,
		`y`.`version` AS `ye_version`,
		`y`.`content` AS `ye_content`,
		`na`.`account_id` AS `account_id`
	FROM
		(
			(
				(
					(
						(
							(
								(
									(
										(
											(
												(
													(
														(
															(
																(
																	(
																		(
																			(
																				(
																					(
																						(
																							(
																								(
																									(
																										(
																											`tbl_news` `n`
																											LEFT JOIN `tbl_account` `aaa` ON (
																												(
																													`aaa`.`id` = `n`.`create_account_id`
																												)
																											)
																										)
																										LEFT JOIN `tbl_centers` `cc` ON (
																											(`cc`.`id` = `n`.`centers_id`)
																										)
																									)
																									LEFT JOIN `tbl_room` `rr` ON ((`rr`.`id` = `n`.`room_id`))
																								)
																								LEFT JOIN `tbl_user` `uu` ON (
																									(`uu`.`id` = `aaa`.`user_id`)
																								)
																							)
																							LEFT JOIN `tbl_account` `aaaa` ON (
																								(
																									`n`.`approve_account_id` = `aaaa`.`id`
																								)
																							)
																						)
																						LEFT JOIN `tbl_user` `uuuu` ON (
																							(
																								`aaaa`.`user_id` = `uuuu`.`id`
																							)
																						)
																					)
																					LEFT JOIN `tbl_account` `aaaaa` ON (
																						(
																							`n`.`update_account_id` = `aaaaa`.`id`
																						)
																					)
																				)
																				LEFT JOIN `tbl_user` `uuuuu` ON (
																					(
																						`aaaaa`.`user_id` = `uuuuu`.`id`
																					)
																				)
																			)
																			LEFT JOIN `tbl_news_content` `c` ON ((`n`.`id` = `c`.`news_id`))
																		)
																		LEFT JOIN `tbl_news_group` `ng` ON (
																			(
																				(`n`.`id` = `ng`.`news_id`)
																				AND (`ng`.`delete_flag` = 0)
																			)
																		)
																	)
																	LEFT JOIN `tbl_authority_group` `ag` ON (
																		(
																			`ag`.`id` = `ng`.`group_name`
																		)
																	)
																)
																LEFT JOIN `tbl_news_accounts` `na` ON (
																	(
																		(`na`.`news_id` = `n`.`id`)
																		AND (`na`.`type` = 0)
																		AND (`na`.`delete_flag` = 0)
																	)
																)
															)
															LEFT JOIN `tbl_account` `a` ON (
																(`a`.`id` = `na`.`account_id`)
															)
														)
														LEFT JOIN `tbl_user` `u` ON ((`u`.`id` = `a`.`user_id`))
													)
													LEFT JOIN `tbl_nqs_relation` `nn` ON (
														(
															(`nn`.`obj_id` = `n`.`id`)
															AND (`nn`.`delete_flag` = 0)
														)
													)
												)
												LEFT JOIN `tbl_nqs` `nq` ON (
													(
														(
															`nq`.`version` = `nn`.`nqs_version`
														)
														AND (`nn`.`tag` = `nq`.`tag`)
														AND (`nq`.`delete_flag` = 0)
													)
												)
											)
											LEFT JOIN `tbl_attachment` `at` ON (
												(
													(
														`at`.`source_id` = `n`.`img_id`
													)
													AND (`at`.`delete_flag` = 0)
												)
											)
										)
										LEFT JOIN `tbl_news_comment` `nc` ON (
											(
												(`nc`.`news_id` = `n`.`id`)
												AND (`nc`.`delete_flag` = 0)
											)
										)
									)
									LEFT JOIN `tbl_account` `anc` ON (
										(
											`anc`.`id` = `nc`.`comment_account_id`
										)
									)
								)
								LEFT JOIN `tbl_user` `unc` ON (
									(`unc`.`id` = `anc`.`user_id`)
								)
							)
							LEFT JOIN `tbl_news_accounts` `nao` ON (
								(
									(`nao`.`news_id` = `n`.`id`)
									AND (`nao`.`delete_flag` = 0)
									AND (`nao`.`type` = 1)
								)
							)
						)
						LEFT JOIN `tbl_account` `ao` ON (
							(
								`ao`.`id` = `nao`.`account_id`
							)
						)
					)
					LEFT JOIN `tbl_user` `uo` ON ((`uo`.`id` = `ao`.`user_id`))
				)
				LEFT JOIN `tbl_yelf_news` `yn` ON (
					(
						(`yn`.`news_id` = `n`.`id`)
						AND (`yn`.`delete_flag` = 0)
					)
				)
			)
			LEFT JOIN `tbl_yelf` `y` ON (
				(
					(
						`y`.`version` = `yn`.`yelf_version`
					)
					AND (`y`.`delete_flag` = 0)
				)
			)
		)
);