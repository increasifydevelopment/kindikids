/**
 * 
 * @author gfwang
 * @date 2016年6月22日
 * @des 是否有权限查看菜单
 */
function medicineFilter() {
	return function(list, value) {
		var output = [];
		angular.forEach(list, function(data, index, array) {
			if (data.nameIndex == value) {
				output.push(data);
			}
		});
		return output;
	};
};
function wordsFilter() {
	return function(value) {
		var reValue = value.replace("\n","<br/>");
		console.log(reValue);
		return reValue;
	};
};
function childFilter() {
	return function(list, editId) {
		var output = [];
		// 如果没有选择小孩，则取最大的
		var length = list.length;
		if (length === 0) {
			return output;
		}
		if (!editId) {
			output.push(list[length - 1]);
			return output;
		}
		angular.forEach(list, function(data, index, array) {
			if (data.userInfo.id && data.userInfo.id == editId) {
				output.push(data);
			}

		});
		return output;
	};
};
function parentFilter() {
	return function(list, editId) {
		var output = [];
		// 如果没有选择小孩，则取最大的
		var length = list.length;
		if (length === 0) {
			return output;
		}
		if (!editId) {
			output.push(list[length - 1]);
			return output;
		}
		angular.forEach(list, function(data, index, array) {
			if (data.id && data.id == editId) {
				output.push(data);
			}

		});
		return output;
	};
};
function currentAttendsFilter() {
	return function(attends) {
		var output = "";
		var index=0;
		if(!attends){
			return "";
		}
		if(attends.monday){
			index++;
			output="Monday";
		}
		if(attends.tuesday){	
			index++;		
			output+=" and Tuesday";		
		}
		if(attends.wednesday){		
			index++;	
			output+=" and Wednesday";			
		}
		if(attends.thursday){
			index++;
			output+=" and Thursday";
		}
		if(attends.friday){
			index++;
			output+=" and Friday"; 
		}
		//去除第一个and
		if(output.indexOf("and")==1){
			output=output.substring(4);
		}
		//如果有多个选择，只需要最后一个为and，前面为逗号
		var i=0;
		while (output.indexOf("and") >= 0 && i<index-2){
			i++;
            output = output.replace("and", ",");
        }

		return output;
	};
};

/**
 * 
 * @author abliu
 * @date 2016年8月19日
 * @des 过滤requestlog集合数量
 */
function requestLogFilter() {
	return function(list, loadAll) {
		var output = [];
		if (loadAll){
			return list;
		}
		angular.forEach(list, function(data, index, array) {
			if (!loadAll && index<=4) {
				output.push(data);
			}else{
				return output;
			}
		});
		return output;
	};
};
/**
 * 
 * @author abliu
 * @date 2016年8月19日
 * @des 转园记录的园区
 */
function attendanceHistoryFilter() {
	return function(obj) {
		var output = [];
		if(obj.monday){
			output="Mo";
		}
		if(obj.tuesday){

			output+=",Tu";
		}
		if(obj.wednesday){
			output+=",We";
		}
		if(obj.thursday){
			output+=",Th"; 
		}
		if(obj.friday){
			output+=",Fr"; 
		}

		if(output.indexOf(',')==0){
			output=output.substring(1);
		}

		return output;
	};
};
/**
 * 
 * @author abliu
 * @date 2016年8月29日
 * @des 过滤园区roomgroup是否disabled
 */
function centerInfoFilter() {
	return function(list,objid,childId) {
		var output = [];
		angular.forEach(list, function(data, index, array) {
			if(data.status==1){
				output.push(data);				
			}else if (childId!=undefined && data.status==0 && data.id && data.id == objid) {
				output.push(data);
			}
		});
		return output;
	};
};
function ageGroup() {
	return function(age) {
		if(age==null||age==undefined){
			return "";
		}
		return 'Age '+age+'-'+(age+1)
	};
};
function ageGroupTitle() {
	return function(age) {
		if(age==null||age==undefined){
			return "";
		}
		return 'Age Group '+age+' - '+(age+1)+' years'
	};
};
function childNameFilter() {
	return function(obj) {
		if(!obj.middleName){
			return handleName(obj.firstName)+' '+handleName(obj.lastName);
		}
		return handleName(obj.firstName)+' '+handleName(obj.middleName)+' '+handleName(obj.lastName);
	};
};
function handleName(name){
	return !name?'':name;
}
var app = angular.module('kindKidsApp');
app.filter('childFilter', [ childFilter ]);
app.filter('parentFilter', [ parentFilter ]);
app.filter('medicineFilter', [ medicineFilter ]);
app.filter('currentAttendsFilter', [ currentAttendsFilter ]);
app.filter('requestLogFilter', [ requestLogFilter ]);
app.filter('attendanceHistoryFilter', [ attendanceHistoryFilter ]);
app.filter('centerInfoFilter', [ centerInfoFilter ]);
app.filter('ageGroup', [ ageGroup ]);
app.filter('ageGroupTitle', [ ageGroupTitle ]);
app.filter('wordsFilter', [ wordsFilter ]);
app.filter('childNameFilter', [childNameFilter]);
