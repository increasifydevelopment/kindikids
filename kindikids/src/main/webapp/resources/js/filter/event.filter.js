'use strict';

function imageFilter() {
	return function(imageList, isMyChild, childIds) {
		if (!isMyChild || childIds==null || childIds==undefined) {
			return imageList;
		}
		var resultList=[];
		for (var i = 0, m = imageList.length; i < m; i++) {
			if(imageList[i].deleteFlag!=0) continue;
			var childDocs = imageList[i].childSelectList;
			var haveChild=false;
			for (var k=0, l = childDocs.length; k < l; k++) {
				for (var j = 0, n = childIds.length; j < n; j++) {
					if(childDocs[k].id==childIds[j]){
						haveChild=true;
						break;
					}
				}
				if(haveChild){
					resultList.push(imageList[i]);
					break;
				}
			}

		}

		return resultList;
	};
}
var app = angular.module('kindKidsApp');
app.filter('imageFilter', [imageFilter]);