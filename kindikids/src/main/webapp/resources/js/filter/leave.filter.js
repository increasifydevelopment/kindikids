'use strict';
//请假单状态
function leaveStatusFilter() {
	return function (input) {
		if (input === 0) {
			return "Requested";
		}
		if (input === 1) {
			return "Approved";
		}
		if (input === 2) {
			return "Rejected";
		}
		if (input === 3) {
			return "Cancelled";
		}
		if (input === 4) {
			return "Withdrawn";
		}
		if (input === 5) {
			return "Lapsed";
		}
		return '';
	};
};
angular.module('kindKidsApp').filter('leaveStatusFilter', [leaveStatusFilter]);
//请假单类型
function leaveTypeFilter() {
	return function (input) {
		if (input === 0) {
			return "Personal Leave";
		}
		if (input === 1) {
			return "Annual Leave";
		}
		if (input === 2) {
			return "Long Service Leave";
		}
		if (input === 3) {
			return "Parental Leave";
		}
		if (input === 4) {
			return "Professional Development Leave";
		}
		if (input === 5) {
			return "Compassionate Leave";
		}
		if (input === 6) {
			return "Other Leave";
		}
		if (input === 7) {
			return "RDO";
		}
		if (input === 8) {
			return "Change of Shift";
		}
		if (input === 9) {
			return "Change of RDO";
		}
		if (input === 10) {
			return "Casual Unavailability";
		}
		if (input === 11) {
			return "Practicum (Uni/TAFE leave)";
		}
		if (input === 12) {
			return "Jury Duty";
		}
		if (input === 13) {
			return "Workers Compensation";
		}
		if (input === 14) {
			return "Time in Lieu";
		}
		return '';
	};
}
angular.module('kindKidsApp').filter('leaveTypeFilter', [leaveTypeFilter]);


function roleTypeFilter() {
	return function (input) {
		if (input === "Diploma") {
			return "DIP";
		}
		if (input === "Educational Leader") {
			return "EL";
		}
		if (input === "First Aid Officer") {
			return "FAO";
		}
		if (input === "Second In Charge") {
			return "SIC";
		}
		if (input === "Certified Supervisor") {
			return "CS";
		}
		if (input === "ECT") {
			return "ECT";
		}
		if (input === "Nominated Supervisor") {
			return "NS";
		}

		return input;
	};
}
angular.module('kindKidsApp').filter('roleTypeFilter', [roleTypeFilter]);

function leaveFilter() {
	return function (input) {
		if (input === 9) {
			return true;
		}
		if (input === 8) {
			return true;
		}
		if (input === 7) {
			return true;
		}
		if (input === 2) {
			return true;
		}
		if (input === 3) {
			return true;
		}
		return false;
	};
};
angular.module('kindKidsApp').filter('leaveFilter', [leaveFilter]);