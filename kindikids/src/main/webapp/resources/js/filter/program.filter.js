'use strict';

function programType() {
	return function (input) {
		if (input === 0) {
			return "Explore Curriculum";
		}
		if (input === 1) {
			return "Grow Curriculum";
		}
		if (input === 2) {
			return "Lesson";
		}
		if (input === 3) {
			return "School Readiness";
		}
		if (input === 4) {
			return "Interest";
		}
		if (input === 5) {
			return "Interest Follow up";
		}
		if (input === 6) {
			return "Observation";
		}
		if (input === 7) {
			return "Observation Follow up";
		}
		if (input === 8) {
			return "Learning Story";
		}
		if (input === 9) {
			return "Learning Story Follow up";
		}
		if (input === 10) {
			return "Weekly Evaluation";
		}
		if (input === 11) {
			return "Meal Register";
		}
		if (input === 12) {
			return "Sleep / Rest Register";
		}
		if (input === 13) {
			return "Nappy Change Register";
		}
		if (input === 14) {
			return "Toilet Training Register";
		}
		if (input === 15) {
			return "Sunscreen Application Register";
		}
		if (input === 16) {
			return "Other Task";
		}
		if (input === 17) {
			return "Task";
		}
		if (input === 20) {
			return "Meeting Task";
		}
		if (input === 21) {
			return "Money Task";
		}
		if (input === 22) {
			return "Hat Task";
		}
		if (input === 18) {
			return "Administration of Medication";
		}
		return '';
	};
};

function registerType() {
	return function (input) {
		if (input === 11) {
			return "Meal Register";
		}
		if (input === 12) {
			return "Sleep / Rest Register";
		}
		if (input === 13) {
			return "Nappy Change Register";
		}
		if (input === 14) {
			return "Toilet Training Register";
		}
		if (input === 15) {
			return "Sunscreen Application Register";
		}
		return '';
	};
};

function programStatus() {
	return function (input) {
		if (input == 0) {
			return 'Pending';
		}
		if (input == 1) {
			return 'Complete';
		}
		if (input == 2) {
			return 'Overdue';
		}
		if (input == -1) {
			return 'Obsolete';
		}
	}
}

function orderClass() {
	return function (direction) {
		if (direction === -1) {
			return "fa-sort-down";
		} else if (direction === 1) {
			return "fa-sort-up";
		} else {
			return '';
		}
	};
};
angular.module('kindKidsApp').filter('programType', [programType]);
angular.module('kindKidsApp').filter('registerType', [registerType]);
angular.module('kindKidsApp').filter('programStatus', [programStatus]);
angular.module('kindKidsApp').filter('orderClass', [ orderClass ]);