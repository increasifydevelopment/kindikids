'use strict';
function fileExtension() {
	return function(fileName) {
		var result = "word";
		fileName = fileName == null ? '' : fileName;
		var arryNames = fileName.split('.');
		if (arryNames.length > 1) {
			result = arryNames[arryNames.length - 1];
			if (result == "docx" || result == "doc") {
				result = "word";
			}
		}

		return result;
	};
}
function fileNameSplitFilter() {
	return function(fileName) {
		if (!fileName) {
			return '';
		}
		var suffix = '...';
		var length=15;
		if (fileName.length > length) {
			return fileName.substr(0, length) + suffix;
		}
		return fileName;
	};
}
var app = angular.module('kindKidsApp');
app.filter('fileExtension', [ fileExtension ]);
app.filter('fileNameSplitFilter', [ fileNameSplitFilter ]);