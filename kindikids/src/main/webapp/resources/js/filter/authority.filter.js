/**
 * 
 * @author gfwang
 * @date 2016年6月22日
 * @des 是否有权限查看菜单
 */
function haveFunction() {
	return function(funs, name) {
		var result = false;
		angular.forEach(funs, function(data, index, array) {
			if (data!=undefined && data.name == name) {
				result = true;
				return;
			}
		});
		return result;
	};
};
/**
 * 
 * @author gfwang
 * @date 2016年6月22日
 * @des 是否有权限查看菜单
 */
function haveMenu() {
	return function(menus, name) {
		var result = false;
		angular.forEach(menus, function(data, index, array) {
			if (data&&data.name == name) {
				result = true;
				return;
			}
		});
		return result;
	};
};
/**
 * 
 * @author abliu
 * @date 2016年6月22日
 * @des 是否该角色 name 支持多权限0;1;2 一个传入0
 */
function haveRole() {
	return function(roles, name) {
		var result = false;
		angular.forEach(roles, function(data, index, array) {
			if (data!=undefined && (";"+name+";").indexOf(";"+data.value+";")>=0) {
				result = true;
				return;
			}
		});
		return result;
	};
};
var app=angular.module('kindKidsApp');
app.filter('haveFunction', [ haveFunction ]);
app.filter('haveMenu', [ haveMenu ]);
app.filter('haveRole', [ haveRole ]);