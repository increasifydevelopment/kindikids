function attendanceDateM() {
	return function(input) {
		var m = new moment(input, "YYYY/MM");
		var d = m._d;
		var mm = (d.getMonth() + 1) < 10 ? "0" + (d.getMonth() + 1) : d.getMonth() + 1;
		switch (mm) {
			case '01':
				mm = "January";
				break;
			case '02':
				mm = "February";
				break;
			case '03':
				mm = "March";
				break;
			case '04':
				mm = "April";
				break;
			case '05':
				mm = "May";
				break;
			case '06':
				mm = "June";
				break;
			case '07':
				mm = "July";
				break;
			case '08':
				mm = "August";
				break;
			case '09':
				mm = "September";
				break;
			case 10:
				mm = "October";
				break;
			case 11:
				mm = "November";
				break;
			case 12:
				mm = "December";
				break;
			default:
				mm = "";
				break;
		}
		return mm;
	};
};

function attendanceDateY() {
	return function(input) {
		var m = new moment(input, "YYYY/MM");		
		var d = m._d;
		if (d == "Invalid Date") {
			return "";
		}
		var yyyy = d.getFullYear();
		return yyyy;
	};
};

function weekStr() {
	return function(input) {
		var week;
		switch (input) {
			case 0:
				week = "SUN";
				break;
			case 1:
				week = "MON";
				break;
			case 2:
				week = "TUE";
				break;
			case 3:
				week = "WED";
				break;
			case 4:
				week = "THU";
				break;
			case 5:
				week = "FRI";
				break;
			case 6:
				week = "SAT";
				break;
		}
		return week;
	};
};

function compareDate() {
	return function(input) {
		var now = moment([]);
		var m = new moment(input, "YYYY-MM-DDTHH:mm:ss.SSS");
		var sss = m.diff(now, 'day');
		if (sss >= 0) {
			return true;
		}
		return false;
	};
};
var app = angular.module('kindKidsApp');
app.filter('attendanceDateM', [attendanceDateM]);
app.filter('attendanceDateY', [attendanceDateY]);
app.filter('weekStr', [weekStr]);
app.filter('compareDate', [compareDate]);