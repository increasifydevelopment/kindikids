'use strict';
var app=angular.module('kindKidsApp');
//请假单类型
function getLeaveRangeFilter() {
	return function(weekData,startData,endData) {
		var range=1
		weekData=new moment(weekData,"YYYY-MM-DDTHH:mm:ss.SSS");
		startData=new moment(startData,"YYYY-MM-DDTHH:mm:ss.SSS");
		endData=new moment(endData,"YYYY-MM-DDTHH:mm:ss.SSS");
		var startday=startData.diff(weekData,'day');
		var endday=endData.diff(weekData,'day');
		if(startday<0){
			startday=0
		}
		range=endday-startday+1;

		return range;
	};
}
app.filter('getLeaveRangeFilter', [ getLeaveRangeFilter ]);