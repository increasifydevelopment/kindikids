'use strict';

function messageClass() {
	return function(readFalg) {
		if (readFalg === 0) {
			return "message unread";
		} else if (readFalg === 1) {
			return "message";
		} else {
			return '';
		}
	};
};
angular.module('kindKidsApp').filter('messageClass', [ messageClass ]);

function readCssFilter() {
	return function(readFalg) {
		if (readFalg === 1) {
			return "read";
		} else {
			return '';
		}
	};
}
angular.module('kindKidsApp').filter('readCssFilter', [ readCssFilter ]);
function groupFilter() {
	return function(list, flag) {
		var output = [];
		angular.forEach(list, function(data, index, array) {
			// group
			if (flag == 1 && data.groupName) {
				output.push(data);
			}
			if (flag == 0 && !data.groupName) {
				output.push(data);
			}
		});
		return output;
	};
}
angular.module('kindKidsApp').filter('groupFilter', [ groupFilter ]);