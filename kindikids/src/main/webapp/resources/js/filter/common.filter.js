'use strict';

function userNameUpperCase() {
	return function(userName) {
		if (userName == undefined) {
			return "";
		}
		var arryNames = userName.trim().split(' ');
		if (arryNames.length <= 1) {
			var firstChar = arryNames[0].trim().charAt(0);
			return firstChar.toUpperCase();
		} else {
			var firstChar = arryNames[0].trim().charAt(0);
			var lastChar = arryNames[arryNames.length - 1].trim().charAt(0);
			var sign = firstChar + lastChar;
			return sign.toUpperCase();
		}
	};
};

function commotDate() {
	return function(input, format) {
		var date = "";
		var m = new moment(input, "YYYY-MM-DDTHH:mm:ss.SSS");
		date = m.format(format);
		return date;
	}
};

function getFirstEvent() {
	return function(input) {
		var result = "";
		var eventNames = input.trim().split('</br>');
		result = eventNames[0];
		return result;
	}
};
//获取季节URL
function getSeason() {
	return function(input) {
		var m = new moment(input, "YYYY/MM");
		var d = m._d;
		var mm = (d.getMonth() + 1) < 10 ? "0" + (d.getMonth() + 1) : d.getMonth() + 1;
		var season = "";
		switch (mm) {
			case 12:
			case '01':
			case '02':
				season = "bg-summer";
				break;
			case '03':
			case '04':
			case '05':
				season = "bg-autumn";
				break;
			case '06':
			case '07':
			case '08':
				season = "bg-winter";
				break;
			case '09':
			case 10:
			case 11:
				season = "bg-spring";
				break;

		}
		return season;
	};
};
angular.module('kindKidsApp').filter('userNameUpperCase', [userNameUpperCase]);
angular.module('kindKidsApp').filter('commotDate', [commotDate]);
angular.module('kindKidsApp').filter('getSeason', [getSeason]);

angular.module('kindKidsApp').filter('changeDateM', [changeDateM]);
angular.module('kindKidsApp').filter('changeDateY', [changeDateY]);
angular.module('kindKidsApp').filter('weekStr', [weekStr]);
angular.module('kindKidsApp').filter('nqsTipFilter', [nqsTipFilter]);
angular.module('kindKidsApp').filter('getFirstEvent', [getFirstEvent]);


//============================日历列表 list

function changeDateM() {
	return function(input) {
		var m = new moment(input, "YYYY/MM");
		var d = m._d;
		var mm = (d.getMonth() + 1) < 10 ? "0" + (d.getMonth() + 1) : d.getMonth() + 1;
		switch (mm) {
			case '01':
				mm = "January";
				break;
			case '02':
				mm = "February";
				break;
			case '03':
				mm = "March";
				break;
			case '04':
				mm = "April";
				break;
			case '05':
				mm = "May";
				break;
			case '06':
				mm = "June";
				break;
			case '07':
				mm = "July";
				break;
			case '08':
				mm = "August";
				break;
			case '09':
				mm = "September";
				break;
			case 10:
				mm = "October";
				break;
			case 11:
				mm = "November";
				break;
			case 12:
				mm = "December";
				break;
			default:
				mm = "";
				break;
		}
		return mm;
	};
};

function changeDateY() {
	return function(input) {
		var m = new moment(input, "YYYY/MM");
		var d = m._d;
		if (d == "Invalid Date") {
			return "";
		}
		var yyyy = d.getFullYear();
		return yyyy;
	};
};

function weekStr() {
	return function(input) {
		var week;
		switch (input) {
			case 0:
				week = "SUN";
				break;
			case 1:
				week = "MON";
				break;
			case 2:
				week = "TUE";
				break;
			case 3:
				week = "WED";
				break;
			case 4:
				week = "THU";
				break;
			case 5:
				week = "FRI";
				break;
			case 6:
				week = "SAT";
				break;
		}
		return week;
	};
};

function nqsTipFilter() {
	return function(value,tag) {
		if(!value||tag==1){
			return "";
		}
		var prefix=value.substring(0,1)*1;
		var style="";
		switch (prefix) {
		case 1:
			style = "nqsQA1";
			break;
		case 2:
			style = "nqsQA2";
			break;
		case 3:
			style = "nqsQA3";
			break;
		case 4:
			style = "nqsQA4";
			break;
		case 5:
			style = "nqsQA5";
			break;
		case 6:
			style = "nqsQA6";
			break;
		case 7:
			style = "nqsQA7";
			break;
		}
		return style;
	};
};