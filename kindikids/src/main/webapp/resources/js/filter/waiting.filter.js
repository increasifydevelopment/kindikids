/**
 * 处理归档来源
 */
function waitinglistType() {
    return function (type) {
        if (type == 0) {
            return 'APPLICATION';
        }
        if (type == 1) {
            return 'EXTERNAL';
        }
        if (type == 2) {
            return 'SIBLING';
        }
        if (type == 3) {
            return 'ARCHIVED';
        }
    }
};

function sortClass() {
    return function (model, column) {
        if (model.column === column) {
            if (model.flag == true) {
                return 'sorting_desc'
            } else {
                return 'sorting_asc'
            }
        }
    }
}

function isRyde() {
    return function (center) {
        if (center.ryde) {
            return 'Ryde';
        } else {
            return center.name;
        }
    }
}

var app = angular.module('kindKidsApp');
app.filter('waitinglistType', [waitinglistType]);
app.filter('sortClass', [sortClass]);
app.filter('isRyde', [isRyde]);