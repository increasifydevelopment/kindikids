'use strict';

function tagArr() {
	return function(input) {
		if (!input) {
			return '';
		}
		var result = "";
		var targetTags = [{
			tagText: 'Certified Supervisor',
			tag: "CS"
		}, {
			tagText: 'First Aid Officer',
			tag: "FAO"
		}, {
			tagText: 'Nominated Supervisor',
			tag: "NS"
		}, {
			tagText: 'Early Child Teacher',
			tag: "ECT"
		}, {
			tagText: 'Diploma',
			tag: "DIP"
		}, {
			tagText: 'Educational Leader',
			tag: "EL"
		}];
		var tag = input.split(",");
		for (var j = 0, n = tag.length; j < n; j++) {
			for (var i = 0, m = targetTags.length; i < m; i++) {
				if (targetTags[i].tag == tag[j]) {
					result =result+ targetTags[i].tagText + "</br>";
				}
			}
		}
		//去除最后</br>
		if (result.length > 5) {
			result = result.substring(0, result.length - 5);
		}
		return result;
	};
};

function tagTitle() {
	return function(input) {
		var targetTags = [{
			tagText: 'Certified Supervisor',
			tag: "CS"
		}, {
			tagText: 'First Aid Officer',
			tag: "FAO"
		}, {
			tagText: 'Nominated Supervisor',
			tag: "NS"
		}, {
			tagText: 'Early Child Teacher',
			tag: "ECT"
		}];
		for (var i = 0, m = targetTags.length; i < m; i++) {
			if (targetTags[i].tag == input) {
				return targetTags[i].tagText;
			}
		}

	};
};

angular.module('kindKidsApp').filter('tagArr', [tagArr]);
angular.module('kindKidsApp').filter('tagTitle', [tagTitle]);