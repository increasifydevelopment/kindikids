/**
 * 
 * @author gfwang
 * @date 2016年6月22日
 * @des 是否有权限查看菜单
 */
function haveRepeats() {
	return function(choose, value) {
		var result = false;
		var repeats = value.split(',');
		angular.forEach(repeats, function(data, index, array) {
			if (choose && data && data == choose) {
				result = true;
				return;
			}
		});
		return result;
	};
};
function getRepeatMsg() {
	return function(type, startDate, endDate) {
		if (type == null || type == undefined) {
			return "";
		}
		type = type * 1;
		var msg = "";
		switch (type) {
		case 0:
			msg = 'Repeats on every Day';
			break;
		case 1:
			msg = 'Repeats on every Week';
			break;
		case 2:
			msg = 'Repeats on every Month';
			break;
		case 3:
			msg = 'Repeats on every Quarter';
			break;
		case 4:
			msg = 'Repeats on every Year';
			break;
		case 5:
			msg = 'Repeats on Once';
			break;
		case 6:
			msg = 'Repeats on';
			break;
		default:
			break;
		}
		return msg;
	};
};

var app = angular.module('kindKidsApp');
app.filter('haveRepeats', [ haveRepeats ]);
app.filter('getRepeatMsg', [ getRepeatMsg ]);