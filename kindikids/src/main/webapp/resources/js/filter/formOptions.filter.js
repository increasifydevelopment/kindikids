'use strict';
//请假单类型
function formOptionsFilter($filter) {
  return function(type) {
    var options=[
      {id:1,text:'TextBox'},
      {id:2,text:'TextArea'},
      {id:3,text:'Radio'},
      {id:4,text:'CheckBox'},
      {id:5,text:'Select'},
      {id:6,text:'File'},
      {id:7,text:'Table'},
      {id:8,text:'Signature'},
      {id:9,text:'P'},
      {id:10,text:'H1'},
      {id:11,text:'H2'},
      {id:12,text:'H3'},
      {id:13,text:'H4'},
      {id:14,text:'H5'},
      {id:15,text:'H6'}
    ];
    var t = $filter('filter')(options,{id:type},true);
    if(t.length > 0) return t[0].text;
    else return '';
  };
}
angular.module('kindKidsApp').filter('formOptionsFilter',  ['$filter', formOptionsFilter ]);