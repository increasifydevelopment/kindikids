'use strict';

function activityType() {
	return function(input) {
		if (input === 6) {
			return "Observation";
		}
		if (input === 7) {
			return "Obeservation Follow Up";
		}
		if (input === 4) {
			return "Interests";
		}
		if (input === 5) {
			return "Interests Follow Up";
		}
		if (input === 8) {
			return "Learning Story";
		}
		if (input === 9) {
			return "Learning Story Follow Up";
		}
		return '';
	};
};
function getClass() {
	return function(nqsNode) {
		var myclass = '';
		if (nqsNode == 'QA1') {
			myclass = 'color-qa1';
		}
		if (nqsNode == 'QA2') {
			myclass = 'color-qa2';
		}
		if (nqsNode == 'QA3') {
			myclass = 'color-qa3';
		}
		if (nqsNode == 'QA4') {
			myclass = 'color-qa4';
		}
		if (nqsNode == 'QA5') {
			myclass = 'color-qa5';
		}
		if (nqsNode == 'QA6') {
			myclass = 'color-qa6';
		}
		if (nqsNode == 'QA7') {
			myclass = 'color-qa7';
		}
		return myclass;
	};
};
function getProportion() {
	return function(count, maxCount) {
		if (maxCount == 0) {
			return 0;
		}
		if (count >= 100) {
			return (count / maxCount) * 100;
		} else {
			return count;
		}
	};
};
function qipImgFilter() {
	return function(type) {
		var url = '';
		switch (type) {
		case 0:
			url = './resources/library/images/icon-face/1.png';
			break;
		case 1:
			url = 'resources/library/images/icon-face/icon-face-smile.png';
			break;
		case 2:
			url = './resources/library/images/icon-face/2.png';
			break;
		case 3:
			url = './resources/library/images/icon-face/icon-face-dissatisfied.png';
			break;
		case 4:
			url = './resources/library/images/icon-face/3.png';
			break;
		default:
			break;
		}
		return url;
	};
};
function textFilter() {
	return function(type) {
		var text = '';
		switch (type) {
		case 0:
			text = 'Exceeding';
			break;
//		case 1:
//			text = 'High Quality';
//			break;
		case 2:
			text = 'Meeting';
			
			break;
//		case 3:
//			text = 'Low Quality';
//			break;
		case 4:
			text = 'Working Towards';
			break;
		default:
			break;
		}
		return text;
	};
};
angular.module('kindKidsApp').filter('textFilter', [ textFilter ]);
angular.module('kindKidsApp').filter('qipImgFilter', [ qipImgFilter ]);
angular.module('kindKidsApp').filter('activityType', [ activityType ]);
angular.module('kindKidsApp').filter('getClass', [ getClass ]);
angular.module('kindKidsApp').filter('getProportion', [ getProportion ]);