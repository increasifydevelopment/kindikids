'use strict';

function newsFeedType() {
	return function(input) {
		var type = "";
		switch (input) {
			case 0:
				type = "";
				break;
			case 1:
				type = "Explore Curriculum";
				break;
			case 2:
				type = "Grow Curriculum";
				break;
			case 3:
				type = "Lessons";
				break;
			case 4:
				type = "Observation";
				break;
			case 5:
				type = "Observation Follow Up";
				break;
			case 6:
				type = "Interest Follow Up";
				break;
			case 7:
				type = "School Readiness";
				break;
			case 8:
				type = "School Readiness Observation";
				break;
			case 9:
				type = "School Readiness Observation Follow Up";
				break;
			case 10:
				type = "Learning Story";
				break;
			case 11:
				type = "Learning Story Follow up";
				break;
			case 12:
				type = "Today’s Menus";
				break;
			case 13:
				type = "Meal Register";
				break;
			case 14:
				type = "Sleep / Rest Register";
				break;
			case 15:
				type = "Nappy Change Register ";
				break;
			case 16:
				type = "Toilet Training Register ";
				break;
			case 17:
				type = "Weekly Evaluation Explore Curriculum";
				break;
			case 18:
				type = "Birthday";
				break;
			case 19:
				type = "Weekly Evaluation Grow Curriculum";
				break;
			case 20:
				type = "Interest";
				break;
		}
		return type;
	};
};

function newsFeedRating() {
	return function(input) {
		var type = "";
		switch (input) {
			case 1:
				type = "3";
				break;
			case 2:
				type = "icon-face-dissatisfied";
				break;
			case 3:
				type = "2";
				break;
			case 4:
				type = "icon-face-smile";
				break;
			case 5:
				type = "1";
				break;
			default:
				type = "unnamed";
				break;
		}
		return type;
	};
};

function newsFeedCommentDate() {
	return function(input) {
		var str = "";
		var date = "";
		var m = new moment(input, "YYYY-MM-DDTHH:mm:ss.SSS");
		date = m.format("hh:mm:ss A DD MMM YYYY");
		var sss = moment(new Date()).diff(m);
		var ss = sss / 1000;
		if (ss <=0 ||  Math.floor(ss) == 0) {
			str = "1 second ago (" + date + ")";
			return str;
		}
		if (ss > 0 && ss < 60) {

			if (Math.floor(ss) == 1) {
				str = Math.floor(ss) + " second ago (" + date + ")";
			}else{
				str = Math.floor(ss) + " seconds ago (" + date + ")";		
			}
			return str;
		}
		var mm = ss / 60;
		if (mm > 0 && mm < 60) {
			if (Math.floor(mm)  == 1) {
				str = Math.floor(mm) + " minute ago (" + date + ")";
			}else{	
				str = Math.floor(mm) + " minutes ago (" + date + ")";
			}
			
			return str;
		}
		var hh = mm / 60;
		if (hh > 0 && hh < 24) {
			if ( Math.floor(hh) == 1) {
				str = Math.floor(hh) + " hour ago (" + date + ")";
			}else{	
				str = Math.floor(hh) + " hours ago (" + date + ")";
			}
			
			return str;
		}
		var dd = hh / 24;
		if (dd > 0 && dd < 10) {
			if (Math.floor(dd) == 1) {
				str = Math.floor(dd) + " day ago (" + date + ")";
			}else{	
				str = Math.floor(dd) + " days ago (" + date + ")";
			}
			
			return str;
		}
		str = date;
		return str;
	};
};

function haveType() {
	return function(type, typelist) {
		var result = false;
		angular.forEach(typelist.split(','), function(data, index, array) {
			if (data && type == data) {
				result = true;
				return;
			}
		});
		return result;
	};
}
function getGroupName() {
	return function(id, groupSource) {
		var name="";
		angular.forEach(groupSource, function(data, index, array) {
			if (data.id==id) {
				name= data.text;
			}
		});
		return name;
	};
}
var app = angular.module('kindKidsApp');
app.filter('getGroupName', [getGroupName]);
app.filter('haveType', [haveType]);
app.filter('newsFeedType', [newsFeedType]);
app.filter('newsFeedRating', [newsFeedRating]);
app.filter('newsFeedCommentDate', [newsFeedCommentDate]);
