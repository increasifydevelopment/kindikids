function typeOfAttendanceFilter() {
	return function(input) {
		if(input == 0){
			return "Giving Notice";
		}
		if(input == 1){
			return "Absence Request";
		}
		if(input == 2){
			return "Change of Room";
		}
		if(input == 3){
			return "Change of Centre";
		}
		if(input == 4){
			return "Change of Attendance";
		}
		if(input == null || input == undefined){
			return "Change of Attendance";
		}
	};
};
function statusOfRequestLog(){
	return function(input) {
		if(input == 0){
			return "Pending";
		}
		if(input == 1){
			return "Cancelled";
		}
		if(input == 2){
			return "Partial Approved";
		}
		if(input == 4){
			return "Declined";
		}
		if(input == 5 || input == 3){
			return "Approved";
		}
		if(input == 6){
			return "Lapsed";
		}
	};
};
var app = angular.module('kindKidsApp');
app.filter('typeOfAttendanceFilter', [typeOfAttendanceFilter]);
app.filter('statusOfRequestLog', [statusOfRequestLog]);
