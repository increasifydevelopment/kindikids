'use strict';
function shiftTitle() {
	return function(input) {
		if("Diploma"===input){
			return "DIP";
		}
		var arryNames = input.trim().split(' ');
		var upperName = '';
		for (var i = 0 ; i < arryNames.length ; i ++ ){
			upperName = upperName +arryNames[i].trim().charAt(0);
		}
		return upperName;
	};
};
angular.module('kindKidsApp')
		.filter('shiftTitle', [ shiftTitle ]);
