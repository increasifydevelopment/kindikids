'use strict';

/**
 * @ngdoc overview
 * @name urbanApp
 * @description
 * # urbanApp
 * Main module of the application.
 */
angular.module('kindKidsApp', ['ui.router', 'ui.bootstrap',  'ngAnimate', 
                                'oc.lazyLoad', 'ngFileUpload', 'resizer_app', 'angular-flexslider', 'ngImgCrop'
    /* ,
    'ngSanitize',
    'ui.utils',
    'ngTouch',
    'ngFileUpload','ngAnimate','ngStorage'*/
  ])
  //配置导航安全
  .config([
    '$compileProvider',
    function($compileProvider) {
      $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto):/);
    }
  ]);