//页面初始化
$(function () {
	document.onkeydown = function (e) {
		var ev = document.all ? window.event : e;
		if (ev.keyCode == 13) {
			resetPwd();
		}
	};

	var parame = window.location.href;

	if (parame.indexOf("token") > 0) {
		$("#token").val(getQueryString("token"));
	}
});

/**
 * 获取url参数
 * 
 * @param name
 *            参数名
 * @returns 值
 */
function getQueryString(name) {
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
	var r = window.location.search.substr(1).match(reg);
	if (r != null)
		return unescape(r[2]);
	return null;
}

/**
 * 登陆
 */
function resetPwd() {
	var newPwd = $.trim($("#newPwd").val());
	var rePwd = $.trim($("#rePwd").val());
	if (newPwd == '') {
		$.messager.popup("The password is required!", false);
		return;
	}
	if (rePwd == '') {
		$.messager.popup("The password is required!", false);
		return;
	}
	if (rePwd.length > 32 || rePwd.length < 8) {
		$.messager.popup("The Password length must be greater than 8 and less than 32.", false);
		return;
	}
	if (!/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[^]{8,32}$/.test(newPwd)) {
		$.messager.popup("The Password must contain capital letters, lowercase letters, and numbers.", false);
		return;
	}
	if (newPwd != rePwd) {
		$.messager.popup("Please ensure your passwords match.", false);
		return;
	}
	var token = escape($('#token').val());
	$.ajax({
		type: "POST",
		dataType: "json",
		async: true,
		cache: false,
		url: "../../account/resetPwd.do",
		data: "token=" + token + "&newPwd=" + newPwd + "&rePwd=" + rePwd,
		success: function (result) {
			if (result.code == 0) {
				// 修改密码成功后直接进入首页
				window.location.href = "../../index.html";
			} else {
				$.messager.popup(result.msg, false);
			}
		}
	});
}