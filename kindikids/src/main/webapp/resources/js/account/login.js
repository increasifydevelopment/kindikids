var resultUrl = "";
// 页面初始化
$(function () {
	// 获取ResultUrl链接
	var parame = window.location.href;
	var index = parame.indexOf("resultUrl");
	if (index != -1) {
		resultUrl = unescape(parame.substring(index + 10, parame.length));
	}
	if (parame.indexOf("errormsg") > 0) {
		var msg = getQueryString("errormsg");
		$.messager.popup(msg, false);
	}
	document.onkeydown = function (e) {
		var ev = document.all ? window.event : e;
		if (ev.keyCode == 13) {
			login();
		}
	};
});

/**
 * 获取url参数
 * 
 * @param name
 *            参数名
 * @returns 值
 */
function getQueryString(name) {
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
	var r = window.location.search.substr(1).match(reg);
	if (r != null)
		return unescape(r[2]);
	return null;
}

/**
 * 登陆
 */
function login() {
	var username = $.trim($("#username").val());
	var password = $.trim($("#password").val());

	// 数据合法性验证
	if (username == '') {
		$.messager.popup("A valid email address is required.");
		return;
	}
	if (password == '') {
		$.messager.popup("Please enter your password.");
		return;
	}
	if (username.length > 255 || username.length < 3) {
		$.messager.popup("Incorrect email address or password, please try again.");
		return;
	}
	if (password.length > 32 || password.length < 3) {
		$.messager.popup("Incorrect email address or password, please try again.");
		return;
	}

	// 获取google验证
	var googleVlidateId = "";
	try {
		googleVlidateId = grecaptcha.getResponse(id);
	} catch (e) {
		console.log("Google validate error");
	}

	$.ajax({
		type: "POST",
		dataType: "json",
		async: true,
		cache: false,
		url: "./account/login.do",
		data: "account=" + encodeURIComponent(username) + "&password=" + encodeURIComponent(password) + '&random=' + new Date().getTime() + '&googleVlidateId=' + googleVlidateId,
		success: function (result) {
			if (result.code == 3) {
				$('#grecaptcha').show();
				$('#loginBut').attr("disabled", true);
				try {
					grecaptcha.reset(id);
				} catch (e) {
					console.log("Google validate error");
				}
			}
			if (result.code) {
				$.messager.popup(result.msg);
				return;
			}

			// 判断是否存在登陆后需要跳转的页面
			if (resultUrl != null && resultUrl != "") {
				window.location.href = resultUrl;
			} else {
				// 判断是否是未来入园孩子家长登录,是则跳转家庭页面
				if (result.data) {
					window.location.href = "./index.html#/user_family_list/";
				} else {
					window.location.href = "./index.html";
				}
			}
		}
	});
}

//Google 验证

var id;
var onloadCallback = function () {
	id = grecaptcha.render('grecaptcha', {
		'sitekey': '6LenLHYUAAAAAEs011MAaQc-EdHXtLb87U_nVtRm',
		'callback': function () {
			$('#grecaptcha').hide();
			$('#loginBut').removeAttr("disabled");
		},
		'expired-callback	': function () {

		},
		'error-callback	': function () {

		}
	});
}