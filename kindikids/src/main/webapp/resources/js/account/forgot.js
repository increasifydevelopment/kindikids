//页面初始化
$(function() {
	$('#username').keypress(function(e){
		  if(e.which == 13) {  
			  resetPassword();
	    }  
	});
});

/**
 * 登陆
 */
function resetPassword() {
	var username = $.trim($("#username").val());
	if (username == '') {
		$.messager.popup("A valid email address is required.");
		return;
	}
	$.ajax({
		type : "POST",
		dataType : "json",
		async : true,
		cache : false,
		url : "../../account/sendPwdMail.do",
		data : "account=" + username + '&random=' + new Date().getTime(),
		success : function(result) {
			$.messager.popup(result.msg);
			return;
		}
	});
}
