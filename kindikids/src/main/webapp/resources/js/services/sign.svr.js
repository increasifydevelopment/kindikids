'use strict';

/**
 * @author zhengguo
 * @description
 */
function signService($http, commonService) {

	// variable=================================================================
	var output = {};

	/**
	 * 根据name获取centerId
	 */
	output.getCenterId = function(name) {
		return $http({
			method : 'POST',
			url : 'sign/getCentreId.do',
			data : 'name=' + encodeURIComponent(name),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.centerIdResult = result;
		}).error(function(data, status, headers, config) {
			output.centerIdResult = undefined;
			commonService.ajaxE(data);
		});
	};

	/**
	 * 获取center列表
	 */
	output.getCenterList = function() {
		return $http({
			method : 'GET',
			url : 'common/getCenters.do',
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.centerListResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * 获取访客签入未签出的列表
	 */
	output.getSigninList = function(id, params) {
		var param = params ? encodeURIComponent(params) : '';
		return $http({
			method : 'POST',
			url : 'sign/visitorSignInList.do',
			data : 'id=' + id + '&params=' + param,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.signinListResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * 新增一个访客签入信息
	 */
	output.saveVisitorSignIn = function(id, condition) {
		return $http({
			method : 'POST',
			url : 'sign/visitorSignIn.do',
			data : 'id=' + id + '&params=' + encodeURIComponent(JSON.stringify(condition)),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.saveVisitorSigninResult = result;
		}).error(function(data, status, headers, config) {
			output.saveVisitorSigninResult = undefined;
			commonService.ajaxE(data);
		});
	};

	/**
	 * 根据signinID 获取签到信息
	 */
	output.getSignInfo = function(flag, id, inDate, centerId) {
		// flag 0 childInfo 1 visitorInfo 2 staff 3 Centre
		// Manager/Cook/Additional Staff
		var onlineUrl;
		var onlineData;
		if (flag == 0) {
			onlineUrl = 'sign/childSignInfo.do';
			onlineData = 'id=' + id + '&date=' + inDate;
		} else if (flag == 1) {
			onlineUrl = 'sign/getSignIn.do';
			onlineData = 'id=' + id;
		} else if (flag == 2) {
			onlineUrl = 'sign/staffSignInfo.do';
			onlineData = 'id=' + id + '&date=' + inDate + '&centerId=' + centerId;
		} else {// flag == 3
			onlineUrl = 'rosterstaff/getRosterStaffItem.do';
			onlineData = 'objId=' + id;
		}
		return $http({
			method : 'POST',
			url : onlineUrl,
			data : onlineData,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.signInfoResult = result;
		}).error(function(data, status, headers, config) {
			output.signInfoResult = undefined;
			commonService.ajaxE(data);
		});
	};

	/**
	 * 根据centerId 获取staff list签到信息 flag ：0 childSignin, 1 childSignout, 2
	 * staffSignin, 3 staffSignout
	 */
	output.getStaffSigninList = function(flag, id, params) {
		var str = params ? encodeURIComponent(params) : '';
		var onlineUrl;
		var onlineData;
		if (flag == 2 || flag == 3) {
			onlineUrl = 'sign/staffList.do';
			onlineData = 'flag=' + flag + '&id=' + id + "&name=" + str;
		} else {
			onlineUrl = 'attendance/getTodayAttendanceList.do';
			onlineData = 'type=' + flag + '&centerId=' + id + "&name=" + str;
		}
		return $http({
			method : 'POST',
			url : onlineUrl,
			data : onlineData,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.staffSigninListResult = result;
		}).error(function(data, status, headers, config) {
			output.staffSigninListResult = undefined;
			commonService.ajaxE(data);
		});
	};

	/**
	 * 传值staff的id，centerId，和pwd 到后台输入staff签到信息 flag : 2 signin 3 signout
	 */
	output.saveStaffSign = function(flag, centerId, id, params) {
		var str = params ? encodeURIComponent(params) : '';
		var onlineUrl;
		if (flag == 2) {
			onlineUrl = 'sign/staffSignIn.do';
		} else {
			onlineUrl = 'sign/staffSignOut.do';
		}
		return $http({
			method : 'POST',
			url : onlineUrl,
			data : 'centreId=' + centerId + '&id=' + id + "&psw=" + str,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.staffSignResult = result;
		}).error(function(data, status, headers, config) {
			output.staffSignResult = undefined;
			commonService.ajaxE(data);
		});
	};

	/**
	 * 传值child的id，inName, centerId，和sunscreenApp 到后台输入child签到信息 flag : 0 signin
	 * 1 signout
	 */
	output.saveChildSign = function(flag, id, inName, sunscreenApp, centreId) {
		var str = inName ? encodeURIComponent(inName) : '';
		var onlineUrl;
		var onlineData;
		if (flag == 0) {
			onlineUrl = 'sign/childSignIn.do';
			onlineData = 'id=' + id + "&sunscreenApp=" + sunscreenApp + "&inName=" + str;
		} else {
			onlineUrl = 'sign/childSignOut.do';
			onlineData = 'id=' + id + "&outName=" + str;
		}
		return $http({
			method : 'POST',
			url : onlineUrl,
			data : onlineData,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.childSignResult = result;
		}).error(function(data, status, headers, config) {
			output.childSignResult = undefined;
			commonService.ajaxE(data);
		});
	};

	/**
	 * 根据centerId 获取child list签到信息
	 */
	output.getChildSigninList = function(id, str) {
		return $http({
			method : 'POST',
			url : 'attendance/getTodayAttendanceList.do',
			data : 'id=' + id + '$str=' + str,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.childSigninListResult = result;
		}).error(function(data, status, headers, config) {
			output.childSigninListResult = undefined;
			commonService.ajaxE(data);
		});
	};

	/**
	 * 保存sign编辑的签到信息
	 */
	output.saveSignInfo = function(flag, params, comfirmSure, centerId) {
		// 0 child sign in
		// 1 visitor sign in
		// 3 staff sign in/包括几种角色；center manager等
		var onlineUrl;
		if (flag == 0) {
			onlineUrl = 'sign/submitChildSign.do?flag=' + comfirmSure;
		} else if (flag == 1) {
			onlineUrl = 'sign/submitSignIn.do';
		} else if (flag == 2) {
			onlineUrl = 'sign/submitStaffSign.do?comfirmSure=' + comfirmSure + '&centerId=' + centerId;
		} else {
			onlineUrl = 'rosterstaff/updateRosterStaffItem.do?comfirmSure=' + comfirmSure;
		}
		return $http({
			method : 'POST',
			url : onlineUrl,
			data : 'params=' + encodeURIComponent(JSON.stringify(params)),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.saveInfoResult = result;
		}).error(function(data, status, headers, config) {
			output.saveInfoResult = undefined;
			commonService.ajaxE(data);
		});
	};

	/**
	 * 新增一个访客签出信息
	 */
	output.saveVisitorSignout = function(condition) {
		return $http({
			method : 'POST',
			url : 'sign/visitorSignOut.do',
			data : 'params=' + encodeURIComponent(JSON.stringify(condition)),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.saveVisitorSignoutResult = result;
		}).error(function(data, status, headers, config) {
			output.saveVisitorSignoutResult = undefined;
			commonService.ajaxE(data);
		});
	};
	/**
	 * 清除未签出标识
	 */
	output.removeNotHaveSignout = function(accountId) {
		return $http({
			method : 'POST',
			url : 'sign/romoveNotHaveSignout.do',
			data : 'accountId=' + accountId,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function() {
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	}
	return output;
}
angular.module('kindKidsApp').factory('signService', [ '$http', 'commonService', signService ]);