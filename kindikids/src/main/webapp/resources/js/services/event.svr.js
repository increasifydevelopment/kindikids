'use strict';

/**
 * @author abliu
 * @description eventService
 */
function eventService($q, $http, commonService) {
	// variable=================================================================
	var output = {};

	// select===================================================================

	/**
	 * @author abliu
	 * @description
	 * @returns 获取EventList
	 */
	output.getEventList = function(condition) {
		return $http({
			method : 'POST',
			url : 'event/list.do',
			data : "params=" + encodeURIComponent(JSON.stringify(condition)),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.listResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * @author abliu
	 * @description
	 * @returns 获取eventinfo
	 */
	output.getEventInfo = function(id) {
		/* return $http.get("./data/policy.data.json") */
		return $http({
			method : 'POST',
			url : 'event/info.do',
			data : "id=" + encodeURIComponent(id),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.eventInfo = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.getChildId = function(familyId) {
		return $http({
			method : 'POST',
			url : 'event/getChild.do',
			data : "id=" + encodeURIComponent(familyId),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.childInfo = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * @author abliu
	 * @description
	 * @returns 获取GalleryList
	 */
	output.getGalleryList = function(condition) {
		return $http({
			method : 'POST',
			url : 'event/galleryList.do',
			data : "params=" + encodeURIComponent(JSON.stringify(condition)),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.galleryListResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	// edit=====================================================================
	/**
	 * @author abliu
	 * @description
	 * @returns saveEvent
	 */
	output.saveEvent = function(eventInfo) {
		return $http({
			method : 'POST',
			url : 'event/edit.do',
			data : "params=" + encodeURIComponent(JSON.stringify(eventInfo)),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.saveResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	
	output.deleteEvent = function(id) {
		return $http({
			method : 'POST',
			url : 'event/delete.do',
			data : "id=" + id,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.deleteResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.getCenterList = function() {
		return $http({
			method : 'POST',
			url : 'event/centerList.do',
			data : null,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.centerListResult = result.data;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	return output;
}
angular.module('kindKidsApp').factory('eventService', [ '$q', '$http', 'commonService', eventService ]);