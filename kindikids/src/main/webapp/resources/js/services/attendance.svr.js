'use strict';

/**
 * @author abliu
 * @description attendanceService Service
 */
function attendanceService($http, commonService) {
	// variable=================================================================
	var output = {};

	// select===================================================================
	/**
	 * @author abliu
	 * @description
	 * @returns 获取attendanceList
	 */
	output.getAttendanceList = function (condition) {
		return $http({
			method: 'POST',
			url: 'attendance/getAttendanceList.do',
			data: $.param(condition),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.listResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.getRosterAttendanceList = function (condition) {
		return $http({
			method: 'POST',
			url: 'rosterstaff/getAttendanceList.do',
			data: $.param(condition),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.listResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * @author big
	 * @description
	 * @returns 获取RequestLogObj
	 */
	output.getRequestLogObj = function (objId, type) {
		return $http({
			method: 'POST',
			url: 'attendance/getRequestLogObj.do',
			data: "objId=" + objId + "&type=" + type,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.requestLogObj = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.updateAttendanceRequest = function (objId, value) {
		return $http({
			method: 'POST',
			url: 'attendance/updateAttendanceRequest.do',
			data: "objId=" + objId + "&value=" + value,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.updateAttendanceRequestObj = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.updateTemporaryRequest = function (type, objId, value) {
		return $http({
			method: 'POST',
			url: 'attendance/updateTemporaryRequest.do',
			data: "type=" + type + "&objId=" + objId + "&value=" + value,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.updateTemporaryRequestObj = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.replaceChildAttendance = function (absenteeId, childId, day) {
		return $http({
			method: 'POST',
			url: 'attendance/replaceChildAttendance.do',
			data: "absenteeId=" + absenteeId + "&childId=" + childId + "&day=" + day,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.replaceChildAttendanceObj = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};


	output.getAddChilds = function (roomId, day) {
		return $http({
			method: 'POST',
			url: 'attendance/getAddChilds.do',
			data: "roomId=" + roomId + "&day=" + day,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.getAddChildsObj = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};



	// String absenteeId, String oldChildId, Date day, int dayOrInstances, int type
	output.approveReplace = function (absenteeId, oldChildId, day, dayOrInstances, type, overOld) {
		return $http({
			method: 'POST',
			url: 'attendance/approveReplace.do',
			data: "absenteeId=" + absenteeId + "&oldChildId=" + oldChildId + "&day=" + day + "&dayOrInstances=" + dayOrInstances + "&type=" + type + "&overOld=" + overOld,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.approveReplaceObj = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.approveAdd = function (approveAddCondition) {
		return $http({
			method: 'POST',
			url: 'attendance/approveAdd.do',
			data: $.param(approveAddCondition),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.approveAddObj = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	//String roomId, String childId, Date day
	output.subtractedAttendance = function (roomId, childId, day, type, cover, interim) {
		return $http({
			method: 'POST',
			url: 'attendance/subtractedAttendance.do',
			data: "roomId=" + roomId + "&childId=" + childId + "&day=" + day + "&type=" + type + "&cover=" + cover + "&interim=" + interim,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.subtractedAttendanceObj = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	//add by zhengguo
	output.exportExcel = function (SignCondition) {
		return $http({
			method: 'POST',
			url: 'sign/export.do',
			data: "params=" + JSON.stringify(SignCondition),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.getUUIDResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.childExportExcel = function (SignCondition) {
		return $http({
			method: 'POST',
			url: 'sign/childExport.do',
			data: "params=" + JSON.stringify(SignCondition),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.getChildUUIDResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.staffExportExcel = function (SignCondition) {
		return $http({
			method: 'POST',
			url: 'sign/staffExport.do',
			data: "params=" + JSON.stringify(SignCondition),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.getStaffUUIDResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};


	return output;
}
angular.module('kindKidsApp').factory('attendanceService', ['$http', 'commonService', attendanceService]);