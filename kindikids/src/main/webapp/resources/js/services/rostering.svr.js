'use strict';

/**
 * @author zhengguo
 * @description
 */
function rosteringService($http, commonService) {

	// variable=================================================================
	var output = {};

	/**
	 * 保存shiftManage 列表
	 */
	output.saveRosterShift = function (condition) {
		var p = JSON.stringify(condition);
		return $http({
			method: 'POST',
			url: 'shift/saveShift.do',
			data: 'json=' + JSON.stringify(condition),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.saveShiftResult = result;
		}).error(function (data, status, headers, config) {
			output.saveShiftResult = undefined;
			commonService.ajaxE(data);
		});
	};


	output.goRequestState = function (leaveId) {
		return $http({
			method: 'POST',
			url: 'leave/goRequestState.do',
			data: 'leaveId=' + leaveId,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.goResult = result;
		}).error(function (data, status, headers, config) {
			output.saveShiftResult = undefined;
			commonService.ajaxE(data);
		});
	};

	/**
	 * 删除对应的 shift task 行
	 */
	output.delShiftTask = function (id) {
		return $http({
			method: 'POST',
			url: 'shift/deleteShiftTime.do?id=' + id,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.delShiftTaskResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * 获取对应的 task template 数组
	 */
	output.getRosterTemplates = function (id) {
		return $http({
			method: 'GET',
			url: 'rosterstaff/getRosterStaffTemplates.do?centreId=' + id,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.getRosterTemplatesResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * 获取对应的 task template 数组
	 */
	output.chooseTemplates = function (rosterId, day) {
		return $http({
			method: 'POST',
			url: 'rosterstaff/chooseTemplates.do',
			data: 'rosterId=' + rosterId + '&day=' + day,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.chooseTemplatesResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * 更新排班表的状态 draft 0 / submit 1 / approve 2
	 */
	output.setRosterStatu = function (rosterId, type, alert) {
		return $http({
			method: 'POST',
			url: 'rosterstaff/setRosterStatu.do',
			data: 'rosterId=' + rosterId + '&statu=' + type + "&alert=" + alert,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.setRosterStatuResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * 保存排班为模板
	 */
	output.setRosterTemplate = function (rosterId, templateFlag, name) {
		var boolflag;
		if (templateFlag == 0) {
			boolflag = true;
		} else {
			boolflag = false;
		}
		return $http({
			method: 'POST',
			url: 'rosterstaff/setRosterTemplate.do',
			data: 'rosterId=' + rosterId + '&templateFlag=' + boolflag + '&name=' + encodeURIComponent(name),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.setRosterTemplateResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * 获取shift task vos 列表
	 */
	output.getShiftPageList = function (cId) {
		return $http({
			method: 'POST',
			url: 'shift/getShiftVo.do?centreId=' + cId,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.shiftPageResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * 获取roster staff list 列表
	 */
	output.getRosterStaffList = function (json) {
		return $http({
			method: 'POST',
			url: 'rosterstaff/getRosterStaff.do',
			data: 'json=' + JSON.stringify(json),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.rosterStaffListResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * 创建 blankRoster
	 */
	output.createBlankRoster = function (centreId, day) {
		return $http({
			method: 'POST',
			url: 'rosterstaff/createBlankRoster.do',
			data: 'centreId=' + centreId + '&day=' + day,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.blankRosterResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * 根据添加staff
	 */
	output.addRosterStaff = function (json, alert) {
		return $http({
			method: 'POST',
			url: 'rosterstaff/addRosterStaff.do',
			data: 'json=' + JSON.stringify(json) + "&alert=" + alert,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.addRosterStaffResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * 根据id删除staff
	 */
	output.deleteRosterStaff = function (objId, alert) {
		return $http({
			method: 'POST',
			url: 'rosterstaff/deleteRosterStaff.do',
			data: 'objId=' + objId + "&alert=" + alert,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.deleteRosterStaffResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};


	/**
	 * 创建 blankRoster
	 */
	output.createBlankRoster = function (centreId, day) {
		return $http({
			method: 'POST',
			url: 'rosterstaff/createBlankRoster.do',
			data: 'centreId=' + centreId + '&day=' + day,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.blankRosterResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * 获取shiftTaskList列表
	 */
	output.getShiftTaskList = function (cId) {
		return $http({
			method: 'GET',
			url: 'shift/getShiftTasks.do?centreId=' + cId,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.shiftTaskResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * 获取staffLeave列表
	 */
	output.getStaffLeaveList = function (condition) {
		return $http({
			method: 'POST',
			url: 'leave/getLeaveList.do',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			data: 'params=' + JSON.stringify(condition)
		}).success(function (result) {
			output.listResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * 获取LeaveTypeList列表
	 */
	output.getLeaveList = function (type) {
		return $http({
			method: 'POST',
			url: 'rostering/getLeaveTypeList.do?type=' + type,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.leaveListResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * 获取myLeave列表
	 */
	output.getMyLeaveList = function (condition) {
		return $http({
			method: 'POST',
			url: 'leave/getMyLeaveList.do',
			data: 'params=' + JSON.stringify(condition),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.myListResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * 新增一个staff_leave申请
	 */
	output.saveStaffLeave = function (condition, confirm) {
		if (confirm == undefined || confirm == null) {
			confirm = false;
		}
		var leaveVo = {};
		leaveVo.id = condition.id;
		leaveVo.startDate = condition.startDate;
		leaveVo.endDate = condition.endDate;
		leaveVo.type = condition.type;
		leaveVo.isAdmin = condition.isAdmin;
		leaveVo.confirm = confirm;
		leaveVo.reason = condition.reason;
		leaveVo.accountId = condition.accountId;
		leaveVo.centerId = condition.centerId;
		return $http({
			method: 'POST',
			url: 'leave/saveLeave.do?',
			data: 'params=' + JSON.stringify(leaveVo),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.saveResult = result;
		}).error(function (data, status, headers, config) {
			output.saveResult = undefined;
			commonService.ajaxE(data);
		});
	};

	/**
	 * 新增一个my_leave申请
	 */
	output.saveMyLeave = function (condition, confirm) {
		if (confirm == undefined || confirm == null) {
			confirm = false;
		}
		var leaveVo = {};
		leaveVo.id = condition.id;
		leaveVo.startDate = condition.startDate;
		leaveVo.endDate = condition.endDate;
		leaveVo.type = condition.type;
		leaveVo.isAdmin = condition.isAdmin;
		leaveVo.confirm = confirm;
		leaveVo.reason = condition.reason;
		leaveVo.accountId = condition.accountId;

		return $http({
			method: 'POST',
			url: 'leave/saveMyLeave.do?',
			data: 'params=' + JSON.stringify(leaveVo),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.saveMyResult = result;
		}).error(function (data, status, headers, config) {
			output.saveResult = undefined;
			commonService.ajaxE(data);
		});
	};

	/**
	 * 处理staff_leave申请
	 */
	output.operaStaffLeave = function (leaveId, operaType, reason, list, confirm) {
		var param = !list ? '' : JSON.stringify(list);
		if (confirm == undefined || confirm == null) {
			confirm = false;
		}
		return $http({
			method: 'POST',
			url: 'leave/operaLeave.do',
			data: 'leaveId=' + leaveId + '&operaType=' + operaType + '&reason=' + reason + '&params=' + param + '&confirm=' + confirm,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.operaResult = result;
		}).error(function (data, status, headers, config) {
			output.operaResult = undefined;
			commonService.ajaxE(data);
		});
	};

	/**
	 * 处理my_leave申请
	 */
	output.operaMyLeave = function (leaveId, operaType, reason, list) {
		var param = !list ? '' : JSON.stringify(list);
		return $http({
			method: 'POST',
			url: 'leave/myOperaLeave.do',
			data: 'leaveId=' + leaveId + '&operaType=' + operaType + '&reason=' + reason + '&params=' + param,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.operaMyResult = result;
		}).error(function (data, status, headers, config) {
			output.operaResult = undefined;
			commonService.ajaxE(data);
		});
	};

	/**
	 * 获取approve_leave审批时间列表
	 */
	output.getLeaveDate = function (leaveId) {
		return $http({
			method: 'GET',
			url: 'leave/leaveDate.do?leaveId=' + leaveId,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.leaveListResult = result;
		}).error(function (data, status, headers, config) {
			output.leaveListResult = undefined;
			commonService.ajaxE(data);
		});
	};

	/**
	 * by mingwang
	 * 获取personleave+absence
	 */
	output.getAbsenceLeave = function () {
		return $http({
			method: 'POST',
			url: 'rosterstaff/geRosterLeaveVoList.do',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.absenceLeaveListResult = result;
		}).error(function (data, status, headers, config) {
			output.absenceLeaveListResult = undefined;
			commonService.ajaxE(data);
		});
	};

	/**
	 * by mingwang
	 * 保存absence
	 */
	output.saveAbsenceLeave = function (itemInfo) {
		return $http({
			method: 'POST',
			url: 'rosterstaff/saveRosterLeave.do',
			data: "params=" + encodeURIComponent(JSON.stringify(itemInfo)),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.saveAbsenceLeaveResult = result;
		}).error(function (data, status, headers, config) {
			output.saveAbsenceLeaveResult = undefined;
			commonService.ajaxE(data);
		});
	};

	/**
	 * by mingwang
	 * 删除absence
	 */
	output.delAbsenceLeave = function (itemId) {
		return $http({
			method: 'POST',
			url: 'rosterstaff/deleteRosterLeave.do?id=' + itemId,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.delAbsenceLeaveResult = result;
		}).error(function (data, status, headers, config) {
			output.delAbsenceLeaveResult = undefined;
			commonService.ajaxE(data);
		});
	};

	output.saveClosurePeriod = function (info) {
		return $http({
			method: 'POST',
			url: 'rosterstaff/saveClosurePeriod.do',
			data: "params=" + encodeURIComponent(JSON.stringify(info)),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.saveClosurePeriodResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	}

	output.deleteClosurePeriod = function (id) {
		return $http({
			method: 'POST',
			url: 'rosterstaff/deleteClosurePeriod.do',
			data: "id=" + id,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.deleteClosurePeriodResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	}

	output.payrollData = function (condition) {
		return $http({
			method: 'POST',
			url: 'rosterstaff/payrollData.do',
			data: "params=" + encodeURIComponent(JSON.stringify(condition)),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.payrollDataResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	}

	return output;
}
angular.module('kindKidsApp').factory('rosteringService', ['$http', 'commonService', rosteringService]);