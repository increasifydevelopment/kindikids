'use strict';

/**
 * @author xdwang
 * @description Common Service
 */
function commonService($http) {
	var output = {};
	var returnUrlList = [];
	output.ajaxE = function(data) {
		if (data != null && data.msg != null && data.msg != "") {
			try {
				var href = window.location.href;
				if (data.msg == 'SessionLost') {
					gotoLogin(href);
					$.messager.popup("The session timeout!");
				}else if(data.msg == 'No permissions'){
					$.messager.popup(data.msg);
					gotoLogin(href);
				}
				// 去除遮罩层
				$(".loadmask").remove();
				$(".loadmask-msg").remove();
				$.messager.popup(data.msg);
				return;
			} catch (e) {
				// console.log(e);
			}
		}
	};
	function gotoLogin(href){
		for (var i = 0, m = returnUrlList.length; i < m; i++) {
			if (href.indexOf(returnUrlList[i]) > 0) {
				// 跳转到登录页并添加返回页面
				location.href = "login.html?resultUrl=" + escape(href);
				return;
			}
		}
		// 跳转到登录页
		location.href = "login.html";
		return;
	}

	/**
	 * @author abliu
	 * @description 获取登录者用户信息
	 * @returns 当前登录人的信息
	 */
	output.getCurrentUser = function() {
		return $http({
			method : 'POST',
			url : 'account/getCurrentUser.do',
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			if (result.code == 0) {
				output.currentUser = result.data;
			} else {
				$.messager.popup(result.msg);
			}
		}).error(function(result, status, headers, config) {
			output.ajaxE(result);
		});
	};

	/**
	 * @author abliu
	 * @description 获取搜索条件的园区和room集合
	 * @returns 园区和room集合
	 */
	output.getMenuOrgs = function(type) {
		return $http({
			method : 'POST',
			url : 'common/getMenuOrgs.do?type=' + type,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			if (result.code == 0) {
				output.orgList = result.data;
			} else {
				$.messager.popup(result.msg);
			}
		}).error(function(result, status, headers, config) {
			output.ajaxE(result);
		});
	};

	/**
	 * @author abliu
	 * @description 退出系统
	 */
	output.logout = function() {
		output.logoutResult = false;
		return $http({
			method : 'GET',
			url : 'account/logout.do',
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			if (result.code == 0) {
				output.logoutResult = true;
			} else {
				$.messager.popup(result.msg);
			}
		}).error(function(result, status, headers, config) {
			// output.ajaxE(result);
		});
	};

	/**
	 * @author abliu
	 * @description 获取当前页面权限
	 */
	output.getFun = function(pageUrl) {
		return $http({
			method : 'POST',
			url : 'function/getPageFunctions.do',
			data : "pageUrl=" + pageUrl,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.functions = result.data;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	/**
	 * @author gfwang
	 * @description 获取当前menu
	 */
	output.getMenus = function() {
		return $http({
			method : 'POST',
			url : 'function/getLeftMenus.do',
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			if (result.code == -1) {
				var href = window.location.href;
				// todo
				$.messager.popup("The session timeout!");
				for (var i = 0, m = returnUrlList.length; i < m; i++) {
					if (href.indexOf(returnUrlList[i]) > 0) {
						// 跳转到登录页并添加返回页面
						location.href = "login.html?resultUrl=" + escape(href);
						return;
					}
				}
				// 跳转到登录页
				location.href = "login.html";
				return;
			}
			output.menus = result.data;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	/**
	 * @author abliu
	 * @description 获取当前页面权限
	 */
	output.getNextColor = function(familyId, userType) {
		familyId = !familyId ? "" : familyId;
		return $http({
			method : 'POST',
			url : 'common/getNextColor.do',
			data : "familyId=" + familyId + "&userType=" + userType,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.nextResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.getProfileSelectList = function(type) {
		return $http({
			method : 'POST',
			url : 'profile/getSelectList.do',
			data : "type=" + type,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.profileListResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	
	output.getProfileInfo = function(id,profileId) {
		id=!id?"":id;
		profileId=!profileId?"":profileId;
		return $http({
			method : 'POST',
			url : 'profile/profile.do',
			data : "id=" + id+'&templateId='+profileId,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.profileResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.selectChange= function(id) {
		return $http({
			method : 'POST',
			url : 'profile/info.do',
			data : 'id=' + id,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.selectChangeResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	return output;
}

angular.module('kindKidsApp').factory('commonService', [ '$http', commonService ]);