'use strict';

/**
 * @author abliu
 * @description calendarService Service
 */
function calendarService($http, commonService) {
	// variable=================================================================
	var output = {};

	// select===================================================================
	/**
	 * @author abliu
	 * @description
	 * @returns 获取calendarList
	 */
	output.getCalendarList = function(condition) {
		/* return $http.get("./data/calendar.data.json")*/
		return $http({
			method : 'POST',
			url : 'calendar/getCalendarList.do',
			data : $.param(condition),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.listResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.updateLeaveColor = function(id,color) {
		return $http({
			method : 'POST',
			url : 'calendar/updateLeaveColor.do',
			data : "leaveId="+id+"&color="+color,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.saveColorResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.updateEventColor = function(id,color) {
		return $http({
			method : 'POST',
			url : 'calendar/updateEventColor.do',
			data : "eventId="+id+"&color="+color,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.saveEventColorResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	
	
	

	return output;
}
angular.module('kindKidsApp').factory('calendarService', [ '$http', 'commonService',calendarService]);