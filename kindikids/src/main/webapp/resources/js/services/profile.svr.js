'use strict';

/**
 * @author gfwang
 * @description meeting Service
 */
function profileService($http, commonService) {

	var output = {};

	// 获取日历
	/**
	 * 获取program list 页面的列表内容信息
	 */
	output.getList = function(condition) {
		return $http({
			method : 'POST',
			url : 'profile/list.do',
			data : $.param(condition),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.listResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.saveProfile = function(obj, formObj) {
		formObj = !formObj ? "" : encodeURIComponent(JSON.stringify(formObj));
		return $http({
			method : 'POST',
			url : 'profile/save.do',
			data : $.param(obj)+"&formJson=" + formObj,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.saveResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	output.getProfileInfo = function(id) {
		return $http({
			method : 'POST',
			url : 'profile/info.do',
			data : 'id=' + id,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.profileInfoResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.changeState = function(id, state) {
		return $http({
			method : 'POST',
			url : 'profile/opera.do',
			data : 'id=' + id + '&state=' + state,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.stateResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	return output;
}
angular.module('kindKidsApp').factory('profileService', [ '$http', 'commonService', profileService ]);