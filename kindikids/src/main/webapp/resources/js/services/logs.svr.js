'use strict';

/**
 * @author abliu
 * @description logsService 
 */
function logsService($q,$http, commonService) {
	var output = {};

	/**
	 * @author abliu
	 * @description
	 * @returns 获取LogsList
	 */
	output.getLogsList = function(condition) {
		return $http({
			method : 'POST',
			url : 'logs/getLogsList.do',
			data : "params="+encodeURIComponent(JSON.stringify(condition)),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.listResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	return output;
}
angular.module('kindKidsApp').factory('logsService', [ '$q', '$http', 'commonService',logsService]);