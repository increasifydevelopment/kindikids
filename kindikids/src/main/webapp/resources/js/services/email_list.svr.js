'use strict';
function emailListSvr($q, $http, commonService) {
    var output = {};
    output.getList = function (condition) {
        return $http({
            method: 'POST',
            url: 'emailList/list.do',
            data: 'params=' + encodeURIComponent(JSON.stringify(condition)),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).success(function (result) {
            output.pageListResult = result;
        }).error(function (data, status, headers, config) {
            commonService.ajaxE(data);
        });
    }

    output.sendPlanEmail = function (condition) {
        return $http({
            method: 'POST',
            url: 'emailList/sendPlanEmail.do',
            data: 'params=' + encodeURIComponent(JSON.stringify(condition)),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).success(function (result) {
            output.sendPlanEmailResult = result;
        }).error(function (data, status, headers, config) {
            commonService.ajaxE(data);
        });
    }

    return output;
}
angular.module('kindKidsApp').factory('emailListSvr', ['$q', '$http', 'commonService', emailListSvr]);