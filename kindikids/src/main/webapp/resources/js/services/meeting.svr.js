'use strict';

/**
 * @author gfwang
 * @description meeting Service
 */
function meetingService($http, commonService) {

	var output = {};

	/** *********************************************manager开始********************************** */

	// 获取日历
	/**
	 * 获取program list 页面的列表内容信息
	 */
	output.getCalendarList = function(params) {
		return $http({
			method : 'POST',
			url : 'task/getCalendar.do',
			data : 'params=' + encodeURIComponent(JSON.stringify(params)),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.calendarResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.getManagerList = function(condition) {
		return $http({
			method : 'POST',
			url : 'meetManager/getMeetingManaList.do',
			data : $.param(condition),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.managerListResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.createMeeting = function(obj) {
		return $http({
			method : 'POST',
			url : 'meetManager/createMeeting.do',
			data:'params=' + encodeURIComponent(JSON.stringify(obj)),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.createMeetingResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	output.updateMeeting = function(obj,isSure) {
		console.log(obj);
		return $http({
			method : 'POST',
			url : 'meetManager/saveMeetingMana.do',
			data : 'params='+encodeURIComponent(JSON.stringify(obj))+'&confirm='+isSure,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.updateMeetingResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.deleteMeeting = function(meetId) {
		return $http({
			method : 'POST',
			url : 'meetManager/deleteMeeting.do' + '?meetingId=' + meetId,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.deleteResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.getSelectList = function() {
		return $http({
			method : 'POST',
			url : 'meetManager/getSelectList.do',
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.selectListResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.getTableInfo = function(meetingId, templateId) {
		return $http({
			method : 'POST',
			url : 'meetManager/getMeetingManaInfo.do?id=' + meetingId + '&tempId=' + templateId,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.tableResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	/** *********************************************manager结束********************************** */

	/** *********************************************agenda开始********************************** */
	// 获取纪要集合
	output.getAgeList = function(condition) {
		return $http({
			method : 'POST',
			url : 'meeting/getMeetingAgendaList.do',
			data : $.param(condition),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.ageListResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	// 保存
	output.saveAgenadeInfo = function(obj) {
		return $http({
			method : 'POST',
			url : 'meeting/addMeetingAgenda.do',
			data : 'params=' + encodeURIComponent(JSON.stringify(obj)),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.saveAgeResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	// 获取一个
	output.getAgenadeInfo = function(id) {
		return $http({
			method : 'POST',
			url : 'meeting/getMeetingAgenda.do',
			data : 'id=' + id,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.singleAgeInfoResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	// 修改状态
	output.changeAgendeState = function(templateId, state) {
		return $http({
			method : 'POST',
			url : 'meeting/updateMeetingAgendaState.do?id=' + templateId + '&agendaState=' + state,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.ageStateResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	// 获取分类集合
	output.getTypeList = function() {
		return $http({
			method : 'POST',
			url : 'meeting/getAgendaType.do',
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.typeList = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	// 更新分类集合
	output.updateTypeList = function(list) {
		return $http({
			method : 'POST',
			url : 'meeting/updateAgendaType.do',
			data : 'params=' + encodeURIComponent(JSON.stringify(list)),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.updateTypeResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	/** *********************************************agenda结束********************************** */

	/** *********************************************template开始********************************** */

	// 获取模板list
	output.getTemplateList = function(condition) {
		return $http({
			method : 'POST',
			url : 'meeting/getMeetingTempList.do',
			data : $.param(condition),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.templateListResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	// 保存模板
	output.saveTemplate = function(obj) {
		return $http({
			method : 'POST',
			url : 'meeting/saveMeetingTemp.do',
			data : 'params=' + encodeURIComponent(JSON.stringify(obj)),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.saveTempalteResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	// 获取模板信息
	output.getTemplateInfo = function(templateId) {
		console.log();
		return $http({
			method : 'POST',
			url : 'meeting/getMeetingTempInfo.do?id=' + templateId,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.getTempResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	// 获取模板信息
	output.changeTemplateState = function(templateId, state) {
		return $http({
			method : 'POST',
			url : 'meeting/operaMeetingTemp.do?id=' + templateId + '&state=' + state,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.tempStaeResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	/** *********************************************agenda结束********************************** */

	return output;
}
angular.module('kindKidsApp').factory('meetingService', [ '$http', 'commonService', meetingService ]);