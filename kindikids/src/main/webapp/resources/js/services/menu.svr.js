'use strict';

function menuService($http, commonService) {

	var output = {};

	
	output.addMenu = function(menuInfo) {
		return $http({
			method : 'POST',
			url : 'menu/addMenu.do',
			data : 'params=' + encodeURIComponent(JSON.stringify(menuInfo)),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.addResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	

	output.getMenuList = function(condition) {
		return $http({
			method : 'POST',
			url : 'menu/getMenuList.do',
			data : 'params=' + encodeURIComponent(JSON.stringify(condition)),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.listResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	
	output.getMenuInfo = function(menuId) {
		return $http({
			method : 'POST',
			url : 'menu/getMenuInfo.do',
			data : 'menuId=' + menuId,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.result = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	
	output.saveMenuInfo = function(weekMenuVo) {
		return $http({
			method : 'POST',
			url : 'menu/saveMenuInfo.do',
			data : 'params=' + encodeURIComponent(JSON.stringify(weekMenuVo)),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.result = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	
	output.deleteMenu = function(menuId) {
		return $http({
			method : 'POST',
			url : 'menu/deleteMenu.do',
			data : 'menuId=' + menuId,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.result = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.getAllMenus = function() {
		return $http({
			method : 'POST',
			url : 'menu/getSelectMenus.do',
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.result = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.getCurrentMenu = function(roomId,index) {
		return $http({
			method : 'POST',
			url : 'menu/getCurrentMenu.do',
			data : 'roomId=' + roomId + '&index=' + index,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.result = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	
	output.saveMenu = function(roomId,selectMenuId,index) {
		return $http({
			method : 'POST',
			url : 'menu/saveMenu.do',
			data : 'roomId=' + roomId + '&menuId=' + selectMenuId + '&index=' + index,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.result = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};




	return output;
}
angular.module('kindKidsApp').factory('menuService', [ '$http', 'commonService', menuService ]);