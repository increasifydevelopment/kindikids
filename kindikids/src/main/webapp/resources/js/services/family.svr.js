'use strict';

/**
 * @author gfwang
 * @description family Service
 */
function familyService($http, commonService) {

	// variable=================================================================
	var output = {};

	output.getRequestLogObj = function (objId, type) {
		return $http({
			method: 'POST',
			url: 'attendance/getRequestLogObj.do',
			data: "objId=" + objId + "&type=" + type,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.requestLogObj = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	// select===================================================================
	output.getFaimilyList = function (condition) {
		return $http({
			method: 'POST',
			url: 'family/list.do',
			data: "params=" + encodeURIComponent(JSON.stringify(condition)),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.listResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	output.getOrgs = function () {
		return $http({
			method: 'POST',
			url: 'common/getMenuOrgs.do',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.orgs = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	output.saveChild = function (childInfo, familyId, familyName) {
		childInfo.userInfo.familyId = familyId;
		familyName = familyName ? encodeURIComponent(familyName) : "";
		return $http({
			method: 'POST',
			url: 'family/saveChild.do',
			data: 'params=' + encodeURIComponent(JSON.stringify(childInfo)) + "&familyName=" + familyName,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.saveChildResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	output.saveParent = function (parentInfo, familyId, familyName) {
		parentInfo.familyId = familyId;
		familyName = familyName ? familyName : "";
		return $http({
			method: 'POST',
			url: 'family/saveParent.do?familyName=' + familyName,
			data: 'params=' + encodeURIComponent(JSON.stringify(parentInfo)),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.saveParentResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	}

	output.getFamilyInfo = function (familyId) {
		return $http({
			method: 'POST',
			url: 'family/info.do',
			data: 'familyId=' + familyId,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.familys = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	}
	/**
	 * 归档
	 */
	output.archiveChild = function (childInfo) {
		return $http({
			method: 'POST',
			url: 'family/archive.do',
			data: 'params=' + encodeURIComponent(JSON.stringify(childInfo)),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.aresult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	}
	/**
	 * 
	 */
	output.unArchiveChild = function (userId) {
		return $http({
			method: 'GET',
			url: 'family/unArchive.do?userId=' + userId,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.uaresult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	}
	output.deleteChild = function (userId, sureDelete) {
		return $http({
			method: 'GET',
			url: 'family/delete.child.do?userId=' + userId + "&sureDelete=" + sureDelete,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.dcresult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	}
	output.deleteParent = function (userId) {
		return $http({
			method: 'GET',
			url: 'family/delete.parent.do?userId=' + userId,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.dpresult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	}
	output.sendWelEmail = function (userId) {
		userId = !userId ? "" : userId;
		return $http({
			method: 'GET',
			url: 'family/sendEmail.do?userId=' + userId,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.eresult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.getFamilyOrgs = function () {
		return $http({
			method: 'GET',
			url: 'family/familyOrgs.do',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.orgsResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.submitChangeAttendance = function (changeAttendanceRequest, isOverOld, isFullOver) {
		return $http({
			method: 'POST',
			url: 'attendance/submitChangeAttendance.do',
			data: "json=" + JSON.stringify(changeAttendanceRequest) + "&isOverOld=" + isOverOld + "&isFullOver=" + isFullOver,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.changeAttendanceResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.getAttendance = function (userId) {
		return $http({
			method: 'POST',
			url: 'attendance/getAttendance.do',
			data: "userId=" + userId,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.attendanceObj = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	/**
	 * 是否有 request 的 离园申请
	 */
	output.isHaveRequestGivingNotice = function (accountId) {
		return $http({
			method: 'GET',
			url: 'givingNotice/haveGivingNotice.do?id=' + accountId,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.isHaveGivingNoticeResule = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	/**
	 * 是否有 request 的 小孩请假
	 */
	output.isHaveRequestAbsteen = function (accountId) {
		return $http({
			method: 'GET',
			url: 'Absentee/haveAbsentee.do?id=' + accountId,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.isHaveAbsteenResule = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * 从外部到内部
	 */
	output.enrolChildService = function (userId, accountId) {
		return $http({
			method: 'GET',
			url: 'family/enrolChild.do?userId=' + userId + "&accountId=" + accountId,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.enrolResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	// lOG List=============================
	output.getLogList = function (userId) {
		return $http({
			method: 'POST',
			url: 'family/getLogList.do',
			data: "userId=" + userId,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.logResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.saveLog = function (log) {
		if (log.reason == undefined) {
			log.reason = "";
		}
		return $http({
			method: 'POST',
			url: 'family/addLogs.do',
			//			data : "params=" + encodeURIComponent(reason) + "&logDate=" + date + "&id=" + id + "&userId=" + userId + "&file=" + file,
			data: "params=" + encodeURIComponent(JSON.stringify(log)),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.saveResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.deleteLog = function (id) {
		return $http({
			method: 'POST',
			url: 'family/deleteLog.do',
			data: "logId=" + id,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.delResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});

	};

	output.getAgeStage = function () {
		return $http({
			method: 'POST',
			url: 'family/getAllAgeStage.do',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.ageResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.getEylfCheck = function (age, id) {
		return $http({
			method: 'POST',
			url: 'family/getEylf.do',
			data: "ageScope=" + age + '&childId=' + id,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.eylfCheckResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.changeOutAttend = function (flag, userId) {
		return $http({
			method: 'POST',
			url: 'family/attendChange.do',
			data: "userId=" + userId + '&attendFlag=' + flag,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.changeOutAttendResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.getTaskList = function (accountId) {
		return $http({
			method: 'POST',
			url: 'family/taskList.do',
			data: "accountId=" + accountId,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.taskListResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.exportReportsPdf = function (reportVo) {
		return $http({
			method: 'POST',
			url: 'family/reports.do',
			data: "params=" + encodeURIComponent(JSON.stringify(reportVo)),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.reportsResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	}

	output.moveAnotherFamily = function (model) {
		return $http({
			method: 'POST',
			url: 'family/moveFamily.do',
			data: 'params=' + encodeURIComponent(JSON.stringify(model)),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.moveAnotherFamilyResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	}

	output.moveToOutside = function (userId, flag) {
		return $http({
			method: 'POST',
			url: 'family/moveToOutside.do',
			data: 'userId=' + userId + "&flag=" + flag,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.moveToOutsideResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	}

	output.complete = function (childInfo) {
		return $http({
			method: 'POST',
			url: 'family/complete.do',
			data: 'params=' + encodeURIComponent(JSON.stringify(childInfo)),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.completeResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	}

	output.sendEnrolmentEmail = function (id) {
		return $http({
			method: 'POST',
			url: 'family/enrolmentEmail.do',
			data: 'id=' + id,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.sendEnrolmentEmailResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	}

	return output;
}
angular.module('kindKidsApp').factory('familyService', ['$http', 'commonService', familyService]);