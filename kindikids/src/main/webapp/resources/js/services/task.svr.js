'use strict';

/**
 * @author zhengguo
 * @description
 */
function taskService($http, commonService) {

	// variable=================================================================
	var output = {};

	/**
	 * get lesson program 信息
	 */
	output.getLessonInfo = function (id) {
		return $http({
			method: 'POST',
			url: 'program/getProgramLessonInfo.do',
			data: 'lessonId=' + id,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.getLessonResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * obsolete task 禁用task接口
	 */
	output.obsoleteTask = function (id, followUpFlag, date) {
		followUpFlag = !followUpFlag ? '' : followUpFlag;
		return $http({
			method: 'POST',
			url: 'task/obsoleteTask.do',
			data: 'taskInstanceId=' + id + '&followUpFlag=' + followUpFlag + '&day=' + date,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.obsoleteTaskResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * get room center name 信息
	 */
	output.getNameList = function (id) {
		return $http({
			method: 'POST',
			url: 'program/getNameByRoomId.do',
			data: 'roomId=' + id,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.nameResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * save weekly program 信息
	 */
	output.saveWeekly = function (info) {
		return $http({
			method: 'POST',
			url: 'program/saveWeeklyEvaluation.do',
			data: 'params=' + encodeURIComponent(JSON.stringify(info)),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.weeklyResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * del weekly program 信息
	 */
	output.delWeekly = function (id) {
		return $http({
			method: 'POST',
			url: 'program/removeWeeklyEvaluation.do',
			data: 'id=' + id,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.delWeeklyResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * get weekly program 信息
	 */
	output.getWeekly = function (id) {
		return $http({
			method: 'POST',
			url: 'program/getWeeklyEvaluation.do',
			data: 'id=' + id,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.getWeeklyResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * 获取program list 页面的时间日历部分信息
	 */
	output.getProgramList = function (params) {
		return $http({
			method: 'POST',
			url: 'task/getCalendar.do',
			data: 'params=' + encodeURIComponent(JSON.stringify(params)),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.programListResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * 获取program list 页面的列表内容信息
	 */
	output.getProgramListInfo = function (json) {
		return $http({
			method: 'POST',
			url: 'task/getUserTaskList.do',
			data: 'json=' + encodeURIComponent(JSON.stringify(json)),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.programListInfoResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * 获取today notes 列表
	 */
	output.getNoteList = function (json) {
		return $http({
			method: 'POST',
			url: 'task/getTodayNote.do',
			data: 'json=' + encodeURIComponent(JSON.stringify(json)),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.noteListResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * get interest /observation/ learning program 信息
	 */
	output.getInObLeInfo = function (id) {
		return $http({
			method: 'POST',
			url: 'program/getProgramIntobsleaInfo.do',
			data: 'intobsleaId=' + id,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.inObLeResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * get medication program 信息
	 */
	output.getMedication = function (id) {
		if (!id) {
			id = '';
		}
		return $http({
			method: 'POST',
			url: 'program/getProgramMedication.do',
			data: 'id=' + id,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.medicationResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * save medication program 信息
	 */
	output.saveMedication = function (params, date) {
		return $http({
			method: 'POST',
			url: 'program/addProgramMedication.do',
			data: 'date=' + date + '&params=' + encodeURIComponent(JSON.stringify(params)),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.saveMedicationResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * //type-----11：meal / 12:sleep / 13: nappy / 14:tolit / 15 : sunscreen
	 */
	output.getRegisterInfo = function (roomId, id) {
		if (!id) {
			id = '';
		}
		return $http({
			method: 'POST',
			url: 'program/getProgramRegisterTask.do',
			data: 'id=' + id + '&roomId=' + roomId,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.inRegisterResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * save //type-----11：meal / 12:sleep / 13: nappy / 14:tolit / 15 :
	 * sunscreen
	 */
	output.saveRegisterInfo = function (params, isSubmit) {
		params.isSubmit = isSubmit;
		return $http({
			method: 'POST',
			url: 'program/addProgramRegister.do',
			data: 'params=' + encodeURIComponent(JSON.stringify(params)),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.saveRegisterResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.updateNappyToiletTime = function (params) {
		return $http({
			method: 'POST',
			url: 'program/updateNappyToiletTime.do',
			data: 'params=' + encodeURIComponent(JSON.stringify(params)),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.updateResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * add and update lesson program 信息
	 */
	output.updateLessonInfo = function (condition, date) {
		return $http({
			method: 'POST',
			url: 'program/addProgramLesson.do',
			data: 'date=' + date + '&params=' + encodeURIComponent(JSON.stringify(condition)),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.lessonResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * get 空对象 0 : explore / 1 : grow / 3 : school readiness program need roomId
	 */
	output.getTaskExGrSc = function (type, id, roomId) {
		if (!id) {
			id = '';
		}
		var onlineUrl = '';
		if (type == 0) {
			onlineUrl = 'program/getProgramTaskExplore.do';
		} else if (type == 1) {
			onlineUrl = 'program/getProgramTaskGrow.do';
		} else {
			onlineUrl = 'program/getProgramTaskSchRea.do';
		}
		return $http({
			method: 'POST',
			url: onlineUrl,
			data: 'id=' + id + '&roomId=' + roomId,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.taskExGrScResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * add and update 0 : explore / 1 : grow / 3 : school readiness program need
	 * roomId
	 */
	output.saveTaskExGrSc = function (type, params) {
		var onlineUrl = '';
		if (type == 0) {
			onlineUrl = 'program/saveProgramTaskExplore.do';
		} else if (type == 1) {
			onlineUrl = 'program/saveProgramTaskGrow.do';
		} else {
			onlineUrl = 'program/saveProgramTaskSchRea.do';
		}
		return $http({
			method: 'POST',
			url: onlineUrl,
			data: 'params=' + encodeURIComponent(JSON.stringify(params)),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.saveExGrScResult = result;
		}).error(function (data, status, headers, config) {
			output.saveExGrScResult = undefined;
			commonService.ajaxE(data);
		});
	};

	/**
	 * add and update // type 4 : interest / 5 : observation / 6 : learningStory
	 * 对应后台 0；1；2 全部存一张表 need roomId
	 */
	output.saveInObLeInfo = function (params, date, confirm) {
		return $http({
			method: 'POST',
			url: 'program/addProgramIntobslea.do',
			data: 'date=' + date + '&params=' + encodeURIComponent(JSON.stringify(params)) + '&confirm=' + confirm,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.inObLeInfoResult = result;
		}).error(function (data, status, headers, config) {
			output.inObLeInfoResult = undefined;
			commonService.ajaxE(data);
		});
	};

	/**
	 * add and update // type 7 : interest / 8 : observation / 9 : learningStory
	 * follow up 全部存一张表 need intobsleaId
	 */
	output.getInObLeFollowInfo = function (id) {
		return $http({
			method: 'POST',
			url: 'program/getfollowupInfo.do',
			data: 'id=' + id,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.inObLeInfoFollowResult = result;
		}).error(function (data, status, headers, config) {
			output.inObLeInfoFollowResult = undefined;
			commonService.ajaxE(data);
		});
	};

	/**
	 * save // type 7 : interest / 8 : observation / 9 : learningStory follow up
	 * 全部存一张表 need intobsleaId
	 */
	output.saveInObLeFollowInfo = function (params) {
		return $http({
			method: 'POST',
			url: 'program/saveFollowupInfo.do',
			data: 'params=' + encodeURIComponent(JSON.stringify(params)),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.saveObLeInfoFollowResult = result;
		}).error(function (data, status, headers, config) {
			output.saveObLeInfoFollowResult = undefined;
			commonService.ajaxE(data);
		});
	};

	/**
	 * 全部存一张表 save need params 页面数据对象
	 */
	output.updateFollowUpInfo = function (params) {
		return $http({
			method: 'POST',
			url: 'program/saveFollowupInfo.do',
			data: 'params=' + encodeURIComponent(JSON.stringify(params)),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.followUpResult = result;
		}).error(function (data, status, headers, config) {
			output.followUpResult = undefined;
			commonService.ajaxE(data);
		});
	};

	/**
	 * del type 4 : interest / 5 : observation / 6 : learningStory modal 信息
	 */
	output.delInObLeInfo = function (id) {
		return $http({
			method: 'POST',
			url: 'program/deleteProgramIntobslea.do',
			data: 'intobsleaId=' + id,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.delInObLeResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.hasFollowUp = function (id) {
		return $http({
			method: 'POST',
			url: 'program/hasFollowUp.do',
			data: 'intobsleaId=' + id,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.hasResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * del lesson modal 信息
	 */
	output.delLessonInfo = function (id) {
		return $http({
			method: 'POST',
			url: 'program/deleteProgramLesson.do',
			data: 'lessonId=' + id,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.delLessonResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * get interest /observation/ learning program 信息
	 */
	output.shareNewsfeed = function (nqsInfo, objId) {
		return $http({
			method: 'POST',
			url: 'program/shareNewsfeed.do',
			data: 'objId=' + objId + "&params=" + encodeURIComponent(JSON.stringify(nqsInfo)),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.shareResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	/**
	 * **************************************add by gfwang
	 * start****************************
	 */
	/**
	 * 获取task示例
	 */
	output.getFormInstance = function (taskInstanceId) {
		return $http({
			method: 'POST',
			url: 'task/getCustmerTaskInstance.do',
			data: 'taskInstanceId=' + taskInstanceId,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.formList = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	/**
	 * 
	 */
	output.getWhenRequiredList = function (taskInstanceId) {
		return $http({
			method: 'GET',
			url: 'task/getWhenRequiredTasks.do',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.whenTaskList = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	output.getWhenRequiredList2 = function (taskInstanceId) {
		return $http({
			method: 'GET',
			url: 'task/getWhenRequiredTasks2.do',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.whenTaskList = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	output.getWhenSelect = function (taskId) {
		return $http({
			method: 'GET',
			url: 'task/getWhenRequiredSelects.do?taskId=' + taskId,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.whenSelects = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	output.saveWhenTask = function (taskId, obj, day) {
		return $http({
			method: 'POST',
			url: 'task/saveWhenRequiredTask.do?taskId=' + taskId + '&day=' + day,
			data: "json=" + encodeURIComponent(JSON.stringify(obj)),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.saveWhenResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	output.saveTaskFormValue = function (taskId, type, statu, json) {
		json = !json ? '' : encodeURIComponent(JSON.stringify(json));
		return $http({
			method: 'POST',
			url: 'task/saveTaskFormValue.do',
			data: 'id=' + taskId + '&type=' + type + '&statu=' + statu + '&json=' + json,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.saveTaskFormValueResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.getMeetingRow = function (rowId) {
		return $http({
			method: 'POST',
			url: 'meetManager/getRowInfo.do?id=' + rowId,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.meetingRowResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	output.getTaskCount = function (json) {
		return $http({
			method: 'POST',
			url: 'task/getTaskCount.do',
			data: 'json=' + encodeURIComponent(JSON.stringify(json)),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.taskCountResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	output.getPageList = function (params) {
		return $http({
			method: 'POST',
			url: 'task/taskTableView.do',
			data: 'json=' + encodeURIComponent(JSON.stringify(params)),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.taskTableResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	output.exportData = function (params) {
		return $http({
			method: 'POST',
			url: 'task/export.do',
			data: 'params=' + encodeURIComponent(JSON.stringify(params)),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.exportDataResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	/**
	 * **************************************add by gfwang
	 * end****************************
	 */
	return output;
}
angular.module('kindKidsApp').factory('taskService', ['$http', 'commonService', taskService]);