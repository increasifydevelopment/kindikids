'use strict';

/**
 * @author jfzhao
 * @description Form view service
 */
function formService($http, Upload) {
	var output = {};

	output.getForm = function(code) {
		output.getFormResult = {
			"id" : "",
			"name" : "Form Name",
			"attributes" : []
		};
		return $http({
			method : 'GET',
			url : './form/getFrom.do?code=' + code,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(data) {
			if (!data.code) {
				output.getFormResult = data.data;
				if (output.getFormResult.attributes != undefined) {
					output.getFormResult.attributes.forEach(function(e) {
						e.json = JSON.parse(e.json);
					});
				}
			}
		}).error(function(data, status, headers, config) {
		});
	};

	output.saveForm = function(formInfo) {
		output.saveFormResult = undefined;
		return $http({
			method : 'POST',
			url : './form/saveFrom.do',
			data : "params=" + encodeURIComponent(JSON.stringify(formInfo)),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(data) {
			if (!data.code) {
				output.saveFormResult = data.msg;
			} else {
				alert("Error");
			}
		}).error(function(data, status, headers, config) {
		});
	};

	output.initForm = function(code, instanceId) {
		output.initFormResult = [];
		var paramas = 'code=' + code;
		if (instanceId != 0) {
			paramas += '&instanceId=' + instanceId;
		}
		return $http({
			method : 'GET',
			url : './form/getInstanceTemplate.do?' + paramas,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(data) {
			if (!data.code) {
				output.initFormResult = data.data;
			}
		}).error(function(data, status, headers, config) {
		});
	};

	output.getInstance = function(instanceId) {
		output.instanceResult = {};
		return $http({
			method : 'GET',
			url : './form/getInstance.do?instanceId=' + instanceId,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(data) {
			if (!data.code) {
				output.instanceResult = data.data;
			}
		}).error(function(data, status, headers, config) {
		});
	};

	output.saveInstance = function(valueInfo) {
		output.saveInstanceResult = undefined;
		return $http({
			method : 'POST',
			url : './form/saveInstance.do',
			data : "params=" + encodeURIComponent(JSON.stringify(valueInfo)),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(data) {
			if (!data.code) {
				output.saveInstanceResult = data.msg;
			}
		}).error(function(data, status, headers, config) {
		});
	}
	// 这个接口只处理获取全部数据,没有业务逻辑处理
	output.getOptions = function(url, json, attributes, selectvalue, ismultiple, flag) {
		if (flag) {
			url = url + "&flag=" + flag;
		}
		output.optionsResult = [];
		return $http({
			method : 'GET',
			url : url,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(data) {
			if (!data.code) {
				output.optionsResult = data.data;
				console.log('json.options--start' + new Date().getTime())
				if (json != undefined) {
					json.options = data.data;
					output.getNewOptions(attributes, selectvalue, ismultiple, flag);
				}
				console.log('json.options--end' + new Date().getTime())
			}
		}).error(function(data, status, headers, config) {
		});
	};

	// 处理option与选中值
	output.getNewOptions = function(attributes, selectvalue, ismultiple) {
		var options = attributes.json.options;
		if (selectvalue != null && selectvalue != undefined) {
			for (var j = 0, n = selectvalue.length; j < n; j++) {
				if (attributes.attrId != selectvalue[j].formAttrId) {
					continue;
				}
				if (ismultiple == 0) {
					attributes.value = selectvalue[j].value;
				} else {
					if (attributes.value == undefined) {
						attributes.value = [];
					}
					// 填充value
					attributes.value.push(selectvalue[j].value);
				}
				var k = 0, m = options.length;
				for (k = 0; k < m; k++) {
					// 判断选中的值是否在集合中
					if (options[k].id == selectvalue[j].value) {
						options[k].valid = true;
						break;
					}
				}
				if (k >= m) {
					// 说明不存在
					var option = {};
					option.id = selectvalue[j].value;
					option.text = selectvalue[j].name;
					option.disabled = true;
					option.valid = true;
					options.push(option);
				}
			}
		} // 去除是disabled 未true并且是valid为false的
		var k = options.length - 1;
		for (; k >= 0; k--) {
			if (options[k].disabled && (options[k].valid == undefined || !options[k].valid)) {
				options.splice(k, 1);
			}
		}
	};

	output.getFiles = function(attr) {
		if (attr.value == undefined) {
			return;
		}
		return $http({
			method : 'POST',
			url : 'common/getFileList.do',
			data : 'sourceId=' + attr.value,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(data) {
			if (!data.code) {
				attr.json.files = data.data;
			}
		}).error(function(data, status, headers, config) {
		});
	};

	output.upload = function(file, sourceId, callback, attr) {
		output.fileResult = undefined;
		var params = "";
		if (sourceId != undefined) {
			params = '?sourceId=' + sourceId
		}
		return Upload.upload({
			url : 'common/uploadBatch.do' + params,
			file : file,
		}).progress(function(evt) {
			var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
			callback(attr, progressPercentage);
		}).success(function(data, status, headers, config) {
			if (!data.code) {
				output.fileResult = data.data;
			}
		}).error(function(data, status, headers, config) {
		});
	}

	return output;
};

angular.module('kindKidsApp').factory('formService', [ '$http', 'Upload', formService ]);