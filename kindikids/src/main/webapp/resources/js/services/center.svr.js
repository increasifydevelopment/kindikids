'use strict';

/**
 * @author abliu
 * @description message Service
 */
function centerService($http, commonService) {

	var output = {};

	/**
	 * @author gfwang
	 * @description 获取message列表
	 */
	output.getCenterList = function(condition) {
		return $http({
			method : 'POST',
			url : 'center/list.do',
			data : $.param(condition),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.listResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * @author gfwang
	 * @description 获取园区信息
	 */
	output.getCenterInfo = function(centerId) {
		return $http({
			method : 'POST',
			url : 'center/info.do?centerId=' + centerId,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.singleResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	/**
	 * @author gfwang
	 * @description 获取分组信息
	 */
	output.getGroupInfo = function(groupId) {
		return $http({
			method : 'POST',
			url : 'center/getGroupInfo.do?groupId=' + groupId,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.gResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	/**
	 * @author gfwang
	 * @description 获取menu
	 */
	output.getCenterMenu = function() {
		return $http({
			method : 'POST',
			url : 'center/getCenterMenu.do',
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.menuResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	/**
	 * @author gfwang
	 * @description 归档园区
	 */
	output.archiveCenter = function(centerId, flag) {
		return $http({
			method : 'POST',
			url : 'center/archiveCenter.do?centerId=' + centerId + "&flag=" + flag,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.acResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	/**
	 * @author gfwang
	 * @description 归档room
	 */
	output.archiveRoom = function(roomId, flag) {
		return $http({
			method : 'POST',
			url : 'center/archiveRoom.do?roomId=' + roomId + "&flag=" + flag,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.arResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * @author gfwang
	 * @description 归档分组
	 */
	output.archiveGroup = function(groupId, flag) {
		return $http({
			method : 'POST',
			url : 'center/archiveGroup.do?groupId=' + groupId + "&flag=" + flag,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.agResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * 新增center
	 */
	output.addCenter = function(centerInfo) {
		return $http({
			method : 'POST',
			url : 'center/addCenter.do',
			data : 'params=' + encodeURIComponent(JSON.stringify(centerInfo)),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.addResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	/**
	 * 新增room
	 */
	output.editRoom = function(roomInfo) {
		return $http({
			method : 'POST',
			url : 'center/addRoom.do',
			data : 'params=' + encodeURIComponent(JSON.stringify(roomInfo)),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.addRoomResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	/**
	 * 新增group
	 */
	output.editGroup = function(groupInfo) {
		return $http({
			method : 'POST',
			url : 'center/addGroup.do',
			data : $.param(groupInfo),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.addGroupResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	/**
	 * 新增group
	 */
	output.getNutMenu = function(weekNum, centerId) {
		return $http({
			method : 'POST',
			url : 'center/getNutMenu.do?weekNum=' + weekNum + "&centerId=" + centerId,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.nutMenuResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	/**
	 * cai dan
	 */
	output.updateMenu = function(menuList) {
		return $http({
			method : 'POST',
			url : 'center/addWeekMenu.do',
			data : 'params=' + encodeURIComponent(JSON.stringify(menuList)),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.updateMenuResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.chooseCheck = function(id, checkFlag) {
		return $http({
			method : 'POST',
			url : 'center/check.do',
			data : 'id=' + id + '&check=' + checkFlag,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		});
	};
	return output;
}
angular.module('kindKidsApp').factory('centerService', [ '$http', 'commonService', centerService ]);