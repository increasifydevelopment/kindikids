'use strict';

/**
 * @author abliu
 * @description Nqs Service
 */
function nqsService($http, commonService) {

	// variable=================================================================
	var output = {};

	/**
	 * @author abliu
	 * @description 获取Nqs列表
	 * @returns
	 */
	output.getNqsList = function(date) {
		date=!date?"":date;
		return $http({
			method : 'POST',
			url : './nqs/list.do?dateStr='+date,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.data = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	return output;
}
angular.module('kindKidsApp').factory('nqsService', [ '$http', 'commonService', nqsService ]);