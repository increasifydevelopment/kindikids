'use strict';
function attendanceManagementSvr($q, $http, commonService) {
	var output = {};

	output.getPageList = function (condition) {
		return $http({
			method: 'POST',
			url: 'attendance/getPagarList.do',
			data: 'params=' + encodeURIComponent(JSON.stringify(condition)),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.pageListResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	output.getPageListExternal = function (condition) {
		return $http({
			method: 'POST',
			url: 'attendance/getExternalList.do',
			data: 'params=' + encodeURIComponent(JSON.stringify(condition)),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.pageListExternalResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	return output;
}
angular.module('kindKidsApp').factory('attendanceManagementSvr', ['$q', '$http', 'commonService', attendanceManagementSvr]);