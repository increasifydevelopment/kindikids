'use strict';

/**
 * @author jfxu
 * @description
 */
function staffService($http, commonService) {

	// variable=================================================================
	var output = {};

	/**
	 * 获取staff列表
	 */
	output.getStaffList = function (condition) {
		return $http({
			method: 'POST',
			url: 'staff/staffList.do',
			data: "params=" + encodeURIComponent(JSON.stringify(condition)),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.listResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * 获取员工详细信息
	 */
	output.getStaffInfo = function (uid) {
		return $http({
			method: 'POST',
			url: 'staff/getStaffInfo.do',
			data: 'userId=' + uid,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.staffResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * 更新员工详细信息
	 */
	output.updateStaff = function (condition) {
		return $http({
			method: 'POST',
			url: 'staff/updateStaff.do',
			data: "params=" + encodeURIComponent(JSON.stringify(condition)),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.updateResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.getRoleList = function () {
		return $http({
			method: 'POST',
			url: 'staff/getRoleList.do',
			data: null,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.roleResult = result.data;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.deleteStaff = function (uid) {
		return $http({
			method: 'POST',
			url: 'staff/deleteStaff.do',
			data: 'userId=' + uid,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.deleteResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.archiveStaff = function (uid, type) {
		return $http({
			method: 'POST',
			url: 'staff/archiveStaff.do',
			data: 'userId=' + uid + '&type=' + type,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.archiveResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	}

	output.sendWelEmail = function (uid, accountId) {
		return $http({
			method: 'POST',
			url: 'staff/sendWelEmail.do',
			data: 'userId=' + uid + '&accountId=' + accountId,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.sendResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	output.getOrgs = function () {
		return $http({
			method: 'GET',
			url: 'family/familyOrgs.do',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.orgsResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.agree = function () {
		return $http({
			method: 'POST',
			url: 'account/agree.do',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.argeeResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.saveEmploy = function (employ) {
		return $http({
			method: 'POST',
			url: 'staff/saveEmploy.do',
			data: "params=" + encodeURIComponent(JSON.stringify(employ)),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.saveEmployResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	}

	output.cancelEmploy = function (id) {
		return $http({
			method: 'POST',
			url: 'staff/cancelEmploy.do',
			data: "id=" + id,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.cancelEmployResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	}

	return output;
}
angular.module('kindKidsApp').factory('staffService', ['$http', 'commonService', staffService]);