'use strict';

/**
 * @author xdwang
 * @description File Service
 */
function fileService($http, commonService, Upload) {
	var output = {};

	/**
	 * @author xdwang
	 * @description 上传头像
	 */
	output.uploadAvatar = function(file) {
		output.file = undefined;
		return Upload.upload({
			url : 'account/uploadAvatar.do',
			file : file
		}).progress(function(evt) {
			var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
		}).success(function(result, status, headers, config) {
			if (result.code) {
				output.file = result.data;
			} else {
				$.messager.popup(result.msg);
			}
		}).error(function(data, status, headers, config) {
			console.log('error status: ' + status);
			commonService.ajaxE(data);
		});
	};

	/**
	 * @author xdwang
	 * @description 上传附件
	 */
	output.uploadFile = function(file) {
		return Upload.upload({
			url : 'common/upload.annex.do',
			file : file
		}).progress(function(evt) {
			var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
		}).success(function(result, status, headers, config) {
			output.fdata = result;
		}).error(function(data, status, headers, config) {
			console.log('error status: ' + status);
			commonService.ajaxE(data);
		});
	};

	/**
	 * @author abliu
	 * @description 上传附件
	 */
	output.uploadFileProgress = function(file,index,callback,fileList) {
		return Upload.upload({
			url : 'common/upload.annex.do',
			file : file
		}).progress(function(evt) {
			var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
			callback(fileList,index,progressPercentage);
		}).success(function(result, status, headers, config) {
			output.fdata = result;
		}).error(function(data, status, headers, config) {
			console.log('error status: ' + status);
			commonService.ajaxE(data);
		});
	};

	/**
	 * @author abliu
	 * @description 上传附件
	 */
	output.uploadResizeImage=function(fileType,fileStrBase64,fileName){           
			return $http({
				method : 'POST',
				url : 'common/upload.base64file.do',
				data :"&fileStrBase64="+encodeURIComponent(fileStrBase64)+"&fileName="+fileName,
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				}
			}).success(function(result) {
				output.fdata = result;
			}).error(function(data, status, headers, config) {
				commonService.ajaxE(data);
			});
        };

	return output;
}

angular.module('kindKidsApp').factory('fileService', [ '$http', 'commonService', 'Upload', fileService ]);