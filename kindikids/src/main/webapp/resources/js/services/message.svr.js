'use strict';

/**
 * @author abliu
 * @description message Service
 */
function messageService($http, commonService) {

	// variable=================================================================
	var output = {};

	// select===================================================================

	/**
	 * @author abliu
	 * @description 获取message列表
	 * @returns page对象，包含PCC集合和分页信息
	 */
	output.getMessageList = function(condition) {
		return $http({
			method : 'POST',
			url : 'message/list.do',
			data : "params=" + JSON.stringify(condition),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.listResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * @author abliu
	 * @description 获取message信息
	 * @returns date对象,包含message对象及信息，
	 */
	output.getMessageInfo = function(id) {
		return $http({
			method : 'POST',
			url : 'message/info.do',
			data : "id=" + id,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.singleResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * @author abliu
	 * @description 获取message信息
	 * @returns date对象,包含message对象及信息，
	 */
	output.getUnReadCount = function(id) {
		return $http({
			method : 'GET',
			url : 'message/unread.do',
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.count = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	
	/**
	 * 
	 */
	output.getSessionTimeOut = function(id) {
		return $http({
			method : 'GET',
			url : 'message/sessionTimeOut.do',
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.count = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	
	// edit=====================================================================

	/**
	 * @author abliu
	 * @description
	 * @returns date对象,包含提示信息，
	 */
	output.saveMessageInfo = function(message) {
		return $http({
			method : 'POST',
			url : 'message/send.do',
			data : 'params=' + encodeURIComponent(JSON.stringify(message)),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.data = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	/**
	 * @author abliu
	 * @description
	 * @returns date对象,包含提示信息，
	 */
	output.forward = function(accountIds,msgId) {
		var obj={}
		accountIds= !accountIds ?null:accountIds;
		return $http({
			method : 'POST',
			url : 'message/forward.do',
			data : 'msgId=' + msgId+"&accountIdsJson="+JSON.stringify(accountIds),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.forwardData = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	// private==================================================================

	return output;
}
angular.module('kindKidsApp').factory('messageService', [ '$http', 'commonService', messageService ]);