'use strict';

/**
 * @author gfwang
 * @description 小孩离园
 */
function givingNoticeService($q, $http, commonService) {
	// variable=================================================================
	var output = {};

	// select===================================================================
	/**
	 * apply giving notice
	 */
	output.applyGivingNotice = function(givingNoticeInfo) {
		return $http({
			method : 'POST',
			url : 'givingNotice/saveGivingNoticeInfo.do',
			data : 'params=' + encodeURIComponent(JSON.stringify(givingNoticeInfo)),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.applyResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.getGivingInfo = function(givingId) {
		return $http({
			method : 'GET',
			url : 'givingNotice/getGivingNoticeInfo.do?id=' + givingId,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.givingInfoResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	output.cancelGivingNotice = function(givingId) {
		return $http({
			method : 'GET',
			url : 'givingNotice/cancelGivingNotice.do?id=' + givingId,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.cancleResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	output.declineGivingNotice = function(givingId) {
		return $http({
			method : 'GET',
			url : 'givingNotice/rejectGivingNotice.do?id=' + givingId,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.declineResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	return output;
}
angular.module('kindKidsApp').factory('givingNoticeService', [ '$q', '$http', 'commonService', givingNoticeService ]);