'use strict';

/**
 * @author abliu
 * @description NewsFeed Service
 */
function newsFeedService($q, $http, commonService) {
	// variable=================================================================
	var output = {};

	// select===================================================================
	/**
	 * @author abliu
	 * @description
	 * @returns 获取newsfeedList
	 */
	output.getNewsFeedList = function (condition) {
		return $http({
			method: 'POST',
			url: 'news/list.do',
			data: "params=" + encodeURIComponent(JSON.stringify(condition)),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.listResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * @author abliu
	 * @description
	 * @returns 获取newsfeedinfo
	 */
	output.getNewsFeedInfo = function (newsFeedId) {
		return $http({
			method: 'POST',
			url: 'news/info.do',
			data: "id=" + encodeURIComponent(newsFeedId),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.getNewsfeedInfoResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	// edit=====================================================================

	/**
	 * @author abliu
	 * @description
	 * @returns date对象,包含提示信息，
	 */
	output.saveNewsFeedInfo = function (newsFeedInfo) {
		return $http({
			method: 'POST',
			url: 'news/send.do',
			data: "params=" + encodeURIComponent(JSON.stringify(newsFeedInfo)),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.saveNewsFeedData = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};


    /**
	 * @author abliu
	 * @description
	 * @returns deleteNews，
	 */
	output.deleteNews = function (id) {
		return $http({
			method: 'POST',
			url: 'news/delete.news.do',
			data: "id=" + id,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.deleteNewsResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};


	/**
	 * @author abliu
	 * @description
	 * @returns applove NewsFeed，
	 */
	output.approvalNewsFeed = function (id) {
		return $http({
			method: 'POST',
			url: 'news/approve.do',
			data: "id=" + id,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.approvalResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * @author abliu
	 * @description
	 * @returns unApprove NewsFeed，
	 */
	output.unApproveNewsFeed = function (id) {
		return $http({
			method: 'POST',
			url: 'news/unApprove.do',
			data: "id=" + id,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.unApprovalResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * @author abliu
	 * @description
	 * @returns scoreNewsFeed，
	 */
	output.scoreNewsFeed = function (id, score) {
		return $http({
			method: 'POST',
			url: 'news/score.do',
			data: "id=" + id + "&score=" + score,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.scorelResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};



	/**
	  * @author abliu
	  * @description sendComment
	  */
	output.sendComment = function (commentInfo) {
		return $http({
			method: 'POST',
			url: 'news/comment.do',
			data: "params=" + encodeURIComponent(JSON.stringify(commentInfo)),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.sendCommentResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

    /**
       * @author abliu
       * @description sendComment
       */
	output.deleteComment = function (id) {
		return $http({
			method: 'POST',
			url: 'news/delete.commont.do',
			data: "id=" + id,
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.deleteCommentResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.getNewFeedback = function (condition) {
		return $http({
			method: 'POST',
			url: 'news/feedback.do',
			data: "params=" + encodeURIComponent(JSON.stringify(condition)),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			}
		}).success(function (result) {
			output.newFeedbackResult = result;
		}).error(function (data, status, headers, config) {
			commonService.ajaxE(data);
		});
	}

	return output;
}
angular.module('kindKidsApp').factory('newsFeedService', ['$q', '$http', 'commonService', newsFeedService]);