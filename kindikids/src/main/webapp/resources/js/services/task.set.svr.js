'use strict';

/**
 * @author gfwang
 * @description
 */
function taskSettingService($http, commonService) {

	// variable=================================================================
	var output = {};

	/**
	 * 获取task列表
	 */
	output.getTaskList = function(condition) {
		return $http({
			method : 'POST',
			url : 'task/getTaskList.do',
			data : $.param(condition),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.listResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * 启用或者禁用
	 */
	output.disableEnable = function(taskId, statu) {
		return $http({
			method : 'POST',
			url : 'task/updateStatu.do?taskId=' + taskId + "&statu=" + statu,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.changeResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * save
	 */
	output.saveTask = function(taskInfo) {
		return $http({
			method : 'POST',
			url : 'task/saveTask.do',
			data : "json=" + encodeURIComponent(JSON.stringify(taskInfo)),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.saveResult = result;
		}).error(function(data, status, headers, config) {
			output.saveResult = undefined;
			commonService.ajaxE(data);
		});
	};

	/**
	 * save
	 */
	output.getTask = function(taskId) {
		return $http({
			method : 'POST',
			url : 'task/getTask.do',
			data : "taskId=" + taskId,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.taskResult = result;
		}).error(function(data, status, headers, config) {
			output.saveResult = undefined;
			commonService.ajaxE(data);
		});
	};
	return output;
}
angular.module('kindKidsApp').factory('taskSettingService', [ '$http', 'commonService', taskSettingService ]);