'use strict';

/**
 * @author gfwang
 * @description
 */
function dashboardService($http, commonService) {

	// variable=================================================================
	var output = {};

	/**
	 * 获取task列表
	 */
	output.getNqsList = function(condition) {
		return $http({
			method : 'POST',
			url : 'dashboard/getNqsList.do',
			data : 'params=' + encodeURIComponent(JSON.stringify(condition)),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.nqsListResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.getTaskCategories = function(condition) {
		return $http({
			method : 'POST',
			url : 'dashboard/getTaskCategories.do',
			data : 'params=' + encodeURIComponent(JSON.stringify(condition)),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.taskCategoriesResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	output.getQipList = function(condition) {
		return $http({
			method : 'POST',
			url : 'dashboard/getQipList.do',
			data : 'params=' + encodeURIComponent(JSON.stringify(condition)),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.qipListResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	output.getTaskProgressList = function(condition) {
		return $http({
			method : 'POST',
			url : 'dashboard/getTaskProgressList.do',
			data : 'params=' + encodeURIComponent(JSON.stringify(condition)),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.taskProgressListResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	output.getActivitiesList = function(condition) {
		return $http({
			method : 'POST',
			url : 'dashboard/getActivitiesList.do',
			data : 'params=' + encodeURIComponent(JSON.stringify(condition)),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.activitiesListResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	return output;
}
angular.module('kindKidsApp').factory('dashboardService', [ '$http', 'commonService', dashboardService ]);