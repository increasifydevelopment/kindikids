'use strict';

/**
 * @author abliu
 * @description policy Service
 */
function policyService($q,$http, commonService) {
	// variable=================================================================
	var output = {};

	// select===================================================================

	/**
	 * @author abliu
	 * @description
	 * @returns getCategoriesSrcList
	 */
	output.getCategoriesSrcList=function(){
		return $http({
			method : 'POST',
			url : 'policy/getCategoriesSrcList.do',
			data : "",
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.categoriesSrcResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	}

	/**
	 * @author abliu
	 * @description
	 * @returns 获取policyList
	 */
	output.getPolicyList = function(condition) {
		return $http({
			method : 'POST',
			url : 'policy/list.do',
			data : "params="+encodeURIComponent(JSON.stringify(condition)),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.listResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * @author abliu
	 * @description
	 * @returns 获取policyinfo
	 */
	output.getPolicyInfo = function(id) {
		/*return $http.get("./data/policy.data.json")*/
		return $http({
			method : 'POST',
			url : 'policy/info.do',
			data : "id="+encodeURIComponent(id),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.policyInfo = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	// edit=====================================================================
	/**
	 * @author abliu
	 * @description
	 * @returns SavePolicyinfo
	 */
	output.savePolicyInfo = function(policyInfo) {
		
		return $http({
			method : 'POST',
			url : 'policy/edit.do',
			data : "params="+encodeURIComponent(JSON.stringify(policyInfo)),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.saveResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	/**
	 * @author abliu
	 * @description
	 * @returns deletePolicyInfo
	 */
	output.deletePolicyInfo = function(id) {		
		return $http({
			method : 'POST',
			url : 'policy/deletePolicy.do',
			data : "policyId="+id,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.deleteResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	

	/**
	 * @author abliu
	 * @description
	 * @returns saveCategory
	 */
	output.saveCategory = function(policyCategories) {		
		return $http({
			method : 'POST',
			url : 'policy/editCategory.do',
			data : "params="+encodeURIComponent(JSON.stringify(policyCategories)),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.saveCategoryResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};


	/**
	 * @author abliu
	 * @description
	 * @returns existDeletePolicy
	 */
	output.existDeletePolicy = function(policyCategoryId) {		
		return $http({
			method : 'POST',
			url : 'policy/existDeletePolicy.do',
			data : "policyCategoriesId="+policyCategoryId,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.existDeletePolicyResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	
	

	return output;
}
angular.module('kindKidsApp').factory('policyService', [ '$q', '$http', 'commonService',policyService]);