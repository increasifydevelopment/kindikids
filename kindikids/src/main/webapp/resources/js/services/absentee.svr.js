'use strict';

/**
 * @author gfwang
 * @description 小孩离园
 */
function absebteeService($q, $http, commonService) {
	// variable=================================================================
	var output = {};

	// select===================================================================
	/**
	 * apply giving notice
	 */
	output.applyAbsebtee = function(absebteeInfo, isSure) {
		return $http({
			method : 'POST',
			url : 'Absentee/saveAbsentee.do',
			data : 'params=' + encodeURIComponent(JSON.stringify(absebteeInfo)) + '&isSure=' + isSure,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.applyResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};

	output.getAbsebteeInfo = function(absebteeId, userId) {
		return $http({
			method : 'GET',
			url : 'Absentee/getAbsentee.do?id=' + absebteeId + '&userId=' + userId,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.absebteeResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	output.cancelAbsebtee = function(absebteeId) {
		return $http({
			method : 'GET',
			url : 'Absentee/cancelAbsentee.do?id=' + absebteeId,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.cancleResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	output.declineAbsebtee = function(absebteeId) {
		return $http({
			method : 'GET',
			url : 'Absentee/rejectAbsentee.do?id=' + absebteeId,
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.declineResult = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	output.getAbsenteeDays = function(absebteeInfo) {
		return $http({
			method : 'POST',
			url : 'Absentee/getAbsenteeDays.do',
			data : 'params=' + encodeURIComponent(JSON.stringify(absebteeInfo)),
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		}).success(function(result) {
			output.absebteeInfoForDays = result;
		}).error(function(data, status, headers, config) {
			commonService.ajaxE(data);
		});
	};
	return output;
}
angular.module('kindKidsApp').factory('absebteeService', [ '$q', '$http', 'commonService', absebteeService ]);