'use strict';

function waitingService($http, commonService) {
    var output = {};
    output.getWaitingList = function (params) {
        return $http({
            method: 'POST',
            url: './waiting/list.do',
            data: 'params=' + encodeURIComponent(JSON.stringify(params)),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).success(function (result) {
            output.waitingResult = result;
        }).error(function (data, status, headers, config) {
            commonService.ajaxE(data);
        });
    }
    output.getChildDetails = function (id) {
        return $http({
            method: 'POST',
            url: './waiting/details.do',
            data: 'id=' + encodeURIComponent(id),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).success(function (result) {
            output.detailsResult = result;
        }).error(function (data, status, headers, config) {
            commonService.ajaxE(data);
        });
    }

    output.getCount = function () {
        return $http({
            method: 'GET',
            url: './waiting/count.do',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).success(function (result) {
            output.countResult = result;
        }).error(function (data, status, headers, config) {
            commonService.ajaxE(data);
        });
    }

    output.setpPositioningDate = function (item) {
        return $http({
            method: 'POST',
            url: './waiting/position.do',
            data: 'params=' + encodeURIComponent(JSON.stringify(item)),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).success(function (result) {
            output.positionResult = result;
        }).error(function (data, status, headers, config) {
            commonService.ajaxE(data);
        });
    }

    output.submitMoveTo = function (params) {
        return $http({
            method: 'POST',
            url: './waiting/submitMove.do',
            data: 'params=' + encodeURIComponent(JSON.stringify(params)),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).success(function (result) {
            output.submitMoveResult = result;
        }).error(function (data, status, headers, config) {
            commonService.ajaxE(data);
        });
    }

    output.sendPlanEmail = function () {
        return $http({
            method: 'POST',
            url: './waiting/sendPlanEmail.do',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        })
    }
    output.save = function (params) {
        return $http({
            method: 'POST',
            url: './waiting/save.do',
            data: 'params=' + encodeURIComponent(JSON.stringify(params)),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).success(function (result) {
            output.saveReuslt = result;
        }).error(function (data, status, headers, config) {
            commonService.ajaxE(data);
        });
    }

    output.exportCsv = function (params) {
        return $http({
            method: 'POST',
            url: './waiting/csv.do',
            data: 'params=' + encodeURIComponent(JSON.stringify(params)),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).success(function (result) {
            output.exportCsvResult = result;
        }).error(function (data, status, headers, config) {
            commonService.ajaxE(data);
        });
    }

    return output;
}
angular.module('kindKidsApp').factory('waitingService', ['$http', 'commonService', waitingService]);