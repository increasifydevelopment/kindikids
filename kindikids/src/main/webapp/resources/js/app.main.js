'use strict';

angular.module('kindKidsApp').controller(
	'AppCtrl',
	[
		'$scope',
		'$rootScope',
		'$window',
		'$location',
		'$filter',
		'$timeout',
		'$uibModal',
		'commonService',
		'signService',
		function AppCtrl($scope, $rootScope, $window, $location, $filter, $timeout, $uibModal, commonService, signService) {

			$rootScope.getCurrtenUser = function () {
				commonService.getCurrentUser().then(angular.bind(this, function then() {
					$rootScope.currentUser = commonService.currentUser;
					if ($rootScope.currentUser) {
						var centreId = $rootScope.currentUser.userInfo.centersId;
						// 是否只有兼职
						$rootScope.isOnlyCasual = ($filter('haveRole')($scope.currentUser.roleInfoVoList, '4;') && !centreId);
						// 是否包含兼职角色
						var isCasual = ($filter('haveRole')($scope.currentUser.roleInfoVoList, '4;'));
						// 是否园长
						var isCentreManager = ($filter('haveRole')($scope.currentUser.roleInfoVoList, '1;5'));
						$rootScope.isCenterManager = isCentreManager;
						// 是否CEO
						$rootScope.isCeoRole = ($filter('haveRole')($scope.currentUser.roleInfoVoList, '0'));
						// 如果为二级园长+兼职，且没有园区
						$rootScope.centreAndCasual = (isCasual && isCentreManager) && !centreId || (isCentreManager && !centreId);
						$rootScope.isParent = $filter('haveRole')($scope.currentUser.roleInfoVoList, '10');
						$rootScope.isOnlyEduCook = !$filter('haveRole')($scope.currentUser.roleInfoVoList, '0;1;5;10');
					}
					$scope.openTemsCondition();
				}));
			};
			$rootScope.getCurrtenUser();

			$scope.initMenus = function () {
				commonService.getMenus().then(angular.bind(this, function then() {
					$scope.menus = commonService.menus.returnObj;
				}));
			};

			$scope.logout = function () {
				commonService.logout().then(angular.bind(this, function then() {
					$timeout(function () {
						location.href = "login.html";
					}, 100);
				}));
			};
			// 打开漏斗
			$scope.pushButton = function () {
				$('.sidebar-inner').addClass('open');
				$('body').addClass('push-open push-open-filter');
			};
			// 关闭漏斗
			$scope.closeButton = function () {
				$('.sidebar-inner').removeClass('open');
				$('body').removeClass('push-open push-open-filter');
			};
			$scope.closeMainMenuButton = function () {
				$('.menu-main').removeClass('open');
				$('body').removeClass('push-open');
			};
			$scope.pushMainMenuButton = function () {
				$('.menu-main').addClass('open');
				$('body').addClass('push-open');
			};
			$scope.hideMenu = function () {
				var width = $(document).width();
				if (width <= 768) {
					$("#menuButton").click();
				}
			};
			/**
			 * @author abliu
			 * @description 打开选择NQS的集合 chooseNqs 传入选中的nqs集合
			 *              [{"version":"1.1.1","content":"test"},{"version":"1.1.2","content":"test2"}];;
			 * @description saveNqsResult=function nqs选中后保存nqs集合,返回新选中的
			 *              [{"version":"1.1.1","content":"test"},{"version":"1.1.2","content":"test2"}];;
			 * @returns
			 */
			$rootScope.openNqs = function (chooseNqs, saveNqsResult, dateStr) {
				var modalNqs = $uibModal.open({
					templateUrl: 'views/common/nqs.html?res_v=' + $scope.res_v,
					backdrop: 'static',
					keyboard: false,
					controller: "nqsCtrl",
					resolve: {
						chooseNqs: function () {
							return JSON.stringify(chooseNqs);
						},
						saveNqsResult: function () {
							return saveNqsResult;
						},
						dateStr: function () {
							return dateStr;
						}
					}
				}).result.then(function (selectedItem) {
				});
			};
			$rootScope.showLoading = function (tag) {
				$(tag).mymask('');
			};

			$rootScope.hideLoading = function (tag) {
				if ($(tag).myisMasked()) {
					$(tag).myunmask();
				}
			};

			$rootScope.confirmbyMessage = function (title, content, callback) {
				$.messager.model = {
					cancel: {
						text: "No",
						classed: 'btn-default'
					},
					ok: {
						text: "Yes",
						classed: 'btn-general closeAllModal'
					}
				};
				$.messager.confirm(title, content, callback);
			};

			$rootScope.isImage = function (fileName) {
				if (!fileName) {
					return "";
				}
				var suffix = fileName.split('.').pop().toLowerCase();
				if (".jpg,.jpeg,.png,.gif,".indexOf(suffix + ',') < 0) {
					return "Please ensure your file is either a .png, .jpg or .gif file type.";
				}
				return "";

			};

			$rootScope.message = function (content, time) {
				var msgObj = {
					content: content,
					time: time
				};
				$uibModal.open({
					templateUrl: 'views/common/message.html?res_v=' + $scope.res_v,
					controller: "msgCtrl",
					keyboard: false,
					windowClass: "notify-modal",
					animation: true,
					resolve: {
						msgObj: function () {
							return msgObj;
						},
					}
				});
			};

			/**
			 * 如果是第一次登陆则弹出该框
			 */
			$scope.openTemsCondition = function () {
				// 判断是否第一次
				if (!$scope.currentUser) {
					return;
				}
				var notSignOut = $scope.currentUser.accountInfo.notSignOut;
				if (notSignOut) {
					signService.removeNotHaveSignout($scope.currentUser.accountInfo.id).then(angular.bind(this, function then() {
					}));
					var msg = 'You did not sign out at ' + $filter('date')(notSignOut, 'dd MMM yyyy')
						+ ', please improve the daily attendance information !'
					$scope.confirm('Warning', msg, 'OK', function () {
					});
				}
				var flag = $scope.currentUser.accountInfo.agreeFlag;
				if (!flag) {
					$uibModal.open({
						templateUrl: 'terms_conditions.html?res_v=' + $scope.res_v,
						controller: "termConditionCtrl",
						size: "lg",
						backdrop: 'static',
						keyboard: false,
						resolve: {

						}
					});
				}
			};

			// 拆分后台过来时间的方法
			$rootScope.changeBackTime = function (time) {
				if (!time) {
					return '';
				}
				var inTime;
				var hour = parseInt(time.substr(11, 2));
				var minute = time.substr(13, 3);
				if (hour < 12) {
					// 如果24小时制的时间为00点，在12小时制为12 AM
					if (hour == 0) {
						hour = 12;
						inTime = "12" + minute + ' ' + 'AM';
					} else {
						inTime = time.substr(11, 5) + ' ' + 'AM';
					}
				} else {
					// 如果24小时制的时间为12点，在12小时制为12 PM
					if (hour == 12) {
						inTime = "12" + minute + ' ' + 'PM';
					} else {
						inTime = (parseInt(time.substr(11, 2)) - 12) + time.substr(13, 3) + ' ' + 'PM';
					}

				}
				if (inTime == '00:00 AM') {
					inTime = '';
				}
				return dividZero(inTime);
			};
			// 去除首字符0
			function dividZero(time) {
				var hour = time.split(':')[0];
				var min = time.split(':')[1];
				return hour * 1 + ':' + min;
			}

			// 组合前台时间传入后台
			$rootScope.changeHeadTime = function (time) {
				if (!time) {
					return 'T00:00:00.000';
				}
				var inTime;
				if (time.split(':')[1].substr(3, 2) == 'AM') {
					var hour;
					if (time.split(':')[0].length == 1) {
						hour = '0' + time.split(':')[0];
					} else {
						hour = time.split(':')[0];
						// 如果是上午12点改成24小时制为00点
						if (hour == 12) {
							hour = "00";
						}
					}
					inTime = 'T' + hour + ':' + time.split(':')[1].substr(0, 2) + ':00.000';
				} else {
					inTime = 'T' + (getHour(parseInt(time.split(':')[0]))) + ':' + time.split(':')[1].substr(0, 2) + ':00.000';
				}
				return inTime;
			};
			function getHour(hour) {
				if (hour == 12) {
					return hour;
				}
				return hour + 12;
			}
			$scope.openProfile = function (type, id) {
				if (type == 1) {
					if ('app.main.user.staff.list' === $scope.$state.current.name && $scope.$stateParams.userId != '*') {
						$scope.$state.reload();
					} else {
						$scope.$state.go("app.main.user.staff.list", {
							userId: id
						});
					}
					return;
				}

				if ('app.main.user.family' === $scope.$state.current.name && $scope.$stateParams.userId != '*') {
					$scope.$state.go("app.main.user.family", {
						familyId: id
					}, {
							reload: true
						});

					// $scope.$state.reload();
				} else {
					$scope.$state.go("app.main.user.family", {
						familyId: id
					});
				}
			};


			/**
			 * @author gfwang
			 * @deprecated 公共图片预览
			 * @param list 图片集合
			 * @param index 默认打开图片索引
			 */
			$rootScope.visitImage = function (list, index, title, code) {
				if (!code) {
					code = -1;
				}
				$uibModal.open({
					templateUrl: 'views/common/img.modal.html?res_v=' + $scope.res_v,
					controller: "visitImageCtrl",
					backdrop: 'static',
					keyboard: false,
					windowClass: 'modal fade modal-fullscreen force-fullscreen gallery-zoomin-modal gallery-with-edit',
					resolve: {
						imageList: function () {
							return list;
						},
						index: function () {
							return index;
						},
						title: function () {
							return title;
						},
						code: function () {
							return code;
						}
					}
				});

			};



			/**
			 * @author abliu
			 * @description 统一打开提示框页面
			 * @description title 提示框标题 content:提示框内容 okText:ok按钮名
			 *              okCallback:ok回调 cancelText:取消文本
			 *              cancelCallback:取消回调
			 * @description okText如果为null则不需要ok按钮，取消如果为null则不需要取消按钮
			 * @returns
			 */
			$rootScope.confirm = function (title, content, okText, okCallback, cancelText, cancelCallback) {
				var confirmObj = {
					title: title,
					content: content,
					ok: {
						text: okText,
						callback: okCallback
					},
					cancel: {
						text: cancelText,
						callback: cancelCallback
					}
				};
				$uibModal.open({
					templateUrl: 'views/common/confirm.html?res_v=' + $scope.res_v,
					controller: "confirmCtrl",
					backdrop: 'static',
					keyboard: false,
					windowClass: "modal-index",
					resolve: {
						confirmObj: function () {
							return confirmObj;
						},
					}
				});
			};

			$rootScope.confirm2 = function (title, content, fun1Text, func1Callback, fun2Text, func2Callback, cancelText, cancelCallback) {
				var confirmObj = {
					title: title,
					content: content,
					fun1: {
						text: fun1Text,
						callback: func1Callback
					},
					fun2: {
						text: fun2Text,
						callback: func2Callback
					},
					cancel: {
						text: cancelText,
						callback: cancelCallback
					}
				};
				$uibModal.open({
					templateUrl: 'views/common/confirm2.html?res_v=' + $scope.res_v,
					controller: "confirmCtrl2",
					backdrop: 'static',
					keyboard: false,
					windowClass: "modal-index",
					resolve: {
						confirmObj: function () {
							return confirmObj;
						},
					}
				});
			};

			/**
			 * @author abliu
			 * @description 处理由小孩页面直接跳转过来的condition传递
			 */
			$rootScope.galleryCondition = {
				childId: null,
				pageIndex: 1,
				avatar: '',
				personColor: "",
				name: ''
			};



		}]);