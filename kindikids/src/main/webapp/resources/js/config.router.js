'use strict';
var res_v = '20190409_v4056';
angular.module('kindKidsApp').run(['$rootScope', '$state', '$stateParams', 'messageService', 'waitingService', function ($rootScope, $state, $stateParams, messageService, waitingService) {
	$rootScope.innerRouterChange = true;
	$rootScope.res_v = res_v;
	$rootScope.routerChangeTime = 0;
	$rootScope.$state = $state;
	$rootScope.$stateParams = $stateParams;

	// 查询waiting各个列表数量
	$rootScope.getApplicationCount = function () {
		waitingService.getCount().then(angular.bind(this, function then() {
			$rootScope.countResult = waitingService.countResult.data;
		}));
	};

	$rootScope.getUnreadFunction = function () {
		messageService.getUnReadCount().then(angular.bind(this, function then() {
			$rootScope.unreadCount = messageService.count;
		}));
	};
	/**
	 * 未读个数
	 */
	$rootScope.unreadCount = 0;
	var getUnreadTimeOut = function () {
		$rootScope.getUnread = setInterval(function () {
			// 路由10分钟没有变化，则不请求后台
			var dateRange = new Date().getTime() - $rootScope.routerChangeTime;
			if (dateRange > (10 * 60 * 1000)) {
				window.clearInterval($rootScope.getUnread);
				$rootScope.routerChangeTime = 0;
				return;
			}
			$rootScope.getUnreadFunction();
		}, 1000 * 10 * 6 * 1);
	};

	var getSessionTimeOut = function () {
		$rootScope.getTimeOut = setInterval(function () {
			messageService.getSessionTimeOut().then(angular.bind(this, function then() {}));
		}, 1000 * 10 * 6 * 1);
	};

	$rootScope.$on('$stateChangeSuccess', function (obj1, obj2) {
		// 路由切換，關閉下拉
		var mainMenu = "app.main.event.gallerylist;app.main.event.list;";
		if (mainMenu.indexOf(obj2.name + ";") < 0 || $rootScope.innerRouterChange) {
			$('.menu-main').removeClass('open');
			$('body').removeClass('push-open');
		}

		var exceptionRoutes = 'app.landSign;app.personSignList;app.visitorSignin;app.visitorSignout;app.visitorWelcome;app.visitorSignoutSign;';
		if (exceptionRoutes.indexOf(obj2.name + ";") > -1) {
			if (!$rootScope.getUnread) {
				return;
			}
			window.clearInterval($rootScope.getUnread);
			return;
		}
		$rootScope.getUnreadFunction();
		$rootScope.getApplicationCount();
		if ($rootScope.routerChangeTime == 0) {
			getUnreadTimeOut();
		}
		$rootScope.routerChangeTime = new Date().getTime();
		getSessionTimeOut();
		window.scrollTo(0, 0);
	});
}, ]).config(
	[
		'$stateProvider',
		'$urlRouterProvider',
		'$httpProvider',
		function ($stateProvider, $urlRouterProvider, $httpProvider) {
			// Initialize get if not there
			if (!$httpProvider.defaults.headers.get) {
				$httpProvider.defaults.headers.get = {};
			}
			// Enables Request.IsAjaxRequest() in ASP.NET MVC
			$httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
			// Disable IE ajax request caching
			$httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
			$httpProvider.defaults.headers.get['Pragma'] = 'no-cache';
			// For unmatched routes
			$urlRouterProvider.otherwise('/news_list');
			// Application routes
			$stateProvider
				// app
				.state('app', {
					abstract: true,
					template: '<div ui-view></div>',
				}).state(
					'app.visitorSignin', {
						url: '/visitor_signin/{centerId}/{centerName}',
						templateUrl: 'views/sign/visitor-signin.html?res_v=' + res_v,
						resolve: {
							deps: [
								'$ocLazyLoad',
								function ($ocLazyLoad) {
									return $ocLazyLoad.load(['./resources/js/controllers/sign/visitorSignin.ctrl.js?res_v=' + res_v,
										'./resources/js/directives/validate/sign.vali.js?res_v=' + res_v,
									]);
								}
							]
						}
					}).state(
					'app.personSignList', {
						url: '/person_signList/{flag}/{centerId}/{centerName}',
						templateUrl: 'views/sign/person-sign-list.html?res_v=' + res_v,
						resolve: {
							deps: [
								'$ocLazyLoad',
								function ($ocLazyLoad) {
									return $ocLazyLoad.load(['./resources/js/controllers/sign/personSignList.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/sign/personSign.ctrl.js?res_v=' + res_v,
										'./resources/js/directives/validate/sign.vali.js?res_v=' + res_v,
									]);
								}
							]
						}
					}).state(
					'app.visitorSignoutSign', {
						url: '/visitor_signoutSign/:flag/{centerName}/{fullName}',
						templateUrl: 'views/sign/visitor-signout-sign.html?res_v=' + res_v,
						resolve: {
							deps: [
								'$ocLazyLoad',
								function ($ocLazyLoad) {
									return $ocLazyLoad.load(['./resources/js/controllers/sign/visitorSignoutSign.ctrl.js?res_v=' + res_v,
										'./resources/js/directives/validate/sign.vali.js?res_v=' + res_v,
									]);
								}
							]
						}
					}).state('app.landSign', {
					url: '/landing_signin/{centerName}',
					templateUrl: 'views/sign/landing-signin.html?res_v=' + res_v,
					resolve: {
						deps: ['$ocLazyLoad', function ($ocLazyLoad) {
							return $ocLazyLoad.load(['./resources/js/controllers/sign/landSign.ctrl.js?res_v=' + res_v, ]);
						}]
					}
				}).state('app.visitorSignout', {
					url: '/visitor_signout/{centerId}/{centerName}',
					templateUrl: 'views/sign/visitor-signout.html?res_v=' + res_v,
					resolve: {
						deps: ['$ocLazyLoad', function ($ocLazyLoad) {
							return $ocLazyLoad.load(['./resources/js/controllers/sign/visitorSignout.ctrl.js?res_v=' + res_v, ]);
						}]
					}
				}).state('app.visitorWelcome', {
					url: '/visitor-welcome-bye/{centerId}/{flag}/{centerName}/{name}',
					templateUrl: 'views/sign/visitor-welcome-bye.html?res_v=' + res_v,
					resolve: {
						deps: ['$ocLazyLoad', function ($ocLazyLoad) {
							return $ocLazyLoad.load(['./resources/js/controllers/sign/visitorMessage.ctrl.js?res_v=' + res_v, ]);
						}]
					}
				})
				// 跳转登录页面
				.state('app.cancleSuccess', {
					url: '/cancleSuccess',
					templateUrl: 'views/account/visitor-welcome-bye.html?res_v=' + res_v,
					resolve: {
						deps: ['$ocLazyLoad', function ($ocLazyLoad) {
							return $ocLazyLoad.load(['./resources/js/controllers/cancle.success.ctrl.js?res_v=' + res_v, ]);
						}]
					}
				})

				.state('app.main', {
					abstract: true,
					templateUrl: 'sidebar.html?res_v=' + res_v,
				})

				// program 模块
				.state(
					'app.main.taskView', {
						url: '/task-view',
						templateUrl: 'views/program/task-view.html?res_v=' + res_v,
						resolve: {
							deps: [
								'$ocLazyLoad',
								function ($ocLazyLoad) {
									return $ocLazyLoad.load(['./resources/js/services/task.svr.js?res_v=' + res_v,
										'./resources/js/controllers/program/task-view.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/program/lesson.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/program/explore-grow.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/program/interest.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/program/interest-followUp.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/program/share.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/program/weekly.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/program/register.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/program/medication.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/program/otherTask.ctrl.js?res_v=' + res_v,
										'./resources/js/directives/program.directive.js?res_v=' + res_v,
										'./resources/js/directives/validate/program.vali.js?res_v=' + res_v,
										'./resources/js/filter/attendance_children.filter.js?res_v=' + res_v,
										'./resources/js/filter/program.filter.js?res_v=' + res_v,
										'./resources/js/controllers/program/task.form.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/program/view.meeting.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/program/hat.money.ctrl.js?res_v=' + res_v,
									]);
								}
							]
						}
					})
				// program table view 模块
				.state(
					'app.main.taskTable', {
						url: '/task-table',
						templateUrl: 'views/program/task-table.html?res_v=' + res_v,
						resolve: {
							deps: [
								'$ocLazyLoad',
								function ($ocLazyLoad) {
									return $ocLazyLoad.load(['./resources/js/services/task.svr.js?res_v=' + res_v,
										'./resources/js/controllers/program/task-view.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/program/lesson.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/program/explore-grow.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/program/interest.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/program/interest-followUp.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/program/share.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/program/weekly.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/program/register.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/program/medication.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/program/otherTask.ctrl.js?res_v=' + res_v,
										'./resources/js/directives/program.directive.js?res_v=' + res_v,
										'./resources/js/directives/validate/program.vali.js?res_v=' + res_v,
										'./resources/js/filter/attendance_children.filter.js?res_v=' + res_v,
										'./resources/js/filter/program.filter.js?res_v=' + res_v,
										'./resources/js/controllers/program/task.form.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/program/view.meeting.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/program/hat.money.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/program/task-table.ctrl.js?res_v=' + res_v,
										'./resources/js/filter/child.filter.js?res_v=' + res_v,
									]);
								}
							]
						}
					})

				// task 模块
				.state(
					'app.main.taskSetting', {
						url: '/task_setting',
						templateUrl: 'views/task/task-setting.html',
						resolve: {
							deps: [
								'$ocLazyLoad',
								function ($ocLazyLoad) {
									console.log('routerstart=' + new Date().getTime());
									return $ocLazyLoad.load(['./resources/js/controllers/task/task-setting.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/task/frequency.ctrl.js?res_v=' + res_v,
										'./resources/js/services/task.set.svr.js?res_v=' + res_v,
										'./resources/js/directives/task.directive.js?res_v=' + res_v,
										'./resources/js/filter/task.filter.js?res_v=' + res_v,
										'./resources/js/directives/validate/task.vali.js?res_v=' + res_v,
									]);
								}
							]
						}
					})
				// appliction - list
				.state(
					'app.main.applictionList', {
						url: '/application_list',
						templateUrl: 'views/waitingList/application-list.html',
						resolve: {
							deps: [
								'$ocLazyLoad',
								function ($ocLazyLoad) {
									return $ocLazyLoad
										.load([
											'./resources/js/controllers/waiting/waitingList.ctrl.js?res_v=' + res_v,
											'./resources/js/services/waiting.svr.js?res_v=' + res_v,
											'./resources/library/vendor/bootstrap-datetimepicker.js?res_v=' + res_v,
											'./resources/js/filter/waiting.filter.js?res_v=' + res_v,
											'./resources/js/filter/common.filter.js?res_v=' + res_v,
											'./resources/js/controllers/family/family_edit_absentee.ctrl.js?res_v=' + res_v,
											'./resources/js/directives/validate/program.vali.js?res_v=' + res_v,
											'./resources/js/directives/validate/family.vali.js?res_v=' + res_v,
											'./resources/js/directives/validate/parent.vali.js?res_v=' + res_v,
											'./resources/js/directives/validate/change_attendance.vali.js?res_v=' + res_v,
											'./resources/js/services/absentee.svr.js?res_v=' + res_v,
											'./resources/js/services/attend_child.giving.svr.js?res_v=' + res_v,
											'./resources/js/controllers/family/family_edit_givingNotice.ctrl.js?res_v=' + res_v,
											'./resources/js/services/attendance.svr.js?res_v=' + res_v,
											'./resources/js/controllers/family/family_edit_changeAttendance.ctrl.js?res_v=' + res_v,
											'./resources/js/filter/child.filter.js?res_v=' + res_v,
											'./resources/js/controllers/family/family_edit.ctrl.js?res_v=' + res_v,
											'./resources/js/controllers/logs/logs.list.ctrl.js?res_v=' + res_v,
											'./resources/js/services/logs.svr.js?res_v=' + res_v,
											'./resources/js/controllers/family/family_edit_requestLog.ctrl.js?res_v=' + res_v,
											'./resources/js/directives/ngBindUnsafe.directive.js?res_v=' + res_v,
											'./resources/js/controllers/family/family_edit_requestLogModal.ctrl.js?res_v=' + res_v,
											'./resources/js/controllers/family/family_edit_attendanceLog.ctrl.js?res_v=' + res_v,
											'./resources/js/services/family.svr.js?res_v=' + res_v,
											'./resources/js/directives/family.directive.js?res_v=' + res_v,
											'./resources/js/directives/program.directive.js?res_v=' + res_v,
											'./resources/js/controllers/family/family_select.ctrl.js?res_v=' + res_v,
											'./resources/js/controllers/waiting/allocateModal.ctrl.js?res_v=' + res_v,
											'./resources/js/controllers/waiting/profileModal.ctrl.js?res_v=' + res_v,
										]);
								}
							]
						}
					})

				.state(
					'app.main.profile', {
						url: '/profile_list/{profileType}',
						templateUrl: 'views/profile/template-profile.html?res_v=' + res_v,
						resolve: {
							deps: [
								'$ocLazyLoad',
								function ($ocLazyLoad) {
									return $ocLazyLoad.load(['./resources/js/controllers/profile/profile.ctrl.js?res_v=' + res_v,
										'./resources/js/services/profile.svr.js?res_v=' + res_v,
										'./resources/js/directives/meeting.directive.js?res_v=' + res_v,
										'./resources/js/controllers/confirm.ctrl.js?res_v=' + res_v,
									]);
								}
							]
						}
					})

				.state('app.main.user', {
					abstract: true,
					template: '<div ui-view></div>'
				}).state(
					'app.main.user.staff', {
						templateUrl: 'views/user/staff/staff.main.html?res_v=' + res_v,
						resolve: {
							deps: [
								'$ocLazyLoad',
								function ($ocLazyLoad) {
									return $ocLazyLoad.load(['./resources/js/directives/ng-infinite-scroll/ng-infinite-scroll.js?res_v=' + res_v,
										'./resources/js/controllers/staff.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/staff.edit.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/staff.employment.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/confirm.ctrl.js?res_v=' + res_v,
										'./resources/js/directives/staff.directive.js?res_v=' + res_v,
										'./resources/js/directives/validate/staff.vali.js?res_v=' + res_v,
										'./resources/library/vendor/summernote.min.js?res_v=' + res_v,
										'./resources/library/vendor/bootstrap-datetimepicker.js?res_v=' + res_v,
										'./resources/js/controllers/logs/logs.list.ctrl.js?res_v=' + res_v,
										'./resources/js/services/logs.svr.js?res_v=' + res_v,
										'./resources/js/controllers/staff_change_orgs.ctrl.js?res_v=' + res_v,

									]);
								}
							]
						}
					}).state('app.main.user.staff.list', {
					url: '/user_staff_list/{userId}',
					templateUrl: 'views/user/staff/list.html?res_v=' + res_v,
					resolve: {
						deps: ['$ocLazyLoad', function ($ocLazyLoad) {
							return $ocLazyLoad.load(['./resources/js/directives/family.directive.js?res_v=' + res_v]);
						}]
					}
				}).state(
					'app.main.user.family', {
						url: '/user_family_list/{familyId}',
						templateUrl: 'views/user/family/list.html?res_v=' + res_v,
						resolve: {
							deps: [
								'$ocLazyLoad',
								function ($ocLazyLoad) {
									return $ocLazyLoad.load(['./resources/js/directives/ng-infinite-scroll/ng-infinite-scroll.js?res_v=' + res_v,
										'./resources/js/controllers/family/family.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/family/family_edit.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/logs/logs.list.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/family/family_edit_changeAttendance.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/family/family_edit_requestLog.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/family/family_edit_requestLogModal.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/family/family_edit_attendanceLog.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/family/family_edit_givingNotice.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/family/family_edit_absentee.ctrl.js?res_v=' + res_v,
										'./resources/js/services/family.svr.js?res_v=' + res_v, './resources/js/services/logs.svr.js?res_v=' + res_v,
										'./resources/js/services/attendance.svr.js?res_v=' + res_v,
										'./resources/js/directives/family.directive.js?res_v=' + res_v,
										'./resources/js/directives/validate/family.vali.js?res_v=' + res_v,
										'./resources/js/directives/validate/parent.vali.js?res_v=' + res_v,
										'./resources/js/directives/validate/change_attendance.vali.js?res_v=' + res_v,
										'./resources/js/filter/child.filter.js?res_v=' + res_v,
										'./resources/js/controllers/confirm.ctrl.js?res_v=' + res_v,
										'./resources/library/vendor/summernote.min.js?res_v=' + res_v,
										'./resources/js/services/attend_child.giving.svr.js?res_v=' + res_v,
										'./resources/js/services/absentee.svr.js?res_v=' + res_v, './resources/js/services/task.svr.js?res_v=' + res_v,
										'./resources/js/controllers/program/task.form.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/program/medication.ctrl.js?res_v=' + res_v,
										'./resources/js/directives/validate/program.vali.js?res_v=' + res_v,
										'./resources/js/controllers/confirm.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/family/family_select.ctrl.js?res_v=' + res_v,
									]);
								}
							]
						}
					}).state(
					'app.main.message', {
						abstract: true,
						templateUrl: 'views/message/message.main.html?res_v=' + res_v,
						resolve: {
							deps: [
								'$ocLazyLoad',
								function ($ocLazyLoad) {
									return $ocLazyLoad.load(['./resources/js/directives/ng-infinite-scroll/ng-infinite-scroll.js?res_v=' + res_v,
										'./resources/js/directives/nqs.directive.js?res_v=' + res_v,
										'./resources/js/controllers/nqs.ctrl.js?res_v=' + res_v, './resources/js/services/nqs.svr.js?res_v=' + res_v,
										'./resources/js/controllers/message.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/message_create.ctrl.js?res_v=' + res_v,
										'./resources/js/services/message.svr.js?res_v=' + res_v,
										'./resources/js/directives/message.directive.js?res_v=' + res_v,
										'./resources/js/filter/message.filter.js?res_v=' + res_v,
									]);
								}
							]
						}
					}).state('app.main.message.list', {
					url: '/message_list',
					templateUrl: 'views/message/list.html?res_v=' + res_v,
				}).state('app.main.message.edit', {
					url: '/message_edit/{messageId}',
					templateUrl: 'views/message/edit.html?res_v=' + res_v,
				}).state('app.main.newsfeed', {
					abstract: true,
					template: '<div ui-view></div>'
				}).state(
					'app.main.newsfeed.list', {
						url: '/news_list',
						templateUrl: 'views/newsfeed/list.html?res_v=' + res_v,
						resolve: {
							deps: [
								'$ocLazyLoad',
								function ($ocLazyLoad) {
									// 進入首頁就加載
									// $rootScope.getUnreadFunction();
									return $ocLazyLoad.load(['./resources/js/filter/newsfeed.filter.js?res_v=' + res_v,
										'./resources/js/directives/ng-infinite-scroll/ng-infinite-scroll.js?res_v=' + res_v,
										'./resources/js/directives/newsfeed.directive.js?res_v=' + res_v,
										'./resources/js/services/newsfeed.svr.js?res_v=' + res_v,
										'./resources/js/controllers/newsfeed.ctrl.js?res_v=' + res_v,
									]);
								}
							]
						}
					}).state('app.main.newsfeed.edit', {
					url: '/news_edit',
					templateUrl: 'views/newsfeed/edit.html?res_v=' + res_v,
				}).state('app.main.policy', {
					abstract: true,
					template: '<div ui-view></div>'
				}).state(
					'app.main.policy.list', {
						url: '/policy_list',
						templateUrl: 'views/policy/list.html?res_v=' + res_v,
						resolve: {
							deps: [
								'$ocLazyLoad',
								function ($ocLazyLoad) {
									return $ocLazyLoad.load(['./resources/js/controllers/policy/list.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/policy/edit.ctrl.js?res_v=' + res_v,
										'./resources/js/services/policy.svr.js?res_v=' + res_v,
										'./resources/js/directives/policy.directive.js?res_v=' + res_v,
										'./resources/js/filter/policy.filter.js?res_v=' + res_v,
										'./resources/js/controllers/policy/edit.categories.ctrl.js?res_v=' + res_v,
									]);
								}
							]
						}
					}).state(
					'app.main.event', {
						abstract: true,
						templateUrl: 'views/event/main.html?res_v=' + res_v,
						resolve: {
							deps: [
								'$ocLazyLoad',
								function ($ocLazyLoad) {
									return $ocLazyLoad.load(['./resources/js/directives/ng-infinite-scroll/ng-infinite-scroll.js?res_v=' + res_v,
										'./resources/js/controllers/event/event.ctrl.js?res_v=' + res_v,
										'./resources/js/services/event.svr.js?res_v=' + res_v,
									]);
								}
							]
						}
					}).state('app.main.event.gallerylist', {
					url: '/gallery_list',
					templateUrl: 'views/event/gallery.list.html?res_v=' + res_v,
					resolve: {
						deps: ['$ocLazyLoad', function ($ocLazyLoad) {
							return $ocLazyLoad.load(['./resources/js/controllers/event/eventChildren.ctrl.js?res_v=' + res_v, ]);
						}]
					}
				}).state('app.main.event.list', {
					url: '/event_list',
					templateUrl: 'views/event/list.html?res_v=' + res_v,
					resolve: {
						deps: ['$ocLazyLoad', function ($ocLazyLoad) {
							return $ocLazyLoad.load([]);
						}]
					}
				}).state(
					'app.main.event.edit', {
						url: '/event_edit/{eventId}',
						templateUrl: 'views/event/detail.html?res_v=' + res_v,
						resolve: {
							deps: [
								'$ocLazyLoad',
								function ($ocLazyLoad) {
									return $ocLazyLoad.load(['./resources/js/controllers/event/eventImage.ctrl.js?res_v=' + res_v,
										'./resources/js/directives/event.directive.js?res_v=' + res_v,
										'./resources/js/filter/event.filter.js?res_v=' + res_v,
										'./resources/js/directives/validate/event.vali.js?res_v=' + res_v,
									]);
								}
							]
						}
					}).state('app.main.rostering', {
					abstract: true,
					template: '<div ui-view></div>'
				})

				.state(
					'app.main.rostering.rosteringMain', {
						templateUrl: 'views/rostering/rostering-main.html?res_v=' + res_v,
						resolve: {
							deps: [
								'$ocLazyLoad',
								function ($ocLazyLoad) {
									return $ocLazyLoad.load(['./resources/js/directives/ng-infinite-scroll/ng-infinite-scroll.js?res_v=' + res_v,
										'./resources/js/services/rostering.svr.js?res_v=' + res_v,
										'./resources/js/controllers/rostering/rostering.main.ctrl.js?res_v=' + res_v,
										'./resources/js/directives/rostering.directive.js?res_v=' + res_v,
									]);
								}
							]
						}
					})

				.state(
					'app.main.rostering.rosteringMain.staffListList', {
						url: '/rosteringStaffList',
						templateUrl: 'views/rostering/rostering-staff.html?res_v=' + res_v,
						resolve: {
							deps: [
								'$ocLazyLoad',
								function ($ocLazyLoad) {
									return $ocLazyLoad.load(['./resources/js/directives/ng-infinite-scroll/ng-infinite-scroll.js?res_v=' + res_v,
										'./resources/js/services/rostering.svr.js?res_v=' + res_v,
										'./resources/js/services/attendance.svr.js?res_v=' + res_v,
										'./resources/js/filter/attendance_children.filter.js?res_v=' + res_v,
										'./resources/js/filter/rosterStaff.filter.js?res_v=' + res_v,
										'./resources/js/controllers/rostering/rosteringStaffList.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/sign/visitorExport.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/sign/visitorEditSign.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/rostering/staffOtherEdit.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/rostering/staffRoster.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/sign/childSignSearch.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/rostering/saveTemplate.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/rostering/selectTemplate.ctrl.js?res_v=' + res_v,
										'./resources/js/directives/staff.directive.js?res_v=' + res_v,
										'./resources/js/directives/validate/sign.vali.js?res_v=' + res_v,
										'./resources/js/directives/validate/rostering.vali.js?res_v=' + res_v,
										'./resources/js/controllers/rostering/rejectAndCancelLeave.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/rostering/closurePeriod.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/rostering/payroll.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/rostering/addLeave.ctrl.js?res_v=' + res_v,
										'./resources/js/filter/common.filter.js?res_v=' + res_v,
									]);
								}
							]
						}
					})

				.state(
					'app.main.rostering.rosteringMain.shiftManagement', {
						url: '/shiftManagement',
						templateUrl: 'views/rostering/shiftManagement.html?res_v=' + res_v,
						resolve: {
							deps: [
								'$ocLazyLoad',
								function ($ocLazyLoad) {
									return $ocLazyLoad.load(['./resources/js/directives/ng-infinite-scroll/ng-infinite-scroll.js?res_v=' + res_v,
										'./resources/js/services/rostering.svr.js?res_v=' + res_v,
										'./resources/js/directives/shiftManage.dir.js?res_v=' + res_v,
										'./resources/js/filter/shiftManage.filter.js?res_v=' + res_v,
										'./resources/js/controllers/rostering/shiftManage.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/rostering/shiftTask.ctrl.js?res_v=' + res_v,
									]);
								}
							]
						}
					}).state(
					'app.main.rostering.leaveAbsence', {
						url: '/leaveAbsence',
						templateUrl: 'views/rostering/leave-absence/leave-absence-from-work.html?res_v=' + res_v,
						resolve: {
							deps: [
								'$ocLazyLoad',
								function ($ocLazyLoad) {
									return $ocLazyLoad.load(['./resources/js/controllers/rostering/leave-absence/staffRoster.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/rostering/leave-absence/leaveItem.ctrl.js?res_v=' + res_v,
										'./resources/js/services/rostering.svr.js?res_v=' + res_v,
										'./resources/js/directives/rostering.directive.js?res_v=' + res_v,
										'./resources/js/directives/validate/rostering.vali.js?res_v=' + res_v,
									]);
								}
							]
						}
					})

				.state(
					'app.main.rostering.rosteringMain.staffLeave', {
						url: '/staffLeave',
						templateUrl: 'views/rostering/staff-leave.html?res_v=' + res_v,
						resolve: {
							deps: [
								'$ocLazyLoad',
								function ($ocLazyLoad) {
									return $ocLazyLoad.load(['./resources/js/directives/ng-infinite-scroll/ng-infinite-scroll.js?res_v=' + res_v,
										'./resources/js/services/rostering.svr.js?res_v=' + res_v, './resources/js/filter/leave.filter.js?res_v=' + res_v,
										'./resources/js/directives/validate/rostering.vali.js?res_v=' + res_v,
										'./resources/js/controllers/rostering/staffLeave.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/rostering/addLeave.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/rostering/approveLeave.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/rostering/rejectAndCancelLeave.ctrl.js?res_v=' + res_v,
									]);
								}
							]
						}
					}).state(
					'app.main.rostering.rosteringMain.myLeave', {
						url: '/myLeave',
						templateUrl: 'views/rostering/my-leave.html?res_v=' + res_v,
						resolve: {
							deps: [
								'$ocLazyLoad',
								function ($ocLazyLoad) {
									return $ocLazyLoad.load(['./resources/js/directives/ng-infinite-scroll/ng-infinite-scroll.js?res_v=' + res_v,
										'./resources/js/services/rostering.svr.js?res_v=' + res_v, './resources/js/filter/leave.filter.js?res_v=' + res_v,
										'./resources/js/directives/validate/rostering.vali.js?res_v=' + res_v,
										'./resources/js/controllers/rostering/myLeave.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/rostering/applyLeave.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/confirm.ctrl.js?res_v=' + res_v,
									]);
								}
							]
						}
					})

				.state(
					'app.main.agenda', {
						url: '/meeting_agenda',
						templateUrl: 'views/meeting/meeting.agenda.html?res_v=' + res_v,
						resolve: {
							deps: [
								'$ocLazyLoad',
								function ($ocLazyLoad) {
									return $ocLazyLoad.load(['./resources/js/directives/ng-infinite-scroll/ng-infinite-scroll.js?res_v=' + res_v,
										'./resources/js/directives/validate/meeting.vali.js?res_v=' + res_v,
										'./resources/js/controllers/meeting/meeting.agenda.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/meeting/modal/agenda.type.ctrl.js?res_v=' + res_v,
										'./resources/js/services/meeting.svr.js?res_v=' + res_v, './resources/js/filter/meeting.filter.js?res_v=' + res_v,
										'./resources/js/directives/meeting.directive.js?res_v=' + res_v,
										'./resources/js/controllers/confirm.ctrl.js?res_v=' + res_v,
									]);
								}
							]
						}
					})

				.state(
					'app.main.template ', {
						url: '/meeting_template',
						templateUrl: 'views/meeting/meeting.template.html?res_v=' + res_v,
						resolve: {
							deps: [
								'$ocLazyLoad',
								function ($ocLazyLoad) {
									return $ocLazyLoad.load(['./resources/js/directives/ng-infinite-scroll/ng-infinite-scroll.js?res_v=' + res_v,
										'./resources/js/directives/validate/meeting.vali.js?res_v=' + res_v,
										'./resources/js/controllers/meeting/meeting.template.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/meeting/modal/meeting.frequency.ctrl.js?res_v=' + res_v,
										'./resources/js/services/meeting.svr.js?res_v=' + res_v, './resources/js/filter/meeting.filter.js?res_v=' + res_v,
										'./resources/js/directives/meeting.directive.js?res_v=' + res_v,
										'./resources/js/controllers/confirm.ctrl.js?res_v=' + res_v,
									]);
								}
							]
						}
					})

				.state(
					'app.main.manager ', {
						url: '/meeting_manager',
						templateUrl: './views/meeting/meeting.management.html?res_v=' + res_v,
						resolve: {
							deps: [
								'$ocLazyLoad',
								function ($ocLazyLoad) {
									return $ocLazyLoad.load(['./resources/js/directives/validate/meeting.vali.js?res_v=' + res_v,
										'./resources/js/controllers/meeting/meeting.manager.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/meeting/modal/meet.manager.edit.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/meeting/modal/meeting.table.edit.ctrl.js?res_v=' + res_v,
										'./resources/js/filter/attendance_children.filter.js?res_v=' + res_v,
										'./resources/js/services/meeting.svr.js?res_v=' + res_v, './resources/js/filter/meeting.filter.js?res_v=' + res_v,
										'./resources/js/directives/meeting.directive.js?res_v=' + res_v,
										'./resources/js/controllers/confirm.ctrl.js?res_v=' + res_v,
									]);
								}
							]
						}
					})

				.state(
					'app.main.dashboard ', {
						url: '/dashboard',
						templateUrl: './views/dashboard/dashboard.html?res_v=' + res_v,
						resolve: {
							deps: [
								'$ocLazyLoad',
								function ($ocLazyLoad) {
									return $ocLazyLoad.load(['./resources/js/controllers/dashboard/dashboard.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/confirm.ctrl.js?res_v=' + res_v,
										'./resources/js/services/dashboard.svr.js?res_v=' + res_v,
										'./resources/js/filter/dashboard.filter.js?res_v=' + res_v,
									]);
								}
							]
						}
					})

				.state(
					'app.main.center', {
						abstract: true,
						templateUrl: 'views/center/center.main.html?res_v=' + res_v,
						resolve: {
							deps: [
								'$ocLazyLoad',
								function ($ocLazyLoad) {
									return $ocLazyLoad.load(['./resources/js/directives/ng-infinite-scroll/ng-infinite-scroll.js?res_v=' + res_v,
										'./resources/js/controllers/center/center.ctrl.js?res_v=' + res_v,
										'./resources/js/directives/validate/center.vali.js?res_v=' + res_v,
										'./resources/js/controllers/center/center.modal.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/menu/menu.create.ctr.js?res_v=' + res_v,
										'./resources/js/filter/program.filter.js?res_v=' + res_v, './resources/js/services/center.svr.js?res_v=' + res_v,
										'./resources/js/services/menu.svr.js?res_v=' + res_v,
									]);
								}
							]
						}
					}).state('app.main.center.list', {
					url: '/center_list',
					templateUrl: 'views/center/list.html?res_v=' + res_v,
				}).state('app.main.center.edit', {
					url: '/center_edit/{centerId}',
					templateUrl: 'views/center/edit.html?res_v=' + res_v,
				}).state(
					'app.main.menuList', {
						url: '/menu_list',
						templateUrl: 'views/menu/list.html?res_v=' + res_v,
						resolve: {
							deps: [
								'$ocLazyLoad',
								function ($ocLazyLoad) {
									return $ocLazyLoad.load(['./resources/js/directives/ng-infinite-scroll/ng-infinite-scroll.js?res_v=' + res_v,
										'./resources/js/controllers/menu/menu.list.ctr.js?res_v=' + res_v,
										'./resources/js/controllers/menu/menu.create.ctr.js?res_v=' + res_v,
										'./resources/js/controllers/confirm.ctrl.js?res_v=' + res_v,
										'./resources/js/directives/validate/center.vali.js?res_v=' + res_v,
										'./resources/js/services/menu.svr.js?res_v=' + res_v,
									]);
								}
							]
						}
					}).state('app.main.attendance', {
					abstract: true,
					template: '<div ui-view></div>'
				}).state(
					'app.main.attendance.children', {
						url: '/attendance_children',
						templateUrl: 'views/user/family/attendance_children_list.html?res_v=' + res_v,
						resolve: {
							deps: [
								'$ocLazyLoad',
								function ($ocLazyLoad) {
									return $ocLazyLoad.load(['./resources/js/filter/attendance_children.filter.js?res_v=' + res_v,
										'./resources/js/filter/common.filter.js?res_v=' + res_v,
										'./resources/js/directives/attendance_children.directive.js?res_v=' + res_v,
										'./resources/js/controllers/attendance/attendance_children.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/attendance/attendance_change.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/attendance/attendance_choose.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/sign/visitorEditSign.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/sign/childSignSearch.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/sign/childExport.ctrl.js?res_v=' + res_v,
										'./resources/js/directives/validate/sign.vali.js?res_v=' + res_v,
										'./resources/js/services/attendance.svr.js?res_v=' + res_v,
									]);
								}
							]
						}
					}).state(
					'app.main.attendanceManagement', {
						url: '/attendance_management',
						templateUrl: 'views/user/family/attendance-management.html?res_v=' + res_v,
						resolve: {
							deps: [
								'$ocLazyLoad',
								function ($ocLazyLoad) {
									return $ocLazyLoad.load(['./resources/js/directives/ng-infinite-scroll/ng-infinite-scroll.js?res_v=' + res_v,
										'./resources/js/controllers/attendance/attendance_management.ctrl.js?res_v=' + res_v,
										'./resources/js/services/attendance_management.svr.js?res_v=' + res_v,
										'./resources/js/filter/attendanceManagement.filter.js?res_v=' + res_v,
										'./resources/js/filter/common.filter.js?res_v=' + res_v,
										'./resources/js/controllers/family/family_edit_absentee.ctrl.js?res_v=' + res_v,
										'./resources/js/services/absentee.svr.js?res_v=' + res_v,
										'./resources/js/directives/validate/program.vali.js?res_v=' + res_v,
										'./resources/js/directives/validate/family.vali.js?res_v=' + res_v,
										'./resources/js/directives/validate/parent.vali.js?res_v=' + res_v,
										'./resources/js/directives/validate/change_attendance.vali.js?res_v=' + res_v,
										'./resources/js/services/absentee.svr.js?res_v=' + res_v,
										'./resources/js/services/attend_child.giving.svr.js?res_v=' + res_v,
										'./resources/js/controllers/family/family_edit_givingNotice.ctrl.js?res_v=' + res_v,
										'./resources/js/services/attendance.svr.js?res_v=' + res_v,
										'./resources/js/controllers/family/family_edit_changeAttendance.ctrl.js?res_v=' + res_v,
										'./resources/js/filter/child.filter.js?res_v=' + res_v,
										'./resources/js/controllers/family/family_edit.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/logs/logs.list.ctrl.js?res_v=' + res_v,
										'./resources/js/services/logs.svr.js?res_v=' + res_v,
										'./resources/js/controllers/family/family_edit_requestLog.ctrl.js?res_v=' + res_v,
										'./resources/js/directives/ngBindUnsafe.directive.js?res_v=' + res_v,
										'./resources/js/controllers/family/family_edit_requestLogModal.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/family/family_edit_attendanceLog.ctrl.js?res_v=' + res_v,
										'./resources/js/services/family.svr.js?res_v=' + res_v,
										'./resources/js/directives/family.directive.js?res_v=' + res_v,
									])
								}
							]
						}
					}).state(
					'app.main.emaiLlist', {
						url: '/email_list',
						templateUrl: 'views/user/family/email_list.html?res_v=' + res_v,
						resolve: {
							deps: [
								'$ocLazyLoad',
								function ($ocLazyLoad) {
									return $ocLazyLoad.load(['./resources/js/controllers/attendance/email_list.ctrl.js?res_v=' + res_v,
										'./resources/js/services/email_list.svr.js?res_v=' + res_v,
										'./resources/js/controllers/family/family_edit_absentee.ctrl.js?res_v=' + res_v,
										'./resources/js/directives/validate/program.vali.js?res_v=' + res_v,
										'./resources/js/directives/validate/family.vali.js?res_v=' + res_v,
										'./resources/js/directives/validate/parent.vali.js?res_v=' + res_v,
										'./resources/js/directives/validate/change_attendance.vali.js?res_v=' + res_v,
										'./resources/js/services/absentee.svr.js?res_v=' + res_v,
										'./resources/js/services/attend_child.giving.svr.js?res_v=' + res_v,
										'./resources/js/controllers/family/family_edit_givingNotice.ctrl.js?res_v=' + res_v,
										'./resources/js/services/attendance.svr.js?res_v=' + res_v,
										'./resources/js/controllers/family/family_edit_changeAttendance.ctrl.js?res_v=' + res_v,
										'./resources/js/filter/child.filter.js?res_v=' + res_v,
										'./resources/js/controllers/family/family_edit.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/logs/logs.list.ctrl.js?res_v=' + res_v,
										'./resources/js/services/logs.svr.js?res_v=' + res_v,
										'./resources/js/controllers/family/family_edit_requestLog.ctrl.js?res_v=' + res_v,
										'./resources/js/directives/ngBindUnsafe.directive.js?res_v=' + res_v,
										'./resources/js/controllers/family/family_edit_requestLogModal.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/family/family_edit_attendanceLog.ctrl.js?res_v=' + res_v,
										'./resources/js/services/family.svr.js?res_v=' + res_v,
										'./resources/js/directives/family.directive.js?res_v=' + res_v,
										'./resources/js/directives/program.directive.js?res_v=' + res_v,
										'./resources/js/controllers/family/family_select.ctrl.js?res_v=' + res_v,
									])
								}
							]
						}
					}).state(
					'app.main.attendance.visitors', {
						url: '/attendance_visitors',
						templateUrl: 'views/sign/sign_list.html?res_v=' + res_v,
						resolve: {
							deps: [
								'$ocLazyLoad',
								function ($ocLazyLoad) {
									return $ocLazyLoad.load(['./resources/js/filter/common.filter.js?res_v=' + res_v,
										'./resources/js/filter/attendance_children.filter.js?res_v=' + res_v,
										'./resources/js/directives/attendance_children.directive.js?res_v=' + res_v,
										'./resources/js/controllers/sign/sign_list.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/sign/visitorExport.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/sign/visitorEditSign.ctrl.js?res_v=' + res_v,
										'./resources/js/directives/validate/sign.vali.js?res_v=' + res_v,
										'./resources/js/services/attendance.svr.js?res_v=' + res_v,
									])
								}
							]
						}
					}).state('app.main.calendar', {
					abstract: true,
					template: '<div ui-view></div>'
				}).state(
					'app.main.calendar.list', {
						url: '/calendar_view',
						templateUrl: 'views/calendar/list.html?res_v=' + res_v,
						resolve: {
							deps: [
								'$ocLazyLoad',
								function ($ocLazyLoad) {
									return $ocLazyLoad.load(['./resources/js/filter/common.filter.js?res_v=' + res_v,
										'./resources/js/filter/leave.filter.js?res_v=' + res_v, './resources/js/filter/calendar.filter.js?res_v=' + res_v,
										'./resources/js/directives/attendance_children.directive.js?res_v=' + res_v,
										'./resources/js/controllers/calendar/calendar.ctrl.js?res_v=' + res_v,
										'./resources/js/controllers/calendar/chooseColor.ctrl.js?res_v=' + res_v,
										'./resources/js/services/calendar.svr.js?res_v=' + res_v,
									]);
								}
							]
						}
					}).state('app.main.form', {
					abstract: true,
					url: '/form',
					template: '<div ui-view></div>'
				}).state('app.main.form.view', {
					url: '/view/:code/:instanceId',
					templateUrl: 'views/form-builder/view.html?res_v=' + res_v,
					resolve: {
						deps: ['$ocLazyLoad', function ($ocLazyLoad) {
							return $ocLazyLoad.load(['./resources/js/directives/select2.dir.js?res_v=' + res_v, ]);
						}]
					}
				});
		}
	]).config(['$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
	$ocLazyLoadProvider.config({
		debug: false,
		events: false
	});
}]);