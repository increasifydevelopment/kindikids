'use strict';

function confirmCtrl($scope, $uibModalInstance, confirmObj) {

	$scope.confirmObj = confirmObj;

	$scope.okFunction = function() {
		if ($scope.confirmObj.ok.callback != null) {
			$scope.confirmObj.ok.callback();
		}
		$uibModalInstance.dismiss('cancel');
	};

	$scope.cancelFunction = function() {
		if ($scope.confirmObj.cancel.callback != null) {
			$scope.confirmObj.cancel.callback();
		}
		$uibModalInstance.dismiss('cancel');
	};
	$scope.close = function() {
		$uibModalInstance.dismiss('cancel');
	};
};

angular.module('kindKidsApp').controller('confirmCtrl',
		[ '$scope', '$uibModalInstance', 'confirmObj', confirmCtrl ]);