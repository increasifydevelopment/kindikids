'use strict';

/**
 * @author abliu
 * @description attendance Controller
 */
function attendanceCtrl($scope, $rootScope, $timeout, $filter, $window, attendanceService, commonService, $uibModal) {
	// variable=================================================================
	$scope.page = {};
	$scope.page.condition = {
		keyword: null,
		time: null,
		roomId: null,
		centersId: null,
		type: 2, // type 0:传日期进行搜索，1：上一周 2：本周 3：下一周 4: 左侧菜单，保留weekdate查询
		weekDate: null
	};
	// 判断本周最大列集合数
	$scope.maxRowLength = 0;
	$scope.maxRowList = [];
	$scope.weekRowList = [];
	$scope.maxRowListIndex = 0;
	$scope.currentRepeadChildList = null;
	$scope.sourceLength = 0;
	$scope.yearList = [];
	$scope.isCenterManager = $filter('haveRole')($scope.currentUser.roleInfoVoList, '1;5');

	$scope.functions = {};
	$scope.getWattingListFun = function () {
		commonService.getFun("/waiting_list").then(angular.bind(this, function then() {
			$scope.functions = commonService.functions;
		}));
	};
	/**
	 * @author abliu
	 * @description
	 * @returns 获取attendance列表
	 */
	$scope.getAttendanceList = function () {
		$scope.showLoading("#mainList");
		// 园长默认选择当前园add by gfwang
		if (($scope.isCenterManager || $filter('haveRole')($scope.currentUser.roleInfoVoList, '2')) && $scope.currentUser.userInfo.centersId) {
			$scope.page.condition.centersId = $scope.currentUser.userInfo.centersId;
		}
		attendanceService.getAttendanceList($scope.page.condition).then(angular.bind(this, function then() {
			$("#searchDiv").removeClass("open");
			$scope.hideLoading("#mainList");
			var result = attendanceService.listResult;
			if (result.code == 0) {
				getMaxRowLength(result.data.weekDays);
				$scope.page = result.data;
			} else {
				$rootScope.message(result.msg);
			}
		}));
	};

	function getMaxRowLength(list) {
		$scope.maxRowLength = 0;
		$scope.maxRowListIndex = 0;
		$scope.maxRowList = [];
		$scope.weekRowList = [];
		for (var i = 0, m = list.length; i < m; i++) {
			var length = list[i].attendanceChilds.length;
			if (length > $scope.maxRowLength) {
				$scope.maxRowLength = length;
			}
		}

		for (var i = 0, m = $scope.maxRowLength; i < m; i++) {
			$scope.maxRowList.push({
				"id": $scope.sourceLength
			});
			$scope.sourceLength++;
		}
		for (var i = 0, m = 7; i < m; i++) {
			$scope.weekRowList.push({
				"id": i + 1 + $scope.sourceLength
			});
			$scope.sourceLength++;
		}

	}


	$scope.isStart = true;
	/**
	 * @author abliu
	 * @description
	 * @returns 获取园区及room集合
	 */
	$scope.getMenuOrgs = function () {
		commonService.getMenuOrgs(4).then(angular.bind(this, function then() {
			$scope.orgList = commonService.orgList;
			if ($scope.isStart) {
				$scope.page.condition.centersId = $scope.orgList.centersInfos[0].id;
				$scope.isStart = false;
			}
			$scope.getAttendanceList();
		}));
	};

	/**
	 * @author abliu
	 * @description
	 * @returns 设置年份集合，从2010年开始到当前年加1年
	 */
	$scope.getYearList = function () {
		var yyyy = new Date().getFullYear();
		var m = yyyy - 2015;
		for (var i = 0; i <= m + 1; i++) {
			$scope.yearList.push(2015 + i);
		}
	};

	/**
	 * @author abliu
	 * @description
	 * @returns 设置年份
	 */
	$scope.setTimeY = function (yyyy) {
		if ($scope.page.condition.time != null) {
			var times = $scope.page.condition.time.split("/");
			if (times.length > 1) {
				$scope.page.condition.time = yyyy + "/" + times[1];
				$scope.page.condition.type = 0;
				$scope.getAttendanceList();
			}
		}
	};

	/**
	 * @author abliu
	 * @description
	 * @returns 设置月份
	 */
	$scope.setTimeM = function (mm) {
		if ($scope.page.condition.time != null) {
			var times = $scope.page.condition.time.split("/");
			if (times.length > 1) {
				$scope.page.condition.time = times[0] + "/" + mm;
				$scope.page.condition.type = 0;
				$scope.getAttendanceList();
			}
		}
	};

	/**
	 * @author abliu
	 * @description
	 * @returns 设置type
	 */
	$scope.setType = function (type) {
		$scope.page.condition.type = type;
		$scope.getAttendanceList();
	};

	/**
	 * @author abliu
	 * @description
	 * @returns 打开替换上课 modalType:0为新增 1为替换 2为去除
	 */
	$scope.openChange = function (currentChooseUser, day, modalType) {


		if (modalType == 1 && !$filter('haveFunction')($scope.functions, 'attendance_replace_child')) {
			//没有权限
			$rootScope.message("No permissions");
			return;
		}

		var absenteeId = currentChooseUser.absenteeId;
		var roomId = $scope.page.condition.roomId;
		var week = new moment(day).format("dddd");
		$uibModal.open({
			templateUrl: 'views/user/family/attendance_children_change.html?res_v=' + $scope.res_v,
			controller: 'attendanceChangeCtrl',
			backdrop: 'static',
			keyboard: false,
			windowClass: 'popup-att',
			resolve: {
				currentChooseUser: function () {
					return currentChooseUser;
				},
				absenteeId: function () {
					return absenteeId;
				},
				day: function () {
					return day;
				},
				modalType: function () {
					return modalType;
				},
				roomId: function () {
					return roomId == "" ? currentChooseUser.roomId : roomId;
				},
				week: function () {
					return week;
				},
				callbackFunction: function () {
					return $scope.getAttendanceListCallback;
				},
				interim: function () {
					return currentChooseUser.interim;
				}
			}
		});
	};

	$scope.getAttendanceListCallback = function () {
		$scope.getMenuOrgs();
		$scope.page.condition.type = 4;
		$scope.getAttendanceList();
	};

	/**
	 * @author zhengguo
	 * @description 打开签到信息的编辑框 absenteeId 需要签到信息的ID
	 * @returns 保存编辑结果
	 */
	$scope.openEditSign = function (childId, inDate, interim) {
		var info = {
			id: childId,
			inDate: inDate,
			interim: interim
		};
		$uibModal.open({
			templateUrl: 'views/sign/visitor-edit-sign.html?res_v=' + $scope.res_v,
			controller: 'visitorEditSignCtrl',
			backdrop: 'static',
			keyboard: false,
			windowClass: 'visitor-sign-out-popup modal-align-top',
			resolve: {
				signId: function () {
					return info;
				},
				centerId: function () {
					return '';
				}
			}
		}).result.then(function (flag) {
			if (flag == 1) {
				$scope.getMenuOrgs();
				$scope.getAttendanceListCallback();
			}
		});
	};

	/**
	 * @author zhengguo
	 * @description 打开小孩签到的搜索框
	 * @returns 通过选择的小孩的确认调用签到编辑modal方法 $scope.openEditSign(childId); type : 0
	 *          child
	 */
	$scope.childSignin = function () {
		var modalInstance = $uibModal.open({
			templateUrl: 'views/sign/child-sign-search.html?res_v=' + $scope.res_v,
			controller: 'childSignSearchCtrl',
			backdrop: 'static',
			keyboard: false,
			/* windowClass:'visitor-sign-out-popup modal-align-top', */
			resolve: {
				centerId: function () {
					return $scope.page.condition.centersId;
				},
				roomId: function () {
					return $scope.page.condition.roomId;
				},
				type: function () {
					return 0;
				}
			}
		}).result.then(function (childId) {
			if (childId) {
				// 加上当前时间："2016-09-29T19:49:53.000"
				// 2016-09-29T00:00:00.000
				/*
				 * var now = new Date(); var nowMonth = now.getMonth() < 9 ?
				 * '0'+(now.getMonth()+1) : (now.getMonth()+1); var nowDate =
				 * now.getFullYear()+"-"+nowMonth+"-"+now.getDate()+"T00:00:00.000";
				 */
				var nowDate = null;
				$scope.getMenuOrgs();
				$scope.openEditSign(childId, nowDate);
			}
		});
	}

	/**
	 * @author zhengguo
	 * @description 打开小孩签到导出的modal框
	 * @returns 调用导出url
	 */
	$scope.childExport = function () {
		var modalInstance = $uibModal.open({
			templateUrl: 'views/sign/childExport.html?res_v=' + $scope.res_v,
			controller: 'childExportCtrl',
			backdrop: 'static',
			keyboard: false,
			/* windowClass:'visitor-sign-out-popup modal-align-top', */
			resolve: {
				orgId: function () {
					return !$scope.page.condition.roomId ? $scope.page.condition.centersId : $scope.page.condition.roomId;
				}
			}
		}).result.then(function (exportData) {
			if (exportData) {
				$scope.exportCondition = {};
				$scope.exportCondition.centreId = $scope.page.condition.centersId;
				$scope.exportCondition.roomId = $scope.page.condition.roomId;
				if (exportData.flag == 1) {
					$scope.exportCondition.accountId = exportData.id;
				}
				$scope.exportCondition.startDate = exportData.startDate;
				$scope.exportCondition.endDate = exportData.endDate;
			}
			attendanceService.childExportExcel($scope.exportCondition).then(angular.bind(this, function then() {
				$scope.childUuidResult = attendanceService.getChildUUIDResult;
				// 继续导出excel
				$window.location.href = "./sign/childExcel.do?uuid=" + $scope.childUuidResult.data;
			}));
		});
	}
	$scope.isOrganisation = function () {
		if (($scope.page.condition.centersId == null || $scope.page.condition.centersId == '') && ($scope.page.condition.roomId == null || $scope.page.condition.roomId == '')) {
			return true;
		} else {
			return false;
		}
	}
}
angular.module('kindKidsApp').controller('attendanceCtrl',
	['$scope', '$rootScope', '$timeout', '$filter', '$window', 'attendanceService', 'commonService', '$uibModal', attendanceCtrl]);