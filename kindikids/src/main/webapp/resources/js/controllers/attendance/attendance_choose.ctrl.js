'use strict';

/**
 * @author abliu
 * @description attendanceChooseCtrl Controller
 */
function attendanceChooseCtrl($scope, $rootScope, $timeout, $window, $uibModalInstance, attendanceService, commonService, internal, siblings, external, callbackFunction, chooseId, chooseType, allInternal) {
	$scope.internal = internal;
	$scope.siblings = siblings;
	$scope.external = external;
	$scope.callbackFunction = callbackFunction;
	$scope.chooseId = chooseId;
	$scope.chooseType = chooseType;

	$scope.internal = $scope.internal.concat(allInternal);

	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};

	$scope.confirm = function () {
		var obj = {};
		for (var i = 0, m = $scope.internal.length; i < m; i++) {
			if ($scope.internal[i].id == $scope.chooseId) {
				obj = $scope.internal[i];
				$scope.callbackFunction(obj);
				$uibModalInstance.dismiss('cancel');
				return;
			}
		}
		for (var i = 0, m = $scope.siblings.length; i < m; i++) {
			if ($scope.siblings[i].id == $scope.chooseId) {
				obj = $scope.siblings[i];
				$scope.callbackFunction(obj);
				$uibModalInstance.dismiss('cancel');
				return;
			}
		}
		for (var i = 0, m = $scope.external.length; i < m; i++) {
			if ($scope.external[i].id == $scope.chooseId) {
				obj = $scope.external[i];
				$scope.callbackFunction(obj);
				$uibModalInstance.dismiss('cancel');
				return;
			}
		}

	};

}
angular.module('kindKidsApp').controller('attendanceChooseCtrl', ['$scope', '$rootScope', '$timeout', '$window', '$uibModalInstance', 'attendanceService', 'commonService', 'internal', 'siblings', 'external', 'callbackFunction', 'chooseId', 'chooseType', 'allInternal', attendanceChooseCtrl]);