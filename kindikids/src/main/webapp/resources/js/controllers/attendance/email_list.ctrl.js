'use strict';

function emailListCtrl($scope, $rootScope, $timeout, $window, $uibModal, emailListSvr) {
    $scope.condition = {
        maxSize: 7,
        pageIndex: 1,
        status: -1,
    };

    $scope.statuseOption = [{
        "value": -1,
        "text": "All"
    }, {
        "value": 1,
        "text": "Sent"
    }, {
        "value": 0,
        "text": "Unsent"
    }];

    $scope.getList = function () {
        $scope.showLoading('#mainList');
        emailListSvr.getList($scope.condition).then(angular.bind(this, function then() {
            $scope.hideLoading('#mainList');
            var result = emailListSvr.pageListResult;
            if (result.code == 0) {
                $scope.list = result.data.list;
                $scope.condition = result.data.condition;
            }
        }));
    }

    $scope.changePage = function (pageIndex) {
        $scope.condition.pageIndex = pageIndex;
        $scope.getList();
    }

    $scope.keyEvent = function (ev) {
        if (ev.keyCode == 13) {
            $scope.changePage(1);
            $("input").blur();
        }
    };

    $scope.$watch('condition.status', function (newValue, oldValue) {
        if (newValue == undefined || oldValue == undefined || newValue == oldValue) {
            return;
        }
        $scope.changePage(1);
    }, true);

    $scope.$watch('checkAll', function (newValue) {
        if (newValue) {
            $scope.condition.checkIds = $scope.condition.allIds;
        } else {
            $scope.condition.checkIds = [];
        }

    }, true);

    $scope.condition.checkIds = [];
    $scope.checkFun = function (id, e) {
        if ($scope.condition.checkIds.indexOf(id) == -1) {
            $scope.condition.checkIds.push(id);
        } else {
            $scope.condition.checkIds.splice($scope.condition.checkIds.indexOf(id), 1);
        }
        e.stopPropagation();
    }

    $scope.sendPlanEmail = function () {
        emailListSvr.sendPlanEmail($scope.condition).then(angular.bind(this, function then() {
            var result = emailListSvr.sendPlanEmailResult;
            if (result.code == 0) {
                $scope.condition.checkIds = [];
                $scope.checkAll = false;
                $scope.changePage(1);
            }
            $rootScope.message(result.msg);
        }))
    }

    $scope.openProfile = function (familyId) {
        $uibModal.open({
            templateUrl: 'views/user/family/child_edit.html?res_v=' + $scope.res_v,
            controller: 'userFamilyEditCtrl',
            animation: true,
            size: "lg",
            backdrop: 'static',
            keyboard: false,
            windowClass: "profile-popup profile-popup-family modal-align-top",
            resolve: {
                familyId: function () {
                    return familyId;
                },
                functions: function () {
                    return $scope.functions;
                },
                currentUser: function () {
                    return $scope.currentUser;
                },
                callbackReload: function () {
                    return $scope.getList;
                }
            }
        });
    };
}
angular.module('kindKidsApp').controller(
    'emailListCtrl',
    ['$scope', '$rootScope', '$timeout', '$window', '$uibModal', 'emailListSvr', emailListCtrl]);