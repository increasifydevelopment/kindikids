'use strict';

/**
 * @author abliu
 * @description attendanceChangeCtrl Controller
 */
function attendanceChangeCtrl($scope, $rootScope, $timeout, $window, $uibModal, $uibModalInstance, attendanceService,
	commonService, currentChooseUser, absenteeId, day, callbackFunction, modalType, roomId, week, interim) {
	$scope.modalType = modalType;
	$scope.week = week;
	$scope.currentChooseUser = currentChooseUser;
	$scope.childId = currentChooseUser.childId;
	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
	$scope.close = function () {
		$scope.confirm('Close', 'Exit this page?', 'OK', function () {
			$scope.cancel();
		}, 'NO');
	};
	$scope.overOld = false;
	$scope.currentChildRequest = "";

	//如果圆满是否提示
	$scope.alertOver = true;
	//是否处理统一安排的情况
	$scope.dealAll = false;
	//是否替换覆盖原来的
	$scope.overOld = false;

	//减去永久是否覆盖
	$scope.coverAll = false;

	//添加
	$scope.approveAddCondition = {
		roomId: "",
		childId: "",
		day: "",
		addForMax: false,
		alertOther: false,
		dealAll: false,
		addForMaxAll: false,
		coverOld: false
	};

	//初始化方法
	$scope.initAttendanceChange = function () {
		//加载当前请假小孩信息
		attendanceService.replaceChildAttendance(absenteeId, $scope.childId, day).then(angular.bind(this, function then() {
			var result = attendanceService.replaceChildAttendanceObj;
			if (result.code == 0) {
				//加载可选择第一个小孩的信息

				//加载页面信息
				$scope.info = result.data;
				//
				if ($scope.info.internal.length > 0) {
					$scope.currentChildRequest = $scope.info.internal[0];
				}

			} else {
				$rootScope.message(result.msg);
			}
		}));

	};

	/**
	 * @author abliu
	 * @description
	 * @returns 初始化新增人员
	 */
	$scope.initAttendanceAdd = function () {
		//加载当前请假小孩信息
		attendanceService.getAddChilds(roomId, day).then(angular.bind(this, function then() {
			var result = attendanceService.getAddChildsObj;
			if (result.code == 0) {
				//加载可选择第一个小孩的信息				
				//加载页面信息
				$scope.info = result.data;
				//
				if ($scope.info.internal.length > 0) {
					$scope.currentChildRequest = $scope.info.internal[0];
				} else {
					if ($scope.info.siblings.length > 0) {
						$scope.currentChildRequest = $scope.info.siblings[0];
					} else {
						if ($scope.info.external.length > 0) {
							$scope.currentChildRequest = $scope.info.external[0];
						}
					}
				}
			} else {
				$rootScope.message(result.msg);
			}
		}));
	};

	/**
	 * @author abliu
	 * @description
	 * @returns 打开选择人员 type:0 替换小孩选择 1 新增小孩选择
	 */
	$scope.openChoose = function (type) {
		var modalInstance = $uibModal.open({
			templateUrl: 'views/user/family/attendance_children_choose.html?res_v=' + $scope.res_v,
			controller: 'attendanceChooseCtrl',
			backdrop: 'static',
			keyboard: false,
			transclude: false,
			resolve: {
				internal: function () {
					return $scope.info.internal;
				},
				siblings: function () {
					return $scope.info.siblings;
				},
				external: function () {
					return $scope.info.external;
				},
				callbackFunction: function () {
					return $scope.chooseCallback;
				},
				chooseId: function () {
					return $scope.currentChildRequest.id;
				},
				chooseType: function () {
					return type;
				},
				allInternal: function () {
					return $scope.info.allInternal
				}
			}
		});
	};

	/**
	 * @author abliu
	 * @description
	 * @returns 选择人员后返回
	 */
	$scope.chooseCallback = function (obj) {
		$scope.currentChildRequest = obj;
	};

	/**
	 * @author abliu
	 * @description
	 * @returns 替换  type 0:today 1:代表周
	 */
	$scope.approveReplace = function (type, dayOrInstances) {
		$scope.overOld = false;
		$scope.approveReplaceSubmit(type, dayOrInstances);
	};
	$scope.approveReplaceSubmit = function (type, dayOrInstances) {
		// String absenteeId, String oldChildId, Date day, int dayOrInstances, int type
		attendanceService.approveReplace($scope.currentChildRequest.requestId, $scope.childId, day, dayOrInstances, type, $scope.overOld).then(angular.bind(this, function then() {
			var result = attendanceService.approveReplaceObj;
			if (result.code == 0) {
				//加载可选择第一个小孩的信息
				//加载页面信息
				callbackFunction();
				$scope.cancel();
			} else if (result.code == 101) {
				$rootScope.confirm('', result.msg, 'Confirm', function () {
					$scope.overOld = true;
					$scope.approveReplaceSubmit(type, dayOrInstances);
				}, 'Cancel', null);
			} else {
				$rootScope.message(result.msg);
			}
		}));
	};;

	/**
	 * @author abliu
	 * @description
	 * @returns 替换  
	 */
	$scope.approveAdd = function () {
		$scope.approveAddCondition.addForMax = false;
		$scope.approveAddCondition.alertOther = false;
		$scope.approveAddCondition.dealAll = false;
		$scope.approveAddCondition.coverOld = false;
		$scope.approveAddCondition.addForMaxAll = false;
		$scope.approveAddSubmit();
	};

	$scope.approveAddSubmit = function () {
		$scope.approveAddCondition.roomId = roomId;
		$scope.approveAddCondition.childId = $scope.currentChildRequest.id;
		$scope.approveAddCondition.day = day;
		$scope.approveAddCondition.interim = $scope.currentChildRequest.interim;
		attendanceService.approveAdd($scope.approveAddCondition).then(angular.bind(this, function then() {
			var result = attendanceService.approveAddObj;
			if (result.code == 0) {
				//加载可选择第一个小孩的信息
				//加载页面信息
				callbackFunction();
				$scope.cancel();
			}
			//园满提醒
			else if (result.code == 1) {
				$rootScope.confirm('', result.msg, 'Confirm', function () {
					$scope.approveAddCondition.addForMax = true;
					$scope.approveAddSubmit();
				}, 'Cancel', null);
			}
			//是否全部安排提醒
			else if (result.code == 2) {
				$rootScope.confirm('', result.msg, 'Approve Permanently', function () {
					$scope.approveAddCondition.alertOther = true;
					$scope.approveAddCondition.dealAll = true;
					$scope.approveAddCondition.addForMax = false;
					$scope.approveAddSubmit();
				}, 'Approve for Today', function () {
					$scope.approveAddCondition.alertOther = true;
					$scope.approveAddCondition.dealAll = false;
					$scope.approveAddSubmit();
				});
			}
			//是否有未来申请在
			else if (result.code == 3) {
				$rootScope.confirm('', result.msg, 'Confirm', function () {
					$scope.approveAddCondition.coverOld = true;
					$scope.approveAddSubmit();
				}, 'Cancel', null);
			}
			//全部安排的时候 是否满园
			else if (result.code == 4) {
				$rootScope.confirm('', result.msg, 'Confirm', function () {
					$scope.approveAddCondition.addForMaxAll = true;
					$scope.approveAddSubmit();
				}, 'Cancel', null);
			}
			// 临时排课无申请时确认提示.
			else if (result.code == 5) {
				$rootScope.confirm('', result.msg, 'Confirm', function () {
					$scope.approveAddCondition.confirm = true;
					$scope.approveAddSubmit();
				}, 'Cancel', null);
			}
			// 临时排课当天已存在排课错误提示.
			else if (result.code == 6) {
				$rootScope.confirm('', result.msg, 'Cancel', null);
			} else {
				$rootScope.message(result.msg);
			}
		}));
	};

	$scope.approveDel = function (type) {
		$scope.coverAll = false;
		$scope.approveDelSubmit(type);
	};

	$scope.approveDelSubmit = function (type) {
		//type:0 代表全部  1：代表今天
		attendanceService.subtractedAttendance(roomId, $scope.childId, day, type, $scope.coverAll, interim).then(angular.bind(this, function then() {
			var result = attendanceService.subtractedAttendanceObj;
			if (result.code == 0) {
				//刷新日历
				callbackFunction();
				$scope.cancel();
			} else if (result.code == 3) {
				$rootScope.confirm('', result.msg, 'Confirm', function () {
					$scope.coverAll = true;
					$scope.approveDelSubmit(type);
				}, 'Cancel', null);
			} else {
				$rootScope.message(result.msg);
			}
		}));
	};

	/**
	 * @author zhengguo
	 * @description 打开签到信息的编辑框
	 * absenteeId 需要签到信息的ID
	 * @returns 保存编辑结果
	 */
	$scope.openChange = function (signId) {
		var modalInstance = $uibModal.open({
			templateUrl: 'views/sign/visitor-edit-sign.html?res_v=' + $scope.res_v,
			controller: 'visitorEditSignCtrl',
			backdrop: 'static',
			keyboard: false,
			windowClass: 'popup-att',
			resolve: {
				signId: function () {
					return signId;
				},
				centerId: function () {
					return '';
				},
				callbackFunction: function () {
					return /*$scope.getAttendanceList*/;
				}
			}
		});
	};

}
angular.module('kindKidsApp').controller('attendanceChangeCtrl', ['$scope', '$rootScope', '$timeout', '$window',
	'$uibModal', '$uibModalInstance', 'attendanceService', 'commonService', 'currentChooseUser', 'absenteeId', 'day', 'callbackFunction', 'modalType', 'roomId', 'week', 'interim', attendanceChangeCtrl
]);