'use strict';
/**
 * @author hxzhang
 * @description attendance_management.ctrl
 */
function attendanceManagementCtrl($scope, $rootScope, $timeout, $window, $uibModal, attendanceManagementSvr, familyService, absebteeService,
	givingNoticeService, attendanceService) {

	var title = "Warning";
	var msgCancle = "If you cancel this request, you will lose your position in the queue.";
	var msgDecline = "If you decline this request, your existing queue position will be lost.";
	var msgCancle2 = "Are you sure you want to cancel this request?";
	var msgDecline2 = "Are you sure you want to decline this request?";

	// 页面list参数对象
	// type : null All Type
	// type : 0 Giving Notice
	// type : 1 Absence Request
	// type : 2 Change of Room
	// type : 3 Change of Centre
	// type : 4 Change of Attendance

	$scope.initGetPagarList = function () {
		$scope.condition = {
			type: null,
			maxSize: 7,
			pageIndex: 1,
		};
		$scope.condition2 = {
			type: null,
			maxSize: 7,
			pageIndex: 1,
		};
		$scope.getPageList();
	};

	$scope.getPageList = function () {
		$scope.showLoading('#mainList');
		attendanceManagementSvr.getPageList($scope.condition).then(angular.bind(this, function then() {
			$scope.hideLoading('#mainList');
			$scope.result = attendanceManagementSvr.pageListResult;
			if ($scope.result.code == 0) {
				$scope.list = $scope.result.data.list;
				$scope.condition.type = $scope.result.data.condition.type;
				$scope.condition.pageIndex = $scope.result.data.condition.pageIndex;
				$scope.condition.pageSize = $scope.result.data.condition.pageSize;
				$scope.condition.totalSize = $scope.result.data.condition.totalSize;
				if ($scope.logIndex != -1) {
					$scope.requestLogVos.obj[0] = $scope.list[$scope.logIndex];
				}
			}
		}));
	};

	//外部小孩列表
	$scope.getPageListExternal = function () {
		$scope.showLoading('#mainList');
		$scope.condition2.sortName = $scope.sortByColumn.column;
		$scope.condition2.sortOrder = $scope.sortByColumn.order;
		attendanceManagementSvr.getPageListExternal($scope.condition2).then(angular.bind(this, function then() {
			$scope.hideLoading('#mainList');
			var result = attendanceManagementSvr.pageListExternalResult;
			if ($scope.result.code == 0) {
				$scope.listExternal = result.data.list;
				$scope.condition2.type = result.data.condition.type;
				$scope.condition2.pageIndex = result.data.condition.pageIndex;
				$scope.condition2.pageSize = result.data.condition.pageSize;
				$scope.condition2.totalSize = result.data.condition.totalSize;
			}
		}));
	};
	// 排序
	$scope.sortByColumn = {
		column: 'aa.appDate,aa.`NAME`',
		order: 'desc',
		flag: true,
		fun: function (column) {
			if ($scope.sortByColumn.flag) {
				$scope.sortByColumn.order = "asc";
				$scope.sortByColumn.flag = false;
			} else {
				$scope.sortByColumn.order = "desc";
				$scope.sortByColumn.flag = true;
			}
			$scope.sortByColumn.column = column;
			$scope.getPageListExternal();
		}
	}

	$scope.$watch('condition.appDate', function (newValue, oldValue) {
		if (newValue == undefined || 'Invalid date' == newValue) {
			return;
		}
		$scope.changePage(1);
	}, true);

	$scope.$watch('condition.opeDate', function (newValue, oldValue) {
		if (newValue == undefined || 'Invalid date' == newValue) {
			return;
		}
		$scope.changePage(1);
	}, true);

	$scope.$watch('condition2.reqDates', function (newValue, oldValue) {
		if (newValue == undefined) {
			return;
		}
		$scope.changePage(1);
	}, true);
	$scope.$watch('condition2.age', function (newValue, oldValue) {
		if (newValue == undefined) {
			return;
		}
		$scope.changePage(1);
	}, true);
	$scope.changePage = function (pageIndex) {
		if ($scope.condition.type == 5) {
			$scope.condition2.pageIndex = pageIndex;
			$scope.getPageListExternal();
		} else {
			$scope.condition.pageIndex = pageIndex;
			$scope.getPageList();
		}

	};
	$scope.showLiveAge = false;
	$scope.chooseType = function (type) {
		$scope.condition.type = type;
		if (type == 5) {//如果为外部小孩
			$scope.getPageListExternal();
			$scope.showLiveAge = true;
		} else {
			$scope.getPageList();
			$scope.showLiveAge = false;
		}
	};

	$scope.operateModal = function (model, value) {
		switch (value) {
			case 0:// approved申请
				if (model.logType == 0) {
					var scope = $scope.$new();
					scope.childInfo = model.childVo;
					scope.givingNoticeId = model.objId;
					scope.givingNoticeStatus = 3;
					$uibModal.open({
						templateUrl: 'views/user/family/giving_notice.html?res_v=' + $scope.res_v,
						controller: 'givingNoticeCtrl',
						backdrop: 'static',
						keyboard: false,
						windowClass: "profile-popup",
						scope: scope
					});
				}
				if (model.logType == 1) {
					var scope = $scope.$new();
					scope.childInfo = model.childVo;
					scope.absenteeId = model.objId;
					scope.absenteeStatus = 3;
					$uibModal.open({
						templateUrl: 'views/user/family/attendance_child_absentee.html?res_v=' + $scope.res_v,
						controller: 'absentCtrl',
						backdrop: 'static',
						keyboard: false,
						windowClass: "profile-popup",
						scope: scope
					});
				}
				if (model.logType == 3) {
					$scope.openRequest(model.objId, 3, false);
				}
				break;
			case 1:// Reject申请
				if (model.logType == 0) {
					$scope.confirm(title, msgDecline2, 'Yes', function () {
						givingNoticeService.declineGivingNotice(model.objId).then(angular.bind(this, function then() {
							var result = givingNoticeService.declineResult;
							$rootScope.message(result.msg);
							$scope.getPageList();
						}));
					}, 'No', null);
				}
				if (model.logType == 1) {
					$scope.confirm(title, msgDecline2, 'Yes', function () {
						absebteeService.declineAbsebtee(model.objId).then(angular.bind(this, function then() {
							var result = absebteeService.declineResult;
							$rootScope.message(result.msg);
							$scope.getPageList();
						}));
					}, 'No', null);
				}
				if (model.logType == 2) {
					$scope.operateRequest(model, 4);
				}
				if (model.logType == 3) {
					$scope.operateRequest(model, 4);
				}
				if (model.logType == 4) {
					$scope.operateRequest(model, 4);
				}
				break;
			case 2:// Cancel申请
				if (model.logType == 0) {
					$scope.confirm(title, msgCancle2, 'Yes', function () {
						givingNoticeService.cancelGivingNotice(model.objId).then(angular.bind(this, function then() {
							var result = givingNoticeService.cancleResult;
							$rootScope.message(result.msg);
							$scope.getPageList();
						}));
					}, 'No', null);
				}
				if (model.logType == 1) {
					$scope.confirm(title, msgCancle2, 'Yes', function () {
						absebteeService.cancelAbsebtee(model.objId).then(angular.bind(this, function then() {
							var result = absebteeService.cancleResult;
							$rootScope.message(result.msg);

							$scope.getPageList();
						}));
					}, 'No', null);
				}
				if (model.logType == 2) {
					$scope.operateRequest(model, 1);
				}
				if (model.logType == 3) {
					$scope.operateRequest(model, 1);
				}
				if (model.logType == 4) {
					$scope.operateRequest(model, 1);
				}
				break;

			default:
				break;
		}
	}

	$scope.changeAttendanceCallback = function (childId, type) {
		if (modalInstance) {
			modalInstance.close();
		}
		if ($scope.condition.type == 5) {
			$scope.getPageListExternal();
		} else {
			$scope.getPageList($scope.requestLogVos);
		}
	};


	$scope.operateRequest = function (model, status) {
		if (model.type == 10 || model.type == 11) {
			$scope.operateStateTemporary(model.type, model.id, status, model)
		}
		if (model.type == 0 || model.type == 1 || model.type == 2 || model.type == 4 || model.type == 5) {
			$scope.operateState(model.objId, 1, model);
		}
	}

	$scope.operateState = function (objId, state, requestlog) {
		$scope.confirm(title, msgCancle, 'Yes', function () {
			// 更新后台请求状态
			attendanceService.updateAttendanceRequest(objId, state).then(angular.bind(this, function then() {
				var result = attendanceService.updateAttendanceRequestObj;
				if (result.code == 0) {
					requestlog.state = state;
					$scope.changeAttendanceCallback($scope.childId, requestlog.type);
				} else {
					$rootScope.message(result.msg);
				}
				;
			}));
		}, 'No', null);
	};

	$scope.operateStateTemporary = function (type, objId, state, requestlog) {
		$scope.confirm(title, msgCancle, 'Yes', function () {
			// 更新后台请求状态
			attendanceService.updateTemporaryRequest(type, objId, state).then(angular.bind(this, function then() {
				var result = attendanceService.updateTemporaryRequestObj;
				if (result.code == 0) {
					requestlog.state = state;
				} else {
					$rootScope.message(result.msg);
				}
				;
			}));
		}, 'No', null);
	};

	$scope.openRequest = function (objId, type, isRead) {
		attendanceService.getRequestLogObj(objId, type).then(angular.bind(this, function then() {
			var result = attendanceService.requestLogObj;
			if (result.code == 0) {
				$scope.openWin(objId, '', type, result.data, isRead);
			} else {
				$rootScope.message(result.msg);
			}
			;
		}));

	};

	$scope.openWin = function (objId, childId, type, childInfo, isRead) {
		// 打开不同编辑页面
		if (type == 0 || type == 1 || type == 2 || type == 3 || type == 4 || type == 5 || type == 6) { // 园长操作转园申请
			$uibModal.open({
				templateUrl: 'views/user/family/change_attendance.html?res_v=' + $scope.res_v,
				controller: 'userFamilyEditChangeAttendanceCtrl',
				size: "lg",
				backdrop: 'static',
				keyboard: false,
				windowClass: "profile-popup",
				resolve: {
					childInfo: function () {
						return childInfo;
					},
					childId: function () {
						return childInfo.accountInfo.id;
					},
					type: function () {
						return type;
					},
					functions: function () {
						return $scope.functions;
					},
					currentUser: function () {
						return $scope.currentUser;
					},
					callbackFunction: function () {
						return $scope.changeAttendanceCallback;
					},
					objId: function () {
						return objId;
					},
					isRead: function () {
						return isRead;
					}
				}
			});

		}
	};

	$scope.forwardOpt = function (childInfo, id, operaType) {
		if (operaType == 12) {
			$scope.givingNoticeLog(childInfo, id, operaType, true);
		} else {
			$scope.childAbsenteeLog(childInfo, id, operaType, true);
		}

	};
	$scope.givingNoticeLog = function (childInfo, id, operaType, isReadOnly) {
		var scope = $scope.$new();
		scope.childInfo = childInfo;
		scope.givingNoticeId = id;
		scope.givingNoticeStatus = !operaType ? -1 : operaType;
		scope.isReadOnly = isReadOnly;
		$uibModal.open({
			templateUrl: 'views/user/family/giving_notice.html?res_v=' + $scope.res_v,
			controller: 'givingNoticeCtrl',
			backdrop: 'static',
			keyboard: false,
			windowClass: "profile-popup",
			scope: scope
		});
	};
	$scope.childAbsenteeLog = function (childInfo, id, operaType, isReadOnly) {
		var scope = $scope.$new();
		scope.childInfo = childInfo;
		scope.absenteeId = id;
		scope.absenteeStatus = !operaType ? -1 : operaType;
		scope.isReadOnly = isReadOnly;
		$uibModal.open({
			templateUrl: 'views/user/family/attendance_child_absentee.html?res_v=' + $scope.res_v,
			controller: 'absentCtrl',
			backdrop: 'static',
			keyboard: false,
			windowClass: "profile-popup",
			scope: scope
		});
	};
	$scope.openRequest = function (objId, type, isRead) {
		attendanceService.getRequestLogObj(objId, type).then(angular.bind(this, function then() {
			var result = attendanceService.requestLogObj;
			if (result.code == 0) {
				$scope.openWin(objId, '', type, result.data, isRead);
			} else {
				$rootScope.message(result.msg);
			}
			;
		}));

	};
	$scope.openProfile = function (familyId) {
		$uibModal.open({
			templateUrl: 'views/user/family/child_edit.html?res_v=' + $scope.res_v,
			controller: 'userFamilyEditCtrl',
			animation: true,
			size: "lg",
			backdrop: 'static',
			keyboard: false,
			windowClass: "profile-popup profile-popup-family modal-align-top",
			resolve: {
				familyId: function () {
					return familyId;
				},
				functions: function () {
					return $scope.functions;
				},
				currentUser: function () {
					return $scope.currentUser;
				},
				callbackReload: function () {
					return $scope.getPageListExternal;
				}
			}
		});
	};

	$scope.keyEvent = function (ev) {
		if (ev.keyCode == 13) {
			$scope.changePage(1);
			$("input").blur();
		}
	};
	// $scope.keyEvent4Select = function (ev) {
	// 	if (ev.keyCode == 13) {
	// 		$scope.changePage(1);
	// 		$("select").blur();
	// 	}
	// };

	var modalInstance = null;

	$scope.logIndex = -1;
	$scope.openRequestLog = function (requestVo, childInfo, index) {
		$scope.logIndex = index;
		$scope.requestLogVos = {
			obj: new Array(requestVo)
		}
		modalInstance = $uibModal.open({
			templateUrl: 'views/user/family/request_log_modal.html?res_v=' + $scope.res_v,
			controller: 'requestlogModalCtr',
			backdrop: 'static',
			keyboard: false,
			windowClass: "profile-popup",
			resolve: {
				requestLogVos: function () {
					return $scope.requestLogVos;
				},
				functions: function () {
					return $scope.functions;
				},
				childInfo: function () {
					return childInfo;
				},
				callbackFunction: function () {
					return $scope.changeAttendanceCallback;
				}
			}
		});
	}
	$scope.clearAppDate = function (flag) {
		if (flag) {
			$scope.condition2.appStartDate = null;
			$scope.condition2.appEndDate = null;
		}
		$scope.changePage(1);
	};
	$scope.clearReqDate = function (flag) {
		if (flag) {
			$scope.condition2.reqStartDate = null;
			$scope.condition2.reqEndDate = null;
		}
		$scope.changePage(1);
	};

	$scope.attendanceManagementExport = function () {
		$window.location.href = "./attendance/csv.do?" + $.param($scope.condition);
	}
}
angular.module('kindKidsApp').controller(
	'attendanceManagementCtrl',
	['$scope', '$rootScope', '$timeout', '$window', '$uibModal', 'attendanceManagementSvr', 'familyService', 'absebteeService',
		'givingNoticeService', 'attendanceService', attendanceManagementCtrl]);