'use strict';

function confirmCtrl2($scope, $uibModalInstance, confirmObj) {

    $scope.confirmObj = confirmObj;

    $scope.function1 = function () {
        if ($scope.confirmObj.fun1.callback != null) {
            $scope.confirmObj.fun1.callback();
        }
        $uibModalInstance.dismiss('cancel');
    };

    $scope.function2 = function () {
        if ($scope.confirmObj.fun2.callback != null) {
            $scope.confirmObj.fun2.callback();
        }
        $uibModalInstance.dismiss('cancel');
    };

    $scope.cancelFunction = function () {
        if ($scope.confirmObj.cancel.callback != null) {
            $scope.confirmObj.cancel.callback();
        }
        $uibModalInstance.dismiss('cancel');
    };
    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    };
};

angular.module('kindKidsApp').controller('confirmCtrl2',
    ['$scope', '$uibModalInstance', 'confirmObj', confirmCtrl2]);