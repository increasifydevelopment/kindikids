'use strict';

/**
 * @author zhengguo
 * @description child export Controller
 */
function childExportCtrl($scope, $timeout, $location, orgId, $uibModalInstance, signService) {

	$scope.orgId = orgId;
	$scope.exportData = {};
	$scope.dateChange = function(num) {
		$scope.exportData.flag = num;
	};

	$scope.childExportInit = function() {
		var baseFormater = 'YYYY-MM-DDT00:00:00.000';
		$scope.exportData.startDate = moment().format(baseFormater);
		$scope.exportData.endDate = moment().format(baseFormater);
	};
	$scope.ok = function() {
		if ($scope.exportData.flag == 1 && !$scope.exportData.id) {
			$scope.message('Please choose a child.');
			return;
		}
		$uibModalInstance.close($scope.exportData);
		$uibModalInstance.dismiss('cancel');
	};

	$scope.cancel = function() {
		$uibModalInstance.dismiss('cancel');
	};
	
	$scope.clearTime=function(flag){
		if(!flag){return;}
		$scope.exportData.startDate=null;
		$scope.exportData.endDate=null;
	};
	$scope.close = function() {
        $scope.confirm('Close', 'Exit this page?', 'OK', function() {
            $scope.cancel();
        }, 'NO');        
    };

}
angular.module('kindKidsApp').controller('childExportCtrl',
		[ '$scope', '$timeout', '$location', 'orgId', '$uibModalInstance', 'signService', childExportCtrl ]);