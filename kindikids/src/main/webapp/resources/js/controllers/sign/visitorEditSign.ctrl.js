'use strict';

/**
 * @author zhengguo
 * @description visitor edit sign Controller
 */
function visitorEditSignCtrl($scope, $rootScope, $timeout, $location, signId, centerId, $uibModalInstance, signService) {
	console.log('centreid' + centerId);
	$scope.today = signId.inDate;
	$scope.accountId = signId.id;
	// 获取页面数据方法
	$scope.pageList = function () {
		// 根据signinID，获取签到信息//$scope.signID
		signService.getSignInfo($scope.flag, $scope.signID, $scope.signDate, centerId).then(
			angular.bind(this, function then() {
				if (signService.signInfoResult.code == 0) {
					$scope.condition = signService.signInfoResult.data;
					// 转换时间
					if ($scope.condition) {
						if ($scope.condition.centreId) {
							$scope.inCentreId = angular.copy($scope.condition.centreId)
						}
						// 组合fullName
						if (!$scope.condition.fullName) {
							$scope.condition.fullName = '';
							if ($scope.condition.middleName) {
								$scope.condition.fullName = $scope.condition.firstName + ' ' + $scope.condition.middleName + ' ' +
									$scope.condition.lastName;
							} else {
								$scope.condition.fullName = $scope.condition.firstName + ' ' + $scope.condition.lastName;
							}
						}
						// 把accountId 放入对象，方便后续操作传给后台
						$scope.condition.accountId = $scope.signID;
						if ($scope.condition.inTime) {
							$scope.condition.inTimeAll = $scope.condition.inTime.substr(0, 10);
						}
						$scope.condition.signInTime = $scope.changeBackTime($scope.condition.inTime);
						$scope.condition.signOutTime = $scope.changeBackTime($scope.condition.outTime);
						// 给签名板赋值
						if ($scope.condition.signInName) {
							$scope.signaturePad2.fromDataURL($scope.condition.signInName);
						}
						if ($scope.condition.signOutName) {
							$scope.signaturePad1.fromDataURL($scope.condition.signOutName);
						}
					}
				}
			}))
	}

	$scope.initVisitorEditSign = function () {

		// 如果是以属性的方式获取的id，则证明是child sign edit,否则为visitor sign edit
		if (signId.id) {
			$scope.flag = 0; // child sign edit
			$scope.signID = signId.id;
			$scope.signDate = signId.inDate;
		} else if (signId.centerId) { // 只存在centerId
			$scope.flag = 2; // staff sign edit
			$scope.signID = signId.centerId;
			$scope.signDate = signId.inDate;
		} else {
			$scope.flag = 1; // visitor sign edit
			$scope.signID = signId;
		}

		// 初始化signature
		var wrapper1 = document.getElementById("signature-pad-6");
		var clearButton1 = wrapper1.querySelector("[data-action=clear6]");
		var canvas1 = wrapper1.querySelector("canvas");

		var wrapper2 = document.getElementById("signature-pad-7");
		var clearButton2 = wrapper2.querySelector("[data-action=clear7]");
		var canvas2 = wrapper2.querySelector("canvas");

		$scope.resizeCanvas = function (canvas) {
			$timeout(function () {
				var ratio = Math.max(window.devicePixelRatio || 1, 1);
				canvas.width = canvas.offsetWidth * ratio;
				canvas.height = canvas.offsetHeight * ratio;
				canvas.getContext("2d").scale(ratio, ratio);
			});
		};

		$scope.resizeCanvas(canvas1);
		$scope.signaturePad1 = new SignaturePad(canvas1);
		clearButton1.addEventListener("click", function (event) {
			$scope.signaturePad1.clear();
			$('#editSignForm').bootstrapValidator('updateStatus', 'signinSignature', 'NOT_VALIDATED');
		});

		$scope.resizeCanvas(canvas2);
		$scope.signaturePad2 = new SignaturePad(canvas2);
		clearButton2.addEventListener("click", function (event) {
			$scope.signaturePad2.clear();
			$('#editSignForm').bootstrapValidator('updateStatus', 'signoutSignature', 'NOT_VALIDATED');
		});

		$timeout(function () {
			// 初始化获取centerList
			if ($scope.flag == 1 || $scope.flag == 2) {
				signService.getCenterList().then(angular.bind(this, function then() {
					// 获取center list
					$scope.centerList = signService.centerListResult;
					$scope.pageList();
				}))
			} else {
				$scope.pageList();
			}
		}, 100);

	}

	$scope.submitSignInfo = function () {
		$('#editSignForm').data('bootstrapValidator').validate();
		var flag = false;
		$("small").map(function () {
			if ($(this).attr('data-bv-result') == "INVALID" && !$($(this).parent().parent()).hasClass('ng-hide')) {
				flag = true;
			}
		});

		if (flag) {
			return;
		}
		// 签字板64位赋值
		if ($scope.signaturePad1.isEmpty()) {
			$scope.condition.signOutName = null;
		} else {
			$scope.condition.signOutName = $scope.signaturePad1.toDataURL();
		}
		if ($scope.signaturePad2.isEmpty()) {
			$scope.condition.signInName = null;
		} else {
			$scope.condition.signInName = $scope.signaturePad2.toDataURL();
		}

		// 拼接前台时间
		if (!$scope.condition.signInTime) {
			$scope.condition.inTime = $scope.condition.inTimeAll.replace(/\-/g, "/") + "T00:00:00.000";
		} else {
			$scope.condition.inTime = $scope.condition.inTimeAll.replace(/\-/g, "/") + $scope.changeHeadTime($scope.condition.signInTime);
		}
		if ($scope.condition.signOutTime) {
			$scope.condition.outTime = $scope.condition.inTimeAll.replace(/\-/g, "/") + $scope.changeHeadTime($scope.condition.signOutTime);
		} else {
			$scope.condition.outTime = null;
		}
		// add gfwang 解决兼职签到，没有园区的问题10/31
		$scope.condition.location = signId.localtion;

		// +++++++++++update by big+++++++
		$scope.comfirmSure = false;
		$scope.saveSignInfoAction();
		// +++++++++++++++++++++++++++++++

		/* $scope.signaturePad1.fromDataURL(qq); */
	};

	$scope.comfirmSure = false;
	$scope.saveSignInfoAction = function () {
		if ($scope.flag == 0 || $scope.flag == 2) {
			$scope.condition.centreId = centerId;
		}
		if ($scope.flag == 0) {
			$scope.condition.interim = signId.interim;
		}
		$scope.showLoading('#showLoading');
		signService.saveSignInfo($scope.flag, $scope.condition, $scope.comfirmSure).then(angular.bind(this, function then() {
			$scope.saveResult = signService.saveInfoResult;
			$scope.hideLoading('#showLoading');
			if ($scope.saveResult && $scope.saveResult.code == 0) {
				$uibModalInstance.close(1);
				$uibModalInstance.dismiss('cancel');
			} else if ($scope.saveResult.code == 2) {
				$rootScope.confirm('', $scope.saveResult.msg, 'Confirm', function () {
					$scope.comfirmSure = true;
					$scope.saveSignInfoAction();
				}, 'Cancel', function () {
					$scope.comfirmSure = false;
				});
			} else {
				$scope.message($scope.saveResult.msg);
			}
		}));
	};

	$scope.cancel = function () {
		/* $uibModalInstance.close(false); */
		$scope.confirmSuccess = function () {
			$uibModalInstance.dismiss('cancel');
		}
		$scope.confirm('Close', 'Exit this page?', 'OK', $scope.confirmSuccess, 'NO');
	};
}
angular.module('kindKidsApp').controller('visitorEditSignCtrl',
	['$scope', '$rootScope', '$timeout', '$location', 'signId', 'centerId', '$uibModalInstance', 'signService', visitorEditSignCtrl]);