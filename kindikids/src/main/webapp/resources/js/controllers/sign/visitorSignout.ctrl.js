'use strict';

/**
 * @author zhengguo
 * @description visitor sign out Controller
 */
function visitorSignoutCtrl($scope,$timeout, $location, $stateParams, $uibModal, signService) {   
	
	$scope.initVisitorSignout = function(){
		//初始化visitor signin 列表
		//先测试，固化一个centerId；
		$scope.centerName =$stateParams.centerName;
		$scope.centerId = $stateParams.centerId;
		$scope.getList($scope.centerId);
	}
	
	$scope.gotoIndex = function(){
		//跳转到默认主页
		$location.path('/');
	}
	
	$scope.search = function () {
		/*if($scope.params){*/
			$scope.getList($scope.centerId,$scope.params);
	/*	} else {
			$rootScope.message('search content is required');
		}*/
	}
	
	$scope.keyEvent = function (ev) {
    if (ev.keyCode == 13) {
    	$scope.search();
    }
	}
	
	$scope.getList = function(id,params){
		signService.getSigninList(id,params).then(angular.bind(this, function then() {
			//获取signinList
			if(signService.signinListResult && signService.signinListResult.code == 0){
				$scope.signinList = signService.signinListResult.data; 
			}
		}))
	}
	
	
}
angular.module('kindKidsApp').controller('visitorSignoutCtrl', [ '$scope', '$timeout', '$location', '$stateParams', '$uibModal', 'signService',visitorSignoutCtrl ]);