'use strict';

/**
 * @author zhengguo
 * @description child sign search Controller
 */
function childSignSearchCtrl($scope, $rootScope, $timeout, $location,
		$uibModalInstance, centerId, roomId, type, signService) {
	if (centerId) {
		$scope.orgId = centerId;
	}
	if (roomId) {
		$scope.orgId = roomId;
	}
	//type : 0 child       1 staff
	$scope.inType = type;
	$scope.url = '';
	if(!$scope.orgId){
		$scope.orgId = '';
	}
	if(type == 0){
		$scope.url = 'sign/getAllChilds.do?orgId='+$scope.orgId;
	} else {
		$scope.url = './common/getStffByChoooseCenter.do?c='+$scope.orgId;
	}
	
	$scope.ok = function() {
		if ($scope.childId) {
			$uibModalInstance.close($scope.childId);
			$uibModalInstance.dismiss('cancel');
		} else {
			if(type == 0){
				$rootScope.message('Please select \'Child\'  first');
			}else{
				$rootScope.message('Please select a staff to sign in');
			}
		}
	}
	$scope.close = function() {
		$scope.confirm('Close', 'Exit this page?', 'OK', function() {
			$scope.cancel();
		}, 'NO');		
	};

	$scope.cancel = function() {
		$uibModalInstance.dismiss('cancel');
	};

}
angular.module('kindKidsApp').controller(
		'childSignSearchCtrl',
		[ '$scope', '$rootScope', '$timeout', '$location', '$uibModalInstance',
				'centerId', 'roomId', 'type', 'signService', childSignSearchCtrl ]);