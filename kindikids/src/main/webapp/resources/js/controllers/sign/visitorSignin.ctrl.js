'use strict';

/**
 * @author zhengguo
 * @description visitor sign in Controller
 */
function visitorSignCtrl($scope,$timeout, $location, $stateParams, $uibModal, signService) {   
	
	$scope.initVisitorSign = function(){
		$scope.centerName =$stateParams.centerName;
		$scope.centerId = $stateParams.centerId;
		//初始化获取centerList
		signService.getCenterList().then(angular.bind(this, function then() {
			//获取center list
			$scope.centerList = signService.centerListResult; 
			//页面信息绑定值
			$scope.condition = {
					firstName:'',
					surname:'',
					visitReason:'',
					organisation:'',
					contactNumber:'',
					signInName:'',
					centerId:$scope.centerId
			}
		}))
		
		// 初始化signature
		var wrapper1 = document.getElementById("signature-pad-1");
		var clearButton1 = wrapper1.querySelector("[data-action=clear1]");
		var canvas1 = wrapper1.querySelector("canvas");
		
		$scope.resizeCanvas = function(canvas) {
			$timeout(function(){
				var ratio =  Math.max(window.devicePixelRatio || 1, 1);
				canvas.width =canvas.offsetWidth* ratio;
				canvas.height = canvas.offsetHeight * ratio;
				canvas.getContext("2d").scale(ratio, ratio);
			});
		};		
		
		$scope.resizeCanvas(canvas1);
		$scope.signaturePad1 = new SignaturePad(canvas1);
		/*$scope.image66 = $scope.signaturePad1.toDataURL();*/
		clearButton1.addEventListener("click", function(event) {
			$scope.signaturePad1.clear();
			$('#visitorSigninForm').bootstrapValidator('updateStatus','signatrue', 'NOT_VALIDATED');
		});
		
		//监控签名change事件
		$scope.signaturePad1.onEnd  = function(){
			$('#visitorSigninForm').bootstrapValidator('updateStatus','signatrue', 'VALID');
		};
	}
	
	$scope.gotoIndex = function(){
		//跳转到默认主页
		$location.path('/');
	}
	
	$scope.submitSignin = function () {
		$('#visitorSigninForm').data('bootstrapValidator').validate();
		if($scope.signaturePad1.isEmpty()){
			$('#visitorSigninForm').data('bootstrapValidator').updateStatus('signatrue', 'INVALID','notEmpty');
		} else if($('#visitorSigninForm').data('bootstrapValidator').isValid()){
			$scope.condition.signInName = $scope.signaturePad1.toDataURL();
			// 显示遮罩层
			$scope.showLoading("#visitorSignin");
			signService.saveVisitorSignIn($scope.centerId,$scope.condition).then(angular.bind(this, function then() {
				// 隐藏遮罩层
				$scope.hideLoading("#visitorSignin");
				if(signService.saveVisitorSigninResult && signService.saveVisitorSigninResult.code == 0){
					/*$rootScope.message(signService.saveVisitorSigninResult.msg);*/
					$location.path('visitor-welcome-bye/'+$scope.centerId+'/1/'+$scope.centerName+'/'+$scope.condition.firstName+' '+$scope.condition.surname);
				}
			}));
		}
		/*$scope.signaturePad1.fromDataURL(qq);*/
	}
	
}
angular.module('kindKidsApp').controller('visitorSignCtrl', [ '$scope', '$timeout', '$location', '$stateParams','$uibModal', 'signService',visitorSignCtrl ]);