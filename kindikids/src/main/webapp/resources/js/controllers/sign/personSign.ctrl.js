'use strict';

/**
 * @author zhengguo
 * @description personSign Controller
 */
function personSignCtrl($scope, $rootScope, $timeout, $location, $uibModalInstance, $filter, personInfo, flag, signService) {

	$scope.initPersonSignInfo = function () {

		$scope.inPersonInfo = personInfo;
		$scope.inPersonInfo.sunscreen = false;
		/* flag 0 childSignin 1 childSignout 2 staffSignin 3 staffSignout */
		$scope.inFlag = flag;
		// 初始化signature
		var wrapper1 = document.getElementById("signature-pad-11");
		var clearButton1 = wrapper1.querySelector("[data-action=clear11]");
		var canvas1 = wrapper1.querySelector("canvas");

		$scope.resizeCanvas = function (canvas) {
			$timeout(function () {
				var ratio = Math.max(window.devicePixelRatio || 1, 1);
				canvas.width = canvas.offsetWidth * ratio;
				canvas.height = canvas.offsetHeight * ratio;
				canvas.getContext("2d").scale(ratio, ratio);
			});
		};

		$scope.resizeCanvas(canvas1);
		$scope.signaturePad2 = new SignaturePad(canvas1);
		clearButton1.addEventListener("click", function (event) {
			$scope.signaturePad2.clear();
			$('#childSigninSignForm').bootstrapValidator('updateStatus', 'childSigninSignature', 'NOT_VALIDATED');
		});

		// 监控签名change事件
		$scope.signaturePad2.onEnd = function () {
			$('#childSigninSignForm').bootstrapValidator('updateStatus', 'childSigninSignature', 'VALID');
		};

		$scope.isCMEdu = ($filter('haveRole')($scope.inPersonInfo.roleInfoVoList, '1;5;2'));
		$scope.isCook = ($filter('haveRole')($scope.inPersonInfo.roleInfoVoList, '9'));

	};

	var isSubmit = true;
	$scope.ok = function () {
		if (!isSubmit) {
			return;
		}
		isSubmit = false;
		$('#childSigninSignForm').data('bootstrapValidator').validate();
		if (($scope.inFlag == 0 || $scope.inFlag == 1) && $scope.signaturePad2.isEmpty()) {
			$('#childSigninSignForm').data('bootstrapValidator').updateStatus('childSigninSignature', 'INVALID', 'notEmpty');
			isSubmit = true;
		} else {

			// staff 签到信息save
			if (($scope.inFlag == 2 || $scope.inFlag == 3) && $scope.personPwd && $scope.policy) {
				// 显示遮罩层
				$scope.showLoading("#childOrStaffSign");
				signService.saveStaffSign($scope.inFlag, $scope.inPersonInfo.centersId, $scope.inPersonInfo.id, $scope.personPwd).then(
					angular.bind(this, function then() {
						var result = signService.staffSignResult;
						// 隐藏遮罩层
						$scope.hideLoading("#childOrStaffSign");
						isSubmit = true;
						if (result && result.code == 0) {
							$uibModalInstance.close(true);
							$uibModalInstance.dismiss('cancel');
							if ($scope.inFlag == 2) {
								$location.path('visitor-welcome-bye/' + $scope.inPersonInfo.centersId + '/1/' + signService.staffSignResult.data
									+ '/' + $scope.inPersonInfo.fullName);
							} else {
								$location.path('visitor-welcome-bye/' + $scope.inPersonInfo.centersId + '/2/' + signService.staffSignResult.data
									+ '/' + $scope.inPersonInfo.fullName);
							}
						} else if (result && result.code == 2) {
							// 最后2个员工签出时，检测到有小孩未签出，给予confirm 确认
							$scope.confirm('', result.msg, null, null, 'Cancel', null);
						} else {
							$rootScope.message(result.msg);
						}

					}));
			}

			// child 签到信息save 0 签入， 1签出
			if (($scope.inFlag == 0 && $scope.inPersonInfo.sunscreen) || $scope.inFlag == 1) {
				// 显示遮罩层
				$scope.showLoading("#childOrStaffSign");
				$scope.inPersonInfo.inName = $scope.signaturePad2.toDataURL();
				signService.saveChildSign($scope.inFlag, $scope.inPersonInfo.childId, $scope.inPersonInfo.inName, $scope.inPersonInfo.sunscreen)
					.then(
						angular.bind(this, function then() {
							isSubmit = true;
							// 隐藏遮罩层
							$scope.hideLoading("#childOrStaffSign");
							if (signService.childSignResult && signService.childSignResult.code == 0) {
								$uibModalInstance.close(true);
								$uibModalInstance.dismiss('cancel');
								if ($scope.inFlag == 0) {
									$location.path('visitor-welcome-bye/' + $scope.inPersonInfo.centersId + '/1/'
										+ signService.childSignResult.data + '/' + $scope.inPersonInfo.name);
								} else {
									$location.path('visitor-welcome-bye/' + $scope.inPersonInfo.centersId + '/2/'
										+ signService.childSignResult.data + '/' + $scope.inPersonInfo.name);
								}
							} else {
								$rootScope.message(signService.childSignResult.msg);
							}
						}));
			}
			isSubmit = true;
		}
		isSubmit = true;
	}

	$scope.cancel = function () {
		/* $uibModalInstance.close(false); */
		$scope.confirmSuccess = function () {
			$uibModalInstance.dismiss('cancel');
		}
		$rootScope.confirm('Close', 'Exit this page?', 'OK', $scope.confirmSuccess, 'NO');
	};

}
angular.module('kindKidsApp').controller('personSignCtrl',
	['$scope', '$rootScope', '$timeout', '$location', '$uibModalInstance', '$filter', 'personInfo', 'flag', 'signService', personSignCtrl]);