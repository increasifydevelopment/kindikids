'use strict';

/**
 * @author zhengguo
 * @description visitor and staff export Controller
 */
function visitorExportCtrl($scope, $timeout, $location,  $uibModalInstance) {   
	$scope.dateInfo={};
		$scope.visitorExportInit = function(){		
			var baseFormater = 'YYYY-MM-DDT00:00:00.000';
			$scope.dateInfo.startDate = moment().format(baseFormater);
			$scope.dateInfo.endDate = moment().format(baseFormater);
		};

		$scope.ok = function(){
			$uibModalInstance.close($scope.dateInfo); 
			$uibModalInstance.dismiss('cancel');
		}
		
		$scope.cancel = function() {
			$uibModalInstance.dismiss('cancel');
		};
		$scope.clearTime=function(flag){
			if(!flag){return;}
			$scope.dateInfo.startDate=null;
			$scope.dateInfo.endDate=null;
		};
	
}
angular.module('kindKidsApp').controller('visitorExportCtrl', [ '$scope', '$timeout', '$location', '$uibModalInstance', visitorExportCtrl ]);