'use strict';

/**
 * @author zhengguo
 * @description landing sign Controller
 */
function landSignCtrl($scope,$rootScope,$timeout, $location, signService, $stateParams) {   
	
	$scope.errorMsg="";
	$scope.initLand = function(){
		$scope.inCenterName = $stateParams.centerName;
		//获取路由中的centerName 到后台拿到centerID
		signService.getCenterId($scope.inCenterName).then(angular.bind(this, function then() {
			//获取signinList
			if( signService.centerIdResult && signService.centerIdResult.code == 0){
				$scope.inCenterId = signService.centerIdResult.data; 
			} else {
				$scope.errorMsg=signService.centerIdResult.msg;
				$rootScope.message(signService.centerIdResult.msg);
			}
		}));
	}
	
	$scope.signin = function(){
		if($scope.inCenterId){
			$location.path('visitor_signin/'+$scope.inCenterId+'/'+$scope.inCenterName);
		} else {
			$rootScope.message($scope.errorMsg);
		}
	}
	
	$scope.signout = function(){
		if($scope.inCenterId){
			$location.path('visitor_signout/'+$scope.inCenterId+'/'+$scope.inCenterName);
		} else {
			$rootScope.message($scope.errorMsg);
		}
	}
	
	//该personSign方法传flag 
	/*0 childSignin
	1 childSignout
	2 staffSignin
	3 staffSignout*/
	
	$scope.personSign = function(flag){
		if($scope.inCenterId){
			$location.path('person_signList/'+flag+'/'+$scope.inCenterId+'/'+$scope.inCenterName);
		} else {
			$rootScope.message($scope.errorMsg);
		}
	}

		$scope.gotoIndex = function(){
			//跳转到默认主页
			$location.path('/');
		};
	
}
angular.module('kindKidsApp').controller('landSignCtrl', [ '$scope','$rootScope', '$timeout', '$location', 'signService', '$stateParams', landSignCtrl ]);