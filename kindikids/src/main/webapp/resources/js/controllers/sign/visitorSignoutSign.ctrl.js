'use strict';

/**
 * @author zhengguo
 * @description visitor sign out sign Controller
 */
function visitSignoutSignCtrl($scope,$timeout, $location, $uibModal,signService) {   
	
	$scope.fullName = $scope.$stateParams.fullName;
	$scope.centerName = $scope.$stateParams.centerName;
	
	$scope.condition = {
			id : $scope.$stateParams.flag
	}
	
	$scope.gotoIndex = function(){
		//跳转到默认主页
		$location.path('/');
	}
	
	$scope.initSign = function(){
		
		// 初始化signature
		var wrapper1 = document.getElementById("signature-pad-2");
		var clearButton1 = wrapper1.querySelector("[data-action=clear1]");
		var canvas1 = wrapper1.querySelector("canvas");
		
		$scope.resizeCanvas = function(canvas) {
			$timeout(function(){
				var ratio =  Math.max(window.devicePixelRatio || 1, 1);
				canvas.width =canvas.offsetWidth* ratio;
				canvas.height = canvas.offsetHeight * ratio;
				canvas.getContext("2d").scale(ratio, ratio);
			});
		};		
		
		$scope.resizeCanvas(canvas1);
		$scope.signaturePad2 = new SignaturePad(canvas1);
		clearButton1.addEventListener("click", function(event) {
			$scope.signaturePad2.clear();
			$('#visitorSignoutSignForm').bootstrapValidator('updateStatus','signoutSignature', 'NOT_VALIDATED');
		});
		
		//监控签名change事件
		$scope.signaturePad2.onEnd  = function(){
			$('#visitorSignoutSignForm').bootstrapValidator('updateStatus','signoutSignature', 'VALID');
		};

	}
	
	$scope.submitSign = function () {
		if($scope.signaturePad2.isEmpty()){
			$('#visitorSignoutSignForm').data('bootstrapValidator').updateStatus('signoutSignature', 'INVALID','notEmpty');
		} else {		
			$scope.condition.signOutName = $scope.signaturePad2.toDataURL();
			// 显示遮罩层
			$scope.showLoading("#visitorSignout");
			signService.saveVisitorSignout($scope.condition).then(angular.bind(this, function then() { 
				// 隐藏遮罩层
				$scope.hideLoading("#visitorSignout");
				if(signService.saveVisitorSignoutResult && signService.saveVisitorSignoutResult.code == 0){
					/*$rootScope.message(signService.saveVisitorSignoutResult.msg);*/
					$location.path('visitor-welcome-bye/'+$scope.condition.id+'/2/'+$scope.centerName+'/'+$scope.fullName);
				}
			}));
		}
		/*$scope.signaturePad2.fromDataURL(qq);*/
	}

	
}
angular.module('kindKidsApp').controller('visitSignoutSignCtrl', [ '$scope', '$timeout', '$location', '$uibModal', 'signService', visitSignoutSignCtrl ]);