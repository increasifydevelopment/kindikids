'use strict';

/**
 * @author abliu
 * @description signListCtrl Controller
 */
function signListCtrl($scope, $rootScope, $timeout, $filter, $window, attendanceService, commonService, $uibModal) {
	// variable=================================================================  
	$scope.page = {};
	$scope.page.condition = {
		keyword: null,
		time: null,
		roomId: null,
		centersId: null,
		type: 2,  //type 0:传日期进行搜索， 1：上一周2：本周 3：下一周 4:日期不动，根据当前weekdate时间查
		weekDate: null,
		listType: 2 //listType 0:children 1:staff 2:vistor
	};
	//判断本周最大列集合数
	$scope.maxRowLength = 0;
	$scope.maxRowList = [];
	$scope.weekRowList = [];
	$scope.maxRowListIndex = 0;
	$scope.currentRepeadChildList = null;
	$scope.sourceLength = 0;
	$scope.yearList = [];
	//是否为园长
	$scope.isCenterManager = $filter('haveRole')($scope.currentUser.roleInfoVoList, '1;5');
	/**
	* @author abliu
	* @description
	* @returns 获取sign列表
	*/
	$scope.getSignList = function () {
		$scope.showLoading("#mainList");
		//园长默认选择当前园add by gfwang
		if ($scope.isCenterManager && $scope.currentUser.userInfo.centersId) {
			$scope.page.condition.centersId = $scope.currentUser.userInfo.centersId;
		}
		attendanceService.getAttendanceList($scope.page.condition).then(angular.bind(this, function then() {
			$("#searchDiv").removeClass("open");
			$scope.hideLoading("#mainList");
			var result = attendanceService.listResult;
			$scope.hideLoading("#newsFeedList");
			if (result.code == 0) {
				getMaxRowLength(result.data.weekDays);
				$scope.page = result.data;
				//获取导出信息 add by zhengguo
				$scope.exportCondition = {
					centreId: $scope.page.condition.centersId,
				}
				if (result.data.condition.search && $scope.isEmpty()) {
					$scope.confirm('Message', 'No results match your search.', 'OK', function () {
						$("#privacy-modal").modal('hide');
					});
				}
				$scope.page.condition.search = false;
			} else {
				$rootScope.message(result.msg);
			}
		}));
	};

	$scope.isEmpty = function () {
		var list = $scope.page.weekDays;
		for (var i in list) {
			if (list[i].signs.length != 0) {
				return false;
			}
		}
		return true;
	}

	$scope.search = function () {
		$scope.page.condition.pageIndex = 1;
		$scope.page.condition.search = true;
		$scope.getSignList();
	};

	function getMaxRowLength(list) {
		$scope.maxRowLength = 0;
		$scope.maxRowListIndex = 0;
		$scope.maxRowList = [];
		$scope.weekRowList = [];
		for (var i = 0, m = list.length; i < m; i++) {
			var length = list[i].signs.length;
			if (length > $scope.maxRowLength) {
				$scope.maxRowLength = length;
			}
		}

		for (var i = 0, m = $scope.maxRowLength; i < m; i++) {
			$scope.maxRowList.push({ "id": $scope.sourceLength });
			$scope.sourceLength++;
		}
		for (var i = 0, m = 7; i < m; i++) {
			$scope.weekRowList.push({ "id": i + 1 + $scope.sourceLength });
			$scope.sourceLength++;
		}

	};

	/**
	* @author abliu
	* @description
	* @returns 获取园区及room集合
	*/
	$scope.getMenuOrgs = function () {
		commonService.getMenuOrgs(2).then(angular.bind(this, function then() {
			$scope.orgList = commonService.orgList;
		}));
	};

	/**
	* @author abliu
	* @description
	* @returns 设置年份集合，从2010年开始到当前年加1年
	*/
	$scope.getYearList = function () {
		var yyyy = new Date().getFullYear();
		var m = yyyy - 2015;
		for (var i = 0; i <= m + 1; i++) {
			$scope.yearList.push(2015 + i);
		}
	};

	/**
	* @author abliu
	* @description
	* @returns 设置年份
	*/
	$scope.setTimeY = function (yyyy) {
		if ($scope.page.condition.time != null) {
			var times = $scope.page.condition.time.split("/");
			if (times.length > 1) {
				$scope.page.condition.time = yyyy + "/" + times[1];
				$scope.page.condition.type = 0;
				$scope.getSignList();
			}
		}
	};

	/**
	* @author abliu
	* @description
	* @returns 设置月份
	*/
	$scope.setTimeM = function (mm) {
		if ($scope.page.condition.time != null) {
			var times = $scope.page.condition.time.split("/");
			if (times.length > 1) {
				$scope.page.condition.time = times[0] + "/" + mm;
				$scope.page.condition.type = 0;
				$scope.getSignList();
			}
		}
	};


	/**
	* @author abliu
	* @description
	* @returns 设置type
	*/
	$scope.setType = function (type) {
		$scope.page.condition.type = type;
		$scope.page.condition.search = true;
		$scope.getSignList();
	};


	/**
	* @author zhengguo
	* @description 打开签到信息的编辑框
	* absenteeId 需要签到信息的ID
	* @returns 保存编辑结果
	*/
	$scope.openEditSign = function (signId) {
		var modalInstance = $uibModal.open({
			templateUrl: 'views/sign/visitor-edit-sign.html?res_v=' + $scope.res_v,
			controller: 'visitorEditSignCtrl',
			backdrop: 'static',
			keyboard: false,
			windowClass: 'visitor-sign-out-popup modal-align-top',
			resolve: {
				signId: function () {
					return signId;
				}, centerId: function () {
					return '';
				}
			}
		}).result.then(function (flag) {
			if (flag == 1) {
				$scope.getSignListCallback();
			}
		});
	};

	$scope.getSignListCallback = function () {
		$scope.page.condition.type = 4;
		$scope.getSignList();
	};

	/**
	* @author zhengguo
	* @description 导出excel
	* @need exportCondition.centreId 空就是所有center；exportCondition.startDate; exportCondition.dateInfo;
	* @returns 返回uuid
	*/
	$scope.exportExcel = function () {
		//弹出export 导出日期的
		var modalInstance = $uibModal.open({
			templateUrl: 'views/sign/export-confirm.html?res_v=' + $scope.res_v,
			controller: 'visitorExportCtrl',
			backdrop: 'static',
			keyboard: false,
			/*windowClass:'visitor-sign-out-popup modal-align-top',*/
		}).result.then(function (dateInfo) {
			if (dateInfo) {
				$scope.exportCondition.startDate = dateInfo.startDate;
				$scope.exportCondition.endDate = dateInfo.endDate;
			}
			attendanceService.exportExcel($scope.exportCondition).then(angular.bind(this, function then() {
				$scope.uuidResult = attendanceService.getUUIDResult;
				//继续导出excel
				$window.location.href = "./sign/excel.do?uuid=" + $scope.uuidResult.data;
			}));
		});
	};



}
angular.module('kindKidsApp').controller('signListCtrl', ['$scope', '$rootScope', '$timeout', '$filter', '$window', 'attendanceService', 'commonService', '$uibModal', signListCtrl]);