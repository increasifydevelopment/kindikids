'use strict';

/**
 * @author zhengguo
 * @description person sign list Controller
 */
function personSignListCtrl($scope,$rootScope,$timeout, $location, $stateParams, $uibModal, signService) {   
	
	$scope.initChildInfo = function(){
		//初始化child signin list 列表
		$scope.centerName =$stateParams.centerName;
		$scope.centerId = $stateParams.centerId;
		//不同的页面List ;
		/*flag:
	  0 childSignin
		1 childSignout
		2 staffSignin
		3 staffSignout*/
		$scope.flag = $stateParams.flag;
		$scope.getList($scope.flag,$scope.centerId);
	}
	
	$scope.gotoIndex = function(){
		//跳转到默认主页
		$location.path('/');
	}
	
	$scope.search = function () {
		/*if($scope.params){*/
			$scope.getList($scope.flag,$scope.centerId,$scope.params);
		/*} else {
			$rootScope.message('search content is required');
		}*/
	}
	
	$scope.keyEvent = function (ev) {
    if (ev.keyCode == 13) {
    	$scope.search();
    }
	}
	
	//将后台拿到的List按顺序转换成4个一组，页面展示需要
	$scope.changeListFormat  = function(list){
		var newList = angular.copy(list);
		var resultList =[];
		if(newList.length > 0){
			var listLen = newList.length;
			var num = Math.floor(listLen/4);
			for(var p = 0 ; p < num ; p ++){
				var litList = [];
				for(var i = 0 ; i < 4 ; i ++){
					litList.push(newList[0]);
					newList.splice(0,1);
				}
				resultList.push(litList);
			}
			//余数循环
			var otherList = [];
			for(var j = 0 ; j < listLen%4 ; j++){
				otherList.push(newList[0]);
				newList.splice(0,1);
			}
			resultList.push(otherList);
		}
		return resultList;
	};
	
	$scope.getList = function(flag,id,params){
		/*0 childSignin 		1 childSignout 		2 staffSignin 		3 staffSignout*/
		
		signService.getStaffSigninList(flag,id,params).then(angular.bind(this, function then() {
			//获取signinList
			if(signService.staffSigninListResult && signService.staffSigninListResult.code == 0){
				$scope.staffInList = signService.staffSigninListResult.data; 
				$scope.pageList = $scope.changeListFormat($scope.staffInList);
			} else {
				$rootScope.message(signService.staffSigninListResult.msg);
			}
		}))
	}
	
	/**
	* @author zhengguo
	* @description child 签到modal
	* @need child info
	* @returns 签到后到后台存储数据，list列表重新刷新
	*/	
	$scope.childSigninModal =function(childInfo){
		//弹出export 导出日期的
		var modalInstance = $uibModal.open({
			templateUrl : 'views/sign/personSign-sign.html?res_v='+$scope.res_v,
			controller : 'personSignCtrl',
			backdrop : 'static',
			keyboard : false,
			resolve : {
				personInfo : function() {
					return childInfo;
				},
				flag : function() {
					return $scope.flag;
				}
			}
			/*windowClass:'visitor-sign-out-popup modal-align-top',*/
		}).result.then(function(flag){
			if(flag){
				//更新页面list
				$scope.getList($scope.flag,$scope.centerId,$scope.params);
			}
		});
	};
	
	
}
angular.module('kindKidsApp').controller('personSignListCtrl', [ '$scope', '$rootScope','$timeout', '$location', '$stateParams', '$uibModal', 'signService',personSignListCtrl ]);