'use strict';

/**
 * @author zhengguo
 * @description visitor message Controller
 */
function visitorMessageCtrl($scope,$timeout, $location, $stateParams) {   
		//flag 为1是欢迎页面，flag为2是退出页面
		$scope.flag = $stateParams.flag;
		$scope.name = $stateParams.name;
		$scope.centerName = $stateParams.centerName;
		$scope.centerId = $stateParams.centerId;
		//默认签到后3秒后跳转到签到主页面

			$timeout(function () {
				$location.path('landing_signin/'+$scope.centerName);
			},3000);

	/*	$scope.gotoIndex = function(){
			//跳转到默认主页
			$location.path('/');
		}*/
	
}
angular.module('kindKidsApp').controller('visitorMessageCtrl', [ '$scope', '$timeout', '$location', '$stateParams',visitorMessageCtrl ]);