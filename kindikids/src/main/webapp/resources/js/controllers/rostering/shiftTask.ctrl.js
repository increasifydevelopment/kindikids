'use strict';

function shiftTaskCtrl($scope, $rootScope, centerId, taskVos,
		$uibModalInstance, rosteringService) {

	$scope.initModal = function() {
		$scope.allshiftTaskLength = 0;// 存储所有可以选择的task长度
		$scope.shiftTaskList = [];// 选中的task的id的集合
		$scope.condition = {};
		$scope.condition.cId = centerId;
		$scope.condition.taskVos = taskVos;
		$scope.myChooseTask = [];
		// 获取供选择的task源数组
		rosteringService.getShiftTaskList($scope.condition.cId).then(
				angular.bind(this, function then() {
					var result = rosteringService.shiftTaskResult;
					if (result.code) {
						$scope.shiftTask = result.data;
						$scope.allShiftTask = angular.copy($scope.shiftTask);
						$scope.allshiftTaskLength = $scope.allShiftTask.length;
						if ($scope.condition.taskVos.length > 0) {
							$scope.shiftTaskList = angular
									.copy($scope.condition.taskVos);

							$scope.myChooseTask = angular
									.copy($scope.condition.taskVos);
							// 去除所有shiftTask中的已被选项
							/*
							 * for(var j = 0 ; j < $scope.shiftTaskList.length ;
							 * j ++){ $scope.shiftTaskList[j].editButtonShowFlag =
							 * true; for(var i = 0 ; i < $scope.shiftTask.length ;
							 * i ++){ if($scope.shiftTaskList[j].id ==
							 * $scope.shiftTask[i].id){
							 * $scope.shiftTask.splice(i,1); i--; } } }
							 */
						} else {
							// 初始化创建一个空的
							var one = {
								id : '',
								editButtonShowFlag : false
							}
							$scope.shiftTaskList.push(one);
						}
					}
				}));
	};
	
	//处理 已经不能选中的 task
	$scope.optionsHandle=function(item){
	    var has=false;
	    angular.forEach($scope.shiftTask, function(data, index, array) {
			if (data.id==item.id) {
				has = true;
			}
		});
	   var selectOps=angular.copy($scope.shiftTask);
	    if(!has){
	    	selectOps.push({id:item.id,text:item.taskName});
	    }
	    return selectOps;
	};
	
	$scope.addTask = function() {
		if ($scope.shiftTaskList.length < $scope.allshiftTaskLength) {
			var one = {
				id : '',
				editButtonShowFlag : false
			}
			$scope.shiftTaskList.push(one);
		} else {
			$scope.message('Arrive the max select task number');
		}
	}

	$scope.changeTask = function(item) {
		if (item.id) {
			var length = $scope.myChooseTask.length;
			for (var i = 0; i < length; i++) {
				var id = item.id;
				if (id == $scope.myChooseTask[i].id) {
					item.id = '';
					$scope.message(getNameById($scope.myChooseTask[i].id)
							+ ' already exist');
					initMyChoose();
					return;
				}

			}
			initMyChoose();
		}
	};
	function initMyChoose() {
		$scope.myChooseTask = [];
		for (var i = 0; i < $scope.shiftTaskList.length; i++) {
			var id = $scope.shiftTaskList[i].id;
			if (id)
				$scope.myChooseTask.push({
					id : $scope.shiftTaskList[i].id
				});
		}
	}
	function getNameById(id) {
		for (var i = 0; i < $scope.allShiftTask.length; i++) {
			if (id == $scope.allShiftTask[i].id) {
				return $scope.allShiftTask[i].text;
			}
		}
	}

	$scope.editItem = function(item) {
		item.editButtonShowFlag = false;
	}

	$scope.delItem = function(item, itemIndex) {
		if (item.id) {
			for (var i = 0; i < $scope.myChooseTask.length; i++) {
				if (item.id == $scope.myChooseTask[i].id) {
					$scope.myChooseTask.splice(i, 1);
					//$scope.shiftTask.push($scope.myChooseTask[i]);
				}
			}
		}
		$scope.shiftTaskList.splice(itemIndex, 1);
	}

	$scope.saveItem = function(item) {
		if (item.id) {
			item.editButtonShowFlag = true;
		} else {
			$scope.message('please select a task');
		}
	}

	$scope.ok = function() {
		for (var i = 0; i < $scope.shiftTaskList.length; i++) {
			if (!$scope.shiftTaskList[i].id) {
				$scope.shiftTaskList.splice(i, 1);
				i--;
			}
		}
		$uibModalInstance.close($scope.shiftTaskList);
		$uibModalInstance.dismiss('cancel');
	};

	$scope.cancel = function() {
		/* $uibModalInstance.close(false); */
		$scope.confirmSuccess = function() {
			$uibModalInstance.dismiss('cancel');
		}
		$rootScope.confirm('Close', 'Exit this page?', 'OK',
				$scope.confirmSuccess, 'NO');
	};

};

angular.module('kindKidsApp').controller(
		'shiftTaskCtrl',
		[ '$scope', '$rootScope', 'centerId', 'taskVos', '$uibModalInstance',
				'rosteringService', shiftTaskCtrl ]);