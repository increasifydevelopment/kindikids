'use strict';

/**
 * @author zhengguo
 * @description staffLeave Controller
 */
function staffLeaveCtrl($scope, $filter, $rootScope, $timeout, $window, $uibModal, rosteringService, staffService) {

	$rootScope.searchRoleAndType = false;
	$rootScope.showRosterOrg = false;
	$rootScope.isInLeavePage = true;
	$scope.initStaffLeave = function () {
		if ($filter('haveRole')($scope.currentUser.roleInfoVoList, '0;4;10')) {
			$scope.mainPage.centerId = '';
		}
		// 页面list参数对象
		$scope.condition = {
			listType: 1,
			maxSize: 7,
			pageIndex: 1,
			selectCenterId: $scope.mainPage.centerId,
			leaveType: null,
			/* bigTotalItems:180, */
			/* bigCurrentPage:1, */
		};
		$scope.getPage();
	};
	$scope.$watch('condition.startDate', function (newValue, oldValue) {
		if (newValue == undefined || 'Invalid date' == newValue) {
			return;
		}
		$scope.changeLeavePage(1);
	}, true);
	$scope.$watch('condition.endDate', function (newValue, oldValue) {
		if (newValue == undefined || 'Invalid date' == newValue) {
			return;
		}
		$scope.changeLeavePage(1);
	}, true);
	$scope.$watch('condition.appDate', function (newValue, oldValue) {
		if (newValue == undefined || 'Invalid date' == newValue) {
			return;
		}
		$scope.changeLeavePage(1);
	}, true);

	// 监测外层包裹页面中的center的变化，会随之变化LIST
	$scope.$watch('mainPage.leaveType', function (newValue) {
		$scope.condition.leaveType = $scope.mainPage.leaveType;
		$scope.getPage();
	});

	$scope.keyEvent = function (ev) {
		if (ev.keyCode == 13) {
			$scope.condition.pageIndex = 1;
			$scope.getPage();
		}
	};

	$scope.changeLeavePage = function (pageIndex) {
		$scope.condition.pageIndex = pageIndex;
		$scope.getPage();
	};

	// 监测外层包裹页面中的center的变化，会随之变化LIST
	$scope.$watch('mainPage.centerId', function (newValue) {
		$scope.condition.selectCenterId = $scope.mainPage.centerId;
		$scope.getPage();
	});

	$scope.getPage = function () {
		// 页面list数据
		$scope.showLoading("#staffLeaveList");
		rosteringService.getStaffLeaveList($scope.condition).then(angular.bind(this, function then() {
			$scope.hideLoading("#staffLeaveList");
			$scope.leaveList = rosteringService.listResult;
			$scope.condition.totalItems = $scope.leaveList.data.condition.totalSize;
			$scope.condition.pageSize = $scope.leaveList.data.condition.pageSize;
		}));
	};


	// 新增一条请假
	$scope.addLeaveModal = function () {
		$uibModal.open({
			templateUrl: 'views/rostering/addLeave.html?res_v=' + $scope.res_v,
			controller: 'addLeaveCtrl',
			backdrop: 'static',
			keyboard: false,
			scope: $scope,
			resolve: {
				flag: function () {
					return -1;
				},
				leaveInfo: function () {
					return null;
				},
				callBackFun: function () {
					return $scope.getPage;
				}
			}
		}).result.then(function (backData) {
			if (backData) {
				// 调用审批是否为带薪不带薪请假
				$scope.approveModal(backData);
			}
		});
	};

	// 同意请假
	$scope.approveModal = function (item, confirm) {
		$rootScope.confirm('Confirm', 'Are you sure to approve the leave request?', 'OK', function () {
			$scope.ok(item, confirm);
		}, 'NO');
	};

	$scope.ok = function (item, confirm) {
		$scope.itemData = item;
		rosteringService.operaStaffLeave($scope.itemData.id, 1, '', null, confirm).then(angular.bind(this, function then() {
			$scope.operaInResult = rosteringService.operaResult;
			if ($scope.operaInResult.code == 0) {
				$scope.itemData.status = 1;
				$rootScope.message($scope.operaInResult.msg);
				$scope.getPage();
				$uibModalInstance.dismiss('cancel');
			}
			if ($scope.operaInResult.code == 1) {
				$scope.message($scope.operaInResult.msg);
			}
			if ($scope.operaInResult.code == 2) {
				$rootScope.confirm('Confirm', $scope.operaInResult.msg, 'OK', function () {
					$scope.ok($scope.itemData, true);
				}, 'NO');
			}
		}));
	}

	// 拒绝或者取消请假
	$scope.rejectAndCancelModal = function (item, num) {
		$uibModal.open({
			templateUrl: 'views/rostering/rejectAndCancelModal.html?res_v=' + $scope.res_v,
			controller: 'rejectAndCancelCtrl',
			backdrop: 'static',
			keyboard: false,
			scope: $scope,
			resolve: {
				itemData: function () {
					return item;
				},
				itemNum: function () {
					return num;
				},
				callback: function () {
					return null;
				}
			}
		})
	};

	// 重新编辑请假
	$scope.openLeave = function (leaveInfo) {
		$uibModal.open({
			templateUrl: 'views/rostering/addLeave.html?res_v=' + $scope.res_v,
			controller: 'addLeaveCtrl',
			backdrop: 'static',
			keyboard: false,
			scope: $scope,
			resolve: {
				flag: function () {
					return -1;
				},
				leaveInfo: function () {
					return leaveInfo;
				},
				callBackFun: function () {
					return $scope.getPage;
				}
			}
		})
	}



}
angular.module('kindKidsApp').controller('staffLeaveCtrl',
	['$scope', '$filter', '$rootScope', '$timeout', '$window', '$uibModal', 'rosteringService', 'staffService', staffLeaveCtrl]);