'use strict';
function closurePeriodCtrl($scope, $filter, $rootScope, $timeout, $window, $uibModal, $uibModalInstance, rosteringService, model, isAdd, centres, callBackFun) {
    $scope.isAdd = isAdd;
    $scope.centres = centres;
    $scope.sourceCentres = [];
    if (isAdd) {
        $scope.info = {
            'holiday': 1,
            'edit': true,
            'centres': []
        };
    } else {
        $scope.info = model;
    }

    $scope.cancelModal = function () {
        $uibModalInstance.dismiss('cancel');
    }

    $scope.save = function () {
        $('#closurePeriodForm').data('bootstrapValidator').validate();
        if (!$('#closurePeriodForm').data('bootstrapValidator').isValid()) {
            return;
        }
        rosteringService.saveClosurePeriod($scope.info).then(function () {
            var result = rosteringService.saveClosurePeriodResult;
            if (result.code == 0) {
                callBackFun();
                $uibModalInstance.dismiss('cancel');
            } else if (result.code == 2) {
                //提醒
                $rootScope.confirm('Confirm', 'This occurs within a period where you have rostered staff on. What would you like to do?', 'Save and remove rostered staff', function () {
                    $scope.info.confirmRoster = true;
                    $scope.save();
                }, 'Cancel', null);
            } else if (result.code == 3) {
                //提醒
                $rootScope.confirm('Confirm', 'An event has been scheduled within this period. What would you like to do?', 'Save and delete event', function () {
                    $scope.info.confirmEvent = true;
                    $scope.save();
                }, 'Cancel', null);
            } else {
                $rootScope.message(result.msg);
            }
        })
    }

    $scope.delete = function (id) {
        rosteringService.deleteClosurePeriod(id).then(function () {
            var result = rosteringService.deleteClosurePeriodResult;
            if (result.code == 0) {
                callBackFun();
                $uibModalInstance.dismiss('cancel');
            } else {
                $rootScope.message(result.msg);
            }
        });
    }

    $scope.dealChoose = function (id) {
        var index = $scope.info.centres.indexOf(id);
        if (index == -1) {
            $scope.info.centres.push(id);
        } else {
            $scope.info.centres.splice(index, 1);
        }
    }
}
angular.module('kindKidsApp').controller('closurePeriodCtrl', ['$scope', '$filter', '$rootScope', '$timeout', '$window', '$uibModal', '$uibModalInstance', 'rosteringService', 'model', 'isAdd', 'centres', 'callBackFun', closurePeriodCtrl]);