'use strict';

function selectTemplateCtrl($scope,  $rootScope, centerId,weekDate,$uibModalInstance, rosteringService) {
	
	$scope.initModal = function(){
		//初始化出来所有可选择的排班模板
		rosteringService.getRosterTemplates(centerId).then(angular.bind(this, function then() {
			var result = rosteringService.getRosterTemplatesResult;
			$scope.taskGroup = [];
			if(result && result.length > 0){
				$scope.taskGroup = result;
			}
		}));
	}
	
	$scope.ok = function() {
		if($scope.taskId){
			//更新页面模板
			rosteringService.chooseTemplates($scope.taskId,weekDate).then(angular.bind(this, function then() {
				var result = rosteringService.chooseTemplatesResult;
				if(result.code == 0){
					$uibModalInstance.close(result.data);
					$uibModalInstance.dismiss('cancel');
				} else {
	    		$scope.message(result.msg);
	    	}
			}))
		} else {
			$scope.message('Please select a roster template.');
		}
	};

	$scope.cancel = function() {
		$scope.confirmSuccess = function(){
			$uibModalInstance.dismiss('cancel');
		}
		$rootScope.confirm('Close','Exit this page?','OK',$scope.confirmSuccess,'NO');
	};

};

angular.module('kindKidsApp').controller('selectTemplateCtrl',	
		[ '$scope', '$rootScope', 'centerId','weekDate','$uibModalInstance', 'rosteringService', selectTemplateCtrl ]);