'use strict';

function addLeaveCtrl($scope, $rootScope, $uibModalInstance, rosteringService, leaveInfo, callBackFun) {

	$scope.initAddLeave = function () {
		$scope.leaveCondition = {
			id: '',
			startDate: null,
			endDate: null,
			type: null,
			isAdmin: true,
			reason: '',
			isEdit: false
		}
		if (leaveInfo != null && leaveInfo.id != undefined && leaveInfo.id != '') {
			$scope.leaveCondition.id = leaveInfo.id;
			$scope.leaveCondition.startDate = leaveInfo.startDate;
			$scope.leaveCondition.startTime = $scope.changeBackTime(leaveInfo.startDate);
			$scope.leaveCondition.endDate = leaveInfo.endDate;
			$scope.leaveCondition.endTime = $scope.changeBackTime(leaveInfo.endDate);
			$scope.leaveCondition.type = leaveInfo.leaveTypeStr;
			$scope.leaveCondition.reason = leaveInfo.leaveReason;
			$scope.leaveCondition.accountId = leaveInfo.accountId;
			$scope.leaveCondition.isEdit = true;
			$scope.leaveCondition.user = {
				name: leaveInfo.userName,
				avatar: leaveInfo.avatar,
				personColor: leaveInfo.personColor
			}
		}

		// 获取请假类型的list/0:代表是所有请假类型
		rosteringService.getLeaveList(0).then(angular.bind(this, function then() {
			$scope.leaveList = rosteringService.leaveListResult;
		}));
	};

	// 校验当选择leaveType 为年假和长假时，必须为周一开始，周五结束的日期
	$scope.changeType = function () {
		if ($scope.leaveCondition.type != 9 && $scope.leaveCondition.type != 8 && $scope.leaveCondition.type != 7 && $scope.leaveCondition.type != 2 && $scope.leaveCondition.type != 3) {
			$scope.leaveCondition.startTime = '6:00 AM';
			$scope.leaveCondition.endTime = '7:00 PM';
		} else {
			$scope.leaveCondition.startTime = '';
			$scope.leaveCondition.endTime = '';
		}
	}

	$scope.ok = function () {
		console.log('add leave');

		if ($scope.leaveCondition.type == 2) {
			if (dealDate($scope.leaveCondition.startDate).getDay() == 1) {
				$('#addLeaveForm').bootstrapValidator('updateStatus', 'fromDate',
					'VALID');
			} else {
				$('#addLeaveForm').data('bootstrapValidator').updateStatus('fromDate',
					'INVALID', 'digits');
			}
			if (dealDate($scope.leaveCondition.endDate).getDay() == 5) {
				$('#addLeaveForm').bootstrapValidator('updateStatus', 'toDate',
					'VALID');
			} else {
				$('#addLeaveForm').data('bootstrapValidator').updateStatus('toDate',
					'INVALID', 'digits');
			}
		} else {
			$('#addLeaveForm').data('bootstrapValidator').updateStatus('fromDate',
				'VALID', 'digits');
			$('#addLeaveForm').data('bootstrapValidator').updateStatus('fromDate',
				'VALID', 'date');
			$('#addLeaveForm').data('bootstrapValidator').updateStatus('toDate',
				'VALID', 'digits');
			$('#addLeaveForm').data('bootstrapValidator').updateStatus('toDate',
				'VALID', 'date');
		}
		$('#addLeaveForm').data('bootstrapValidator').validate();
		if ($('#addLeaveForm').data('bootstrapValidator').isValid()) {
			$rootScope.confirm('Confirm', 'Are you sure to approve the leave request?', 'OK', function () {
				confirmFun(false);
			}, 'NO');
		}
	};

	function dealDate(dateStr) {
		var date = dateStr.substr(6) + '/' + dateStr.substr(3, 2) + '/' + dateStr.substr(0, 2);
		return new Date(date);
	}

	function confirmFun(confirm) {
		$scope.leaveCondition.startDate = $scope.leaveCondition.startDate ? $scope.leaveCondition.startDate + $scope.changeHeadTime($scope.leaveCondition.startTime) : $scope.leaveCondition.startDate;
		$scope.leaveCondition.endDate = $scope.leaveCondition.endDate ? $scope.leaveCondition.endDate + $scope.changeHeadTime($scope.leaveCondition.endTime) : $scope.leaveCondition.endDate;
		rosteringService.saveMyLeave($scope.leaveCondition, confirm).then(angular.bind(this, function then() {
			var result = rosteringService.saveMyResult;
			if (result.code == 0) {
				result.data.managerApp = true;
				$uibModalInstance.dismiss('cancel');
				$scope.message(result.msg);
				if (callBackFun) {
					callBackFun();
				}
			} else if (result.code == 2) {
				$rootScope.confirm('Confirm', result.msg, 'OK', function () {
					confirmFun(true);
				}, 'NO');
			} else if (result.code == 22) {
				$rootScope.confirm('Warning', result.msg, 'OK');
			} else {
				$scope.message(result.msg);
			}
		}));
	}

	$scope.cancel = function () {
		/* $uibModalInstance.close(false); */
		$scope.confirmSuccess = function () {
			$uibModalInstance.dismiss('cancel');
		}
		$rootScope.confirm('Close', 'Exit this page?', 'OK', $scope.confirmSuccess, 'NO');
	};

};

angular.module('kindKidsApp').controller('addLeaveCtrl', ['$scope', '$rootScope', '$uibModalInstance', 'rosteringService', 'leaveInfo', 'callBackFun', addLeaveCtrl]);