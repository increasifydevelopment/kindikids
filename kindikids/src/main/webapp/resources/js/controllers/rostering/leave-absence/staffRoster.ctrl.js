'use strict';

function leaveAbsenceCtrl($scope, $rootScope, $filter, $timeout, $window,
		$uibModal, commonService, rosteringService) {
	$scope.pageInfo = {};
	$scope.pageInfo.condition = {
		pageIndex : 1,
	};
	$scope.initRosterLeaveItemList = function() {
		rosteringService
				.getAbsenceLeave()
				.then(
						angular
								.bind(
										this,
										function then() {
											$scope.pageInfo.rosterLeaveInfos = rosteringService.absenceLeaveListResult.data;
										}));
	};

	$scope.leaveItemInfo = {
		isAdd : true
	};

	$scope.addColumn = function() {
		$scope.leaveItemInfo = {
			isAdd : true
		};
		$scope.openModal();
	};
	$scope.openModal = function(itemInfo,accountId) {
		if (itemInfo) {
			$scope.leaveItemInfo = {
				id : itemInfo.id,
				accountId : accountId,
				beginAbsenceDate : itemInfo.beginAbsenceDate,
				absenceReason : itemInfo.absenceReason,
				rosterLeaveId : itemInfo.rosterLeaveId,
				isAdd : false
			};
		}
		$uibModal.open({
			templateUrl : 'views/rostering/leave-absence/absence.add.html?res_v='+$scope.res_v,
			controller : 'leaveItemCtrl',
			backdrop : 'static',
			keyboard : false,
			resolve : {
				leaveItemInfo : function() {
					return $scope.leaveItemInfo;
				}
			}
		}).result.then(function(data) {
			$scope.initRosterLeaveItemList();
		});

	};
	
	

};
angular.module('kindKidsApp').controller(
		'leaveAbsenceCtrl',
		[ '$scope', '$rootScope', '$filter', '$timeout', '$window',
				'$uibModal', 'commonService', 'rosteringService',
				leaveAbsenceCtrl ]);