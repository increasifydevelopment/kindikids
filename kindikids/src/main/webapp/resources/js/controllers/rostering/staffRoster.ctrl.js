'use strict';

function staffRosterCtrl($scope, $rootScope, data, $uibModalInstance,
	rosteringService) {

	$scope.dataInfo = data;
	$scope.page = {};
	// 如果shiftTimeCode存在，则实际为添加正常排班教师，选择时间的框为隐藏
	if ($scope.dataInfo.shiftTimeCode) {
		$scope.timeInputShow = true;
		$scope.page.shiftTimeCode = $scope.dataInfo.shiftTimeCode;
	}
	$scope.initModal = function () {
		// 初始化出来所有可选择的排班员工数组
		/*
		 * rosteringService.getRosterStaffOfRole(data.rosterId,data.centreId,data.staffType,data.day).then(angular.bind(this,
		 * function then() { var result = rosteringService.rosterStaffOfRoleResult;
		 * if(result.code){ $scope.staffList = result.data; } else {
		 * $scope.message(result.msg); } }));
		 */
	}



	$scope.ok = function () {
		if ($scope.page.staffId) {
			$('#addSignForm').data('bootstrapValidator').validate();
			var flag = false;
			$("small").map(function () {
				if ($(this).attr('data-bv-result') == "INVALID" && !$($(this).parent().parent()).hasClass('ng-hide')) {
					flag = true;
				}
			});
			if (flag) {
				return;
			}
			// 4中特殊排班员工的处理和正常排班教师处理
			var flag = 0; // 4种特殊员工
			if ($scope.page.shiftTimeCode) {
				flag = 1; // 正常排班教师
			}
			var json = {
				rosterId: $scope.dataInfo.rosterId,
				day: $scope.dataInfo.day,
				staffAccountId: $scope.page.staffId,
				staffType: $scope.dataInfo.staffType,
				shiftId: $scope.dataInfo.shiftId,
				confirm: $scope.addRosterConfirm
			};
			if (flag == 0) {
				// 处理时间格式：2016-09-29T19:49:53.000
				var inNowDate = data.day.substr(0, 10);
				json.startTime = inNowDate + $scope.changeHeadTime($scope.page.sTime);
				json.endTime = inNowDate + $scope.changeHeadTime($scope.page.eTime);
			} else {
				json.shiftTimeCode = $scope.page.shiftTimeCode;
			}
			//add by big
			$scope.alertAddRosterStaffSubmit = true;
			$scope.addRosterStaffSubmit(json, flag);
		} else {
			$scope.message('Please select a staff member.');
		}
	};

	$scope.alertAddRosterStaffSubmit = true;
	$scope.addRosterStaffSubmit = function (json, flag) {
		$scope.showLoading("#myMask");
		rosteringService.addRosterStaff(json, $scope.alertAddRosterStaffSubmit).then(
			angular.bind(this, function then() {
				var result = rosteringService.addRosterStaffResult;
				if (result.code == 0) {
					var outData = {
						flag: flag,
						data: result.data
					};
					$uibModalInstance.close(outData);
					$uibModalInstance.dismiss('cancel');
				} else if (result.code == 2) {
					$rootScope.confirm('Confirm', result.msg, 'OK', function () {
						$scope.addRosterConfirm = true;
						$scope.ok();
					}, 'Cancel', null);
				} else if (result.code == 11) {
					//提醒
					$rootScope.confirm('Confirm', result.msg, 'OK', function () {
						$scope.hideLoading("#myMask");
						$scope.alertAddRosterStaffSubmit = false;
						$scope.addRosterStaffSubmit(json, flag);
					}, 'Cancel', null);
				} else if (result.code == 22) {
					$rootScope.confirm('Warning', result.msg, 'OK');
				} else {
					$scope.message(result.msg);
				}
				$scope.hideLoading("#myMask");
			}));
	};


	$scope.cancel = function () {
		$scope.confirmSuccess = function () {
			$uibModalInstance.dismiss('cancel');
		};
		$rootScope.confirm('Close', 'Exit this page?', 'OK', $scope.confirmSuccess,
			'NO');
	};

};

angular.module('kindKidsApp').controller(
	'staffRosterCtrl',
	['$scope', '$rootScope', 'data', '$uibModalInstance', 'rosteringService',
		staffRosterCtrl
	]);