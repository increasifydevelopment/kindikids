'use strict';

function rejectAndCancelCtrl($scope, $rootScope, $uibModalInstance, rosteringService, itemData, itemNum, callback) {

	$scope.initModal = function () {

		$scope.itemData = itemData;
		$scope.itemNum = itemNum;

		$scope.operaCondition = {
			leaveId: $scope.itemData.id
		}
	}

	$scope.ok = function () {
		$('#rejectAndCancelForm').data('bootstrapValidator').validate();
		if ($('#rejectAndCancelForm').data('bootstrapValidator').isValid()) {
			if ($scope.itemNum == 0) {
				$scope.operaCondition.operaType = 2;
			} else {
				$scope.operaCondition.operaType = 3;
			}
			rosteringService.operaStaffLeave($scope.operaCondition.leaveId, $scope.operaCondition.operaType, $scope.operaCondition.reason).then(angular.bind(this, function then() {
				$scope.operaInResult = rosteringService.operaResult;
				if ($scope.operaInResult.code == 0) {
					$scope.itemData.status = $scope.itemNum == 0 ? 2 : 3;
					$scope.itemData.reason = $scope.operaCondition.reason;
					$rootScope.message($scope.operaInResult.msg);
					$uibModalInstance.dismiss('cancel');
					if (callback) {
						callback();
					}
				} else {
					$rootScope.message($scope.operaInResult.msg);
				}
			}));
		}
	};

	$scope.cancel = function () {
		/*$uibModalInstance.close(false);*/
		$scope.confirmSuccess = function () {
			$uibModalInstance.dismiss('cancel');
		}
		$rootScope.confirm('Close', 'Exit this page?', 'OK', $scope.confirmSuccess, 'NO');
	};

};

angular.module('kindKidsApp').controller('rejectAndCancelCtrl',
	['$scope', '$rootScope', '$uibModalInstance', 'rosteringService', 'itemData', 'itemNum', 'callback', rejectAndCancelCtrl]);