'use strict';

/**
 * @author zhengguo
 * @description staff other edit sign Controller
 */
function staffOtherEditCtrl($scope, $rootScope, $timeout, $location, $uibModal, rosterStaffId, $uibModalInstance, signService, rosteringService, callBackFun, centerId) {

	$scope.initStaffOtherModal = function () {
		// 根据rosterStaffId，获取签到信息
		signService.getSignInfo(3, rosterStaffId).then(angular.bind(this, function then() {
			if (signService.signInfoResult.code == 0) {
				$scope.condition = signService.signInfoResult.data;
				// 转换时间
				if ($scope.condition) {
					if ($scope.condition.day) {
						$scope.condition.inTimeAll = $scope.condition.day.substr(0, 10);
					}
					$scope.condition.rosterTimeStart = $scope.changeBackTime($scope.condition.startTime);
					$scope.condition.rosterTimeEnd = $scope.changeBackTime($scope.condition.endTime);
					if ($scope.condition.signInDate) {
						$scope.condition.staffInTime = $scope.changeBackTime($scope.condition.signInDate);
					}
					if ($scope.condition.signOutDate) {
						$scope.condition.staffOutTime = $scope.changeBackTime($scope.condition.signOutDate);
					}
					// 处理请假时间点
					for (var i = 0; i < $scope.condition.leaves.length; i++) {
						$scope.condition.leaves[i].startTime = $scope.changeBackTime($scope.condition.leaves[i].startDate);
						$scope.condition.leaves[i].endTime = $scope.changeBackTime($scope.condition.leaves[i].endDate);
					}
				}
			}
		}))
	}

	$scope.submitSignInfo = function () {
		$('#staffSignForm').data('bootstrapValidator').validate();
		var flag = false;
		$("small").map(function () {
			if ($(this).attr('data-bv-result') == "INVALID" && !$($(this).parent().parent()).hasClass('ng-hide')) {
				flag = true;
			}
		});
		if (flag) {
			return;
		}
		// 拼接前台时间
		// abliu 20161123注释，如果为空则提交失败
		/*
		 * if(!$scope.condition.day){ $scope.condition.day =
		 * $scope.condition.inTimeAll +"T00:00:00.000"; }
		 */
		$scope.condition.startTime = $scope.condition.inTimeAll + $scope.changeHeadTime($scope.condition.rosterTimeStart);
		$scope.condition.endTime = $scope.condition.inTimeAll + $scope.changeHeadTime($scope.condition.rosterTimeEnd);

		if ($scope.condition.staffInTime) {
			$scope.condition.signInDate = $scope.condition.inTimeAll + $scope.changeHeadTime($scope.condition.staffInTime);
		} else {
			$scope.condition.signInDate = '';
		}
		if ($scope.condition.staffOutTime) {
			$scope.condition.signOutDate = $scope.condition.inTimeAll + $scope.changeHeadTime($scope.condition.staffOutTime);
		} else {
			$scope.condition.signOutDate = '';
		}

		// +++++++++++update by big+++++++
		$scope.comfirmSure = false;
		$scope.saveSignInfoAction();
		// +++++++++++++++++++++++++++++++

		/* $scope.signaturePad1.fromDataURL(qq); */
	};

	$scope.comfirmSure = false;
	$scope.saveSignInfoAction = function () {
		$scope.showLoading("#showLoading");
		signService.saveSignInfo($scope.flag, $scope.condition, $scope.comfirmSure).then(angular.bind(this, function then() {
			$scope.saveResult = signService.saveInfoResult;
			$scope.hideLoading("#showLoading");
			if ($scope.saveResult && $scope.saveResult.code == 0) {
				$uibModalInstance.close(1);
				$uibModalInstance.dismiss('cancel');
			} else if ($scope.saveResult.code == 2) {
				$rootScope.confirm('', $scope.saveResult.msg, 'Confirm', function () {
					$scope.comfirmSure = true;
					$scope.saveSignInfoAction();
				}, 'Cancel', function () {
					$scope.comfirmSure = false;
				});
			} else {
				$scope.message($scope.saveResult.msg);
			}
		}));
	};

	$scope.cancel = function () {
		/* $uibModalInstance.close(false); */
		$scope.confirmSuccess = function () {
			$uibModalInstance.dismiss('cancel');
		}
		$scope.confirm('Close', 'Exit this page?', 'OK', $scope.confirmSuccess, 'NO');
	};
	/**
	 * add leave
	 */
	$scope.addFlag = false;
	// 初始化请假对象
	$scope.initLeaveInfo = function () {
		$scope.leaveInfo = {
			id: null,
			startTime: null,
			endTime: null,
			type: "9",
			reason: null,
			isAdmin: true,
			centerId: centerId
		}
	}

	$scope.addLeave = function (flag) {
		$scope.addFlag = flag;
		if (!flag) {
			$scope.initLeaveInfo();
		}
	}

	$scope.saveLeave = function (confirm) {
		// 处理请假人
		$scope.leaveInfo.accountId = $scope.condition.staffAccountId;
		// 验证
		// 处理时间
		$scope.leaveInfo.startDate = $scope.leaveInfo.startDate ? $scope.leaveInfo.startDate + $scope.changeHeadTime($scope.leaveInfo.startTime) : $scope.leaveInfo.startDate;
		$scope.leaveInfo.endDate = $scope.leaveInfo.endDate ? $scope.leaveInfo.endDate + $scope.changeHeadTime($scope.leaveInfo.endTime) : $scope.leaveInfo.endDate;
		rosteringService.saveStaffLeave($scope.leaveInfo, confirm).then(function () {
			var saveStaffLeaveResult = rosteringService.saveResult;
			if (saveStaffLeaveResult.code == 0) {
				$scope.addFlag = false;
				$scope.message(saveStaffLeaveResult.msg);
				$scope.initStaffOtherModal();
				$scope.initLeaveInfo();
				callBackFun();
			} else if (saveStaffLeaveResult.code == 2) {
				$rootScope.confirm('Confirm', saveStaffLeaveResult.msg, 'OK', function () {
					$scope.saveLeave(true);
				}, 'NO');
			} else if (saveStaffLeaveResult.code == 22) {
				$rootScope.confirm('Warning', saveStaffLeaveResult.msg, 'OK');
			} else {
				$scope.message(saveStaffLeaveResult.msg);
			}
		});
	}

	$scope.editLeave = function (leaveInfo) {
		if (leaveInfo.disabled) {
			leaveInfo.disabled = false;
		} else {
			leaveInfo.disabled = true;
		}

	}

	$scope.updateLeave = function (info, confirm) {
		var leaveInfo = {};
		leaveInfo.id = info.id;
		leaveInfo.startDate = info.startDate ? info.startDate + $scope.changeHeadTime(info.startTime) : info.startDate;
		leaveInfo.endDate = info.endDate ? info.endDate + $scope.changeHeadTime(info.endTime) : info.endDate;
		leaveInfo.type = info.leaveTypeStr;
		leaveInfo.reason = info.leaveReason;
		leaveInfo.accountId = info.accountId;
		leaveInfo.isAdmin = true;
		rosteringService.saveStaffLeave(leaveInfo, confirm).then(function () {
			var saveStaffLeaveResult = rosteringService.saveResult;
			if (saveStaffLeaveResult.code == 0) {
				$scope.addFlag = false;
				$scope.message(saveStaffLeaveResult.msg);
				$scope.initStaffOtherModal();
				$scope.initLeaveInfo();
				callBackFun();
			} else if (saveStaffLeaveResult.code == 2) {
				$rootScope.confirm('Confirm', saveStaffLeaveResult.msg, 'OK', function () {
					$scope.updateLeave(info, true);
				}, 'NO');
			} else {
				$scope.message(saveStaffLeaveResult.msg);
			}
		});
	}

	function dealDate(dateStr) {
		if (!dateStr) {
			return;
		}
		if (dateStr.split(' ').length == 1) {
			return dateStr.split(' ')[0].split('/')[2] + '-' + dateStr.split(' ')[0].split('/')[1] + '-' + dateStr.split(' ')[0].split('/')[0];
		}
		return dateStr.split(' ')[0].split('/')[2] + '-' + dateStr.split(' ')[0].split('/')[1] + '-' + dateStr.split(' ')[0].split('/')[0] + 'T' + dateStr.split(' ')[1] + ':00.000';
	}

	// 拒绝或者取消请假
	$scope.cancelLeave = function (item) {
		$uibModal.open({
			templateUrl: 'views/rostering/rejectAndCancelModal.html?res_v=' + $scope.res_v,
			controller: 'rejectAndCancelCtrl',
			backdrop: 'static',
			keyboard: false,
			scope: $scope,
			resolve: {
				itemData: function () {
					return item;
				},
				itemNum: function () {
					return 1; // 1 代表取消
				},
				callback: function () {
					return cancelCallbackFun;
				}
			}
		})
	};

	function cancelCallbackFun() {
		$scope.initStaffOtherModal();
		callBackFun();
	}

	$scope.changeType = function () {
		if ($scope.leaveInfo.type != 9 && $scope.leaveInfo.type != 8 && $scope.leaveInfo.type != 7 && $scope.leaveInfo.type != 2 && $scope.leaveInfo.type != 3) {
			$scope.leaveInfo.startTime = $scope.condition.rosterTimeStart;
			$scope.leaveInfo.endTime = $scope.condition.rosterTimeEnd;
		} else {
			$scope.leaveInfo.startTime = '';
			$scope.leaveInfo.endTime = '';
		}
	}

	$scope.changeTypeStr = function (info) {
		if (info.leaveTypeStr != "9" && info.leaveTypeStr != "8" && info.leaveTypeStr != "7" && info.leaveTypeStr != "2" && info.leaveTypeStr != "3") {
			info.startTime = $scope.condition.rosterTimeStart;
			info.endTime = $scope.condition.rosterTimeEnd;
		} else {
			info.startTime = '';
			info.endTime = '';
		}
	}

}
angular.module('kindKidsApp').controller('staffOtherEditCtrl',
	['$scope', '$rootScope', '$timeout', '$location', '$uibModal', 'rosterStaffId', '$uibModalInstance', 'signService', 'rosteringService', 'callBackFun', 'centerId', staffOtherEditCtrl]);