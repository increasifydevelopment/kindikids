'use strict';

function saveTemplateCtrl($scope, $rootScope, $uibModalInstance,rosterId,type, rosteringService) {

	$scope.ok = function() {
		if ($scope.templateName) {
			// 调用保存模板方法,并提示返回的信息
			// 保存template
			rosteringService.setRosterTemplate(rosterId, type, $scope.templateName).then(angular.bind(this, function then() {
				var result = rosteringService.setRosterTemplateResult;
				if (result.code == 0) {
					$uibModalInstance.close(true);
				}
				$scope.message(result.msg);
			}));
		} else {
			$scope.message('Please enter template name.');
		}
	};

	$scope.cancel = function() {
		/* $uibModalInstance.close(false); */
		$scope.confirmSuccess = function() {
			$uibModalInstance.dismiss('cancel');
		};
		$rootScope.confirm('Close', 'Exit this page?', 'OK', $scope.confirmSuccess, 'NO');
	};

};

angular.module('kindKidsApp').controller('saveTemplateCtrl', [ '$scope', '$rootScope', '$uibModalInstance','rosterId','type', 'rosteringService', saveTemplateCtrl ]);