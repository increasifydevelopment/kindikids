'use strict';

function leaveItemCtrl($scope, $rootScope, $filter, $timeout, $window,
		$uibModal, $uibModalInstance, leaveItemInfo, commonService,
		rosteringService) {

	$scope.itemInfo = leaveItemInfo;

	// 保存
	$scope.save = function(itemInfo) {
		$('#editLeaveForm').data('bootstrapValidator').validate();
		if ($('#editLeaveForm').data('bootstrapValidator').isValid()) {
			var saveItemInfo = {
				accountId : itemInfo.accountId,
				itemList : [ {
					id : itemInfo.id,
					beginAbsenceDate : itemInfo.beginAbsenceDate,
					endAbsenceDate : itemInfo.endAbsenceDate,
					absenceReason : itemInfo.absenceReason
				} ]
			};
			rosteringService
					.saveAbsenceLeave(saveItemInfo)
					.then(
							angular
									.bind(
											this,
											function then() {
												var saveResult = rosteringService.saveAbsenceLeaveResult;
												if (saveResult.code == 0) {
													$uibModalInstance
															.close(saveResult.data);
												} else {
													$rootScope
															.message(saveResult.msg);
												}
											}));
		}
	};

	// 删除
	$scope.del = function(itemId) {
		if (!itemId) {
			return;
		}
		$scope.confirm('Delete', 'Are you sure you want to delete?', 'Yes', function() {
		rosteringService.delAbsenceLeave(itemId).then(
				angular.bind(this, function then() {
					var delResult = rosteringService.delAbsenceLeaveResult;
					if (delResult.code == 0) {
						$uibModalInstance.close();
					} else {
						$rootScope.message(delResult.msg);
					}
				}));
		},'No',null);

	};
	$scope.cancel = function() {
		$scope.confirmSuccess = function() {
			$uibModalInstance.dismiss('cancel');
			$scope.itemInfo = leaveItemInfo;
		};
		$rootScope.confirm('Close', 'Exit this page?', 'OK',
				$scope.confirmSuccess, 'NO');
	};

};

angular.module('kindKidsApp').controller(
		'leaveItemCtrl',
		[ '$scope', '$rootScope', '$filter', '$timeout', '$window',
				'$uibModal', '$uibModalInstance', 'leaveItemInfo',
				'commonService', 'rosteringService', leaveItemCtrl ]);