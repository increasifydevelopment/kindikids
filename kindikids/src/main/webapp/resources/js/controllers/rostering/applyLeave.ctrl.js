'use strict';

function applyLeaveCtrl($scope, $rootScope, $uibModalInstance, itemData, rosteringService) {

	$scope.itemData = itemData;

	$scope.initApplyLeave = function () {

		$scope.leaveCondition = {
			startDate: '',
			endDate: '',
			type: '',
			isAdmin: false
		}
		// 获取请假类型的list/0:代表是所有请假类型
		rosteringService.getLeaveList(1).then(angular.bind(this, function then() {
			$scope.leaveList = rosteringService.leaveListResult;
		}));
	}

	// 校验当选择leaveType 为年假和长假时，必须为周一开始，周五结束的日期
	$scope.changeType = function () {
		if ($scope.leaveCondition.type != 9 && $scope.leaveCondition.type != 8 && $scope.leaveCondition.type != 7 && $scope.leaveCondition.type != 2 && $scope.leaveCondition.type != 3) {
			$scope.leaveCondition.startTime = '6:00 AM';
			$scope.leaveCondition.endTime = '7:00 PM';
		} else {
			$scope.leaveCondition.startTime = '';
			$scope.leaveCondition.endTime = '';
		}
	}

	$scope.ok = function (confirm) {
		console.log('apply leave');
		$('#applyLeaveForm').data('bootstrapValidator').validate();
		if ($('#applyLeaveForm').data('bootstrapValidator').isValid()) {
			$scope.showLoading('#applyLeaveForm');
			$scope.leaveCondition.startDate = $scope.leaveCondition.startDate ? $scope.leaveCondition.startDate + $scope.changeHeadTime($scope.leaveCondition.startTime) : $scope.leaveCondition.startDate;
			$scope.leaveCondition.endDate = $scope.leaveCondition.endDate ? $scope.leaveCondition.endDate + $scope.changeHeadTime($scope.leaveCondition.endTime) : $scope.leaveCondition.endDate;
			rosteringService.saveStaffLeave($scope.leaveCondition, confirm).then(angular.bind(this, function then() {
				if (rosteringService.saveResult) {
					$scope.newLeave = rosteringService.saveResult;
					$scope.hideLoading('#applyLeaveForm');
					// $rootScope.message($scope.newLeave.msg);
					// 刷新页面重新获取列表，分页实时应用的需要
					if ($scope.newLeave.code == 0) {
						// 页面刷新方法
						$scope.getPage();
						/* $scope.leaveList.data.list.unshift($scope.newLeave.data); */
						$uibModalInstance.dismiss('cancel');
					} else if ($scope.newLeave.code == 2) {
						$rootScope.confirm('Confirm', $scope.newLeave.msg, 'OK', function () {
							$scope.ok(true);
						}, 'NO');
					} else {
						$scope.message($scope.newLeave.msg);
					}
				}
			}));
		}
	};

	$scope.cancel = function () {
		/* $uibModalInstance.close(false); */
		$scope.confirmSuccess = function () {
			$uibModalInstance.dismiss('cancel');
		}
		$rootScope.confirm('Close', 'Exit this page?', 'OK', $scope.confirmSuccess, 'NO');
	};
};

angular.module('kindKidsApp').controller('applyLeaveCtrl',
	['$scope', '$rootScope', '$uibModalInstance', 'itemData', 'rosteringService', applyLeaveCtrl]);