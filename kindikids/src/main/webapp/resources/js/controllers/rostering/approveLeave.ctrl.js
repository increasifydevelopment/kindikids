'use strict';

function approveLeaveCtrl($scope, $rootScope, $uibModalInstance, itemData,managerApp, rosteringService) {

	$scope.itemData = itemData;

	$scope.initApproveLeave = function() {
		// 增加modal框样式
		$('.modal').addClass('leave-approve-popup');
		// 获取列表数据
		rosteringService.getLeaveDate($scope.itemData.id).then(angular.bind(this, function then() {
			$scope.leaveList = rosteringService.leaveListResult;
			for (var i = 0; i < $scope.leaveList.data.length; i++) {
				$scope.leaveList.data[i].paidFlag = '1';
			}
		}));
	}

	$scope.ok = function(confirm) {
		if(managerApp){
			confirm=managerApp;
		}
		rosteringService.operaStaffLeave($scope.itemData.id, 1, '', $scope.leaveList.data, confirm).then(angular.bind(this, function then() {
			$scope.operaInResult = rosteringService.operaResult;
			if ($scope.operaInResult.code == 0) {
				$scope.itemData.status = 1;
				$rootScope.message($scope.operaInResult.msg);
				$scope.getPage();
				$uibModalInstance.dismiss('cancel');
			}
			if ($scope.operaInResult.code == 1) {
				$scope.message($scope.operaInResult.msg);
			}
			if ($scope.operaInResult.code == 2) {
				$rootScope.confirm('Confirm', $scope.operaInResult.msg, 'OK', function() {
					$scope.ok(true);
				}, 'NO');
			}
		}));
	};

	$scope.cancel = function() {
		$scope.confirmSuccess = function() {
			rosteringService.goRequestState($scope.itemData.id).then(angular.bind(this, function then() {
				$scope.getPage();
				$uibModalInstance.dismiss('cancel');
			}));
		};
		$rootScope.confirm('Close', 'Exit this page?', 'OK', $scope.confirmSuccess, 'NO');
	};

};

angular.module('kindKidsApp').controller('approveLeaveCtrl',
		[ '$scope', '$rootScope', '$uibModalInstance', 'itemData','managerApp', 'rosteringService', approveLeaveCtrl ]);