'use strict';

/**
 * @author zhengguo
 * @description staffLeave Controller
 */
function rosteringMainCtrl($scope, $rootScope, $filter, $timeout, $window, $uibModal, rosteringService, staffService, commonService) {

	$scope.initRosteringMain = function () {
		$scope.mainPage = {
			centerId: '',
			roomId: '',
			role: '',
			staffType: '',
			roster: '',
			leaveType: null
		};

		//搜索条件过滤器
		/*		$scope.roleFilter = function(item){
					if($scope.currUser.roleValue == 0){
						return true;
					}
					if($scope.currUser.roleValue == 1 || $scope.currUser.roleValue == 5){
						if(item.value == 1 || item.value == 2 || item.value == 9){
							return true;
						}
						return false;
					}
					return false;
				}*/
		$scope.reloadMenuOrgs = function () {

			commonService.getMenuOrgs(6).then(angular.bind(this, function then() {
				$scope.roles = commonService.orgList;
			}));
		};
		$scope.reloadMenuOrgs();
		rosteringService.getLeaveList(0).then(angular.bind(this, function then() {
			$scope.leftLeaveList = rosteringService.leaveListResult.data;
		}));
		/*	staffService.getRoleList().then(angular.bind(this, function then() {
				//获取role/center/room/group list
				$scope.roles = staffService.roleResult; 
			}))*/

		$scope.currUser = {
			roleValue: 0,
			centersId: ''
		};
	};
}
angular.module('kindKidsApp').controller('rosteringMainCtrl', ['$scope', '$rootScope', '$filter', '$timeout', '$window', '$uibModal', 'rosteringService', 'staffService', 'commonService', rosteringMainCtrl]);