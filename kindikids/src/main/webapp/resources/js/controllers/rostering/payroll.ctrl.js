'use strict';

function payrollCtrl($scope, $rootScope, $uibModalInstance, $window, rosteringService) {

    $scope.selectCondition = {
        centreIds: [],
        staffIds: [],
        date: null,
        startDate: null,
        endDate: null
    }

    var baseFormater = 'YYYY-MM-DDT00:00:00.000';
    $scope.selectCondition.startDate = moment().format(baseFormater);
    $scope.selectCondition.endDate = moment().format(baseFormater);

    $scope.export = function () {
        $scope.showLoading('#payroll');
        rosteringService.payrollData($scope.selectCondition).then(function () {
            $scope.hideLoading('#payroll');
            var result = rosteringService.payrollDataResult;
            if (result.code == 0) {
                $window.location.href = './rosterstaff/payrollExport.do?' + 'id=' + result.data;
                $uibModalInstance.dismiss('cancel');
            }
        });
    }

    $scope.cancel = function () {
        $scope.confirmSuccess = function () {
            $uibModalInstance.dismiss('cancel');
        }
        $rootScope.confirm('Close', 'Exit this page?', 'OK', $scope.confirmSuccess, 'NO');
    };

};

angular.module('kindKidsApp').controller('payrollCtrl',
    ['$scope', '$rootScope', '$uibModalInstance', '$window', 'rosteringService', payrollCtrl]);