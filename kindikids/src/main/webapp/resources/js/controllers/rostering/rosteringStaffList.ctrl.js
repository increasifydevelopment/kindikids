'use strict';

/**
 * @author zhengguo
 * @description rosteringStaffList Controller
 */
function rosteringStaffListCtrl($scope, $rootScope, $filter, $timeout, $window, $uibModal, rosteringService, staffService, attendanceService) {

	// variable=================================================================  
	$scope.page = {};
	/*$scope.noList = true;*/
	$scope.rosterShow = true; //是否只显示当前用户的roster / 1为否，所有人
	$scope.staffTypeShow = 1; // 1 所有人    2 全职   3 兼职
	$rootScope.searchRoleAndType = true;
	//add by gfwang 控制是否显示Myrost
	$rootScope.showRosterOrg = true;
	//是否在请假页面
	$rootScope.isInLeavePage = false;
	$scope.page.condition = {
		keyword: null,
		time: null,
		roomId: $scope.mainPage.roomId,
		centersId: $scope.mainPage.centerId,
		type: 2, //type 0:传日期进行搜索，1：上一周 2：本周 3：下一周 4: 左侧菜单，保留weekdate查询
		weekDate: null,
		listType: 1
	};
	//判断本周最大列集合数
	$scope.maxRowLength = 0;
	$scope.maxRowList = [];
	$scope.weekRowList = [];
	$scope.maxRowListIndex = 0;
	$scope.currentRepeadChildList = null;
	$scope.sourceLength = 0;
	$scope.yearList = [];

	$scope.todayIndex = "";

	$scope.isCeoCm = ($filter('haveRole')($scope.currentUser.roleInfoVoList, '1;5;0'));

	$scope.initTodayIndex = function (weekDays) {
		$scope.todayIndex = "";
		for (var i = 0, m = weekDays.length; i < m; i++) {
			if (weekDays[i].today) {
				if (i == 0) {
					$scope.todayIndex = 0;
					return;
				}
				$scope.todayIndex = i - 1;
			}
		}

	};

	/*$scope.isCenterManager = $filter('haveRole')($scope.currentUser.roleInfoVoList, '1;5');*/
	//监测外层包裹页面中的role的变化，会随之变化LIST	
	$scope.$watch('mainPage.role', function (newValue) {
		$scope.roleShow = $scope.mainPage.role;
	});

	//监测外层包裹页面中的staffType的变化，会随之变化LIST	
	// 1 所有人    2 全职   3 兼职
	$scope.$watch('mainPage.staffType', function (newValue) {
		if (newValue) {
			$scope.staffTypeShow = newValue;
		}
	});

	//监测外层包裹页面中的roster的变化，会随之变化LIST	
	$scope.$watch('mainPage.roster', function (newValue) {
		if (newValue == 1) {
			$scope.rosterShow = true;
		}
		if (newValue == 2) {
			//myroster
			$scope.rosterShow = false;
		}
	});

	//监测外层包裹页面中的center的变化，会随之变化LIST	
	$scope.$watch('mainPage.centerId', function (newValue) {
		$scope.page.condition.centersId = $scope.mainPage.centerId;
		//初始化时，无weekDate，不能让type 为4
		if ($scope.page.condition.weekDate) {
			$scope.page.condition.type = 4; //本页保留weekdate查询
		}
		if ($scope.page.condition.centersId == "" || $scope.page.condition.centersId == undefined) {
			return;
		}
		$scope.getAttendanceList();
	});

	/**
	 * @author abliu
	 * @description
	 * @returns 获取attendance列表
	 */
	$scope.getAttendanceList = function () {
		//园长默认选择当前园add by gfwang
		/*	if ($scope.isCenterManager && $scope.currentUser.userInfo.centersId) {
				$scope.page.condition.centersId = $scope.currentUser.userInfo.centersId;
			}*/
		var centreId = $scope.page.condition.centersId;
		attendanceService.getRosterAttendanceList($scope.page.condition).then(angular.bind(this, function then() {
			var result = attendanceService.listResult;
			if (result.code == 0) {
				getMaxRowLength(result.data.weekDays);
				$scope.page = result.data;
				if (!$scope.page.condition.currentCentreManager && $rootScope.isCenterManager) {
					$rootScope.rosteringHide = true;
				} else {
					$rootScope.rosteringHide = false;
				}
				//通过now 与weekdate判断是否下周可以请求

				//根据页面来的日期，Employment Type 等获取rosterstaff 数据
				//排班列表必须是存在园的情况下才有
				$scope.initTodayIndex($scope.page.weekDays);
				if (centreId) {
					$scope.page.condition.centersId = centreId;
					$scope.getRosterStaff();
				}
			} else {
				$.messager.popup(result.msg);
			}
		}));
	};

	function getMaxRowLength(list) {
		$scope.maxRowLength = 0;
		$scope.maxRowListIndex = 0;
		$scope.maxRowList = [];
		$scope.weekRowList = [];
		for (var i = 0, m = list.length; i < m; i++) {
			var length = list[i].signs.length;
			if (length > $scope.maxRowLength) {
				$scope.maxRowLength = length;
			}
		}

		for (var i = 0, m = $scope.maxRowLength; i < m; i++) {
			$scope.maxRowList.push({
				"id": $scope.sourceLength
			});
			$scope.sourceLength++;
		}
		for (var i = 0, m = 7; i < m; i++) {
			$scope.weekRowList.push({
				"id": i + 1 + $scope.sourceLength
			});
			$scope.sourceLength++;
		}

	};

	/**
	 * @author abliu
	 * @description
	 * @returns 获取园区及room集合
	 */
	$scope.getMenuOrgs = function () {
		commonService.getMenuOrgs(4).then(angular.bind(this, function then() {
			$scope.orgList = commonService.orgList;
		}));
	};

	/**
	 * @author abliu
	 * @description
	 * @returns 设置年份集合，从2010年开始到当前年加1年
	 */
	$scope.getYearList = function () {
		var yyyy = new Date().getFullYear();
		var m = yyyy - 2015;
		for (var i = 0; i <= m + 1; i++) {
			$scope.yearList.push(2015 + i);
		}
	};

	/**
	 * @author abliu
	 * @description
	 * @returns 设置年份
	 */
	$scope.setTimeY = function (yyyy) {
		if ($scope.page.condition.time != null) {
			var times = $scope.page.condition.time.split("/");
			if (times.length > 1) {
				$scope.page.condition.time = yyyy + "/" + times[1];
				$scope.page.condition.type = 0;
				$scope.getAttendanceList();
			}
		}
	};

	/**
	 * @author abliu
	 * @description
	 * @returns 设置月份
	 */
	$scope.setTimeM = function (mm) {
		if ($scope.page.condition.time != null) {
			var times = $scope.page.condition.time.split("/");
			if (times.length > 1) {
				$scope.page.condition.time = times[0] + "/" + mm;
				$scope.page.condition.type = 0;
				$scope.getAttendanceList();
			}
		}
	};


	/**
	 * @author abliu
	 * @description
	 * @returns 设置type
	 */
	$scope.setType = function (type) {
		$scope.page.condition.type = type;
		$scope.getAttendanceList();
	};

	$scope.initRosteringStaffList = function () {
		//初始化时会给mainPage.centerId赋值，给对应的页面位置的dom标签上增加active class
		//TODO
		//初始化给role赋值为0；所有人
		$scope.roleShow == 0

		$scope.getAttendanceList();
		$scope.getYearList();
		//获取请假类型的list/0:代表是所有请假类型
		rosteringService.getLeaveList(2).then(angular.bind(this, function then() {
			$scope.leaveList = rosteringService.leaveListResult;
		}));
		//固定周一到周五5天，循环使用
		$scope.rosterWeekList = [0, 1, 2, 3, 4];
	};

	function dateComp(chooseDate) {
		return new moment(chooseDate, 'YYYY-MM-DD').isBefore(new moment($scope.currentUser.currtenDate, 'DD/MM/YYYY'))
	}
	//处理后台获取的数据格式
	$scope.manageData = function (data) {
		$scope.noList = false;
		$scope.noListForPTUser = false;
		$scope.rosterPageInfo = {};
		$scope.rosterPageInfo = data;
		//计算Centre Manager，Additional Staff, Cook，Sign Staff 数组的最大长度，方便循环使用
		var cmMaxlength = 0;
		var asMaxlength = 0;
		var cookMaxlength = 0;
		var ssMaxlength = 0;
		var shiftMaxlength = 0;
		//固定每周周一到周五5天
		var addPersonObj = {
			staffAccountId: 1
		};
		for (var i = 0; i < 5; i++) {
			var isBeforDay = dateComp($scope.rosterPageInfo.roleDayVos[i].day);
			$scope.rosterPageInfo.roleDayVos[i].isBeforDay = isBeforDay;
			//在最后一个位置加上一个空对象，以显示页面增加人的圆框展示
			$scope.rosterPageInfo.roleDayVos[i].roleRosterStaffMap[0].push(addPersonObj);
			//固定每天该对象内，有4中role类型的数组Center Manager，Additional Staff, Cook，Sign Staff 
			if ($scope.rosterPageInfo.roleDayVos[i].roleRosterStaffMap[0].length > cmMaxlength) {
				cmMaxlength = $scope.rosterPageInfo.roleDayVos[i].roleRosterStaffMap[0].length;
			}
			//在最后一个位置加上一个空对象，以显示页面增加人的圆框展示
			$scope.rosterPageInfo.roleDayVos[i].roleRosterStaffMap[2].push(addPersonObj);
			if ($scope.rosterPageInfo.roleDayVos[i].roleRosterStaffMap[2].length > asMaxlength) {
				asMaxlength = $scope.rosterPageInfo.roleDayVos[i].roleRosterStaffMap[2].length;
			}
			//在最后一个位置加上一个空对象，以显示页面增加人的圆框展示
			$scope.rosterPageInfo.roleDayVos[i].roleRosterStaffMap[1].push(addPersonObj);
			if ($scope.rosterPageInfo.roleDayVos[i].roleRosterStaffMap[1].length > cookMaxlength) {
				cookMaxlength = $scope.rosterPageInfo.roleDayVos[i].roleRosterStaffMap[1].length;
			}
			//在最后一个位置加上一个空对象，以显示页面增加人的圆框展示
			$scope.rosterPageInfo.roleDayVos[i].roleRosterStaffMap[3].push(addPersonObj);
			if ($scope.rosterPageInfo.roleDayVos[i].roleRosterStaffMap[3].length > ssMaxlength) {
				ssMaxlength = $scope.rosterPageInfo.roleDayVos[i].roleRosterStaffMap[3].length;
			}
			for (var j = 0; j < $scope.rosterPageInfo.rosterStaffShiftTimeVos.length; j++) {
				var date = $scope.rosterPageInfo.rosterStaffShiftTimeVos[j].rosterStaffShiftVos[i].day;
				var isStaffBeforDay = dateComp(date);
				$scope.rosterPageInfo.rosterStaffShiftTimeVos[j].rosterStaffShiftVos[i].isBeforDay = isStaffBeforDay;
			}
		}

		$scope.cmMaxArr = [];
		for (var i = 0; i < cmMaxlength; i++) {
			$scope.cmMaxArr.push(i);
		}
		$scope.asMaxArr = [];
		for (var i = 0; i < asMaxlength; i++) {
			$scope.asMaxArr.push(i);
		}
		$scope.cookMaxArr = [];
		for (var i = 0; i < cookMaxlength; i++) {
			$scope.cookMaxArr.push(i);
		}
		$scope.ssMaxArr = [];
		for (var i = 0; i < ssMaxlength; i++) {
			$scope.ssMaxArr.push(i);
		}
		$scope.shiftMaxArr = [];
		for (var i = 0; i < $scope.rosterPageInfo.rosterStaffShiftTimeVos.length; i++) {
			$scope.shiftMaxArr.push(i);
		}

	}

	//控制虚线展示
	$scope.dateListComplete = false;
	$scope.rpUserId = "";
	//获取
	$scope.getRosterStaff = function () {
		//参数day，roleId，centerId
		var json = {
			day: $scope.page.condition.weekDate, //本周的任意一天后台都会处理，给weekDate即可
			staffType: null,
			centerId: $scope.page.condition.centersId
		};
		rosteringService.getRosterStaffList(json).then(angular.bind(this, function then() {
			var result = rosteringService.rosterStaffListResult;
			if (result.code) {
				if (result.data) {
					$scope.manageData(result.data);
					//判断责任人
					$scope.rpUserId = getRP($scope.rosterPageInfo);
				}
				if (result.data == null || result.data.rosterInfo == null) {
					//data为null 2种情况
					//1、角色为ceo 或者当前园的院长、0/1/5 展示可添加排班模板的空页面
					var roleResult = $filter('haveRole')($scope.currentUser.roleInfoVoList, '0;1;5');
					//add by big 如果是园长但是没有园区 那么不给新增显示请假信息
					//++++++++++++++++++++++++++++++++++++++++++++++++++++
					if ($scope.isCenterManager && !$scope.currentUser.userInfo.centersId) {
						roleResult = false;
					}
					//++++++++++++++++++++++++++++++++++++++++++++++++++++
					if (roleResult) {
						$scope.noList = true;
						$scope.noListForPTUser = false;
						if (result.data == null) {
							$scope.rosterPageInfo = {};
						}

					} else {
						//2、其他角色进入列表展示页面，并看排班的状态，如果状态为approve状态，则显示，否则只显示请假信息
						//但是需要清理掉页面排班信息
						if (result.data == null) {
							$scope.rosterPageInfo = {};
						}
						$scope.noList = false;
						$scope.noListForPTUser = true;
					}
				}

				//abliu 20161123 定时执行虚线显示
				$scope.dateListComplete = true;
			} else {
				$scope.message(result.msg);
			}
		}));
	};

	$scope.todayList = function () {
		$scope.page.condition.type = 2; //type 0:传日期进行搜索，1：上一周 2：本周 3：下一周 4: 左侧菜单，保留weekdate查询
		$scope.getAttendanceList();
	};


	//请求创建空的roster
	//require : centreId,day   $scope.page.condition.now,
	$scope.blankRoster = function () {
		if ($scope.page.condition.centersId) {
			rosteringService.createBlankRoster($scope.page.condition.centersId, $scope.page.condition.weekDate).then(angular.bind(this, function then() {
				var result = rosteringService.blankRosterResult;
				if (result.code == 0) {
					$scope.manageData(result.data);
				} else {
					$scope.message(result.msg);
				}
			}));
		} else {
			$scope.message('choose a centre first');
		}
	};

	$scope.removeStaffAlert = true;
	$scope.removeStaff = function (item, index) {
		$rootScope.confirm("Delete?", "Are you sure you want to remove this staff member?", 'OK', function () {
			$scope.removeStaffAlert = true;
			$scope.removeStaffSubmit(item, index);
		}, 'NO');
	};

	$scope.removeStaffSubmit = function (item, index) {
		rosteringService.deleteRosterStaff(item[index].id, $scope.removeStaffAlert).then(angular.bind(this, function then() {
			var result = rosteringService.deleteRosterStaffResult;
			if (result.code == 0) {
				var addPersonObj = {
					staffAccountId: 1
				};
				//前端删除自己,填充空对象
				item.splice(index, 1, addPersonObj);
				$scope.reloadMenuOrgs();
				//刷新页面解决其有可能会转移到未排班已签到行
				$scope.getRosterStaff();
			} else if (result.code == 11) {
				//提醒
				$rootScope.confirm('confirm', result.msg, 'OK', function () {
					$scope.removeStaffAlert = false;
					$scope.removeStaffSubmit(item, index);
				}, 'Cancel', null);
			} else {
				$scope.message(result.msg);
			}
		}));
	};



	$scope.changeWeekDate = function (num) {
		if (num == 0) {
			return 'Monday';
		}
		if (num == 1) {
			return 'Tuesday';
		}
		if (num == 2) {
			return 'Wednesday';
		}
		if (num == 3) {
			return 'Thursday';
		}
		if (num == 4) {
			return 'Friday';
		}
	}

	$scope.alert = true;
	$scope.setRosterStatu = function (type, title) {
		if ($scope.rosterPageInfo && $scope.rosterPageInfo.rosterInfo && $scope.rosterPageInfo.rosterInfo.id) {
			if ($scope.rosterPageInfo.rosterInfo.state <= type) {

				var tipMsg = validationEmpty();
				$scope.confirmSuccess = function () {
					//update by big
					$scope.alert = true;
					$scope.setRosterStatuSubmit(type, title);
				};
				if (tipMsg) {
					$rootScope.confirm(title, tipMsg, 'OK', $scope.confirmSuccess, 'NO');
				} else {
					$scope.confirmSuccess();
				}
			} else {
				$scope.message('status can not change to the same and lower');
			}
		} else {
			$scope.message('Please select a roster.');
		}
	};

	/**
	 * 提交请求
	 * update by big
	 */
	$scope.setRosterStatuSubmit = function (type, title) {
		rosteringService.setRosterStatu($scope.rosterPageInfo.rosterInfo.id, type, $scope.alert).then(angular.bind(this, function then() {
			var result = rosteringService.setRosterStatuResult;
			if (result.code == 0) {
				$scope.rosterPageInfo.rosterInfo.state = type;
				//为approve的时候才刷新左侧
				if (type == 2) {
					$scope.reloadMenuOrgs();
				}
				$scope.message(result.msg);
			} else if (result.code == 11) {
				//提醒
				$rootScope.confirm('confirm', result.msg, 'OK', function () {
					$scope.alert = false;
					$scope.setRosterStatuSubmit(type, title);
				}, 'Cancel', null);
			} else {
				$scope.message(result.msg);
			}
		}));
	};

	function validationEmpty() {
		//提示有未填充的排班位置数量
		var tipMsg = '';
		var tipNum = 0;
		//先检查特殊情况的center Manager  / cook  / additial staff
		//每周固定的5天
		for (var i = 0; i < 5; i++) {
			//三种特殊类型，为固定的前三个 0 center Manager
			//如果是有一个；即为增加圆框，表示源数据为0
			if ($scope.rosterPageInfo.roleDayVos[i].roleRosterStaffMap[0].length == 1) {
				tipNum = tipNum + 1;
			}
		}
		if (tipNum > 0) {
			tipMsg = tipMsg + 'Please fill the Centre Manager\'s shift</br>';
		}
		tipNum = 0;
		for (var i = 0; i < 5; i++) {
			//三种特殊类型，为固定的前三个 1 Cook
			//如果是有一个；即为增加圆框，表示源数据为0
			if ($scope.rosterPageInfo.roleDayVos[i].roleRosterStaffMap[1].length == 1) {
				tipNum = tipNum + 1;
			}
		}
		if (tipNum > 0) {
			tipMsg = tipMsg + 'Please fill the Cook\'s shift</br>';
		}
		/*	tipNum = 0;
			for (var i = 0; i < 5; i++) {
				//三种特殊类型，为固定的前三个 2 Additional Staff
				//如果是有一个；即为增加圆框，表示源数据为0
				if ($scope.rosterPageInfo.roleDayVos[i].roleRosterStaffMap[2].length == 1) {
					tipNum = tipNum + 1;
				}
			}
			if(tipNum>0){
				tipMsg = tipMsg + 'Additional Staff have ' + tipNum + ' position not filled;</br> ';
			}*/
		//教师排班
		tipNum = 0;
		for (var j = 0; j < $scope.rosterPageInfo.rosterStaffShiftTimeVos.length; j++) {
			for (var i = 0; i < 5; i++) {
				var rosterStaffs = $scope.rosterPageInfo.rosterStaffShiftTimeVos[j].rosterStaffShiftVos[i].rosterStaffVos;
				if (rosterStaffs.length == 0) {
					tipNum = tipNum + 1;
				}
			}
		}
		if (tipNum > 0) {
			tipMsg = tipMsg + 'Please fill all Educator\'s shifts</br> ';
		}
		return tipMsg;
	}

	/**
	 * @author zhengguo
	 * @description 打开员工签到的搜索框
	 * @returns 通过选择的员工的确认调用签到编辑modal方法
	 * $scope.openEditSign(staffId);
	 * type : 1  staff
	 */
	$scope.staffSignin = function () {
		var modalInstance = $uibModal.open({
			templateUrl: 'views/sign/child-sign-search.html?res_v=' + $scope.res_v,
			controller: 'childSignSearchCtrl',
			backdrop: 'static',
			keyboard: false,
			/*windowClass:'visitor-sign-out-popup modal-align-top',		*/
			resolve: {
				centerId: function () {
					return $scope.page.condition.centersId;
				},
				roomId: function () {
					return '';
				},
				type: function () {
					return 1;
				}
			}
		}).result.then(function (staffId) {
			if (staffId) {
				$scope.staffSignEdit(staffId);
			}
		});
	}

	/**
	 * @author zhengguo
	 * @description 打开确认保存排班为模板的modal 并取名,确认后将name传出来，再调用保存方法
	 * @returns 提示保存成功
	 * type : 0 代表保存为模板，1代表为将模板释放
	 */
	$scope.saveAsTemplate = function (type) {
		if ($scope.rosterPageInfo && $scope.rosterPageInfo.rosterInfo.id) {
			if (type == 0) {
				$uibModal.open({
					templateUrl: 'views/rostering/saveTemplateModal.html?res_v=' + $scope.res_v,
					controller: 'saveTemplateCtrl',
					backdrop: 'static',
					keyboard: false,
					resolve: {
						rosterId: function () {
							return $scope.rosterPageInfo.rosterInfo.id;
						},
						type: function () {
							return type;
						}
					}
				}).result.then(function (isSuccess) {
					if (!isSuccess) {
						return;
					}
					$scope.rosterPageInfo.rosterInfo.templateFlag = 1;
					//刷新数据源解决
					$scope.reloadMenuOrgs();
					/*	if (templateName) {
							// 调用保存模板方法,并提示返回的信息
							// 保存template
							rosteringService.setRosterTemplate($scope.rosterPageInfo.rosterInfo.id, type, templateName).then(
									angular.bind(this, function then() {
										var result = rosteringService.setRosterTemplateResult;
										if (result.code == 0) {
											$scope.rosterPageInfo.rosterInfo.templateFlag = 1;
										}
										$scope.message(result.msg);
									}));
						}*/
				});
			} else {
				rosteringService.setRosterTemplate($scope.rosterPageInfo.rosterInfo.id, type, null).then(angular.bind(this, function then() {
					var result = rosteringService.setRosterTemplateResult;
					if (result.code == 0) {
						$scope.rosterPageInfo.rosterInfo.templateFlag = 0;
					}
					$scope.message(result.msg);
				}));
			}
		} else {
			$scope.message('Please select a roster template.');
		}
	}

	/**
	 * @author zhengguo
	 * @description 打开选择排班的人员选择modal 再调用保存方法
	 * @returns 更新列表中，将新增的人加入进去
	 */
	$scope.addStaffModal = function (type, day, item, timeCode, index, shiftId) {
		var data = {
			shiftTimeCode: timeCode,
			staffType: type,
			day: day,
			centreId: $scope.page.condition.centersId,
			rosterId: $scope.rosterPageInfo.rosterInfo.id,
			shiftId: shiftId
		}
		var modalInstance = $uibModal.open({
			templateUrl: 'views/rostering/staffRosterModal.html?res_v=' + $scope.res_v,
			controller: 'staffRosterCtrl',
			backdrop: 'static',
			keyboard: false,
			size: 'lg',
			/*windowClass:'visitor-sign-out-popup modal-align-top',		*/
			resolve: {
				data: function () {
					return data;
				},
			}
		}).result.then(function (outData) {
			if (outData.flag == 0) {
				//将4种特殊类型员工 单个返回的对象加入对应的位置中
				item.splice(-1, 0, outData.data);
				if (type == 2 && item.length > $scope.asMaxArr.length) {
					$scope.asMaxArr.push($scope.asMaxArr.length);
				}
				if (type == 0 && item.length > $scope.cmMaxArr.length) {
					$scope.cmMaxArr.push($scope.cmMaxArr.length);
				}
				if (type == 1 && item.length > $scope.cookMaxArr.length) {
					$scope.cookMaxArr.push($scope.cookMaxArr.length);
				}
				if (type == 3 && item.length > $scope.ssMaxArr.length) {
					$scope.ssMaxArr.push($scope.ssMaxArr.length);
				}
			} else {
				//正常排班教师，将返回的对象放入对应的位置上
				if (type == 3) {
					item.splice(item.length, 0, outData.data);
				} else {
					item.splice(index, 1, outData.data);
				}
			}
			//检查是否和最下面的额外的签到人员有重复Sign Staff
			//如果有的话，需要将其删掉
			//刷新数据源解决
			$scope.reloadMenuOrgs();
			$scope.getRosterStaff();
		});
	}

	/**
	 * @author zhengguo
	 * @description 打开选择排班模板的modal 并取名,确认后将id传出来
	 * @returns 再调用页面更新方法
	 */
	$scope.selectTemplate = function () {
		if ($scope.page.condition.centersId) {
			var modalInstance = $uibModal.open({
				templateUrl: 'views/rostering/selectTemplateModal.html?res_v=' + $scope.res_v,
				controller: 'selectTemplateCtrl',
				backdrop: 'static',
				keyboard: false,
				/*windowClass:'visitor-sign-out-popup modal-align-top',		*/
				resolve: {
					centerId: function () {
						return $scope.page.condition.centersId;
					},
					weekDate: function () {
						return $scope.page.condition.weekDate;
					},
				}
			}).result.then(function (data) {
				if (data) {
					//更新页面模板
					$scope.manageData(data);
				}
			});
		} else {
			$scope.message('choose a centre first');
		}
	}

	/**
	 * @author zhengguo
	 * @description 打开签到信息的编辑框
	 * absenteeId 需要签到信息的ID，该人签到时间
	 * @returns 保存编辑结果
	 */
	$scope.staffOtherSignEdit = function (id) {
		var scope = $rootScope.$new();
		scope.isCurrentCentreManager = $scope.page.condition.currentCentreManager;
		var modalInstance = $uibModal.open({
			templateUrl: 'views/rostering/staffOtherSignEdit.html?res_v=' + $scope.res_v,
			controller: 'staffOtherEditCtrl',
			backdrop: 'static',
			keyboard: false,
			windowClass: 'visitor-sign-out-popup', //modal-align-top
			scope: scope,
			resolve: {
				rosterStaffId: function () {
					//signId.centerId 进入modal 内区分是edit staff sign
					return id;
				},
				callBackFun: function () {
					return $scope.getAttendanceList;
				},
				centerId: function () {
					return $scope.page.condition.centersId;
				}
			}
		}).result.then(function (flag) {
			if (flag == 1) {
				$scope.reloadMenuOrgs();
				$scope.page.condition.type = 4; //本页保留weekdate查询
				$scope.getAttendanceList();
			}
		});
	};

	/**
	 * @author zhengguo
	 * @description 打开签到信息的编辑框
	 * absenteeId 需要签到信息的ID，该人签到时间
	 * @returns 保存编辑结果
	 */
	$scope.staffSignEdit = function (signId, inDate) {
		//"2016-09-29T19:49:53.000"
		if (!inDate) {
			var a = new Date();
			var nowMonth = a.getMonth() < 9 ? '0' + (a.getMonth() + 1) : (a.getMonth() + 1);
			inDate = a.getFullYear() + "-" + nowMonth + "-" + a.getDate() + "T00:00:00.000";
		}
		var info = {};
		info.centerId = signId;
		info.inDate = inDate;
		//add gfwang 解决兼职签到，没有园区的问题10/31
		info.localtion = $scope.mainPage.centerId;
		var modalInstance = $uibModal.open({
			templateUrl: 'views/sign/visitor-edit-sign.html?res_v=' + $scope.res_v,
			controller: 'visitorEditSignCtrl',
			backdrop: 'static',
			keyboard: false,
			windowClass: 'visitor-sign-out-popup', // modal-align-top
			resolve: {
				signId: function () {
					//signId.centerId 进入modal 内区分是edit staff sign
					return info;
				},
				centerId: function () {
					return $scope.page.condition.centersId;
				}
			}
		}).result.then(function (flag) {
			if (flag == 1) {
				$scope.reloadMenuOrgs();
				$scope.page.condition.type = 4; //本页保留weekdate查询
				$scope.getAttendanceList();
			}
		});
	};


	/**
	 * @author zhengguo
	 * @description 导出excel
	 * @need exportCondition.centreId 空就是所有center；exportCondition.startDate; exportCondition.dateInfo;
	 * @returns 返回uuid
	 */
	$scope.staffExport = function () {
		//弹出export 导出日期的
		var modalInstance = $uibModal.open({
			templateUrl: 'views/sign/export-confirm.html?res_v=' + $scope.res_v,
			controller: 'visitorExportCtrl',
			backdrop: 'static',
			keyboard: false,
			/*windowClass:'visitor-sign-out-popup modal-align-top',*/
		}).result.then(function (dateInfo) {
			if (dateInfo) {
				$scope.exportCondition = {};
				$scope.exportCondition.startDate = dateInfo.startDate;
				$scope.exportCondition.endDate = dateInfo.endDate;
				$scope.exportCondition.centreId = $scope.page.condition.centersId;
			}
			attendanceService.staffExportExcel($scope.exportCondition).then(angular.bind(this, function then() {
				$scope.uuidResult = attendanceService.getStaffUUIDResult;
				//继续导出excel
				$window.location.href = "./sign/staffExcel.do?uuid=" + $scope.uuidResult.data;
			}));
		});
	};

	function getRP(rosterPageInfo) {
		var todayIndex = $scope.todayIndex;

		var rpId = "";
		var defaultRp = "";
		//判断责任人id

		var roleDayVos = rosterPageInfo.roleDayVos[todayIndex];
		if (roleDayVos == undefined) {
			return rpId;
		}
		//centermanager集合
		var cmList = roleDayVos.roleRosterStaffMap[0];
		if (cmList == undefined) {
			return rpId;
		}
		for (var i = 0, m = cmList.length; i < m; i++) {
			if (cmList[i].staffAccountId == 1) {
				continue;
			}
			//签到未签出则就是该园长为责任人
			if (cmList[i].sinState == 1 && cmList[i].soutState == null) {
				rpId = cmList[i].userId;
				return rpId;
			}
			if (defaultRp == "") {
				defaultRp = cmList[i].userId;
			}
		}


		if (rpId == "") {
			//判断shift的第一个与最后一个是否签到为责任人了
			var shiftRoster = rosterPageInfo.rosterStaffShiftTimeVos;
			if (shiftRoster != undefined) {
				var shiftLength = shiftRoster.length;
				if (shiftLength == 0) return rpId;
				var fristShift = shiftRoster[0].rosterStaffShiftVos[todayIndex];
				var lastShift = "";
				if (shiftLength > 1) {
					lastShift = shiftRoster[shiftLength - 1].rosterStaffShiftVos[todayIndex];
				}
				if (fristShift.userId != null) {
					if (fristShift.sinState == 1 && fristShift.soutState == null) {
						rpId = fristShift.userId;
						return rpId;
					} else {
						if (defaultRp == "") {
							defaultRp = fristShift.userId;
						}
					}
				}
				if (lastShift != "" && lastShift.userId != null) {
					if (lastShift.sinState == 1 && lastShift.soutState == null) {
						rpId = lastShift.userId;
						return rpId;
					} else {
						if (defaultRp == "") {
							defaultRp = lastShift.userId;
						}
					}
				}
			}
		}

		//shift也没有责任人，则采用默认责任人
		if (rpId == "") {
			return defaultRp;
		}

		return rpId;
	};

	$scope.openClosurePeriod = function (model, isAdd) {
		$uibModal.open({
			templateUrl: 'views/rostering/closurePeriodModal.html?res_v=' + $scope.res_v,
			controller: 'closurePeriodCtrl',
			backdrop: 'static',
			keyboard: false,
			scope: $scope,
			resolve: {
				model: function () {
					return model;
				},
				isAdd: function () {
					return isAdd;
				},
				centres: function () {
					return $scope.roles.centersInfos;
				},
				callBackFun: function () {
					return $scope.getRosterStaff;
				}
			}
		})
	}

	$scope.openPayroll = function () {
		$uibModal.open({
			templateUrl: 'views/rostering/payrollModal.html?res_v=' + $scope.res_v,
			controller: 'payrollCtrl',
			backdrop: 'static',
			keyboard: false,
			scope: $scope
		})
	}

	$scope.openLeave = function (leaveInfo) {
		$uibModal.open({
			templateUrl: 'views/rostering/addLeave.html?res_v=' + $scope.res_v,
			controller: 'addLeaveCtrl',
			backdrop: 'static',
			keyboard: false,
			scope: $scope,
			resolve: {
				flag: function () {
					return -1;
				},
				leaveInfo: function () {
					return leaveInfo;
				},
				callBackFun: function () {
					return $scope.getAttendanceList;
				}
			}
		})
	}

}
angular.module('kindKidsApp').controller('rosteringStaffListCtrl', ['$scope', '$rootScope', '$filter', '$timeout', '$window', '$uibModal', 'rosteringService', 'staffService', 'attendanceService', rosteringStaffListCtrl]);