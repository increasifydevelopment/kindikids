'use strict';

/**
 * @author zhengguo
 * @description myLeave Controller
 */
function myLeaveCtrl($scope, $filter, $rootScope, $timeout, $window, $uibModal, rosteringService, staffService) {

	$rootScope.searchRoleAndType = false;
	$rootScope.showRosterOrg = false;
	$rootScope.isInLeavePage = true;
	$scope.initMyLeave = function () {
		//页面list参数对象
		$scope.condition = {
			listType: 0,
			maxSize: 7,
			pageIndex: 1,
			selectCenterId: $scope.mainPage.centerId,
			leaveType: null
			/*bigTotalItems:180,*/
			/*bigCurrentPage:1,	*/
		};
		$scope.getPage();
	};
	$scope.changeLeavePage = function (pageIndex) {
		$scope.condition.pageIndex = pageIndex;
		$scope.getPage();
	};

	$scope.getPage = function () {
		//页面list数据
		$scope.showLoading('#myLeaveForm');
		rosteringService.getMyLeaveList($scope.condition).then(angular.bind(this, function then() {
			$scope.hideLoading('#myLeaveForm');
			$scope.leaveList = rosteringService.myListResult;
			$scope.condition.totalItems = $scope.leaveList.data.condition.totalSize;
			$scope.condition.pageSize = $scope.leaveList.data.condition.pageSize;
		}));
	};

	//监测外层包裹页面中的center的变化，会随之变化LIST	
	$scope.$watch('mainPage.leaveType', function (newValue) {
		$scope.condition.leaveType = $scope.mainPage.leaveType;
		$scope.getPage();
	});

	//监测外层包裹页面中的center的变化，会随之变化LIST	
	$scope.$watch('mainPage.centerId', function (newValue) {
		$scope.condition.selectCenterId = $scope.mainPage.centerId;
		$scope.getPage();
	});

	//申请请假
	$scope.applyLeaveModal = function (item) {
		$uibModal.open({
			templateUrl: 'views/rostering/applyLeave.html?res_v=' + $scope.res_v,
			controller: 'applyLeaveCtrl',
			backdrop: 'static',
			keyboard: false,
			scope: $scope,
			resolve: {
				itemData: function () {
					return item;
				}
			}
		})
	};

	$scope.withDrawModal = function (item) {

		$scope.confirmSuccess = function () {
			//返回true 
			rosteringService.operaMyLeave(item.id, 0, '').then(angular.bind(this, function then() {
				$scope.operaInResult = rosteringService.operaMyResult;
				if ($scope.operaInResult.code == 0) {
					item.status = 4;
					$rootScope.message($scope.operaInResult.msg);
				}
			}));
		}

		$rootScope.confirm('', 'Are you sure you want to withdraw your Leave request?', 'Confirm', $scope.confirmSuccess, 'Cancel');
	}
}
angular.module('kindKidsApp').controller('myLeaveCtrl', ['$scope', '$filter', '$rootScope', '$timeout', '$window', '$uibModal', 'rosteringService', 'staffService', myLeaveCtrl]);