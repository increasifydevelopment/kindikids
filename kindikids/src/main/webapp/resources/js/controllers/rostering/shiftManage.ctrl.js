'use strict';

/**
 * @author zhengguo
 * @description shift management Controller
 */
function shiftManageCtrl($scope, $rootScope, $timeout, $window, $filter, $uibModal, rosteringService) {

	$rootScope.searchRoleAndType = false;
	$scope.initShiftManage = function() {
		//左边搜索栏显示：Role Employment Type
		/*$scope.$apply();*/
		//初始化页面的popover（tagging 使用）
		$scope.initPopover();
		//设置一周七天的循环
		$scope.weekList = [0, 1, 2, 3, 4, 5, 6];
		//页面list参数对象
		$scope.page = {
			centerId: $scope.mainPage.centerId,
			//数组
			rosterShiftTimeVos: [
				/*{
  					shiftStartTime :'',
  					shiftEndTime:'',
  					shiftWeekVos:[{
  						weekNum:'',//从1-7
  						tags:'',
  						inTags:[];
  					}],
  					rosterShiftTaskVos:[{
  						id:''
  					}],
  				  }*/
			],
		}
		$scope.getPage();
	}


	//初始化popover 方法
	$scope.initPopover = function() {
		$(".shift-tagging").popover({
			html: true,
			content: function() {
				return $(".role-tagging-content").html();
			},
			title: function() {
				return $(".role-tagging-title").html();
			},
			placement: "auto"
		});
	}

	//页面list数据
	$scope.getPage = function() {
		if ($scope.page.centerId) {
			rosteringService.getShiftPageList($scope.page.centerId).then(angular.bind(this, function then() {
				var result = rosteringService.shiftPageResult;
				if (result.code == 0 || result.code == 1) {
					//判断是否为归档的园，归档的园不可操作 code == 1  归档
					if (result.code == 0) {
						$scope.operateFlag = true;
					} else {
						$scope.operateFlag = false;
					}
					//判断是否返回的data为空
					if (result.data) {
						$scope.page = result.data;
						//处理时间格式和tags
						for (var i = 0; i < $scope.page.rosterShiftTimeVos.length; i++) {
							//计算taskvos 的数量
							$scope.page.rosterShiftTimeVos[i].taskVosNum = $scope.page.rosterShiftTimeVos[i].rosterShiftTaskVos.length;
							//处理时间格式
							$scope.page.rosterShiftTimeVos[i].shiftEndTime = $scope.changeBackTime($scope.page.rosterShiftTimeVos[i].shiftEndTime);
							$scope.page.rosterShiftTimeVos[i].shiftStartTime = $scope.changeBackTime($scope.page.rosterShiftTimeVos[i].shiftStartTime);
							//处理tags
							for (var j = 0; j < $scope.page.rosterShiftTimeVos[i].shiftWeekVos.length; j++) {
								$scope.page.rosterShiftTimeVos[i].shiftWeekVos[j].inTags = angular.copy(targetTags);
								var InTag;
								var nowTags = $scope.page.rosterShiftTimeVos[i].shiftWeekVos[j].tags.trim().split(',');
								for (var p = 0; p < targetTags.length; p++) {
									InTag = $filter('shiftTitle')(targetTags[p].tagText);
									for (var q = 0; q < nowTags.length; q++) {
										if (nowTags[q] == InTag) {
											$scope.page.rosterShiftTimeVos[i].shiftWeekVos[j].inTags[p].checked = true;
										}
									}
								}
							}
						}
						//初始化popover方法
						$timeout(function() {
							$scope.initPopover();
						}, 200)
					} else {
						//请求后台为空，清空前台shift列表
						var cid = angular.copy($scope.page.centerId);
						$scope.page = {};
						$scope.page = {
							centerId: cid,
							rosterShiftTimeVos: []
						};
					}
				} else {
					$scope.message(result.msg);
				}
			}));
		}
	}

	//监测外层包裹页面中的center的变化，会随之变化LIST
	$scope.$watch('mainPage.centerId', function(newValue) {
		if ($scope.mainPage && $scope.mainPage.centerId) {
			if (!$scope.page) {
				$scope.page = {};
			}
			$scope.page.centerId = $scope.mainPage.centerId;
			$scope.getPage();
		}
	});

	var targetTags = [{
		tagText: 'Certified Supervisor',
		checked: false
	}, {
		tagText: 'First Aid Officer',
		checked: false
	}, {
		tagText: 'Nominated Supervisor',
		checked: false
	}, {
		tagText: 'Early Child Teacher',
		checked: false
	}, {
		tagText: 'Diploma',
		checked: false
	}, {
		tagText: 'Educational Leader',
		checked: false
	} ];

	//新增一行排班
	$scope.addShift = function() {
		if ($scope.page.centerId) {
			//创建一个空对象
			var rosterShiftTimeVo = {};
			rosterShiftTimeVo.shiftStartTime = "";
			rosterShiftTimeVo.shiftEndTime = "";
			rosterShiftTimeVo.rosterShiftTaskVos = [];
			rosterShiftTimeVo.taskVosNum = 0;
			rosterShiftTimeVo.shiftWeekVos = [];
			var shiftWeekVo;
			for (var i = 0; i < 7; i++) {
				shiftWeekVo = {};
				shiftWeekVo.weekNum = i;
				shiftWeekVo.tags = '';
				shiftWeekVo.inTags = angular.copy(targetTags);
				rosterShiftTimeVo.shiftWeekVos.push(shiftWeekVo);
			}
			$scope.page.rosterShiftTimeVos.push(rosterShiftTimeVo);
			//初始化popover方法
			$timeout(function() {
				$scope.initPopover();
			}, 200)
		} else {
			$scope.message('choose a center first');
		}
	}
var submitFlag=true;
	$scope.saveAllShift = function() {
		//首先将所有popover弹框隐藏
		var pop = $('.popover');
		pop.css('display', 'none');
		pop.prev().trigger("click");
		//处理传递数据的格式
		if ($scope.page && $scope.page.rosterShiftTimeVos.length > 0) {
			if(!submitFlag){
				return;
			}
			submitFlag=false;
			var saveInfo = angular.copy($scope.page);
			//处理时间格式：2016-09-29T19:49:53.000
			//处理tags
			for (var i = 0; i < saveInfo.rosterShiftTimeVos.length; i++) {
				//处理时间格式：2016-09-29T19:49:53.000
				var nowDate = new Date();
				var nowMonth = nowDate.getMonth() >= 9 ? nowDate.getMonth() + 1 : '0' + (nowDate.getMonth() + 1);
				var nowDay = nowDate.getDate() > 9 ? nowDate.getDate() : '0' + nowDate.getDate();
				var inNowDate = nowDate.getFullYear() + "-" + nowMonth + "-" + nowDay;
				saveInfo.rosterShiftTimeVos[i].shiftEndTime = inNowDate + $scope.changeHeadTime(saveInfo.rosterShiftTimeVos[i].shiftEndTime);
				saveInfo.rosterShiftTimeVos[i].shiftStartTime = inNowDate + $scope.changeHeadTime(saveInfo.rosterShiftTimeVos[i].shiftStartTime);
				//处理tags
				for (var j = 0; j < saveInfo.rosterShiftTimeVos[i].shiftWeekVos.length; j++) {
					/*saveInfo.rosterShiftTimeVos[i].shiftWeekVos[j].tags='';*/
					var InTag;
					saveInfo.rosterShiftTimeVos[i].shiftWeekVos[j].tags = '';
					for (var q = 0; q < saveInfo.rosterShiftTimeVos[i].shiftWeekVos[j].inTags.length; q++) {
						if (saveInfo.rosterShiftTimeVos[i].shiftWeekVos[j].inTags[q].checked) {
							InTag = $filter('shiftTitle')(saveInfo.rosterShiftTimeVos[i].shiftWeekVos[j].inTags[q].tagText);
							if (q == saveInfo.rosterShiftTimeVos[i].shiftWeekVos[j].inTags.length - 1) {
								saveInfo.rosterShiftTimeVos[i].shiftWeekVos[j].tags = saveInfo.rosterShiftTimeVos[i].shiftWeekVos[j].tags + InTag;
							} else {
								saveInfo.rosterShiftTimeVos[i].shiftWeekVos[j].tags = saveInfo.rosterShiftTimeVos[i].shiftWeekVos[j].tags + InTag + ',';
							}
						}
					}
				}
			}
			//保存页面shift数据
			rosteringService.saveRosterShift(saveInfo).then(angular.bind(this, function then() {
				var result = rosteringService.saveShiftResult;
				if (result.code == 0) {
					$scope.page.id = result.data.id;
					//更新id赋值，后续操作编辑和删除需id
					for (var i = 0; i < result.data.rosterShiftTimeVos.length; i++) {
						$scope.page.rosterShiftTimeVos[i].id = result.data.rosterShiftTimeVos[i].id;
						//更新id赋值，后续操作编辑和删除需id
						for (var j = 0; j < result.data.rosterShiftTimeVos[i].shiftWeekVos.length; j++) {
							$scope.page.rosterShiftTimeVos[i].shiftWeekVos[j].id = result.data.rosterShiftTimeVos[i].shiftWeekVos[j].id;
						}
					}

				}
				$scope.message(result.msg);
				submitFlag=true;
			}));
		} else {
			$scope.message("No shifts are defined. Please define a shift and try again.");
		}
	}

	$scope.delShiftTask = function(id, index) {
		$rootScope.confirm("Delete?", "Are you sure?", 'OK', function() {
			if (!id) {
				//前端删除
				$scope.page.rosterShiftTimeVos.splice(index, 1);
				$scope.message('delete success');
				return;
			}
			//后端删除
			rosteringService.delShiftTask(id).then(angular.bind(this, function then() {
				var result = rosteringService.delShiftTaskResult;
				if (result.code == 0) {
					//前端删除
					$scope.page.rosterShiftTimeVos.splice(index, 1);
					$scope.message('delete success');
				} else {
					$scope.message(result.msg);
				}
			}));
		}, 'NO');

	}

	$scope.editTaskModal = function(shiftOne) {
		if ($scope.page.centerId) {
			$uibModal.open({
				templateUrl: 'views/rostering/shiftTask.html?res_v='+$scope.res_v,
				controller: 'shiftTaskCtrl',
				backdrop: 'static',
				keyboard : false,
				scope: $scope,
				resolve: {
					centerId: function() {
						return $scope.page.centerId;
					},
					taskVos: function() {
						return shiftOne.rosterShiftTaskVos;
					},
				}
			}).result.then(function(taskArr) {
				shiftOne.rosterShiftTaskVos = taskArr;
				shiftOne.taskVosNum = taskArr.length;
			});
		} else {
			$scope.message('Please choose a center first');
		}
	}

}
angular.module('kindKidsApp').controller('shiftManageCtrl', ['$scope', '$rootScope', '$timeout', '$window', '$filter', '$uibModal', 'rosteringService', shiftManageCtrl]);