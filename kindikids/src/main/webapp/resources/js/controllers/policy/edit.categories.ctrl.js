'use strict';

/**
 * @author abliu
 * @description policyEditCategoriesCtrl Controller
 */
function policyEditCategoriesCtrl($scope, $rootScope, $timeout, $uibModalInstance, policyService, callbackFun) {

	$scope.policyInfo = {};

	/**
	 * @author abliu
	 * @description
	 * @returns 获取Categories信息
	 */
	$scope.getEditInfo = function() {
		policyService.getCategoriesSrcList().then(angular.bind(this, function then() {
			var result = policyService.categoriesSrcResult;
			if (result.code == 0) {
				$scope.policyCategoriesSrcList = result.data;
			}
		}));
	};


	/**
	 * @author abliu
	 * @description
	 * @returns 删除分类
	 */
	$scope.existDeleteCategory = function(name,id) {
		//判断该分类是否可以删除
		if(id==""){
			deleteCategory(name);		
		}else{
			//保存policy			
			policyService.existDeletePolicy(id).then(angular.bind(this, function then() {
				var result = policyService.existDeletePolicyResult;
				if (result.code != 0) {
					$rootScope.message(result.msg);
					return;
				}else{
					deleteCategory(name);
				}
			}));
		}		
	};

	function deleteCategory(name) {
		for (var i = 0, m = $scope.policyCategoriesSrcList.length; i < m; i++) {
			if ($scope.policyCategoriesSrcList[i].name == name && $scope.policyCategoriesSrcList[i].deleteFlag==0) {
				$scope.policyCategoriesSrcList[i].deleteFlag = 1;
				break;
			}
		}
	};

	$scope.addCategoryInfo = {
		"id": "",
		"name": "",
		"deleteFlag": 0
	};

	/**
	 * @author abliu
	 * @description
	 * @returns 新增分类
	 */
	$scope.addCategory = function() {
		//判断类别是否为空
		if ($scope.addCategoryInfo.name == "") {
			return;
		}
		for (var i = 0, m = $scope.policyCategoriesSrcList.length; i < m; i++) {
			if ($scope.policyCategoriesSrcList[i].name == $scope.addCategoryInfo.name && $scope.policyCategoriesSrcList[i].deleteFlag == 0) {
				$rootScope.message("This category already exists");
				return;
			}
		}

		$scope.policyCategoriesSrcList.push($scope.addCategoryInfo);
		//重置addFileInfo
		$scope.addCategoryInfo = {
			"id": "",
			"name": "",
			"deleteFlag": 0
		};
	};

	/**
	 * @author abliu
	 * @description
	 * @returns 新增附件信息
	 */
	$scope.saveCategory = function() {

		var haveCategory = false;

		//判斷分类内容
		for (var i = 0, m = $scope.policyCategoriesSrcList.length; i < m; i++) {

			var policyCategory = $scope.policyCategoriesSrcList[i];
			if (policyCategory.deleteFlag == 0) {
				//分類名称不能为空
				if (policyCategory.name == null || policyCategory.name == undefined || policyCategory.name.trim() == "") {
					$rootScope.message("Category Name is required");
					return false;
				}
				//判断分類名称大小
				if (policyCategory.name.length < 0 || policyCategory.name.length > 255) {
					$rootScope.message("The Category must be less than 255 characters long.");
					return false;
				}
				haveCategory = true;
			}
		}

		//判断附件至少需要一个
		if (!haveCategory) {
			$rootScope.message("Category Name is required");
			return false;
		}

		$scope.showLoading("#editInfo");

		//保存policy			
		policyService.saveCategory($scope.policyCategoriesSrcList).then(angular.bind(this, function then() {
			var result = policyService.saveCategoryResult;
			if (result.code == 0) {
				$uibModalInstance.dismiss('cancel');
			} else {
				$rootScope.message(result.msg);
			}
			callbackFun();
			$scope.hideLoading("#editInfo");
		}));
	};


	/**
	 * @author abliu
	 * @description 回车添加分类
	 * @returns 
	 */
	$scope.enterAddCategory = function(ev) {
		if (ev.keyCode == 13) {
			$scope.addCategory();
		}
	};

	// 关闭模态框
	$scope.close = function() {
		$scope.confirm('Close', 'Exit this page?', 'OK', function() {
			$uibModalInstance.dismiss('cancel');
		}, 'NO');
	};
}
angular.module('kindKidsApp').controller('policyEditCategoriesCtrl', ['$scope', '$rootScope', '$timeout', '$uibModalInstance', 'policyService', 'callbackFun', policyEditCategoriesCtrl]);