'use strict';

/**
 * @author abliu
 * @description policyEdit Controller
 */
function policyEditCtrl($scope, $rootScope, $window, $timeout, $uibModalInstance, policyService, fileService, policyId, callbackFun) {

	$scope.policyInfo = {};
	/**
	 * @author abliu
	 * @description
	 * @returns 获取policy信息
	 */
	$scope.getEditInfo = function() {
		policyService.getPolicyInfo(policyId).then(angular.bind(this, function then() {
			var result = policyService.policyInfo;
			if (result.code == 0) {
				$scope.policyInfo = result.data;
				$scope.policyVisibilityView = [];
				//处理角色集合
				for (var i = 0, m = $scope.policyInfo.policyVisibility.length; i < m; i++) {
					$scope.policyVisibilityView.push($scope.policyInfo.policyVisibility[i].roleId);
				}
			} else {
				$rootScope.message(result.msg);
			}
		}));
	};


	/**
	 * @author abliu
	 * @description
	 * @returns 删除附件信息
	 */
	$scope.deleteFile = function(attachId) {
		//删除附件
		for (var i = 0, m = $scope.policyInfo.policyDocList.length; i < m; i++) {
			if ($scope.policyInfo.policyDocList[i].attachId == attachId) {
				$scope.policyInfo.policyDocList[i].deleteFlag = 1;
				break;
			}
		}
	};

	$scope.addFileInfo = {
		"id": "",
		"documentsId": "",
		"version": "",
		"fileName": "",
		"attachId": "",
		"visitUrl": "",
		"deleteFlag": 0
	};

	/**
	 * @author abliu
	 * @description
	 * @returns 新增附件信息
	 */
	$scope.addFile = function() {
		//判断附件是否为空
		if ($scope.addFileInfo.attachId == "") {
			return;
		}
		//判断附件是否为空
		if ($scope.addFileInfo.version == "") {
			$rootScope.message("The Version is required.");
			return;
		}

		//判断version大小
		if ($scope.addFileInfo.version.length < 0 || $scope.addFileInfo.version.length > 50) {
			$rootScope.message("The Version must be less than 50 characters long.");
			return false;
		}

		//判断version是有重复
		for (var i = 0, m = $scope.policyInfo.policyDocList.length; i < m; i++) {
			if ($scope.policyInfo.policyDocList[i].version.trim() == $scope.addFileInfo.version.trim() && $scope.policyInfo.policyDocList[i].deleteFlag != 1) {
				$rootScope.message("This version already exists. Please choose a newer version before proceeding.");
				return;
			}
		}

		$scope.policyInfo.policyDocList.splice(0, 0, $scope.addFileInfo);
		//重置addFileInfo
		$scope.addFileInfo = {
			"id": "",
			"documentsId": "",
			"version": "",
			"fileName": "",
			"attachId": "",
			"visitUrl": "",
			"deleteFlag": 0
		};
	};

	/**
	 * @author abliu
	 * @description
	 * @returns 新增附件信息
	 */
	$scope.savePolicy = function() {

		if (!validatePolicy()) {
			return false;
		}

		$scope.showLoading("#editInfo");
		//设置上传文件PDF word ToDo
		//表单输入验证 ToDo
		$scope.policyInfo.policyVisibility = [];
		//处理角色集合
		for (var i = 0, m = $scope.policyVisibilityView.length; i < m; i++) {
			var policyVisibility = {
				"roleId": ""
			};
			policyVisibility.roleId = $scope.policyVisibilityView[i];
			$scope.policyInfo.policyVisibility.push(policyVisibility);
		}
		//保存policy			
		policyService.savePolicyInfo($scope.policyInfo).then(angular.bind(this, function then() {
			var result = policyService.saveResult;
			if (result.code == 0) {
				$uibModalInstance.dismiss('cancel');
			} else {
				$rootScope.message(result.msg);
			}
			callbackFun();
			$scope.hideLoading("#editInfo");
		}));
	};

	/**
	 * @author abliu
	 * @description 文件上传
	 */
	$scope.upload = function($files) {
		if ($files == null) {
			return;
		}
		var file = $files[0];
		if(file==undefined){
			return;
		}
		var AllImgExt = ".doc|.docx|.pdf|";
		var extName = file.name.substring(file.name.lastIndexOf(".")).toLowerCase(); //（把路径中的所有字母全部转换为小写）          
		if (AllImgExt.indexOf(extName + "|") == -1) {
			$rootScope.message("Only allow upload PDF or WORD file");
			return false;
		}
		if (file.size < 10485760) {
			$scope.showLoading("#uploadFile");
			fileService.uploadFile(file).then(angular.bind(this, function then() {
				var result = fileService.fdata;
				if (result.code == 0) {
					$scope.addFileInfo.attachId = result.data.relativePathName;
					$scope.addFileInfo.fileName = result.data.fileName;
				} else {
					$rootScope.message(result.msg);
				}
				$scope.hideLoading("#uploadFile");
			}));
		} else {
			$rootScope.message('Sorry, the filesize is too large, please reduce the filesize and try again.');
			$scope.hideLoading("#uploadFile");
		}
	};

	/**
	 * 下载文件
	 */
	$scope.download = function(file) {
		$window.location.href = "./common/download.do?fileRelativeIdName=" + file.attachId + "&fileName=" + encodeURIComponent(file.fileName);
	};

	/**
	 * @author abliu
	 * @description 获取选择的NQS
	 */
	$scope.saveNqsResult = function(newNqs) {
		$scope.policyInfo.nqsInfo = newNqs;
	};


	// 关闭模态框
	$scope.close = function() {
		$scope.confirm('Close', 'Exit this page?', 'OK', function() {
			$uibModalInstance.dismiss('cancel');
		}, 'NO');
	};

	/**
	 * @author abliu
	 * @description 验证保存合法性,返回true 和false
	 */
	function validatePolicy(){
		//policyName不能为空
		if ($scope.policyInfo.policyName == null || $scope.policyInfo.policyName == undefined || $scope.policyInfo.policyName.trim() == "") {
			$rootScope.message("The Policy Name is required.");
			return false;
		}
		//判断policyName大小
		if ($scope.policyInfo.policyName.length < 0 || $scope.policyInfo.policyName.length > 255) {
			$rootScope.message("The Policy Name must be less than 255 characters long.");
			return false;
		}
		//Category不能为空
		if ($scope.policyInfo.categoryId == null || $scope.policyInfo.categoryId == undefined || $scope.policyInfo.categoryId.trim() == "") {
			$rootScope.message("Category Name is required");
			return false;
		}
		//判断附件至少需要一个
		if ($scope.policyInfo.policyDocList == null || $scope.policyInfo.policyDocList == undefined || $scope.policyInfo.policyDocList.length == 0) {
			$rootScope.message("Policy Documents are required.");
			return false;
		}

		//nqs必填
		if ($scope.policyInfo.nqsInfo == null || $scope.policyInfo.nqsInfo == undefined || $scope.policyInfo.nqsInfo.length == 0) {
			$rootScope.message("The NQS is required.");
			return false;
		}		
		//权限不能为空
		if ($scope.policyVisibilityView == null || $scope.policyVisibilityView == undefined || $scope.policyVisibilityView.length == 0) {
			$rootScope.message("The Visibility is required.");
			return false;
		}

		return true;
	}
}
angular.module('kindKidsApp').controller('policyEditCtrl', ['$scope', '$rootScope', '$window', '$timeout', '$uibModalInstance', 'policyService', 'fileService', 'policyId', 'callbackFun', policyEditCtrl]);