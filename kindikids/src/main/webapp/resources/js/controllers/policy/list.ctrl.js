'use strict';

/**
 * @author abliu
 * @description policy Controller
 */
function policyCtrl($scope, $rootScope, $window, $timeout, $uibModal,
	commonService, policyService) {

	// variable=================================================================
	$scope.page = {};
	$scope.page.condition = {
		pageIndex: 1,
		keyword: '',
		categoryId: null,
		maxSize: 7,
		nqsVersion: ""
	};

	$scope.policyCategoriesSrcList = [];

	// select===================================================================

	$scope.functions = {};
	$scope.getPolicyFun = function () {
		commonService.getFun("/policy_list").then(
			angular.bind(this, function then() {
				$scope.functions = commonService.functions;
			}));
	};

	$scope.getCategoriesSrcList = function () {
		policyService.getCategoriesSrcList().then(
			angular.bind(this, function then() {
				var result = policyService.categoriesSrcResult;
				if (result.code == 0) {
					$scope.policyCategoriesSrcList = result.data;
				}
			}));
	}

	/**
	 * @author abliu
	 * @description
	 * @returns 获取 PolicyList
	 */
	$scope.getPolicyList = function () {
		policyService.getPolicyList($scope.page.condition).then(
			angular.bind(this, function then() {
				var result = policyService.listResult;
				if (result.code == 0) {
					$scope.page = result.data;
					$scope.page.condition.maxSize = 7;
					if ($scope.page.condition.search && $scope.page.list.length == 0) {
						$scope.confirm('Message', 'No results match your search.', 'OK', function () {
							$("#privacy-modal").modal('hide');
						});
					}
					$scope.page.condition.search = false;
				} else {
					$rootScope.message(result.msg);
				}
			}));
	}

	$scope.getPolicyListPageIndex = function (pageIndex) {
		$scope.page.condition.pageIndex = pageIndex;
		$scope.getPolicyList();
	};
	/**
	 * 下载文件
	 */
	$scope.download = function (file) {
		$window.location.href = "./common/download.do?fileRelativeIdName="
			+ file.attachId + "&fileName="
			+ encodeURIComponent(file.fileName);
	};

	$scope.enterSearch = function (ev) {
		if (ev.keyCode == 13) {
			$scope.page.condition.pageIndex = 1;
			$scope.search();
		}
	};
	$scope.search = function () {
		$scope.page.condition.pageIndex = 1;
		$scope.page.condition.search = true;
		$scope.getPolicyList();
	};
	$scope.setNQS = function (value) {
		$scope.page.condition.nqsVersion = value;
		$scope.search();
	};
	$scope.newsKeyup = function (ev) {
		if (ev.keyCode == 13) {
			$scope.search();
		}
	};
	$scope.getPolicyListByCategory = function () {
		$scope.page.condition.pageIndex = 1;
		$scope.getPolicyList();
	};

	/**
	 * @author abliu
	 * @description
	 * @returns 获取打开编辑窗口
	 */
	$scope.openEdit = function (id) {
		$uibModal.open({
			templateUrl: 'views/policy/edit.html?res_v=' + $scope.res_v,
			controller: "policyEditCtrl",
			backdrop: 'static',
			keyboard: false,
			resolve: {
				policyId: function () {
					return id;
				},
				callbackFun: function () {
					return $scope.getPolicyList;
				}
			}
		});
	};

	/**
	 * @author abliu
	 * @description
	 * @returns 获取打开编辑窗口
	 */
	$scope.openEditCategories = function () {
		$uibModal.open({
			templateUrl: 'views/policy/edit.categories.html?res_v='
				+ $scope.res_v,
			controller: "policyEditCategoriesCtrl",
			backdrop: 'static',
			keyboard: false,
			resolve: {
				callbackFun: function () {
					return $scope.editCategoriesCallback;
				}
			}
		});
	};

	$scope.editCategoriesCallback = function () {
		$scope.page.condition.categoryId = null;
		$scope.getCategoriesSrcList();
	};

	$scope.deletePolicy = function (id) {
		// 确认是否删除
		$scope.confirm('Delete',
			'all versions of this policy will be deleted?', 'YES',
			function () {
				// 保存policy
				policyService.deletePolicyInfo(id).then(
					angular.bind(this, function then() {
						var result = policyService.deleteResult;

						$rootScope.message(result.msg);
						$scope.getPolicyList();
					}));
			}, 'NO');
	};

}
angular.module('kindKidsApp').controller(
	'policyCtrl',
	['$scope', '$rootScope', '$window', '$timeout', '$uibModal',
		'commonService', 'policyService', policyCtrl]);