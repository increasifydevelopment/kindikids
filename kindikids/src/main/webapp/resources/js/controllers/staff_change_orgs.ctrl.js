'use strict';

/**
 * @author gfwang
 * @description staffChangeOrgController
 */
function staffChangeOrgController($scope, $timeout, $window, $uibModalInstance, staffService) {
	$scope.getOrgs = function() {
		staffService.getOrgs().then(angular.bind(this, function then() {
			var result = staffService.orgsResult;
			$scope.centersList = result.code.centersInfos;
			$scope.roomList = result.code.roomInfos;
			$scope.groupList = result.code.groups;
		}));
	};
	$scope.close = function() {
		$uibModalInstance.close(false);
		$uibModalInstance.dismiss('cancel');
	};
	$scope.ok = function() {
		$uibModalInstance.close(true);
		$uibModalInstance.dismiss('cancel');
	};
}
angular.module('kindKidsApp').controller('staffChangeOrgController',
		[ '$scope', '$timeout', '$window', '$uibModalInstance', 'staffService', staffChangeOrgController ]);