'use strict';

function cancleCtrl($scope, $timeout) {
	$timeout(function() {
		location.href = "login.html";
	}, 3000);
};
angular.module('kindKidsApp').controller('cancleCtrl', [ '$scope', '$timeout', cancleCtrl ]);