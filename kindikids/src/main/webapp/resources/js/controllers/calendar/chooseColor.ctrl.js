'use strict';

/**
 * @author abliu
 * @description chooseColorCtrl Controller
 */
function chooseColorCtrl($scope, $rootScope, $window, $timeout, $uibModalInstance, obj,orgcolor, callbackFun,isLeave) {

	$scope.isLeave=isLeave;
	
	if(orgcolor==null || orgcolor==""){
		orgcolor=1;
	}
	$scope.objColor =orgcolor;

	/**
	 * @author abliu
	 * @description
	 * @returns 新增附件信息
	 */
	$scope.saveChoose= function() {
		callbackFun(obj,$scope.objColor);
		$uibModalInstance.dismiss('cancel');
	};



	// 关闭模态框
	$scope.close = function() {
		$scope.confirm('Close', 'Exit this page?', 'OK', function() {
			$uibModalInstance.dismiss('cancel');
		}, 'NO');
	};

	
}
angular.module('kindKidsApp').controller('chooseColorCtrl', ['$scope', '$rootScope', '$window', '$timeout', '$uibModalInstance','obj','orgcolor', 'callbackFun','isLeave', chooseColorCtrl]);