'use strict';

/**
 * @author abliu
 * @description calendar Controller
 */
function calendarCtrl($scope, $rootScope, $timeout, $window, $filter, $uibModal, commonService, calendarService) {
	// variable=================================================================  
	$scope.page = {};
	$scope.page.condition = {
		keyword: null,
		time: null,
		roomId: null,
		centersId: null,
		type: 2, //type 0:传日期进行搜索，1：上一周 2：本周 3：下一周 4: 左侧菜单，保留weekdate查询
		weekDate: null
	};
	$scope.includeUrl = 'head.html?res_v=' + $scope.res_v;
	//判断本周最大列集合数
	$scope.yearList = [];
	$scope.isCenterManager = $filter('haveRole')($scope.currentUser.roleInfoVoList, '1;5');
	$scope.isCeoOrCasual = $filter('haveRole')($scope.currentUser.roleInfoVoList, '0;4');
	$scope.leaveList = new Array();
	/**
	 * @author abliu
	 * @description
	 * @returns 获取Calendar列表
	 */
	$scope.search = function () {
		$scope.page.condition.search = true;
		$scope.getCalendarList();
	}
	$scope.getCalendarList = function () {
		//园长默认选择当前园add by gfwang
		if (!$scope.isCeoOrCasual) {
			$scope.page.condition.centersId = $scope.currentUser.userInfo.centersId;
		}
		calendarService.getCalendarList($scope.page.condition).then(angular.bind(this, function then() {
			$("#searchDiv").removeClass("open");
			var result = calendarService.listResult;
			if (result.code == 0) {
				var length = result.data.leave.length;
				transformLeaveList(result.data.leave, result.data.condition.weekDate);
				$scope.page = result.data;
				if (result.data.condition.search && length == 0 && $scope.isEmpty()) {
					$scope.confirm('Message', 'No results match your search.', 'OK', function () {
						$("#privacy-modal").modal('hide');
					});
				}
				$scope.page.condition.search = false;
			} else {
				$rootScope.message(result.msg);
			}
		}));
	};

	$scope.isEmpty = function () {
		var list = $scope.page.weekDays;
		for (var i in list) {
			if (list[i].eventList.length != 0) {
				return false;
			}
		}
		return true;
	}

	/**
	 * @author abliu
	 * @description
	 * @returns 获取园区及room集合
	 */
	$scope.getMenuOrgs = function () {
		commonService.getMenuOrgs(5).then(angular.bind(this, function then() {
			$scope.orgList = commonService.orgList;
		}));
	};

	/**
	 * @author abliu
	 * @description
	 * @returns 设置年份集合，从2010年开始到当前年加1年
	 */
	$scope.getYearList = function () {
		var yyyy = new Date().getFullYear();
		var m = yyyy - 2015;
		for (var i = 0; i <= m + 1; i++) {
			$scope.yearList.push(2015 + i);
		}
	};

	/**
	 * @author abliu
	 * @description
	 * @returns 设置年份
	 */
	$scope.setTimeY = function (yyyy) {
		if ($scope.page.condition.time != null) {
			var times = $scope.page.condition.time.split("/");
			if (times.length > 1) {
				$scope.page.condition.time = yyyy + "/" + times[1];
				$scope.page.condition.type = 0;
				$scope.getCalendarList();
			}
		}
	};

	/**
	 * @author abliu
	 * @description
	 * @returns 设置月份
	 */
	$scope.setTimeM = function (mm) {
		if ($scope.page.condition.time != null) {
			var times = $scope.page.condition.time.split("/");
			if (times.length > 1) {
				$scope.page.condition.time = times[0] + "/" + mm;
				$scope.page.condition.type = 0;
				$scope.getCalendarList();
			}
		}
	};


	/**
	 * @author abliu
	 * @description
	 * @returns 设置type
	 */
	$scope.setType = function (type) {
		$scope.page.condition.type = type;
		$scope.page.condition.search = true;
		$scope.getCalendarList();
	};

	$scope.goEventEidt = function (id) {
		$rootScope.$state.go("app.main.event.edit", {
			"eventId": id
		});
	};

	/**
	 * @author abliu
	 * @description
	 * @returns 获取打开编辑窗口
	 */
	$scope.openChooseLeaveColor = function (obj, isLeave) {
		// 家长登陆后只能查看不能编辑
		if ($scope.currentUser.userInfo.userType == 1) {
			return;
		}
		$uibModal.open({
			templateUrl: 'views/calendar/chooseColor.html?res_v=' + $scope.res_v,
			controller: "chooseColorCtrl",
			backdrop: 'static',
			keyboard: false,
			resolve: {
				obj: function () {
					return obj;
				},
				orgcolor: function () {
					return obj.leaveColor;
				},
				callbackFun: function () {
					return $scope.saveColorCallback;
				},
				isLeave: function () {
					return isLeave;
				}
			}
		});
	};

	$scope.saveColorCallback = function (obj, color) {
		//更新请假颜色
		calendarService.updateLeaveColor(obj.id, color).then(angular.bind(this, function then() {
			var result = calendarService.saveColorResult;
			if (result.code == 0) {
				obj.leaveColor = color;
			} else {
				$rootScope.message(result.msg);
			}
		}));
	};

	$scope.openChooseEventColor = function (obj, isLeave) {
		// 家长登陆后只能查看不能编辑
		if ($scope.currentUser.userInfo.userType == 1) {
			return;
		}
		$uibModal.open({
			templateUrl: 'views/calendar/chooseColor.html?res_v=' + $scope.res_v,
			controller: "chooseColorCtrl",
			backdrop: 'static',
			keyboard: false,
			resolve: {
				obj: function () {
					return obj;
				},
				orgcolor: function () {
					return obj.eventColor;
				},
				callbackFun: function () {
					return $scope.saveEventColorCallback;
				},
				isLeave: function () {
					return isLeave;
				}
			}
		});
	};

	//
	$scope.saveEventColorCallback = function (obj, color) {

		//更新event颜色
		calendarService.updateEventColor(obj.id, color).then(angular.bind(this, function then() {
			var result = calendarService.saveEventColorResult;
			if (result.code == 0) {
				for (var i = 0, m = $scope.page.weekDays.length; i < m; i++) {
					for (var j = 0, n = $scope.page.weekDays[i].eventList.length; j < n; j++) {
						if ($scope.page.weekDays[i].eventList[j].id == obj.id) {
							$scope.page.weekDays[i].eventList[j].eventColor = color;
						}
					}
				}
			} else {
				$rootScope.message(result.msg);
			}
		}));
	};

	function transformLeaveList(list, weekDate) {
		weekDate = new moment(weekDate, "YYYY-MM-DDTHH:mm:ss.SSS");
		var date = '';
		var tArray = new Array();
		var row = 0;
		var col = 0;
		tArray = [];
		while (list.length > 0 && row < 1000) {
			tArray[row] = new Array();
			//列归0
			col = 0;
			//循环7天
			for (var i = 0; i < 7;) {
				date = new moment(weekDate, "YYYY-MM-DDTHH:mm:ss.SSS");
				//每周的开始时间+1天				
				date.add(i, 'd');
				var ishave = false;
				//循环请假记录看是否有当天的记录
				for (var j = 0; j < list.length; j++) {
					var startDate = new moment(list[j].startDate, "YYYY-MM-DDTHH:mm:ss.SSS");
					var endDate = new moment(list[j].endDate, "YYYY-MM-DDTHH:mm:ss.SSS");
					var startday = startDate.diff(date, 'day');
					var endday = endDate.diff(date, 'day');
					if (startday < 0 && date.diff(weekDate, 'day') == 0) {
						startday = 0
					}
					if (startday == 0) {
						var range = endday + 1;
						if (endday > 7 - i) {
							range = 7 - i;
						}
						list[j].range = range;
						if (list[j].leaveColor == undefined || list[j].leaveColor == null) {
							list[j].leaveColor = range;
						}
						//满足记录
						tArray[row][col] = list[j];
						//删除请假集合
						list.splice(j, 1);
						//查看该周是否还有剩余天,让i从第几天开始循环
						i = i + range;
						ishave = true;
						break;
					}
				}
				//如果没有找到则i++;
				if (!ishave) {
					//满足记录
					var leave = {
						"range": 1,
						"leaveColor": "1",
						"userName": ""
					};
					tArray[row][col] = leave;
					i++;
				}
				col++;
			}
			row++;
		}
		$scope.leaveList = tArray;
	}


}
angular.module('kindKidsApp').controller('calendarCtrl', ['$scope', '$rootScope', '$timeout', '$window', '$filter', '$uibModal', 'commonService', 'calendarService', calendarCtrl]);