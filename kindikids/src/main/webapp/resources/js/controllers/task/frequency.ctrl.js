'use strict';

/**
 * @author gfwang
 * @description frequencyCtrl
 */
function frequencyCtrl($scope, $timeout, $location, $filter, $uibModalInstance, Obj, taskSettingService, $stateParams) {
	$scope.taskInfo = {
		taskFrequencyInfo : Obj.taskFrequencyInfo,
		statu : Obj.statu
	};

	$scope.cancel = function() {
		$scope.confirm('Close', 'Exit this page?', 'OK', function() {
			$uibModalInstance.dismiss('cancel');
		}, 'NO');
	};

	$scope.initFrequency = function() {
		$scope.taskInfo.taskFrequencyInfo.repeatType = !$scope.taskInfo.taskFrequencyInfo.repeatType ? '0'
				: $scope.taskInfo.taskFrequencyInfo.repeatType + '';
		$scope.taskInfo.taskFrequencyInfo.haveEndDate = !$scope.taskInfo.taskFrequencyInfo.haveEndDate ? 0
				: $scope.taskInfo.taskFrequencyInfo.haveEndDate;
		var type = $scope.taskInfo.taskFrequencyInfo.repeatType * 1;
		if (type == 5) {
			$scope.taskInfo.taskFrequencyInfo.dayToStartDatePick = $scope.taskInfo.taskFrequencyInfo.dayToStart;
			$scope.taskInfo.taskFrequencyInfo.dayToEndDatePick = $scope.taskInfo.taskFrequencyInfo.dayToEnd;
		}
	};

	$scope.repeatChange = function() {
		$scope.taskInfo.taskFrequencyInfo.startsDate = null;
		$scope.taskInfo.taskFrequencyInfo.haveEndDate = 0;
		$scope.taskInfo.taskFrequencyInfo.endsDate = null;
		$scope.taskInfo.taskFrequencyInfo.dayToStart = null;
		$scope.taskInfo.taskFrequencyInfo.dayToEnd = null;
	};
	$scope.save = function() {
		initData();
		var validateResult = myvailidate();
		if (validateResult) {
			$scope.message(validateResult, 1000);
			return;
		}
		Obj.callBackSynchrFrequency($scope.taskInfo.taskFrequencyInfo);
		$uibModalInstance.dismiss('cancel');
	};
	function initData() {
		var type = $scope.taskInfo.taskFrequencyInfo.repeatType * 1;
		if (type == 5) {
			setData($scope.taskInfo.taskFrequencyInfo.dayToStartDatePick, $scope.taskInfo.taskFrequencyInfo.dayToEndDatePick);
		}
	}
	function setData(startData, endData) {
		$scope.taskInfo.taskFrequencyInfo.dayToStart = startData;
		$scope.taskInfo.taskFrequencyInfo.dayToEnd = endData;
	}

	function myvailidate() {
		var type = $scope.taskInfo.taskFrequencyInfo.repeatType;
		if ($filter('haveRepeats')(type, '6')) {
			return '';
		}
		if ($filter('haveRepeats')(type, '0,1,2,3,4,5')) {
			// 开始时间必填
			if (!$scope.taskInfo.taskFrequencyInfo.startsDate) {
				return 'Please enter a date for the "Starts on" field';
			}
			// 开始时间必须大于等于今天
			if (dateComp($scope.taskInfo.taskFrequencyInfo.startsDate) == -1) {
				return 'The \'Starts on\' date cannot be in the past';
			}
			// 结束时间必填
			if ($scope.taskInfo.taskFrequencyInfo.haveEndDate * 1 == 1 && !$scope.taskInfo.taskFrequencyInfo.endsDate) {
				return 'Please enter a date for the "Ends on" field';
			}
			// 结束时间必须大于等于开始时间
			if ($scope.taskInfo.taskFrequencyInfo.haveEndDate * 1 == 1
					&& dateComp($scope.taskInfo.taskFrequencyInfo.endsDate, $scope.taskInfo.taskFrequencyInfo.startsDate) == -1) {
				return 'The \'Ends on\' date must be later than the \'Starts on\' date';
			}
		}
		if ($filter('haveRepeats')(type, '1,2,4,5')) {
			// 开始时间必填
			if (!$scope.taskInfo.taskFrequencyInfo.dayToStart) {
				return 'Start date is required';
			}
			// 结束时间必填
			if (!$scope.taskInfo.taskFrequencyInfo.dayToEnd) {
				return 'Due Date is required';
			}
			if ($filter('haveRepeats')(type, '1,2')) {
				// 结束时间必须大于等于开始时间
				if ($scope.taskInfo.taskFrequencyInfo.dayToStart * 1 > $scope.taskInfo.taskFrequencyInfo.dayToEnd * 1) {
					return 'The Due Date must be later than the Start Date';
				}
			} else if ($filter('haveRepeats')(type, '4')) {
				// 结束时间必须大于等于开始时间
				if (dateComp($scope.taskInfo.taskFrequencyInfo.dayToEnd, $scope.taskInfo.taskFrequencyInfo.dayToStart, 'DD/MM') == -1) {
					return 'The Due Date must be later than the Start Date';
				}
			} else {
				// 结束时间必须大于等于开始时间
				if (dateComp($scope.taskInfo.taskFrequencyInfo.dayToEnd, $scope.taskInfo.taskFrequencyInfo.dayToStart) == -1) {
					return 'The Due Date must be later than the Start Date';
				}
			}
		}
	}
	function dateComp(bigDate, smallDate, formatter) {
		if (!formatter) {
			formatter = 'DD/MM/YYYY';
		}
		var choose = new moment(bigDate, formatter)._d;
		var today = null;
		if (smallDate == null) {
			// smallDate = new moment().format(formatter);
			today = new moment(Obj.currtenDate, formatter)._d;
		} else {
			today = new moment(smallDate, formatter)._d;
		}
		if (choose >= today) {
			return 1;
		} else if (choose == today) {
			return 0;
		} else {
			return -1;
		}
	}

}
angular.module('kindKidsApp').controller('frequencyCtrl',
		[ '$scope', '$timeout', '$location', '$filter', '$uibModalInstance', 'Obj', 'taskSettingService', '$stateParams', frequencyCtrl ]);