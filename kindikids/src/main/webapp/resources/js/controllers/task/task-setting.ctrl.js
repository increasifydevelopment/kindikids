'use strict';

/**
 * @author gfwang
 * @description taskSettingCtrl
 */
function taskSettingCtrl($scope, $rootScope, $timeout, $location, $uibModal, taskSettingService, $stateParams) {
	console.log('taskSettingCtrl|satrt' + new Date().getTime());
	/** **************************************************列表start********************************************** */
	/**
	 * 初始化task列表
	 */
	$scope.initTaskList = function () {
		// 页面list参数对象
		$scope.condition = {
			listType: 1,
			maxSize: 7,
			pageIndex: 1,
			// pageSize : 12,
			keyWord: ''
		};
		$scope.getTaskList();
	};
	$scope.enterSearch = function (ev) {
		if (ev.keyCode == 13) {
			$scope.condition.search = true;
			$scope.condition.pageIndex = 1;
			$scope.getTaskList();
		}
	};
	$scope.search = function (ev) {
		$scope.condition.pageIndex = 1;
		$scope.getTaskList();
	};
	$scope.getTaskList = function () {
		console.log('getTaskList|satrt' + new Date().getTime());
		$scope.showLoading("#taskList");
		// 页面list数据
		taskSettingService.getTaskList($scope.condition).then(angular.bind(this, function then() {
			var result = taskSettingService.listResult;
			$scope.taskList = result.data.list;
			$scope.condition.totalItems = result.data.condition.totalSize;
			$scope.condition.pageSize = result.data.condition.pageSize;
			$scope.hideLoading("#taskList");
			console.log('getTaskList|end' + new Date().getTime());
			if ($scope.condition.search && $scope.taskList.length == 0) {
				$scope.confirm('Message', 'No results match your search.', 'OK', function () {
					$("#privacy-modal").modal('hide');
				});
			}
			$scope.condition.search = false;
		}));
	};
	$scope.changeTaskList = function (pageIndex) {
		$scope.condition.pageIndex = pageIndex;
		$scope.getTaskList();
	};
	/** **************************************************列表end********************************************** */

	function validate() {
		if (!$scope.taskInfo.taskInfo || !$scope.taskInfo.taskInfo.shiftFlag) {
			$('#taskForm').bootstrapValidator('updateStatus', 'responsible', 'VALID');
		}
	}
	/**
	 * 保存 gfwang
	 */
	$scope.saveTask = function () {
		// validate();
		var flag = false;
		$('#taskForm').data('bootstrapValidator').validate();
		$("small").map(function () {
			if ($(this).attr('data-bv-result') == "INVALID") {
				flag = true;
			}
		});
		if (flag) {
			return;
		}
		$scope.showLoading("#myTask");
		var followArr = $scope.taskInfo.followUpInfos;
		for (var i = 0; i < followArr.length; i++) {
			if (followArr[i].durationStep) {
				followArr[i].durationStep = followArr[i].durationStep * 1;
			}
		}
		$scope.saveObj = angular.copy($scope.taskInfo);
		setBeforeSaveTaskInfo();
		// 页面list数据

		taskSettingService.saveTask($scope.saveObj).then(angular.bind(this, function then() {
			var result = taskSettingService.saveResult;
			if (result.code == 0) {
				$scope.initTaskModel();
				$scope.getTaskList();
				$scope.message(result.msg);
			} else if (result.code == 101) {
				$scope.hideLoading("#myTask");
				$rootScope.confirm('', result.msg, 'Confirm', function () {
					$scope.taskInfo.overWrite = true;
					$scope.saveTask();
				}, 'Cancel', function () {
					$scope.taskInfo.overWrite = false;
				});
			} else {
				$scope.message(result.msg);
			}
			$scope.hideLoading("#myTask");
		}));
	};
	/** *****************************************nqs***************************** */

	/**
	 * @author gfwang
	 * @description 保存Nqs
	 */
	$scope.saveNqsResult = function (newNqs) {
		$scope.taskInfo.nqsList = newNqs;
	};
	/** *****************************************nqs***************************** */
	// 保存前赋值
	function setBeforeSaveTaskInfo() {
		var selectTaskVisibles = $scope.saveObj.selectTaskVisibles;
		var responsibleTaskVisibles = $scope.saveObj.responsibleTaskVisibles;
		var taskImplementersVisibles = $scope.saveObj.taskImplementersVisibles;

		$scope.saveObj.selectTaskVisibles = [];
		// 如果 选择编辑选项的时候，次下拉框会出现 有子节点的情况，保存的时候需要去除子节点的内容
		angular.forEach(selectTaskVisibles, function (data, index, array) {
			// 判断是否为合法值
			if (data) {
				if (!data.id) {
					// 如果是选项的时候，取子节点
					var childs = data.children;
					if (childs) {
						for (var i = 0; i < childs.length; i++) {
							$scope.saveObj.selectTaskVisibles.push({
								objId: childs[i].id.split(',')[0],
								type: childs[i].type
							});
						}
					}
				} else {
					// 正常保存
					$scope.saveObj.selectTaskVisibles.push({
						objId: data.id.split(',')[0],
						type: data.type
					});
				}

			}
		});

		$scope.saveObj.responsibleTaskVisibles = [];
		angular.forEach(responsibleTaskVisibles, function (data, index, array) {
			// 判断是否为合法值
			if (data && data.id) {
				$scope.saveObj.responsibleTaskVisibles.push({
					objId: data.id.split(',')[0],
					type: data.type
				});
			}
		});

		$scope.saveObj.taskImplementersVisibles = [];
		angular.forEach(taskImplementersVisibles, function (data, index, array) {
			// 判断是否为合法值
			if (data && data.id) {
				$scope.saveObj.taskImplementersVisibles.push({
					objId: data.id.split(',')[0],
					type: data.type
				});
			}
		});

		if ($scope.saveObj.taskCategory == '0' && $scope.saveObj.shiftFlag == 'true') {
			$scope.saveObj.responsibleTaskVisibles = [];
			$scope.saveObj.taskImplementersVisibles = [];
		}
		angular.forEach($scope.saveObj.followUpInfos, function (followup, index, array) {
			var followUpResponsibleTaskVisibles = followup.responsibleTaskVisibles;
			// 滞空重新赋值
			followup.responsibleTaskVisibles = [];
			angular.forEach(followUpResponsibleTaskVisibles, function (data, index, array) {
				// 判断是否为合法值
				if (data && data.id) {
					followup.responsibleTaskVisibles.push({
						objId: data.id.split(',')[0],
						type: data.type
					});
				}
			});
			// 滞空重新赋值
			var followUpTaskImplementersVisibles = followup.taskImplementersVisibles;
			followup.taskImplementersVisibles = [];
			angular.forEach(followUpTaskImplementersVisibles, function (data, index, array) {
				// 判断是否为合法值
				if (data && data.id) {
					followup.taskImplementersVisibles.push({
						objId: data.id.split(',')[0],
						type: data.type
					});
				}
			});
		});
	}

	$scope.changeShift = function () {
		$scope.taskInfo.followUpInfos = [];
		$scope.taskInfo.followFlag = 'true';
	};

	// 是否开启清除子下拉选择项，解决，先加载getInfo,然后执行ngwatch问题
	$scope.openClear = true;

	$scope.initTaskModel = function (id) {
		$scope.formObj = "";
		$scope.showLoading("#myTask");
		$scope.initSelect2();
		// task基本信息
		$scope.taskInfo = {
			id: "",
			taskFormObj: {
				"id": "",
				"name": "Form Name",
				"attributes": []
			},
			// 选择
			selectTaskVisibles: [],
			// 责任人
			responsibleTaskVisibles: [],
			// 填写人
			taskImplementersVisibles: [],
			// 频率
			taskFrequencyInfo: {},
			followUpInfos: [],
			taskCategory: '0',
			shiftFlag: 'true',
			nqsList: []
		};
		if (!id) {
			$scope.chooseNqs = [];
			$('.task-list div').removeClass('active');
			$scope.hideLoading("#myTask");
			return;
		}
		$scope.openClear = false;
		taskSettingService.getTask(id).then(angular.bind(this, function then() {
			var result = taskSettingService.taskResult;
			if (result.code == 0) {
				$scope.taskInfo = result.data;
				// 解决执行顺序的问题
				dealInfo();
			} else {
				$scope.message(result.msg);
			}
			$scope.openClear = true;
			$scope.hideLoading("#myTask");
		}));
	};
	// 处理加载的数据
	function dealInfo() {

		$scope.taskInfo.taskCategory = $scope.taskInfo.taskCategory + "";
		$scope.taskInfo.shiftFlag = $scope.taskInfo.shiftFlag + "";

		$scope.taskInfo.selectTaskVisibles = $scope.taskInfo.selectTaskVisiblesSelect;
		$scope.taskInfo.responsibleTaskVisibles = $scope.taskInfo.responsibleTaskVisiblesSelect;
		$scope.taskInfo.taskImplementersVisibles = $scope.taskInfo.taskImplementersVisiblesSelect;

		angular.forEach($scope.taskInfo.followUpInfos, function (followUp, index, array) {
			followUp.responsibleTaskVisibles = followUp.responsibleTaskVisiblesSelect;
			followUp.taskImplementersVisibles = followUp.taskImplementersVisiblesSelect;
		});
	}

	$scope.$watch('taskInfo.selectObjIdTMP', function (newValue, oldValue) {
		// 清除子下拉
		$timeout(function () {
			if (!$scope.openClear) {
				$scope.openClear = true;
				return;
			}
			$(".myselect").empty();
		}, 0);
	}, true);

	// 制空select2
	$scope.initSelect2 = function () {
		$(".myselect,.myselectParent").empty();
	};

	/**
	 * 禁用或者启用 gfwang
	 */
	$scope.enableOrDisable = function (statu) {
		// 页面list数据
		taskSettingService.disableEnable($scope.taskInfo.id, statu).then(angular.bind(this, function then() {
			var result = taskSettingService.changeResult;
			if (result.code == 0) {
				$scope.taskInfo.statu = statu;
			}
			$scope.message(result.msg);
		}));
	};

	// 打开或者关闭按钮事件
	$scope.addFlowUp = function (currtenIndex, isOpen) {
		if (isOpen) {
			$scope.taskInfo.followUpInfos.push({});
		} else {
			$scope.confirm('Warning', 'Are you sure you want to remove the follow up workflow?', 'confirm', function () {
				removeFlowUp(currtenIndex);
			}, 'cancel', function () {
				if (currtenIndex == null) {
					$scope.taskInfo.followFlag = true;
				} else {
					$scope.taskInfo.followUpInfos[currtenIndex].followFlag = true;
				}
			});
		}
	};
	/**
	 * 循环结束事件
	 */
	$scope.repeatEvent = function () {
		var form = $('#taskForm').bootstrapValidator();
		form.bootstrapValidator('addField', 'followResponsible', {
			validators: {
				notEmpty: {
					message: "The Responsible Person is required"
				}
			}
		});
		form.bootstrapValidator('addField', 'followDuration', {
			validators: {
				digits: {
					message: 'Please enter a valid duration amount and try again.'
				},
				notEmpty: {
					message: "The duration for this step is required"
				},
				stringLength: {
					max: 20,
					message: "The Duration for this step must be less than 20 characters long"
				}
			}
		});
		form.bootstrapValidator('addField', 'followComplete', {
			validators: {
				notEmpty: {
					message: "The Who can complete this step is required"
				}
			}
		});
	};

	/**
	 * 改变选择框，同时改变校验方式
	 */
	function changeValidate(type) {
		return;
		$timeout(function () {
			var form = $('#taskForm').bootstrapValidator();
			form.bootstrapValidator('removeField', 'selectObjId');
			form.bootstrapValidator('removeField', 'responsible');
			form.bootstrapValidator('removeField', 'implementers');
			var message = "";
			switch (type) {
				case 0:
					message = "The Select Centre is required";
					break;
				case 1:
					message = "The Select Room is required";
					break;
				case 2:
					message = "The Select Children is required";
					break;
				case 3:
					message = "The Select Staff is required";
					break;
				default:
					break;
			}
			form.bootstrapValidator('addField', 'selectObjId', {
				validators: {
					notEmpty: {
						message: message
					}
				}
			});

			form.bootstrapValidator('addField', 'responsible', {
				validators: {
					notEmpty: {
						message: "The Responsible People is required"
					}
				}
			});
			form.bootstrapValidator('addField', 'implementers', {
				validators: {
					notEmpty: {
						message: "The Task Implementers is required"
					}
				}
			});
		}, 100);
	}

	// 关闭按钮 移除 flowup
	function removeFlowUp(currtenIndex) {
		if (currtenIndex != 0 && !currtenIndex) {
			// 如果移除主task则清除所有flowup
			$scope.taskInfo.followUpInfos = [];
			return;
		}
		// 移除当前按钮后面的所有flowup
		for (var i = $scope.taskInfo.followUpInfos.length; i > currtenIndex; i--) {
			$scope.taskInfo.followUpInfos.splice(i, 1);
		}
	}

	/**
	 * 选择 分类触发
	 */
	$scope.categoryChange = function () {
		$scope.initSelect2();
		$scope.taskInfo.selectTaskVisibles = [];
		$scope.taskInfo.responsibleTaskVisibles = [];
		$scope.taskInfo.shiftFlag = 'false';
		$scope.taskInfo.taskImplementersVisibles = [];
		$scope.taskInfo.selectObjId = "";
		$scope.taskInfo.responsiblePeopleId = "";
		$scope.taskInfo.taskImplementersId = "";

		if ($scope.taskInfo.taskCategory == '0') {
			$scope.taskInfo.followUpInfos = [];
			$scope.taskInfo.shiftFlag = 'true';
			$scope.taskInfo.taskFrequencyInfo = {};
			$scope.taskInfo.followFlag = false;
		}

		angular.forEach($scope.taskInfo.followUpInfos, function (followUp, index, array) {
			followUp.responsibleTaskVisibles = [];
			followUp.taskImplementersVisibles = [];
			followUp.responsiblePersonIdTMP = [];
			followUp.completeStepIdTMP = [];
		});
		changeValidate($scope.taskInfo.taskCategory * 1);
	};

	/**
	 * 打开频率模态框 gfwang
	 */
	$scope.openFrequency = function () {
		var statu = !$scope.taskInfo ? null : $scope.taskInfo.statu;
		var taskFrequencyInfo = !$scope.taskInfo.taskFrequencyInfo ? {} : $scope.taskInfo.taskFrequencyInfo;
		var Obj = {
			taskFrequencyInfo: taskFrequencyInfo,
			statu: statu,
			callBackSynchrFrequency: $scope.callBackSynchrFrequency
		};
		Obj = angular.copy(Obj);
		Obj.currtenDate = $scope.currentUser.currtenDate;
		$uibModal.open({
			templateUrl: 'views/task/modal/modal-frequency.html?res_v=' + $scope.res_v,
			controller: 'frequencyCtrl',
			backdrop: 'static',
			keyboard: false,
			resolve: {
				Obj: function () {
					return Obj;
				}
			}
		});
	};
	/**
	 * 同步Frequency回调 gfwang
	 */
	$scope.callBackSynchrFrequency = function (frequencyInfo) {
		$scope.taskInfo.taskFrequencyInfo = frequencyInfo;
	};

	/** *********************** 自定义表单 ****************************** */

	$scope.formObj = null;

	$scope.settingForm = function (obj) {
		var taskFormObj = obj.taskFormObj;
		$uibModal.open({
			templateUrl: 'views/form-builder/builder.html?res_v=' + $scope.res_v,
			controller: 'builderFormCtrl',
			animation: true,
			size: "lg",
			backdrop: 'static',
			keyboard: false,
			windowClass: "modal-form-create modal-grey modal-align-top",
			resolve: {
				code: function () {
					return !obj.formId ? 0 : obj.formId;
				},
				hiddenSave: function () {
					if (!$scope.taskInfo) {
						return true;
					}
					return $scope.taskInfo.statu == 1 ? true : false;
				},
				formObj: function () {
					return taskFormObj;
				}
			}
		}).result.then(function (taskFormObj) {
			obj.taskFormObj = taskFormObj;
		});
	};
}
angular.module('kindKidsApp').controller('taskSettingCtrl',
	['$scope', '$rootScope', '$timeout', '$location', '$uibModal', 'taskSettingService', '$stateParams', taskSettingCtrl]);