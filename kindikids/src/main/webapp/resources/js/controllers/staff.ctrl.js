'use strict';

/**
 * @author jfxu
 * @description
 */
function userStaffCtrl($scope, $rootScope, $timeout, $window, $uibModal, staffService, fileService) {
	$rootScope.subScope = $scope;
	$scope.page = {};
	$scope.page.condition = {
		pageIndex: 1,
		roleId: '',
		centerId: '',
		roomId: '',
		userId: '',
		params: '', // 关键字搜索
		casual: 0, // 显示兼职
		myStatus: 0
		// 默认为active
	};
	$scope.menu = {
		isAdd: true
		// 是否新增
	}
	$scope.busy = false;
	// 当前登录用户信息
	$scope.currUser = {
		id: null,
		firstRoleId: null, // 第一角色id
		roleValue: null, // 第一角色类型【0,1,5】
		roleTemp: -1, // 临时排班角色
		secondRole: -1,
		centerId: null, // 园区id
		roomId: null,
		isCM: 0, // 是否为园长
		isCEO: 0, // 是否为CEO
		showAdd: false, // 显示添加按钮
		showSend: false, // 显示发送邮件
		showDelete: false, // 显示删除
		showArchive: false, // 显示归档
		showSave: true, // 显示保存
		showEmploy: false
		// Employ编辑
	};

	// 页面初始化加载数据
	$scope.getRoleList = function () {
		staffService.getRoleList().then(angular.bind(this, function then() {
			// 获取role/center/room/group list
			$scope.roles = staffService.roleResult;
		})).then(function () {
			// 初始化搜索列表
			$scope.initStaff();
		}).then(function () {
			$scope.page.condition.pageIndex = 1;
			$scope.getStaffList(false);
		});
	};
	// 初始化数据
	$scope.initStaff = function () {
		for (var i = 0; i < $scope.currentUser.roleInfoVoList.length; i++) {
			// 如果当前角色为临时排班角色，设置roleTemp为当前角色
			if ($scope.currentUser.roleInfoVoList[i].tempFlag == 1) {
				$scope.currUser.roleTemp = $scope.currentUser.roleInfoVoList[i].value;
			}
			if ($scope.currentUser.roleInfoVoList[i].value == 1 || $scope.currentUser.roleInfoVoList[i].value == 5) {
				$scope.currUser.isCM = 1;
				break;
			}
			if ($scope.currentUser.roleInfoVoList[i].value == 0) {
				$scope.currUser.isCEO = 1;
				break;
			}
		}

		// 当前登录用户的第一角色
		var iLength = $scope.currentUser.roleInfoVoList.length;
		var jLength = $scope.roles.roleInfos.length;
		for (var i = 0; i < iLength; i++) {
			for (var j = 0; j < jLength; j++) {
				if ($scope.currentUser.roleInfoVoList[i].id == $scope.roles.roleInfos[j].id) {
					if ($scope.roles.roleInfos[j].roleType == 1) {
						$scope.currUser.firstRoleId = $scope.roles.roleInfos[j].id; // first
						// role
					}
				}
			}
		}
		// 当前登录用户的园区和id
		$scope.currUser.centerId = $scope.currentUser.userInfo.centersId;
		$scope.currUser.roomId = $scope.currentUser.userInfo.roomId;
		$scope.currUser.id = $scope.currentUser.userInfo.id;

		// 确定当前登录用户的一级角色
		var arr = [0, 1, 5, 2, 9]; // ceo,centerManager,second,edu,cook

		for (var i = 0; i < $scope.currentUser.roleInfoVoList.length; i++) {
			// 临时排班角色不能作为第一角色
			if ($scope.currentUser.roleInfoVoList[i].tempFlag == 1) {
				continue;
			}
			if ($scope.currentUser.roleInfoVoList[i].value == arr[0]) {
				$scope.currUser.roleValue = 0;
				$scope.currUser.showEmploy = true;
				continue;
			} else if ($scope.currentUser.roleInfoVoList[i].value == arr[1]) {
				$scope.currUser.roleValue = 1;
				//update by gfwan on 17/04/2017
				//$scope.page.condition.centerId = $scope.currUser.centerId;
				continue;
			} else if ($scope.currentUser.roleInfoVoList[i].value == arr[2]) {
				//update by gfwan on 17/04/2017
				$scope.currUser.roleValue = 5;
				$scope.page.condition.userId = "";
				//$scope.page.condition.centerId = $scope.currUser.centerId;
				continue;
			} else if ($scope.currentUser.roleInfoVoList[i].value == arr[3]) {
				$scope.currUser.roleValue = 2;
				$scope.page.condition.centerId = $scope.currUser.centerId;
				// 如果临时排班角色为园长，设置为可以看到园中所有角色（不设置为只看自己）
				if ($scope.currUser.roleTemp != 1) {
					$scope.page.condition.userId = $scope.currentUser.userInfo.id;
				}
				continue;
			} else if ($scope.currentUser.roleInfoVoList[i].value == arr[4]) {
				$scope.currUser.roleValue = 9;
				$scope.page.condition.centerId = $scope.currUser.centerId;
				// 如果临时排班角色为园长，设置为可以看到园中所有角色（不设置为只看自己）
				if ($scope.currUser.roleTemp != 1) {
					$scope.page.condition.userId = $scope.currentUser.userInfo.id;
				}
				continue;
			}
		}
		if ($scope.isCenterManager) {
			$scope.page.condition.centerId = '';
		}
		// 一般员工只能看到自己的信息
		if (arr.indexOf($scope.currUser.roleValue) == -1 || ($scope.currUser.roleValue == 2 && $scope.currUser.roleTemp != 1)
			|| ($scope.currUser.roleValue == 9 && $scope.currUser.roleTemp != 1)) {
			$scope.page.condition.centerId = "";
			$scope.page.condition.userId = $scope.currentUser.userInfo.id;
		} else {
			$scope.currUser.showAdd = true;
			$scope.currUser.showSend = true;
			$scope.currUser.showDelete = true;
			$scope.currUser.showArchive = true;
		}

	};
	// $scope.getRoleList();

	// 搜索条件过滤器
	$scope.locationFilter = function (item) {
		if ($scope.currUser.roleValue == 0) {
			return true;
		}
		if ($scope.currUser.centerId == null) {
			return false;
		}
		if ($scope.currUser.roleValue == 1 || $scope.currUser.roleValue == 5) {
			//update by gfwan on 04/17/2017 解决园长能看到所有员工
			//if (item.id == $scope.currUser.centerId) {
			return true;
			//}
			//return false;
		}
		if ($scope.currUser.roleValue == 2) {
			if (item.id == $scope.currUser.centerId) {
				var length = $scope.roles.roomInfos.length;
				for (var i = 0; i < length; i++) {
					if ($scope.roles.roomInfos[i].id == $scope.currUser.roomId) {
						var currRoom = $scope.roles.roomInfos[i];
						$scope.roles.roomInfos.splice(0, length);
						$scope.roles.roomInfos.push(currRoom);
						return true;
					}
				}
			}
			return false;
		}
		if ($scope.currUser.roleValue == 9) {
			if (item.id == $scope.currUser.centerId) {
				$scope.roles.roomInfos = '';
				return true;
			}
			return false;
		}
		return false;
	}
	// 搜索条件过滤器
	$scope.roleFilter = function (item) {
		return true;
		if ($scope.currUser.roleValue == 0) {
			return true;
		}
		if ($scope.currUser.roleValue == 1 || $scope.currUser.roleValue == 5) {
			if (item.value == 1 || item.value == 2 || item.value == 9) {
				return true;
			}
			return false;
		}
		if ($scope.currUser.roleValue == 2 && item.value == 2) {
			return true;
		}
		if ($scope.currUser.roleValue == 9 && item.value == 9) {
			return true;
		}
		// 如果排班角色为园长，左侧导航能看见所有的（除ceo）
		if ($scope.currUser.roleTemp == 1) {
			if (item.value == 1 || item.value == 2 || item.value == 9) {
				return true;
			}
			return true;
			// 如果排班角色不为园长，左侧导航增加该角色
		} else if ($scope.currUser.roleTemp == item.value) {
			return true;
		}
		return false;
	};

	// 获取员工列表
	$scope.getStaffList = function (isDown) {
		if (isDown) {
			if ($scope.page.list.length >= $scope.page.condition.totalSize) {
				return;
			}
			$scope.page.condition.pageIndex += 1;
			$scope.busy = true;
		}
		if ($scope.page.condition.roomId) {
			$scope.page.condition.centerId = '';
		}
		$scope.showLoading("#staff_list");
		var param = $scope.page.condition.params;
		staffService.getStaffList($scope.page.condition).then(angular.bind(this, function then() {
			var result = staffService.listResult;
			result.data.condition.params = param;
			$scope.hideLoading("#staff_list");
			if (result.code == 0) {
				$scope.dealListResult(result.data, isDown);
				if (result.data.condition.search && result.data.list.length == 0) {
					$scope.confirm('Message', 'No results match your search.', 'OK', function () {
						$("#privacy-modal").modal('hide');
					});
				}
				$scope.page.condition.search = false;
			} else {
				$rootScope.message(result.msg);
			}
		}));
	};
	$scope.dealListResult = function (data, isAppend) {
		// 判断是否为 追加
		if (isAppend) {
			if (!$scope.page.list) {
				$scope.page = data;
			} else {
				$scope.page.condition = data.condition;
				var length = data.list.length;
				for (var i = 0; i < length; i++) {
					// 向列表追加元素
					$scope.page.list.push(data.list[i]);
				}
			}
			$timeout(function () {
				$scope.busy = false;
			}, 2000);
		} else {
			$scope.page = data;
		}
	}

	/*
	 * $scope.scorellUp = function() { //第一次不加载，通过getRoleList加载
	 * if(!$scope.isFirst){ $scope.page.condition.pageIndex = 1;
	 * $scope.getStaffList(false); } }
	 */

	// isOpenByMain 是否主页打开
	// 查看员工详细信息
	$scope.openProfile = function (userId) {
		if (!userId) {
			$scope.menu.isAdd = true;
		} else {
			$scope.menu.isAdd = false;
		}
		$uibModal.open({
			templateUrl: 'views/user/staff/staff_edit.html?res_v=' + $scope.res_v,
			controller: 'userStaffEditCtrl',
			size: "lg",
			backdrop: 'static',
			keyboard: false,
			windowClass: "profile-popup modal-align-top",
			resolve: {
				userId: function () { // 用户id
					return userId;
				},
				roles: function () { // 角色、园区、Room、Group数据
					return $scope.roles;
				},
				menu: function () { // 菜单功能
					return $scope.menu;
				},
				currUser: function () { // 当前登录用户信息
					return $scope.currUser;
				},
				reloadCallBack: function () {
					return $scope.getStaffList;
				}
			}
		});
	};

	$scope.goList = function () {
		$rootScope.$state.go("app.main.user.staff.list");
	};
	$scope.setSearch = function (id) {
		$scope.page.condition.roleId = id;
	};
	// 回车键搜索
	$scope.enterSearch = function (ev) {
		if (ev.keyCode == 13) {
			$scope.goList();
			$scope.page.condition.pageIndex = 1;
			$scope.page.condition.search = true;
			$scope.getStaffList();
		}
	};
	$scope.search = function () {
		$scope.goList();
		$scope.page.condition.pageIndex = 1;
		$scope.page.condition.search = true;
		$scope.getStaffList();
	}
	$scope.reloadPage = function () {
		if ($scope.$stateParams.userId && $scope.$stateParams.userId != '*') {
			$timeout(function () {
				staffService.getRoleList().then(angular.bind(this, function then() {
					// 获取role/center/room/group list
					$scope.roles = staffService.roleResult;
				})).then(function () {
					// 初始化搜索列表
					$scope.initStaff();
				}).then(function () {
					$scope.page.condition.pageIndex = 1;
					$scope.getStaffList(false);
					$scope.openProfile($scope.$stateParams.userId);// .split('kik')[0]
				});
			}, 100);
		}
	}

}
angular.module('kindKidsApp').controller('userStaffCtrl',
	['$scope', '$rootScope', '$timeout', '$window', '$uibModal', 'staffService', 'fileService', userStaffCtrl]);
