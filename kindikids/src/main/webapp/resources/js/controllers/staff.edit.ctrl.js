'use strict';

/**
 * @author jfxu
 * @description
 */
function userStaffEditCtrl($scope, $rootScope, $timeout, $filter, $window, $uibModal, reloadCallBack, $uibModalInstance, commonService, staffService, fileService, familyService, userId, roles, menu,
	currUser) {
	$scope.logUserId = userId;
	$scope.functions = [];
	commonService.getFun("/user_staff_list").then(
		angular.bind(this, function then() {
			$scope.functions = commonService.functions;
			$scope.haveStaffSend = $filter('haveFunction')($scope.functions,
				'staff_send_email');
			$scope.havaStaffArchive = $filter('haveFunction')($scope.functions,
				'staff_archive');
			$scope.havaStaffDelete = $filter('haveFunction')($scope.functions,
				'staff_delete');
			$scope.haveStaffSave = $filter('haveFunction')($scope.functions,
				'staff_save');
		}));

	$rootScope.subScope = $scope;

	$scope.userId = userId; // selected userId
	$scope.roles = roles; // all roles info
	$scope.menu = menu; // 菜单功能[添加员工]
	$scope.currUser = currUser; // 当前登录用户信息
	$scope.isArchive = false; // 是否归档 add by big
	$scope.addStaff = !userId ? true : false
	$scope.isCeoCM = ($filter('haveRole')($rootScope.currentUser.roleInfoVoList, '0;1;5'));

	// 当前打开的员工信息
	$scope.employ = {
		isCM: false, // 是否为园长
		showSRole: false, // 是否显示第二角色
		showStaffType: false, //是否显示stafftype
		showCenter: true, // 是否显示园区 casual =>不显示center、room、group
		showRoom: true, // 是否显示Room cook =>显示center
		showGroup: true, // 是否显示组
		// CEO 不显示center、room、group
		// Educator 显示所有
		// Casual 不显示center、room、group

		showName: '',
		firstRole: {}, // 第一角色
		secondRole: [], // 所有第二角色
		roles: [], // 选中的第二角色
		centerId: null, // 园区id
		status: 1
		// 账号状态
	};


	$scope.currEmployDisable = false;

	/**
	 * 获取颜色 add by gfwang
	 */
	$scope.getCurrtenColor = function (userId, userType) {
		if (userId) {
			$scope.getStaffInfo(userId, null);
			return;
		}
		commonService.getNextColor($scope.familyId, userType).then(angular.bind(this, function then() {
			var result = commonService.nextResult;
			if (result.code == 0) {
				var color = result.msg;
				$scope.getStaffInfo(userId, color);
			}
		}));

	};
	$scope.getStaffInfo = function (userId, color) {
		if (!userId) { // add
			$scope.staffInfo = {
				userInfo: {
					centersId: null,
					roomId: null,
					groupId: null,
					personColor: color,
					fileList: []
				},
				contactInfo: [{
					key: 1
				}],
				credentialsInfo: [{
					type: 1,
					fileList: []
				}, {
					type: 2,
					fileList: []
				}, {
					type: 3,
					fileList: [],
					fileList2: []
				}, {
					type: 4,
					fileList: []
				}, {
					type: 5,
					fileList: []
				}],
				medicalInfo: {
					id: null
				},
				medicalImmunisationInfo: [{
					name: '1'
				}, {
					name: '2'
				}, {
					name: '3'
				}, {
					name: '4'
				}, {
					name: '5'
				}, {
					name: '6'
				}],
				roleInfo: [{
					centersId: null
				}],
				accountInfo: {
					account: null
				},
				employInfo: {
					monday: false,
					monStartTime: '6:00 AM',
					monEndTime: '7:00 PM',
					tuesday: false,
					tueStartTime: '6:00 AM',
					tueEndTime: '7:00 PM',
					wednesday: false,
					wedStartTime: '6:00 AM',
					wedEndTime: '7:00 PM',
					thursday: false,
					thuStartTime: '6:00 AM',
					thuEndTime: '7:00 PM',
					friday: false,
					friStartTime: '6:00 AM',
					friEndTime: '7:00 PM'
				}
			}

			// 园长登录添加员工则centerId与园长一致
			if ($scope.currUser.roleValue == 1 || $scope.currUser.roleValue == 5) {
				$scope.staffInfo.userInfo.centersId = $scope.currUser.centerId;
			}
			$scope.getProfileByUserId();
			$scope.showForm = true;
			$scope.currUser.showSave = true;
			$scope.logList = [];
			$scope.showNote = true;
		} else { // edit
			staffService.getStaffInfo(userId).then(
				angular.bind(this, function then() {
					var result = staffService.staffResult;
					if (result.code != 0) {
						$rootScope.message(result.msg);
						return;
					}
					$scope.employ.roles = [];
					$scope.staffInfo = result.data;

					$scope.employ.centerId = $scope.staffInfo.userInfo.centersId; // user
					// centerId
					$scope.employ.status = $scope.staffInfo.accountInfo.status;

					var iLength = $scope.staffInfo.roleInfo.length;
					var jLength = $scope.roles.roleInfos.length;
					for (var i = 0; i < iLength; i++) {
						for (var j = 0; j < jLength; j++) {
							if ($scope.staffInfo.roleInfo[i].roleId == $scope.roles.roleInfos[j].id) {
								if ($scope.roles.roleInfos[j].roleType == 1) { // 一级角色
									$scope.employ.firstRole = $scope.staffInfo.roleInfo[i]; // first
									// role

									// 不可修改同级角色的Employ信息
									if (($scope.currUser.firstRoleId == $scope.employ.firstRole.roleId) &&
										($scope.currUser.isCEO == 0 && $scope.currUser.isCM == 0)) {
										$scope.currEmployDisable = true;
									}


									if ($scope.roles.roleInfos[j].value == 1 || $scope.roles.roleInfos[j].value == 5) { // center
										// manager
										$scope.employ.isCM = true;
									}

									if ($scope.roles.roleInfos[j].value == 0) { // center
										// manager
										$scope.employ.isCEO = true;
									}

									var employRoleIndex = $scope.roles.roleInfos[j].value == 5 ? 1 : $scope.roles.roleInfos[j].value;
									var currentRoleIndex = $scope.currUser.roleValue == 5 ? 1 : $scope.currUser.roleValue;
									if (currentRoleIndex > employRoleIndex) {
										$scope.currEmployDisable = true;
									}

								} else if ($scope.roles.roleInfos[j].roleType == 0) { // 二级角色
									$scope.employ.secondRole.push($scope.staffInfo.roleInfo[i]); // second
									// role
									// list
									$scope.employ.roles.push($scope.staffInfo.roleInfo[i].roleId); // selected
									// roleId
								}
							}
						}
					}

					$scope.initRole(); // 初始化角色选择
					$scope.dealButtons();

					if ($scope.staffInfo.employInfo != null) {
						// 计算总时间
						$scope.totalHours = $scope.staffInfo.employInfo.monHours + $scope.staffInfo.employInfo.tueHours + $scope.staffInfo.employInfo.wedHours + $scope.staffInfo.employInfo.thuHours + $scope.staffInfo.employInfo.friHours;
						// 设置区域
						if ($scope.staffInfo.employInfo.region == 0) {
							$scope.staffInfo.employInfo.edensorPark = true;
							$scope.staffInfo.employInfo.ryde = true;
						}
						if ($scope.staffInfo.employInfo.region == 1) {
							$scope.staffInfo.employInfo.edensorPark = true;
						}
						if ($scope.staffInfo.employInfo.region == 2) {
							$scope.staffInfo.employInfo.ryde = true;
						}
					} else {
						$scope.staffInfo.employInfo = {
							staffType: $scope.staffInfo.userInfo.staffType,
							centreId: $scope.staffInfo.userInfo.centersId,
							roomId: $scope.staffInfo.userInfo.roomId,
							groupId: $scope.staffInfo.userInfo.groupId,
							primaryRole: $scope.employ.firstRole.roleId
						};
						$scope.totalHours = 0;
					}

					$scope.staffInfo.employInfo.roles = $scope.employ.roles;

					//add gfwang
					//TODO
					$scope.getProfileByUserId();
					$scope.showForm = true;
					// add mingwang
					//staffInfo.userInfo.id != currUser.id
					if ($scope.currUser.isCEO == 1) {
						$scope.showNote = true;
					} else if ($scope.currUser.isCM == 1 && $scope.currUser.centerId == $scope.staffInfo.userInfo.centersId) {
						$scope.showNote = true;
					} else {
						$scope.showNote = false;
					}

				}));
		}
	}
	// init staff info
	$scope.getCurrtenColor($scope.userId, 0);

	// 选择一级角色
	$scope.secondSelect = [];

	// 更换角色  by mingwang
	$scope.roleChange = function () {
		var frs = $scope.employ.firstRole.roleId;
		var val = $scope.getValueByRoleId(frs);
		$scope.initRole();
		if (val == 1 || val == 2) {
			$scope.employ.roles = [];
		}
		$scope.changeValidate();
	};

	// 初始化角色   Modify by mingwang
	$scope.initRole = function () {
		var frs = $scope.employ.firstRole.roleId;
		var val = $scope.getValueByRoleId(frs);

		if (val == 0) { // CEO
			$scope.secondSelect = [];
			$scope.employ.showCenter = false;
			$scope.employ.showRoom = false;
			$scope.employ.showGroup = false;
			$scope.employ.showRegion = false;

			$scope.employ.showSRole = false;
			$scope.employ.showStaffType = false;
			$scope.staffInfo.userInfo.centersId = null;
			$scope.staffInfo.userInfo.roomId = null;
			$scope.staffInfo.userInfo.groupId = null;
			$scope.employ.roles = [];
			$scope.staffInfo.userInfo.staffType = null;
		} else if (val == 1) { // Center Manager
			$scope.secondSelect = [3, 7, 8, 12, 13];
			$scope.employ.showCenter = true;
			$scope.employ.showRoom = false;
			$scope.employ.showGroup = false;
			$scope.employ.showRegion = true;

			$scope.employ.showSRole = true;
			$scope.employ.showStaffType = false;
			$scope.staffInfo.userInfo.roomId = null;
			$scope.staffInfo.userInfo.groupId = null;
			//			$scope.employ.roles = [];
			$scope.staffInfo.userInfo.staffType = null;
			$scope.employ.showName = $scope.getNameByRoleId($scope.employ.firstRole.roleId);
		} else if (val == 2) { // Educator
			//$scope.secondSelect = [ 3, 4, 5, 6, 7, 8 ];
			$scope.secondSelect = [3, 5, 6, 7, 12, 13];
			$scope.employ.showCenter = true;
			$scope.employ.showGroup = true;
			$scope.employ.showRoom = true;
			$scope.employ.showRegion = true;

			//			$scope.employ.roles = [];
			$scope.employ.showSRole = true;
			$scope.employ.showStaffType = true;
			$scope.employ.showName = $scope.getNameByRoleId($scope.employ.firstRole.roleId);
		} else if (val == 9) { // Cook
			//$scope.secondSelect = [ 4 ];
			$scope.secondSelect = [];

			$scope.employ.showCenter = true;
			$scope.employ.showRoom = false;
			$scope.employ.showGroup = false;
			$scope.employ.showRegion = true;

			$scope.employ.showSRole = false;
			$scope.employ.showStaffType = true;
			$scope.employ.showName = $scope.getNameByRoleId($scope.employ.firstRole.roleId);

			$scope.staffInfo.userInfo.roomId = null;
			$scope.staffInfo.userInfo.groupId = null;
		} else { // other
			$scope.secondSelect = [];
			$scope.employ.roles = [];
			$scope.staffInfo.userInfo.staffType = null;
			$scope.employ.showGroup = true;
			$scope.employ.showRoom = true;
			$scope.employ.showCenter = true;
			$scope.employ.showRegion = true;

			$scope.employ.showSRole = false;
			$scope.employ.showStaffType = false;
			$scope.employ.showName = '';
		}
		// Casual
		if ($scope.staffInfo.userInfo.staffType == 2) {
			$scope.employ.showCenter = false;
			$scope.employ.showRoom = false;
			$scope.employ.showGroup = false;

			$scope.staffInfo.userInfo.centersId = $scope.currUser.centerId;
			$scope.staffInfo.userInfo.roomId = null;
			$scope.staffInfo.userInfo.groupId = null;
		}
		$scope.validateEmployment();
	}

	// 监听二级角色
	$scope.$watch('staffInfo.userInfo.staffType', function (newValue, oldValue) {
		if ($scope.staffInfo == undefined || $scope.staffInfo.userInfo.staffType == null) return;
		if ($scope.staffInfo.userInfo.staffType != 2) {
			var frs = $scope.employ.firstRole.roleId;
			var val = $scope.getValueByRoleId(frs);
			if (val == 2) { // Educator
				$scope.employ.showCenter = true;
				$scope.employ.showRoom = true;
				$scope.employ.showGroup = true;
			} else if (val == 9) { // Cook
				$scope.employ.showCenter = true;
				$scope.employ.showRoom = false;
				$scope.employ.showGroup = false;
			}
		} else {
			$scope.employ.showCenter = false;
			$scope.employ.showRoom = false;
			$scope.employ.showGroup = false;

			$scope.staffInfo.userInfo.centersId = $scope.currUser.centerId;
			$scope.staffInfo.userInfo.roomId = null;
			$scope.staffInfo.userInfo.groupId = null;
		}
	});

	// 初始化一级角色
	$scope.roleFilter = function (item) {
		return true;
		/*if ($scope.currUser.roleValue == 0) { // ceo
			return true;
		} 
		// 园长不可选择CEO和CM,但可以查看
		if (item.value == 0) {
			return false;
		}
		// 除CEO以外不可添加CEO
		if ($scope.menu.isAdd) {
			if (item.value == 1) {
				return false;
			}
			return true;
		}
		return true;*/
	};

	// 初始化二级角色
	$scope.secondRoleFilter = function (item) {
		for (var i = 0; i < $scope.secondSelect.length; i++) {
			if ($scope.secondSelect[i] == item.value) {
				return true;
			}
		}
		return false;
	}

	// 园长只能选择对应的园区
	$scope.centerFilter = function (item) {
		if ($scope.centreAndCasual) {
			return true;
		}
		// 如果当期登录这是ceo 那么 可以选择全部的任一一个
		if ($scope.currUser.isCEO == 1) {
			return true;
		}

		// center manager
		if ($scope.employ.isCM) { // 当前打开的角色是否为园长
			return true;
			/*if (item.id != $scope.employ.centerId) {
				return false;
			}*/
		}
		if ($scope.currUser.isCM == 1) { // 当前登录用户是否为园长
			return true;
			/*if (item.id != $scope.currUser.centerId) {
				return false;
			}*/
		}
		return true;
	}

	// 通过角色id取得value
	$scope.getValueByRoleId = function (roleId) {
		var jLength = $scope.roles.roleInfos.length;
		for (var j = 0; j < jLength; j++) {
			if ($scope.roles.roleInfos[j].id == roleId) {
				return $scope.roles.roleInfos[j].value;
			}
		}
	}
	// 通过角色id取得角色名称
	$scope.getNameByRoleId = function (roleId) {
		var jLength = $scope.roles.roleInfos.length;
		for (var j = 0; j < jLength; j++) {
			if ($scope.roles.roleInfos[j].id == roleId) {
				return $scope.roles.roleInfos[j].name;
			}
		}
	}

	// medical
	$scope.setNull = function (immu) {
		if (!immu.hadVaccine) {
			immu.vaccineDate = null;
		}
		if (!immu.confirmed) {
			immu.confirmedDate = null;
		}
	}

	$scope.dealButtons = function () {
		if ($scope.employ.status == 0) { // 已归档用户只能Unachieve和Delete
			$scope.currUser.showSend = false;
			$scope.currUser.showSave = false;
			// 归档初始化
			$scope.isArchive = true;
		} else if ($scope.employ.status == 1) {
			$scope.currUser.showSend = true;
			$scope.currUser.showSave = true;
		}
		// 非CEO、园长不可sendEmail
		if ($scope.currUser.isCEO == 0 && $scope.currUser.isCM == 1 && $scope.employ.status == 1) {
			$scope.currUser.showSend = true;
		} else if ($scope.currUser.isCEO == 1 && $scope.currUser.isCM == 0 && $scope.employ.status == 1) {
			$scope.currUser.showSend = true;
		} else {
			$scope.currUser.showSend = false;
		}
	}

	/**
	 * 保存员工信息
	 */
	$scope.saveStaff = function () {
		// 表单校验
		$scope.checkEmploy();

		var flag = false;
		$('#staffForm').data('bootstrapValidator').validate();
		$("small").map(function () {
			if ($(this).attr('data-bv-result') == "INVALID") {
				flag = true;
			}
		});
		if (flag) {
			return;
		}

		/*
		 * $rootScope.message('success'); console.log($scope.staffInfo); return;
		 */

		$scope.showLoading("#modalMain");
		$scope.staffInfo.roleInfo = [];
		$scope.staffInfo.roleInfo.push($scope.employ.firstRole);
		// 组装account_role
		for (var i = 0; i < $scope.employ.roles.length; i++) {
			var newRoleInfo = {
				accountId: $scope.staffInfo.accountInfo.id,
				createAccountId: null,
				createTime: null,
				deleteFlag: 0,
				id: null,
				roleId: $scope.employ.roles[i],
				updateAccountId: null,
				updateTime: null,
			}
			$scope.staffInfo.roleInfo.push(newRoleInfo);
		}
		// account账号
		$scope.staffInfo.accountInfo.account = $scope.staffInfo.userInfo.email;

		if ($scope.staffInfo.getJson) {
			$scope.staffInfo.json = JSON.stringify($scope.staffInfo.getJson());
		}
		// 处理employInfo
		// 0 : All 1 : Edensor Park 2 : Ryde
		if ($scope.staffInfo.employInfo.edensorPark && $scope.staffInfo.employInfo.ryde) {
			$scope.staffInfo.employInfo.region = 0;
		}
		if ($scope.staffInfo.employInfo.edensorPark && !$scope.staffInfo.employInfo.ryde) {
			$scope.staffInfo.employInfo.region = 1;
		}
		if ($scope.staffInfo.employInfo.ryde && !$scope.staffInfo.employInfo.edensorPark) {
			$scope.staffInfo.employInfo.region = 2;
		}
		$scope.staffInfo.employInfo.centreId = $scope.staffInfo.userInfo.centersId;
		$scope.staffInfo.employInfo.roomId = $scope.staffInfo.userInfo.roomId;
		$scope.staffInfo.employInfo.groupId = $scope.staffInfo.userInfo.groupId;
		$scope.staffInfo.employInfo.staffType = $scope.staffInfo.userInfo.staffType;
		$scope.staffInfo.employInfo.primaryRole = $scope.employ.firstRole.roleId;
		$scope.staffInfo.employInfo.tags = $scope.employ.roles;

		staffService.updateStaff($scope.staffInfo).then(angular.bind(this, function then() {
			var result = staffService.updateResult;
			if (result.code == 0) {
				$scope.hideLoading("#modalMain");
				$uibModalInstance.dismiss('cancel');
				reloadCallBack();
			} else {
				$rootScope.message(result.msg);
				$scope.hideLoading("#modalMain");
			}
		}));
	};

	/**
	 * archive 归档
	 */
	$scope.archiveStaff = function (userId, type) {
		var title = "";
		var content = "";
		if (type == 0) {
			title = "Archive";
			content = "Are you sure you want to archive this staff member?";
		} else {
			title = "Unarchive";
			content = "Are you sure you want to unarchive this staff member?";
		}
		$scope.confirm(title, content, 'Yes', function () {
			staffService.archiveStaff(userId, type).then(angular.bind(this, function then() {
				var result = staffService.archiveResult;
				if (result.code != 1) {
					$scope.employ.status = type;
					$scope.staffInfo.accountInfo.status = type;
					$scope.dealButtons();
					clearOrg(result.code)
					$rootScope.message(result.msg);
					if (type == 0) {
						$scope.isArchive = true;
					} else {
						$scope.isArchive = false;
					}
					$scope.getStaffInfo(userId, null);
				} else {
					$rootScope.message(result.msg);
				}
			}));
		}, 'No', null);
	};

	function clearOrg(code) {
		if (code == 2) {
			$scope.staffInfo.userInfo.centersId = "";
			$scope.staffInfo.userInfo.roomId = "";
			$scope.staffInfo.userInfo.groupId = "";
		} else if (code == 3) {
			$scope.staffInfo.userInfo.roomId = "";
			$scope.staffInfo.userInfo.groupId = "";
		} else if (code == 4) {
			$scope.staffInfo.userInfo.groupId = "";
		}
	}
	/**
	 * 发送欢迎邮件
	 */
	$scope.sendWelEmail = function (userId, accountId) {
		staffService.sendWelEmail(userId, accountId).then(angular.bind(this, function then() {
			var result = staffService.sendResult;
			$rootScope.message(result.msg);
		}));
	};

	/**
	 * 删除员工
	 */
	$scope.deleteStaff = function (userId) {
		$scope.confirm('Delete', 'Are you sure you want to delete this staff?', 'Yes', function () {
			staffService.deleteStaff(userId).then(angular.bind(this, function then() {
				var result = staffService.deleteResult;
				if (result.code == 0) {
					//$timeout(function() {
					// 关闭并刷新
					$uibModalInstance.dismiss('cancel');
					$scope.$state.reload();
					//}, 2000);
				} else {
					$rootScope.message(result.msg);
				}
			}));
		}, 'No', null);
	};

	/**
	 * 添加紧急联系人
	 */
	$scope.addContact = function () {
		var newOne = {
			address: null,
			homePhoneNumChild: null,
			id: null,
			name: null,
			personAuthorityChild: null,
			phoneNum: null,
			postcode: null,
			relation: null,
			relationshipChild: null,
			state: null,
			suburb: null,
			userId: null,
			workPhoneNumChild: null
		}
		$scope.staffInfo.contactInfo.push(newOne);
	}

	/**
	 * 删除紧急联系人
	 */
	$scope.deleteContact = function (index, staffInfo) {
		staffInfo.contactInfo.splice(index, 1);
		$timeout(function () {
			var flag = false;
			$("#staffProfile small").map(function () {
				if ($(this).attr('data-bv-result') == "INVALID") {
					flag = true;
				}
			});
			if (!flag) {
				$("#staffProfileTab").find('a').removeClass('my-validate');
			}
		}, 100);
	};

	$scope.validateEmployment = function () {
		$timeout(function () {
			var flag = false;
			$("#staffEmployment small").map(function () {
				if ($(this).attr('data-bv-result') == "INVALID") {
					flag = true;
				}
			});
			if (!flag) {
				$("#staffEmploymentTab").find('a').removeClass('my-validate');
			}
		}, 100);
	};

	// 解决删除联系人，tab验证红色不去除问题
	/*	$scope.validateTag = function($tabPane) {
			var tabId = $tabPane.attr('id');
			if (tabId) {
				// 搜索菜单
				var $icon = $('a[href="#' + tabId + '"][data-toggle="tab"]');
				// 如果当前tabs有验证不通过的，则给当前菜单加class
				if ($("#" + tabId).find("small[data-bv-result='INVALID']").length > 0) {
					$icon.addClass('my-validate');
				} else {
					$icon.removeClass('my-validate');
				}
			}
		};*/
	/**
	 * 删除credent
	 */
	$scope.deleteCredent = function (item) {
		$scope.staffInfo.credentialsInfo.splice($scope.staffInfo.credentialsInfo.indexOf(item), 1);
	};

	/**
	 * 添加证书
	 */
	$scope.addQualification = function (type) {
		var newOne = {
			checkNum: null,
			completedDate: null,
			courseName: null,
			certificateImgId: null,
			employVerifyAttachId: null,
			expiryDate: null,
			id: null,
			letterImgId: null,
			qualObtained: null,
			type: type,
			userId: null,
			fileList: [],
			fileList2: []
		}
		$scope.staffInfo.credentialsInfo.push(newOne);
	}

	// 头像上传
	$scope.myImage = '';
	$scope.avatarImage = '';
	$scope.showAvatarImage = true;
	$scope.avatarfile = null;
	/**
	 * @author abliu
	 * @description 文件上传
	 */
	$scope.uploadAvatar = function ($file) {
		var file = $file;
		if (file == undefined) {
			return;
		}
		var isImagemSG = $rootScope.isImage($file.name);
		if (isImagemSG) {
			$rootScope.message(isImagemSG);
			return
		}
		if (file.name) {
			$scope.showAvatarImage = false;
		}
		var reader = new FileReader();
		reader.onload = function (evt) {
			$scope.$apply(function ($scope) {
				$scope.myImage = evt.target.result;
				$scope.avatarfile = file;
			});
		};
		reader.readAsDataURL(file);
	};

	$scope.cancelAvatar = function () {
		$scope.myImage = '';
		$scope.avatarImage = '';
		$scope.avatarName = '';
		$scope.showAvatarImage = true;
	};
	$scope.saveAvatar = function (myImage) {
		$scope.showLoading("#staffAvatar");
		fileService.uploadResizeImage('', myImage, $scope.avatarfile.name).then(angular.bind(this, function then() {
			var result = fileService.fdata;
			if (result.code == 0) {
				$scope.staffInfo.userInfo.avatar = result.data.relativePathName;
				$scope.staffInfo.userInfo.avatarTemp = result.data.visitedUrl;
				$scope.cancelAvatar();
			} else {
				$rootScope.message(result.msg);
			}
			$scope.hideLoading("#staffAvatar");
		}));
	};
	// endimgCrop===================================================================

	// 文件上传
	$scope.upload = function (uploadFile, fileList) {
		var $files = [];
		var isArray = $.isArray(uploadFile);
		var tempFile = null;
		if (!isArray && uploadFile) {
			if (fileList.length >= 1) {
				if (uploadFile.name == fileList[0].fileName) {
					$rootScope.message(uploadFile.name + ' is exist!');
					return;
				}
				tempFile = fileList[0];
				$scope.removeFile(0, fileList);
			}
			$files.push(uploadFile);
		} else {
			$files = uploadFile;
		}
		if ($files == null || $files.length == 0) {
			return;
		}
		var length = $files.length;
		var msg = $scope.vlidateFileFormat($files);
		if (msg) {
			if (!isArray && tempFile)
				fileList.push(tempFile);
			$rootScope.message(msg);
			return;
		}
		for (var i = 0; i < length; i++) {
			var file = $files[i];
			if (file != null) {
				if (file.size < 10485760) {
					var fileLength = fileList.length;
					var fileObj = {};
					fileObj.fileName = file.name;
					fileObj.progress = 0;
					fileList.push(fileObj);

					fileService.uploadFileProgress(file, fileLength, callbackUploadFile, fileList).then(angular.bind(this, function then() {
						var result = fileService.fdata;
						if (result.code == 0) {
							for (var i = 0, m = fileList.length; i < m; i++) {
								if (fileList[i].fileName == result.data.fileName) {
									fileList[i].progress = 100;
									fileList[i] = result.data;
									return;
								}
							}
						} else {
							$rootScope.message(result.msg);
							for (var i = 0, m = fileList.length; i < m; i++) {
								if (fileList[i].fileName == result.data.fileName) {
									fileList.splice(i, 1);
									return;
								}
							}
						}
					}));
				} else {
					$rootScope.message('Sorry, the filesize is too large, please reduce the filesize and try again.');
				}
			}
		}
	};

	function callbackUploadFile(fileList, index, progressPercentage) {
		if (progressPercentage == 100 && index != undefined) {
			progressPercentage = 98;
		}
		if (fileList[index].progress != undefined) {
			fileList[index].progress = progressPercentage;
		}
	}
	$scope.vlidateFileFormat = function ($files) {
		var result = "";
		for (var i = 0; i < $files.length; i++) {
			var fileName = $files[i].name;
			var suffix = fileName.split('.').pop().toLowerCase();
			if (".jpg,.jpeg,.png,.gif,.pdf,.doc,.docx,.xls,.xlsx,".indexOf(suffix + ',') < 0) {
				result = "Please ensure your file is either a .pdf, ,doc, ,docx, ,xlsx, .xls, .png, .jpg or .gif file type.";
			}
		}
		return result;
	};

	// 下载附件
	$scope.download = function (file) {
		$window.location.href = "./common/download.do?fileRelativeIdName=" + file.relativePathName + "&fileName=" + encodeURIComponent(file.fileName);
	};
	// 删除附件
	$scope.removeFile = function (index, fileList) {
		fileList.splice(index, 1);
	};

	// loading
	$scope.showLoading = function (tag) {
		$(tag).mymask('');
	}
	$scope.hideLoading = function (tag) {
		if ($(tag).myisMasked()) {
			$(tag).myunmask();
		}
	}

	// 关闭模态框
	$scope.cancel = function () {
		$scope.confirm('Close', 'Exit this page?', 'OK', function () {
			$uibModalInstance.dismiss('cancel');
		}, 'NO');
	};
	$scope.cancelConfirm = function () {
		$uibModal.open({
			templateUrl: 'views/user/staff/exit.html?res_v=' + $scope.res_v,
			controller: 'confirmCtrl',
			windowClass: "modal-index",
			backdrop: 'static',
			keyboard: false,
			resolve: {
				flag: function () {
					return -1;
				}
			}
		}).result.then(function (flag) {
			if (!flag) {
				return;
			}
			$uibModalInstance.dismiss('cancel');
		});
	}

	$scope.changeValidate = function () {
		$timeout(function () {
			var tabId = $("#myindate").parents('.tab-pane').attr('id');
			if (tabId) {
				// 搜索菜单
				var $icon = $('#employmentTab');
				// 如果当前tabs有验证不通过的，则给当前菜单加class
				if ($("#" + tabId).find("small[data-bv-result='INVALID']").length > 0) {
					$icon.addClass('my-validate');
				} else {
					$icon.removeClass('my-validate');
				}
			}
		}, 100);
	};


	// 动态验证
	$scope.ev = function () {
		$('#staffForm').bootstrapValidator('addField', 'contactName', {
			validators: {
				notEmpty: {
					message: 'An emergency contact name is required.'
				},
				stringLength: {
					max: 255,
					message: "Emergency contact must be less than 255 characters long"
				}
			}
		});
		$('#staffForm').bootstrapValidator('addField', 'contactAddress', {
			validators: {
				stringLength: {
					max: 255,
					message: "The contact address must be less than 255 characters long"
				}
			}
		});
		$('#staffForm').bootstrapValidator('addField', 'contactSuburb', {
			validators: {
				stringLength: {
					max: 255,
					message: "The suburb must be less than 255 characters long"
				}
			}
		});
		$('#staffForm').bootstrapValidator('addField', 'contactPostCode', {
			validators: {
				stringLength: {
					max: 50,
					message: "The postcode must be less than 50 characters long"
				}
			}
		});
		$('#staffForm').bootstrapValidator('addField', 'contactPhone', {
			validators: {
				stringLength: {
					max: 50,
					message: "The phone number must be less than 50 characters long"
				}
			}
		});
		$('#staffForm').bootstrapValidator('addField', 'contactRelation', {
			validators: {
				stringLength: {
					max: 255,
					message: "The relation must be less than 255 characters long"
				}
			}
		});
	}
	$scope.ev2 = function () {
		$('#staffForm').bootstrapValidator('addField', 'qualObtained', {
			validators: {
				stringLength: {
					max: 255,
					message: "The Qualification obtained must be less than 255 characters long"
				}
			}
		});
		$('#staffForm').bootstrapValidator('addField', 'courseName', {
			validators: {
				stringLength: {
					max: 255,
					message: "The Course Name must be less than 255 characters long"
				}
			}
		});
		$('#staffForm').bootstrapValidator('addField', 'checkNum', {
			validators: {
				stringLength: {
					max: 255,
					message: "The Working with Children's Check Number must be less than 255 characters long"
				}
			}
		});

		/*
		 * $('#staffForm').bootstrapValidator('addField', 'completedDate', {
		 * validators : { date : { format : 'DD/MM/YYYY', message : 'The value
		 * is not a valid date' } } });
		 * $('#staffForm').bootstrapValidator('addField', 'expiryDate', {
		 * validators : { date : { format : 'DD/MM/YYYY', message : 'The value
		 * is not a valid date' } } });
		 */
	}
	// Employ动态验证
	$scope.checkEmploy = function () {
		try {
			if ($('#center').hasClass('ng-hide')) {
				$('#staffForm').bootstrapValidator('updateStatus', 'centerId', 'NOT_VALIDATED');
				$('#staffForm').bootstrapValidator('removeField', 'centerId');
			} else {
				var val = $scope.getValueByRoleId($scope.employ.firstRole.roleId);
				if (val != 9) {
					$('#staffForm').bootstrapValidator('addField', 'centerId', {
						validators: {
							notEmpty: {
								message: "The Centre for this staff member is required."
							}
						}
					});
				}
			}
			if ($('#room').hasClass('ng-hide')) {
				$('#staffForm').bootstrapValidator('updateStatus', 'roomId', 'NOT_VALIDATED');
				$('#staffForm').bootstrapValidator('removeField', 'roomId');
			} else {
				$('#staffForm').bootstrapValidator('addField', 'roomId', {
					validators: {
						notEmpty: {
							message: "The Room for this staff member is required."
						}
					}
				});
			}
			// if ($('#group').hasClass('ng-hide')) {
			// 	$('#staffForm').bootstrapValidator('updateStatus', 'groupId', 'NOT_VALIDATED');
			// 	$('#staffForm').bootstrapValidator('removeField', 'groupId');
			// } else {
			// 	$('#staffForm').bootstrapValidator('addField', 'groupId', {
			// 		validators: {
			// 			notEmpty: {
			// 				message: "The Group for this staff member is required."
			// 			}
			// 		}
			// 	});
			// }
			if ($('#selectRegion').hasClass('ng-hide')) {
				$('#staffForm').bootstrapValidator('updateStatus', 'region', 'NOT_VALIDATED');
				$('#staffForm').bootstrapValidator('removeField', 'region');
			} else {
				$('#staffForm').bootstrapValidator('addField', 'region', {
					validators: {
						choice: {
							min: 1,
							message: "The Region for this staff member is required."
						}
					}
				});
			}
		} catch (e) {

		}
	}

	// 选项卡切换
	$scope.showTab = function (tab) {
		$('.tab-pane,.profile-tab-selection>li').removeClass('active');
		$('#' + tab + 'Tab').addClass('active');
		$('#' + tab).addClass('active');
	};


	//gfwang
	$scope.getProfileByUserId = function () {
		var userId = $scope.staffInfo.userInfo.id;
		commonService.getProfileSelectList(1).then(angular.bind(this, function then() {
			var result = commonService.profileListResult;
			if (result.code == 0) {
				$scope.profileSelectList = result.data;
				commonService.getProfileInfo(userId).then(angular.bind(this, function then() {
					var result = commonService.profileResult;
					if (result.code == 0) {
						if (!result.data) {
							$scope.staffInfo.code = 0;
							return;
						}
						var profileId = result.data.id;
						var name = result.data.templateName;
						$scope.staffInfo.profileId = profileId;
						$scope.staffInfo.code = !result.data.instanceId ? 0 : result.data.instanceId;
						$scope.staffInfo.valueId = !result.data.valueId ? 0 : result.data.valueId;
						//加载form
						$scope.staffInfo.initForm($scope.staffInfo.code, $scope.staffInfo.valueId, $scope.staffInfo);
						if (result.data.deleteFlag == 1 || result.data.state == 0 || result.data.currtenFlag == 0) {
							$scope.profileSelectList.push({
								id: profileId,
								text: name,
								disabled: true
							});
						}
					} else {
						$scope.message(result.msg);
					}
				}));
			} else {
				$scope.message(result.msg);
			}
		}));
	};
	// end gfwang
	$scope.selectChange = function () {
		if (!$scope.staffInfo.profileId) {
			return;
		}
		var userId = $scope.staffInfo.userInfo.id;
		commonService.getProfileInfo(userId, $scope.staffInfo.profileId).then(angular.bind(this, function then() {
			var result = commonService.profileResult;
			if (result.data) {
				$scope.staffInfo.code = !result.data.instanceId ? 0 : result.data.instanceId;
				var valueId = !result.data.valueId ? 0 : result.data.valueId;
				$scope.staffInfo.initForm($scope.staffInfo.code, valueId, $scope.staffInfo);
			} else {
				commonService.selectChange($scope.staffInfo.profileId).then(angular.bind(this, function then() {
					var result = commonService.selectChangeResult;
					$scope.staffInfo.initForm(result.data.instanceId, 0, $scope.staffInfo);
				}));
			}
		}));

	};

	$scope.staffTypeOption = [{
		"value": 0,
		"text": "Full Time"
	}, {
		"value": 1,
		"text": "Part Time"
	}, {
		"value": 2,
		"text": "Casual"
	}];

	$scope.addlogObj = {};
	$scope.addFlag = false;
	$scope.addLog = function (userId) {
		$scope.addlogObj.userId = userId;
		$scope.addFlag = true;
		familyService.saveLog($scope.addlogObj).then(angular.bind(this, function then() {
			var result = familyService.saveResult;
			if (result.code == 0) {
				$scope.getLogList(userId);
				$scope.addlogObj = {};
				$scope.addFlag = false;
			} else {
				$rootScope.message(result.msg);
				$scope.addFlag = false;
			}

		}));
	};

	$scope.getLogList = function (userId) {
		if (!userId) {
			userId = $scope.userId;
		}
		$timeout(function () {
			familyService.getLogList(userId).then(angular.bind(this, function then() {
				var result = familyService.logResult;
				if (result.code == 0) {
					$scope.logList = result.data;
				} else {
					$rootScope.message(result.msg);
				}
			}));
		}, 200)
	};

	$scope.saveFlag = false;
	$scope.saveLog = function (id, userId) {
		var reason = $("#editReason" + id).val();
		var date = $("#editDate" + id).val();
		var logObj = "";
		angular.forEach($scope.logList, function (logg) {
			if (id == logg.id) {
				logObj = angular.copy(logg);
			}
		});
		logObj.reason = reason;
		logObj.date = date;
		logObj.userId = userId;
		$scope.saveFlag = true;
		familyService.saveLog(logObj).then(angular.bind(this, function then() {
			var result = familyService.saveResult;
			if (result.code == 0) {
				$scope.getLogList(userId);
				$scope.saveFlag = false;
			} else {
				$rootScope.message(result.msg);
				$scope.saveFlag = false;
			}
		}));
	};

	$scope.deleteLog = function (id, userId) {
		familyService.deleteLog(id).then(angular.bind(this, function then() {
			var result = familyService.delResult;
			if (result.code == 0) {
				$scope.getLogList(userId);
			} else {
				$rootScope.message(result.msg);
			}

		}));
	};

	$scope.removeLogFile = function (log) {
		if (log) {
			$timeout(function () {
				angular.forEach($scope.logList, function (logg) {
					if (log.id == logg.id) {
						logg.file = undefined;
						logg.attachId = null;
					}
				});
			});
		} else {
			$scope.addlogObj.file = undefined;
		}

	};

	$scope.uploadFile = function ($files, logId) {
		if ($files == null) {
			return;
		}
		var file = $files[0];
		if (file == undefined) {
			return;
		}
		var AllImgExt = ".doc|.docx|.pdf|.jpg|.png|.gif|.xls|.xlsx|.tiff|";
		var extName = file.name.substring(file.name.lastIndexOf(".")).toLowerCase(); //（把路径中的所有字母全部转换为小写）          
		if (AllImgExt.indexOf(extName + "|") == -1) {
			$rootScope.message("Please ensure your file is a pdf,doc,docx,xlsx,xls,png,jpg,gif,tiff file type.");
			return false;
		}
		if (file.size < 10485760) {
			$scope.showLoading("#uploadFile");
			fileService.uploadFile(file).then(angular.bind(this, function then() {
				var result = fileService.fdata;
				if (result.code == 0) {
					if (logId) {
						angular.forEach($scope.logList, function (logg) {
							if (logId == logg.id) {
								logg.file = result.data;
								logg.fileName = file.name;
							}
						});
					} else {
						$scope.addlogObj.file = result.data;
						$scope.addlogObj.fileName = file.name;
					}
				} else {
					$rootScope.message(result.msg);
				}
				$scope.hideLoading("#uploadFile");
			}));
		} else {
			$rootScope.message('Sorry, the filesize is too large, please reduce the filesize and try again.');
			$scope.hideLoading("#uploadFile");
		}
	};

	$scope.openEmploymentModal = function (isAdd, employ) {
		$uibModal.open({
			templateUrl: 'views/user/staff/staff_employment_modal.html?res_v=' + $scope.res_v,
			controller: 'staffEmploymentCtrl',
			windowClass: "modal-index",
			size: 'lg',
			backdrop: 'static',
			keyboard: false,
			resolve: {
				employ: function () {
					return angular.copy(employ);
				},
				roles: function () {
					return roles;
				},
				centerFilter: function () {
					return $scope.centerFilter;
				},
				accountId: function () {
					return $scope.staffInfo.accountInfo.id;
				},
				staffTypeOption: function () {
					return $scope.staffTypeOption;
				},
				callback: function () {
					return $scope.reload;
				},
				currUser: function () {
					return $scope.currUser;
				},
				isAdd: function () {
					return isAdd;
				}
			}
		})
	}

	$scope.reload = function () {
		$scope.getStaffInfo(userId, null);
	}

	$scope.cancelEmploy = function (id) {
		staffService.cancelEmploy(id).then(function () {
			var result = staffService.cancelEmployResult;
			if (result.code == 0) {
				$scope.reload();
			}
		})
	}

	$scope.$watch('employ.firstRole.roleId', function (newValue, oldValue) {
		if (newValue == oldValue || !$scope.addStaff) {
			return;
		}
		for (var i = 0; i < $scope.roles.roleInfos.length; i++) {
			if (newValue === $scope.roles.roleInfos[i].id && ($scope.roles.roleInfos[i].value == 0 || $scope.roles.roleInfos[i].value == 1)) {
				if ($scope.staffInfo.employInfo.monday) {
					$scope.staffInfo.employInfo.monHours = 7.6;
				}
				if ($scope.staffInfo.employInfo.tuesday) {
					$scope.staffInfo.employInfo.tueHours = 7.6;
				}
				if ($scope.staffInfo.employInfo.wednesday) {
					$scope.staffInfo.employInfo.wedHours = 7.6;
				}
				if ($scope.staffInfo.employInfo.thursday) {
					$scope.staffInfo.employInfo.thuHours = 7.6;
				}
				if ($scope.staffInfo.employInfo.friday) {
					$scope.staffInfo.employInfo.friHours = 7.6;
				}
				$scope.dealTotalHours();
			}
			if (newValue === $scope.roles.roleInfos[i].id && ($scope.roles.roleInfos[i].value == 2 || $scope.roles.roleInfos[i].value == 9)) {
				if ($scope.staffInfo.employInfo.monday) {
					$scope.staffInfo.employInfo.monHours = 0;
				}
				if ($scope.staffInfo.employInfo.tuesday) {
					$scope.staffInfo.employInfo.tueHours = 0;
				}
				if ($scope.staffInfo.employInfo.wednesday) {
					$scope.staffInfo.employInfo.wedHours = 0;
				}
				if ($scope.staffInfo.employInfo.thursday) {
					$scope.staffInfo.employInfo.thuHours = 0;
				}
				if ($scope.staffInfo.employInfo.friday) {
					$scope.staffInfo.employInfo.friHours = 0;
				}
				$scope.dealTotalHours();
			}
		}
	});

	$scope.$watch('staffInfo.userInfo.staffType', function (newValue, oldValue) {
		if (newValue == oldValue || newValue == null || !$scope.addStaff) {
			return;
		}

		if (newValue === 0) {
			if ($scope.staffInfo.employInfo.monday) {
				$scope.staffInfo.employInfo.monHours = 7.6;
			}
			if ($scope.staffInfo.employInfo.tuesday) {
				$scope.staffInfo.employInfo.tueHours = 7.6;
			}
			if ($scope.staffInfo.employInfo.wednesday) {
				$scope.staffInfo.employInfo.wedHours = 7.6;
			}
			if ($scope.staffInfo.employInfo.thursday) {
				$scope.staffInfo.employInfo.thuHours = 7.6;
			}
			if ($scope.staffInfo.employInfo.friday) {
				$scope.staffInfo.employInfo.friHours = 7.6;
			}
			$scope.dealTotalHours();
		} else {
			if ($scope.staffInfo.employInfo.monday) {
				$scope.staffInfo.employInfo.monHours = 0;
			}
			if ($scope.staffInfo.employInfo.tuesday) {
				$scope.staffInfo.employInfo.tueHours = 0;
			}
			if ($scope.staffInfo.employInfo.wednesday) {
				$scope.staffInfo.employInfo.wedHours = 0;
			}
			if ($scope.staffInfo.employInfo.thursday) {
				$scope.staffInfo.employInfo.thuHours = 0;
			}
			if ($scope.staffInfo.employInfo.friday) {
				$scope.staffInfo.employInfo.friHours = 0;
			}
			$scope.dealTotalHours();
		}
	});

	$scope.dealTotalHours = function () {
		$scope.totalHours = 0;
		if ($scope.staffInfo.employInfo.monday) {
			var monHours = $scope.staffInfo.employInfo.monHours;
			if (monHours == '') {
				$scope.totalHours = $scope.totalHours;
			} else {
				$scope.totalHours = $scope.totalHours + parseFloat(monHours);
			}
		}
		if ($scope.staffInfo.employInfo.tuesday) {
			var tueHours = $scope.staffInfo.employInfo.tueHours;
			if (tueHours == '') {
				$scope.totalHours = $scope.totalHours;
			} else {
				$scope.totalHours = $scope.totalHours + parseFloat(tueHours);
			}
		}
		if ($scope.staffInfo.employInfo.wednesday) {
			var wedHours = $scope.staffInfo.employInfo.wedHours;
			if (wedHours == '') {
				$scope.totalHours = $scope.totalHours;
			} else {
				$scope.totalHours = $scope.totalHours + parseFloat(wedHours);
			}
		}
		if ($scope.staffInfo.employInfo.thursday) {
			var thuHours = $scope.staffInfo.employInfo.thuHours;
			if (thuHours == '') {
				$scope.totalHours = $scope.totalHours;
			} else {
				$scope.totalHours = $scope.totalHours + parseFloat(thuHours);
			}
		}
		if ($scope.staffInfo.employInfo.friday) {
			var friHours = $scope.staffInfo.employInfo.friHours;
			if (friHours == '') {
				$scope.totalHours = $scope.totalHours;
			} else {
				$scope.totalHours = $scope.totalHours + parseFloat(friHours);
			}
		}
		$scope.totalHours = $scope.totalHours.toFixed(2);
	}


	$scope.$watch('staffInfo.employInfo.monday', function (newValue, oldValue) {
		if (newValue == oldValue || !$scope.addStaff) {
			return;
		}
		if (newValue) {
			$scope.staffInfo.employInfo.monStartTime = '6:00 AM';
			$scope.staffInfo.employInfo.monEndTime = '7:00 PM';
			$scope.staffInfo.employInfo.monHours = $scope.getHoursByChoose();
		} else {
			$scope.staffInfo.employInfo.monStartTime = null;
			$scope.staffInfo.employInfo.monEndTime = null;
			$scope.staffInfo.employInfo.monHours = null;
		}
		$scope.dealTotalHours();
	});

	$scope.$watch('staffInfo.employInfo.tuesday', function (newValue, oldValue) {
		if (newValue == oldValue || !$scope.addStaff) {
			return;
		}
		if (newValue) {
			$scope.staffInfo.employInfo.tueStartTime = '6:00 AM';
			$scope.staffInfo.employInfo.tueEndTime = '7:00 PM';
			$scope.staffInfo.employInfo.tueHours = $scope.getHoursByChoose();
		} else {
			$scope.staffInfo.employInfo.tueStartTime = null;
			$scope.staffInfo.employInfo.tueEndTime = null;
			$scope.staffInfo.employInfo.tueHours = null;
		}
		$scope.dealTotalHours();
	});

	$scope.$watch('staffInfo.employInfo.wednesday', function (newValue, oldValue) {
		if (newValue == oldValue || !$scope.addStaff) {
			return;
		}
		if (newValue) {
			$scope.staffInfo.employInfo.wedStartTime = '6:00 AM';
			$scope.staffInfo.employInfo.wedEndTime = '7:00 PM';
			$scope.staffInfo.employInfo.wedHours = $scope.getHoursByChoose();
		} else {
			$scope.staffInfo.employInfo.wedStartTime = null;
			$scope.staffInfo.employInfo.wedEndTime = null;
			$scope.staffInfo.employInfo.wedHours = null;
		}
		$scope.dealTotalHours();
	});

	$scope.$watch('staffInfo.employInfo.thursday', function (newValue, oldValue) {
		if (newValue == oldValue || !$scope.addStaff) {
			return;
		}
		if (newValue) {
			$scope.staffInfo.employInfo.thuStartTime = '6:00 AM';
			$scope.staffInfo.employInfo.thuEndTime = '7:00 PM';
			$scope.staffInfo.employInfo.thuHours = $scope.getHoursByChoose();
		} else {
			$scope.staffInfo.employInfo.thuStartTime = null;
			$scope.staffInfo.employInfo.thuEndTime = null;
			$scope.staffInfo.employInfo.thuHours = null;
		}
		$scope.dealTotalHours();
	});

	$scope.$watch('staffInfo.employInfo.friday', function (newValue, oldValue) {
		if (newValue == oldValue || !$scope.addStaff) {
			return;
		}
		if (newValue) {
			$scope.staffInfo.employInfo.friStartTime = '6:00 AM';
			$scope.staffInfo.employInfo.friEndTime = '7:00 PM';
			$scope.staffInfo.employInfo.friHours = $scope.getHoursByChoose();
		} else {
			$scope.staffInfo.employInfo.friStartTime = null;
			$scope.staffInfo.employInfo.friEndTime = null;
			$scope.staffInfo.employInfo.friHours = null;
		}
		$scope.dealTotalHours();
	});

	$scope.getHoursByChoose = function () {
		if (!$scope.employ.firstRole.roleId) {
			return 0;
		}
		for (var i = 0; i < $scope.roles.roleInfos.length; i++) {
			if ($scope.employ.firstRole.roleId === $scope.roles.roleInfos[i].id && ($scope.roles.roleInfos[i].value == 0 || $scope.roles.roleInfos[i].value == 1 | $scope.roles.roleInfos[i].value == 5)) {
				return 7.6;
			}
		}
		if ($scope.staffInfo.userInfo.staffType == null) {
			return 0;
		}
		if ($scope.staffInfo.userInfo.staffType == 0) {
			return 7.6;
		}
		return 0;
	}

}
angular.module('kindKidsApp').controller(
	'userStaffEditCtrl',
	['$scope', '$rootScope', '$timeout', '$filter', '$window', '$uibModal', 'reloadCallBack', '$uibModalInstance', 'commonService', 'staffService', 'fileService', 'familyService', 'userId', 'roles', 'menu',
		'currUser', userStaffEditCtrl
	]);