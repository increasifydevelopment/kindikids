'use strict';

function termConditionCtrl($scope, $uibModalInstance, staffService) {

	$scope.agreen = function() {
		// 保存green记录
		staffService.agree().then(angular.bind(this, function then() {

		}));
		$uibModalInstance.dismiss('cancel');
	};
	$scope.remove = function(){
		
	};

};

angular.module('kindKidsApp').controller('termConditionCtrl', [ '$scope', '$uibModalInstance', 'staffService', termConditionCtrl ]);