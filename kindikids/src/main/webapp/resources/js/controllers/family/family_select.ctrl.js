'use strict';

function familySelectCtrl($scope, $rootScope, $timeout, $window, $uibModal, $uibModalInstance, userId, callbackReload) {
    $scope.model = {
        userId: userId,
        familyId: null
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.submit = function () {
        if ($scope.model.familyId == null || $scope.model.familyId == undefined) {
            $rootScope.message("Please select the family.");
            return;
        }
        $uibModalInstance.dismiss('cancel');
        callbackReload($scope.model);
    }

}
angular.module('kindKidsApp').controller('familySelectCtrl', ['$scope', '$rootScope', '$timeout', '$window', '$uibModal', '$uibModalInstance', 'userId', 'callbackReload', familySelectCtrl]);