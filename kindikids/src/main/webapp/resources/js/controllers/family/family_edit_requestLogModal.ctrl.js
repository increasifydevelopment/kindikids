'use strict';

/**
 * @author gfwang
 * @description requestlogModalCtr
 */
function requestlogModalCtr($scope, $rootScope, $timeout, $window,$uibModalInstance, $filter, familyService, requestLogVos,callbackFunction,functions,childInfo) {
	$scope.requestLogVos=requestLogVos;
	$scope.loadAll=true;
	$scope.changeAttendanceCallback=callbackFunction;
	$scope.functions=functions;
	$scope.childInfo=childInfo;

	$scope.cancel = function() {
		$uibModalInstance.dismiss('cancel');
	};
}
angular.module('kindKidsApp').controller(
		'requestlogModalCtr',
		[ '$scope', '$rootScope', '$timeout', '$window', '$uibModalInstance', '$filter', 'familyService', 'requestLogVos','callbackFunction','functions','childInfo', requestlogModalCtr ]);