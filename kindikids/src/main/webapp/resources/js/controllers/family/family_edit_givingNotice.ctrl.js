'use strict';

/**
 * @author zhengguo
 * @description 离园申请单
 */
function givingNoticeCtrl($scope, $rootScope, $timeout, $window, $uibModalInstance, $filter, givingNoticeService) {

	$scope.givingNoticeInfo = {};
	/**
	 * @author zhengguo
	 * @description
	 * @returns 提交离园申请
	 */
	$scope.isApprove = true;
	$scope.isParent = true;
	$scope.initGivingNotice = function () {
		$scope.isParent = $filter('haveRole')($scope.currentUser.roleInfoVoList, '10');
		// 初始化signature
		var wrapper5 = document.getElementById("signature-pad-5");
		var clearButton5 = wrapper5.querySelector("[data-action=clear5]");
		var canvas5 = wrapper5.querySelector("canvas");
		$scope.signaturePad5;

		var wrapper6 = document.getElementById("signature-pad-6");
		var clearButton6 = wrapper6.querySelector("[data-action=clear6]");
		var canvas6 = wrapper6.querySelector("canvas");
		$scope.signaturePad6;

		$scope.resizeCanvas = function (canvas) {
			$timeout(function () {
				var ratio = Math.max(window.devicePixelRatio || 1, 1);
				canvas.width = canvas.offsetWidth * ratio;
				canvas.height = canvas.offsetHeight * ratio;
				canvas.getContext("2d").scale(ratio, ratio);
			});
		};
		$scope.resizeCanvas(canvas5);

		$scope.canvasUrl5 = canvas5.toDataURL();
		$scope.signaturePad5 = new SignaturePad(canvas5);
		clearButton5.addEventListener("click", function (event) {
			$scope.signaturePad5.clear();
		});
		$scope.resizeCanvas(canvas6);
		$scope.signaturePad6 = new SignaturePad(canvas6);
		clearButton6.addEventListener("click", function (event) {
			$scope.signaturePad6.clear();
		});

		$(function () {
			$('.modal').on('shown.bs.modal', function (e) {
				$scope.resizeCanvas(canvas5);
				$scope.resizeCanvas(canvas6);
			});
		});

		if (!$scope.givingNoticeId) {
			// 初始化人名
			var cfirstName = !$scope.childInfo.userInfo.firstName ? "" : $scope.childInfo.userInfo.firstName;
			var cmiddleName = !$scope.childInfo.userInfo.middleName ? "" : $scope.childInfo.userInfo.middleName;
			var clastName = !$scope.childInfo.userInfo.lastName ? "" : $scope.childInfo.userInfo.lastName;
			$scope.givingNoticeInfo.childName = cfirstName + ' ' + cmiddleName + ' ' + clastName;
			if ($scope.parentList.length != 0) {
				var parentFirst = !$scope.parentList[0].firstName ? "" : $scope.parentList[0].firstName;
				var parentMiddle = !$scope.parentList[0].middleName ? "" : $scope.parentList[0].middleName;
				var parentLast = !$scope.parentList[0].lastName ? "" : $scope.parentList[0].lastName;
				$scope.givingNoticeInfo.parentName = parentFirst + ' ' + parentMiddle + ' ' + parentLast;
			}

			$scope.givingNoticeInfo.monday = $scope.childInfo.attendance.monday;
			$scope.givingNoticeInfo.tuesday = $scope.childInfo.attendance.tuesday;
			$scope.givingNoticeInfo.wednesday = $scope.childInfo.attendance.wednesday;
			$scope.givingNoticeInfo.thursday = $scope.childInfo.attendance.thursday;
			$scope.givingNoticeInfo.friday = $scope.childInfo.attendance.friday;

			$scope.givingNoticeInfo.lodgedDate = moment().format('DD/MM/YYYY');
			$scope.isEdit = true;
			$scope.isApprove = false;
			return;
		}
		// 获取离园
		givingNoticeService.getGivingInfo($scope.givingNoticeId).then(
			angular.bind(this, function then() {
				var result = givingNoticeService.givingInfoResult;
				if (result.code == 0) {
					$scope.givingNoticeInfo = result.data;
					var childCenterId = $scope.givingNoticeInfo.centerId;
					// Last Day after 3 Full Weeks
					// (ceo可以编辑和园长及二级园长只能编辑自己园区的)
					if (($scope.isCeoRole || ($scope.isCenterManager && $scope.currentUser.userInfo.centersId == childCenterId)) &&
						$scope.givingNoticeInfo.state == 0) {
						$scope.isEdit = true;
					}
				} else {
					$rootScope.message(result.msg);
				}
			}));
	};

	$scope.save = function (isApprove) {
		$('#givingNoticeForm').data('bootstrapValidator').validate();
		if (!$('#givingNoticeForm').data('bootstrapValidator').isValid()) {
			return;
		}
		if (!isApprove && !validateDate()) {
			$scope.confirm('Warning', 'Sorry, you can not request a leave within 3 weeks. This will be reviewed by Centre Managers. Are you sure you want to continue?', 'Yes', function () {
				confirmPaln();
			}, 'No', null);
			return;
		}
		confirmPaln();

	};

	function confirmPaln() {
		if ($scope.isParent) {
			$scope.saveSure();
			return;
		}
		$scope.confirm('Confirm', 'Is this change made for the Position Plan of the Next Year?', 'YES', function () {
			$scope.givingNoticeInfo.isPlan = true;
			$scope.saveSure();
		}, 'NO', function () {
			$scope.givingNoticeInfo.isPlan = false;
			$scope.saveSure();
		});
	}

	$scope.saveSure = function () {
		// 设置签名
		$scope.setPad();
		$scope.givingNoticeInfo.childId = $scope.childInfo.accountInfo.id;
		givingNoticeService.applyGivingNotice($scope.givingNoticeInfo).then(angular.bind(this, function then() {
			var result = givingNoticeService.applyResult;
			if (result.code == 0 || result.code == 2) {
				$scope.givingNoticeInfo = result.data;
				$uibModalInstance.dismiss('cancel');
				$scope.reloadParent();
				if (result.code == 2) {
					$scope.$parent.callbackArchive($scope.childInfo.userInfo.id);
				}
			}
			$rootScope.message(result.msg);
		}));
	};

	function get3WeekDate() {
		return moment().add(21, 'days').format('DD/MM/YYYY');
	}
	/**
	 * 验证离园日期是否大于等于 当前时间+3周
	 */
	function validateDate() {
		var chooseDate = $scope.givingNoticeInfo.lastDate;
		var choose = new moment(chooseDate, 'DD/MM/YYYY')._d;
		var after3Weel = moment(get3WeekDate(), 'DD/MM/YYYY')._d;
		return choose >= after3Weel;
	}
	/**
	 * 刷新
	 */
	$scope.reloadParent = function () {
		// 刷新log
		$scope.$parent.changeAttendanceCallback($scope.childInfo.userInfo.id, 4);
		if ($scope.$parent.isHaveRequestGivingNotice) {
			// 回调父页面的获取是否有request的请求 gfwang
			$scope.$parent.isHaveRequestGivingNotice($scope.childInfo.accountInfo.id);
		}
	};

	/**
	 * 设置 签名
	 */
	$scope.setPad = function () {
		$scope.givingNoticeInfo.parentSignatureId = $scope.signaturePad5.isEmpty() ? "" : $scope.signaturePad5.toDataURL();
		$scope.givingNoticeInfo.staffSignatureId = $scope.signaturePad6.isEmpty() ? "" : $scope.signaturePad6.toDataURL();
	};
	/**
	 * 获取签名
	 */
	$scope.getPad = function () {
		$scope.signaturePad7.fromDataURL($scope.givingNoticeInfo.parentSignatureId);
		$scope.signaturePad6.fromDataURL($scope.givingNoticeInfo.bankParentSignatureId);
		$scope.signaturePad5.fromDataURL($scope.givingNoticeInfo.staffSignatureId);
	};
	$scope.cancel = function () {
		$scope.confirm('Close', 'Exit this page?', 'OK', function () {
			$uibModalInstance.dismiss('cancel');
		}, 'NO');
	};
}
angular.module('kindKidsApp').controller('givingNoticeCtrl', ['$scope', '$rootScope', '$timeout', '$window', '$uibModalInstance', '$filter', 'givingNoticeService', givingNoticeCtrl]);