'use strict';

/**
 * @author gfwang
 * @description userFamilyCtrl Controller
 */
function userFamilyCtrl($scope, $rootScope, $timeout, $window, familyService, fileService, commonService, $uibModal) {
	$rootScope.subScope = $scope;
	$scope.page = {};
	$scope.page.condition = {
		pageIndex: 1,
		readFlag: '-1',
		centerId: '',
		roomId: '',
		keyWords: '',
		myStatus: 5
	};

	$scope.functions = [];
	commonService.getFun("/user_family_list").then(angular.bind(this, function then() {
		$scope.functions = commonService.functions;
	}));


	$scope.busy = false;
	/**
	 * @author gfwang
	 * @description 回车执行翻页以及查询
	 */
	$scope.enterSearch = function (ev) {
		if (ev.keyCode == 13) {
			$rootScope.$state.go("app.main.user.family");
			$scope.page.condition.pageIndex = 1;
			$scope.page.condition.search = true;
			$scope.getList();
		}
	};
	$scope.search = function () {
		$rootScope.$state.go("app.main.user.family");
		$scope.page.condition.pageIndex = 1;
		$scope.getList();
	}
	$scope.mySearch = function () {
		$rootScope.$state.go("app.main.user.family");
		$scope.page.condition.search = true;
		$scope.page.condition.pageIndex = 1;
		$scope.getList();
	}
	$scope.getList = function (isDown) {
		if (isDown) {
			// 如果当前页面中list的个数大于，等于数据库中所有记录数，则不请求后台
			if ($scope.page.list.length >= $scope.page.condition.totalSize) {
				return;
			}
			$scope.page.condition.pageIndex += 1;
			$scope.busy = true;
		}
		$scope.showLoading("#family_list");
		familyService.getFaimilyList($scope.page.condition).then(angular.bind(this, function then() {
			var result = familyService.listResult;
			$scope.hideLoading("#family_list");
			if (result.code == 0) {
				if (result.data.list.length == 0 && !result.data.condition.search) {
					$scope.busy = false;
					return;
				}
				$scope.dealListResult(result.data, isDown);
				if (result.data.condition.search && result.data.list.length == 0) {
					$scope.confirm('Message', 'No results match your search.', 'OK', function () {
						$("#privacy-modal").modal('hide');
					});
				}
				$scope.page.condition.search = false;
			} else {
				$rootScope.message(result.msg);
			}

		}));
	};
	$scope.scorellUp = function () {
		$scope.page.condition.pageIndex = 1;
		$scope.page.condition.search = true;
		$scope.getList(false);
	};
	/**
	 * 处理查询结果
	 */
	$scope.dealListResult = function (data, isAppend) {
		// 判断是否为 追加
		if (isAppend) {
			if (!$scope.page.list) {
				$scope.page = data;
				// $scope.defaultList = data;
			} else {
				$scope.page.condition = data.condition;
				var length = data.list.length;
				for (var i = 0; i < length; i++) {
					// 向列表追加元素
					$scope.page.list.push(data.list[i]);
				}
			}
			$timeout(function () {
				$scope.busy = false;
			}, 2000);
		} else {
			$scope.page = data;
		}
	};
	$scope.openProfile = function (familyId) {
		$uibModal.open({
			templateUrl: 'views/user/family/child_edit.html?res_v=' + $scope.res_v,
			controller: 'userFamilyEditCtrl',
			animation: true,
			size: "lg",
			backdrop: 'static',
			keyboard: false,
			windowClass: "profile-popup profile-popup-family modal-align-top",
			resolve: {
				familyId: function () {
					return familyId;
				},
				functions: function () {
					return $scope.functions;
				},
				currentUser: function () {
					return $scope.currentUser;
				},
				callbackReload: function () {
					return $scope.scorellUp;
				}
			}
		});
	};

	// imgCrop======================================================================
	$scope.myImage = '';
	$scope.avatarImage = '';
	$scope.showAvatarImage = true;
	/**
	 * @author abliu
	 * @description 文件上传
	 */
	$scope.uploadAvatar = function ($file) {
		var file = $file;
		if (file == undefined) {
			return;
		}
		if (file.name) {
			$scope.showAvatarImage = false
		}
		var reader = new FileReader();
		reader.onload = function (evt) {
			$scope.$apply(function ($scope) {
				$scope.myImage = evt.target.result;
			});
		};
		reader.readAsDataURL(file);
	};

	$scope.cancelAvatar = function () {
		$scope.myImage = '';
		$scope.avatarImage = '';
		$scope.showAvatarImage = true;
	};
	$scope.saveAvatar = function (myImage) {
		// 调用fileservice中的uploadResizeImage
	};
	$scope.chooseIds = "";
	$scope.choose = function (chooseId) {
		$scope.chooseIds = chooseId;
	};
	$scope.getOrgs = function () {
		commonService.getMenuOrgs(0).then(angular.bind(this, function then() {
			$scope.orgs = commonService.orgList;

		}));
	};

	$scope.reloadPage = function () {
		if ($scope.$stateParams.familyId && $scope.$stateParams.familyId != '*') {
			$timeout(function () {
				$scope.openProfile($scope.$stateParams.familyId);
			});
		}
	};




}
angular.module('kindKidsApp').controller('userFamilyCtrl', ['$scope', '$rootScope', '$timeout', '$window', 'familyService', 'fileService', 'commonService', '$uibModal', userFamilyCtrl]);