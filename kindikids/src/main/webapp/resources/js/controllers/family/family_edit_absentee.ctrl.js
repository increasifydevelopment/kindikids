'use strict';

/**
 * @author zhengguo
 * @description 离园申请单
 */
function absentCtrl($scope, $rootScope, $timeout, $window, $uibModalInstance, $filter, absebteeService) {

	/**
	 * @author zhengguo
	 * @description
	 * @returns 提交离园申请
	 */
	$scope.isParent = true;
	$scope.initAbsentee = function() {
		$scope.isParent = $filter('haveRole')($scope.currentUser.roleInfoVoList, '10');
		// 初始化signature
		var wrapper3 = document.getElementById("signature-pad-3");
		var clearButton3 = wrapper3.querySelector("[data-action=parentSign]");
		var canvas3 = wrapper3.querySelector("canvas");
		$scope.signaturePad3;

		var wrapper4 = document.getElementById("signature-pad-4");
		var clearButton4 = wrapper4.querySelector("[data-action=managerSign]");
		var canvas4 = wrapper4.querySelector("canvas");
		$scope.signaturePad4;

		$scope.resizeCanvas = function(canvas) {
			$timeout(function() {
				var ratio = Math.max(window.devicePixelRatio || 1, 1);
				canvas.width = canvas.offsetWidth * ratio;
				canvas.height = canvas.offsetHeight * ratio;
				canvas.getContext("2d").scale(ratio, ratio);
			});

		};

		$scope.resizeCanvas(canvas3);
		$scope.signaturePad3 = new SignaturePad(canvas3);
		clearButton3.addEventListener("click", function(event) {
			$scope.absenteeRequestInfo.parentSignatureId = null;
			$scope.signaturePad3.clear();
		});

		$scope.resizeCanvas(canvas4);
		$scope.signaturePad4 = new SignaturePad(canvas4);
		clearButton4.addEventListener("click", function(event) {
			$scope.givingNoticeInfo.chargeSignatureId = null;
			$scope.signaturePad4.clear();
		});

		$(function() {
			$('.modal').on('shown.bs.modal', function(e) {
				$scope.resizeCanvas(canvas3);
				$scope.resizeCanvas(canvas4);
			});
		});

		if (!$scope.absenteeId) {
			$scope.absenteeRequestInfo = {};
			// 初始化人名
			var cfirstName = !$scope.childInfo.userInfo.firstName ? "" : $scope.childInfo.userInfo.firstName;
			var cmiddleName = !$scope.childInfo.userInfo.middleName ? "" : $scope.childInfo.userInfo.middleName;
			var clastName = !$scope.childInfo.userInfo.lastName ? "" : $scope.childInfo.userInfo.lastName;
			$scope.absenteeRequestInfo.childName = cfirstName + ' ' + cmiddleName + ' ' + clastName;

			if ($scope.parentList && $scope.parentList.length > 0) {
				var firstParent = $scope.parentList[0];
				var parentFirst = !firstParent.firstName ? "" : firstParent.firstName;
				var parentMiddle = !firstParent.middleName ? "" : firstParent.middleName;
				var parentLast = !firstParent.lastName ? "" : firstParent.lastName;
				$scope.absenteeRequestInfo.parentName = parentFirst + ' ' + parentMiddle + ' ' + parentLast;
			} else {
				$scope.absenteeRequestInfo.parentName = null;
			}
			absebteeService.getAbsebteeInfo('', $scope.childInfo.userInfo.id).then(angular.bind(this, function then() {
				var result = absebteeService.absebteeResult;
				if (result.code == 0) {
					$scope.absenteeRequestInfo.absentNum = result.data.absentNum;
				} else {
					$rootScope.message(result.msg);
				}
			}));
			return;
		}
		// 获取请假
		absebteeService.getAbsebteeInfo($scope.absenteeId, $scope.childInfo.userInfo.id).then(angular.bind(this, function then() {
			var result = absebteeService.absebteeResult;
			if (result.code == 0) {
				$scope.absenteeRequestInfo = result.data;
			} else {
				$rootScope.message(result.msg);
			}
		}));

	};

	/**
	 * 监控Last day at Centre
	 */
	$scope.$watch('absenteeRequestInfo.beginDate', function(newValue, oldValue) {
		if ($scope.absenteeRequestInfo && $scope.absenteeRequestInfo.id) {
			return;
		}
		if (newValue != null && $scope.absenteeRequestInfo.endDate != null) {
			$scope.absenteeRequestInfo.childId = $scope.childInfo.accountInfo.id;
			absebteeService.getAbsenteeDays($scope.absenteeRequestInfo).then(angular.bind(this, function then() {
				var result = absebteeService.absebteeInfoForDays;
				if (result.code == 0) {
					$scope.absenteeRequestInfo.absentNum = result.data.absentNum;
					$scope.absenteeRequestInfo.absentTotalNum = result.data.absentTotalNum;
				} else {
					$rootScope.message(result.msg);
				}
			}));
		}
	}, true);
	/**
	 * 监控Date Returning to the Centre
	 */
	$scope.$watch('absenteeRequestInfo.endDate', function(newValue, oldValue) {
		if ($scope.absenteeRequestInfo && $scope.absenteeRequestInfo.id) {
			return;
		}
		if (newValue != null && $scope.absenteeRequestInfo.beginDate != null) {
			$scope.absenteeRequestInfo.childId = $scope.childInfo.accountInfo.id;
			absebteeService.getAbsenteeDays($scope.absenteeRequestInfo).then(angular.bind(this, function then() {
				var result = absebteeService.absebteeInfoForDays;
				if (result.code == 0) {
					$scope.absenteeRequestInfo.absentNum = result.data.absentNum;
					$scope.absenteeRequestInfo.absentTotalNum = result.data.absentTotalNum;
				} else {
					$rootScope.message(result.msg);
				}
			}));
		}
	}, true);
	// 焦点事件触发
	$scope.changeDate = function() {
		$("#lastDayAtCentre").focus();
		$("#lastDayAtCentre").blur();
		/*
		 * $("#deturningCentre").focus(); $("#deturningCentre").blur();
		 */

	};
	/**
	 * 申请|同意
	 */
	$scope.save = function(isApprove) {
		$('#absenteeForm').data('bootstrapValidator').validate();
		if (!$('#absenteeForm').data('bootstrapValidator').isValid()) {
			return;
		}
		if (!isApprove && !isLessToday()) {
			$scope.confirm('Warning', 'Last day at Centre is less today,Are you Sure?', 'Yes', function() {
				$scope.saveSure();
			}, 'No', null);
			return;
		}
		$scope.saveSure();
	};
	// 设置防止多次提交标识
	var isSave = true;
	$scope.saveSure = function(isSure) {
		if (!isSave) {
			return;
		}
		isSave = false;
		isSure = !isSure ? false : true;
		// 设置签名
		$scope.setPad();
		$scope.absenteeRequestInfo.childId = $scope.childInfo.accountInfo.id;
		absebteeService.applyAbsebtee($scope.absenteeRequestInfo, isSure).then(angular.bind(this, function then() {
			var result = absebteeService.applyResult;
			$scope.absenteeRequestInfo.parentSignatureId = null;
			$scope.absenteeRequestInfo.chargeSignatureId = null;
			if (result.code == 0) {
				$scope.absenteeRequestInfo = result.data;
				$uibModalInstance.dismiss('cancel');
				$scope.reloadParent();
			}
			if (result.code == 1) {
				$rootScope.message(result.msg);
			}
			if (result.code == 2) {
				$scope.confirm("Confirm", result.msg, 'OK', function() {
					$scope.saveSure(true);
				}, 'NO');
			}
			isSave = true;
		}));
	};
	function isLessToday() {
		var chooseDate = $scope.absenteeRequestInfo.beginDate;
		var choose = new moment(chooseDate, 'DD/MM/YYYY')._d;
		var temp = new moment().format('DD/MM/YYYY');
		var today = new moment(temp, 'DD/MM/YYYY')._d;
		return choose >= today;
	}
	/**
	 * 刷新
	 */
	$scope.reloadParent = function() {
		// 刷新requestlog
		$scope.$parent.changeAttendanceCallback($scope.childInfo.userInfo.id, 4);
		if ($scope.$parent.isHaveRequestAbsteen) {
			// 回调父页面的获取是否有request的请求 gfwang
			$scope.$parent.isHaveRequestAbsteen($scope.childInfo.accountInfo.id);
		}

	};

	/**
	 * 设置签名字段
	 */
	$scope.setPad = function() {
		$scope.absenteeRequestInfo.parentSignatureId = $scope.signaturePad3.isEmpty() ? "" : $scope.signaturePad3.toDataURL();
		$scope.absenteeRequestInfo.chargeSignatureId = $scope.signaturePad4.isEmpty() ? "" : $scope.signaturePad4.toDataURL();
	};
	/**
	 * 获取画板值
	 */
	$scope.getPad = function() {
		$scope.signaturePad3.fromDataURL($scope.absenteeRequestInfo.parentSignatureId);
		$scope.signaturePad4.fromDataURL($scope.absenteeRequestInfo.bankParentSignatureId);
	};

	$scope.cancel = function() {
		$scope.confirm('Close', 'Exit this page?', 'OK', function() {
			$uibModalInstance.dismiss('cancel');
		}, 'NO');
	};

}
angular.module('kindKidsApp').controller('absentCtrl',
		[ '$scope', '$rootScope', '$timeout', '$window', '$uibModalInstance', '$filter', 'absebteeService', absentCtrl ]);