'use strict';

/**
 * @author abliu
 * @description requestlogCtr
 */
function requestlogCtr($scope, $rootScope, $timeout, $window, $uibModal, $filter, familyService, absebteeService, givingNoticeService,
		attendanceService) {

	/**
	 * 是否为家长
	 */
	$scope.isParent = $filter('haveRole')($rootScope.currentUser.roleInfoVoList, '10');
	/**
	 * 是否为管理员 CEO 园长 二级园长
	 */
	$scope.isManager = $filter('haveRole')($rootScope.currentUser.roleInfoVoList, '0;1;5;');

	$scope.isCEO = $filter('haveRole')($rootScope.currentUser.roleInfoVoList, '0');

	$scope.isCenterManager = $filter('haveRole')($scope.currentUser.roleInfoVoList, '1;5;');

	$scope.initPermiss = function(childInfo) {
		// 是否禁用center
		$scope.isCurrentCenterManager = true;

		if ($scope.isCenterManager) {
			var childCenter = $scope.childInfo.userInfo.centersId;
			var currtenCenter = $scope.currentUser.userInfo.centersId;
			if (childCenter != currtenCenter) {
				$scope.isCurrentCenterManager = false;
			}
		}
	};

	/**
	 * @author zhengguo
	 * @description
	 * @returns 打开Giving Notice Form 离院申请单
	 */
	$scope.givingNoticeLog = function(childInfo, id, operaType, isReadOnly) {
		var scope = $scope.$new();
		scope.childInfo = childInfo;
		scope.givingNoticeId = id;
		scope.givingNoticeStatus = !operaType ? -1 : operaType;
		scope.isReadOnly = isReadOnly;
		$uibModal.open({
			templateUrl : 'views/user/family/giving_notice.html?res_v=' + $scope.res_v,
			controller : 'givingNoticeCtrl',
			backdrop : 'static',
			keyboard : false,
			windowClass : "profile-popup",
			scope : scope
		});
	};

	// 分发离园申请，请假申请操作
	$scope.forwardOpt = function(childInfo, id, operaType) {
		if (operaType == 12) {
			$scope.givingNoticeLog(childInfo, id, operaType, true);
		} else {
			$scope.childAbsenteeLog(childInfo, id, operaType, true);
		}

	};
	var title = "Warning";
	var msgCancle = "If you cancel this request, you will lose your position in the queue.";
	var msgDecline = "If you decline this request, your existing queue position will be lost.";
	var msgCancle2 = "Are you sure you want to cancel this request?";
	var msgDecline2 = "Are you sure you want to decline this request?";

	/**
	 * 刷新
	 */
	$scope.reloadParent = function(requestType) {
		// 刷新requestlog
		$scope.changeAttendanceCallback($scope.childInfo.userInfo.id, requestType);
		// 回调父页面的获取是否有request的请求 gfwang
		if ($scope.isHaveRequestAbsteen) {
			$scope.isHaveRequestAbsteen($scope.childInfo.accountInfo.id);
		}
		if ($scope.isHaveRequestGivingNotice) {
			$scope.isHaveRequestGivingNotice($scope.childInfo.accountInfo.id);
		}
	};
	// 取消，拒绝操作
	$scope.declineOrCancleGivNotice = function(givingNoticeId, optType) {
		if (optType == 1) {
			$scope.confirm(title, msgCancle2, 'Yes', function() {
				givingNoticeService.cancelGivingNotice(givingNoticeId).then(angular.bind(this, function then() {
					var result = givingNoticeService.cancleResult;
					$rootScope.message(result.msg);
					$scope.reloadParent(4);
				}));
			}, 'No', null);
		} else {
			$scope.confirm(title, msgDecline2, 'Yes', function() {
				givingNoticeService.declineGivingNotice(givingNoticeId).then(angular.bind(this, function then() {
					var result = givingNoticeService.declineResult;
					$rootScope.message(result.msg);
					$scope.reloadParent(4);
				}));
			}, 'No', null);
		}

	};
	// 取消，拒绝操作
	$scope.declineOrCancleAbsentee = function(absenteeId, optType) {
		if (optType == 1) {
			$scope.confirm(title, msgCancle2, 'Yes', function() {
				absebteeService.cancelAbsebtee(absenteeId).then(angular.bind(this, function then() {
					var result = absebteeService.cancleResult;
					$rootScope.message(result.msg);
					$scope.reloadParent(5);
				}));
			}, 'No', null);
		} else {
			$scope.confirm(title, msgDecline2, 'Yes', function() {
				absebteeService.declineAbsebtee(absenteeId).then(angular.bind(this, function then() {
					var result = absebteeService.declineResult;
					$rootScope.message(result.msg);
					$scope.reloadParent(5);
				}));
			}, 'No', null);
		}

	};
	/**
	 * @author gfwang
	 * @description
	 * @returns 打开请假页面
	 */
	$scope.childAbsenteeLog = function(childInfo, id, operaType, isReadOnly) {
		var scope = $scope.$new();
		scope.childInfo = childInfo;
		scope.absenteeId = id;
		scope.absenteeStatus = !operaType ? -1 : operaType;
		scope.isReadOnly = isReadOnly;
		$uibModal.open({
			templateUrl : 'views/user/family/attendance_child_absentee.html?res_v=' + $scope.res_v,
			controller : 'absentCtrl',
			backdrop : 'static',
			keyboard : false,
			windowClass : "profile-popup",
			scope : scope
		});
	};

	/**
	 * @author abliu
	 * @description
	 * @returns 操作请求状态
	 */
	$scope.operateState = function(objId, state, requestlog) {
		$scope.confirm(title, msgCancle, 'Yes', function() {
			// 更新后台请求状态
			attendanceService.updateAttendanceRequest(objId, state).then(angular.bind(this, function then() {
				var result = attendanceService.updateAttendanceRequestObj;
				if (result.code == 0) {
					requestlog.state = state;
					$scope.changeAttendanceCallback($scope.childId, requestlog.type);
				} else {
					$rootScope.message(result.msg);
				}
				;
			}));
		}, 'No', null);
	};

	/**
	 * @author big
	 * @description
	 * @returns 取消waitinglist 添加
	 */
	$scope.operateStateTemporary = function(type, objId, state, requestlog) {
		$scope.confirm(title, msgCancle, 'Yes', function() {
			// 更新后台请求状态
			attendanceService.updateTemporaryRequest(type, objId, state).then(angular.bind(this, function then() {
				var result = attendanceService.updateTemporaryRequestObj;
				if (result.code == 0) {
					requestlog.state = state;
				} else {
					$rootScope.message(result.msg);
				}
				;
			}));
		}, 'No', null);
	};

	/**
	 * @author abliu
	 * @description
	 * @returns 打开编辑页面
	 */
	$scope.openRequest = function(objId, type, isRead) {
		attendanceService.getRequestLogObj(objId, type).then(angular.bind(this, function then() {
			var result = attendanceService.requestLogObj;
			if (result.code == 0) {
				$scope.openWin(objId, '', type, result.data, isRead);
			} else {
				$rootScope.message(result.msg);
			}
			;
		}));

	};

	$scope.openWin = function(objId, childId, type, childInfo, isRead) {
		// 打开不同编辑页面
		if (type == 0 || type == 1 || type == 2 || type == 3 || type == 4 || type == 5) { // 园长操作转园申请
			$uibModal.open({
				templateUrl : 'views/user/family/change_attendance.html?res_v=' + $scope.res_v,
				controller : 'userFamilyEditChangeAttendanceCtrl',
				size : "lg",
				backdrop : 'static',
				keyboard : false,
				windowClass : "profile-popup",
				resolve : {
					childInfo : function() {
						return childInfo;
					},
					childId : function() {
						return childInfo.accountInfo.id;
					},
					type : function() {
						return type;
					},
					functions : function() {
						return $scope.functions;
					},
					currentUser : function() {
						return $scope.currentUser;
					},
					callbackFunction : function() {
						return $scope.changeAttendanceCallback;
					},
					objId : function() {
						return objId;
					},
					isRead : function() {
						return isRead;
					}
				}
			});

		}
	};

}
angular.module('kindKidsApp').controller(
		'requestlogCtr',
		[ '$scope', '$rootScope', '$timeout', '$window', '$uibModal', '$filter', 'familyService', 'absebteeService', 'givingNoticeService',
				'attendanceService', requestlogCtr ]);