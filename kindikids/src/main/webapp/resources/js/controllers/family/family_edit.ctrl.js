'use strict';

/**
 * @author gfwang
 * @description userFamilyCtrl Controller
 */
function userFamilyEditCtrl($scope, $rootScope, $timeout, $window, $uibModal, $uibModalInstance, $filter, commonService, familyService, fileService,
	familyId, functions, currentUser, callbackReload, attendanceService) {
	$scope.functions = functions;
	commonService.getFun("/user_family_list").then(angular.bind(this, function then() {
		$scope.functions = commonService.functions;
	}));
	// 获取Task页面的用户权限
	commonService.getFun("/task").then(angular.bind(this, function then() {
		$scope.taskfunctions = commonService.functions;
	}));
	$rootScope.subScope = $scope;
	$scope.childList = [];
	// 当前选择编辑的
	$scope.currtenEditId = "";
	$scope.currtenIndex = -1;
	$scope.parentList = [];
	$scope.parentInfo = {};
	$scope.familyId = familyId;
	$scope.isOpenAdd = true;
	$scope.currentUser = currentUser;
	// $scope.isCreate = false;

	/**
	 * 当前编辑的类型，true小孩,false家长
	 */
	$scope.editFlag = true;
	$scope.status = 1;
	/**
	 * 编辑
	 */
	$scope.editFamily = function (editId) {
		if (editId === $scope.currtenEditId) {
			return;
		} else {
			currtenEditId = editId;
		}
	};

	function synchroData(childInfo) {
		$scope.currentCenterId = childInfo.userInfo.centersId;
		$scope.currentMonday = childInfo.attendance.monday;
		$scope.currentTuesday = childInfo.attendance.tuesday;
		$scope.currentWednesday = childInfo.attendance.wednesday;
		$scope.currentThursday = childInfo.attendance.thursday;
		$scope.currentFriday = childInfo.attendance.friday;
		$scope.currentChangeDate = childInfo.attendance.changeDate;
	}

	function handleOpenAdd() {
		$scope.isOpenAdd = true;
		for (var i = 0; i < $scope.childList.length; i++) {
			var user = $scope.childList[i];
			if (!user.userInfo.id) {
				$scope.isOpenAdd = false;
			}
		}

		for (var i = 0; i < $scope.parentList.length; i++) {
			var parent = $scope.parentList[i];
			if (!parent.id) {
				$scope.isOpenAdd = false;
			}
		}
	}

	/**
	 * 选择需要编辑的小孩
	 */
	$scope.childChange = function (childInfo, index) {
		if (childInfo.emergencySignatureInfo) {
			$scope.ngModel.value = childInfo.emergencySignatureInfo.emergencySignature;
		} else {
			$scope.ngModel.value = null;
		}
		$scope.currtenIndex = index;
		$scope.editFlag = true;
		$scope.currtenEditId = childInfo.userInfo.id;
		$scope.isArchiverStatus = childInfo.accountInfo.status == 0 ? true : false;
		$scope.isOutChildAndUse = childInfo.attendance.attendFlag;
		$scope.editFamily(childInfo.userInfo.id, true);
		$scope.getProfileByUserId(childInfo.userInfo.id, childInfo);
		$scope.taskListFun(childInfo.accountInfo.id);
		$scope.isOut = childInfo.attendance.type == 1 ? true : false;
		$scope.currtenChildCenterId = childInfo.userInfo.centersId;
		if (!childInfo.attendance.orientationTime) {
			childInfo.attendance.orientationTime = $scope.changeBackTime(childInfo.attendance.orientation);
		}
		childInfo.enrolments.forEach(function (enrolment) {
			enrolment.orientationTime = $scope.changeBackTime(enrolment.orientationDate);
		});
		if ($scope.isCenterManager && $scope.currtenChildCenterId == $scope.currentUser.userInfo.centersId) {
			$scope.isCreate = true;
		}
		synchroData(childInfo);
	};

	$scope.parentChange = function (parentId, parentIndex) {
		$scope.editFlag = false;
		$scope.currtenEditId = parentId;
		$scope.currtenIndex = parentIndex;
		$scope.editFamily(parentId);
	};
	$scope.showTab = function (tab) {
		$('.tab-pane,.profile-tab-selection>li').removeClass('active');
		$('#' + tab + 'Tab').addClass('active');
		$('#' + tab).addClass('active');
	};

	$scope.addChild = function () {
		$scope.ngModel.value = undefined;

		// 外部不给添加
		if ($scope.isOut) {
			return;
		}
		$scope.isArchiverStatus = false;
		if (!$scope.isOpenAdd) {
			$rootScope.message('Please save the first child\'s information');
			return;
		} else {
			$scope.getCurrtenColor(2);
		}
		$scope.editFlag = true;
		if (!$scope.familyId) {
			$scope.initModel();
		}
	};
	$scope.addParent = function () {
		// 外部不给添加
		if ($scope.isOut) {
			return;
		}
		if ($scope.isOpenAdd) {
			$scope.editFlag = false;
			$scope.currtenEditId = "";
			$scope.parentList.push({});
			$scope.getCurrtenColor(1);
			$scope.currtenIndex = $scope.parentList.length - 1;
			handleOpenAdd();
			// $scope.isOpenAdd = false;
		} else {
			$rootScope.message('Please save the first child\'s information');
		}
	};
	$scope.saveChild = function (childInfo) {
		var flag = false;
		$('#childForm').data('bootstrapValidator').validate();
		$("small").map(function () {
			if ($(this).attr('data-bv-result') == "INVALID") {
				flag = true;
			}
		});

		if (flag) {
			// $.messager.popup('Please check that the input is correct.');
			return;
		}

		childInfo.coverOld = false;
		childInfo.overFull = false;

		// 改变园区
		var isChangeCenter = $scope.currentCenterId != childInfo.userInfo.centersId;
		// 改变上课周期
		var isChangeMon = $scope.currentMonday != childInfo.attendance.monday;
		var isChangeTue = $scope.currentTuesday != childInfo.attendance.tuesday;
		var isChangeWed = $scope.currentWednesday != childInfo.attendance.wednesday;
		var isChangeThu = $scope.currentThursday != childInfo.attendance.thursday;
		var isChangeFri = $scope.currentFriday != childInfo.attendance.friday;
		// 改变入园时间
		var isChangeDate = $scope.currentChangeDate != childInfo.attendance.changeDate;
		// 非新增,未入园,改变了园区,上课周期,入园时间
		if ((childInfo.userInfo.id != '' || childInfo.userInfo.id != null) && (childInfo.attendance.enrolled == 0 && childInfo.attendance.type == 0) && (isChangeCenter || isChangeMon || isChangeTue || isChangeWed || isChangeThu || isChangeFri || isChangeDate)) {
			$scope.confirm('Confirm', 'Is this change made for the Position Plan of the Next Year?', 'YES', function () {
				childInfo.plan = 1;
				$scope.saveChildSure(childInfo);
				synchroData(childInfo);
			}, 'NO', function () {
				childInfo.plan = 2;
				$scope.saveChildSure(childInfo);
				synchroData(childInfo);
			});
			return;
		}
		childInfo.plan = 0;
		$scope.saveChildSure(childInfo);
	};
	$scope.logSourceInfo = {
		"sourceId": " "
	};

	$scope.saveChildSure = function (childInfo) {

		$scope.showLoading("#child_1");
		// 紧急联系人签名赋值
		if (!childInfo.emergencySignatureInfo) {
			childInfo.emergencySignatureInfo = new Object();
		}
		childInfo.emergencySignatureInfo.emergencySignature = $scope.ngModel.value;
		// 拼接日期
		if (childInfo.attendance.orientation) {
			childInfo.attendance.orientation = childInfo.attendance.orientation.replace(/\-/g, "/") + $scope.changeHeadTime(childInfo.attendance.orientationTime);
		}
		if (childInfo.getJson) {
			childInfo.json = JSON.stringify(childInfo.getJson());
		}

		familyService.saveChild(childInfo, $scope.familyId, $scope.familyName).then(angular.bind(this, function then() {
			var result = familyService.saveChildResult;
			$scope.hideLoading("#child_1");
			if (result.code == 0) {
				$scope.familyId = result.data.userInfo.familyId;
				result.data.attendance.orientationTime = $scope.changeBackTime(result.data.attendance.orientation);
				$scope.childInfoCopy(childInfo, result.data);
				$scope.currtenEditId = result.data.userInfo.id;
				handleOpenAdd();
				// $scope.isOpenAdd = true;
				callbackReload();
				$scope.logSourceInfo.sourceId = "";
				$timeout(function () {
					$scope.logSourceInfo.sourceId = childInfo.userInfo.id;
				}, 200);
				// abliu
				$scope.initAvatar = childInfo.userInfo.avatar;

				if (childInfo.saveSuccess) {
					childInfo.saveSuccess();
				}
				$scope.getProfileByUserId(childInfo.userInfo.id, childInfo);

				if (childInfo.accountInfo.status == 0) {
					$scope.isArchiverStatus = true;
				}
				// $scope.$state.reload();
				$rootScope.message(result.msg);
			} else if (result.code == 3) {
				$rootScope.confirm('', result.msg, 'Confirm', function () {
					childInfo.coverOld = true;
					$scope.saveChildSure(childInfo);
				}, 'Cancel', null);
			} else if (result.code == 11) {
				$rootScope.confirm('', result.msg, 'Confirm', function () {
					childInfo.overFull = true;
					$scope.saveChildSure(childInfo);
				}, 'Cancel', null);
			} else {
				$rootScope.message(result.msg);
			}
		}));
	};

	// 数据拷贝
	$scope.childInfoCopy = function (childInfo, data) {
		objectClone(childInfo.userInfo, data.userInfo);
		objectClone(childInfo.accountInfo, data.accountInfo);
		objectClone(childInfo.background, data.background);
		objectClone(childInfo.custodyArrange, data.custodyArrange);
		objectClone(childInfo.dietaryRequire, data.dietaryRequire);
		objectClone(childInfo.healthMedical, data.healthMedical);
		objectClone(childInfo.medical, data.medical);
		objectClone(childInfo.attendance, data.attendance);
		childInfo.emergencyContactList = listClone(data.emergencyContactList);
		childInfo.healthMedicalDetailList = listClone(data.healthMedicalDetailList);
		childInfo.requestLogVos = listClone(data.requestLogVos);
		childInfo.emailMsgList = listClone(data.emailMsgList);
		// +++++++++++++++++++++++++++++++++++++++++++++++
		$scope.requestLogVos.obj = data.requestLogVos;
		// +++++++++++++++++++++++++++++++++++++++++++++++
		// childInfo.attendanceHistoryInfos =
		// objectClone(data.attendanceHistoryInfos);
		objectClone(childInfo.attendanceHistoryInfos, data.attendanceHistoryInfos);
		// childInfo.attendanceHistoryInfos.futureAttendance =
		// listClone(data.attendanceHistoryInfos.futureAttendance);
	};
	/**
	 * 保存家长
	 */
	$scope.saveParent = function (parentInfo) {
		var flag = false;
		$('#parentForm').data('bootstrapValidator').validate();
		$("small").map(function () {
			if ($(this).attr('data-bv-result') == "INVALID") {
				flag = true;
			}
		});
		if (flag) {
			return;
		}
		$scope.saveParentSure(parentInfo);

	};

	$scope.saveParentSure = function (parentInfo) {
		$scope.showLoading("#parent_1");
		familyService.saveParent(parentInfo, $scope.familyId, $scope.familyName).then(angular.bind(this, function then() {
			var result = familyService.saveParentResult;
			$scope.hideLoading("#parent_1");
			if (result.code == 0) {
				$scope.currtenEditId = result.data.id;
				objectClone(parentInfo, result.data);
				// parentInfo.id = result.data.id;
				// $scope.isOpenAdd = true;
				handleOpenAdd();
				callbackReload();
				$scope.logSourceInfo.sourceId = "";
				$timeout(function () {
					$scope.logSourceInfo.sourceId = parentInfo.id;
				}, 200);
			}
			$rootScope.message(result.msg);
		}));
	};

	function objectClone(obj1, obj2) {
		if (obj1 == null) {
			obj1 = {};
		}
		for (var p in obj2) {
			var name = p; // 属性名称
			try {
				obj1[name] = obj2[p];
			} catch (e) { }
		}
	}

	/**
	 * add by big
	 */
	function listClone(source) {
		var target = new Array();
		var index = 0;
		for (var obj in source) {
			target[index] = {};
			objectClone(target[index], source[obj]);
			index++;
		}
		return target;
	}

	/**
	 * 归档小孩
	 */
	$scope.archiveChild = function (childInfo) {
		if (childInfo.attendance.leaveDate == null || childInfo.attendance.leaveDate == '' || childInfo.attendance.leaveDate == undefined) {
			$scope.confirm('Archive', 'This child is currently Active. You will need to enter the Last Day before Archiving.', 'No', null);
			return;
		}

		$scope.confirm('Archive', 'Are you sure you want to archive this child?', 'Yes', function () {
			familyService.archiveChild(childInfo).then(angular.bind(this, function then() {
				var result = familyService.aresult;
				if (result.code == 0) {
					$scope.$state.reload();
					childInfo.accountInfo.status = 0;
					$scope.currtenEditId = $scope.currtenEditId;
					$scope.isArchiverStatus = true;
					$scope.changeAttendanceCallback(childInfo.userInfo.id);
					callbackReload();
				}
				$rootScope.message(result.msg);
			}));
		}, 'No', null);
	};
	/**
	 * 取消 小孩
	 */
	$scope.unArchiveChild = function (childInfo) {
		$scope.confirm('Unarchive', 'Please do not unarchive this child if you are re-enrolling, you will need to move them to the Sibling or External waitlist.', 'Unarchive', function () {
			familyService.unArchiveChild(childInfo.userInfo.id).then(angular.bind(this, function then() {
				var result = familyService.uaresult;
				if (result.code == 0) {
					$scope.$state.reload();
					childInfo.accountInfo.status = 1;
					$scope.currtenEditId = $scope.currtenEditId;
					childInfo.attendance.leaveDate = null;
					$scope.isArchiverStatus = false;
					$scope.changeAttendanceCallback(childInfo.userInfo.id);
					callbackReload();
				}
				$rootScope.message(result.msg);
			}));
		}, 'Cancel', null);
	};

	$scope.sureDelete = false;
	/**
	 * 删除 type=0小孩
	 */
	$scope.deleteUser = function (type) {
		var msg = type == 0 ? 'Are you sure you want to delete this child?' : 'Are you sure to delete this parent?'
		$scope.confirm('Delete', msg, 'Yes', function () {
			if (type == 0) {
				if (!$scope.currtenEditId) {
					$scope.currtenEditId = $scope.childList[0].userInfo.id;
					$scope.currtenIndex = 0;
					$scope.editFlag = true;
					// $scope.isOpenAdd = true;
					// 如果删除未保存的，则直接删除最后一个小孩
					$scope.childList.splice($scope.childList.length - 1, 1);
					if ($scope.childList.length == 0) {
						$uibModalInstance.dismiss('cancel');
					}
					handleOpenAdd();
					return;
				}
				$scope.sureDelete = false;
				$scope.deleteUserAction();
			} else {
				if (!$scope.currtenEditId) {
					$scope.parentList.splice($scope.parentList.length - 1, 1);
					$scope.currtenEditId = $scope.childList[0].userInfo.id;
					$scope.currtenIndex = 0;
					$scope.editFlag = true;
					// $scope.isOpenAdd = true;
					handleOpenAdd();
					return;
				}
				familyService.deleteParent($scope.currtenEditId).then(angular.bind(this, function then() {
					var result = familyService.dpresult;
					if (result.code == 0) {
						// 删除后默认选择第一个小孩
						$scope.parentList.splice($scope.currtenIndex, 1);
						$scope.currtenEditId = $scope.childList[0].userInfo.id;
						$scope.currtenIndex = 0;
						$scope.editFlag = true;
						// $scope.isOpenAdd = true;
						handleOpenAdd();
						callbackReload();
					} else {
						$rootScope.message(result.msg);
					}

				}));
			}
		}, 'No', null);
	};

	$scope.deleteUserAction = function () {
		familyService.deleteChild($scope.currtenEditId, $scope.sureDelete).then(angular.bind(this, function then() {
			var result = familyService.dcresult;
			if (result.code == 0) {
				// 删除后默认选择第一个小孩
				$scope.childList.splice($scope.currtenIndex, 1);
				callbackReload();
				if ($scope.childList.length == 0) {
					$uibModalInstance.dismiss('cancel');
					return;
				} else {
					$scope.currtenEditId = $scope.childList[0].userInfo.id;
					$scope.currtenIndex = 0;
					$scope.isOut = $scope.childList[0].attendance.type == 1 ? true : false;
				}
				$scope.editFlag = true;
				// $scope.isOpenAdd = true;
				handleOpenAdd();
			} else if (result.code == 2) {
				$rootScope.confirm('', result.msg, 'Confirm', function () {
					$scope.sureDelete = true;
					$scope.deleteUserAction();
				}, 'Cancel', null);
			} else {
				$rootScope.message(result.msg);
			};
		}));
	}

	/**
	 * 发送邮件
	 */
	$scope.sendWelEmail = function (parentId) {
		familyService.sendWelEmail(parentId).then(angular.bind(this, function then() {
			var result = familyService.eresult;
			$rootScope.message(result.msg);
		}));
	};
	var isSubmit = true;
	/**
	 * 新增紧急联系人
	 */
	$scope.addConcat = function (childInfo) {
		isSubmit = false;
		childInfo.emergencyContactList.push({});
		$timeout(function () {
			isSubmit = true;
		}, 200);

	};
	/**
	 * 删除紧急联系人
	 */
	$scope.deleteConcat = function (index, childInfo) {
		childInfo.emergencyContactList.splice(index, 1);
		$timeout(function () {
			$scope.validateTag($("#firstName").parents('.tab-pane'));
		}, 100);

	};
	// 解决删除联系人，tab验证红色不去除问题
	$scope.validateTag = function ($tabPane) {
		var tabId = $tabPane.attr('id');
		if (tabId) {
			// 搜索菜单
			var $icon = $('a[href="/#' + tabId + '"][data-toggle="tab"]');
			// 如果当前tabs有验证不通过的，则给当前菜单加class
			if ($("#" + tabId).find("small[data-bv-result='INVALID']").length > 0) {
				$icon.addClass('my-validate');
			} else {
				$icon.removeClass('my-validate');
			}
		}
	};

	/**
	 * 新增人
	 */
	$scope.addPerson = function (childInfo) {
		isSubmit = false;
		childInfo.background.personList.push({});
		$timeout(function () {
			isSubmit = true;
		}, 200);
	};

	// TODO
	$scope.personChange = function (po, value) {
		// 如果为Undefiend，则初始化（1个都没有，编辑会出现这样的情况）
		if (!po.personList) {
			po.personList = [];
		}
		$scope.removeAll(po.personList, true);
		if (value && po.personList.length <= 0) {
			po.personList.push({});
		}
		backgroundValidate();
	};

	function backgroundValidate() {
		$timeout(function () {
			$scope.validateTag($(".backgrounditem").parents('.tab-pane'));
		}, 100);
	}
	$scope.removeAll = function (list, isPerson) {
		for (var i = list.length - 1; i >= 0; i--) {
			var value = "";
			if (isPerson) {
				value = list[i].name;
			} else {
				value = list[i].petName;
			}
			if (value) {
				continue;
			}
			list.splice(i, 1);
		}
	};

	/**
	 * 移除人
	 */
	$scope.removrePerson = function (index, childInfo) {
		childInfo.background.personList.splice(index, 1);
		if (!childInfo.background.personList || childInfo.background.personList.length == 0) {
			childInfo.background.childBackgroundInfo.sameHousehold = false;
		}
		backgroundValidate();
	};
	/**
	 * 新增宠物
	 */
	$scope.addPets = function (childInfo) {
		isSubmit = false;
		childInfo.background.petList.push({});
		$timeout(function () {
			isSubmit = true;
		}, 200);
	};
	$scope.petsChange = function (po, value) {
		// 如果为Undefiend，则初始化（1个都没有，编辑会出现这样的情况）
		if (!po.petList) {
			po.petList = [];
		}
		$scope.removeAll(po.petList, false);
		if (value && po.petList.length <= 0) {
			po.petList.push({});
		}
		backgroundValidate();
	};
	/**
	 * 移除宠物
	 */
	$scope.removePets = function (index, childInfo) {
		childInfo.background.petList.splice(index, 1);
		if (!childInfo.background.petList || childInfo.background.petList.length == 0) {
			childInfo.background.childBackgroundInfo.childHavePets = false;
		}
		backgroundValidate();
	};
	$scope.cancel = function () {
		$scope.confirm('Close', 'Exit this page?', 'OK', function () {
			$uibModalInstance.dismiss('cancel');
		}, 'NO');
	};

	/**
	 * 打开gallery页面，将小孩的信息直接带过去，存在已知问题，修改头像后带过去是最新的
	 */
	$scope.initAvatar = "";
	$scope.setInitAvatar = function (childInfo) {
		$scope.initAvatar = childInfo.userInfo.avatar;
	};
	$scope.openGallery = function (childInfo) {
		if (childInfo.accountInfo.id == null || childInfo.accountInfo.id == "") {
			$rootScope.message("Please save first !");
			return;
		}
		$rootScope.galleryCondition.childId = childInfo.accountInfo.id;
		if (childInfo.userInfo.avatarTemp != undefined) {
			$rootScope.galleryCondition.avatar = childInfo.userInfo.avatarTemp;
		} else {
			$rootScope.galleryCondition.avatar = childInfo.userInfo.avatar;
		}
		$rootScope.galleryCondition.avatar = $scope.initAvatar;
		$rootScope.galleryCondition.personColor = childInfo.userInfo.personColor;
		$rootScope.galleryCondition.name = childInfo.userInfo.fullName;

		$rootScope.$state.go("app.main.event.gallerylist");
		$uibModalInstance.dismiss('cancel');
	};
	// imgCrop======================================================================
	$scope.myImage = '';
	$scope.avatarImage = '';
	$scope.showAvatarImage = true;
	$scope.avatarfile = null;
	/**
	 * @author abliu
	 * @description 文件上传
	 */
	$scope.uploadAvatar = function ($file) {
		var file = $file;
		if (file == undefined) {
			return;
		}
		var isImagemSG = $rootScope.isImage($file.name);
		if (isImagemSG) {
			$rootScope.message(isImagemSG);
			return



		}
		if (file.name) {
			$scope.showAvatarImage = false;
		}
		var reader = new FileReader();
		reader.onload = function (evt) {
			$scope.$apply(function ($scope) {
				$scope.myImage = evt.target.result;
				$scope.avatarfile = file;
			});
		};
		reader.readAsDataURL(file);
	};

	$scope.cancelAvatar = function () {
		$scope.myImage = '';
		$scope.avatarImage = '';
		$scope.avatarName = '';
		$scope.showAvatarImage = true;
	};
	$scope.saveAvatar = function (myImage, childInfo) {
		$scope.showLoading("#childAvatar");
		fileService.uploadResizeImage('', myImage, $scope.avatarfile.name).then(angular.bind(this, function then() {
			var result = fileService.fdata;
			if (result.code == 0) {
				childInfo.userInfo.avatar = result.data.relativePathName;
				childInfo.userInfo.avatarTemp = result.data.visitedUrl;
				$scope.cancelAvatar();
			} else {
				$rootScope.message(result.msg);
			}
			$scope.hideLoading("#childAvatar");

		}));
		// 调用fileservice中的uploadResizeImageo(￣▽￣)d
	};
	/**
	 * 删除附件
	 */
	$scope.removeFile = function (index, fileList) {
		fileList.splice(index, 1);
	};
	/**
	 * 文件上传
	 */
	$scope.upload = function (uploadFile, fileList) {
		var $files = [];
		var isArray = $.isArray(uploadFile);
		var tempFile = null;
		// 如果是不是数组（单文件）
		if (!isArray && uploadFile) {
			if (fileList.length >= 1) {
				if (uploadFile.name == fileList[0].fileName) {
					$rootScope.message(uploadFile.name + ' is exist!');
					return;
				}
				tempFile = fileList[0];
				$scope.removeFile(0, fileList);
			}
			$files.push(uploadFile);
		} else {
			// 多附件上传时 判断是否重复附件
			var repeatFileNames = $scope.isRepeat(fileList, uploadFile)
			if (repeatFileNames) {
				$rootScope.message(repeatFileNames + ' is exist!');
				return;
			}
			$files = uploadFile;
		}
		if ($files == null || $files.length == 0) {
			return;
		}
		var length = $files.length;
		var msg = $scope.vlidateFileFormat($files);
		if (msg) {
			// 如果为单文件则还回来
			if (!isArray && tempFile)
				fileList.push(tempFile);
			$rootScope.message(msg);
			return;
		}
		for (var i = 0; i < length; i++) {
			var file = $files[i];
			if (file != null) {
				if (file.size < 10485760) {
					var fileLength = fileList.length;
					var fileObj = {};
					fileObj.fileName = file.name;
					fileObj.progress = 0;
					fileList.push(fileObj);
					fileService.uploadFileProgress(file, fileLength, callbackUploadFile, fileList).then(angular.bind(this, function then() {
						var result = fileService.fdata;
						if (result.code == 0) {
							for (var i = 0, m = fileList.length; i < m; i++) {
								if (fileList[i].fileName == result.data.fileName) {
									fileList[i].progress = 100;
									fileList[i] = result.data;
									return;
								}
							}
						} else {
							for (var i = 0, m = fileList.length; i < m; i++) {
								if (fileList[i].fileName == result.data.fileName) {
									fileList.splice(i, 1);
									return;
								}
							}
							$rootScope.message(result.msg);
						}
					}));
				} else {
					$rootScope.message('Sorry, the filesize is too large, please reduce the filesize and try again.');
				}
			}
		}
	};
	$scope.vlidateFileFormat = function ($files) {
		var result = "";
		for (var i = 0; i < $files.length; i++) {
			var fileName = $files[i].name;
			var suffix = fileName.split('.').pop().toLowerCase();
			if (".jpg,.jpeg,.png,.pdf,.doc,.docx,.xls,.xlsx,.gif,".indexOf(suffix + ',') < 0) {
				result = "Please ensure your file is either a .pdf, ,doc, ,docx, ,xlsx, .xls, .png, .jpg or .gif file type.";
			}
		}
		return result;
	};
	$scope.isRepeat = function (oldFileList, $files) {
		var result = "";
		for (var i = 0; i < oldFileList.length; i++) {
			var oldname = oldFileList[i].fileName;
			for (var j = 0; j < $files.length; j++) {
				var newname = $files[j].name;
				if (oldname == newname) {
					result += oldname + ",";
				}
			}
		}
		if (result) {
			result = result.substring(0, result.length - 1);
		}
		return result;
	};

	function callbackUploadFile(fileList, index, progressPercentage) {
		if (progressPercentage == 100 && index != undefined) {
			progressPercentage = 98;
		}
		if (fileList[index].progress != undefined) {
			fileList[index].progress = progressPercentage;
		}
	}

	/**
	 * 下载文件
	 */
	$scope.download = function (file) {
		$window.location.href = "./common/download.do?fileRelativeIdName=" + file.relativePathName + "&fileName=" + encodeURIComponent(file.fileName);
	};

	/**
	 * 是否外来的
	 */
	$scope.isOut = false;
	/**
	 * 初始化列表
	 */
	$scope.initModel = function () {
		$scope.isCEO = $filter('haveRole')($scope.currentUser.roleInfoVoList, '0;');
		$scope.isAdminToAttend = $filter('haveRole')($scope.currentUser.roleInfoVoList, '1;5;');
		$scope.isParent = $filter('haveRole')($scope.currentUser.roleInfoVoList, '10');

		$scope.ngModel = new Object();
		$scope.ngModel.value;
		$scope.ngModel.signaturePad;

		if ($scope.familyId) {
			$scope.pushEmptyChild();
			$scope.isOpenAdd = true;
			familyService.getFamilyInfo($scope.familyId).then(angular.bind(this, function then() {
				var result = familyService.familys;
				if (result.code == 0) {
					$scope.familyName = result.data.familyName;
					$scope.childList = result.data.childList;
					$scope.childList[0].attendance.orientationTime = $scope.changeBackTime(result.data.childList[0].attendance.orientation);
					$scope.childList[0].enrolments.forEach(function (enrolment) {
						enrolment.orientationTime = $scope.changeBackTime(enrolment.orientationDate);
					});
					$scope.currtenEditId = $scope.childList[0].userInfo.id;
					$scope.currtenIndex = 0;
					$scope.isArchiverStatus = $scope.childList[0].accountInfo.status == 0 ? true : false;
					$scope.parentList = result.data.parentList;
					$scope.isOut = $scope.childList[0].attendance.type == 1 ? true : false;
					$scope.currtenChildCenterId = $scope.childList[0].userInfo.centersId;
					$scope.ngModel.value = $scope.childList[0].emergencySignatureInfo.emergencySignature;
					// 园长登录判断该孩子是否为自己园区
					if ($scope.isCenterManager && $scope.currtenChildCenterId == $scope.currentUser.userInfo.centersId) {
						$scope.isCreate = true;
					}
					if (!$scope.isFirstParent()) {
						$scope.disableFamilyName = true;
					}
					// 外部小孩是能在 外部小孩下拉框中被选中
					$scope.isOutChildAndUse = $scope.childList[0].attendance.attendFlag;

					synchroData($scope.childList[0]);

				} else {
					$rootScope.message(result.msg);
				}
			}));
		} else {
			$scope.getCurrtenColor(2);
		}
	};

	$scope.initPermiss = function (childInfo) {
		// 是否禁用center
		$scope.isDisableCenter = false;
		$scope.isShowOpt = true; // 是否是当前园
		var isCenterManager = $filter('haveRole')($scope.currentUser.roleInfoVoList, '1;5;');
		if (isCenterManager) {
			var childCenter = childInfo.userInfo.centersId;
			var currtenCenter = $scope.currentUser.userInfo.centersId;
			if (childCenter != currtenCenter && !$scope.isOut) {
				$scope.isShowOpt = false;
			} else {
				$scope.isDisableCenter = true;
			}
		}
		// (functions | haveFunction:'family_archive')||
		// (functions | haveFunction:'family_unarchive')||
		// (functions | haveFunction:'family_save_child')||(functions |
		// haveFunction:'family_delete_child'))
		$timeout(function () {
			$scope.haveArchive = $filter('haveFunction')($scope.functions, 'family_archive');
			$scope.haveUnarchive = $filter('haveFunction')($scope.functions, 'family_unarchive');
			$scope.haveSaveChild = $filter('haveFunction')($scope.functions, 'family_save_child');
			$scope.haveDeleteChild = $filter('haveFunction')($scope.functions, 'family_delete_child');
		}, 100);


	};

	/**
	 * 获取颜色
	 */
	$scope.getCurrtenColor = function (userType) {
		commonService.getNextColor($scope.familyId, userType).then(angular.bind(this, function then() {
			var result = commonService.nextResult;
			if (result.code == 0) {
				var color = result.msg;
				if (userType == 2) {
					$scope.pushEmptyChild(color);
					$scope.currtenIndex = $scope.childList.length - 1;
				} else {
					var parent = {
						personColor: color
					};
					$scope.parentList[$scope.parentList.length - 1] = parent;
				}

			}
		}));

	};
	$scope.pushEmptyChild = function (color) {

		var isCenterManager = $filter('haveRole')($scope.currentUser.roleInfoVoList, '1;5;');
		var centerId = "";
		// 园长新增的时候只能新增当前园
		if (isCenterManager) {
			centerId = $scope.currentUser.userInfo.centersId;
		}
		var childInfo = {
			userInfo: {
				fileList: [],
				centersId: centerId,
				personColor: color
			},
			accountInfo: {},
			custodyArrange: {
				custodyParentId: 'notApplicable',
				fileList: []
			},
			emergencyContactList: [],
			healthMedical: {
				fileList: []
			},
			healthMedicalDetailList: [{
				nameIndex: 0
			}, {
				nameIndex: 1
			}, {
				nameIndex: 2
			}, {
				nameIndex: 3
			}, {
				nameIndex: 4
			}, {
				nameIndex: 5
			}, {
				nameIndex: 6
			}, {
				nameIndex: 7
			}, {
				nameIndex: 8
			}, {
				nameIndex: 9
			}, {
				nameIndex: 10
			}, {
				nameIndex: 11
			}, {
				nameIndex: 12
			}],
			background: {
				childBackgroundInfo: {},
				petList: [],
				personList: []
			},
			dietaryRequire: {
				fileList: []
			},
			medical: {
				fileList: []
			},
			attendance: {
				type: 0,
				monday: false,
				tuesday: false,
				wednesday: false,
				thursday: false,
				friday: false,
				changeDate: undefined
			},
			attendanceHistoryInfos: {
				attendanceHistoryGroupVos: [],
				futureAttendance: []
			}
		};
		childInfo.emergencyContactList.push({});
		// childInfo.background.personList.push({});
		// childInfo.background.petList.push({});
		$scope.childList.push(childInfo);
		$scope.currtenEditId = "";
		$scope.isOpenAdd = false;
		$scope.logList = [];
	};

	$scope.showLoading = function (tag) {
		$(tag).mymask('');
	};
	$scope.hideLoading = function (tag) {
		if ($(tag).myisMasked()) {
			$(tag).myunmask();
		}
	};

	/**
	 * ng-repeat 循环结束
	 */
	$scope.ev = function (type) {
		$timeout(function () {
			var form = $('#childForm').bootstrapValidator();
			if (type == 0) {

				form.bootstrapValidator('addField', 'concatName', {
					validators: {
						notEmpty: {
							message: "An Authorised/ Emergency contact is required"
						},
						stringLength: {
							max: 255,
							message: "The Name must be less than 255 characters long"
						}
					}
				});
				form.bootstrapValidator('addField', 'concatAddress', {
					validators: {
						stringLength: {
							max: 255,
							message: "The Address must be less than 255 characters long"
						}
					}
				});
				form.bootstrapValidator('addField', 'concatSuburb', {
					validators: {
						stringLength: {
							max: 255,
							message: "The Suburb must be less than 255 characters long"
						}
					}
				});
				form.bootstrapValidator('addField', 'concatPostCode', {
					validators: {
						stringLength: {
							max: 50,
							message: "The PostCode must be less than 50 characters long"
						},
						digits: {
							message: 'The PostCode can only consist of a number'
						}
					}
				});
				form.bootstrapValidator('addField', 'concatHomePhone', {
					validators: {
						stringLength: {
							max: 50,
							message: "The Home Phone number must be less than 50 characters long"
						}
					}
				});
				form.bootstrapValidator('addField', 'concatWorkPhone', {
					validators: {
						stringLength: {
							max: 50,
							message: "The Work Phone number must be less than 50 characters long"
						}
					}
				});
				form.bootstrapValidator('addField', 'concatMobilePhone', {
					validators: {
						stringLength: {
							max: 50,
							message: "The Mobile Phone number must be less than 50 characters long"
						}
					}
				});
			} else if (type == 1) {
				form.bootstrapValidator('addField', 'personName', {
					validators: {
						notEmpty: {
							message: "An Authorised/ Emergency contact is required"
						},
						stringLength: {
							max: 255,
							message: "The Name must be less than 255 characters long"
						}
					}
				});
				form.bootstrapValidator('addField', 'personSurname', {
					validators: {
						stringLength: {
							max: 255,
							message: "The Surname must be less than 255 characters long"
						}
					}
				});
				form.bootstrapValidator('addField', 'personRela', {
					validators: {
						stringLength: {
							max: 50,
							message: "The Relationship must be less than 50 characters long"
						}
					}
				});

			} else if (type == 2) {
				form.bootstrapValidator('addField', 'petName', {
					validators: {
						notEmpty: {
							message: "An Authorised/ Emergency contact is required"
						},
						stringLength: {
							max: 50,
							message: "The Name must be less than 255 characters long"
						}
					}
				});
				form.bootstrapValidator('addField', 'petType', {
					validators: {
						stringLength: {
							max: 50,
							message: "The Type must be less than 255 characters long"
						}
					}
				});
			}
		}, 200);
	};

	$scope.centersList = [];
	$scope.roomList = [];
	$scope.groupList = [];

	$scope.$watch('currtenEditId', function (newValue, oldValue) {
		familyService.getFamilyOrgs($scope.currtenEditId).then(angular.bind(this, function then() {
			var result = familyService.orgsResult;
			$scope.centersList = result.code.centersInfos;
			$scope.roomList = result.code.roomInfos;
			$scope.groupList = result.code.groups;
		}));
	}, true);
	/**
	 * 是否影藏取消按钮
	 */
	$scope.isClearDownload = false;
	$scope.$watch('isArchiverStatus', function (newValue, oldValue) {
		$timeout(function () {
			$scope.outChildAchived == null;
			$scope.isAchive(newValue, oldValue);
		}, 100);

	}, true);
	$scope.isAchive = function (newValue, oldValue) {
		if (!newValue) {
			$scope.enabledForm();
			return;
		}
		$scope.disableForm();
	};

	$scope.$watch('isOutChildAndUse', function (newValue, oldValue) {
		console.log('isOutChildAndUse is change');
		$timeout(function () {
			if (!$scope.isOut) {
				$scope.outChildAchived = false;
				$scope.enabledForm();
				$scope.isClearDownload = false;
				return;
			}
			if (!newValue) {
				// 外部小孩被归档
				$scope.outChildAchived = true;
				$scope.disableForm();
				$scope.isClearDownload = true;
			} else {
				$scope.outChildAchived = false;
				$scope.enabledForm();
				$scope.isClearDownload = false;
			}
		}, 100);

	}, true);
	/**
	 * 启用禁用
	 */
	$scope.disableForm = function () {
		var tabs = new Array("#childBasic_1", "#childBackground_1", "#childDietary_1", "#childBasic_1", "#childMedical_1");
		for (var i = 0; i < tabs.length; i++) {
			var tabId = tabs[i];
			$(tabId).find("input").attr("disabled", 'disable');
			$(tabId).find("select").attr("disabled", 'disable');
			$(tabId).find("textarea").attr("disabled", 'disable');
			$(tabId).find("button").attr("disabled", 'disable');
		}

	};
	/**
	 * 清除禁用
	 */
	$scope.enabledForm = function () {
		var tabs = new Array("#childBasic_1", "#childBackground_1", "#childDietary_1", "#childBasic_1", "#childMedical_1");
		for (var i = 0; i < tabs.length; i++) {
			var tabId = tabs[i];
			$(tabId).find("input").removeAttr("disabled");
			$(tabId).find("select").removeAttr("disabled");
			$(tabId).find("textarea").removeAttr("disabled");
			$(tabId).find("button").removeAttr("disabled");
		}
	};
	// endimgCrop===================================================================

	// changeAttendance=============================================================

	$scope.enrolChild = function (childInfo) {
		familyService.enrolChildService(childInfo.userInfo.id, childInfo.accountInfo.id).then(angular.bind(this, function then() {
			var result = familyService.enrolResult;
			if (result.code == 0) {
				childInfo.attendance.type = 0;
				$scope.isOut = false;
				childInfo.accountInfo.status = 1;
				$scope.isArchiverStatus = false;
				callbackReload();
			}
			$rootScope.message(result.msg);
		}));
	};

	/**
	 * @author abliu
	 * @description
	 * @returns 打开更改changeAttendance type=0:家长申请 type=1:园长申请转园 type=2:CEO、园长直接更改
	 *          type=3:园长操作转园申请 type=6:外部小孩入园
	 */
	$scope.openChangeAttendance = function (childInfo, childId, type) {
		var modalInstance = $uibModal.open({
			templateUrl: 'views/user/family/change_attendance.html?res_v=' + $scope.res_v,
			controller: 'userFamilyEditChangeAttendanceCtrl',
			size: "lg",
			backdrop: 'static',
			keyboard: false,
			windowClass: "profile-popup",
			resolve: {
				childInfo: function () {
					return childInfo;
				},
				childId: function () {
					return childId;
				},
				type: function () {
					return type;
				},
				functions: function () {
					return $scope.functions;
				},
				currentUser: function () {
					return $scope.currentUser;
				},
				callbackFunction: function () {
					return $scope.changeAttendanceCallback;
				},
				objId: function () {
					return "";
				},
				isRead: function () {
					return false;
				}
			}
		});
	};

	/**
	 * @author abliu
	 * @description
	 * @returns changeAttendanceCallback
	 */
	$scope.changeAttendanceCallback = function (childId, type) {
		$uibModalInstance.dismiss('cancel');
		// 更新Atendance的内容
		// 更新最新request内容
		familyService.getAttendance(childId).then(angular.bind(this, function then() {
			var result = familyService.attendanceObj;
			if (result.code == 0) {
				for (var i = 0, m = $scope.childList.length; i < m; i++) {
					if ($scope.childList[i].userInfo.id == result.data.attendanceInfo.userId) {
						// 替换属性

						$scope.childList[i].attendance = result.data.attendanceInfo;
						$scope.childList[i].attendance.orientationTime = $scope.changeBackTime(result.data.attendanceInfo.orientation);
						$scope.childList[i].userInfo.centersId = result.data.userInfo.centersId;
						$scope.childList[i].userInfo.roomId = result.data.userInfo.roomId;
						$scope.childList[i].userInfo.groupId = result.data.userInfo.groupId;
						$scope.childList[i].attendanceHistoryInfos = result.data.historys;
						$scope.childList[i].requestLogVos = result.data.requestLogs;
						$scope.childList[i].accountInfo = result.data.accountInfo;
						$scope.childList[i].emailMsgList = result.data.emailMsgList;
						if (type == 6) {
							callbackReload();
							$scope.isOut = false;
						}
						// 园长approved转园申请,将孩子更改为当前园
						if (type == 3) {
							$scope.isShowOpt = true; // 是否是当前园
						}

						$scope.requestLogVos.obj = result.data.requestLogs;
						break;
					}
				}
			}
		}));
		// $scope.childList
	};
	// 时间为今天，且直接同意，直接把小孩状态改成archive
	$scope.callbackArchive = function (childId) {
		for (var i = 0; i < $scope.childList.length; i++) {
			var tmpChildId = $scope.childList[i].userInfo.id;
			if (tmpChildId === childId) {
				$scope.childList[i].accountInfo.status = 0;
				$scope.isArchiverStatus = true;
			}
		}
	};
	// 控制request列表是否加载全部内容
	$scope.loadAll = false;
	$scope.requestLogVos = {
		"obj": {}
	};
	/**
	 * @author abliu
	 * @description
	 * @returns 打开请求历史记录
	 */
	$scope.openRequestLog = function (requestLogVos, childInfo) {
		$scope.requestLogVos.obj = requestLogVos;
		$uibModal.open({
			templateUrl: 'views/user/family/request_log_modal.html?res_v=' + $scope.res_v,
			controller: 'requestlogModalCtr',
			backdrop: 'static',
			keyboard: false,
			windowClass: "profile-popup",
			resolve: {
				requestLogVos: function () {
					return $scope.requestLogVos;
				},
				functions: function () {
					return $scope.functions;
				},
				childInfo: function () {
					return childInfo;
				},
				callbackFunction: function () {
					return $scope.changeAttendanceCallback;
				}
			}
		});
	};

	/**
	 * @author abliu
	 * @description
	 * @returns 打开attendancelist历史记录
	 */
	$scope.openAttendanceLog = function (attendanceHistoryInfos) {
		$uibModal.open({
			templateUrl: 'views/user/family/attendance_log.html?res_v=' + $scope.res_v,
			controller: 'attendanceLogCtrl',
			backdrop: 'static',
			keyboard: false,
			windowClass: "profile-popup",
			resolve: {
				attendanceHistoryInfos: function () {
					return attendanceHistoryInfos;
				}
			}
		});
	};

	/**
	 * @author zhengguo
	 * @description
	 * @returns 打开Giving Notice Form 离院申请单
	 */
	$scope.givingNotice = function (childInfo, id, operaType) {
		var scope = $scope.$new();
		scope.childInfo = childInfo;
		scope.givingNoticeId = id;
		scope.givingNoticeStatus = !operaType ? -1 : operaType;
		$uibModal.open({
			templateUrl: 'views/user/family/giving_notice.html?res_v=' + $scope.res_v,
			controller: 'givingNoticeCtrl',
			backdrop: 'static',
			keyboard: false,
			windowClass: "profile-popup",
			scope: scope
		});
	};
	$scope.isHaveRequestNoticeResult = false;
	/**
	 * 是否有 request 的 离园申请 gfwang
	 */
	$scope.isHaveRequestGivingNotice = function (accountId) {
		familyService.isHaveRequestGivingNotice(accountId).then(angular.bind(this, function then() {
			var result = familyService.isHaveGivingNoticeResule;
			if (result.code == 0) {
				var count = result.data;
				$scope.isHaveRequestNoticeResult = count > 0 ? true : false;
			}
		}));
	};
	/**
	 * 是否有 request 的 小孩请假 gfwang
	 */
	$scope.isHaveRequestAbsteen = function (accountId) {
		familyService.isHaveRequestAbsteen(accountId).then(angular.bind(this, function then() {
			var result = familyService.isHaveAbsteenResule;
			if (result.code == 0) {
				var count = result.data;
				var index = getIndexByAccoutId(accountId);
				$scope.childList[index].isHaveRequestAbsteenResult = count > 0 ? true : false;
			}
		}));
	};
	// gfwang
	function getIndexByAccoutId(accountId) {
		var index = -1;
		for (var i = 0; i < $scope.childList.length; i++) {
			var child = $scope.childList[i];
			if (child.accountInfo.id && child.accountInfo.id == accountId) {
				index = i;
			}
		}
		return index;
	}

	/**
	 * @author gfwang
	 * @description
	 * @returns 打开请假页面
	 */
	$scope.childAbsentee = function (childInfo, id, operaType) {
		var scope = $scope.$new();
		scope.childInfo = childInfo;
		scope.absenteeId = id;
		scope.absenteeStatus = !operaType ? -1 : operaType;
		$uibModal.open({
			templateUrl: 'views/user/family/attendance_child_absentee.html?res_v=' + $scope.res_v,
			controller: 'absentCtrl',
			backdrop: 'static',
			keyboard: false,
			windowClass: "profile-popup",
			scope: scope
		});
	};
	// end change
	// Attendance========================================================

	// lOG List=============================
	$scope.getLogList = function (userId) {
		if (!userId) {
			return;
		}
		familyService.getLogList(userId).then(angular.bind(this, function then() {
			var result = familyService.logResult;
			if (result.code == 0) {
				$scope.logList = result.data;
			} else {
				$rootScope.message(result.msg);
			}

		}));
	};

	$scope.saveColumn = function (ev, id, userId) {
		if (ev.keyCode == 13) {
			$scope.saveLog(id, userId);
		}
	};
	$scope.saveFlag = false;
	$scope.saveLog = function (id, userId) {
		var reason = $("#editReason" + id).val();
		var date = $("#editDate" + id).val();
		var logObj = "";
		angular.forEach($scope.logList, function (logg) {
			if (id == logg.id) {
				logObj = angular.copy(logg);
			}
		});
		logObj.reason = reason;
		logObj.date = date;
		logObj.userId = userId;
		$scope.saveFlag = true;
		familyService.saveLog(logObj).then(angular.bind(this, function then() {
			var result = familyService.saveResult;
			if (result.code == 0) {
				$scope.getLogList(userId);
				$scope.saveFlag = false;
			} else {
				$rootScope.message(result.msg);
				$scope.saveFlag = false;
			}

		}));
	};

	$scope.addlogObj = {};
	$scope.addColumn = function (ev, id) {
		if (ev.keyCode == 13) {
			$scope.addLog(id);
		}
	};
	$scope.addFlag = false;
	$scope.addLog = function (userId) {
		$scope.addlogObj.userId = userId;
		$scope.addFlag = true;
		familyService.saveLog($scope.addlogObj).then(angular.bind(this, function then() {
			var result = familyService.saveResult;
			if (result.code == 0) {
				$scope.getLogList(userId);
				$scope.addlogObj = {};
				$scope.addFlag = false;
			} else {
				$rootScope.message(result.msg);
				$scope.addFlag = false;
			}

		}));
	};

	$scope.deleteLog = function (id, userId) {
		familyService.deleteLog(id).then(angular.bind(this, function then() {
			var result = familyService.delResult;
			if (result.code == 0) {
				$scope.getLogList(userId);
			} else {
				$rootScope.message(result.msg);
			}

		}));
	};
	// gfwang
	$scope.getProfileByUserId = function (id, childInfo) {
		// 获取下拉list
		commonService.getProfileSelectList(0).then(angular.bind(this, function then() {
			var result = commonService.profileListResult;
			if (result.code == 0) {
				$scope.profileSelectList = result.data;
				// 获取当前用户选中的form
				commonService.getProfileInfo(id).then(angular.bind(this, function then() {
					var result = commonService.profileResult;
					if (result.code == 0) {
						if (!result.data) {
							// 没有查到此人的form
							childInfo.code = 0;
							childInfo.profileId = '';
							return;
						}
						var profileId = result.data.id;
						var name = result.data.templateName;
						childInfo.profileId = profileId;
						childInfo.code = !result.data.instanceId ? 0 : result.data.instanceId;
						childInfo.valueId = !result.data.valueId ? 0 : result.data.valueId;
						// 加载form
						childInfo.initForm(childInfo.code, childInfo.valueId, childInfo, childInfo.userInfo.status == 0 || $scope.isParent);
						if (result.data.deleteFlag == 1 || result.data.state == 0 || result.data.currtenFlag == 0) {
							$scope.profileSelectList.push({
								id: profileId,
								text: name,
								disabled: true
							});
						}
					} else {
						$scope.message(result.msg);
					}
				}));
			} else {
				$scope.message(result.msg);
			}
		}));
	};
	// end gfwang
	$scope.selectChange = function (childInfo) {
		if (!childInfo.profileId) {
			return;
		}
		commonService.getProfileInfo($scope.currtenEditId, childInfo.profileId).then(angular.bind(this, function then() {
			var result = commonService.profileResult;
			if (result.data) {
				childInfo.code = !result.data.instanceId ? 0 : result.data.instanceId;
				var valueId = !result.data.valueId ? 0 : result.data.valueId;
				childInfo.initForm(childInfo.code, valueId, childInfo, childInfo.userInfo.status == 0 || $scope.isParent);
			} else {
				commonService.selectChange(childInfo.profileId).then(angular.bind(this, function then() {
					var result = commonService.selectChangeResult;
					childInfo.initForm(result.data.instanceId, 0, childInfo, childInfo.userInfo.status == 0 || $scope.isParent);
				}));
			}
		}));

	};
	// 获取小孩的所有的年龄段
	$scope.selectAllAge = function (childAccountId) {
		familyService.getAgeStage().then(angular.bind(this, function then() {
			var result = familyService.ageResult;
			$scope.title = 0;
			$scope.openEylfCheck(0, childAccountId);
			$scope.ages = result;
		}));
	};
	// 获取指定小孩年龄段的能力
	$scope.openEylfCheck = function (age, id) {
		$scope.title = age;
		if (!id) {
			return;
		}
		familyService.getEylfCheck(age, id).then(angular.bind(this, function then() {
			var result = familyService.eylfCheckResult;
			$scope.eylfs = result;
		}));
	};

	$scope.showReason = function (check) {
		check.show = !check.show;
	};

	/**
	 * 将孩子的能力导出PDF
	 */
	$scope.reportsToPdf = function (childInfo) {
		$scope.reportVo = {
			title: "",
			userId: "",
			list: null
		}
		$scope.reportVo.list = $scope.eylfs;
		$scope.reportVo.userId = childInfo.userInfo.id;
		$scope.reportVo.title = $filter('ageGroupTitle')($scope.title);

		familyService.exportReportsPdf($scope.reportVo).then(angular.bind(this, function then() {
			var result = familyService.reportsResult;
			if (result.code == 0) {
				window.location.href = "family/reportsPdf.do?id=" + result.data;
			}
		}));
	};


	// 判断当前用户是否为第一个家长
	$scope.isFirstParent = function () {
		var isParent = $filter('haveRole')($scope.currentUser.roleInfoVoList, '10;');
		if (!isParent) {
			return true;
		}
		if ($scope.parentList && $scope.parentList[0].id && $scope.currentUser.userInfo.id == $scope.parentList[0].id) {
			return true;
		} else {
			return false;
		}
	};

	$scope.changeAttentFlag = function (attendFlag, childInfo) {
		var userId = childInfo.userInfo.id;
		familyService.changeOutAttend(attendFlag, userId).then(angular.bind(this, function then() {
			var result = familyService.changeOutAttendResult;
			if (result.code == 0) {
				if (callbackReload) {
					callbackReload();
				}
				$scope.isOutChildAndUse = attendFlag;
				childInfo.attendance.attendFlag = attendFlag;
				childInfo.attendance.type = result.data.type;
				childInfo.attendance.enrolled = result.data.enrolled;
			} else {
				$scope.message(result.msg);
			}
		}));
	};

	$scope.taskListFun = function (accountId) {
		familyService.getTaskList(accountId).then(angular.bind(this, function then() {
			var result = familyService.taskListResult;
			if (result.code == 0) {
				$scope.taskList = result.data;
			} else {
				$scope.message(result.msg);
			}
		}));
	};

	/**
	 * 打开task modal
	 */
	var modalOpen = true;
	$scope.openModal = function (task, roomId) {
		if (task.type == 17) {
			$scope.openTaskModal(task.type, task.id, task.name, task.date);
		}
		if (task.type == 18) {
			$scope.medicationModal(task.type, task.taskId, task.id, roomId);
		}
	}

	$scope.openTaskModal = function (type, myTaskId, taskName, taskDate) {
		if (!modalOpen) {
			return;
		}
		$scope.showLoading('#mainLoad');
		var isAfterToday = dateComp(taskDate);
		modalOpen = false;
		$uibModal.open({
			templateUrl: 'views/program/task.form.html?res_v=' + $scope.res_v,
			controller: 'taskFormCtrl',
			backdrop: 'static',
			keyboard: false,
			windowClass: 'modal-grey task-popup',
			animation: true,
			resolve: {
				myTaskId: function () {
					return myTaskId;
				},
				taskName: function () {
					return taskName;
				},
				functions: function () {
					return $scope.taskfunctions;
				},
				isAfterToday: function () {
					return isAfterToday;
				},
				taskDate: function () {
					return $scope.taskDate;
				},
				isParent: function () {
					return $scope.isParent;
				}
			}

		}).result.then(function (instanceId) {
			modalOpen = true;
			// $scope.getPageInfo();
		});
		modalOpen = true;
	};

	$scope.medicationModal = function (type, medicationId, id, roomId) {
		if (!modalOpen) {
			return;
		}
		var modalInstance = $uibModal.open({
			templateUrl: 'views/program/medication.html?res_v=' + $scope.res_v,
			controller: 'medicationCtrl',
			backdrop: 'static',
			keyboard: false,
			windowClass: 'modal-grey',
			resolve: {
				type: function () {
					return type;
				},
				roomId: function () {
					return roomId;
				},
				medicationId: function () {
					return medicationId;
				},
				obsId: function () {
					return !id ? "" : id;
				},
				surpassListFlag: function () {
					return $scope.surpassListFlag;
				},
				todayDate: function () {
					return $scope.taskDate;
				},
				updateListFun: function () {
					return function () {

					};
				},
				functions: function () {
					return $scope.functions;
				},
				isParent: function () {
					return $scope.isParent;
				}
			}
		})
	};

	function dateComp(chooseDate) {
		return new moment(chooseDate, 'YYYY-MM-DD').isAfter(new moment($scope.currentUser.currtenDate, 'DD/MM/YYYY'));
	}



	$scope.removeLogFile = function (log) {
		if (log) {
			$timeout(function () {
				angular.forEach($scope.logList, function (logg) {
					if (log.id == logg.id) {
						logg.file = undefined;
						logg.attachId = null;
					}
				});
			});
		} else {
			$scope.addlogObj.file = undefined;
		}
	};

	$scope.uploadFile = function ($files, logId) {
		if ($files == null) {
			return;
		}
		var file = $files[0];
		if (file == undefined) {
			return;
		}
		var AllImgExt = ".doc|.docx|.pdf|.jpg|.png|.gif|.xls|.xlsx|.tiff|";
		var extName = file.name.substring(file.name.lastIndexOf(".")).toLowerCase(); // （把路径中的所有字母全部转换为小写）
		if (AllImgExt.indexOf(extName + "|") == -1) {
			$rootScope.message("Please ensure your file is a pdf,doc,docx,xlsx,xls,png,jpg,gif,tiff file type.");
			return false;
		}
		if (file.size < 10485760) {
			$scope.showLoading("#uploadFile");
			fileService.uploadFile(file).then(angular.bind(this, function then() {
				var result = fileService.fdata;
				if (result.code == 0) {
					if (logId) {
						angular.forEach($scope.logList, function (logg) {
							if (logId == logg.id) {
								logg.file = result.data;
								logg.fileName = file.name;
							}
						});
					} else {
						$scope.addlogObj.file = result.data;
						$scope.addlogObj.fileName = file.name;
					}
				} else {
					$rootScope.message(result.msg);
				}
				$scope.hideLoading("#uploadFile");
			}));
		} else {
			$rootScope.message('Sorry, the filesize is too large, please reduce the filesize and try again.');
			$scope.hideLoading("#uploadFile");
		}
	};

	$scope.getPad = function () {
		$scope.signaturePad.fromDataURL($scope.emergencySignature);
	};

	$scope.openSelectModel = function (userId) {
		var modalInstance = $uibModal.open({
			templateUrl: 'views/user/family/select_family.html?res_v=' + $scope.res_v,
			controller: 'familySelectCtrl',
			backdrop: 'static',
			keyboard: false,
			transclude: false,
			resolve: {
				userId: function () {
					return userId;
				},
				callbackReload: function () {
					return closeModel;
				}
			}
		});
	}

	function closeModel(model) {
		familyService.moveAnotherFamily(model).then(angular.bind(this, function () {
			var result = familyService.moveAnotherFamilyResult;
			if (result.code == 0) {
				$uibModalInstance.dismiss('cancel');
				callbackReload();
			}
		}));
	}

	$scope.moveToOutside = function (childInfo, isSibling) {
		if (childInfo.attendance.type == 0 && childInfo.attendance.enrolled == 0) {
			$scope.confirm('Warning', 'This Child has a Start Date. Are you sure you want to move them back into the Waitlist?', 'OK', function () {
				moveToOutsideFun(childInfo.userInfo.id, isSibling);
			}, 'NO');
		} else {
			moveToOutsideFun(childInfo.userInfo.id, isSibling);
		}
	}

	function moveToOutsideFun(userId, isSibling) {
		familyService.moveToOutside(userId, isSibling).then(angular.bind(this, function () {
			var result = familyService.moveToOutsideResult;
			if (result.code == 0) {
				$uibModalInstance.dismiss('cancel');
				callbackReload();
			}
		}));
	}

	$scope.givingNoticeLog = function (childInfo, id, operaType, isReadOnly) {
		var scope = $scope.$new();
		scope.childInfo = childInfo;
		scope.givingNoticeId = id;
		scope.givingNoticeStatus = !operaType ? -1 : operaType;
		scope.isReadOnly = isReadOnly;
		$uibModal.open({
			templateUrl: 'views/user/family/giving_notice.html?res_v=' + $scope.res_v,
			controller: 'givingNoticeCtrl',
			backdrop: 'static',
			keyboard: false,
			windowClass: "profile-popup",
			scope: scope
		});
	};

	$scope.forwardOpt = function (childInfo, id, operaType) {
		if (operaType == 12) {
			$scope.givingNoticeLog(childInfo, id, operaType, true);
		} else {
			$scope.childAbsenteeLog(childInfo, id, operaType, true);
		}

	};

	$scope.childAbsenteeLog = function (childInfo, id, operaType, isReadOnly) {
		var scope = $scope.$new();
		scope.childInfo = childInfo;
		scope.absenteeId = id;
		scope.absenteeStatus = !operaType ? -1 : operaType;
		scope.isReadOnly = isReadOnly;
		$uibModal.open({
			templateUrl: 'views/user/family/attendance_child_absentee.html?res_v=' + $scope.res_v,
			controller: 'absentCtrl',
			backdrop: 'static',
			keyboard: false,
			windowClass: "profile-popup",
			scope: scope
		});
	};

	$scope.openRequest = function (objId, type, isRead) {
		attendanceService.getRequestLogObj(objId, type).then(angular.bind(this, function then() {
			var result = attendanceService.requestLogObj;
			if (result.code == 0) {
				$scope.openWin(objId, '', type, result.data, isRead);
			} else {
				$rootScope.message(result.msg);
			}
			;
		}));

	};

	$scope.openWin = function (objId, childId, type, childInfo, isRead) {
		// 打开不同编辑页面
		if (type == 0 || type == 1 || type == 2 || type == 3 || type == 4 || type == 5) { // 园长操作转园申请
			$uibModal.open({
				templateUrl: 'views/user/family/change_attendance.html?res_v=' + $scope.res_v,
				controller: 'userFamilyEditChangeAttendanceCtrl',
				size: "lg",
				backdrop: 'static',
				keyboard: false,
				windowClass: "profile-popup",
				resolve: {
					childInfo: function () {
						return childInfo;
					},
					childId: function () {
						return childInfo.accountInfo.id;
					},
					type: function () {
						return type;
					},
					functions: function () {
						return $scope.functions;
					},
					currentUser: function () {
						return $scope.currentUser;
					},
					callbackFunction: function () {
						return $scope.changeAttendanceCallback;
					},
					objId: function () {
						return objId;
					},
					isRead: function () {
						return isRead;
					}
				}
			});

		}
	};

	$scope.complete = function (childInfo) {
		childInfo.json = JSON.stringify(childInfo.getJson());
		familyService.complete(childInfo).then(angular.bind(this, function then() {
			var result = familyService.completeResult;
			if (result.code == 0) {
				$rootScope.message('Successful.');
			}
		}));
	}

	$scope.sendConfirmationEnrolmentEmail = function (id) {
		familyService.sendEnrolmentEmail(id).then(angular.bind(this, function then() {
			var result = familyService.sendEnrolmentEmailResult;
			if (result.code == 0) {
				$rootScope.message('Confirmation of Enrolment email has been successfully sent.');
			}
		}));
	}

}
angular.module('kindKidsApp').controller(
	'userFamilyEditCtrl', ['$scope', '$rootScope', '$timeout', '$window', '$uibModal', '$uibModalInstance', '$filter', 'commonService', 'familyService',
		'fileService', 'familyId', 'functions', 'currentUser', 'callbackReload', 'attendanceService', userFamilyEditCtrl
	]);