'use strict';

/**
 * @author gfwang
 * @description attendanceLog
 */
function attendanceLogCtrl($scope, $rootScope, $timeout, $window,$uibModalInstance, $filter, familyService,attendanceHistoryInfos) {
	$scope.attendanceHistoryInfos=attendanceHistoryInfos;


	$scope.cancel = function() {
		$uibModalInstance.dismiss('cancel');
	};
}
angular.module('kindKidsApp').controller(
		'attendanceLogCtrl',
		[ '$scope', '$rootScope', '$timeout', '$window', '$uibModalInstance', '$filter', 'familyService', 'attendanceHistoryInfos', attendanceLogCtrl ]);
