'use strict';

/**
 * @author gfwang
 * @description userFamilyEditChangeAttendanceCtrl
 */
function userFamilyEditChangeAttendanceCtrl($scope, $rootScope, $timeout, $window, $uibModalInstance, $filter, familyService, childInfo, childId,
	type, functions, currentUser, callbackFunction, objId, isRead) {
	$scope.type = type;
	// type=0:家长申请
	// type=1:园长申请转园
	// type=2:CEO直接更改园

	// type=3:园长操作转园申请
	// type=6:外部小孩入园
	// type=4:Transfer this child to another room
	// type=5:CEO 园长直接更改课程Change attendance
	$scope.childId = childId;
	$scope.functions = functions;
	$scope.currentUser = $rootScope.currentUser;
	$scope.callbackFunction = callbackFunction; // 保存内容后进行回调
	$scope.isCeo = false; // 是否是CEO角色
	$scope.isAdmin = false; // 是否是CEO角色
	$scope.isRead = isRead;

	$scope.centersList = [];
	$scope.roomList = [];
	$scope.groupList = [];
	$scope.overOld = false;
	$scope.isFullOver = false;

	$scope.changeAttendanceRequest = {
		id: objId,
		childId: childId,
		newCentreId: childInfo.userInfo.centersId,
		newRoomId: childInfo.userInfo.roomId,
		newGroupId: childInfo.userInfo.groupId,
		mondayBool: childInfo.attendance.monday,
		tuesdayBool: childInfo.attendance.tuesday,
		wednesdayBool: childInfo.attendance.wednesday,
		thursdayBool: childInfo.attendance.thursday,
		fridayBool: childInfo.attendance.friday,
		sourceType: $scope.type,
		changeDate: (childInfo.attendance.changeDate != null && childInfo.attendance.createAccountId == "") ? childInfo.attendance.changeDate : null,
		orientation: childInfo.attendance.orientation != null ? childInfo.attendance.orientation : null,
		orientationTime: childInfo.attendance.orientationTime != null ? childInfo.attendance.orientationTime : null
	};

	$scope.getOrgInfo = function () {
		familyService.getFamilyOrgs("").then(angular.bind(this, function then() {
			var result = familyService.orgsResult;
			$scope.centersList = result.code.centersInfos;
			$scope.roomList = result.code.roomInfos;
			$scope.groupList = result.code.groups;
		}));
	};

	$scope.changeAttendanceCtrlInit = function () {
		$scope.isCEO = $filter('haveRole')($scope.currentUser.roleInfoVoList, '0;');
		$scope.isAdmin = $filter('haveRole')($scope.currentUser.roleInfoVoList, '0;1;5;');
	};

	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
	$scope.cancel1 = function () {
		$scope.confirm('Close', 'Exit this page?', 'OK', function () {
			$uibModalInstance.dismiss('cancel');
		}, 'NO');
	};

	$scope.planConfirm = function () {
		if (type == 2 || type == 3 || type == 4 || type == 5) {
			$('#changeattendanceForm').data('bootstrapValidator').validate();
			if (!$('#changeattendanceForm').data('bootstrapValidator').isValid()) {
				return;
			}
			$scope.confirm('Confirm', 'Is this change made for the Position Plan of the Next Year?', 'YES', function () {
				$scope.changeAttendanceRequest.isPlan = true;
				$scope.submit();
			}, 'NO', function () {
				$scope.changeAttendanceRequest.isPlan = false;
				$scope.submit();
			});
		} else {
			$scope.submit();
		}
	}

	$scope.submit = function () {
		$scope.overOld = false;
		$scope.isFullOver = false;
		$scope.submitServer();
	};

	$scope.submitServer = function () {
		$('#changeattendanceForm').data('bootstrapValidator').validate();
		if ($('#changeattendanceForm').data('bootstrapValidator').isValid()) {
			$scope.showLoading("#changeAttendanceDiv");
			// 拼接日期
			if ($scope.changeAttendanceRequest.orientation) {
				$scope.changeAttendanceRequest.orientation = $scope.changeAttendanceRequest.orientation.replace(/\-/g, "/") +
					$scope.changeHeadTime($scope.changeAttendanceRequest.orientationTime);
			}
			familyService.submitChangeAttendance($scope.changeAttendanceRequest, $scope.overOld, $scope.isFullOver).then(
				angular.bind(this, function then() {
					var result = familyService.changeAttendanceResult;
					$scope.hideLoading("#changeAttendanceDiv");
					if (result.code == 0) {
						$uibModalInstance.dismiss('cancel');
						$scope.callbackFunction(childInfo.userInfo.id, type);
					} else if (result.code == 3) {
						$rootScope.confirm('confirm', result.msg, 'Ok', function () {
							$scope.overOld = true;
							$scope.submitServer();
						}, 'Cancel', null);
					} else if (result.code == 11) {
						$rootScope.confirm('confirm', result.msg, 'Ok', function () {
							$scope.isFullOver = true;
							$scope.submitServer();
						}, 'Cancel', null);
					} else {
						$rootScope.message(result.msg);
					}
				}));
		}
	};

	$scope.showLoading = function (tag) {
		$(tag).mymask('');
	};
	$scope.hideLoading = function (tag) {
		if ($(tag).myisMasked()) {
			$(tag).myunmask();
		}
	};
}
angular.module('kindKidsApp').controller(
	'userFamilyEditChangeAttendanceCtrl', ['$scope', '$rootScope', '$timeout', '$window', '$uibModalInstance', '$filter', 'familyService', 'childInfo', 'childId', 'type',
		'functions', 'currentUser', 'callbackFunction', 'objId', 'isRead', userFamilyEditChangeAttendanceCtrl
	]);