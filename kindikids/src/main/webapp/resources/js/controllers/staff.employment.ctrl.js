'use strict';

function staffEmploymentCtrl($scope, $rootScope, $timeout, $filter, $window, $uibModal, $uibModalInstance, commonService, staffService, fileService, employ, roles, centerFilter, accountId, staffTypeOption, callback, currUser, isAdd) {
    $scope.staffInfo = {};
    $scope.employ = {};
    $scope.employ.firstRole = {};
    $scope.roles = roles;
    $scope.centerFilter = centerFilter;
    $scope.staffTypeOption = staffTypeOption;
    $scope.totalHours = 0;
    $scope.currUser = currUser;
    $scope.isAdd = isAdd;

    $scope.staffInfo.employInfo = employ;
    $scope.staffInfo.employInfo.accountId = accountId;
    if ($scope.isAdd) {
        $scope.staffInfo.employInfo.effectiveDate = null;
    }
    $scope.staffInfo.userInfo = {
        staffType: employ.staffType,
        centersId: employ.centreId,
        roomId: employ.roomId,
        groupId: employ.groupId
    };
    $scope.employ.firstRole = {
        roleId: employ.primaryRole
    }
    if ($scope.isAdd) {
        $scope.employ.roles = employ.roles;
    } else {
        $scope.employ.roles = employ.tags;
    }

    // 设置区域
    if (employ.region == 0) {
        $scope.staffInfo.employInfo.edensorPark = true;
        $scope.staffInfo.employInfo.ryde = true;
    }
    if (employ.region == 1) {
        $scope.staffInfo.employInfo.edensorPark = true;
    }
    if (employ.region == 2) {
        $scope.staffInfo.employInfo.ryde = true;
    }
    if ($scope.staffInfo.employInfo.monday) {
        var monHours = $scope.staffInfo.employInfo.monHours;
        $scope.totalHours = $scope.totalHours + parseFloat(monHours);
    }
    if ($scope.staffInfo.employInfo.tuesday) {
        var tueHours = $scope.staffInfo.employInfo.tueHours;
        $scope.totalHours = $scope.totalHours + parseFloat(tueHours);
    }
    if ($scope.staffInfo.employInfo.wednesday) {
        var wedHours = $scope.staffInfo.employInfo.wedHours;
        $scope.totalHours = $scope.totalHours + parseFloat(wedHours);
    }
    if ($scope.staffInfo.employInfo.thursday) {
        var thuHours = $scope.staffInfo.employInfo.thuHours;
        $scope.totalHours = $scope.totalHours + parseFloat(thuHours);
    }
    if ($scope.staffInfo.employInfo.friday) {
        var friHours = $scope.staffInfo.employInfo.friHours;
        $scope.totalHours = $scope.totalHours + parseFloat(friHours);
    }
    $scope.totalHours = $scope.totalHours.toFixed(2);

    $scope.$watch('staffInfo.employInfo.monday', function (newValue, oldValue) {
        if (newValue == oldValue) {
            return;
        }
        if (newValue) {
            $scope.staffInfo.employInfo.monStartTime = '6:00 AM';
            $scope.staffInfo.employInfo.monEndTime = '7:00 PM';
            $scope.staffInfo.employInfo.monHours = $scope.getHoursByChoose();
        } else {
            $scope.staffInfo.employInfo.monStartTime = null;
            $scope.staffInfo.employInfo.monEndTime = null;
            $scope.staffInfo.employInfo.monHours = null;
        }
        $scope.dealTotalHours();
    });

    $scope.$watch('staffInfo.employInfo.tuesday', function (newValue, oldValue) {
        if (newValue == oldValue) {
            return;
        }
        if (newValue) {
            $scope.staffInfo.employInfo.tueStartTime = '6:00 AM';
            $scope.staffInfo.employInfo.tueEndTime = '7:00 PM';
            $scope.staffInfo.employInfo.tueHours = $scope.getHoursByChoose();
        } else {
            $scope.staffInfo.employInfo.tueStartTime = null;
            $scope.staffInfo.employInfo.tueEndTime = null;
            $scope.staffInfo.employInfo.tueHours = null;
        }
        $scope.dealTotalHours();
    });

    $scope.$watch('staffInfo.employInfo.wednesday', function (newValue, oldValue) {
        if (newValue == oldValue) {
            return;
        }
        if (newValue) {
            $scope.staffInfo.employInfo.wedStartTime = '6:00 AM';
            $scope.staffInfo.employInfo.wedEndTime = '7:00 PM';
            $scope.staffInfo.employInfo.wedHours = $scope.getHoursByChoose();
        } else {
            $scope.staffInfo.employInfo.wedStartTime = null;
            $scope.staffInfo.employInfo.wedEndTime = null;
            $scope.staffInfo.employInfo.wedHours = null;
        }
        $scope.dealTotalHours();
    });

    $scope.$watch('staffInfo.employInfo.thursday', function (newValue, oldValue) {
        if (newValue == oldValue) {
            return;
        }
        if (newValue) {
            $scope.staffInfo.employInfo.thuStartTime = '6:00 AM';
            $scope.staffInfo.employInfo.thuEndTime = '7:00 PM';
            $scope.staffInfo.employInfo.thuHours = $scope.getHoursByChoose();
        } else {
            $scope.staffInfo.employInfo.thuStartTime = null;
            $scope.staffInfo.employInfo.thuEndTime = null;
            $scope.staffInfo.employInfo.thuHours = null;
        }
        $scope.dealTotalHours();
    });

    $scope.$watch('staffInfo.employInfo.friday', function (newValue, oldValue) {
        if (newValue == oldValue) {
            return;
        }
        if (newValue) {
            $scope.staffInfo.employInfo.friStartTime = '6:00 AM';
            $scope.staffInfo.employInfo.friEndTime = '7:00 PM';
            $scope.staffInfo.employInfo.friHours = $scope.getHoursByChoose();
        } else {
            $scope.staffInfo.employInfo.friStartTime = null;
            $scope.staffInfo.employInfo.friEndTime = null;
            $scope.staffInfo.employInfo.friHours = null;
        }
        $scope.dealTotalHours();
    });

    $scope.dealTotalHours = function () {
        $scope.totalHours = 0;
        if ($scope.staffInfo.employInfo.monday) {
            var monHours = $scope.staffInfo.employInfo.monHours;
            if (monHours == '') {
                $scope.totalHours = $scope.totalHours;
            } else {
                $scope.totalHours = $scope.totalHours + parseFloat(monHours);
            }
        }
        if ($scope.staffInfo.employInfo.tuesday) {
            var tueHours = $scope.staffInfo.employInfo.tueHours;
            if (tueHours == '') {
                $scope.totalHours = $scope.totalHours;
            } else {
                $scope.totalHours = $scope.totalHours + parseFloat(tueHours);
            }
        }
        if ($scope.staffInfo.employInfo.wednesday) {
            var wedHours = $scope.staffInfo.employInfo.wedHours;
            if (wedHours == '') {
                $scope.totalHours = $scope.totalHours;
            } else {
                $scope.totalHours = $scope.totalHours + parseFloat(wedHours);
            }
        }
        if ($scope.staffInfo.employInfo.thursday) {
            var thuHours = $scope.staffInfo.employInfo.thuHours;
            if (thuHours == '') {
                $scope.totalHours = $scope.totalHours;
            } else {
                $scope.totalHours = $scope.totalHours + parseFloat(thuHours);
            }
        }
        if ($scope.staffInfo.employInfo.friday) {
            var friHours = $scope.staffInfo.employInfo.friHours;
            if (friHours == '') {
                $scope.totalHours = $scope.totalHours;
            } else {
                $scope.totalHours = $scope.totalHours + parseFloat(friHours);
            }
        }
        $scope.totalHours = $scope.totalHours.toFixed(2);
    }

    $scope.closeModal = function () {
        $scope.confirm('Close', 'Exit this page?', 'OK', function () {
            $uibModalInstance.dismiss('cancel');
        }, 'NO');
    }

    $scope.saveEmployment = function () {
        $scope.checkEmploy();
        $('#staffEmployment').data('bootstrapValidator').validate();
        if (!$('#staffEmployment').data('bootstrapValidator').isValid()) {
            return;
        }
        // 0 : All 1 : Edensor Park 2 : Ryde
        if ($scope.staffInfo.employInfo.edensorPark && $scope.staffInfo.employInfo.ryde) {
            $scope.staffInfo.employInfo.region = 0;
        }
        if ($scope.staffInfo.employInfo.edensorPark && !$scope.staffInfo.employInfo.ryde) {
            $scope.staffInfo.employInfo.region = 1;
        }
        if ($scope.staffInfo.employInfo.ryde && !$scope.staffInfo.employInfo.edensorPark) {
            $scope.staffInfo.employInfo.region = 2;
        }
        $scope.staffInfo.employInfo.centreId = $scope.staffInfo.userInfo.centersId;
        $scope.staffInfo.employInfo.roomId = $scope.staffInfo.userInfo.roomId;
        $scope.staffInfo.employInfo.groupId = $scope.staffInfo.userInfo.groupId;
        $scope.staffInfo.employInfo.staffType = $scope.staffInfo.userInfo.staffType;
        $scope.staffInfo.employInfo.primaryRole = $scope.employ.firstRole.roleId;
        $scope.staffInfo.employInfo.tags = $scope.employ.roles;

        staffService.saveEmploy($scope.staffInfo.employInfo).then(function () {
            var result = staffService.saveEmployResult;
            if (result.code == 0) {
                $uibModalInstance.dismiss('cancel');
                callback();
            } else if (result.code == 2) {
                $scope.confirm2('Warning', 'Please note, this person has been rostered on for a day that you are removing from their available days. What would you like to do?',
                    'Save without changing roster',
                    function () {
                        $scope.staffInfo.employInfo.confirm = 1;
                        $scope.saveEmployment();
                    }, 'Save and remove from roster',
                    function () {
                        $scope.staffInfo.employInfo.confirm = 2;
                        $scope.saveEmployment();
                    }, 'Cancel');
                $scope.staffInfo.employInfo.rosterStaffInfos = result.data;
            } else if (result.code == 3) {
                $scope.confirm2('Warning', 'Please note, this person has been rostered on for a region that you are removing from their profile. What would you like to do?',
                    'Save without changing roster',
                    function () {
                        $scope.staffInfo.employInfo.confirm2 = 1;
                        $scope.saveEmployment();
                    }, 'Save and remove from roster',
                    function () {
                        $scope.staffInfo.employInfo.confirm2 = 2;
                        $scope.saveEmployment();
                    }, 'Cancel');
            } else if (result.code == 4) {
                $scope.confirm2('Warning', 'Please note, this person has been rostered on for a Centre that you are removing from their profile. What would you like to do?',
                    'Save without changing roster',
                    function () {
                        $scope.staffInfo.employInfo.confirm2 = 1;
                        $scope.saveEmployment();
                    }, 'Save and remove from roster',
                    function () {
                        $scope.staffInfo.employInfo.confirm2 = 2;
                        $scope.saveEmployment();
                    }, 'Cancel');
            } else {
                $rootScope.message(result.msg);
                $scope.confirm('Warning', result.msg, 'OK');
            }
        });
    }

    $scope.$watch('employ.firstRole.roleId', function (newValue, oldValue) {
        if (newValue == oldValue) {
            return;
        }
        for (var i = 0; i < $scope.roles.roleInfos.length; i++) {
            if (newValue === $scope.roles.roleInfos[i].id && ($scope.roles.roleInfos[i].value == 0 || $scope.roles.roleInfos[i].value == 1)) {
                if ($scope.staffInfo.employInfo.monday) {
                    $scope.staffInfo.employInfo.monHours = 7.6;
                }
                if ($scope.staffInfo.employInfo.tuesday) {
                    $scope.staffInfo.employInfo.tueHours = 7.6;
                }
                if ($scope.staffInfo.employInfo.wednesday) {
                    $scope.staffInfo.employInfo.wedHours = 7.6;
                }
                if ($scope.staffInfo.employInfo.thursday) {
                    $scope.staffInfo.employInfo.thuHours = 7.6;
                }
                if ($scope.staffInfo.employInfo.friday) {
                    $scope.staffInfo.employInfo.friHours = 7.6;
                }
                $scope.dealTotalHours();
            }
            if (newValue === $scope.roles.roleInfos[i].id && ($scope.roles.roleInfos[i].value == 2 || $scope.roles.roleInfos[i].value == 9)) {
                if ($scope.staffInfo.employInfo.monday) {
                    $scope.staffInfo.employInfo.monHours = 0;
                }
                if ($scope.staffInfo.employInfo.tuesday) {
                    $scope.staffInfo.employInfo.tueHours = 0;
                }
                if ($scope.staffInfo.employInfo.wednesday) {
                    $scope.staffInfo.employInfo.wedHours = 0;
                }
                if ($scope.staffInfo.employInfo.thursday) {
                    $scope.staffInfo.employInfo.thuHours = 0;
                }
                if ($scope.staffInfo.employInfo.friday) {
                    $scope.staffInfo.employInfo.friHours = 0;
                }
                $scope.dealTotalHours();
            }
        }
    });

    $scope.$watch('staffInfo.userInfo.staffType', function (newValue, oldValue) {
        if (newValue == oldValue || newValue == null) {
            return;
        }

        if (newValue === 0) {
            if ($scope.staffInfo.employInfo.monday) {
                $scope.staffInfo.employInfo.monHours = 7.6;
            }
            if ($scope.staffInfo.employInfo.tuesday) {
                $scope.staffInfo.employInfo.tueHours = 7.6;
            }
            if ($scope.staffInfo.employInfo.wednesday) {
                $scope.staffInfo.employInfo.wedHours = 7.6;
            }
            if ($scope.staffInfo.employInfo.thursday) {
                $scope.staffInfo.employInfo.thuHours = 7.6;
            }
            if ($scope.staffInfo.employInfo.friday) {
                $scope.staffInfo.employInfo.friHours = 7.6;
            }
            $scope.dealTotalHours();
        } else {
            if ($scope.staffInfo.employInfo.monday) {
                $scope.staffInfo.employInfo.monHours = 0;
            }
            if ($scope.staffInfo.employInfo.tuesday) {
                $scope.staffInfo.employInfo.tueHours = 0;
            }
            if ($scope.staffInfo.employInfo.wednesday) {
                $scope.staffInfo.employInfo.wedHours = 0;
            }
            if ($scope.staffInfo.employInfo.thursday) {
                $scope.staffInfo.employInfo.thuHours = 0;
            }
            if ($scope.staffInfo.employInfo.friday) {
                $scope.staffInfo.employInfo.friHours = 0;
            }
            $scope.dealTotalHours();
        }
    });

    // 监听二级角色
    $scope.$watch('staffInfo.userInfo.staffType', function (newValue, oldValue) {
        if ($scope.staffInfo == undefined || $scope.staffInfo.userInfo.staffType == null) return;
        if ($scope.staffInfo.userInfo.staffType != 2) {
            var frs = $scope.employ.firstRole.roleId;
            var val = $scope.getValueByRoleId(frs);
            if (val == 2) { // Educator
                $scope.employ.showCenter = true;
                $scope.employ.showRoom = true;
                $scope.employ.showGroup = true;
            } else if (val == 9) { // Cook
                $scope.employ.showCenter = true;
                $scope.employ.showRoom = false;
                $scope.employ.showGroup = false;
            }
        } else {
            $scope.employ.showCenter = false;
            $scope.employ.showRoom = false;
            $scope.employ.showGroup = false;

            $scope.staffInfo.userInfo.centersId = $scope.currUser.centerId;
            $scope.staffInfo.userInfo.roomId = null;
            $scope.staffInfo.userInfo.groupId = null;
        }
    });

    // Employ动态验证
    $scope.checkEmploy = function () {
        try {
            if ($('#center').hasClass('ng-hide')) {
                $('#staffEmployment').bootstrapValidator('updateStatus', 'centerId', 'NOT_VALIDATED');
                $('#staffEmployment').bootstrapValidator('removeField', 'centerId');
            } else {
                var val = $scope.getValueByRoleId($scope.employ.firstRole.roleId);
                if (val != 9) {
                    $('#staffEmployment').bootstrapValidator('addField', 'centerId', {
                        validators: {
                            notEmpty: {
                                message: "The Centre for this staff member is required."
                            }
                        }
                    });
                }
            }
            if ($('#room').hasClass('ng-hide')) {
                $('#staffEmployment').bootstrapValidator('updateStatus', 'roomId', 'NOT_VALIDATED');
                $('#staffEmployment').bootstrapValidator('removeField', 'roomId');
            } else {
                $('#staffEmployment').bootstrapValidator('addField', 'roomId', {
                    validators: {
                        notEmpty: {
                            message: "The Room for this staff member is required."
                        }
                    }
                });
            }
            // if ($('#group').hasClass('ng-hide')) {
            //     $('#staffEmployment').bootstrapValidator('updateStatus', 'groupId', 'NOT_VALIDATED');
            //     $('#staffEmployment').bootstrapValidator('removeField', 'groupId');
            // } else {
            //     $('#staffEmployment').bootstrapValidator('addField', 'groupId', {
            //         validators: {
            //             notEmpty: {
            //                 message: "The Group for this staff member is required."
            //             }
            //         }
            //     });
            // }
            if ($('#selectRegion').hasClass('ng-hide')) {
                $('#staffEmployment').bootstrapValidator('updateStatus', 'region', 'NOT_VALIDATED');
                $('#staffEmployment').bootstrapValidator('removeField', 'region');
            } else {
                $('#staffEmployment').bootstrapValidator('addField', 'region', {
                    validators: {
                        choice: {
                            min: 1,
                            message: "The Region for this staff member is required."
                        }
                    }
                });
            }
        } catch (e) {

        }
    }


    $scope.roleChange = function () {
        var frs = $scope.employ.firstRole.roleId;
        var val = $scope.getValueByRoleId(frs);
        $scope.initRole();
        if (val == 1 || val == 2) {
            $scope.employ.roles = [];
        }
    }

    $scope.getValueByRoleId = function (roleId) {
        var jLength = $scope.roles.roleInfos.length;
        for (var j = 0; j < jLength; j++) {
            if ($scope.roles.roleInfos[j].id == roleId) {
                return $scope.roles.roleInfos[j].value;
            }
        }
    }

    $scope.initRole = function () {
        var frs = $scope.employ.firstRole.roleId;
        var val = $scope.getValueByRoleId(frs);

        if (val == 0) { // CEO
            $scope.secondSelect = [];
            $scope.employ.showCenter = false;
            $scope.employ.showRoom = false;
            $scope.employ.showGroup = false;
            $scope.employ.showRegion = false;

            $scope.employ.showSRole = false;
            $scope.employ.showStaffType = false;
            $scope.staffInfo.userInfo.centersId = null;
            $scope.staffInfo.userInfo.roomId = null;
            $scope.staffInfo.userInfo.groupId = null;
            $scope.employ.roles = [];
            $scope.staffInfo.userInfo.staffType = null;
        } else if (val == 1) { // Center Manager
            $scope.secondSelect = [3, 7, 8, 12, 13];
            $scope.employ.showCenter = true;
            $scope.employ.showRoom = false;
            $scope.employ.showGroup = false;
            $scope.employ.showRegion = true;

            $scope.employ.showSRole = true;
            $scope.employ.showStaffType = false;
            $scope.staffInfo.userInfo.roomId = null;
            $scope.staffInfo.userInfo.groupId = null;
            //			$scope.employ.roles = [];
            $scope.staffInfo.userInfo.staffType = null;
            $scope.employ.showName = $scope.getNameByRoleId($scope.employ.firstRole.roleId);
        } else if (val == 2) { // Educator
            //$scope.secondSelect = [ 3, 4, 5, 6, 7, 8 ];
            $scope.secondSelect = [3, 5, 6, 7, 12, 13];
            $scope.employ.showCenter = true;
            $scope.employ.showGroup = true;
            $scope.employ.showRoom = true;
            $scope.employ.showRegion = true;
            //			$scope.employ.roles = [];
            $scope.employ.showSRole = true;
            $scope.employ.showStaffType = true;
            $scope.employ.showName = $scope.getNameByRoleId($scope.employ.firstRole.roleId);
        } else if (val == 9) { // Cook
            //$scope.secondSelect = [ 4 ];
            $scope.secondSelect = [];

            $scope.employ.showCenter = true;
            $scope.employ.showRoom = false;
            $scope.employ.showGroup = false;
            $scope.employ.showRegion = true;

            $scope.employ.showSRole = false;
            $scope.employ.showStaffType = true;
            $scope.employ.showName = $scope.getNameByRoleId($scope.employ.firstRole.roleId);

            $scope.staffInfo.userInfo.roomId = null;
            $scope.staffInfo.userInfo.groupId = null;
        } else { // other
            $scope.secondSelect = [];
            $scope.employ.roles = [];
            $scope.staffInfo.userInfo.staffType = null;
            $scope.employ.showGroup = true;
            $scope.employ.showRoom = true;
            $scope.employ.showCenter = true;
            $scope.employ.showRegion = true;

            $scope.employ.showSRole = false;
            $scope.employ.showStaffType = false;
            $scope.employ.showName = '';
        }
        // Casual
        if ($scope.staffInfo.userInfo.staffType == 2) {
            $scope.employ.showCenter = false;
            $scope.employ.showRoom = false;
            $scope.employ.showGroup = false;

            $scope.staffInfo.userInfo.centersId = $scope.currUser.centerId;
            $scope.staffInfo.userInfo.roomId = null;
            $scope.staffInfo.userInfo.groupId = null;
        }
    }

    $scope.getNameByRoleId = function (roleId) {
        var jLength = $scope.roles.roleInfos.length;
        for (var j = 0; j < jLength; j++) {
            if ($scope.roles.roleInfos[j].id == roleId) {
                return $scope.roles.roleInfos[j].name;
            }
        }
    }

    $scope.getHoursByChoose = function () {
        if (!$scope.employ.firstRole.roleId) {
            return 0;
        }
        for (var i = 0; i < $scope.roles.roleInfos.length; i++) {
            if ($scope.employ.firstRole.roleId === $scope.roles.roleInfos[i].id && ($scope.roles.roleInfos[i].value == 0 || $scope.roles.roleInfos[i].value == 1 | $scope.roles.roleInfos[i].value == 5)) {
                return 7.6;
            }
        }
        if ($scope.staffInfo.userInfo.staffType == null) {
            return 0;
        }
        if ($scope.staffInfo.userInfo.staffType == 0) {
            return 7.6;
        }
        return 0;
    }

    // 初始化二级角色
    $scope.secondRoleFilter = function (item) {
        for (var i = 0; i < $scope.secondSelect.length; i++) {
            if ($scope.secondSelect[i] == item.value) {
                return true;
            }
        }
        return false;
    }
}
angular.module('kindKidsApp').controller(
    'staffEmploymentCtrl',
    ['$scope', '$rootScope', '$timeout', '$filter', '$window', '$uibModal', '$uibModalInstance', 'commonService', 'staffService', 'fileService', 'employ', 'roles', 'centerFilter', 'accountId', 'staffTypeOption', 'callback', 'currUser', 'isAdd', staffEmploymentCtrl]);