'use strict';

function menuModalCtrl($scope, $rootScope, $timeout, $window, $uibModal, $uibModalInstance, menuService, fileService,image_resizer) {

	$scope.cancel = function() {
		$scope.confirm('Close','Exit this page?','OK',function(){
			$uibModalInstance.dismiss('cancel');
		},'NO');
	};

	$scope.saveMenu = function() {
		$('#addMenuForm').data('bootstrapValidator').validate();
		if (!$('#addMenuForm').data('bootstrapValidator').isValid()) {
			return;
		}
		var menuInfo = {
			name : $scope.menuName
		};

		$scope.showLoading("#mainDiv");
		menuService.addMenu(menuInfo).then(angular.bind(this, function then() {
			var addResult = menuService.addResult;
			if (addResult.code == 0) {
				$scope.initPager();
				$uibModalInstance.dismiss('cancel');
			}else{
				$rootScope.message(addResult.msg);
			}
			$scope.hideLoading("#mainDiv");
		}));
	};

	$scope.getMenuInfo = function(){
		$scope.updateFlag = true;
		menuService.getMenuInfo($scope.menu.menuId).then(angular.bind(this, function then(){
			var result = menuService.result;
			if(result.code == 0){
				$scope.WeekNutrionalMenuList = result.data;
			} else{
				$rootScope.message(addResult.msg);
			}
			$scope.updateFlag = false;
		}));
	}

	$scope.updateMenu = function(){
		var flag = false;
		$('#menuForm').data('bootstrapValidator').validate();
		$("small").map(function() {
			if ($(this).attr('data-bv-result') == "INVALID") {
				flag = true;
			}
		});
		if (flag) {
			return;
		}
		$timeout(function(){
			var weekMenuVo = {
				list : $scope.WeekNutrionalMenuList,
				menuName : $scope.menu.name
			}
			$scope.showLoading("#menuForm");
			menuService.saveMenuInfo(weekMenuVo).then(angular.bind(this, function then(){
				var result = menuService.result;
				if(result.code == 0){
					$scope.WeekNutrionalMenuList = result.data;
					$scope.initPager();
					$uibModalInstance.dismiss('cancel');
				} else{
					$rootScope.message(result.msg);
				}
				$scope.hideLoading("#menuForm");
			}));
		},200)
		
	}

	$scope.uploadImage = function($file, menu) {
		if ($file == null) {
			return;
		}
		var formatMsg=$scope.isImage($file.name);
		if (formatMsg) {
			$rootScope.message(formatMsg);
			return
		}
		if ($file.size >= 10485760) {
			$rootScope.message("Sorry, the filesize is too large, please reduce the filesize and try again.");
			return;
		}
		$scope.showLoading("#menuMain");
		image_resizer.resize({
			image : $file,
			max_width : 1024
		}).then(function(data) {
			var fileStrBase64 = data.base64_resized_image;
			var fileType = data.file_type;
			fileService.uploadResizeImage(fileType, fileStrBase64, $file.name).then(angular.bind(this, function then() {
				var result = fileService.fdata;
				if (result.code == 0) {
					menu.image = result.data;
				} else {
					$rootScope.message(result.msg);
				}
				$scope.hideLoading("#menuMain");
			}));
		});

	};

	$scope.deleteImage = function(image) {
		image.fileIdName = '';
		image.fileName = '';
		image.relativePathName = '';
		image.visitedUrl = '';
	};


}
angular.module('kindKidsApp').controller('menuModalCtrl',
		[ '$scope', '$rootScope', '$timeout', '$window', '$uibModal', '$uibModalInstance', 'menuService', 'fileService', 'image_resizer', menuModalCtrl ]);
