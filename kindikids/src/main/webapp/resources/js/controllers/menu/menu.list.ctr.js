'use strict';


function nutritionalMenuCtrl($scope, $rootScope, $timeout, $window, $uibModal, menuService, fileService) {

	$scope.page = {};
	$scope.busy = false;
	$scope.page.condition = {
		pageIndex : 1,
		keyWords : ''
	};

	$scope.addMenu = function(){
		var scope = $scope.$new();
		$uibModal.open({
			templateUrl : 'views/menu/create.modal.html?res_v='+$scope.res_v,
			controller : 'menuModalCtrl',
			backdrop : 'static',
			keyboard : false,
			scope : scope
		});
	}

	$scope.openProfile = function(menu){
		var scope = $scope.$new();
		scope.menu = angular.copy(menu);
		$uibModal.open({
			templateUrl : 'views/menu/edit.modal.html?res_v='+$scope.res_v,
			controller : 'menuModalCtrl',
			backdrop : 'static',
			size : "lg",
			keyboard : false,
			scope : scope
		});
	}


	$scope.deleteMenu = function(menu){
		$scope.confirm('Delete', 'Are you sure to delete this menu?', 'Yes', function() {
			menuService.deleteMenu(menu.menuId).then(angular.bind(this, function then() {
			var result = menuService.result;
			if (result.code == 0) {
				$scope.initPager();
			} else {
				$rootScope.message(result.msg);
			}
		}));
		}, 'No', null);
		
	}

	$scope.initPager = function(){
		$scope.page.condition.pageIndex = 1;

		$scope.getMenuList(false);
	}

	$scope.getMenuList = function(isDown) {
		if($rootScope.isOnlyCasual){
			return;
		}
		if (isDown) {
			if ($scope.page.list.length >= $scope.page.condition.totalSize) {
				return;
			}
			$scope.page.condition.pageIndex += 1;
			$scope.busy = true;
		}
		// $scope.page.condition.centerId = $rootScope.currentUser.userInfo.centersId;
		var keyWords = angular.copy($scope.page.condition.keyWords);
		$scope.showLoading("#menu_list");
		// var param = $scope.page.condition.params;
		menuService.getMenuList($scope.page.condition).then(angular.bind(this, function then() {
			var result = menuService.listResult;
			// result.data.condition.params = param;
			$scope.hideLoading("#menu_list");
			if (result.code == 0) {
				result.data.condition.keyWords = keyWords;
				$scope.dealListResult(result.data, isDown);
			} else {
				$rootScope.message(result.msg);
			}
		}));
	};

	$scope.dealListResult = function(data, isAppend) {
		// 判断是否为 追加
		if (isAppend) {
			if (!$scope.page.list) {
				$scope.page = data;
			} else {
				$scope.page.condition = data.condition;
				var length = data.list.length;
				for (var i = 0; i < length; i++) {
					// 向列表追加元素
					$scope.page.list.push(data.list[i]);
				}
			}
			$timeout(function() {
				$scope.busy = false;
			}, 2000);
		} else {
			$scope.page = data;
		}
	}

	
	// 回车键搜索
	$scope.enterSearch = function(ev) {
		if (ev.keyCode == 13) {
			// $scope.goList();
			$scope.page.condition.pageIndex = 1;
			$scope.getMenuList();
		}
	};



}
angular.module('kindKidsApp').controller('nutritionalMenuCtrl',
		[ '$scope', '$rootScope', '$timeout', '$window', '$uibModal', 'menuService', 'fileService', nutritionalMenuCtrl ]);
