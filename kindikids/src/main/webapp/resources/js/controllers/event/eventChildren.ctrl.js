'use strict';

/**
 * @author zhengguo
 * @description eventChildren Controller
 */
function eventChildrenCtrl($scope, $rootScope, $window, $timeout, $uibModal,
	commonService, eventService, fileService, $filter) {

	$scope.childId = null;
	$scope.selectModel = {};
	$scope.$parent.$parent.isGallery = true;
	$scope.page = {};
	$scope.page.condition = {
		childId: null,
		pageIndex: 1,
		avatar: '',
		personColor: "",
		name: ''
	};

	$scope.showImage = function (obj, index) {
		var title = $scope.page.condition.name + " " + $filter('date')(obj.createTime, "dd MMM yyyy");
		var list = angular.copy(obj.images);
		angular.forEach(list, function (data, index, array) {
			data.visitUrl = data.visitedUrl;
		}
		);

		$scope.visitImage(list, index, title);
	};

	$scope.initCondition = function () {
		if ($rootScope.galleryCondition.childId != null) {
			$scope.childId = $rootScope.galleryCondition.childId;
			$scope.page.condition = $rootScope.galleryCondition;
			$scope.search();
			$rootScope.galleryCondition = {
				childId: null,
				pageIndex: 1,
				avatar: '',
				personColor: "",
				name: ''
			};
		}
	};



	/*	//页面数据结构
			$scope.page.list = [
					          {
					          	describe:'',
					          	creatTime:'',
					          	images:[]
					          }
					]*/


	$scope.search = function (isDown) {
		if ($scope.childId) {
			$scope.page.condition.childId = $scope.childId;
			if ($scope.selectModel.length > 0) {
				$scope.page.condition.avatar = $scope.selectModel[0].avatar;
				$scope.page.condition.personColor = $scope.selectModel[0].personColor;
				$scope.page.condition.name = $scope.selectModel[0].text;
			}
			if (isDown) {
				// 如果当前页面中list的个数大于，等于数据库中所有记录数，则不请求后台
				if ($scope.page.list != undefined && $scope.page.list.length >= $scope.page.condition.totalSize) {
					return;
				}
				$scope.page.condition.pageIndex += 1;
				$scope.busy = true;
			} else {
				$scope.page.condition.pageIndex = 1;
			}
			eventService.getGalleryList($scope.page.condition).then(angular.bind(this, function then() {
				var result = eventService.galleryListResult;
				if (result.code == 0) {
					dealListResult(result.data, isDown);
					if (result.data.list.length == 0) {
						$scope.confirm('Message', 'No results match your search.', 'OK', function () {
							$("#privacy-modal").modal('hide');
						});
					}
				} else {
					$rootScope.message(result.msg);
				}
			}));
		} else {
			$scope.message('please choose a child');
		}
	}

	/**
	 * 处理查询结果
	 */
	var dealListResult = function (data, isAppend) {
		// 判断是否为 追加
		if (isAppend) {
			if (!$scope.page.list) {
				$scope.page = data;
			} else {
				$scope.page.condition = data.condition;
				var length = data.list.length;
				for (var i = 0; i < length; i++) {
					// 向列表追加元素
					$scope.page.list.push(data.list[i]);
				}
			}
			$scope.busy = false;
		} else {
			$scope.page.list = data.list;
			$scope.page.condition = data.condition;
		}
	}

}
angular.module('kindKidsApp').controller(
	'eventChildrenCtrl', ['$scope', '$rootScope', '$window', '$timeout', '$uibModal',
		'commonService', 'eventService', 'fileService', '$filter',
		eventChildrenCtrl
	]);