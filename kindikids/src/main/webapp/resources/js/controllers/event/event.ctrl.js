'use strict';

/**
 * @author abliu
 * @description event Controller
 */
function eventCtrl($scope, $rootScope, $window, $timeout, $uibModal,
	commonService, eventService, image_resizer, fileService, $filter) {

	// variable=================================================================
	$scope.page = {};
	$scope.page.condition = {
		pageIndex: 1,
		keyword: '',
		startTime: null,
		endTime: null,
		date: null,
		nqsVersion: ""
	};

	$scope.isGoList = false;

	// select===================================================================

	$scope.functions = {};
	$scope.getEventFun = function () {
		commonService.getFun("/event_list").then(
			angular.bind(this, function then() {
				$scope.functions = commonService.functions;
			}));
	};

	/**
	 * 获取园区信息
	 */
	$scope.getCenterList = function () {
		eventService.getCenterList().then(angular.bind(this, function then() {
			// 获取role/center/room/group list
			$scope.centerList = eventService.centerListResult;
			$scope.allCenters = {
				name: "All Centres",
				id: "allcentres"
			}
			$scope.centerList.splice(0, 0, $scope.allCenters);
		}));
	};

	/**
	 * @author abliu
	 * @description
	 * @returns 获取 EventList
	 */
	$scope.getEventList = function (isDown) {
		if ($scope.isGoList) {
			$scope.page.condition.date = null;
			$scope.page.condition.pageIndex = 1;
			$scope.isGoList = false;
		}
		if (isDown) {
			// 如果当前页面中list的个数大于，等于数据库中所有记录数，则不请求后台
			if ($scope.page.list != undefined
				&& $scope.page.list.length >= $scope.page.condition.totalSize) {
				return;
			}
			$scope.page.condition.pageIndex += 1;
			$scope.busy = true;
		}
		$scope.isGallery = false;
		if ($scope.page.condition.date == "Invalid date - Invalid date") {
			$scope.page.condition.date = "";
		}
		if ($scope.page.condition.date != undefined
			&& $scope.page.condition.date != ""
			&& $scope.page.condition.startTime == null) {
			$scope.setTime($scope.page.condition.date);
		}

		eventService.getEventList($scope.page.condition).then(
			angular.bind(this, function then() {
				var result = eventService.listResult;
				if (result.code == 0) {
					dealListResult(result.data, isDown);
					if (result.data.condition.search && result.data.list.length == 0) {
						$scope.confirm('Message', 'No results match your search.', 'OK', function () {
							$("#privacy-modal").modal('hide');
						});
					}
					$scope.page.condition.search = false;
				} else {
					$rootScope.message(result.msg);
				}
			}));
	}

	/**
	 * 处理查询结果
	 */
	var dealListResult = function (data, isAppend) {
		// 判断是否为 追加
		if (isAppend) {
			if (!$scope.page.list) {
				$scope.page = data;
			} else {
				$scope.page.condition = data.condition;
				var length = data.list.length;
				for (var i = 0; i < length; i++) {
					// 向列表追加元素
					$scope.page.list.push(data.list[i]);
				}
			}
			$scope.busy = false;
		} else {
			$scope.page.list = data.list;
			$scope.page.condition = data.condition;
		}
		setConditionDate();
	}

	function setConditionDate() {
		if ($scope.page.condition.startTime == null
			|| $scope.page.condition.endTime == null) {
			return;
		}
		var startTime = new moment($scope.page.condition.startTime,
			"YYYY-MM-DDTHH:mm:ss.SSS");
		var d = startTime._d;
		var yyyy = d.getFullYear();
		var mm = (d.getMonth() + 1) < 10 ? "0" + (d.getMonth() + 1) : d
			.getMonth() + 1;
		var dd = d.getDate() < 10 ? "0" + d.getDate() : d.getDate();
		$scope.page.condition.date = dd + "/" + mm + "/" + yyyy;

		var endTime = new moment($scope.page.condition.endTime,
			"YYYY-MM-DDTHH:mm:ss.SSS");
		d = endTime._d;
		yyyy = d.getFullYear();
		mm = (d.getMonth() + 1) < 10 ? "0" + (d.getMonth() + 1)
			: d.getMonth() + 1;
		dd = d.getDate() < 10 ? "0" + d.getDate() : d.getDate();
		$scope.page.condition.date += " - " + dd + "/" + mm + "/" + yyyy;
	}

	// 时间设置TODO
	$scope.setTime = function (value) {
		var times = value.split("-");
		if (times.length >= 2) {
			var m = new moment(times[0].trim(), "DD/MM/YYYY");
			var d = m._d;
			var yyyy = d.getFullYear();
			var mm = (d.getMonth() + 1) < 10 ? "0" + (d.getMonth() + 1) : d
				.getMonth() + 1;
			var dd = d.getDate() < 10 ? "0" + d.getDate() : d.getDate();
			var start = yyyy + "-" + mm + "-" + dd + "T00:00:00.000";
			var m = new moment(times[1].trim(), "DD/MM/YYYY");
			var d = m._d;
			var yyyy = d.getFullYear();
			var mm = (d.getMonth() + 1) < 10 ? "0" + (d.getMonth() + 1) : d
				.getMonth() + 1;
			var dd = d.getDate() < 10 ? "0" + d.getDate() : d.getDate();
			var end = yyyy + "-" + mm + "-" + dd + "T00:00:00.000";
			$scope.page.condition.startTime = start;
			$scope.page.condition.endTime = end;
		} else {
			$scope.page.condition.startTime = null;
			$scope.page.condition.endTime = null;
		}
	};

	$scope.getEventListPageIndex = function (pageIndex) {
		$scope.page.condition.pageIndex = pageIndex;
		$scope.getEventList();
	};

	$scope.rangeCallback = function (flag) {
		// 为true则清空range数据
		if (flag) {
			$scope.page.condition.startTime = null;
			$scope.page.condition.endTime = null;
			$scope.page.condition.date = "";
		}
		$scope.page.condition.pageIndex = 1;
		$scope.search();
		// $scope.goList();
	};

	$scope.enterSearch = function (ev) {
		if (ev.keyCode == 13) {
			$scope.search();
		}
	};
	$scope.search = function () {
		$scope.page.condition.search = true;
		$scope.page.condition.pageIndex = 1;
		$scope.getEventList();
		$scope.goList();
	}
	$scope.setNQS = function (value) {
		$scope.page.condition.nqsVersion = value;
		$scope.search();
	};
	$scope.newsKeyup = function (ev) {
		if (ev.keyCode == 13) {
			$scope.search();
		}
	};
	$scope.eventInfo = {
		"startTime": null,
		"endTime": null
	};

	$scope.visibilityOption = [{
		"value": 0,
		"text": "Public"
	}, {
		"value": 1,
		"text": "All Staff"
	}, {
		"value": 2,
		"text": "All Parents"
	}];

	$scope.isMyChild = false;

	/**
	 * @author abliu
	 * @description
	 * @returns 获取event信息
	 */
	$scope.getEventInfo = function () {
		$scope.isGallery = false;
		var eventId = $scope.$stateParams.eventId;
		if (eventId == 0)
			eventId = "";
		eventService
			.getEventInfo(eventId)
			.then(
				angular
					.bind(
						this,
						function then() {
							var result = eventService.eventInfo;
							if (result.code == 0) {
								$scope.eventInfo.description = result.data.description;
								$scope.eventInfo.endTime = result.data.endTime;
								$scope.eventInfo.eventDocumentVoList = result.data.eventDocumentVoList;
								$scope.eventInfo.id = result.data.id;
								$scope.eventInfo.nqsInfo = result.data.nqsInfo;
								$scope.eventInfo.startTime = result.data.startTime;
								$scope.eventInfo.title = result.data.title;
								$scope.eventInfo.visibility = result.data.visibility;
								$scope.eventInfo.centerId = result.data.centerId;
								$scope.eventInfo.createTime = result.data.createTime;
							} else {
								$rootScope.message(result.msg);
							}
						}));
	};

	/**
	 * @author abliu
	 * @description
	 * @returns 通过familyId获取childIds
	 */
	$scope.getChildId = function () {
		var familyId = $rootScope.currentUser.userInfo.familyId;
		if (familyId == null) {
			return;
		}
		eventService.getChildId(familyId).then(
			angular.bind(this, function then() {
				var result = eventService.childInfo;
				if (result.code == 0) {
					$scope.childIdsInfo = result.data;
				} else {
					$rootScope.message(result.msg);
				}
			}));
	};

	$scope.saveEvent = function () {
		var flag = false;
		$('#eventForm').data('bootstrapValidator').validate();
		$("small").map(function () {
			if ($(this).attr('data-bv-result') == "INVALID") {
				flag = true;
			}
		});
		if (flag) {
			return;
		}
		$scope.showLoading("#editEvent");
		if ($scope.eventInfo.date == "Invalid date - Invalid date") {
			$scope.eventInfo.date = "";
		}
		if ($scope.eventInfo.date != undefined && $scope.eventInfo.date != ""
			&& $scope.eventInfo.startTime == null) {
			$scope.setTimeEdit($scope.eventInfo.date);
		}
		// 校验数据合法性
		eventService.saveEvent($scope.eventInfo).then(
			angular.bind(this, function then() {
				var result = eventService.saveResult;
				if (result.code == 0) {
					$scope.goList();
				} else if (result.code == 2) {
					$scope.hideLoading("#editEvent");
					//提醒
					$rootScope.confirm('Confirm', 'The date is included in a Closure Period/Public Holiday. Are you sure to create the event?', 'Yes', function () {
						$scope.eventInfo.confirm = true;
						$scope.saveEvent();
					}, 'No', null);
				} else {
					$scope.hideLoading("#editEvent");
					$rootScope.message(result.msg);
				}
			}));
	};

	$scope.deleteEvent = function () {
		$scope.confirm('Delete', 'Are you sure?', 'Yes', function () {
			eventService.deleteEvent($scope.eventInfo.id).then(
				angular.bind(this, function then() {
					var result = eventService.deleteResult;
					if (result.code == 0) {
						// 这里要跳到列表页面
						$scope.goList();
					} else {
						$rootScope.message(result.msg);
					}
				}));
		}, 'No', null);
	}

	$scope.editRangeCallback = function (flag) {
		// 为true则清空range数据
		if (flag) {
			$scope.eventInfo.startTime = null;
			$scope.eventInfo.endTime = null;
			$scope.eventInfo.date = "";
		}
	}

	// 时间设置TODO
	$scope.setTimeEdit = function (value) {
		var times = value.split("-");
		if (times.length >= 2) {
			var m = new moment(times[0].trim(), "DD/MM/YYYY");
			var d = m._d;
			var yyyy = d.getFullYear();
			var mm = (d.getMonth() + 1) < 10 ? "0" + (d.getMonth() + 1) : d
				.getMonth() + 1;
			var dd = d.getDate() < 10 ? "0" + d.getDate() : d.getDate();
			var start = yyyy + "-" + mm + "-" + dd + "T00:00:00.000";
			var m = new moment(times[1].trim(), "DD/MM/YYYY");
			var d = m._d;
			var yyyy = d.getFullYear();
			var mm = (d.getMonth() + 1) < 10 ? "0" + (d.getMonth() + 1) : d
				.getMonth() + 1;
			var dd = d.getDate() < 10 ? "0" + d.getDate() : d.getDate();
			var end = yyyy + "-" + mm + "-" + dd + "T00:00:00.000";
			$scope.eventInfo.startTime = start;
			$scope.eventInfo.endTime = end;
		} else {
			$scope.eventInfo.startTime = null;
			$scope.eventInfo.endTime = null;
		}
	};

	/**
	 * 跳转到列表
	 */
	$scope.goEdit = function (id) {
		// 判斷是否在内部js執行go方法跳轉路由
		$rootScope.$state.go("app.main.event.edit", {
			"eventId": id
		});
		$rootScope.innerRouterChange = true;
		// 获取列表信息
	}

	/**
	 * 跳转到列表
	 */
	$scope.goList = function () {
		$rootScope.innerRouterChange = false;
		$scope.isGoList = true;
		$scope.isGallery = false;
		$rootScope.$state.go("app.main.event.list");
		$timeout(function () {
			$rootScope.innerRouterChange = true;
		}, 1000);
	}

	$scope.isGallery = true;
	/**
	 * 跳转到相册列表
	 */
	$scope.goGallery = function () {
		$rootScope.innerRouterChange = false;
		$scope.isGallery = true;
		$rootScope.$state.go("app.main.event.gallerylist");
		$timeout(function () {
			$rootScope.innerRouterChange = true;
		}, 1000);
	}

	/**
	 * @author abliu
	 * @description 获取选择的NQS
	 */
	$scope.saveNqsResult = function (newNqs) {
		$scope.eventInfo.nqsInfo = newNqs;
	};

	/**
	 * @author abliu
	 * @description
	 * @returns 获取打开编辑窗口
	 */
	$scope.openImageEdit = function (list, index) {
		$uibModal
			.open({
				templateUrl: 'views/event/editImage.html?res_v='
					+ $scope.res_v,
				controller: "eventImageCtrl",
				backdrop: 'static',
				keyboard: false,
				windowClass: 'modal fade modal-fullscreen force-fullscreen gallery-zoomin-modal gallery-with-edit',
				resolve: {
					imageList: function () {
						return list;
					},
					functions: function () {
						return $scope.functions;
					},
					index: function () {
						return index;
					},
					title: function () {
						return $scope.eventInfo.title;
					}
				}
			});

	};

	/**
	 * @author abliu
	 * @description 判断重名
	 */
	$scope.isRepeatAnnex = function () {
		var eventDocumentVoList = $scope.eventInfo.eventDocumentVoList;
		for (var i = 0, m = eventDocumentVoList.length; i < m; i++) {
			if (eventDocumentVoList[i].deleteFlag == 1)
				continue;
			for (var i = 0; i < $files.length; i++) {
				if (eventDocumentVoList[i].fileName == $files[i].name) {
					return $files[i].name;
				}
			}
		}
		;
	};

	/**
	 * @author abliu
	 * @description 文件上传
	 */
	$scope.upload = function ($files) {
		if ($files == null || $files.length == 0) {
			return;
		}
		var currentUploadLength = $files.length;
		var currentUploadedLength = 0;
		/*
		 * //判断附件是中重复 var compareResule = $scope.isRepeatAnnex($files); if
		 * (compareResule != '') { //提示文件重复 todo $.messager.popup("The file " +
		 * compareResule + " already exists."); return; }
		 */
		for (var i = 0; i < $files.length; i++) {
			var file = $files[i];
			if (file != null) {
				if (file.size < 10485760) {
					// 等待上传
					$scope.showLoading("#editEvent");

					// 转换大小
					image_resizer
						.resize({
							image: file,
							max_width: 1024
						})
						.then(
							function (data) {
								var fileStrBase64 = data.base64_resized_image;
								var fileType = data.file_type;
								var fileName = data.file_name;
								fileService
									.uploadResizeImage(fileType,
										fileStrBase64, fileName)
									.then(
										angular
											.bind(
												this,
												function then() {
													var result = fileService.fdata;
													if (result.code == 0) {

														var fileVo = {};
														fileVo.attachId = result.data.relativePathName;
														fileVo.fileName = result.data.fileName;
														fileVo.visitUrl = result.data.visitedUrl;
														fileVo.deleteFlag = 0;
														fileVo.id = "";
														fileVo.documentsId = "";

														$scope.eventInfo.eventDocumentVoList
															.splice(
																0,
																0,
																fileVo);

													} else {
														$.messager
															.popup(result.msg);
													}

													currentUploadedLength++;
													if (currentUploadedLength == currentUploadLength) {
														$scope
															.hideLoading("#editEvent");
													}
												}));
							},
							function (resize_error) {
								$scope.hideLoading("#editEvent");

								$.messager
									.popup("Sorry, the filesize is too large, please reduce the filesize and try again.");
							});
				} else {
					$.messager
						.popup("Sorry, the filesize is too large, please reduce the filesize and try again.");
				}
			}
		}
	};
	$scope.showImage = function (event, index) {
		var dateFomatter = "dd MMM yyyy";
		var startTime = $filter('date')(event.startTime, dateFomatter);
		var endTime = $filter('date')(event.endTime, dateFomatter);
		var title = startTime + " - " + endTime;
		$scope.visitImage(event.eventDocumentVoList, index, title, 1);
	};
}
angular.module('kindKidsApp').controller(
	'eventCtrl',
	['$scope', '$rootScope', '$window', '$timeout', '$uibModal',
		'commonService', 'eventService', 'image_resizer',
		'fileService', '$filter', eventCtrl]);