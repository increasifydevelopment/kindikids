'use strict';

/**
 * @author abliu
 * @description eventImageCtrl Controller
 */
function eventImageCtrl($scope, $rootScope, $window, $timeout, $uibModalInstance,$filter, commonService, eventService,imageList,functions,index,title) {

	$scope.initIndex=index;
	$scope.title=title;
	$scope.imageList=imageList;
	$scope.functions=functions;

	
	
	
	/**
	 * @author abliu
	 * @description
	 * @returns 保存imageInfo信息
	 */
	$scope.save = function() {
		//判断是否有选择tag人		
		$scope.close();
	};
	var getItem=function(){
		var item=null;
		var resultList=$filter('imageFilter')($filter('filter')(imageList,{deleteFlag:0}),$scope.childIdsInfo);
		angular.forEach(resultList, function(data,index,array){
			if($scope.initIndex==index){
				item=data;
			}
		});
		return item;
	};
	
	/**
	 * @author abliu
	 * @description
	 * @returns 保存imageInfo信息
	 */
	$scope.deleteImage = function() {
		$scope.confirm('Delete', 'Are you sure?', 'OK', function() {
			//删除
			var item=getItem();
			item.deleteFlag=1;
			$scope.close();
		}, 'NO');	
	};
      
	$scope.close1 = function() {
		$scope.confirm('Close', 'Exit this page?', 'OK', function() {
			$scope.close();
		}, 'NO');		
	};
	
	// 关闭模态框
	$scope.close = function() {
		$uibModalInstance.dismiss('cancel');		
	};

}
angular.module('kindKidsApp').controller('eventImageCtrl', ['$scope', '$rootScope', '$window', '$timeout', '$uibModalInstance', '$filter','commonService', 'eventService','imageList','functions','index','title', eventImageCtrl]);