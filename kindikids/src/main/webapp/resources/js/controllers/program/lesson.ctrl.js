'use strict';

/**
 * @author zhengguo
 * @description lesson Controller
 */
function lessonCtrl($scope, $rootScope, $timeout, $location, type, roomId, surpassListFlag, updateListFun, lessonId, obsId, functions, todayDate,
	$uibModal, $uibModalInstance, image_resizer, taskService, fileService) {
	$scope.obsId = obsId;
	$scope.functions = functions;
	// 是否刷新List页面标识
	$scope.updateListFlag = false;
	$scope.initLesson = function () {
		$scope.surpassListFlag = surpassListFlag;

		$scope.page = {};
		// 初始化,如果传入的roomId，当前为新建
		// 获取centerName roomName
		if (roomId) {
			$scope.getName(roomId);
		}

		$scope.newsInfo = {
			fileVoList: []
		};

		// 如果传入了lessonId，则表示是进入编辑页面
		if (lessonId) {
			taskService.getLessonInfo(lessonId).then(angular.bind(this, function then() {
				var result = taskService.getLessonResult;
				if (result.code == 0) {
					$scope.newsInfo.fileVoList = result.data.images;
					updateNoFileArray($scope.newsInfo.fileVoList.length);
					$scope.page = result.data.programLessonInfo;
					$scope.getName($scope.page.roomId);
					$scope.tagAccountIds = result.data.tagAccountIds;
					$scope.nqsVoList = result.data.nqsVoList;
					$scope.selecterPos = result.data.selecterPos;
				} else {
					$scope.message(result.msg);
				}
			}));
		}
	};

	$scope.obsolete = function () {
		// 判断是否有id ，调用接口去更改program状态
		if ($scope.obsId) {
			taskService.obsoleteTask($scope.obsId).then(angular.bind(this, function then() {
				var result = taskService.obsoleteTaskResult;
				$scope.message(result.msg);
				// updateListFun();
				$scope.updateListFlag = true;
			}));
		} else {
			$scope.message('Please \'Save\' before archiving');
		}
	}

	// 根据roomId获取对应的centerName 和 roomName 并把todayDate日期放进对象中
	$scope.getName = function (roomId) {
		taskService.getNameList(roomId).then(angular.bind(this, function then() {
			$scope.condition = taskService.nameResult;
			// 赋值传递进来主页面日历的当天日期
			$scope.condition.todayDate = todayDate;
		}));
	};

	$scope.noFileVoList = ["1", "2", "3", "4"];
	function updateNoFileArray(count) {
		var arr = new Array(4 - count);
		for (var i = 0; i < 4 - count; i++) {
			arr[i] = i;
		}
		$scope.noFileVoList = arr;
	}
	;

	// *********************************************************************
	// 文件上传 start
	// **********************************************************************
	/**
	 * @author abliu
	 * @description 文件上传
	 */
	$scope.upload = function ($files, newsInfoObj) {
		if ($files == null || $files.length == 0) {
			return;
		}

		if (newsInfoObj.fileVoList.length + $files.length > 4) {
			// 提示文件重复 todo
			$rootScope.message("You can only upload a maximum of 4 images.");
			return;
		}
		// 判断附件是中重复
		var compareResule = $scope.isRepeatAnnex($files, newsInfoObj);
		if (compareResule != '') {
			// 提示文件重复 todo
			$rootScope.message("Sorry, file " + compareResule + " already exists, please try a different filename.");
			return;
		}
		$scope.nowFilesLength = $files.length + newsInfoObj.fileVoList.length;
		for (var i = 0; i < $files.length; i++) {
			var file = $files[i];
			if (file != null) {
				if (file.size < 10485760) {
					// 等待上传
					$scope.showLoading("#loadingMain");
					// 转换大小
					image_resizer.resize({
						image: file,
						max_width: 1024
					}).then(function (data) {
						var fileStrBase64 = data.base64_resized_image;
						var fileType = data.file_type;
						var fileName = data.file_name;
						fileService.uploadResizeImage(fileType, fileStrBase64, fileName).then(angular.bind(this, function then() {
							var result = fileService.fdata;
							if (result.code == 0) {
								var length = newsInfoObj.fileVoList.length;
								var fileVo = {};
								fileVo.relativePathName = result.data.relativePathName;
								fileVo.fileName = result.data.fileName;
								fileVo.visitedUrl = result.data.visitedUrl;
								newsInfoObj.fileVoList.push(fileVo);
								// 更新没图片的集合
								updateNoFileArray($scope.newsInfo.fileVoList.length);
								$('.imageUpload').addClass('slideDown');
							} else {
								$rootScope.message(result.msg);
							}
							if (newsInfoObj.fileVoList.length == $scope.nowFilesLength) {
								$scope.hideLoading("#loadingMain");
							}
						}));
					}, function (resize_error) {
						$scope.hideLoading("#loadingMain");
						$rootScope.message("Sorry, the filesize is too large, please reduce the filesize and try again.");
					});
				} else {
					$rootScope.message("Sorry, the filesize is too large, please reduce the filesize and try again.");
				}
			}
		}
	};

	/**
	 * @author abliu
	 * @description 删除附件
	 */
	$scope.removeFile = function (fileName, newsInfoObj) {
		for (var i = 0, m = newsInfoObj.fileVoList.length; i < m; i++) {
			if (newsInfoObj.fileVoList[i].relativePathName == fileName) {
				newsInfoObj.fileVoList.splice(i, 1);
				// 更新没图片的集合
				updateNoFileArray($scope.newsInfo.fileVoList.length);
				if ($scope.newsInfo.fileVoList.length == 0) {
					$('.imageUpload').removeClass('slideDown');
				}
				return;
			}
		}
	};

	/**
	 * @author abliu
	 * @description 判断附件是否重复
	 */
	$scope.isRepeatAnnex = function ($files, newsInfoObj) {
		var annxs = "";
		if (newsInfoObj.fileVoList == undefined || newsInfoObj.fileVoList.length == 0) {
			return "";
		}
		for (var j = 0, m = newsInfoObj.fileVoList.length; j < m; j++) {
			for (var i = 0; i < $files.length; i++) {
				if (newsInfoObj.fileVoList[j].fileName == $files[i].name) {
					return $files[i].name;
				}
			}
		}
		return annxs;
	};

	// *********************************************************************
	// 文件上传 end
	// **********************************************************************

	$scope.updateInfo = function (needShareFlag) {
		$('#editLessonForm').data('bootstrapValidator').validate();
		if ($('#editLessonForm').data('bootstrapValidator').isValid()) {
			$scope.showLoading("#loadingMain");
			$scope.page.roomId = roomId;

			// 添加images
			var allInfo = {};
			allInfo.programLessonInfo = $scope.page;
			allInfo.images = $scope.newsInfo.fileVoList;
			// 赋值NQS
			allInfo.nqsVoList = $scope.nqsVoList;
			// 赋值Tags
			allInfo.tagAccountIds = $scope.tagAccountIds;

			taskService.updateLessonInfo(allInfo, todayDate).then(angular.bind(this, function then() {
				var result = taskService.lessonResult;
				if (result.code == 0) {
					if (!$scope.obsId) {
						$scope.obsId = result.data.temp1;
					}
					// 重新复制id，方便后续操作比如删除
					$scope.page.id = result.data.id;
					$scope.page.newsfeedId = result.data.newsfeedId;
					// 回调更新页面List
					// updateListFun();
					$scope.updateListFlag = true;
					if (!needShareFlag) {
						$scope.message('update lesson success');
					} else {
						// 给个状态控制是否在保存返回后调用分享的方法
						$rootScope.confirm('Save and Push to Newsfeed', '', 'Share', $scope.ok, 'Cancel');
					}
				} else {
					$scope.message(result.msg);
				}
				$scope.hideLoading("#loadingMain");
			}));
		}
	};

	$scope.delInfo = function () {
		$scope.confirmSuccess = function () {
			if ($scope.page.id) {
				taskService.delLessonInfo($scope.page.id).then(angular.bind(this, function then() {
					var result = taskService.delLessonResult;
					if (result.code == 0) {
						// 回调更新页面List
						updateListFun();
						$scope.message(result.msg);
						// 关闭modal
						$uibModalInstance.dismiss('cancel');
					} else {
						$scope.message(result.msg);
					}
				}));
			} else {
				$scope.cancel();
			}
		};

		$rootScope.confirm('Warning', 'Are you sure you want to delete?', 'YES', $scope.confirmSuccess, 'NO');
	};

	$scope.cancel = function () {
		$scope.confirmSuccess = function () {
			$uibModalInstance.dismiss('cancel');
			if ($scope.updateListFlag) {
				updateListFun();
			}
		};
		$rootScope.confirm('Close', 'Exit this page?', 'OK', $scope.confirmSuccess, 'NO');
	};

	// 分享到newsfeed
	/**
	 * @author zhengguo
	 * @description 打开share to newsfeed编辑框
	 * @returns 保存
	 */
	$scope.shareNewfeed = function (type) {
		// 调用保存方法
		$scope.updateInfo(true);
	};
	$scope.ok = function () {
		//防止多次点击
		$scope.showLoading("#loadingMain");
		$scope.programNewsFeedVo = {};
		$scope.programNewsFeedVo.nqsVoList = $scope.nqsVoList;
		$scope.programNewsFeedVo.roomId = roomId;
		$scope.programNewsFeedVo.newsId = !$scope.page ? "" : $scope.page.newsfeedId;
		$scope.programNewsFeedVo.taskType = 2;
		$scope.programNewsFeedVo.actList = [{
			name: $scope.page.lessonName
		}];
		$scope.programNewsFeedVo.accounts = $scope.tagAccountIds;
		$scope.programNewsFeedVo.content = !$scope.page ? "" : $scope.page.reflection;
		taskService.shareNewsfeed($scope.programNewsFeedVo, !$scope.page ? "" : $scope.page.id).then(angular.bind(this, function then() {
			var result = taskService.shareResult;
			if (result.code == 0) {
				// 关闭modal
				$scope.message('Successfully shared to the newsfeed');
			} else {
				$scope.message(result.msg);
			}
			$scope.shareFlag = false;
			$scope.hideLoading("#loadingMain");
		}));
	};
	function initNewsFeedId(newsId) {
		$scope.page.newsfeedId = newsId;
	}
	$scope.nqsVoList = [];
	$scope.saveNqsResult = function (newNqs) {
		$scope.nqsVoList = newNqs;
	};
}
angular.module('kindKidsApp').controller(
	'lessonCtrl',
	['$scope', '$rootScope', '$timeout', '$location', 'type', 'roomId', 'surpassListFlag', 'updateListFun', 'lessonId', 'obsId', 'functions',
		'todayDate', '$uibModal', '$uibModalInstance', 'image_resizer', 'taskService', 'fileService', lessonCtrl]);