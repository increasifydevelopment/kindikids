'use strict';

/**
 * @author zhengguo
 * @description medication program Controller
 */
function medicationCtrl($scope, $rootScope, $timeout, $location, type, roomId, todayDate, surpassListFlag, updateListFun, functions, medicationId,
		obsId, $uibModal, $uibModalInstance, image_resizer, taskService, fileService, isParent) {
	$scope.functions = functions;
	$scope.obsId = obsId;
	$scope.signaturePad2 = [];
	$scope.signaturePad3 = [];
	$scope.isParent=isParent;
	// 是否刷新List页面标识
	$scope.updateListFlag = false;

	function initListSigna() {
		for (var i = 1; i <= $scope.pageInfo.timeMedicationInfos.length; i++) {
			var wrapper2 = document.getElementById("signature-pad-" + i);
			var clearButton2 = wrapper2.querySelector("[data-action=AdministeringClear" + i + "]");
			var canvas2 = wrapper2.querySelector("canvas");

			var wrapper3 = document.getElementById("signature-Checking-" + i);
			var clearButton3 = wrapper3.querySelector("[data-action=CheckingClear" + i + "]");
			var canvas3 = wrapper3.querySelector("canvas");

			$scope.resizeCanvas = function(canvas) {
				$timeout(function() {
					var ratio = Math.max(window.devicePixelRatio || 1, 1);
					canvas.width = canvas.offsetWidth * ratio;
					canvas.height = canvas.offsetHeight * ratio;
					canvas.getContext("2d").scale(ratio, ratio);
				});
			};

			$scope.resizeCanvas(canvas2);
			$scope.signaturePad2[i - 1] = new SignaturePad(canvas2);
			clearButton2.addEventListener("click", function(event) {
				$scope.signaturePad2[i - 1].clear();
			});

			$scope.resizeCanvas(canvas3);
			$scope.signaturePad3[i - 1] = new SignaturePad(canvas3);
			clearButton3.addEventListener("click", function(event) {
				$scope.signaturePad3[i - 1].clear();
			});

			$scope.signaturePad2[i - 1].fromDataURL($scope.pageInfo.timeMedicationInfos[i - 1].staffAdministeringSignature);
			$scope.signaturePad3[i - 1].fromDataURL($scope.pageInfo.timeMedicationInfos[i - 1].staffChecking);

		}
	}

	$scope.initMedic = function() {
		$timeout(function() {
			initListSigna();
		}, 200);
	}

	$scope.initMedication = function() {
		$scope.room = {};
		if (roomId) {
			$scope.room.orgId1 = roomId;
		}
		$scope.condition = {};

		$scope.todayDate = todayDate;

		$scope.surpassListFlag = surpassListFlag;

		$scope.page = {
			type : type,
			roomId : roomId
		}

		// 获取对象
		taskService
				.getMedication(medicationId)
				.then(
						angular
								.bind(
										this,
										function then() {
											var result = taskService.medicationResult;
											if (result.code == 0) {
												// 重新复制id，方便后续操作比如删除
												$scope.pageInfo = result.data;
												if (result.data.programMedicationInfoWithBLOBs && result.data.programMedicationInfoWithBLOBs.roomId) {
													$scope.room.orgId1 = result.data.programMedicationInfoWithBLOBs.roomId;
												}
												// 处理下时间格式
												if ($scope.pageInfo.programMedicationInfoWithBLOBs.day) {
													$scope.pageInfo.programMedicationInfoWithBLOBs.day = $scope.pageInfo.programMedicationInfoWithBLOBs.day
															.substr(8, 2)
															+ '/'
															+ $scope.pageInfo.programMedicationInfoWithBLOBs.day.substr(5, 2)
															+ '/'
															+ $scope.pageInfo.programMedicationInfoWithBLOBs.day.substr(0, 4);
												}
												if ($scope.pageInfo.programMedicationInfoWithBLOBs.expiryDate) {
													$scope.pageInfo.programMedicationInfoWithBLOBs.expiryDate = $scope.pageInfo.programMedicationInfoWithBLOBs.expiryDate
															.substr(8, 2)
															+ '/'
															+ $scope.pageInfo.programMedicationInfoWithBLOBs.expiryDate.substr(5, 2)
															+ '/'
															+ $scope.pageInfo.programMedicationInfoWithBLOBs.expiryDate.substr(0, 4);
												}

												// 处理吃药的时间
												for (var i = 0; i < $scope.pageInfo.timeMedicationInfos.length; i++) {
													$scope.pageInfo.timeMedicationInfos[i].day = $scope
															.changeBackTime($scope.pageInfo.timeMedicationInfos[i].day);
													$scope.pageInfo.timeMedicationInfos[i].administeringValue.value = $scope.pageInfo.timeMedicationInfos[i].administeringValue.text;
													$scope.pageInfo.timeMedicationInfos[i].checkingValue.value = $scope.pageInfo.timeMedicationInfos[i].checkingValue.text;

												}

												// 处理签到信息展示
												$scope.signaturePad0.fromDataURL($scope.pageInfo.programMedicationInfoWithBLOBs.parentSignatureText);

												// 处理
												// $scope.pageInfo.twoAdminister
												$scope.pageInfo.twoAdminister = [];
												$scope.pageInfo.twoAdminister
														.push($scope.pageInfo.programMedicationInfoWithBLOBs.mustAssignAccountId1);
												$scope.pageInfo.twoAdminister
														.push($scope.pageInfo.programMedicationInfoWithBLOBs.mustAssignAccountId2);

											} else {
												$scope.message(result.msg);
											}
										}));

		// **********************************************************************************
		// 初始化signature start
		// **********************************************************************************
		var wrapper0 = document.getElementById("signature-pad-0");
		var clearButton0 = wrapper0.querySelector("[data-action=clear0]");
		var canvas0 = wrapper0.querySelector("canvas");

		// for(var i = 0; i < $scope.pageInfo.timeMedicationInfos.length; i++){
		// var wrapper2 = document.getElementById("signature-Administering-"+i);
		// var clearButton2 =
		// wrapper2.querySelector("[data-action=AdministeringClear"+i+"]");
		// var canvas2 = wrapper2.querySelector("canvas");
		//			
		// var wrapper3 = document.getElementById("signature-Checking-"+i);
		// var clearButton3 =
		// wrapper3.querySelector("[data-action=CheckingClear"+i+"]");
		// var canvas3 = wrapper3.querySelector("canvas");
		//			
		// $scope.resizeCanvas(canvas2);
		// $scope.signaturePad2 = new SignaturePad(canvas2);
		// clearButton2.addEventListener("click", function(event) {
		// $scope.signaturePad2.clear();
		// });
		//
		// $scope.resizeCanvas(canvas3);
		// $scope.signaturePad3 = new SignaturePad(canvas3);
		// clearButton3.addEventListener("click", function(event) {
		// $scope.signaturePad3.clear();
		// });
		// }

		// var wrapper2 = document.getElementById("signature-pad-2");
		// var clearButton2 = wrapper2.querySelector("[data-action=clear2]");
		// var canvas2 = wrapper2.querySelector("canvas");
		//
		// var wrapper3 = document.getElementById("signature-pad-3");
		// var clearButton3 = wrapper3.querySelector("[data-action=clear3]");
		// var canvas3 = wrapper3.querySelector("canvas");

		$scope.resizeCanvas = function(canvas) {
			$timeout(function() {
				var ratio = Math.max(window.devicePixelRatio || 1, 1);
				canvas.width = canvas.offsetWidth * ratio;
				canvas.height = canvas.offsetHeight * ratio;
				canvas.getContext("2d").scale(ratio, ratio);
			});
		};

		// 监控签名change事件
		/*
		 * $scope.signaturePad1.onEnd = function(){
		 * $('#childSigninSignForm').bootstrapValidator('updateStatus','childSigninSignature',
		 * 'VALID'); };
		 */

		$scope.resizeCanvas(canvas0);
		$scope.signaturePad0 = new SignaturePad(canvas0);
		clearButton0.addEventListener("click", function(event) {
			$scope.signaturePad0.clear();
		});

		// **********************************************************************************
		// 初始化signature end
		// **********************************************************************************
	};

	$scope.obsolete = function() {
		// 判断是否有id ，调用接口去更改program状态
		if ($scope.obsId) {
			taskService.obsoleteTask($scope.obsId).then(angular.bind(this, function then() {
				var result = taskService.obsoleteTaskResult;
				$scope.message(result.msg);
				// updateListFun();
				$scope.updateListFlag = true;
			}))
		} else {
			$scope.message('Please \'Save\' before archiving');
		}
	}

	$scope.addTime = function() {
		var data = {
			day : '',
			chooseFlag : false,
			administeringValue : {},
			checkingValue : {}
		}
		$scope.pageInfo.timeMedicationInfos.push(data);
	}

	$scope.delTime = function(itemIndex) {
		$scope.pageInfo.timeMedicationInfos.splice(itemIndex, 1);
	}

	$scope.updateInfo = function() {
		$('#editMedicationForm').data('bootstrapValidator').validate();
		if ($('#editMedicationForm').data('bootstrapValidator').isValid()) {
			$scope.showLoading("#medicationId");
			var saveInfo = angular.copy($scope.pageInfo);
			if (!$scope.signaturePad0.isEmpty()) {
				saveInfo.programMedicationInfoWithBLOBs.parentSignatureText = $scope.signaturePad0.toDataURL();
			}
			for (var i = 0; i < $scope.pageInfo.timeMedicationInfos.length; i++) {
				if ($scope.pageInfo.timeMedicationInfos[i].administeringValue.value) {
					saveInfo.timeMedicationInfos[i].administeringValue.text = $scope.pageInfo.timeMedicationInfos[i].administeringValue.value;
				}
				if ($scope.pageInfo.timeMedicationInfos[i].checkingValue.value) {
					saveInfo.timeMedicationInfos[i].checkingValue.text = $scope.pageInfo.timeMedicationInfos[i].checkingValue.value;
				}
			}

			// 处理时间格式 11：12 PM 并将未填时间的清空
			var nDate = new Date();
			var nowMonth = nDate.getMonth() < 9 ? '0' + (nDate.getMonth() + 1) : (nDate.getMonth() + 1);
			var nowDate = nDate.getFullYear() + '-' + nowMonth + '-' + nDate.getDate();
			for (var i = 0; i < saveInfo.timeMedicationInfos.length; i++) {
				if (saveInfo.timeMedicationInfos[i].day) {
					saveInfo.timeMedicationInfos[i].day = nowDate + $scope.changeHeadTime(saveInfo.timeMedicationInfos[i].day);
				} else {
					saveInfo.timeMedicationInfos.splice(i, 1);
					i--;
				}
			}

			// 处理绑定的2个staffid pageInfo.twoAdminister
			saveInfo.programMedicationInfoWithBLOBs.mustAssignAccountId1 = $scope.pageInfo.twoAdminister[0];
			saveInfo.programMedicationInfoWithBLOBs.mustAssignAccountId2 = $scope.pageInfo.twoAdminister[1];
			// 保存roomId
			saveInfo.programMedicationInfoWithBLOBs.roomId = roomId;

			taskService.saveMedication(saveInfo, todayDate).then(angular.bind(this, function then() {
				var result = taskService.saveMedicationResult;
				if (result.code == 0) {
					if (!$scope.obsId) {
						$scope.obsId = result.data.temp1;
					}
					// 重新复制id，方便后续操作比如删除
					$scope.pageInfo.programMedicationInfoWithBLOBs.id = result.data.programMedicationInfoWithBLOBs.id;
					// 回调页面list刷新数据
					// updateListFun();
					$scope.updateListFlag = true;
					$scope.message('update success');
				} else {
					$scope.message(result.msg);
				}
				$scope.hideLoading("#medicationId");
			}));
		}
	}

	$scope.cancel = function() {
		$scope.confirmSuccess = function() {
			$uibModalInstance.dismiss('cancel');
			if ($scope.updateListFlag) {
				updateListFun();
			}
		}
		$rootScope.confirm('Close', 'Exit this page?', 'OK', $scope.confirmSuccess, 'NO');
	};

}
angular.module('kindKidsApp').controller(
		'medicationCtrl',
		[ '$scope', '$rootScope', '$timeout', '$location', 'type', 'roomId', 'todayDate', 'surpassListFlag', 'updateListFun', 'functions',
				'medicationId', 'obsId', '$uibModal', '$uibModalInstance', 'image_resizer', 'taskService', 'fileService','isParent', medicationCtrl ]);