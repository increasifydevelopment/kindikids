'use strict';

/**
 * @author zhengguo
 * @description other task Controller
 */
function otherTaskCtrl($scope, $rootScope, $timeout, $location, isIncRep, roomId, type, chooseDate, reloadPage, $uibModal, $uibModalInstance,
		taskService) {

	$scope.taskList = [];
	$scope.choooseTaskId = '';
	$scope.isIncRep = isIncRep;
	$scope.initSelectData = function() {
		if ($scope.isIncRep) {
			taskService.getWhenRequiredList2().then(angular.bind(this, function then() {
				var result = taskService.whenTaskList;
				$scope.taskList = result.data;
				$scope.choooseTaskId = $scope.taskList[0].id;
			}));
		} else {
			taskService.getWhenRequiredList().then(angular.bind(this, function then() {
				var result = taskService.whenTaskList;
				$scope.taskList = result.data;
			}));
		}
	};

	$scope.isRoomFlag = false;
	$scope.selectList = [];
	$scope.mySelect = [];
	$scope.$watch('choooseTaskId', function(newValue) {
		if (!newValue) {
			return;
		}
		var taskId = newValue;
		taskService.getWhenSelect(taskId).then(angular.bind(this, function then() {
			var result = taskService.whenSelects;
			$scope.selectList = result.data;
			if (result.msg * 1 == 1) {
				$scope.isRoomFlag = true;
			} else {
				$scope.isRoomFlag = false;
			}
		}));
	});

	var saveList = [];
	function initSaveList() {
		var tmpSelect = angular.copy($scope.selectList);
		if ($scope.isRoomFlag) {
			var arr = [];
			angular.forEach(tmpSelect, function(data, index, array) {
				arr = arr.concat(data.children);
			});
			tmpSelect = arr;
		}
		angular.forEach(tmpSelect, function(data, index, array) {
			angular.forEach($scope.mySelect, function(chooseId, index, array) {
				if (data && data.id == chooseId) {
					saveList.push({
						id : chooseId,
						type : data.type
					});
				}
			});
		});
	}

	$scope.submit = function() {
		$scope.showLoading("#main");
		initSaveList();
		taskService.saveWhenTask($scope.choooseTaskId, saveList, chooseDate).then(angular.bind(this, function then() {
			var result = taskService.saveWhenResult;
			if (result.code == 0) {
				reloadPage();
				$uibModalInstance.dismiss('cancel');
			}
			$scope.hideLoading("#main");
			$scope.message(result.msg);
		}));
	};
	$scope.cancel = function() {
		$uibModalInstance.dismiss('cancel');
	};
	$scope.close = function() {
		$scope.confirm('Close', 'Exit this page?', 'OK', function() {
			$scope.cancel();
		}, 'NO');
	};

}
angular.module('kindKidsApp').controller(
		'otherTaskCtrl',
		[ '$scope', '$rootScope', '$timeout', '$location', 'isIncRep', 'roomId', 'type', 'chooseDate', 'reloadPage', '$uibModal',
				'$uibModalInstance', 'taskService', otherTaskCtrl ]);