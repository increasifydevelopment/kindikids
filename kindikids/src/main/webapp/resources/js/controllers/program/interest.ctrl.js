'use strict';

/**
 * @author zhengguo
 * @description interest /observation/ learning story Controller
 */
function interestCtrl($scope, $rootScope, $timeout, $location, type, todayDate, surpassListFlag, updateListFun, roomId, functions, intobsleaId,
		obsId, isDisable, $uibModal, $uibModalInstance, image_resizer, obj, taskService, fileService) {
	$scope.functions = functions;
	// 是否刷新List页面标识
	$scope.updateListFlag = false;
	// followup点击链接过来的不能编辑小孩
	$scope.isDisable = isDisable;
	$scope.initInterest = function() {
		$scope.templates = [ {
			id : 0,
			name : 'Explore Curriculum'
		}, {
			id : 1,
			name : 'Grow Curriculum'
		}, {
			id : 3,
			name : 'School Readiness'
		} ];

		$scope.surpassListFlag = surpassListFlag;

		// select2 ajax url 参数
		if (roomId) {
			$scope.orgId = roomId;
			$scope.getName(roomId);
		}
		// 页面图片集合
		$scope.newsInfo = {
			fileVoList : []
		};

		// 如果传入了intobsleaId，则表示是进入编辑页面
		if (intobsleaId) {
			// 定义禁用task的id
			$scope.obsId = obsId;
			taskService.getInObLeInfo(intobsleaId).then(
					angular.bind(this, function then() {
						var result = taskService.inObLeResult;
						if (result.code == 0) {
							$scope.newsInfo.fileVoList = result.data.images;
							updateNoFileArray($scope.newsInfo.fileVoList.length);
							$scope.page = result.data.programIntobsleaInfo;
							// select2 ajax url 参数
							$scope.orgId = $scope.page.roomId;
							// 获取name
							$scope.getName($scope.page.roomId);
							// 手动创建一个选择child的备选数组，初始化时候使用
							var data = {};
							var arr = [];
							data.avatar = result.data.childAvatar;
							data.id = $scope.page.childId;
							data.personColor = result.data.personColor;
							data.selected = true;
							data.text = result.data.text;
							arr.push(data);
							$scope.condition.childInfo1 = arr;

							// 处理下时间格式
							$scope.page.evaluationDate = $scope.page.evaluationDate.substr(8, 2) + '/' + $scope.page.evaluationDate.substr(5, 2)
									+ '/' + $scope.page.evaluationDate.substr(0, 4);
						} else {
							$scope.message(result.msg);
						}
					}));
		} else {
			// type 4 : interest / 6 : observation / 8 : learningStory
			$scope.page = {
				type : type,
				roomId : roomId,
			};
		}
	};

	$scope.obsolete = function(type) {
		// 判断是否有id ，调用接口去更改program状态
		if ($scope.obsId) {
			taskService.obsoleteTask($scope.obsId).then(angular.bind(this, function then() {
				var result = taskService.obsoleteTaskResult;
				$scope.message(result.msg);
				// updateListFun();
				$scope.updateListFlag = true;
			}))
		} else {
			if (type == 4) {
				$scope.message('Please \'Save\' before archiving');
			} else if (type == 6) {
				$scope.message('Please \'Save\' before archiving');
			} else if (type == 8) {
				$scope.message('Please \'Save\' before archiving');
			}

		}
	}

	// 根据roomId获取对应的centerName 和 roomName 并把todayDate日期放进对象中
	$scope.getName = function(roomId) {
		taskService.getNameList(roomId).then(angular.bind(this, function then() {
			$scope.nameInfo = taskService.nameResult;
			// 赋值传递进来主页面日历的当天日期
			$scope.nameInfo.todayDate = todayDate;
		}))
	}

	$scope.condition = {};
	// 转换select2-model选择的小孩
	$scope.$watch('condition.childInfo1', function(newVal, oldVal) {
		if (newVal) {
			$scope.condition.childInfo = newVal[0];
			if ($scope.condition.childInfo) {
				if ($scope.condition.childInfo.personColor == null) {
					$scope.condition.childInfo.personColor = 'blue';
				}
			}
		}
	});

	// *********************************************************************
	// 文件上传 start
	// **********************************************************************

	$scope.noFileVoList = [ "1" ];
	function updateNoFileArray(count) {
		var arr = new Array(1 - count);
		for (var i = 0; i < 1 - count; i++) {
			arr[i] = i;
		}
		$scope.noFileVoList = arr;
	}
	;

	/**
	 * @author abliu
	 * @description 文件上传
	 */
	$scope.upload = function($files, newsInfoObj) {
		if ($files == null || $files.length == 0) {
			return;
		}

		if (newsInfoObj.fileVoList.length + $files.length > 1) {
			// 提示文件重复 todo
			$rootScope.message("Number of images is not greater than 1.");
			return;
		}
		// 判断附件是中重复
		var compareResule = $scope.isRepeatAnnex($files, newsInfoObj);
		if (compareResule != '') {
			// 提示文件重复 todo
			$rootScope.message("Sorry, file " + compareResule + " already exists, please try a different filename.");
			return;
		}
		for (var i = 0; i < $files.length; i++) {
			var file = $files[i];
			if (file != null) {
				if (file.size < 10485760) {
					// 等待上传
					$scope.showLoading("#InterChangeSet");
					// 转换大小
					image_resizer.resize({
						image : file,
						max_width : 1024
					}).then(function(data) {
						var fileStrBase64 = data.base64_resized_image;
						var fileType = data.file_type;
						var fileName = data.file_name;
						fileService.uploadResizeImage(fileType, fileStrBase64, fileName).then(angular.bind(this, function then() {
							var result = fileService.fdata;
							if (result.code == 0) {
								var length = newsInfoObj.fileVoList.length;
								var fileVo = {};
								fileVo.relativePathName = result.data.relativePathName;
								fileVo.fileName = result.data.fileName;
								fileVo.visitedUrl = result.data.visitedUrl;
								newsInfoObj.fileVoList.push(fileVo);
								// 更新没图片的集合
								updateNoFileArray($scope.newsInfo.fileVoList.length);
								// 展示imageui
								$('.imageUpload').addClass('slideDown');
							} else {
								$rootScope.message(result.msg);
							}
							$scope.hideLoading("#InterChangeSet");
						}));
					}, function(resize_error) {
						$scope.hideLoading("#InterChangeSet");
						$rootScope.message("Sorry, the filesize is too large, please reduce the filesize and try again.");
					});
				} else {
					$rootScope.message("Sorry, the filesize is too large, please reduce the filesize and try again.");
				}
			}
		}
	};

	/**
	 * @author abliu
	 * @description 删除附件
	 */
	$scope.removeFile = function(fileName, newsInfoObj) {
		for (var i = 0, m = newsInfoObj.fileVoList.length; i < m; i++) {
			if (newsInfoObj.fileVoList[i].relativePathName == fileName) {
				newsInfoObj.fileVoList.splice(i, 1);
				// 更新没图片的集合
				updateNoFileArray($scope.newsInfo.fileVoList.length);
				if ($scope.newsInfo.fileVoList.length == 0) {
					$('.imageUpload').removeClass('slideDown');
				}
				return;
			}
		}
	};

	/**
	 * @author abliu
	 * @description 判断附件是否重复
	 */
	$scope.isRepeatAnnex = function($files, newsInfoObj) {
		var annxs = "";
		if (newsInfoObj.fileVoList == undefined || newsInfoObj.fileVoList.length == 0) {
			return "";
		}
		for (var j = 0, m = newsInfoObj.fileVoList.length; j < m; j++) {
			for (var i = 0; i < $files.length; i++) {
				if (newsInfoObj.fileVoList[j].fileName == $files[i].name) {
					return $files[i].name;
				}
			}
		}
		return annxs;
	};

	// *********************************************************************
	// 文件上传 end
	// **********************************************************************

	$scope.updateInfo = function(confirm) {
		confirm = !confirm ? false : true;
		$('#editInterestForm').data('bootstrapValidator').validate();
		var flag = false;
		$("small").map(function() {
			if ($(this).attr('data-bv-result') == "INVALID" && !$($(this).parent().parent()).hasClass('ng-hide')) {
				flag = true;
			}
		});
		if (flag) {
			return;
		}
		$scope.showLoading("#InterChangeSet");
		// 添加images
		var allInfo = {};
		allInfo.programIntobsleaInfo = $scope.page;
		allInfo.images = $scope.newsInfo.fileVoList;
		taskService.saveInObLeInfo(allInfo, todayDate, confirm).then(angular.bind(this, function then() {
			var result = taskService.inObLeInfoResult;
			if (result.code == 0) {
				// 重新复制id，方便后续操作比如删除
				$scope.page.id = result.data.id;
				$scope.page.newsfeedId = result.data.newsfeedId;
				// 赋值禁用task的id 新增的时候赋值
				if (!$scope.obsId) {
					$scope.obsId = result.data.temp1;
				}
				// 回调更新页面List
				// updateListFun();
				$scope.updateListFlag = true;
				if ($scope.needShareFlag && $scope.page.id) {
					$scope.needShareFlag = false;
					$scope.updateListFlag = false;
					$scope.shareFlag = true;
					var dataInfo = {
						taskType : type,
						contentTip0 : $scope.page.observation ? $scope.page.observation : '',
						contentTip1 : $scope.page.learning ? $scope.page.learning : '',
						contentTip2 : $scope.page.objective ? $scope.page.objective : '',
						contentTip3 : $scope.page.educatorExperience ? $scope.page.educatorExperience : '',
						accounts : $.isArray($scope.page.childId) ? $scope.page.childId : new Array($scope.page.childId),
						roomId : roomId,
						newsId : $scope.page.newsfeedId ? $scope.page.newsfeedId : null,
						schoolFlag : $scope.page.category
					};
					taskService.shareNewsfeed(dataInfo, $scope.page.id).then(angular.bind(this, function then() {
						var result = taskService.shareResult;
						// updateListFun();
						$scope.updateListFlag = true;
						// 赋值newsfeedId
						$scope.message('Successfully shared to the newsfeed');
						$scope.page.newsfeedId = result.data;
						$scope.hideLoading("#InterChangeSet");
					}));
					$scope.shareFlag = false;
				} else {
					$scope.message('update success');
					$scope.hideLoading("#InterChangeSet");
				}
				// 更新成功,如果是从子模态框过来的则刷新子模态框
				if (isDisable) {
					obj.refresh();
				}
			} else if (result.code == 2) {
				$scope.confirm('Confirm', result.msg, 'OK', function() {
					$scope.updateInfo(true);
				}, 'NO');
				$scope.hideLoading("#InterChangeSet");
			} else {
				$scope.message(result.msg);
				$scope.hideLoading("#InterChangeSet");
			}
		}));
	}

	$scope.delInfo = function() {
		$scope.confirmSuccess = function() {
			if ($scope.page.id) {
				taskService.delInObLeInfo($scope.page.id).then(angular.bind(this, function then() {
					var result = taskService.delInObLeResult;
					if (result.code == 0) {
						// 回调更新页面List
						updateListFun();
						// 关闭modal
						$scope.message(result.msg);
						$uibModalInstance.dismiss('cancel');
						// 删除成功,如果是从子模态框过来的则关闭子模态框
						if (isDisable) {
							obj.canalCurrent();
						}
					} else {
						$scope.message(result.msg);
					}
				}));
			} else {
				$scope.cancel();
			}
		};

		if ($scope.page.id) {
			taskService.hasFollowUp($scope.page.id).then(
					angular.bind(this, function then() {
						var result = taskService.hasResult;
						if (result.data == true) {
							$rootScope.confirm('Warning', 'Your follow up will be deleted, are you sure you want to delete?', 'YES',
									$scope.confirmSuccess, 'NO');
						} else {
							$rootScope.confirm('Warning', 'Are you sure you want to delete?', 'YES', $scope.confirmSuccess, 'NO');
						}
					}));
		} else {
			$scope.cancel();
		}

	}

	$scope.cancel = function() {
		$scope.confirmSuccess = function() {
			$uibModalInstance.dismiss('cancel');
			if ($scope.updateListFlag && !isDisable) {
				updateListFun();
			}
		}
		$rootScope.confirm('Close', 'Exit this page?', 'OK', $scope.confirmSuccess, 'NO');
	};

	// 分享到newsfeed
	/**
	 * @author zhengguo 直接到后台去保存 传值： 页面对象
	 * @returns
	 */

	$scope.shareFlag = false;

	$scope.shareNewfeed = function() {
		if ($scope.shareFlag) {
			return;
		}
		// 调用保存方法，先保存
		$scope.updateInfo();
		// 给个状态控制是否在保存返回后调用分享的方法
		$scope.needShareFlag = true;
	};

}

angular.module('kindKidsApp').controller(
		'interestCtrl',
		[ '$scope', '$rootScope', '$timeout', '$location', 'type', 'todayDate', 'surpassListFlag', 'updateListFun', 'roomId', 'functions',
				'intobsleaId', 'obsId', 'isDisable', '$uibModal', '$uibModalInstance', 'image_resizer', 'obj', 'taskService', 'fileService',
				interestCtrl ]);