'use strict';

/**
 * @author gfwang
 * @description task form
 */
function viewMeetingTaskCtrl($scope, $rootScope, $timeout, $location, $uibModal, $uibModalInstance, obj, taskService) {

	$scope.state=obj.state;
	
	$scope.functions=obj.functions;
	
	$scope.isAfterToday=obj.isAfterToday;
	
	$scope.initTable=function(){
		if($scope.state==1){
			$scope.disabled=true;
		}
		taskService.getMeetingRow(obj.rowId).then(angular.bind(this, function then() {
			var result = taskService.meetingRowResult;
			if(result.code==0){
				$scope.list=result.data;
			}else{
				$scope.message(result.msg);
			}
		}));
	};
	
	
	$scope.obsolete = function() {
		// 判断是否有id ，调用接口去更改program状态
		taskService.obsoleteTask(obj.keyId).then(angular.bind(this, function then() {
			var result = taskService.obsoleteTaskResult;
			if(result.code==0){
				$uibModalInstance.close(true);
			}else{
				$scope.message(result.msg);
			}
		}));
	};

	$scope.submit=function(){
		$scope.showLoading('#myForm');
		taskService.saveTaskFormValue(obj.keyId,0,1).then(angular.bind(this, function then() {
			var result = taskService.saveTaskFormValueResult;
			if(result.code==0){
				$uibModalInstance.close(true);
			}
			else{
			$scope.message(result.msg);	
			}
			$scope.hideLoading('#myForm');
		}));
	};
	
	
	$scope.cancel = function() {
		if($scope.disabled){
			$uibModalInstance.close();
			return;
		}
		$scope.confirm('Close', 'Exit this page?', 'OK', function() {
			$uibModalInstance.close();
		}, 'NO');
	};
}
angular.module('kindKidsApp').controller('viewMeetingTaskCtrl',
		[ '$scope', '$rootScope', '$timeout', '$location', '$uibModal', '$uibModalInstance', 'obj', 'taskService', viewMeetingTaskCtrl ]);