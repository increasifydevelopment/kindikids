'use strict';

/**
 * @author zhengguo
 * @description weeklyCtrl program Controller
 */
function weeklyCtrl($scope, $rootScope, $timeout, $location, type, roomId, isAfterToday, updateListFun, functions, todayDate, evalId, obsId,
		$uibModal, $uibModalInstance, image_resizer, taskService, fileService) {
	$scope.functions = functions;
	$scope.isAfterToday = isAfterToday;
	// 是否刷新List页面标识
	$scope.updateListFlag = false;

	$scope.obsId = obsId;
	$scope.initWeekly = function() {

		// 页面信息
		$scope.condition = {
			id : '',
			weeklyEvalution : ''
		};

		if (roomId) {
			$scope.orgId = roomId;
			$scope.getName(roomId);
		}

		$scope.page = {
			type : type,
			roomId : roomId
		}
		// 编辑
		if (evalId) {
			taskService.getWeekly(evalId).then(angular.bind(this, function then() {
				var result = taskService.getWeeklyResult;
				if (result.code == 0) {
					// 重新复制id，方便后续操作比如删除
					$scope.condition = result.data;
					$scope.getName(result.data.roomId);
				} else {
					$scope.message(result.msg);
				}
			}));
		}
	};

	$scope.obsolete = function() {
		// 判断是否有id ，调用接口去更改program状态
		if ($scope.obsId) {
			taskService.obsoleteTask($scope.obsId).then(angular.bind(this, function then() {
				var result = taskService.obsoleteTaskResult;
				$scope.message(result.msg);
			}))
		} else {
			$scope.message('Please \'Save\' before archiving');
		}
	}

	// 根据roomId获取对应的centerName 和 roomName 并把todayDate日期放进对象中
	$scope.getName = function(roomId) {
		taskService.getNameList(roomId).then(angular.bind(this, function then() {
			$scope.nameInfo = taskService.nameResult;
			// 赋值传递进来主页面日历的当天日期
			$scope.nameInfo.todayDate = todayDate;
		}))
	}

	// type 10 : weekly
	$scope.updateInfo = function() {
		$scope.showLoading("#weeklyEvaluationId");
		$scope.condition.id = evalId;
		taskService.saveWeekly($scope.condition).then(angular.bind(this, function then() {
			var result = taskService.weeklyResult;
			if (result.code == 0) {
				// 重新复制id，方便后续操作比如删除
				$scope.condition.id = result.data.id;
				// 回调更新页面List
				// updateListFun();
				$scope.updateListFlag = true;
				if ($scope.needShareFlag && $scope.condition && $scope.condition.id) {
					$scope.needShareFlag = false;
					var modalInstance = $uibModal.open({
						templateUrl : 'views/program/shareNewsfeed.html?res_v=' + $scope.res_v,
						controller : 'shareCtrl',
						backdrop : 'static',
						keyboard : false,
						windowClass : 'modal-grey',
						resolve : {
							type : function() {
								return type;
							},
							objId : function() {
								return !$scope.condition ? "" : $scope.condition.id;
							},
							newsId : function() {
								return !$scope.condition ? "" : $scope.condition.newsfeedId;
							},
							roomId : function() {
								return roomId;
							},
							taskType : function() {
								return 10;
							},
							content : function() {
								return !$scope.condition ? "" : $scope.condition.weeklyEvalution;
							},
							actList : function() {
								return [];
							},
							callback : function() {
								return initNewsFeedId;
							}
						}
					}).result.then(function(flag) {
						if (flag) {
							// updateListFun();
							$scope.updateListFlag = true;
						}
					});
				} else {
					$scope.message('update success');
				}
			} else {
				$scope.message(result.msg);
			}
			$scope.hideLoading("#weeklyEvaluationId");
		}));
	}

	$scope.delInfo = function() {
		taskService.delWeekly($scope.condition.id).then(angular.bind(this, function then() {
			var result = taskService.delWeeklyResult;
			if (result.code == 0) {
				// 关闭modal
				$uibModalInstance.dismiss('cancel');
			} else {
				$scope.message(result.msg);
			}
		}));
	}

	$scope.cancel = function() {
		$scope.confirmSuccess = function() {
			$uibModalInstance.dismiss('cancel');
			if ($scope.updateListFlag) {
				updateListFun();
			}
		}
		$rootScope.confirm('Close', 'Exit this page?', 'OK', $scope.confirmSuccess, 'NO');
	};

	// 分享到newsfeed
	/**
	 * @author zhengguo
	 * @description 打开share to newsfeed编辑框
	 * @returns 保存
	 */
	$scope.shareNewfeed = function(type) {
		// 先保存
		$scope.updateInfo();
		// 给个状态控制是否在保存返回后调用分享的方法
		$scope.needShareFlag = true;
	};

	function initNewsFeedId(newsId) {
		$scope.condition.newsfeedId = newsId;
	}

}
angular.module('kindKidsApp').controller(
		'weeklyCtrl',
		[ '$scope', '$rootScope', '$timeout', '$location', 'type', 'roomId', 'isAfterToday', 'updateListFun', 'functions', 'todayDate', 'evalId',
				'obsId', '$uibModal', '$uibModalInstance', 'image_resizer', 'taskService', 'fileService', weeklyCtrl ]);