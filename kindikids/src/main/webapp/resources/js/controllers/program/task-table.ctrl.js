'use strict';
/**
 * hxzhang task table view
 */
function taskTableCtrl($scope, $filter, $timeout, $location, $uibModal, taskService, commonService, familyService, $window, $stateParams) {

    $scope.page = {};
    var now = new Date();
    $scope.page.condition = {
        keyword: null,
        time: null,
        roomId: null,
        centersId: null,
        weekDate: null,
        listType: 1,
        programType: '',
        nqsVersion: "",
        maxSize: 7,
        pageIndex: 1,
        sortName: 'v.end_date',
        sortOrder: 'asc',
        taskStatus: [],
        startDate: now,
        endDate: now
    };

    $scope.condition = {
        totalSize: 0
    }

    $scope.functions = [];
    commonService.getFun("/task").then(angular.bind(this, function then() {
        $scope.functions = commonService.functions;
    }));

    // 获取分页
    $scope.getPageList = function () {
        $scope.showLoading('#taskTable');
        taskService.getPageList($scope.page.condition).then(angular.bind(this, function () {
            $scope.hideLoading('#taskTable');
            var result = taskService.taskTableResult;
            if (result.code == 0) {
                $scope.page = result.data;
                // 判断头像是否超过4张
                for (var i = 0; i < $scope.page.list.length; i++) {
                    if ($scope.page.list[i].avatar) {
                        if ($scope.page.list[i].avatar.length > 4) {
                            $scope.page.list[i].avatar.splice(3, $scope.page.list[i].avatar.length - 3);
                            $scope.page.list[i].moreAva = true;
                        } else {
                            $scope.page.list[i].moreAva = false;
                        }
                    }
                    if ($scope.page.list[i].implementersAvatar) {
                        if ($scope.page.list[i].implementersAvatar.length > 4) {
                            $scope.page.list[i].implementersAvatar.splice(3, $scope.page.list[i].implementersAvatar.length - 3);
                            $scope.page.list[i].moreImplementersAva = true;
                        } else {
                            $scope.page.list[i].moreImplementersAva = false;
                        }
                    }
                }
                $scope.condition = result.data.condition;
                $scope.chooseCentreOrRoom();
            } else {
                $.messager.popup(result.msg);
            }
        }));
    }
    $scope.changePage = function (pageIndex) {
        $scope.condition.pageIndex = pageIndex;
        $scope.getPageList();
    }

    $scope.initTaskTable = function () {
        $scope.getPageList();
    }

    $scope.clearDate = function (flag) {
        if (flag) {
            $scope.page.condition.startDate = null;
            $scope.page.condition.endDate = null;
        }
    }

    $scope.chooseCentreOrRoom = function () {
        familyService.getFamilyOrgs().then(angular.bind(this, function then() {
            var result = familyService.orgsResult;
            $scope.centersList = result.code.centersInfos;
            if ($scope.page.condition.roomIds == null || $scope.page.condition.roomIds.length == 0) {
                $scope.roomList = result.code.roomInfos;
            }
            /* else {
                $scope.roomList = $scope.page.condition.roomIds;
            } */
            $scope.sourceRoomList = result.code.roomInfos;
            if (null == $scope.page.condition.centreIds || $scope.page.condition.centreIds.length == 0) {
                return;
            }
            var newRoomList = new Array();
            for (var i in $scope.sourceRoomList) {
                var room = $scope.sourceRoomList[i];
                if ($.inArray(room.centersId, $scope.page.condition.centreIds) == -1) {
                    continue;
                }
                newRoomList.push(room);
            }
            $scope.roomList = newRoomList;
        }));
    }
    $scope.$watch('condition.centreIds', function (newValue, oldValue) {
        if (newValue == undefined || null == newValue) {
            return;
        }
        if (newValue != null && newValue.length == 0) {
            $scope.roomList = $scope.sourceRoomList;
            return;
        }
        var newRoomList = new Array();
        for (var i in $scope.sourceRoomList) {
            var room = $scope.sourceRoomList[i];
            if ($.inArray(room.centersId, newValue) == -1) {
                continue;
            }
            newRoomList.push(room);
        }
        $scope.roomList = newRoomList;
    }, true);

    $scope.search = function () {
        $scope.changePage(1);
    }
    $scope.export = function () {
        $scope.showLoading('#taskTableView');
        taskService.exportData($scope.page.condition).then(angular.bind(this, function () {
            var result = taskService.exportDataResult;
            $scope.hideLoading('#taskTableView');
            if (result.code == 0) {
                //继续导出excel
                $window.location.href = "./task/csv.do?uuid=" + result.data;
            }
        }));
    }

    $scope.sort = {
        column: 'v.end_date',
        direction: 1,
        toggle: function (sortName) {
            if (this.sortName == sortName) {
                this.direction = -this.direction || -1;
            } else {
                this.sortName = sortName;
                this.direction = -1;
            }

            $scope.page.condition.sortName = this.sortName;
            if (this.direction == -1) {
                $scope.page.condition.sortOrder = "desc";
            } else {
                $scope.page.condition.sortOrder = "asc";
            }
            this.column = sortName;
            $scope.changePage(1);
        }
    };

    $scope.setDefaultVal = function () {
        $scope.page.condition.startDate = $scope.page.condition.now;
        $scope.page.condition.endDate = $scope.page.condition.now;
    }










    // TASK MODEL
    //===========================================================================================================
    var modalOpen = true;
    // 列表编辑
    $scope.openModal = function (type, id, item) {
        if (type == 0 || type == 1 || type == 3) {
            $scope.exploreAndGrowModal(type, id, item);
        }
        if (type == 2) {
            $scope.lessonModal(type, id, item);
        }
        if (type == 4 || type == 6 || type == 8) {
            $scope.interestModal(type, id, item);
        }
        if (type == 5 || type == 7 || type == 9) {
            $scope.interestFollowUpModal(type, id, item);
        }
        if (type == 10) {
            $scope.evalutionModal(type, id, item);
        }
        if (type == 11 || type == 12 || type == 13 || type == 14 || type == 15) {
            $scope.registerModal(type, id, item);
        }
        // 自定义task
        if (type == 17) {
            console.log('openModal|satrt=' + new Date().getTime());
            /* $scope.message('In development...'); return; */

            var myTaskId = item.id;
            var taskName = item.taskName;
            $scope.openTaskModal(myTaskId, taskName);
        }
        if (type == 18) {
            $scope.medicationModal(type, id, item);
        }
        // meeting gfwang add
        if (type == 20) {
            $scope.openMeetingTask(item);
        }
        if (type == 21) {
            $scope.openHatMoneyModel(item, 'Money Task', 'Are you Complete?');
        }
        if (type == 22) {
            $scope.openHatMoneyModel(item, 'Hat Task', 'Are you Complete?');
        }
    };


    $scope.exploreAndGrowModal = function (type, ExGrScId, item) {
        if (!modalOpen) {
            return;
        }
        if (!$scope.page.condition.roomId && !ExGrScId) {
            $scope.message('please select a room first');
        } else {
            $uibModal.open({
                templateUrl: 'views/program/exploreAndGrow.html?res_v=' + $scope.res_v,
                controller: 'exploreAndGrowCtrl',
                backdrop: 'static',
                keyboard: false,
                windowClass: 'modal-grey task-popup modal-align-top',
                resolve: {
                    type: function () {
                        return type;
                    },
                    roomId: function () {
                        return getRoomId(item);
                    },
                    ExGrScId: function () {
                        return ExGrScId;
                    },
                    obsId: function () {
                        return !item ? "" : item.id;
                    },
                    surpassListFlag: function () {
                        return $scope.surpassListFlag;
                    },
                    InterFunction: function () {
                        return $scope.interestModal;
                    },
                    InterFullowFunction: function () {
                        return $scope.interestFollowUpModal;
                    },
                    todayDate: function () {
                        return $scope.taskDate;
                    },
                    updateListFun: function () {
                        return $scope.fun;
                    },
                    functions: function () {
                        return $scope.functions;
                    }
                }
            })
        }
    };
    $scope.lessonModal = function (type, lessonId, item) {
        if (!modalOpen) {
            return;
        }
        if (!$scope.page.condition.roomId && !lessonId) {
            $scope.message('please select a room first');
        } else {
            var modalInstance = $uibModal.open({
                templateUrl: 'views/program/lesson.html?res_v=' + $scope.res_v,
                controller: 'lessonCtrl',
                backdrop: 'static',
                keyboard: false,
                windowClass: 'modal-grey task-popup modal-align-top',
                resolve: {
                    type: function () {
                        return type;
                    },
                    roomId: function () {
                        return getRoomId(item);
                    },
                    surpassListFlag: function () {
                        return $scope.surpassListFlag;
                    },
                    lessonId: function () {
                        return lessonId;
                    },
                    obsId: function () {
                        return !item ? "" : item.id;
                    },
                    todayDate: function () {
                        return $scope.taskDate;
                    },
                    updateListFun: function () {
                        return $scope.fun;
                    },
                    functions: function () {
                        return $scope.functions;
                    }
                }
            })
        }
    };

    $scope.interestModal = function (type, intobsleaId, item) {
        if (!modalOpen) {
            return;
        }
        if (!$scope.page.condition.roomId && !intobsleaId) {
            $scope.message('please select a room first');
        } else {
            var modalInstance = $uibModal.open({
                templateUrl: 'views/program/interest-ob-learn.html?res_v=' + $scope.res_v,
                controller: 'interestCtrl',
                backdrop: 'static',
                keyboard: false,
                windowClass: 'modal-grey task-popup',
                resolve: {
                    type: function () {
                        return type;
                    },
                    todayDate: function () {
                        return $scope.taskDate;
                    },
                    surpassListFlag: function () {
                        return $scope.surpassListFlag;
                    },
                    roomId: function () {
                        return getRoomId(item);
                    },
                    intobsleaId: function () {
                        return intobsleaId;
                    },
                    obsId: function () {
                        return !item ? "" : item.id;
                    },
                    updateListFun: function () {
                        return $scope.fun;
                    },
                    functions: function () {
                        return $scope.functions;
                    },
                    isDisable: function () {
                        return false;
                    },
                    obj: function () {
                        return null;
                    }
                }
            })
        }
    };
    $scope.interestFollowUpModal = function (type, intobsleaId, item) {
        if (!modalOpen) {
            return;
        }
        var isAfterToday = dateComp($scope.taskDate);
        $uibModal.open({
            templateUrl: 'views/program/interest-ob-learn-followUp.html?res_v=' + $scope.res_v,
            controller: 'interestFollowUpCtrl',
            backdrop: 'static',
            keyboard: false,
            windowClass: 'modal-grey task-popup modal-align-top',
            resolve: {
                type: function () {
                    return type;
                },
                intobsleaId: function () {
                    return intobsleaId;
                },
                obsId: function () {
                    return !item ? "" : item.id;
                },
                roomId: function () {
                    return $scope.page.condition.roomId;
                },
                surpassListFlag: function () {
                    return $scope.surpassListFlag;
                },
                todayDate: function () {
                    return $scope.taskDate;
                },
                updateListFun: function () {
                    return $scope.fun;
                },
                functions: function () {
                    return $scope.functions;
                },
                isAfterToday: function () {
                    return isAfterToday;
                }
            }
        });
    };
    $scope.evalutionModal = function (type, evalId, item) {
        if (!modalOpen) {
            return;
        }
        if (!$scope.page.condition.roomId && !evalId) {
            $scope.message('please select a room first');
        } else {
            var isAfterToday = dateComp($scope.taskDate);
            $uibModal.open({
                templateUrl: 'views/program/weekly.html?res_v=' + $scope.res_v,
                controller: 'weeklyCtrl',
                backdrop: 'static',
                keyboard: false,
                windowClass: 'modal-grey task-popup',
                resolve: {
                    type: function () {
                        return type;
                    },
                    roomId: function () {
                        return getRoomId(item);
                    },
                    evalId: function () {
                        return evalId;
                    },
                    obsId: function () {
                        return !item ? "" : item.id;
                    },
                    todayDate: function () {
                        return $scope.taskDate;
                    },
                    updateListFun: function () {
                        return $scope.fun;
                    },
                    functions: function () {
                        return $scope.functions;
                    },
                    isAfterToday: function () {
                        return isAfterToday;
                    }
                }
            })
        }
    };
    $scope.registerModal = function (type, registerId, item) {
        if (!modalOpen) {
            return;
        }
        if (!$scope.page.condition.roomId && !registerId) {
            $scope.message('please select a room first');
        } else {
            var modalInstance = $uibModal.open({
                templateUrl: 'views/program/register.html?res_v=' + $scope.res_v,
                controller: 'registerCtrl',
                backdrop: 'static',
                keyboard: false,
                windowClass: 'modal-grey task-popup',
                resolve: {
                    type: function () {
                        return type;
                    },
                    roomId: function () {
                        return getRoomId(item);
                    },
                    registerId: function () {
                        return registerId;
                    },
                    obsId: function () {
                        return !item ? "" : item.id;
                    },
                    todayDate: function () {
                        return $scope.taskDate;
                    },
                    updateListFun: function () {
                        return $scope.fun;
                    },
                    functions: function () {
                        return $scope.functions;
                    }
                }
            })
        }
    };
    $scope.openTaskModal = function (myTaskId, taskName) {
        // openTaskModal|satrt=1478236301639
        console.log('openTaskModal|satrt=' + new Date().getTime());
        if (!modalOpen) {
            return;
        }
        $scope.showLoading('#mainLoad');
        var isAfterToday = dateComp($scope.taskDate);
        modalOpen = false;
        $uibModal.open({
            templateUrl: 'views/program/task.form.html?res_v=' + $scope.res_v,
            controller: 'taskFormCtrl',
            backdrop: 'static',
            keyboard: false,
            windowClass: 'modal-grey task-popup',
            animation: true,
            resolve: {
                myTaskId: function () {
                    return myTaskId;
                },
                taskName: function () {
                    return taskName;
                },
                functions: function () {
                    return $scope.functions;
                },
                isAfterToday: function () {
                    return isAfterToday;
                },
                taskDate: function () {
                    return $scope.taskDate;
                },
                isParent: function () {
                    return null;
                }
            }

        }).result.then(function (instanceId) {
            modalOpen = true;
            $scope.changePage(1);
        });
        modalOpen = true;
    };
    $scope.medicationModal = function (type, medicationId, item) {
        if (!modalOpen) {
            return;
        }
        if (!$scope.page.condition.roomId && !medicationId) {
            $scope.message('please select a room first');
        } else {
            var modalInstance = $uibModal.open({
                templateUrl: 'views/program/medication.html?res_v=' + $scope.res_v,
                controller: 'medicationCtrl',
                backdrop: 'static',
                keyboard: false,
                windowClass: 'modal-grey',
                resolve: {
                    type: function () {
                        return type;
                    },
                    roomId: function () {
                        return getRoomId(item);
                    },
                    medicationId: function () {
                        return medicationId;
                    },
                    obsId: function () {
                        return !item ? "" : item.id;
                    },
                    surpassListFlag: function () {
                        return $scope.surpassListFlag;
                    },
                    todayDate: function () {
                        return $scope.taskDate;
                    },
                    updateListFun: function () {
                        return $scope.fun;
                    },
                    functions: function () {
                        return $scope.functions;
                    },
                    isParent: function () {
                        return null;
                    }
                }
            })
        }
    };
    $scope.openMeetingTask = function (obj) {
        if (!modalOpen) {
            return;
        }
        var isAfterToday = dateComp($scope.taskDate);
        $uibModal.open({
            templateUrl: './views/program/view.meeting.html?res_v=' + $scope.res_v,
            controller: 'viewMeetingTaskCtrl',
            backdrop: 'static',
            keyboard: false,
            windowClass: 'modal-grey task-popup',
            animation: true,
            size: "lg",
            resolve: {
                obj: function () {
                    return {
                        rowId: obj.valueId,
                        state: obj.statu,
                        keyId: obj.id,
                        functions: $scope.functions,
                        isAfterToday: isAfterToday
                    };
                }
            }

        }).result.then(function (success) {
            if (success) {
                $scope.changePage(1);
            }
        });
    };
    $scope.openHatMoneyModel = function (obj, title, content) {
        if (!modalOpen) {
            return;
        }
        var isAfterToday = dateComp($scope.taskDate);
        $uibModal.open({
            templateUrl: './views/program/hat.money.html?res_v=' + $scope.res_v,
            controller: 'hatMoneyTaskCtrl',
            backdrop: 'static',
            keyboard: false,
            windowClass: 'modal-grey task-popup',
            animation: true,
            resolve: {
                obj: function () {
                    return {
                        title: title,
                        content: content,
                        keyId: obj.id,
                        functions: $scope.functions,
                        isAfterToday: isAfterToday
                    };
                }
            }

        }).result.then(function (success) {
            if (success) {
                $scope.changePage(1);
            }
        });
    };

    function getRoomId(item) {
        if (!item) {
            return $scope.page.condition.roomId;
        }
        return item.roomId;
    }

    function dateComp(chooseDate) {
        return new moment(chooseDate, 'YYYY-MM-DD').isAfter(new moment($scope.currentUser.currtenDate, 'DD/MM/YYYY'));
    }

    //===========================================================================================================
}
angular.module('kindKidsApp').controller('taskTableCtrl', ['$scope', '$filter', '$timeout', '$location', '$uibModal', 'taskService', 'commonService', 'familyService', '$window', '$stateParams', taskTableCtrl]);