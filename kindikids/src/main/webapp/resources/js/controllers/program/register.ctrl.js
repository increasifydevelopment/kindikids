'use strict';


/**
 * @author zhengguo
 * @description registerCtrl program Controller
 */
function registerCtrl($scope, $rootScope, $timeout, $location, type, roomId, updateListFun, todayDate, functions, registerId, obsId, $uibModal,
		$uibModalInstance, image_resizer, taskService, fileService) {
	/*$(function(){
		 $.scrollUp({
		        scrollImg:true
		  });
	});
*/
	$scope.functions = functions;
	$scope.obsId = obsId;
	// 是否刷新List页面标识
	$scope.updateListFlag = false;
	$scope.initRegister = function() {
		// type-----11：meal / 12:sleep / 13: nappy / 14:tolit / 15 : sunscreen
		// 对应后台
		$scope.condition = {
			type : type
		};

		// select2 ajax 参数
		if (roomId) {
			$scope.orgId = roomId;
			$scope.getName(roomId);
		}

		// 如果是新增，无registerId，从后台拿一个新对象,否则为编辑
		taskService.getRegisterInfo(roomId, registerId).then(
				angular.bind(this, function then() {
					var result = taskService.inRegisterResult;
					if (result.code == 0) {
						if (registerId) {
							$scope.pageInfo = result.data;
							// select2 id赋值
							$scope.orgId = $scope.pageInfo.programRegisterTaskInfo.roomId;
							// 获取name
							$scope.getName($scope.pageInfo.programRegisterTaskInfo.roomId);

							for (var i = 0; i < $scope.pageInfo.registerItemList.length; i++) {
								if ($scope.pageInfo.registerItemList[i].programRegisterSleepInfo) {
									if ($scope.pageInfo.registerItemList[i].programRegisterSleepInfo.sleepTime) {
										$scope.pageInfo.registerItemList[i].programRegisterSleepInfo.sleepTime = $scope
												.changeBackTime($scope.pageInfo.registerItemList[i].programRegisterSleepInfo.sleepTime);
									}
									if ($scope.pageInfo.registerItemList[i].programRegisterSleepInfo.wokeupTime) {
										$scope.pageInfo.registerItemList[i].programRegisterSleepInfo.wokeupTime = $scope
												.changeBackTime($scope.pageInfo.registerItemList[i].programRegisterSleepInfo.wokeupTime);
									}
								}
							}
						} else {
							$scope.pageInfo = result.data;
							$scope.pageInfo.programRegisterTaskInfo.roomId = roomId;
							$scope.pageInfo.programRegisterTaskInfo.type = type;
							var itemList = {
								programRegisterItemInfo : {},
								programRegisterSleepInfo : {}
							};
							$scope.pageInfo.registerItemList.push(itemList);
						}
					} else {
						$scope.message(result.msg);
					}
				}));
	};

	$scope.obsolete = function() {
		// 判断是否有id ，调用接口去更改program状态
		if ($scope.obsId) {
			taskService.obsoleteTask($scope.obsId).then(angular.bind(this, function then() {
				var result = taskService.obsoleteTaskResult;
				// updateListFun();
				$scope.updateListFlag = true;
				$scope.message(result.msg);
			}));
		} else {
			$scope.message('Please \'Save\' before archiving');
		}
	}

	// 根据roomId获取对应的centerName 和 roomName 并把todayDate日期放进对象中
	$scope.getName = function(roomId) {
		taskService.getNameList(roomId).then(angular.bind(this, function then() {
			$scope.nameInfo = taskService.nameResult;
			// 赋值传递进来主页面日历的当天日期
			$scope.nameInfo.todayDate = todayDate;
		}))
	}

	$scope.delItem = function(index) {
		$scope.pageInfo.registerItemList.splice(index, 1);
	}

	$scope.addItem = function() {
		var oneList = {
			programRegisterItemInfo : {},
			programRegisterSleepInfo : {}
		};
		$scope.pageInfo.registerItemList.push(oneList);
		// 手动将增加的模块加入到bootstrapValidator校验中
		$timeout(function() {
			$('#editRegisterForm').bootstrapValidator('addField', 'childSelect', {
				validators : {
					notEmpty : {
						message : 'This is required'
					}
				}
			});
		}, 200);
	}

	$scope.updateTime = function(itemInfo) {
		// taskService.updateNappyToiletTime(itemInfo).then(angular.bind(this,function
		// then(){
		// var result = taskService.updateResult;
		// }));
		for (var i = 0; i < $scope.pageInfo.registerItemList.length; i++) {
			var date = new Date();
			var now = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + "T" + date.getHours() + ":" + date.getMinutes() + ":"
					+ date.getSeconds() + "." + date.getMilliseconds();
			if ($scope.pageInfo.registerItemList[i].programRegisterItemInfo.childId == itemInfo.childId) {
				$scope.pageInfo.registerItemList[i].changeTime = now;
			}
		}
	};

	$scope.updateInfo = function(isSubmit) {
		$('#editRegisterForm').data('bootstrapValidator').validate();
		var flag = false;
		$("small").map(function() {
			if ($(this).attr('data-bv-result') == "INVALID") {
				flag = true;
			}
		});
		if (flag) {
			return;
		}
		var savePageInfo = angular.copy($scope.pageInfo);
		// 处理时间格式问题 sleep register 下存在时间格式转换
		var nDate = new Date();
		var nowDate = nDate.getFullYear() + '-' + (nDate.getMonth() + 1) + '-' + nDate.getDate();
		// 将select2Model的临时存数组制空，后台是以对象接受的
		for (var i = 0; i < savePageInfo.registerItemList.length; i++) {
			savePageInfo.registerItemList[i].programRegisterItemUsers = null;
		}
		if (savePageInfo.programRegisterTaskInfo.type == 12) {
			for (var i = 0; i < savePageInfo.registerItemList.length; i++) {
				if (savePageInfo.registerItemList[i].programRegisterSleepInfo) {
					if (savePageInfo.registerItemList[i].programRegisterSleepInfo.sleepTime) {
						savePageInfo.registerItemList[i].programRegisterSleepInfo.sleepTime = nowDate
								+ $scope.changeHeadTime(savePageInfo.registerItemList[i].programRegisterSleepInfo.sleepTime);
					}
					if (savePageInfo.registerItemList[i].programRegisterSleepInfo.wokeupTime) {
						savePageInfo.registerItemList[i].programRegisterSleepInfo.wokeupTime = nowDate
								+ $scope.changeHeadTime(savePageInfo.registerItemList[i].programRegisterSleepInfo.wokeupTime);
					}
				}
			}
		}
		$scope.showLoading("#mealRegisterId");
		taskService
				.saveRegisterInfo(savePageInfo, isSubmit)
				.then(
						angular
								.bind(
										this,
										function then() {
											var result = taskService.saveRegisterResult;
											if (result.code == 0) {
												// 重新赋值id，方便后续操作比如删除
												$scope.pageInfo.programRegisterTaskInfo.id = result.data.programRegisterTaskInfo.id;
												for (var i = 0; i < $scope.pageInfo.registerItemList.length; i++) {
													$scope.pageInfo.registerItemList[i].programRegisterItemInfo.newsfeedId = result.data.registerItemList[i].programRegisterItemInfo.newsfeedId;
												}
												// 回调更新页面List
												// updateListFun();
												$scope.updateListFlag = true;
												$scope.message('update success');
											} else {
												$scope.message(result.msg);
											}
											$scope.hideLoading("#mealRegisterId");
										}));
	};

	$scope.cancel = function() {
		$scope.confirmSuccess = function() {
			$uibModalInstance.dismiss('cancel');
			if ($scope.updateListFlag) {
				updateListFun();
			}
		}
		$rootScope.confirm('Close', 'Exit this page?', 'OK', $scope.confirmSuccess, 'NO');
	};

}
angular.module('kindKidsApp').controller(
		'registerCtrl',
		[ '$scope', '$rootScope', '$timeout', '$location', 'type', 'roomId', 'updateListFun', 'todayDate', 'functions', 'registerId', 'obsId',
				'$uibModal', '$uibModalInstance', 'image_resizer', 'taskService', 'fileService', registerCtrl ]);