'use strict';

/**
 * @author zhengguo
 * @description lesson Controller
 */
function shareCtrl($scope, $rootScope, $timeout, $location, objId, taskType,roomId,newsId,content,callback,actList, type, $uibModal, $uibModalInstance, image_resizer, taskService,
		fileService) {
	$scope.programNewsFeedVo={};
	$scope.programNewsFeedVo.content=content;
	$scope.initLesson = function() {
		$scope.page = {
			type : type,
		};
		$scope.selectModel = '';
	};
	$scope.taskType=taskType;
	$scope.shareFlag = false;
	$scope.ok = function() {
		if($scope.shareFlag){
			return;
		}
		//防止多次点击
		$scope.showLoading("#shareChangeSet");
		$scope.shareFlag = true;
		$scope.programNewsFeedVo.nqsVoList = $scope.nqsVoList;
		$scope.programNewsFeedVo.roomId = roomId;
		$scope.programNewsFeedVo.newsId=newsId;
		$scope.programNewsFeedVo.taskType=taskType;
		$scope.programNewsFeedVo.actList=actList;
		taskService.shareNewsfeed($scope.programNewsFeedVo, !objId?"":objId).then(angular.bind(this, function then() {
			var result = taskService.shareResult;
			if (result.code == 0) {
				callback(result.data);
				// 关闭modal
				$uibModalInstance.close(true);
				$scope.message('Successfully shared to the newsfeed');
			} else {
				$scope.message(result.msg);
			}
			$scope.shareFlag = false;
			$scope.hideLoading("#shareChangeSet");
		}));

	};

	$scope.cancel = function() {
		$rootScope.confirm('Close','Exit this page?','OK',function(){
			$uibModalInstance.close(false);
		},'NO');
	};

	// 打开nqs，调用公共方法返回选择的nqs
	$scope.nqsVoList = [];
	$scope.saveNqsResult = function(newNqs) {
		$scope.nqsVoList = newNqs;
	};

}
angular.module('kindKidsApp').controller(
		'shareCtrl',
		[ '$scope', '$rootScope', '$timeout', '$location', 'objId','taskType' ,'roomId','newsId', 'content','callback','actList','type', '$uibModal', '$uibModalInstance', 'image_resizer',
				'taskService', 'fileService', shareCtrl ]);