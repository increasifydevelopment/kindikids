'use strict';

/**
 * @author zhengguo
 * @description 5 interest /7 observation/ 9 learning story Controller
 */
function interestFollowUpCtrl($scope, $rootScope, $timeout, $location, type, surpassListFlag, isAfterToday, updateListFun, functions, roomId,
		todayDate, intobsleaId, obsId, $uibModal, $uibModalInstance, image_resizer, taskService, fileService) {
	$scope.isAfterToday = isAfterToday;
	$scope.functions = functions;
	// 是否刷新List页面标识
	$scope.updateListFlag = false;
	if (roomId) {
		$scope.orgId = roomId;
	}

	$scope.obsId = obsId;

	$scope.condition = {
		type : type,
		surpassListFlag : surpassListFlag
	};

	$scope.initInterestFollowUp = function() {
		$scope.templates = [ {
			id : 0,
			name : 'Achieved'
		}, {
			id : 1,
			name : 'Not Achieved'
		}, {
			id : 2,
			name : 'Not Assessed'
		} ];
		// 必须传入了intobsleaId，初始化页面信息
		taskService.getInObLeFollowInfo(intobsleaId).then(
				angular.bind(this,
						function then() {
							var result = taskService.inObLeInfoFollowResult;
							if (result.code == 0) {
								$scope.page = result.data;
								// select2 页面url请求需要roomId
								$scope.orgId = $scope.page.intobsleaInfo.roomId;
								// 获取name
								$scope.getName($scope.page.intobsleaInfo.roomId);
								if (!$scope.page.fileList) {
									$scope.page.fileList = [];
								}
								$scope.newsInfo = {
									fileVoList : $scope.page.fileList
								}
								updateNoFileArray($scope.newsInfo.fileVoList.length);

								// 处理下时间格式
								$scope.page.intobsleaInfo.evaluationDate = $scope.page.intobsleaInfo.evaluationDate.substr(8, 2) + '/'
										+ $scope.page.intobsleaInfo.evaluationDate.substr(5, 2) + '/'
										+ $scope.page.intobsleaInfo.evaluationDate.substr(0, 4);
								$scope.page.intobsleaInfo.day = $scope.page.intobsleaInfo.day.substr(8, 2) + '/'
								+ $scope.page.intobsleaInfo.day.substr(5, 2) + '/'
								+ $scope.page.intobsleaInfo.day.substr(0, 4);
								// 孩子的能力为空,调用validate验证
								if (!result.data.eylfList.eylfCheckList) {
									$scope.validate();
								}
							} else {
								$scope.message(result.msg);
							}
						}));
	};

	$scope.obsolete = function(type) {
		// 判断是否有id ，调用接口去更改program状态
		if ($scope.obsId) {
			taskService.obsoleteTask($scope.obsId).then(angular.bind(this, function then() {
				var result = taskService.obsoleteTaskResult;
				$scope.message(result.msg);
				// updateListFun();
				$scope.updateListFlag = true;
			}))
		} else {
			if (type == 5) {
				$scope.message('Please \'Save\' before archiving');
			} else if (type == 7) {
				$scope.message('Please \'Save\' before archiving');
			} else if (type == 9) {
				$scope.message('Please \'Save\' before archiving');
			}
		}
	}

	// 根据roomId获取对应的centerName 和 roomName 并把todayDate日期放进对象中
	$scope.getName = function(roomId) {
		taskService.getNameList(roomId).then(angular.bind(this, function then() {
			$scope.nameInfo = taskService.nameResult;
			// 赋值传递进来主页面日历的当天日期
			$scope.nameInfo.todayDate = todayDate;
		}))
	}

	// *********************************************************************
	// 文件上传 start
	// **********************************************************************

	$scope.noFileVoList = [ "1" ];
	function updateNoFileArray(count) {
		var arr = new Array(1 - count);
		for (var i = 0; i < 1 - count; i++) {
			arr[i] = i;
		}
		$scope.noFileVoList = arr;
	}
	;

	/**
	 * @author abliu
	 * @description 文件上传
	 */
	$scope.upload = function($files, newsInfoObj) {
		if ($files == null || $files.length == 0) {
			return;
		}

		if (newsInfoObj.fileVoList.length + $files.length > 1) {
			// 提示文件重复 todo
			$rootScope.message("Number of images is not greater than 1.");
			return;
		}
		// 判断附件是中重复
		var compareResule = $scope.isRepeatAnnex($files, newsInfoObj);
		if (compareResule != '') {
			// 提示文件重复 todo
			$rootScope.message("Sorry, file " + compareResule + " already exists, please try a different filename.");
			return;
		}
		for (var i = 0; i < $files.length; i++) {
			var file = $files[i];
			if (file != null) {
				if (file.size < 10485760) {
					// 等待上传
					$scope.showLoading("#followChangeSet");
					// 转换大小
					image_resizer.resize({
						image : file,
						max_width : 1024
					}).then(function(data) {
						var fileStrBase64 = data.base64_resized_image;
						var fileType = data.file_type;
						var fileName = data.file_name;
						fileService.uploadResizeImage(fileType, fileStrBase64, fileName).then(angular.bind(this, function then() {
							var result = fileService.fdata;
							if (result.code == 0) {
								var length = newsInfoObj.fileVoList.length;
								var fileVo = {};
								fileVo.relativePathName = result.data.relativePathName;
								fileVo.fileName = result.data.fileName;
								fileVo.visitedUrl = result.data.visitedUrl;
								newsInfoObj.fileVoList.push(fileVo);
								// 更新没图片的集合
								updateNoFileArray($scope.newsInfo.fileVoList.length);
								$('.imageUpload').addClass('slideDown');
							} else {
								$rootScope.message(result.msg);
							}
							$scope.hideLoading("#followChangeSet");
						}));
					}, function(resize_error) {
						$scope.hideLoading("#followChangeSet");
						$rootScope.message("Sorry, the filesize is too large, please reduce the filesize and try again.");
					});
				} else {
					$rootScope.message("Sorry, the filesize is too large, please reduce the filesize and try again.");
				}
			}
		}
	};

	/**
	 * @author abliu
	 * @description 删除附件
	 */
	$scope.removeFile = function(fileName, newsInfoObj) {
		for (var i = 0, m = newsInfoObj.fileVoList.length; i < m; i++) {
			if (newsInfoObj.fileVoList[i].relativePathName == fileName) {
				newsInfoObj.fileVoList.splice(i, 1);
				// 更新没图片的集合
				updateNoFileArray($scope.newsInfo.fileVoList.length);
				if ($scope.newsInfo.fileVoList.length == 0) {
					$('.imageUpload').removeClass('slideDown');
				}
				return;
			}
		}
	};

	/**
	 * @author abliu
	 * @description 判断附件是否重复
	 */
	$scope.isRepeatAnnex = function($files, newsInfoObj) {
		var annxs = "";
		if (newsInfoObj.fileVoList == undefined || newsInfoObj.fileVoList.length == 0) {
			return "";
		}
		for (var j = 0, m = newsInfoObj.fileVoList.length; j < m; j++) {
			for (var i = 0; i < $files.length; i++) {
				if (newsInfoObj.fileVoList[j].fileName == $files[i].name) {
					return $files[i].name;
				}
			}
		}
		return annxs;
	};

	// *********************************************************************
	// 文件上传 end
	// **********************************************************************

	$scope.updateInfo = function() {
		$('#eylfCheck').data('bootstrapValidator').validate();
		var result = $('#eylfCheck').data('bootstrapValidator').isValid();
		if (!result) {
			if ($(".has-error").offset() != undefined) {
				var top = $(".has-error").offset().top - 20;
				$('.modal')[0].scrollTop = top;
				// document.documentElement.scrollTop = top;
				// document.body.scrollTop = top;
				// $("body,html,document").scrollTop(top);
			}
			return;
		}
		$scope.showLoading("#followChangeSet");
		taskService.saveInObLeFollowInfo($scope.page).then(angular.bind(this, function then() {
			var result = taskService.saveObLeInfoFollowResult;
			if (result.code == 0) {
				// 重新复制id，方便后续操作比如删除
				$scope.page.followUpInfo.id = result.data.followUpInfo.id;
				// 回调更新页面List
				// updateListFun();
				$scope.updateListFlag = true;
				if ($scope.needShareFlag && $scope.page && $scope.page.followUpInfo.id) {
					$scope.needShareFlag = false;
					$scope.updateListFlag = false;
					$scope.showLoading("#followChangeSet");
					var objId = $scope.page.followUpInfo.id;
					initNewsfeedVo();
					taskService.shareNewsfeed($scope.programNewsFeedVo, !objId ? "" : objId).then(angular.bind(this, function then() {
						var result = taskService.shareResult;
						if (result.code == 0) {
							$scope.page.followUpInfo.newsfeedId = result.data;
							// updateListFun();
							$scope.updateListFlag = true;
							$scope.message('Successfully shared to the newsfeed');
						} else {
							$scope.message(result.msg);
						}
						$scope.hideLoading("#followChangeSet");
					}));
				} else {
					$scope.message('update success');
				}
			} else {
				$scope.message(result.msg);
			}
			$scope.hideLoading("#followChangeSet");
		}));
	}

	$scope.delInfo = function() {
		taskService.delLessonInfo($scope.page.id).then(angular.bind(this, function then() {
			var result = taskService.delLessonResult;
			if (result.code == 0) {
				// 回调更新页面List
				updateListFun();
				$scope.message('update success');
				// 关闭modal
				$uibModalInstance.dismiss('cancel');
			} else {
				$scope.message(result.msg);
			}
		}));
	}

	$scope.cancel = function() {
		$scope.confirmSuccess = function() {
			$uibModalInstance.dismiss('cancel');
			if ($scope.updateListFlag) {
				updateListFun();
			}
		}
		$rootScope.confirm('Close', 'Exit this page?', 'OK', $scope.confirmSuccess, 'NO');
	};

	// 分享到newsfeed
	/**
	 * @author zhengguo
	 * @description 打开share to newsfeed编辑框
	 * @returns 保存
	 */
	$scope.shareNewfeed = function(type) {
		// 先保存
		$scope.updateInfo();
		// 给个状态控制是否在保存返回后调用分享的方法
		$scope.needShareFlag = true;
	};

	function initNewsfeedVo() {
		$scope.eylfMyCheck();
		var contentArr = setContent();
		// if()
		$scope.programNewsFeedVo = {
			newsId : $scope.page.followUpInfo.newsfeedId,
			contentTip0 : contentArr[3] ? contentArr[3] : '',
			contentTip1 : contentArr[0] ? contentArr[0] : '',
			contentTip2 : contentArr[1] ? contentArr[1] : '',
			contentTip3 : contentArr[2] ? contentArr[2] : '',
			eylfList : $scope.shareNewsEylfList,
			taskType : type,
			accounts : new Array($scope.page.child.id),
			schoolFlag : $scope.page.intobsleaInfo.category
		};
	}
	function setContent() {
		var contentArr = [];
		if (type == 5) {
			contentArr.push($scope.page.intobsleaInfo.observation);
			contentArr.push($scope.page.intobsleaInfo.learning);
			contentArr.push($scope.page.intobsleaInfo.educatorExperience);
			contentArr.push($scope.page.followUpInfo.observation);
		} else if (type == 7 || type == 9) {
			contentArr.push($scope.page.intobsleaInfo.educatorExperience);
			contentArr.push($scope.page.intobsleaInfo.objective);
			contentArr.push($scope.page.followUpInfo.observation);
		}
		return contentArr;
	}

	$scope.eylfMyCheck = function() {
		$scope.shareNewsEylfList = [];
		angular.forEach($scope.page.eylfList, function(eylf, index, array) {
			angular.forEach(eylf.eylfCilckList, function(data, index, array) {
				if (data.checkFlag) {
					$scope.shareNewsEylfList.push(data);
				}
			});
		});
	};
	$scope.validate = function() {
		$timeout(function() {
			$('#eylfCheck').bootstrapValidator({
				excluded : [ ':disabled', ':hidden' ],
				message : "This value is not valid",
				feedbackIcons : {
					validating : "glyphicon glyphicon-refresh"
				},
				fields : {
					reason : {
						validators : {
							notEmpty : {
								message : "The Reason is required"
							},
							stringLength : {
								max : 255,
								message : "The Reason must be less than 255 characters long"
							}
						}
					}
				}
			});
		}, 200);
	};

	/**
	 * 打开Interest,Observation,LearningStory模态框
	 */
	$scope.openIntObsLeaModal = function(type, intobsleaId, taskInstanceId) {
		var obj = {
			canalCurrent : canalCurrent,
			refresh : $scope.initInterestFollowUp
		};
		if (!roomId && !intobsleaId) {
			$scope.message('please select a room first');
		} else {
			var modalInstance = $uibModal.open({
				templateUrl : 'views/program/interest-ob-learn.html?res_v=' + $scope.res_v,
				controller : 'interestCtrl',
				backdrop : 'static',
				keyboard : false,
				windowClass : 'modal-grey task-popup',
				resolve : {
					type : function() {
						return type;
					},
					todayDate : function() {
						return $scope.page.intobsleaInfo.day;
					},
					surpassListFlag : function() {
						return $scope.surpassListFlag;
					},
					roomId : function() {
						return roomId;
					},
					intobsleaId : function() {
						return intobsleaId;
					},
					obsId : function() {
						return !taskInstanceId ? "" : taskInstanceId;
					},
					updateListFun : function() {
						return updateListFun;
					},
					functions : function() {
						return $scope.functions;
					},
					isDisable : function() {
						return true;
					},
					obj : function() {
						return obj;
					}
				}
			})
		}
	};
	function canalCurrent() {
		$uibModalInstance.dismiss('cancel');
	}

}
angular.module('kindKidsApp').controller(
		'interestFollowUpCtrl',
		[ '$scope', '$rootScope', '$timeout', '$location', 'type', 'surpassListFlag', 'isAfterToday', 'updateListFun', 'functions', 'roomId',
				'todayDate', 'intobsleaId', 'obsId', '$uibModal', '$uibModalInstance', 'image_resizer', 'taskService', 'fileService',
				interestFollowUpCtrl ]);