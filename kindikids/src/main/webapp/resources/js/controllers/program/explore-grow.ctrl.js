'use strict';

/**
 * @author zhengguo
 * @description explore and grow schoolReadiness Controller
 */
function exploreAndGrowCtrl($scope, $rootScope, $timeout, $location, type, todayDate, roomId, ExGrScId, functions, obsId, InterFunction,
	InterFullowFunction, surpassListFlag, updateListFun, $uibModal, $uibModalInstance, image_resizer, taskService, fileService) {
	$scope.functions = functions;
	// 是否刷新List页面标识
	$scope.updateListFlag = false;
	$scope.initExpAndGrow = function () {
		$scope.page = {
			type: type,
			roomId: roomId,
			id: ExGrScId,
			obsId: obsId, // 禁用task专用id
			surpassListFlag: surpassListFlag
		}
		// explore / grow /school readiness program编辑框 type 0 : explore / 1 :
		// grow /
		// 3 : school readiness
		// 根据type，传入roomId获取空白的对象，新建program
		taskService.getTaskExGrSc(type, $scope.page.id, roomId).then(angular.bind(this, function then() {
			$scope.pageInfo = taskService.taskExGrScResult;
			// 根据roomId获取对应的centerName 和 roomName 并把todayDate日期放进对象中
			$scope.getName($scope.pageInfo.data.programTaskExgrscInfo.roomId);
			// 拿到已存在的images
			$scope.newsInfo = {
				fileVoList: $scope.pageInfo.data.images
			};
			if ($scope.newsInfo.fileVoList.length > 0) {
				updateNoFileArray($scope.newsInfo.fileVoList.length);
			}
			if (!$scope.pageInfo.data.programTaskExgrscInfo.roomId) {
				$scope.pageInfo.data.programTaskExgrscInfo.roomId = roomId;
			}
			if ($scope.pageInfo.data.programTaskExgrscInfo.type) {
				$scope.page.type = $scope.pageInfo.data.programTaskExgrscInfo.type;
			}
			$scope.tagAccountIds = $scope.pageInfo.data.tagAccountIds;
			$scope.selecterPos = $scope.pageInfo.data.selecterPos;
			$scope.nqsVoList = $scope.pageInfo.data.nqsVoList;
		}))
	};

	$scope.obsolete = function () {
		// 判断是否有id ，调用接口去更改program状态
		if ($scope.page.obsId) {
			taskService.obsoleteTask($scope.page.obsId).then(angular.bind(this, function then() {
				var result = taskService.obsoleteTaskResult;
				// updateListFun();
				$scope.updateListFlag = true;
				$scope.message(result.msg);
			}))
		} else {
			$scope.message('Please \'Save\' before archiving');
		}
	}

	// 根据roomId获取对应的centerName 和 roomName 并把todayDate日期放进对象中
	$scope.getName = function (roomId) {
		taskService.getNameList(roomId).then(angular.bind(this, function then() {
			$scope.condition = taskService.nameResult;
			// 赋值传递进来主页面日历的当天日期
			$scope.condition.todayDate = todayDate;
		}))
	}

	// 新增activity
	$scope.addAct = function () {
		// 计算可以操作的activities 的条目，限制不给超过15条
		var actNum = 0;
		for (var i = 0; i < $scope.pageInfo.data.actList.length; i++) {
			if ($scope.pageInfo.data.actList[i].operation == true) {
				actNum++;
			}
		}
		if (actNum < 15) {
			var arr = {
				name: '',
				operation: true,
				edit: true,
			};
			$scope.pageInfo.data.actList.push(arr);
		} else {
			$scope.message('All operation activities should be less than 15');
		}
	}

	$scope.delAct = function (index) {
		$scope.pageInfo.data.actList.splice(index, 1);
	}

	function isRepeatAct(actArr) {
		for (var i = 0; i < actArr.length; i++) {
			var name = actArr[i].name;
			var count = 0;
			for (var j = 0; j < actArr.length; j++) {
				var nameTmp = actArr[j].name;
				// followup带过来的活动不需要验证,只验证手动创建的活动.
				var operation = (actArr[i].operation == false && actArr[j].operation == false) ? false : true;
				if (name == nameTmp && operation) {
					count++;
				}
			}
			if (count >= 2) {
				return name;
			}
		}
		return '';
	}

	// 保存或者更新页面信息到后台
	$scope.updateInfo = function () {
		// explore / grow /school readiness program编辑框 type 0 : explore / 1 :
		// grow /
		// 3 : school readiness
		// 使用遮障层
		// 防止多次点击
		$scope.showLoading("#ExChangeSet");
		var saveInfo = angular.copy($scope.pageInfo.data);
		var repeatActResult = isRepeatAct(saveInfo.actList);

		if (repeatActResult != '') {
			$scope.message(repeatActResult + ' is repeat!');
			$scope.hideLoading("#ExChangeSet");
			return;
		}
		// 初始化存储taginput 的各个字段拼接字符串的string
		saveInfo.programTaskExgrscInfo.activities = "";

		if (saveInfo.actList.length > 0) {
			for (var i = 0; i < saveInfo.actList.length; i++) {
				if (!saveInfo.actList[i].operation) {
					continue;
				}
				// 如果其状态为编辑状态或者其状态为不可编辑的条目，过滤掉，不保存并删掉
				saveInfo.programTaskExgrscInfo.activities = saveInfo.programTaskExgrscInfo.activities + saveInfo.actList[i].name + ',';
			}
		}

		// 赋值NQS
		saveInfo.nqsVoList = $scope.nqsVoList;
		// 赋值TAG
		saveInfo.tagAccountIds = $scope.tagAccountIds;


		taskService.saveTaskExGrSc($scope.page.type, saveInfo).then(angular.bind(this, function then() {
			if (taskService.saveExGrScResult) {
				var result = taskService.saveExGrScResult;
				if (result.code == 1) {
					$scope.hideLoading("#ExChangeSet");
					$scope.message(result.msg);
					return;
				}
				// 把返回的对象的ID赋值给当前页面使用的对象，方便后续update操作
				$scope.pageInfo.data.programTaskExgrscInfo.id = result.data.programTaskExgrscInfo.id;
				// 回调更新页面List
				// updateListFun();
				$scope.updateListFlag = true;
				if (!$scope.shareFlag) {
					$scope.message('save success');
				}
			}
			// 撤销遮障层
			$scope.hideLoading("#ExChangeSet");
		}))
	}

	// *********************************************************************
	// 文件上传 start
	// **********************************************************************

	$scope.noFileVoList = ["1", "2", "3", "4"];

	function updateNoFileArray(count) {
		var arr = new Array(4 - count);
		for (var i = 0; i < 4 - count; i++) {
			arr[i] = i;
		}
		$scope.noFileVoList = arr;
	};

	/**
	 * @author abliu
	 * @description 文件上传
	 */
	$scope.upload = function ($files, newsInfoObj) {
		if ($files == null || $files.length == 0) {
			return;
		}

		if (newsInfoObj.fileVoList.length + $files.length > 4) {
			// 提示文件重复 todo
			$rootScope.message("You can only upload a maximum of 4 images.");
			return;
		}
		// 判断附件是中重复
		var compareResule = $scope.isRepeatAnnex($files, newsInfoObj);
		if (compareResule != '') {
			// 提示文件重复
			$rootScope.message("Sorry, file " + compareResule + " already exists, please try a different filename.");
			return;
		}
		$scope.nowFilesLength = $files.length + newsInfoObj.fileVoList.length;
		for (var i = 0; i < $files.length; i++) {
			var file = $files[i];
			if (file != null) {
				if (file.size < 10485760) {
					// 等待上传
					$scope.showLoading("#ExChangeSet");
					// 转换大小
					image_resizer.resize({
						image: file,
						max_width: 1024
					}).then(function (data) {
						var fileStrBase64 = data.base64_resized_image;
						var fileType = data.file_type;
						var fileName = data.file_name;
						fileService.uploadResizeImage(fileType, fileStrBase64, fileName).then(angular.bind(this, function then() {
							var result = fileService.fdata;
							if (result.code == 0) {
								var length = newsInfoObj.fileVoList.length;
								var fileVo = {};
								fileVo.relativePathName = result.data.relativePathName;
								fileVo.fileName = result.data.fileName;
								fileVo.visitedUrl = result.data.visitedUrl;
								newsInfoObj.fileVoList.push(fileVo);
								// 更新没图片的集合
								updateNoFileArray($scope.newsInfo.fileVoList.length);
								$('.imageUpload').addClass('slideDown');
							} else {
								$rootScope.message(result.msg);
							}
							if (newsInfoObj.fileVoList.length == $scope.nowFilesLength) {
								$scope.hideLoading("#ExChangeSet");
							}
						}));
					}, function (resize_error) {
						$scope.hideLoading("#ExChangeSet");
						$rootScope.message("Sorry, the filesize is too large, please reduce the filesize and try again.");
					});
				} else {
					$rootScope.message("Sorry, the filesize is too large, please reduce the filesize and try again.");
				}
			}
		}
	};

	/**
	 * @author abliu
	 * @description 删除附件
	 */
	$scope.removeFile = function (fileName, newsInfoObj) {
		for (var i = 0, m = newsInfoObj.fileVoList.length; i < m; i++) {
			if (newsInfoObj.fileVoList[i].relativePathName == fileName) {
				newsInfoObj.fileVoList.splice(i, 1);
				// 更新没图片的集合
				updateNoFileArray($scope.newsInfo.fileVoList.length);
				if ($scope.newsInfo.fileVoList.length == 0) {
					$('.imageUpload').removeClass('slideDown');
				}
				return;
			}
		}
	};

	/**
	 * @author abliu
	 * @description 判断附件是否重复
	 */
	$scope.isRepeatAnnex = function ($files, newsInfoObj) {
		var annxs = "";
		if (newsInfoObj.fileVoList == undefined || newsInfoObj.fileVoList.length == 0) {
			return "";
		}
		for (var j = 0, m = newsInfoObj.fileVoList.length; j < m; j++) {
			for (var i = 0; i < $files.length; i++) {
				if (newsInfoObj.fileVoList[j].fileName == $files[i].name) {
					return $files[i].name;
				}
			}
		}
		return annxs;
	};

	// *********************************************************************
	// 文件上传 end
	// **********************************************************************

	$scope.interestFuc = function (type, id, oId) {
		if (type == 4 || type == 6 || type == 8) {
			InterFunction(type, id, oId);
		} else {
			// 5,7,9 follow up
			InterFullowFunction(type, id, oId);
		}
	}

	$scope.cancel = function () {
		$scope.confirmSuccess = function () {
			$uibModalInstance.dismiss('cancel');
			// 有更新或分享NewsFeed操作,关闭后刷新List
			if ($scope.updateListFlag) {
				updateListFun();
			}
		}
		$rootScope.confirm('Close', 'Exit this page?', 'OK', $scope.confirmSuccess, 'NO');
	};

	// 分享到newsfeed
	/**
	 * @author zhengguo
	 * @description 打开share to newsfeed编辑框
	 * @returns 保存
	 */
	$scope.shareNewfeed = function () {
		$scope.shareFlag = true;
		// 调用保存方法,先保存
		$scope.updateInfo();
		// 给个状态控制是否在保存返回后调用分享的方法
		$rootScope.confirm('Save and Push to Newsfeed', '', 'Share', $scope.ok, 'Cancel');
	};

	$scope.ok = function () {
		//防止多次点击
		$scope.showLoading("#ExChangeSet");
		$scope.programNewsFeedVo = {};
		$scope.programNewsFeedVo.nqsVoList = $scope.nqsVoList;
		$scope.programNewsFeedVo.roomId = $scope.pageInfo.data.programTaskExgrscInfo.roomId;
		$scope.programNewsFeedVo.newsId = $scope.pageInfo.data.programTaskExgrscInfo.newsfeedId;
		$scope.programNewsFeedVo.taskType = $scope.page.type;
		$scope.programNewsFeedVo.actList = $scope.pageInfo.data.actList;
		$scope.programNewsFeedVo.accounts = $scope.tagAccountIds;
		$scope.programNewsFeedVo.content = $scope.pageInfo.data.programTaskExgrscInfo.reflection;
		taskService.shareNewsfeed($scope.programNewsFeedVo, !$scope.pageInfo.data.programTaskExgrscInfo.id ? "" : $scope.pageInfo.data.programTaskExgrscInfo.id).then(angular.bind(this, function then() {
			var result = taskService.shareResult;
			if (result.code == 0) {
				// 关闭modal
				$scope.message('Successfully shared to the newsfeed');
			} else {
				$scope.message(result.msg);
			}
			$scope.shareFlag = false;
			$scope.hideLoading("#ExChangeSet");
		}));
	};

	function initNewsFeedId(newsId) {
		$scope.pageInfo.data.programTaskExgrscInfo.newsfeedId = newsId;
	}

	$scope.nqsVoList = [];
	$scope.saveNqsResult = function (newNqs) {
		$scope.nqsVoList = newNqs;
	};
}
angular.module('kindKidsApp').controller(
	'exploreAndGrowCtrl', ['$scope', '$rootScope', '$timeout', '$location', 'type', 'todayDate', 'roomId', 'ExGrScId', 'functions', 'obsId', 'InterFunction',
		'InterFullowFunction', 'surpassListFlag', 'updateListFun', '$uibModal', '$uibModalInstance', 'image_resizer', 'taskService',
		'fileService', exploreAndGrowCtrl
	]);