'use strict';

/**
 * @author gfwang
 * @description task form
 */
function taskFormCtrl($scope, $rootScope, $timeout, $location, $uibModal, $uibModalInstance, myTaskId, functions, isAfterToday, taskName, taskDate, isParent, taskService) {
	// 获取当前需要编辑的task gfwang
	//	taskFormCtrl|satrt=1478236306653
	console.log('taskFormCtrl|satrt=' + new Date().getTime());
	$scope.isAfterToday = isAfterToday;
	$scope.isParent = isParent;
	$scope.hideLoading('#mainLoad');

	$scope.functions = functions;
	$scope.formSave = function (taskId, type, state, childSave) {
		$scope.showLoading('#myForm');
		var json = childSave.getJson();
		taskService.saveTaskFormValue(taskId, type, state, json).then(angular.bind(this, function then() {
			var result = formService.saveTaskFormValueResult;
			$uibModalInstance.close();
			$scope.hideLoading('#myForm');
		}));
	};

	$scope.obsolete = function (taskId, type) {
		var isFollowup = type == 1 ? true : false;
		// 判断是否有id ，调用接口去更改program状态
		taskService.obsoleteTask(taskId, isFollowup, taskDate).then(angular.bind(this, function then() {
			var result = taskService.obsoleteTaskResult;
			if (result.code == 0) {
				$uibModalInstance.close();
			} else {
				$scope.message(result.msg);
			}
		}));
	};

	$scope.initList = function () {
		console.log('taskFormCtrl|initList|satrt=' + new Date().getTime());
		$scope.showLoading('#myForm');
		taskService.getFormInstance(myTaskId).then(angular.bind(this, function then() {
			var result = taskService.formList;
			console.log('taskFormCtrl|initList|middle1=' + new Date().getTime());
			// var taskList = handleTaskList(result.data);
			$scope.list = getInstanceId(result.data);
			console.log('taskFormCtrl|initList|middle2=' + new Date().getTime());
			// 打开form表单
			$scope.currtenIndex = $scope.list.length;
			var flag = true;
			for (var i = 0; i < $scope.list.length; i++) {
				var item = $scope.list[i];
				if (item.statu == 0 && flag) {
					$scope.currtenIndex = i;
					flag = false;
				}
			}
			console.log('taskFormCtrl|initList|end=' + new Date().getTime());
			$scope.hideLoading('#myForm');
			$('.modal-dialog').addClass('modal-full');
		}));
	};

	function getInstanceId(data) {
		var list = new Array();
		list.push({
			code: data.formId,
			taskId: data.id,
			statu: data.statu,
			instanceId: data.valueId,
			type: 0,
			taskName: data.taskName,
			childSave: {},
			operationFlag: data.operationFlag,
			nqsVos: data.nqsVos
		});
		var followName = 'Follow up ';
		for (var i = 0; i < data.followInstanceInfos.length; i++) {
			var follow = data.followInstanceInfos[i];
			list.push({
				code: follow.formId,
				taskId: follow.id,
				statu: follow.statu,
				instanceId: follow.valueId,
				type: 1,
				taskName: followName + ' ' + (i + 1),
				childSave: {},
				operationFlag: follow.operationFlag
			});
		}
		return list;
	}
	$scope.cancel = function () {
		$scope.confirm('Close', 'Exit this page?', 'OK', function () {
			$uibModalInstance.close();
		}, 'NO');
	};
}
angular.module('kindKidsApp').controller('taskFormCtrl', ['$scope', '$rootScope', '$timeout', '$location', '$uibModal', '$uibModalInstance', 'myTaskId', 'functions', 'isAfterToday', 'taskName', 'taskDate', 'isParent', 'taskService', taskFormCtrl]);