'use strict';

/**
 * @author gfwang
 * @description task form
 */
function hatMoneyTaskCtrl($scope, $rootScope, $timeout, $location, $uibModal,
		$uibModalInstance, obj, taskService) {
	
	$scope.title=obj.title;
	$scope.content=obj.content;
	$scope.functions=obj.functions;
	$scope.isAfterToday=obj.isAfterToday;
	$scope.obsolete = function() {
		$scope.showLoading('#myForm');
		// 判断是否有id ，调用接口去更改program状态
		taskService.obsoleteTask(obj.keyId).then(angular.bind(this, function then() {
			var result = taskService.obsoleteTaskResult;
			if (result.code == 0) {
				$uibModalInstance.close(true);
			} else {
				$scope.message(result.msg);
			}
			$scope.hideLoading('#myForm');
		}));
	};
	$scope.submit = function() {
		$scope.showLoading('#myForm');
		taskService.saveTaskFormValue(obj.keyId, 0, 1).then(angular.bind(this, function then() {
			var result = taskService.saveTaskFormValueResult;
			if (result.code == 0) {
				$uibModalInstance.close(true);
			} else {
				$scope.message(result.msg);
			}
			$scope.hideLoading('#myForm');
		}));
	};
	$scope.cancel = function() {
		$scope.confirm('Close', 'Exit this page?', 'OK', function() {
			$uibModalInstance.close();
		}, 'NO');
	};
}
angular.module('kindKidsApp').controller('hatMoneyTaskCtrl',
		[ '$scope', '$rootScope', '$timeout', '$location', '$uibModal', '$uibModalInstance', 'obj', 'taskService', hatMoneyTaskCtrl ]);