'use strict';

/**
 * @author zhengguo
 * @description task view Controller
 */
function taskViewCtrl($scope, $filter, $timeout, $location, $uibModal, taskService, commonService, staffService, $stateParams) {

	// 页面list参数对象
	$scope.condition = {
		maxSize: 7,
		pageIndex: 1,
		// 获取页面信息时，后台获取
		/*
		 * totalItems:30, pageSize:2
		 */
	};
	$scope.functions = [];
	commonService.getFun("/task").then(angular.bind(this, function then() {
		$scope.functions = commonService.functions;
	}));
	// 存放当前日期
	$scope.taskDate = null;
	$scope.page = {};
	$scope.page.condition = {
		keyword: null,
		time: null,
		roomId: null,
		centersId: null,
		type: 2,// type 0:传日期进行搜索，1：上一周 2：本周 3：下一周 4: 左侧菜单，保留weekdate查询
		weekDate: null,
		listType: 1,
		programType: '',
		programStatus: '0',
		nqsVersion: ""
	};
	$scope.isCenterManager = $filter('haveRole')($scope.currentUser.roleInfoVoList, '1;5');

	// 获取页面日历
	$scope.getPageCalendar = function () {
		// delete by big 解决园长第一次进来选择allcentre 但是又让centersId赋值了 导致看不见meeting task
		/*
		 * if ($scope.isCenterManager) { $scope.page.condition.centersId =
		 * $scope.currentUser.userInfo.centersId; }
		 */
		/* console.log($scope.page.condition); */
		taskService.getProgramList($scope.page.condition).then(angular.bind(this, function then() {
			var result = taskService.programListResult;
			var onePage = angular.copy($scope.page.condition);
			if (result.code == 0) {
				$scope.page = result.data;
				// 如果programType，programStatus有值，则需要保留重新赋值
				if (onePage.programType) {
					$scope.page.condition.programType = onePage.programType;
				}
				if (onePage.programStatus) {
					$scope.page.condition.programStatus = onePage.programStatus;
				}
				// 重新请求日历时，更新taskDate/循环出来哪天是today
				for (var i = 0; i < result.data.weekDays.length; i++) {
					if (result.data.weekDays[i].today == true) {
						$scope.taskDate = result.data.weekDays[i].date;
						break;
					}
				}
				// 更新页面数据
				$scope.getPageInfo();
			} else {
				$.messager.popup(result.msg);
			}
		}));
	};

	$scope.setNQS = function (value) {
		$scope.condition.search = true;
		$scope.page.condition.nqsVersion = value;
		$scope.getPageInfo();
	};

	$scope.newsKeyup = function (ev) {
		if (ev.keyCode == 13) {
			$scope.condition.search = true;
			$scope.getPageInfo();
		}
	};

	$scope.search = function () {
		$scope.condition.search = true;
		$scope.getPageInfo();
	}

	// 根据页面日历对应的program list
	$scope.getPageInfo = function () {
		$scope.showLoading(".mainList");
		// 重置
		$scope.surpassListFlag = false;
		// 大于当前日期不请求list数据2016-09-29T19:49:53.000
		var nowDate = new Date($scope.page.condition.now.substr(0, 10));
		var nowMonth = nowDate.getMonth() >= 9 ? nowDate.getMonth() + 1 : '0' + (nowDate.getMonth() + 1);
		var nowDay = nowDate.getDate() > 9 ? nowDate.getDate() : '0' + nowDate.getDate();
		var inNowDate = nowDate.getFullYear() + "-" + nowMonth + "-" + nowDay;
		var indate = new Date(inNowDate);
		var nowTaskDate = $scope.taskDate.substr(0, 10);
		var intaskdate = new Date(nowTaskDate);
		// 当前日期大于等于program list 列表日期时，请求后台获取列表,给一个标识，
		// 请求的列表控制不能分享，系统定时任务生成的task为可编辑保存，不可分享newsfeed;包括：explore / grow /
		// school readiness / follow up
		// 如果有其他task，则可只展示
		// 隐藏create task 按钮中的其他task，禁止创建，除了other task 可以创建
		if (indate < intaskdate) {
			$scope.surpassListFlag = true;
		}
		var json = {
			statu: $scope.page.condition.programStatus ? $scope.page.condition.programStatus : null,
			centreId: $scope.page.condition.centersId ? $scope.page.condition.centersId : null,
			roomId: $scope.page.condition.roomId ? $scope.page.condition.roomId : null,
			type: $scope.page.condition.programType ? $scope.page.condition.programType : null,
			day: $scope.taskDate,
			// pageSize : 15,
			pageIndex: $scope.page.condition.chooseLocation ? $scope.page.condition.pageIndex : $scope.condition.pageIndex,
			nqsVersion: $scope.page.condition.nqsVersion,
		}
		// 获取页面LIST
		taskService.getProgramListInfo(json).then(angular.bind(this, function then() {
			var result = taskService.programListInfoResult;
			if (result.code == 0) {
				$scope.pageList = result.data;
				// 判断是否图片超过4张
				for (var i = 0; i < $scope.pageList.list.length; i++) {
					if ($scope.pageList.list[i].avatar) {
						if ($scope.pageList.list[i].avatar.length > 4) {
							$scope.pageList.list[i].avatar.splice(3, $scope.pageList.list[i].avatar.length - 3);
							$scope.pageList.list[i].moreAva = true;
						} else {
							$scope.pageList.list[i].moreAva = false;
						}
					}
				}
				// 分页设置
				$scope.condition.totalItems = result.data.condition.totalSize;
				$scope.condition.pageSize = result.data.condition.pageSize;
				$scope.condition.pageIndex = result.data.condition.pageIndex;
				$scope.page.condition.nqsVersion = result.data.condition.nqsVersion;
				if ($scope.condition.search && $scope.pageList.list.length == 0 && $scope.condition.pageIndex == 1) {
					$scope.confirm('Message', 'No results match your search.', 'OK', function () {
						$("#privacy-modal").modal('hide');
					});
				}
				$scope.condition.search = false;
			} else {
				$.messager.popup(result.msg);
			}
			$scope.hideLoading(".mainList");
		}));

		// 获取Today's Note
		taskService.getNoteList(json).then(angular.bind(this, function then() {
			var result = taskService.noteListResult;
			if (result.code == 0) {
				$scope.showOutData = result.data;
				/*
				 * // 处理时间 11：12 PM if ($scope.showOutData &&
				 * $scope.showOutData.medicationInfos) { for (var i = 0; i <
				 * $scope.showOutData.medicationInfos.length; i++) { for (var j =
				 * 0; j <
				 * $scope.showOutData.medicationInfos[i].timeMedicationInfos.length;
				 * j++) {
				 * $scope.showOutData.medicationInfos[i].timeMedicationInfos[j].day =
				 * $scope
				 * .changeBackTime($scope.showOutData.medicationInfos[i].timeMedicationInfos[j].day); } } }
				 */
				$scope.page.condition.chooseLocation = false;
			}
		}));

		$scope.getTaskCount(json);
		/*
		 * } else { if ($scope.pageList && $scope.pageList.list) {
		 * $scope.pageList.list = []; // 分页设置 $scope.condition.totalItems = 0; } }
		 */
	};

	// 分页变化
	$scope.changeLeavePage = function () {
		$scope.getPageInfo();
	}

	// 设置年范围 设置年份集合，从2010年开始到当前年加1年
	$scope.getYearList = function () {
		$scope.yearList = [];
		var yyyy = new Date().getFullYear();
		var m = yyyy - 2015;
		for (var i = 0; i <= m + 1; i++) {
			$scope.yearList.push(2015 + i);
		}
	};
	// 设置年份，调用方法
	$scope.setTimeY = function (yyyy) {
		if ($scope.page.condition.time != null) {
			var times = $scope.page.condition.time.split("/");
			if (times.length > 1) {
				$scope.page.condition.time = yyyy + "/" + times[1];
				$scope.page.condition.type = 0;
				// 初始化页码到第一页
				$scope.condition.pageIndex = 1;
				$scope.getPageCalendar();
			}
		}
	};

	// 设置月份调用方法
	$scope.setTimeM = function (mm) {
		if ($scope.page.condition.time != null) {
			var times = $scope.page.condition.time.split("/");
			if (times.length > 1) {
				$scope.page.condition.time = times[0] + "/" + mm;
				$scope.page.condition.type = 0;
				// 初始化页码到第一页
				$scope.condition.pageIndex = 1;
				$scope.getPageCalendar();
			}
		}
	};
	// 设置type //type 0:传日期进行搜索，1：上一周 2：本周 3：下一周 4: 左侧菜单，保留weekdate查询
	$scope.setType = function (type) {
		$scope.condition.search = true;
		$scope.page.condition.type = type;
		// 初始化页码到第一页
		$scope.condition.pageIndex = 1;
		$scope.getPageCalendar();
	};

	$scope.toThisDay = function (inDate) {
		// 更新页面红点所在日期样式
		for (var i = 0; i < $scope.page.weekDays.length; i++) {
			$scope.page.weekDays[i].today = false;
		}
		var arr = inDate.date.split('-');
		$scope.page.condition.time = arr[0] + '/' + arr[1];
		inDate.today = true;
		// 更新页面list数据
		var p = angular.copy(inDate);
		$scope.taskDate = p.date;
		// 初始化页码到第一页
		$scope.condition.pageIndex = 1;
		$scope.getPageInfo();
	};

	function getRoomId(item) {
		if (!item) {
			return $scope.page.condition.roomId;
		}
		return item.roomId;
	}

	$scope.initTaskView = function () {

		commonService.getMenuOrgs(5).then(angular.bind(this, function then() {
			$scope.orgs = commonService.orgList;
		}));

		/*
		 * $scope.currUser = { roleValue : 0, centersId : '' }
		 */
		$scope.getPageCalendar();
		$scope.getYearList();
	}

	// 搜索条件过滤器
	/*
	 * $scope.locationFilter = function(item) { if ($scope.currUser.roleValue ==
	 * 0) { return true; } if ($scope.currUser.centerId == null) { return false; }
	 * if ($scope.currUser.roleValue == 1 || $scope.currUser.roleValue == 5) {
	 * if (item.id == $scope.currUser.centerId) { return true; } return false; }
	 * return false; }
	 */

	// 列表编辑
	$scope.openModal = function (type, id, item) {
		if (type == 0 || type == 1 || type == 3) {
			$scope.exploreAndGrowModal(type, id, item);
		}
		if (type == 2) {
			$scope.lessonModal(type, id, item);
		}
		if (type == 4 || type == 6 || type == 8) {
			$scope.interestModal(type, id, item);
		}
		if (type == 5 || type == 7 || type == 9) {
			$scope.interestFollowUpModal(type, id, item);
		}
		if (type == 10) {
			$scope.evalutionModal(type, id, item);
		}
		if (type == 11 || type == 12 || type == 13 || type == 14 || type == 15) {
			$scope.registerModal(type, id, item);
		}
		// 自定义task
		if (type == 17) {
			console.log('openModal|satrt=' + new Date().getTime());
			/* $scope.message('In development...'); return; */

			var myTaskId = item.id;
			var taskName = item.taskName;
			$scope.openTaskModal(myTaskId, taskName);
		}
		if (type == 18) {
			$scope.medicationModal(type, id, item);
		}
		// meeting gfwang add
		if (type == 20) {
			$scope.openMeetingTask(item);
		}
		if (type == 21) {
			$scope.openHatMoneyModel(item, 'Money Task', 'Are you Complete?');
		}
		if (type == 22) {
			$scope.openHatMoneyModel(item, 'Hat Task', 'Are you Complete?');
		}
	};
	function dateComp(chooseDate) {
		return new moment(chooseDate, 'YYYY-MM-DD').isAfter(new moment($scope.currentUser.currtenDate, 'DD/MM/YYYY'));
	}

	$scope.openHatMoneyModel = function (obj, title, content) {
		if (!modalOpen) {
			return;
		}
		var isAfterToday = dateComp($scope.taskDate);
		$uibModal.open({
			templateUrl: './views/program/hat.money.html?res_v=' + $scope.res_v,
			controller: 'hatMoneyTaskCtrl',
			backdrop: 'static',
			keyboard: false,
			windowClass: 'modal-grey task-popup',
			animation: true,
			resolve: {
				obj: function () {
					return {
						title: title,
						content: content,
						keyId: obj.id,
						functions: $scope.functions,
						isAfterToday: isAfterToday
					};
				}
			}

		}).result.then(function (success) {
			if (success) {
				$scope.getPageInfo();
			}
		});
	};
	$scope.openMeetingTask = function (obj) {
		if (!modalOpen) {
			return;
		}
		var isAfterToday = dateComp($scope.taskDate);
		$uibModal.open({
			templateUrl: './views/program/view.meeting.html?res_v=' + $scope.res_v,
			controller: 'viewMeetingTaskCtrl',
			backdrop: 'static',
			keyboard: false,
			windowClass: 'modal-grey task-popup',
			animation: true,
			size: "lg",
			resolve: {
				obj: function () {
					return {
						rowId: obj.valueId,
						state: obj.statu,
						keyId: obj.id,
						functions: $scope.functions,
						isAfterToday: isAfterToday
					};
				}
			}

		}).result.then(function (success) {
			if (success) {
				$scope.getPageInfo();
			}
		});
	};

	var modalOpen = true;

	/** *****************自定义表单填写 gfwang*************** */
	$scope.openTaskModal = function (myTaskId, taskName) {
		// openTaskModal|satrt=1478236301639
		console.log('openTaskModal|satrt=' + new Date().getTime());
		if (!modalOpen) {
			return;
		}
		$scope.showLoading('#mainLoad');
		var isAfterToday = dateComp($scope.taskDate);
		modalOpen = false;
		$uibModal.open({
			templateUrl: 'views/program/task.form.html?res_v=' + $scope.res_v,
			controller: 'taskFormCtrl',
			backdrop: 'static',
			keyboard: false,
			windowClass: 'modal-grey task-popup',
			animation: true,
			resolve: {
				myTaskId: function () {
					return myTaskId;
				},
				taskName: function () {
					return taskName;
				},
				functions: function () {
					return $scope.functions;
				},
				isAfterToday: function () {
					return isAfterToday;
				},
				taskDate: function () {
					return $scope.taskDate;
				},
				isParent: function () {
					return null;
				}
			}

		}).result.then(function (instanceId) {
			modalOpen = true;
			$scope.getPageInfo();
		});
		modalOpen = true;
	};
	/** *****************自定义表单填写 gfwang*************** */

	/**
	 * @author zhengguo
	 * @description 打开explore / grow /school readiness program编辑框 type 0 :
	 *              explore / 1 : grow / 3 : school readiness
	 * @returns 保存/删除/编辑
	 */
	$scope.exploreAndGrowModal = function (type, ExGrScId, item) {
		if (!modalOpen) {
			return;
		}
		if (!$scope.page.condition.roomId && !ExGrScId) {
			$scope.message('please select a room first');
		} else {
			$uibModal.open({
				templateUrl: 'views/program/exploreAndGrow.html?res_v=' + $scope.res_v,
				controller: 'exploreAndGrowCtrl',
				backdrop: 'static',
				keyboard: false,
				windowClass: 'modal-grey task-popup modal-align-top',
				resolve: {
					type: function () {
						return type;
					},
					roomId: function () {
						return getRoomId(item);
					},
					ExGrScId: function () {
						return ExGrScId;
					},
					obsId: function () {
						return !item ? "" : item.id;
					},
					surpassListFlag: function () {
						return $scope.surpassListFlag;
					},
					InterFunction: function () {
						return $scope.interestModal;
					},
					InterFullowFunction: function () {
						return $scope.interestFollowUpModal;
					},
					todayDate: function () {
						return $scope.taskDate;
					},
					updateListFun: function () {
						return $scope.getPageInfo;
					},
					functions: function () {
						return $scope.functions;
					}
				}
			})
		}
	};

	/**
	 * @author zhengguo
	 * @description 打开 interest 、observation and learningStory program编辑框 type 4 :
	 *              interest / 6 : observation / 8 : learningStory
	 * @returns 保存/删除/编辑
	 */
	$scope.interestModal = function (type, intobsleaId, item) {
		if (!modalOpen) {
			return;
		}
		if (!$scope.page.condition.roomId && !intobsleaId) {
			$scope.message('please select a room first');
		} else {
			var modalInstance = $uibModal.open({
				templateUrl: 'views/program/interest-ob-learn.html?res_v=' + $scope.res_v,
				controller: 'interestCtrl',
				backdrop: 'static',
				keyboard: false,
				windowClass: 'modal-grey task-popup',
				resolve: {
					type: function () {
						return type;
					},
					todayDate: function () {
						return $scope.taskDate;
					},
					surpassListFlag: function () {
						return $scope.surpassListFlag;
					},
					roomId: function () {
						return getRoomId(item);
					},
					intobsleaId: function () {
						return intobsleaId;
					},
					obsId: function () {
						return !item ? "" : item.id;
					},
					updateListFun: function () {
						return $scope.getPageInfo;
					},
					functions: function () {
						return $scope.functions;
					},
					isDisable: function () {
						return false;
					},
					obj: function () {
						return null;
					}
				}
			})
		}
	};

	/**
	 * @author zhengguo
	 * @description 打开 interest 、observation and learningStory follow up
	 *              program编辑框 type 5 : interest / 7 : observation / 9 :
	 *              learningStory
	 * @returns 保存/删除/编辑
	 */
	$scope.interestFollowUpModal = function (type, intobsleaId, item) {
		if (!modalOpen) {
			return;
		}
		var isAfterToday = dateComp($scope.taskDate);
		$uibModal.open({
			templateUrl: 'views/program/interest-ob-learn-followUp.html?res_v=' + $scope.res_v,
			controller: 'interestFollowUpCtrl',
			backdrop: 'static',
			keyboard: false,
			windowClass: 'modal-grey task-popup modal-align-top',
			resolve: {
				type: function () {
					return type;
				},
				intobsleaId: function () {
					return intobsleaId;
				},
				obsId: function () {
					return !item ? "" : item.id;
				},
				roomId: function () {
					return $scope.page.condition.roomId;
				},
				surpassListFlag: function () {
					return $scope.surpassListFlag;
				},
				todayDate: function () {
					return $scope.taskDate;
				},
				updateListFun: function () {
					return $scope.getPageInfo;
				},
				functions: function () {
					return $scope.functions;
				},
				isAfterToday: function () {
					return isAfterToday;
				}
			}
		});
	};

	/**
	 * @author zhengguo
	 * @description 打开lesson program编辑框
	 * @returns 保存/删除/编辑
	 */
	$scope.lessonModal = function (type, lessonId, item) {
		if (!modalOpen) {
			return;
		}
		if (!$scope.page.condition.roomId && !lessonId) {
			$scope.message('please select a room first');
		} else {
			var modalInstance = $uibModal.open({
				templateUrl: 'views/program/lesson.html?res_v=' + $scope.res_v,
				controller: 'lessonCtrl',
				backdrop: 'static',
				keyboard: false,
				windowClass: 'modal-grey task-popup modal-align-top',
				resolve: {
					type: function () {
						return type;
					},
					roomId: function () {
						return getRoomId(item);
					},
					surpassListFlag: function () {
						return $scope.surpassListFlag;
					},
					lessonId: function () {
						return lessonId;
					},
					obsId: function () {
						return !item ? "" : item.id;
					},
					todayDate: function () {
						return $scope.taskDate;
					},
					updateListFun: function () {
						return $scope.getPageInfo;
					},
					functions: function () {
						return $scope.functions;
					}
				}
			})
		}
	};

	/**
	 * @author zhengguo
	 * @description 打开 Weekly Evalution program编辑框
	 * @returns 保存/删除/编辑
	 */
	$scope.evalutionModal = function (type, evalId, item) {
		if (!modalOpen) {
			return;
		}
		if (!$scope.page.condition.roomId && !evalId) {
			$scope.message('please select a room first');
		} else {
			var isAfterToday = dateComp($scope.taskDate);
			$uibModal.open({
				templateUrl: 'views/program/weekly.html?res_v=' + $scope.res_v,
				controller: 'weeklyCtrl',
				backdrop: 'static',
				keyboard: false,
				windowClass: 'modal-grey task-popup',
				resolve: {
					type: function () {
						return type;
					},
					roomId: function () {
						return getRoomId(item);
					},
					evalId: function () {
						return evalId;
					},
					obsId: function () {
						return !item ? "" : item.id;
					},
					todayDate: function () {
						return $scope.taskDate;
					},
					updateListFun: function () {
						return $scope.getPageInfo;
					},
					functions: function () {
						return $scope.functions;
					},
					isAfterToday: function () {
						return isAfterToday;
					}
				}
			})
		}
	};

	/**
	 * @author zhengguo
	 * @description 打开 register program编辑框 type-----11：meal / 12:sleep / 13:
	 *              nappy / 14:tolit / 15 : sunscreen
	 * @returns 保存/删除/编辑
	 */
	$scope.registerModal = function (type, registerId, item) {
		if (!modalOpen) {
			return;
		}
		if (!$scope.page.condition.roomId && !registerId) {
			$scope.message('please select a room first');
		} else {
			var modalInstance = $uibModal.open({
				templateUrl: 'views/program/register.html?res_v=' + $scope.res_v,
				controller: 'registerCtrl',
				backdrop: 'static',
				keyboard: false,
				windowClass: 'modal-grey task-popup',
				resolve: {
					type: function () {
						return type;
					},
					roomId: function () {
						return getRoomId(item);
					},
					registerId: function () {
						return registerId;
					},
					obsId: function () {
						return !item ? "" : item.id;
					},
					todayDate: function () {
						return $scope.taskDate;
					},
					updateListFun: function () {
						return $scope.getPageInfo;
					},
					functions: function () {
						return $scope.functions;
					}
				}
			})
		}
	};

	/**
	 * @author zhengguo
	 * @description 打开 medication program编辑框 type-----16：medication
	 * @returns 保存/删除/编辑
	 */
	$scope.medicationModal = function (type, medicationId, item) {
		if (!modalOpen) {
			return;
		}
		if (!$scope.page.condition.roomId && !medicationId) {
			$scope.message('please select a room first');
		} else {
			var modalInstance = $uibModal.open({
				templateUrl: 'views/program/medication.html?res_v=' + $scope.res_v,
				controller: 'medicationCtrl',
				backdrop: 'static',
				keyboard: false,
				windowClass: 'modal-grey',
				resolve: {
					type: function () {
						return type;
					},
					roomId: function () {
						return getRoomId(item);
					},
					medicationId: function () {
						return medicationId;
					},
					obsId: function () {
						return !item ? "" : item.id;
					},
					surpassListFlag: function () {
						return $scope.surpassListFlag;
					},
					todayDate: function () {
						return $scope.taskDate;
					},
					updateListFun: function () {
						return $scope.getPageInfo;
					},
					functions: function () {
						return $scope.functions;
					},
					isParent: function () {
						return null;
					}
				}
			})
		}
	};

	/**
	 * @author zhengguo
	 * @description 打开 other task program type-----17
	 * @returns 选择
	 */
	$scope.otherTaskModal = function (type) {
		if (!modalOpen) {
			return;
		}
		$uibModal.open({
			templateUrl: 'views/program/otherTask.html?res_v=' + $scope.res_v,
			controller: 'otherTaskCtrl',
			backdrop: 'static',
			keyboard: false,
			windowClass: 'modal-grey',
			resolve: {
				type: function () {
					return type;
				},
				roomId: function () {
					return $scope.page.condition.roomId;
				},
				reloadPage: function () {
					return $scope.getPageInfo;
				},
				chooseDate: function () {
					return $scope.taskDate;
				},
				isIncRep: function () {
					return false;
				}
			}
		});
	};
	$scope.incidentReportModal = function () {
		if (!modalOpen) {
			return;
		}
		$uibModal.open({
			templateUrl: 'views/program/otherTask.html?res_v=' + $scope.res_v,
			controller: 'otherTaskCtrl',
			backdrop: 'static',
			keyboard: false,
			windowClass: 'modal-grey',
			resolve: {
				type: function () {
					return 16;
				},
				roomId: function () {
					return $scope.page.condition.roomId;
				},
				reloadPage: function () {
					return $scope.getPageInfo;
				},
				chooseDate: function () {
					return $scope.taskDate;
				},
				isIncRep: function () {
					return true;
				}
			}
		});
	};

	$scope.getTaskCount = function (json) {
		taskService.getTaskCount(json).then(angular.bind(this, function then() {
			$scope.taskCounts = taskService.taskCountResult.data;
		}));

	};

}
angular.module('kindKidsApp').controller('taskViewCtrl',
	['$scope', '$filter', '$timeout', '$location', '$uibModal', 'taskService', 'commonService', 'staffService', '$stateParams', taskViewCtrl]);