'use strict';

/**
 * @author abliu
 * @description message Controller
 */
function messageCtrl($scope, $rootScope, $timeout, $window, $uibModal, $filter,
	messageService, fileService) {
	$rootScope.subScope = $scope;

	// variable=================================================================
	$scope.page = {};
	$scope.page.condition = {
		pageIndex: 1,
		readFlag: '-1',
		nqsVersion: ""
	};
	$scope.messageInfo = {};
	$scope.busy = false;
	// select===================================================================

	$scope.defaultList = {};

	$scope.select2Model = {};

	/**
	 * @author abliu
	 * @description 向下滚动获取message列表
	 */
	$scope.getMessageList = function (isDown) {
		if (isDown) {
			// 如果当前页面中list的个数大于，等于数据库中所有记录数，则不请求后台
			if ($scope.page.list.length >= $scope.page.condition.totalSize) {
				return;
			}
			$scope.page.condition.pageIndex += 1;
			$scope.busy = true;
		}
		// 请求service后台返回message列表数据
		messageService.getMessageList($scope.page.condition).then(
			angular.bind(this, function then() {
				var result = messageService.listResult;
				if (result.code == 0) {
					$scope.dealListResult(result.data, isDown);
					if (result.data.condition.search && result.data.list.length == 0) {
						$scope.confirm('Message', 'No results match your search.', 'OK', function () {
							$("#privacy-modal").modal('hide');
						});
					}
					$scope.page.condition.search = false;
				} else {
					$rootScope.message(result.msg);
				}

			}));
	};
	/**
	 * 处理查询结果
	 */
	$scope.dealListResult = function (data, isAppend) {
		// 判断是否为 追加
		if (isAppend) {
			if (!$scope.page.list) {
				$scope.page = data;
				// $scope.defaultList = data;
			} else {
				$scope.page.condition = data.condition;
				var length = data.list.length;
				for (var i = 0; i < length; i++) {
					// 向列表追加元素
					$scope.page.list.push(data.list[i]);
				}
			}
			$timeout(function () {
				$scope.busy = false;
			}, 2000);
		} else {
			$scope.page = data;
			if ($scope.page.list.length == 0) {
				$scope.visibility = {
					"visibility": "visible"
				};
				$scope.initshow = false;
			}
		}
	}

	/**
	 * @author abliu
	 * @description 排序
	 */
	$scope.messageSort = {
		column: 'm_subject',
		direction: 1,
		toggle: function (column) {
			if (this.column == column) {
				this.direction = -this.direction || -1;
			} else {
				this.column = column;
				this.direction = -1;
			}

			$scope.page.condition.sortName = this.column;
			if (this.direction == -1) {
				$scope.page.condition.sortOrder = "desc";
			} else {
				$scope.page.condition.sortOrder = "asc";
			}
			$scope.getMessageList();
		}
	};

	$scope.scorellUp = function () {
		$scope.page.condition.pageIndex = 1;
		$scope.getMessageList(false);
	}
	/**
	 * @author abliu
	 * @description 获取message列表
	 */
	$scope.getMessages = function () {
		$scope.clearData();
		$scope.messages = {};
		$scope.titleReceive = [];
		$scope.showLoading("#main_edit");
		var msgId = $scope.$stateParams.messageId;
		// 请求service后台返回message数据
		messageService.getMessageInfo(msgId).then(
			angular.bind(this, function then() {
				var result = messageService.singleResult;
				if (result.code == 0) {
					$scope.messages = result.data;
					$scope.userList = $scope.messages[0].userList;
					$scope.titleReceive = $filter("orderBy")(
						$scope.messages[0].receiverVo, 'groupName');
					// 获取收件人，发件人
					$scope.getDefaultUsers();
					$rootScope.getUnreadFunction();
				} else {
					$rootScope.message(result.msg);
				}
				$scope.hideLoading("#main_edit");
			}));

	};
	/**
	 * 当前邮件的收件人，发件人
	 */
	$scope.defaultUser = "";
	/**
	 * 获取当前邮件的发件人，和所有接收人
	 */
	$scope.getDefaultUsers = function () {
		var mainMessage = $scope.messages[0];
		// 取主邮件发送人
		var receivedAccounts = mainMessage.sendAccountId + ",";
		// 获取主邮件的收件人
		for (var i = 0; i < mainMessage.receiverVo.length; i++) {
			receivedAccounts += mainMessage.receiverVo[i].id + ",";
		}
		$scope.defaultUser = receivedAccounts;
	}

	$scope.repalyForward = {};

	/**
	 * 保存
	 */
	$scope.createNewMessageInfo = function (type) {
		$scope.showLoading('#myCreate');
		if (!$scope.initMessageInfo(type)) {
			$scope.hideLoading("#myCreate");
			return;
		}
		messageService.saveMessageInfo($scope.messageInfo).then(
			angular.bind(this, function then() {
				var result = messageService.data;
				$scope.hideLoading("#myCreate");
				if (result.code == 0) {
					$scope.messageInfo = result.data;
					$scope.clearData();
					$scope.$state.reload();
				} else {
					$rootScope.message(result.msg);
				}
			}));
	}
	$scope.clearData = function () {
		$scope.chooseNqs = [];
		$scope.messageInfo = {};
		$scope.fileList = [];
		$scope.messageInfo.viewAccountIds = "";
		$scope.repalyForward.replayContent = "";
		$scope.defaultUser = "";
	}
	/**
	 * 跳转到列表
	 */
	$scope.goList = function () {
		$rootScope.$state.go("app.main.message.list");
	}
	/**
	 * nqs数组转str
	 */
	$scope.nqsArr2Str = function (arr) {
		var size = arr.length;
		var str = "";
		for (var i = 0; i < size; i++) {
			var item = arr[i];
			str += item.version + ",";
		}
		return str;
	}
	/**
	 * 初始化 邮件信息
	 */
	$scope.initMessageInfo = function (type) {
		$scope.messageInfo.messageType = type;
		$scope.messageInfo.chooseNqs = $scope.chooseNqs;
		// 正常发送
		if (type == 1 || type == 2) {
			// 回复1人
			// 初始化，只需要接收人和内容
			$scope.messageInfo = {};
			// 设置发送类型
			$scope.messageInfo.messageType = type;

			var mainMessage = $scope.messages[0];
			// 取主邮件发送人
			var receivedAccounts = mainMessage.sendAccountId + ",";
			// 获取主邮件的收件人
			if (type == 2) {
				for (var i = 0; i < mainMessage.receiverVo.length; i++) {
					receivedAccounts += mainMessage.receiverVo[i].id + ",";
				}
			}
			// 所有回复人
			$scope.messageInfo.receiverIds = receivedAccounts;
			// 内容
			$scope.messageInfo.content = $scope.repalyForward.replayContent;
			$scope.messageInfo.msgId = $scope.$stateParams.messageId;

			return true;
		} else if (type == 3) {
			// 转发
			var mainMessage = $scope.messages[0];
			$scope.messageInfo.msgId = mainMessage.msgId;
			// 转发内容
			$scope.messageInfo.content = $scope.repalyForward.forwardContent;
			$scope.messageInfo.receiverIds = $scope
				.getSendUsersStr($scope.messageInfo.viewAccountIds);
			return true
		}
		$scope.messageInfo.receiverIds = $scope
			.getSendUsersStr($scope.messageInfo.viewAccountIds);

		return true;
	}

	/**
	 * 把接收人凭借字符串
	 */
	$scope.getSendUsersStr = function (array) {
		var value = "";
		if (!array) {
			return value;
		}
		var len = array.length;
		for (var i = 0; i < len; i++) {
			value = value + array[i] + ',';
		}
		return value;
	}
	/**
	 * @author hxhzang
	 * @description 回车执行翻页以及查询
	 */
	$scope.enterSearch = function (ev) {
		if (ev.keyCode == 13) {
			$scope.search();
		}
	};

	$scope.setNQS = function (value) {
		$scope.page.condition.nqsVersion = value;
		$scope.search();
	};
	$scope.newsKeyup = function (ev) {
		if (ev.keyCode == 13) {
			$scope.search();
		}
	};
	$scope.search = function () {
		$scope.goList();
		$scope.page.condition.pageIndex = 1;
		$scope.page.condition.search = true;
		$scope.getMessageList();
	};

	// 打开创建message框
	$scope.openCreateMsg = function () {
		$uibModal.open({
			templateUrl: 'views/message/createMessage.html?res_v=' +
				$scope.res_v,
			controller: 'msgCreateCtrl',
			backdrop: 'static',
			keyboard: false,
			windowClass: "message-create",
			resolve: {

			}
		});
	};
	$scope.changeShow = function () {
		$scope.showMore = true;
	}
	$scope.showMore = false;
	$scope.addPerson = function () {
		$uibModal.open({
			templateUrl: 'views/message/forward.html?res_v=' + $scope.res_v,
			controller: 'msgForwardCtrl',
			backdrop: 'static',
			keyboard: false,
			windowClass: "message-create",
			resolve: {
				defaultUser: function () {
					return $scope.defaultUser;
				},
				msgId: function () {
					return $scope.$stateParams.messageId;
				}
			}
		}).result.then(function (userList) {
			if (!userList) {

				return;
			}
			initLookUser(userList);
		});;
	};

	function initLookUser(userList) {
		for (var i = 0; i < userList.length; i++) {
			var item = userList[i];
			var user = {
				id: item.id,
				name: item.text,
				groupFlag: 0,
				deleteClass: true
			};
			$scope.titleReceive.push(user);
			$scope.userList.push(user);
			$scope.defaultUser = $scope.defaultUser + item.id + ',';
		}
	}

}
angular.module('kindKidsApp').controller(
	'messageCtrl', ['$scope', '$rootScope', '$timeout', '$window', '$uibModal',
		'$filter', 'messageService', 'fileService', messageCtrl
	]);