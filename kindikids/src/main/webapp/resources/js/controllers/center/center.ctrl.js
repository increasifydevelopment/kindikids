'use strict';

/**
 * @author gfwang
 * @description centerCtrl Controller
 */
function centerCtrl($scope, $rootScope, $timeout, $window, $location, centerService, fileService, commonService, image_resizer, $uibModal) {
	$rootScope.subScope = $scope;
	$scope.page = {};
	$scope.page.condition = {
		pageIndex : 1,
	};
	$scope.busy = false;
	// show image
	$scope.showAvatarImage = true;

	$scope.getCenterList = function(isDown) {
		$scope.flag=null;
		if (isDown) {
			// 如果当前页面中list的个数大于，等于数据库中所有记录数，则不请求后台
			if ($scope.page.list.length >= $scope.page.condition.totalSize) {
				return;
			}
			$scope.page.condition.pageIndex += 1;
			$scope.busy = true;
		}
		$scope.showLoading("#centerList");
		centerService.getCenterList($scope.page.condition).then(angular.bind(this, function then() {
			var result = centerService.listResult;
			$scope.hideLoading("#centerList");
			if (result.code == 0) {
				$scope.dealListResult(result.data, isDown);
			} else {
				$rootScope.message(result.msg);
			}

		}));
	};
	$scope.scorellUp = function() {
		$scope.page.condition.pageIndex = 1;
		$scope.getList(false);
	};
	/**
	 * 处理查询结果
	 */
	$scope.dealListResult = function(data, isAppend) {
		// 判断是否为 追加
		if (isAppend) {
			if (!$scope.page.list) {
				$scope.page = data;
				// $scope.defaultList = data;
			} else {
				$scope.page.condition = data.condition;
				var length = data.list.length;
				for (var i = 0; i < length; i++) {
					// 向列表追加元素
					$scope.page.list.push(data.list[i]);
				}
			}
			$timeout(function() {
				$scope.busy = false;
			}, 2000);
		} else {
			$scope.page = data;
		}
	};

	/**
	 * 获取园区信息
	 */
	$scope.getCenter = function() {
		// 初始化页面
		$scope.page.condition.pageIndex = 1;
		var centerId = $scope.$stateParams.centerId;
		centerService.getCenterInfo(centerId).then(angular.bind(this, function then() {
			var result = centerService.singleResult;
			if (result.code == 0) {
				$scope.centerInfoVo = result.data;
			} else {
				$rootScope.message(result.msg);
			}
		}));
	};
	$scope.flag='-1';
	$scope.clink=function(centerName){
		$scope.flag=centerName;
	}

	$scope.goEdit = function(centerId) {
		$location.path('center_edit/' + centerId);
	};
	/**
	 * 新增园区
	 */
	$scope.addCenter = function() {
		var scope = $scope.$new();
		// callback
		scope.pushCenter = function(centerInfo) {
			$scope.page.list.push({
				center : centerInfo
			});
		};
		scope.getReloadList = function() {
			$scope.getCenterList();
			$scope.getCenterMenuList();
		};
		$uibModal.open({
			templateUrl : 'views/center/modal/add_center.html?res_v='+$scope.res_v,
			controller : 'centerModalCtrl',
			backdrop : 'static',
			keyboard : false,
			scope : scope
		});
	};
	$scope.updateCenter = function() {
		var flag = false;
		$('#addCenterForm').data('bootstrapValidator').validate();
		$("small").map(function() {
			if ($(this).attr('data-bv-result') == "INVALID") {
				flag = true;
			}
		});
		if (flag) {
			return;
		}
		$scope.showLoading("#addCenterForm");
		centerService.addCenter($scope.centerInfoVo).then(angular.bind(this, function then() {
			var addResult = centerService.addResult;
			$scope.hideLoading("#addCenterForm");
			$scope.getCenterMenuList();
			$rootScope.message(addResult.msg);
		}));

	};

	// left menu
	$scope.getCenterMenuList = function() {
		centerService.getCenterMenu().then(angular.bind(this, function then() {
			var menuResult = centerService.menuResult;
			if (menuResult.code == 0) {
				$scope.menuList = menuResult.data;
			}

		}));
	};
	/**
	 * 新增room
	 */
	$scope.editRoom = function(roomInfo, text) {
		var scope = $rootScope.$new();
		if (roomInfo) {
			scope.roomInfo = {
				id : roomInfo.id,
				centersId : $scope.$stateParams.centerId,
				name : roomInfo.name,
				ageGroup : roomInfo.ageGroup,
				childCapacity : roomInfo.childCapacity
			};
		} else {
			scope.roomInfo = {
				centersId : $scope.$stateParams.centerId
			};
		}
		// callback
		scope.pushRoom = function(roomInfo) {
			$scope.centerInfoVo.roomList.push(roomInfo);
		};
		scope.replaceRoom = function(updateResult) {
			roomInfo.name = updateResult.name;
			roomInfo.ageGroup = updateResult.ageGroup;
			roomInfo.childCapacity = updateResult.childCapacity;
		};
		scope.text = text;
		$uibModal.open({
			templateUrl : 'views/center/modal/edit_room.html?res_v='+$scope.res_v,
			controller : 'centerModalCtrl',
			backdrop : 'static',
			keyboard : false,
			scope : scope
		});
	};
	/**
	 * 新增分组
	 */
	$scope.addGroup = function(roomInfo) {
		var scope = $rootScope.$new();
		scope.groupInfo = {
			roomId : roomInfo.id,
			centerId : $scope.$stateParams.centerId
		};
		// callback
		scope.pushGroup = function(groupInfo) {
			if (!roomInfo.groupList) {
				roomInfo.groupList = [];
			}
			roomInfo.groupList.push(groupInfo);
		};
		$uibModal.open({
			templateUrl : 'views/center/modal/add_group.html?res_v='+$scope.res_v,
			controller : 'centerModalCtrl',
			backdrop : 'static',
			keyboard : false,
			scope : scope
		});
	};
	/**
	 * 查看分组
	 */
	$scope.viewGroup = function(roomInfo, groupInfo) {
		var scope = $rootScope.$new();
		scope.groupInfo = angular.copy(groupInfo);
		// 园区状态
		scope.centerStatus = $scope.centerInfoVo.center.status;
		scope.roomStatus = roomInfo.status;
		// 更新后替换
		scope.replaceGroup = function(updateResult) {
			groupInfo.name = updateResult.name;
		};
		scope.archiveAlert = function(title, msg) {
			archiveAlert(title, msg);
		};
		$uibModal.open({
			templateUrl : 'views/center/modal/view_group.html?res_v='+$scope.res_v,
			controller : 'centerModalCtrl',
			backdrop : 'static',
			keyboard : false,
			scope : scope
		});
	};
	/**
	 * 编辑菜单
	 */
	$scope.editMenu = function(menuIndex) {
		var scope = $rootScope.$new();
		scope.menuIndex = menuIndex;
		scope.centerStatus = $scope.centerInfoVo.center.status;
		$uibModal.open({
			templateUrl : 'views/center/modal/nutrional_menu.html?res_v='+$scope.res_v,
			controller : 'centerModalCtrl',
			backdrop : 'static',
			keyboard : false,
			size : "lg",
			scope : scope
		});
	};

	$scope.deleteImage = function() {
		$scope.centerInfoVo.image = null;
	};
	// imgCrop======================================================================
	$scope.myImage = '';
	$scope.avatarImage = '';
	$scope.showAvatarImage = true;
	$scope.avatarfile = null;
	/**
	 * @author abliu
	 * @description 文件上传
	 */
	$scope.uploadImage = function($file) {
		var file = $file;
		if (file == undefined) {
			return;
		}
		var formatMsg=$scope.isImage($file.name);
		if (formatMsg) {
			$rootScope.message(formatMsg);
			return
		}
		if (file.name) {
			$scope.showAvatarImage = false;
		}
		var reader = new FileReader();
		reader.onload = function(evt) {
			$scope.$apply(function($scope) {
				$scope.myImage = evt.target.result;
				$scope.avatarfile = file;
			});
		};
		reader.readAsDataURL(file);
	};

	$scope.cancelAvatar = function() {
		$scope.myImage = '';
		$scope.avatarImage = '';
		$scope.avatarName = '';
		$scope.showAvatarImage = true;
	};
	$scope.saveAvatar = function(myImage, childInfo) {
		$scope.showLoading("#addCenterForm");
		fileService.uploadResizeImage('', myImage, $scope.avatarfile.name).then(angular.bind(this, function then() {
			var result = fileService.fdata;
			if (result.code == 0) {
				$scope.centerInfoVo.image = result.data;
				$scope.cancelAvatar();
			} else {
				$rootScope.message(result.msg);
			}
			$scope.hideLoading("#addCenterForm");

		}));
		// 调用fileservice中的uploadResizeImageo(￣▽￣)d
	};

	// ========================archive============
	/**
	 * room ，flag 0标识归档，1标识未归档
	 */
	$scope.archiveCenter = function(flag) {
		var centerId = $scope.$stateParams.centerId;
		centerService.archiveCenter(centerId, flag).then(angular.bind(this, function then() {
			var result = centerService.acResult;
			if (result.code == 0) {
				$scope.centerInfoVo.center.status = flag;
				$rootScope.message(result.msg);
			} else {
				var title = flag == 0 ? "Archive Centre" : "Unarchive Centre";
				archiveAlert(title, result.msg);
			}
		}));
	};

	/**
	 * room ，flag 0标识归档，1标识未归档
	 */
	$scope.archiveRoom = function(roomInfo, flag) {
		centerService.archiveRoom(roomInfo.id, flag).then(angular.bind(this, function then() {
			var result = centerService.arResult;
			if (result.code == 0) {
				roomInfo.status = flag;
				$rootScope.message(result.msg);
			} else {
				var title = flag == 0 ? "Archive Room" : "Unarchive Room";
				archiveAlert(title, result.msg);
			}
		}));
	};

	function archiveAlert(title, msg) {
		$.messager.model = {
			ok : {
				text : "Close",
				classed : 'btn-general closeAllModal'
			}
		};
		$.messager.alert(title, msg);
	}
	
	$scope.choose = function(id,check,type){
		if(type==1){
			check.checkFlag=!check.checkFlag;
		}
		centerService.chooseCheck(id,check.checkFlag).then(angular.bind(this, function then(){
		}));
	}

	$scope.chooseMenu = function(room,index) {
		var scope = $rootScope.$new();
		scope.room = room;
		scope.index = index;
		$uibModal.open({
			templateUrl : 'views/center/modal/edit.menu.html?res_v='+$scope.res_v,
			controller : 'centerModalCtrl',
			backdrop : 'static',
			size : "lg",
			keyboard : false,
			scope : scope
		});
	};


}
angular.module('kindKidsApp').controller(
		'centerCtrl',
		[ '$scope', '$rootScope', '$timeout', '$window', '$location', 'centerService', 'fileService', 'commonService', 'image_resizer', '$uibModal',
				centerCtrl ]);