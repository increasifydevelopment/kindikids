'use strict';

/**
 * @author gfwang
 * @description centerCtrl Controller
 */
function centerModalCtrl($scope, $rootScope, $timeout, $window, centerService, fileService, commonService, menuService, $uibModalInstance, image_resizer, $filter) {
	$scope.cancel = function() {
		$scope.confirm('Close','Exit this page?','OK',function(){
			$uibModalInstance.dismiss('cancel');
		},'NO');
	};
	/**
	 * 新增
	 */
	$scope.saveCenter = function() {
		$('#addCenterForm').data('bootstrapValidator').validate();
		if (!$('#addCenterForm').data('bootstrapValidator').isValid()) {
			return;
		}
		var centerInfoVo = {
			center : {
				name : $scope.centerName
			}
		};
		$scope.showLoading("#mainDiv");
		centerService.addCenter(centerInfoVo).then(angular.bind(this, function then() {
			var addResult = centerService.addResult;
			if (addResult.code == 0) {
				$scope.getReloadList();
				$uibModalInstance.dismiss('cancel');
			}else{
				$rootScope.message(addResult.msg);
			}
			$scope.hideLoading("#mainDiv");
		}));
	};
	$scope.editRoom = function() {
		var flag = false;
		$('#editRoomForm').data('bootstrapValidator').validate();
		$("small").map(function() {
			if ($(this).attr('data-bv-result') == "INVALID") {
				flag = true;
			}
		});
		if (flag) {
			return;
		}
		var roomId = $scope.roomInfo.id;
		$scope.showLoading("#mainDiv");
		centerService.editRoom($scope.roomInfo).then(angular.bind(this, function then() {
			var addResult = centerService.addRoomResult;
			if (addResult.code == 0) {
				if (roomId) {
					$scope.replaceRoom(addResult.data);
				} else {
					$scope.pushRoom(addResult.data);
				}
				$uibModalInstance.dismiss('cancel');
			}else{
				$rootScope.message(addResult.msg);
			}
			$scope.hideLoading("#mainDiv");
		}));
	};
	/**
	 * 编辑分组
	 */
	$scope.editGroup = function(groupId) {
		var flag = false;
		$('#groupForm').data('bootstrapValidator').validate();
		$("small").map(function() {
			if ($(this).attr('data-bv-result') == "INVALID") {
				flag = true;
			}
		});
		if (flag) {
			return;
		}
		$scope.showLoading("#mainDiv");
		$scope.groupInfo.centerId = $scope.$stateParams.centerId;
		centerService.editGroup($scope.groupInfo).then(angular.bind(this, function then() {
			var addResult = centerService.addGroupResult;
			if (addResult.code == 0) {
				if ($scope.groupInfo.id) {
					// 如果编辑
					$scope.replaceGroup(addResult.data);
				} else {
					$scope.pushGroup(addResult.data);
				}
				$uibModalInstance.dismiss('cancel');
			}else{
				$rootScope.message(addResult.msg);
			}
			$scope.hideLoading("#mainDiv");
		}));
	};
	/**
	 * 获取分组信息
	 */
	$scope.getGroupInfo = function() {
		var groupId = $scope.groupInfo.id;
		centerService.getGroupInfo(groupId).then(angular.bind(this, function then() {
			var result = centerService.gResult;
			if (result.code == 0) {
				$scope.groupInfoVo = result.data;
				$scope.groupInfo.name = $scope.groupInfoVo.name;
			} else {
				$rootScope.message(result.msg);
			}
		}));
	};
	/**
	 * room ，flag 0标识归档，1标识未归档
	 */
	$scope.archiveGroup = function(groupId, flag) {
		centerService.archiveGroup(groupId, flag).then(angular.bind(this, function then() {
			var result = centerService.agResult;
			if (result.code == 0) {
				$scope.groupInfoVo.status = flag;
				$rootScope.message(result.msg);
			} else {
				var title = flag == 0 ? "Archive Group" : "Unarchive Group";
				$scope.archiveAlert(title, result.msg);
			}
		}));
	};

	$scope.getNutMenu = function() {
		centerService.getNutMenu($scope.menuIndex, $scope.$stateParams.centerId).then(angular.bind(this, function then() {
			var result = centerService.nutMenuResult;
			if (result.code == 0) {
				$scope.WeekNutrionalMenuList = result.data;
			}
		}));
	};

	$scope.updateMenu = function() {
		$('#menuFrom').data('bootstrapValidator').validate();
		if (!$('#menuFrom').data('bootstrapValidator').isValid()) {
			return;
		}
		$scope.showLoading("#menuMain");
		centerService.updateMenu($scope.WeekNutrionalMenuList).then(angular.bind(this, function then() {
			var result = centerService.updateMenuResult;
			if (result.code == 0) {
				$uibModalInstance.dismiss('cancel');
			}else{
				$rootScope.message(result.msg);
			}
			$scope.hideLoading("#menuMain");
		}));
	};

	// 上传图片
	$scope.uploadImage = function($file, menu) {
		if ($file == null) {
			return;
		}
		var formatMsg=$scope.isImage($file.name);
		if (formatMsg) {
			$rootScope.message(formatMsg);
			return
		}
		if ($file.size >= 10485760) {
			$rootScope.message("Sorry, the filesize is too large, please reduce the filesize and try again.");
			return;
		}
		$scope.showLoading("#menuMain");
		image_resizer.resize({
			image : $file,
			max_width : 1024
		}).then(function(data) {
			var fileStrBase64 = data.base64_resized_image;
			var fileType = data.file_type;
			fileService.uploadResizeImage(fileType, fileStrBase64, $file.name).then(angular.bind(this, function then() {
				var result = fileService.fdata;
				if (result.code == 0) {
					menu.image = result.data;
				} else {
					$rootScope.message(result.msg);
				}
				$scope.hideLoading("#menuMain");
			}));
		});

	};
	$scope.showLoading = function(tag) {
		$(tag).mymask('');
	};
	$scope.hideLoading = function(tag) {
		if ($(tag).myisMasked()) {
			$(tag).myunmask();
		}
	};
	$scope.deleteImage = function(image) {
		image.fileIdName = '';
		image.fileName = '';
		image.relativePathName = '';
		image.visitedUrl = '';
	};

	$scope.selectMenuId = '';

	$scope.initMenu = function(){
		$scope.getMenus();
		
		// $scope.selectMenuId = '43a0b2b3-5bfa-4024-881b-4e2d1d6b30fd';
	}

	$scope.getCurrentMenu = function(roomId,index){
		menuService.getCurrentMenu(roomId,index).then(angular.bind(this, function then() {
			var result = menuService.result;
			if (result.code == 0) {
				$scope.dbMenuId = result.data;
				$scope.selectMenuId = angular.copy($scope.dbMenuId);
				$scope.selectMenu = $filter("filter")($scope.MenuList, {
										'menuId' : $scope.selectMenuId
									})[0];
				$scope.selectMenuId = $scope.dbMenuId==null?'':angular.copy($scope.dbMenuId);
			}else{
				$rootScope.message(result.msg);
			}
			
		}));
	}

	$scope.$watch('selectMenuId', function(newValue, oldValue) {
		if(!$scope.MenuList){
			return;
		}
		$scope.selectMenu = $filter("filter")($scope.MenuList, {
										'menuId' : newValue
									})[0];
	}, true);

	$scope.getMenus = function(){
		menuService.getAllMenus().then(angular.bind(this, function then() {
			var result = menuService.result;
			if (result.code == 0) {
				$scope.MenuList = result.data;
				$scope.getCurrentMenu($scope.room.id,$scope.index);
			}else{
				$rootScope.message(result.msg);
			}
			
		}));
	}

	$scope.saveMenu = function(){
		menuService.saveMenu($scope.room.id,$scope.selectMenuId,$scope.index).then(angular.bind(this, function then() {
			var result = menuService.result;
			if (result.code == 0) {
				$uibModalInstance.dismiss('cancel');
			}else{
				$rootScope.message(result.msg);
			}
			
		}));
	}

	$scope.deleteMenu = function(){
		$scope.confirm('Delete', 'Are you sure to delete this menu?', 'Yes', function() {
			$scope.selectMenuId = '';
			$scope.saveMenu();
		}, 'No', null);
	}


}
angular.module('kindKidsApp').controller(
		'centerModalCtrl',
		[ '$scope', '$rootScope', '$timeout', '$window', 'centerService', 'fileService', 'commonService', 'menuService', '$uibModalInstance', 'image_resizer', '$filter',
				centerModalCtrl ]);