'use strict';

/**
 * @author gfwang
 * @description meetingprofileCtrlAgendaCtrl
 */
function profileCtrl($scope, $rootScope, $timeout, $location, $uibModal, profileService, $stateParams) {

	var type = $scope.$stateParams.profileType;
	$scope.baseName = type * 1 == 0 ? 'Orientation' : 'Induction';

	$scope.profileTemplate = {
		profileTempType: type
	};

	/** **************************************************列表start********************************************** */
	// 页面list参数对象
	$scope.condition = {
		listType: 1,
		maxSize: 7,
		pageIndex: 1,
		//pageSize : 12,
		keyWord: '',
		type: type
	};
	/**
	 * 初始化task列表
	 */
	$scope.initList = function () {
		$scope.getProfileList();
	};
	$scope.enterSearch = function (ev) {
		if (ev.keyCode == 13) {
			$scope.condition.pageIndex = 1;
			$scope.condition.search = true;
			$scope.getProfileList();
		}
	};
	$scope.search = function () {
		$scope.condition.pageIndex = 1;
		$scope.getProfileList();
	};
	$scope.getProfileList = function () {
		$scope.showLoading("#myProfile");
		// 页面list数据
		profileService.getList($scope.condition).then(angular.bind(this, function then() {
			var result = profileService.listResult;
			$scope.profileList = result.data.list;
			$scope.condition.totalItems = result.data.condition.totalSize;
			$scope.condition.pageSize = result.data.condition.pageSize;
			$scope.hideLoading("#myProfile");
			if ($scope.condition.search && $scope.profileList.length == 0) {
				$scope.confirm('Message', 'No results match your search.', 'OK', function () {
					$("#privacy-modal").modal('hide');
				});
			}
			$scope.condition.search = false;
		}));
	};
	$scope.changeTemplateList = function (pageIndex) {
		$scope.condition.pageIndex = pageIndex;
		$scope.getProfileList();
	};
	/** **************************************************列表end********************************************** */

	/** *****************************************nqs***************************** */
	/** *****************************************nqs***************************** */

	/**
	 * 保存 gfwang
	 */
	$scope.save = function () {
		$scope.showLoading("#profileSave");
		profileService.saveProfile($scope.profileTemplate, $scope.formObj).then(angular.bind(this, function then() {
			var result = profileService.saveResult;
			if (result.code == 0) {
				$scope.initModel();
				$scope.getProfileList();
			} else {
				$scope.message(result.msg);
			}
			$scope.formObj = "";
			$scope.hideLoading("#profileSave");
		}));
	};

	$scope.initModel = function (id) {
		$scope.formObj = "";
		if (!id) {
			$scope.profileTemplate = {
				profileTempType: type
			};
			$('.task-list div').removeClass('active');
			return;
		}
		$scope.showLoading("#profileSave");
		profileService.getProfileInfo(id).then(angular.bind(this, function then() {
			var result = profileService.profileInfoResult;
			if (result.code == 0) {
				$scope.profileTemplate = result.data;
			} else {
				$scope.message(result.msg);
			}
			$scope.hideLoading("#profileSave");
		}));
	};

	/**
	 * 禁用或者启用 gfwang
	 */
	$scope.enableOrDisable = function (state) {
		// 页面list数据
		profileService.changeState($scope.profileTemplate.id, state).then(angular.bind(this, function then() {
			var result = profileService.stateResult;
			if (result.code == 0) {
				$scope.profileTemplate.state = state;
			} else {
				$scope.message(result.msg);
			}
		}));
	};

	$scope.formObj = null;

	$scope.settingForm = function () {
		$uibModal.open({
			templateUrl: 'views/form-builder/builder.html?res_v=' + $scope.res_v,
			controller: 'builderFormCtrl',
			animation: true,
			size: "lg",
			backdrop: 'static',
			keyboard: false,
			windowClass: "modal-form-create modal-grey modal-align-top",
			resolve: {
				code: function () {
					return !$scope.profileTemplate.instanceId ? 0 : $scope.profileTemplate.instanceId;
				},
				hiddenSave: function () {
					if (!$scope.profileTemplate) {
						return true;
					}
					return $scope.profileTemplate.state == 0 ? true : false;
				},
				formObj: function () {
					return $scope.formObj;
				}
			}
		}).result.then(function (formObj) {
			$scope.formObj = formObj;
		});
	};

}
angular.module('kindKidsApp').controller('profileCtrl',
	['$scope', '$rootScope', '$timeout', '$location', '$uibModal', 'profileService', '$stateParams', profileCtrl]);