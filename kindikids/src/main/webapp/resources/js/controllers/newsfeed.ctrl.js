'use strict';

/**
 * @author abliu
 * @description newsFeed Controller
 */
function newsfeedCtrl($scope, $rootScope, $timeout, $window, newsFeedService, fileService, image_resizer, commonService, $filter) {

	$scope.masonryobj = {};
	//是否家长

	$scope.busy = false;

	// variable=================================================================
	$scope.page = {};
	$scope.page.condition = {
		pageIndex: 1,
		accountId: "",
		centersId: "",
		roomId: "",
		keyword: '',
		location: null,
		score: null,
		nqsVersion: "",
		startDate: null,
		endDate: null,
		type: null,
		status: -1,
		visibility: 0,
		selectAccounts: null,
		time: "All Time",
		timeSource: null,
		date: ""
	};

	// select===================================================================
	$scope.functions = {};
	$scope.getNewsFeedFun = function () {
		commonService.getFun("/news_list").then(angular.bind(this, function then() {
			$scope.functions = commonService.functions;
			/*
			 * //判断是否有权限 if(!$filter('haveFunction')($scope.functions,
			 * 'news_create')){ if($("#newsFeedCreat")!=undefined){ try{
			 * $scope.masonryobj.remove($("#newsFeedCreat")); }catch(e){} } }
			 */
		}));
	};
	/*
	 * 指定人(家长)的center,rroom信息*/
	$scope.getOrgs = function () {
		commonService.getMenuOrgs(1).then(angular.bind(this, function then() {
			$scope.orgs = commonService.orgList;

		}));
	};

	/**
	 * @author abliu
	 * @description
	 * @returns 获取newsfeedList
	 */
	$scope.getNewsFeedList = function (isDown) {
		if (isDown) {
			// 如果当前页面中list的个数大于，等于数据库中所有记录数，则不请求后台
			// if ($scope.page.list != undefined && $scope.page.list.length >= $scope.page.condition.totalSize) {
			// 	return;
			// }
			$scope.page.condition.pageIndex += 1;
			$scope.busy = true;
		}
		if ($scope.page.condition.date != undefined && $scope.page.condition.startDate == null) {
			updateTime($scope.page.condition.time, $scope.page.condition.date);
		}
		newsFeedService.getNewsFeedList($scope.page.condition).then(angular.bind(this, function then() {
			//$("#searchDiv").removeClass("open");
			var result = newsFeedService.listResult;
			$scope.hideLoading("#newsFeedList");
			if (result.code == 0) {
				dealListResult(result.data, isDown);
				if (result.data.condition.search && result.data.list.length == 0) {
					$scope.confirm('Message', 'No results match your search.', 'OK', function () {
						$("#privacy-modal").modal('hide');
					});
				}
				$scope.page.condition.search = false;
			} else {
				$.messager.popup(result.msg);
			}
		}));
	};

	$scope.search = function () {
		$scope.page.condition.feedbackSearch = $scope.feedback;
		$scope.page.condition.pageIndex = 1;
		$scope.page.condition.search = true;
		$scope.getNewsFeedList(false);
		$scope.showLoading("#newsFeedList");
		$scope.getNewFeedback();
	};
	$scope.newsKeyup = function (ev) {
		if (ev.keyCode == 13) {
			$scope.search();
		}
	};



	$scope.reset = function () {
		$scope.page.condition.pageIndex = 1;
		$scope.page.condition.keyword = "";
		$('.Location li').removeClass('active');
		$("#locationOrg").parent('li').addClass('active');
		$scope.page.condition.roomId = '';
		$scope.page.condition.centersId = '';
		$scope.page.condition.score = null;
		$scope.page.condition.nqsVersion = null;
		$scope.page.condition.time = 'All Time';
		$scope.page.condition.startDate = null;
		$scope.page.condition.endDate = null;
		$scope.page.condition.date = null;
		$scope.page.condition.type = null;
		$scope.page.condition.status = -1;
	};

	/**
	 * 处理查询结果
	 */
	var dealListResult = function (data, isAppend) {
		// 判断是否为 追加
		if (isAppend) {
			if (!$scope.page.list) {
				$scope.page = data;
			} else {
				$scope.page.condition = data.condition;
				var length = data.list.length;
				for (var i = 0; i < length; i++) {
					// 向列表追加元素
					$scope.page.list.push(data.list[i]);
				}
			}
			$scope.busy = false;
		} else {
			$scope.page.list = data.list;
			$scope.page.condition = data.condition;
			if ($scope.page.list.length == 0) {
				$scope.visibility = {
					"visibility": "visible"
				};
				$scope.initshow = false;
			}
		}
		setConditionDate();
	}

	function setConditionDate() {
		if ($scope.page.condition.startDate == null || $scope.page.condition.endDate == null) {
			return;
		}
		var startDate = new moment($scope.page.condition.startDate, "YYYY-MM-DDTHH:mm:ss.SSS");
		var d = startDate._d;
		var yyyy = d.getFullYear();
		var mm = (d.getMonth() + 1) < 10 ? "0" + (d.getMonth() + 1) : d.getMonth() + 1;
		var dd = d.getDate() < 10 ? "0" + d.getDate() : d.getDate();
		$scope.page.condition.date = dd + "/" + mm + "/" + yyyy;

		var endDate = new moment($scope.page.condition.endDate, "YYYY-MM-DDTHH:mm:ss.SSS");
		d = endDate._d;
		yyyy = d.getFullYear();
		mm = (d.getMonth() + 1) < 10 ? "0" + (d.getMonth() + 1) : d.getMonth() + 1;
		dd = d.getDate() < 10 ? "0" + d.getDate() : d.getDate();
		$scope.page.condition.date += " - " + dd + "/" + mm + "/" + yyyy;
	}

	$scope.scorellUp = function () {
		$scope.page.condition.pageIndex = 1;
		$scope.getNewsFeedList(false);
	}

	$scope.visibility = {
		"visibility": "hidden"
	};
	$scope.initshow = true;
	$scope.initListMasonry = function () {
		$timeout(function () {
			try {
				// ie
				if (!!window.ActiveXObject || "ActiveXObject" in window) {
					$timeout(function () {
						$scope.masonryobj.reloadItems();
						$scope.masonryobj.layout();
					}, 500);
				}
				$scope.masonryobj.reloadItems();
				$scope.masonryobj.layout();
				$timeout(function () {
					$scope.visibility = {
						"visibility": "visible"
					};
					$scope.initshow = false;
				}, 50);
			} catch (e) {

			}
		}, 50);
	}


	$scope.setVisibility = function (value) {
		$scope.page.condition.accountId = "";
		$scope.page.condition.visibility = value;
		$scope.search();
	};

	$scope.setRating = function (value) {
		$scope.page.condition.score = value;
		$scope.search();
	};

	$scope.setNQS = function (value) {
		$scope.page.condition.nqsVersion = value;
		$scope.search();
	};

	// 时间设置TODO
	$scope.setTime = function (key, value) {
		updateTime(key, value);
		$scope.search();
	};

	function updateTime(key, value) {
		$scope.page.condition.time = key;
		var times = value.split("-");
		if (times.length >= 2) {
			var m = new moment(times[0].trim(), "DD/MM/YYYY");
			var d = m._d;
			var yyyy = d.getFullYear();
			var mm = (d.getMonth() + 1) < 10 ? "0" + (d.getMonth() + 1) : d.getMonth() + 1;
			var dd = d.getDate() < 10 ? "0" + d.getDate() : d.getDate();
			var start = yyyy + "-" + mm + "-" + dd + "T00:00:00.000";
			var m = new moment(times[1].trim(), "DD/MM/YYYY");
			var d = m._d;
			var yyyy = d.getFullYear();
			var mm = (d.getMonth() + 1) < 10 ? "0" + (d.getMonth() + 1) : d.getMonth() + 1;
			var dd = d.getDate() < 10 ? "0" + d.getDate() : d.getDate();
			var end = yyyy + "-" + mm + "-" + dd + "T00:00:00.000";
			$scope.page.condition.startDate = start;
			$scope.page.condition.endDate = end;
		} else {
			$scope.page.condition.startDate = null;
			$scope.page.condition.endDate = null;
			$scope.page.condition.date = null;
		}
	}

	$scope.clearDate = function () {
		$scope.page.condition.date = null;
	};

	$scope.setType = function (value) {
		$scope.page.condition.type = value;
		$scope.search();
	};

	$scope.setStatus = function (value) {
		$scope.page.condition.status = value;
		$scope.search();
	};

	$scope.setStatusInit = function (value) {
		$scope.page.condition.status = value;
	};


	$scope.rangeCallback = function (flag) {
		//为true则清空range数据
		if (flag) {
			$scope.page.condition.startDate = null;
			$scope.page.condition.endDate = null;
			$scope.page.condition.date = undefined;
		}
		$scope.page.condition.pageIndex = 1;
		$scope.search();
	};

	// other====================================================================





	// Edit==========================================================
	$scope.editNewsInfo = [];
	$scope.editGroupSource = {};
	$scope.initEditSelect = {};
	$scope.initContentValue = "";
	$scope.noEditFileVoList = [];
	// select===================================================================
	/**
	 * @author abliu
	 * @description
	 * @returns
	 */
	$scope.getEditNewsInfo = function (editNewsInfoId) {
		closePopover();
		$scope.initContentValue = "";
		newsFeedService.getNewsFeedInfo(editNewsInfoId).then(angular.bind(this, function then() {
			var result = newsFeedService.getNewsfeedInfoResult;
			if (result.code == 0) {
				$scope.editNewsInfo = result.data;
				if (editNewsInfoId == "") {
					$scope.editNewsInfo.newsInfo.newsType = 0;
				}
				// 更新group组
				updateGroupSource($scope.editNewsInfo);
				// 初始化富文本框内容
				$scope.initContentValue = $scope.editNewsInfo.content;
				$scope.initEditSelect = $scope.editNewsInfo.selectAccounts;
				// 更新没图片的集合
				updateNoEditFileArray($scope.editNewsInfo.fileVoList.length);
			} else {
				$.messager.popup(result.msg);
			}
		}));
	};

	function updateGroupSource(editNewsInfo) {
		var groupSourceList = [];
		var j = 0;
		var n = editNewsInfo.groupName.length;

		for (var i = 0, m = editNewsInfo.groupNameSource.length; i < m; i++) {
			var groupSource = {};
			groupSource.id = editNewsInfo.groupNameSource[i].id;
			groupSource.text = editNewsInfo.groupNameSource[i].text;
			groupSource.selected = false;

			for (j = 0; j < n; j++) {
				if (editNewsInfo.groupName[j] == editNewsInfo.groupNameSource[i].id) {
					var groupSourceTrue = {};
					groupSourceTrue.id = editNewsInfo.groupNameSource[i].id;
					groupSourceTrue.text = editNewsInfo.groupNameSource[i].text;
					groupSourceTrue.selected = true;
					groupSourceList.push(groupSourceTrue);
					break;
				}
			}
			if (j >= n) {
				groupSourceList.push(groupSource);
			}
		}
		var k = 0, o = groupSourceList.length;
		for (j = 0, n = editNewsInfo.groupNamePo.length; j < n; j++) {
			for (k = 0; k < o; k++) {
				if (groupSourceList[k].id == editNewsInfo.groupNamePo[j].id) {
					break;
				}
			}
			if (k >= o) {
				var groupSourceTrue = {};
				groupSourceTrue.id = editNewsInfo.groupNamePo[j].id;
				groupSourceTrue.text = editNewsInfo.groupNamePo[j].text;
				groupSourceTrue.selected = true;
				groupSourceTrue.disabled = "disabled";
				groupSourceList.push(groupSourceTrue);
			}
		}
		$scope.editGroupSource = groupSourceList;
	}
	;

	/**
	 * @author abliu
	 * @description 保存更新newsfeed
	 */
	$scope.saveNewsFeedEdit = function () {
		var errorMsg = validateNews($scope.editNewsInfo);
		if (errorMsg != "") {
			$.messager.popup(errorMsg);
			return;
		}
		$scope.showLoading("#edit-post-modal");
		newsFeedService.saveNewsFeedInfo($scope.editNewsInfo).then(angular.bind(this, function then() {
			var result = newsFeedService.saveNewsFeedData;
			$scope.hideLoading("#edit-post-modal");
			if (result.code == 0) {
				if ($scope.editNewsInfo.newsInfo.id == null) {
					initPage();
					// 刷新当前页面newsfeed集合 todo
					refreshNewsFeedSelectOrAdd(result.data, true);
				} else {
					refreshNewsFeedSelectOrAdd($scope.editNewsInfo.newsInfo.id, false);
				}
				$('#edit-post-modal').modal('hide');
			} else {
				$.messager.popup(result.msg);
			}
		}));
	};

	/**
	 * @author abliu
	 * @description 文件上传
	 */
	$scope.upload = function ($files, newsInfoObj) {
		if ($files == null || $files.length == 0) {
			return;
		}

		var currentUploadLength = $files.length;
		var currentUploadedLength = 0;

		if (newsInfoObj.fileVoList.length + $files.length > 4) {
			// 提示文件重复 todo
			$.messager.popup("You can only upload a maximum of 4 images.");
			return;
		}
		// 判断附件是中重复
		var compareResule = $scope.isRepeatAnnex($files, newsInfoObj);
		if (compareResule != '') {
			// 提示文件重复 todo
			$.messager.popup("Sorry, file " + compareResule + " already exists, please try a different filename.");
			return;
		}
		for (var i = 0; i < $files.length; i++) {
			var file = $files[i];
			if (file != null) {
				if (file.size < 10485760) {
					// 等待上传
					//if (newsInfoObj.newsInfo.id !== null) {
					$scope.showLoading("#edit-post-modal");
					/*} else {
						$scope.showLoading("#newsFeedCreat");
					}*/
					// 转换大小
					image_resizer.resize({
						image: file,
						max_width: 1024
					}).then(function (data) {
						var fileStrBase64 = data.base64_resized_image;
						var fileType = data.file_type;
						var fileName = data.file_name;
						// fileStrBase64="data:"+fileType+";base64,"+fileStrBase64;
						// $scope.imagesrc="data:"+fileType+";base64,"+fileStrBase64;
						fileService.uploadResizeImage(fileType, fileStrBase64, fileName).then(angular.bind(this, function then() {
							var result = fileService.fdata;
							if (result.code == 0) {
								var length = newsInfoObj.fileVoList.length;

								var fileVo = {};
								fileVo.relativePathName = result.data.relativePathName;
								fileVo.fileName = result.data.fileName;
								fileVo.visitedUrl = result.data.visitedUrl;
								newsInfoObj.fileVoList.push(fileVo);


								// 更新没图片的集合
								updateNoEditFileArray(newsInfoObj.fileVoList.length);

							} else {
								$.messager.popup(result.msg);
							}

							currentUploadedLength++;
							if (currentUploadedLength == currentUploadLength) {
								$scope.hideLoading("#edit-post-modal");

							}
						}));
					}, function (resize_error) {
						$scope.hideLoading("#edit-post-modal");

						$.messager.popup("Sorry, this file format is not supported. Please upload a .png, .jpg or .gif file and try again.");
					});
				} else {
					$.messager.popup("Sorry, the filesize is too large, please reduce the filesize and try again.");
				}
			}
		}
	};

	/**
	 * @author abliu
	 * @description 删除附件
	 */
	$scope.removeFile = function (fileName, newsInfoObj) {
		for (var i = 0, m = newsInfoObj.fileVoList.length; i < m; i++) {
			if (newsInfoObj.fileVoList[i].relativePathName == fileName) {
				newsInfoObj.fileVoList.splice(i, 1);

				// 更新没图片的集合
				updateNoEditFileArray(newsInfoObj.fileVoList.length);

				return;
			}
		}
	};

	/**
	 * @author abliu
	 * @description 判断附件是否重复
	 */
	$scope.isRepeatAnnex = function ($files, newsInfoObj) {
		var annxs = "";
		if (newsInfoObj.fileVoList == undefined || newsInfoObj.fileVoList.length == 0) {
			return "";
		}
		for (var j = 0, m = newsInfoObj.fileVoList.length; j < m; j++) {
			for (var i = 0; i < $files.length; i++) {
				if (newsInfoObj.fileVoList[j].fileName == $files[i].name) {
					return $files[i].name;
				}
			}
		}
		return annxs;
	};

	/**
	 * @author abliu
	 * @description 获取选择的NQS
	 */
	$scope.saveNqsResultEdit = function (newNqs) {
		$scope.editNewsInfo.nqsVoList = newNqs;
	};

	$scope.closePrivacyModal = function () {
		$scope.confirm('Close', 'Exit this page?', 'OK', function () {
			$("#privacy-modal").modal('hide');
		}, 'NO');
	}

	// private==================================================================

	$scope.noFileVoList = ["1", "2", "3", "4"];

	function updateNoEditFileArray(count) {
		var arr = new Array(4 - count);
		for (var i = 0; i < 4 - count; i++) {
			arr[i] = i;
		}
		$scope.noEditFileVoList = arr;
	};

	// Edit=====================================================================

	// Delete===================================================================

	$scope.deleteNewsId = "";
	/**
	 * @author abliu
	 * @description deleteNews
	 */
	$scope.openDeleteNews = function (id) {
		$scope.deleteNewsId = id;
	};

	/**
	 * @author abliu
	 * @description deleteNews
	 */
	$scope.deleteNews = function (id) {
		newsFeedService.deleteNews(id).then(angular.bind(this, function then() {
			var result = newsFeedService.deleteNewsResult;
			if (result.code == 0) {
				$('#delete-modal').modal('hide');
				refreshNewsFeedDel(id);
			} else {
				$.messager.popup(result.msg);
			}
		}));
	};

	// Delete===================================================================

	// Approval=================================================================

	/**
	 * @author abliu
	 * @description approval
	 */
	$scope.approvalNewsFeed = function (id) {
		newsFeedService.approvalNewsFeed(id).then(angular.bind(this, function then() {
			var result = newsFeedService.approvalResult;
			if (result.code == 0) {
				// 更新列表
				refreshNewsFeedSelectOrAdd(id, false);
			} else {
				$.messager.popup(result.msg);
			}
		}));
	};

	/**
	 * @author abliu
	 * @description approval
	 */
	$scope.unApproveNewsFeed = function (id) {
		newsFeedService.unApproveNewsFeed(id).then(angular.bind(this, function then() {
			var result = newsFeedService.unApprovalResult;
			if (result.code == 0) {
				// 更新列表
				refreshNewsFeedSelectOrAdd(id, false);
			} else {
				$.messager.popup(result.msg);
			}
		}));
	};

	$scope.scoreNewsId = "";
	$scope.openScoreNews = function (id) {
		closePopover();
		$scope.scoreNewsId = id;
	};

	function closePopover() {
		if ($scope.page.list == null) {
			return;
		}
		var m = $scope.page.list.length;
		for (var i = 0; i < m; i++) {
			$scope.page.list[i].newsInfo.isopen = false;
		}
	}

	/**
	 * @author abliu
	 * @description scoreNewsFeed
	 */
	$scope.scoreNewsFeed = function (score) {
		closePopover();
		newsFeedService.scoreNewsFeed($scope.scoreNewsId, score).then(angular.bind(this, function then() {
			var result = newsFeedService.scorelResult;
			if (result.code == 0) {
				// $.messager.popup(result.msg);
				// 更新列表
				refreshNewsFeedSelectOrAdd($scope.scoreNewsId, false);
			} else {
				$.messager.popup(result.msg);
			}
		}));
	};

	// Approval=================================================================

	// Comment==============================================================

	/**
	 * @author abliu
	 * @description 发送
	 */
	$scope.enterSend = function (ev, newsId) {
		if (ev.keyCode !== 13)
			return;
		$scope.sendComment(newsId);
	};
	var sendCommentFlag = false;

	/**
	 * @author abliu
	 * @description sendComment
	 */
	$scope.sendComment = function (newsId) {
		var commentInfo = {};
		commentInfo.id = "";
		commentInfo.newsId = newsId;
		commentInfo.content = $("#" + newsId).val();
		if (commentInfo.content.trim() == "") {
			/* $.messager.popup("please input comment"); */
			return;
		}
		if (sendCommentFlag) {
			return;
		}
		sendCommentFlag = true;
		newsFeedService.sendComment(commentInfo).then(angular.bind(this, function then() {
			var result = newsFeedService.sendCommentResult;
			sendCommentFlag = false;
			if (result.code == 0) {
				$("#" + newsId).val("");
				// 更新列表
				refreshNewsFeedSelectOrAdd(newsId, false);
			} else {
				$.messager.popup(result.msg);
			}
		}));
	};

	$scope.deleteCommentId = "";
	$scope.deleteCommentNewsId = ""
	/**
	 * @author abliu
	 * @description deleteComment
	 */
	$scope.deleteComment = function (commentId, newsId) {
		$scope.deleteCommentId = commentId;
		$scope.deleteCommentNewsId = newsId;
		deleteCommentSure();
	};

	var deleteCommentSure = function () {
		newsFeedService.deleteComment($scope.deleteCommentId).then(angular.bind(this, function then() {
			var result = newsFeedService.deleteCommentResult;
			if (result.code == 0) {
				// 更新列表
				refreshNewsFeedSelectOrAdd($scope.deleteCommentNewsId, false);
			} else {
				$.messager.popup(result.msg);
			}
		}));
	};

	/**
	 * @author abliu
	 * @description 发送
	 */
	$scope.enterSave = function (ev, commentId, newsId) {
		if (ev.keyCode !== 13)
			return;
		$scope.updateComment(commentId, newsId);
	};

	/**
	 * @author abliu
	 * @description updateComment
	 */
	$scope.updateComment = function (commentId, newsId) {
		var commentInfo = {};
		commentInfo.id = commentId;
		commentInfo.newsId = newsId;
		commentInfo.content = $("#" + commentId).val();
		if (commentInfo.content.trim() == "") {
			$.messager.popup("Comment cannot be empty");
			return;
		}

		newsFeedService.sendComment(commentInfo).then(angular.bind(this, function then() {
			var result = newsFeedService.sendCommentResult;
			if (result.code == 0) {
				// $.messager.popup(result.msg);
				refreshNewsFeedSelectOrAdd(newsId, false);
			} else {
				$.messager.popup(result.msg);
			}
		}));
	};

	$scope.closeId = "";
	$scope.closeModal = function () {
		$("#" + $scope.closeId).modal('hide');
	};
	$scope.openCloseModal = function (id) {
		$scope.closeId = id;
	};

	// Comment==============================================================

	// private==================================================================

	// 验证newsfeed输入合法性
	function validateNews(newsObj) {
		return "";
		console.log(newsObj);
		/*
		 * var errorMsg=""; if($filter('haveType')(newsObj.newsInfo.newsType,
		 * '3,12')){ return errorMsg; } if(newsObj.content==undefined ||
		 * newsObj.content.trim()==""){ errorMsg="The content is required and
		 * can't be empty"; return errorMsg; } if(newsObj.groupName==undefined ||
		 * newsObj.groupName.length<=0){ errorMsg="Please select visibility";
		 * return errorMsg; } if(newsObj.nqsVoList==undefined ||
		 * newsObj.nqsVoList.length<=0){ errorMsg="Please select NQS"; return
		 * errorMsg; } return errorMsg; };
		 */
	}

	$scope.noFileVoList = ["1", "2", "3", "4"];

	function updateNoFileArray(count) {
		var arr = new Array(4 - count);
		for (var i = 0; i < 4 - count; i++) {
			arr[i] = i;
		}
		$scope.noFileVoList = arr;
	}
	;

	/**
	 * @author abliu
	 * @description 新增成功后初始化页面
	 */
	function initPage() {
		// 获取创建模块的空对象
		//$scope.getCreateNewsFeedInfo();
		// 展示imageui
		$('.imageUpload').removeClass('slideDown');
		$('.tagChild').removeClass('slideDown');
		// 更新列表信息 todo
		// $scope.getNewsFeedList();
	}
	;

	function refreshNewsFeedSelectOrAdd(id, isadd) {
		/*
		 * if(isadd){ //更新列表信息 todo $scope.page.condition.pageIndex = 1;
		 * $scope.getNewsFeedList(); return; }
		 */

		var page = {};
		page.condition = {
			pageIndex: 1
		};
		page.condition.id = id
		newsFeedService.getNewsFeedList(page.condition).then(angular.bind(this, function then() {
			var result = newsFeedService.listResult;
			if (result.code == 0) {
				var list = result.data.list;
				if (list.length != 1) {
					// 更新列表信息 todo
					$scope.getNewsFeedList();
					return;
				}
				if (isadd) {
					$scope.page.list.splice(0, 0, list[0]);
					$scope.initListMasonry();
					return;
				}
				var m = $scope.page.list.length;
				for (var i = 0; i < m; i++) {
					if (list[0].newsInfo.id == $scope.page.list[i].newsInfo.id) {
						$scope.page.list[i].commentList = list[0].commentList;
						$scope.page.list[i].content = list[0].content;
						$scope.page.list[i].accounts = list[0].accounts;
						$scope.page.list[i].tagAccounts = list[0].tagAccounts;
						$scope.page.list[i].fileVoList = list[0].fileVoList;
						$scope.page.list[i].groupName = list[0].groupName;
						$scope.page.list[i].newsInfo = list[0].newsInfo;
						$scope.page.list[i].nqsVoList = list[0].nqsVoList;
						$scope.page.list[i].yelfList = list[0].yelfList;
						$scope.initListMasonry();
						return;
					}
				}

			} else {
				// 更新列表信息 todo
				$scope.getNewsFeedList();
			}
		}));
	}
	;

	function refreshNewsFeedDel(id) {
		var i = 0, m = $scope.page.list.length;
		for (i = 0; i < m; i++) {
			if (id == $scope.page.list[i].newsInfo.id) {
				$scope.page.list.splice(i, 1);
				$scope.initListMasonry();
				return;
			}
		}
		if (i >= m) {
			// 更新列表信息 todo
			$scope.getNewsFeedList();
		}
	};

	/**获取最后一次评论是家长的NEWS COUNT */
	$scope.getNewFeedback = function () {
		newsFeedService.getNewFeedback($scope.page.condition).then(function () {
			var result = newsFeedService.newFeedbackResult
			$scope.feedbacks = result.data;
		});
	}

	$scope.feedback = false;
	$scope.searchFeedback = function () {
		$scope.feedback = $scope.feedback ? false : true;
		if ($scope.feedback) {
			$scope.page.condition.feedbacks = $scope.feedbacks;
		} else {
			$scope.page.condition.feedbacks = [];
		}
		$scope.search();
	}

}
angular.module('kindKidsApp')
	.controller(
		'newsfeedCtrl',
		['$scope', '$rootScope', '$timeout', '$window', 'newsFeedService', 'fileService', 'image_resizer', 'commonService', '$filter',
			newsfeedCtrl]);