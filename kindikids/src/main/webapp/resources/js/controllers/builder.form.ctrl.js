'use strict';

/**
 * @author abliu
 * @description builder Controller
 */
function builderFormCtrl($scope, $uibModalInstance, formService, formObj, code, hiddenSave) {

	$scope.code = code;

	$scope.hiddenSave = hiddenSave;

	$scope.options = [ {
		id : 1,
		text : 'TextBox'
	}, {
		id : 2,
		text : 'TextArea'
	}, {
		id : 3,
		text : 'Radio'
	}, {
		id : 4,
		text : 'CheckBox'
	}, {
		id : 5,
		text : 'Select'
	}, {
		id : 6,
		text : 'File'
	}, {
		id : 7,
		text : 'Table'
	}, {
		id : 8,
		text : 'Signature'
	}, {
		id : 9,
		text : 'P'
	}, {
		id : 10,
		text : 'H1'
	}, {
		id : 11,
		text : 'H2'
	}, {
		id : 12,
		text : 'H3'
	}, {
		id : 13,
		text : 'H4'
	}, {
		id : 14,
		text : 'H5'
	}, {
		id : 15,
		text : 'H6'
	} ];
	$scope.options = [ {
		id : 1,
		text : 'TextBox'
	}, {
		id : 2,
		text : 'TextArea'
	}, {
		id : 3,
		text : 'Radio'
	}, {
		id : 4,
		text : 'CheckBox'
	}, {
		id : 5,
		text : 'Select'
	}, {
		id : 6,
		text : 'File'
	}, {
		id : 7,
		text : 'Table'
	}, {
		id : 8,
		text : 'Signature'
	}, {
		id : 9,
		text : 'P'
	}, {
		id : 10,
		text : 'H1'
	}, {
		id : 11,
		text : 'H2'
	}, {
		id : 12,
		text : 'H3'
	}, {
		id : 13,
		text : 'H4'
	}, {
		id : 14,
		text : 'H5'
	}, {
		id : 15,
		text : 'H6'
	} ];

	// 新增filed,选择的字段
	$scope.selectFiled = "";

	/**
	 * @author abliu
	 * @description 初始化form
	 */
	$scope.initForm = function() {
		formService.getOptions('common/getFormGroups.do').then(angular.bind(this, function then() {
			$scope.dynamicOptions = formService.optionsResult;
		}));
		if ($scope.code == "0") {
			if (!formObj) {
				$scope.form = {
					"id" : "",
					"name" : "Form Name",
					"attributes" : []
				};
			} else {
				$scope.form = formObj;
			}
		} else {
			if (!formObj) {
				formService.getForm($scope.code).then(angular.bind(this, function then() {
					$scope.form = formService.getFormResult;
				}));
			} else {
				$scope.form = formObj;
			}
		}
	};

	/**
	 * @author abliu
	 * @description save form
	 */
	$scope.saveForm = function() {
		// 处理form排序
		var m = $scope.form.attributes.length;
		for (var i = 0; i < m; i++) {
			$scope.form.attributes[i].sort = i;
		}

		$uibModalInstance.close($scope.form);
		// formService.saveForm($scope.form).then(angular.bind(this, function
		// then() {
		// }));
	};

	$scope.close = function() {
		$scope.confirm('Close', 'Exit this page?', 'OK', function() {
			$uibModalInstance.dismiss('cancel');
		}, 'NO');
	};
	/**
	 * @author abliu
	 * @description 新增form 字段
	 */
	$scope.addFiled = function() {
		if ($scope.selectFiled == undefined || $scope.selectFiled == "") {
			return;
		}
		var sort = $scope.form.attributes.length;
		var filed = {};
		switch ($scope.selectFiled) {
		case 1:
			filed = {
				"id" : "",
				"name" : "",
				"type" : 1,
				"labelText" : "TextBox",
				"sort" : sort,
				"json" : {
					"helpText" : "",
					"placeholder" : ""
				},
				"value" : null
			};
			break;
		case 2:
			filed = {
				"id" : "",
				"name" : "",
				"type" : 2,
				"labelText" : "TextArea",
				"sort" : sort,
				"json" : {
					"helpText" : "",
					"placeholder" : ""
				},
				"value" : null
			};
			break;
		case 3:
			filed = {
				"id" : "",
				"name" : "Radio",
				"type" : 3,
				"labelText" : "Radio",
				"sort" : sort,
				"json" : {
					"options" : [ {
						"id" : "1",
						"text" : "1"
					}, {
						"id" : "2",
						"text" : "2"
					} ]
				},
				"value" : null
			};
			break;
		case 4:
			filed = {
				"id" : "",
				"name" : "CheckBox",
				"type" : 4,
				"labelText" : "CheckBox",
				"sort" : sort,
				"json" : {
					"options" : [ {
						"id" : "1",
						"text" : "1"
					}, {
						"id" : "2",
						"text" : "2"
					} ]
				},
				"value" : null
			};
			break;
		case 5: // OptionsType代表是否支持多选？
			filed = {
				"id" : "",
				"name" : "",
				"type" : 5,
				"labelText" : "Select",
				"sort" : sort,
				"json" : {
					"multiple" : 0,
					"optionsType" : 1,
					"options" : [ {
						"id" : "1",
						"text" : "1"
					}, {
						"id" : "2",
						"text" : "2"
					} ]
				},
				"value" : null
			};
			break;
		case 6:
			filed = {
				"id" : "",
				"name" : "",
				"type" : 6,
				"labelText" : "File",
				"sort" : sort,
				"json" : {
					"helpText" : ""
				},
				"value" : null
			};
			break;
		case 7:
			filed = {
				"id" : "",
				"name" : "",
				"type" : 7,
				"labelText" : "Table",
				"sort" : sort,
				"json" : {
					"addRow" : 0,
					"cols" : 1,
					"rows" : 1,
					"value" : [ {
						"col" : 0,
						"row" : 0,
						"value" : "Table"
					} ]
				},
				"value" : null
			};
			tableValueToArray(filed);
			break;
		case 8:
			filed = {
				"id" : "",
				"name" : "",
				"type" : 8,
				"labelText" : "Signature",
				"sort" : sort,
				"value" : null
			};
			break;
		case 9:
			filed = {
				"id" : "",
				"name" : "",
				"type" : 9,
				"labelText" : "P",
				"sort" : sort,
				"value" : null
			};
			break;
		case 10:
			filed = {
				"id" : "",
				"name" : "",
				"type" : 10,
				"labelText" : "H1",
				"sort" : sort,
				"value" : null
			};
			break;
		case 11:
			filed = {
				"id" : "",
				"name" : "",
				"type" : 11,
				"labelText" : "H2",
				"sort" : sort,
				"value" : null
			};
			break;
		case 12:
			filed = {
				"id" : "",
				"name" : "",
				"type" : 12,
				"labelText" : "H3",
				"sort" : sort,
				"value" : null
			};
			break;
		case 13:
			filed = {
				"id" : "",
				"name" : "",
				"type" : 13,
				"labelText" : "H4",
				"sort" : sort,
				"value" : null
			};
			break;
		case 14:
			filed = {
				"id" : "",
				"name" : "",
				"type" : 14,
				"labelText" : "H5",
				"sort" : sort,
				"value" : null
			};
			break;
		case 15:
			filed = {
				"id" : "",
				"name" : "",
				"type" : 15,
				"labelText" : "H6",
				"sort" : sort,
				"value" : null
			};
			break;
		}

		$scope.form.attributes.push(filed);
	};

	/**
	 * @author abliu
	 * @description 字段排序 上
	 */
	$scope.upFiled = function(index) {
		if (index == 0) {
			return;
		}
		var obj = $scope.form.attributes[index];
		if (obj.isopen) {
			$scope.index = $scope.index - 1;
		} else if ($scope.form.attributes[index - 1].isopen) {
			$scope.index = $scope.index + 1;
		}
		$scope.form.attributes.splice(index, 1);
		$scope.form.attributes.splice(--index, 0, obj);
	};

	/**
	 * @author abliu
	 * @description 字段排序 下
	 */
	$scope.downFiled = function(index) {
		// 最后一个直接跳出
		if (index + 1 == $scope.form.attributes.length) {
			return;
		}
		var obj = $scope.form.attributes[index];
		if (obj.isopen) {
			$scope.index = $scope.index + 1;
		} else if ($scope.form.attributes[index + 1].isopen) {
			$scope.index = $scope.index - 1;
		}
		$scope.form.attributes.splice(index, 1);
		$scope.form.attributes.splice(++index, 0, obj);
	};

	/**
	 * @author abliu
	 * @description 删除
	 */
	$scope.delFiled = function(index) {
		$scope.form.attributes.splice(index, 1);
	};

	// TEXT类型编辑的对象
	$scope.textObj = {}
	// 编辑集合中的索引
	$scope.index = 0
	/**
	 * @author abliu
	 * @description 打开form 字段 编辑
	 */
	$scope.openTextFiled = function(index) {
		closePopover();
		$scope.index = index;
		$scope.textObj.json = {};
		$scope.textObj.name = $scope.form.attributes[index].name;
		$scope.textObj.type = $scope.form.attributes[index].type;
		$scope.textObj.labelText = $scope.form.attributes[index].labelText;
		if ($scope.form.attributes[index].json != undefined && $scope.form.attributes[index].json.helpText != undefined) {
			$scope.textObj.json.helpText = $scope.form.attributes[index].json.helpText;
		}
		if ($scope.form.attributes[index].json != undefined && $scope.form.attributes[index].json.placeholder != undefined) {
			$scope.textObj.json.placeholder = $scope.form.attributes[index].json.placeholder;
		}
	};
	/**
	 * @author abliu
	 * @description 保存对字段的编辑
	 */
	$scope.saveTextFiled = function(index) {
		$scope.cancelPopover(index);
		$scope.form.attributes[index].name = $scope.textObj.name;
		$scope.form.attributes[index].labelText = $scope.textObj.labelText;
		if ($scope.form.attributes[index].json != undefined && $scope.form.attributes[index].json.helpText != undefined) {
			$scope.form.attributes[index].json.helpText = $scope.textObj.json.helpText;
		}
		if ($scope.form.attributes[index].json != undefined && $scope.form.attributes[index].json.placeholder != undefined) {
			$scope.form.attributes[index].json.placeholder = $scope.textObj.json.placeholder;
		}
	};

	/**
	 * @author abliu
	 * @description 取消对字段的编辑
	 */
	$scope.cancelPopover = function(index) {
		$scope.form.attributes[index].isopen = false;
	}

	// Form类型编辑的对象
	$scope.formObj = {}
	/**
	 * @author abliu
	 * @description 打开form 字段 编辑
	 */
	$scope.openFormPopover = function() {
		closePopover();
		$scope.formObj.name = $scope.form.name;
	};

	/**
	 * @author abliu
	 * @description 保存对form的编辑
	 */
	$scope.saveFormPopover = function() {
		$scope.form.name = $scope.formObj.name;
		$scope.cancelFormPopover();
	};

	/**
	 * @author abliu
	 * @description 取消对字段的编辑
	 */
	$scope.cancelFormPopover = function() {
		$scope.form.isopen = false;
	}

	// CHECK OR RADIOS编辑的对象
	$scope.checkradiosObj = {}
	/**
	 * @author abliu
	 * @description 打开CHECK RADIOS 字段 编辑
	 */
	$scope.openCheckRadiosPopover = function(index) {
		closePopover();
		var obj = $scope.form.attributes[index];
		$scope.index = index;
		$scope.checkradiosObj.json = {};
		$scope.checkradiosObj.name = obj.name;
		$scope.checkradiosObj.type = obj.type;
		$scope.checkradiosObj.labelText = obj.labelText;
		$scope.checkradiosObj.options = {};
		var m = obj.json.options.length;
		for (var i = 0; i < m; i++) {
			if (i > 0) {
				$scope.checkradiosObj.options.id += "\n" + obj.json.options[i].id;
				$scope.checkradiosObj.options.text += "\n" + obj.json.options[i].text;
			} else {
				$scope.checkradiosObj.options.id = obj.json.options[i].id;
				$scope.checkradiosObj.options.text = obj.json.options[i].text;
			}
		}
	};

	/**
	 * @author abliu
	 * @description 保存对checkbox的编辑
	 */
	$scope.saveCheckRadiosPopover = function(index, type) {
		var ids = $scope.checkradiosObj.options.id.split("\n");
		var texts = $scope.checkradiosObj.options.text.split("\n");
		if (ids.length != texts.length) {
			if (type == 3) {
				$scope.message("Your radio button options can not be the same value. Please check your values and try again.");
				return;
			} else if (type == 4) {
				$scope.message("Your checkbox options can not be the same value. Please check your values and try again.");
				return;
			}
		}

		var msg = checkOptionRepeat(ids, type);
		if (msg != "") {
			$scope.message(msg);
			return;
		}

		$scope.form.attributes[index].name = $scope.checkradiosObj.name;
		$scope.form.attributes[index].labelText = $scope.checkradiosObj.labelText;
		$scope.form.attributes[index].json.options = [];

		var m = ids.length;
		for (var i = 0; i < m; i++) {
			var item = {
				"id" : ids[i],
				"text" : texts[i]
			};
			$scope.form.attributes[index].json.options.push(item);
		}
		$scope.cancelPopover(index);
	};

	/**
	 * @author abliu
	 * @description 判断option是否重复
	 */
	function checkOptionRepeat(options, index) {
		var m = options.length;
		for (var i = 0; i < m; i++) {
			for (var j = 0; j < m; j++) {
				if (j != i && options[i] == options[j]) {
					if (index == 3) {
						return "Your radio button options can not be the same value. Please check your values and try again.";
					} else if (index == 4) {
						return "Your checkbox options can not be the same value. Please check your values and try again.";
					} else if (index == 5) {
						return "Your drop down box options can not be the same value. Please check your values and try again.";
					}
				}
			}
		}
		return "";
	}

	// select编辑的对象
	$scope.selectObj = {}
	/**
	 * @author abliu
	 * @description 打开CHECK RADIOS 字段 编辑
	 */
	$scope.openSelectPopover = function(index) {
		closePopover();
		var obj = $scope.form.attributes[index];
		$scope.index = index;
		$scope.selectObj.json = {};
		$scope.selectObj.name = obj.name;
		$scope.selectObj.type = obj.type;
		$scope.selectObj.labelText = obj.labelText;
		$scope.selectObj.options = {};
		$scope.selectObj.json.multiple = obj.json.multiple;
		$scope.selectObj.json.optionsType = obj.json.optionsType;
		$scope.selectObj.ajaxValue = obj.json.ajaxValue;

		// 处理已删除的数据源
		getNewOptions($scope.selectObj.ajaxValue, obj.json.ajaxName);

		var m = obj.json.options.length;
		for (var i = 0; i < m; i++) {
			if (i > 0) {
				$scope.selectObj.options.id += "\n" + obj.json.options[i].id;
				$scope.selectObj.options.text += "\n" + obj.json.options[i].text;
			} else {
				$scope.selectObj.options.id = obj.json.options[i].id;
				$scope.selectObj.options.text = obj.json.options[i].text;
			}
		}
	};

	// 处理option与选中值
	function getNewOptions(ajaxValue, ajaxName) {
		var sourceOptions = $scope.dynamicOptions;
		if (ajaxValue == null || ajaxValue == undefined) {
			return;
		}
		for (var j = 0, n = ajaxValue.length; j < n; j++) {
			var i = 0, m = sourceOptions.length;
			for (; i < m; i++) {
				if (sourceOptions[i].id == ajaxValue[j]) {
					break;
				}
			}
			if (i >= m) {
				// 说明不存在
				var option = {};
				option.id = ajaxValue[j];
				option.text = ajaxName[j];
				option.disabled = true;
				sourceOptions.push(option);
				break;
			}
		}

	}

	/**
	 * @author abliu
	 * @description 保存对form的编辑
	 */
	$scope.saveSelectPopover = function(index, type) {
		if ($scope.selectObj.json.optionsType == 0) {
			// 判断选中的值是否在该集合中
			var ajaxName = [];
			var ajaxValue = $scope.selectObj.ajaxValue;
			var sourceOptions = $scope.dynamicOptions;
			for (var j = 0, n = ajaxValue.length; j < n; j++) {
				var i = 0, m = sourceOptions.length;
				for (; i < m; i++) {
					if (sourceOptions[i].id == ajaxValue[j] && !sourceOptions[i].disabled) {
						ajaxName.push(sourceOptions[i].text);
						break;
					}
				}
				if (i >= m) {
					// 表示不存在
					$scope.message("Option has been deleted!");
					return;
				}
			}

			$scope.form.attributes[index].name = $scope.selectObj.name;
			$scope.form.attributes[index].labelText = $scope.selectObj.labelText;
			$scope.form.attributes[index].json.multiple = $scope.selectObj.json.multiple;
			$scope.form.attributes[index].json.optionsType = $scope.selectObj.json.optionsType;

			$scope.form.attributes[index].json.options = [];
			$scope.form.attributes[index].json.ajaxUrl = undefined;
			$scope.form.attributes[index].json.ajaxValue = $scope.selectObj.ajaxValue;
			$scope.form.attributes[index].json.ajaxName = ajaxName;
			$scope.form.attributes[index].json.ajaxUrl = './common/getFormUsersByGroups.do?chooseGroups='
					+ encodeURIComponent(JSON.stringify($scope.selectObj.ajaxValue));
		} else {
			var ids, texts;
			if ($scope.selectObj.options.id == undefined) {
				ids = "";
			} else {
				ids = $scope.selectObj.options.id.split("\n");
			}
			if ($scope.selectObj.options.text == undefined) {
				texts = "";
			} else {
				texts = $scope.selectObj.options.text.split("\n");
			}

			if (ids.length != texts.length) {
				$scope.message("Your drop down box options can not be the same value. Please check your values and try again.");
				return;
			}

			var msg = checkOptionRepeat(ids, type);
			if (msg != "") {
				$scope.message(msg);
				return;
			}
			$scope.form.attributes[index].name = $scope.selectObj.name;
			$scope.form.attributes[index].labelText = $scope.selectObj.labelText;
			$scope.form.attributes[index].json.multiple = $scope.selectObj.json.multiple;
			$scope.form.attributes[index].json.optionsType = $scope.selectObj.json.optionsType;

			$scope.form.attributes[index].json.options = [];
			$scope.form.attributes[index].json.ajaxUrl = undefined;
			var m = ids.length;
			for (var i = 0; i < m; i++) {
				var item = {
					"id" : ids[i],
					"text" : texts[i]
				};
				$scope.form.attributes[index].json.options.push(item);
			}
		}
		$scope.cancelPopover(index);
	};

	// table编辑的对象
	$scope.tableObj = {}
	/**
	 * @author abliu
	 * @description 打开table字段 编辑
	 */
	$scope.opentablePopover = function(index) {
		closePopover();
		var sourceObj = $scope.form.attributes[index];
		var obj = $scope.tableObj;

		$scope.index = index;

		changeTableObj(obj, sourceObj);
	};

	/**
	 * @author abliu
	 * @description 保存table字段 编辑
	 */
	$scope.savetablePopover = function(index) {
		var obj = $scope.form.attributes[index];
		var sourceObj = $scope.tableObj;

		changeTableObj(obj, sourceObj);

		tableValueToArray(obj);

		$scope.cancelPopover(index);
	};

	/**
	 * @author abliu
	 * @description 添加tabel 值
	 */
	$scope.addtableRowColValue = function() {
		var item = {};
		item.row = 0;
		item.col = 0;
		item.value = "";
		$scope.tableObj.json.value.push(item);
	};
	/**
	 * @author abliu
	 * @description 移除tabel 值
	 */
	$scope.removetableRowColValue = function(index) {
		$scope.tableObj.json.value.splice(index, 1);
	};

	/**
	 * @author abliu
	 * @description 保存table字段 替换
	 */
	function changeTableObj(obj, sourceObj) {
		obj.json = {};
		obj.json.value = [];
		obj.name = sourceObj.name;
		obj.type = sourceObj.type;
		obj.labelText = sourceObj.labelText;
		obj.json.cols = sourceObj.json.cols;
		obj.json.rows = sourceObj.json.rows;
		obj.json.addRow = sourceObj.json.addRow;

		var m = sourceObj.json.value.length;
		for (var i = 0; i < m; i++) {
			var item = {};
			item.row = sourceObj.json.value[i].row;
			item.col = sourceObj.json.value[i].col;
			item.value = sourceObj.json.value[i].value;
			obj.json.value.push(item);
		}
	}

	/**
	 * @author abliu
	 * @description 关闭所有弹出框
	 */
	function closePopover() {
		$scope.form.isopen = false;
		var m = $scope.form.attributes.length;
		for (var i = 0; i < m; i++) {
			$scope.form.attributes[i].isopen = false;
		}
	}

	function tableValueToArray(obj) {
		var cols = obj.json.cols;
		var rows = obj.json.rows;
		var value = obj.json.value;
		var tArray = new Array();

		obj.json.tArray = [];

		for (var k = 0; k < rows; k++) {
			tArray[k] = new Array();
			for (var j = 0; j < cols; j++) {
				var m = value.length;
				var l = 0;
				for (l = 0; l < m; l++) {
					if (value[l].row == k && value[l].col == j) {
						tArray[k][j] = value[l].value;
						break;
					}
				}
				if (l >= m) {
					tArray[k][j] = null;
				}
			}
			obj.json.tArray.push(tArray[k]);
		}
	}
	;

};

angular.module('kindKidsApp').controller('builderFormCtrl',
		[ '$scope', '$uibModalInstance', 'formService', 'formObj', 'code', 'hiddenSave', builderFormCtrl ]);