'use strict';

function profileModalCtrl($scope, $rootScope, $timeout, $window, $uibModal, $uibModalInstance, waitingService, appId, callbackReload) {
    $scope.callbackReload = callbackReload;

    waitingService.getChildDetails(appId).then(
        angular.bind(this, function then() {
            var result = waitingService.detailsResult;
            if (result.code == 0) {
                $scope.details = result.data;
            } else {
                $rootScope.message(result.msg);
            }
        })
    );

    $scope.cancel = function () {
        $scope.confirm('Close', 'Exit this page?', 'OK', function () {
            $uibModalInstance.dismiss('cancel');
        }, 'NO');
    };

    $scope.save = function (details) {
        waitingService.save(details.child).then(angular.bind(this, function then() {
            var result = waitingService.saveReuslt;
            $rootScope.message(result.msg);
            $scope.callbackReload();
        }));
    }
}
angular.module('kindKidsApp').controller('profileModalCtrl', ['$scope', '$rootScope', '$timeout', '$window', '$uibModal', '$uibModalInstance', 'waitingService', 'appId', 'callbackReload', profileModalCtrl]);