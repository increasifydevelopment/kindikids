'use strict';

function waitingCtrl($scope, $rootScope, $timeout, $window, $uibModal, waitingService) {
    $scope.condition = {
        keyword: null,
        centersId: null,
        listType: [],
        maxSize: 7,
        pageIndex: 1,
        centerTypes: [],
        selectCenterIds: []
    };

    var dealSelectCenterIds = function () {
        $scope.condition.selectCenterIds = [];
        if ($scope.condition.centerTypes.length == -1) {
            return;
        }
        for (var i in $scope.condition.centerTypes) {
            var centerId = $scope.condition.centerTypes[i];
            if (centerId == 1) {
                for (var ii in $scope.condition.allCenters) {
                    var center = $scope.condition.allCenters[ii];
                    if (center.ryde) {
                        continue;
                    }
                    $scope.condition.selectCenterIds.push(center.id);
                }
            } else {
                $scope.condition.selectCenterIds.push(centerId);
            }
        }
    }

    $scope.waitingList = function () {
        $scope.showLoading('#waitinglistId');
        dealSelectCenterIds();
        waitingService.getWaitingList($scope.condition).then(angular.bind(this, function then() {
            $scope.hideLoading('#waitinglistId');
            var result = waitingService.waitingResult;
            if (result.code == 0) {
                $scope.waitingListResult = result.data;
                $scope.condition.pageIndex = $scope.waitingListResult.condition.pageIndex;
                $scope.condition.pageSize = $scope.waitingListResult.condition.pageSize;
                $scope.condition.totalSize = $scope.waitingListResult.condition.totalSize;
                $scope.condition.allCenters = $scope.waitingListResult.condition.allCenters;

                $scope.condition.appStartDate = $scope.waitingListResult.condition.appStartDate;
                $scope.condition.appEndDate = $scope.waitingListResult.condition.appEndDate;
                $scope.condition.requestStartDate = $scope.waitingListResult.condition.requestStartDate;
                $scope.condition.requestEndDate = $scope.waitingListResult.condition.requestEndDate;

                $rootScope.countResult.counts = $scope.waitingListResult.condition.counts;
                if ($scope.waitingListResult.list.length == 0) {
                    $scope.confirm('Message', 'No results match your search.', 'OK', function () {
                        $("#privacy-modal").modal('hide');
                    });
                }
            } else {
                $rootScope.message(result.msg);
            }
        }));
    }

    $scope.changePage = function (index) {
        $scope.condition.pageIndex = index;
        $scope.waitingList();
    }

    $scope.keyEvent = function (ev) {
        if (ev.keyCode == 13) {
            $scope.changePage(1);
            $("input").blur();
        }
    };

    $scope.setDatePosition = function (id, index) {
        $scope.setDatePositionId = id;
        $scope.setDatePositionIndex = id;
    }

    $scope.changeModel = function (item) {
        $scope.positioningDate(item);
    }

    $scope.positioningDate = function (item) {
        waitingService.setpPositioningDate(item).then(angular.bind(this, function then() {
            var result = waitingService.positionResult;
            if (result.code == 0) {
                $scope.waitingList();
            }
        }))
    }

    $scope.sendPlanEmail = function () {
        waitingService.sendPlanEmail().then(angular.bind(this, function then() {
            $rootScope.message('Email has been successfully sent.');
        }))
    }

    $scope.openAllocateModel = function (model) {
        if (model.listType == 1) {
            $scope.moveType = 2;
        } else {
            $scope.moveType = 1;
        }
        $scope.listType = model.listType;
        $scope.familyToType = 0;
        $scope.moveModel = model;
        $scope.allocateModal(model, $scope.moveType, $scope.listType, $scope.familyToType);
    }

    $scope.allocateModal = function (model, moveType, listType, familyToType) {
        $uibModal.open({
            templateUrl: 'views/waitingList/allocate-modal.html?res_v=' + $scope.res_v,
            controller: 'allocateModalCtrl',
            backdrop: 'static',
            keyboard: false,
            transclude: false,
            resolve: {
                model: function () {
                    return model;
                },
                moveType: function () {
                    return moveType;
                },
                listType: function () {
                    return listType;
                },
                familyToType: function () {
                    return familyToType;
                },
                callbackReload: function () {
                    return $scope.reloadFun;
                }
            }
        });
    }

    $scope.reloadFun = function () {
        $scope.changePage(1);
    }

    $scope.openProfile = function (familyId) {
        $uibModal.open({
            templateUrl: 'views/user/family/child_edit.html?res_v=' + $scope.res_v,
            controller: 'userFamilyEditCtrl',
            animation: true,
            size: "lg",
            backdrop: 'static',
            keyboard: false,
            windowClass: "profile-popup profile-popup-family modal-align-top",
            resolve: {
                familyId: function () {
                    return familyId;
                },
                functions: function () {
                    return $scope.functions;
                },
                currentUser: function () {
                    return $scope.currentUser;
                },
                callbackReload: function () {
                    return $scope.waitingList;
                }
            }
        });
    };

    $scope.openAppProfile = function (id) {
        $uibModal.open({
            templateUrl: 'views/waitingList/profile-modal.html?res_v=' + $scope.res_v,
            controller: 'profileModalCtrl',
            animation: true,
            size: "lg",
            backdrop: 'static',
            keyboard: false,
            windowClass: "profile-popup profile-popup-family modal-align-top",
            resolve: {
                appId: function () {
                    return id;
                },
                callbackReload: function () {
                    return $scope.waitingList;
                }
            }
        });
    }

    $scope.openWin = function (objId, childId, type, childInfo, isRead) {
        // 打开不同编辑页面
        if (type == 0 || type == 1 || type == 2 || type == 3 || type == 4 || type == 5 || type == 6) { // 园长操作转园申请
            $uibModal.open({
                templateUrl: 'views/user/family/change_attendance.html?res_v=' + $scope.res_v,
                controller: 'userFamilyEditChangeAttendanceCtrl',
                size: "lg",
                backdrop: 'static',
                keyboard: false,
                windowClass: "profile-popup",
                resolve: {
                    childInfo: function () {
                        return childInfo;
                    },
                    childId: function () {
                        return childInfo.accountInfo.id;
                    },
                    type: function () {
                        return type;
                    },
                    functions: function () {
                        return $scope.functions;
                    },
                    currentUser: function () {
                        return $scope.currentUser;
                    },
                    callbackFunction: function () {
                        return $scope.waitingList;
                    },
                    objId: function () {
                        return objId;
                    },
                    isRead: function () {
                        return isRead;
                    }
                }
            });

        }
    };

    $scope.changeCenter = function (type, listType) {
        $scope.condition.listType = listType;
        $scope.condition.centre = type;
        $scope.changePage(1);
    }

    $scope.selectList = function (listType) {
        if (listType == -1) {
            $scope.condition.listType = [];
        } else {
            if ($scope.condition.listType.indexOf(listType) === -1) {
                $scope.condition.listType.push(listType);
            } else {
                $scope.condition.listType.splice($scope.condition.listType.indexOf(listType), 1);
            }
        }
    }

    $scope.selectRequestedDays = function (selectType) {
        switch (selectType) {
            case 1:
                $scope.condition.monday = $scope.condition.monday == true ? false : true;
                break;
            case 2:
                $scope.condition.tuesday = $scope.condition.tuesday == true ? false : true;
                break;
            case 3:
                $scope.condition.wednesday = $scope.condition.wednesday == true ? false : true;
                break;
            case 4:
                $scope.condition.thursday = $scope.condition.thursday == true ? false : true;
                break;
            case 5:
                $scope.condition.friday = $scope.condition.friday == true ? false : true;
                break;
            default:
                $scope.condition.monday = false;
                $scope.condition.tuesday = false;
                $scope.condition.wednesday = false;
                $scope.condition.thursday = false;
                $scope.condition.friday = false;
                break;
        }
    }

    $scope.search = function () {
        $scope.changePage(1);
    }

    $scope.selectCenterFun = function (types) {
        if (types == -1) {
            $scope.condition.centerTypes = [];
        } else {
            if ($scope.condition.centerTypes.indexOf(types) === -1) {
                $scope.condition.centerTypes.push(types);
                if (types == 1) {
                    for (var index in $scope.condition.allCenters) {
                        var center = $scope.condition.allCenters[index];
                        if ($scope.condition.centerTypes.indexOf(center.id) != -1 && !center.ryde) {
                            $scope.condition.centerTypes.splice($scope.condition.centerTypes.indexOf(center.id), 1);
                        }
                    }
                } else {
                    for (var index in $scope.condition.allCenters) {
                        var center = $scope.condition.allCenters[index];
                        if (types == center.id && !center.ryde && $scope.condition.centerTypes.indexOf(1) != -1) {
                            $scope.condition.centerTypes.splice($scope.condition.centerTypes.indexOf(1), 1);
                        }
                    }
                }
            } else {
                $scope.condition.centerTypes.splice($scope.condition.centerTypes.indexOf(types), 1);
            }
        }

    }

    $scope.cancelFunction = function () {
        $('#confirmId').modal('hide');
        $scope.zindex = 1041;
    }

    $scope.okFunction = function () {
        $('#confirmId').modal('hide');
        $('#' + $scope.modelId).modal('hide');
        $scope.zindex = 1041;
    }

    $scope.dealClass = function (center, type) {
        if (type == 3) {
            return 'pbadge-gray';
        }
        if (center.ryde) {
            return 'pbadge-red';
        } else {
            return 'pbadge-mint';
        }
    }

    $scope.appDateCallback = function (flag) {
        if (flag) {
            $scope.condition.appStartDate = null;
            $scope.condition.appEndDate = null;
        }
    }

    $scope.requestedDateCallback = function (flag) {
        if (flag) {
            $scope.condition.requestStartDate = null;
            $scope.condition.requestEndDate = null;
        }
    }

    $scope.positionManagerExport = function () {
        dealSelectCenterIds();
        waitingService.exportCsv($scope.condition).then(angular.bind(this, function then() {
            var result = waitingService.exportCsvResult;
            if (result.code == 0) {
                $window.location.href = "./waiting/exportCsv.do?uuid=" + result.data;
            }
        }))
    }

}
angular.module('kindKidsApp').controller('waitingCtrl', ['$scope', '$rootScope', '$timeout', '$window', '$uibModal', 'waitingService', waitingCtrl]);