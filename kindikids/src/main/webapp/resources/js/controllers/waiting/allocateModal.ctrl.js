'use strict';

function allocateModalCtrl($scope, $rootScope, $timeout, $window, $uibModal, $uibModalInstance, model, moveType, listType, familyToType, callbackReload, waitingService) {
    $scope.moveModel = model;
    $scope.moveType = moveType;
    $scope.listType = listType;
    $scope.familyToType = familyToType;
    $scope.callbackReload = callbackReload;

    $scope.changeMoveTo = function (type) {
        $scope.moveType = type;
        $scope.familyToType = familyToType;
    }

    $scope.changeFamily = function (type) {
        $scope.familyToType = type;
    }

    $scope.planConfirm = function () {
        $scope.submitModel = new Object();
        $scope.submitModel.objId = $scope.moveModel.id;
        $scope.submitModel.fromType = $scope.moveModel.listType;
        $scope.submitModel.moveToType = $scope.moveType;
        $scope.submitModel.familyToType = $scope.familyToType;
        if ($scope.submitModel.familyToType == 1) {
            $scope.submitModel.familyId = $scope.moveModel.chooseFamilyId;
        }
        // if ($scope.submitModel.fromType == 0 && ($scope.moveType == 1 || $scope.moveType == 2) && $scope.familyToType == 0) {
        //     $scope.confirm('Confirm', 'Is this change made for the Position Plan of the Next Year?', 'YES', function () {
        //         $scope.submitModel.isPlan = true;
        //         $scope.submitMove();
        //     }, 'NO', function () {
        //         $scope.submitModel.isPlan = false;
        //         $scope.submitMove();
        //     });
        // } else {
        //     $scope.submitMove();
        // }

        $scope.submitMove();
    }

    $scope.submitMove = function () {
        waitingService.submitMoveTo($scope.submitModel).then(angular.bind(this, function then() {
            var result = waitingService.submitMoveResult;
            if (result.code == 0) {
                $scope.callbackReload();
                $uibModalInstance.dismiss('cancel');
            } else if (result.code == 2) {
                $scope.confirm('Confirm', result.msg, 'Confirm', function () {
                    $scope.submitModel.familyId = result.data;
                    $scope.submitMove();
                }, 'Cancel');
            } else {
                $rootScope.message(result.msg);
            }
        }))
    }

    $scope.cancel = function () {
        $scope.confirm('Close', 'Exit this page?', 'OK', function () {
            $uibModalInstance.dismiss('cancel');
        }, 'NO');
    };
}
angular.module('kindKidsApp').controller('allocateModalCtrl', ['$scope', '$rootScope', '$timeout', '$window', '$uibModal', '$uibModalInstance', 'model', 'moveType', 'listType', 'familyToType', 'callbackReload', 'waitingService', allocateModalCtrl]);