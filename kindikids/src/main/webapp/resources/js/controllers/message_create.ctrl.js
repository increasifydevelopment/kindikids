'use strict';

function msgCreateCtrl($scope, $rootScope, $timeout, $window,
		$uibModalInstance, messageService, fileService) {

	$scope.messageInfo = {};
	// $scope.msgObj=msgObj;
	// 选择的nqs
	$scope.chooseNqs = [];
	/**
	 * @author abliu
	 * @description 获取message列表
	 */
	$scope.saveNqsResult = function(newNqs) {
		$scope.chooseNqs = newNqs;
	}

	/**
	 * 保存
	 */
	$scope.createNewMessageInfo = function() {
		$scope.showLoading('#myCreate');
		$scope.messageInfo.messageType = 0;
		$scope.messageInfo.chooseNqs = $scope.chooseNqs;
		$scope.messageInfo.receiverIds = $scope
				.getSendUsersStr($scope.messageInfo.viewAccountIds);

		messageService.saveMessageInfo($scope.messageInfo).then(
				angular.bind(this, function then() {
					var result = messageService.data;
					$scope.hideLoading("#myCreate");
					if (result.code == 0) {
						$scope.messageInfo = result.data;
						$uibModalInstance.dismiss('cancel');
						$scope.$state.reload();
					} else {
						$rootScope.message(result.msg);
					}
				}));
	}

	/**
	 * 把接收人凭借字符串
	 */
	$scope.getSendUsersStr = function(array) {
		var value = "";
		if (!array) {
			return value;
		}
		var len = array.length;
		for (var i = 0; i < len; i++) {
			value = value + array[i] + ',';
		}
		return value;
	}
	$scope.vlidateFileFormat = function($files) {
		var result = "";
		for (var i = 0; i < $files.length; i++) {
			var fileName = $files[i].name;
			var suffix = fileName.split('.').pop().toLowerCase();
			if (".jpg,.jpeg,.png,.pdf,.doc,.docx,.xls,.xlsx,.gif,"
					.indexOf(suffix + ',') < 0) {
				result = "Please ensure your file is either a .pdf, ,doc, ,docx, ,xlsx, .xls, .png, .jpg or .gif file type.";
			}
		}
		return result;
	};
	// 已上传附件
	$scope.fileList = [];
	/**
	 * 文件上传
	 */
	$scope.upload = function($files) {
		if ($files == null || $files.length == 0) {
			return;
		}
		$("#myCreate").mymask('');
		var size = 0;
		var length = $files.length;
		var msg = $scope.vlidateFileFormat($files);
		if (msg) {
			$rootScope.message(msg);
			$scope.hideLoading("#myCreate");
			return;
		}
		for (var i = 0; i < length; i++) {
			var file = $files[i];
			if (file != null) {
				var compareResule = $scope.isRepeatAnnex($files);
				if (compareResule != '') {
					if ($("#myCreate").myisMasked()) {
						$("#myCreate").myunmask();
					}
					compareResule = compareResule.substring(0,
							compareResule.length - 2);
					$rootScope.message(compareResule + ' already exit!');
					return;
				}
				if (file.size < 10485760) {
					var fileLength = $scope.fileList.length;
					var fileObj = {};
					fileObj.fileName = file.name;
					fileObj.progress = 0;
					$scope.fileList.push(fileObj);

					fileService
							.uploadFileProgress(file, fileLength,
									callbackUploadFile, $scope.fileList)
							.then(
									angular
											.bind(
													this,
													function then() {
														var result = fileService.fdata;
														size++;
														if (result.code == 0) {
															$scope.messageInfo.relativePathNames = !$scope.messageInfo.relativePathNames ? ""
																	: $scope.messageInfo.relativePathNames;
															$scope.messageInfo.fileNames = !$scope.messageInfo.fileNames ? ""
																	: $scope.messageInfo.fileNames;
															$scope.messageInfo.relativePathNames += result.data.relativePathName
																	+ ",";
															$scope.messageInfo.fileNames += result.data.fileName
																	+ ",";
															$scope
																	.dealFiles(
																			true,
																			result.data);

															if (size == length) {
																if ($(
																		"#myCreate")
																		.myisMasked()) {
																	$(
																			"#myCreate")
																			.myunmask();
																}
															}
														} else {
															$scope
																	.dealFiles(
																			false,
																			result.data);
															$rootScope
																	.message(result.msg);
														}
														if (size == length) {
															if ($("#myCreate")
																	.myisMasked()) {
																$("#myCreate")
																		.myunmask();
															}
														}
													}));
				} else {
					$rootScope.message('Sorry, the filesize is too large, please reduce the filesize and try again.');
					if ($("#myCreate").myisMasked()) {
						$("#myCreate").myunmask();
					}
				}
			}
		}

	};

	function callbackUploadFile(fileList, index, progressPercentage) {
		if (progressPercentage == 100 && index != undefined) {
			progressPercentage = 98;
		}
		if (fileList[index].progress != undefined) {
			fileList[index].progress = progressPercentage;
		}
	}

	$scope.dealFiles = function(isSucess, data) {
		for (var i = 0, m = $scope.fileList.length; i < m; i++) {
			if ($scope.fileList[i].fileName == data.fileName) {
				if (isSucess) {
					$scope.fileList[i].progress = 100;
					$scope.fileList[i] = data;
					return;
				} else {
					$scope.fileList.splice(i, 1);
					return;
				}
			}
		}
	}

	/**
	 * 下载文件
	 */
	$scope.download = function(file) {
		$window.location.href = "./common/download.do?fileRelativeIdName="
				+ file.relativePathName + "&fileName="
				+ encodeURIComponent(file.fileName);
	};

	/**
	 * 处理附件信息
	 */
	/*
	 * $scope.dealFiles = function(isOld, AnnexIds, AnnexNames) { AnnexIds =
	 * AnnexIds == null ? "" : AnnexIds; AnnexNames = AnnexNames == null ? "" :
	 * AnnexNames; var annexIdArr = AnnexIds.split(','); var annexNameArr =
	 * AnnexNames.split(','); if (!isOld) { annexIdArr.push(AnnexIds);
	 * annexNameArr.push(AnnexNames); } for (var i = 0; i < annexIdArr.length -
	 * 1; i++) { var currAnnex = {}; if (annexIdArr[i] == '' && annexNameArr[i] ==
	 * '') { continue; } currAnnex.annexId = annexIdArr[i]; currAnnex.annexName =
	 * annexNameArr[i]; currAnnex.isOld = isOld;
	 * $scope.fileList.push(currAnnex); } }
	 */

	/**
	 * 删除附件
	 */
	$scope.removeFile = function(cfile) {
		var index = -1;
		var files = $scope.fileList;
		$scope.messageInfo.relativePathNames = "";
		$scope.messageInfo.fileNames = "";
		for (var i = 0; i < files.length; i++) {
			if (files[i].relativePathName == cfile.relativePathName) {
				index = i;
			} else {
				$scope.messageInfo.relativePathNames += files[i].relativePathName
						+ ',';
				$scope.messageInfo.fileNames += files[i].fileName + ',';
			}
		}
		if (index > -1) {
			// 移除，（移除的索引,移除个数）
			$scope.fileList.splice(index, 1);
		}
	};
	/**
	 * 判断是否重复附件
	 */
	$scope.isRepeatAnnex = function($files) {
		var annxs = "";
		if (!$scope.messageInfo.fileNames) {
			return "";
		}
		var annexNameArr = $scope.messageInfo.fileNames.split(',');
		for ( var name in annexNameArr) {
			for (var i = 0; i < $files.length; i++) {
				if (annexNameArr[name] == $files[i].name) {
					// 拼接重复的
					annxs += $files[i].name + "; ";
				}
			}
		}
		return annxs;
	};

	$scope.close = function() {
		$scope.confirm('Close', 'Exit this page?', 'OK',
				function() {
					$uibModalInstance.dismiss('cancel');
				}, 'NO');

	};
};

angular.module('kindKidsApp').controller(
		'msgCreateCtrl',
		[ '$scope', '$rootScope', '$timeout', '$window', '$uibModalInstance',
				'messageService', 'fileService', msgCreateCtrl ]);
/** *****************forward gfwang********************* */
function msgForwardCtrl($scope, $rootScope, $timeout, $window,
		$uibModalInstance, messageService, fileService, defaultUser, msgId) {
	$scope.defaultUser = defaultUser;
	$scope.msgId = msgId;
	$scope.close = function() {
		$scope.confirm('Close', 'Exit this page?', 'OK',
				function() {
					$uibModalInstance.dismiss('cancel');
				}, 'NO');
	};
	$scope.userList = [];
	$scope.saveUser = function() {
		messageService.forward($scope.accountIds, msgId).then(
				angular.bind(this, function then() {
					var result = messageService.forwardData;
					if (result.code == 0) {
						$uibModalInstance.close($scope.userList);
					} else {
						$scope.message(result.msg);
					}
				}));
	};
}
angular.module('kindKidsApp').controller(
		'msgForwardCtrl',
		[ '$scope', '$rootScope', '$timeout', '$window', '$uibModalInstance',
				'messageService', 'fileService', 'defaultUser', 'msgId',
				msgForwardCtrl ]);
