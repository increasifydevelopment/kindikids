'use strict';

function msgCtrl($scope,$timeout, $uibModalInstance,msgObj) {
	
	$scope.msgObj=msgObj;

	$scope.init = function() {
		if(isNaN($scope.msgObj.time)){
			$scope.msgObj.time=1500;
		}
		$timeout(function() {
			$scope.close();
		},$scope.msgObj.time);
	};
	
	$scope.close=function(){
		$uibModalInstance.dismiss('cancel');
	};
};

angular.module('kindKidsApp').controller('msgCtrl',	
		[ '$scope','$timeout', '$uibModalInstance', 'msgObj', msgCtrl ]);