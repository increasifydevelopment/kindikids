'use strict';

/**
 * @author abliu
 * @description nqs Controller
 */
function nqsCtrl($scope, chooseNqs,dateStr, saveNqsResult, $rootScope, $timeout,
		$window, $uibModalInstance, nqsService) {
	$scope.newChooseNqsObj = JSON.parse(chooseNqs);

	/**
	 * @author abliu
	 * @description 获取NQS集合
	 */
	$scope.getNqsList = function() {
		// 请求service后台返回message列表数据
		nqsService.getNqsList(dateStr).then(angular.bind(this, function then() {
			var result = nqsService.data;
			if (result.code == 0) {
				$scope.page = result.data;
			} else {
				alert(result.msg);
			}
		}));
	};

	/**
	 * @author abliu
	 * @description 保存新的nqs
	 */
	$scope.saveNqs = function() {
		// 返回选中的NQS给调用者
		saveNqsResult($scope.getData());
		$uibModalInstance.dismiss('cancel');
	};
	
	$scope.getData=function(){
		var array=[];
		$("input[type='checkbox']:checkbox:checked").each(function(){ 
			  var version=$(this).attr("version");
			  if(version){
				  var content=$(this).attr("content");
	              var tag=$(this).attr("tag");
	              var nqs={"version":version,"content":content,"tag":tag};
	              array.push(nqs);
			  }
			 
		});
	   return array;
	};

	$scope.cancel = function() {
		$scope.confirm('Close', 'Exit this page?', 'OK', function() {
			$uibModalInstance.dismiss('cancel');
		}, 'NO');	
	};
}
angular.module('kindKidsApp').controller(
		'nqsCtrl',
		[ '$scope', 'chooseNqs', 'dateStr','saveNqsResult', '$rootScope', '$timeout',
				'$window', '$uibModalInstance', 'nqsService', nqsCtrl ]);