'use strict';

function logsCtrl($scope,$rootScope, logsService) {

	$scope.page = {};
	$scope.page.condition = {
		pageIndex: 1,
		sourceId: "",
		maxSize:7
	};

	$scope.$watch('logSourceInfo.sourceId', function(newValue) {						
		$scope.getLogsList();
	});
	/**
	 * @author abliu
	 * @description
	 * @returns 获取 logsList
	 */
	$scope.getLogsList = function(userId) {
		if($scope.logSourceInfo){
			$scope.page.condition.sourceId=$scope.logSourceInfo.sourceId;
		}
		if(userId){
			$scope.page.condition.sourceId=userId;
		}
		if($scope.page.condition.sourceId=="") return;
		logsService.getLogsList($scope.page.condition).then(angular.bind(this, function then() {
			var result = logsService.listResult;
			if (result.code == 0) {
				$scope.page = result.data;
				$scope.page.condition.maxSize=7;
			} else {
				$rootScope.message(result.msg);
			}
		}));
	}

	$scope.changeLogsPage=function(index){
		$scope.page.condition.pageIndex=index;
		$scope.getLogsList();
	}
};

angular.module('kindKidsApp').controller('logsCtrl', ['$scope','$rootScope', 'logsService', logsCtrl]);