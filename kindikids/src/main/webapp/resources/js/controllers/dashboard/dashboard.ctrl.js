'use strict';

/**
 * @author gfwang
 * @description dashboard
 */
function dashboardCtrl($scope, $rootScope, $timeout, $location, $uibModal, dashboardService, $stateParams) {
	$rootScope.subScope = $scope;
	$scope.page = {};
	$scope.condition = {
		startDate : null,
		endDate : null,
		ids : [],
		chooseOrgs : []
	};
	$scope.chooseOrgs = [];
	$scope.dashboardData = {};
	$scope.getAll = function() {
		$scope.getNqsList();
		$scope.getTaskCategories();
		$scope.getQipList();
		$scope.getTaskProgressList();
		$scope.getActivitiesList();
	};
	$scope.getNqsList = function() {
		$scope.showLoading("#nqsList");
		$scope.condition.chooseOrgs = $scope.chooseOrgs;
		dashboardService.getNqsList($scope.condition).then(angular.bind(this, function then() {
			var result = dashboardService.nqsListResult;
			if (result.code == 0) {
				var data = result.data;
				$scope.dashboardData.nqsList = data.nqsList;
				$scope.dashboardData.nqsMaxCount = data.nqsMaxCount;
			} 
			$scope.hideLoading("#nqsList");
		}));
	};

	$scope.getTaskCategories = function() {
		$scope.showLoading("#categoriesList");
		$scope.condition.chooseOrgs = $scope.chooseOrgs;
		dashboardService.getTaskCategories($scope.condition).then(angular.bind(this, function then() {
			var result = dashboardService.taskCategoriesResult;
			if (result.code == 0) {
				var data = result.data;
				$scope.dashboardData.taskCategories = data.taskCategories;
				$scope.dashboardData.tcMaxCount = data.tcMaxCount;
			}
			$scope.hideLoading("#categoriesList");
		}));
	};

	$scope.getQipList = function() {
		$scope.showLoading("#qipList");
		$scope.condition.chooseOrgs = $scope.chooseOrgs;
		dashboardService.getQipList($scope.condition).then(angular.bind(this, function then() {
			var result = dashboardService.qipListResult;
			if (result.code == 0) {
				var data = result.data;
				$scope.condition.startDate = data.condition.startDate;
				$scope.condition.endDate = data.condition.endDate;
				$scope.dashboardData.qipList = data.qipList;
				$scope.dashboardData.qipMaxCount = data.qipMaxCount;
			} else {
				$scope.message(result.msg);
			}
			$scope.hideLoading("#qipList");
		}));
	};

	$scope.getTaskProgressList = function() {
		$scope.showLoading("#progressList");
		$scope.condition.chooseOrgs = $scope.chooseOrgs;
		dashboardService.getTaskProgressList($scope.condition).then(angular.bind(this, function then() {
			var result = dashboardService.taskProgressListResult;
			if (result.code == 0) {
				var data = result.data;
				$scope.dashboardData.taskProgress = data.taskProgress;
				$scope.dashboardData.tpMaxCount = data.tpMaxCount;
			} 
			$scope.hideLoading("#progressList");
		}));
	};

	$scope.getActivitiesList = function() {
		$scope.showLoading("#activeList");
		$scope.condition.chooseOrgs = $scope.chooseOrgs;
		dashboardService.getActivitiesList($scope.condition).then(angular.bind(this, function then() {
			var result = dashboardService.activitiesListResult;
			if (result.code == 0) {
				var data = result.data;
				$scope.dashboardData.activityList = data.activityList;
				$scope.dashboardData.acMaxCount = data.acMaxCount;
			}
			$scope.hideLoading("#activeList");
		}));
	};

	$scope.clearDate = function(flag) {
		if (flag) {
			$scope.condition.startDate = null;
			$scope.condition.endDate = null;
		}
		$scope.validateDate();
	};

	/**
	 * 验证时间是否大于当前时间
	 */
	$scope.validateDate = function() {
		var baseFormater = 'YYYY-MM-DDT00:00:00.000';
		var isAfter_start = new moment($scope.condition.startDate, baseFormater).isAfter(new moment($scope.currentUser.currtenDate, 'DD/MM/YYYY'));
		var isAfter_end = new moment($scope.condition.endDate, baseFormater).isAfter(new moment($scope.currentUser.currtenDate, 'DD/MM/YYYY'));
		if (isAfter_start) {
			$scope.message('startTime cannot more than today !');
			$scope.condition.startDate = moment().format(baseFormater);
			$scope.condition.endDate = moment().format(baseFormater);
			return;
		}
		if (isAfter_end) {
			$scope.message('endTime cannot more than today !');
			$scope.condition.endDate = moment().format(baseFormater);
		}
	};
}
angular.module('kindKidsApp').controller('dashboardCtrl',
		[ '$scope', '$rootScope', '$timeout', '$location', '$uibModal', 'dashboardService', '$stateParams', dashboardCtrl ]);