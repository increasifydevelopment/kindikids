'use strict';

/**
 * @author gfwang
 * @description taskSettingCtrl
 */
function meetingManagerCtrl($scope, $rootScope, $filter, $timeout, $location, $uibModal, meetingService, $stateParams) {

	// 页面list参数对象
	$scope.condition = {
		maxSize: 7,
		pageIndex: 1,
		startDate: null,
		endDate: null,
		name: null
		// 获取页面信息时，后台获取
		/*
		 * totalItems:30, pageSize:2
		 */
	}

	// 存放当前日期
	$scope.taskDate = null;
	$scope.page = {};
	$scope.page.condition = {
		time: null,
		roomId: null,
		centersId: null,
		type: 2,// type 0:传日期进行搜索，1：上一周 2：本周 3：下一周 4: 左侧菜单，保留weekdate查询
		weekDate: null,
		listType: 1,
		programType: '',
		programStatus: ''
	};
	$scope.isCenterManager = $filter('haveRole')($scope.currentUser.roleInfoVoList, '1;5');

	// 获取页面日历
	$scope.getPageCalendar = function () {
		if ($scope.isCenterManager) {
			$scope.page.condition.centersId = $scope.currentUser.userInfo.centersId;
		}
		meetingService.getCalendarList($scope.page.condition).then(angular.bind(this, function then() {
			var result = meetingService.calendarResult;
			if (result.code == 0) {
				$scope.page = result.data;
				// 重新请求日历时，更新taskDate/循环出来哪天是today
				for (var i = 0; i < result.data.weekDays.length; i++) {
					if (result.data.weekDays[i].today == true) {
						$scope.taskDate = result.data.weekDays[i].date;
						break;
					}
				}
				// 更新页面数据
				$scope.getPageInfo();
			} else {
				$.messager.popup(result.msg);
			}
		}));
	};

	$scope.enterSearch = function (ev) {
		if (ev.keyCode == 13) {
			$scope.condition.pageIndex = 1;
			$scope.condition.search = true;
			$scope.getPageInfo();
		}
	};
	$scope.search = function () {
		$scope.condition.pageIndex = 1;
		$scope.getPageInfo();
	};

	// 根据页面日历对应的program list
	$scope.getPageInfo = function (clearDate) {
		// 重置
		$scope.surpassListFlag = false;
		// 大于当前日期不请求list数据2016-09-29T19:49:53.000
		var nowDate = new Date($scope.page.condition.now.substr(0, 10));
		var nowMonth = nowDate.getMonth() >= 9 ? nowDate.getMonth() + 1 : '0' + (nowDate.getMonth() + 1);
		var nowDay = nowDate.getDate() > 9 ? nowDate.getDate() : '0' + nowDate.getDate();
		var inNowDate = nowDate.getFullYear() + "-" + nowMonth + "-" + nowDay;
		var indate = new Date(inNowDate);
		var nowTaskDate = $scope.taskDate.substr(0, 10);
		var intaskdate = new Date(nowTaskDate);
		// 当前日期大于等于program list 列表日期时，请求后台获取列表,给一个标识，
		// 请求的列表控制不能分享，系统定时任务生成的task为可编辑保存，不可分享newsfeed;包括：explore / grow /
		// school readiness / follow up
		// 如果有其他task，则可只展示
		// 隐藏create task 按钮中的其他task，禁止创建，除了other task 可以创建
		if (indate < intaskdate) {
			$scope.surpassListFlag = true;
		}
		var json = {
			statu: $scope.page.condition.programStatus ? $scope.page.condition.programStatus : null,
			centreId: $scope.page.condition.centersId ? $scope.page.condition.centersId : null,
			roomId: $scope.page.condition.roomId ? $scope.page.condition.roomId : null,
			type: $scope.page.condition.programType ? $scope.page.condition.programType : null,
			day: $scope.taskDate,
			//	pageSize : 15,
			pageIndex: $scope.condition.pageIndex,
			name: $scope.condition.name,
			startDate: $scope.condition.startDate,
			endDate: $scope.condition.endDate
		};
		// 获取页面LIST
		meetingService.getManagerList(json).then(angular.bind(this, function then() {
			var result = meetingService.managerListResult;
			if (result.code == 0) {
				$scope.pageList = result.data;
				// 分页设置
				$scope.condition.totalItems = result.data.condition.totalSize;
				$scope.condition.pageSize = result.data.condition.pageSize;
				if ($scope.condition.search && $scope.pageList.list.length == 0) {
					$scope.confirm('Message', 'No results match your search.', 'OK', function () {
						$("#privacy-modal").modal('hide');
					});
				}
				$scope.condition.search = false;
			} else {
				$.messager.popup(result.msg);
			}
		}));
	};

	// 分页变化
	$scope.changeLeavePage = function () {
		$scope.getPageInfo();
	};


	// 设置年范围 设置年份集合，从2010年开始到当前年加1年
	$scope.getYearList = function () {
		$scope.yearList = [];
		var yyyy = new Date().getFullYear();
		var m = yyyy - 2015;
		for (var i = 0; i <= m + 1; i++) {
			$scope.yearList.push(2015 + i);
		}
	};
	// 设置年份，调用方法
	$scope.setTimeY = function (yyyy) {
		if ($scope.page.condition.time != null) {
			var times = $scope.page.condition.time.split("/");
			if (times.length > 1) {
				$scope.page.condition.time = yyyy + "/" + times[1];
				$scope.page.condition.type = 0;
				$scope.condition.pageIndex = 1;
				$scope.getPageCalendar();
			}
		}
	};

	// 设置月份调用方法
	$scope.setTimeM = function (mm) {
		if ($scope.page.condition.time != null) {
			var times = $scope.page.condition.time.split("/");
			if (times.length > 1) {
				$scope.page.condition.time = times[0] + "/" + mm;
				$scope.page.condition.type = 0;
				$scope.condition.pageIndex = 1;
				$scope.getPageCalendar();
			}
		}
	};
	// 设置type //type 0:传日期进行搜索，1：上一周 2：本周 3：下一周 4: 左侧菜单，保留weekdate查询
	$scope.setType = function (type) {
		$scope.condition.search = true;
		$scope.page.condition.type = type;
		$scope.condition.pageIndex = 1;
		$scope.getPageCalendar();
	};

	$scope.toThisDay = function (inDate) {
		// 更新页面红点所在日期样式
		for (var i = 0; i < $scope.page.weekDays.length; i++) {
			$scope.page.weekDays[i].today = false;
		}
		var arr = inDate.date.split('-');
		$scope.page.condition.time = arr[0] + '/' + arr[1];
		inDate.today = true;
		// 更新页面list数据
		var p = angular.copy(inDate);
		$scope.taskDate = p.date;
		$scope.condition.pageIndex = 1;
		$scope.getPageInfo();
	};

	$scope.initTaskView = function () {

		/*
		 * commonService.getMenuOrgs(5).then(angular.bind(this, function then() {
		 * $scope.orgs = commonService.orgList; }));
		 */

		$scope.currUser = {
			roleValue: 0,
			centersId: ''
		}
		$scope.getPageCalendar();
		$scope.getYearList();
	}

	// 搜索条件过滤器
	$scope.locationFilter = function (item) {
		if ($scope.currUser.roleValue == 0) {
			return true;
		}
		if ($scope.currUser.centerId == null) {
			return false;
		}
		if ($scope.currUser.roleValue == 1 || $scope.currUser.roleValue == 5) {
			if (item.id == $scope.currUser.centerId) {
				return true;
			}
			return false;
		}
		return false;
	}


	/** *****************自定义表单填写 gfwang*************** */
	$scope.openTaskModal = function (myTaskId, taskName) {
		$uibModal.open({
			templateUrl: 'views/program/task.form.html?res_v=' + $scope.res_v,
			controller: 'taskFormCtrl',
			backdrop: 'static',
			keyboard: false,
			windowClass: 'modal-grey task-popup',
			animation: true,
			size: "lg",
			resolve: {
				myTaskId: function () {
					return myTaskId;
				},
				taskName: function () {
					return taskName;
				}
			}

		}).result.then(function (instanceId) {
			console.log(instanceId);
		});
	};
	/** *****************自定义表单填写 gfwang*************** */

	/**
	 * ********************************************************模態框
	 * start********************************************
	 */

	$scope.initModalData = function (callback, meetingId, templateId) {
		meetingService.getSelectList().then(angular.bind(this, function then() {
			var result = meetingService.selectListResult;
			if (result.code == 0) {
				callback(result.data, meetingId, templateId);
			} else {
				$scope.message(result.msg);
			}
		}));
	};

	$scope.createMeeting = function (data) {
		data.meetingDate = $scope.taskDate;
		$uibModal.open({
			templateUrl: './views/meeting/modal/meeting.manager.add.html?res_v=' + $scope.res_v,
			controller: 'meetManagerEditCtrl',
			backdrop: 'static',
			keyboard: false,
			windowClass: 'modal-grey task-popup',
			animation: true,
			size: "md",
			resolve: {
				obj: function () {
					return data;
				}
			}
		}).result.then(function (flag) {
			if (flag) {
				$scope.getPageInfo();
			}
		});
	};
	$scope.editMeeting = function (data, meetingId, templateId) {
		data.meetingId = meetingId;
		data.templateId = templateId;
		$uibModal.open({
			templateUrl: './views/meeting/modal/meeting.manager.edit.html?res_v=' + $scope.res_v,
			controller: 'meetManagerEditCtrl',
			backdrop: 'static',
			keyboard: false,
			windowClass: 'modal-grey task-popup',
			animation: true,
			size: "lg",
			resolve: {
				obj: function () {
					return data;
				}
			}

		}).result.then(function (flag) {
			if (flag) {
				$scope.getPageInfo();
			}
		});
	};
}
angular.module('kindKidsApp').controller('meetingManagerCtrl',
	['$scope', '$rootScope', '$filter', '$timeout', '$location', '$uibModal', 'meetingService', '$stateParams', meetingManagerCtrl]);