'use strict';

/**
 * @author gfwang
 * @description taskSettingCtrl
 */
function meetingTemplateCtrl($scope, $rootScope, $timeout, $location, $uibModal, meetingService, $stateParams) {
	function initTempleModel() {
		$scope.meeting = {
			meetingTemplateInfo : {},
			frequency : {},
			nqsList : [],
			columnList : [ {
				columnType : 0,
				columnName : 'Action Item'
			}, {
				columnType : 1,
				columnName : 'Responsibility'
			}, {
				columnType : 2,
				columnName : 'Timeframe'
			}, {
				columnType : 3,
				columnName : 'Status'
			} ]
		};
	}
	initTempleModel();
	/** **************************************************列表start********************************************** */

	// 初始化task列表
	$scope.initTemplateList = function() {
		// 页面list参数对象
		$scope.condition = {
			listType : 1,
			maxSize : 7,
			pageIndex : 1,
			//pageSize : 12,
			meetingTempName : ''
		};
		$scope.getTemplateList();
	};
	$scope.enterSearch = function(ev) {
		if (ev.keyCode == 13) {
			$scope.condition.pageIndex = 1;
			$scope.getTemplateList();
		}
	};

	$scope.getTemplateList = function() {
		$scope.showLoading("#templateList");
		// 页面list数据
		meetingService.getTemplateList($scope.condition).then(angular.bind(this, function then() {
			var result = meetingService.templateListResult;
			$scope.templateList = result.data.list;
			$scope.condition.totalItems = result.data.condition.totalSize;
			$scope.condition.pageSize = result.data.condition.pageSize;
			$scope.hideLoading("#templateList");
		}));
	};
	$scope.changeTemplateList = function(pageIndex) {
		$scope.condition.pageIndex = pageIndex;
		$scope.getTemplateList();
	};
	/** **************************************************列表end********************************************** */
	/**
	 * 保存 gfwang
	 */
	$scope.save = function() {
		var flag = false;
		$('#templateForm').data('bootstrapValidator').validate();
		$("small").map(function() {
			if ($(this).attr('data-bv-result') == "INVALID") {
				flag = true;
			}
		});
		if (flag) {
			return;
		}
		$scope.showLoading("#myTemplate");
		$scope.meeting.nqsList = $scope.chooseNqs;
		// 页面list数据
		meetingService.saveTemplate($scope.meeting).then(angular.bind(this, function then() {
			var result = meetingService.saveTempalteResult;
			if (result.code == 0) {
				$scope.initModel();
				$scope.getTemplateList();
			}
			$scope.message(result.msg);
			$scope.hideLoading("#myTemplate");
		}));
	};

	$scope.initModel = function(id) {
		if (!id) {
			$('.task-list div').removeClass('active');
			initTempleModel();
			$scope.chooseNqs = [];
			$scope.addData=null;
			return;
		}
		meetingService.getTemplateInfo(id).then(angular.bind(this, function then() {
			var result = meetingService.getTempResult;
			if (result.code == 0) {
				$scope.meeting = result.data;
				$scope.chooseNqs = !result.data.nqsList ? [] : result.data.nqsList;
				$scope.addData=null;
			} else {
				$scope.message(result.msg);
			}
		}));
	};
	$scope.enterAddColumn = function(ev){
		if (ev.keyCode == 13){
			$scope.addColumn();
		}
	};
	$scope.addColumn = function() {
		if (!$scope.addData) {
			$scope.message('A column name is required for ALL columns');
			return;
		}
		if ($scope.addData.length > 255) {
			$scope.addData='';
			$scope.message('Add Column Name must be less than 255 characters long');
			return;
		}
		if(haveRepeat()){
			$scope.message($scope.addData+' already exists');
			return;
		}
		$scope.meeting.columnList = !$scope.meeting.columnList ? [] : $scope.meeting.columnList;
		$scope.meeting.columnList.push({
			columnName : $scope.addData
		});
		$scope.addData = '';
	};
	function haveRepeat(){
		for(var i=0;i<$scope.meeting.columnList.length;i++){
			var tip1=$scope.meeting.columnList[i].columnName;
			if($scope.addData===tip1){
				return true;
			}
		}
		return false;
	}
	
	$scope.removeColumn = function(index) {
		$scope.meeting.columnList.splice(index, 1);
	};

	$scope.repeatDone = function() {
		var form = $('#templateForm').bootstrapValidator();
		form.bootstrapValidator('addField', 'columnName', {
			validators : {
				notEmpty : {
					message : "The Column Name is required"
				},
				stringLength : {
					max : 255,
					message : "The Column Name must be less than 255 characters long"
				}
			}
		});
	};

	/**
	 * 禁用或者启用 gfwang
	 */
	$scope.enableOrDisable = function(state) {
		// 页面list数据
		console.log(state);
		meetingService.changeTemplateState($scope.meeting.meetingTemplateInfo.id, state).then(angular.bind(this, function then() {
			var result = meetingService.tempStaeResult;
			if (result.code == 0) {
				$scope.meeting.meetingTemplateInfo.state = state;
			} else {
				$scope.message(result.msg);
			}
		}));
	};

	$scope.chooseNqs = [];

	/**
	 * @author gfwang
	 * @description 获取message列表
	 */
	$scope.saveNqsResult = function(newNqs) {
		$scope.chooseNqs = newNqs;
	};

	/**
	 * 打开频率模态框 gfwang
	 */
	$scope.openFrequency = function() {
		var state = !$scope.meeting.meetingTemplateInfo ? null : $scope.meeting.meetingTemplateInfo.state;
		var frequencyInfo = !$scope.meeting.frequency ? {} : $scope.meeting.frequency;
		var Obj = {
			frequencyInfo : frequencyInfo,
			state : state,
			callBackSynchrFrequency : $scope.callBackSynchrFrequency
		};
		Obj = angular.copy(Obj);
		Obj.currtenDate = $scope.currentUser.currtenDate;
		$uibModal.open({
			templateUrl : './views/meeting/modal/template.frequency.html?res_v='+$scope.res_v,
			controller : 'meetingfrequencyCtrl',
			backdrop : 'static',
			keyboard : false,
			resolve : {
				Obj : function() {
					return Obj;
				}
			}
		});
	};
	/**
	 * 同步Frequency回调 gfwang
	 */
	$scope.callBackSynchrFrequency = function(frequencyInfo) {
		$scope.meeting.frequency = frequencyInfo;
	};

}
angular.module('kindKidsApp').controller('meetingTemplateCtrl',
		[ '$scope', '$rootScope', '$timeout', '$location', '$uibModal', 'meetingService', '$stateParams', meetingTemplateCtrl ]);