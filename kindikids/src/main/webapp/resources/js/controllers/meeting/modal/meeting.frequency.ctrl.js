'use strict';

/**
 * @author gfwang
 * @description meetingfrequencyCtrl
 */
function meetingfrequencyCtrl($scope, $rootScope, $timeout, $location, $uibModalInstance, $filter, Obj, meetingService, $stateParams) {
	$scope.frequencyInfo = Obj.frequencyInfo;
	$scope.state = (Obj.state!=null && Obj.state == 0) ? true : false;

	$scope.close = function() {
		$scope.confirm('Close', 'Exit this page?', 'OK', function() {
		$uibModalInstance.dismiss('cancel');}
		,'NO');
	};

	$scope.initFrequency = function() {
		$scope.frequencyInfo.repeats = !$scope.frequencyInfo.repeats ? '1' : $scope.frequencyInfo.repeats+'';
		$scope.frequencyInfo.haveEndDate = !$scope.frequencyInfo.haveEndDate ? 0 : $scope.frequencyInfo.haveEndDate;
	};

	$scope.repeatChange = function() {
		$scope.frequencyInfo.startsOnDate = null;
		$scope.frequencyInfo.haveEndDate = 0;
		$scope.frequencyInfo.endOnDate = null;
		$scope.frequencyInfo.weekdayStart = null;
		$scope.frequencyInfo.weekdayDue = null;
	};
	$scope.save = function() {
		var validateResult = myvailidate();
		if (validateResult) {
			$scope.message(validateResult, 1000);
			return;
		}
		Obj.callBackSynchrFrequency($scope.frequencyInfo);
		$uibModalInstance.dismiss('cancel');
	};
	function myvailidate() {
		var type = $scope.frequencyInfo.repeats;
		if ($filter('haveRepeats')(type, '6')) {
			return '';
		}
		if ($filter('haveRepeats')(type, '1,2,4')) {
			if (!$scope.frequencyInfo.startsOnDate) {
				return 'Please enter a date for the "Starts on" field';
			}
			//开始时间必须大于等于今天
			if (dateComp($scope.frequencyInfo.startsOnDate)==-1) {
				return 'The \'Starts on\' date cannot be in the past';
			}
			if ($scope.frequencyInfo.haveEndDate * 1 == 1 && !$scope.frequencyInfo.endOnDate) {
				return 'Please enter a date for the "Ends on" field';
			}
			if ($scope.frequencyInfo.haveEndDate * 1 == 1 && dateComp($scope.frequencyInfo.endOnDate, $scope.frequencyInfo.startsOnDate) == -1) {
				return 'The \'Ends on\' date must be later than the \'Starts on\' date';
			}

			// 验证下面
			var dayStartName = '';
			var dayEndName = '';

			if ($filter('haveRepeats')(type, '1')) {
				dayStartName = 'Weekday to Start';
				dayEndName = 'Weekday to Due';
			}
			if ($filter('haveRepeats')(type, '2')) {
				dayStartName = 'Day to Start';
				dayEndName = 'Day to End';
			}
			if ($filter('haveRepeats')(type, '4')) {
				dayStartName = 'Day to Start';
				dayEndName = 'Day to Due';
			}

			if (!$scope.frequencyInfo.weekdayStart) {
				return dayStartName + ' is required';
			}
		/*	if (!$scope.frequencyInfo.weekdayDue) {
				return dayEndName + ' is required';
			}
			if ($filter('haveRepeats')(type, '1,2')) {
				if ($scope.frequencyInfo.weekdayStart * 1 > $scope.frequencyInfo.weekdayDue * 1) {
					return dayEndName + ' must be greater than or equal to the ' + dayStartName;
				}
			} else if ($filter('haveRepeats')(type, '4')) {
				if (dateComp($scope.frequencyInfo.weekdayDue, $scope.frequencyInfo.weekdayStart, 'DD/MM') == -1) {
					return dayEndName + ' must be greater than or equal to the ' + dayStartName;
				}
			} else {
				if (dateComp($scope.frequencyInfo.weekdayDue, $scope.frequencyInfo.weekdayStart) == -1) {
					return dayEndName + ' must be greater than or equal to the ' + dayStartName;
				}
			}*/
		}
	}
	function dateComp(bigDate, smallDate, formatter) {
		if (!formatter) {
			formatter = 'DD/MM/YYYY';
		}
		var choose = new moment(bigDate, formatter)._d;
		var today = null;
		if (smallDate == null) {
			//smallDate = new moment().format(formatter);
			today = new moment(Obj.currtenDate, formatter)._d;
		} else {
			today = new moment(smallDate, formatter)._d;
		}
		if (choose >= today) {
			return 1;
		} else if (choose == today) {
			return 0;
		} else {
			return -1;
		}
	}
}
angular.module('kindKidsApp').controller(
		'meetingfrequencyCtrl',
		[ '$scope', '$rootScope', '$timeout', '$location', '$uibModalInstance', '$filter', 'Obj', 'meetingService', '$stateParams',
				meetingfrequencyCtrl ]);