'use strict';

/**
 * @author gfwang
 * @description taskSettingCtrl
 */
function meetingAgendaTypeCtrl($scope, $rootScope, $timeout, $location, $uibModalInstance, obj,meetingService, $stateParams) {
	$scope.list = obj.list;
	$scope.updateList = function() {
		$("#agendaTypeForm").data('bootstrapValidator').validate();
		if(!$("#agendaTypeForm").data('bootstrapValidator').isValid()){
			return;
		}
		meetingService.updateTypeList($scope.list).then(angular.bind(this, function then() {
			var result = meetingService.updateTypeResult;
			if (result.code == 0) {
				$uibModalInstance.dismiss('cancel');
				obj.reloadTypeFunction();
			} else {
				$scope.message(result.msg);
			}
		}));
	};
	$scope.repeatDone=function(){
		var form = $('#agendaTypeForm').bootstrapValidator();
		form.bootstrapValidator('addField', 'typeName', {
			validators : {
				notEmpty : {
					message : "The Type Name is required"
				},
				stringLength : {
					max : 255,
					message : "The Type Name must be less than 255 characters long"
				}
			}
		});
		/*form.bootstrapValidator('addField', 'typeNameTemp', {
			validators : {
				stringLength : {
					max : 255,
					message : "The Type Name must be less than 255 characters long"
				}
			}
		});*/
	};
	
	$scope.addData='';
	$scope.addType = function() {
		if (!$scope.addData) {
			$scope.message('Add Type Name is required');
			return;
		}
		if ($scope.addData.length > 255) {
			$scope.message('Add Type Name must be less than 255 characters long');
			return;
		}
		if(haveRepeat()){
			$scope.message($scope.addData+' already exists');
			return;
		}
		$scope.list.push({typeName:$scope.addData});
		$scope.addData='';
	};
	$scope.deleteType = function(index) {
		$scope.list.splice(index, 1);
	};
	function haveRepeat(){
		for(var i=0;i<$scope.list.length;i++){
			var tip1=$scope.list[i].typeName;
			if($scope.addData===tip1){
				return true;
			}
		}
		return false;
	}
	$scope.close = function() {
		$scope.confirm('Close', 'Exit this page?', 'OK', function() {
			$uibModalInstance.dismiss('cancel');
		}, 'NO');
	};
}
angular.module('kindKidsApp').controller('meetingAgendaTypeCtrl',
		[ '$scope', '$rootScope', '$timeout', '$location', '$uibModalInstance', 'obj','meetingService', '$stateParams', meetingAgendaTypeCtrl ]);