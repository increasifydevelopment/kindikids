'use strict';

/**
 * @author gfwang
 * @description meetingfrequencyCtrl
 */
function meetingTableEdit($scope, $rootScope, $timeout, $location, $filter, meetingService, $stateParams) {
	$scope.thList = [];

	$scope.table = [];
	$scope.initTable = function(meetingId, templateId, viewFlag, carrierObj,isReadOnly) {
		// 赋值载体方法
		if (carrierObj) {
			carrierObj.getTable = $scope.getTable;
		}
		if(isReadOnly){
			$scope.isReadOnly=isReadOnly;
		}
		$scope.viewFlag = viewFlag;
		meetingService.getTableInfo(meetingId, templateId).then(angular.bind(this, function then() {
			var result = meetingService.tableResult;
			if (result.code == 0) {
				$scope.thList = result.data.colList;
				$scope.table = !result.data.table?[]:result.data.table;
				if ($scope.viewFlag) {
					//创建的时候
					pushEmptyRow();
				} else {
					//编辑的时候
					if (!$scope.table[0].rowId) {
						$scope.table = [];
						pushEmptyRow();
					}
				}
			} else {
				$scope.message(result.msg);
			}
		}));
	};
	// 新增一空行
	function pushEmptyRow() {
		var row = {
			rowList : []
		};
		for (var i = 0; i < $scope.thList.length; i++) {
			var item = $scope.thList[i];
			row.rowList.push({
				columnId : item.id,
				columnType : item.columnType
			});
		}
		$scope.table.push(row);
	}
	// 获取一个表格对象
	function getCell(rowId, columnId, value) {
		return {
			rowId : rowId,
			columnId : columnId,
			value : value
		};
	}
	$scope.getTable = function() {
		return $scope.table;
	};

	// 新增行
	$scope.addRow = function() {
		pushEmptyRow();
	};
	// 移除行
	$scope.removeRow = function(index) {
		$scope.table.splice(index, 1);
	};

}
angular.module('kindKidsApp').controller('meetingTableEdit',
		[ '$scope', '$rootScope', '$timeout', '$location', '$filter', 'meetingService', '$stateParams', meetingTableEdit ]);