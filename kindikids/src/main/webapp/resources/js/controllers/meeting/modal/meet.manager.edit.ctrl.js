'use strict';

/**
 * @author gfwang
 * @description taskSettingCtrl
 */
function meetManagerEditCtrl($scope, $rootScope, $timeout, $location, $uibModalInstance, $uibModal, obj, meetingService, $stateParams) {

	$scope.meetingVo = {};

	// 下拉列表数据
	$scope.agendaList = obj.meetingAgenda;
	$scope.templateList = obj.meetingTemp;

	// 创建
	$scope.createMeeting = function() {
		$scope.showLoading('myAddMeeting');
		$("#meetingaddForm").data('bootstrapValidator').validate();
		if (!$("#meetingaddForm").data('bootstrapValidator').isValid()) {
			return;
		}
		$scope.meetingVo.meetingInfo.createTime = obj.meetingDate;
		meetingService.createMeeting($scope.meetingVo).then(angular.bind(this, function then() {
			var result = meetingService.createMeetingResult;
			$scope.hideLoading('myAddMeeting');
			if (result.code == 0) {
				$uibModalInstance.close(true);
			} else {
				$scope.message(result.msg);
			}
		}));
	};
	// nqs
	$scope.chooseNqs = [];
	$scope.templateId = '';
	// 选择模板时获取NQS数据
	$scope.changeTemplate = function() {
		var templateId = $scope.meetingVo.meetingInfo.templateId;
		if (!templateId) {
			return;
		}
		meetingService.getTemplateInfo(templateId).then(angular.bind(this, function then() {
			var result = meetingService.getTempResult;
			if (result.code == 0) {
				$scope.chooseNqs = !result.data.nqsList ? [] : result.data.nqsList;
			} else {
				$scope.message(result.msg);
			}
		}));
	};

	$scope.viewTemplate = function(id) {
		$uibModal.open({
			templateUrl : './views/meeting/modal/meeting.viewtable.html?res_v='+$scope.res_v,
			controller : 'meetViewTableCtrl',
			backdrop : 'static',
			keyboard : false,
			windowClass : 'modal-grey task-popup',
			animation : true,
			size : "lg",
			resolve : {
				templateId : function() {
					return id;
				},
				getTable : function() {
					return {};
				},
				viewFlag : function() {
					return true;
				}
			}
		}).result.then(function(instanceId) {
			console.log(instanceId);
		});
	};

	$scope.close = function() {
		$scope.confirm('Close', 'Exit this page?', 'OK', function() {
			$uibModalInstance.dismiss('cancel');
		}, 'NO');
	};

	/* ****************************************************Edit*************************************** * */
	$scope.meetingId = obj.meetingId;
	$scope.templateId = obj.templateId;
	$scope.meet = {
		meetingInfo : {}
	};
	// 是否为未来时间
	$scope.isReadOnly = false;
	// 载体对象，获取ng-include中的保存方法
	$scope.carrierObj = {};
	$scope.getMeetingInfo = function() {
		meetingService.getTableInfo($scope.meetingId, $scope.templateId).then(angular.bind(this, function then() {
			var result = meetingService.tableResult;
			if (result.code == 0) {
				$scope.meet = result.data;
				isReadOnlyFunction($scope.meet.meetingInfo.createTime);
				$scope.agendaChange();
				delete $scope.meet.table;
			} else {
				$scope.message(result.msg);
			}
		}));
	};

	function isReadOnlyFunction(date) {
		var formatter = 'DD/MM/YYYY';
		var today = new moment($scope.currentUser.currtenDate, formatter)._d;
		var choose = new moment(date, formatter)._d;
		if (choose > today) {
			$scope.isReadOnly = true;
		} else if (choose == today) {
			$scope.isReadOnly = false;
		} else {
			$scope.isReadOnly = false;
		}
	}
	// 改变议程 修改item
	$scope.agendaChange = function() {
		var agendaId = $scope.meet.meetingInfo.meetingAgendaId;
		if (!agendaId) {
			return;
		}
		meetingService.getAgenadeInfo(agendaId).then(angular.bind(this, function then() {
			var result = meetingService.singleAgeInfoResult;
			if (result.code == 0) {
				$scope.itemList = result.data.agendaItems;
				if (result.data.deleteFlag || result.data.state == 0||result.data.currtenFlag==0) {
					var oldAgenda = {
						id : result.data.id,
						text : result.data.agendaName,
						disableFlag : true
					};
					$scope.agendaList.push(oldAgenda);
				}
			} else {
				$scope.message(result.msg);
			}

		}));
	};

	$scope.update = function() {
		var formTable = $scope.carrierObj.getTable();
		$scope.meet.table = formTable;
		$scope.showLoading('#myEditMeet');
		//delete $scope.meet.nqsList;
		meetingService.updateMeeting($scope.meet, true).then(angular.bind(this, function then() {
			var result = meetingService.updateMeetingResult;
			$scope.hideLoading('#myEditMeet');
			if (result.code == 0) {
				$uibModalInstance.close(true);
			}  else {
				$scope.message(result.msg);
			}
		}));
	};
	$scope.deleteMeeting = function() {
		$scope.confirm('Delete', 'Are you sure?', 'YES', function() {
			meetingService.deleteMeeting($scope.meetingId, $scope.templateId).then(angular.bind(this, function then() {
				var result = meetingService.deleteResult;
				if (result.code == 0) {
					$uibModalInstance.close(true);
				} else {
					$scope.message(result.msg);
				}
			}));
		}, 'NO');
	};

}

angular.module('kindKidsApp').controller(
		'meetManagerEditCtrl',
		[ '$scope', '$rootScope', '$timeout', '$location', '$uibModalInstance', '$uibModal', 'obj', 'meetingService', '$stateParams',
				meetManagerEditCtrl ]);

/* ************************************其它************************************* * */
// 表格
function meetViewTableCtrl($scope, $rootScope, $timeout, $location, $uibModalInstance, templateId, viewFlag, getTable, meetingService, $stateParams) {
	$scope.templateId = templateId;
	$scope.viewFlag = viewFlag;
	$scope.getTable = getTable;

	$scope.close = function() {
		if (viewFlag) {
			$uibModalInstance.dismiss('cancel');
			return;
		}
		$scope.confirm('Close', 'Exit this page?', 'OK', function() {
			$uibModalInstance.dismiss('cancel');
		}, 'NO');
	};
}
angular.module('kindKidsApp').controller(
		'meetViewTableCtrl',
		[ '$scope', '$rootScope', '$timeout', '$location', '$uibModalInstance', 'templateId', 'viewFlag', 'getTable', 'meetingService',
				'$stateParams', meetViewTableCtrl ]);