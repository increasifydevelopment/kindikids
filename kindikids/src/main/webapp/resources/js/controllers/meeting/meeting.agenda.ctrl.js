'use strict';

/**
 * @author gfwang
 * @description meetingAgendaCtrl
 */
function meetingAgendaCtrl($scope, $rootScope, $timeout, $location, $uibModal, meetingService, $stateParams) {

	$scope.meetingAgendaInfo = {
		nqsList : [],
		agendaItems : new Array({})
	};

	/** **************************************************列表start********************************************** */
	// 页面list参数对象
	$scope.condition = {
		listType : 1,
		maxSize : 7,
		pageIndex : 1,
	//	pageSize : 12,
		meetingAgendaName : '',
		typeId : '',
		startTime : null,
		endTime : null
	};
	/**
	 * 初始化task列表
	 */
	$scope.initAgendaList = function() {
		$scope.getAgendaList();
	};
	$scope.enterSearch = function(ev) {
		if (ev.keyCode == 13) {
			$scope.condition.pageIndex = 1;
			$scope.getAgendaList();
		}
	};

	$scope.getAgendaList = function(clearDate) {
		$scope.showLoading("#agendaList");
		if (clearDate) {
			$scope.condition.startTime = null;
			$scope.condition.endTime = null;
		}
		// 页面list数据
		meetingService.getAgeList($scope.condition).then(angular.bind(this, function then() {
			var result = meetingService.ageListResult;
			$scope.agendaList = result.data.list;
			$scope.condition.totalItems = result.data.condition.totalSize;
			$scope.condition.pageSize = result.data.condition.pageSize;
			$scope.hideLoading("#agendaList");
		}));
	};
	$scope.changeAgendaList = function(pageIndex) {
		$scope.condition.pageIndex = pageIndex;
		$scope.getAgendaList();
	};
	/** **************************************************列表end********************************************** */

	/** *****************************************nqs***************************** */
	$scope.chooseNqs = [];

	/**
	 * @author gfwang
	 * @description 保存Nqs
	 */
	$scope.saveNqsResult = function(newNqs) {
		$scope.chooseNqs = newNqs;
	};
	/** *****************************************nqs***************************** */

	/**
	 * 保存 gfwang
	 */
	$scope.save = function() {
		var flag = false;
		$('#agendaForm').data('bootstrapValidator').validate();
		if($scope.meetingAgendaInfo.agendaTypeId){}{
			$('#agendaForm').bootstrapValidator('updateStatus', 'agendaType', 'NOT_VALIDATED');	
		}
		$("small").map(function() {
			if ($(this).attr('data-bv-result') == "INVALID") {
				flag = true;
			}
		});
		if (flag) {
			return;
		}
	
	    var result=validateData();
		if(!result.flag){
			$scope.message(result.msg);
			return;
		}
		
		$scope.showLoading("#myAgenda");
		$scope.meetingAgendaInfo.nqsList = $scope.chooseNqs;
		meetingService.saveAgenadeInfo($scope.meetingAgendaInfo).then(angular.bind(this, function then() {
			var result = meetingService.saveAgeResult;
			if (result.code == 0) {
				$scope.initAgeModel();
				$scope.getAgendaList();
			} else {
				$scope.message(result.msg);
			}
			$scope.hideLoading("#myAgenda");
		}));
	};

	function validateData(){
		for (var i = 0; i < $scope.meetingAgendaInfo.agendaItems.length; i++) {
			var data=$scope.meetingAgendaInfo.agendaItems[i];
			if(data.itemTime){
				$scope.meetingAgendaInfo.agendaItems[i].itemTime=data.itemTime*1;
			}else{
				data.itemTime=null;
			}
			if(data.itemTime&&(data.itemTime+'').length>6){
				return {flag:false,msg:"Agenda time is too long"};
			}
		}
		return {flag:true};;
	}
	
	
	$scope.initAgeModel = function(id) {
		$scope.initMeetingType();
		if (!id) {
			$scope.meetingAgendaInfo = {
				nqsList : [],
				agendaItems : new Array({})
			};
			$scope.chooseNqs = [];
			$('.task-list div').removeClass('active');
			return;
		}
		$scope.showLoading("#myAgenda");
		meetingService.getAgenadeInfo(id).then(angular.bind(this, function then() {
			var result = meetingService.singleAgeInfoResult;
			if (result.code == 0) {
				$scope.meetingAgendaInfo = result.data;
				var typeInfo = result.data.typeInfo;
				if (typeInfo.deleteFlag == 1) {
					$scope.typeList.push(typeInfo);
				}

				$scope.chooseNqs = !result.data.nqsList ? [] : result.data.nqsList;
			} else {
				$scope.message(result.msg);
			}
			$scope.hideLoading("#myAgenda");
		}));
	};

	/**
	 * 禁用或者启用 gfwang
	 */
	$scope.enableOrDisable = function(state) {
		// 页面list数据
		meetingService.changeAgendeState($scope.meetingAgendaInfo.id, state).then(angular.bind(this, function then() {
			var result = meetingService.ageStateResult;
			if (result.code == 0) {
				$scope.meetingAgendaInfo.state = state;
			} else {
				$scope.message(result.msg);
			}
		}));
	};

	$scope.removeItem = function(index) {
		$scope.meetingAgendaInfo.agendaItems.splice(index, 1);
	};

	$scope.addItem = function() {
		$scope.meetingAgendaInfo.agendaItems.push({});
	};

	// **********************************模态框 start*******************
	// 左侧菜单
	$scope.initMeetingType = function() {
		meetingService.getTypeList().then(angular.bind(this, function then() {
			var result = meetingService.typeList;
			if (result.code == 0) {
				$scope.typeList = result.data;
			} else {
				$scope.message(result.msg);
			}
		}));
	};

	$scope.openType = function() {
		var obj = {
			list : !$scope.typeList ? [] : angular.copy($scope.typeList),
			reloadTypeFunction : $scope.initMeetingType
		};
		for (var i = obj.list.length-1; i >= 0; i--) {
			if (obj.list[i].deleteFlag == 1) {
				obj.list.splice(i, 1);
			}
		}
		$uibModal.open({
			templateUrl : './views/meeting/modal/agenda.type.html?res_v='+$scope.res_v,
			controller : 'meetingAgendaTypeCtrl',
			backdrop : 'static',
			keyboard : false,
			resolve : {
				obj : function() {
					return obj;
				}
			}
		});
	};

	// **********************************模态框 end**************************

}
angular.module('kindKidsApp').controller('meetingAgendaCtrl',
		[ '$scope', '$rootScope', '$timeout', '$location', '$uibModal', 'meetingService', '$stateParams', meetingAgendaCtrl ]);