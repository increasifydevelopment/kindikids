'use strict';

/**
 * @author jfzhao
 * @description Form view controller
 */
function viewFormCtrl($scope, $filter, formService) {

	$scope.addFiles = [];
	$scope.removeFiles = [];

	var noLabel = new Array(10, 11, 12, 13, 14, 15, 9, 7);

	$scope.isdisable = false;
	$scope.initForm = function(code, instanceId, childSave, isdisable) {
		$scope.isdisable = isdisable;
		$scope.code = code;
		$scope.instanceId = instanceId;
		childSave.initForm = $scope.initForm;
		if (!code && !instanceId) {
			return;
		}
		childSave.getJson = $scope.getJson;
		childSave.saveSuccess = $scope.saveSuccess;
		formService.initForm($scope.code, $scope.instanceId).then(angular.bind(this, function then() {
			$scope.showLoading(".myTaskForm");
			$scope.formInfo = formService.initFormResult;
			if ($scope.formInfo.instanceId == null) {
				$scope.valueInfo = {
					valueStringInfo : [],
					valueTableInfo : [],
					valueTextInfo : []
				};
				$scope.pushValue();
				$scope.hideLoading(".myTaskForm");
			} else {
				formService.getInstance($scope.instanceId).then(angular.bind(this, function then() {
					$scope.valueInfo = formService.instanceResult;
					$scope.pushValue();
					$scope.hideLoading(".myTaskForm");
				}));
			}
		}));
	};

	$scope.close = function() {
		$uibModalInstance.dismiss('cancel');
	};

	$scope.pushValue = function() {
		for (var i = 0; i < $scope.formInfo.attributes.length; i++) {
			if (noLabel.indexOf($scope.formInfo.attributes[i].type) != -1) {
				$scope.formInfo.attributes[i].hasLabel = false;
			} else {
				$scope.formInfo.attributes[i].hasLabel = true;
			}
			if ($scope.formInfo.attributes[i].type >= 1 && $scope.formInfo.attributes[i].type <= 6 && $scope.formInfo.attributes[i].type != 5
					&& $scope.formInfo.attributes[i].type != 2) {
				var result = $filter('filter')($scope.valueInfo.valueStringInfo, {
					formAttrId : $scope.formInfo.attributes[i].attrId
				}, true);
				if (result.length > 0) {
					$scope.formInfo.attributes[i].value = result[0].value;
				}
			}
			if ($scope.formInfo.attributes[i].json != null) {
				if (isString($scope.formInfo.attributes[i].json)) {
					$scope.formInfo.attributes[i].json = JSON.parse($scope.formInfo.attributes[i].json);
				}
			}
			if ($scope.formInfo.attributes[i].type == 6) {
				formService.getFiles($scope.formInfo.attributes[i]);
			}
			if ($scope.formInfo.attributes[i].type == 2) {
				var result = $filter('filter')($scope.valueInfo.valueTextInfo, {
					formAttrId : $scope.formInfo.attributes[i].attrId
				}, true);
				if (result.length > 0) {
					$scope.formInfo.attributes[i].value = result[0].value;
				}
			}
			// optionsType==0 自定义数据源
			if ($scope.formInfo.attributes[i].type == 5 && $scope.formInfo.attributes[i].json.optionsType == 0) {
				var attributes = $scope.formInfo.attributes[i];
				var selectvalue = $scope.valueInfo.valueSelectInfo;
				var ismultiple = $scope.formInfo.attributes[i].json.multiple;
				pushSelectValue(attributes, selectvalue, ismultiple, $scope.formInfo.attributes[i].json.optionsType);
			}
			if ($scope.formInfo.attributes[i].type == 5 && $scope.formInfo.attributes[i].json.optionsType == 1) {
				var attributes = $scope.formInfo.attributes[i];
				var selectvalue = $scope.valueInfo.valueSelectInfo;
				var ismultiple = $scope.formInfo.attributes[i].json.multiple;
				pushSelectValue(attributes, selectvalue, ismultiple, $scope.formInfo.attributes[i].json.optionsType);
			}
			if ($scope.formInfo.attributes[i].type == 7) {
				if ($scope.instanceId == '0') {
					if ($scope.formInfo.attributes[i].json.rows == undefined || $scope.formInfo.attributes[i].json.rows == "") {
						$scope.formInfo.attributes[i].instanceAttributes.push({
							formAttrId : $scope.formInfo.attributes[i].attrId,
							type : 1,
							value : 0
						});
					} else {
						$scope.formInfo.attributes[i].instanceAttributes.push({
							formAttrId : $scope.formInfo.attributes[i].attrId,
							type : 1,
							value : $scope.formInfo.attributes[i].json.rows
						});
					}
					if ($scope.formInfo.attributes[i].json.cols == undefined || $scope.formInfo.attributes[i].json.cols == "") {
						$scope.formInfo.attributes[i].instanceAttributes.push({
							formAttrId : $scope.formInfo.attributes[i].attrId,
							type : 2,
							value : 0
						});
					} else {
						$scope.formInfo.attributes[i].instanceAttributes.push({
							formAttrId : $scope.formInfo.attributes[i].attrId,
							type : 2,
							value : $scope.formInfo.attributes[i].json.cols
						});
					}
					for (var j = 0; j < $scope.formInfo.attributes[i].json.value.length; j++) {
						$scope.formInfo.attributes[i].json.value[j].inputFlag = false;
						$scope.formInfo.attributes[i].json.value[j].formAttrId = $scope.formInfo.attributes[i].attrId;
						$scope.formInfo.attributes[i].json.value[j].row = $scope.formInfo.attributes[i].json.value[j].row * 1;
						$scope.formInfo.attributes[i].json.value[j].col = $scope.formInfo.attributes[i].json.value[j].col * 1;
						$scope.valueInfo.valueTableInfo.push($scope.formInfo.attributes[i].json.value[j]);
					}
				}
				$scope.formInfo.attributes[i].json.rows = $filter('filter')($scope.formInfo.attributes[i].instanceAttributes, {
					formAttrId : $scope.formInfo.attributes[i].attrId,
					type : 1
				}, true)[0].value;
				$scope.formInfo.attributes[i].json.cols = $filter('filter')($scope.formInfo.attributes[i].instanceAttributes, {
					formAttrId : $scope.formInfo.attributes[i].attrId,
					type : 2
				}, true)[0].value;
				$scope.formInfo.attributes[i].calcValue = new Array();
				for (var row = 0; row < $scope.formInfo.attributes[i].json.rows; row++) {
					var rows = new Array();
					for (var col = 0; col < $scope.formInfo.attributes[i].json.cols; col++) {
						var t = $filter('filter')($scope.valueInfo.valueTableInfo, {
							formAttrId : $scope.formInfo.attributes[i].attrId,
							col : col,
							row : row
						}, true);
						if (t.length > 0) {
							rows.push(t[0]);
							continue;
						}
						rows.push({
							"col" : col,
							"row" : row,
							inputFlag : true,
							formAttrId : $scope.formInfo.attributes[i].attrId
						});
					}
					$scope.formInfo.attributes[i].calcValue.push(rows);
				}
			}
			if ($scope.formInfo.attributes[i].type == 8) {
				var t = $filter('filter')($scope.valueInfo.valueTextInfo, {
					formAttrId : $scope.formInfo.attributes[i].attrId
				}, true);
				if (t.length > 0) {
					$scope.formInfo.attributes[i].value = t[0].value;
				}
			}
			if ($scope.formInfo.attributes[i].type == 4) {
				if ($scope.formInfo.attributes[i].value != null) {
					if (isString($scope.formInfo.attributes[i].value)) {
						$scope.formInfo.attributes[i].value = JSON.parse($scope.formInfo.attributes[i].value);
					}
				}
			}
		}
	};

	$scope.upload = function(file, attr) {
		if (file == undefined || file.length == 0) {
			return;
		}
		if (attr.json.files == undefined) {
			attr.json.files = [];
		}
		if (file.length > 10) {
			$scope.message("File too much. The maximum file number for files is 10. Please try uploading a little less file.");
			return;
		}
		for (var i = 0; i < file.length; i++) {
			if (file[i].size > 10485760) {
				$scope
						.message("Sorry, the filesize is too large, please reduce the filesize and try again. The maximum file size for files is 10MB. Please try uploading a smaller file.");
				return;
			}
		}
		// 先设置attr.json.file中添加值
		for (var i = 0; i < file.length; i++) {
			var fileObj = {};
			fileObj.fileName = file[i].name;
			fileObj.progress = 0;
			attr.json.files.push(fileObj);
		}

		formService.upload(file, attr.value, callbackUploadFile, attr).then(angular.bind(this, function then() {
			if (formService.fileResult == undefined) {
				// 减去已经添加的附件
				for (var i = 0, m = attr.json.files.length; i < m; i++) {
					if (attr.json.files[i].progress != undefined) {
						attr.json.files.splice(i, 1);
					}
				}
				return;
			}
			if (attr.value == undefined) {
				attr.value = formService.fileResult.sourceId;
			}
			for (var i = 0; i < formService.fileResult.files.length; i++) {
				$scope.addFiles.push(formService.fileResult.files[i].id);
				// 更新目前的文件
				for (var j = 0, m = attr.json.files.length; j < m; j++) {
					if (attr.json.files[j].fileName == formService.fileResult.files[i].fileName && attr.json.files[j].progress != undefined) {
						attr.json.files[j].progress = 100;
						attr.json.files[j] = formService.fileResult.files[i];
					}
				}

			}
		}));
	};

	function callbackUploadFile(attr, progressPercentage) {
		if (progressPercentage == 100) {
			progressPercentage = 98;
		}
		for (var i = 0, m = attr.json.files.length; i < m; i++) {
			if (attr.json.files[i].progress != undefined) {
				attr.json.files[i].progress = progressPercentage;
			}
		}
	}

	$scope.removeFile = function(attr, fileIndex) {
		var file = attr.json.files.splice(fileIndex, 1);
		$scope.removeFiles.push(file[0].id);
	};

	$scope.tableValueChange = function(tableValue) {
		tableValue.ischange = true;
	};

	$scope.addRow = function(table) {
		var newRow = [];
		for (var i = 0; i < table.json.cols; i++) {
			newRow.push({
				"col" : i,
				"row" : table.calcValue.length,
				inputFlag : true,
				formAttrId : table.attrId
			});
		}
		table.calcValue.push(newRow);
	};

	$scope.removeRow = function(table, rowIndex) {
		table.calcValue.splice(rowIndex, 1);
	};

	$scope.getJson = function() {
		$scope.valueInfo = {
			instanceId : $scope.formInfo.instanceId,
			formId : $scope.formInfo.formId,
			valueStringInfo : [],
			valueTableInfo : [],
			valueTextInfo : [],
			instanceAttrInfo : [],
			addFiles : $scope.addFiles,
			removeFiles : $scope.removeFiles
		};
		for (var i = 0; i < $scope.formInfo.attributes.length; i++) {
			if ($scope.formInfo.attributes[i].type >= 1 && $scope.formInfo.attributes[i].type <= 6 && $scope.formInfo.attributes[i].type != 5
					&& $scope.formInfo.attributes[i].type != 2) {
				if ($scope.formInfo.attributes[i].type == 4) {
					$scope.valueInfo.valueStringInfo.push({
						instanceId : $scope.instanceId,
						formAttrId : $scope.formInfo.attributes[i].attrId,
						value : JSON.stringify($scope.formInfo.attributes[i].value)
					});
				} else {
					$scope.valueInfo.valueStringInfo.push({
						instanceId : $scope.instanceId,
						formAttrId : $scope.formInfo.attributes[i].attrId,
						value : $scope.formInfo.attributes[i].value
					});
				}
			}
			if ($scope.formInfo.attributes[i].type == 2) {
				$scope.valueInfo.valueTextInfo.push({
					instanceId : $scope.instanceId,
					formAttrId : $scope.formInfo.attributes[i].attrId,
					value : $scope.formInfo.attributes[i].value
				});
			}
			// abliu 保存seletct的值
			if ($scope.formInfo.attributes[i].type == 5) {
				getSelectValue($scope.formInfo.attributes[i], $scope.formInfo.attributes[i].json.multiple);
			}
			if ($scope.formInfo.attributes[i].signaturePad && $scope.formInfo.attributes[i].type == 8) {
				if ($scope.formInfo.attributes[i].signaturePad.isEmpty()) {
					$scope.formInfo.attributes[i].value = undefined;
				} else {
					$scope.formInfo.attributes[i].value = $scope.formInfo.attributes[i].signaturePad.toDataURL();
				}
				$scope.valueInfo.valueTextInfo.push({
					formAttrId : $scope.formInfo.attributes[i].attrId,
					value : $scope.formInfo.attributes[i].value
				});
			}
			if ($scope.formInfo.attributes[i].type == 7) {
				$scope.valueInfo.instanceAttrInfo.push({
					formAttrId : $scope.formInfo.attributes[i].attrId,
					type : 1,
					value : $scope.formInfo.attributes[i].calcValue.length
				});
				$scope.valueInfo.instanceAttrInfo.push({
					formAttrId : $scope.formInfo.attributes[i].attrId,
					type : 2,
					value : $scope.formInfo.attributes[i].json.cols
				});
				for (var row = 0; row < $scope.formInfo.attributes[i].calcValue.length; row++) {
					for (var col = 0; col < $scope.formInfo.attributes[i].calcValue[row].length; col++) {
						$scope.valueInfo.valueTableInfo.push({
							formAttrId : $scope.formInfo.attributes[i].attrId,
							col : col,
							row : row,
							value : $scope.formInfo.attributes[i].calcValue[row][col].value,
							inputFlag : $scope.formInfo.attributes[i].calcValue[row][col].inputFlag
						});
					}
				}
			}
		}
		return $scope.valueInfo;
	};

	// ismultiple==0表示单选
	function getSelectValue(attributes, ismultiple) {
		var options = attributes.json.options;
		var instanceId = $scope.instanceId;
		var formAttrId = attributes.attrId;
		var type = attributes.json.optionsType;
		if (ismultiple == 1) {
			if (attributes.value == undefined) {
				return;
			}
			for (var i = 0, m = attributes.value.length; i < m; i++) {
				if (type == 1) {
					var k = 0, n = options.length;
					for (k = 0; k < n; k++) {
						if (options[k].id == attributes.value[i]) {
							var valueobj = {};
							valueobj.instanceId = instanceId
							valueobj.formAttrId = formAttrId;
							valueobj.value = attributes.value[i];
							valueobj.name = options[k].text;
							if ($scope.valueInfo.valueSelectInfo == undefined) {
								$scope.valueInfo.valueSelectInfo = [];
							}
							$scope.valueInfo.valueSelectInfo.push(valueobj);
						}
					}
				} else {
					var valueobj = {};
					valueobj.instanceId = instanceId
					valueobj.formAttrId = formAttrId;
					valueobj.value = attributes.value[i];
					valueobj.name = "";
					if ($scope.valueInfo.valueSelectInfo == undefined) {
						$scope.valueInfo.valueSelectInfo = [];
					}
					$scope.valueInfo.valueSelectInfo.push(valueobj);
				}
			}
		} else {
			if (type == 1) {
				var k = 0, n = options.length;
				for (k = 0; k < n; k++) {
					if (options[k].id == attributes.value) {
						var valueobj = {};
						valueobj.instanceId = instanceId
						valueobj.formAttrId = formAttrId;
						valueobj.value = attributes.value;
						valueobj.name = options[k].text;
						if ($scope.valueInfo.valueSelectInfo == undefined) {
							$scope.valueInfo.valueSelectInfo = [];
						}
						$scope.valueInfo.valueSelectInfo.push(valueobj);
					}
				}
			} else {
				var valueobj = {};
				valueobj.instanceId = instanceId
				valueobj.formAttrId = formAttrId;
				valueobj.value = attributes.value;
				valueobj.name = "";
				if ($scope.valueInfo.valueSelectInfo == undefined) {
					$scope.valueInfo.valueSelectInfo = [];
				}
				$scope.valueInfo.valueSelectInfo.push(valueobj);
			}
		}
	}
	;

	$scope.saveSuccess = function() {
		$scope.addFiles = [];
		$scope.removeFiles = [];
	};

	$scope.save = function(saveCallback) {
		$scope.getJson();
		formService.saveInstance($scope.valueInfo).then(angular.bind(this, function then() {
			var instanceId = formService.saveInstanceResult;
			saveCallback(instanceId);
			$scope.saveSuccess();
		}));
	};
	function isString(obj) { // 判断对象是否是字符串
		return Object.prototype.toString.call(obj) === "[object String]";
	}
	// 将select已选择的数据push数据集合attributes.json.options中
	function pushSelectValue(attributes, selectvalue, ismultiple, optionsType) {
		var options = attributes.json.options;
		if (selectvalue != null && selectvalue != undefined) {
			for (var j = 0, n = selectvalue.length; j < n; j++) {
				if (attributes.attrId != selectvalue[j].formAttrId) {
					continue;
				}
				if (ismultiple == 0) {
					attributes.value = selectvalue[j].value;
				} else {
					if (attributes.value == undefined) {
						attributes.value = [];
					}
					// 填充value
					attributes.value.push(selectvalue[j].value);
				}
				if (optionsType == 0) {
					var option = {};
					option.id = selectvalue[j].value;
					option.text = selectvalue[j].name;
					option.disabled = true;
					option.valid = true;
					options.push(option);
				}
			}
		}
	}
};

angular.module('kindKidsApp').controller('viewFormCtrl', [ '$scope', '$filter', 'formService', viewFormCtrl ]);