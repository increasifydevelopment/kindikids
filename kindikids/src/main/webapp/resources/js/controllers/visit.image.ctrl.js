'use strict';

function visitImageCtrl($scope, $uibModalInstance, imageList, index,title,code) {
	$scope.initIndex = index;
	$scope.list = imageList;
	$scope.title=title;
	//1 标题随image一起变动，-1 标题固定
	$scope.code=code;
	$scope.close = function() {
		$uibModalInstance.dismiss('cancel');
	};
};

angular.module('kindKidsApp').controller('visitImageCtrl', [ '$scope', '$uibModalInstance', 'imageList', 'index','title','code', visitImageCtrl ]);