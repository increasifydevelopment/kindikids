var app = angular.module('kindKidsApp');
app.directive("programActive", function($timeout, $parse,$filter) {
	return {
		restrict : 'AE',
		link : function(scope, element, attrs, ngModelCtrl) {
			$timeout(function() {
				$(element).bind('click', function() {
					$('.menu-block3 li').removeClass('active');
					$(element).parent('li').addClass('active');
					var value = $(element).attr('value');
					var type = $(element).attr('type');
					scope.page.condition.pageIndex = 1;
					scope.page.condition.chooseLocation = true;
					if (type == '1') {
						scope.page.condition.roomId = "";
						scope.page.condition.centersId = "";
					} else if (type == '2') {
						scope.page.condition.centersId = value;
						scope.page.condition.roomId = "";
					} else if (type == '3') {
						scope.page.condition.roomId = value;
						scope.page.condition.centersId = "";
					}
					//刷新页面列表的方法
					scope.search();
				});
				
				//默认选中 gfwang
				/*var result=$filter('haveRole')(scope.currentUser.roleInfoVoList, '0')||scope.isOnlyCasual;
				if(result){
					return;
				}
				var centerId=scope.currentUser.userInfo.centersId;
				var value=$(element).attr('value');
				if(centerId==value){
					$('.menu-block3 li').removeClass('active');
					$(element).parent('li').addClass('active');
					scope.page.condition.centersId=centerId;
					scope.page.condition.roomId='';
					scope.getPageInfo();
					return;
				}*/
			}, 200);
		}
	};
});

app.directive("programType", function($timeout, $parse) {
	return {
		restrict : 'AE',
		link : function(scope, element, attrs, ngModelCtrl) {
			$timeout(function() {
				$(element).bind('click', function() {
					$('.menu-block1 li').removeClass('active');
					$(element).addClass('active');
					var value = $(element).attr('value');
					scope.page.condition.programType = value;
					//刷新页面列表的方法
					scope.search();
				});
			}, 200);
		}
	};
});

app.directive("goTop", function($timeout, $parse) {
	return {
		restrict : 'AE',
		link : function(scope, element, attrs, ngModelCtrl) {
			$timeout(function() {
				$(element).ready(function () {
		            $.goup({
		                trigger: -1,
		                bottomOffset: 50,
		                locationOffset: 80,
		                titleAsText: true,
		                zIndex:2000,
		                elementTag:'.modal'
		            });
		        });
			}, 200);
		}
	};
});

app.directive("programStatus", function($timeout, $parse) {
	return {
		restrict : 'AE',
		link : function(scope, element, attrs, ngModelCtrl) {
			$timeout(function() {
				$(element).bind('click', function() {
					$('.menu-block2 li').removeClass('active');
					$(element).addClass('active');
					var value = $(element).attr('value');
					scope.page.condition.programStatus = value;
					//刷新页面列表的方法
					scope.search();
				});
			}, 200);
		}
	};
});

app.directive("showReason", function($timeout, $parse) {
	return {
		restrict : 'AE',
		scope : {
			model : '=ngModel',
		},
		link : function(scope, element, attrs, ngModelCtrl) {
			$timeout(function() {
				scope.$watch('model',function(newVal){
					if(newVal == 2){
						$(element).parent().parent().parent().find('.not-assessed-reason').removeClass('hidden');
					} else {
						$(element).parent().parent().parent().find('.not-assessed-reason').addClass('hidden');
					}
				})
			}, 200);
		}
	};
});

app.directive("sleepTime", function($timeout, $parse) {
	return {
		restrict : 'AE',
		scope : {
			model : '=ngModel',
		},
		link : function(scope, element, attrs, ngModelCtrl) {
			$timeout(function() {
				scope.$watch('model',function(newVal){
					if(newVal == 5){
						$(element).parent().parent().parent().parent().parent().find('.sleepTime').removeClass('hidden');
					} else {
						$(element).parent().parent().parent().parent().parent().find('.sleepTime').addClass('hidden');
					}
				})
			}, 200);
		}
	};
});

app.directive('mySelectAjax', function($timeout, $parse) {
	return {
		restrict : 'ACE',
		scope : {
			model : '=ngModel',
			list : '=',
			select2Model : '=',
			initSelect : '=',
			param : '='
		},
		link : function(scope, element, attrs) {
			$timeout(function() {
				scope.$watch('param', function(newValue) {
					var data = scope.select2Model;
					var url = attrs["url"];
					var config = {
						placeholder : 'Select option',
						allowClear : true,
						data : data,
						ajax : {
							url : url,
							delay : 250,
							data : function(params) {
								return {
									params : params.term,
									roleId : newValue
								};
							},
							processResults : function(data) {
								return {
									results : data
								};
							},
						}
					};
					$(element).select2(config);
				});

				$(element).on('change', function() {
					scope.$apply(function() {
						// 监控
						scope.select2Model = $(element).select2('data');
					});
				});
			}, 200);

		}
	};
});
