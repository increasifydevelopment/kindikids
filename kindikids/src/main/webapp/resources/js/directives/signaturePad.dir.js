/**
 * @author jfzhao
 * 重写Jquery resize方法
 * 原方法只会在window对象的size改变时触发方法
 * 现方法会在document
 **/
(function ($, window, undefined) {
  var elems = $([]),
    jq_resize = $.resize = $.extend($.resize, {}),
    timeout_id,
    str_setTimeout = 'setTimeout',
    str_resize = 'resize',
    str_data = str_resize + '-special-event',
    str_delay = 'delay',
    str_throttle = 'throttleWindow';
  jq_resize[str_delay] = 250;
  jq_resize[str_throttle] = true;
  $.event.special[str_resize] = {
    setup: function () {
      if (!jq_resize[str_throttle] && this[str_setTimeout]) {
        return false;
      }
      var elem = $(this);
      elems = elems.add(elem);
      $.data(this, str_data, {
        w: elem.width(),
        h: elem.height()
      });
      if (elems.length === 1) {
        loopy();
      }
    },
    teardown: function () {
      if (!jq_resize[str_throttle] && this[str_setTimeout]) {
        return false;
      }
      var elem = $(this);
      elems = elems.not(elem);
      elem.removeData(str_data);
      if (!elems.length) {
        clearTimeout(timeout_id);
      }
    },
    add: function (handleObj) {
      if (!jq_resize[str_throttle] && this[str_setTimeout]) {
        return false;
      }
      var old_handler;

      function new_handler(e, w, h) {
        var elem = $(this),
          data = $.data(this, str_data);
        data.w = w !== undefined ? w : elem.width();
        data.h = h !== undefined ? h : elem.height();
        old_handler.apply(this, arguments);
      }
      if ($.isFunction(handleObj)) {
        old_handler = handleObj;
        return new_handler;
      } else {
        old_handler = handleObj.handler;
        handleObj.handler = new_handler;
      }
    }
  };

  function loopy() {
    timeout_id = window[str_setTimeout](function () {
      elems.each(function () {
        var elem = $(this),
          width = elem.width(),
          height = elem.height(),
          data = $.data(this, str_data);
        if (width !== data.w || height !== data.h) {
          elem.trigger(str_resize, [data.w = width, data.h = height]);
        }
      });
      loopy();
    }, jq_resize[str_delay]);
  }
})(jQuery, this);

angular.module('kindKidsApp')
  .directive("signaturePad", function ($timeout, $parse) {
    return {
      restrict: 'E',
      template: '<div id="signature-pad" class="m-signature-pad">' +
        '<div class="m-signature-pad--body">' +
        '<canvas></canvas>' +
        '</div>' +
        '<div class="m-signature-pad--footer">' +
        '<button type="button" class="button btn btn-default btn-sm clear" data-action="clear">Clear</button>' +
        '<div class="description">Sign above</div>' +
        '</div>' +
        '</div>',
      replace: true,
      scope: {
        ngModel: '='
      },
      link: function (scope, element, attrs) {
        $timeout(function () {
          var wrapper = element[0],
            clearButton = wrapper.querySelector("[data-action=clear]"),
            saveButton = wrapper.querySelector("[data-action=save]"),
            canvas = wrapper.querySelector("canvas"),
            signaturePad;

          function resizeCanvas() {
            var ratio = Math.max(window.devicePixelRatio || 1, 1);
            if (canvas.offsetWidth * ratio != 0 && canvas.offsetHeight * ratio != 0) {
              canvas.width = canvas.offsetWidth * ratio;
              canvas.height = canvas.offsetHeight * ratio;
              canvas.getContext("2d").scale(ratio, ratio);
              if (scope.ngModel.value != undefined) {
                scope.ngModel.signaturePad.fromDataURL(scope.ngModel.value);
              }
            }

          }

          element.bind('resize', resizeCanvas);

          scope.ngModel.signaturePad = new SignaturePad(canvas, {
            onEnd: function () {
              scope.ngModel.value = scope.ngModel.signaturePad.toDataURL();
            }
          });
          resizeCanvas();

          if (scope.ngModel.value != undefined) {
            scope.ngModel.signaturePad.fromDataURL(scope.ngModel.value);
          }

          clearButton.addEventListener("click", function (event) {
            scope.ngModel.signaturePad.clear();
          });
        });
      }
    };
  });

angular.module('kindKidsApp')
  .directive("signaturePad2", function ($timeout, $parse) {
    return {
      restrict: 'E',
      template: '<div id="signature-pad" class="m-signature-pad">' +
        '<div class="m-signature-pad--body">' +
        '<canvas></canvas>' +
        '</div>' +
        '<div class="m-signature-pad--footer">' +
        '<button type="button" class="button btn btn-default btn-sm clear" data-action="clear">Clear</button>' +
        '<div class="description">Sign above</div>' +
        '</div>' +
        '</div>',
      replace: true,
      scope: {
        ngModel: '='
      },
      link: function (scope, element, attrs) {
        $timeout(function () {
          var wrapper = element[0],
            clearButton = wrapper.querySelector("[data-action=clear]"),
            canvas = wrapper.querySelector("canvas"),
            signaturePad;

          function resizeCanvas() {
            var ratio = Math.max(window.devicePixelRatio || 1, 1);
            if (canvas.offsetWidth * ratio != 0 && canvas.offsetHeight * ratio != 0) {
              canvas.width = canvas.offsetWidth * ratio;
              canvas.height = canvas.offsetHeight * ratio;
              canvas.getContext("2d").scale(ratio, ratio);
              if (scope.ngModel.value != undefined) {
                scope.ngModel.signaturePad.fromDataURL(scope.ngModel.value);
              }
            }

          }

          element.bind('resize', resizeCanvas);

          scope.ngModel.signaturePad = new SignaturePad(canvas, {
            onEnd: function () {
              scope.ngModel.value = scope.ngModel.signaturePad.toDataURL();
            }
          });
          resizeCanvas();

          if (scope.ngModel.value != undefined) {
            scope.ngModel.signaturePad.fromDataURL(scope.ngModel.value);
          }

          clearButton.addEventListener("click", function (event) {
            scope.ngModel.signaturePad.clear();
            scope.ngModel.value = null;
          });
        });
      }
    };
  });