'use strict';
var app = angular.module('kindKidsApp');
app.directive('policyActive', function($timeout, $parse,$filter) {
  return {
    restrict : 'A',
    link : function(scope, element, attrs) {
      $timeout(function() {
        var id = $(element).attr('value');
        if(id==scope.page.condition.categoryId){
           $(element).parent('li').addClass('active');
        }
        $(element).click(function() {
          var callbackfun=$('#category ul').attr("clickFun");
          $('#category li').removeClass('active');
          $(this).parent('li').addClass('active');

          var id = $(this).attr('value');  
          scope.page.condition.categoryId = id;       
          if(id=="null"){
           scope.page.condition.categoryId =null;
          }
                    
          eval(callbackfun);          
        });
      }, 200);
    }
  };
});