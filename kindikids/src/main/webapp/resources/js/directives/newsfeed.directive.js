'use strict';
var app = angular.module('kindKidsApp');
app
  .directive("masonry", ['$filter', '$timeout', function ($filter, $timeout) {
    return {
      restrict: 'ACE',
      scope: {
        masonryobj: '=',
        functions: '='
      },
      link: function (scope, element, attrs) {
        $timeout(function () {
          var elm = $(element)[0];
          scope.masonryobj = new Masonry(elm, {
            itemSelector: '.post-each'
          });

          /*   //判断是否有权限
             if(scope.functions!=undefined && scope.functions.length!=undefined && !$filter('haveFunction')(scope.functions, 'news_create')){ 
               try{
                  scope.masonryobj.remove($("#newsFeedCreat"));    
               }catch(e){

               }              
             }*/

        });
      }
    };
  }]).directive("addMasonry", function ($timeout, $parse) {
    return {
      restrict: 'AC',
      scope: {
        masonryobj: '='
      },
      link: function (scope, element, attrs) {
        $timeout(function () {
          scope.masonryobj.appended($(element));
          $(element).find('.post-more').each(function () {
            $(this).click(function (event) {
              event.preventDefault();
              $(this).parents('article').find('.post-excerpt').css('display', 'none');
              $(this).parents('article').find('.post-full-content').slideDown(300, function () {
                scope.masonryobj.layout();
              });
            });
          });
          $(element).find('.post-less').each(function () {
            $(this).click(function (event) {
              event.preventDefault();
              $(this).parents('article').find('.post-full-content').slideUp(50, function () {
                $(this).parents('article').find('.post-excerpt').slideDown(200, function () {
                  scope.masonryobj.layout();
                });
              });
            });
          });
        });
      }
    };
  }).directive("imagecomplete", function ($timeout, $parse) {
    return {
      restrict: 'AC',
      scope: {
        masonryobj: '='
      },
      link: function (scope, element, attrs) {
        $timeout(function () {
          var index = attrs["index"];
          if (index > 0) { return; }
          $(element)[0].onload = function () {
            try {
              scope.masonryobj.layout();
            } catch (e) {
            }
          };
          $(element)[0].onerror = function () {
            try {
              scope.masonryobj.layout();
            } catch (e) {
            }
          };

        });
      }
    };
  });


app.directive("dyCompile", ["$compile", function ($compile) {
  return {
    replace: true,
    restrict: 'EA',
    link: function (scope, elm, iAttrs) {
      var DUMMY_SCOPE = {
        $destroy: angular.noop
      },
        root = elm,
        childScope,
        destroyChildScope = function () {
          (childScope || DUMMY_SCOPE).$destroy();
        };

      iAttrs.$observe("html", function (html) {
        if (html) {
          destroyChildScope();
          childScope = scope.$new(false);
          var content = $compile(html)(childScope);
          root.replaceWith(content);
          root = content;
        }

        scope.$on("$destroy", destroyChildScope);
      });
    }
  };
}]);




app.directive("commentEdit", function ($timeout, $parse) {
  return {
    restrict: 'ACE',
    link: function (scope, element, attrs) {
      $timeout(function () {
        $(element).click(function (event) {
          event.preventDefault();
          var value = $(this).parents('.comment-display').find('.form-control-static').text();
          $(this).parents('.comment-display').find('.form-control-static, .comment-actions').addClass('hidden');
          $(this).parents('.comment-display').find('.message-editing, .comment-sent').removeClass('hidden');
          $(this).parents('.comment-display').find('.message-editing').val(value);

        });

      });
    }
  };
});

app.directive("commentCancel", function ($timeout, $parse) {
  return {
    restrict: 'ACE',
    link: function (scope, element, attrs) {
      $timeout(function () {
        $(element).click(function (event) {
          event.preventDefault();
          $(this).parents('.comment-display').find('.form-control-static, .comment-actions').removeClass('hidden');
          $(this).parents('.comment-display').find('.message-editing, .comment-sent').addClass('hidden');

        });

      });
    }
  };
});

app.directive('newsLoctionActive', function ($timeout, $parse) {
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      $timeout(function () {
        var id = $(element).attr('value');
        if (id == scope.page.condition.centersId || scope.page.condition.roomId == id) {
          $(element).parent('li').addClass('active');
        }
        $(element).click(function () {
          $('.Location li').removeClass('active');
          $(this).parent('li').addClass('active');
          var type = $(this).attr('mtype');
          var id = $(this).attr('value');
          if (type == 0) {
            scope.page.condition.roomId = '';
            scope.page.condition.centersId = '';
          } else if (type == 1) {
            scope.page.condition.roomId = "";
            scope.page.condition.centersId = id;
          } else {
            scope.page.condition.roomId = id;
            scope.page.condition.centersId = '';
          }
          scope.search();
          //scope.closeButton();                
        });
      }, 200);
    }
  };
});
