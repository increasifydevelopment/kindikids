angular.module('kindKidsApp').directive("direselect22", function($timeout, $parse) {
	return {
		restrict : 'ACE',
		link : function(scope, element, attrs) {
			var url = attrs["url"];
			var data = [];
			var config = {
				placeholder : 'Select option',
				data : data,
				ajax : {
					url : url,
					delay : 250,
					data : function(params) {
						return {
							p : params.term
						};
					},
					processResults : function(data) {
						return {
							results : data.data
						};
					},
				}
			};
			$timeout(function() {
				$(element).select2(config);
			});
		}
	};
});
angular.module('kindKidsApp').directive("direselect2", function($timeout, $parse) {
	return {
		restrict : 'ACE',
		link : function(scope, element, attrs) {
			var url = attrs["url"];
			var config;
			$timeout(function() {
				$(element).select2();
			});
		}
	};
});