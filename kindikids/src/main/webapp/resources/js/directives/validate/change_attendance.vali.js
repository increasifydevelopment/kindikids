'use strict';

var app = angular.module('kindKidsApp');
app.directive("changeattendanceForm", function ($timeout, $parse, $filter) {
	return {
		restrict: 'AC',
		link: function (scope, element, attrs) {
			$timeout(function () {
				var json = null;
				if (scope.type != 0) {
					json = {
						validators: {
							choice: {
								min: 1,
								message: 'Please choose at least 1 day'
							}
						}
					};
				} else {
					json = {
						validators: {
							choice: {
								min: 2,
								message: 'Must select a minimum of 2 days'
							}
						}
					};
				}

				// 配置bootstrapValidator校验规则
				$(element).bootstrapValidator({
					excluded: [':disabled'],
					message: "This value is not valid",
					feedbackIcons: {
						validating: 'glyphicon glyphicon-refresh'
					},
					fields: {
						childCenters: {
							validators: {
								notEmpty: {
									message: "The center is required"
								}
							}
						},
						childRooms: {
							validators: {
								notEmpty: {
									message: "The room is required"
								}
							}
						},
						// childGroup : {
						// 	validators : {
						// 		notEmpty : {
						// 			message : "The group is required"
						// 		}
						// 	}
						// },
						changeDate: {
							validators: {
								notEmpty: {
									message: "The Date of Change is required"
								}
							}
						},
						attendanceLesson: json
					}
				});
			}, 0);
		}
	};
});