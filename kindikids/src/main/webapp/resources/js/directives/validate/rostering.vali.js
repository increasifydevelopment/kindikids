var app = angular.module('kindKidsApp');
app.directive("addLeaveForm", function ($timeout, $parse, $filter) {
	return {
		restrict: 'AC',
		link: function (scope, element, attrs) {
			// 配置bootstrapValidator校验规则
			$(element).bootstrapValidator({
				excluded: [':disabled'],
				message: "This value is not valid",
				feedbackIcons: {
					validating: 'glyphicon glyphicon-refresh'
				},
				fields: {
					staffId: {
						validators: {
							notEmpty: {
								message: 'Please select a staff member.'
							}
						}
					},
					fromDate: {
						validators: {
							notEmpty: {
								message: 'A \'From\' date is required'
							},
							digits: {
								message: 'From date must be Monday'
							},
							greaterThan: {
								message: 'Sorry, the \'To\' date must be later than the \'From\' date'
							},
							date: {
								message: 'Sorry, the \'From\' date cannot be a Saturday or Sunday'
							}
						}
					},
					toDate: {
						validators: {
							notEmpty: {
								message: 'A \'To\' date is required'
							},
							digits: {
								message: 'To date must be Friday'
							},
							greaterThan: {
								message: 'Sorry, the \'To\' date must be later than the \'From\' date'
							},
							date: {
								message: 'Sorry, the \'To\' date cannot be a Saturday or Sunday'
							}
						}
					},
					leaveType: {
						validators: {
							notEmpty: {
								message: 'Please select the Leave type'
							}
						}
					},
				}
			})
		}
	};
});
app.directive("applyLeaveForm", function ($timeout, $parse, $filter) {
	return {
		restrict: 'AC',
		link: function (scope, element, attrs) {
			// 配置bootstrapValidator校验规则
			$(element).bootstrapValidator({
				excluded: [':disabled'],
				message: "This value is not valid",
				feedbackIcons: {
					validating: 'glyphicon glyphicon-refresh'
				},
				fields: {
					/* startDate: {
						validators: {
							notEmpty: {
								message: 'A \'From\' date is required'
							},
							digits: {
								message: 'From date must be Monday'
							},
							greaterThan: {
								message: 'Sorry, the \'To\' date must be later than the \'From\' date'
							},
							lessThan: {
								message: 'Sorry, the \'From\' date must be in the future'
							},
							date: {
								message: 'Sorry, the \'From\' date cannot be a Saturday or Sunday'
							}
						}
					},
					endDate: {
						validators: {
							notEmpty: {
								message: 'A \'To\' date is required'
							},
							digits: {
								message: 'To date must be Friday'
							},
							greaterThan: {
								message: 'Sorry, the \'To\' date must be later than the \'From\' date'
							},
							date: {
								message: 'Sorry, the \'From\' date cannot be a Saturday or Sunday'
							}
						}
					}, */
					applyType: {
						validators: {
							notEmpty: {
								message: 'Please select the Leave type'
							}
						}
					},
					policy: {
						validators: {
							notEmpty: {
								message: 'Please confirm your request complies with the Kindikids Annual and Other Leave Policy.'
							}
						}
					},
				}
			});
		}
	};
});
app.directive("rejectCancelForm", function ($timeout, $parse, $filter) {
	return {
		restrict: 'AC',
		link: function (scope, element, attrs) {
			// 配置bootstrapValidator校验规则
			$(element).bootstrapValidator({
				excluded: [':disabled'],
				message: "This value is not valid",
				feedbackIcons: {
					validating: 'glyphicon glyphicon-refresh'
				},
				fields: {
					reasonText: {
						validators: {
							notEmpty: {
								message: 'This is required'
							},
							stringLength: {
								max: 255,
								message: "The Reason must be less than 255 characters long"
							}
						}
					},
				}
			});
		}
	};
});

app.directive("editLeaveForm", function ($timeout, $parse, $filter) {
	return {
		restrict: 'AC',
		link: function (scope, element, attrs) {
			// 配置bootstrapValidator校验规则
			$(element).bootstrapValidator({
				excluded: [':disabled'],
				message: "This value is not valid",
				feedbackIcons: {
					validating: 'glyphicon glyphicon-refresh'
				},
				fields: {
					staff: {
						validators: {
							notEmpty: {
								message: 'Please select a staff member.'
							}
						}
					},
					dateName: {
						validators: {
							notEmpty: {
								message: 'Absence Date is required'
							}
						}
					},
					reason: {
						validators: {
							stringLength: {
								max: 255,
								message: "The Reason must be less than 255 characters long"
							}
						}
					}
				}
			});
		}
	};
});

app.directive("closurePeriodForm", function ($timeout, $parse, $filter) {
	return {
		restrict: 'AC',
		link: function (scope, element, attrs) {
			// 配置bootstrapValidator校验规则
			$(element).bootstrapValidator({
				excluded: [':disabled'],
				message: "This value is not valid",
				feedbackIcons: {
					validating: 'glyphicon glyphicon-refresh'
				},
				fields: {
					centre: {
						validators: {
							choice: {
								min: 1,
								message: "Please select at least one Centre."
							}
						}
					},
					dateFrom: {
						validators: {
							notEmpty: {
								message: 'The Date from(Inclusive) is required.'
							}
						}
					},
					dateTo: {
						validators: {
							notEmpty: {
								message: 'The Date to(Inclusive) is required.'
							}
						}
					},
					name: {
						validators: {
							notEmpty: {
								message: 'The Name of Closure Period is required.'
							},
							stringLength: {
								max: 50,
								message: "The Name of Closure Period must be less than 50 characters long"
							}
						}
					}
				}
			});
		}
	};
});

/*
 * app.directive("staffCenter", function($timeout, $parse) { return { restrict :
 * 'AE', link : function(scope, element, attrs, ngModelCtrl) {
 * $timeout(function() { $(element).bind('click', function() { $('.menu-block2
 * li').removeClass('active'); $(element).parent('li').addClass('active'); var
 * value = $(element).attr('value'); scope.page.condition.centerId = value; }); },
 * 200); } }; });
 */