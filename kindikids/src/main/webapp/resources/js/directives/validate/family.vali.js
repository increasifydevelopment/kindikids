var app = angular.module('kindKidsApp');
app.directive("childForm",
    function ($timeout, $parse, $filter) {
        return {
            restrict: 'AC',
            link: function (scope, element, attrs) {
                $timeout(function () {
                        // 如果为ceo 园长 2园长 家长 不禁用表单,可编辑
                        var result = $filter('haveRole')(scope.currentUser.roleInfoVoList, '0;1;5;10');
                        if (!result) {
                            scope.disableForm();
                            scope.isClearDownload = true;
                        } else {
                            if ($filter('haveRole')(scope.currentUser.roleInfoVoList, '1;5')) {
                                if (!scope.isShowOpt) {
                                    scope.disableForm();
                                    scope.isClearDownload = true;
                                }
                            }
                        }
                        if (!scope.isFirstParent()) {
                            scope.disableForm();
                            scope.isShowOpt = false;
                            scope.isClearDownload = true;
                        }
                    },
                    200);
                scope.isAchive(scope.isArchiverStatus, null);

                // 表单校验
                $(element).bootstrapValidator({
                    excluded: [':disabled'],
                    message: "This value is not valid",
                    feedbackIcons: {
                        validating: 'glyphicon glyphicon-refresh'
                    },
                    fields: {
                        'conditions2[]': {
                            validators: {
                                notEmpty: {
                                    message: 'Please confirm you have provided information to the best of your knowledge.'
                                }
                            }
                        },
                        'conditions1[]': {
                            validators: {
                                notEmpty: {
                                    message: 'Please confirm you have provided information to the best of your knowledge.'
                                }
                            }
                        },
                        firstName: {
                            validators: {
                                notEmpty: {
                                    message: "The Child's First Name is required"
                                },
                                stringLength: {
                                    max: 50,
                                    message: "The Child's First Name must be less than 50 characters long"
                                }
                            }
                        },
                        middleName: {
                            validators: {
                                stringLength: {
                                    max: 50,
                                    message: "The Child's Middle Name must be less than 50 characters long"
                                }
                            }
                        },
                        lastName: {
                            validators: {
                                notEmpty: {
                                    message: "The Child's Surname is required"
                                },
                                stringLength: {
                                    max: 50,
                                    message: "The Child's Surname must be less than 50 characters long"
                                }
                            }
                        },
                        genderRadios: {
                            validators: {
                                notEmpty: {
                                    message: "Child's Gender is required"
                                }
                            }
                        },
                        childBirthday: {
                            validators: {
                                notEmpty: {
                                    message: "Date of birth is required"
                                },
                                callback: {
                                    message: 'Date of birth must be earlier than or equal to the current date',
                                    callback: function (value, validator, $field, options) {
                                        var choose = new moment(value, 'DD/MM/YYYY')._d;
                                        var temp = new moment().format('DD/MM/YYYY');
                                        var today = new moment(temp, 'DD/MM/YYYY')._d;
                                        return choose <= today;
                                    }
                                },
                            }
                        },
                        crn: {
                            validators: {
                                stringLength: {
                                    max: 50,
                                    message: "The Enrolment ID of the child must be less than 50 characters long"
                                }
                            }
                        },
                        address: {
                            validators: {
                                stringLength: {
                                    max: 255,
                                    message: "The Adress of the child must be less than 255 characters long"
                                }
                            }
                        },
                        suburb: {
                            validators: {
                                stringLength: {
                                    max: 255,
                                    message: "The Suburb must be less than 255 characters long"
                                }
                            }
                        },
                        postcode: {
                            validators: {
                                regexp: {
                                    regexp: /^[0-9]+$/,
                                    message: "The Post Code can only consist of number"
                                },
                                stringLength: {
                                    max: 50,
                                    message: "The Post Code of the child must be less than 50 characters long"
                                }
                            }
                        },
                        otherspaecify: {
                            validators: {
                                stringLength: {
                                    max: 500,
                                    message: "The Other, please specify must be less than 500 characters long"
                                }
                            }
                        },
                        relationtochild: {
                            validators: {
                                stringLength: {
                                    max: 500,
                                    message: "The relation to the child or access to the child must be less than 500 characters long"
                                }
                            }
                        },
                        medicareNumber: {
                            validators: {
                                stringLength: {
                                    max: 255,
                                    message: "The Medicare Number must be less than 255 characters long"
                                }
                            }
                        },
                        medicalCentreName: {
                            validators: {
                                stringLength: {
                                    max: 255,
                                    message: "The Medical Centre Name must be less than 255 characters long"
                                }
                            }
                        },
                        medicalPractitionerName: {
                            validators: {
                                stringLength: {
                                    max: 255,
                                    message: "The Medical Practitioner Name must be less than 255 characters long"
                                }
                            }
                        },
                        medicalPhone: {
                            validators: {
                                stringLength: {
                                    max: 50,
                                    message: "The Phone must be less than 50 characters long"
                                }
                            }
                        },
                        medicalAddress: {
                            validators: {
                                stringLength: {
                                    max: 255,
                                    message: "The Address must be less than 255 characters long"
                                }
                            }
                        },
                        dentistName: {
                            validators: {
                                stringLength: {
                                    max: 255,
                                    message: "The Dentist name must be less than 255 characters long"
                                }
                            }
                        },
                        dentistPhone: {
                            validators: {
                                stringLength: {
                                    max: 50,
                                    message: "The Phone must be less than 50 characters long"
                                }
                            }
                        },
                        dentistAddress: {
                            validators: {
                                stringLength: {
                                    max: 255,
                                    message: "The Address must be less than 255 characters long"
                                }
                            }
                        },
                        languageHome: {
                            validators: {
                                stringLength: {
                                    max: 255,
                                    message: "The Language used in the child's home must be less than 255 characters long"
                                }
                            }
                        },
                        backgroundChild: {
                            validators: {
                                stringLength: {
                                    max: 255,
                                    message: "The Cultural background of the child must be less than 255 characters long"
                                }
                            }
                        },
                        drinkMilk: {
                            validators: {
                                stringLength: {
                                    max: 255,
                                    message: "The specify must be less than 255 characters long"
                                }
                            }
                        },
                        sleepTime: {
                            validators: {
                                stringLength: {
                                    max: 800,
                                    message: "The specify must be less than 800 characters long"
                                }
                            }
                        },
                        clothingNeeds: {
                            validators: {
                                stringLength: {
                                    max: 800,
                                    message: "The specify must be less than 800 characters long"
                                }
                            }
                        },
                        assistanceDuring: {
                            validators: {
                                stringLength: {
                                    max: 800,
                                    message: "The specify must be less than 800 characters long"
                                }
                            }
                        },
                        religiousRequirements: {
                            validators: {
                                stringLength: {
                                    max: 800,
                                    message: "The child to experience at KINDKIDS must be less than 800 characters long"
                                }
                            }
                        },
                        experience: {
                            validators: {
                                stringLength: {
                                    max: 255,
                                    message: "The specify must be less than 255 characters long"
                                }
                            }
                        },
                        interests: {
                            validators: {
                                stringLength: {
                                    max: 255,
                                    message: "The child's interests that you would like to share must be less than 255 characters long"
                                }
                            }
                        },
                        dairySpecify: {
                            validators: {
                                stringLength: {
                                    max: 255,
                                    message: "The specify must be less than 255 characters long"
                                }
                            }
                        },
                        nutsSpecify: {
                            validators: {
                                stringLength: {
                                    max: 255,
                                    message: "The specify must be less than 255 characters long"
                                }
                            }
                        },
                        eggsSpecify: {
                            validators: {
                                stringLength: {
                                    max: 255,
                                    message: "The specify must be less than 255 characters long"
                                }
                            }
                        },
                        meatSpecify: {
                            validators: {
                                stringLength: {
                                    max: 255,
                                    message: "The specify must be less than 255 characters long"
                                }
                            }
                        },
                        applicableSpecify: {
                            validators: {
                                stringLength: {
                                    max: 255,
                                    message: "The specify must be less than 255 characters long"
                                }
                            }
                        },
                        childCenters: {
                            validators: {
                                notEmpty: {
                                    message: "The center is required"
                                },
                            }
                        },
                        childRooms: {
                            validators: {
                                notEmpty: {
                                    message: "The room is required"
                                },
                            }
                        },
                        childGroup: {
                            validators: {
                                notEmpty: {
                                    message: "The group is required"
                                },
                            }
                        },
                        sleepTimes: {
                            validators: {
                                stringLength: {
                                    max: 255,
                                    message: "The Sleep times must be less than 255 characters long"
                                }
                            }
                        },
                        mealTimes: {
                            validators: {
                                stringLength: {
                                    max: 255,
                                    message: "The Meal times must be less than 255 characters long"
                                }
                            }
                        },
                        bottleTimes: {
                            validators: {
                                stringLength: {
                                    max: 255,
                                    message: "The Bottle times must be less than 255 characters long"
                                }
                            }
                        },
                        enrolmentDate: {
                            validators: {
                                notEmpty: {
                                    message: "The enrolmentDate is required"
                                }
                            }
                        },
                        selectCenttr: {
                            validators: {
                                notEmpty: {
                                    message: "Centre is required"
                                }
                            }
                        },
                        changeDate: {
                            validators: {
                                notEmpty: {
                                    message: "First day at Kindikids is required"
                                }
                            }
                        },
                        attendanceLesson: {
                            validators: {
                                choice: {
                                    min: 1,
                                    message: 'Must select a minimum of 1 days'
                                }
                            }
                        }
                        /*
                         * , roomSelect : { validators : { notEmpty : { message : "The
                         * Room is required" } } }, groupSelect : { validators : {
                         * notEmpty : { message : "The Group is required" } } }
                         */
                    }
                }).on('status.field.bv',
                    function (e, data) {
                        var $tabPane = data.element.parents('.tab-pane');
                        scope.validateTag($tabPane);
                    });
            }

        };
    });

app.directive("givingNoticeForm",
    function ($timeout, $parse, $filter) {
        return {
            restrict: 'AC',
            link: function (scope, element, attrs) {
                // 配置bootstrapValidator校验规则
                $(element).bootstrapValidator({
                    excluded: [':disabled'],
                    message: "This value is not valid",
                    feedbackIcons: {
                        validating: 'glyphicon glyphicon-refresh'
                    },
                    fields: {
                        childName: {
                            validators: {
                                notEmpty: {
                                    message: "Child's Name is required"
                                },
                                stringLength: {
                                    max: 50,
                                    message: "Child's Name be less than 50 characters long"
                                }
                            }
                        },
                        parentName: {
                            validators: {
                                notEmpty: {
                                    message: "Parent's Name is required"
                                },
                                stringLength: {
                                    max: 50,
                                    message: "Parent's Name be less than 50 characters long"
                                }
                            }
                        },
                        lastDay: {
                            validators: {
                                notEmpty: {
                                    message: 'Last Day after 3 Full Weeks is required'
                                },
                                callback: {
                                    message: 'Time must be greater than or equal to the current date',
                                    callback: function (value, validator, $field, options) {
                                        var choose = new moment(value, 'DD/MM/YYYY')._d;
                                        var temp = new moment().format('DD/MM/YYYY');
                                        var today = new moment(temp, 'DD/MM/YYYY')._d;
                                        return choose >= today;
                                    }
                                },
                            }
                        },
                        reson: {
                            validators: {
                                stringLength: {
                                    max: 255,
                                    message: "Reson for Giving Notice be less than 255 characters long"
                                }
                            }
                        },
                        accountName: {
                            validators: {
                                /*
                                 * notEmpty : { message : 'Account Name is required' },
                                 */
                                stringLength: {
                                    max: 255,
                                    message: "Account Name be less than 255 characters long"
                                }
                            }
                        },
                        bsb: {
                            validators: {
                                /*
                                 * notEmpty : { message : 'BSB is required' },
                                 */
                                stringLength: {
                                    max: 50,
                                    message: "BSB be less than 50 characters long"
                                }
                            }
                        },
                        accountNum: {
                            validators: {
                                /*
                                 * notEmpty : { message : 'Account Number is
                                 * required' },
                                 */
                                stringLength: {
                                    max: 50,
                                    message: "Account Number be less than 50 characters long"
                                }
                            }
                        },
                        nominate: {
                            validators: {
                                stringLength: {
                                    max: 50,
                                    message: "This be less than 50 characters long"
                                }
                            }
                        },
                        supervisor: {
                            validators: {
                                stringLength: {
                                    max: 50,
                                    message: "This be less than 50 characters long"
                                }
                            }
                        },
                        acceptAnswer: {
                            validators: {
                                notEmpty: {
                                    message: 'This checkbox is required'
                                },
                            }
                        },
                        acceptAnswer1: {
                            validators: {
                                notEmpty: {
                                    message: 'This checkbox is required'
                                },
                            }
                        }
                        /*,
                    parentPad : {
    					validators : {
    						notEmpty : {
    							message : 'Parent Signature is required'
    						},
    					}
    				},staffSign : {
    					validators : {
    						notEmpty : {
    							message : 'Staff Signature is required'
    						},
    					}
    				},parentSignTwo : {
    					validators : {
    						notEmpty : {
    							message : 'Parent Signature is required'
    						},
    					}
    				}*/
                    }
                });
            }

        };
    });

app.directive("absenteeForm", function ($timeout, $parse, $filter) {
    return {
        restrict: 'AC',
        link: function (scope, element, attrs) {
            // 配置bootstrapValidator校验规则
            $(element).bootstrapValidator({
                excluded: [':disabled'],
                message: "This value is not valid",
                feedbackIcons: {
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    childName: {
                        validators: {
                            notEmpty: {
                                message: "Child's Name is required"
                            },
                            stringLength: {
                                max: 50,
                                message: "Child's Name be less than 50 characters long"
                            }
                        }
                    },
                    parentName: {
                        validators: {
                            notEmpty: {
                                message: "Parent's Name is required"
                            },
                            stringLength: {
                                max: 50,
                                message: "Parent's Name be less than 50 characters long"
                            }
                        }
                    },
                    checkParentRead: {
                        validators: {
                            notEmpty: {
                                message: "This checkbox is required"
                            }
                        }
                    },
                    lastDayAtCentre: {
                        validators: {
                            notEmpty: {
                                message: 'Last day at Centre is required'
                            },
                            callback: {
                                message: 'Last day at Centre must be less than Date Returning to the Centre',
                                callback: function (value, validator, $field, options) {
                                    var choose = new moment(value, 'DD/MM/YYYY')._d;
                                    var endDateStr = $('#deturningCentre').val();
                                    if (!endDateStr) {
                                        return true;
                                    }
                                    var endDate = new moment(endDateStr, 'DD/MM/YYYY')._d;
                                    return choose <= endDate;
                                }
                            }
                        }
                    },
                    deturningCentre: {
                        validators: {
                            notEmpty: {
                                message: 'Date Returning to the Centre is required'
                            },
                            callback: {
                                message: 'Date Returning to the Centre must be later than the Last Day at the centre',
                                callback: function (value, validator, $field, options) {
                                    var choose = new moment(value, 'DD/MM/YYYY')._d;
                                    var startDateStr = $("#lastDayAtCentre").val();
                                    if (!startDateStr) {
                                        return true;
                                    }
                                    var startDate = new moment(startDateStr, 'DD/MM/YYYY')._d;
                                    return choose >= startDate;
                                }
                            }
                        }
                    },
                    reasonAbsence: {
                        validators: {
                            stringLength: {
                                max: 255,
                                message: "Reason for Absence be less than 255 characters long"
                            }
                        }
                    },
                    weeksAbsent: {
                        validators: {
                            stringLength: {
                                max: 255,
                                message: "Number of absent days be less than 255 characters long"
                            }
                        }
                    },
                    absentNum: {
                        validators: {
                            stringLength: {
                                max: 255,
                                message: "Number of absent days be less than 255 characters long"
                            }
                        }
                    },
                    totalNumber: {
                        validators: {
                            stringLength: {
                                max: 255,
                                message: "Total number of absent days be less than 255 characters long"
                            }
                        }
                    },
                    email: {
                        validators: {
                            emailAddress: {
                                message: "The input is not a valid Email Address"
                            },
                            stringLength: {
                                max: 100,
                                message: "The Email must be less than 100 characters long"
                            }
                        }
                    },
                    contactNumber: {
                        validators: {
                            stringLength: {
                                max: 100,
                                message: "The Contact Number must be less than 100 characters long"
                            }
                        }
                    }
                }
            });
        }
    };
});