var app = angular.module('kindKidsApp');
app.directive("staffForm", function ($timeout, $parse) {
	return {
		restrict: 'AC',
		link: function (scope, element, attrs) {
			var userId = scope.userId;
			// 表单校验
			$(element).bootstrapValidator({
				excluded: [':disabled'],
				message: "This value is not valid",
				feedbackIcons: {
					// valid: "glyphicon glyphicon-ok",
					// invalid: "glyphicon glyphicon-remove",
					validating: "glyphicon glyphicon-refresh"
				},
				fields: {
					//base info
					firstName: {
						validators: {
							notEmpty: {
								message: "First name is required."
							},
							stringLength: {
								max: 50,
								message: "The first name must be less than 50 characters long"
							}
						}
					},
					middleName: {
						validators: {
							stringLength: {
								max: 50,
								message: "The middle name must be less than 50 characters long"
							}
						}
					},
					lastName: {
						validators: {
							notEmpty: {
								message: "Last name is required."
							},
							stringLength: {
								max: 50,
								message: "The last name must be less than 50 characters long"
							}
						}
					},
					email: {
						validators: {
							notEmpty: {
								message: "Email address is required."
							},
							emailAddress: {
								message: "Please provide a valide email address"
							},
							stringLength: {
								max: 255,
								message: "The email must be less than 255 characters long"
							},
							remote: {
								type: "POST",
								url: "./common/validateEmail.do",
								data: {
									email: 'email',
									userId: userId
								},
								message: "Sorry, the email address already exists in the system, please check and try again."
							}
						}
					},
					genderRadios: {
						validators: {
							notEmpty: {
								message: "Gender is required."
							}
						}
					},
					address: {
						validators: {
							stringLength: {
								max: 255,
								message: "The address must be less than 255 characters long"
							}
						}
					},
					suburb: {
						validators: {
							stringLength: {
								max: 255,
								message: "The suburb must be less than 255 characters long"
							}
						}
					},
					postcode: {
						validators: {
							stringLength: {
								max: 50,
								message: "The postcode must be less than 50 characters long"
							}
						}
					},
					phoneNumber: {
						validators: {
							stringLength: {
								max: 50,
								message: "The phone number must be less than 50 characters long"
							}
						}
					},
					mobileNumber: {
						validators: {
							stringLength: {
								max: 50,
								message: "The mobile number must be less than 50 characters long"
							}
						}
					},
					ethnicity: {
						validators: {
							stringLength: {
								max: 255,
								message: "The ethnicity must be less than 255 characters long"
							}
						}
					},
					childrenNum: {
						validators: {
							stringLength: {
								max: 255,
								message: "The children num must be less than 255 characters long"
							}
						}
					},
					childrenAge: {
						validators: {
							stringLength: {
								max: 255,
								message: "The children age must be less than 255 characters long"
							}
						}
					},

					//medical
					medicalCondition: {
						validators: {
							stringLength: {
								max: 255,
								message: "The medical condition must be less than 255 characters long"
							}
						}
					},
					medicalAllergies: {
						validators: {
							stringLength: {
								max: 255,
								message: "The medical allergies must be less than 255 characters long"
							}
						}
					},
					medication: {
						validators: {
							stringLength: {
								max: 255,
								message: "The medication must be less than 255 characters long"
							}
						}
					},
					medicalCertificate: {
						validators: {
							stringLength: {
								max: 255,
								message: "The medical certificate must be less than 255 characters long"
							}
						}
					},

					//employ
					roleId: {
						validators: {
							notEmpty: {
								message: "The role of this staff member is required."
							}
						}
					}

				}
			}).on('status.field.bv', function (e, data) {
				/*  var $tabPane = data.element.parents('.tab-pane');
	                scope.validateTag($tabPane);*/
				var $tabPane = data.element.parents('.tab-pane');
				var tabId = $tabPane.attr('id');
				if (tabId) {
					//搜索菜单
					var $icon = $('a[tab="' + tabId + '"][data-toggle="tab"]');
					//如果当前tabs有验证不通过的，则给当前菜单加class
					if ($("#" + tabId).find("small[data-bv-result='INVALID']").length > 0) {
						$icon.addClass('my-validate');
					} else {
						$icon.removeClass('my-validate');
					}
				}
			});

		}
	};
});

app.directive("staffEmployment", function ($timeout, $parse) {
	return {
		restrict: 'AC',
		link: function (scope, element, attrs) {
			var userId = scope.userId;
			// 表单校验
			$(element).bootstrapValidator({
				excluded: [':disabled'],
				message: "This value is not valid",
				feedbackIcons: {
					// valid: "glyphicon glyphicon-ok",
					// invalid: "glyphicon glyphicon-remove",
					validating: "glyphicon glyphicon-refresh"
				},
				fields: {
					roleId: {
						validators: {
							notEmpty: {
								message: "The role of this staff member is required."
							}
						}
					},
					effectiveDate: {
						validators: {
							notEmpty: {
								message: "The Effective from Date for this staff member is required."
							}
						}
					}
				}
			})
		}
	};
});
