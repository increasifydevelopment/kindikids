var app = angular.module('kindKidsApp');
app.directive("parentForm", function($timeout, $parse, $filter) {
	return {
		restrict : 'AC',
		link : function(scope, element, attrs) {
			
			var disableFunction=function(element){
				$(element).find("input").attr("disabled", 'disable');
				$(element).find("select").attr("disabled", 'disable');
				$(element).find("textarea").attr("disabled", 'disable');
			}
			
			var userId = scope.currtenEditId;
			var result = $filter('haveRole')(scope.currentUser.roleInfoVoList, '0;10');
			//判断是否为园长
			var isCenterMnager=$filter('haveRole')(scope.currentUser.roleInfoVoList, '1;5');
			var flag=false;
			if(isCenterMnager){
				for(var i=0;i<scope.childList.length;i++){
					//如果当前family的小孩有1个是属于当前园长的，当前园长就可以编辑这个小孩
					var child=scope.childList[i];
					var childCenterId=child.userInfo.centersId;
					var childOutCentre=!child.userInfo.outCenterIds?[]:child.userInfo.outCenterIds;
					//有一个外部小孩就可以编辑所有家长
					if(childCenterId==scope.currentUser.userInfo.centersId||childOutCentre.length>0){
						flag=true;
					}
				}
			}
			
			if (!result&&(isCenterMnager&&!flag)|| (!scope.isFirstParent()&&scope.currtenEditId!=scope.currentUser.userInfo.id)) {
				scope.hideParentOpt=true;
				disableFunction(element);
			}
            //如果是老师，则禁用
			if(scope.isOnlyEduCook){
				disableFunction(element);
			}
			
			// 表单校验
			$(element).bootstrapValidator({
				excluded : [ ':disabled' ],
				message : "This value is not valid",
				feedbackIcons : {
					// valid: "glyphicon glyphicon-ok",
					// invalid: "glyphicon glyphicon-remove",
					validating : "glyphicon glyphicon-refresh"
				},
				fields : {
					firstName : {
						validators : {
							notEmpty : {
								message : "Given name is required"
							},
							stringLength : {
								max : 50,
								message : "The Given Name must be less than 50 characters long"
							}
						}
					},
					middleName : {
						validators : {
							stringLength : {
								max : 50,
								message : "The Middle Name must be less than 50 characters long"
							}
						}
					},
					lastName : {
						validators : {
							notEmpty : {
								message : "Surname is required"
							},
							stringLength : {
								max : 50,
								message : "The Surname must be less than 50 characters long"
							}
						}
					},
					parentBirthday : {
						validators : {
							date : {
								format : 'DD/MM/YYYY',
								message : 'The value is not a valid date'
							}
						}
					},
					email : {
						validators : {
							notEmpty : {
								message : "An email address is required."
							},
							emailAddress : {
								message : "The input is not a valid Email Address"
							},
							stringLength : {
								max : 255,
								message : "The Email must be less than 255 characters long"
							},
							remote : {
								type : "POST",
								url : "./common/validateEmail.do",
								data : {
									email : 'email',
									userId : userId
								},
								message : "Sorry, this email already exists."
							}
						}
					},
					genderRadios : {
						validators : {
							notEmpty : {
								message : "Gender is required"
							}
						}
					},
					address : {
						validators : {
							stringLength : {
								max : 255,
								message : "The Adress must be less than 255 characters long"
							}
						}
					},
					suburb : {
						validators : {
							stringLength : {
								max : 255,
								message : "The Suburb must be less than 255 characters long"
							}
						}
					},
					postcode : {
						validators : {
							regexp : {
								regexp : /^[0-9]+$/,
								message : "The Post Code can only consist of number"
							},
							stringLength : {
								max : 20,
								message : "The Post Code  must be less than 50 characters long"
							}
						}
					},
					homePhone : {
						validators : {
							stringLength : {
								max : 50,
								message : "The Home Phone number  must be less than 50 characters long"
							}
						}
					},
					workPhone : {
						validators : {
							stringLength : {
								max : 50,
								message : "The Work Phone numbermust be less than 50 characters long"
							}
						}
					},
					mobilePhone : {
						validators : {
							stringLength : {
								max : 50,
								message : "The Mobile Phone number must be less than 50 characters long"
							}
						}
					},
					occupation : {
						validators : {
							stringLength : {
								max : 255,
								message : "The Occupation must be less than 255 characters long"
							}
						}
					},
					work : {
						validators : {
							stringLength : {
								max : 255,
								message : "The Place of work must be less than 255 characters long"
							}
						}
					},
					languagesSpoken : {
						validators : {
							stringLength : {
								max : 255,
								message : "The Languages spoken at home must be less than 255 characters long"
							}
						}
					}
				}
			});
		}

	};
});
