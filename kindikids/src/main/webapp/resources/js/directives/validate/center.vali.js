var app = angular.module('kindKidsApp');
app.directive("centerForm", function($timeout, $parse, $filter) {
	return {
		restrict : 'AC',
		link : function(scope, element, attrs) {
			$timeout(function() {
				var centerId = scope.$stateParams.centerId;
				// 表单校验
				$(element).bootstrapValidator({
					excluded : [ ':disabled' ],
					message : "This value is not valid",
					feedbackIcons : {
						validating : "glyphicon glyphicon-refresh"
					},
					fields : {
						centerAddress : {
							validators : {
								stringLength : {
									max : 255,
									message : "The Centre Address must be less than 255 characters long"
								}
							}
						},
						centerContact : {
							validators : {
								stringLength : {
									max : 255,
									message : "The Centre Contact must be less than 255 characters long"
								}
							}

						},
						centerName : {
							validators : {
								notEmpty : {
									message : "The Centre Name is required"
								},
								stringLength : {
									max : 255,
									message : "The Centre Name must be less than 255 characters long"
								},
								remote : {
									type : "POST",
									url : "./center/validateCenterName.do",
									data : {
										centerName : 'centerName',
										centerId : centerId
									},
									message : "Centre Name cannot be repeated"
								}
							}
						},
						centerEmail : {
							emailAddress : {
								message : "The input is not a valid Email Address"
							},
							stringLength : {
								max : 255,
								message : "The Email Address must be less than 255 characters long"
							}
						}
					}
				}).on('success.form.bv', function(e) {
					e.preventDefault();
				});
			}, 100);
		}

	};
});
app.directive("roomForm", function($timeout, $parse, $filter) {
	return {
		restrict : 'AC',
		link : function(scope, element, attrs) {
			var roomId = scope.roomInfo.id;
			var centerId = scope.roomInfo.centersId;
			// 表单校验
			$(element).bootstrapValidator({
				excluded : [ ':disabled' ],
				message : "This value is not valid",
				feedbackIcons : {
					validating : "glyphicon glyphicon-refresh"
				},
				fields : {
					roomName : {
						validators : {
							notEmpty : {
								message : "The Room Name is required"
							},
							stringLength : {
								max : 255,
								message : "The Room Name must be less than 255 characters long"
							},
							remote : {
								type : "POST",
								url : "./center/validateRoomName.do",
								data : {
									roomName : 'roomName',
									centerId : centerId,
									roomId : roomId,
								},
								message : "Room Name cannot be repeated"
							}
						}
					},
					ageGroup : {
						validators : {
							stringLength : {
								max : 255,
								message : "The Age Group must be less than 255 characters long"
							}
						}
					},
					childCapacity : {
						validators : {
							notEmpty : {
								message : "The Child Capacity is required"
							},
							stringLength : {
								max : 10,
								message : "The Child Capacity must be less than 10 characters long"
							},
							regexp : {
								regexp : /^[0-9]+$/,
								message : "Please enter a valid number for the Child Capacity and try again."
							}
						}
					}
				}
			}).on('success.form.bv', function(e) {
				e.preventDefault();
			});
		}

	};
});

app.directive("groupForm", function($timeout, $parse, $filter) {
	return {
		restrict : 'AC',
		link : function(scope, element, attrs) {
			var roomId = scope.groupInfo.roomId;
			var groupId = scope.groupInfo.id;
			// 表单校验
			$(element).bootstrapValidator({
				excluded : [ ':disabled' ],
				message : "This value is not valid",
				feedbackIcons : {
					validating : "glyphicon glyphicon-refresh"
				},
				fields : {
					groupName : {
						validators : {
							notEmpty : {
								message : "The Group Name is required"
							},
							stringLength : {
								max : 255,
								message : "The Group Name must be less than 255 characters long"
							},
							remote : {
								type : "POST",
								url : "./center/validateGroupName.do",
								data : {
									groupName : 'groupName',
									groupId : groupId,
									roomId : roomId,
								},
								message : "Group Name cannot be repeated"
							}
						}
					}
				}
			}).on('success.form.bv', function(e) {
				e.preventDefault();
			});
		}

	};
});

app.directive("menuForm", function($timeout, $parse, $filter) {
	return {
		restrict : 'AC',
		link : function(scope, element, attrs) {
			var menuId = '';
			// 表单校验
			$(element).bootstrapValidator({
				excluded : [ ':disabled' ],
				message : "This value is not valid",
				feedbackIcons : {
					validating : "glyphicon glyphicon-refresh"
				},
				fields : {
					menuName : {
						validators : {
							notEmpty : {
								message : "The Menu Name is required"
							},
							stringLength : {
								max : 50,
								message : "The Menu Name must be less than 50 characters long"
							},
							remote : {
								type : "POST",
								url : "./menu/validateMenuName.do",
								data : {
									menuName : 'menuName',
									menuId : menuId,
								},
								message : "Menu Name cannot be duplicated"
							}
						}
					}
				}
			}).on('success.form.bv', function(e) {
				e.preventDefault();
			});
		}

	};
});


app.directive("formMenu", function($timeout, $parse, $filter) {
	return {
		restrict : 'AC',
		link : function(scope, element, attrs) {
			var menuId = scope.menu.menuId;
				// 表单校验
				$(element).bootstrapValidator({
					excluded : [ ':disabled' ],
					message : "This value is not valid",
					feedbackIcons : {
						validating : "glyphicon glyphicon-refresh"
					},
					fields : {
						menuName : {
							validators : {
								notEmpty : {
									message : "The Menu Name is required"
								},
								stringLength : {
									max : 50,
									message : "The Menu Name must be less than 50 characters long"
								},
								remote : {
									type : "POST",
									url : "./menu/validateMenuName.do",
									data : {
										menuName : 'menuName',
										menuId : menuId,
									},
									message : "Menu Name cannot be duplicated"
								}
							}
						},
						breakfast : {
							validators : {
								stringLength : {
									max : 255,
									message : "The Breakfast must be less than 255 characters long"
								}
							}
						},
						morningTea : {
							validators : {
								stringLength : {
									max : 255,
									message : "The Morning Tea must be less than 255 characters long"
								}
							}
						},
						lunch : {
							validators : {
								stringLength : {
									max : 255,
									message : "The Lunch must be less than 255 characters long"
								}
							}
						},
						dessert : {
							validators : {
								stringLength : {
									max : 255,
									message : "The Dessert must be less than 255 characters long"
								}
							}
						},
						afternoonTeam : {
							validators : {
								stringLength : {
									max : 255,
									message : "The Afternoon Tea must be less than 255 characters long"
								}
							}
						},
						lateAfternoonTea : {
							validators : {
								stringLength : {
									max : 255,
									message : "The Late Afternoon Tea must be less than 255 characters long"
								}
							}
						}
					}
				}).on('success.form.bv', function(e) {
					e.preventDefault();
				});

		}

	};
});
