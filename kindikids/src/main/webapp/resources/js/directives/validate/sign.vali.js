var app = angular.module('kindKidsApp');
app
	.directive(
		"editSignForm",
		function ($timeout, $parse, $filter) {
			return {
				restrict: 'AC',
				link: function (scope, element, attrs) {
					// 配置bootstrapValidator校验规则
					$(element)
						.bootstrapValidator(
							{
								excluded: [':disabled'],
								message: "This value is not valid",
								feedbackIcons: {
									validating: 'glyphicon glyphicon-refresh'
								},
								fields: {
									inTime: {
										validators: {
											notEmpty: {
												message: 'Date is required'
											},
											callback: {
												message: 'Date cannot be in the future',
												callback: function (value, validator, $field, options) {
													var choose = new moment(value, 'DD/MM/YYYY')._d;
													var temp = new moment().format('DD/MM/YYYY');
													var today = new moment(temp, 'DD/MM/YYYY')._d;
													return choose <= today;
												}
											}
										}
									},
									signinTime: {
										validators: {
											notEmpty: {
												message: 'Please enter the sign in \'Time\''
											},
										}
									},
									centerName: {
										validators: {
											notEmpty: {
												message: 'This is required'
											},
										}
									},
									ContactNum: {
										validators: {
											stringLength: {
												max: 50,
												message: "The Contact Number must be less than 50 numbers long"
											},
										}
									},
									reason: {
										validators: {
											stringLength: {
												max: 255,
												message: "The Reason must be less than 255 characters long"
											},
										}
									},
									organisation: {
										validators: {
											stringLength: {
												max: 50,
												message: "The Reason must be less than 50 characters long"
											},
										}
									},
								}
							});
				}
			};
		});

app
	.directive(
		"staffSignForm",
		function ($timeout, $parse, $filter) {
			return {
				restrict: 'AC',
				link: function (scope, element, attrs) {
					// 配置bootstrapValidator校验规则
					$(element)
						.bootstrapValidator(
							{
								excluded: [':disabled'],
								message: "This value is not valid",
								feedbackIcons: {
									validating: 'glyphicon glyphicon-refresh'
								},
								fields: {
									rosterTimeStart: {
										validators: {
											notEmpty: {
												message: 'Please enter the Start Time'
											},
										}
									},
									signDate: {
										validators: {
											notEmpty: {
												message: 'Date is required'
											},
										}
									},
									rosterTimeEnd: {
										validators: {
											notEmpty: {
												message: 'Please enter the End Time'
											},
										}
									},
								}
							});
				}
			};
		});

app
	.directive(
		"addSignForm",
		function ($timeout, $parse, $filter) {
			return {
				restrict: 'AC',
				link: function (scope, element, attrs) {
					// 配置bootstrapValidator校验规则
					$(element)
						.bootstrapValidator(
							{
								excluded: [':disabled'],
								message: "This value is not valid",
								feedbackIcons: {
									validating: 'glyphicon glyphicon-refresh'
								},
								fields: {
									rosterTimeStart1: {
										validators: {
											notEmpty: {
												message: 'Please enter the Start Time'
											},
										}
									},
									rosterTimeEnd1: {
										validators: {
											notEmpty: {
												message: 'Please enter the End Time'
											},
										}
									},
								}
							});
				}
			};
		});
app.directive("visitorSigninForm", function ($timeout, $parse, $filter) {
	return {
		restrict: 'AC',
		link: function (scope, element, attrs) {
			//配置bootstrapValidator校验规则
			$(element).bootstrapValidator({
				excluded: [':disabled'],
				message: "This value is not valid",
				feedbackIcons: {
					validating: 'glyphicon glyphicon-refresh'
				},
				fields: {
					firstName: {
						validators: {
							notEmpty: {
								message: 'First name is required'
							}, stringLength: {
								max: 50,
								message: "The First Name must be less than 50 characters long"
							}
						}
					},
					Surname: {
						validators: {
							notEmpty: {
								message: 'Surname is required'
							}, stringLength: {
								max: 50,
								message: "The Surname must be less than 50 characters long"
							}
						}
					},
					ReasonForVisit: {
						validators: {
							stringLength: {
								max: 255,
								message: "The Reason must be less than 255 characters long"
							},
						}
					},
					centreName: {
						validators: {
							notEmpty: {
								message: 'This is required'
							},
						}
					},
					ContactNum: {
						validators: {
							stringLength: {
								max: 50,
								message: "The Contact Number must be less than 50 numbers long"
							},
						}
					},
					signatrue: {
						validators: {
							notEmpty: {
								message: 'Signature is required'
							},
						}
					},
					organisation: {
						validators: {
							stringLength: {
								max: 255,
								message: "The Organisation must be less than 255 numbers long"
							},
						}
					}
				}
			});
		}
	};
});
app.directive("visitorSignoutSignForm", function ($timeout, $parse, $filter) {
	return {
		restrict: 'AC',
		link: function (scope, element, attrs) {
			//配置bootstrapValidator校验规则
			$(element).bootstrapValidator({
				excluded: [':disabled'],
				message: "This value is not valid",
				feedbackIcons: {
					validating: 'glyphicon glyphicon-refresh'
				},
				fields: {
					signoutSignature: {
						validators: {
							notEmpty: {
								message: 'Signature is required'
							},
						}
					},
				}
			});
		}
	};
});

app.directive("childSigninSignForm", function ($timeout, $parse, $filter) {
	return {
		restrict: 'AC',
		link: function (scope, element, attrs) {
			//配置bootstrapValidator校验规则
			$(element).bootstrapValidator({
				excluded: [':disabled'],
				message: "This value is not valid",
				feedbackIcons: {
					validating: 'glyphicon glyphicon-refresh'
				},
				fields: {
					sunscreen: {
						validators: {
							notEmpty: {
								message: 'Please confirm that Sunscreen has been applied'
							},
						}
					},
					childSigninSignature: {
						validators: {
							notEmpty: {
								message: 'Signature is required'
							},
						}
					},
					personPassword: {
						validators: {
							notEmpty: {
								message: 'This is required'
							},
							stringLength: {
								max: 30,
								message: "The Password must be less than 30 characters long"
							}
						}
					},
					policy1: {
						validators: {
							notEmpty: {
								message: 'This is required'
							}
						}
					},
					policy2: {
						validators: {
							notEmpty: {
								message: 'This is required'
							}
						}
					},
				}
			});
		}
	};
});
