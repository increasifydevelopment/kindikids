'use strict';
var app = angular.module('kindKidsApp');
app.directive("eventForm", function($timeout, $parse, $filter) {
	return {
		restrict: 'AC',
		link: function(scope, element, attrs) {
			$(element).bootstrapValidator({
				excluded: [':disabled'],
				message: "This value is not valid",
				feedbackIcons: {
					validating: "glyphicon glyphicon-refresh"
				},
				fields: {
					title: {
						validators: {
							notEmpty: {
								message: "The Title is required"
							},
							stringLength: {
								max: 255,
								message: "The Title must be less than 255 characters long"
							}
						}
					},
					description: {
						validators: {
							stringLength: {
								max: 500,
								message: "Must be less than 500 characters"
							}
						}
					},
					visibility: {
						validators: {
							notEmpty: {
								message: "The Visibility is required"
							}
						}
					},
				}
			}).on('success.form.bv', function(e) {
				e.preventDefault();
			});

		}
	};
});