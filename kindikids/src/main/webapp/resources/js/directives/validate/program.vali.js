var app = angular.module('kindKidsApp');
app
	.directive(
		"editLessonForm",
		function ($timeout, $parse, $filter) {
			return {
				restrict: 'AC',
				link: function (scope, element, attrs) {
					// 配置bootstrapValidator校验规则
					$(element)
						.bootstrapValidator(
							{
								excluded: [':disabled'],
								message: "This value is not valid",
								feedbackIcons: {
									validating: 'glyphicon glyphicon-refresh'
								},
								fields: {
									lessonName: {
										validators: {
											notEmpty: {
												message: 'Lesson name is required'
											},
										}
									},
								}
							});
				}
			};
		});
app
	.directive(
		"editRegisterForm",
		function ($timeout, $parse, $filter) {
			return {
				restrict: 'AC',
				link: function (scope, element, attrs) {
					// 配置bootstrapValidator校验规则
					$(element)
						.bootstrapValidator(
							{
								excluded: [':disabled'],
								message: "This value is not valid",
								feedbackIcons: {
									validating: 'glyphicon glyphicon-refresh'
								},
								fields: {
									'childSelect': {
										validators: {
											notEmpty: {
												message: 'This is required'
											},
										}
									},
								}
							});
				}
			};
		});
app
	.directive(
		"editMedicationForm",
		function ($timeout, $parse, $filter) {
			return {
				restrict: 'AC',
				link: function (scope, element, attrs) {
					// 配置bootstrapValidator校验规则
					$(element)
						.bootstrapValidator(
							{
								excluded: [':disabled'],
								message: "This value is not valid",
								feedbackIcons: {
									validating: 'glyphicon glyphicon-refresh'
								},
								fields: {
									dateName: {
										validators: {
											notEmpty: {
												message: 'Missing Required Fields'
											},
										}
									},
									childName: {
										validators: {
											notEmpty: {
												message: 'Missing Required Fields'
											},
										}
									},
									medicationName: {
										validators: {
											notEmpty: {
												message: 'Missing Required Fields'
											},
										}
									},
									expiryDate: {
										validators: {
											notEmpty: {
												message: 'Missing Required Fields'
											},
										}
									},
								}
							});
				}
			};
		});
app
	.directive(
		"editInterestForm",
		function ($timeout, $parse, $filter) {
			return {
				restrict: 'AC',
				link: function (scope, element, attrs) {
					// 配置bootstrapValidator校验规则
					$(element)
						.bootstrapValidator(
							{
								excluded: [':disabled'],
								message: "This value is not valid",
								feedbackIcons: {
									validating: 'glyphicon glyphicon-refresh'
								},
								fields: {
									childSelect: {
										validators: {
											notEmpty: {
												message: 'Please select a child'
											},
										}
									},
									interestName: {
										validators: {
											notEmpty: {
												message: 'Interest name is required'
											},
										}
									},
									observationName: {
										validators: {
											notEmpty: {
												message: 'Observation is required'
											},
										}
									},
								}
							});
				}
			};
		});

