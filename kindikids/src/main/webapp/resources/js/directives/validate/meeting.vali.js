var app = angular.module('kindKidsApp');
app.directive("agendaForm", function($timeout, $parse, $filter) {
	return {
		restrict : 'AC',
		link : function(scope, element, attrs) {
			scope.$watch('meetingAgendaInfo.id', function(newValue) {
				$(element).bootstrapValidator('removeField', 'agendaName');
				$(element).bootstrapValidator('removeField', 'agendaType');
				$(element).bootstrapValidator('removeField', 'paragraph');
				
				$(element).bootstrapValidator('addField', 'agendaName', {
					validators : {
						notEmpty : {
							message : "The Agenda Name is required"
						},
						stringLength : {
							max : 255,
							message : "The Agenda Name must be less than 255 characters long"
						},
						remote : {
							type : "POST",
							url : "./meeting/validateMeetingAgendaName.do",
							data : {
								agendaName : 'agendaName',
								id : newValue
							},
							message : "Agenda Name cannot be duplicated",
						}
					}
				});
				$(element).bootstrapValidator('addField', 'agendaType', {
					validators : {
						notEmpty : {
							message : "The Agenda Type is required"
						}
						
					}
				});
				$(element).bootstrapValidator('addField', 'paragraph', {
					validators : {
						stringLength : {
							max : 500,
							message : "The Paragraph must be less than 500 characters long"
						}
					}
				});
				$(element).bootstrapValidator('updateStatus', 'agendaName', null);
				$(element).bootstrapValidator('updateStatus', 'agendaType', null);
				$(element).bootstrapValidator('updateStatus', 'paragraph', null);
			});
			var myvalidate = function() {
				$(element).bootstrapValidator({
					excluded : [ ':disabled' ],
					message : "This value is not valid",
					feedbackIcons : {
						validating : "glyphicon glyphicon-refresh"
					},
					fields : {
						agendaName : {
							validators : {
								notEmpty : {
									message : "The Agenda Name is required"
								},
								stringLength : {
									max : 255,
									message : "The Agenda Name must be less than 255 characters long"
								}
							}
						},
						agendaType : {
							validators : {
								notEmpty : {
									message : "The Agenda Type is required"
								}
							}
						},
						paragraph : {
							validators : {
								stringLength : {
									max : 500,
									message : "The Paragraph must be less than 500 characters long"
								}
							}
						}
					}
				}).on('success.form.bv', function(e) {
					e.preventDefault();
				});
			};
				myvalidate();
				
		}
	};
});

app.directive("templateForm", function($timeout, $parse, $filter) {
	return {
		restrict : 'AC',
		link : function(scope, element, attrs) {
			scope.$watch('meeting.meetingTemplateInfo.id', function(newValue) {
				$(element).bootstrapValidator('removeField', 'templateName');
				$(element).bootstrapValidator('addField', 'templateName', {
					validators : {
						notEmpty : {
							message : "The Meeting Template Name is required"
						},
						stringLength : {
							max : 255,
							message : "The Meeting Template Name must be less than 255 characters long"
						},
						remote : {
							type : "POST",
							url : "./meeting/validateMeetingTempName.do",
							data : {
								templateName : 'templateName',
								id : newValue
							},
							message : "Meeting template name cannot be duplicated",
						}
					}
				});
				$(element).bootstrapValidator('updateStatus', 'templateName', null);
			});

			function myValidate() {
				$(element).bootstrapValidator({
					excluded : [ ':disabled' ],
					message : "This value is not valid",
					feedbackIcons : {
						validating : "glyphicon glyphicon-refresh"
					},
					fields : {
						templateName : {
							validators : {
								notEmpty : {
									message : "The Meeting Template Name is required"
								},
								stringLength : {
									max : 255,
									message : "The Meeting Template Name must be less than 255 characters long"
								},
								remote : {
									type : "POST",
									url : "./meeting/validateMeetingTempName.do",
									data : {
										templateName : 'templateName'
									},
									message : "Meeting template name cannot be duplicated"
								}
							}
						}
					}
				}).on('success.form.bv', function(e) {
					e.preventDefault();
				});
			}
			$timeout(function() {
				myValidate();
			}, 100);

		}

	};
});
app.directive("meetingaddForm", function($timeout, $parse, $filter) {
	return {
		restrict : 'AC',
		link : function(scope, element, attrs) {
			$(element).bootstrapValidator({
				excluded : [ ':disabled' ],
				message : "This value is not valid",
				feedbackIcons : {
					validating : "glyphicon glyphicon-refresh"
				},
				fields : {
					meetingName : {
						validators : {
							notEmpty : {
								message : "The Meeting Name is required"
							},
							stringLength : {
								max : 255,
								message : "The Meeting Name must be less than 255 characters long"
							},
							remote : {
								type : "POST",
								url : "./meetManager/validateMeetingName.do",
								data : {
									meetingName : 'meetingName'
								},
								message : "Meeting name cannot be duplicated"
							}
						}
					},
					meetingDay : {
						validators : {
							notEmpty : {
								message : "The Date is required"
							}
						}
					},
					meetingTemplate : {
						validators : {
							notEmpty : {
								message : "The Meeting Template is required"
							}
						}
					}
				}
			}).on('success.form.bv', function(e) {
				e.preventDefault();
			});

		}
	};
});