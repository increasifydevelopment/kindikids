var app = angular.module('kindKidsApp');
app.directive("taskForm", function($timeout, $parse, $filter) {
	return {
		restrict : 'AC',
		link : function(scope, element, attrs) {
			scope.$watch('taskInfo.id', function(newValue) {
				$(element).bootstrapValidator('removeField', 'taskName');
				$(element).bootstrapValidator('addField', 'taskName', {
					validators : {
						notEmpty : {
							message : "The Task Name is required"
						},
						stringLength : {
							max : 255,
							message : "The Task Name must be less than 255 characters long"
						},
						remote : {
							type : "POST",
							url : "./task/validataTaskName.do",
							data : {
								taskName : 'taskName',
								taskId : newValue
							},
							message : "Oops, it looks like this task already exists. Please check the name of your task and try again."
						}
					}
				});
				// 解决标红问题
				$(element).bootstrapValidator('updateStatus', 'taskName', null);
			});
			scope.$watch('taskInfo.shiftFlag', function(newValue) {
				myvalidate();
			});
			$timeout(function() {
				myvalidate();
			}, 100);
			var myvalidate = function() {
				// 表单校验
				$(element).bootstrapValidator({
					excluded : [ ':disabled' ],
					message : "This value is not valid",
					feedbackIcons : {
						validating : "glyphicon glyphicon-refresh"
					},
					fields : {
						taskName : {
							validators : {
								notEmpty : {
									message : "The Task Name is required"
								},
								stringLength : {
									max : 255,
									message : "The Task Name must be less than 255 characters long"
								},
								remote : {
									type : "POST",
									url : "./task/validataTaskName.do",
									data : {
										taskName : 'taskName'
									},
									message : "Oops, it looks like this task already exists. Please check the name of your task and try again."
								}
							}
						},
						taskCategory : {
							validators : {
								notEmpty : {
									message : "The Task Category is required"
								}
							}
						},
						selectObjId : {
							validators : {
								notEmpty : {
									message : "The Select Centre is required"
								}
							}
						},
						cpRadios : {
							validators : {
								notEmpty : {
									message : "This is Educators' Shift X ?"
								}
							}
						},
						responsible : {
							validators : {
								notEmpty : {
									message : "The Responsible People is required"
								}
							}
						},
						implementers : {
							validators : {
								notEmpty : {
									message : "The Task Implementers is required"
								}
							}
						}
					}
				}).on('success.form.bv', function(e) {
					e.preventDefault();
				});
			};
		}
	};
});

app.directive("modelForm", function($timeout, $parse, $filter) {
	return {
		restrict : 'AC',
		link : function(scope, element, attrs) {
			// 表单校验
			var myvalidate = function() {
				$(element).bootstrapValidator({
					message : "This value is not valid",
					feedbackIcons : {
						validating : "glyphicon glyphicon-refresh"
					},
					fields : {
						startsOnDate : {
							validators : {
								notEmpty : {
									message : "The Starts on date is required"
								},
								callback : {
									message : 'Starts on date must be greater than or equal to the current date',
									callback : function(value, validator, $field, options) {
										var choose = new moment(value, 'DD/MM/YYYY')._d;
										var temp = new moment().format('DD/MM/YYYY');
										var today = new moment(temp, 'DD/MM/YYYY')._d;
										return choose >= today;
									}
								}
							}
						},
						endsOnDate : {
							trigger : null,
							validators : {
								stringLength : {
									max : 255,
									message : "The End on date must be less than 255 characters long"
								},
								notEmpty : {
									message : "The End on date date is required"
								}
							}
						}
					}
				}).on('success.form.bv', function(e) {
					e.preventDefault();
				});
			};
			//myvalidate();
			/*scope.$watch('taskInfo.taskFrequencyInfo.haveEndDate', function(newValue) {
				$("#modelForm").data('bootstrapValidator').updateStatus('endsOnDate', 'VALIDATED', null).validateField('endsOnDate');
				//$("#modelForm").data('bootstrapValidator').resetForm();
			});*/

		}

	};
});
