var app = angular.module('kindKidsApp');
app.directive("doActive", function($timeout, $parse) {
	return {
		restrict : 'ACE',
		link : function(scope, element, attrs, ngModelCtrl) {
			$timeout(function() {
				$(element).bind('click', function() {
					$('.task-list div').removeClass('active');
					$(element).addClass('active');
				});
			}, 200);
		}
	};
});
app.directive("taskSelectAjax", function($timeout, $parse) {
	return {
		restrict : 'ACE',
		scope : {
			model : '=ngModel',
			list : '=',
			select2Model : '=',
			parentNode : '='
		},
		require : "ngModel",
		link : function(scope, element, attrs, ngModelCtrl) {
			if (!scope.parentNode) {
				// 去除这个监控会导致 第一个无法选中两个
				scope.$watch('select2Model', function(newValue) {
					var url = attrs["url"];
					var config = {
						placeholder : 'Select option',
						data : scope.select2Model,
						ajax : {
							url : url,
							delay : 250,
							data : function(params) {
								return {
									findText : params.term
								};
							},
							processResults : function(data) {
								return {
									results : data
								};
							},
						}
					};
					$(element).select2(config);
				});
			} else {
				scope.$watch('parentNode', function(newValue) {
					var data = scope.select2Model;
					var url = attrs["url"];
					if (scope.parentNode) {
						var parentNode = new Array();
						angular.forEach(scope.parentNode, function(data, index, array) {
							var childs = data.children;
							if (childs) {
								for (var i = 0; i < childs.length; i++) {
									var tmpChild = childs[i];
									tmpChild.id = tmpChild.id.split(',')[0];
									parentNode.push(tmpChild);
								}
							} else {
								if (data && data.id) {
									var id = data.id.split(',')[0];
									data.id = id;
									parentNode.push({
										id : data.id.split(',')[0],
										type : data.type
									});
								}
							}
						});
						url = url + "&selectJson=" + encodeURIComponent(JSON.stringify(parentNode));
					}
					$(element).select2({
						placeholder : 'Select option',
						data : data,
						ajax : {
							url : url,
							delay : 250,
							data : function(params) {
								return {
									findText : params.term
								};
							},
							processResults : function(data) {
								return {
									results : data
								};
							},
						}
					});
				});
			}

			$(element).on('change', function(o, l) {
				scope.$apply(function() {
					// 监控
					var value = $(element).select2('data');
					// 解决type丢失问题
					angular.forEach(value, function(data, index, array) {
						var type = null;
						try {
							type = data.id.split(',')[1];
						} catch (e) {
						}
						data.type = type * 1;
					});
					scope.select2Model = value;
				});
			});
		}
	};
});