'use strict';
var app = angular.module('kindKidsApp');
app.directive('selectAjaxEvent', function($timeout, $parse) {
	return {
		restrict: 'ACE',
		scope: {
			model: '=ngModel',
			list: '=',
			select2Model: '=',
			initSelect: '='
		},
		link: function(scope, element, attrs) {
			$timeout(function() {
				var data = scope.select2Model;
				if(data!=undefined && data.length>0){
					for(var i=0,m=data.length;i<m;i++){
						if(data[i].id=="0"){
							data[i].text="Everyone";
							break;
						}
					}
				}

				var url = attrs["url"];
				var config = {
					placeholder: 'Select option',
					data: data,
					ajax: {
						url: url,
						delay: 250,
						data: function(params) {
							return {
								p: params.term
							};
						},
						processResults: function(data) {
							var defaultData={
								"avatar":"",
								"id":"0",
								"personColor":"",
								"selected":true,
								"text":"Everyone"
							}
							data.splice(0, 0, defaultData);
							return {
								results: data
							};
						},
					}
				};
				$(element).select2(config);

				$(element).on('change', function() {
					scope.$apply(function() {
						// 监控
						scope.select2Model = $(element).select2('data');
					});
				});
				$(element).css("display","block");
			}, 200);

		}
	};
});
app.directive("carousel", function($timeout, $parse) {
	return {
		restrict : 'ACE',
		link : function(scope, element, attrs, ngModelCtrl) {
			$timeout(function() {
				var carouselObj=$(element).carousel({interval:false});
			}, 200);
		}
	};
});