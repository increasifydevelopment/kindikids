var app = angular.module('kindKidsApp');

app.directive("shiftTagging", function($timeout, $parse) {
	return {
		restrict : 'AE',
		scope : {
			tagVo : '='
		},
		link : function(scope, element, attrs, ngModelCtrl) {
				$(element).on('shown.bs.popover', function () {
					var popId = $(element).attr("aria-describedby");
					var pop = document.getElementById(popId); 
					var checkInputs = $(pop).find("input");
					//根据传入的数据，控制checked显示
					for(var i = 0 ; i < scope.tagVo.inTags.length;i++){
						if(scope.tagVo.inTags[i].checked){
							$(checkInputs[i]).attr('checked',true);
						}
					}
				//绑定确定和取消按钮点击事件
					//确认按钮
					$($(pop).find(".btn-general")[0]).bind('click', function() {
						//根据popover里面checkbox 的选中情况，更新外围对象字段值
						//$("input[type='checkbox']").is(':checked')
						for(var i = 0 ; i < checkInputs.length; i ++){
							if($(checkInputs[i]).is(':checked')){
								scope.tagVo.inTags[i].checked = true;
							} else {
								scope.tagVo.inTags[i].checked = false;
							}
						}
						$(pop).popover('hide');
						$(element).trigger("click");
						scope.$apply();
					});
					//取消按钮
					$($(pop).find(".btn-default")[0]).bind('click', function() {
						$(element).popover('hide');
						$(element).trigger("click");
					});
				})
		}
	};
});

