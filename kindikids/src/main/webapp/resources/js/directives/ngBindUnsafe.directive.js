'use strict';
var app = angular.module('kindKidsApp');
app.directive('ngBindUnsafe', function() {
  return {
    restrict : 'A',
    scope : {
    	ngBindUnsafe : '='
    },
    link : function(scope, element, attrs) {
      $(element).html(scope.ngBindUnsafe);
    }
  };
});