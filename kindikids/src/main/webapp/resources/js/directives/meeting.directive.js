var app = angular.module('kindKidsApp');
app.directive("doActive", function($timeout, $parse) {
	return {
		restrict : 'ACE',
		link : function(scope, element, attrs, ngModelCtrl) {
			$timeout(function() {
				$(element).bind('click', function() {
					$('.task-list div').removeClass('active');
					$(element).addClass('active');
				});
			}, 200);
		}
	};
});
app.directive("typeChoose", function($timeout, $parse) {
	return {
		restrict : 'ACE',
		link : function(scope, element, attrs, ngModelCtrl) {
			$timeout(function() {
				 var id = $(element).attr('value');
			        if(id==scope.condition.typeId){
			           $(element).parent('li').addClass('active');
			        }
				$(element).bind('click', function() {
					$('.menu-block li').removeClass('active');
					$(element).parent('li').addClass('active');
					var value = $(element).attr('value');
					scope.condition.pageIndex = 1;
					scope.condition.typeId = value;
					// 刷新页面列表的方法
					scope.getAgendaList();
					scope.initAgeModel();
				});

			}, 200);
		}
	};
});

app.directive("myMinutes", function($timeout, $filter, $parse) {
	return {
		restrict : 'ACE',
		require : 'ngModel',
		link : function(scope, element, attrs, ngModelCtrl) {
			$timeout(function() {
				$(element).datetimepicker({
					format : 'ss',
					parseInputDate : function(inputDate) {
						if (isNaN(inputDate)) {
							ngModelCtrl.$setViewValue('00');
							return new moment('00', 'ss');
						}
						if (inputDate * 1 > 59 || inputDate * 1 < 0) {
							ngModelCtrl.$setViewValue('00');
							return new moment('00', 'ss');
						}
						// 页面加载的时候，文本框输入的内容，转换时间为DD/MM/YYYY
						ngModelCtrl.$setViewValue(new moment(inputDate, 'ss')._i);
						return new moment(inputDate, 'ss');
					}
				}).on("dp.change", function(val) {
					ngModelCtrl.$setViewValue($(this).val());
					$(element).trigger("keyup");
				});
			}, 200);
		}
	};
});
app.directive("myTime", function($timeout, $filter, $parse) {
	return {
		restrict : 'ACE',
		require : 'ngModel',
		link : function(scope, element, attrs, ngModelCtrl) {
				$(element).keydown(function(event) {
					var keyCode = event.keyCode;
					if (keyCode < 48 || keyCode > 57) {
						event.keyCode = 0;
					}
				});
		}
	};
});