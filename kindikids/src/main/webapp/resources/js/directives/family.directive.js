var app = angular.module('kindKidsApp');

app.directive('myActive', function($timeout, $parse, $filter) {
	return {
		restrict : 'A',
		link : function(scope, element, attrs) {
			$timeout(function() {
				$(element).click(function() {
					$('#orgMenu li').removeClass('active');
					$(this).parent('li').addClass('active');
					var type = $(this).attr('mtype');
					var id = $(this).attr('value');
					if (type == 0) {
						scope.page.condition.roomId = '';
						scope.page.condition.centerId = '';
					} else if (type == 1) {
						scope.page.condition.roomId = "";
						scope.page.condition.centerId = id;
					} else {
						scope.page.condition.roomId = id;
						scope.page.condition.centerId = '';
					}
					scope.page.condition.pageIndex = 1;
					scope.mySearch();
				});

				var result = $filter('haveRole')(
						scope.currentUser.roleInfoVoList, '0;1;5')
						|| scope.isOnlyCasual;
				if (result) {
					return;
				}
				// by mingwang
				// parent登陆左侧导航选中问题
				/* --start---- */
				var mtype = $(element).attr('mtype');
				var index = $(element).attr('index');
				if (mtype == 1 && index == 0) {
					$(element).parent('li').addClass('active');
				}
				/* ----end------ */
				var centerId = scope.currentUser.userInfo.centersId;
				var value = $(element).attr('value');
				if (centerId == value) {
					$('.menu-block3 li').removeClass('active');
					$(element).parent('li').addClass('active');
					scope.page.condition.centersId = centerId;
					scope.page.condition.roomId = '';
					scope.mySearch();
					return;
				}
			}, 200);
		}
	};
});
app.directive('myOtherActive', function($timeout, $parse) {
	return {
		restrict : 'A',
		link : function(scope, element, attrs) {
			$timeout(function() {
				$(element).click(function() {
					$('#otherMenu li').removeClass('active');
					$(this).parent('li').addClass('active');
					var myStatus = $(this).attr('mystatus');
					scope.page.condition.myStatus = myStatus;
					scope.page.condition.pageIndex = 1;
					scope.mySearch();
				});
			}, 200);
		}
	};
});

app.directive("replaceActive", ["$compile","$timeout", function($compile,$timeout) {
  return {
    replace: true,
    restrict: 'EA',
    link: function(scope, elm, iAttrs) {
//    	$timeout(function(){
//    	});
    	
    	    var DUMMY_SCOPE = {
    	            $destroy: angular.noop
    	          },
    	          root = elm,
    	          childScope,
    	          destroyChildScope = function() {
    	            (childScope || DUMMY_SCOPE).$destroy();
    	          };

    	        iAttrs.$observe("html", function(html) {
    	          if (html) {
    	            html = "<div>" + html + "</div>";
    	            destroyChildScope();
    	            childScope = scope.$new(false);
    	            var content = $compile(html)(childScope);
    	            root.replaceWith(content);
    	            root = content;
    	          }

    	          scope.$on("$destroy", destroyChildScope);
    	        });
    	
    	
  
    }
  };
}]);

app.directive("loadHtmlchild", function($timeout, $parse) {
	return {
		restrict : 'AC',
		scope : {
			contentlength : '=contentlength'
		},
		link : function(scope, element, attrs) {
			$timeout(function() {
				scope.$watch('contentlength', function(newValue) {
					var html = attrs["loadHtmlchild"];
					$(element).html("<div>" + html + "</div>");
				});
			});

		}
	};
});

app.directive('dateValidate', function($timeout, $parse) {
	return {
		restrict : 'A',
		link : function(scope, element, attrs) {
			$timeout(function() {
				var formId = $(element).parents('form').attr('id');
				console.log(formId);
				scope.$watch(attrs['ngModel'], function(newValue) {
					var name = attrs['name'];
					if (newValue) {
						$("#" + formId).bootstrapValidator('updateStatus',
								name, 'VALID');
					} else {
						// $('#givingNoticeForm').data('bootstrapValidator').updateStatus(name,
						// 'INVALID', 'notEmpty');
					}
				});
			}, 200);
		}
	};
});

app.directive("logEdit", function($timeout, $parse) {
	return {
		restrict : 'ACE',
		link : function(scope, element, attrs) {
			$timeout(function() {
				$(element).click(
						function(event) {
							event.preventDefault();
							var id = attrs["id"];
							
							var reason = $(this).parents('.logInfo').find(
									'#staticReason' + id).children().html();
							var reg = new RegExp("<br>","g");//g,表示全部替换。
							reason = reason.replace(reg,"\n");
							var date = $(this).parents('.logInfo').find(
									'#staticDate' + id).text();
							$(this).parents('.logInfo').find('.staic-control')
									.addClass('hidden');
							$(this).parents('.logInfo').find('.edit-control')
									.removeClass('hidden');
//							$(this).parents('.logInfo').find(
//									'#delFile' + id).removeClass('hidden');
//							$(this).parents('.logInfo').find(
//									'#uploadFile' + id).removeClass('hidden');
							$(this).parents('.logInfo')
									.find('#editReason' + id).val(reason);
							$(this).parents('.logInfo').find('#editDate' + id)
									.val(date);
						});

			});
		}
	};
});

app.directive("logCancel", function($timeout, $parse) {
	return {
		restrict : 'ACE',
		link : function(scope, element, attrs) {
			$timeout(function() {
				$(element).click(
						function(event) {
							event.preventDefault();
							$(this).parents('.logInfo').find('.staic-control')
									.removeClass('hidden');
							$(this).parents('.logInfo').find('.edit-control')
									.addClass('hidden');
//							$(this).parents('.logInfo').find('.del-control')
//									.addClass('hidden');
//							$(this).parents('.logInfo').find('.del-control')
//							.addClass('hidden');
						});
			});
		}
	};
});
