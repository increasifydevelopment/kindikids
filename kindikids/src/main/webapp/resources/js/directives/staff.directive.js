var app = angular.module('kindKidsApp');
app.directive("myStaffActive", function ($timeout, $parse) {
	return {
		restrict: 'AE',
		link: function (scope, element, attrs, ngModelCtrl) {
			$timeout(function () {
				$(element).bind('click', function () {
					$('.menu-block1 li').removeClass('active');
					$(element).parent('li').addClass('active');
					var value = $(element).attr('value');
					scope.page.condition.roleId = value;
					scope.page.condition.pageIndex = 1;
					scope.search();
				});
			}, 200);
		}
	};
});
app.directive("myStaffActive2", function ($timeout, $parse) {
	return {
		restrict: 'AE',
		link: function (scope, element, attrs, ngModelCtrl) {
			$timeout(function () {
				$(element).bind('click', function () {
					$('.menu-block2 li').removeClass('active');
					$(element).parent('li').addClass('active');
					var value = $(element).attr('value');
					var type = $(element).attr('type');
					scope.page.condition.pageIndex = 1;
					if (type == '1') {
						scope.page.condition.roomId = "";
						if (scope.currUser.roleValue == 0 || scope.currUser.roleValue == 1 || scope.currUser.roleValue == 5) {
							scope.page.condition.centerId = "";
						}
						scope.page.condition.casual = 0;
					} else if (type == '2') {
						scope.page.condition.centerId = value;
						scope.page.condition.roomId = "";
						scope.page.condition.casual = 1;
					} else if (type == '3') {
						scope.page.condition.roomId = value;
						scope.page.condition.casual = 1;
					}
					scope.search();
				});
			}, 200);
		}
	};
});

app.directive('mySelectAjax', function ($timeout, $parse) {
	return {
		restrict: 'ACE',
		scope: {
			model: '=ngModel',
			list: '=',
			select2Model: '=',
			initSelect: '=',
			param: '='
		},
		link: function (scope, element, attrs) {
			$timeout(function () {
				scope.$watch('param', function (newValue) {
					var data = scope.select2Model;
					var url = attrs["url"];
					var config = {
						placeholder: 'Select option',
						allowClear: true,
						data: data,
						ajax: {
							url: url,
							delay: 250,
							data: function (params) {
								return {
									params: params.term,
									roleId: newValue
								};
							},
							processResults: function (data) {
								return {
									results: data
								};
							},
						}
					};
					$(element).select2(config);
				});

				$(element).on('change', function () {
					scope.$apply(function () {
						// 监控
						scope.select2Model = $(element).select2('data');
					});
				});
			}, 200);

		}
	};
});
app.directive('staffOtherActive', function ($timeout, $parse) {
	return {
		restrict: 'A',
		link: function (scope, element, attrs) {
			$timeout(function () {
				$(element).click(function () {
					$('#otherMenu li').removeClass('active');
					$(this).parent('li').addClass('active');
					var myStatus = $(this).attr('mystatus');
					scope.page.condition.myStatus = myStatus;
					scope.page.condition.pageIndex = 1;
					scope.search();
				});
			}, 200);
		}
	};
});
