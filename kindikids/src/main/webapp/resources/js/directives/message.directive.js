var app = angular.module('kindKidsApp');
app.directive("doActive", function ($timeout, $parse) {
	return {
		restrict: 'ACE',
		link: function (scope, element, attrs, ngModelCtrl) {
			$timeout(function () {
				$(element).bind('click', function () {
					var others = $(element).siblings();
					angular.forEach(others, function (ele, index, array) {
						$(ele).removeClass('active');
					});
					$(element).addClass('active');
					var value = $(element).val();
					try {
						scope.page.condition.readFlag = value;
						scope.page.condition.pageIndex = 1;
					} catch (e) {

					}
					scope.search();
				});
			}, 200);
		}
	};
});
app.directive("messagemenue", function ($timeout, $parse) {
	return {
		templateUrl: '../../views/message/message.menu.html',
		restrict: 'E',
		replace: true
	}
});
app.directive("download", function ($timeout, $parse) {
	return {
		restrict: 'ACE',
		scope: {
			filename: "=",
			baseUrl: "="
		},
		link: function (scope, element, attrs) {
			$timeout(function () {
				scope.downloadUrl = scope.baseUrl + encodeURIComponent(scope.filename);
				$(element).prop('href', scope.downloadUrl);
			}, 200);
		}
	};
});
app.directive("messageContent", function ($timeout, $parse) {
	return {
		restrict: 'ACE',
		scope: {
			content: "="
		},
		link: function (scope, element, attrs) {
			$timeout(function () {
				if (scope.content) {
					$(element).html(scope.content.replace(new RegExp(/</g), '&lt;'));
					$(element).html(scope.content.replace("</br>", "<br>"));
				}
			}, 200);
		}
	};
});
app.directive("replayForward", function ($timeout, $parse) {
	return {
		restrict: 'ACE',
		scope: {
			flag: "="
		},
		link: function (scope, element, attrs) {
			$timeout(function () {
				$("#forward").hide();
				$(element).click(function () {
					$(this).siblings().find('a').css("background-color", "#FCFCFC");
					if (scope.flag) {
						$("#forward").hide();
						$("#reply").show();
						$(this).find('a').css("background-color", "#E5E5E5");
					} else {
						$("#reply").hide();
						$("#forward").show();
						$(this).find('a').css("background-color", "#E5E5E5");
					}
				});
			}, 200);
		}
	};
});
app.directive("showMore", function ($timeout, $parse) {
	return {
		restrict: 'AEC',
		link: function (scope, ele, attr) {
			$timeout(function () {
				$(ele).click(function (event) {
					event.preventDefault();
					$(this).css('display', 'none');
					$('.shotern').css('display', 'inline-block');
				});
			}, 200);
		}
	};
});
app.directive("showMore2", function ($timeout, $parse) {
	return {
		restrict: 'AEC',
		link: function (scope, ele, attr) {
			$timeout(function () {
				$(ele).click(function (event) {
					event.preventDefault();
					$(this).css('display', 'none');
					$('.shotern2').css('display', 'inline');
				});
			}, 200);
		}
	};
});