var app = angular.module('kindKidsApp');

app.directive("staffCenter", function ($timeout, $parse, $filter) {
	return {
		restrict: 'AE',
		link: function (scope, element, attrs, ngModelCtrl) {
			$timeout(function () {
				var result = $filter('haveRole')(scope.currentUser.roleInfoVoList, '0;4;10');

				if (!scope.mainPage.centerId) {
					if (result) {
						//园长和ceo默认第一个center
						$($('.menu-block2 li')[0]).addClass('active');
						var initValue = $($('.menu-block2 a')[0]).attr('value');
						scope.mainPage.centerId = initValue;
						scope.$apply();
					} else {
						var userCenterId = scope.currentUser.userInfo.centersId;
						$('.menu-block2 a').map(function () {
							if ($(this).attr('value') == userCenterId) {
								$(this).parent('li').addClass('active');
								scope.mainPage.centerId = userCenterId;
								scope.$apply();
							}
						});
					}
				}
				var id = $(element).attr('value');
				if (id == scope.mainPage.centerId) {
					$(element).parent('li').addClass('active');
				}
				$(element).bind('click', function () {
					var type = $(element).attr('type');
					var value = $(element).attr('value');
					if (type == 0) {
						$('.menu-block0 li').removeClass('active');
						scope.mainPage.role = value;
					} else if (type == 1) {
						$('.menu-block1 li').removeClass('active');
						scope.mainPage.staffType = value;
					} else if (type == 2) {
						$('.menu-block2 li').removeClass('active');
						scope.mainPage.centerId = value;
					} else if (type == 3) {
						$('.menu-block3 li').removeClass('active');
						scope.mainPage.roster = value;
					} else {
						$('.menu-block4 li').removeClass('active');
						scope.mainPage.leaveType = value;
					}
					$(element).parent('li').addClass('active');
					scope.$apply();
				});
			}, 200);
		}
	};
});


app.directive('selectImageAjaxRoster', function ($timeout, $parse, $filter) {
	return {
		restrict: 'ACE',
		scope: {
			model: '=ngModel',
			list: '=',
			select2Model: '=',
			initSelect: '='
		},
		link: function (scope, element, attrs) {
			function template(data) {
				var tag = "";
				var targetTags = [{
					tagText: 'Certified Supervisor',
					tag: "CS"
				}, {
					tagText: 'First Aid Officer',
					tag: "FAO"
				}, {
					tagText: 'Nominated Supervisor',
					tag: "NS"
				}, {
					tagText: 'ECT',
					tag: "ECT"
				}, {
					tagText: 'Diploma',
					tag: "DIP"
				}, {
					tagText: 'Educational Leader',
					tag: "EL"
				}];
				if (data.roleInfoVoList) {
					if (!data.workDay) {
						tag += '<a class="label-general" style="height:20px;float:right;line-height:16px;color:#ff5f63;font-weight:bold;margin-top: 5px;"' +
							'>' + 'UNAVAILABLE' + '</a>';
					}
					if (data.staffType != null) {
						if (data.staffType == 0) {
							var staffType = 'FT';
						}
						if (data.staffType == 1) {
							var staffType = 'PT';
						}
						if (data.staffType == 2) {
							var staffType = 'Casual';
						}
						tag += '<a class="label-general"  style="height:20px;float:right;color:rgb(176,83,193);margin-top: 5px;line-height:16px;"' +
							'>' + staffType + '</a>';
					}
					for (var j = 0, n = data.roleInfoVoList.length; j < n; j++) {
						for (var i = 0, m = targetTags.length; i < m; i++) {
							if (targetTags[i].tagText == data.roleInfoVoList[j].name) {
								tag += '<a class="label-general"  style="height:20px;float:right;margin-top: 5px;line-height:16px;"' +
									'title="' + targetTags[i].tagText + '" >' + targetTags[i].tag + '</a>';
							}
						}
					}
					if (data.time) {
						tag += '<a class="label-general" style="height:20px;float:right;line-height:16px;font-weight:bold;color:#0B9942;margin-top: 5px;"' +
							'>' + data.time + '</a>';
					}
				}
				if (data.avatar) {
					return $('<span><img src="' + data.avatar + '" class="name-short name-image-short" /> ' + data.text + tag + '</span>');
				} else {
					// 初始化未选择
					if (data.id == '' && data.text == "Select option") {
						return $('<span></span> ');
					}
					// 判断无头像
					var imageName = $filter('userNameUpperCase')(data.text);
					if (!imageName) {
						return;
					}
					var color = data.personColor;
					if (color == "" || color == undefined) {
						color = "blue";
					}
					return $('<span><span class="name-short name-image-short ' + color + '">' + imageName + '</span> ' + data.text + tag + '</span>');
				}
			};

			function templateOption(data) {
				var tag = "";

				var targetTags = [{
					tagText: 'Certified Supervisor',
					tag: "CS"
				}, {
					tagText: 'First Aid Officer',
					tag: "FAO"
				}, {
					tagText: 'Nominated Supervisor',
					tag: "NS"
				}, {
					tagText: 'ECT',
					tag: "ECT"
				}, {
					tagText: 'Diploma',
					tag: "DIP"
				}, {
					tagText: 'Educational Leader',
					tag: "EL"
				}];
				if (data.roleInfoVoList) {
					if (!data.workDay) {
						tag += '<a class="label-general" style="height:20px;float:right;line-height:16px;color:#ff5f63;font-weight:bold;margin-right:10px;"' +
							'>' + 'UNAVAILABLE' + '</a>';
					}
					if (data.staffType != null) {
						if (data.staffType == 0) {
							var staffType = 'FT';
						}
						if (data.staffType == 1) {
							var staffType = 'PT';
						}
						if (data.staffType == 2) {
							var staffType = 'Casual';
						}
						tag += '<a class="label-general"  style="height:20px;float:right;color:rgb(176,83,193);line-height:16px;"' +
							'>' + staffType + '</a>';
					}
					for (var j = 0, n = data.roleInfoVoList.length; j < n; j++) {
						for (var i = 0, m = targetTags.length; i < m; i++) {
							if (targetTags[i].tagText == data.roleInfoVoList[j].name) {
								tag += '<a class="label-general"  style="height:20px;float:right;line-height:16px;"' +
									'title="' + targetTags[i].tagText + '" >' + targetTags[i].tag + '</a>';
							}
						}
					}
				}
				if (data.time) {
					tag += '<a class="label-general" style="height:20px;float:right;line-height:16px;font-weight:bold;color:#0B9942;margin-right:10px;"' +
						'>' + data.time + '</a>';
				}
				if (data.avatar) {
					return $('<span><img src="' + data.avatar + '" class="name-short name-image-short" /> ' + data.text + tag + '</span>');
				} else {
					// 初始化未选择
					if (data.id == '' && data.text == "Select option") {
						return $('<span></span> ');
					}
					// 判断无头像
					var imageName = $filter('userNameUpperCase')(data.text);
					if (!imageName) {
						return;
					}
					var color = data.personColor;
					if (color == "" || color == undefined) {
						color = "blue";
					}
					return $('<span><span class="name-short name-image-short ' + color + '">' + imageName + '</span> ' + data.text + tag + '</span>');
				}
			};;

			$(element).on('change', function () {
				scope.$apply(function () {
					// 监控
					var value = $(element).select2('data');
					scope.select2Model = value;
				});
			});

			$timeout(function () {
				var data = scope.select2Model;
				var url = attrs["url"];
				var config = {
					placeholder: 'Select option',
					data: data,
					ajax: {
						url: url,
						delay: 250,
						data: function (params) {
							return {
								p: params.term
							};
						},
						processResults: function (data) {
							return {
								results: data
							};
						},
					},
					templateResult: templateOption,
					templateSelection: template
				};

				$(element).select2(config);
			}, 200);
		}
	};
});