angular.module('kindKidsApp')
.directive("nqsChecked",function($timeout,$parse){
    return {
        restrict: 'ACE',
        scope: {
          newchoosenqsobj: '='
        },
        link: function(scope, element, attrs,ngModelCtrl) {
            $timeout(function() {
            	var modelValueOld=scope.newchoosenqsobj;
            	var versionOld=$(element).attr("version");
            	var newTag=$(element).attr("tag");
                for(var i=0,m=modelValueOld.length;i<m;i++){
                    //初始化选择状态
                    if(modelValueOld[i].version==versionOld&&modelValueOld[i].tag==newTag){
                        $(element).prop("checked",true);
                        break;
                    }
                }            				

				/*//当checkbox选中和取消时,更新选中集合
            	$(element).change(function(){
            		var modelValue=scope.newchoosenqsobj;
            		var version=$(element).attr("version");
            		var arrTag=$(element).attr("tag")*1;
                    var i=0;
                    var m=modelValue.length;
                    for(i=0;i<m;i++){
                        if(modelValue[i].version==versionOld&&arrTag==modelValue[i].tag){                            
                            break;
                        }
                    }  

            		if($(element).prop("checked")){
                        var content=$(element).attr("content");
                        var tag=$(element).attr("tag");
                        var nqs={"version":version,"content":content,"tag":tag};
            			//选中   
            			if(i>=m){
            				modelValue.push(nqs);
                        }         			
            		}else{
            			//取消选中
                        if(i<m){
                            modelValue.splice(i,1);
                        }             			
            		}           		

            		scope.newchoosenqsobj=modelValue; 
            	});         */       
                    
            },200); 
        }
    };
});