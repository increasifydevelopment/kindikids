'use strict';
var app = angular.module('kindKidsApp');
app.directive("select2", function ($timeout, $parse) {
	return {
		restrict: 'ACE',
		require: 'ngModel',
		scope: {
			select2Model: '=',
			model: '=ngModel'
		},
		link: function (scope, element, attrs) {
			$timeout(function () {
				$(element).select2();
				if (scope.select2Model == undefined) {
					var select2 = $(element).select2({
						placeholder: 'Select option'
					});
					scope.$watch('model', function (newValue) {
						// select2.val(scope.model);
						$(element).trigger('change.select2');
					});
				} else {
					scope.$watch('select2Model', function (newValue) {
						var data = scope.select2Model;
						var url = attrs["url"];
						var config = {
							placeholder: 'Select option',
							data: data
						};
						$(element).select2(config);
					});
				}

			});
		}
	};
});
app.directive('icheck', function ($timeout, $parse) {
	return {
		require: 'ngModel',
		link: function ($scope, element, $attrs, ngModelCtrl) {
			return $timeout(function () {
				var value;
				value = $attrs['value'];
				$scope.$watch($attrs['ngModel'], function (newValue) {
					$(element).iCheck('update');
				});
				$scope.$watch($attrs['ngDisabled'], function (newValue) {
					$(element).iCheck('update');
				});
				return $(element).iCheck({
					checkboxClass: 'icheckbox_minimal-pink',
					radioClass: 'iradio_minimal-pink',
					increaseArea: '30%' // optional

				}).on('ifChanged', function (event) {
					if ($attrs['ngModel']) {
						// 添加icheck选择后进行校验
						var form = $(element).parents("form")[0];
						if (form != undefined && form.length != 0) {
							var name = $(element).attr("name");
							var bootstrapData = $(form).data('bootstrapValidator');
							if (bootstrapData != undefined && name != undefined) {
								try {
									bootstrapData.updateStatus(name, 'VALIDATED', null).validateField(name);
								} catch (e) {}
							}
						}
						if ($(element).attr('type') === 'radio') {
							return $scope.$apply(function () {
								return ngModelCtrl.$setViewValue(value);
							});
						} else {
							return $scope.$apply(function () {
								return ngModelCtrl.$setViewValue(event.target.checked);
							});
						}
					}

				});
			});
		}
	};
});
app.directive('selectAjaxClear', function ($timeout, $parse) {
	return {
		restrict: 'ACE',
		scope: {
			model: '=ngModel',
			list: '=',
			select2Model: '=',
			initSelect: '=',
			max: '='
		},
		link: function (scope, element, attrs) {
			$.fn.modal.Constructor.prototype.enforceFocus = function () {};
			$timeout(function () {
				if (scope.initSelect == undefined) {
					var data = scope.select2Model;
					var url = attrs["url"];
					var config = {
						placeholder: 'Select option',
						data: data,
						maximumSelectionLength: scope.max,
						allowClear: true,
						ajax: {
							url: url,
							delay: 250,
							data: function (params) {

								if (!params.term) {
									$('.select2-results').addClass('hide');
									return {
										p: params.term
									};
								} else {
									$('.select2-results').removeClass('hide');
									return {
										p: params.term
									};
								}
							},
							processResults: function (data) {
								return {
									results: data
								};
							},
						}
					};
					$(element).select2(config);
				} else {
					scope.$watch('initSelect', function (newValue) {
						var data = scope.select2Model;
						// 初始化
						var url = attrs["url"];
						var config = {
							placeholder: 'Select option',
							data: data,
							ajax: {
								url: url,
								delay: 250,
								data: function (params) {
									return {
										p: params.term
									};
								},
								processResults: function (data) {
									return {
										results: data
									};
								}
							}
						};
						$(element).select2(config);
					});
				}

				$(element).on('change', function () {
					scope.$apply(function () {
						// 监控
						scope.select2Model = $(element).select2('data');
					});
				});
			}, 200);

		}
	};
});
app.directive('selectAjax', function ($timeout, $parse) {
	return {
		restrict: 'ACE',
		scope: {
			model: '=ngModel',
			list: '=',
			select2Model: '=',
			initSelect: '=',
			max: '=',
			relationValue: '@'
		},
		link: function (scope, element, attrs) {
			$.fn.modal.Constructor.prototype.enforceFocus = function () {};
			$timeout(function () {
				if (scope.initSelect == undefined) {
					var data = scope.select2Model;
					var url = attrs["url"];
					var config = {
						placeholder: 'Select option',
						data: data,
						maximumSelectionLength: scope.max,
						ajax: {
							url: url,
							delay: 250,
							data: function (params) {
								if (!params.term) {
									$('.select2-results').addClass('hide');
									return {
										p: params.term
									};
								} else {
									$('.select2-results').removeClass('hide');
									return {
										p: params.term
									};
								}
							},
							processResults: function (data) {
								return {
									results: data
								};
							},
						}
					};
					$(element).select2(config);
				} else {
					scope.$watch('initSelect', function (newValue) {
						var data = scope.select2Model;
						// 初始化
						var url = attrs["url"];
						var config = {
							placeholder: 'Select option',
							data: data,
							ajax: {
								url: url,
								delay: 250,
								data: function (params) {
									return {
										p: params.term
									};
								},
								processResults: function (data) {
									return {
										results: data
									};
								}
							}
						};
						$(element).select2(config);
					});
				}

				$(element).on('change', function () {
					scope.$apply(function () {
						// 监控
						scope.select2Model = $(element).select2('data');
					});
				});
			}, 200);

		}
	};
});
app.directive('selectAjax2', function ($timeout, $parse) {
	return {
		restrict: 'ACE',
		scope: {
			model: '=ngModel',
			list: '=',
			select2Model: '=',
			initSelect: '=',
			max: '='
		},
		link: function (scope, element, attrs) {
			$.fn.modal.Constructor.prototype.enforceFocus = function () {};
			$timeout(function () {
				if (scope.initSelect == undefined) {
					var data = scope.select2Model;
					var url = attrs["url"];
					var config = {
						placeholder: 'Select option',
						data: data,
						maximumSelectionLength: scope.max,
						ajax: {
							url: url,
							delay: 250,
							data: function (params) {

								if (!params.term) {
									$('.select2-results').addClass('hide');
									return {
										p: params.term
									};
								} else {
									$('.select2-results').removeClass('hide');
									return {
										p: params.term
									};
								}
							},
							processResults: function (data) {
								return {
									results: data
								};
							},
						}
					};
					$(element).select2(config);
				} else {
					scope.$watch('initSelect', function (newValue) {
						var data = scope.select2Model;
						// 初始化
						var url = attrs["url"];
						var config = {
							placeholder: 'Select option',
							data: data,
							ajax: {
								url: url,
								delay: 250,
								data: function (params) {
									return {
										p: params.term
									};
								},
								processResults: function (data) {
									return {
										results: data
									};
								}
							}
						};
						$(element).select2(config);
					});
				}

				$(element).on('change', function () {
					scope.$apply(function () {
						// 监控
						scope.select2Model = $(element).select2('data');
						for (var i = 0; i < scope.select2Model.length; i++) {
							var item = scope.select2Model[i];
							if (item.disabled != null && item.disabled == false) {
								continue;
							}
							scope.model.push(item.id);
						}
					});
				});
			}, 200);

		}
	};
});

app.directive('selectAjax3', function ($timeout, $parse) {
	return {
		restrict: 'ACE',
		scope: {
			model: '=ngModel',
			list: '=',
			select2Model: '=',
			initSelect: '=',
			max: '=',
			relationValue: '@'
		},
		link: function (scope, element, attrs) {
			$.fn.modal.Constructor.prototype.enforceFocus = function () {};
			$timeout(function () {
				if (scope.initSelect == undefined) {
					var data = scope.select2Model;
					var url = attrs["url"];
					var config = {
						placeholder: 'Select option',
						data: data,
						maximumSelectionLength: scope.max,
						ajax: {
							url: url,
							delay: 250,
							data: function (params) {
								return {
									p: params.term
								};
							},
							processResults: function (data) {
								return {
									results: data
								};
							},
						}
					};
					$(element).select2(config);
				} else {
					scope.$watch('initSelect', function (newValue) {
						var data = scope.select2Model;
						// 初始化
						var url = attrs["url"];
						var config = {
							placeholder: 'Select option',
							data: data,
							ajax: {
								url: url,
								delay: 250,
								data: function (params) {
									return {
										p: params.term
									};
								},
								processResults: function (data) {
									return {
										results: data
									};
								}
							}
						};
						$(element).select2(config);
					});
				}

				$(element).on('change', function () {
					scope.$apply(function () {
						// 监控
						scope.select2Model = $(element).select2('data');
					});
				});
			}, 200);

		}
	};
});

app.directive("summernote", function ($timeout, $parse) {
	return {
		restrict: 'ACE',
		require: 'ngModel',
		scope: {
			initValue: "="
		},
		link: function (scope, element, attrs, ngModelCtrl) {
			$timeout(function () {
				var placeholder = attrs["placeholder"];
				try {
					// Text editor initialize
					$(element).summernote({
						disableDragAndDrop: true, // 禁止拖拽
						placeholder: placeholder,
						height: 200, // set editor height
						minHeight: null, // set minimum height of editor
						maxHeight: null, // set maximum height of editor
						focus: true, // set focus to editable area after
						// initializing summernote
						toolbar: [
							// [groupName, [list of button]]
							['style', ['bold', 'italic', 'clear']]
						],
						callbacks: {
							onChange: function (contents, $editable) {
								ngModelCtrl.$setViewValue(contents);
							}
						}
					});
				} catch (e) {
					console.log(e);
				}
				scope.$watch('initValue', function (newValue) {
					$(element).summernote('code', newValue)
				});

			}, 200);
		}
	};
});
app.directive("tooltip", function ($timeout, $parse) {
	return {
		restrict: 'AC',
		link: function (scope, element, attrs) {
			$timeout(function () {

				if ($(element).attr("data-toggle") == "tooltip") {
					$('[data-toggle="tooltip"]').tooltip();
				}
				if ($(element).attr("data-toggle") == "tooltip-cus") {
					$(element).tooltip({
						template: '<div class="tooltip tooltip-auto"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
					});
				}
				if ($(element).attr("data-toggle") == "tooltip-menu") {
					$(element).tooltip({
						template: '<div class="tooltip tooltip-menu"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
					});
				}

			});
		}
	};
});

app.directive('repeatDone', function () {
	return {
		link: function (scope, element, attrs) {
			if (scope.$last) { // 这个判断意味着最后一个 OK
				scope.$eval(attrs.repeatDone) // 执行绑定的表达式
			}
		}
	}
});

app.directive('nqszindex', function ($timeout, $parse) {
	return {
		link: function (scope, element, attrs) {

			$timeout(function () {
				/*
				 * var modaldiv = $(element).parent().parent().parent("div");
				 * var zindex = parseInt(modaldiv.css("z-index"));
				 * modaldiv.css("z-index", zindex + 100);
				 * modaldiv.prev().css("z-index", zindex + 50);
				 */
			});

		}
	};
});
app.directive("mydownload", function ($timeout, $parse) {
	return {
		restrict: 'AC',
		require: 'ngModel',
		scope: {
			file: "="
		},
		link: function (scope, element, attrs) {
			$timeout(function () {
				$(element).click(function () {
					alert();
				});
			}, 100);

		}
	};
});
app.directive("dateMask", function ($timeout, $parse) {
	return {
		restrict: 'AC',
		require: 'ngModel',
		link: function (scope, element, attrs, ngModelCtrl) {
			// 去除settimeout解决IE浏览器打开页面提示验证错误
			$(element).mask("00/00/0000", {
				placeholder: "__/__/____",
				onComplete: function (val, e, el, options) {
					// 自动更改时间为有效日期
					var m = new moment(val, 'DD/MM/YYYY', true);
					var d = m._d;
					var yyyy = d.getFullYear();
					var mm = (d.getMonth() + 1) < 10 ? "0" + (d.getMonth() + 1) : d.getMonth() + 1;
					var dd = d.getDate() < 10 ? "0" + d.getDate() : d.getDate();
					var changeVal = dd + "/" + mm + "/" + yyyy;
					if (val != changeVal) {
						val = changeVal;
						$(element).val(val);
					}
					ngModelCtrl.$setViewValue(val);

				}
			});
		}
	};
});
// 日历控件在手动输入的时候，ngmodel值会更改，所有通过该参数区分是输入更改，还是编辑回来后，更改的ngmodel
var isInput = true;
// add by zhengguo
app.directive('myDatePicker',
	function ($timeout, $filter) {
		return {
			restrict: 'ACE',
			require: 'ngModel',
			link: function (scope, element, attrs, ngModelCtrl) {

				$timeout(
					function () {
						var picker = undefined;
						var isInput = false;
						scope.$watch(attrs['ngModel'], function (newValue) {
							if (ngModelCtrl.$viewValue == "Invalid date") {
								ngModelCtrl.$setViewValue(undefined);
								if (picker != undefined) {
									$(element).data('daterangepicker').setStartDate("");
								}
							}
							if (isInput) {
								if (ngModelCtrl.$viewValue == "") {
									$(element).data('daterangepicker').setStartDate("");
								}
								return;
							}
							if (ngModelCtrl.$viewValue != undefined) {
								if (picker != undefined) {
									$(element).data('daterangepicker').setStartDate($filter('date')(ngModelCtrl.$viewValue, "dd/MM/yyyy"));
								}
							}
						});

						var startDate = "";
						if (ngModelCtrl.$viewValue != undefined && ngModelCtrl.$viewValue != null) {
							startDate = $filter('date')(ngModelCtrl.$viewValue, "dd/MM/yyyy");
						}

						// 输入时不更新ngmodel
						$(element).on("keydown", function (e) {
							isInput = true;
						});

						picker = $(element).daterangepicker({
								locale: {
									format: "DD/MM/YYYY"
								},
								singleDatePicker: true,
								showDropdowns: true,
								startDate: startDate,
								drops: "auto"
							}).on("showCalendar.daterangepicker", function () {
								// 在staff_leave页面因为被表格样式影响，增加样式控制
								$(element).parent().find('thead').find('th').css({
									'background-color': '#fff',
									'color': '#333333'
								});
								$(element).parent().find('td').css({
									'white-space': 'normal'
								});
							})
							.on(
								"hide.daterangepicker",
								function () {
									isInput = false;
									// 校验
									var _this = $(this);
									if (_this.val() == "Invalid date") {
										ngModelCtrl.$setViewValue("");
									} else {
										ngModelCtrl.$setViewValue(_this.val());
									}
									$(element).trigger("keyup");

									/**
									 * *******************校验
									 * staffLeave 页面
									 * addLeaveForm start
									 * ************************
									 */

									if (_this.attr('name') == 'fromDate') {
										// 校验前清空之前的校验
										$('#addLeaveForm').bootstrapValidator('updateStatus', 'fromDate', 'NOT_VALIDATED');
										if (_this.val()) {
											var sDate = _this.val().substr(6) + '/' + _this.val().substr(3, 2) + '/' +
												_this.val().substr(0, 2);
											var standDate = new Date(sDate);

											if (scope.leaveCondition.type) {
												// 年假和长假
												// fromDate校验为周一
												if (scope.leaveCondition.type == 2) {
													if (standDate.getDay() == 1) {
														$('#addLeaveForm').bootstrapValidator('updateStatus', 'fromDate', 'VALID');
													} else {
														$('#addLeaveForm').data('bootstrapValidator').updateStatus('fromDate',
															'INVALID', 'digits');
													}
												} else {
													// 普通请假，校验不能选择周六周日
													if (standDate.getDay() == 0 || standDate.getDay() == 6) {
														$('#addLeaveForm').data('bootstrapValidator').updateStatus('fromDate',
															'INVALID', 'date');
													} else {
														$('#addLeaveForm').bootstrapValidator('updateStatus', 'fromDate', 'VALID');
													}
												}
											}
											if (scope.leaveCondition.endDate) {
												var mDate = scope.leaveCondition.endDate.substr(6) + '/' +
													scope.leaveCondition.endDate.substr(3, 2) + '/' +
													scope.leaveCondition.endDate.substr(0, 2);
												var mStandDate = new Date(mDate);
												// 校验请假截止日期大于等于开始日期
												if (mStandDate >= standDate) {
													// 年假和长假
													// toDate校验为周五
													if (scope.leaveCondition.type == 2) {
														if (mStandDate.getDay() == 5) {
															$('#addLeaveForm').bootstrapValidator('updateStatus', 'toDate', 'VALID');
														} else {
															$('#addLeaveForm').data('bootstrapValidator').updateStatus('toDate',
																'INVALID', 'digits');
														}
													}
												} else {
													$('#addLeaveForm').data('bootstrapValidator').updateStatus('fromDate', 'INVALID',
														'greaterThan');
												}
											}
											/*
											 * else {
											 * $('#addLeaveForm').data('bootstrapValidator').updateStatus('toDate',
											 * 'INVALID','notEmpty'); }
											 */
										} else {
											// 校验fromDate
											$('#addLeaveForm').data('bootstrapValidator').updateStatus('fromDate', 'INVALID',
												'notEmpty');
										}
									}

									if (_this.attr('name') == 'toDate') {
										// 校验前清空之前的校验
										$('#addLeaveForm').bootstrapValidator('updateStatus', 'toDate', 'NOT_VALIDATED');
										if (_this.val()) {
											var sDate = _this.val().substr(6) + '/' + _this.val().substr(3, 2) + '/' +
												_this.val().substr(0, 2);
											var standDate = new Date(sDate);

											if (scope.leaveCondition.type) {
												// 年假和长假
												// toDate校验为周五
												if (scope.leaveCondition.type == 2) {
													if (standDate.getDay() == 5) {
														$('#addLeaveForm').bootstrapValidator('updateStatus', 'toDate', 'VALID');
													} else {
														$('#addLeaveForm').data('bootstrapValidator').updateStatus('toDate',
															'INVALID', 'digits');
													}
												} else {
													// 普通请假，校验不能选择周六周日
													if (standDate.getDay() == 0 || standDate.getDay() == 6) {
														$('#addLeaveForm').data('bootstrapValidator').updateStatus('toDate',
															'INVALID', 'date');
													} else {
														$('#addLeaveForm').bootstrapValidator('updateStatus', 'toDate', 'VALID');
													}
												}
											}

											if (scope.leaveCondition.startDate) {
												var mDate = scope.leaveCondition.startDate.substr(6) + '/' +
													scope.leaveCondition.startDate.substr(3, 2) + '/' +
													scope.leaveCondition.startDate.substr(0, 2);
												var mStandDate = new Date(mDate);
												// 校验请假截止日期大于等于开始日期
												if (mStandDate <= standDate) {
													// 年假和长假
													// fromDate校验为周一
													if (scope.leaveCondition.type == 2) {
														if (mStandDate.getDay() == 1) {
															$('#addLeaveForm')
																.bootstrapValidator('updateStatus', 'fromDate', 'VALID');
														} else {
															$('#addLeaveForm').data('bootstrapValidator').updateStatus('fromDate',
																'INVALID', 'digits');
														}
													}
												} else {
													$('#addLeaveForm').data('bootstrapValidator').updateStatus('toDate', 'INVALID',
														'greaterThan');
												}
											} else {
												$('#addLeaveForm').data('bootstrapValidator').updateStatus('fromDate', 'INVALID',
													'notEmpty');
											}
										} else {
											// 校验toDate
											$('#addLeaveForm').data('bootstrapValidator').updateStatus('toDate', 'INVALID',
												'notEmpty');
										}
									}

									/**
									 * *******************校验
									 * staffLeave 页面
									 * addLeaveForm end
									 * ************************
									 */

									/**
									 * *******************校验
									 * myLeave 页面 applyLeaveForm
									 * start
									 * ************************
									 */

									if (_this.attr('name') == 'startDate') {
										if (_this.val()) {
											var sDate = _this.val().substr(6) + '/' + _this.val().substr(3, 2) + '/' +
												_this.val().substr(0, 2);
											var standDate = new Date(sDate);

											var nowFullDate = new Date();
											var nowDate = new Date(nowFullDate.getFullYear() + '/' + (nowFullDate.getMonth() + 1) +
												'/' + nowFullDate.getDate());
											// 必须大于当前日期
											if (standDate < nowDate) {
												$('#applyLeaveForm').bootstrapValidator('updateStatus', 'startDate', 'VALID');
												$('#applyLeaveForm').data('bootstrapValidator').updateStatus('startDate', 'INVALID',
													'lessThan');
											} else {
												$('#applyLeaveForm').bootstrapValidator('updateStatus', 'startDate', 'VALID');

												if (scope.leaveCondition.type) {
													// 年假和长假
													// fromDate校验为周一
													if (scope.leaveCondition.type == 2) {
														if (standDate.getDay() == 1) {
															$('#applyLeaveForm').bootstrapValidator('updateStatus', 'startDate',
																'VALID');
														} else {
															$('#applyLeaveForm').data('bootstrapValidator').updateStatus('startDate',
																'INVALID', 'digits');
														}
													} else {
														// 普通请假，校验不能选择周六周日
														if (standDate.getDay() == 0 || standDate.getDay() == 6) {
															$('#applyLeaveForm').data('bootstrapValidator').updateStatus('startDate',
																'INVALID', 'different');
														} else {
															$('#applyLeaveForm').bootstrapValidator('updateStatus', 'startDate',
																'VALID');
														}
													}
												}
											}

											if (scope.leaveCondition.endDate) {
												var mDate = scope.leaveCondition.endDate.substr(6) + '/' +
													scope.leaveCondition.endDate.substr(3, 2) + '/' +
													scope.leaveCondition.endDate.substr(0, 2);
												var mStandDate = new Date(mDate);
												// 校验请假截止日期大于等于开始日期
												if (mStandDate >= standDate) {
													// 年假和长假
													// toDate校验为周五
													if (scope.leaveCondition.type == 2) {
														if (mStandDate.getDay() == 5) {
															$('#applyLeaveForm').bootstrapValidator('updateStatus', 'endDate',
																'VALID');
														} else {
															$('#applyLeaveForm').data('bootstrapValidator').updateStatus('endDate',
																'INVALID', 'digits');
														}
													}
												} else {
													$('#applyLeaveForm').data('bootstrapValidator').updateStatus('startDate',
														'INVALID', 'greaterThan');
												}
											}
											/*
											 * else {
											 * $('#applyLeaveForm').data('bootstrapValidator').updateStatus('endDate',
											 * 'INVALID','notEmpty'); }
											 */
										} else {
											// 校验fromDate
											$('#applyLeaveForm').data('bootstrapValidator').updateStatus('startDate', 'INVALID',
												'notEmpty');
										}
									}

									if (_this.attr('name') == 'endDate') {
										if (_this.val()) {
											var sDate = _this.val().substr(6) + '/' + _this.val().substr(3, 2) + '/' +
												_this.val().substr(0, 2);
											var standDate = new Date(sDate);

											if (scope.leaveCondition.type) {
												// 年假和长假
												// toDate校验为周五
												if (scope.leaveCondition.type == 2) {
													if (standDate.getDay() == 5) {
														$('#applyLeaveForm').bootstrapValidator('updateStatus', 'endDate', 'VALID');
													} else {
														$('#applyLeaveForm').data('bootstrapValidator').updateStatus('endDate',
															'INVALID', 'digits');
													}
												} else {
													// 普通请假，校验不能选择周六周日
													if (standDate.getDay() == 0 || standDate.getDay() == 6) {
														$('#applyLeaveForm').data('bootstrapValidator').updateStatus('endDate',
															'INVALID', 'different');
													} else {
														$('#applyLeaveForm').bootstrapValidator('updateStatus', 'endDate', 'VALID');
													}
												}
											}

											if (scope.leaveCondition.startDate) {
												$('#applyLeaveForm').bootstrapValidator('updateStatus', 'startDate', 'VALID');
												var mDate = scope.leaveCondition.startDate.substr(6) + '/' +
													scope.leaveCondition.startDate.substr(3, 2) + '/' +
													scope.leaveCondition.startDate.substr(0, 2);
												var mStandDate = new Date(mDate);

												var nowFullDate = new Date();
												var nowDate = new Date(nowFullDate.getFullYear() + '/' + (nowFullDate.getMonth() + 1) +
													'/' + nowFullDate.getDate());
												// 必须大于当前日期
												if (mStandDate < nowDate) {
													$('#applyLeaveForm').bootstrapValidator('updateStatus', 'startDate', 'VALID');
													$('#applyLeaveForm').data('bootstrapValidator').updateStatus('startDate',
														'INVALID', 'lessThan');
												} else {
													$('#applyLeaveForm').bootstrapValidator('updateStatus', 'startDate', 'VALID');
													// 校验请假截止日期大于等于开始日期
													if (mStandDate <= standDate) {
														// 年假和长假
														// fromDate校验为周一
														if (scope.leaveCondition.type == 2) {
															if (mStandDate.getDay() == 1) {
																$('#applyLeaveForm').bootstrapValidator('updateStatus', 'startDate',
																	'VALID');
															} else {
																$('#applyLeaveForm').data('bootstrapValidator').updateStatus(
																	'startDate', 'INVALID', 'digits');
															}
														}
													} else {
														$('#applyLeaveForm').data('bootstrapValidator').updateStatus('endDate',
															'INVALID', 'greaterThan');
													}
												}
											} else {
												$('#applyLeaveForm').data('bootstrapValidator').updateStatus('startDate', 'INVALID',
													'notEmpty');
											}
										} else {
											// 校验toDate
											$('#applyLeaveForm').data('bootstrapValidator').updateStatus('endDate', 'INVALID',
												'notEmpty');
										}
									}

									/**
									 * *******************校验
									 * myLeave 页面 applyLeaveForm
									 * end
									 * ************************
									 */

								});

						$(element).mask("00/00/0000", {
							onComplete: function (val, e, el, options) {
								// 自动更改时间为有效日期
								var m = new moment(val, 'DD/MM/YYYY', true);
								var d = m._d;
								var yyyy = d.getFullYear();
								var mm = (d.getMonth() + 1) < 10 ? "0" + (d.getMonth() + 1) : d.getMonth() + 1;
								var dd = d.getDate() < 10 ? "0" + d.getDate() : d.getDate();
								var changeVal = dd + "/" + mm + "/" + yyyy;
								if (val != changeVal) {
									val = changeVal;
									$(element).val(val);
									$(element).trigger("keyup");
								}
								ngModelCtrl.$setViewValue(val);
								try {
									var form = $(element).parents("form")[0];
									if (form != undefined && form.length != 0) {
										var name = $(element).attr("name");
										var bootstrapData = $(form).data('bootstrapValidator');
										if (bootstrapData != undefined && name != undefined) {
											bootstrapData.updateStatus(name, 'VALIDATED', null).validateField(name);
										}
									}
								} catch (e) {

								}

								isInput = false;
							}
						});

					}, 200);
			}
		}
	});
// add by zhengguo
app.directive('myDatePicker2',
	function ($timeout, $filter) {
		return {
			restrict: 'ACE',
			require: 'ngModel',
			scope: {
				func: '=',
				obj: '=',
				callback: '='
			},
			link: function (scope, element, attrs, ngModelCtrl) {
				scope.changeModel = function () {
					if (typeof (scope.func) != 'function') {
						return;
					}
					if (typeof (scope.obj) != "object") {
						return;
					}
					scope.func.call(this, scope.obj);
				};
				$timeout(
					function () {
						var picker = undefined;
						var isInput = false;
						scope.$watch(attrs['ngModel'], function (newValue) {
							if (ngModelCtrl.$viewValue == "Invalid date") {
								ngModelCtrl.$setViewValue(undefined);
								if (picker != undefined) {
									$(element).data('daterangepicker').setStartDate("");
								}
							}
							if (isInput) {
								if (ngModelCtrl.$viewValue == "") {
									$(element).data('daterangepicker').setStartDate("");
								}
								return;
							}
							if (ngModelCtrl.$viewValue != undefined) {
								if (picker != undefined) {
									$(element).data('daterangepicker').setStartDate($filter('date')(ngModelCtrl.$viewValue, "dd/MM/yyyy"));
								}
							}
						});

						var startDate = "";
						if (ngModelCtrl.$viewValue != undefined && ngModelCtrl.$viewValue != null) {
							startDate = $filter('date')(ngModelCtrl.$viewValue, "dd/MM/yyyy");
						}

						// 输入时不更新ngmodel
						$(element).on("keydown", function (e) {
							isInput = true;
						});

						picker = $(element).daterangepicker({
								locale: {
									format: "DD/MM/YYYY"
								},
								singleDatePicker: true,
								showDropdowns: true,
								startDate: startDate,
								drops: "auto"
								
							}).on("showCalendar.daterangepicker", function () {
								// 在staff_leave页面因为被表格样式影响，增加样式控制
								$(element).parent().find('thead').find('th').css({
									'background-color': '#fff',
									'color': '#333333'
								});
								$(element).parent().find('td').css({
									'white-space': 'normal'
								});
							})
							.on(
								"hide.daterangepicker",
								function () {
									if (scope.callback) {
										scope.callback();
									}
									isInput = false;
									// 校验
									var _this = $(this);
									if (_this.val() == "Invalid date") {
										ngModelCtrl.$setViewValue("");
									} else {
										ngModelCtrl.$setViewValue(_this.val());
									}
									scope.changeModel();
									$(element).trigger("keyup");

									/**
									 * *******************校验
									 * staffLeave 页面
									 * addLeaveForm start
									 * ************************
									 */

									if (_this.attr('name') == 'fromDate') {
										// 校验前清空之前的校验
										$('#addLeaveForm').bootstrapValidator('updateStatus', 'fromDate', 'NOT_VALIDATED');
										if (_this.val()) {
											$('#addLeaveForm').bootstrapValidator('updateStatus', 'fromDate', 'VALID');
											var sDate = _this.val().substr(6) + '/' + _this.val().substr(3, 2) + '/' +
												_this.val().substr(0, 2);
											var standDate = new Date(sDate);

											if (scope.leaveCondition.type) {
												// 年假和长假
												// fromDate校验为周一
												if (scope.leaveCondition.type == 2) {
													if (standDate.getDay() == 1) {
														$('#addLeaveForm').bootstrapValidator('updateStatus', 'fromDate', 'VALID');
													} else {
														$('#addLeaveForm').data('bootstrapValidator').updateStatus('fromDate',
															'INVALID', 'digits');
													}
												} else {
													// 普通请假，校验不能选择周六周日
													if (standDate.getDay() == 0 || standDate.getDay() == 6) {
														$('#addLeaveForm').data('bootstrapValidator').updateStatus('fromDate',
															'INVALID', 'date');
													} else {
														$('#addLeaveForm').bootstrapValidator('updateStatus', 'fromDate', 'VALID');
													}
												}
											}
											if (scope.leaveCondition.endDate) {
												$('#addLeaveForm').bootstrapValidator('updateStatus', 'toDate', 'VALID');
												var mDate = scope.leaveCondition.endDate.substr(6) + '/' +
													scope.leaveCondition.endDate.substr(3, 2) + '/' +
													scope.leaveCondition.endDate.substr(0, 2);
												var mStandDate = new Date(mDate);
												// 校验请假截止日期大于等于开始日期
												if (mStandDate >= standDate) {
													// 年假和长假
													// toDate校验为周五
													if (scope.leaveCondition.type == 2) {
														if (mStandDate.getDay() == 5) {
															$('#addLeaveForm').bootstrapValidator('updateStatus', 'toDate', 'VALID');
														} else {
															$('#addLeaveForm').data('bootstrapValidator').updateStatus('toDate',
																'INVALID', 'digits');
														}
													}
												} else {
													$('#addLeaveForm').data('bootstrapValidator').updateStatus('fromDate', 'INVALID',
														'greaterThan');
												}
											}
											/*
											 * else {
											 * $('#addLeaveForm').data('bootstrapValidator').updateStatus('toDate',
											 * 'INVALID','notEmpty'); }
											 */
										} else {
											// 校验fromDate
											$('#addLeaveForm').data('bootstrapValidator').updateStatus('fromDate', 'INVALID',
												'notEmpty');
										}
									}

									if (_this.attr('name') == 'toDate') {
										// 校验前清空之前的校验
										$('#addLeaveForm').bootstrapValidator('updateStatus', 'toDate', 'NOT_VALIDATED');
										if (_this.val()) {
											$('#addLeaveForm').bootstrapValidator('updateStatus', 'toDate', 'VALID');
											var sDate = _this.val().substr(6) + '/' + _this.val().substr(3, 2) + '/' +
												_this.val().substr(0, 2);
											var standDate = new Date(sDate);

											if (scope.leaveCondition.type) {
												// 年假和长假
												// toDate校验为周五
												if (scope.leaveCondition.type == 2) {
													if (standDate.getDay() == 5) {
														$('#addLeaveForm').bootstrapValidator('updateStatus', 'toDate', 'VALID');
													} else {
														$('#addLeaveForm').data('bootstrapValidator').updateStatus('toDate',
															'INVALID', 'digits');
													}
												} else {
													// 普通请假，校验不能选择周六周日
													if (standDate.getDay() == 0 || standDate.getDay() == 6) {
														$('#addLeaveForm').data('bootstrapValidator').updateStatus('toDate',
															'INVALID', 'date');
													} else {
														$('#addLeaveForm').bootstrapValidator('updateStatus', 'toDate', 'VALID');
													}
												}
											}

											if (scope.leaveCondition.startDate) {
												$('#addLeaveForm').bootstrapValidator('updateStatus', 'fromDate', 'VALID');
												var mDate = scope.leaveCondition.startDate.substr(6) + '/' +
													scope.leaveCondition.startDate.substr(3, 2) + '/' +
													scope.leaveCondition.startDate.substr(0, 2);
												var mStandDate = new Date(mDate);
												// 校验请假截止日期大于等于开始日期
												if (mStandDate <= standDate) {
													// 年假和长假
													// fromDate校验为周一
													if (scope.leaveCondition.type == 2) {
														if (mStandDate.getDay() == 1) {
															$('#addLeaveForm')
																.bootstrapValidator('updateStatus', 'fromDate', 'VALID');
														} else {
															$('#addLeaveForm').data('bootstrapValidator').updateStatus('fromDate',
																'INVALID', 'digits');
														}
													}
												} else {
													$('#addLeaveForm').data('bootstrapValidator').updateStatus('toDate', 'INVALID',
														'greaterThan');
												}
											} else {
												$('#addLeaveForm').data('bootstrapValidator').updateStatus('fromDate', 'INVALID',
													'notEmpty');
											}
										} else {
											// 校验toDate
											$('#addLeaveForm').data('bootstrapValidator').updateStatus('toDate', 'INVALID',
												'notEmpty');
										}
									}

									/**
									 * *******************校验
									 * staffLeave 页面
									 * addLeaveForm end
									 * ************************
									 */

									/**
									 * *******************校验
									 * myLeave 页面 applyLeaveForm
									 * start
									 * ************************
									 */

									if (_this.attr('name') == 'startDate') {
										if (_this.val()) {
											$('#applyLeaveForm').bootstrapValidator('updateStatus', 'startDate', 'VALID');
											var sDate = _this.val().substr(6) + '/' + _this.val().substr(3, 2) + '/' +
												_this.val().substr(0, 2);
											var standDate = new Date(sDate);

											var nowFullDate = new Date();
											var nowDate = new Date(nowFullDate.getFullYear() + '/' + (nowFullDate.getMonth() + 1) +
												'/' + nowFullDate.getDate());
											// 必须大于当前日期
											if (standDate < nowDate) {
												$('#applyLeaveForm').bootstrapValidator('updateStatus', 'startDate', 'VALID');
												$('#applyLeaveForm').data('bootstrapValidator').updateStatus('startDate', 'INVALID',
													'lessThan');
											} else {
												$('#applyLeaveForm').bootstrapValidator('updateStatus', 'startDate', 'VALID');

												if (scope.leaveCondition.type) {
													// 年假和长假
													// fromDate校验为周一
													if (scope.leaveCondition.type == 2) {
														if (standDate.getDay() == 1) {
															$('#applyLeaveForm').bootstrapValidator('updateStatus', 'startDate',
																'VALID');
														} else {
															$('#applyLeaveForm').data('bootstrapValidator').updateStatus('startDate',
																'INVALID', 'digits');
														}
													} else {
														// 普通请假，校验不能选择周六周日
														if (standDate.getDay() == 0 || standDate.getDay() == 6) {
															$('#applyLeaveForm').data('bootstrapValidator').updateStatus('startDate',
																'INVALID', 'different');
														} else {
															$('#applyLeaveForm').bootstrapValidator('updateStatus', 'startDate',
																'VALID');
														}
													}
												}
											}

											if (scope.leaveCondition.endDate) {
												$('#applyLeaveForm').bootstrapValidator('updateStatus', 'endDate', 'VALID');
												var mDate = scope.leaveCondition.endDate.substr(6) + '/' +
													scope.leaveCondition.endDate.substr(3, 2) + '/' +
													scope.leaveCondition.endDate.substr(0, 2);
												var mStandDate = new Date(mDate);
												// 校验请假截止日期大于等于开始日期
												if (mStandDate >= standDate) {
													// 年假和长假
													// toDate校验为周五
													if (scope.leaveCondition.type == 2) {
														if (mStandDate.getDay() == 5) {
															$('#applyLeaveForm').bootstrapValidator('updateStatus', 'endDate',
																'VALID');
														} else {
															$('#applyLeaveForm').data('bootstrapValidator').updateStatus('endDate',
																'INVALID', 'digits');
														}
													}
												} else {
													$('#applyLeaveForm').data('bootstrapValidator').updateStatus('startDate',
														'INVALID', 'greaterThan');
												}
											}
											/*
											 * else {
											 * $('#applyLeaveForm').data('bootstrapValidator').updateStatus('endDate',
											 * 'INVALID','notEmpty'); }
											 */
										} else {
											// 校验fromDate
											$('#applyLeaveForm').data('bootstrapValidator').updateStatus('startDate', 'INVALID',
												'notEmpty');
										}
									}

									if (_this.attr('name') == 'endDate') {
										if (_this.val()) {
											$('#applyLeaveForm').bootstrapValidator('updateStatus', 'endDate', 'VALID');
											var sDate = _this.val().substr(6) + '/' + _this.val().substr(3, 2) + '/' +
												_this.val().substr(0, 2);
											var standDate = new Date(sDate);

											if (scope.leaveCondition.type) {
												// 年假和长假
												// toDate校验为周五
												if (scope.leaveCondition.type == 2) {
													if (standDate.getDay() == 5) {
														$('#applyLeaveForm').bootstrapValidator('updateStatus', 'endDate', 'VALID');
													} else {
														$('#applyLeaveForm').data('bootstrapValidator').updateStatus('endDate',
															'INVALID', 'digits');
													}
												} else {
													// 普通请假，校验不能选择周六周日
													if (standDate.getDay() == 0 || standDate.getDay() == 6) {
														$('#applyLeaveForm').data('bootstrapValidator').updateStatus('endDate',
															'INVALID', 'different');
													} else {
														$('#applyLeaveForm').bootstrapValidator('updateStatus', 'endDate', 'VALID');
													}
												}
											}

											if (scope.leaveCondition.startDate) {
												$('#applyLeaveForm').bootstrapValidator('updateStatus', 'startDate', 'VALID');
												var mDate = scope.leaveCondition.startDate.substr(6) + '/' +
													scope.leaveCondition.startDate.substr(3, 2) + '/' +
													scope.leaveCondition.startDate.substr(0, 2);
												var mStandDate = new Date(mDate);

												var nowFullDate = new Date();
												var nowDate = new Date(nowFullDate.getFullYear() + '/' + (nowFullDate.getMonth() + 1) +
													'/' + nowFullDate.getDate());
												// 必须大于当前日期
												if (mStandDate < nowDate) {
													$('#applyLeaveForm').bootstrapValidator('updateStatus', 'startDate', 'VALID');
													$('#applyLeaveForm').data('bootstrapValidator').updateStatus('startDate',
														'INVALID', 'lessThan');
												} else {
													$('#applyLeaveForm').bootstrapValidator('updateStatus', 'startDate', 'VALID');
													// 校验请假截止日期大于等于开始日期
													if (mStandDate <= standDate) {
														// 年假和长假
														// fromDate校验为周一
														if (scope.leaveCondition.type == 2) {
															if (mStandDate.getDay() == 1) {
																$('#applyLeaveForm').bootstrapValidator('updateStatus', 'startDate',
																	'VALID');
															} else {
																$('#applyLeaveForm').data('bootstrapValidator').updateStatus(
																	'startDate', 'INVALID', 'digits');
															}
														}
													} else {
														$('#applyLeaveForm').data('bootstrapValidator').updateStatus('endDate',
															'INVALID', 'greaterThan');
													}
												}
											} else {
												$('#applyLeaveForm').data('bootstrapValidator').updateStatus('startDate', 'INVALID',
													'notEmpty');
											}
										} else {
											// 校验toDate
											$('#applyLeaveForm').data('bootstrapValidator').updateStatus('endDate', 'INVALID',
												'notEmpty');
										}
									}

									/**
									 * *******************校验
									 * myLeave 页面 applyLeaveForm
									 * end
									 * ************************
									 */

								});

						$(element).mask("00/00/0000", {
							onComplete: function (val, e, el, options) {
								// 自动更改时间为有效日期
								var m = new moment(val, 'DD/MM/YYYY', true);
								var d = m._d;
								var yyyy = d.getFullYear();
								var mm = (d.getMonth() + 1) < 10 ? "0" + (d.getMonth() + 1) : d.getMonth() + 1;
								var dd = d.getDate() < 10 ? "0" + d.getDate() : d.getDate();
								var changeVal = dd + "/" + mm + "/" + yyyy;
								if (val != changeVal) {
									val = changeVal;
									$(element).val(val);
									$(element).trigger("keyup");
								}
								ngModelCtrl.$setViewValue(val);
								try {
									var form = $(element).parents("form")[0];
									if (form != undefined && form.length != 0) {
										var name = $(element).attr("name");
										var bootstrapData = $(form).data('bootstrapValidator');
										if (bootstrapData != undefined && name != undefined) {
											bootstrapData.updateStatus(name, 'VALIDATED', null).validateField(name);
										}
									}
								} catch (e) {

								}

								isInput = false;
							}
						});

					}, 200);
			}
		}
	});

// add by zhengguo
app.directive('selectImageAjax', function ($timeout, $parse, $filter) {
	return {
		restrict: 'ACE',
		scope: {
			model: '=ngModel',
			list: '=',
			select2Model: '=',
			initSelect: '='
		},
		link: function (scope, element, attrs) {
			function template(data) {
				if (data.avatar) {
					return $('<span><img src="' + data.avatar + '" class="name-short name-image-short" /> ' + data.text + '</span>');
				} else {
					// 初始化未选择
					if (data.id == '' && data.text == "Select option") {
						return $('<span></span> ');
					}
					// 判断无头像
					var imageName = $filter('userNameUpperCase')(data.text);
					if (!imageName) {
						return;
					}
					var color = data.personColor;
					if (color == "" || color == undefined) {
						color = "blue";
					}
					return $('<span><span class="name-short name-image-short ' + color + '">' + imageName + '</span> ' + data.text + '</span>');
				}
			};

			$(element).on('change', function () {
				scope.$apply(function () {
					// 监控
					var value = $(element).select2('data');
					scope.select2Model = value;
				});
			});

			$timeout(function () {
				var data = scope.select2Model;
				var url = attrs["url"];
				// 追加设置选择条目限制参数
				if ($(element).attr('maxsize')) {
					var config = {
						placeholder: 'Select option',
						data: data,
						maximumSelectionLength: $(element).attr('maxsize'),
						ajax: {
							url: url,
							delay: 250,
							data: function (params) {
								return {
									p: params.term
								};
							},
							processResults: function (data) {
								return {
									results: data
								};
							},
						},
						templateResult: template,
						templateSelection: template
					};
				} else {
					var config = {
						placeholder: 'Select option',
						data: data,
						ajax: {
							url: url,
							delay: 250,
							data: function (params) {
								return {
									p: params.term
								};
							},
							processResults: function (data) {
								return {
									results: data
								};
							},
						},
						templateResult: template,
						templateSelection: template
					};
				}
				$(element).select2(config);
			}, 700);
		}
	};
});

app.directive("imageselect", function ($timeout, $parse, $filter) {
	return {
		restrict: 'ACE',
		scope: {
			model: '=ngModel',
			select2Model: '='
		},
		link: function (scope, element, attrs) {

			function showSelectOption(obj) {
				var $state = "";
				// 判断有头像
				if (obj.avatar) {
					$state = $('<span><img  src="' + obj.avatar + '" class="name-short name-image-short" /> ' + obj.text + '</span>');
				} else {

					// 判断无头像
					var imageName = $filter('userNameUpperCase')(obj.text);
					if (!imageName) {
						return;
					}
					var color = obj.personColor;
					if (color == "" || color == undefined) {
						color = "blue";
					}
					$state = $('<span ><span class="name-short name-image-short ' + color + '">' + imageName + '</span> ' + obj.text + '</span>');
				}
				return $state;
			}

			function formatState(state) {
				var $state = showSelectOption(state);
				return $state;
			};

			function template(data, container) {

				var $state = showSelectOption(data);

				return $state;
			};
			$timeout(function () {
				var data = scope.select2Model;
				if (data) {
					for (var i = 0, m = data.length; i < m; i++) {
						if (data[i].id == scope.model) {
							data[i].selected = true;
						} else {
							data[i].selected = false;
						}
					}
				}
				var config = {
					placeholder: 'Select option',
					data: data,
					templateResult: formatState,
					templateSelection: template
				};
				$(element).select2(config);
			});
		}
	};
});

// add by zhengguo
app.directive('myDatePickerTimer', function ($timeout, $filter) {
	return {
		restrict: 'ACE',
		require: 'ngModel',
		link: function (scope, element, attrs, ngModelCtrl) {
			$timeout(function () {
				$(element).datetimepicker({
					format: 'LT',
					widgetPositioning: {
						vertical: "auto",
						horizontal: "left"
					}
				}).on("dp.hide", function () {
					// 赋值
					var _this = $(this);
					// 当时间选择下午PM时，小时选择到12时，直接默认'12:00
					// PM'
					// TODO
					/*
					 * if (_this.val().substr(6, 2) == 'PM' &&
					 * _this.val().substr(0, 2) == '12') { _this.val('11:59
					 * PM'); }
					 */
					ngModelCtrl.$setViewValue(_this.val());
					$(element).trigger("keyup");
					if (_this.val() && attrs["name"] == "signinTime") {
						$('#editSignForm').data('bootstrapValidator').updateStatus('signinTime', 'VALID', 'notEmpty');
					}
					if (_this.val() && attrs["name"] == "rosterTimeStart") {
						$('#staffSignForm').data('bootstrapValidator').updateStatus('rosterTimeStart', 'VALID', 'notEmpty');
					}
					if (_this.val() && attrs["name"] == "rosterTimeEnd") {
						$('#staffSignForm').data('bootstrapValidator').updateStatus('rosterTimeEnd', 'VALID', 'notEmpty');
					}
					if (_this.val() && attrs["name"] == "rosterTimeStart1") {
						$('#addSignForm').data('bootstrapValidator').updateStatus('rosterTimeStart1', 'VALID', 'notEmpty');
					}
					if (_this.val() && attrs["name"] == "rosterTimeEnd1") {
						$('#addSignForm').data('bootstrapValidator').updateStatus('rosterTimeEnd1', 'VALID', 'notEmpty');
					}
				})
			});
			$.fn.datetimepicker.defaults.widgetPositioning.vertical = 'bottom';
		}
	}
});

app.directive('datePickerZ', function ($timeout, $filter) {
	return {
		restrict: 'ACE',
		require: 'ngModel',
		scope: {
			model: '=ngModel'
		},
		link: function (scope, element, attrs, ngModelCtrl) {
			$timeout(function () {

				if (ngModelCtrl.$viewValue != undefined && ngModelCtrl.$viewValue != null) {
					scope.model = $filter('date')(ngModelCtrl.$viewValue, "dd/MM/yyyy");
				}

				$(element).datetimepicker({
					format: 'DD/MM/YYYY',
					widgetPositioning: {
						vertical: "auto",
						horizontal: "left"
					}
				}).on("dp.hide", function () {
					// 赋值
					var _this = $(this);
					ngModelCtrl.$setViewValue(_this.val());
					$(element).trigger("keyup");
				})
			});
			$.fn.datetimepicker.defaults.widgetPositioning.vertical = 'bottom';
		}
	}
});

app.directive('datePickerTimerZ', function ($timeout, $filter) {
	return {
		restrict: 'ACE',
		require: 'ngModel',
		scope: {
			model: '=ngModel'
		},
		link: function (scope, element, attrs, ngModelCtrl) {
			$timeout(function () {

				if (ngModelCtrl.$viewValue != undefined && ngModelCtrl.$viewValue != null) {
					scope.model = $filter('date')(ngModelCtrl.$viewValue, "dd/MM/yyyy HH:mm");
				}

				$(element).datetimepicker({
					format: 'DD/MM/YYYY HH:mm',
					widgetPositioning: {
						vertical: "auto",
						horizontal: "left"
					}
				}).on("dp.hide", function () {
					// 赋值
					var _this = $(this);
					ngModelCtrl.$setViewValue(_this.val());
					$(element).trigger("keyup");
				})
			});
			$.fn.datetimepicker.defaults.widgetPositioning.vertical = 'bottom';
		}
	}
});

app.directive('datePickerNoYear', function ($timeout, $filter) {
	return {
		restrict: 'ACE',
		require: 'ngModel',
		link: function (scope, element, attrs, ngModelCtrl) {
			$timeout(function () {
				$(element).datetimepicker({
					format: 'DD/MM',
				}).on("dp.hide", function () {
					// 赋值
					var _this = $(this);
					ngModelCtrl.$setViewValue(_this.val());
					$(element).trigger("keyup");
				});
			});
		}
	};
});
// newsFeed时间控件
app.directive('timeRange', function ($timeout) {
	return {
		restrict: 'AC',
		scope: {
			starttime: "=",
			endtime: "=",
			callback: '='
		},
		link: function (scope, element, attrs) {

			$(element).daterangepicker({
				locale: {
					format: "DD/MM/YYYY"
				},
				// singleDatePicker:true,
				showDropdowns: true
			}, function (start, end, label) {
				// 自动更改时间为有效日期
				dataFormat(scope, start, end);
				if (scope.callback) {
					scope.callback();
				}

			}).on('cancel.daterangepicker', function (ev, picker) {
				$(element).data('daterangepicker').setStartDate('');
				$(element).data('daterangepicker').setEndDate('');
				scope.starttime = null;
				scope.endtime = null;
				$(element).val('');
				if (scope.callback) {
					scope.callback(true);
				}
			}).on('apply.daterangepicker', function (ev, picker) {
				if ($(element).val() == "Invalid date - Invalid date") {
					$(element).data('daterangepicker').setStartDate('');
					$(element).data('daterangepicker').setEndDate('');
					scope.starttime = null;
					scope.endtime = null;
					$(element).val('');
				}
				if ($(element).val() != "") {
					dataFormat(scope, picker.startDate, picker.endDate);
					if (scope.callback) {
						scope.callback();
					}
				}
			});

			$timeout(function () {
				scope.$watch('starttime', function (newValue) {

					if (newValue != undefined && newValue != null) {
						console.log("2222222222222222");
						var m = new moment(newValue, "YYYY-MM-DDTHH:mm:ss.SSS");
						$(element).data('daterangepicker').setStartDate(m);
					} else {
						$(element).val('');
					}
				});
				scope.$watch('endtime', function (newValue) {
					if (newValue != undefined && newValue != null) {
						var m = new moment(newValue, "YYYY-MM-DDTHH:mm:ss.SSS");
						$(element).data('daterangepicker').setEndDate(m);
					} else {
						$(element).val('');
					}
				});

			});

		}

	}
});

app.directive('timeRangeSingle', function ($timeout) {
	return {
		restrict: 'AC',
		scope: {
			date: "=",
			callback: '='
		},
		link: function (scope, element, attrs) {

			$(element).daterangepicker({
				locale: {
					format: "DD/MM/YYYY",
				},
				autoApply: true,
				singleDatePicker: true,
				showDropdowns: true
			}, function (date, label) {
				// 自动更改时间为有效日期
				dataFormatSingle(scope, date);
				if (scope.callback) {
					scope.callback();
				}

			}).on('hide.daterangepicker', function (ev, picker) {
				var m = new moment(scope.date, "YYYY-MM-DDTHH:mm:ss.SSS");
				$(element).data('daterangepicker').setStartDate(m);
			})

			// $timeout(function () {
			// 	scope.$watch('date', function (newValue) {
			// 		if (newValue != undefined && newValue != null) {
			// 			var m = new moment(newValue, "YYYY-MM-DDTHH:mm:ss.SSS");
			// 			$(element).data('daterangepicker').setStartDate(m);
			// 		} else {
			// 			$(element).val('');
			// 		}
			// 	});
			// });
		}
	}
});


function dataFormatSingle(scope, time) {
	var d = time._d;
	var yyyy = d.getFullYear();
	var mm = (d.getMonth() + 1) < 10 ? "0" + (d.getMonth() + 1) : d.getMonth() + 1;
	var dd = d.getDate() < 10 ? "0" + d.getDate() : d.getDate();
	var date = yyyy + "-" + mm + "-" + dd + "T00:00:00.000";
	scope.date = date;
}

function dataFormat(scope, sDate, eDate) {
	var d = sDate._d;
	var yyyy = d.getFullYear();
	var mm = (d.getMonth() + 1) < 10 ? "0" + (d.getMonth() + 1) : d.getMonth() + 1;
	var dd = d.getDate() < 10 ? "0" + d.getDate() : d.getDate();
	var start = yyyy + "-" + mm + "-" + dd + "T00:00:00.000";
	var d = eDate._d;
	var yyyy = d.getFullYear();
	var mm = (d.getMonth() + 1) < 10 ? "0" + (d.getMonth() + 1) : d.getMonth() + 1;
	var dd = d.getDate() < 10 ? "0" + d.getDate() : d.getDate();
	var end = yyyy + "-" + mm + "-" + dd + "T00:00:00.000";
	scope.$apply(function () {
		scope.starttime = start;
		scope.endtime = end;
	});
}

app.directive('taginput', function ($timeout, $parse) {
	return {
		restrict: 'E',
		replace: true,
		scope: {
			model: '=ngModel',
			interInfo: '=',
		},
		require: 'ngModel',
		template: '<input type="text" posetcodeInput class="tags tags-input" data-type="tags" />',
		link: function (scope, element, attrs, ngModelCtrl) {
			// 初始化tagInput
			function initTagInput() {
				var tagsType = $(element).data('type')
				var highlightColor = $(element).data('highlight-color')
				if (tagsType === 'tags') {

					$(element).tagsInput({
						width: 'auto',
						onChange: function () {
							var value = $(element).val();
							ngModelCtrl.$setViewValue(value);
						},
						interactive: scope.interInfo,
						onRemoveTag: scope.removeFun
					});

					$timeout(function () {
						if (scope.interInfo == false) {
							// 手动隐藏叉号按钮，实现不允许删除
							var arr = $(element).parent().find('.tag');
							for (var i = 0; i < arr.length; i++) {
								$($(arr[i]).find('a')[0]).addClass('hidden');
							}
						}
					}, 200);
				}
			};
			var list = [];

			initTagInput();

			// 覆盖默认的$render方法
			ngModelCtrl.$render = function () {
				var modelValue = ngModelCtrl.$viewValue;
				if (modelValue != undefined) {
					list = modelValue.split(",");
					$(element).importTags(modelValue);
				}
			};
		}
	}
});

app.directive("loadHtmlCommon", function ($timeout, $parse) {
	return {
		restrict: 'AC',
		link: function (scope, element, attrs) {
			$timeout(function () {
				var html = attrs["html"];
				$(element).html(html);
			});
		}
	};
});

app.directive("initsubclick", function ($timeout, $parse) {
	return {
		restrict: 'AC',
		link: function (scope, element, attrs) {
			$timeout(function () {
				$(element).click(function () {
					$('.show-popup').removeClass('show-popup');
					$(this).parents('.side-item').find('.sub-popup').addClass('show-popup');
					$('.overlap-for-menu').addClass('show');
					$('body').addClass('overflow-hidden');
				});

				$(element).parent().find('.sub-popup a').click(function () {
					$(this).parents('.sub-popup').removeClass('show-popup');
					$('.overlap-for-menu').removeClass('show');
					$('body').addClass('overflow-hidden');
				});
			}, 300);
		}
	};
});

app.directive("initoverlapmenu", function ($timeout, $parse) {
	return {
		restrict: 'AC',
		link: function (scope, element, attrs) {
			$timeout(function () {
				$(element).click(function () {
					$(this).parents('.sub-popup').removeClass('show-popup');
					$('.overlap-for-menu').removeClass('show');
					$('body').addClass('overflow-hidden');
					$('.show-popup').removeClass('show-popup');
				});
			}, 300);
		}
	};
});

app.directive("loadHtml", function ($timeout, $parse) {
	return {
		restrict: 'AC',
		scope: {
			contentlength: '=contentlength'
		},
		link: function (scope, element, attrs) {
			$timeout(function () {
				scope.$watch('contentlength', function (newValue) {
					var html = attrs["loadHtml"];
					$(element).html(html);
				});
			});

		}
	};
});

//open toggle
app.directive('toggleBox', function () {
	return {
		restrict: 'ACE',
		link: function (scope, element, attrs) {
			$(element).click(function () {
				$(this).next(".collapse").toggleClass("in").parent(".panel-default-tab").siblings(".panel-default-tab").children(".collapse").removeClass("in");
			});
		}
	}
});

app.directive('radioChecked', function () {
	return {
		restrict: 'ACE',
		require: 'ngModel',
		scope: {
			model: '=ngModel',
			value: '=value'
		},
		link: function (scope, element, attrs) {
			if (scope.value == scope.model) {
				$(element).parents('.radio').addClass('checked');
			}
			$(element).click(function () {
				$(this).parents('.radio').toggleClass('checked').siblings('.radio').removeClass('checked');
				console.log(scope.value);
				if ($(this).parents('.radio').hasClass('checked')) {
					scope.model = scope.value;
				} else {
					scope.model = null;
				}
				scope.$apply(attrs.ngModel);
			});
		}
	}
});