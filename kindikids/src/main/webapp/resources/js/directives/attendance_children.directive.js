'use strict';
var app = angular.module('kindKidsApp');
app.directive('orgActive', function($timeout, $parse,$filter) {
  return {
    restrict : 'A',
    link : function(scope, element, attrs) {
      $timeout(function() {
        var id = $(element).attr('value');
        if(id&&(id==scope.page.condition.centersId || scope.page.condition.roomId==id)){
          $(element).parent('li').addClass('active');
        }
        $(element).click(function() {
          var callbackfun=$('.Location ul').attr("clickFun");
          $('.Location li').removeClass('active');
          $(this).parent('li').addClass('active');
          var type = $(this).attr('mtype');
          var id = $(this).attr('value');
          if (type == 0) {
            scope.page.condition.roomId = '';
            scope.page.condition.centersId = '';
          } else if (type == 1) {
            scope.page.condition.roomId="";
            scope.page.condition.centersId = id;
          } else {
            scope.page.condition.roomId = id;
            scope.page.condition.centersId = '';
          }
          scope.page.condition.type=4;
          eval(callbackfun);          
        });
      }, 200);
    }
  };
});