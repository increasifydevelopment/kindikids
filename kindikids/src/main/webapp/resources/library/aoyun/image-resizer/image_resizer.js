(function(ng){
    'use strict';

    // NOTE: Dynamic languages allow to compare Objects with a boolean value, however, it is not a good practice.
    //  typeof some_concrete_object always returns object and instanceof doesn't always work.

    /**
     * @param option
     * @returns {boolean}
     * @private
     */
    function validate_input(option) {
        var reason = '';
        var is_valid = false;

        if (!option.image) {
            reason = 'ERROR: Please pass a file.';
        } else if (!option.max_width && !option.max_height) {
            reason = 'ERROR: Please pass the width or height in pixels to be resized.';
        } else if (/^image\//.test(option.image.type) === false) {
            reason = 'ERROR: Not an image file.';
        } else {
            is_valid = true;
        }

        return {
            reason: reason,
            is_valid: is_valid
        }
    }


    /**
     * TODO: Implement the max height feature as well
     * @param option
     * @returns {{width: *, height: *}}
     * @private
     */
    function get_resize_dimension(option) {
        var width = option.width;
        var height = option.height;
        var max_width = option.max_width;

        var final_width = width;
        var final_height = height;

        if (width > max_width) {
            final_width = max_width;
            final_height = Math.ceil(height / width * max_width)
        }

        return {
            width: final_width,
            height: final_height
        }
    }

    var resizer = function($q, image_reader_service, image_resize_service){
        return {
            resize: function(option) {
                return $q(function(resolve, reject) {
                    var validation_helper = validate_input(option);

                    if (!validation_helper.is_valid) {
                        return reject(validation_helper.reason);
                    }

                    var image = option.image;
                    var file_name=image.name;

                    image_reader_service.read_as_base64(image).then(function(data_url){
                        // TODO: Do a garbage collection or find a cleaner way to get width and height out of
                        //  the file object
                        var img = document.createElement('img');
                        img.src = data_url;

                        img.onload = function(event) {
                            var resized_dimension = get_resize_dimension({
                                width: img.naturalWidth,
                                height: img.naturalHeight,
                                max_width: option.max_width
                            });

                            image_resize_service.resizeImage(data_url, resized_dimension, function(err, resized_image_data_url) {
                                if (err) {
                                    console.error(err);
                                    return;
                                }

                                var resized_image_parts = resized_image_data_url.split(';base64,');

                                var file_parts = option.image.name.split('.');
                                var file_type = file_parts[file_parts.length - 1];
                                var resized_image = resized_image_parts[1];

                                var resized_file = {
                                    file_type: file_type,
                                    base64_resized_image: resized_image,
                                    file_name:file_name
                                };

                                resolve(resized_file);
                            });
                        };

                    }, function(err){
                        console.error(err);
                    });
                });
            }
        };
    };

    var module = ng.module("resizer_app", ['lib-images-resizer', 'lib-base64-images-reader']);
    module.service("image_resizer",resizer);
}(angular));
