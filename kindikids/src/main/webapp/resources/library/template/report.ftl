<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0"/>
    <!-- The above 3 meta tags-children-list *must* come first in the head; any other head content must come *after* these tags-children-list -->
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <title>Child Report</title>
    <!-- Bootstrap core CSS -->
  	<!--   <link href="../styles/bootstrap.css" rel="stylesheet"> -->
  	<!--   <link href="styles/custombox.css" rel="stylesheet"> -->
    <!-- Custom styles for this template -->
    <link href="../styles/report.css" rel="stylesheet" />
    <link href="../styles/print.css" rel="stylesheet" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
<body>
<div class="printWrap">
    <div class="print-header">
    	${ReportVo.avatar}
    	<span>${ReportVo.name}</span>
    </div>
    <div class="text-center heading">
     ${ReportVo.title}
    </div>
	<#list ReportVo.list as obj>
		<h4 class="sub-heading">${obj.version}. ${obj.content}</h4>
		<table style="width:100%;" class="report-table">
		<#list obj.eylfCheckList as eylf>
				<tr>
            		<td width="75%;">${eylf.content}</td>
            		<#switch eylf.value>
					<#case 0>
						<td width="25%" class="report-status text-right"><span class="disabled">Achieved</span></td>
						<#break>
					<#case 1>
						<td class="report-status text-right"><span class="disabled">Not Achieved</span></td>
						<#break>
					<#case 2>
						<td class="report-status text-right"><span>Not Assessed</span></td>
						<#break>
					</#switch>
       		  </tr>
       		  <#if eylf.value == 2> 
       		  	<tr>
            		<td colspan="2">
                		<div class="report-reason"><strong>Reason:</strong>${eylf.reason}</div>
            		</td>
            	</tr>
              </#if>
        </#list>
        </table>
	</#list>
</div>
</body>
</html>