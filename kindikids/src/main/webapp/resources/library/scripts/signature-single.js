
//The following code for render multiple signature pad
var wrapper1 = document.getElementById("signature-pad-1"),
    clearButton1 = wrapper1.querySelector("[data-action=clear1]"),
    canvas1 = wrapper1.querySelector("canvas"),
    signaturePad1;



function resizeCanvas(canvas) {
    var ratio =  window.devicePixelRatio || 1;
    canvas.width = canvas.offsetWidth * ratio;
    canvas.height = canvas.offsetHeight * ratio;
    canvas.getContext("2d").scale(ratio, ratio);
}

resizeCanvas(canvas1);
signaturePad1 = new SignaturePad(canvas1);
clearButton1.addEventListener("click", function (event) {
    signaturePad1.clear();
});


//Modal box 
$('.modal').on('shown.bs.modal', function (e) {    
	 resizeCanvas(canvas1);
});
