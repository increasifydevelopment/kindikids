 $(function(){
    var nqsStaffPie = {
        type: 'doughnut',
        data: {
            labels: [
                "QA 1. Educational program and practice",
                "QA 2. Children's health and safety",
                "QA 3. Physical environment",
                "QA 4. Staffing arrangements",
                "QA 5. Relationships with children",
                "QA 6. Collaborative partnerships with families and communities",
                "QA 7. Leadership and service management",
            ],
            datasets: [
                {
                    data: [10, 10, 10, 10, 10, 15, 35],
                    backgroundColor: [
                        "#43B74F",
                        "#FFD522",
                        "#E991A5",
                        "#ABD25F",
                        "#FAA21B",
                        "#E2178F",
                        "#1595D3"
                    ],
                    hoverBackgroundColor: [
                        "#43B74F",
                        "#FFD522",
                        "#E991A5",
                        "#ABD25F",
                        "#FAA21B",
                        "#E2178F",
                        "#1595D3"
                    ]
                }]
        },
        options: {
            responsive: true,
            legend: {
                position: 'left',
            },
            title: {
                display: true,
                text: 'NQS % Comparison'
            },
            animation: {
                animateScale: true,
                animateRotate: true
            },
            tooltips: {
                   callbacks: {
                        label: function(tooltipItem, data) {
                            var allData = data.datasets[tooltipItem.datasetIndex].data;
                            var tooltipLabel = data.labels[tooltipItem.index];
                            var tooltipData = allData[tooltipItem.index];
                            var total = 0;
                            for (var i in allData) {
                                total += allData[i];
                            }
                            var tooltipPercentage = Math.round((tooltipData / total) * 100);
                            return tooltipLabel + ': ' + tooltipPercentage + '%';
                        }
                    }
                   
            }
        }
    };

    var nqsStaffBar = {
            labels: [
                "QA 1. Educational program and practice",
                "QA 2. Children's health and safety",
                "QA 3. Physical environment",
                "QA 4. Staffing arrangements",
                "QA 5. Relationships with children",
                "QA 6. Collaborative partnerships with families and communities",
                "QA 7. Leadership and service management",
            ],
            datasets: [
            {   
                backgroundColor: [
                    "#43B74F",
                    "#FFD522",
                    "#E991A5",
                    "#ABD25F",
                    "#FAA21B",
                    "#E2178F",
                    "#1595D3"
                ],
                borderColor: [
                    "#43B74F",
                    "#FFD522",
                    "#E991A5",
                    "#ABD25F",
                    "#FAA21B",
                    "#E2178F",
                    "#1595D3"
                ],
                data: [150, 100, 200, 120, 140, 155, 240]
            }
        ]
    };

    var nqsChildPercentage = {
            labels: ["Average", "Child 1", "Child 2", "Child 3", "Child 4", "Child 5", "Child 6"],
            datasets: [{
                label: "QA 1. Educational program and practice",
                backgroundColor: "#43B74F",
                data: [15, 20, 10, 10, 10, 15, 20]
            }, {
                label: "QA 2. Children's health and safety",
                backgroundColor: "#FFD522",
                data: [15, 10, 15, 15, 20, 10, 10]
            }, {
                label: 'QA 3. Physical environment',
                backgroundColor: "#E991A5",
                data: [10, 10, 15, 15, 15, 10, 10]
            },{
                label: 'QA 4. Staffing arrangements',
                backgroundColor: "#ABD25F",
                data: [15, 10, 15, 15, 15, 20, 15]
            },{
                label: 'QA 5. Relationships with children',
                backgroundColor: "#FAA21B",
                data: [20, 10, 15, 15, 10, 5, 15]
            },{
                label: 'QA 6. Collaborative partnerships with families and communities',
                backgroundColor: "#E2178F",
                data: [5, 10, 15, 15, 15, 30, 20]
            },{
                label: 'QA 7. Leadership and service management',
                backgroundColor: "#1595D3",
                data: [20, 30, 15, 15, 15, 10, 10]
            }]
    };

    var nqsChildNumber = {
            labels: ["Average", "Child 1", "Child 2", "Child 3", "Child 4", "Child 5", "Child 6"],
            datasets: [{
                label: "QA 1. Educational program and practice",
                backgroundColor: "#43B74F",
                data: [20, 10, 10, 10, 10, 15, 40]
            }, {
                label: "QA 2. Children's health and safety",
                backgroundColor: "#FFD522",
                data: [15, 10, 15, 15, 20, 10, 10]
            }, {
                label: 'QA 3. Physical environment',
                backgroundColor: "#E991A5",
                data: [5, 10, 15, 15, 15, 10, 10]
            },{
                label: 'QA 4. Staffing arrangements',
                backgroundColor: "#ABD25F",
                data: [5, 10, 15, 15, 15, 20, 15]
            },{
                label: 'QA 5. Relationships with children',
                backgroundColor: "#FAA21B",
                data: [5, 10, 45, 15, 10, 5, 15]
            },{
                label: 'QA 6. Collaborative partnerships with families and communities',
                backgroundColor: "#E2178F",
                data: [5, 10, 15, 15, 15, 30, 20]
            },{
                label: 'QA 7. Leadership and service management',
                backgroundColor: "#1595D3",
                data: [5, 30, 15, 15, 15, 10, 10]
            }]
    };

    var childActData1 = {
            labels: ["Average", "Child 1", "Child 2", "Child 3", "Child 4", "Child 5", "Child 6"],
            datasets: [{
                label: "Obesevation",
                backgroundColor: "#52ADDA",
                data: [15, 20, 10, 10, 10, 15, 20]
            }, {
                label: "Obsevation Follow Up",
                backgroundColor: "#68B8DF",
                data: [15, 10, 15, 30, 35, 10, 10]
            }, {
                label: 'Interests',
                backgroundColor: "#C9C9C9",
                data: [10, 40, 30, 15, 15, 10, 10]
            },{
                label: 'Interest Follow Ups',
                backgroundColor: "#DBDBDB",
                data: [35, 10, 15, 15, 15, 20, 15]
            },{
                label: 'Learning Story',
                backgroundColor: "#AACD4B",
                data: [20, 10, 15, 15, 10, 15, 25]
            },{
                label: 'Learning Story Follow Ups',
                backgroundColor: "#B5D954",
                data: [5, 10, 15, 15, 15, 30, 20]
            }]
    };

    var childActData2 = {
            labels: ["Average", "Child 1", "Child 2", "Child 3", "Child 4", "Child 5", "Child 6"],
            datasets: [{
                label: "Obesevation",
                backgroundColor: "#52ADDA",
                data: [15, 20, 10, 10, 10, 15, 20]
            }, {
                label: "Obsevation Follow Up",
                backgroundColor: "#68B8DF",
                data: [15, 10, 15, 50, 35, 10, 10]
            }, {
                label: 'Interests',
                backgroundColor: "#C9C9C9",
                data: [10, 40, 30, 15, 35, 10, 10]
            },{
                label: 'Interest Follow Ups',
                backgroundColor: "#DBDBDB",
                data: [35, 10, 15, 45, 15, 20, 15]
            },{
                label: 'Learning Story',
                backgroundColor: "#AACD4B",
                data: [20, 10, 15, 15, 10, 15, 25]
            },{
                label: 'Learning Story Follow Ups',
                backgroundColor: "#B5D954",
                data: [5, 10, 15, 15, 15, 30, 55]
            }]
    };

    var staffTaskData1 = {
            labels: ["Average", "Staff 1", "Staff 2", "Staff 3", "Staff 4", "Staff 5", "Staff 6"],
            datasets: [{
                label: "Center Tasks",
                backgroundColor: "#002C3D",
                data: [35, 18, 15, 25, 25, 25, 25]
            }, {
                label: "Room Tasks",
                backgroundColor: "#00668F",
                data: [25, 22, 15, 25, 25, 25, 25]
            }, {
                label: 'Staff Tasks',
                backgroundColor: "#00AFF5",
                data: [22, 25, 60, 25, 25, 25, 25]
            },{
                label: 'Other Tasks',
                backgroundColor: "#5CD1FF",
                data: [18, 35, 10, 25, 25, 25, 25]
            }]
    };

    var staffTaskData2 = {
            labels: ["Average", "Staff 1", "Staff 2", "Staff 3", "Staff 4", "Staff 5", "Staff 6"],
            datasets: [{
                label: "Center Tasks",
                backgroundColor: "#002C3D",
                data: [35, 18, 15, 25, 25, 25, 25]
            }, {
                label: "Room Tasks",
                backgroundColor: "#00668F",
                data: [25, 22, 15, 25, 25, 25, 35]
            }, {
                label: 'Staff Tasks',
                backgroundColor: "#00AFF5",
                data: [22, 25, 60, 25, 25, 25, 25]
            },{
                label: 'Other Tasks',
                backgroundColor: "#5CD1FF",
                data: [18, 35, 10, 25, 25, 25, 25]
            }]
    };


    window.onload = function() {
        var ctx1 = document.getElementById("canvas-nqs-1").getContext("2d");
        window.nqsStaffDoughnutChart = new Chart(ctx1, nqsStaffPie);

        var ctx2 = document.getElementById("canvas-nqs-2").getContext("2d");
        window.nqsStaffNumberBarChart = new Chart(ctx2, {
                type: 'horizontalBar',
                data: nqsStaffBar,
                options: {
                    elements: {
                        rectangle: {
                            borderWidth: 2,
                            borderColor: 'rgb(0, 255, 0)',
                            borderSkipped: 'bottom'
                        }
                    },
                    responsive: true,
                    legend: {
                        // position: 'top',
                        display:false
                    },
                    title: {
                        display: true,
                        text: 'NQS Number Comparison'
                    }
                }
        });

        var ctxChild1 = document.getElementById("canvas-child-nqs-1").getContext("2d");
            window.nqsChildPercentChart = new Chart(ctxChild1, {
                type: 'bar',
                data: nqsChildPercentage,
                options: {
                    title:{
                        display:true,
                        text:"Comparison of the percentage of activities completed for each child"
                    },
                    tooltips: {
                        mode: 'label'
                    },
                    responsive: true,
                    scales: {
                        xAxes: [{
                            stacked: true,
                        }],
                        yAxes: [{
                            stacked: true,
                            ticks: {
                                // Create scientific notation labels
                                callback: function(value, index, values) {
                                    return value + ' %';
                                }
                            },
                        }]
                    },
                    legend:{
                        position:'top',
                        fullWidth: true
                    }
                }
        });

        var ctxChild2 = document.getElementById("canvas-child-nqs-2").getContext("2d");
            window.nqsChildAmountChart = new Chart(ctxChild2, {
                type: 'bar',
                data: nqsChildNumber,
                options: {
                    title:{
                        display:true,
                        text:"Comparison of the breakup of the number of activities completed for each child"
                    },
                    tooltips: {
                        mode: 'label'
                    },
                    responsive: true,
                    scales: {
                        xAxes: [{
                            stacked: true,
                        }],
                        yAxes: [{
                            stacked: true
                        }]
                    },
                    legend:{
                        position:'top'
                    }
                }
        });

        var ctxChild3 = document.getElementById("canvas-child-act-1").getContext("2d");
            window.childActPercentChart = new Chart(ctxChild3, {
                type: 'horizontalBar',
                data: childActData1,
                options: {
                    title:{
                        display:true,
                        text:"Comparison of the percentage of activities completed fro each child"
                    },
                    tooltips: {
                        mode: 'label'
                    },
                    responsive: true,
                    scales: {
                        xAxes: [{
                            stacked: true,
                             ticks: {
                                // Create scientific notation labels
                                callback: function(value, index, values) {
                                    return value + ' %';
                                }
                            },
                        }],
                        yAxes: [{
                            stacked: true,
                           
                        }]
                    },
                    legend:{
                        position:'top'
                    }
                }
        });

        var ctxChild4 = document.getElementById("canvas-child-act-2").getContext("2d");
            window.childActAmountChart = new Chart(ctxChild4, {
                type: 'horizontalBar',
                data: childActData2,
                options: {
                    title:{
                        display:true,
                        text:"Comparison of the amount of activities completed fro each child"
                    },
                    tooltips: {
                        mode: 'label'
                    },
                    responsive: true,
                    scales: {
                        xAxes: [{
                            stacked: true,
                        }],
                        yAxes: [{
                            stacked: true,
                        }]
                    },
                    legend:{
                        position:'top'
                    }
                }
        });

        var ctxStaffTask1 = document.getElementById("canvas-task-1").getContext("2d");
            window.taskChart1 = new Chart(ctxStaffTask1, {
                type: 'bar',
                data: staffTaskData1,
                options: {
                    title:{
                        display:true,
                        text:"Comparison of the percentage of tasks completed by each educator"
                    },
                    tooltips: {
                        mode: 'label'
                    },
                    responsive: true,
                    scales: {
                        xAxes: [{
                            stacked: true,
                        }],
                        yAxes: [{
                            stacked: true,
                            ticks: {
                                // Create scientific notation labels
                                callback: function(value, index, values) {
                                    return value + ' %';
                                }
                            },
                        }]
                    },
                    legend:{
                        position:'top'
                    }
                }
        });

        var ctxStaffTask2 = document.getElementById("canvas-task-2").getContext("2d");
            window.taskChart2 = new Chart(ctxStaffTask2, {
                type: 'horizontalBar',
                data: staffTaskData2,
                options: {
                    title:{
                        display:true,
                        text:"Comparison of the total number of tasks completed by each educator"
                    },
                    tooltips: {
                        mode: 'label'
                    },
                    responsive: true,
                    scales: {
                        xAxes: [{
                            stacked: true,
                        }],
                        yAxes: [{
                            stacked: true
                        }]
                    },
                    legend:{
                        position:'top'
                    }
                }
        });
    };

 });    