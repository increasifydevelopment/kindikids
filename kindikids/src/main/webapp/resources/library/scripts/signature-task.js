
// var wrapper = document.getElementById("signature-pad"),
//     clearButton = wrapper.querySelector("[data-action=clear]"),
//     saveButton = wrapper.querySelector("[data-action=save]"),
//     canvas = wrapper.querySelector("canvas"),
//     signaturePad;

// // Adjust canvas coordinate space taking into account pixel ratio,
// // to make it look crisp on mobile devices.
// // This also causes canvas to be cleared.
// function resizeCanvas() {
//     // When zoomed out to less than 100%, for some very strange reason,
//     // some browsers report devicePixelRatio as less than 1
//     // and only part of the canvas is cleared then.
//     var ratio =  Math.max(window.devicePixelRatio || 1, 1);
//     canvas.width = canvas.offsetWidth * ratio;
//     canvas.height = canvas.offsetHeight * ratio;
//     canvas.getContext("2d").scale(ratio, ratio);
// }

// window.onresize = resizeCanvas;
// resizeCanvas();

// signaturePad = new SignaturePad(canvas);

// clearButton.addEventListener("click", function (event) {
//     signaturePad.clear();
// });

// saveButton.addEventListener("click", function (event) {
//     if (signaturePad.isEmpty()) {
//         alert("Please provide signature first.");
//     } else {
//         window.open(signaturePad.toDataURL());
//     }
// });


//The following code for render multiple signature pad
var wrapper1 = document.getElementById("signature-pad-1"),
    clearButton1 = wrapper1.querySelector("[data-action=clear1]"),
    canvas1 = wrapper1.querySelector("canvas"),
    signaturePad1;

var wrapper2 = document.getElementById("signature-pad-2"),
    clearButton2 = wrapper2.querySelector("[data-action=clear2]"),
    canvas2 = wrapper2.querySelector("canvas"),
    signaturePad2;

var wrapper3 = document.getElementById("signature-pad-3"),
    clearButton3 = wrapper3.querySelector("[data-action=clear3]"),
    canvas3 = wrapper3.querySelector("canvas"),
    signaturePad3;



function resizeCanvas(canvas) {
    var ratio =  window.devicePixelRatio || 1;
    canvas.width = canvas.offsetWidth * ratio;
    canvas.height = canvas.offsetHeight * ratio;
    canvas.getContext("2d").scale(ratio, ratio);
}

resizeCanvas(canvas1);
signaturePad1 = new SignaturePad(canvas1);
clearButton1.addEventListener("click", function (event) {
    signaturePad1.clear();
});


resizeCanvas(canvas2);
signaturePad2 = new SignaturePad(canvas2);
clearButton2.addEventListener("click", function (event) {
    signaturePad2.clear();
});

resizeCanvas(canvas3);
signaturePad3 = new SignaturePad(canvas3);
clearButton3.addEventListener("click", function (event) {
    signaturePad3.clear();
});




//Modal box 
$('.modal').on('shown.bs.modal', function (e) {    
   resizeCanvas(canvas1);
   resizeCanvas(canvas2);
   resizeCanvas(canvas3);
});
