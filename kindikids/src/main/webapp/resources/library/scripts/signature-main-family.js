
// var wrapper = document.getElementById("signature-pad"),
//     clearButton = wrapper.querySelector("[data-action=clear]"),
//     saveButton = wrapper.querySelector("[data-action=save]"),
//     canvas = wrapper.querySelector("canvas"),
//     signaturePad;

// // Adjust canvas coordinate space taking into account pixel ratio,
// // to make it look crisp on mobile devices.
// // This also causes canvas to be cleared.
// function resizeCanvas() {
//     // When zoomed out to less than 100%, for some very strange reason,
//     // some browsers report devicePixelRatio as less than 1
//     // and only part of the canvas is cleared then.
//     var ratio =  Math.max(window.devicePixelRatio || 1, 1);
//     canvas.width = canvas.offsetWidth * ratio;
//     canvas.height = canvas.offsetHeight * ratio;
//     canvas.getContext("2d").scale(ratio, ratio);
// }

// window.onresize = resizeCanvas;
// resizeCanvas();

// signaturePad = new SignaturePad(canvas);

// clearButton.addEventListener("click", function (event) {
//     signaturePad.clear();
// });

// saveButton.addEventListener("click", function (event) {
//     if (signaturePad.isEmpty()) {
//         alert("Please provide signature first.");
//     } else {
//         window.open(signaturePad.toDataURL());
//     }
// });


//The following code for render multiple signature pad
var wrapper3 = document.getElementById("signature-pad-3"),
    clearButton3 = wrapper3.querySelector("[data-action=clear3]"),
    canvas3 = wrapper3.querySelector("canvas"),
    signaturePad3;

var wrapper4 = document.getElementById("signature-pad-4"),
    clearButton4 = wrapper4.querySelector("[data-action=clear4]"),
    canvas4= wrapper4.querySelector("canvas"),
    signaturePad4;

var wrapper5 = document.getElementById("signature-pad-5"),
    clearButton5 = wrapper5.querySelector("[data-action=clear5]"),
    canvas5 = wrapper5.querySelector("canvas"),
    signaturePad5;

var wrapper6 = document.getElementById("signature-pad-6"),
    clearButton6 = wrapper6.querySelector("[data-action=clear6]"),
    canvas6= wrapper6.querySelector("canvas"),
    signaturePad6;

var wrapper7 = document.getElementById("signature-pad-7"),
    clearButton7 = wrapper7.querySelector("[data-action=clear7]"),
    canvas7= wrapper7.querySelector("canvas"),
    signaturePad7;


function resizeCanvas(canvas) {
    var ratio =  window.devicePixelRatio || 1;
    canvas.width = canvas.offsetWidth * ratio;
    canvas.height = canvas.offsetHeight * ratio;
    canvas.getContext("2d").scale(ratio, ratio);
}


resizeCanvas(canvas3);
signaturePad3 = new SignaturePad(canvas3);
clearButton3.addEventListener("click", function (event) {
    signaturePad3.clear();
});

resizeCanvas(canvas4);
signaturePad4 = new SignaturePad(canvas4);
clearButton4.addEventListener("click", function (event) {
    signaturePad4.clear();
});

resizeCanvas(canvas5);
signaturePad5 = new SignaturePad(canvas5);
clearButton5.addEventListener("click", function (event) {
    signaturePad5.clear();
});

resizeCanvas(canvas6);
signaturePad6 = new SignaturePad(canvas6);
clearButton6.addEventListener("click", function (event) {
    signaturePad6.clear();
});

resizeCanvas(canvas7);
signaturePad7 = new SignaturePad(canvas7);
clearButton7.addEventListener("click", function (event) {
    signaturePad7.clear();
});




//To redraw canvas under tabs - Angular need to re-write code, the following is based on bootstrap jquery plugin. 
//Sample only.
$(function(){

      $('.modal').on('shown.bs.modal', function (e) {
          resizeCanvas(canvas3);
          resizeCanvas(canvas4);
          resizeCanvas(canvas5);
          resizeCanvas(canvas6);
          resizeCanvas(canvas7);
      });

});