 $(function() {

        
        //Side Menu
        $('.sub-click').click(function(){
            $('.show-popup').removeClass('show-popup');
            $(this).parents('.side-item').find('.sub-popup').addClass('show-popup');
            $('.overlap-for-menu').addClass('show');
            $('body').addClass('overflow-hidden');
        });

        $('.sub-popup a').click(function(){
            $(this).parents('.sub-popup').removeClass('show-popup');
            $('.overlap-for-menu').removeClass('show');
            $('body').addClass('overflow-hidden');
        });


        //Flexslider
        $('.flexslider').flexslider({
            animation: "slide"
          });

        //Select 2 - multiple select & autocomplete
        $("#recipent").select2();

        $("#childTagged, .multipleSelect, .childTaggedPopup, #childTaggedPopup, .privacy-select, .role-select-specific").select2();

        $(".task-responsible-people").select2();

        $(".task-implementers").select2();

        $(".task-select2").select2();

        $(".select2-single").select2();

        // $('select').on('select2:open', function(e) {
        //     $('.select2-search input').prop('focus',false);
        // });

        //Shortern
        $('.showmore').click(function(event){
            event.preventDefault();
            $(this).css('display','none');
            $('.shotern').css('display','inline-block');
        });


        //Tooltip
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="tooltip-cus"]').tooltip({
              template: '<div class="tooltip tooltip-auto"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
            });
        $('[data-toggle="tooltip-menu"]').tooltip({
              template: '<div class="tooltip tooltip-menu"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
        });

        //Push Menu
        $('.pushButton').click(function() {
            $('.sidebar-inner').addClass('open');
            $('body').addClass('push-open');
        });

        $('.closeButton').click(function() {
            $('.sidebar-inner').removeClass('open');
            $('body').removeClass('push-open');
        });

        //Newsfeed button action
        $('.toggleTagChild').click(function(){
             $('.tagChild').toggleClass('slideDown');
        });

        $('.toggleImageUpload').click(function(){
            $('.imageUpload').toggleClass('slideDown');
        });


        $('.post-more').each(function(){
            $(this).click(function(event){
                event.preventDefault();
                $(this).parents('article').find('.post-excerpt').css('display','none');
                $(this).parents('article').find('.post-full-content').slideDown();
            });
        });

        var popoverTemplate = [
            '<div class="popover cPopover" role="tooltip">',
            '<div class="popover-arrow"></div>',
            '<h3 class="popover-title"></h3>',
            '<div class="popover-content"></div></div>'].join('');
        var contentValue = [
            '<span class="rating"><img src="../images/icon-face/icon-face-happy.png" class="img-responsive"/></span>',
            '<span class="rating"><img src="../images/icon-face/icon-face-smile.png" class="img-responsive"/></span>',
            '<span class="rating"><img src="../images/icon-face/icon-face-normal.png" class="img-responsive"/></span>',
            '<span class="rating"><img src="../images/icon-face/icon-face-dissatisfied.png" class="img-responsive"/></span>',
            '<span class="rating"><img src="../images/icon-face/icon-face-sad.png" class="img-responsive"/></span>'
            ].join('');
        $('.customPopover').popover({

            content: contentValue,
            template: popoverTemplate,
            placement: 'left',
            html:true
        });

        //Date range 
        $('.time-range').daterangepicker({
            showDropdowns: true
        });

        //Date picker
        // $('.date-picker').datetimepicker({
        //     format: "DD/MM/YYYY"
        // });
        // $( ".date-picker" ).datepicker({
        //   changeMonth: true,
        //   changeYear: true,
        //   dateFormat: "dd/mm/yy",
        //    beforeShow: function(input, obj) {
        //         $(input).after($(input).datepicker('widget'));
        //     }
        // });

        $('.date-picker').daterangepicker({
            locale: {
              format: 'DD/MM/YYYY'
            },
            singleDatePicker: true,
            showDropdowns: true
        });

        //Time picker inline
        $('.time-picker').datetimepicker({
            format: 'LT'
        });

        //Text editor initialize
           $('#summernote').summernote({
                height: 200,                 // set editor height
                minHeight: null,             // set minimum height of editor
                maxHeight: null,             // set maximum height of editor
                focus: true,  // set focus to editable area after initializing summernote
                toolbar: [
                ['style', ['bold', 'italic', 'clear']]
              ]
            });

        //Close All modals
        $('.closeAllModal').click(function(){
            $('.modal').modal('hide');
        });

        //Comment Edit
        $('.comment-edit').each(function(){
            $(this).click(function(event){
                event.preventDefault();
                var value = $(this).parents('.comment-display').find('.form-control-static').text();
                $(this).parents('.comment-display').find('.form-control-static, .comment-actions').addClass('hidden');
                $(this).parents('.comment-display').find('.message-editing, .comment-sent').removeClass('hidden');
                $(this).parents('.comment-display').find('.message-editing').val(value);
            });
        });


        //Show notify
        // $('.show-notify').click(function(){
        //     $('.notify').addClass('show').delay(2000).queue(function(){
        //         $(this).removeClass("show").dequeue();
        //     });
        // });

        $('.showAlert').click(function(){
            $.messager.model = { 
                cancel: { text: "No", classed: 'btn-default' },
                ok:{ text: "Yes", classed: 'btn-general closeAllModal' }
                
              };
              $.messager.confirm("Warning", "Unsaved changes. Are you sure to cancel?");
        });

        $('.action-archive').click(function(){
            $.messager.model = { 
                cancel: { text: "No", classed: 'btn-default' },
                ok:{ text: "Yes", classed: 'btn-general' }
                
              };
              $.messager.confirm("Archive", "Are you sure you want to archive?");
        });

        $('.action-save').click(function(){
            $.messager.model = { 
                cancel: { text: "No", classed: 'btn-default' },
                ok:{ text: "Yes", classed: 'btn-general' }
                
              };
              $.messager.confirm("Save", "Are you sure you want to save?");
        });

        $('.action-delete').click(function(){
            $.messager.model = { 
                cancel: { text: "No", classed: 'btn-default' },
                ok:{ text: "Yes", classed: 'btn-general' }
                
              };
              $.messager.confirm("Delete", "Are you sure you want to delete?");
        });

        $('.action-sent').click(function(){
            $.messager.popup("Sent!");
        });

        $('.child-cancel-request').click(function(){
            $.messager.model = { 
                cancel: { text: "No", classed: 'btn-default' },
                ok:{ text: "Yes", classed: 'btn-general' }
                
              };
              $.messager.confirm("Warning", "If you cancel this request, you will lose your position in the queue.");
        });

        $('.child-attendance-request').click(function(){
            $.messager.model = { 
                cancel: { text: "No", classed: 'btn-default' },
                ok:{ text: "Yes", classed: 'btn-general' }
                
              };
              $.messager.confirm("Warning", "If you change this request, your existing attendance will be lost.");
        });

        $('.child-remove, .staff-remove').click(function(){
            $.messager.model = { 
                cancel: { text: "No", classed: 'btn-default' },
                ok:{ text: "Yes", classed: 'btn-general' }
                
              };
              $.messager.confirm("Warning", "Are you sure?");
        });

        $('input[name="exportChildSign"]').click(function () {
            if (this.value == "bychild") {
                $('.export-child-select').removeClass('hidden');
            } else {
                 $('.export-child-select').addClass('hidden');
            }
        });

            
        //Profile checkbox
        $('.show-expandable').each(function(){
            var expandableArea = $(this).parents('.expandable-block').find('.expandable-area');

            $(this).on('change',function(){
                 if(this.checked){
                    expandableArea.addClass('show');
                } else {
                    expandableArea.removeClass('show');
                }    
            })
            
        });


        $('input[name=faimilyStatusRadios1],input[name="relationshipRadios1"]').click(function () {
            var expandableArea = $(this).parents('.expandable-block').find('.expandable-area');
            if (this.value == "show-expandable") {
                expandableArea.addClass('show');
            } else {
                expandableArea.removeClass('show');
            }
        });


        $('.radioCheck').click(function () {
            var expandableAreaYes = $(this).parents('.expandable-block').find('.expandable-area-yes');
            var expandableAreaNo = $(this).parents('.expandable-block').find('.expandable-area-no');
            if (this.value == "yes") {
                expandableAreaYes.show();
                expandableAreaNo.hide();
            } else if (this.value == "no") {
                expandableAreaYes.hide();
                expandableAreaNo.show();
            }
        });

        //Selection sample
        $('.centre-selection').on('change',function(){
            $(this).parents('.form-horizontal').find('.showSelect_1').show();
        });

        $('.room-selection').on('change',function(){
            $(this).parents('.form-horizontal').find('.showSelect_2').show();
        });

        //Employment
        $('.role-select').on('change',function(){
            if($(this).val() == "Educator"){
                $(this).parents('.expandable-block').find('.expandable-educator').removeClass('hidden');
                $(this).parents('.expandable-block').find('.expandable-cook').addClass('hidden');
            }
            else if($(this).val() == "Cook"){
                $(this).parents('.expandable-block').find('.expandable-cook').removeClass('hidden');
                $(this).parents('.expandable-block').find('.expandable-educator').addClass('hidden');
            }else{
                $(this).parents('.expandable-block').find('.expandable-educator, .expandable-cook').addClass('hidden');
            }
        });

        //Profile Children Reporst
        $('.dropdown-tab-nav a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $('.dropdown-tab-nav li.active').removeClass('active');
            $(this).parent('li').addClass('active');
        });

        $('.not-assessed-btn').click(function(){
            $(this).parents('.row').find('.not-assessed-reason-block').toggleClass('hidden');
        });

        //Image Select
        $(".image-select").chosen();

        //File Upload Alert Sample
        // window.addEventListener('load', function() {
        //         var el = document.getElementById('testfilefield');

        //         // Block the "dragover" event
        //         el.addEventListener('dragover', function(e) {
        //             e.stopPropagation();
        //             e.preventDefault();
        //         }, false);

        //         // Handle the "drop" event
        //         el.addEventListener('drop', function(e) {
        //             var firstFile = e.dataTransfer.files[0];
        //             console.log(firstFile);
        //             alert('Drop!');
        //         }, false);
        // }, false);

        $('.fileUpload-drag-area input[type="file"]').change(function() {
           alert($(this).val()); 
        });

        //Centre Notice & Alert
        $('.centre-update').click(function(){
            $.messager.model = { 
                cancel: { text: "No", classed: 'btn-default' },
                ok:{ text: "Yes", classed: 'btn-general' }
                
              };
              $.messager.confirm("Update Centre Info", "Are you sure?");
        });

        $('.centre-archive').click(function(){
            $.messager.model = { 
                cancel: { text: "No", classed: 'btn-default' },
                ok:{ text: "Yes", classed: 'btn-general' }
                
              };
              $.messager.confirm("Archive Centre", "This centre cannot be archived since active staff or children exist.");
        });

        $('.room-archive').click(function(){
            $.messager.model = { 
                cancel: { text: "No", classed: 'btn-default' },
                ok:{ text: "Yes", classed: 'btn-general' }
                
              };
              $.messager.confirm("Archive Room", "This room cannot be archived since active staff or children exist.");
        });

         $('.group-archive').click(function(){
            $.messager.model = { 
                cancel: { text: "No", classed: 'btn-default' },
                ok:{ text: "Yes", classed: 'btn-general' }
                
              };
              $.messager.confirm("Archive Group", "This group cannot be archived since active staff or children exist.");
        });

        $('.group-update').click(function(){
            $.messager.model = { 
                cancel: { text: "No", classed: 'btn-default' },
                ok:{ text: "Yes", classed: 'btn-general' }
                
              };
              $.messager.confirm("Update Group Info", "Are you sure?");
        });

         //Room Select
        $('.room-select').on('change',function(){
            if($(this).val() == "nursery"){
                $('.group-wrap').removeClass('hidden');
                $('.no-group-found').addClass('hidden');
            }
            else{
               $('.group-wrap').addClass('hidden');
               $('.no-group-found').removeClass('hidden');
            }
        });

        //Task - Repeats Select
        $('.repeats-select').on('change',function(){
            if($(this).val() == "daily" || $(this).val() == "once-off"){
                $('.general-field').removeClass('hidden');
                $('.weekly-field, .monthly-filed .qy-field').addClass('hidden');
            }
            else if($(this).val() == "weekly"){
                $('.weekly-field, .general-field').removeClass('hidden');
                $('.qy-field, monthly-field').addClass('hidden');
            }
            else if($(this).val() == "monthly"){
                $('.monthly-field, .general-field').removeClass('hidden');
                $('.weekly-field, .qy-field').addClass('hidden');
            }
            else if($(this).val() == "quarterly"){
                $('.general-field').removeClass('hidden');
                $('.weekly-field, .monthly-field, .qy-field').addClass('hidden');
            }
            else if($(this).val() == "yearly"){
                $('.qy-field, .general-field').removeClass('hidden');
                $('.weekly-field, .monthly-field').addClass('hidden');
            }
            else{
               $('.frq-field').addClass('hidden');
            }
        });

        $('input[name="endsRadios"]').click(function () {
            if (this.value == "occurrences") {
                $(this).parents('.form-group').find('.number-occurences').prop("disabled", false);
                $(this).parents('.form-group').find('.date-picker').prop("disabled", true);
            } else if (this.value == "on-date") {
               $(this).parents('.form-group').find('.number-occurences').prop("disabled", true);
               $(this).parents('.form-group').find('.date-picker').prop("disabled", false);
            } else{
               $(this).parents('.form-group').find('.number-occurences,.date-picker').prop("disabled", true);
            }
        });

        //Task Detail buttons collapse for better responsive
        $('.toggle-task-buttons').click(function(){
            $(this).parents('.function-wrap').find('.task-buttons').toggleClass('task-buttons-open');
            $(this).toggleClass('toggle-open');
        });

        //Task Category
        $('.task-category').on('change',function(){
            if($(this).val() == "centre-tasks"){
                $('.task-category-block, .frequency-block').addClass('hidden');
                $('.centre-tasks').removeClass('hidden');
            }
            else if($(this).val() == "room-tasks"){
                $('.task-category-block').addClass('hidden');
                $('.room-tasks, .frequency-block').removeClass('hidden');
            }
            else if($(this).val() == "children-tasks"){
                $('.task-category-block').addClass('hidden');
                $('.children-tasks, .frequency-block').removeClass('hidden');
            }
            else if($(this).val() == "staff-tasks"){
                $('.task-category-block').addClass('hidden');
                $('.staff-tasks, .frequency-block').removeClass('hidden');
            }
            else{
               $('.task-category-block, .frequency-block').addClass('hidden');
            }
        });

        $('input[name="cpRadios"]').click(function(){

             if (this.value == "other") {
                $('.centre-task-implementers, .frequency-block').removeClass('hidden');
            } else {
                $('.centre-task-implementers, .frequency-block').addClass('hidden');
            }
            
        });

        //Sign In Landing 
        $('.sign-click').click(function(event){
            event.preventDefault();
            $(this).parents('.sign-block').find('.sign-menu li').toggleClass('show');

        });

        //Sign Out Staff Alert
        $('.alert-sign-out-staff').click(function(){
            $.messager.model = { 
                cancel: { text: "No", classed: 'btn-default' },
                ok:{ text: "Yes", classed: 'btn-general' }
                
              };
              $.messager.confirm("Warning", "Make sure there are no children on premises");
        });

        //Program Modules, registers, tasks...
        $('.sleep-radios').click(function(){
            var expandableArea = $(this).parents('.expandable-block').find('.expandable-area');

             if (this.value == "sleep") {
                expandableArea.addClass('show');
            } else {
                expandableArea.removeClass('show');
            }
            
        });


        $('.eylf-select').on('change',function(){

                 if($(this).val() == "not-assessed"){
                    $(this).parents('.outcome-block').find('.not-assessed-reason').removeClass('hidden');
                } else {
                    $(this).parents('.outcome-block').find('.not-assessed-reason').addClass('hidden');
                }    
        })

        $('.other-task-select').on('change',function(){
            if($(this).val() == "task-child-med"){
                $('#child-medication-modal').modal();
            } else {
           
            }   
        });

         //For Responsive Menu
         $(document).on('click','.navbar-collapse.in',function(e) {
            if( $(e.target).is('a') ) {
                $(this).collapse('hide');
            }
        });

        //Gallery Setting
        $(".openNQSmodal").on('click',function(){
            //console.log("openNQSmodal is fired!");
            $("#nqs-modal").modal('show');
        });

        //Print - Newsfeed
         $(".printThisNewsfeed").on('click',function(){
             //console.log("clickable is fired!");
             var array = $(this).parents(),
                 printContainer = "";
             //console.log(array);
             $.each(array, function(index, value){
                 if((value.id).length > 0){
                     //console.log(index + ': ' + value.id);
                     printContainer = "#" + value.id;
                     return false;
                 }
                 return true;
             });
             var  template = $(printContainer).find(".printable").html();

             $("#printableArea").html(template);
             window.print();
         });

        // Enables popover #2
            $(".shift-tagging").popover({
                html : true, 
                content: function() {
                  return $(".role-tagging-content").html();
                },
                title: function() {
                  return $(".role-tagging-title").html();
                },
                placement:"auto"
            });

        // Meeting minutes selection
         $('input[name="mtRadios"]').click(function () {

            if (this.value == "template") {
                $('.create-template').removeClass('hidden');
                $('.select-template').addClass('hidden');
            } else {
                $('.create-template').addClass('hidden');
                $('.select-template').removeClass('hidden');
            }
        });

        //Induction & Orientation
        $('.induction-version-select, .orientation-version-select').on('change',function(){
            if($(this).val() == "version-shows"){
                $('.content-selected').removeClass('hidden');
            }
            else if($(this).val() == "version-change"){
                $('#io-change').modal('show');
                
            }

        });

        //Policy Storage
        $('input[name="policyRadios"]').click(function () {
            if (this.value == "policyRadios_1") {
                $('.policy-new').removeClass('hidden');
                $('.policy-existing').addClass('hidden');
            } else {
                $('.policy-existing').removeClass('hidden');
                $('.policy-new').addClass('hidden');
            }
        });
      
        
});

//Multiple Modal fixes
(function($, window) {
    'use strict';

    var MultiModal = function(element) {
        this.$element = $(element);
        this.modalCount = 0;
    };

    MultiModal.BASE_ZINDEX = 1040;

    MultiModal.prototype.show = function(target) {
        var that = this;
        var $target = $(target);
        var modalIndex = that.modalCount++;

        $target.css('z-index', MultiModal.BASE_ZINDEX + (modalIndex * 20) + 10);

        // Bootstrap triggers the show event at the beginning of the show function and before
        // the modal backdrop element has been created. The timeout here allows the modal
        // show function to complete, after which the modal backdrop will have been created
        // and appended to the DOM.
        window.setTimeout(function() {
            // we only want one backdrop; hide any extras
            if(modalIndex > 0)
                $('.modal-backdrop').not(':first').addClass('hidden');

            that.adjustBackdrop();
        });
    };

    MultiModal.prototype.hidden = function(target) {
        this.modalCount--;

        if(this.modalCount) {
           this.adjustBackdrop();

            // bootstrap removes the modal-open class when a modal is closed; add it back
            $('body').addClass('modal-open');
        }
    };

    MultiModal.prototype.adjustBackdrop = function() {
        var modalIndex = this.modalCount - 1;
        $('.modal-backdrop:first').css('z-index', MultiModal.BASE_ZINDEX + (modalIndex * 20));
    };

    function Plugin(method, target) {
        return this.each(function() {
            var $this = $(this);
            var data = $this.data('multi-modal-plugin');

            if(!data)
                $this.data('multi-modal-plugin', (data = new MultiModal(this)));

            if(method)
                data[method](target);
        });
    }

    $.fn.multiModal = Plugin;
    $.fn.multiModal.Constructor = MultiModal;

    $(document).on('show.bs.modal', function(e) {
        $(document).multiModal('show', e.target);
    });

    $(document).on('hidden.bs.modal', function(e) {
        $(document).multiModal('hidden', e.target);
    });
}(jQuery, window));