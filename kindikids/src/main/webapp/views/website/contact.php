<?php
// Check for empty fields
if(empty($_POST['name'])  		||
   empty($_POST['email']) 		||
   empty($_POST['website']) 		||
   empty($_POST['message'])	||
   !filter_var($_POST['email'],FILTER_VALIDATE_EMAIL))
   {
	echo "No arguments Provided!";
	return false;
   }
	
$name = $_POST['name'];
$email_address = $_POST['email'];
$website = $_POST['website'];
$message = $_POST['message'];
	
// Create the email and send the message
$to = 'sam_halcrow@theonlineexperts.com.au'; // Add your email address inbetween the '' replacing yourname@yourdomain.com - This is where the form will send a message to.
$email_subject = "Website Appointment Request:  $name";
$email_body = "You have received a new enquiry from your website.\n\n"."Here are the details:\n\nName: $name\n\nEmail: $email_address\n\nWebsite: $website\n\nMessage:\n$message";
$headers = "From: noreply@byblosfinance.com\n"; // This is the email address the generated message will be from. We recommend using something like noreply@yourdomain.com.
$headers .= "Reply-To: $email_address";	
mail($to,$email_subject,$email_body,$headers);
return true;			
?>