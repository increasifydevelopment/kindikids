<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Home Page</title>
<jsp:include page="../common/header.jsp"></jsp:include>
<script src="${root }/resources/js/public/account/reset.js"></script>
</head>
<body>
	<center>
		<table>
			<tr>
				<td>New password：</td>
				<td><input id="newPwd" type="text" value="" /></td>
			</tr>
			<tr>
				<td>Confirm password：</td>
				<td><input id="rePwd" type="text" value="" /></td>
				<input id="token" type="hidden" value="${token }">
			</tr>
			<tr>
				<td><input type="button" value="Submit" onclick="resetPwd()"></td>
			</tr>
			<tr>
				<td><a href="${root }/account/login.do">login</a></td>
			</tr>
		</table>
	</center>
</body>
</html>