<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Login</title>
<jsp:include page="common/header.jsp"></jsp:include>
<script src="${root }/resources/js/public/account/login.js"></script>
</head>
<body>
	<center>
		<table>
			<tr>
				<td>Email：</td>
				<td><input id="username" type="text" value="" /></td>
			</tr>
			<tr>
				<td>Password：</td>
				<td><input id="password" type="password" value="" /></td>
			</tr>
			<tr>
				<td><input type="button" value="login" onclick="login()"></td>
				<td><input type="reset" value="reset"></td>
			</tr>
			<tr>
				<td><a href="${root }/account/sendPwdMail.do">Forgot your password?</a></td>
			</tr>
		</table>
	</center>
</body>
</html>