package com.aoyuntek.aoyun.aop;

import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

/**
 * @description 日志拦截器
 * @author xdwang
 * @create 2015年12月22日下午9:33:15
 * @version 1.0
 */
@Aspect
public class ControllerLog {

    /**
     * 日志
     */
    public static Logger log = Logger.getLogger(ControllerLog.class);

    /**
     * @description 拦截器
     * @author xdwang
     * @create 2015年12月22日下午9:32:56
     * @version 1.0
     */
    @Pointcut(value = "execution(* aoyuntek.aoyun.controller..*.*(..))")
    public void recordLog() {
    }

    /**
     * @description 已经记录下操作日志@Before 方法执行前
     * @author xdwang
     * @create 2015年12月22日下午9:32:44
     * @version 1.0
     */
    @Before(value = "recordLog()")
    public void before() {
        this.printLog("已经记录下操作日志@Before 方法执行前");
    }

    /**
     * @description 已经记录下操作日志@Around 方法执行前
     * @author xdwang
     * @create 2015年12月22日下午9:31:19
     * @version 1.0
     * @param pjp
     *            ProceedingJoinPoint
     * @return Object
     * @throws Throwable
     *             异常
     */
    @Around(value = "recordLog()")
    public Object around(ProceedingJoinPoint pjp) throws Throwable {
        this.printLog("已经记录下操作日志@Around 方法执行前" + pjp);
        Object[] args = pjp.getArgs();
        for (Object element : args) {
            System.out.println(element);
        }
        return pjp.proceed();
        // this.printLog("已经记录下操作日志@Around 方法执行后");
    }

    /**
     * @description 已经记录下操作日志@After 方法执行后
     * @author xdwang
     * @create 2015年12月22日下午9:31:08
     * @version 1.0
     */
    @After(value = "recordLog()")
    public void after() {
        this.printLog("已经记录下操作日志@After 方法执行后");
    }

    /**
     * @description 记录日志
     * @author xdwang
     * @create 2015年12月13日下午8:13:53
     * @version 1.0
     * @param str
     *            日志内容
     */
    private void printLog(String str) {
        System.out.println(str);
        log.info(str);
    }

}
