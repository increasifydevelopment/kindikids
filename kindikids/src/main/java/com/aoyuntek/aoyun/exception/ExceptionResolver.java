package com.aoyuntek.aoyun.exception;

import java.io.IOException;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import com.theone.web.util.RequestUtil;
import com.theone.web.util.ResponseUtil;
import com.theone.web.util.ResponseVo;

/**
 * @description 异常
 * @author xdwang
 * @create 2015年12月22日下午9:33:54
 * @version 1.0
 */
@Component("AdminExceptionResolver")
public class ExceptionResolver implements HandlerExceptionResolver {

    /**
     * 日志
     */
    private static Log logger = LogFactory.getLog(ExceptionResolver.class);
    /**
     * 配置文件
     */
    @Value("#{sys}")
    private Properties sysProperties;

    @Override
    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        logger.error("用户访问" + request.getRequestURI() + "发生错误,错误信息:" + ex.getMessage(), ex);
        ex.printStackTrace();
        if (RequestUtil.isAsynRequest(request)) {
            // CSOFF: MagicNumber
            response.setStatus(500);
            // CSOFF: MagicNumber
            ResponseUtil.sendJson(new ResponseVo(false, "System error"), response);
        } else {
            try {
                ResponseUtil.gotoLogin(response, sysProperties.getProperty("system.login.url"));
            } catch (IOException e) {
                logger.error("resolveException|error", e);
            }
        }
        return null;
    }
}
