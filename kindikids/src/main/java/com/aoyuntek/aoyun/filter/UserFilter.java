package com.aoyuntek.aoyun.filter;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.aoyuntek.aoyun.dao.FunctionInfoMapper;
import com.aoyuntek.aoyun.entity.vo.RoleInfoVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.enums.common.AuthFunctionType;
import com.aoyuntek.framework.constants.PropertieNameConts;
import com.aoyuntek.framework.constants.SessionConts;
import com.aoyuntek.framework.spring.SpringApplicationContextHolder;
import com.theone.resource.util.PropertiesCacheUtil;
import com.theone.web.util.RequestUtil;
import com.theone.web.util.ResponseUtils;
import com.theone.web.util.ResponseVo;
import com.theone.web.util.SessionUtil;

/**
 * Servlet Filter implementation class UserFilter
 */
// CSOFF: MagicNumber
public class UserFilter implements Filter {
	/**
	 * Logger
	 */
	public static Logger log = Logger.getLogger(UserFilter.class);

	@Autowired
	private FunctionInfoMapper functionMapper;

	/**
	 * Default constructor.
	 */
	public UserFilter() {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;

		String baseUrl = req.getContextPath();
		String requestUrl = req.getRequestURI();
		requestUrl = requestUrl.replaceFirst(baseUrl, "");
		requestUrl = requestUrl.replaceAll("[?].*", "");

		// session失效后自动跳转登录页面
		HttpSession session = req.getSession(true);
		Date now = new Date();
		Date lastTime = (Date) session.getAttribute("lastOperationTime");
		if (lastTime == null) {
			session.setAttribute("lastOperationTime", now);
			lastTime = now;
		}
		int indexOf = requestUrl.indexOf("sessionTimeOut.do");
		if (indexOf >= 0 && (now.getTime() - lastTime.getTime()) > Integer.parseInt(PropertiesCacheUtil.getValue("session_time_out", PropertieNameConts.SYSTEM)) * 1000) {
			log.info("sessionTimeOut==========================" + session.getAttribute("lastOperationTime"));
			if (RequestUtil.isAsynRequest(req)) {
				res.setStatus(500);
				ResponseUtils.sendJson(new ResponseVo(false, "SessionLost"), res);
			} else {
				ResponseUtils.gotoLogin(res, PropertiesCacheUtil.getValue("system.login.url", PropertieNameConts.SYSTEM));
			}
			return;
		}
		if (indexOf < 0) {
			if (requestUrl.indexOf("unread.do") < 0) {
				session.setAttribute("lastOperationTime", now);
			}
		} else {
			return;
		}

		// 登录过滤无需过滤
		// beforeLoginExceptionUrls.contains(requestUrl)
		if (functionMapper.haveFunctionUrlByType(AuthFunctionType.BeforLogin.getValue(), requestUrl) > 0) {
			log.info("例外地址 系统允许放行");
			chain.doFilter(request, response);
			return;
		}

		// 看看是否登录
		UserInfoVo userVo = (UserInfoVo) SessionUtil.getModel(req, SessionConts.USERSESSION);
		if (userVo == null) {
			// 如果是ajax请求 那么返回错误json
			if (RequestUtil.isAsynRequest(req)) {
				res.setStatus(500);
				ResponseUtils.sendJson(new ResponseVo(false, "SessionLost"), res);
			} else {
				ResponseUtils.gotoLogin(res, PropertiesCacheUtil.getValue("system.login.url", PropertieNameConts.SYSTEM));
			}
			return;
		}

		if (functionMapper.haveFunctionUrlByType(AuthFunctionType.AfterLogin.getValue(), requestUrl) > 0) {
			log.info("例外地址 系统允许放行");
			chain.doFilter(request, response);
			return;
		}
		// 是否开启权限
		boolean isopen = Boolean.parseBoolean(PropertiesCacheUtil.getValue("open.authority", PropertieNameConts.SYSTEM));
		if (!isopen) {
			chain.doFilter(request, response);
			return;
		}

		List<RoleInfoVo> roles = userVo.getRoleInfoVoList();
		int count = 0;
		for (RoleInfoVo role : roles) {
			count += functionMapper.getCountByUrlRole(role.getId(), requestUrl);
		}
		if (count <= 0) {
			System.err.println("-----------------------------------------------------------requestUrl=" + requestUrl);
			// 如果是ajax请求 那么返回错误json
			if (RequestUtil.isAsynRequest(req)) {
				res.setStatus(500);
				ResponseUtils.sendJson(new ResponseVo(false, PropertiesCacheUtil.getValue("no.permissions", PropertieNameConts.TIP)), res);
			} else {
				ResponseUtils.gotoLogin(res, PropertiesCacheUtil.getValue("system.login.url", PropertieNameConts.SYSTEM));
			}
			return;
		}
		chain.doFilter(request, response);
	}

	@Override
	public void init(FilterConfig fConfig) throws ServletException {
		functionMapper = SpringApplicationContextHolder.getWebApplicationContext().getBean(FunctionInfoMapper.class);
		/*
		 * if (exceptionUrls == null) { exceptionUrls = new ArrayList<String>(); } String value = fConfig.getInitParameter("exceptionUrls"); if (StringUtil.isNotEmpty(value)) {
		 * String[] array = value.split(";"); for (String exceptionUrl : array) { if (StringUtil.isNotEmpty(exceptionUrl)) { exceptionUrls.add(exceptionUrl); } } }
		 */
		// appLoginService =
		// SpringContextBeanFactory.getBean(IAppLoginService.class);
	}

	@Override
	public void destroy() {

	}
}
