package com.aoyuntek.aoyun.filter;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.theone.BaseUtil;
import com.theone.string.util.StringUtil;
import com.theone.web.util.RequestUtil;
import com.theone.web.util.ResponseUtils;
import com.theone.web.util.ResponseVo;
import com.theone.web.util.SessionUtil;

/**
 * xssString: 特殊字符以逗号分割 notfilter: 不需要过滤的系统地址 errorUrl: 遇到特殊字符跳转到的 url 内部地址
 * intervalTime:刷新时间间隔 毫秒 fastUrl:刷新太快 跳转的内部地址 Filter implementation class
 * TheOneSecurityFilter
 */
public class TheOneSecurityFilter extends BaseUtil implements Filter {

	private static String xssString = "";

	private static String[] xssList = null;

	private static String notfilter = "";

	private static String errorUrl = "";

	private static String fastUrl = "";
	
	private static String errorMsg = "";

	private static String fastMsg = "";

	private static int intervalTime = -1;
	private static int intervalSize = -1;

	private static final String LastVisitTime = "{0}_LastVisitTime";
	private static final String LastVisitCount = "{0}_LastVisitCount";

	/**
	 * Default constructor.
	 */
	public TheOneSecurityFilter() {
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest arg01, ServletResponse arg02, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) arg01;
		HttpServletResponse res = (HttpServletResponse) arg02;
		String baseUrl = request.getContextPath();
		String requestUrl = request.getRequestURI();
		requestUrl = requestUrl.replace(baseUrl, "");
		LOGGER.debug("request url=" + requestUrl);
		
		if (notfilter.contains(requestUrl)) {
			chain.doFilter(request, arg02);
			return;
		}
		
		if (isSoFast(request,requestUrl)) {
			 if (RequestUtil.isAsynRequest(request)) {
				 res.setStatus(500);
	                ResponseUtils.sendJson(new ResponseVo(false, fastMsg), res);
	            } else {
	            	res.sendRedirect(fastUrl);
	            }
			return;
		}
		
		if (validate(request)) {
			 if (RequestUtil.isAsynRequest(request)) {
				 res.setStatus(500);
	              ResponseUtils.sendJson(new ResponseVo(false, errorMsg), res);
	            } else {
	            	res.sendRedirect(errorUrl);
	            }
			return;
		}
		chain.doFilter(request, arg02);
	}

	/**
	 * 判断用户是否刷新太快
	 * 
	 * @param request
	 * @return
	 */
	private boolean isSoFast(HttpServletRequest request,String requestUrl) {
		if (intervalTime == -1) {
			return false;
		}
		String sessionKey_LastVisitTime=MessageFormat.format(LastVisitTime, requestUrl);
		String  sessionKey_LastVisitCount=MessageFormat.format(LastVisitCount, requestUrl);
		long lastTime = SessionUtil.getLong(request,sessionKey_LastVisitTime );
		LOGGER.info("lastTime :" + lastTime);
		if (lastTime == Long.MIN_VALUE) {
			SessionUtil.setValue(request,sessionKey_LastVisitTime, new Date().getTime());
			return false;
		}

		long span = new Date().getTime() - lastTime;
		LOGGER.info("span :" + span);
		if (span < intervalTime) {
			//判断这是第几次
			long lastSize = SessionUtil.getLong(request, sessionKey_LastVisitCount);
			if(lastSize+1>=intervalSize){
				return true;
			}
			
			if(lastSize==Long.MIN_VALUE){
				lastSize=0;
			}
			
			//更新session
			//SessionUtil.setValue(request, sessionKey_LastVisitTime, new Date().getTime());
			SessionUtil.setValue(request, sessionKey_LastVisitCount,lastSize+1);
		}else{
			request.getSession().removeAttribute(sessionKey_LastVisitCount);
			request.getSession().removeAttribute(sessionKey_LastVisitTime);
			//SessionUtil.removeValue(request, );
			//SessionUtil.removeValue(request, );
		}
		SessionUtil.setValue(request,sessionKey_LastVisitTime, new Date().getTime());
		return false;
	}

	@SuppressWarnings("rawtypes")
	private boolean validate(HttpServletRequest request) {
		Map map = request.getParameterMap();
		Set keSet = map.entrySet();
		for (Iterator itr = keSet.iterator(); itr.hasNext();) {
			Map.Entry me = (Map.Entry) itr.next();
			// Object name = me.getKey();
			Object ov = me.getValue();
			String[] value = new String[1];
			if (ov instanceof String[]) {
				value = (String[]) ov;
			} else {
				value[0] = ov.toString();
			}
			for (int k = 0; k < value.length; k++) {
				if (isContainXss(value[k])) {
					LOGGER.info(value[k] + " 包含特殊字符 ,已被系统拦截 ip：" + RequestUtil.getIpAddr(request));
					return true;
				}
			}
		}
		return false;
	}

	private boolean isContainXss(String text) {
		if (xssList == null) {
			return false;
		}
		for (String item : xssList) {
			if (text.contains(item)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		LOGGER.info("xss 安全过滤包初始化 begin");
		if (StringUtil.isEmpty(xssString)) {
			xssString = fConfig.getInitParameter("xssString");
			LOGGER.info("xssString:" + xssString);
			if (StringUtil.isNotEmpty(xssString)) {
				xssList = xssString.split(",");
			}
		}
		if (StringUtil.isEmpty(notfilter)) {
			notfilter = fConfig.getInitParameter("notfilter");
			LOGGER.info("notfilter:" + notfilter);
		}
		if (StringUtil.isEmpty(errorUrl)) {
			errorUrl = fConfig.getInitParameter("errorUrl");
			LOGGER.info("errorUrl:" + errorUrl);
		}
		if (StringUtil.isEmpty(fastUrl)) {
			fastUrl = fConfig.getInitParameter("fastUrl");
			LOGGER.info("fastUrl:" + fastUrl);
		}
		if (StringUtil.isEmpty(errorMsg)) {
			errorMsg = fConfig.getInitParameter("errorMsg");
			LOGGER.info("errorMsg:" + errorMsg);
		}
		if (StringUtil.isEmpty(fastMsg)) {
			fastMsg = fConfig.getInitParameter("fastMsg");
			LOGGER.info("fastMsg:" + fastMsg);
		}
		if (intervalTime == -1) {
			intervalTime = Integer.valueOf(fConfig.getInitParameter("intervalTime"));
			LOGGER.info("intervalTime:" + intervalTime);
		}
		if (intervalSize == -1) {
			intervalSize = Integer.valueOf(fConfig.getInitParameter("intervalSize"));
			LOGGER.info("intervalSize:" + intervalSize);
		}

		LOGGER.info("xss 安全过滤包初始化 end");
	}

	public static void main(String[] args) {
		String specialStr = "[^\\:\\!\"\\#\\$\\%\\&\\'\\(\\)\\*\\+\\,\\-\\.\\/\\:\\;\\<\\=\\>\\?\\@\\[\\\\\\]\\^\\_\\`\\{\\|\\}\\~]*";
		Pattern pattern = Pattern.compile(specialStr);
		System.out.println(pattern.pattern());
		Matcher matcher = pattern.matcher("dfa");
		System.out.println(matcher.matches());
	}

}
