package com.aoyuntek.aoyun.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.aoyuntek.aoyun.util.SystemUtil;

/**
 * @description 用户访问过滤器 1：记录用户访问轨迹 2：用于限制用户访问地址控制
 * @author xdwang
 * @create 2015年12月10日下午7:11:07
 * @version 1.0
 */
@SuppressWarnings("serial")
public class VisitFilter extends HttpServlet implements Filter {

    /**
     * 日志实例
     */
    private static Logger logger = Logger.getLogger(VisitFilter.class);

    /**
     * 异常地址集合
     */
    private static List<String> exceptionUrls;

    @Override
    public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain arg2) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) arg0;
        logRequest(request);
        // 获取请求地址
        String baseUrl = request.getContextPath();
        String requestUrl = request.getRequestURI();
        requestUrl = requestUrl.replace(baseUrl, "");
        logger.info("visit url = " + requestUrl + " request mouth = " + request.getMethod());
        if (exceptionUrls.contains(requestUrl)) {
            logger.info("visit url is excettion url");
            arg2.doFilter(arg0, arg1);
            return;
        }
        // 记录访问日志
        SystemUtil.logVisitUrl(request, requestUrl);
        arg2.doFilter(arg0, arg1);
    }

    /**
     * @description request里包含的参数及值
     * @author xdwang
     * @create 2015年12月13日下午8:24:54
     * @version 1.0
     * @param request
     *            HttpServletRequest
     */
    public void logRequest(HttpServletRequest request) {
        @SuppressWarnings("unchecked")
        Enumeration<String> enumeration = request.getParameterNames();
        StringBuilder sb = new StringBuilder();
        while (enumeration.hasMoreElements()) {
            String string = (String) enumeration.nextElement();
            sb.append("{name:" + string + ",value:" + request.getParameter(string) + "}|");
        }
        logger.info(sb.toString());
    }

    @Override
    public void init(FilterConfig arg0) throws ServletException {
        if (exceptionUrls == null) {
            exceptionUrls = new ArrayList<String>();
        }
    }
}
