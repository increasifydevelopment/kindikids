package com.aoyuntek.aoyun.util;

import java.beans.PropertyEditorSupport;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.aoyuntek.aoyun.constants.DateFormatterConstants;
import com.theone.date.util.DateUtil;
import com.theone.string.util.StringUtil;

public class AoyunDateEditor extends PropertyEditorSupport {
    /**
     * 日志
     */
    public static Logger logger = Logger.getLogger(AoyunDateEditor.class);
    /**
     * 集合
     */
    private static Map<String, String> map = new HashMap<String, String>();

    public AoyunDateEditor() {
        map.put(DateUtil.yyyyMMddHHmmssSpt, "^\\d{4}\\D+\\d{1,2}\\D+\\d{1,2}\\D+\\d{1,2}\\D+\\d{1,2}\\D+\\d{1,2}\\D*$");
        map.put(DateUtil.yyyyMMddSpt, "^^\\d{4}\\D+\\d{2}\\D+\\d{2}$");
        map.put(DateFormatterConstants.SYS_T_FORMAT, "\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}.\\d{3}$");
        map.put(DateFormatterConstants.SYS_FORMAT2, "^^\\d{2}\\D+\\d{2}\\D+\\d{4}$");
        //2016-08-25T10:04:50.517
    }
    
    public static void main(String[] args) {
    	AoyunDateEditor ssss=new AoyunDateEditor();
		System.out.println(ssss.getFormatString("02/03/2016"));
	}

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        // 如果没值，设值为null
        if (StringUtil.isEmpty(text)) {
            setValue(null);
        }
        try {
            String formatStr = getFormatString(text);
            setValue(new SimpleDateFormat(formatStr).parse(text));
        } catch (Exception e) {
            //logger.error("=========找不到对应时间格式===========", e);
            setValue(null); // 如果没值，设值为null
        }
    }

    @Override
    public String getAsText() {
        if (getValue() == null) {
            return "";
        }
        return getValue().toString();
    }

    public static String getFormatString(String text) {
        String formatString = "";
        for (Map.Entry<String, String> entry : map.entrySet()) {
            Pattern pattern = Pattern.compile(entry.getValue());
            Matcher matcher = pattern.matcher(text);
            if (matcher.matches()) {
                formatString = entry.getKey();
                break;
            }
        }
        if (StringUtil.isEmpty(formatString)) {
            throw new IllegalArgumentException(String.format("类型转换失败，需要格式[yyyy-MM-dd HH:mm:ss 或者 yyyy-MM-dd 或者 yyyy-MM-ddTHH:mm:ss SSS ]，但格式是[%s]", text));
        }
        return formatString;
    }
}
