package com.aoyuntek.aoyun.util;

import java.text.MessageFormat;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.aoyuntek.aoyun.entity.po.AccountInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.vo.RoleInfoVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.framework.constants.PropertieNameConts;
import com.aoyuntek.framework.constants.SessionConts;
import com.aoyuntek.framework.constants.SystemKeys;
import com.theone.resource.util.PropertiesCacheUtil;
import com.theone.web.util.RequestUtil;
import com.theone.web.util.SessionUtil;

/**
 * @description 系统工具
 * @author xdwang
 * @create 2015年12月22日下午9:36:40
 * @version 1.0
 */
@Component
@Transactional
public class SystemUtil {

    /**
     * 日志（格式化）
     */
    private static Logger loggerUrl = Logger.getLogger("VisitUrlLog");

    /**
     * @description 获取当前按登陆者
     * @author xdwang
     * @create 2015年12月22日下午9:38:16
     * @version 1.0
     * @param request
     *            HttpServletRequest对象
     * @return 用户信息
     */
    public static UserInfoVo getCurrentUser(HttpServletRequest request) {
        Object object = SessionUtil.getModel(request, SessionConts.USERSESSION);
        if (object != null) {
            return (UserInfoVo) object;
        }
        return null;
    }

    /**
     * @description 记录访问地址
     * @author xdwang
     * @create 2015年12月22日下午9:37:47
     * @version 1.0
     * @param request
     *            HttpServletRequest对象
     * @param url
     *            访问地址
     */
    public static void logVisitUrl(HttpServletRequest request, String url) {
        UserInfoVo user = getCurrentUser(request);
        if (user == null) {
            user = new UserInfoVo();
            user.setAccountInfo(new AccountInfo());
            user.setUserInfo(new UserInfo());
            user.setRoleInfoVoList(new ArrayList<RoleInfoVo>());
        }
        String format = PropertiesCacheUtil.getValue(SystemKeys.VISITLOGPATTERN, PropertieNameConts.SYSTEM);
        loggerUrl.info(MessageFormat.format(format, RequestUtil.getIpAddr(request), user.getAccountInfo().getId(),
                user.getAccountInfo().getAccount(), url));
    }

}
