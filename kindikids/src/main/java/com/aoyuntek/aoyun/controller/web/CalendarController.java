package com.aoyuntek.aoyun.controller.web;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aoyuntek.aoyun.condtion.AttendanceCondtion;
import com.aoyuntek.aoyun.entity.vo.CalendarListVo;
import com.aoyuntek.aoyun.service.calendar.ICalendarService;
import com.theone.common.util.ServiceResult;
import com.theone.web.util.ResponseUtils;

@Controller
@RequestMapping(value = "/calendar")
public class CalendarController extends BaseWebController {
	
    @Autowired
    private ICalendarService calendarService;

	@ResponseBody
    @RequestMapping("/getCalendarList.do")
    public Object getCalendarList(HttpServletRequest request, AttendanceCondtion condition) {
        ServiceResult<CalendarListVo> result = calendarService.getCalendarList(getCurrentUser(request), condition);
        return ResponseUtils.sendMsg(result.isSuccess() ? 0 : -1, result.getMsg(), result.getReturnObj());
    }
	
	@ResponseBody
    @RequestMapping("/updateLeaveColor.do")
    public Object updateLeaveColor(HttpServletRequest request,String leaveId,String color) {		  
	      ServiceResult<Object> result = calendarService.updateLeaveColor(leaveId,color);
        return ResponseUtils.sendMsg(result.isSuccess() ? 0 : -1, result.getMsg(), result.getReturnObj());
    }
	
	@ResponseBody
    @RequestMapping("/updateEventColor.do")
    public Object updateEventColor(HttpServletRequest request,String eventId,String color) {		  
	      ServiceResult<Object> result = calendarService.updateEventColor(eventId,color);
        return ResponseUtils.sendMsg(result.isSuccess() ? 0 : -1, result.getMsg(), result.getReturnObj());
    }
}
