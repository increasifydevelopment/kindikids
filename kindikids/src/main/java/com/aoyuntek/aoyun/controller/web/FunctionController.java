package com.aoyuntek.aoyun.controller.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aoyuntek.aoyun.entity.po.FunctionInfo;
import com.aoyuntek.aoyun.entity.po.MenuInfo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.service.IFunctionService;
import com.theone.common.util.ServiceResult;
import com.theone.web.util.ResponseUtils;

/**
 * 
 * @description 获取function
 * @author gfwang
 * @create 2016年7月19日下午8:41:11
 * @version 1.0
 */
@Controller
@RequestMapping("/function")
public class FunctionController extends BaseWebController {
    @Autowired
    private IFunctionService functionService;

    /**
     * 
     * @description 获取 某个菜单下 按钮权限
     * @author gfwang
     * @create 2016年9月14日上午11:51:57
     * @version 1.0
     * @param request
     * @param pageUrl
     *            menuUrl
     * @return
     */
    @RequestMapping(value = "/getPageFunctions.do", method = RequestMethod.POST)
    @ResponseBody
    public Object getPageFunctions(HttpServletRequest request, String pageUrl) {
        ServiceResult<List<FunctionInfo>> result = functionService.getFunctionList(getCurrentUser(request), pageUrl);
        return ResponseUtils.sendMsg(result.isSuccess(), result.getReturnObj());
    }

    /**
     * 
     * @description 获取左侧菜单的权限
     * @author gfwang
     * @create 2016年9月14日上午11:52:15
     * @version 1.0
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getLeftMenus.do", method = RequestMethod.POST)
    public Object getLeftMenus(HttpServletRequest request) {
        UserInfoVo user = getCurrentUser(request);
        if(user==null){
            return ResponseUtils.sendMsg(-1);
        }
        ServiceResult<List<MenuInfo>> list = functionService.getMenuList(getCurrentUser(request));
        return sendSuccessResponse(list);
    }
}
