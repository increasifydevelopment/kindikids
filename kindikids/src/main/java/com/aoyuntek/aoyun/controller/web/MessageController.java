package com.aoyuntek.aoyun.controller.web;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aoyuntek.aoyun.condtion.MessageCondition;
import com.aoyuntek.aoyun.constants.DateFormatterConstants;
import com.aoyuntek.aoyun.entity.vo.MessageInfoVo;
import com.aoyuntek.aoyun.entity.vo.MessageListVo;
import com.aoyuntek.aoyun.entity.vo.ReceiverVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.service.IMessageInfoService;
import com.aoyuntek.aoyun.uitl.GsonUtil;
import com.aoyuntek.framework.annotation.UserAuthority;
import com.aoyuntek.framework.model.Pager;
import com.google.gson.reflect.TypeToken;
import com.theone.common.util.ServiceResult;
import com.theone.json.util.JsonUtil;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;
import com.theone.web.util.ResponseUtils;

/**
 * 
 * @description 站内信
 * @author gfwang
 * @create 2016年6月23日上午11:07:07
 * @version 1.0
 */
@Controller
@RequestMapping(value = "/message")
public class MessageController extends BaseWebController {
    private static Logger log = Logger.getLogger(MessageController.class);
    /**
     * IMessageInfoService
     */
    @Autowired
    private IMessageInfoService messageInfoService;

    /**
     * 
     * @description 获取收发件箱MGS list
     * @author bbq
     * @create 2016年6月24日下午3:14:47
     * @version 1.0
     * @param request
     *            HttpServletRequest对象
     * @param condition
     *            站内信查询条件
     * @return 操作结果
     */
    @ResponseBody
    @RequestMapping(value = "/list.do", method = RequestMethod.POST)
    public Object getList(HttpServletRequest request, String params) {
        MessageCondition condition = (MessageCondition) GsonUtil.jsonToBean(params, MessageCondition.class, DateFormatterConstants.SYS_FORMAT,
                DateFormatterConstants.SYS_FORMAT2, DateFormatterConstants.SYS_T_FORMAT, DateFormatterConstants.SYS_T_FORMAT2);
        // 获取当前用户
        UserInfoVo currentUser = getCurrentUser(request);
        // 根据当前用户获取此人所有的收件人
        ServiceResult<Pager<MessageListVo, MessageCondition>> result = messageInfoService.getPagerMsgs(currentUser, condition);
        return ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
    }

    /**
     * @description 获取message信息
     * @author hxzhang
     * @create 2016年6月28日下午2:29:37
     * @version 1.0
     * @param request
     *            HttpServletRequest
     * @param id
     *            message ID
     * @return 返回结果
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Parent, Role.Cook })
    @ResponseBody
    @RequestMapping(value = "/info.do", method = RequestMethod.POST)
    public Object getMessageInfo(HttpServletRequest request, String id) {
        // 获取当前登录用户
        UserInfoVo currentUser = getCurrentUser(request);
        // 获取Message信息
        ServiceResult<List<MessageInfoVo>> result = messageInfoService.dealMessageInfo(id, currentUser);
        return ResponseUtils.sendMsg(0, handleBackData(result.getReturnObj()));
    }

    private List handleBackData(List<MessageInfoVo> list) {
        MessageInfoVo baseMessage = list.get(0);
        List<ReceiverVo> receiverVoList = baseMessage.getReceiverVo();

        if (ListUtil.isEmpty(receiverVoList)) {
            return list;
        }
        List<ReceiverVo> userList = new ArrayList<ReceiverVo>();
        for (ReceiverVo looker : receiverVoList) {
            if (StringUtil.isEmpty(looker.getGroupName())) {
                // 添加到人員列表
                userList.add(looker);
                continue;
            }
        }
        list.get(0).setUserList(userList);
        return list;
    }

    /**
     * @description 发送message
     * @author hxzhang
     * @create 2016年6月28日下午2:29:02
     * @version 1.0
     * @param request
     *            HttpServletRequest
     * @param nsqVersion
     *            nsqVersion
     * @return 返回结果
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Parent, Role.Cook })
    @ResponseBody
    @RequestMapping(value = "/send.do", method = RequestMethod.POST)
    public Object sendMessage(HttpServletRequest request) {
        // json反序列化
        String jsonStr = ServletRequestUtils.getStringParameter(request, "params", "");
        MessageCondition messageCondition = (MessageCondition) JsonUtil
                .jsonToBean(jsonStr, MessageCondition.class, DateFormatterConstants.SYS_FORMAT);

        System.err.println(JsonUtil.objectToJson(messageCondition));
        // 获取当前登录用户
        UserInfoVo currentUser = getCurrentUser(request);
        // 获取参数
        // 保存Message相关信息
        ServiceResult<Object> result = messageInfoService.addMessage(messageCondition, currentUser);

        return ResponseUtils.sendMsg(result.getCode(), result.getMsg());
    }

    /**
     * @description 获取未读个数
     * @author bbq
     * @create 2016年6月27日下午2:14:30
     * @version 1.0
     * @param request
     * @return 个数
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Parent, Role.Cook })
    @ResponseBody
    @RequestMapping(value = "/unread.do", method = RequestMethod.GET)
    public Object getNoReadCount(HttpServletRequest request) {
        log.debug("====================================get unread count========================start");
        UserInfoVo currentUser = getCurrentUser(request);
        String accountId = currentUser.getAccountInfo().getId();
        int unreadCount = messageInfoService.getUnreadCount(accountId);
        log.debug("====================================get unread count========================end");
        return unreadCount;
    }

    @ResponseBody
    @RequestMapping(value = "/forward.do", method = RequestMethod.POST)
    public Object forward(HttpServletRequest request, String accountIdsJson, String msgId) {
        List<String> accountIds = JsonUtil.jsonToList(accountIdsJson, new TypeToken<List<String>>() {
        }.getType());
        ServiceResult<Object> result = messageInfoService.updateForward(accountIds, msgId);
        return ResponseUtils.sendMsg(result.getCode(), result.getMsg());
    }
}
