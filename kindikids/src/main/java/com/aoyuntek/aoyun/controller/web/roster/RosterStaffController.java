package com.aoyuntek.aoyun.controller.web.roster;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.lf5.util.StreamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aoyuntek.aoyun.condtion.AttendanceCondtion;
import com.aoyuntek.aoyun.condtion.PayrollCondition;
import com.aoyuntek.aoyun.condtion.RosterStaffCondition;
import com.aoyuntek.aoyun.constants.DateFormatterConstants;
import com.aoyuntek.aoyun.controller.web.BaseWebController;
import com.aoyuntek.aoyun.entity.po.ClosurePeriod;
import com.aoyuntek.aoyun.entity.po.roster.RosterStaffInfo;
import com.aoyuntek.aoyun.entity.vo.AttendanceReturnVo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.roster.RosterLeaveVo;
import com.aoyuntek.aoyun.entity.vo.roster.RosterStaffVo;
import com.aoyuntek.aoyun.entity.vo.roster.RosterWeekVo;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.enums.roster.RosterStaffType;
import com.aoyuntek.aoyun.service.IPayrollService;
import com.aoyuntek.aoyun.service.attendance.IAttendanceService;
import com.aoyuntek.aoyun.service.roster.IRosterService;
import com.aoyuntek.aoyun.uitl.GsonUtil;
import com.aoyuntek.framework.annotation.UserAuthority;
import com.google.gson.reflect.TypeToken;
import com.theone.common.util.ServiceResult;
import com.theone.date.util.DateUtil;
import com.theone.string.util.StringUtil;
import com.theone.web.util.ResponseUtils;

/**
 * 
 * @author dlli5 at 2016年10月17日上午10:00:12
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
@Controller
@RequestMapping("/rosterstaff")
public class RosterStaffController extends BaseWebController {
	@Autowired
	private IRosterService rosterService;
	@Autowired
	private IAttendanceService attendanceService;
	@Autowired
	private IPayrollService payrollService;

	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
	@ResponseBody
	@RequestMapping("/getAttendanceList.do")
	public Object getAttendanceList(HttpServletRequest request, AttendanceCondtion attendanceCondtion) {
		ServiceResult<AttendanceReturnVo> result = attendanceService.getAttendanceList(getCurrentUser(request), attendanceCondtion);
		return ResponseUtils.sendMsg(result.isSuccess() ? 0 : -1, result.getMsg(), result.getReturnObj());
	}

	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge, Role.Casual, Role.Educator, Role.Parent, Role.Cook, })
	@ResponseBody
	@RequestMapping(value = "/getRosterStaff.do", method = RequestMethod.POST)
	public Object getRosterStaff(HttpServletRequest request, String json) throws Exception {
		Type typeToken = new TypeToken<RosterStaffCondition>() {
		}.getType();
		RosterStaffCondition condition = (RosterStaffCondition) GsonUtil.jsonToBean(json, typeToken, DateFormatterConstants.SYS_T_FORMAT,
				DateFormatterConstants.SYS_FORMAT2);
		ServiceResult<RosterWeekVo> result = rosterService.getWeekRoster(condition, getCurrentUser(request));
		return ResponseUtils.sendMsg(true, result.getReturnObj());
	}

	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
	@ResponseBody
	@RequestMapping(value = "/getRosterStaffTemplates.do", method = RequestMethod.GET)
	public Object getRosterStaffTemplates(HttpServletRequest request, String centreId) throws Exception {
		ServiceResult<List<SelecterPo>> result = rosterService.getRosterStaffTemplates(centreId);
		return result.getReturnObj();
	}

	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
	@ResponseBody
	@RequestMapping(value = "/chooseTemplates.do", method = RequestMethod.POST)
	public Object chooseTemplates(HttpServletRequest request, String rosterId, Date day) throws Exception {
		Date dayOfWeek = DateUtil.getWeekStart(day);
		Date nowOfWeek = DateUtil.getWeekStart(new Date());
		if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(dayOfWeek, nowOfWeek) > 0) {
			return ResponseUtils.sendMsg(1, getTipMsg("Roster.staff.opertion.time"));
		}
		ServiceResult<RosterWeekVo> result = rosterService.dealChooseTemplates(rosterId, day, getCurrentUser(request), request);
		return ResponseUtils.sendMsg((int) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
	@ResponseBody
	@RequestMapping(value = "/createBlankRoster.do", method = RequestMethod.POST)
	public Object createBlankRoster(HttpServletRequest request, String centreId, Date day) {
		Date dayOfWeek = DateUtil.getWeekStart(day);
		Date nowOfWeek = DateUtil.getWeekStart(new Date());
		if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(dayOfWeek, nowOfWeek) > 0) {
			return ResponseUtils.sendMsg(1, getTipMsg("Roster.staff.opertion.time"));
		}
		ServiceResult<RosterWeekVo> result = rosterService.createBlankRoster(centreId, day, getCurrentUser(request));
		return ResponseUtils.sendMsg((int) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
	@ResponseBody
	@RequestMapping(value = "/getRosterStaffOfRole.do", method = RequestMethod.GET)
	public Object getRosterStaffOfRole(HttpServletRequest request, String p, String rosterId, String centreId, int rosterStaffType, Date day, String id) {
		ServiceResult<List<SelecterPo>> result = rosterService.getRosterStaffOfRole(p, rosterId, centreId, rosterStaffType, day, id, getCurrentUser(request));
		return result.getReturnObj();
	}

	@ResponseBody
	@RequestMapping(value = "/getRosterStaffForToday.do", method = RequestMethod.GET)
	public Object getRosterStaffForToday(HttpServletRequest request, String accountId, Date today, String p) {
		ServiceResult<List<SelecterPo>> result = rosterService.getRosterStaffForToday(accountId, today, p);
		return result.getReturnObj();
	}

	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
	@ResponseBody
	@RequestMapping(value = "/addRosterStaff.do", method = RequestMethod.POST)
	public Object addRosterStaff(HttpServletRequest request, String json, Boolean alert) {
		Type typeToken = new TypeToken<RosterStaffInfo>() {
		}.getType();
		RosterStaffInfo rosterStaffInfo = null;
		try {
			rosterStaffInfo = (RosterStaffInfo) GsonUtil.jsonToBean(json, typeToken, DateFormatterConstants.SYS_T_FORMAT, DateFormatterConstants.SYS_FORMAT2);
		} catch (Exception e) {
			return ResponseUtils.sendMsg(1, getTipMsg("illegal.request"));
		}

		// if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(new Date(),
		// rosterStaffInfo.getDay()) < 0) {
		// return ResponseUtils.sendMsg(1,
		// getTipMsg("Roster.staff.add.date.fail"));
		// }

		if (rosterStaffInfo.getStaffType() != RosterStaffType.Sign.getValue()) {
			if (rosterStaffInfo.getStartTime() == null || rosterStaffInfo.getEndTime() == null) {
				return ResponseUtils.sendMsg(1, getTipMsg("illegal.request"));
			}
		}

		if (rosterStaffInfo.getStartTime() != null && rosterStaffInfo.getEndTime() != null) {
			if (rosterStaffInfo.getStartTime().getTime() >= rosterStaffInfo.getEndTime().getTime()) {
				return ResponseUtils.sendMsg(1, getTipMsg("Roster.staff.addorupdate.same.time"));
			}
		}

		ServiceResult<RosterStaffVo> result = rosterService.addRosterStaff(rosterStaffInfo, getCurrentUser(request), alert, request);
		return ResponseUtils.sendMsg((int) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
	@ResponseBody
	@RequestMapping(value = "/deleteRosterStaff.do", method = RequestMethod.POST)
	public Object deleteRosterStaff(HttpServletRequest request, String objId, Boolean alert) {
		ServiceResult<RosterStaffVo> result = rosterService.deleteRosterStaff(objId, getCurrentUser(request), alert, request);
		return ResponseUtils.sendMsg((int) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
	@ResponseBody
	@RequestMapping(value = "/getRosterStaffItem.do", method = RequestMethod.POST)
	public Object getRosterStaffItem(HttpServletRequest request, String objId) {
		ServiceResult<RosterStaffVo> result = rosterService.getRosterStaff(objId, getCurrentUser(request));
		return ResponseUtils.sendMsg((int) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
	@ResponseBody
	@RequestMapping(value = "/updateRosterStaffItem.do", method = RequestMethod.POST)
	public Object updateRosterStaffItem(HttpServletRequest request, String params, Boolean comfirmSure) {
		Type typeToken = new TypeToken<RosterStaffVo>() {
		}.getType();
		RosterStaffVo rosterStaffInfo = null;
		try {
			rosterStaffInfo = (RosterStaffVo) GsonUtil.jsonToBean(params, typeToken, DateFormatterConstants.SYS_T_FORMAT, DateFormatterConstants.SYS_FORMAT2);
		} catch (Exception e) {
			return ResponseUtils.sendMsg(1, getTipMsg("illegal.request"));
		}
		// 验证 id 不能为null
		if (StringUtil.isEmpty(rosterStaffInfo.getId())) {
			return ResponseUtils.sendMsg(1, getTipMsg("illegal.request"));
		}
		// 验证排班时间不能为null
		if (rosterStaffInfo.getStartTime() == null || rosterStaffInfo.getEndTime() == null) {
			return ResponseUtils.sendMsg(1, getTipMsg("illegal.request"));
		}
		// 验证签出时间不为null 那么签入时间不可能为null
		if (rosterStaffInfo.getSignOutDate() != null && rosterStaffInfo.getSignInDate() == null) {
			return ResponseUtils.sendMsg(1, getTipMsg("Roster.staff.update.signin.null"));
		}
		// 如果签到时间不为null 那么需要判断 不能签到未来时间
		if (rosterStaffInfo.getSignInDate() != null) {
			if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(new Date(), rosterStaffInfo.getDay()) > 0) {
				return ResponseUtils.sendMsg(1, getTipMsg("Roster.staff.update.signin.Future"));
			}
		}
		if (rosterStaffInfo.getStartTime() != null && rosterStaffInfo.getEndTime() != null) {
			if (rosterStaffInfo.getStartTime().getTime() >= rosterStaffInfo.getEndTime().getTime()) {
				return ResponseUtils.sendMsg(1, getTipMsg("Roster.staff.addorupdate.same.time"));
			}
		}
		ServiceResult<RosterStaffVo> result = rosterService.updateRosterStaffItem(rosterStaffInfo, getCurrentUser(request), comfirmSure);
		return ResponseUtils.sendMsg((int) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
	@ResponseBody
	@RequestMapping(value = "/setRosterStatu.do", method = RequestMethod.POST)
	public Object setRosterStatu(HttpServletRequest request, String rosterId, int statu, Boolean alert) throws Exception {
		ServiceResult<RosterStaffVo> result = rosterService.updateRosterStatu(rosterId, (short) statu, getCurrentUser(request), request, alert);
		return ResponseUtils.sendMsg((int) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
	@ResponseBody
	@RequestMapping(value = "/setRosterTemplate.do", method = RequestMethod.POST)
	public Object setRosterTemplate(HttpServletRequest request, String rosterId, boolean templateFlag, String name) {
		ServiceResult<RosterStaffVo> result = rosterService.dealRosterAsTemplate(rosterId, templateFlag, name, getCurrentUser(request));
		return ResponseUtils.sendMsg((int) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	@ResponseBody
	@RequestMapping(value = "/geRosterLeaveVoList.do", method = RequestMethod.POST)
	public Object geRosterLeaveVoList(HttpServletRequest request) {
		ServiceResult<List<RosterLeaveVo>> result = rosterService.getRosterLeaveVoList();
		return ResponseUtils.sendMsg((int) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	@ResponseBody
	@RequestMapping(value = "/saveRosterLeave.do", method = RequestMethod.POST)
	public Object saveRosterLeave(HttpServletRequest request, String params) throws UnsupportedEncodingException {
		RosterLeaveVo rosterLeaveInfo = (RosterLeaveVo) GsonUtil.jsonToBean(params, RosterLeaveVo.class, DateFormatterConstants.SYS_FORMAT2,
				DateFormatterConstants.SYS_FORMAT, DateFormatterConstants.SYS_T_FORMAT, DateFormatterConstants.SYS_T_FORMAT2);
		UserInfoVo user = getCurrentUser(request);
		ServiceResult<Object> result = rosterService.saveRosterLeave(rosterLeaveInfo, user);
		return ResponseUtils.sendMsg((int) result.getCode(), result.getMsg(), result.getReturnObj());

	}

	@ResponseBody
	@RequestMapping(value = "/deleteRosterLeave.do", method = RequestMethod.POST)
	public Object deleteRosterLeave(HttpServletRequest request, String id) {

		ServiceResult<Object> result = rosterService.deleteRosterLeave(id);
		return ResponseUtils.sendMsg((int) result.getCode(), result.getMsg());

	}

	@ResponseBody
	@RequestMapping(value = "/saveClosurePeriod.do", method = RequestMethod.POST)
	public Object saveClosurePeriod(HttpServletRequest request, String params) {
		ClosurePeriod closurePeriod = (ClosurePeriod) GsonUtil.jsonToBean(params, ClosurePeriod.class, DateFormatterConstants.SYS_FORMAT2);
		ServiceResult<Object> result = rosterService.saveClosurePeriod(closurePeriod, getCurrentUser(request), request);
		return ResponseUtils.sendMsg((int) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	@ResponseBody
	@RequestMapping(value = "/deleteClosurePeriod.do", method = RequestMethod.POST)
	public Object deleteClosurePeriod(HttpServletRequest request, String id) {
		ServiceResult<Object> result = rosterService.deleteClosurePeriod(id);
		return ResponseUtils.sendMsg((int) result.getCode(), result.getMsg());
	}

	@RequestMapping(value = "/payrollData.do", method = RequestMethod.POST)
	@ResponseBody
	public Object payrollData(HttpServletRequest request, String params) {
		PayrollCondition condition = (PayrollCondition) GsonUtil.jsonToBean(params, PayrollCondition.class, DateFormatterConstants.SYS_T_FORMAT);
		StringBuffer buffer = payrollService.payrollExport(condition);
		HttpSession session = request.getSession();
		String uuid = UUID.randomUUID().toString();
		session.setAttribute(uuid, buffer);
		return ResponseUtils.sendMsg((int) 0, (Object) uuid);
	}
	
	
	@RequestMapping(value = "/payrollExport.do", method = RequestMethod.GET)
	public void payrollExport(HttpServletRequest request, HttpServletResponse response, String id) {
		HttpSession session = request.getSession();
		StringBuffer buffer = (StringBuffer) session.getAttribute(id);
		session.removeAttribute(id);
		InputStream input = new ByteArrayInputStream(buffer.toString().getBytes());
		try {
			response.reset();
			response.setContentType("application/octet-stream;charset=utf-8");
			response.setHeader("Content-Disposition", "attachment;filename=\"" + new String(("Payroll.csv").getBytes(), "iso-8859-1") + "\"");
			OutputStream os = response.getOutputStream();
			StreamUtils.copy(input, os);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
