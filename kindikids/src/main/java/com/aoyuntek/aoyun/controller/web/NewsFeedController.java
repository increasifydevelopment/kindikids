package com.aoyuntek.aoyun.controller.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aoyuntek.aoyun.condtion.NewsCondition;
import com.aoyuntek.aoyun.constants.DateFormatterConstants;
import com.aoyuntek.aoyun.entity.po.NewsCommentInfo;
import com.aoyuntek.aoyun.entity.po.NewsInfo;
import com.aoyuntek.aoyun.entity.vo.NewsFeedInfoVo;
import com.aoyuntek.aoyun.entity.vo.NewsInfoListVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.service.INewsInfoService;
import com.aoyuntek.framework.model.Pager;
import com.theone.common.util.ServiceResult;
import com.theone.json.util.JsonUtil;
import com.theone.string.util.StringUtil;
import com.theone.web.util.ResponseUtils;

/**
 * @description 微博模块
 * @author gfwang
 * @create 2016年6月27日下午2:12:35
 * @version 1.0
 */
@Controller
@RequestMapping("/news")
public class NewsFeedController extends BaseWebController {

	/**
	 * INewsInfoService
	 */
	@Autowired
	private INewsInfoService newsInfoService;

	/**
	 * 
	 * @description 获取当前园区内，当前登录人可查看的所有微博
	 * @author bbq
	 * @create 2016年6月27日上午9:59:42
	 * @version 1.0
	 * @param request
	 *            HttpServletRequest对象
	 * @param condition
	 *            微博查询条件
	 * @return lists
	 */
	@ResponseBody
	@RequestMapping("/list.do")
	public Object list(HttpServletRequest request) {
		String jsonStr = ServletRequestUtils.getStringParameter(request, "params", "");
		NewsCondition condition = (NewsCondition) JsonUtil.jsonToBean(jsonStr, NewsCondition.class, DateFormatterConstants.SYS_T_FORMAT);
		// 获取当前用户accountId
		UserInfoVo currentUser = getCurrentUser(request);
		long start = System.currentTimeMillis();
		ServiceResult<Pager<NewsInfoListVo, NewsCondition>> result = newsInfoService.saveGetNewsList(condition, currentUser);
		long end = System.currentTimeMillis();
		System.err.println("---------------------->" + (end - start));
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	/**
	 * @description 打开一条微博
	 * @author bbq
	 * @create 2016年6月27日上午10:01:33
	 * @version 1.0
	 * @param newsId
	 *            微博ID
	 * @param centersId
	 *            园区ID
	 * @return 微博+评论
	 */
	@ResponseBody
	@RequestMapping("/info.do")
	public Object getInfo(HttpServletRequest request, String id) {
		UserInfoVo currentUser = getCurrentUser(request);
		ServiceResult<NewsFeedInfoVo> result = newsInfoService.getNews(id, currentUser);
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	/**
	 * @description 手动发送新微博
	 * @author hxzhang
	 * @create 2016年6月27日下午3:29:53
	 * @version 1.0
	 * @param request
	 *            HttpServletRequest
	 * @param newsInfo
	 *            newsInfo实体
	 * @param centersInfo
	 *            centersInfo实体
	 * @return 返回操作结果
	 */
	@ResponseBody
	@RequestMapping("/send.do")
	public Object manualSendNews(HttpServletRequest request) {
		String jsonStr = ServletRequestUtils.getStringParameter(request, "params", "");
		NewsFeedInfoVo newsFeedInfoVo = (NewsFeedInfoVo) JsonUtil.jsonToBean(jsonStr, NewsFeedInfoVo.class, DateFormatterConstants.SYS_FORMAT);
		ServiceResult<Object> result = newsInfoService.addOrUpdateNews(newsFeedInfoVo, getCurrentUser(request));
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	/**
	 * @description 评论|回复
	 * @author hxzhang
	 * @create 2016年6月27日上午10:03:21
	 * @version 1.0
	 * @param request
	 * @return 成功|失败
	 */
	@ResponseBody
	@RequestMapping("/comment.do")
	public Object comment(HttpServletRequest request) {
		String jsonStr = ServletRequestUtils.getStringParameter(request, "params", "");
		NewsCommentInfo newsCommentInfo = (NewsCommentInfo) JsonUtil.jsonToBean(jsonStr, NewsCommentInfo.class, DateFormatterConstants.SYS_FORMAT);
		// 获取当前登录用户
		UserInfoVo currentUser = getCurrentUser(request);
		if (StringUtil.isNotEmpty(newsCommentInfo.getContent()) && newsCommentInfo.getContent().length() > 255) {
			return sendFailResponse(getTipMsg("comment_length"));
		}
		// 保存newsCommentInfo
		ServiceResult<Object> result = newsInfoService.addOrUpdateNewsComment(newsCommentInfo, currentUser);
		if (result.isSuccess()) {
			NewsCommentInfo info = (NewsCommentInfo) result.getReturnObj();
			newsInfoService.updateParentComment(info.getNewsId());
		}
		return ResponseUtils.sendMsg(result.getCode(), result.getMsg());
	}

	/**
	 * 
	 * @description 删除微博
	 * @author bqbao
	 * @create 2016年6月27日上午10:11:25
	 * @version 1.0
	 * @param newsId
	 *            微博ID
	 * @return true|false
	 */
	// @UserAuthority(roles = { Role.Director, Role.CentreManager })
	@ResponseBody
	@RequestMapping("/delete.news.do")
	public Object deleteNews(HttpServletRequest request, String id) {
		// 获取当前登录用户
		UserInfoVo currentUser = getCurrentUser(request);
		ServiceResult<Integer> result = newsInfoService.removeNewsByNewsId(id, currentUser);
		return ResponseUtils.sendMsg(result.getCode(), result.getMsg());
	}

	/**
	 * 
	 * @description 删除评论|回复
	 * @author bqbao
	 * @create 2016年6月27日上午10:12:07
	 * @version 1.0
	 * @param newsCommentId
	 *            评论ID
	 * @return true|false
	 */
	@ResponseBody
	@RequestMapping("/delete.commont.do")
	public Object deleteCommont(HttpServletRequest request, String id) {
		// 获取当前登录用户
		UserInfoVo currentUser = getCurrentUser(request);
		ServiceResult<Object> result = newsInfoService.removeNewsCommentsBynewsCommentId(id, currentUser);
		if (result.isSuccess()) {
			NewsInfo info = (NewsInfo) result.getReturnObj();
			newsInfoService.updateParentComment(info.getId());
		}
		return ResponseUtils.sendMsg(result.getCode(), result.getMsg());
	}

	/**
	 * @description 审核news
	 * @author hxzhang
	 * @create 2016年7月14日下午7:23:43
	 * @version 1.0
	 * @param request
	 *            HttpServletRequest
	 * @param id
	 *            微博ID
	 * @return 返回操作结果
	 */
	@ResponseBody
	@RequestMapping("/approve.do")
	public Object approveNews(HttpServletRequest request, String id) {
		// 获取当前登录用户
		UserInfoVo currentUser = getCurrentUser(request);
		// 批准微博
		ServiceResult<Object> result = newsInfoService.updateNews(id, currentUser);
		return ResponseUtils.sendMsg(result.getCode(), result.getMsg());
	}

	/**
	 * @description 将通过审核的news变为草稿
	 * @author hxzhang
	 * @create 2016年7月14日下午7:22:26
	 * @version 1.0
	 * @param request
	 *            HttpServletRequest
	 * @param id
	 *            微博ID
	 * @return 返回操作结果
	 */
	@ResponseBody
	@RequestMapping("/unApprove.do")
	public Object unApproveNews(HttpServletRequest request, String id) {
		// 获取当前登录用户
		UserInfoVo currentUser = getCurrentUser(request);
		// 取消批准微博
		ServiceResult<Object> result = newsInfoService.updateUnApproveNews(id, currentUser);
		return ResponseUtils.sendMsg(result.getCode(), result.getMsg());
	}

	/**
	 * @description 微博评分
	 * @author hxzhang
	 * @create 2016年7月14日下午7:57:28
	 * @version 1.0
	 * @param request
	 *            HttpServletRequest
	 * @param id
	 *            微博ID
	 * @param score
	 *            评分
	 * @return 返回结果
	 */
	@ResponseBody
	@RequestMapping("/score.do")
	public Object score(HttpServletRequest request, String id, String score) {
		// 获取当前登录用户
		UserInfoVo currentUser = getCurrentUser(request);
		ServiceResult<Object> result = newsInfoService.putScore(id, score, currentUser);
		return ResponseUtils.sendMsg(result.getCode(), result.getMsg());
	}

	/**
	 * @description Newsfeed下载PDF
	 * @author Hxzhang
	 * @create 2016年9月26日下午2:06:34
	 */
	@ResponseBody
	@RequestMapping(value = "/pdf.do", method = RequestMethod.GET)
	public void NewsfeedPdf(HttpServletRequest request, HttpServletResponse response, String id) throws Exception {
		// 设置文件后缀
		// String fn = warrantyInfo.getWarrantyNum() + "_certificate.pdf";
		String fn = "PDF.pdf";
		// 读取字符编码
		String csvEncoding = "UTF-8";
		// 设置响应
		response.setCharacterEncoding(csvEncoding);
		response.setContentType("text/pdf; charset=" + csvEncoding);
		response.setHeader("Content-Disposition", "attachment; filename=" + new String(fn.getBytes(), csvEncoding));
		try {
			newsInfoService.getNewsfeedPdf(id, response.getOutputStream(), getCurrentUser(request));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @author hxzhang 2018年11月7日
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/feedback.do", method = RequestMethod.POST)
	public Object newFeedback(HttpServletRequest request, String params) {
		String jsonStr = ServletRequestUtils.getStringParameter(request, "params", "");
		NewsCondition condition = (NewsCondition) JsonUtil.jsonToBean(jsonStr, NewsCondition.class, DateFormatterConstants.SYS_T_FORMAT);
		ServiceResult<Object> result = newsInfoService.getNewFeedback(condition, getCurrentUser(request));
		return ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
	}
}
