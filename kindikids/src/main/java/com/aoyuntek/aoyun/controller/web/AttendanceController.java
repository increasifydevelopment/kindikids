package com.aoyuntek.aoyun.controller.web;

import java.io.OutputStream;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aoyuntek.aoyun.condtion.ApproveAddCondition;
import com.aoyuntek.aoyun.condtion.AttendanceCondtion;
import com.aoyuntek.aoyun.condtion.AttendanceManagementCondition;
import com.aoyuntek.aoyun.condtion.ExternalListCondition;
import com.aoyuntek.aoyun.constants.DateFormatterConstants;
import com.aoyuntek.aoyun.dao.ChildAttendanceInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.entity.po.ChangeAttendanceRequestInfo;
import com.aoyuntek.aoyun.entity.po.ChildAttendanceInfo;
import com.aoyuntek.aoyun.entity.po.ReplaceChildAttendanceVo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.vo.AttendanceChildVo;
import com.aoyuntek.aoyun.entity.vo.AttendanceReturnVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.child.AttendancePostion;
import com.aoyuntek.aoyun.enums.AttendanceRequestType;
import com.aoyuntek.aoyun.enums.ChangeAttendanceRequestType;
import com.aoyuntek.aoyun.enums.ChildType;
import com.aoyuntek.aoyun.enums.EmailType;
import com.aoyuntek.aoyun.enums.EnrolledState;
import com.aoyuntek.aoyun.enums.RequestLogType;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.factory.AttendanceFactory;
import com.aoyuntek.aoyun.factory.UserFactory;
import com.aoyuntek.aoyun.service.IUserInfoService;
import com.aoyuntek.aoyun.service.attendance.IAttendanceService;
import com.aoyuntek.aoyun.uitl.DateUtil;
import com.aoyuntek.aoyun.uitl.GsonUtil;
import com.aoyuntek.aoyun.util.AoyunDateEditor;
import com.aoyuntek.framework.annotation.UserAuthority;
import com.theone.common.util.ServiceResult;
import com.theone.string.util.StringUtil;
import com.theone.web.util.ResponseUtils;

@Controller
@RequestMapping(value = "/attendance")
public class AttendanceController extends BaseWebController {

	@Autowired
	private IAttendanceService attendanceService;
	@Autowired
	private ChildAttendanceInfoMapper childAttendanceInfoMapper;
	@Autowired
	private IUserInfoService userInfoService;
	@Autowired
	private UserInfoMapper userInfoMapper;
	@Autowired
	private UserFactory userFactory;
	@Autowired
	private AttendanceFactory attendanceFactory;

	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
	@ResponseBody
	@RequestMapping("/subtractedAttendance.do")
	public Object subtractedAttendance(HttpServletRequest request, String roomId, String childId, Date day, int type, boolean cover, boolean interim) {
		if (!mustInput(childId, roomId)) {
			return ResponseUtils.sendMsg(-1, getTipMsg("illegal.request"));
		}
		if (!mustInput(day, type)) {
			return ResponseUtils.sendMsg(-1, getTipMsg("illegal.request"));
		}

		// 不能减去以前的历史签到记录
		if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(day, new Date()) > 0) {
			return ResponseUtils.sendMsg(-1, getTipMsg("attendance.subtractedAttendance.day"));
		}

		ServiceResult<Object> result = attendanceService.dealSubtractedAttendance(getCurrentUser(request), roomId, childId, day, type, cover, interim);
		return ResponseUtils.sendMsg(result.getCode(), result.getMsg());
	}

	@ResponseBody
	@RequestMapping("/getRequestLogObj.do")
	public Object getRequestLogObj(HttpServletRequest request, String objId, int type) {
		ServiceResult<Object> result = attendanceService.getRequestLogObj(getCurrentUser(request), objId, type);
		return ResponseUtils.sendMsg(result.isSuccess() ? 0 : -1, result.getMsg(), result.getReturnObj());
	}

	@ResponseBody
	@RequestMapping("/updateAttendanceRequest.do")
	public Object updateAttendanceRequest(HttpServletRequest request, String objId, int value) {
		ServiceResult<Object> result = attendanceService.updateAttendanceRequest(getCurrentUser(request), objId, (short) value);
		return ResponseUtils.sendMsg(result.isSuccess() ? 0 : -1, result.getMsg(), result.getReturnObj());
	}

	@ResponseBody
	@RequestMapping("/updateTemporaryRequest.do")
	public Object updateTemporaryRequest(HttpServletRequest request, int type, String objId, int value) {
		ServiceResult<Object> result = attendanceService.updateTemporaryRequest(getCurrentUser(request), type, objId, (short) value);
		return ResponseUtils.sendMsg(result.isSuccess() ? 0 : -1, result.getMsg(), result.getReturnObj());
	}

	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
	@ResponseBody
	@RequestMapping("/approveReplace.do")
	public Object approveReplace(HttpServletRequest request, String absenteeId, String oldChildId, Date day, int dayOrInstances, int type, boolean overOld) {
		if (!mustInput(absenteeId, oldChildId)) {
			return ResponseUtils.sendMsg(-1, getTipMsg("illegal.request"));
		}
		if (!mustInput(day, dayOrInstances, type)) {
			return ResponseUtils.sendMsg(-1, getTipMsg("illegal.request"));
		}

		ServiceResult<Object> result = attendanceService.dealApproveReplace(getCurrentUser(request), absenteeId, oldChildId, day, dayOrInstances, type, overOld);
		return ResponseUtils.sendMsg(result.getCode(), result.getMsg());
	}

	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
	@ResponseBody
	@RequestMapping("/approveAdd.do")
	public Object approveAdd(HttpServletRequest request, ApproveAddCondition condtion) {
		if (!mustInput(condtion.getRoomId(), condtion.getChildId())) {
			return ResponseUtils.sendMsg(-1, getTipMsg("illegal.request"));
		}
		if (!mustInput(condtion.getDay())) {
			return ResponseUtils.sendMsg(-1, getTipMsg("illegal.request"));
		}
		UserInfo child = userInfoMapper.getUserInfoByAccountId(condtion.getChildId());
		ChildAttendanceInfo oldAttendanceInfo = childAttendanceInfoMapper.selectByUserId(child.getId());
		ServiceResult<ChildAttendanceInfo> result = attendanceService.dealApproveAdd(getCurrentUser(request), condtion);
		// 处理成功并且不是临时安排内部孩子上课
		if (result.isSuccess() && !condtion.isInterim()) {
			if (oldAttendanceInfo.getEnrolled() == EnrolledState.Default.getValue()) {
				ChildAttendanceInfo newAttendanceInfo = childAttendanceInfoMapper.selectByUserId(child.getId());
				if (newAttendanceInfo.getEnrolled() == EnrolledState.Enrolled.getValue()) {
					// familyService.sendEmailForParent(child.getFamilyId(),
					// true, false);
					// 发送入园邮件
					// userFactory.sendParentEmailByChild(getSystemEmailMsg("into_center_title"),
					// getSystemEmailMsg("into_center_content"),
					// condtion.getChildId(), true,
					// EmailType.intoCenter.getValue(), null, null, null,
					// false);
				}
			} else {
				// 非外部小孩,既是Change attendance的小孩(等于是approved了Change
				// attendance申请需要给家长发送邮件)
				AttendancePostion attendancePostion = attendanceFactory.getChildPostion(condtion.getChildId(), condtion.getDay());
				oldAttendanceInfo.setNewMonday(attendancePostion.getMonday());
				oldAttendanceInfo.setNewTuesday(attendancePostion.getTuesday());
				oldAttendanceInfo.setNewWednesday(attendancePostion.getWednesday());
				oldAttendanceInfo.setNewThursday(attendancePostion.getThursday());
				oldAttendanceInfo.setNewFriday(attendancePostion.getFriday());
				oldAttendanceInfo.setRequestId(result.getReturnObj().getRequestId());

				String title = getSystemEmailMsg("approve_attendance_title");
				String content = getSystemEmailMsg("approve_attendance_content");

				userFactory.sendParentEmailByChild(title, content, condtion.getChildId(), true, EmailType.approve_attendance.getValue(), null, null, oldAttendanceInfo,
						oldAttendanceInfo.isPlan());
			}
		}
		return ResponseUtils.sendMsg(result.getCode(), result.getMsg());
	}

	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
	@ResponseBody
	@RequestMapping("/getAddChilds.do")
	public Object getAddChilds(HttpServletRequest request, String roomId, Date day) {
		if (!mustInput(roomId)) {
			return ResponseUtils.sendMsg(-1, getTipMsg("illegal.request"));
		}
		if (!mustInput(day)) {
			return ResponseUtils.sendMsg(-1, getTipMsg("illegal.request"));
		}

		ServiceResult<ReplaceChildAttendanceVo> result = attendanceService.getAddChilds(roomId, day);
		return ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
	}

	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
	@ResponseBody
	@RequestMapping("/replaceChildAttendance.do")
	public Object replaceChildAttendance(HttpServletRequest request, String absenteeId, String childId, Date day) {
		if (!mustInput(absenteeId, childId)) {
			return ResponseUtils.sendMsg(-1, getTipMsg("illegal.request"));
		}
		if (!mustInput(day)) {
			return ResponseUtils.sendMsg(-1, getTipMsg("illegal.request"));
		}

		ServiceResult<ReplaceChildAttendanceVo> result = attendanceService.getReplaceChilds(absenteeId, childId, day);
		return ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
	}

	@ResponseBody
	@RequestMapping("/getAttendance.do")
	public Object getAttendance(HttpServletRequest request, String userId) {
		return ResponseUtils.sendMsg(0, attendanceService.getAttendance(userId).getReturnObj());
	}

	@ResponseBody
	@RequestMapping("/submitChangeAttendance.do")
	public Object submitChangeAttendance(HttpServletRequest request, String json, boolean isOverOld, boolean isFullOver) {
		if (StringUtil.isEmpty(json)) {
			return ResponseUtils.sendMsg(-1, getTipMsg("illegal.request"));
		}

		ChangeAttendanceRequestInfo re = (ChangeAttendanceRequestInfo) GsonUtil.jsonToBean(json, ChangeAttendanceRequestInfo.class, DateFormatterConstants.SYS_T_FORMAT2,
				DateFormatterConstants.SYS_FORMAT2, DateFormatterConstants.SYS_T_FORMAT);
		boolean isPlan = re.isPlan();
		UserInfoVo currentUser = getCurrentUser(request);

		boolean isCeo = containRole(Role.CEO, currentUser.getRoleInfoVoList()) == 1;
		boolean isCentreManager = (containRole(Role.CentreManager, currentUser.getRoleInfoVoList()) == 1)
				|| (containRole(Role.EducatorSecondInCharge, currentUser.getRoleInfoVoList()) == 1);
		boolean isParent = containRole(Role.Parent, currentUser.getRoleInfoVoList()) == 1;

		boolean isAdd = StringUtil.isEmpty(re.getId());

		// 只有CEO和CentreManager才能Enrol Child
		if (!(isCeo || isCentreManager) && re.getSourceType() == RequestLogType.CEOExternal2Inside.getValue()) {
			return ResponseUtils.sendMsg(-1, getTipMsg("no.permissions"));
		}

		if (isCentreManager && (StringUtil.isEmpty(currentUser.getUserInfo().getCentersId()))) {
			return ResponseUtils.sendMsg(-1, getTipMsg("no.permissions"));
		}

		// 转换前台请求
		attendanceFactory.convertSourceType(re);

		ChildAttendanceInfo oldAttendanceInfo = childAttendanceInfoMapper.selectByUserId(userInfoMapper.getUserInfoByAccountId(re.getChildId()).getId());
		// 已经是内部孩子就不能再次Enrol Child
		if (re.getSourceType() == RequestLogType.CEOExternal2Inside.getValue() && oldAttendanceInfo.getType() == RequestLogType.ParentRequest.getValue()) {
			return ResponseUtils.sendMsg(-1, getTipMsg("no.permissions"));
		}
		// 【外到内的情况例外，其它必須是入園的 內部的】
		if (re.getSourceType() != ChangeAttendanceRequestType.CEOExternal2Inside.getValue()) {
			// 还未入院 或者是外部的 不能申请
			if ((oldAttendanceInfo.getEnrolled() == EnrolledState.Default.getValue())) {
				return ResponseUtils.sendMsg(-1, getTipMsg("submitChangeAttendanceRequest.enrolledState.false"));
			}

			if (oldAttendanceInfo.getType() == ChildType.External.getValue()) {
				return ResponseUtils.sendMsg(-1, getTipMsg("submitChangeAttendanceRequest.external"));
			}
		}

		re.setMonday((short) (BooleanUtils.isTrue(re.getMondayBool()) ? 1 : 0));
		re.setTuesday((short) (BooleanUtils.isTrue(re.getTuesdayBool()) ? 1 : 0));
		re.setThursday((short) (BooleanUtils.isTrue(re.getThursdayBool()) ? 1 : 0));
		re.setWednesday((short) (BooleanUtils.isTrue(re.getWednesdayBool()) ? 1 : 0));
		re.setFriday((short) (BooleanUtils.isTrue(re.getFridayBool()) ? 1 : 0));

		if (!mustInput(re.getChildId())) {
			return ResponseUtils.sendMsg(-1, getTipMsg("illegal.request"));
		}
		if (!mustInput(re.getChangeDate())) {
			return ResponseUtils.sendMsg(-1, getTipMsg("illegal.request"));
		}
		if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(new Date(), re.getChangeDate()) < 0) {
			return ResponseUtils.sendMsg(-1, getTipMsg("submitChangeAttendance.day.changeDate"));
		}

		AttendanceRequestType attendanceRequestType = AttendanceRequestType.getType(re.getType());
		switch (attendanceRequestType) {
		case K: {
			// 如果是家长申请
			if (isParent) {
				if (!mustCheckNum(2, re.getMondayBool(), re.getTuesdayBool(), re.getThursdayBool(), re.getWednesdayBool(), re.getFridayBool())) {
					return ResponseUtils.sendMsg(-1, getTipMsg("illegal.request"));
				}
				// 选择的时间不能<=now
				if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(new Date(), re.getChangeDate()) <= 0) {
					return ResponseUtils.sendMsg(-1, getTipMsg("submitChangeAttendanceRequest.prant.time"));
				}
				boolean noChange = (oldAttendanceInfo.getMonday() == re.getMondayBool()) && (oldAttendanceInfo.getTuesday() == re.getTuesdayBool())
						&& (oldAttendanceInfo.getThursday() == re.getThursdayBool()) && (oldAttendanceInfo.getWednesday() == re.getWednesdayBool())
						&& (oldAttendanceInfo.getFriday() == re.getFridayBool());
				// 如果请求的当前上课时间跟原来一样 那么给提示
				if (noChange) {
					return ResponseUtils.sendMsg(102, getTipMsg("submitChangeAttendanceRequest.notChanage"));
				}

				// 判斷 如果以前是1 2 4 现在申请 1 2 那么提示
				// boolean isOldContain = attendanceFactory.isOldContain(re,
				// oldAttendanceInfo);
				// if (isOldContain) {
				// return ResponseUtils.sendMsg(-1,
				// getTipMsg("submitChangeAttendanceRequest.old.contain"));
				// }
				// (by hxzhang 家长可取消上课周期)
			} else {
				if (!mustCheckNum(1, re.getMondayBool(), re.getTuesdayBool(), re.getThursdayBool(), re.getWednesdayBool(), re.getFridayBool())) {
					return ResponseUtils.sendMsg(-1, getTipMsg("illegal.request"));
				}
			}
		}
			break;
		case R: {
			if (!mustInput(re.getNewRoomId(), re.getNewGroupId())) {
				return ResponseUtils.sendMsg(-1, getTipMsg("illegal.request"));
			}
			if (!mustCheckNum(1, re.getMondayBool(), re.getTuesdayBool(), re.getThursdayBool(), re.getWednesdayBool(), re.getFridayBool())) {
				return ResponseUtils.sendMsg(-1, getTipMsg("illegal.request"));
			}
		}
			break;
		case C: {
			// 是否是CEO或者是园长同意转园
			boolean approveC = false;
			if (isCeo) {
				approveC = true;
			} else if (isCentreManager) {
				// 院长同意转院
				if (StringUtil.isNotEmpty(re.getId())) {
					approveC = true;
				}
			}
			if (approveC) {
				if (!mustInput(re.getNewCentreId(), re.getNewRoomId())) {
					return ResponseUtils.sendMsg(-1, getTipMsg("illegal.request"));
				}
				if (!mustCheckNum(1, re.getMondayBool(), re.getTuesdayBool(), re.getThursdayBool(), re.getWednesdayBool(), re.getFridayBool())) {
					return ResponseUtils.sendMsg(-1, getTipMsg("illegal.request"));
				}
			} else {
				// 园长申请转园
				if (!mustInput(re.getNewCentreId())) {
					return ResponseUtils.sendMsg(-1, getTipMsg("illegal.request"));
				}
			}
		}
			break;
		default:
			break;
		}
		ServiceResult<ChildAttendanceInfo> result = attendanceService.submitChangeAttendanceRequest(getCurrentUser(request), re, isOverOld, isFullOver);
		// 家长申请 Request to Change Attendance
		if (result.isSuccess() && (re.getSourceType() == RequestLogType.ParentRequest.getValue() || re.getSourceType() == RequestLogType.ChangeAttendance.getValue())) {
			if (re.getSourceType() == RequestLogType.ParentRequest.getValue()) {
				oldAttendanceInfo.setNewMonday(re.getMondayBool());
				oldAttendanceInfo.setNewTuesday(re.getTuesdayBool());
				oldAttendanceInfo.setNewWednesday(re.getWednesdayBool());
				oldAttendanceInfo.setNewThursday(re.getThursdayBool());
				oldAttendanceInfo.setNewFriday(re.getFridayBool());

				String title = getSystemEmailMsg("request_attendance_title");
				String content = getSystemEmailMsg("request_attendance_content");
				// 给家长发送邮件
				userFactory.sendParentEmailByChild(title, content, re.getChildId(), true, EmailType.request_attend.getValue(), null, null, oldAttendanceInfo, isPlan);
				// 给管理员发送邮件
				if (re.getSourceType() == RequestLogType.ParentRequest.getValue()) {
					userFactory.sendEmailCeoManager(title, content, re.getChildId(), EmailType.request_attend.getValue(), null, null, oldAttendanceInfo);
				}
			} else {
				oldAttendanceInfo.setNewMonday(re.getMondayBool());
				oldAttendanceInfo.setNewTuesday(re.getTuesdayBool());
				oldAttendanceInfo.setNewWednesday(re.getWednesdayBool());
				oldAttendanceInfo.setNewThursday(re.getThursdayBool());
				oldAttendanceInfo.setNewFriday(re.getFridayBool());
				oldAttendanceInfo.setRequestId(result.getReturnObj().getRequestId());
				oldAttendanceInfo.setPlan(re.isPlan());

				String title = getSystemEmailMsg("approve_attendance_title");
				String content = getSystemEmailMsg("approve_attendance_content");
				// 给家长发送邮件
				userFactory.sendParentEmailByChild(title, content, re.getChildId(), true, EmailType.approve_attendance.getValue(), null, null, oldAttendanceInfo, isPlan);
			}
		}
		if (oldAttendanceInfo.getEnrolled() == EnrolledState.Default.getValue()) {
			if (result.isSuccess() && isAdd && result.getReturnObj().getEnrolled() == EnrolledState.Enrolled.getValue()) {
				UserInfo childInfo = userInfoService.getUserInfoByAccountId(re.getChildId());
				// 入园邮件
				// userFactory.sendParentEmailByChild(getSystemEmailMsg("into_center_title"),
				// getSystemEmailMsg("into_center_content"), re.getChildId(),
				// true,
				// EmailType.intoCenter.getValue(), null, null, null, isPlan);
				// 给家长发送欢迎邮件
				// familyService.sendEmailForParent(childInfo.getFamilyId(),
				// true, isPlan);
			}
		}

		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	private Date getAppointDate(Date date, ChangeAttendanceRequestInfo re) {
		int i = DateUtil.getDayOfWeek(date);
		int ii = 0;
		if (re.getMondayBool()) {
			ii = 2;
		}
		if (re.getTuesdayBool()) {
			ii = 3;
		}
		if (re.getWednesdayBool()) {
			ii = 4;
		}
		if (re.getThursdayBool()) {
			ii = 5;
		}
		if (re.getFridayBool()) {
			ii = 6;
		}
		int addDay = 0;
		if (ii < i) {
			addDay = ii + 7 - i;
		} else {
			addDay = ii - i;
		}
		return DateUtil.addDay(date, addDay);
	}

	public static void main(String[] args) {
		int d = DateUtil.getDayOfWeek(DateUtil.parse("2017-06-09", "yyyy-MM-dd"));

		int dd = 2;
		if (dd < d) {
			System.err.println(dd + 7 - d);
		}
		System.err.println(DateUtil.getDayOfWeek(DateUtil.parse("2017-06-09", "yyyy-MM-dd")));

		System.err.println(DateUtil.addDay(DateUtil.parse("2017-06-09", "yyyy-MM-dd"), dd + 7 - d));
	}

	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
	@ResponseBody
	@RequestMapping("/getAttendanceList.do")
	public Object getAttendanceList(HttpServletRequest request, AttendanceCondtion attendanceCondtion) {
		logRequest(request);
		ServiceResult<AttendanceReturnVo> result = attendanceService.getAttendanceList(getCurrentUser(request), attendanceCondtion);
		return ResponseUtils.sendMsg(result.isSuccess() ? 0 : -1, result.getMsg(), result.getReturnObj());
	}

	@ResponseBody
	@RequestMapping("/getTodayAttendanceList.do")
	public Object getTodayAttendanceList(HttpServletRequest request, String centerId, String name, int type) {
		if (!mustInput(centerId)) {
			return ResponseUtils.sendMsg(-1, getTipMsg("illegal.request"));
		}
		ServiceResult<List<AttendanceChildVo>> result = attendanceService.getTodayAttendanceList(centerId, name, type);
		return ResponseUtils.sendMsg(result.isSuccess() ? 0 : -1, result.getMsg(), result.getReturnObj());
	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(Date.class, new AoyunDateEditor());
	}

	@RequestMapping("/getPagarList.do")
	@ResponseBody
	public Object getAttendanceManagement(HttpServletRequest request, String params) {
		AttendanceManagementCondition condition = (AttendanceManagementCondition) GsonUtil.jsonToBean(params, AttendanceManagementCondition.class,
				DateFormatterConstants.SYS_T_FORMAT2, DateFormatterConstants.SYS_FORMAT2, DateFormatterConstants.SYS_T_FORMAT);
		ServiceResult<Object> result = attendanceService.getList(getCurrentUser(request), condition);
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	@RequestMapping("/getExternalList.do")
	@ResponseBody
	public Object getExternalList(HttpServletRequest request, String params) {
		ExternalListCondition condition = (ExternalListCondition) GsonUtil.jsonToBean(params, ExternalListCondition.class, DateFormatterConstants.SYS_T_FORMAT2,
				DateFormatterConstants.SYS_FORMAT2, DateFormatterConstants.SYS_T_FORMAT);
		ServiceResult<Object> result = attendanceService.getExternalList(getCurrentUser(request), condition);
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());

	}

	@RequestMapping(value = "/csv.do", method = RequestMethod.GET)
	public void exportCsv(HttpServletRequest request, HttpServletResponse response, AttendanceManagementCondition condition) throws Exception {
		String content = attendanceService.exprotCsv(getCurrentUser(request), condition);
		String fn = "Attendance Management" + ".csv";
		String csvEncoding = "UTF-8";
		response.setCharacterEncoding(csvEncoding);
		response.setContentType("text/csv; charset=" + csvEncoding);
		response.setHeader("Pragma", "public");
		response.setHeader("Cache-Control", "max-age=30");
		response.setHeader("Content-Disposition", "attachment; filename=" + new String(fn.getBytes(), csvEncoding));
		OutputStream os = response.getOutputStream();
		os.write(content.getBytes("GBK"));
		os.flush();
		os.close();
	}
}
