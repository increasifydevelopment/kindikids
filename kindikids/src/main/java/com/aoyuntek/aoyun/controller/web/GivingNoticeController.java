package com.aoyuntek.aoyun.controller.web;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aoyuntek.aoyun.constants.DateFormatterConstants;
import com.aoyuntek.aoyun.dao.GivingNoticeInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.entity.po.GivingNoticeInfoWithBLOBs;
import com.aoyuntek.aoyun.enums.ChangeAttendanceRequestState;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.service.attendance.IGivingNoticeInfoService;
import com.aoyuntek.aoyun.service.attendance.IAttendanceCoreService;
import com.aoyuntek.aoyun.uitl.GsonUtil;
import com.aoyuntek.framework.annotation.UserAuthority;
import com.theone.common.util.ServiceResult;
import com.theone.web.util.ResponseUtils;

@Controller
@RequestMapping("/givingNotice")
public class GivingNoticeController extends BaseWebController {
    @Autowired
    private IGivingNoticeInfoService givingNoticeInfoService;
    @Autowired
    private GivingNoticeInfoMapper givingNoticeInfoMapper;
    @Autowired
    private IAttendanceCoreService systemCoreService;
    @Autowired
    private UserInfoMapper userInfoMapper;

    /**
     * @description 获取离园申请信息
     * @author hxzhang
     * @create 2016年8月26日下午3:11:22
     * @version 1.0
     * @param request
     * @param id
     * @return
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Parent, Role.Cook })
    @ResponseBody
    @RequestMapping(value = "/getGivingNoticeInfo.do", method = RequestMethod.GET)
    public Object getGivingNoticeInfo(HttpServletRequest request, String id) {
        ServiceResult<Object> result = givingNoticeInfoService.getGivingNoticeInfo(id);
        return ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
    }

    /**
     * @description 新增或更新离园申请信息
     * @author hxzhang
     * @create 2016年8月26日下午3:11:58
     * @version 1.0
     * @param request
     * @return
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge, Role.Parent })
    @ResponseBody
    @RequestMapping(value = "/saveGivingNoticeInfo.do", method = RequestMethod.POST)
    public Object saveGivingNoticeInfo(HttpServletRequest request) {
        String jsonStr = ServletRequestUtils.getStringParameter(request, "params", "");
        GivingNoticeInfoWithBLOBs givingNoticeInfoWithBLOBs = (GivingNoticeInfoWithBLOBs) GsonUtil.jsonToBean(jsonStr,
                GivingNoticeInfoWithBLOBs.class, DateFormatterConstants.SYS_T_FORMAT, DateFormatterConstants.SYS_FORMAT2);
        ServiceResult<Object> result = givingNoticeInfoService.addOrUpdateGivingNoticeInfo(givingNoticeInfoWithBLOBs, getCurrentUser(request));
        return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg());
    }

    /**
     * @description 家长取消离园申请
     * @author hxzhang
     * @create 2016年8月26日下午3:12:29
     * @version 1.0
     * @param requst
     * @param id
     * @return
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge, Role.Parent })
    @ResponseBody
    @RequestMapping(value = "/cancelGivingNotice.do", method = RequestMethod.GET)
    public Object cancelGivingNotice(HttpServletRequest request, String id) {
        ServiceResult<Object> result = givingNoticeInfoService.updateOperaGivingNotice(id, ChangeAttendanceRequestState.Cancel.getValue(),
                getCurrentUser(request));
        return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg());
    }

    /**
     * @description 管理员拒绝离园申请
     * @author hxzhang
     * @create 2016年8月26日下午3:12:58
     * @version 1.0
     * @param requst
     * @param id
     * @return
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/rejectGivingNotice.do", method = RequestMethod.GET)
    public Object rejectGivingNotice(HttpServletRequest request, String id) {
        ServiceResult<Object> result = givingNoticeInfoService.updateOperaGivingNotice(id, ChangeAttendanceRequestState.Discard.getValue(),
                getCurrentUser(request));
        return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg());
    }

    /**
     * @description 是否有request的GivingNotice
     * @author hxzhang
     * @create 2016年8月28日下午3:50:20
     * @version 1.0
     * @param requst
     * @param id
     * @return
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Parent, Role.Cook })
    @ResponseBody
    @RequestMapping(value = "/haveGivingNotice.do", method = RequestMethod.GET)
    public Object haveGivingNotice(HttpServletRequest requst, String id) {
        int count = givingNoticeInfoMapper.getCountByChildId(id);
        return ResponseUtils.sendMsg(0, count);
    }
}
