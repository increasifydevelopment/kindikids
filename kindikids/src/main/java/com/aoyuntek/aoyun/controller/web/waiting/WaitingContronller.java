package com.aoyuntek.aoyun.controller.web.waiting;

import java.io.OutputStream;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aoyuntek.aoyun.condtion.waitingList.WaitingListCondition;
import com.aoyuntek.aoyun.constants.DateFormatterConstants;
import com.aoyuntek.aoyun.controller.web.BaseWebController;
import com.aoyuntek.aoyun.entity.po.ApplicationList;
import com.aoyuntek.aoyun.entity.vo.waiting.SubmitModel;
import com.aoyuntek.aoyun.entity.vo.waiting.WaitingListVo;
import com.aoyuntek.aoyun.service.waiting.IWaitingService;
import com.aoyuntek.aoyun.uitl.GsonUtil;
import com.theone.common.util.ServiceResult;
import com.theone.web.util.RequestUtil;
import com.theone.web.util.ResponseUtils;

@RequestMapping("/waiting")
@Controller
public class WaitingContronller extends BaseWebController {
	@Autowired
	private IWaitingService waitingService;

	@RequestMapping(value = "/list.do", method = RequestMethod.POST)
	@ResponseBody
	public Object waitingList(HttpServletRequest request, String params) {
		WaitingListCondition condition = (WaitingListCondition) GsonUtil.jsonToBean(params, WaitingListCondition.class, DateFormatterConstants.SYS_T_FORMAT2,
				DateFormatterConstants.SYS_FORMAT2, DateFormatterConstants.SYS_T_FORMAT);
		ServiceResult<Object> result = waitingService.getList(condition, getCurrentUser(request));
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	@RequestMapping(value = "/details.do", method = RequestMethod.POST)
	@ResponseBody
	public Object childDetails(HttpServletRequest request, String id) {
		ServiceResult<Object> result = waitingService.getDetails(id);
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	@RequestMapping(value = "/count.do", method = RequestMethod.GET)
	@ResponseBody
	public Object listCount(HttpServletRequest request) {
		ServiceResult<Object> result = waitingService.getCount(getCurrentUser(request));
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	@RequestMapping(value = "/position.do", method = RequestMethod.POST)
	@ResponseBody
	public Object positionDate(HttpServletRequest request, String params) {
		WaitingListVo vo = (WaitingListVo) GsonUtil.jsonToBean(params, WaitingListVo.class, DateFormatterConstants.SYS_T_FORMAT2, DateFormatterConstants.SYS_FORMAT2,
				DateFormatterConstants.SYS_T_FORMAT);
		String ip = RequestUtil.getIpAddr(request);
		ServiceResult<Object> result = waitingService.updatePositionDate(vo, ip, getCurrentUser(request));
		return ResponseUtils.sendMsg(result.getCode());
	}

	@RequestMapping(value = "/submitMove.do", method = RequestMethod.POST)
	@ResponseBody
	public Object submitMove(HttpServletRequest request, String params) {
		SubmitModel model = (SubmitModel) GsonUtil.jsonToBean(params, SubmitModel.class);
		ServiceResult<Object> result = waitingService.submitMove(model, getCurrentUser(request));
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	@RequestMapping(value = "/familySelect.do", method = RequestMethod.GET)
	@ResponseBody
	public Object familySelect(HttpServletRequest request, String p) {
		return waitingService.getFamilySelect(p);
	}

	@RequestMapping(value = "/sendPlanEmail.do", method = RequestMethod.POST)
	@ResponseBody
	public void sendPlanEmail(HttpServletRequest request) {
		waitingService.sendPlanEmail();
	}

	@RequestMapping(value = "/save.do", method = RequestMethod.POST)
	@ResponseBody
	public Object save(HttpServletRequest request, String params) {
		ApplicationList app = (ApplicationList) GsonUtil.jsonToBean(params, ApplicationList.class, DateFormatterConstants.SYS_T_FORMAT2,
				DateFormatterConstants.SYS_FORMAT2, DateFormatterConstants.SYS_T_FORMAT);
		String ip = RequestUtil.getIpAddr(request);
		ServiceResult<Object> result = waitingService.save(app, ip, getCurrentUser(request));
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg());
	}

	@ResponseBody
	@RequestMapping(value = "/csv.do", method = RequestMethod.POST)
	public Object csv(HttpServletRequest request, HttpServletResponse response, String params) {
		WaitingListCondition condition = (WaitingListCondition) GsonUtil.jsonToBean(params, WaitingListCondition.class, DateFormatterConstants.SYS_T_FORMAT2,
				DateFormatterConstants.SYS_FORMAT2, DateFormatterConstants.SYS_T_FORMAT);
		String str = waitingService.exportCsv(condition, getCurrentUser(request));
		String uuid = UUID.randomUUID().toString();
		request.getSession().setAttribute(uuid, str);
		return ResponseUtils.sendMsg((Integer) 0, (Object) uuid);
	}

	@RequestMapping(value = "/exportCsv.do", method = RequestMethod.GET)
	public void exportCsv(HttpServletRequest request, HttpServletResponse response, String uuid) throws Exception {
		String content = (String) request.getSession().getAttribute(uuid);
		request.removeAttribute(uuid);
		String fn = "Position Manager" + ".csv";
		String csvEncoding = "UTF-8";
		response.setCharacterEncoding(csvEncoding);
		response.setContentType("text/csv; charset=" + csvEncoding);
		response.setHeader("Pragma", "public");
		response.setHeader("Cache-Control", "max-age=30");
		response.setHeader("Content-Disposition", "attachment; filename=" + new String(fn.getBytes(), csvEncoding));
		OutputStream os = response.getOutputStream();
		os.write(content.getBytes("GBK"));
		os.flush();
		os.close();
	}
}
