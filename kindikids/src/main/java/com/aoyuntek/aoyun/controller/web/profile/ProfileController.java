package com.aoyuntek.aoyun.controller.web.profile;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aoyuntek.aoyun.condtion.ProfileTempCondition;
import com.aoyuntek.aoyun.controller.web.BaseWebController;
import com.aoyuntek.aoyun.entity.po.profile.ProfileTemplateInfo;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.service.IProfileService;
import com.aoyuntek.framework.annotation.UserAuthority;
import com.theone.common.util.ServiceResult;
import com.theone.string.util.StringUtil;
import com.theone.web.util.ResponseUtils;

@Controller
@RequestMapping(value = "/profile")
public class ProfileController extends BaseWebController {
    @Autowired
    private IProfileService profileService;

    /**
     * @description 保存简介模版
     * @author hxzhang
     * @create 2016年10月21日上午9:37:57
     */
    @UserAuthority(roles = { Role.CEO })
    @ResponseBody
    @RequestMapping(value = "/save.do", method = RequestMethod.POST)
    public Object saveProfileTemp(HttpServletRequest request, ProfileTemplateInfo profileTemplateInfo, String formJson) {
        ServiceResult<Object> result = profileService.saveProfileTemp(profileTemplateInfo, getCurrentUser(request), formJson);
        return ResponseUtils.sendMsg(result.getCode(), result.getMsg());
    }

    /**
     * @description 获取模版信息
     * @author hxzhang
     * @create 2016年10月21日上午11:04:16
     */
    @ResponseBody
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge, Role.Parent, Role.Educator, Role.Casual })
    @RequestMapping(value = "/info.do", method = RequestMethod.POST)
    public Object getProfileTempInfo(HttpServletRequest request, String id) {
        ServiceResult<Object> result = profileService.getProfileTempInfo(id);
        return ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
    }

    /**
     * @description 获取分页集合
     * @author hxzhang
     * @create 2016年10月21日上午11:06:03
     */
    @UserAuthority(roles = { Role.CEO })
    @ResponseBody
    @RequestMapping(value = "/list.do", method = RequestMethod.POST)
    public Object getProfileTempList(HttpServletRequest request, ProfileTempCondition condition) {
        ServiceResult<Object> result = profileService.getProfileTempList(condition);
        return ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
    }

    /**
     * @description 启用禁用模版
     * @author hxzhang
     * @create 2016年10月21日上午11:23:02
     */
    @UserAuthority(roles = { Role.CEO })
    @ResponseBody
    @RequestMapping(value = "/opera.do", method = RequestMethod.POST)
    public Object saveOpera(HttpServletRequest request, String id, short state) {
        ServiceResult<Object> result = profileService.saveOpera(id, state, getCurrentUser(request));
        return ResponseUtils.sendMsg(result.getCode(), result.getMsg());
    }

    @ResponseBody
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge, Role.Parent, Role.Educator, Role.Casual })
    @RequestMapping(value = "/getSelectList.do", method = RequestMethod.POST)
    public Object getSelectList(short type) {
        return sendSuccessResponse(profileService.getSelectList(type));
    }

    /**
     * @description 获取用户简介信息
     * @author hxzhang
     * @create 2016年10月21日下午2:07:51
     */
    @UserAuthority(roles = { Role.CEO })
    @ResponseBody
    @RequestMapping(value = "/profile.do", method = RequestMethod.POST)
    public Object getUserProfile(HttpServletRequest request, String id, String templateId) {
        if (StringUtil.isEmpty(id) && StringUtil.isEmpty(templateId)) {
            return sendSuccessResponse();
        }
        if(StringUtil.isEmpty(id)){
            return sendSuccessResponse();
        }
        ServiceResult<Object> result = profileService.getProfileTempByUserId(id, templateId);
        return ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
    }
}
