package com.aoyuntek.aoyun.controller.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aoyuntek.aoyun.constants.DateFormatterConstants;
import com.aoyuntek.aoyun.entity.po.FormInfo;
import com.aoyuntek.aoyun.entity.vo.FormInfoDataVO;
import com.aoyuntek.aoyun.entity.vo.FormInfoVO;
import com.aoyuntek.aoyun.entity.vo.ValueInfoVO;
import com.aoyuntek.aoyun.service.IFormInfoService;
import com.theone.common.util.ServiceResult;
import com.theone.json.util.JsonUtil;
import com.theone.web.util.ResponseUtils;

@Controller
@RequestMapping("/form")
public class FormController extends BaseWebController {

    /**
     * formService
     */
    @Autowired
    private IFormInfoService formService;
    
    /**
     * @param request
     * @return
     * @author jfzhao
     */
    @RequestMapping(value = "/saveFrom.do", method = RequestMethod.POST)
    @ResponseBody
    public Object saveFrom(HttpServletRequest request,String params) {
		FormInfoVO formInfoVO = (FormInfoVO) JsonUtil.jsonToBean(params, FormInfoVO.class, DateFormatterConstants.SYS_T_FORMAT);
        ServiceResult<String> result = formService.saveForm(formInfoVO);
        return ResponseUtils.sendMsg(result.getCode(),result.getReturnObj());
    }

    /**
     * @param request
     * @return
     * @author jfzhao
     */
    @RequestMapping(value = "/getFrom.do", method = RequestMethod.GET)
    @ResponseBody
    public Object getForm(HttpServletRequest request,String code) {
        ServiceResult<FormInfoDataVO> result = formService.getForm(code);
        return ResponseUtils.sendMsg(result.getCode(),result.getReturnObj());
    }

    /**
     * @param request
     * @return
     * @author jfzhao
     */
    @RequestMapping(value = "/listFrom.do", method = RequestMethod.POST)
    @ResponseBody
    public Object listFrom(HttpServletRequest request) {

        ServiceResult<List<FormInfo>> result = new ServiceResult<List<FormInfo>>();
        result.setReturnObj(formService.list());
        result.setCode(0);

        return ResponseUtils.sendMsg(result);
    }
    
    /**
     * @param request
     * @return
     * @author jfzhao
     */
    @RequestMapping(value = "/getInstanceTemplate.do", method = RequestMethod.GET)
    @ResponseBody
    public Object getInstanceTemplate(HttpServletRequest request,String code,String instanceId) {
        ServiceResult<Object> result = formService.getInstanceTemplate(code, instanceId);
        return ResponseUtils.sendMsg(result.getCode(),result.getReturnObj());
    }

    /**
     * @param request
     * @return
     * @author jfzhao
     */
    @RequestMapping(value = "/getInstance.do", method = RequestMethod.GET)
    @ResponseBody
    public Object getInstance(HttpServletRequest request,String instanceId) {
        ServiceResult<ValueInfoVO> result = formService.getInstance(instanceId);
        return ResponseUtils.sendMsg(result.getCode(),result.getReturnObj());
    }
    
    /**
     * @param request
     * @return
     * @author jfzhao
     * @throws Exception 
     */
    @RequestMapping(value = "/saveInstance.do", method = RequestMethod.POST)
    @ResponseBody
    public Object saveInstance(HttpServletRequest request,String params) throws Exception {
    	ValueInfoVO valueInfoVO = (ValueInfoVO) JsonUtil.jsonToBean(params, ValueInfoVO.class);
    	ServiceResult<String> result = formService.saveInstance(valueInfoVO);
        return ResponseUtils.sendMsg(result.getCode(),result.getReturnObj());
    }
}
