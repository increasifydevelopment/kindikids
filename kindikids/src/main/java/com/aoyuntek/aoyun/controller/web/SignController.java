package com.aoyuntek.aoyun.controller.web;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aoyuntek.aoyun.condtion.SignCondition;
import com.aoyuntek.aoyun.constants.DateFormatterConstants;
import com.aoyuntek.aoyun.entity.po.SignVisitorInfoWithBLOBs;
import com.aoyuntek.aoyun.entity.vo.SigninInfoVo;
import com.aoyuntek.aoyun.entity.vo.StaffSignInfoVo;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.enums.SigninType;
import com.aoyuntek.aoyun.factory.UserFactory;
import com.aoyuntek.aoyun.service.attendance.ISignService;
import com.aoyuntek.aoyun.uitl.ExcelUtil;
import com.aoyuntek.aoyun.uitl.ExportConfigure;
import com.aoyuntek.aoyun.uitl.GsonUtil;
import com.aoyuntek.framework.annotation.UserAuthority;
import com.theone.common.util.ServiceResult;
import com.theone.json.util.JsonUtil;
import com.theone.web.util.RequestUtil;
import com.theone.web.util.ResponseUtils;

/**
 * @description 签到控制层
 * @author Hxzhang 2016年9月1日上午10:55:44
 * @version 1.0
 */
@Controller
@RequestMapping("/sign")
public class SignController extends BaseWebController {
    @Autowired
    private ISignService signService;
    @Autowired
    private UserFactory userFactory;

    /**
     * @description 签入
     * @author hxzhang
     * @create 2016年9月1日上午10:57:46
     * @version 1.0
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/visitorSignIn.do", method = RequestMethod.POST)
    public Object visitorSignIn(HttpServletRequest request, String id, String params) {
        SignVisitorInfoWithBLOBs sviWithBLOBs = JsonUtil.jsonToBean(params, SignVisitorInfoWithBLOBs.class, DateFormatterConstants.SYS_T_FORMAT);
        ServiceResult<Object> result = signService.saveVisitorSignIn(sviWithBLOBs, id);
        return ResponseUtils.sendMsg(result.getCode(), result.getMsg());
    }

    public static void main(String[] args) {
        Integer num = (int) (Math.random() * 20) + 1;
        System.err.println(num.shortValue());
    }

    /**
     * @description 签出
     * @author hxzhang
     * @create 2016年9月1日下午4:19:44
     * @version 1.0
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/visitorSignOut.do", method = RequestMethod.POST)
    public Object visitorSignOut(HttpServletRequest request, String params) {
        SignVisitorInfoWithBLOBs sviWithBLOBs = JsonUtil.jsonToBean(params, SignVisitorInfoWithBLOBs.class, DateFormatterConstants.SYS_T_FORMAT);
        ServiceResult<Object> result = signService.saveVisitorSignOut(sviWithBLOBs);
        return ResponseUtils.sendMsg(result.getCode(), result.getMsg());
    }

    /**
     * @description 获取签到列表
     * @author hxzhang
     * @create 2016年9月1日下午4:40:49
     * @version 1.0
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/visitorSignInList.do", method = RequestMethod.POST)
    public Object visitorSignInList(HttpServletRequest request, String id, String params) {
        ServiceResult<Object> result = signService.getVisitorSignInList(id, params);
        return ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
    }

    /**
     * @description 获取签到信息
     * @author hxzhang
     * @create 2016年9月5日下午5:35:13
     * @version 1.0
     * @param request
     * @param id
     * @return
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/getSignIn.do", method = RequestMethod.POST)
    public Object getSignIn(HttpServletRequest request, String id) {
        ServiceResult<Object> result = signService.getSignInInfo(id);
        return ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
    }

    /**
     * @description 编辑签到信息(CEO,园长,二级园长)
     * @author hxzhang
     * @create 2016年9月6日上午8:19:21
     * @version 1.0
     * @param request
     * @param params
     * @return
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/submitSignIn.do", method = RequestMethod.POST)
    public Object submitSignIn(HttpServletRequest request, String params) {
        SigninInfoVo signinInfoVo = JsonUtil.jsonToBean(params, SigninInfoVo.class, DateFormatterConstants.SYS_T_FORMAT2);
        ServiceResult<Object> result = signService.updateSignIn(signinInfoVo, getCurrentUser(request));
        return ResponseUtils.sendMsg(result.getCode(), result.getMsg());
    }

    /**
     * @description 导出数据
     * @author hxzhang
     * @create 2016年9月6日下午8:20:14
     * @version 1.0
     * @param request
     * @param condition
     * @return
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/export.do", method = RequestMethod.POST)
    public Object export(HttpServletRequest request, String params) {
        SignCondition condition = JsonUtil.jsonToBean(params, SignCondition.class, DateFormatterConstants.SYS_T_FORMAT);
        ServiceResult<Object> result = signService.getExport(condition, getCurrentUser(request));
        String uuid = UUID.randomUUID().toString();
        request.getSession().setAttribute(uuid, result.getReturnObj());
        return ResponseUtils.sendMsg(result.getCode(), (Object) uuid);
    }

    /**
     * @description 下载excel
     * @author hxzhang
     * @create 2016年9月6日下午7:54:06
     * @version 1.0
     * @param request
     * @param response
     * @param uuid
     */
    @SuppressWarnings("unchecked")
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/excel.do", method = RequestMethod.GET)
    public void getExcel(HttpServletRequest request, HttpServletResponse response, String uuid) {
        // 通过session获取数据
        List<Map<String, String>> list = (List<Map<String, String>>) request.getSession().getAttribute(uuid);
        // 清空session
        request.removeAttribute(uuid);
        // 写入Excel
        ExcelUtil e = new ExcelUtil();
        e.exportExcel(response, "VisitorSign", ExportConfigure.VisitorSign, list);
    }

    /**
     * @description 通过园区名称获取园区ID
     * @author hxzhang
     * @create 2016年9月10日上午11:13:28
     * @version 1.0
     * @param request
     * @param name
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getCentreId.do", method = RequestMethod.POST)
    public Object getCentreId(HttpServletRequest request, String name) {
        String ip = RequestUtil.getIpAddr(request);
        log.info("getCentreId|start|ip=" + ip);
        ServiceResult<Object> result = signService.getCentreId(name, ip);
        return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
    }

    /**
     * @description 保存员工签入信息
     * @author hxzhang
     * @create 2016年9月12日上午11:28:10
     * @version 1.0
     * @param request
     * @param pwd
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/staffSignIn.do", method = RequestMethod.POST)
    public Object staffSignin(HttpServletRequest request, String id, String centreId, String psw) {
        ServiceResult<Object> result = signService.saveStaffSigninOrSignout(id, centreId, psw, SigninType.IN.getValue());
        return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), (Object) result.getReturnObj());
    }

    /**
     * @description 保存员工签出信息
     * @author hxzhang
     * @create 2016年9月12日下午1:58:31
     * @version 1.0
     * @param request
     * @param params
     * @param psw
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/staffSignOut.do", method = RequestMethod.POST)
    public Object staffSignout(HttpServletRequest request, String id, String centreId, String psw) {
        ServiceResult<Object> result = signService.saveStaffSigninOrSignout(id, centreId, psw, SigninType.OUT.getValue());
        return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), (Object) result.getReturnObj());

    }

    /**
     * @description 获取当天上班员工
     * @author hxzhang
     * @create 2016年9月12日下午2:02:50
     * @version 1.0
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/staffList.do", method = RequestMethod.POST)
    public Object staffList(HttpServletRequest request, String id, String name, int flag) {
        ServiceResult<Object> result = signService.getSignStaffList(id, name, flag == 2);
        return ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
    }

    /**
     * @description 获取员工的签入签出信息
     * @author hxzhang
     * @create 2016年9月20日上午11:15:21
     * @version 1.0
     * @param request
     * @param id
     * @return
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/staffSignInfo.do", method = RequestMethod.POST)
    public Object getStaffSignInfo(HttpServletRequest request, String id, Date date, String centerId) {
        ServiceResult<Object> result = signService.getStaffSignInfo(id, date, centerId);
        return ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
    }

    /**
     * @description 编辑员工签到信息
     * @author hxzhang
     * @create 2016年9月20日上午11:13:31
     * @version 1.0
     * @param request
     * @param params
     * @return
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/submitStaffSign.do", method = RequestMethod.POST)
    public Object submitStaffSign(HttpServletRequest request, String params, Boolean comfirmSure) {
        StaffSignInfoVo staffSignInfoVo = JsonUtil.jsonToBean(params, StaffSignInfoVo.class, DateFormatterConstants.SYS_T_FORMAT2);
        ServiceResult<Object> result = signService.addOrUpdateStaffSign(staffSignInfoVo, getCurrentUser(request), comfirmSure);
        return ResponseUtils.sendMsg(result.getCode(), result.getMsg());
    }

    /**
     * @description 保存小孩签入信息
     * @author hxzhang
     * @create 2016年9月12日下午3:20:38
     * @version 1.0
     * @param request
     * @param params
     * @param id
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/childSignIn.do", method = RequestMethod.POST)
    public Object childSigin(HttpServletRequest request, String id, String inName, boolean sunscreenApp) {
        ServiceResult<Object> result = signService.saveChildSignin(id, inName, sunscreenApp);
        return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
    }

    /**
     * @description 保存小孩签出信息
     * @author hxzhang
     * @create 2016年9月12日下午3:27:12
     * @version 1.0
     * @param request
     * @param params
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/childSignOut.do", method = RequestMethod.POST)
    public Object childSigout(HttpServletRequest request, String id, String outName) {
        ServiceResult<Object> result = signService.saveChildSignout(id, outName);
        return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
    }

    /**
     * @description 获取小孩的签到信息
     * @author hxzhang
     * @create 2016年9月12日下午3:57:33
     * @version 1.0
     * @param request
     * @param id
     * @return
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/childSignInfo.do", method = RequestMethod.POST)
    public Object getChildSignInfo(HttpServletRequest request, String id, Date date) {
        ServiceResult<Object> result = signService.getChildSignInfo(id, date, getCurrentUser(request));
        return ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
    }

    /**
     * @description 编辑小孩的签到信息
     * @author hxzhang
     * @create 2016年9月12日下午4:33:10
     * @version 1.0
     * @param request
     * @return
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/submitChildSign.do", method = RequestMethod.POST)
    public Object submitChildSign(HttpServletRequest request, String params, boolean flag) {
        SigninInfoVo signinInfoVo = (SigninInfoVo) GsonUtil.jsonToBean(params, SigninInfoVo.class, DateFormatterConstants.SYS_T_FORMAT2,
                DateFormatterConstants.SYS_FORMAT, DateFormatterConstants.SYS_FORMAT2, DateFormatterConstants.SYS_T_FORMAT);
        ServiceResult<Object> result = signService.updateChildSign(signinInfoVo, getCurrentUser(request), flag);
        return ResponseUtils.sendMsg(result.getCode(), result.getMsg());
    }

    /**
     * 
     * @description 获取小孩信息
     * @author gfwang
     * @create 2016年9月22日下午6:51:04
     * @version 1.0
     * @param request
     * @param params
     * @param orgId
     *            组织ID
     * @param type
     *            =1 为tag小孩
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getAllChilds.do", method = RequestMethod.GET)
    public Object getAllChild(HttpServletRequest request, String p, String orgId, Integer type) {
        return signService.getAllChilds(p, orgId, type);
    }

    /**
     * @description 小孩列表导出
     * @author hxzhang
     * @create 2016年9月18日下午2:09:39
     * @version 1.0
     * @param request
     * @param params
     * @return
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/childExport.do", method = RequestMethod.POST)
    public Object childExport(HttpServletRequest request, String params) {
        SignCondition condition = (SignCondition) GsonUtil.jsonToBean(params, SignCondition.class, getDateFormatter());
        ServiceResult<Object> result = signService.getChildExport(condition);
        String uuid = UUID.randomUUID().toString();
        request.getSession().setAttribute(uuid, result.getReturnObj());
        return ResponseUtils.sendMsg(result.getCode(), (Object) uuid);
    }

    /**
     * @description 下载excel
     * @author hxzhang
     * @create 2016年9月19日下午2:30:20
     * @version 1.0
     * @param request
     * @param id
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/childExcel.do", method = RequestMethod.GET)
    @SuppressWarnings("unchecked")
    public void childExcel(HttpServletRequest request, HttpServletResponse response, String uuid) {
        // 通过session获取数据
        List<Map<String, String>> list = (List<Map<String, String>>) request.getSession().getAttribute(uuid);
        // 清空session
        request.removeAttribute(uuid);
        // 写入Excel
        ExcelUtil e = new ExcelUtil();
        e.exportExcel(response, "ChildSign", ExportConfigure.ChildSign, list);
    }

    /**
     * @description 获取导出数据
     * @author hxzhang
     * @create 2016年9月20日下午4:14:36
     * @version 1.0
     * @param request
     * @param params
     * @return
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/staffExport.do", method = RequestMethod.POST)
    public Object staffExport(HttpServletRequest request, String params) {
        SignCondition condition = (SignCondition) GsonUtil.jsonToBean(params, SignCondition.class, DateFormatterConstants.SYS_FORMAT,
                DateFormatterConstants.SYS_FORMAT2, DateFormatterConstants.SYS_T_FORMAT, DateFormatterConstants.SYS_T_FORMAT2);
        ServiceResult<Object> result = signService.getStaffExport(condition);
        String uuid = UUID.randomUUID().toString();
        request.getSession().setAttribute(uuid, result.getReturnObj());
        return ResponseUtils.sendMsg(result.getCode(), (Object) uuid);
    }

    /**
     * @description 将数据写入excel
     * @author hxzhang
     * @create 2016年9月20日下午4:16:56
     * @version 1.0
     * @param request
     * @param response
     * @param uuid
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/staffExcel.do", method = RequestMethod.GET)
    public void staffExcel(HttpServletRequest request, HttpServletResponse response, String uuid) {
        // 通过session获取数据
        @SuppressWarnings("unchecked")
        List<Map<String, String>> list = (List<Map<String, String>>) request.getSession().getAttribute(uuid);
        // 清空session
        request.removeAttribute(uuid);
        // 写入Excel
        ExcelUtil e = new ExcelUtil();
        e.exportExcel(response, "StaffSign", ExportConfigure.StaffSign, list);
    }

    /**
     * @description 跳转签到
     * @author gfwang
     * @create 2016年11月16日上午10:48:50
     * @version 1.0
     * @param name
     * @return
     */
    @RequestMapping(value = "/goSign.do", method = RequestMethod.GET)
    public String sendSign(String name) {
        return "redirect:/index.html#/landing_signin/" + name;
    }

    /**
     * @description 清除未签出标识
     * @author hxzhang
     * @create 2016年12月26日上午10:42:10
     */
    @ResponseBody
    @RequestMapping(value = "/romoveNotHaveSignout.do", method = RequestMethod.POST)
    public Object romoveNotHaveSignout(HttpServletRequest request, String accountId) {
        ServiceResult<Object> result = signService.removeNotHaveSignout(accountId);
        userFactory.updateSession(request);
        return ResponseUtils.sendMsg(result.getCode(), result.getMsg());
    }
}
