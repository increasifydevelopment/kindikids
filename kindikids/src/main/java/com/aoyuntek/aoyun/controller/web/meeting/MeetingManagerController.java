package com.aoyuntek.aoyun.controller.web.meeting;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aoyuntek.aoyun.condtion.MeetingCondition;
import com.aoyuntek.aoyun.constants.DateFormatterConstants;
import com.aoyuntek.aoyun.controller.web.BaseWebController;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.aoyun.entity.vo.meeting.MeetingInfoVo;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.service.meeting.IMeetingAgendaService;
import com.aoyuntek.aoyun.service.meeting.IMeetingService;
import com.aoyuntek.aoyun.service.meeting.IMeetingTempService;
import com.aoyuntek.aoyun.uitl.GsonUtil;
import com.aoyuntek.framework.annotation.UserAuthority;
import com.theone.common.util.ServiceResult;
import com.theone.date.util.DateUtil;
import com.theone.web.util.ResponseUtils;

@Controller
@RequestMapping(value = "/meetManager")
public class MeetingManagerController extends BaseWebController {

    @Autowired
    private IMeetingAgendaService meetingAgendaService;
    @Autowired
    private IMeetingTempService meetingTempService;
    @Autowired
    private IMeetingService meetingService;

    /**
     * 
     * @description 获取agenda列表和temp列表(selecterPo)
     * @author mingwang
     * @create 2016年10月17日下午2:03:59
     * @version 1.0
     * @param request
     * @param condition
     * @return
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/getSelectList.do", method = RequestMethod.POST)
    public Object getSelectList(HttpServletRequest request) {
        List<SelecterPo> meetingAgendaList = meetingAgendaService.getAgendaList();
        List<SelecterPo> meetingTempList = meetingTempService.getMeetingTempForSelecterPo();
        HashMap<String, List<SelecterPo>> hashMap = new HashMap<String, List<SelecterPo>>();
        hashMap.put("meetingAgenda", meetingAgendaList);
        hashMap.put("meetingTemp", meetingTempList);
        return sendSuccessResponse(hashMap);
    }

    /**
     * 
     * @description meeting的创建
     * @author mingwang
     * @create 2016年10月17日下午4:40:46
     * @version 1.0
     * @param request
     * @param params
     * @return
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/createMeeting.do", method = RequestMethod.POST)
    public Object createMeeting(HttpServletRequest request, String params) {
        MeetingInfoVo meetingInfoVo = (MeetingInfoVo) GsonUtil.jsonToBean(params, MeetingInfoVo.class, DateFormatterConstants.SYS_FORMAT,
                DateFormatterConstants.SYS_FORMAT2, DateFormatterConstants.SYS_T_FORMAT, DateFormatterConstants.SYS_T_FORMAT2);
        ServiceResult<Object> result = meetingService.addMeetingInfo(meetingInfoVo, getCurrentUser(request));
        return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
    }

    /**
     * 
     * @description 删除meeting
     * @author mingwang
     * @create 2016年10月19日上午11:16:55
     * @version 1.0
     * @param request
     * @param meetingId
     * @return
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/deleteMeeting.do", method = RequestMethod.POST)
    public Object deleteMeeting(HttpServletRequest request, String meetingId) {
        ServiceResult<Object> result = meetingService.updateDeleteByPrimaryKey(meetingId);
        return ResponseUtils.sendMsg(result.getCode(), result.getMsg());
    }

    /**
     * 
     * @description 驗證名稱不重複
     * @author gfwang
     * @create 2016年10月17日下午4:33:20
     * @version 1.0
     * @param response
     * @param id
     * @param name
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/validateMeetingName.do", method = RequestMethod.POST)
    public void validateMeetingName(HttpServletResponse response, String id, String meetingName) {
        boolean result = meetingService.validateMeetingName(id, meetingName);
        bootStrapValidateRemote(response, result);
    }

    /**
     * @description 获取会议分页列表
     * @author hxzhang
     * @create 2016年10月17日下午5:22:55
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/getMeetingManaList.do", method = RequestMethod.POST)
    public Object getMeetingManaList(HttpServletRequest request, MeetingCondition condition) {
        // 创建符合频率的会议模版,过去时间不生成
        if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(condition.getDay(), new Date()) < 0) {
            meetingTempService.createMeetingTempForMeetingTimeTask(condition.getDay());
        }
        ServiceResult<Object> result = meetingService.getMeetingManaList(condition, getCurrentUser(request));
        return ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
    }

    /**
     * @description 获取会议信息
     * @author hxzhang
     * @create 2016年10月18日上午10:08:44
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/getMeetingManaInfo.do", method = RequestMethod.POST)
    public Object getMeetingManaInfo(HttpServletRequest request, String id, String tempId) {
        ServiceResult<Object> result = meetingService.getMeetingManaInfo(id, tempId, getCurrentUser(request));
        return ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
    }

    /**
     * @description 保存会议信息
     * @author hxzhang
     * @create 2016年10月18日下午4:36:50
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/saveMeetingMana.do", method = RequestMethod.POST)
    public Object saveMeetingMana(HttpServletRequest request, String params, boolean confirm) {
        MeetingInfoVo meetingInfoVo = (MeetingInfoVo) GsonUtil.jsonToBean(params, MeetingInfoVo.class, DateFormatterConstants.SYS_FORMAT,
                DateFormatterConstants.SYS_FORMAT2, DateFormatterConstants.SYS_T_FORMAT, DateFormatterConstants.SYS_T_FORMAT2);
        ServiceResult<Object> result = meetingService.saveMeetingMana(meetingInfoVo, getCurrentUser(request), confirm);
        return ResponseUtils.sendMsg(result.getCode(), result.getMsg());
    }

    /**
     * @description 获取一行记录
     * @author hxzhang
     * @create 2016年10月23日下午2:49:26
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge, Role.Educator, Role.Cook })
    @ResponseBody
    @RequestMapping(value = "/getRowInfo.do")
    public Object getMeetingRowInfo(HttpServletRequest request, String id) {
        ServiceResult<Object> result = meetingService.getRowInfo(id);
        return ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
    }
}
