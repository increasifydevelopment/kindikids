package com.aoyuntek.aoyun.controller.web;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import sun.misc.BASE64Decoder;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.aoyuntek.aoyun.dao.AttachmentInfoMapper;
import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.entity.po.AttachmentInfo;
import com.aoyuntek.aoyun.entity.po.CentersInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.uitl.ListSplit;
import com.aoyuntek.framework.constants.PropertieNameConts;
import com.theone.aws.util.FileFactory;
import com.theone.string.util.StringUtil;
import com.theone.web.util.RequestUtil;

@Controller
@RequestMapping(value = "/test")
public class TestController extends BaseWebController {
    @Autowired
    private AttachmentInfoMapper attachmentInfoMapper;
    @Autowired
    private UserInfoMapper userInfoMapper;
    @Autowired
    private CentersInfoMapper centersInfoMapper;

    private final String tempId = "ce5fe702-91de-4edd-bb95-2381a576a1c9";
    private final int SPLIT_COUNT = 50;
    private final String KEY = "d073b34f-d19b-11e6-87ba-00e0703507b4";

    /*
     * @ResponseBody
     * 
     * @RequestMapping(value = "/file.do") public Object testFile(String key)
     * throws Exception { if (!KEY.equals(key)) { return "fail"; } // 获取导入的文件
     * List<AttachmentInfo> attachInfos =
     * attachmentInfoMapper.selectAll(tempId); ListSplit<AttachmentInfo>
     * listSplit = new ListSplit<AttachmentInfo>(); // 切割
     * List<List<AttachmentInfo>> attachInfosList =
     * listSplit.SplitList(attachInfos, SPLIT_COUNT); FileFactory factory = new
     * FileFactory
     * (Region.getRegion(Regions.AP_SOUTHEAST_2),PropertieNameConts.S3); for
     * (List<AttachmentInfo> list : attachInfosList) { updateFile(list,
     * factory); }
     * 
     * return "success"; }
     * 
     * public void updateFile(final List<AttachmentInfo> list, final FileFactory
     * factory) throws IOException { new Thread(new Runnable() { public void
     * run() { for (AttachmentInfo att : list) { // 获取后缀 String suffix =
     * getSuffix(att.getAttachName()); // 拼接文件s3路径 String fileRelativeName =
     * initS3Path(att.getKeyName(), suffix); try { String visitUrl =
     * att.getVisitUrl(); if ("/".equals(visitUrl.substring(0, 1))) { visitUrl =
     * visitUrl.substring(1); } // String httpUrl = BASE_URL + TEST_FILE_PATH;
     * // 获取http文件流 InputStream ins = getStream(visitUrl); if (null != ins) { //
     * 上传文件 String attId = factory.uploadFile(fileRelativeName, ins); // 获取访问路径
     * String visiUrl = factory.getS3RouteUrl(fileRelativeName, 1);
     * att.setAttachId(attId); att.setVisitUrl(visiUrl);
     * attachmentInfoMapper.updateByPrimaryKey(att); } } catch (Exception e) {
     * System.err.println(att.getId()); e.printStackTrace(); } } } }).start(); }
     *//**
     * 
     * @description 获取访问路径
     * @author gfwang
     * @create 2017年1月4日上午8:42:06
     * @version 1.0
     * @param keyName
     * @param suffix
     * @return
     */
    /*
     * private String initS3Path(String keyName, String suffix) { // return
     * TEST_MAIN_PATH + keyName + UUID.randomUUID().toString() + // suffix;
     * return keyName + UUID.randomUUID().toString() + suffix; }
     *//**
     * 
     * @description 获取后缀
     * @author gfwang
     * @create 2017年1月4日上午8:41:48
     * @version 1.0
     * @param name
     * @return
     */
    /*
     * private String getSuffix(String name) { int pointIndex =
     * name.lastIndexOf("."); String a = ""; try { a =
     * name.substring(pointIndex, name.length()); } catch (Exception e) {
     * e.printStackTrace(); } return a; }
     *//**
     * 
     * @description 获取文件流
     * @author gfwang
     * @create 2016年12月27日上午8:45:56
     * @version 1.0
     * @param urlStr
     * @return
     * @throws IOException
     */
    /*
     * private InputStream getStream(String urlStr) { InputStream inputStream =
     * null; try { if ("true".equals(getSystemMsg("old_system_flag"))) {
     * 
     * String httpUrl = getSystemMsg("old_system_url") + urlStr; httpUrl =
     * httpUrl.replace(" ", "%20"); URL url = new URL(httpUrl);
     * HttpURLConnection conn = (HttpURLConnection) url.openConnection(); //
     * 设置超时间为3秒 conn.setConnectTimeout(3 * 1000); // 防止屏蔽程序抓取而返回403错误
     * conn.setRequestProperty("User-Agent",
     * "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)"); // 得到输入流
     * inputStream = conn.getInputStream();
     * 
     * } else { File f = new File(getSystemMsg("local_url") + urlStr); if
     * (f.exists()) { inputStream = new FileInputStream(f); } } } catch
     * (Exception e) { e.printStackTrace(); } return inputStream; }
     * 
     * // 上传头像
     * 
     * @ResponseBody
     * 
     * @RequestMapping(value = "/updateAvtar.do") public Object
     * testUpdateAvtar(String key) throws Exception { if (!KEY.equals(key)) {
     * return "fail"; } // TODO List<UserInfo> user =
     * userInfoMapper.selectAll(tempId, "base64"); // List<UserInfo> user =
     * userInfoMapper.selectTest();
     * 
     * ListSplit<UserInfo> listSplit = new ListSplit<UserInfo>(); FileFactory
     * factory = new
     * FileFactory(Region.getRegion(Regions.AP_SOUTHEAST_2),PropertieNameConts
     * .S3); List<List<UserInfo>> userList = listSplit.SplitList(user,
     * SPLIT_COUNT); for (List<UserInfo> list : userList) { upavtar(list,
     * factory);
     * 
     * } return "success"; }
     * 
     * // 线程上传有户类型为staff和child头像 private void upavtar(final List<UserInfo>
     * userList, final FileFactory factory) { new Thread(new Runnable() { public
     * void run() { // 得到头像被base64编码或 "ce5fe702-91de-4edd-bb95-2381a576a1c9"的用户
     * boolean flag = false; String relativeName = null; String httpUrl = null;
     * String suffix = null; String userId = null; for (UserInfo userInfo :
     * userList) { userId = userInfo.getId(); if
     * (userInfo.getAvatar().contains("base64")) { suffix =
     * getPhotoSffix(userInfo.getAvatar()); httpUrl =
     * getStr(userInfo.getAvatar()); flag = true; } else { suffix =
     * getSuffix(userInfo.getAvatar()); String[] arr =
     * userInfo.getAvatar().split("aoyun/"); httpUrl = arr[1]; } relativeName =
     * initS3Path("", suffix); InputStream avtarStream = null; try { if (flag) {
     * avtarStream = GenerateImage(httpUrl); } else { avtarStream =
     * getStream(httpUrl); } if (null != avtarStream) { // 上传文件到指定位置
     * factory.uploadFile(relativeName, avtarStream);
     * userInfoMapper.updateAvatar(relativeName, userId); } } catch (Exception
     * e) { System.err.println(userId); e.printStackTrace(); }
     * 
     * } }
     * 
     * }
     * 
     * ).start(); }
     *//**
     * 
     * @description 字符串转输入流
     * @author gfwang
     * @create 2016年7月13日下午9:43:31
     * @version 1.0
     * @param fileStr
     *            文件编码的字符串
     * @return InputStream
     */
    /*
     * public InputStream GenerateImage(String fileStr) { //
     * 对字节数组字符串进行Base64解码并生成图片 BASE64Decoder decoder = new BASE64Decoder();
     * InputStream ins = null; try { // Base64解码 byte[] b =
     * decoder.decodeBuffer(fileStr); for (int i = 0; i < b.length; ++i) { if
     * (b[i] < 0) {// 调整异常数据 b[i] += 256; } } // 生成jpeg图片 ins = new
     * ByteArrayInputStream(b); } catch (Exception e) { e.printStackTrace(); }
     * return ins; }
     * 
     * private String getPhotoSffix(String str) { int begin = str.indexOf('/');
     * int end = str.indexOf(';');
     * 
     * return str.substring(begin + 1, end); }
     * 
     * private String getStr(String str) { int begin = str.indexOf(','); return
     * str.substring(begin + 1); }
     */

    /**
     * 
     * @description 追加IP
     * @author mingwang
     * @create 2017年2月10日下午1:35:06
     * @version 1.0
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/appendIp.do")
    public Object appendIp(HttpServletRequest request, String centreName, String ip) {
        // String ip = RequestUtil.getIpAddr(request);
        log.info("ip = " + ip);
        CentersInfo centersInfo = centersInfoMapper.getCentreInfoByCentreName(centreName);
        if (null == centersInfo) {
            return "The centre is not exist";
        }
        String centerIp = centersInfo.getCenterIp();
        if (StringUtil.isEmpty(centerIp)) {
            centerIp = ip;
        } else {
            centerIp = centerIp + "," + ip;
        }
        centersInfoMapper.updateCentreIp(centersInfo.getId(), centerIp);
        return "success: " + centerIp;
    }
}
