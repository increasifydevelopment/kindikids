package com.aoyuntek.aoyun.controller.web.roster;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aoyuntek.aoyun.constants.DateFormatterConstants;
import com.aoyuntek.aoyun.controller.web.BaseWebController;
import com.aoyuntek.aoyun.entity.po.task.TaskInfo;
import com.aoyuntek.aoyun.entity.vo.roster.RosterShiftTimeVo;
import com.aoyuntek.aoyun.entity.vo.roster.RosterShiftVo;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.service.roster.IRosterShiftService;
import com.aoyuntek.aoyun.uitl.GsonUtil;
import com.aoyuntek.framework.annotation.UserAuthority;
import com.google.gson.reflect.TypeToken;
import com.theone.common.util.ServiceResult;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;
import com.theone.web.util.ResponseUtils;

@Controller
@RequestMapping("/shift")
public class ShiftController extends BaseWebController {

    @Autowired
    private IRosterShiftService rosterShiftService;

    @UserAuthority(roles = { Role.CEO, Role.CentreManager })
    @ResponseBody
    @RequestMapping(value = "/saveShift.do", method = RequestMethod.POST)
    public Object saveShift(HttpServletRequest request, String json) throws Exception {
        // 判断
        if (StringUtil.isEmpty(json)) {
            return ResponseUtils.sendMsg(-1, getTipMsg("illegal.request"));
        }
        Type typeToken = new TypeToken<RosterShiftVo>() {
        }.getType();
        RosterShiftVo vo = (RosterShiftVo) GsonUtil.jsonToBean(json, typeToken, DateFormatterConstants.SYS_T_FORMAT,
                DateFormatterConstants.SYS_FORMAT2);

        // 验证
        if (ListUtil.isEmpty(vo.getRosterShiftTimeVos())) {
            return ResponseUtils.sendMsg(0);
        }

        for (RosterShiftTimeVo rosterShiftTimeVo : vo.getRosterShiftTimeVos()) {
            // 时间不能为空
            if (rosterShiftTimeVo.getShiftStartTime() == null || rosterShiftTimeVo.getShiftEndTime() == null) {
                return ResponseUtils.sendMsg(-1, getTipMsg("task.RosterShift.time.startend.null"));
            }
            
            //前者大于后者
            if (rosterShiftTimeVo.getShiftStartTime().compareTo(rosterShiftTimeVo.getShiftEndTime())>=0) {
                return ResponseUtils.sendMsg(-1, getTipMsg("task.RosterShift.time.startend.compare"));
            }
        }

        // 时间段有交集 不允许出现多个相同task
        if (haveOther(vo.getRosterShiftTimeVos())) {
            return ResponseUtils.sendMsg(-1, getTipMsg("task.RosterShift.time.startend.joib.task"));
        }

        ServiceResult<RosterShiftVo> result = rosterShiftService.saveOrUpdateShift(vo, getCurrentUser(request));
        return ResponseUtils.sendMsg((int) result.getCode(), result.getMsg(), result.getReturnObj());
    }

    /**
     * 
     * @author dlli5 at 2016年10月17日上午9:35:08
     * @param rosterShiftTimeVos
     * @return
     */
    private boolean haveOther(List<RosterShiftTimeVo> rosterShiftTimeVos) {
        Map<String, List<RosterShiftTimeVo>> map = new HashMap<String, List<RosterShiftTimeVo>>();
        for (RosterShiftTimeVo rosterShiftTimeVo : rosterShiftTimeVos) {
            for (TaskInfo taskInfo : rosterShiftTimeVo.getRosterShiftTaskVos()) {
                if (map.containsKey(taskInfo.getId())) {
                    map.get(taskInfo.getId()).add(rosterShiftTimeVo);
                } else {
                    List<RosterShiftTimeVo> array = new ArrayList<RosterShiftTimeVo>();
                    array.add(rosterShiftTimeVo);
                    map.put(taskInfo.getId(), array);
                }
            }
        }
        for (String taskId : map.keySet()) {
            if (map.get(taskId).size() == 0) {
                continue;
            }
            List<RosterShiftTimeVo> list = map.get(taskId);
            for (int i = 0; i < list.size() - 1; i++) {
                if (com.aoyuntek.aoyun.uitl.DateUtil.isOverlap(list.get(i).getShiftStartTime(), list.get(i).getShiftEndTime(), list.get(i + 1)
                        .getShiftStartTime(), list.get(i + 1).getShiftEndTime())) {
                    return true;
                }
            }
        }
        return false;
    }

    @UserAuthority(roles = { Role.CEO, Role.CentreManager })
    @ResponseBody
    @RequestMapping(value = "/deleteShiftTime.do", method = RequestMethod.POST)
    public Object deleteShiftTime(HttpServletRequest request, String id) throws Exception {
        // 判断
        if (StringUtil.isEmpty(id)) {
            return ResponseUtils.sendMsg(-1, getTipMsg("illegal.request"));
        }
        ServiceResult<RosterShiftVo> result = rosterShiftService.deleteShiftTime(id, getCurrentUser(request));
        return ResponseUtils.sendMsg((int) result.getCode(), result.getMsg(), result.getReturnObj());
    }

    @UserAuthority(roles = { Role.CEO, Role.CentreManager })
    @ResponseBody
    @RequestMapping(value = "/getShiftVo.do", method = RequestMethod.POST)
    public Object getShift(HttpServletRequest request, String centreId) throws Exception {
        ServiceResult<RosterShiftVo> result = rosterShiftService.getShiftVo(centreId);
        return ResponseUtils.sendMsg((int) result.getCode(), result.getMsg(), result.getReturnObj());
    }

    @UserAuthority(roles = { Role.CEO, Role.CentreManager })
    @ResponseBody
    @RequestMapping(value = "/getShiftTasks.do", method = RequestMethod.GET)
    public Object getShiftTasks(HttpServletRequest request, String centreId) throws Exception {
        ServiceResult<Object> result = rosterShiftService.getShiftTasks(centreId, getCurrentUser(request));
        return ResponseUtils.sendMsg(true, result.getReturnObj());
    }

}
