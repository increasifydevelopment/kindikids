package com.aoyuntek.aoyun.controller.web;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aoyuntek.aoyun.condtion.StaffCondition;
import com.aoyuntek.aoyun.constants.DateFormatterConstants;
import com.aoyuntek.aoyun.entity.po.program.ProgramRegisterItemInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramWeeklyEvalutionInfo;
import com.aoyuntek.aoyun.entity.vo.FollowUpVo;
import com.aoyuntek.aoyun.entity.vo.ProgramIntobsleaInfoVo;
import com.aoyuntek.aoyun.entity.vo.ProgramLessonInfoVo;
import com.aoyuntek.aoyun.entity.vo.ProgramMedicationInfoVo;
import com.aoyuntek.aoyun.entity.vo.ProgramNewsFeedVo;
import com.aoyuntek.aoyun.entity.vo.ProgramRegisterTaskInfoVo;
import com.aoyuntek.aoyun.entity.vo.ProgramTaskExgrscInfoVo;
import com.aoyuntek.aoyun.enums.ExgrscType;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.enums.TaskType;
import com.aoyuntek.aoyun.factory.RosterFactory;
import com.aoyuntek.aoyun.service.ICenterService;
import com.aoyuntek.aoyun.service.IStaffService;
import com.aoyuntek.aoyun.service.program.IProgramIntobsleaService;
import com.aoyuntek.aoyun.service.program.IProgramLessonService;
import com.aoyuntek.aoyun.service.program.IProgramMedicationService;
import com.aoyuntek.aoyun.service.program.IProgramRegisterTaskService;
import com.aoyuntek.aoyun.service.program.IProgramTaskExgrscService;
import com.aoyuntek.aoyun.service.program.ITaskProgramFollowUpService;
import com.aoyuntek.aoyun.service.program.IWeeklyEvaluationService;
import com.aoyuntek.aoyun.uitl.GsonUtil;
import com.aoyuntek.framework.annotation.UserAuthority;
import com.theone.common.util.ServiceResult;
import com.theone.json.util.JsonUtil;
import com.theone.string.util.StringUtil;
import com.theone.web.util.ResponseUtils;

/**
 * 
 * @description room 中task
 * @author gfwang
 * @create 2016年9月20日上午9:14:28
 * @version 1.0
 */
@Controller
@RequestMapping(value = "/program")
public class ProgramController extends BaseWebController {
	/**
	 * log
	 */
	private static Logger log = Logger.getLogger(ProgramController.class);
	@Autowired
	private IProgramLessonService lessonService;
	@Autowired
	private IProgramTaskExgrscService programTaskExgrscService;
	@Autowired
	private IProgramIntobsleaService intobsleaService;
	@Autowired
	private ITaskProgramFollowUpService taskProgramFollowUpService;
	@Autowired
	private IProgramRegisterTaskService registerTaskService;
	@Autowired
	private IWeeklyEvaluationService weeklyEvaluationService;
	@Autowired
	private ICenterService centerService;
	@Autowired
	private IProgramMedicationService programMedicationServiceImpl;
	@Autowired
	private IStaffService staffService;
	@Autowired
	private RosterFactory rosterFactory;

	/**
	 * 
	 * @description 新增/修改program中Lesson
	 * @author mingwang
	 * @create 2016年9月20日上午10:29:31
	 * @version 1.0
	 * @param request
	 * @param programLessonInfo
	 * @return
	 */
	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Cook })
	@ResponseBody
	@RequestMapping(value = "/addProgramLesson.do", method = RequestMethod.POST)
	public Object addProgramLesson(HttpServletRequest request, String params, Date date) {
		log.info("addProgramLesson|start");
		ProgramLessonInfoVo lessonInfoVo = (ProgramLessonInfoVo) GsonUtil.jsonToBean(params, ProgramLessonInfoVo.class,
				DateFormatterConstants.SYS_FORMAT, DateFormatterConstants.SYS_FORMAT2, DateFormatterConstants.SYS_T_FORMAT,
				DateFormatterConstants.SYS_T_FORMAT2);
		// 未排班的兼职及无历史排班的兼职不能进行下步操作
		if (isCasualAndNotRoster(getCurrentUser(request)) && !rosterFactory.havaRoster(getCurrentUser(request))) {
			return sendFailResponse(getTipMsg("no.permissions"));
		}
		ServiceResult<Object> result = lessonService.addOrUpdateLesson(lessonInfoVo, getCurrentUser(request), date);
		log.info("addProgramLesson|end");
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	/**
	 * 
	 * @description 删除program中lesson
	 * @author mingwang
	 * @create 2016年9月20日下午2:19:37
	 * @version 1.0
	 * @param request
	 * @param lessonId
	 * @return
	 */
	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Cook })
	@ResponseBody
	@RequestMapping(value = "/deleteProgramLesson.do", method = RequestMethod.POST)
	public Object deleteProgramLesson(HttpServletRequest request, String lessonId) {
		log.info("deleteProgramLesson|start");
		// 未排班的兼职及无历史排班的兼职不能进行下步操作
		if (isCasualAndNotRoster(getCurrentUser(request)) && !rosterFactory.havaRoster(getCurrentUser(request))) {
			return sendFailResponse(getTipMsg("no.permissions"));
		}
		ServiceResult<Object> result = lessonService.removeLesson(lessonId);
		log.info("deleteProgramLesson|end");
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg());
	}

	/**
	 * 
	 * @description 获取programLessonInfo信息
	 * @author mingwang
	 * @create 2016年9月21日下午5:14:34
	 * @version 1.0
	 * @param request
	 * @param lessonId
	 * @return
	 */
	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Cook })
	@ResponseBody
	@RequestMapping(value = "/getProgramLessonInfo.do", method = RequestMethod.POST)
	public Object getProgramLessonInfo(HttpServletRequest request, String lessonId) {
		log.info("getProgramLessonInfo|start");
		ServiceResult<Object> result = lessonService.getLessonInfo(lessonId);
		log.info("getProgramLessonInfo|end");
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	/**
	 * 
	 * @description share to newsfeed (lesson)
	 * @author mingwang
	 * @create 2016年9月21日下午9:22:23
	 * @version 1.0
	 * @param request
	 * @param lessonId
	 * @return
	 */
	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Cook })
	@ResponseBody
	@RequestMapping(value = "/shareNewsfeed.do", method = RequestMethod.POST)
	public Object shareNewsfeed(HttpServletRequest request, String objId, String params) {
		log.info("lessonNewsfeed|start");
		ProgramNewsFeedVo programNewsFeedVo = (ProgramNewsFeedVo) GsonUtil.jsonToBean(params, ProgramNewsFeedVo.class,
				DateFormatterConstants.SYS_FORMAT, DateFormatterConstants.SYS_FORMAT2, DateFormatterConstants.SYS_T_FORMAT,
				DateFormatterConstants.SYS_T_FORMAT2);
		if (StringUtil.isEmpty(objId)) {
			return sendFailResponse(getTipMsg("program.shareNews.mustSave"));
		}
		if (programNewsFeedVo.getTaskType() == TaskType.SunscreenRegister.getValue()) {
			return sendFailResponse(getTipMsg("no.permissions"));
		}
		// 未排班的兼职不能进行下步操作
		if (isCasualAndNotRoster(getCurrentUser(request)) && !rosterFactory.havaRoster(getCurrentUser(request))) {
			return sendFailResponse(getTipMsg("no.permissions"));
		}
		programNewsFeedVo.setCurrtenUser(getCurrentUser(request));
		ServiceResult<Object> result = lessonService.saveShareNewsfeed(objId, programNewsFeedVo, null);
		log.info("lessonNewsfeed|end");
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	/**
	 * 
	 * @description 新增/更新program中Intobslea(interest,observasion,learningStory)
	 * @author mingwang
	 * @create 2016年9月21日上午10:41:42
	 * @version 1.0
	 * @param request
	 * @return
	 */
	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Cook })
	@ResponseBody
	@RequestMapping(value = "/addProgramIntobslea.do", method = RequestMethod.POST)
	public Object addProgramIntobslea(HttpServletRequest request, String params, Date date, Boolean confirm) {
		log.info("addProgramIntobslea|start");
		ProgramIntobsleaInfoVo intobsleaInfoVo = (ProgramIntobsleaInfoVo) GsonUtil.jsonToBean(params, ProgramIntobsleaInfoVo.class,
				DateFormatterConstants.SYS_FORMAT, DateFormatterConstants.SYS_FORMAT2, DateFormatterConstants.SYS_T_FORMAT,
				DateFormatterConstants.SYS_T_FORMAT2);
		// 未排班的兼职及无历史排班的兼职不能进行下步操作
		if (isCasualAndNotRoster(getCurrentUser(request)) && !rosterFactory.havaRoster(getCurrentUser(request))) {
			return sendFailResponse(getTipMsg("no.permissions"));
		}
		ServiceResult<Object> result = intobsleaService.addOrUpdateintobslea(intobsleaInfoVo, getCurrentUser(request), date, confirm);
		log.info("addProgramIntobslea|end");
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	/**
	 * 
	 * @description 删除program中Intobslea
	 * @author mingwang
	 * @create 2016年9月21日下午1:57:48
	 * @version 1.0
	 * @param request
	 * @param lessonId
	 * @return
	 */
	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Cook })
	@ResponseBody
	@RequestMapping(value = "/deleteProgramIntobslea.do", method = RequestMethod.POST)
	public Object deleteProgramIntobslea(HttpServletRequest request, String intobsleaId) {
		log.info("deleteProgramIntobslea|start");
		// 未排班的兼职及无历史排班的兼职不能进行下步操作
		if (isCasualAndNotRoster(getCurrentUser(request)) && !rosterFactory.havaRoster(getCurrentUser(request))) {
			return sendFailResponse(getTipMsg("no.permissions"));
		}
		ServiceResult<Object> result = intobsleaService.removeIntobslea(intobsleaId);
		log.info("deleteProgramIntobslea|end");
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg());
	}

	/**
	 * 
	 * @description task是否有follow
	 * @author mingwang
	 * @create 2017年1月16日上午10:29:18
	 * @param request
	 * @param intobsleaId
	 * @return
	 */
	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Cook })
	@ResponseBody
	@RequestMapping(value = "/hasFollowUp.do", method = RequestMethod.POST)
	public Object hasFollowUp(HttpServletRequest request, String intobsleaId) {
		log.info("hasFollowUp|start");
		ServiceResult<Object> result = intobsleaService.hasFollowUp(intobsleaId);
		log.info("hasFollowUp|end");
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getReturnObj());
	}

	/**
	 * 
	 * @description 获取ProgramIntobsleaInfo信息
	 * @author mingwang
	 * @create 2016年9月22日上午9:56:35
	 * @version 1.0
	 * @param request
	 * @param lessonId
	 * @return
	 */
	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Cook })
	@ResponseBody
	@RequestMapping(value = "/getProgramIntobsleaInfo.do", method = RequestMethod.POST)
	public Object getProgramIntobsleaInfo(HttpServletRequest request, String intobsleaId) {
		log.info("getProgramIntobsleaInfo|start");
		ServiceResult<Object> result = intobsleaService.getIntobsleaInfo(intobsleaId);
		log.info("getProgramIntobsleaInfo|end");
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	/**
	 * 
	 * @description 新增/更新ProgramRegisterTask
	 * @author mingwang
	 * @create 2016年9月22日下午7:36:19
	 * @version 1.0
	 * @param request
	 * @return
	 */
	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Cook })
	@ResponseBody
	@RequestMapping(value = "/addProgramRegister.do", method = RequestMethod.POST)
	public Object addProgramRegister(HttpServletRequest request, String params) {
		log.info("addProgramRegister|start");
		ProgramRegisterTaskInfoVo registerTaskInfoVo = (ProgramRegisterTaskInfoVo) GsonUtil.jsonToBean(params,
				ProgramRegisterTaskInfoVo.class, DateFormatterConstants.SYS_FORMAT, DateFormatterConstants.SYS_FORMAT2,
				DateFormatterConstants.SYS_T_FORMAT, DateFormatterConstants.SYS_T_FORMAT2);
		ServiceResult<Object> result = registerTaskService.addOrUpdateRegister(registerTaskInfoVo, getCurrentUser(request));
		log.info("addProgramRegister|end");
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	/**
	 * 
	 * @description Nappy/Toilet选中时更新时间
	 * @author mingwang
	 * @create 2016年12月28日上午11:13:29
	 * @param request
	 * @param params
	 * @return
	 */
	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Cook })
	@ResponseBody
	@RequestMapping(value = "/updateNappyToiletTime.do", method = RequestMethod.POST)
	public Object updateNappyToiletTime(HttpServletRequest request, String params) {
		ProgramRegisterItemInfo itemInfo = (ProgramRegisterItemInfo) GsonUtil.jsonToBean(params, ProgramRegisterItemInfo.class,
				DateFormatterConstants.SYS_FORMAT, DateFormatterConstants.SYS_FORMAT2, DateFormatterConstants.SYS_T_FORMAT,
				DateFormatterConstants.SYS_T_FORMAT2);
		ServiceResult<Object> result = registerTaskService.updateRegisterItemTime(itemInfo);
		return ResponseUtils.sendMsg(result.getCode());
	}

	/**
	 * 
	 * @description 获取ProgramRegisterTaskInfo
	 * @author mingwang
	 * @create 2016年9月23日上午9:12:29
	 * @version 1.0
	 * @param request
	 * @param id
	 * @param registerId
	 * @return
	 */
	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Cook })
	@ResponseBody
	@RequestMapping(value = "/getProgramRegisterTask.do", method = RequestMethod.POST)
	public Object getProgramRegisterTask(HttpServletRequest request, String id, String roomId) {
		log.info("getProgramRegisterTask|start");
		ServiceResult<Object> result = registerTaskService.getRegisterTaskInfo(id);
		log.info("getProgramRegisterTask|end");
		return ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
	}

	/**
	 * 
	 * @description 获取当前room签到的所有小孩
	 * @author mingwang
	 * @create 2016年9月26日下午2:36:36
	 * @version 1.0
	 * @param request
	 * @param id
	 * @param roomId
	 * @param date
	 *            日期
	 * @return
	 */
	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Cook })
	@ResponseBody
	@RequestMapping(value = "/getRegisterChilds.do", method = RequestMethod.GET)
	public Object getRegisterChilds(HttpServletRequest request, String roomId, Date date, String p, Short type) {
		log.info("getRegisterChilds|start");
		// 喂药页面获取的是room所在cetre的所有签到小孩
		if (type != null && type == TaskType.AdministrationOfMedication.getValue()) {
			ServiceResult<Object> result = registerTaskService.getCentreSignChilds(roomId, date, p, type);
			return result.getReturnObj();
		}
		ServiceResult<Object> result = registerTaskService.getSignChilds(roomId, date, p, type);
		log.info("getRegisterChilds|end");
		return result.getReturnObj();
	}

	/**
	 * @description 获取孩子集合
	 * @author hxzhang
	 * @create 2017年3月14日上午9:19:58
	 */
	@ResponseBody
	@RequestMapping(value = "/childList.do", method = RequestMethod.GET)
	public Object getChildList(HttpServletRequest request, String roomId, String p) {
		ServiceResult<Object> result = registerTaskService.getChildsByRoom(roomId, p);
		return result.getReturnObj();
	}

	/**
	 * 
	 * @description 新增更新小孩MedicationInfo
	 * @author mingwang
	 * @create 2016年9月26日下午3:46:36
	 * @version 1.0
	 * @param request
	 * @param params
	 * @return
	 */
	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Cook })
	@ResponseBody
	@RequestMapping(value = "/addProgramMedication.do", method = RequestMethod.POST)
	public Object addProgramMedication(HttpServletRequest request, String params, Date date) {
		log.info("addProgramMedication|start");
		ProgramMedicationInfoVo medicationInfoVo = (ProgramMedicationInfoVo) GsonUtil.jsonToBean(params, ProgramMedicationInfoVo.class,
				DateFormatterConstants.SYS_T_FORMAT, DateFormatterConstants.SYS_FORMAT2, DateFormatterConstants.SYS_FORMAT,
				DateFormatterConstants.SYS_T_FORMAT2);
		ServiceResult<Object> result = programMedicationServiceImpl.addOrUpdateMedication(medicationInfoVo, getCurrentUser(request), date);
		log.info("addProgramMedication|end");
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	/**
	 * 
	 * @description 获取ProgramMedication(喂药信息)
	 * @author mingwang
	 * @create 2016年9月26日下午7:57:47
	 * @version 1.0
	 * @param request
	 * @param id
	 * @return
	 */
	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Cook })
	@ResponseBody
	@RequestMapping(value = "/getProgramMedication.do", method = RequestMethod.POST)
	public Object getProgramMedication(HttpServletRequest request, String id) {
		log.info("getProgramMedication|start");
		ServiceResult<Object> result = programMedicationServiceImpl.getMedicationInfo(id);
		ProgramMedicationInfoVo item = (ProgramMedicationInfoVo) result.getReturnObj();
		log.info("getProgramMedication|end");
		return ResponseUtils.sendMsg(result.getCode(), item);
	}

	/**
	 * @description 新增或更新Explore Curriculum
	 * @author hxzhang
	 * @create 2016年9月20日下午9:42:51
	 * @version 1.0
	 * @param request
	 * @param params
	 * @return
	 */
	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Cook })
	@ResponseBody
	@RequestMapping(value = "/saveProgramTaskExplore.do", method = RequestMethod.POST)
	public Object programTaskExplore(HttpServletRequest request, String params) {
		ProgramTaskExgrscInfoVo exgrscInfoVo = JsonUtil.jsonToBean(params, ProgramTaskExgrscInfoVo.class,
				DateFormatterConstants.SYS_T_FORMAT2);
		// 未排班的兼职及无历史排班的兼职不能进行下步操作
		if (isCasualAndNotRoster(getCurrentUser(request)) && !rosterFactory.havaRoster(getCurrentUser(request))) {
			return sendFailResponse(getTipMsg("no.permissions"));
		}
		ServiceResult<Object> result = programTaskExgrscService.addOrUpdatePorgramTaskExgrsc(exgrscInfoVo, getCurrentUser(request),
				ExgrscType.ExploreCurriculum.getValue());
		return ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
	}

	/**
	 * @description 获取当天的Explore Curriculum信息
	 * @author hxzhang
	 * @create 2016年9月21日上午11:37:34
	 * @version 1.0
	 * @param request
	 * @param id
	 * @param roomId
	 * @return
	 */
	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Cook })
	@ResponseBody
	@RequestMapping(value = "/getProgramTaskExplore.do", method = RequestMethod.POST)
	public Object getProgramTaskExplore(HttpServletRequest request, String id, String roomId) {
		ServiceResult<Object> result = programTaskExgrscService.getProgramTaskExgrsc(id, roomId, TaskType.ExploreCurriculum.getValue());
		return ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
	}

	/**
	 * @description 新增或更新School Readiness
	 * @author hxzhang
	 * @create 2016年9月20日下午9:42:51
	 * @version 1.0
	 * @param request
	 * @param params
	 * @return
	 */
	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Cook })
	@ResponseBody
	@RequestMapping(value = "/saveProgramTaskSchRea.do", method = RequestMethod.POST)
	public Object programTaskSchRea(HttpServletRequest request, String params) {
		ProgramTaskExgrscInfoVo exgrscInfoVo = JsonUtil.jsonToBean(params, ProgramTaskExgrscInfoVo.class,
				DateFormatterConstants.SYS_T_FORMAT2);
		// 未排班的兼职及无历史排班的兼职不能进行下步操作
		if (isCasualAndNotRoster(getCurrentUser(request)) && !rosterFactory.havaRoster(getCurrentUser(request))) {
			return sendFailResponse(getTipMsg("no.permissions"));
		}
		ServiceResult<Object> result = programTaskExgrscService.addOrUpdatePorgramTaskExgrsc(exgrscInfoVo, getCurrentUser(request),
				ExgrscType.SchoolReadiness.getValue());
		return ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
	}

	/**
	 * @description 获取当天的School Readiness信息
	 * @author hxzhang
	 * @create 2016年9月21日下午2:03:03
	 * @version 1.0
	 * @param request
	 * @param id
	 * @param roomId
	 * @return
	 */
	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Cook })
	@ResponseBody
	@RequestMapping(value = "/getProgramTaskSchRea.do", method = RequestMethod.POST)
	public Object getProgramTaskSchRea(HttpServletRequest request, String id, String roomId) {
		ServiceResult<Object> result = programTaskExgrscService.getProgramTaskExgrsc(id, roomId, TaskType.SchoolReadiness.getValue());
		return ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
	}

	/**
	 * @description 新增或更新Grow Curriculum
	 * @author hxzhang
	 * @create 2016年9月20日下午9:42:51
	 * @version 1.0
	 * @param request
	 * @param params
	 * @return
	 */
	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Cook })
	@ResponseBody
	@RequestMapping(value = "/saveProgramTaskGrow.do", method = RequestMethod.POST)
	public Object programTaskGrow(HttpServletRequest request, String params) {
		ProgramTaskExgrscInfoVo exgrscInfoVo = JsonUtil.jsonToBean(params, ProgramTaskExgrscInfoVo.class,
				DateFormatterConstants.SYS_T_FORMAT2);
		// 未排班的兼职及无历史排班的兼职不能进行下步操作
		if (isCasualAndNotRoster(getCurrentUser(request)) && !rosterFactory.havaRoster(getCurrentUser(request))) {
			return sendFailResponse(getTipMsg("no.permissions"));
		}
		ServiceResult<Object> result = programTaskExgrscService.addOrUpdatePorgramTaskExgrsc(exgrscInfoVo, getCurrentUser(request),
				ExgrscType.GrowCurriculum.getValue());
		return ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
	}

	/**
	 * @description
	 * @author hxzhang
	 * @create 2016年9月21日下午2:05:45
	 * @version 1.0
	 * @param request
	 * @param id
	 * @param roomId
	 * @return
	 */
	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Cook })
	@ResponseBody
	@RequestMapping(value = "/getProgramTaskGrow.do", method = RequestMethod.POST)
	public Object getProgramTaskGrow(HttpServletRequest request, String id, String roomId) {
		ServiceResult<Object> result = programTaskExgrscService.getProgramTaskExgrsc(id, roomId, TaskType.GrowCurriculum.getValue());
		return ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
	}

	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Cook })
	@ResponseBody
	@RequestMapping(value = "/getSelectStaff.do", method = RequestMethod.GET)
	public Object getSelectStaff(HttpServletRequest request, StaffCondition condition, String p) {
		condition.setParams(p);
		return staffService.getSelecterStaff(condition);
	}

	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
	@ResponseBody
	@RequestMapping(value = "/getCasualStaff.do", method = RequestMethod.GET)
	public Object getCasualStaff(HttpServletRequest request, String p) {
		return staffService.getCasualStaff(p);
	}

	/**
	 * @description 获取followup信息
	 * @author hxzhang
	 * @create 2016年9月22日下午3:45:18
	 * @version 1.0
	 * @param request
	 * @param id
	 * @param taskId
	 * @return
	 */
	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Cook })
	@ResponseBody
	@RequestMapping(value = "/getfollowupInfo.do", method = RequestMethod.POST)
	public Object getFollowupInfo(HttpServletRequest request, String id, String taskId) {
		ServiceResult<Object> result = taskProgramFollowUpService.getTaskProgramFollowUp(id);
		return ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
	}

	/**
	 * @description 保存Followup信息
	 * @author hxzhang
	 * @create 2016年9月22日下午3:50:48
	 * @version 1.0
	 * @param request
	 * @param params
	 * @return
	 */
	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Cook })
	@ResponseBody
	@RequestMapping(value = "/saveFollowupInfo.do", method = RequestMethod.POST)
	public Object saveFollowupInfo(HttpServletRequest request, String params) {
		FollowUpVo followUpVo = JsonUtil.jsonToBean(params, FollowUpVo.class, DateFormatterConstants.SYS_T_FORMAT2);
		// 未排班的兼职及无历史排班的兼职不能进行下步操作
		if (isCasualAndNotRoster(getCurrentUser(request)) && !rosterFactory.havaRoster(getCurrentUser(request))) {
			return sendFailResponse(getTipMsg("no.permissions"));
		}
		ServiceResult<Object> result = taskProgramFollowUpService.addOrUpdateTaskProgramFollowUp(followUpVo, getCurrentUser(request));
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	/**
	 * @description 获取WeeklyEvaluation信息
	 * @author hxzhang
	 * @create 2016年9月23日下午3:30:20
	 * @version 1.0
	 * @param request
	 * @param id
	 * @return
	 */
	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Cook })
	@ResponseBody
	@RequestMapping(value = "/getWeeklyEvaluation.do", method = RequestMethod.POST)
	public Object getWeeklyEvaluation(HttpServletRequest request, String id) {
		ServiceResult<Object> result = weeklyEvaluationService.getWeeklyEvaluationInfo(id);
		return ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
	}

	/**
	 * @description 新增或保存WeeklyEvaluation信息
	 * @author hxzhang
	 * @create 2016年9月23日下午3:58:08
	 * @version 1.0
	 * @param request
	 * @param params
	 * @return
	 */
	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Cook })
	@ResponseBody
	@RequestMapping(value = "/saveWeeklyEvaluation.do", method = RequestMethod.POST)
	public Object saveWeeklyEvaluation(HttpServletRequest request, String params) {
		ProgramWeeklyEvalutionInfo programWeeklyEvalutionInfo = JsonUtil.jsonToBean(params, ProgramWeeklyEvalutionInfo.class,
				DateFormatterConstants.SYS_T_FORMAT2);
		ServiceResult<Object> result = weeklyEvaluationService.addOrUpdateWeeklyEvaluation(programWeeklyEvalutionInfo,
				getCurrentUser(request));
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), (Object) result.getReturnObj());
	}

	/**
	 * 
	 * @description 获取room和center名称
	 * @author gfwang
	 * @create 2016年9月25日下午9:14:36
	 * @version 1.0
	 * @param roomId
	 * @return
	 */
	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Cook })
	@ResponseBody
	@RequestMapping(value = "/getNameByRoomId.do", method = RequestMethod.POST)
	public Object getCenterRoomName(String roomId) {
		return centerService.getCenterRoom(roomId);
	}
}