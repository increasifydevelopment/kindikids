package com.aoyuntek.aoyun.controller.web;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aoyuntek.aoyun.entity.po.OutFamilyPo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.vo.OutModleVo;
import com.aoyuntek.aoyun.factory.UserFactory;
import com.aoyuntek.aoyun.service.ICenterService;
import com.aoyuntek.aoyun.service.IFamilyService;
import com.aoyuntek.aoyun.service.IUserInfoService;
import com.theone.common.util.ServiceResult;
import com.theone.json.util.JsonUtil;
import com.theone.string.util.StringUtil;
import com.theone.web.util.ResponseUtils;

/**
 * @description Kindikids对外接口
 * @author Hxzhang 2016年8月30日下午4:16:22
 * @version 1.0
 */
@Controller
@RequestMapping("/out")
public class OutwardController extends BaseWebController {
	private static Logger log = Logger.getLogger(FamilyController.class);

	@Autowired
	private IFamilyService familyService;
	@Autowired
	private IUserInfoService userService;
	@Autowired
	private ICenterService centerService;
	@Autowired
	private UserFactory userFactory;

	/**
	 * @description 取消发送激活邮件
	 * @author hxzhang
	 * @create 2016年8月30日下午3:44:27
	 * @version 1.0
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/cancelSend.do", method = RequestMethod.GET)
	@ResponseBody
	public void cancelSendActiveEmail(HttpServletResponse response, String id) throws IOException {
		userService.updateCancelSendActiveEmail(id);
		ResponseUtils.gotoLogin(response, getSystemMsg("cancle.email.success"));
	}

	@ResponseBody
	@RequestMapping(value = "/addFamilyOut.do", method = RequestMethod.POST)
	public Object addFamilyOut(HttpServletRequest request, String json) {
		log.info("addFamilyOutController|start|params=" + json);
		if (StringUtil.isEmpty(json)) {
			log.info("return params empty");
			return ResponseUtils.sendMsg(1);
		}
		OutModleVo outModleVo = (OutModleVo) JsonUtil.jsonToBean(json, OutModleVo.class);
		// 封装小孩对象
		UserInfo child = new UserInfo();
		child.setFirstName(outModleVo.getChildFirstname());
		child.setLastName(outModleVo.getChildLastname());
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		try {
			child.setBirthday(sdf.parse(outModleVo.getDobChild()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		if (StringUtil.isNotEmpty(outModleVo.getChildSex())) {
			child.setGender(Short.valueOf(outModleVo.getChildSex()));
		}

		// 封装两个家长对象
		List<UserInfo> parentList = new ArrayList<UserInfo>();
		UserInfo parent1 = new UserInfo();
		parent1.setFirstName(outModleVo.getFirstname1());
		parent1.setLastName(outModleVo.getLastname1());
		parent1.setAddress(outModleVo.getAddress1());
		parent1.setSuburb(outModleVo.getSuburb1());
		parent1.setPostcode(outModleVo.getPostcode1());
		parent1.setPhoneNumber(outModleVo.getHomephone1());
		parent1.setMobileNumber(outModleVo.getMobile1());
		parent1.setWorkPhoneNumber(outModleVo.getWorkphone1());
		parent1.setEmail(outModleVo.getEmail1());
		parentList.add(parent1);
		UserInfo parent2 = new UserInfo();
		parent2.setFirstName(outModleVo.getFirstname2());
		parent2.setLastName(outModleVo.getLastname2());
		parent2.setAddress(outModleVo.getAddress2());
		parent2.setSuburb(outModleVo.getSuburb2());
		parent2.setPostcode(outModleVo.getPostcode2());
		parent2.setPhoneNumber(outModleVo.getHomephone2());
		parent2.setMobileNumber(outModleVo.getMobile2());
		parent2.setWorkPhoneNumber(outModleVo.getWorkphone2());
		parent2.setEmail(outModleVo.getEmail2());
		parentList.add(parent2);

		OutFamilyPo outFamily = new OutFamilyPo();

		outFamily.setChild(child);
		outFamily.setParentList(parentList);
		if (StringUtil.isNotEmpty(outModleVo.getChangeDate())) {
			try {
				outFamily.setChangeDate(sdf.parse(outModleVo.getChangeDate()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}

		outFamily.setMonday(outModleVo.getMonday());
		outFamily.setTuesday(outModleVo.getTuesday());
		outFamily.setWednesday(outModleVo.getWednesday());
		outFamily.setThursday(outModleVo.getThursday());
		outFamily.setFriday(outModleVo.getFriday());
		outFamily.setFamilyName(outModleVo.getLastname1());
		outFamily.setCentreList(outModleVo.getCentreList());

		ServiceResult<Object> result = familyService.addFamilyOut(outFamily, outModleVo);
		if (result.isSuccess()) {
			// 孩子创建成功给家长发送邮件
			userFactory.sendParentEmailForAppliction(getSystemEmailMsg("out_add_child_title"), getSystemEmailMsg("out_add_child_content"),
					true, outFamily);
			// userFactory.sendParentEmailByChild(getSystemEmailMsg("out_add_child_title"),
			// getSystemEmailMsg("out_add_child_content"), null,
			// true, EmailType.out_add_child.getValue(), null, null, null,
			// outFamily);
		}
		log.info("addFamilyOutController|end");
		return ResponseUtils.sendMsg(result.getCode(), result.getMsg());
	}

	/**
	 * @description 获取Center详细信息
	 * @author hxzhang
	 * @create 2016年12月8日上午11:47:45
	 */
	@ResponseBody
	@RequestMapping(value = "/getAllCenters.do")
	public Object getAllCenters(HttpServletRequest request) {
		ServiceResult<Object> result = centerService.getAllCentersInfos();
		return ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
	}

	@ResponseBody
	@RequestMapping(value = "/saveFmily.do")
	public Object saveFmily(HttpServletRequest request, String json) {
		System.err.println(json);
		return sendSuccessResponse();
	}
}
