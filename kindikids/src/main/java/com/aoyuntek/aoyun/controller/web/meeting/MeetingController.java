package com.aoyuntek.aoyun.controller.web.meeting;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aoyuntek.aoyun.condtion.MeetingAgendaCondition;
import com.aoyuntek.aoyun.condtion.MeetingCondition;
import com.aoyuntek.aoyun.constants.DateFormatterConstants;
import com.aoyuntek.aoyun.controller.web.BaseWebController;
import com.aoyuntek.aoyun.entity.po.meeting.AgendaTypeInfo;
import com.aoyuntek.aoyun.entity.vo.meeting.MeetingAgendaInfoVo;
import com.aoyuntek.aoyun.entity.vo.meeting.MeetingTempVo;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.enums.TaskFrequenceRepeatType;
import com.aoyuntek.aoyun.service.meeting.IMeetingAgendaService;
import com.aoyuntek.aoyun.service.meeting.IMeetingTempService;
import com.aoyuntek.aoyun.uitl.GsonUtil;
import com.aoyuntek.framework.annotation.UserAuthority;
import com.google.gson.reflect.TypeToken;
import com.theone.common.util.ServiceResult;
import com.theone.date.util.DateUtil;
import com.theone.json.util.JsonUtil;
import com.theone.list.util.ListUtil;
import com.theone.web.util.ResponseUtils;

@Controller
@RequestMapping(value = "/meeting")
public class MeetingController extends BaseWebController {

    @Autowired
    private IMeetingAgendaService meetingAgendaService;
    @Autowired
    private IMeetingTempService meetingTempService;

    /**
     * 
     * @description 更新Manage Agenda Type
     * @author mingwang
     * @create 2016年10月13日上午9:46:04
     * @version 1.0
     * @param request
     * @param params
     * @return
     */
    @UserAuthority(roles = { Role.CEO })
    @ResponseBody
    @RequestMapping(value = "/updateAgendaType.do", method = RequestMethod.POST)
    public Object updateAgendaType(HttpServletRequest request, String params) {
        log.info("updateAgendaType|start");
        List<AgendaTypeInfo> agendaTypeList = JsonUtil.jsonToList(params, new TypeToken<List<AgendaTypeInfo>>() {
        }.getType(), DateFormatterConstants.SYS_T_FORMAT);
        ServiceResult<Object> result = meetingAgendaService.updateAgendaType(agendaTypeList, getCurrentUser(request));
        log.info("updateAgendaType|end");
        return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
    }

    /**
     * 
     * @description 获取agendaType
     * @author mingwang
     * @create 2016年10月13日上午11:04:36
     * @version 1.0
     * @param request
     * @return
     */
    @UserAuthority(roles = { Role.CEO })
    @ResponseBody
    @RequestMapping(value = "/getAgendaType.do", method = RequestMethod.POST)
    public Object getAgendaType(HttpServletRequest request) {
        ServiceResult<Object> result = meetingAgendaService.getAgendaType();
        return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
    }

    /**
     * 
     * @description 新增/更新meetingAgenda
     * @author mingwang
     * @create 2016年10月13日下午3:43:01
     * @version 1.0
     * @param request
     * @param params
     * @return
     */
    @UserAuthority(roles = { Role.CEO })
    @ResponseBody
    @RequestMapping(value = "/addMeetingAgenda.do", method = RequestMethod.POST)
    public Object addMeetingAgenda(HttpServletRequest request, String params) {
        MeetingAgendaInfoVo meetingAgendaInfoVo = (MeetingAgendaInfoVo) GsonUtil.jsonToBean(params, MeetingAgendaInfoVo.class,
                DateFormatterConstants.SYS_FORMAT, DateFormatterConstants.SYS_FORMAT2, DateFormatterConstants.SYS_T_FORMAT,
                DateFormatterConstants.SYS_T_FORMAT2);
        ServiceResult<Object> result = meetingAgendaService.addAndUpdateMeetingAgenda(meetingAgendaInfoVo, getCurrentUser(request));
        return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
    }

    /**
     * 
     * @description 获取meetingAgenda
     * @author mingwang
     * @create 2016年10月13日下午3:16:40
     * @version 1.0
     * @param request
     * @param id
     * @return
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/getMeetingAgenda.do", method = RequestMethod.POST)
    public Object getMeetingAgenda(HttpServletRequest request, String id) {
        ServiceResult<Object> result = meetingAgendaService.getMeetingAgenda(id);
        return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
    }

    /**
     * 
     * @description 获取meetingAgenda列表
     * @author mingwang
     * @create 2016年10月14日上午10:10:05
     * @version 1.0
     * @param request
     * @param condition
     * @return
     */
    @UserAuthority(roles = { Role.CEO })
    @ResponseBody
    @RequestMapping(value = "/getMeetingAgendaList.do", method = RequestMethod.POST)
    public Object getMeetingAgendaList(HttpServletRequest request, MeetingAgendaCondition condition) {
        ServiceResult<Object> result = meetingAgendaService.getMeetingAgendaList(condition);
        return ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
    }

    /**
     * 
     * @description 验证MeetingAgendaName是否重复
     * @author mingwang
     * @create 2016年10月14日下午2:28:08
     * @version 1.0
     * @param response
     * @param templateName
     * @param id
     */
    @UserAuthority(roles = { Role.CEO })
    @ResponseBody
    @RequestMapping(value = "/validateMeetingAgendaName.do", method = RequestMethod.POST)
    public void validateMeetingAgendaName(HttpServletResponse response, String agendaName, String id) {
        boolean result = meetingAgendaService.validateMeetingAgendaName(agendaName, id);
        bootStrapValidateRemote(response, result);
    }

    /**
     * 
     * @description 修改MeetingAgendaState状态
     * @author mingwang
     * @create 2016年10月14日下午4:20:13
     * @version 1.0
     * @param response
     * @param agendaName
     * @param id
     */
    @UserAuthority(roles = { Role.CEO })
    @ResponseBody
    @RequestMapping(value = "/updateMeetingAgendaState.do", method = RequestMethod.POST)
    public Object updateMeetingAgendaState(HttpServletResponse response, short agendaState, String id) {
        ServiceResult<Object> result = meetingAgendaService.updateAgendaState(agendaState, id);
        return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
    }

    /**
     * @description 获取会议模版列表
     * @author hxzhang
     * @create 2016年10月13日上午10:08:35
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/getMeetingTempList.do", method = RequestMethod.POST)
    public Object getMeetingTempList(HttpServletRequest request, MeetingCondition condition) {
        ServiceResult<Object> result = meetingTempService.getMeetingTempList(condition);
        return ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
    }

    /**
     * @description 新增或编辑会议模版信息
     * @author hxzhang
     * @create 2016年10月13日上午10:03:47
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/saveMeetingTemp.do", method = RequestMethod.POST)
    public Object saveMeetingTemp(HttpServletRequest request, String params) {
        MeetingTempVo meetingTempVo = (MeetingTempVo) GsonUtil.jsonToBean(params, MeetingTempVo.class, DateFormatterConstants.SYS_FORMAT,
                DateFormatterConstants.SYS_FORMAT2, DateFormatterConstants.SYS_T_FORMAT);
        if (!mustInput(meetingTempVo.getMeetingTemplateInfo().getTemplateName())) {
            return ResponseUtils.sendMsg(-1, getTipMsg("meeting_save_name"));
        }

        if (ListUtil.isEmpty(meetingTempVo.getNqsList())) {
            return ResponseUtils.sendMsg(-1, getTipMsg("meeting_save_nqsList"));
        }

        Short shortVal = meetingTempVo.getFrequency().getRepeats();
        if (shortVal == null) {
            return ResponseUtils.sendMsg(-1, getTipMsg("meeting_save_frequency"));
        }

        TaskFrequenceRepeatType frequenceRepeatType = TaskFrequenceRepeatType.getType(shortVal);

        // getStartsDate时间必须大于等于当前时间
        if (meetingTempVo.getFrequency().getStartsOnDate() != null
                && com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(new Date(), meetingTempVo.getFrequency().getStartsOnDate()) < 0) {
            return ResponseUtils.sendMsg(-1, getTipMsg("meeting_save_frequency_start_now"));
        }

        // 判断吐过end选择的话 那么Starts <= Ends
        if (meetingTempVo.getFrequency().getEndOnDate() != null) {
            if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(meetingTempVo.getFrequency().getStartsOnDate(), meetingTempVo.getFrequency().getEndOnDate()) < 0) {
                return ResponseUtils.sendMsg(-1, getTipMsg("meeting_save_frequency_endstart_date"));
            }
        }

        switch (frequenceRepeatType) {
        case Weekly:
        case Monthly:
        case Yearly:
            if (!mustInput(meetingTempVo.getFrequency().getStartsOnDate())) {
                return ResponseUtils.sendMsg(-1, getTipMsg("meeting_save_frequency_startDate"));
            }
            if (meetingTempVo.getFrequency().getHaveEndDate() == 1 && !mustInput(meetingTempVo.getFrequency().getEndOnDate())) {
                return ResponseUtils.sendMsg(-1, getTipMsg("meeting_save_frequency_endDate"));
            }
            break;
        default:
            break;
        }

        // switch (frequenceRepeatType) {
        // case Weekly: {
        // if (Integer.parseInt(meetingTempVo.getFrequency().getWeekdayStart())
        // > Integer.parseInt(meetingTempVo.getFrequency().getWeekdayDue())) {
        // return ResponseUtils.sendMsg(-1,
        // getTipMsg("meeting_save_frequency_weekly_start_due"));
        // }
        // }
        // break;
        // case Monthly: {
        // if (Integer.parseInt(meetingTempVo.getFrequency().getWeekdayStart())
        // > Integer.parseInt(meetingTempVo.getFrequency().getWeekdayDue())) {
        // return ResponseUtils.sendMsg(-1,
        // getTipMsg("meeting_save_frequency_monthy_start_due"));
        // }
        // }
        // break;
        // case Yearly: {
        // String startStr = meetingTempVo.getFrequency().getWeekdayStart() +
        // "/2016";
        // String endStr = meetingTempVo.getFrequency().getWeekdayDue() +
        // "/2016";
        // Date dayToStart = DateUtil.parse(startStr,
        // DateFormatterConstants.SYS_FORMAT2);
        // Date dayToDue = DateUtil.parse(endStr,
        // DateFormatterConstants.SYS_FORMAT2);
        // if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(dayToStart, dayToDue) < 0) {
        // return ResponseUtils.sendMsg(-1,
        // getTipMsg("meeting_save_frequency_YearlyOnceOff_start_due"));
        // }
        // }
        // break;
        // default:
        // break;
        // }
        ServiceResult<Object> result = meetingTempService.addOrUpdateMeetingTemp(meetingTempVo, getCurrentUser(request));
        return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
    }

    /**
     * @description 获取会议模版信息
     * @author hxzhang
     * @create 2016年10月13日下午3:31:34
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/getMeetingTempInfo.do", method = RequestMethod.POST)
    public Object getMeetingTemp(HttpServletRequest request, String id) {
        ServiceResult<Object> result = meetingTempService.getMeetingTempInfo(id);
        return ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
    }

    /**
     * @description 验证会议模版名称是否重复
     * @author hxzhang
     * @create 2016年10月14日上午9:37:35
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/validateMeetingTempName.do", method = RequestMethod.POST)
    public void validateMeetingTempName(HttpServletResponse response, String templateName, String id) {
        boolean result = meetingTempService.validateMeetingTempName(templateName, id);
        bootStrapValidateRemote(response, result);
    }

    /**
     * @description 禁用启用会议模版
     * @author hxzhang
     * @create 2016年10月14日上午11:45:26
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/operaMeetingTemp.do", method = RequestMethod.POST)
    public Object operaMeetingTemp(HttpServletRequest request, String id, short state) {
        ServiceResult<Object> result = meetingTempService.saveOperaMeetingTemp(id, state, getCurrentUser(request));
        return ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
    }

}
