package com.aoyuntek.aoyun.controller.web;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.aoyuntek.aoyun.condtion.AuthGroupCondition;
import com.aoyuntek.aoyun.constants.S3Constants;
import com.aoyuntek.aoyun.dao.AttachmentInfoMapper;
import com.aoyuntek.aoyun.dao.UserStaffMapper;
import com.aoyuntek.aoyun.entity.po.AttachmentInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.vo.BatchFileVo;
import com.aoyuntek.aoyun.entity.vo.FileVo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.aoyun.entity.vo.StaffRoleInfoVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.enums.GroupType;
import com.aoyuntek.aoyun.service.IAuthGroupService;
import com.aoyuntek.aoyun.service.ICommonService;
import com.aoyuntek.aoyun.service.IUserInfoService;
import com.aoyuntek.aoyun.uitl.FilePathUtil;
import com.aoyuntek.aoyun.uitl.SqlUtil;
import com.aoyuntek.framework.constants.PropertieNameConts;
import com.google.gson.reflect.TypeToken;
import com.theone.aws.util.FileFactory;
import com.theone.common.util.ServiceResult;
import com.theone.file.util.FileUtil;
import com.theone.json.util.JsonUtil;
import com.theone.string.util.StringUtil;
import com.theone.web.util.RequestUtil;
import com.theone.web.util.ResponseUtils;

import sun.misc.BASE64Decoder;

/**
 * 
 * @description 公共路由
 * @author gfwang
 * @create 2016年7月5日下午3:32:25
 * @version 1.0
 */
@RequestMapping("/common")
@Controller
public class CommonController extends BaseWebController {

	@Autowired
	private ICommonService commonService;

	@Autowired
	private IUserInfoService userInfoService;

	@Autowired
	private UserStaffMapper staffMapper;

	@Autowired
	private AttachmentInfoMapper attachmentInfoMapper;

	@Autowired
	private IAuthGroupService authGroupService;

	/**
	 * 
	 * @description 上传附件（除可预览图片）
	 * @author gfwang
	 * @create 2016年7月5日下午3:31:09
	 * @version 1.0
	 * @param request
	 *            请求
	 * @return 结果
	 */
	@ResponseBody
	@RequestMapping(value = "/upload.annex.do", method = RequestMethod.POST)
	public Object uploadAnnex(MultipartHttpServletRequest request) {
		// 上传文件
		ServiceResult<FileVo> result = uploadFile(request, FilePathUtil.TEMP_PATH);
		// 上传不成功
		if (!result.isSuccess()) {
			return ResponseUtils.sendMsg(result.getCode(), result.getMsg(), result.getReturnObj().getFileName());
		}
		// 上传成功
		FileVo fileVo = result.getReturnObj();
		return ResponseUtils.sendMsg(0, fileVo);
	}

	/**
	 * 
	 * @description 上传可预览图片
	 * @author gfwang
	 * @create 2016年7月5日下午3:31:39
	 * @version 1.0
	 * @param request
	 *            请求
	 * @return 结果
	 */
	@ResponseBody
	@RequestMapping(value = "/upload.img.do", method = RequestMethod.POST)
	public Object uploadImg(MultipartHttpServletRequest request) {
		// 上传文件
		ServiceResult<FileVo> result = uploadFile(request, FilePathUtil.IMG_PATH);
		// 上传不成功
		if (!result.isSuccess()) {
			return ResponseUtils.sendMsg(result.getCode(), result.getMsg());
		}
		// 上传成功
		FileVo fileVo = result.getReturnObj();
		return ResponseUtils.sendMsg(0, fileVo);
	}

	/**
	 * 
	 * @description 上传base64编码的文件
	 * @author gfwang
	 * @create 2016年7月13日下午9:43:59
	 * @version 1.0
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/upload.base64file.do")
	@ResponseBody
	public Object uploadBase64(HttpServletRequest request) throws Exception {
		// 文件
		String fileStr = RequestUtil.getStringParameter(request, "fileStrBase64", "");
		if (StringUtil.isNotEmpty(fileStr) && fileStr.contains(";base64,")) {
			fileStr = fileStr.split(";base64,")[1];
		}
		// 文件名
		String fileName = RequestUtil.getStringParameter(request, "fileName", "");
		InputStream ins = GenerateImage(fileStr);
		FileFactory fac = new FileFactory(Region.getRegion(Regions.AP_SOUTHEAST_2), PropertieNameConts.S3);
		String ext = FileUtil.getExtensionName(fileName);
		// 上传头像必须为png, jpg, gif
		if (!ext.toLowerCase().equals("png") && !ext.toLowerCase().equals("jpg") && !ext.toLowerCase().equals("gif")
				&& !ext.toLowerCase().equals("jpeg") && !ext.toLowerCase().equals("bmp")) {
			return ResponseUtils.sendMsg(1, getTipMsg("upload_img_error"));
		}
		String fileIdName = UUID.randomUUID().toString() + "." + ext;
		// 上传S3
		String relativePathName = fac.uploadFile(FilePathUtil.TEMP_PATH + fileIdName, ins);
		String visitedUrl = fac.getS3RouteUrl(relativePathName, S3Constants.URL_TIMEOUT_HOUR);
		return ResponseUtils.sendMsg(0, new FileVo(fileIdName, fileName, relativePathName, visitedUrl));
	}

	/**
	 * 
	 * @description 字符串转输入流
	 * @author gfwang
	 * @create 2016年7月13日下午9:43:31
	 * @version 1.0
	 * @param fileStr
	 *            文件编码的字符串
	 * @return InputStream
	 */
	public InputStream GenerateImage(String fileStr) { // 对字节数组字符串进行Base64解码并生成图片
		BASE64Decoder decoder = new BASE64Decoder();
		InputStream ins = null;
		try {
			// Base64解码
			byte[] b = decoder.decodeBuffer(fileStr);
			for (int i = 0; i < b.length; ++i) {
				if (b[i] < 0) {// 调整异常数据
					b[i] += 256;
				}
			}
			// 生成jpeg图片
			ins = new ByteArrayInputStream(b);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ins;
	}

	/**
	 * 
	 * @description 下载附件
	 * @author bbq
	 * @create 2016年6月24日下午3:13:51
	 * @version 1.0
	 * @param request
	 *            HttpServletRequest对象
	 * @throws FileNotFoundException
	 */
	@RequestMapping(value = "/download.do", method = RequestMethod.GET)
	public void download(HttpServletRequest request, HttpServletResponse response, String fileRelativeIdName, String fileName)
			throws FileNotFoundException {
		try {
			fileName = new String(fileName.getBytes("ISO8859-1"), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		if (StringUtil.isNotEmpty(fileName) && StringUtil.isNotEmpty(fileRelativeIdName)) {
			FileFactory fac = new FileFactory(Region.getRegion(Regions.AP_SOUTHEAST_2), PropertieNameConts.S3);
			fac.downloadS3File(response, fileRelativeIdName, fileName);
		}

	}

	/**
	 * 
	 * @description 获取图片地址
	 * @author gfwang
	 * @create 2016年7月9日上午9:07:31
	 * @version 1.0
	 * @param request
	 * @param url
	 *            相对路径
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/img.show.do", method = RequestMethod.GET)
	public String imgShow(HttpServletRequest request, String url) throws Exception {
		if ("null".equals(url) || "undefined".equals(url)) {
			return "redirect:" + getSystemMsg("rootUrl") + "/resources/images/empty.png";
		}
		FileFactory fac = new FileFactory(Region.getRegion(Regions.AP_SOUTHEAST_2), PropertieNameConts.S3);
		String visitUrl = fac.getS3RouteUrl(url, S3Constants.URL_TIMEOUT_HOUR);
		return "redirect:" + visitUrl;
	}

	/**
	 * 
	 * @description 获取人和组
	 * @author gfwang
	 * @create 2016年7月11日下午8:20:02
	 * @version 1.0
	 * @param request
	 * @param p
	 * @param condition
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/get.userAndGroup.do")
	public Object getUserAndGroup(HttpServletRequest request, String p) {
		UserInfoVo currentUser = getCurrentUser(request);
		p = SqlUtil.likeEscape(p);
		Set<SelecterPo> result = commonService.getReceiversMessage(p, currentUser, false, null);
		return result;
	}

	/**
	 * 
	 * @description 转发邮件时获取人员信息
	 * @author gfwang
	 * @create 2016年7月14日下午4:09:26
	 * @version 1.0
	 * @param request
	 * @param p
	 *            参数
	 * @param defaultUser
	 *            默认发件人，和收件人
	 * @return result
	 */
	@ResponseBody
	@RequestMapping("/getUsersInMessage.do")
	public Object getUsersInMessage(HttpServletRequest request, String p, String msgId) {
		UserInfoVo currentUser = getCurrentUser(request);
		p = SqlUtil.likeEscape(p);
		Set<SelecterPo> result = commonService.getReceiversMessage(p, currentUser, true, msgId);
		return result;
	}

	/**
	 * 
	 * @description 获取人newsfeed
	 * @author gfwang
	 * @create 2016年7月11日下午8:20:02
	 * @version 1.0
	 * @param request
	 * @param p
	 * @param condition
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/get.user.do")
	public Object selectUser(HttpServletRequest request, String p, int type) {
		UserInfoVo currentUser = getCurrentUser(request);
		// 处理特殊字符
		p = SqlUtil.likeEscape(p);
		Set<SelecterPo> result = commonService.getUsersNews(p, currentUser, type);
		return result;
	}

	/**
	 * 
	 * @description 获取组newsfeed
	 * @author gfwang
	 * @create 2016年7月11日下午8:20:02
	 * @version 1.0
	 * @param request
	 * @param p
	 * @param condition
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/get.group.do")
	public Object selectGroup(HttpServletRequest request, String p) {
		UserInfoVo currentUser = getCurrentUser(request);
		// 处理特殊字符
		p = SqlUtil.likeEscape(p);
		return commonService.getGroupsNews(p, currentUser);
	}

	/**
	 * 
	 * @description 获取园区
	 * @author gfwang
	 * @create 2016年8月1日下午2:44:43
	 * @version 1.0
	 * @param request
	 * @param p
	 * @return
	 */
	@RequestMapping(value = "/getCenters.do")
	@ResponseBody
	public Object getCenters(HttpServletRequest request, String p) {
		return commonService.getCenters(getCurrentUser(request), p);
	}

	/**
	 * 
	 * @description 获取room
	 * @author gfwang
	 * @create 2016年8月1日下午2:44:53
	 * @version 1.0
	 * @param request
	 * @param parentId
	 * @param p
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getRooms.do")
	public Object getRooms(HttpServletRequest request, String parentId, String p) {
		return commonService.getRooms(getCurrentUser(request), parentId, p);
	}

	/**
	 * 
	 * @description 获取分组
	 * @author gfwang
	 * @create 2016年8月1日下午2:45:07
	 * @version 1.0
	 * @param request
	 * @param parentId
	 * @param p
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getGroups.do")
	public Object getGroups(HttpServletRequest request, String parentId, String p) {
		return commonService.getGroups(getCurrentUser(request), parentId, p);
	}

	/**
	 * @description 验证邮箱是否重复
	 * @author xdwang
	 * @create 2015年12月28日下午8:12:13
	 * @version 1.0
	 * @param request
	 *            HttpServletRequest对象
	 * @param response
	 *            HttpServletResponse对象
	 */
	@RequestMapping(value = "/validateEmail.do", method = RequestMethod.POST)
	public void validateEmail(HttpServletRequest request, HttpServletResponse response, String email, String userId) {
		boolean result = false;
		result = userInfoService.validateEmail(email, userId);
		bootStrapValidateRemote(response, result);
	}

	/**
	 * @description 获取员工信息
	 * @author hxzhang
	 * @create 2016年8月16日上午10:03:09
	 * @version 1.0
	 * @param request
	 *            HttpServletRequest
	 * @param params
	 *            关键字
	 * @return 返回查询结果
	 */
	@RequestMapping(value = "/getStaff.do", method = RequestMethod.GET)
	@ResponseBody
	public Object getStaff(HttpServletRequest request, String p, String c, Integer type) {
		type = type == null ? 0 : type;
		return commonService.getStaff(getCurrentUser(request), p, c, type);
	}

	@RequestMapping(value = "/getStaff2.do", method = RequestMethod.GET)
	@ResponseBody
	public Object getStaff2(HttpServletRequest request, String p, String c, Integer type) {
		type = type == null ? 0 : type;
		return commonService.getStaff2(getCurrentUser(request), p, c, type);
	}

	@RequestMapping(value = "/getStaffChild.do", method = RequestMethod.GET)
	@ResponseBody
	public Object getStaffAndChild(HttpServletRequest request, String p, String c, Integer type) {
		type = type == null ? 0 : type;
		return commonService.getStaffAndChild(getCurrentUser(request), p, c, type);
	}

	/**
	 * 
	 * @description 选择园区获取staff和兼职
	 * @author gfwang
	 * @create 2016年10月28日上午10:35:02
	 * @version 1.0
	 * @param p
	 * @param c
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getStffByChoooseCenter.do", method = RequestMethod.GET)
	public Object getStffByChoooseCenter(String p, String c) {
		return commonService.getStffByChoooseCenter(c, p);
	}

	/**
	 * 
	 * @description 获取组织信息(左侧查询菜单)
	 * @author gfwang
	 * @create 2016年8月4日下午5:06:26
	 * @version 1.0
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping("/getMenuOrgs.do")
	public Object getMenuOrgs(HttpServletRequest request, Short type) {
		StaffRoleInfoVo orgs = commonService.getMenuOrgs(type, getCurrentUser(request));
		return ResponseUtils.sendMsg(0, orgs);
	}

	/**
	 * 
	 * @description 获取下一个人的头像颜色
	 * @author gfwang
	 * @create 2016年9月2日下午5:13:02
	 * @version 1.0
	 * @param request
	 * @param familyId
	 * @param userType
	 * @return 颜色
	 */
	@ResponseBody
	@RequestMapping(value = "/getNextColor.do", method = RequestMethod.POST)
	public Object getNextColor(HttpServletRequest request, String familyId, Short userType) {
		UserInfo user = new UserInfo();
		user.setFamilyId(familyId);
		user.setUserType(userType);
		String color = commonService.getPersonColor(user);
		return sendSuccessResponse(color);
	}

	/*************************************
	 * form 接口
	 ******************************************/
	/**
	 * 
	 * @description 批量文件上传
	 * @author gfwang
	 * @create 2016年9月24日下午6:59:53
	 * @version 1.0
	 * @param request
	 * @param sourceId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/uploadBatch.do", method = RequestMethod.POST)
	public Object uploadBatch(MultipartHttpServletRequest request, String sourceId) {
		ServiceResult<BatchFileVo> result = commonService.uploadBatch(request, sourceId, FilePathUtil.TEMP_PATH);
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	/**
	 * 
	 * @description 获取文件集合
	 * @author gfwang
	 * @create 2016年9月24日下午7:00:15
	 * @version 1.0
	 * @param sourceId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getFileList.do", method = RequestMethod.POST)
	public Object getFileList(String sourceId) {
		List<FileVo> files = attachmentInfoMapper.getNotTempFileListVo(sourceId);
		return sendSuccessResponse(files);
	}

	/**
	 * 
	 * @description 下载附件
	 * @author bbq
	 * @create 2016年6月24日下午3:13:51
	 * @version 1.0
	 * @param request
	 *            HttpServletRequest对象
	 * @throws FileNotFoundException
	 */
	@RequestMapping(value = "/downloadById.do", method = RequestMethod.GET)
	public void downloadById(HttpServletRequest request, HttpServletResponse response, String id) throws FileNotFoundException {
		AttachmentInfo attachmentInfo = attachmentInfoMapper.selectByPrimaryKey(id);
		String fileName = attachmentInfo.getAttachName();
		if (StringUtil.isNotEmpty(fileName) && StringUtil.isNotEmpty(attachmentInfo.getAttachId())) {
			FileFactory fac = new FileFactory(Region.getRegion(Regions.AP_SOUTHEAST_2), PropertieNameConts.S3);
			fac.downloadS3File(response, attachmentInfo.getAttachId(), fileName);
		}

	}

	/**
	 * 
	 * @description 获取分组信息
	 * @author gfwang
	 * @create 2016年9月24日下午7:00:28
	 * @version 1.0
	 * @param p
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getFormGroups.do", method = RequestMethod.GET)
	public Object getFormGroups(String p) {
		AuthGroupCondition condition = new AuthGroupCondition();
		condition.setType(GroupType.FormGroup.getValue());
		condition.setKeyWord(p);
		return sendSuccessResponse(authGroupService.getSelecterGroup(condition));
	}

	/**
	 * 
	 * @description 分组集合获取人
	 * @author gfwang
	 * @create 2016年9月24日下午6:41:48
	 * @version 1.0
	 * @param condition
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getFormUsersByGroups.do", method = RequestMethod.GET)
	public Object getFormUsersByGroups(String chooseGroups, String p, boolean flag) {
		if (StringUtil.isEmpty(p) && flag == false) {
			return sendSuccessResponse(new ArrayList<SelecterPo>());
		}
		AuthGroupCondition condition = new AuthGroupCondition();
		condition.setKeyWord(p);
		List<String> groups = JsonUtil.jsonToList(chooseGroups, new TypeToken<List<String>>() {
		}.getType());
		condition.setChooseGroups(groups);
		return sendSuccessResponse(authGroupService.dealSelectUsersByGroups(condition));
	}
	/*************************************
	 * form 接口
	 ******************************************/

}
