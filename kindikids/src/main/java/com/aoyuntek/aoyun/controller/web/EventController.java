package com.aoyuntek.aoyun.controller.web;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aoyuntek.aoyun.condtion.EventCondition;
import com.aoyuntek.aoyun.condtion.GalleryCondition;
import com.aoyuntek.aoyun.constants.DateFormatterConstants;
import com.aoyuntek.aoyun.entity.vo.event.EventInfoVo;
import com.aoyuntek.aoyun.entity.vo.event.GalleryInfoVo;
import com.aoyuntek.aoyun.service.event.IEventService;
import com.aoyuntek.framework.model.Pager;
import com.theone.common.util.ServiceResult;
import com.theone.json.util.JsonUtil;
import com.theone.web.util.ResponseUtils;

@Controller
@RequestMapping(value = "/event")
public class EventController extends BaseWebController {

	/**
	 * 
	 */
	@Autowired
	private IEventService eventServcie;

	@ResponseBody
	@RequestMapping("/info.do")
	public Object info(HttpServletRequest request, String id) {
		ServiceResult<EventInfoVo> result = eventServcie.getEventInfo(id, getCurrentUser(request));
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());

	}

	@ResponseBody
	@RequestMapping("/edit.do")
	public Object edit(HttpServletRequest request) {
		String jsonStr = ServletRequestUtils.getStringParameter(request, "params", "");
		EventInfoVo eventInfoVo = (EventInfoVo) JsonUtil.jsonToBean(jsonStr, EventInfoVo.class, DateFormatterConstants.SYS_T_FORMAT);
		ServiceResult<Object> result = eventServcie.addOrUpdateEvent(eventInfoVo, getCurrentUser(request));
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	@ResponseBody
	@RequestMapping("/getChild.do")
	public Object getChild(HttpServletRequest request, String id) {
		ServiceResult<Object> result = eventServcie.getChildIdByFamilyId(id);
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());

	}

	@ResponseBody
	@RequestMapping("/list.do")
	public Object list(HttpServletRequest request) {
		String jsonStr = ServletRequestUtils.getStringParameter(request, "params", "");
		EventCondition condition = (EventCondition) JsonUtil.jsonToBean(jsonStr, EventCondition.class, DateFormatterConstants.SYS_T_FORMAT);
		ServiceResult<Pager<EventInfoVo, EventCondition>> result = eventServcie.getEventList(condition, getCurrentUser(request));
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	@ResponseBody
	@RequestMapping("/galleryList.do")
	public Object galleryList(HttpServletRequest request) {
		String jsonStr = ServletRequestUtils.getStringParameter(request, "params", "");
		GalleryCondition condition = (GalleryCondition) JsonUtil.jsonToBean(jsonStr, GalleryCondition.class, DateFormatterConstants.SYS_T_FORMAT);
		ServiceResult<Pager<GalleryInfoVo, GalleryCondition>> result = eventServcie.getGalleryList(condition, getCurrentUser(request));
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	@ResponseBody
	@RequestMapping(value = "/centerList.do", method = RequestMethod.POST)
	public Object centerList(HttpServletRequest request) {
		ServiceResult<Object> result = eventServcie.getCenterList();
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	@ResponseBody
	@RequestMapping("/delete.do")
	public Object delete(HttpServletRequest request, String id) {
		ServiceResult<Object> result = eventServcie.deleteEvent(id, getCurrentUser(request));
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());

	}

}
