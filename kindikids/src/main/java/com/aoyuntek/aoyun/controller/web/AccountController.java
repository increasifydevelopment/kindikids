package com.aoyuntek.aoyun.controller.web;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.enums.AccountActiveStatus;
import com.aoyuntek.aoyun.service.IUserInfoService;
import com.theone.common.util.ServiceResult;
import com.theone.string.util.StringUtil;
import com.theone.web.util.ResponseUtils;

/**
 * @description 帐号控制层
 * @author xdwang
 * @create 2015年12月10日下午7:30:17
 * @version 1.0
 */
@Controller
@RequestMapping("/account")
public class AccountController extends BaseWebController {

	/**
	 * userService
	 */
	@Autowired
	private IUserInfoService userService;

	/**
	 * @description 登陆
	 * @author bbq
	 * @create 2016年6月16日下午7:03:19
	 * @version 1.0
	 * @param request
	 *            HttpServletRequest 对象 account&password
	 * @return 操作结果json
	 */
	@RequestMapping(value = "/login.do", method = RequestMethod.POST)
	@ResponseBody
	public Object login(HttpServletRequest request, String account, String password, String googleVlidateId) {
		// 判空
		if (StringUtil.isEmpty(account) || StringUtil.isEmpty(password)) {
			return ResponseUtils.sendMsg(false, getTipMsg("login.emailPasswordEmpty"));
		}
		// 登陆
		ServiceResult<UserInfoVo> result = userService.saveLogin(account, password, googleVlidateId);
		// 保存当前对象
		if (!result.isSuccess()) {
			return ResponseUtils.sendMsg(result.getCode(), result.getMsg());
		}
		saveUserToSession(request, (UserInfoVo) result.getReturnObj());
		boolean futureEnrolled = ((UserInfoVo) result.getReturnObj()).isFutureEnrolled();
		return ResponseUtils.sendMsg(result.getCode(), futureEnrolled);
	}

	/**
	 * @description 注销登录
	 * @author xdwang
	 * @create 2015年11月28日下午2:45:06
	 * @version 1.0
	 * @param request
	 *            HttpServletRequest对象
	 * @return 操作结果
	 */
	@RequestMapping(value = "/logout.do", method = RequestMethod.GET)
	@ResponseBody
	public Object logout(HttpServletRequest request) {
		removeUserToSession(request);
		return ResponseUtils.sendMsg(0, getTipMsg("logout.success"));
	}

	/**
	 * 
	 * @description 发送重置密码的邮件
	 * @author bbq
	 * @create 2016年6月16日下午7:07:41
	 * @version 1.0
	 * @param request
	 *            HttpServletRequest对象
	 * @return 操作结果json
	 */
	@RequestMapping(value = "/sendPwdMail.do", method = RequestMethod.POST)
	@ResponseBody
	public Object sendPwdMail(String account) {
		ServiceResult<Object> result = userService.dealChangePwdByMail(account);
		return ResponseUtils.sendMsg(result.getCode(), result.getMsg());
	}

	@RequestMapping(value = "/getCurrentUser.do")
	@ResponseBody
	public Object getUser(HttpServletRequest request, HttpServletResponse response) {
		UserInfoVo user = getCurrentUser(request);
		if (user == null) {
			return sendSuccessResponse();
		}
		user.setFutureEnrolled(userService.isFutureEnrolled(user.getUserInfo().getFamilyId()));
		user.setCurrtenDate(new Date());
		user.getAccountInfo().setPsw(null);
		return ResponseUtils.sendMsg(0, user);
	}

	/**
	 * 
	 * @description 重置密码
	 * @author bbq
	 * @create 2016年6月16日下午7:09:52
	 * @version 1.0
	 * @param request
	 *            HttpServletRequest对象
	 * @return 操作结果json
	 */
	@RequestMapping(value = "/resetPwd.do", method = RequestMethod.POST)
	@ResponseBody
	public Object resetPwd(HttpServletRequest request, String token, String newPwd, String rePwd) {
		// 判空
		if (StringUtil.isEmpty(newPwd) || StringUtil.isEmpty(rePwd)) {
			return ResponseUtils.sendMsg(false, getTipMsg("editUser.invalidParam"));
		}
		// 验证密码格式，及两次密码是否一致
		if (!newPwd.equals(rePwd)) {
			return ResponseUtils.sendMsg(false, getTipMsg("resetPwd.findPassword.noSamePwd"));
		}
		// 验证Code合法性，并找到关联用户
		ServiceResult<UserInfoVo> result = userService.dealChangePwd(token, rePwd);
		if (result.isSuccess()) {
			UserInfoVo user = (UserInfoVo) result.getReturnObj();
			if (user.getAccountInfo().getStatus() != AccountActiveStatus.Disabled.getValue()) {
				saveUserToSession(request, user);
			}
		}
		return ResponseUtils.sendMsg(result.getCode(), result.getMsg());
	}

	/**
	 * @description 同意条款
	 * @author hxzhang
	 * @create 2016年10月21日下午4:31:21
	 */
	@ResponseBody
	@RequestMapping(value = "/agree.do", method = RequestMethod.POST)
	public void saveAgree(HttpServletRequest request) {
		UserInfoVo currentUser = userService.saveAgree(getCurrentUser(request));
		saveUserToSession(request, currentUser);
	}

	/**
	 * @description 展示未签出提示
	 * @author hxzhang
	 * @create 2016年12月23日上午10:13:01
	 */
	@ResponseBody
	@RequestMapping(value = "/showNotSignOutTip.do", method = RequestMethod.POST)
	public Object showNotSignOutTip(String id) {
		ServiceResult<Object> result = userService.showNotSignOutTip(id);
		return ResponseUtils.sendMsg(result.getCode(), result.getMsg());
	}

}
