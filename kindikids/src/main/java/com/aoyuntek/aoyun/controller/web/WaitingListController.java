package com.aoyuntek.aoyun.controller.web;

import java.io.FileInputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.ContextLoader;

import com.aoyuntek.aoyun.entity.po.OutFamilyPo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.service.IWaitingListService;
import com.theone.string.util.StringUtil;

@Controller
@RequestMapping("/waitingList")
public class WaitingListController extends BaseWebController {
    @Autowired
    private IWaitingListService waitingListService;

    @ResponseBody
    @RequestMapping(value = "/importWaitingList.do", method = RequestMethod.GET)
    public Object importWaitingList(HttpServletRequest request, String cs, short excel) {
        String path;
        if (excel == 0) {
            path = ContextLoader.getCurrentWebApplicationContext().getServletContext().getRealPath("/") + "templet/excel/" + "one.xlsx";
        } else {
            path = ContextLoader.getCurrentWebApplicationContext().getServletContext().getRealPath("/") + "templet/excel/" + "twothree.xlsx";
        }
        try {
            List<OutFamilyPo> list = readExcel(path);
            waitingListService.importData(list, cs);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Success";
    }

    private List<OutFamilyPo> readExcel(String path) throws Exception {
        FileInputStream is = new FileInputStream(path);
        Workbook wb = new XSSFWorkbook(is);
        Sheet sheet = wb.getSheetAt(0);
        int totalRows = sheet.getPhysicalNumberOfRows();
        int totalCells = sheet.getRow(0).getPhysicalNumberOfCells();
        Row row = sheet.getRow(0);
        List<String> strs = new ArrayList<String>();
        for (int i = 0; i < totalCells; i++) {
            Cell c = row.getCell(i);
            System.err.println(i);
            String str = c.getStringCellValue();
            strs.add(str);
        }
        System.err.println(strs.size());
        System.err.println(strs.toString());
        List<OutFamilyPo> list = new ArrayList<OutFamilyPo>();
        for (int r = 1; r < totalRows; r++) {
            OutFamilyPo outFamily = new OutFamilyPo();
            UserInfo child = new UserInfo();
            List<UserInfo> parentList = new ArrayList<UserInfo>();
            UserInfo parent1 = new UserInfo();
            UserInfo parent2 = new UserInfo();
            Row rr = sheet.getRow(r);
            for (int t = 0; t < strs.size(); t++) {
                Cell cc = rr.getCell(t);
                if (cc == null) {
                    continue;
                }
                String value = "";
                System.err.println(t);
                switch (cc.getCellType()) {
                case HSSFCell.CELL_TYPE_STRING:
                    value = cc.getStringCellValue();
                    break;
                case HSSFCell.CELL_TYPE_NUMERIC:
                    if (strs.get(t).equals("time")) {
                        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                        value = sdf.format(cc.getDateCellValue());
                        value = "05/02/2017 " + value;
                        break;
                    }
                    DecimalFormat df = new DecimalFormat("0");
                    value = df.format(cc.getNumericCellValue());
                    break;
                case HSSFCell.CELL_TYPE_BOOLEAN:
                    value = String.valueOf(cc.getBooleanCellValue());
                    break;
                case HSSFCell.CELL_TYPE_BLANK:
                    value = "";
                    break;
                default:
                    value = "";
                    break;
                }
                try {
                    initOutFamilyPo(outFamily, strs.get(t), value, child, parent1, parent2);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            parentList.add(parent1);
            parentList.add(parent2);
            outFamily.setChild(child);
            outFamily.setParentList(parentList);
            list.add(outFamily);
        }
        System.err.println(list.size());
        return list;
    }

    private void initOutFamilyPo(OutFamilyPo outFamily, String title, String value, UserInfo child, UserInfo parent1, UserInfo parent2)
            throws Exception {

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        switch (title) {
        case "Child First Name":
            child.setFirstName(value);
            break;
        case "Child Last Name":
            child.setLastName(value);
            break;
        case "Child Dob":
            child.setBirthday(sdf.parse(value));
            break;
        case "Enrolment Start":
            if (StringUtil.isNotEmpty(value)) {
                outFamily.setChangeDate(sdf.parse(value));
            }
            break;
        case "Parent 1 First Name":
            parent1.setFirstName(value);
            break;
        case "Parent 1 Last Name":
            parent1.setLastName(value);
            outFamily.setFamilyName(value);
            break;
        case "Parent 1 Mobile":
            parent1.setMobileNumber(value);
            break;
        case "Parent 1 Dob":
            parent1.setBirthday(sdf.parse(value));
            break;
        case "Parent 1 Home Phone":
            parent1.setPhoneNumber(value);
            break;
        case "Parent 1 Place Of Work":
            parent1.setPlaceOfWork(value);
            break;
        case "Parent 1 Work Phone":
            parent1.setWorkPhoneNumber(value);
            break;
        case "Parent 1 Address":
            parent1.setAddress(value);
            break;
        case "Parent 1 Email":
            parent1.setEmail(value);
            break;
        case "Parent 1 Occupation":
            parent1.setOccupation(value);
            break;
        case "Parent 1 Languages Spoken At Home":
            parent1.setLanguagesAtHome(value);
            break;
        case "Parent 2 First Name":
            parent2.setFirstName(value);
            break;
        case "Parent 2 Last Name":
            parent2.setLastName(value);
            break;
        case "Parent 2 Mobile":
            parent2.setMobileNumber(value);
            break;
        case "Parent 2 Dob":
            if (StringUtil.isNotEmpty(value)) {
                parent2.setBirthday(sdf.parse(value));
            }
            break;
        case "Parent 2 Home Phone":
            parent2.setPhoneNumber(value);
            break;
        case "Parent 2 Place Of Work":
            parent2.setPlaceOfWork(value);
            break;
        case "Parent 2 Work Phone":
            parent2.setWorkPhoneNumber(value);
            break;
        case "Parent 2 Address":
            parent2.setAddress(value);
            break;
        case "Parent 2 Email":
            parent2.setEmail(value);
            break;
        case "Parent 2 Occupation":
            parent2.setOccupation(value);
            break;
        case "Parent 2 Languages Spoken At Home":
            parent2.setLanguagesAtHome(value);
            break;
        case "Waitlist Preference Of Days":
            if (StringUtil.isNotEmpty(value)) {
                if (value.equals("Any days")) {
                    outFamily.setMonday(true);
                    outFamily.setTuesday(true);
                    outFamily.setWednesday(true);
                    outFamily.setThursday(true);
                    outFamily.setFriday(true);
                }
                String str = value.replace(" ", "");
                String[] strs = str.split("/");
                for (String s : strs) {
                    switch (s) {
                    case "Monday":
                        outFamily.setMonday(true);
                        break;
                    case "Tuesday":
                        outFamily.setTuesday(true);
                        break;
                    case "Wednesday":
                        outFamily.setWednesday(true);
                        break;
                    case "Thursday":
                        outFamily.setThursday(true);
                        break;
                    case "Friday":
                        outFamily.setFriday(true);
                        break;
                    default:
                        break;
                    }
                }
            }
            break;
        case "Family":
            outFamily.setOldId(value);
            break;
        case "time":
            SimpleDateFormat ss = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
            outFamily.setTime(ss.parse(value));
            break;

        default:
            break;
        }
    }

}
