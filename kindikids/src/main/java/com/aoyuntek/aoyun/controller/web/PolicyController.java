package com.aoyuntek.aoyun.controller.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aoyuntek.aoyun.condtion.PolicyCondition;
import com.aoyuntek.aoyun.constants.DateFormatterConstants;
import com.aoyuntek.aoyun.entity.po.policy.PolicyCategoriesInfo;
import com.aoyuntek.aoyun.entity.vo.policy.PolicyInfoVo;
import com.aoyuntek.aoyun.service.policy.IPolicyService;
import com.aoyuntek.framework.model.Pager;
import com.google.gson.reflect.TypeToken;
import com.theone.common.util.ServiceResult;
import com.theone.json.util.JsonUtil;
import com.theone.web.util.ResponseUtils;

@Controller
@RequestMapping(value = "/policy")
public class PolicyController extends BaseWebController {

    /**
     * 
     */
    @Autowired
    private IPolicyService policyService;

    @ResponseBody
    @RequestMapping("/edit.do")
    public Object edit(HttpServletRequest request) {
        String jsonStr = ServletRequestUtils.getStringParameter(request, "params", "");
        PolicyInfoVo policyInfoVo = (PolicyInfoVo) JsonUtil.jsonToBean(jsonStr, PolicyInfoVo.class, DateFormatterConstants.SYS_T_FORMAT);
        ServiceResult<Object> result = policyService.addOrUpdatePolicy(policyInfoVo, getCurrentUser(request));
        return ResponseUtils.sendMsg(result.isSuccess() ? 0 : -1, result.getMsg(), result.getReturnObj());
    }

    @ResponseBody
    @RequestMapping("/deletePolicy.do")
    public Object deletePolicy(HttpServletRequest request, String policyId) {
        ServiceResult<Object> result = policyService.deletePolicy(policyId);
        return ResponseUtils.sendMsg(result.isSuccess() ? 0 : -1, result.getMsg(), result.getReturnObj());
    }

    @ResponseBody
    @RequestMapping("/existDeletePolicy.do")
    public Object existDeletePolicy(HttpServletRequest request, String policyCategoriesId) {
        ServiceResult<Object> result = policyService.existDeletePolicy(policyCategoriesId);
        return ResponseUtils.sendMsg(result.isSuccess() ? 0 : -1, result.getMsg(), result.getReturnObj());
    }

    @ResponseBody
    @RequestMapping("/editCategory.do")
    public Object editCategory(HttpServletRequest request) {
        String jsonStr = ServletRequestUtils.getStringParameter(request, "params", "");
        List<PolicyCategoriesInfo> PolicyCategoriesInfos = JsonUtil.jsonToList(jsonStr, new TypeToken<List<PolicyCategoriesInfo>>() {
        }.getType(), DateFormatterConstants.SYS_T_FORMAT);
        ServiceResult<Object> result = policyService.addOrUpdatePolicyCategory(PolicyCategoriesInfos, getCurrentUser(request));
        return ResponseUtils.sendMsg(result.isSuccess() ? 0 : -1, result.getMsg(), result.getReturnObj());
    }

    @ResponseBody
    @RequestMapping("/info.do")
    public Object info(HttpServletRequest request, String id) {

        ServiceResult<PolicyInfoVo> result = policyService.getPolicyInfo(id, getCurrentUser(request));
        return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
    }

    @ResponseBody
    @RequestMapping("/list.do")
    public Object list(HttpServletRequest request) {
        String jsonStr = ServletRequestUtils.getStringParameter(request, "params", "");
        PolicyCondition condition = (PolicyCondition) JsonUtil.jsonToBean(jsonStr, PolicyCondition.class, DateFormatterConstants.SYS_T_FORMAT);
        ServiceResult<Pager<PolicyInfoVo, PolicyCondition>> result = policyService.getPolicyList(condition, getCurrentUser(request));
        return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
    }

    @ResponseBody
    @RequestMapping("/getCategoriesSrcList.do")
    public Object getCategoriesSrcList(HttpServletRequest request) {

        ServiceResult<List<PolicyCategoriesInfo>> result = policyService.getCategoriesSrcList();
        return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
    }

}
