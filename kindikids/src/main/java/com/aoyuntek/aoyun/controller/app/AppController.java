package com.aoyuntek.aoyun.controller.app;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 手机端
 * 
 * @author dlli5 at 2016年4月25日下午1:41:32
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
@Controller
@RequestMapping("/app")
public class AppController extends BaseAppController {

	
}
