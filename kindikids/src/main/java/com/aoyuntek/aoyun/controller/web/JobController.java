package com.aoyuntek.aoyun.controller.web;

import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aoyuntek.aoyun.constants.SystemConstants;
import com.aoyuntek.aoyun.dao.CalendarInfoMapper;
import com.aoyuntek.aoyun.entity.po.AccountInfo;
import com.aoyuntek.aoyun.entity.po.CalendarInfo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.enums.TaskType;
import com.aoyuntek.aoyun.service.ICenterService;
import com.aoyuntek.aoyun.service.IFamilyService;
import com.aoyuntek.aoyun.service.IHubworksService;
import com.aoyuntek.aoyun.service.IJobService;
import com.aoyuntek.aoyun.service.IMsgEmailInfoService;
import com.aoyuntek.aoyun.service.IPayrollService;
import com.aoyuntek.aoyun.service.IStaffService;
import com.aoyuntek.aoyun.service.IUserInfoService;
import com.aoyuntek.aoyun.service.IWeekNutrionalMenuService;
import com.aoyuntek.aoyun.service.attendance.IAbsenteeService;
import com.aoyuntek.aoyun.service.attendance.IGivingNoticeInfoService;
import com.aoyuntek.aoyun.service.attendance.ILeaveService;
import com.aoyuntek.aoyun.service.attendance.ISignService;
import com.aoyuntek.aoyun.service.meeting.IMeetingTempService;
import com.aoyuntek.aoyun.service.program.IProgramIntobsleaService;
import com.aoyuntek.aoyun.service.program.IProgramLessonService;
import com.aoyuntek.aoyun.service.program.IProgramMedicationService;
import com.aoyuntek.aoyun.service.program.IProgramRegisterTaskService;
import com.aoyuntek.aoyun.service.program.IProgramTaskExgrscService;
import com.aoyuntek.aoyun.service.program.ITaskProgramFollowUpService;
import com.aoyuntek.aoyun.service.program.IWeeklyEvaluationService;
import com.aoyuntek.aoyun.service.roster.IRosterService;
import com.aoyuntek.framework.annotation.UserAuthority;
import com.theone.date.util.DateUtil;
import com.theone.string.util.StringUtil;
import com.theone.web.util.ResponseUtils;

/**
 * 
 * @description 定时任务手动执行控制器
 * @author gfwang
 * @create 2016年8月24日下午7:58:16
 * @version 1.0
 */
@Controller
@RequestMapping("/job")
public class JobController extends BaseWebController {
	@Autowired
	private ILeaveService leaveService;
	@Autowired
	private IFamilyService familyService;
	@Autowired
	private IGivingNoticeInfoService gnService;
	@Autowired
	private IAbsenteeService abService;
	@Autowired
	private ICenterService centerService;
	@Autowired
	private IJobService jobService;
	@Autowired
	private IStaffService staffService;
	@Autowired
	private ISignService signService;
	@Autowired
	private IProgramTaskExgrscService programTaskExgrscService;
	@Autowired
	private ITaskProgramFollowUpService taskProgramFollowUpService;
	@Autowired
	private IWeeklyEvaluationService weeklyEvaluationService;
	@Autowired
	private IUserInfoService userInfoService;
	@Autowired
	private IProgramRegisterTaskService registerTaskService;
	@Autowired
	private IProgramLessonService programLessonService;
	@Autowired
	private IProgramIntobsleaService programIntobsleaService;
	@Autowired
	private IProgramMedicationService programMedicationService;
	@Autowired
	private IMeetingTempService meetingTempService;
	@Autowired
	private IProgramRegisterTaskService programRegisterTaskService;
	@Autowired
	private IAbsenteeService absenteeService;
	@Autowired
	private IUserInfoService userService;
	@Autowired
	private IWeekNutrionalMenuService weekNutrionalMenuService;
	@Autowired
	private IRosterService rosterService;
	@Autowired
	private IPayrollService payrollService;
	@Autowired
	private IHubworksService hubworksService;

	/**
	 * 日志
	 */
	private Logger logger = Logger.getLogger(JobController.class);

	@UserAuthority(roles = { Role.CEO })
	@ResponseBody
	@RequestMapping(value = "/dealStaffSigninJob.do", method = RequestMethod.GET)
	public Object dealStaffSigninJob(HttpServletRequest request, String date) throws ParseException {
		logger.info("dealRosterJob|start|date=" + date);
		if (!mustInput(date)) {
			return sendFailResponse(getTipMsg("task.date.empty"));
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date now = sdf.parse(date);
		if (DateUtil.getPreviousDay(now) >= DateUtil.getPreviousDay(new Date())) {
			return "date error";
		}
		jobService.dealStaffSigninJob(logger, now, getCurrentUser(request));
		return ResponseUtils.sendMsg(true);
	}

	@UserAuthority(roles = { Role.CEO })
	@ResponseBody
	@RequestMapping(value = "/dealRosterJob.do", method = RequestMethod.GET)
	public Object dealRosterJob(HttpServletRequest request, String date) throws ParseException {
		logger.info("dealRosterJob|start|date=" + date);
		if (!mustInput(date)) {
			return sendFailResponse(getTipMsg("task.date.empty"));
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date now = sdf.parse(date);
		if (DateUtil.getPreviousDay(now) > DateUtil.getPreviousDay(new Date())) {
			return "date error";
		}
		jobService.dealRosterJob(logger, now, getCurrentUser(request));
		return ResponseUtils.sendMsg(true);
	}

	@UserAuthority(roles = { Role.CEO })
	@ResponseBody
	@RequestMapping(value = "/dealSubtractedAttendanceJob.do", method = RequestMethod.GET)
	public Object dealSubtractedAttendanceJob(HttpServletRequest request, String date) throws ParseException {
		logger.info("dealSubtractedAttendanceJob|start|date=" + date);
		if (!mustInput(date)) {
			return sendFailResponse(getTipMsg("task.date.empty"));
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date now = sdf.parse(date);
		if (DateUtil.getPreviousDay(now) > DateUtil.getPreviousDay(new Date())) {
			return "date error";
		}
		jobService.dealSubtractedAttendanceJob(logger, now);
		return ResponseUtils.sendMsg(true);
	}

	@UserAuthority(roles = { Role.CEO })
	@ResponseBody
	@RequestMapping(value = "/dealAttendanceRequestJob.do", method = RequestMethod.GET)
	public Object dealAttendanceRequestJob(HttpServletRequest request, String date) throws ParseException {
		logger.info("dealAttendanceRequestJob|start|date=" + date);
		if (!mustInput(date)) {
			return sendFailResponse(getTipMsg("task.date.empty"));
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date now = null;
		if (StringUtil.isEmpty(date)) {
			now = new Date();
		} else {
			now = sdf.parse(date);
		}
		if (DateUtil.getPreviousDay(now) > DateUtil.getPreviousDay(new Date())) {
			return "date error";
		}
		jobService.dealAttendanceRequestJob(logger, now, getCurrentUser(request));
		return ResponseUtils.sendMsg(1);
	}

	@UserAuthority(roles = { Role.CEO })
	@ResponseBody
	@RequestMapping(value = "/dealEnrolledJob.do", method = RequestMethod.GET)
	public Object dealEnrolledJob(HttpServletRequest request, String date) throws ParseException {
		logger.info("dealEnrolledJob|start|date=" + date);
		if (!mustInput(date)) {
			return sendFailResponse(getTipMsg("task.date.empty"));
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date now = null;
		if (StringUtil.isEmpty(date)) {
			now = new Date();
		} else {
			now = sdf.parse(date);
		}
		// if (DateUtil.getPreviousDay(now) > DateUtil.getPreviousDay(new
		// Date())) {
		// return "date error";
		// }
		jobService.dealEnrolledJob(logger, now);
		return ResponseUtils.sendMsg(true);
	}

	@UserAuthority(roles = { Role.CEO })
	@ResponseBody
	@RequestMapping(value = "/dealTaskJobOfCustmer.do", method = RequestMethod.GET)
	public Object dealTaskJobOfCustmer(HttpServletRequest request, String date) throws Exception {
		logger.info("dealTaskJobOfCustmer|start|date=" + date);
		if (!mustInput(date)) {
			return sendFailResponse(getTipMsg("task.date.empty"));
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date now = null;
		if (StringUtil.isEmpty(date)) {
			now = new Date();
		} else {
			now = sdf.parse(date);
		}
		if (DateUtil.getPreviousDay(now) > DateUtil.getPreviousDay(new Date())) {
			return "date error";
		}
		jobService.dealTaskJobOfCustmer(logger, now);
		return ResponseUtils.sendMsg(true);
	}

	@UserAuthority(roles = { Role.CEO })
	@ResponseBody
	@RequestMapping(value = "/dealOverDueTaskJobOfCustmer.do", method = RequestMethod.GET)
	public Object dealOverDueTaskJobOfCustmer(HttpServletRequest request, String date) throws Exception {
		logger.info("dealOverDueTaskJobOfCustmer|start|date=" + date);
		if (!mustInput(date)) {
			return sendFailResponse(getTipMsg("task.date.empty"));
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date now = null;
		if (StringUtil.isEmpty(date)) {
			now = new Date();
		} else {
			now = sdf.parse(date);
		}
		if (DateUtil.getPreviousDay(now) >= DateUtil.getPreviousDay(new Date())) {
			return "date error";
		}
		jobService.dealOverDueTaskJobOfCustmer(logger, now);
		return ResponseUtils.sendMsg(true);
	}

	@UserAuthority(roles = { Role.CEO })
	@ResponseBody
	@RequestMapping(value = "/dealSigninJob.do", method = RequestMethod.GET)
	public Object dealSigninJob(HttpServletRequest request, String date) throws ParseException {
		logger.info("dealSigninJob|start|date=" + date);
		if (!mustInput(date)) {
			return sendFailResponse(getTipMsg("task.date.empty"));
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date now = null;
		if (StringUtil.isEmpty(date)) {
			now = new Date();
		} else {
			now = sdf.parse(date);
		}
		if (DateUtil.getPreviousDay(now) >= DateUtil.getPreviousDay(new Date())) {
			return "date error";
		}
		jobService.dealChildSigninJob(logger, now);
		return ResponseUtils.sendMsg(true);
	}

	/**
	 * @description 员工请假过期更新为失效状态
	 * @author hxzhang
	 * @create 2016年9月7日下午11:15:22
	 * @version 1.0
	 * @param request
	 * @param date
	 * @return
	 */
	@UserAuthority(roles = { Role.CEO })
	@ResponseBody
	@RequestMapping(value = "/updateLeaveTimed.do", method = RequestMethod.GET)
	public Object testLeave(HttpServletRequest request, String date) {
		logger.info("testLeave|start|date=" + date);
		if (!mustInput(date)) {
			return sendFailResponse(getTipMsg("task.date.empty"));
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date now = null;
			if (StringUtil.isEmpty(date)) {
				now = new Date();
			} else {
				now = sdf.parse(date);
			}
			if (DateUtil.getPreviousDay(now) > DateUtil.getPreviousDay(new Date())) {
				return "date error";
			}
			leaveService.updateLeaveTimedTask(now);
		} catch (ParseException e) {
			logger.error("ParseException", e);
			e.printStackTrace();
		}
		return "success";
	}

	/**
	 * @description 小孩归档一年后归档家长
	 * @author hxzhang
	 * @create 2016年9月7日下午11:14:42
	 * @version 1.0
	 * @param request
	 * @param date
	 * @return
	 */
	@UserAuthority(roles = { Role.CEO })
	@ResponseBody
	@RequestMapping(value = "/updateFamilyTimed.do", method = RequestMethod.GET)
	public Object testParent(HttpServletRequest request, String date) {
		logger.info("testParent|start|date=" + date);
		if (!mustInput(date)) {
			return sendFailResponse(getTipMsg("task.date.empty"));
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date now = null;
			if (StringUtil.isEmpty(date)) {
				now = new Date();
			} else {
				now = sdf.parse(date);
			}
			if (DateUtil.getPreviousDay(now) > DateUtil.getPreviousDay(new Date())) {
				return "date error";
			}
			familyService.updateFamilyTimedTask(now);
		} catch (ParseException e) {
			logger.error("testParent", e);
			e.printStackTrace();
		}
		return "success";
	}

	/**
	 * 
	 * @description 定时发送菜单
	 * @author mingwang
	 * @create 2016年8月24日下午3:29:01
	 * @version 1.0
	 * @param request
	 * @return
	 */
	@UserAuthority(roles = { Role.CEO })
	@ResponseBody
	@RequestMapping(value = "/sendMenu.do", method = RequestMethod.GET)
	public Object sendMenu(HttpServletRequest request, String date) {
		logger.info("sendMenu|start|date=" + date);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date now = null;
			if (StringUtil.isEmpty(date)) {
				now = new Date();
			} else {
				now = sdf.parse(date);
			}
			if (DateUtil.getPreviousDay(now) > DateUtil.getPreviousDay(new Date())) {
				return "date error";
			}
			// centerService.sendTodaysMenu(now);
			weekNutrionalMenuService.sendMenu(now);
		} catch (ParseException e) {
			log.error("sendMenu|ParseException", e);
			e.printStackTrace();
		}
		return "success";
	}

	/**
	 * 
	 * @description 定时发送小孩及员工生日newsfeed
	 * @author abliu
	 * @create 2016年9月26日下午3:29:01
	 * @version 1.0
	 * @param request
	 * @return
	 */
	@UserAuthority(roles = { Role.CEO })
	@ResponseBody
	@RequestMapping(value = "/sendBirthday.do", method = RequestMethod.GET)
	public Object sendBirthday(HttpServletRequest request, String date) {
		logger.info("sendBirthday|start|date=" + date);
		if (!mustInput(date)) {
			return sendFailResponse(getTipMsg("task.date.empty"));
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date now = null;
			if (StringUtil.isEmpty(date)) {
				now = new Date();
			} else {
				now = sdf.parse(date);
			}
			if (DateUtil.getPreviousDay(now) > DateUtil.getPreviousDay(new Date())) {
				return "date error";
			}
			userInfoService.sendUserNewsFeedByBirthday(now);
		} catch (ParseException e) {
			log.error("sendMenu|ParseException", e);
			e.printStackTrace();
		}
		return "success";
	}

	/**
	 * @description 请假到期将请假更新为失效
	 * @author hxzhang
	 * @create 2016年8月28日下午6:01:25
	 * @version 1.0
	 * @param request
	 * @param date
	 * @return
	 */
	@UserAuthority(roles = { Role.CEO })
	@ResponseBody
	@RequestMapping(value = "/testAbsentee.do", method = RequestMethod.GET)
	public Object testAbsenteeStatus(HttpServletRequest request, String date) {
		logger.info("testAbsenteeStatus|start|date=" + date);
		if (!mustInput(date)) {
			return sendFailResponse(getTipMsg("task.date.empty"));
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date now = null;
			if (StringUtil.isEmpty(date)) {
				now = new Date();
			} else {
				now = sdf.parse(date);
			}
			if (DateUtil.getPreviousDay(now) > DateUtil.getPreviousDay(new Date())) {
				return "date error";
			}
			abService.updateAbsenteeStatusTimedTask(now);
		} catch (ParseException e) {
			logger.error("testAbsenteeStatus", e);
			e.printStackTrace();
		}
		return "success";
	}

	/**
	 * @description 每七天发送给家长激活邮件
	 * @author hxzhang
	 * @create 2016年8月28日下午6:01:25
	 * @version 1.0
	 * @param request
	 * @param date
	 * @return
	 */
	@UserAuthority(roles = { Role.CEO })
	@ResponseBody
	@RequestMapping(value = "/testUnActive.do", method = RequestMethod.GET)
	public Object testUnActive(HttpServletRequest request, String date) {
		logger.info("testUnActive|start|date=" + date);
		if (!mustInput(date)) {
			return sendFailResponse(getTipMsg("task.date.empty"));
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date now = null;
			if (StringUtil.isEmpty(date)) {
				now = new Date();
			} else {
				now = sdf.parse(date);
			}
			if (DateUtil.getPreviousDay(now) > DateUtil.getPreviousDay(new Date())) {
				return "date error";
			}
			familyService.sendActiveEmailTimedTask(now);
		} catch (ParseException e) {
			logger.error("testUnActive", e);
			e.printStackTrace();
		}
		return "success";
	}

	/**
	 * @description 测试每七天给员工发送激活邮件
	 * @author hxzhang
	 * @create 2016年9月6日下午6:59:25
	 * @version 1.0
	 * @param request
	 * @param date
	 * @return
	 */
	@UserAuthority(roles = { Role.CEO })
	@ResponseBody
	@RequestMapping(value = "/testStaff.do", method = RequestMethod.GET)
	public Object testStaff(HttpServletRequest request, String date) {
		logger.info("testStaff|start|date=" + date);
		if (!mustInput(date)) {
			return sendFailResponse(getTipMsg("task.date.empty"));
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date now = null;
			if (StringUtil.isEmpty(date)) {
				now = new Date();
			} else {
				now = sdf.parse(date);
			}
			if (DateUtil.getPreviousDay(now) > DateUtil.getPreviousDay(new Date())) {
				return "date error";
			}
			staffService.sendActiveEmailTimedTask(now);
		} catch (ParseException e) {
			logger.error("testStaff", e);
			e.printStackTrace();
		}
		return "success";
	}

	/**
	 * @description 离园申请到期将未审批的申请更新为失效
	 * @author hxzhang
	 * @create 2016年9月7日下午8:29:58
	 * @version 1.0
	 * @param request
	 * @param date
	 * @return
	 */
	@UserAuthority(roles = { Role.CEO })
	@ResponseBody
	@RequestMapping(value = "/testGivingNotice.do", method = RequestMethod.GET)
	public Object testGivingNoticeStatus(HttpServletRequest request, String date) {
		logger.info("testGivingNoticeStatus|start|date=" + date);
		if (!mustInput(date)) {
			return sendFailResponse(getTipMsg("task.date.empty"));
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date now = null;
			if (StringUtil.isEmpty(date)) {
				now = new Date();
			} else {
				now = sdf.parse(date);
			}
			// if (DateUtil.getPreviousDay(now) > DateUtil.getPreviousDay(new
			// Date())) {
			// return "date error";
			// }
			gnService.updateGivingNoticeStatusTimedTask(now);
		} catch (ParseException e) {
			logger.error("testGivingNoticeStatus", e);
			e.printStackTrace();
		}
		return "success";
	}

	/**
	 * @description 测试小孩在18:00有未签出的给CEO,园长和二级园长发送短信
	 * @author hxzhang
	 * @create 2016年9月19日下午5:34:06
	 * @version 1.0
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/testMsg.do", method = RequestMethod.GET)
	public Object testUnSignoutChild(HttpServletRequest request, String date) {
		logger.info("testGivingNoticeStatus|start|date=" + date);
		if (!mustInput(date)) {
			return sendFailResponse(getTipMsg("task.date.empty"));
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date now = null;
			if (StringUtil.isEmpty(date)) {
				now = new Date();
			} else {
				now = sdf.parse(date);
			}
			if (DateUtil.getPreviousDay(now) > DateUtil.getPreviousDay(new Date())) {
				return "date error";
			}
			signService.sendMsgByChildUnSignoutTimedTask(now);
		} catch (ParseException e) {
			logger.error("testGivingNoticeStatus", e);
			e.printStackTrace();
		}
		return "success";
	}

	/**
	 * @description 每天生成Explore Curriculum,Grow Curriculum,School Readiness
	 * @author Hxzhang
	 * @create 2016年9月25日下午9:17:47
	 */
	@ResponseBody
	@RequestMapping(value = "/testTaskExgrsc.do", method = RequestMethod.GET)
	public Object testTaskExgrsc(HttpServletRequest request, String date) {
		logger.info("testTaskExgrsc|start|date=" + date);
		if (!mustInput(date)) {
			return sendFailResponse(getTipMsg("task.date.empty"));
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date now = null;
			if (StringUtil.isEmpty(date)) {
				now = new Date();
			} else {
				now = sdf.parse(date);
			}
			// if (DateUtil.getPreviousDay(now) > DateUtil.getPreviousDay(new
			// Date())) {
			// return "date error";
			// }
			programTaskExgrscService.createProgramTaskExgrscTimedTask(now);
		} catch (ParseException e) {
			logger.error("testTaskExgrsc", e);
			e.printStackTrace();
		}
		return "success";
	}

	/**
	 * @description TaskExgrsc过期测试
	 * @author hxzhang 2016年9月25日下午9:10:50
	 * @param request
	 * @param date
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/testOverDueTaskExgrsc.do", method = RequestMethod.GET)
	public Object testOverDueTaskExgrsc(HttpServletRequest request, String date) {
		logger.info("testTaskExgrsc|start|date=" + date);
		if (!mustInput(date)) {
			return sendFailResponse(getTipMsg("task.date.empty"));
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date now = null;
			if (StringUtil.isEmpty(date)) {
				now = new Date();
			} else {
				now = sdf.parse(date);
			}
			if (DateUtil.getPreviousDay(now) > DateUtil.getPreviousDay(new Date())) {
				return "date error";
			}
			programTaskExgrscService.updateOverDueProgramTaskExgrscTimedTask(now);
		} catch (ParseException e) {
			logger.error("testOverDueTaskExgrsc", e);
			e.printStackTrace();
		}
		return "success";
	}

	/**
	 * @description 自动创建满足条件followup
	 * @author hxzhang
	 * @create 2016年9月25日下午5:37:21
	 * @version 1.0
	 * @param request
	 * @param date
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/testFollowup.do", method = RequestMethod.GET)
	public Object testFollowup(HttpServletRequest request, String date) {
		logger.info("testFollowup|start|date=" + date);
		if (!mustInput(date)) {
			return sendFailResponse(getTipMsg("task.date.empty"));
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date now = null;
			if (StringUtil.isEmpty(date)) {
				now = new Date();
			} else {
				now = sdf.parse(date);
			}
			// if (DateUtil.getPreviousDay(now) > DateUtil.getPreviousDay(new
			// Date())) {
			// return "date error";
			// }
			taskProgramFollowUpService.createProgramFollowupTimedTask(now);
		} catch (ParseException e) {
			logger.error("testFollowup", e);
			e.printStackTrace();
		}
		return "success";
	}

	/**
	 * @description Followup过期测试
	 * @author hxzhang 2016年9月25日下午9:12:10
	 * @param request
	 * @param date
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/testOverDueFollowup.do", method = RequestMethod.GET)
	public Object testOverDueFollowup(HttpServletRequest request, String date) {
		logger.info("testFollowup|start|date=" + date);
		if (!mustInput(date)) {
			return sendFailResponse(getTipMsg("task.date.empty"));
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date now = null;
			if (StringUtil.isEmpty(date)) {
				now = new Date();
			} else {
				now = sdf.parse(date);
			}
			if (DateUtil.getPreviousDay(now) > DateUtil.getPreviousDay(new Date())) {
				return "date error";
			}
			taskProgramFollowUpService.updateOverDueProgramFollowupTimedTask(now);
		} catch (ParseException e) {
			logger.error("testOverDueFollowup", e);
			e.printStackTrace();
		}
		return "success";
	}

	/**
	 * @description 周五生成Weekly
	 * @author hxzhang
	 * @create 2016年9月25日下午6:43:07
	 * @version 1.0
	 * @param request
	 * @param date
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/testWeekly.do", method = RequestMethod.GET)
	public Object testWeekly(HttpServletRequest request, String date) {
		logger.info("testFollowup|start|date=" + date);
		if (!mustInput(date)) {
			return sendFailResponse(getTipMsg("task.date.empty"));
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date now = null;
			if (StringUtil.isEmpty(date)) {
				now = new Date();
			} else {
				now = sdf.parse(date);
			}
			if (DateUtil.getPreviousDay(now) > DateUtil.getPreviousDay(new Date())) {
				return "date error";
			}
			weeklyEvaluationService.createWeeklyEvaluationOnFridayTimedTask(now);
		} catch (ParseException e) {
			logger.error("testWeekly", e);
			e.printStackTrace();
		}
		return "success";
	}

	/**
	 * @description Weekly过期测试
	 * @author hxzhang
	 * @create 2016年9月25日下午9:15:57
	 */
	@ResponseBody
	@RequestMapping(value = "/testOverDueWeekly.do", method = RequestMethod.GET)
	public Object testOverDueWeekly(HttpServletRequest request, String date) {
		logger.info("testFollowup|start|date=" + date);
		if (!mustInput(date)) {
			return sendFailResponse(getTipMsg("task.date.empty"));
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date now = null;
			if (StringUtil.isEmpty(date)) {
				now = new Date();
			} else {
				now = sdf.parse(date);
			}
			if (DateUtil.getPreviousDay(now) > DateUtil.getPreviousDay(new Date())) {
				return "date error";
			}
			weeklyEvaluationService.updateOverDueWeeklyEvaluationTimedTask(now);
		} catch (ParseException e) {
			logger.error("testOverDueWeekly", e);
			e.printStackTrace();
		}
		return "success";
	}

	/**
	 * 
	 * @description lesson过期测试
	 * @author mingwang
	 * @create 2016年9月28日上午9:57:15
	 * @version 1.0
	 * @param request
	 * @param date
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/testOverDueLesson.do", method = RequestMethod.GET)
	public Object testOverDueLesson(HttpServletRequest request, String date) {
		logger.info("testOverDueLesson|start|date=" + date);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date now = null;
			if (StringUtil.isEmpty(date)) {
				now = new Date();
			} else {
				now = sdf.parse(date);
			}
			if (DateUtil.getPreviousDay(now) > DateUtil.getPreviousDay(new Date())) {
				return "date error";
			}
			programLessonService.updateOverDueLessonTimedTask(now);
		} catch (ParseException e) {
			logger.error("testOverDueLesson", e);
			e.printStackTrace();
		}
		return "success";
	}

	/**
	 * 
	 * @description Intobslea过期测试
	 * @author mingwang
	 * @create 2016年9月28日上午9:57:15
	 * @version 1.0
	 * @param request
	 * @param date
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/testOverDueIntobslea.do", method = RequestMethod.GET)
	public Object testOverDueIntobslea(HttpServletRequest request, String date) {
		logger.info("testOverDueIntobslea|start|date=" + date);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date now = null;
			if (StringUtil.isEmpty(date)) {
				now = new Date();
			} else {
				now = sdf.parse(date);
			}
			if (DateUtil.getPreviousDay(now) > DateUtil.getPreviousDay(new Date())) {
				return "date error";
			}
			programIntobsleaService.updateOverDueIntobsleaTimedTask(now);
		} catch (ParseException e) {
			logger.error("testOverDueInsobslea", e);
			e.printStackTrace();
		}
		return "success";
	}

	/**
	 * 
	 * @description 自动生成定时任务Register
	 * @author mingwang
	 * @create 2016年9月25日下午1:57:28
	 * @version 1.0
	 * @param request
	 * @param date
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/createRegisterTask.do", method = RequestMethod.GET)
	public Object createRegisterTask(HttpServletRequest request, String date, short taskType) {
		log.info("creatRegisterTask|start");
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date now = null;
			if (StringUtil.isEmpty(date)) {
				now = new Date();
			} else {
				now = sdf.parse(URLDecoder.decode(date));
			}
			registerTaskService.createTimingTask(now, taskType);
		} catch (ParseException e) {
			logger.error("createRegisterTask", e);
			e.printStackTrace();
			return "fail";
		}
		return "success";
	}

	/**
	 * 
	 * @description ProgramRegister过期测试
	 * @author mingwang
	 * @create 2016年9月28日下午2:03:19
	 * @version 1.0
	 * @param request
	 * @param date
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/testOverDueRegister.do", method = RequestMethod.GET)
	public Object testOverDueRegister(HttpServletRequest request, String date, short taskType) {
		logger.info("testOverDueIntobslea|start|date=" + date);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		try {
			Date now = null;
			if (StringUtil.isEmpty(date)) {
				now = new Date();
			} else {
				now = sdf.parse(URLDecoder.decode(date));
			}
			if (DateUtil.getPreviousDay(now) > DateUtil.getPreviousDay(new Date())) {
				return "date error";
			}
			registerTaskService.updateOverDueRegisterTimedTask(now, taskType);
		} catch (ParseException e) {
			logger.error("testOverDueRegister", e);
			e.printStackTrace();
		}
		return "success";
	}

	/**
	 * msgEmailService
	 */
	@Autowired
	private IMsgEmailInfoService msgEmailService;

	/**
	 * 
	 * @description Medication过期测试
	 * @author mingwang
	 * @create 2016年9月28日下午2:58:23
	 * @version 1.0
	 * @param request
	 * @param date
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/testOverMedication.do", method = RequestMethod.GET)
	public Object testOverMedication(HttpServletRequest request, String date) {
		logger.info("testOverMedication|start|date=" + date);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date now = null;
			if (StringUtil.isEmpty(date)) {
				now = new Date();
			} else {
				now = sdf.parse(date);
			}
			if (DateUtil.getPreviousDay(now) > DateUtil.getPreviousDay(new Date())) {
				return "date error";
			}
			programMedicationService.updateOverDueMedicationTimedTask(now);
		} catch (ParseException e) {
			logger.error("testOverMedication", e);
			e.printStackTrace();
		}
		return "success";
	}

	@RequestMapping(value = "/testEmail.do", method = RequestMethod.GET)
	@ResponseBody
	public Object testEmail() {
		try {
			msgEmailService.sendMsg();
		} catch (Exception e) {
			logger.error("testEmail", e);
			return "fail";
		}
		return "success";
	}

	@RequestMapping(value = "/testMeetingTemp.do", method = RequestMethod.GET)
	@ResponseBody
	public Object testMeetingTemp(HttpServletRequest request, String date) {
		logger.info("testMeetingTemp|start|date=" + date);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date now = null;
			if (StringUtil.isEmpty(date)) {
				now = new Date();
			} else {
				now = sdf.parse(date);
			}
			// if (DateUtil.getPreviousDay(now) > DateUtil.getPreviousDay(new
			// Date())) {
			// return "date error";
			// }
			meetingTempService.createMeetingTempForMeetingTimeTask(now);
		} catch (ParseException e) {
			logger.error("testMeetingTemp", e);
			e.printStackTrace();
		}
		return "success";
	}

	@RequestMapping(value = "/testDepositTask.do", method = RequestMethod.GET)
	@ResponseBody
	public Object testcreateDepositTask(HttpServletRequest request, String date) {
		logger.info("testcreateDepositTask|start|date=" + date);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date now = null;
			if (StringUtil.isEmpty(date)) {
				now = new Date();
			} else {
				now = sdf.parse(date);
			}
			jobService.dealEnrolledJobOneWeekAgo(now);
		} catch (ParseException e) {
			logger.error("testcreateDepositTask", e);
			e.printStackTrace();
		}
		return "success";
	}

	@RequestMapping(value = "/testChangeCenterJobOneWeekAgo.do", method = RequestMethod.GET)
	@ResponseBody
	public Object testChangeCenterJobOneWeekAgo(HttpServletRequest request, String date) {
		logger.info("testChangeCenterJobOneWeekAgo|start|date=" + date);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Date now = null;
			if (StringUtil.isEmpty(date)) {
				now = new Date();
			} else {
				now = sdf.parse(date);
			}
			jobService.dealChangeCenterJobOneWeekAgo(now);
		} catch (ParseException e) {
			logger.error("testChangeCenterJobOneWeekAgo", e);
			e.printStackTrace();
		}
		return "success";
	}

	// 什么时间点没跑，执行什么时间点 ：比如11月27号早上1点没跑，就输入11月27日
	@ResponseBody
	@RequestMapping(value = "/jobExcute.do", method = RequestMethod.GET)
	public Object jobExcute(HttpServletRequest request, String date) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date now = null;
		try {
			if (StringUtil.isEmpty(date)) {
				now = new Date();
			} else {
				now = sdf.parse(date);
			}
			if (DateUtil.getPreviousDay(now) > DateUtil.getPreviousDay(new Date())) {
				return "date error";
			}
		} catch (ParseException e) {
			logger.error("dealDate error", e);
			e.printStackTrace();
		}
		StringBuffer sb = new StringBuffer();
		sb.append(taskJobOverdue(now));
		sb.append(timedTaskJob(now));
		sb.append(attendanceChildStaff(now));
		sb.append(rosterSubtractedTask(now));
		try {
			userInfoService.sendUserNewsFeedByBirthday(now);
		} catch (Exception e) {
			sb.append("sendUserNewsFeedByBirthday error;");
		}
		sb.append(enrolledJob(now));
		return StringUtil.isEmpty(sb.toString()) ? "success" : sb.toString();
	}

	/************************** start ************************/

	private String taskJobOverdue(Date now) {
		StringBuffer sb = new StringBuffer();
		int count = 0;
		logger.info("+++++++++++++++++++++++++++++++TaskJobDueOfCustmer begin+++++++++++++++++++++++++++++++");
		try {
			// 将过期Explore Curriculum,Grow Curriculum,School
			// Readiness状态更新为overDue
			programTaskExgrscService.updateOverDueProgramTaskExgrscTimedTask(now);
		} catch (Exception e) {
			count++;
			log.error("updateOverDueProgramTaskExgrscTimedTask Error", e);
			sb.append("updateOverDueProgramTaskExgrscTimedTask Error;");
		}

		try {
			// 将过期Followup状态更新为overDue
			taskProgramFollowUpService.updateOverDueProgramFollowupTimedTask(now);
		} catch (Exception e) {
			count++;
			log.error("updateOverDueProgramFollowupTimedTask Error", e);
			sb.append("updateOverDueProgramFollowupTimedTask Error;");
		}

		try {
			// 将过期weeklyEvaluation状态更新为overDue
			weeklyEvaluationService.updateOverDueWeeklyEvaluationTimedTask(now);
		} catch (Exception e) {
			count++;
			log.error("updateOverDueWeeklyEvaluationTimedTask Error", e);
			sb.append("updateOverDueWeeklyEvaluationTimedTask Error;");
		}

		try {
			// 将过期的lesson状态更新为overDue
			programLessonService.updateOverDueLessonTimedTask(now);
		} catch (Exception e) {
			count++;
			log.error("updateOverDueLessonTimedTask Error", e);
			sb.append("updateOverDueLessonTimedTask Error;");
		}

		try {
			// 将过期的intobslea状态更新为overDue
			programIntobsleaService.updateOverDueIntobsleaTimedTask(now);
		} catch (Exception e) {
			count++;
			log.error("updateOverDueIntobsleaTimedTask Error", e);
			sb.append("updateOverDueIntobsleaTimedTask Error;");
		}

		try {
			// 将过期的Register状态更新为overDue（MealRegister，SleepRegister，SunscreenRegister，NappyRegister,ToiletRegister）
			programRegisterTaskService.updateOverDueRegisterTimedTask(now, TaskType.MealRegister.getValue());
			programRegisterTaskService.updateOverDueRegisterTimedTask(now, TaskType.SleepRegister.getValue());
			programRegisterTaskService.updateOverDueRegisterTimedTask(now, TaskType.SunscreenRegister.getValue());
			programRegisterTaskService.updateOverDueRegisterTimedTask(now, TaskType.NappyRegister.getValue());
			programRegisterTaskService.updateOverDueRegisterTimedTask(now, TaskType.ToiletRegister.getValue());
		} catch (Exception e) {
			count++;
			log.error("updateOverDueRegisterTimedTask Error", e);
			sb.append("updateOverDueRegisterTimedTask Error;");
		}

		try {
			// 将过期的medication状态更新为overDue
			programMedicationService.updateOverDueMedicationTimedTask(now);
		} catch (Exception e) {
			count++;
			log.error("updateOverDueMedicationTimedTask Error", e);
			sb.append("updateOverDueMedicationTimedTask Error;");
		}

		try {
			jobService.dealOverDueTaskJobOfCustmer(logger, DateUtil.subDays(now, 1));
		} catch (Exception e) {
			count++;
			log.error("dealOverDueTaskJobOfCustmer Error", e);
			sb.append("dealOverDueTaskJobOfCustmer Error;");
		}
		logger.info("+++++++++++++++++++++++++++++++TaskJobDueOfCustmer end +++++++++++++++++++++++++++++++");

		return count > 0 ? sb.toString() : "";
	}

	private String timedTaskJob(Date now) {
		StringBuffer sb = new StringBuffer();
		int count = 0;
		logger.info("+++++++++++++++++++++++++++++++LeaveTimedTaskJob begin+++++++++++++++++++++++++++++++");
		try {
			// leave模块定时任务
			leaveService.updateLeaveTimedTask(now);
		} catch (Exception e) {
			count++;
			log.error("updateLeaveTimedTask Error", e);
			sb.append("updateLeaveTimedTask Error;");
		}

		try {
			// family模块定时任务
			familyService.updateFamilyTimedTask(now);
		} catch (Exception e) {
			count++;
			log.error("updateFamilyTimedTask Error", e);
			sb.append("updateFamilyTimedTask Error;");
		}

		try {
			// center管理模块
			// centerService.sendTodaysMenu(now);
			weekNutrionalMenuService.sendMenu(new Date());
		} catch (Exception e) {
			count++;
			log.error("sendTodaysMenu Error", e);
			sb.append("sendTodaysMenu Error;");
		}

		try {
			// 请假申请过期未批准变为失效
			absenteeService.updateAbsenteeStatusTimedTask(now);
		} catch (Exception e) {
			count++;
			log.error("updateAbsenteeStatusTimedTask Error", e);
			sb.append("updateAbsenteeStatusTimedTask Error;");
		}
		try {
			// 离园申请过期未批准变为失效
			gnService.updateGivingNoticeStatusTimedTask(now);
		} catch (Exception e) {
			count++;
			log.error("updateAbsenteeStatusTimedTask Error", e);
			sb.append("updateAbsenteeStatusTimedTask Error;");
		}

		try {
			// 给未激活的家长每七天发送激活邮件
			familyService.sendActiveEmailTimedTask(now);
		} catch (Exception e) {
			count++;
			log.error("sendActiveEmailTimedTask Error", e);
			sb.append("sendActiveEmailTimedTask Error;");
		}

		try {
			// 给未激活的员工每七天发送激活邮件
			staffService.sendActiveEmailTimedTask(now);
		} catch (Exception e) {
			count++;
			log.error("sendActiveEmailTimedTask Error", e);
			sb.append("sendActiveEmailTimedTask Error;");
		}
		try {
			// 处理Staff Employment
			staffService.dealEmployment(now);
		} catch (Exception e) {
			count++;
			log.error("dealEmployment Error", e);
			sb.append("dealEmployment Error;");
		}
		try {
			staffService.dealAchiveStaff(now);
		} catch (Exception e) {
			count++;
			log.error("dealAchiveStaff Error", e);
			sb.append("dealAchiveStaff Error;");
		}
		try {
			// 镜像公共假期,需要支付人员数据.
			payrollService.dealClosurePeriodData(now);
		} catch (Exception e) {
			count++;
			log.error("dealClosurePeriodData Error", e);
			sb.append("dealClosurePeriodData Error;");
		}
		logger.info("+++++++++++++++++++++++++++++++LeaveTimedTaskJob end +++++++++++++++++++++++++++++++");
		return count > 0 ? sb.toString() : "";
	}

	private String attendanceChildStaff(Date now) {
		StringBuffer sb = new StringBuffer();
		int count = 0;
		logger.info("+++++++++++++++++++++++++++++++AttendanceRequestJob begin+++++++++++++++++++++++++++++++");
		try {
			jobService.dealAttendanceRequestJob(logger, now, null);
		} catch (Exception e) {
			count++;
			log.error("AttendanceRequestJob Error", e);
			sb.append("AttendanceRequestJob Error;");
		}
		logger.info("+++++++++++++++++++++++++++++++AttendanceRequestJob end +++++++++++++++++++++++++++++++");

		logger.info("+++++++++++++++++++++++++++++++ChildSigninJob begin+++++++++++++++++++++++++++++++");
		try {
			int dayOfWeek = DateUtil.getDayOfWeek(now);
			if (dayOfWeek == 1 || dayOfWeek == 7) {
				logger.info("today is 1 or 7");
				return "today is weekday";
			}
			jobService.dealChildSigninJob(logger, DateUtil.subDays(now, 1));
		} catch (Exception e) {
			count++;
			log.error("ChildSigninJob Error", e);
			sb.append("ChildSigninJob Error;");
		}
		logger.info("+++++++++++++++++++++++++++++++ChildSigninJob end +++++++++++++++++++++++++++++++");

		logger.info("+++++++++++++++++++++++++++++++StaffSigninJob begin+++++++++++++++++++++++++++++++");
		try {
			int dayOfWeek = DateUtil.getDayOfWeek(now);
			if (dayOfWeek == 1 || dayOfWeek == 7) {
				logger.info("today is 1 or 7");
				return "today is weekday";
			}
			UserInfoVo userInfoVo = new UserInfoVo();
			userInfoVo.setAccountInfo(new AccountInfo());
			userInfoVo.getAccountInfo().setId(SystemConstants.JobCreateAccountId);
			jobService.dealStaffSigninJob(logger, DateUtil.subDays(now, 1), userInfoVo);
		} catch (Exception e) {
			count++;
			log.error("StaffSigninJob Error", e);
			sb.append("StaffSigninJob Error;");
		}
		logger.info("+++++++++++++++++++++++++++++++StaffSigninJob end +++++++++++++++++++++++++++++++");
		return count > 0 ? sb.toString() : "";
	}

	private String rosterSubtractedTask(Date now) {
		StringBuffer sb = new StringBuffer();
		int count = 0;
		logger.info("+++++++++++++++++++++++++++++++RosterJob begin+++++++++++++++++++++++++++++++");
		try {
			UserInfoVo userInfoVo = new UserInfoVo();
			userInfoVo.setAccountInfo(new AccountInfo());
			userInfoVo.getAccountInfo().setId(SystemConstants.JobCreateAccountId);
			jobService.dealRosterJob(logger, now, userInfoVo);
		} catch (Exception e) {
			log.error("RosterJob Error", e);
			count++;
			sb.append("RosterJob Error;");
		}
		logger.info("+++++++++++++++++++++++++++++++RosterJob end +++++++++++++++++++++++++++++++");

		logger.info("+++++++++++++++++++++++++++++++SubtractedAttendanceJob begin+++++++++++++++++++++++++++++++");
		try {
			jobService.dealSubtractedAttendanceJob(logger, now);
		} catch (Exception e) {
			log.error("SubtractedAttendanceJob Error", e);
			count++;
			sb.append("SubtractedAttendanceJob Error;");
		}
		logger.info("+++++++++++++++++++++++++++++++SubtractedAttendanceJob end +++++++++++++++++++++++++++++++");

		logger.info("+++++++++++++++++++++++++++++++TaskJobOfCustmer begin+++++++++++++++++++++++++++++++");
		try {
			// 每天生成Explore Curriculum,Grow Curriculum,School Readiness
			programTaskExgrscService.createProgramTaskExgrscTimedTask(now);
		} catch (Exception e) {
			log.error("createProgramTaskExgrscTimedTask Error", e);
			count++;
			sb.append("createProgramTaskExgrscTimedTask Error;");
		}

		try {
			// 每天生成Followup
			taskProgramFollowUpService.createProgramFollowupTimedTask(now);
		} catch (Exception e) {
			log.error("createProgramFollowupTimedTask Error", e);
			count++;
			sb.append("createProgramFollowupTimedTask Error;");
		}

		try {
			// 周五生成weeklyEvaluation
			weeklyEvaluationService.createWeeklyEvaluationOnFridayTimedTask(now);
		} catch (Exception e) {
			log.error("createWeeklyEvaluationOnFridayTimedTask Error", e);
			count++;
			sb.append("createWeeklyEvaluationOnFridayTimedTask Error;");
		}

		try {
			// 定时生成RegisterTaskInfo（MealRegister，SleepRegister，SunscreenRegister，NappyRegister，ToiletRegister）
			programRegisterTaskService.createTimingTask(now, TaskType.MealRegister.getValue());
			programRegisterTaskService.createTimingTask(now, TaskType.SleepRegister.getValue());
			programRegisterTaskService.createTimingTask(now, TaskType.SunscreenRegister.getValue());
			programRegisterTaskService.createTimingTask(now, TaskType.NappyRegister.getValue());
			programRegisterTaskService.createTimingTask(now, TaskType.ToiletRegister.getValue());
		} catch (Exception e) {
			log.error("createTimingTask Error", e);
			count++;
			sb.append("createTimingTask Error;");
		}

		try {
			jobService.dealTaskJobOfCustmer(logger, now);
		} catch (Exception e) {
			log.error("dealTaskJobOfCustmer Error", e);
			count++;
			sb.append("dealTaskJobOfCustmer Error;");
		}
		try {
			meetingTempService.createMeetingTempForMeetingTimeTask(now);
		} catch (Exception e) {
			log.error("createMeetingTempForMeetingTimeTask Error", e);
			count++;
			sb.append("createMeetingTempForMeetingTimeTask Error;");
		}
		logger.info("+++++++++++++++++++++++++++++++TaskJobOfCustmer end +++++++++++++++++++++++++++++++");
		return count > 0 ? sb.toString() : "";
	}

	private String enrolledJob(Date now) {
		StringBuffer sb = new StringBuffer();
		int count = 0;
		logger.info("+++++++++++++++++++++++++++++++EnrolledJob begin+++++++++++++++++++++++++++++++");
		try {
			jobService.dealEnrolledJob(logger, now);
		} catch (Exception e) {
			log.error("EnrolledJob Error", e);
			count++;
			sb.append("EnrolledJob Error;");
		}

		try {
			jobService.dealEnrolledJobOneWeekAgo(now);
		} catch (Exception e) {
			log.error("dealEnrolledJobOneWeekAgo Error", e);
			count++;
			sb.append("dealEnrolledJobOneWeekAgo Error;");
		}

		try {
			jobService.dealChangeCenterJobOneWeekAgo(now);
		} catch (Exception e) {
			log.error("dealChangeCenterJobOneWeekAgo Error", e);
			count++;
			sb.append("dealChangeCenterJobOneWeekAgo Error;");
		}
		logger.info("+++++++++++++++++++++++++++++++EnrolledJob end +++++++++++++++++++++++++++++++");
		return count > 0 ? sb.toString() : "";
	}

	/***************** end *******************/

	@ResponseBody
	@RequestMapping(value = "/changeRole.do", method = RequestMethod.GET)
	public Object changeRole() {
		try {
			jobService.updateCaualsRole();
		} catch (Exception e) {
			return "fail";
		}
		return "success";
	}

	@Autowired
	private CalendarInfoMapper calendarInfoMapper;

	@ResponseBody
	@RequestMapping(value = "/dealCal.do", method = RequestMethod.GET)
	public Object dealCal() {
		Date sDate = DateUtil.parse("2016-01-01", DateUtil.yyyyMMddSpt);
		Date eDate = DateUtil.parse("2026-01-01", DateUtil.yyyyMMddSpt);
		int id = 1;
		do {
			CalendarInfo c = new CalendarInfo();
			c.setId(id);
			c.setDate(sDate);
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(sDate);
			int weekday = calendar.get(Calendar.DAY_OF_WEEK);
			c.setWeekDay((short) weekday);
			calendarInfoMapper.insert(c);
			System.err.println(id + " -- " + sDate);
			sDate = DateUtil.addDay(sDate, 1);
			id = id + 1;
		} while (sDate.before(eDate));
		System.err.println("END -- " + sDate);
		return "OK";
	}

	@ResponseBody
	@RequestMapping(value = "/dealClosure.do", method = RequestMethod.GET)
	public Object dealClosurePeriodData(String date) {
		if (!mustInput(date)) {
			return sendFailResponse(getTipMsg("task.date.empty"));
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date now = null;
		try {
			now = sdf.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		payrollService.dealClosurePeriodData(now);
		return "OK";
	}

	@ResponseBody
	@RequestMapping(value = "/syncHubworks.do", method = RequestMethod.GET)
	public Object syncHubworks() {
		return hubworksService.dealChildSync(logger);
	}

	@ResponseBody
	@RequestMapping(value = "/syncChildAttend.do", method = RequestMethod.GET)
	public Object syncChildAttend(String date) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date today = new Date();
		try {
			today = sdf.parse(date);
		} catch (Exception e) {
		}
		return hubworksService.dealChildAttend(today, logger);
	}

	@ResponseBody
	@RequestMapping(value = "/buildCardId.do", method = RequestMethod.GET)
	public Object buildCardId() {
		staffService.dealStaffCardId();
		return "OK";
	}

	@ResponseBody
	@RequestMapping(value = "/lostTags.do", method = RequestMethod.GET)
	public Object lostTags() {
		staffService.dealStaffTags();
		return "OK";
	}
}
