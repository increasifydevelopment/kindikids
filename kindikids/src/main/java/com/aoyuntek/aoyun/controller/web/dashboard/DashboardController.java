package com.aoyuntek.aoyun.controller.web.dashboard;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aoyuntek.aoyun.condtion.dashboard.DashboardCondition;
import com.aoyuntek.aoyun.constants.DateFormatterConstants;
import com.aoyuntek.aoyun.controller.web.BaseWebController;
import com.aoyuntek.aoyun.entity.vo.dashboard.DashboardBaseDataVo;
import com.aoyuntek.aoyun.service.ICenterService;
import com.aoyuntek.aoyun.service.dashboard.IDashboardService;
import com.aoyuntek.aoyun.uitl.GsonUtil;
import com.theone.common.util.ServiceResult;
import com.theone.date.util.DateUtil;
import com.theone.list.util.ListUtil;

/**
 * 
 * @description 统计报表
 * @author gfwang
 * @create 2016年11月9日上午11:30:16
 * @version 1.0
 */
@Controller
@RequestMapping(value = "/dashboard")
public class DashboardController extends BaseWebController {

    /**
     * log
     */
    private static Logger log = Logger.getLogger(DashboardController.class);
    /**
     * centerService
     */
    @Autowired
    private ICenterService centerService;

    @Autowired
    private IDashboardService dashboardService;

    private ServiceResult<Object> getDashboardVo(String params) {
        ServiceResult<Object> result = new ServiceResult<Object>();
        result.setSuccess(false);
        DashboardCondition condition = (DashboardCondition) GsonUtil.jsonToBean(params, DashboardCondition.class, DateFormatterConstants.SYS_FORMAT2,
                DateFormatterConstants.SYS_FORMAT, DateFormatterConstants.SYS_T_FORMAT, DateFormatterConstants.SYS_T_FORMAT2);
        if (ListUtil.isEmpty(condition.getChooseOrgs())) {
            result.setMsg(getTipMsg("dashboard_condition_orgs"));
            return result;
        }
        handleCondition(condition);
        result.setReturnObj(condition);
        result.setSuccess(true);
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/getNqsList.do", method = RequestMethod.POST)
    public Object getNqsList(String params) {
        ServiceResult<Object> result = getDashboardVo(params);
        if (!result.isSuccess()) {
            return sendFailResponse(result.getMsg());
        }
        DashboardCondition condition = (DashboardCondition) result.getReturnObj();
        long a = System.currentTimeMillis();
        DashboardBaseDataVo vo = (DashboardBaseDataVo) dashboardService.getNqs(condition);
        System.err.println("getNqs--" + (System.currentTimeMillis() - a));
        return sendSuccessResponse(vo);
    }

    @ResponseBody
    @RequestMapping(value = "/getTaskCategories.do", method = RequestMethod.POST)
    public Object getTaskCategories(String params) {
        ServiceResult<Object> result = getDashboardVo(params);
        if (!result.isSuccess()) {
            return sendFailResponse(result.getMsg());
        }
        DashboardCondition condition = (DashboardCondition) result.getReturnObj();
        long a = System.currentTimeMillis();
        DashboardBaseDataVo vo = (DashboardBaseDataVo) dashboardService.getTaskCategories(condition).getReturnObj();
        System.err.println("getNqs--" + (System.currentTimeMillis() - a));
        return sendSuccessResponse(vo);
    }

    @ResponseBody
    @RequestMapping(value = "/getQipList.do", method = RequestMethod.POST)
    public Object getQipList(String params) {
        ServiceResult<Object> result = getDashboardVo(params);
        if (!result.isSuccess()) {
            return sendFailResponse(result.getMsg());
        }
        DashboardCondition condition = (DashboardCondition) result.getReturnObj();
        long a = System.currentTimeMillis();
        DashboardBaseDataVo vo = (DashboardBaseDataVo) dashboardService.getQip(condition).getReturnObj();
        vo.setCondition(condition);
        System.err.println("getQip--" + (System.currentTimeMillis() - a));
        return sendSuccessResponse(vo);
    }

    @ResponseBody
    @RequestMapping(value = "/getTaskProgressList.do", method = RequestMethod.POST)
    public Object getTaskProgressList(String params) {
        ServiceResult<Object> result = getDashboardVo(params);
        if (!result.isSuccess()) {
            return sendFailResponse(result.getMsg());
        }
        DashboardCondition condition = (DashboardCondition) result.getReturnObj();
        long a = System.currentTimeMillis();
        DashboardBaseDataVo vo = (DashboardBaseDataVo) dashboardService.getTaskProgress(condition).getReturnObj();
        System.err.println("getTaskProgress--" + (System.currentTimeMillis() - a));
        return sendSuccessResponse(vo);
    }

    @ResponseBody
    @RequestMapping(value = "/getActivitiesList.do", method = RequestMethod.POST)
    public Object getActivitiesList(String params, HttpServletRequest request) {
        ServiceResult<Object> result = getDashboardVo(params);
        if (!result.isSuccess()) {
            return sendFailResponse(result.getMsg());
        }
        DashboardCondition condition = (DashboardCondition) result.getReturnObj();
        long a = System.currentTimeMillis();
        DashboardBaseDataVo vo = dashboardService.getActivitiesList(getCurrentUser(request), condition);
        System.err.println("getActivitiesList--" + (System.currentTimeMillis() - a));
        return sendSuccessResponse(vo);
    }

    /**
     * 
     * @description 获取下拉框集合
     * @author gfwang
     * @create 2016年11月9日上午11:56:44
     * @version 1.0
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getSelectList.do", method = RequestMethod.GET)
    public Object getSelectList(HttpServletRequest request, String p) {
        return dashboardService.getSelectList(getCurrentUser(request), p);
    }

    private void handleCondition(DashboardCondition condition) {
        Date startDate = condition.getStartDate();
        Date endDate = condition.getEndDate();
        if (startDate == null) {
            condition.setStartDate(new Date());
        }
        if (endDate == null) {
            condition.setEndDate(new Date());
        }
        if (DateUtil.afterCurrentTime(startDate)) {
            condition.setStartDate(new Date());
        }
        if (DateUtil.afterCurrentTime(endDate)) {
            condition.setEndDate(new Date());
        }
    }

}