package com.aoyuntek.aoyun.controller.web;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aoyuntek.aoyun.service.IImportSignatureService;

@Controller
@RequestMapping("/importSignature")
public class ImportSignatureController {
    @Autowired
    private IImportSignatureService importSignatureService;

    @ResponseBody
    @RequestMapping(value = "/import.do", method = RequestMethod.GET)
    public Object importData2Stage3(HttpServletRequest request) {
        try {
            importSignatureService.importData2Stage3();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Success";
    }
}
