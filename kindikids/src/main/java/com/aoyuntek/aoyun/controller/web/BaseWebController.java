package com.aoyuntek.aoyun.controller.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.aoyuntek.aoyun.constants.DateFormatterConstants;
import com.aoyuntek.aoyun.entity.po.RoleInfo;
import com.aoyuntek.aoyun.entity.vo.FileVo;
import com.aoyuntek.aoyun.entity.vo.RoleInfoVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.util.AoyunDateEditor;
import com.aoyuntek.framework.constants.PropertieNameConts;
import com.aoyuntek.framework.constants.SessionConts;
import com.aoyuntek.framework.controller.BaseController;
import com.theone.aws.util.FileFactory;
import com.theone.common.util.ServiceResult;
import com.theone.file.util.FileUtil;
import com.theone.string.util.StringUtil;
import com.theone.web.util.ResponseUtils;
import com.theone.web.util.ResponseVo;

/**
 * @description 基类
 * @author xdwang
 * @create 2015年12月10日下午7:52:02
 * @version 1.0
 */
public class BaseWebController extends BaseController {

    @Value("#{tip}")
    private Properties tipProperties;
    @Value("#{sys}")
    private Properties sysProperties;
    @Value("#{email}")
    private Properties emailProperties;

    /**
     * @description 初始化，处理日期格式
     * @author xdwang
     * @create 2015年12月10日下午7:51:40
     * @version 1.0
     * @param binder
     *            WebDataBinder对象
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Date.class, new AoyunDateEditor());
    }

    /**
     * 
     * @author dlli5 at 2016年10月27日下午2:22:32
     * @param result
     * @return
     */
    public <T> Object serviceResult(ServiceResult<T> result) {
        return ResponseUtils.sendMsg((int) result.getCode(), result.getMsg(), result.getReturnObj());
    }

    public String[] getDateFormatter() {
        return new String[] { DateFormatterConstants.SYS_FORMAT, DateFormatterConstants.SYS_FORMAT2, DateFormatterConstants.SYS_T_FORMAT,
                DateFormatterConstants.SYS_T_FORMAT2 };
    }

    /**
     * 
     * @description 是否包含此角色
     * @author gfwang
     * @create 2016年7月21日上午9:17:26
     * @version 1.0
     * @param role
     *            要判断的角色
     * @param roleList
     *            角色集合
     * @return 1包含，-1不包含
     */
    public short containRole(Role role, List<RoleInfoVo> roleList) {
        short result = -1;
        for (RoleInfoVo allRole : roleList) {
            if (allRole.getValue() == role.getValue()) {
                result = 1;
            }
        }
        return result;
    }

    public short containRoles(Role role, List<RoleInfo> roleList) {
        short result = -1;
        for (RoleInfo allRole : roleList) {
            if (allRole.getValue() == role.getValue()) {
                result = 1;
            }
        }
        return result;
    }

    /**
     * 
     * @description have roles
     * @author gfwang
     * @create 2016年9月8日下午1:59:16
     * @version 1.0
     * @param roleList
     * @param roles
     * @return
     */
    public boolean containRoles(List<RoleInfoVo> roleList, Role... roles) {
        boolean result = false;
        for (RoleInfoVo allRole : roleList) {
            for (Role role : roles) {
                if (allRole.getValue() == role.getValue()) {
                    result = true;
                }
            }
        }
        return result;
    }

    /**
     * @description 获取当前用户信息
     * @author xdwang
     * @create 2015年12月10日下午7:53:40
     * @version 1.0
     * @param request
     *            HttpServletRequest对象
     * @return 获取当前用户信息
     */
    public UserInfoVo getCurrentUser(HttpServletRequest request) {
        Object object = request.getSession().getAttribute(SessionConts.USERSESSION);
        if (object != null) {
            return (UserInfoVo) object;
        } else {
            return null;
        }
    }

    /**
     * @description 将用户对象放入session
     * @author xdwang
     * @create 2015年12月10日下午7:53:27
     * @version 1.0
     * @param request
     *            HttpServletRequest
     * @param user
     *            用户对象
     */
    public void saveUserToSession(HttpServletRequest request, UserInfoVo user) {
        request.getSession().setAttribute(SessionConts.USERSESSION, user);
    }

    /**
     * @description 移除用户信息
     * @author xdwang
     * @create 2015年12月10日下午7:53:00
     * @version 1.0
     * @param request
     *            HttpServletRequest对象
     */
    public void removeUserToSession(HttpServletRequest request) {
        request.getSession().removeAttribute(SessionConts.USERSESSION);
    }

    /**
     * @description 获取提示配置信息
     * @author xdwang
     * @create 2015年12月10日下午7:52:40
     * @version 1.0
     * @param tipKey
     *            key
     * @return 对应的value值
     */
    public String getTipMsg(String tipKey) {
        return tipProperties.getProperty(tipKey);
    }

    /**
     * 
     * @author zcfu at 2016年5月4日下午3:33:30
     */
    public String getSystemMsg(String tipKey) {
        return sysProperties.getProperty(tipKey);
    }

    /**
     * @description 获取系统邮件模版配置
     * @author xdwang
     * @create 2015年12月13日下午2:07:28
     * @version 1.0
     * @param tipKey
     *            配置key值
     * @return value值
     */
    public String getSystemEmailMsg(String tipKey) {
        return emailProperties.getProperty(tipKey);
    }

    /**
     * 
     * @description 发送成功响应
     * @author gfwang
     * @create 2016年8月20日下午12:05:03
     * @version 1.0
     * @param msg
     *            msg
     * @return ResponseVo
     */
    public ResponseVo sendSuccessResponse(String msg) {
        return ResponseUtils.sendMsg(0, msg);
    }

    public ResponseVo sendFailResponse(String msg) {
        return ResponseUtils.sendMsg(1, msg);
    }

    public ResponseVo sendFailResponse() {
        return ResponseUtils.sendMsg(1);
    }

    /**
     * 
     * @description 发送成功响应
     * @author gfwang
     * @create 2016年8月20日下午12:06:46
     * @version 1.0
     * @param obj
     *            obj
     * @return ResponseVo
     */
    public ResponseVo sendSuccessResponse(Object obj) {
        return ResponseUtils.sendMsg(0, obj);
    }

    /**
     * 
     * @description 发送成功响应
     * @author gfwang
     * @create 2016年8月20日下午12:06:06
     * @version 1.0
     * @return ResponseVo
     */
    public ResponseVo sendSuccessResponse() {
        return ResponseUtils.sendMsg(0);
    }

    /**
     * 
     * @description 发送成功响应
     * @author gfwang
     * @create 2016年8月20日下午12:06:15
     * @version 1.0
     * @param msg
     *            msg
     * @param obj
     *            obj
     * @return ResponseVo
     */
    public ResponseVo sendSuccessResponse(String msg, Object obj) {
        return ResponseUtils.sendMsg(0, msg, obj);
    }

    /**
     * 
     * @description 公共上传
     * @author bbq
     * @create 2016年6月27日上午9:44:22
     * @version 1.0
     * @param request
     *            MultipartHttpServletRequest对象
     * @param dir
     *            指定目录
     * @return 操作结果
     */
    public ServiceResult<FileVo> uploadFile(MultipartHttpServletRequest request, String dir) {
        ServiceResult<FileVo> result = new ServiceResult<FileVo>();
        result.setSuccess(false);
        Map<String, MultipartFile> map = request.getFileMap();
        int size = map.values().size();
        FileVo fileVo = null;
        // 文件为空
        if (size == 0) {
            result.setMsg(getTipMsg("upload_nofile"));
            return result;
        }
        // 遍历文件集合
        for (MultipartFile file : map.values()) {
            String uploadFileName = file.getOriginalFilename();
            String ext = FileUtil.getExtensionName(uploadFileName);
            String formats = getSystemMsg("attachFormat");

            if (!StringUtil.isEmpty(uploadFileName) && uploadFileName.length() > 255) {
                result.setMsg(getTipMsg("upload_file_long"));
                return result;
            }
            if (!formats.contains(ext.toLowerCase())) {
                result.setMsg(getTipMsg("upload_attacherror"));
                result.setReturnObj(new FileVo(uploadFileName));
                return result;
            }
            String uploadFileId = UUID.randomUUID().toString() + "." + ext;
            // 上传s3
            try {
                FileFactory fac = new FileFactory(Region.getRegion(Regions.AP_SOUTHEAST_2),PropertieNameConts.S3);
                String relaPath = fac.uploadFile(dir + uploadFileId, file.getInputStream());
                // String visitUrl = fac.getS3RouteUrl(relaPath,
                // S3Constants.URL_TIMEOUT_HOUR);
                fileVo = new FileVo(uploadFileId, uploadFileName, relaPath, "");
            } catch (Exception e) {
                e.printStackTrace();
            }
            // 上传本地 fac.uploadFile(contextPath, filePath, fileSingleName,
            // inputStream)
        }
        result.setSuccess(true);
        result.setMsg(getTipMsg("upload succeed"));
        result.setReturnObj(fileVo);
        return result;
    }

    /**
     * 
     * @author dlli5 at 2016年8月23日上午9:27:56
     * @param strings
     * @return
     */
    public boolean mustInput(String... strings) {
        for (String string : strings) {
            if (StringUtil.isEmpty(string)) {
                return false;
            }
        }
        return true;
    }

    public boolean mustInput(Object... strings) {
        for (Object string : strings) {
            if (string == null) {
                return false;
            }
        }
        return true;
    }

    public boolean validateLen(String[] strings, Integer[] maxLengths) {
        for (int i = 0; i < strings.length; i++) {
            if (StringUtil.isNotEmpty(strings[i])) {
                if (strings[i].length() > maxLengths[i]) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean mustCheckNum(int minCheckNum, Boolean... checks) {
        int count = 0;
        for (Boolean bo : checks) {
            if (BooleanUtils.isTrue(bo)) {
                count++;
            }
        }
        if (count < minCheckNum) {
            return false;
        }
        return true;
    }

    /**
     * 
     * @description 後臺校驗
     * @author gfwang
     * @create 2016年10月17日下午1:58:03
     * @version 1.0
     * @param response
     * @param result
     */
    public void bootStrapValidateRemote(HttpServletResponse response, boolean result) {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.append("{\"valid\":" + result + "}");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }

    /**
     * @description 当前用户是兼职且未排班
     * @author hxzhang
     * @create 2017年1月18日下午2:48:33
     */
    public boolean isCasualAndNotRoster(UserInfoVo vo) {
        List<RoleInfoVo> roleInfoVoList = vo.getRoleInfoVoList();
        String centreId = vo.getUserInfo().getCentersId();
        boolean isCasual = containRoles(roleInfoVoList, Role.Casual);
        boolean isCasualAndNotRoster = false;
        if (isCasual && StringUtil.isEmpty(centreId)) {
            isCasualAndNotRoster = true;
        }
        return isCasualAndNotRoster;
    }
}
