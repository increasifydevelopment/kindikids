package com.aoyuntek.aoyun.controller.web;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aoyuntek.aoyun.service.INqsInfoService;
import com.theone.date.util.DateUtil;
import com.theone.string.util.StringUtil;
import com.theone.web.util.ResponseUtils;

/**
 * @description NQS控制器
 * @author gfwang
 * @create 2016年6月27日上午10:09:28
 * @version 1.0
 */
@Controller
@RequestMapping("/nqs")
public class NqsController extends BaseWebController {
    @Autowired
    private INqsInfoService nqsService;

    /**
     * 
     * @description 获取List集合
     * @author gfwang
     * @create 2016年6月27日上午10:09:42
     * @version 1.0
     * @param request
     * @return list
     */
    @ResponseBody
    @RequestMapping(value = "/list.do", method = RequestMethod.POST)
    public Object list(HttpServletRequest request,String dateStr) {
        Date date=new Date();
        if(!StringUtil.isEmpty(dateStr)){
            date  =DateUtil.parse(dateStr, "yyyy-MM-dd");
        }
        return ResponseUtils.sendMsg(0, nqsService.getList(date));
    }
}
