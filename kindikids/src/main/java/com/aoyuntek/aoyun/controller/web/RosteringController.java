package com.aoyuntek.aoyun.controller.web;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.enums.LeaveType;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.framework.annotation.UserAuthority;

@Controller
@RequestMapping(value = "/rostering")
public class RosteringController extends BaseWebController {
	/**
	 * 
	 * @description 获取请假集合
	 * @author gfwang
	 * @create 2016年10月10日下午3:58:25
	 * @version 1.0
	 * @return
	 */
	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge, Role.Educator, Role.Casual, Role.Cook, Role.Parent })
	@ResponseBody
	@RequestMapping(value = "/getLeaveTypeList.do", method = RequestMethod.POST)
	public Object getLeaveTypeList(Short type, HttpServletRequest request) {
		UserInfoVo user = getCurrentUser(request);
		if (containRoles(user.getRoleInfoVoList(), Role.Parent)) {
			List<LeaveType> parentLeaveList = Arrays
					.asList(new LeaveType[] { LeaveType.AnnualLeave, LeaveType.RDO, LeaveType.LongServiceLeave, LeaveType.ProfessionalDevelopmentLeave });
			return sendSuccessResponse(handleList(parentLeaveList, type));
		}
		if (type != null && type == 0) {
			return sendSuccessResponse(handleList(getLeaveList(), type));
		}
		if (type != null && type == 2) {
			if (containRoles(user.getRoleInfoVoList(), Role.CEO)) {
				return sendSuccessResponse(handleList(LeaveType.getCeoLeaveList(), type));
			} else {
				return sendSuccessResponse(handleList(LeaveType.getRosterLeaveList(), type));
			}
		}
		return sendSuccessResponse(handleList(LeaveType.getMyLeaveList(), type));
	}

	private List<LeaveType> getLeaveList() {
		return Arrays.asList(new LeaveType[] { LeaveType.ChangeOfRDO, LeaveType.ChangeOfShift, LeaveType.RDO, LeaveType.AnnualLeave, LeaveType.PersonalLeave,
				LeaveType.LongServiceLeave, LeaveType.ProfessionalDevelopmentLeave, LeaveType.CompassionateLeave, LeaveType.OtherLeave, LeaveType.ParentLeave,
				LeaveType.CasualUnavailability, LeaveType.Practicum, LeaveType.JuryDuty, LeaveType.WorkersCompensation, LeaveType.TimeinLieu });
	}

	private List handleList(List<LeaveType> list, Short type) {
		List<SelecterPo> leaves = new ArrayList<SelecterPo>();
		for (LeaveType item : list) {
			leaves.add(new SelecterPo(item.getValue() + "", item.getDesc()));
		}
		return leaves;
	}
}
