package com.aoyuntek.aoyun.controller.app;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.ServletRequestUtils;

import com.aoyuntek.aoyun.controller.web.BaseWebController;
import com.theone.secure.util.DESUtil;
import com.theone.string.util.StringUtil;

/**
 * 手机BaseAppController
 * 
 * @author dlli5 at 2016年4月25日下午2:07:50
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public class BaseAppController extends BaseWebController {

    /**
     * 获取userId 从token中
     * 
     * @author big at 2014年12月11日下午8:17:42
     * @param request
     * @return
     */
    public String getUserId(HttpServletRequest request) {
        String token = ServletRequestUtils.getStringParameter(request, "token", "null");
        if (StringUtil.isNotEmpty(token) && !"null".equals(token)) {
            try {
                return new String(DESUtil.decode(DESUtil.hex2byte(token), getSystemMsg("APP_TOKEN").getBytes()));
            } catch (Exception e) {
                log.error("BaseAppController getUserId", e);
            }
        }
        return "";
    }

}
