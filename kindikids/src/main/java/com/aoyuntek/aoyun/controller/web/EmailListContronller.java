package com.aoyuntek.aoyun.controller.web;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aoyuntek.aoyun.condtion.EmailListCondition;
import com.aoyuntek.aoyun.constants.DateFormatterConstants;
import com.aoyuntek.aoyun.service.IEmailListService;
import com.aoyuntek.aoyun.uitl.GsonUtil;
import com.theone.common.util.ServiceResult;
import com.theone.web.util.ResponseUtils;

/**
 * @author hxzhang 2018年9月26日
 */
@Controller
@RequestMapping(value = "/emailList")
public class EmailListContronller extends BaseWebController {

	@Autowired
	private IEmailListService emailListService;

	@ResponseBody
	@RequestMapping(value = "/list.do")
	public Object list(HttpServletRequest request, String params) {
		EmailListCondition condition = (EmailListCondition) GsonUtil.jsonToBean(params, EmailListCondition.class, DateFormatterConstants.SYS_T_FORMAT2,
				DateFormatterConstants.SYS_T_FORMAT, DateFormatterConstants.SYS_FORMAT2, DateFormatterConstants.SYS_FORMAT);
		ServiceResult<Object> result = emailListService.getList(condition);
		return ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
	}

	@ResponseBody
	@RequestMapping(value = "/sendPlanEmail.do")
	public Object sendPlanEmail(HttpServletRequest request, String params) {
		EmailListCondition condition = (EmailListCondition) GsonUtil.jsonToBean(params, EmailListCondition.class, DateFormatterConstants.SYS_T_FORMAT2,
				DateFormatterConstants.SYS_T_FORMAT, DateFormatterConstants.SYS_FORMAT2, DateFormatterConstants.SYS_FORMAT);
		ServiceResult<Object> result = emailListService.sendEmail(condition);
		return ResponseUtils.sendMsg(result.getCode(), result.getMsg());
	}
}
