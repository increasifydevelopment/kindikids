package com.aoyuntek.aoyun.controller.web;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aoyuntek.aoyun.condtion.LogsCondition;
import com.aoyuntek.aoyun.constants.DateFormatterConstants;
import com.aoyuntek.aoyun.entity.vo.LogsDetaileVo;
import com.aoyuntek.aoyun.service.logs.ILogsService;
import com.aoyuntek.framework.model.Pager;
import com.theone.common.util.ServiceResult;
import com.theone.json.util.JsonUtil;
import com.theone.web.util.ResponseUtils;

@Controller
@RequestMapping(value = "/logs")
public class LogsController extends BaseWebController {
	
	@Autowired
	private ILogsService logsService;
	
    @ResponseBody
    @RequestMapping(value = "/getLogsList.do", method = RequestMethod.POST)
    public Object getLogsList(HttpServletRequest request, String params) {
    	LogsCondition condition=(LogsCondition)JsonUtil.jsonToBean(params,LogsCondition.class,DateFormatterConstants.SYS_T_FORMAT);
    	ServiceResult<Pager<LogsDetaileVo, LogsCondition>> result=logsService.getLogsList(condition,  getCurrentUser(request));
    	return ResponseUtils.sendMsg((Integer) result.getCode(), result.getReturnObj());
    }

}
