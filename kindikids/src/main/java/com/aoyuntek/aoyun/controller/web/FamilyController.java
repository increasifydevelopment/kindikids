package com.aoyuntek.aoyun.controller.web;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.ContextLoader;

import com.aoyuntek.aoyun.condtion.FamilyCondition;
import com.aoyuntek.aoyun.constants.DateFormatterConstants;
import com.aoyuntek.aoyun.dao.ChildAttendanceInfoMapper;
import com.aoyuntek.aoyun.dao.EmailMsgMapper;
import com.aoyuntek.aoyun.entity.po.ChildAttendanceInfo;
import com.aoyuntek.aoyun.entity.po.EmailMsg;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.vo.ChildInfoVo;
import com.aoyuntek.aoyun.entity.vo.ChildLogsVo;
import com.aoyuntek.aoyun.entity.vo.FileListVo;
import com.aoyuntek.aoyun.entity.vo.MoveFamilyVo;
import com.aoyuntek.aoyun.entity.vo.ReportVo;
import com.aoyuntek.aoyun.entity.vo.StaffRoleInfoVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.enums.ArchivedStatus;
import com.aoyuntek.aoyun.enums.EnrolledState;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.factory.UserFactory;
import com.aoyuntek.aoyun.service.IFamilyService;
import com.aoyuntek.aoyun.uitl.FileUtil;
import com.aoyuntek.aoyun.uitl.GsonUtil;
import com.aoyuntek.aoyun.uitl.PDFUtil;
import com.aoyuntek.framework.annotation.UserAuthority;
import com.aoyuntek.framework.model.Pager;
import com.theone.common.util.ServiceResult;
import com.theone.json.util.JsonUtil;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;
import com.theone.web.util.RequestUtil;
import com.theone.web.util.ResponseUtils;

/**
 * 
 * @description family
 * @author gfwang
 * @create 2016年9月5日下午6:58:28
 * @version 1.0
 */
@Controller
@RequestMapping(value = "/family")
public class FamilyController extends BaseWebController {
	private static Logger log = Logger.getLogger(FamilyController.class);

	@Autowired
	private IFamilyService familyService;
	@Autowired
	private ChildAttendanceInfoMapper childAttendanceInfoMapper;
	@Autowired
	private UserFactory userFactory;
	@Autowired
	private EmailMsgMapper emailMsgMapper;

	/**
	 * 
	 * @description 保存小孩
	 * @author gfwang
	 * @create 2016年7月28日上午11:03:08
	 * @version 1.0
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping(value = "/saveChild.do", method = RequestMethod.POST)
	public Object saveChild(HttpServletRequest request, String params, boolean coverOld) throws Exception {
		// 获取当前登录用户
		UserInfoVo currentUser = getCurrentUser(request);
		ChildInfoVo childInfoVo = (ChildInfoVo) GsonUtil.jsonToBean(params, ChildInfoVo.class, DateFormatterConstants.SYS_T_FORMAT2, DateFormatterConstants.SYS_FORMAT2,
				DateFormatterConstants.SYS_T_FORMAT);
		String familyName = ServletRequestUtils.getStringParameter(request, "familyName", "");
		if (StringUtil.isNotEmpty(familyName) && familyName.length() > 50) {
			return sendFailResponse(getTipMsg("family_name.length"));
		}
		// 判读是否为第一个家长
		if (!isFirstParent(currentUser, childInfoVo.getUserInfo().getFamilyId(), childInfoVo.getUserInfo().getId())) {
			return sendFailResponse(getTipMsg("no.permissions"));
		}
		String ip = RequestUtil.getIpAddr(request);

		// 查询原来的记录
		boolean isAdd = StringUtil.isEmpty(childInfoVo.getUserInfo().getId());
		ChildAttendanceInfo childAttendanceInfoOfOld = null;

		if (StringUtil.isNotEmpty(childInfoVo.getUserInfo().getId())) {
			childAttendanceInfoOfOld = childAttendanceInfoMapper.selectByUserId(childInfoVo.getUserInfo().getId());
		}
		// 判断是否为第一个家长
		// childInfoVo.getAttendance().setType(ChildType.Inside.getValue());
		ServiceResult<Object> result = familyService.addOrUpdateChild(childInfoVo, familyName, currentUser, ip);

		if (result.isSuccess()) {
			ChildAttendanceInfo childAttendanceInfo = childAttendanceInfoMapper.selectByUserId(childInfoVo.getUserInfo().getId());
			if (isAdd) {
				if (childAttendanceInfo.getEnrolled() == EnrolledState.Enrolled.getValue()) {
					// 给家长发送激活邮件
					// familyService.sendEmailForParent(((ChildInfoVo)
					// result.getReturnObj()).getUserInfo().getFamilyId(), true,
					// childInfoVo.getPlan() == 1 ? true : false);
					// 发送入园邮件
					// userFactory.sendParentEmailByChild(getSystemEmailMsg("into_center_title"),
					// getSystemEmailMsg("into_center_content"),
					// ((ChildInfoVo)
					// result.getReturnObj()).getAccountInfo().getId(), true,
					// EmailType.intoCenter.getValue(), null, null, null,
					// childInfoVo.getPlan() == 1 ? true : false);
				}
			} else {
				if (childAttendanceInfoOfOld.getEnrolled() == EnrolledState.Default.getValue()
						&& (childAttendanceInfo.getEnrolled() == EnrolledState.Enrolled.getValue())) {
					// 给家长发送激活邮件
					// familyService.sendEmailForParent(((ChildInfoVo)
					// result.getReturnObj()).getUserInfo().getFamilyId(), true,
					// childInfoVo.getPlan() == 1 ? true : false);
					// 发送入园邮件
					// userFactory.sendParentEmailByChild(getSystemEmailMsg("into_center_title"),
					// getSystemEmailMsg("into_center_content"),
					// ((ChildInfoVo)
					// result.getReturnObj()).getAccountInfo().getId(), true,
					// EmailType.intoCenter.getValue(), null, null, null,
					// childInfoVo.getPlan() == 1 ? true : false);
				}
			}

			// 重新激活家长
			familyService.saveActivateParents(((ChildInfoVo) result.getReturnObj()).getUserInfo().getId());
			// 获取孩子邮件信息
			List<EmailMsg> emailMsgList = emailMsgMapper.getEmailMsgListByUserId(((ChildInfoVo) result.getReturnObj()).getUserInfo().getId());
			((ChildInfoVo) result.getReturnObj()).setEmailMsgList(emailMsgList);

			if (childAttendanceInfo.getEnrolled() == EnrolledState.Default.getValue()) {
				familyService.dealPlanEmail(childAttendanceInfo, childInfoVo.getPlan());
			}
		}
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	/**
	 * 
	 * @description 只有第一个家长可以编辑family信息，其它家长只能编辑自己的信息
	 * @author gfwang
	 * @create 2016年11月29日下午2:02:00
	 * @version 1.0
	 * @param currtenUser
	 * @param familyId
	 * @return
	 */
	public boolean isFirstParent(UserInfoVo currtenUser, String familyId, String editUserId) {
		// 判断是否为家长,不是家长，代表可以往下执行保存
		if (!containRoles(currtenUser.getRoleInfoVoList(), Role.Parent)) {
			return true;
		}
		List<UserInfo> parentList = userFactory.getParentList(familyId);
		String currtenUserId = currtenUser.getUserInfo().getId();
		// 当前用户是否为第一个家长，或者，当前编辑的否为自己
		if (ListUtil.isNotEmpty(parentList) && (currtenUserId.equals(parentList.get(0).getId()) || currtenUserId.equals(editUserId))) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 
	 * @description 保存家长
	 * @author gfwang
	 * @create 2016年7月28日上午11:03:15
	 * @version 1.0
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/saveParent.do", method = RequestMethod.POST)
	public Object saveParent(HttpServletRequest request) {
		// 获取当前登录用户
		UserInfoVo currentUser = getCurrentUser(request);
		String ip = RequestUtil.getIpAddr(request);
		String jsonStr = ServletRequestUtils.getStringParameter(request, "params", "");
		String familyName = ServletRequestUtils.getStringParameter(request, "familyName", "");
		if (StringUtil.isNotEmpty(familyName) && familyName.length() > 50) {
			return sendFailResponse(getTipMsg("family_name.length"));
		}
		UserInfo parentInfo = (UserInfo) JsonUtil.jsonToBean(jsonStr, UserInfo.class, DateFormatterConstants.SYS_FORMAT2);
		// 判读是否为第一个家长
		if (!isFirstParent(currentUser, parentInfo.getFamilyId(), parentInfo.getId())) {
			return sendFailResponse(getTipMsg("no.permissions"));
		}
		ServiceResult<Object> result = familyService.addOrUpdateParent(parentInfo, familyName, currentUser, ip);
		if (result.isSuccess()) {
			// 更新family信息
			familyService.saveFamilyInfo(parentInfo.getId());
		}
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	@ResponseBody
	@RequestMapping(value = "/list.do", method = RequestMethod.POST)
	public Object list(HttpServletRequest request, String params) {
		log.info("list|params=" + params);
		FamilyCondition condition = (FamilyCondition) GsonUtil.jsonToBean(params, FamilyCondition.class, DateFormatterConstants.SYS_T_FORMAT2,
				DateFormatterConstants.SYS_T_FORMAT, DateFormatterConstants.SYS_FORMAT2, DateFormatterConstants.SYS_FORMAT);
		ServiceResult<Pager<FileListVo, FamilyCondition>> result = familyService.getPageList(getCurrentUser(request), condition);
		return ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
	}

	/**
	 * @description 通过familyId获取Family信息
	 * @author hxzhang
	 * @create 2016年7月31日下午2:09:05
	 * @version 1.0
	 * @param request
	 *            HttpServletRequest
	 * @param familyId
	 *            familyId
	 * @return 返回查询结果
	 */
	@ResponseBody
	@RequestMapping("/info.do")
	public Object getFamilyInfo(HttpServletRequest request, String familyId) {
		// 获取当前登录用户
		// UserInfoVo currentUser = getCurrentUser(request);
		long a = System.currentTimeMillis();
		ServiceResult<Object> result = familyService.getFamilyInfo(familyId);
		long b = System.currentTimeMillis();
		System.err.println("==========================" + (b - a));
		return ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
	}

	/**
	 * @description 删除小孩
	 * @author hxzhang
	 * @create 2016年8月1日下午5:52:35
	 * @version 1.0
	 * @param request
	 *            HttpServletRequest
	 * @param userId
	 *            小孩ID
	 * @return 返回操作结果
	 */
	@ResponseBody
	@RequestMapping(value = "/delete.child.do", method = RequestMethod.GET)
	public Object removeChild(HttpServletRequest request, String userId, boolean sureDelete) {
		// 获取当前登录用户
		UserInfoVo currentUser = getCurrentUser(request);
		ServiceResult<Object> result = familyService.removeChild(userId, sureDelete, currentUser);
		if (result.isSuccess()) {
			// 设置归档时间
			familyService.updateArchiveTime(userId);
			// 更新family信息
			familyService.saveFamilyInfo(userId);
		}
		return ResponseUtils.sendMsg(result.getCode(), result.getMsg());
	}

	/**
	 * @description 删除家长
	 * @author hxzhang
	 * @create 2016年8月2日上午8:50:35
	 * @version 1.0
	 * @param request
	 *            HttpServletRequest
	 * @param userId
	 *            家长ID
	 * @return 返回操作结果
	 */
	@ResponseBody
	@RequestMapping(value = "/delete.parent.do", method = RequestMethod.GET)
	public Object removeParent(HttpServletRequest request, String userId) {
		// 获取当前登录用户
		UserInfoVo currentUser = getCurrentUser(request);
		String familyId = userFactory.getFamilyIdByUserId(userId);
		List<UserInfo> parentList = userFactory.getParentList(familyId);
		if (userId.equals(parentList.get(0).getId())) {
			return sendFailResponse(getTipMsg("parent.first.no.delete"));
		}
		ServiceResult<Object> result = familyService.removeParent(userId, currentUser);
		if (result.isSuccess()) {
			// 更新family信息
			familyService.saveFamilyInfo(userId);
		}
		return ResponseUtils.sendMsg(result.getCode(), result.getMsg());
	}

	/**
	 * @description 发送欢迎邮件
	 * @author hxzhang
	 * @create 2016年8月2日上午8:51:07
	 * @version 1.0
	 * @param request
	 *            HttpServletRequest
	 * @return 返回操作结果
	 */
	@ResponseBody
	@RequestMapping(value = "/sendEmail.do", method = RequestMethod.GET)
	public Object sendEmail(HttpServletRequest request, String userId) {
		ServiceResult<Object> result = familyService.saveWelEmail(userId);
		return ResponseUtils.sendMsg(result.getCode(), result.getMsg());
	}

	/**
	 * @description 归档小孩
	 * @author hxzhang
	 * @create 2016年8月2日下午1:42:02
	 * @version 1.0
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/archive.do", method = RequestMethod.POST)
	public Object archiveChild(HttpServletRequest request, String params) {
		ChildInfoVo childInfoVo = (ChildInfoVo) GsonUtil.jsonToBean(params, ChildInfoVo.class, DateFormatterConstants.SYS_T_FORMAT2, DateFormatterConstants.SYS_FORMAT2,
				DateFormatterConstants.SYS_T_FORMAT);
		// 如果离园时间<=当前时间 则提示
		if (childInfoVo.getAttendance().getLeaveDate() != null
				&& com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(childInfoVo.getAttendance().getLeaveDate(), new Date()) > 0) {
			if (childInfoVo.getAccountInfo().getStatus() != ArchivedStatus.Archived.getValue()) {
				return sendFailResponse(getTipMsg("child.attendance.save.leaveDate.before"));
			}
		}
		String userId = childInfoVo.getUserInfo().getId();
		ServiceResult<Object> result = familyService.updateArchiveChild(userId, true, getCurrentUser(request), childInfoVo.getAttendance().getLeaveDate());
		if (result.isSuccess()) {
			// 设置归档时间
			familyService.updateArchiveTime(userId);
			// 更新family信息
			familyService.saveFamilyInfo(userId);
		}
		return ResponseUtils.sendMsg(result.getCode(), result.getMsg());
	}

	/**
	 * 
	 * @description
	 * @author gfwang
	 * @create 2016年8月29日下午9:54:50
	 * @version 1.0
	 * @param request
	 * @param userId
	 * @param accountId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/enrolChild.do", method = RequestMethod.GET)
	public Object enrolChild(HttpServletRequest request, String userId, String accountId) {
		ServiceResult<Object> result = familyService.updateEnrolChild(userId, accountId);
		return ResponseUtils.sendMsg(result.getCode(), result.getMsg());
	}

	/**
	 * @description 取消归档小孩
	 * @author hxzhang
	 * @create 2016年8月3日上午10:06:26
	 * @version 1.0
	 * @param request
	 * @param childId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/unArchive.do", method = RequestMethod.GET)
	public Object unArchiveChild(HttpServletRequest request, String userId) {
		ServiceResult<Object> result = familyService.updateArchiveChild(userId, false, getCurrentUser(request), null);
		if (result.isSuccess()) {
			// 重新激活家长
			familyService.saveActivateParents(userId);
			// 设置归档时间
			familyService.updateArchiveTime(userId);
		}
		return ResponseUtils.sendMsg(result.getCode(), result.getMsg());
	}

	/**
	 * @description 获取组织信息
	 * @author hxzhang
	 * @create 2016年8月5日下午4:14:31
	 * @version 1.0
	 * @param request
	 *            HttpServletRequest
	 * @param userId
	 *            被查看人
	 * @return 返回结果
	 */
	@ResponseBody
	@RequestMapping(value = "/familyOrgs.do", method = RequestMethod.GET)
	public Object getFamilyOrgs(HttpServletRequest request) {
		StaffRoleInfoVo staffRoleInfoVo = familyService.getFamilyOrgs();
		return ResponseUtils.sendMsg(staffRoleInfoVo);
	}

	/**
	 * 
	 * @description 获取列表
	 * @author gfwang
	 * @create 2016年9月5日下午6:59:37
	 * @version 1.0
	 * @param request
	 * @param userId
	 * @return
	 */
	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Cook, Role.Parent })
	@ResponseBody
	@RequestMapping(value = "/getLogList.do", method = RequestMethod.POST)
	public Object getLogList(HttpServletRequest request, String userId) {
		List<ChildLogsVo> list = familyService.getLogList(userId);
		return sendSuccessResponse(list);
	}

	/**
	 * 
	 * @description 新增日志
	 * @author gfwang
	 * @create 2016年9月5日下午7:20:54
	 * @version 1.0
	 * @param request
	 * @param logs
	 * @return
	 */
	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
	@ResponseBody
	@RequestMapping(value = "/addLogs.do", method = RequestMethod.POST)
	public Object addLogs(HttpServletRequest request, String params) {
		ChildLogsVo childLog = (ChildLogsVo) GsonUtil.jsonToBean(params, ChildLogsVo.class, DateFormatterConstants.SYS_FORMAT2, DateFormatterConstants.SYS_T_FORMAT);
		String reason = childLog.getReason();
		if (StringUtil.isNotEmpty(reason) && reason.length() > 500) {
			return sendFailResponse(getTipMsg("child.logs.reason.length"));
		}
		ServiceResult<Object> result = familyService.addLogs(childLog, getCurrentUser(request));
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	/**
	 * 
	 * 
	 * @description 删除日志
	 * @author gfwang
	 * @create 2016年9月5日下午7:42:00
	 * @version 1.0
	 * @param request
	 * @param logId
	 * @return
	 */
	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
	@ResponseBody
	@RequestMapping(value = "/deleteLog.do", method = RequestMethod.POST)
	public Object deleteLog(HttpServletRequest request, String logId) {
		ServiceResult<Object> result = familyService.deleteLogs(logId);
		return ResponseUtils.sendMsg(result.getCode());
	}

	/**
	 * @description 通过RoomId获取已入园且已激活的小孩
	 * @author Hxzhang
	 * @create 2016年9月28日上午10:13:44
	 */
	@ResponseBody
	@RequestMapping(value = "/getEnrolledChild.do", method = RequestMethod.GET)
	public Object getEnrolledChild(HttpServletRequest request, String id, String params) {
		ServiceResult<Object> result = familyService.getEnrolledChildByRoomId(id, params);
		return result.getReturnObj();
	}

	/**
	 * 
	 * @description 获取所有的年龄段
	 * @author sjwang
	 * @create 2016年11月1日上午10:28:09
	 * @version 1.0
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getAllAgeStage.do", method = RequestMethod.POST)
	public List<Integer> getAllAgeScope(HttpServletRequest request) {
		return familyService.getAgeStage();

	}

	/**
	 * 
	 * @description 根据ageStage获得该年龄段的能力
	 * @author sjwang
	 * @create 2016年11月1日上午10:21:41
	 * @version 1.0
	 * @param request
	 * @param ageStage
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getEylf.do", method = RequestMethod.POST)
	public Object getAllEylf(HttpServletRequest request, Integer ageScope, String childId) {
		return familyService.getEylf(ageScope, childId);
	}

	/**
	 * 
	 * @description
	 * @author gfwang
	 * @create 2016年11月30日下午1:25:17
	 * @version 1.0
	 * @param request
	 * @param userId
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/attendChange.do", method = RequestMethod.POST)
	public Object attendChange(HttpServletRequest request, String userId, Boolean attendFlag) {
		ServiceResult<Object> result = familyService.updateAttendChangeState(getCurrentUser(request), userId, attendFlag);
		if (result.isSuccess()) {
			ChildAttendanceInfo changeInfo = childAttendanceInfoMapper.selectByUserId(userId);
			return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), changeInfo);
		}
		return sendFailResponse(result.getMsg());
	}

	/**
	 * @description 给家长获取相关Task列表
	 * @author hxzhang
	 * @create 2017年3月2日上午8:54:27
	 */
	@ResponseBody
	@RequestMapping(value = "/taskList.do", method = RequestMethod.POST)
	public Object taskList(HttpServletRequest request, String accountId) {
		ServiceResult<Object> result = familyService.taskList(accountId);
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	/**
	 * @description 上传孩子的能力
	 * @author hxzhang
	 * @create 2017年3月6日下午2:56:06
	 */
	@ResponseBody
	@RequestMapping(value = "/reports.do", method = RequestMethod.POST)
	public Object reports(HttpServletRequest request, String params) {
		ReportVo reportVo = (ReportVo) JsonUtil.jsonToBean(params, ReportVo.class);
		ServiceResult<Object> result = new ServiceResult<Object>();
		try {
			result = familyService.getReportsHtml(reportVo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		String uuid = UUID.randomUUID().toString();
		if (result.isSuccess()) {
			request.getSession().setAttribute(uuid, result.getReturnObj());
		}
		return ResponseUtils.sendMsg(result.getCode(), (Object) uuid);
	}

	/**
	 * @description 将孩子的能力导出PDF
	 * @author hxzhang
	 * @throws throws
	 *             Exception
	 * @create 2017年3月6日上午9:42:03
	 */
	@ResponseBody
	@RequestMapping(value = "/reportsPdf.do", method = RequestMethod.GET)
	public void reportsPdf(HttpServletRequest request, HttpServletResponse response, String id) throws Exception {
		// 设置文件后缀
		String fn = "PDF.pdf";
		// 读取字符编码
		String csvEncoding = "UTF-8";
		// 设置响应
		response.setCharacterEncoding(csvEncoding);
		response.setContentType("text/pdf; charset=" + csvEncoding);
		response.setHeader("Content-Disposition", "attachment; filename=" + new String(fn.getBytes(), csvEncoding));

		String html = (String) request.getSession().getAttribute(id);
		request.getSession().removeAttribute(id);
		List<File> files = new ArrayList<File>();
		System.err.println(html);
		String filePath = ContextLoader.getCurrentWebApplicationContext().getServletContext().getRealPath("/") + "resources" + File.separator + "library" + File.separator
				+ "template" + File.separator;
		files.add(FileUtil.getFile(html, filePath));
		PDFUtil.htmls2Pdf(files, response.getOutputStream());
	}

	@ResponseBody
	@RequestMapping(value = "/moveFamily.do", method = RequestMethod.POST)
	public Object moveAnotherFamily(HttpServletRequest request, String params) {
		MoveFamilyVo vo = (MoveFamilyVo) GsonUtil.jsonToBean(params, MoveFamilyVo.class);
		ServiceResult<Object> result = familyService.saveMoveAnotherFamily(vo, getCurrentUser(request));
		return ResponseUtils.sendMsg(result.getCode());
	}

	@ResponseBody
	@RequestMapping(value = "/moveToOutside.do", method = RequestMethod.POST)
	public Object moveToOutside(HttpServletRequest request, String userId, boolean flag) {
		ServiceResult<Object> result = familyService.saveMoveToOutside(userId, flag, getCurrentUser(request));
		return ResponseUtils.sendMsg(result.getCode());
	}

	@ResponseBody
	@RequestMapping(value = "/dealNotes.do", method = RequestMethod.GET)
	public Object dealNotes() {
		int count = familyService.dealNotes();
		return count;
	}

	@ResponseBody
	@RequestMapping(value = "/complete.do", method = RequestMethod.POST)
	public Object complete(HttpServletRequest request, String params) throws Exception {
		ChildInfoVo childInfoVo = (ChildInfoVo) GsonUtil.jsonToBean(params, ChildInfoVo.class, DateFormatterConstants.SYS_T_FORMAT2, DateFormatterConstants.SYS_FORMAT2,
				DateFormatterConstants.SYS_T_FORMAT);
		ServiceResult<Object> result = familyService.saveComplete(childInfoVo);
		return ResponseUtils.sendMsg((Integer) result.getCode());
	}

	@ResponseBody
	@RequestMapping(value = "/enrolmentEmail.do", method = RequestMethod.POST)
	public Object sendConfirmationEnrolmentEmail(HttpServletRequest request, String id) {
		ServiceResult<Object> result = familyService.sendConfirmationEnrolmentEmail(id);
		return ResponseUtils.sendMsg((Integer) result.getCode());
	}
}
