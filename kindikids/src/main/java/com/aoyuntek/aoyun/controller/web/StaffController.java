package com.aoyuntek.aoyun.controller.web;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aoyuntek.aoyun.condtion.StaffCondition;
import com.aoyuntek.aoyun.constants.DateFormatterConstants;
import com.aoyuntek.aoyun.entity.po.StaffEmploymentInfo;
import com.aoyuntek.aoyun.entity.vo.StaffInfoVo;
import com.aoyuntek.aoyun.entity.vo.StaffListVo;
import com.aoyuntek.aoyun.entity.vo.StaffRoleInfoVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.service.IStaffService;
import com.aoyuntek.aoyun.uitl.GsonUtil;
import com.aoyuntek.framework.model.Pager;
import com.theone.common.util.ServiceResult;
import com.theone.string.util.StringUtil;
import com.theone.web.util.RequestUtil;
import com.theone.web.util.ResponseUtils;

/**
 * @description 员工控制器类
 * @author jfxu
 * @create 2016/07/26 16:45
 * @version 1.0
 */
@Controller
@RequestMapping(value = "/staff")
public class StaffController extends BaseWebController {

	private static Logger log = Logger.getLogger(StaffController.class);

	@Autowired
	private IStaffService staffService;

	/**
	 * 获取员工列表
	 * 
	 * @author rick
	 * @date 2016年7月27日 上午10:47:47
	 * @param request
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "/staffList.do", method = RequestMethod.POST)
	public @ResponseBody Object staffList(HttpServletRequest request, String params) {
		StaffCondition condition = (StaffCondition) GsonUtil.jsonToBean(params, StaffCondition.class, DateFormatterConstants.SYS_FORMAT,
				DateFormatterConstants.SYS_FORMAT2, DateFormatterConstants.SYS_T_FORMAT, DateFormatterConstants.SYS_T_FORMAT2);
		log.info("condition:" + condition);
		ServiceResult<Pager<StaffListVo, StaffCondition>> result = staffService.getPagerStaffs(condition);
		return ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
	}

	/**
	 * 查询园区、角色、room、group列表
	 * 
	 * @author rick
	 * @date 2016年7月28日 上午11:25:03
	 * @return
	 */
	@RequestMapping(value = "/getRoleList.do", method = RequestMethod.POST)
	public @ResponseBody Object getRoleList() {
		ServiceResult<StaffRoleInfoVo> result = staffService.getRoleList();
		return ResponseUtils.sendMsg(0, result.getReturnObj());
	}

	/**
	 * 获取员工详细信息
	 * 
	 * @author rick
	 * @date 2016年7月27日 上午10:47:50
	 * @param request
	 * @param userId
	 * @return
	 */
	@RequestMapping(value = "/getStaffInfo.do", method = RequestMethod.POST)
	public @ResponseBody Object getStaffInfo(HttpServletRequest request, String userId) {
		ServiceResult<StaffInfoVo> result = staffService.saveGetStaffInfo(userId);
		return ResponseUtils.sendMsg(0, result.getReturnObj());
	}

	/**
	 * 更新员工信息
	 * 
	 * @author rick
	 * @date 2016年7月27日 上午10:47:53
	 * @param request
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	@RequestMapping(value = "/updateStaff.do", method = RequestMethod.POST)
	public @ResponseBody Object updateStaff(HttpServletRequest request) throws UnsupportedEncodingException {
		String jsonStr = ServletRequestUtils.getStringParameter(request, "params", "");
		StaffInfoVo staffInfoVo = (StaffInfoVo) GsonUtil.jsonToBean(jsonStr, StaffInfoVo.class, DateFormatterConstants.SYS_FORMAT, DateFormatterConstants.SYS_T_FORMAT,
				DateFormatterConstants.SYS_T_FORMAT2, DateFormatterConstants.SYS_FORMAT2);
		log.info("updateStaff - jsonStr:" + jsonStr);
		String ip = RequestUtil.getIpAddr(request);
		ServiceResult<Object> result = null;
		try {
			result = staffService.updateStaffInfo(staffInfoVo, getCurrentUser(request), ip);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ResponseUtils.sendMsg(result.getCode(), result.getMsg());
	}

	/**
	 * 删除员工
	 * 
	 * @author rick
	 * @date 2016年7月28日 上午11:24:47
	 * @param request
	 * @param userId
	 * @return
	 */
	@RequestMapping(value = "/deleteStaff.do", method = RequestMethod.POST)
	public @ResponseBody Object deleteStaff(HttpServletRequest request, String userId) {
		UserInfoVo currentUser = getCurrentUser(request);
		if (StringUtil.isNotBlank(userId)) {
			ServiceResult<Object> result = staffService.deleteStaff(userId, currentUser);
			return ResponseUtils.sendMsg(result.getCode(), result.getMsg());
		}
		return ResponseUtils.sendMsg(-1, "Invalid Params.");
	}

	/**
	 * 归档
	 * 
	 * @author rick
	 * @date 2016年8月2日 上午10:51:44
	 * @param request
	 * @param userId
	 * @param type
	 * @return
	 */
	@RequestMapping(value = "/archiveStaff.do", method = RequestMethod.POST)
	public @ResponseBody Object archiveStaff(HttpServletRequest request, String userId, Integer type) {
		UserInfoVo currentUser = getCurrentUser(request);
		ServiceResult<Object> result = null;
		if (type != null) {
			if (type == 1) {
				result = staffService.updateAchive(userId, type, currentUser);
			} else if (type == 0) {
				result = staffService.updateAchive(userId, type, currentUser);
			}
		}
		return ResponseUtils.sendMsg(result.getCode(), result.getMsg());
	}

	/**
	 * 发送欢迎邮件
	 * 
	 * @author rick
	 * @date 2016年8月3日 上午11:47:21
	 * @param request
	 * @param userId
	 * @return
	 */
	@RequestMapping(value = "/sendWelEmail.do", method = RequestMethod.POST)
	public @ResponseBody Object sendWelEmail(HttpServletRequest request, String userId, String accountId) {
		if (StringUtil.isNotBlank(userId) && StringUtil.isNotBlank(accountId)) {
			ServiceResult<Object> result = staffService.insertWelEmail(userId, accountId, false);
			return ResponseUtils.sendMsg(result.getCode(), result.getMsg());
		}
		return ResponseUtils.sendMsg(-1, "Invalid Params.");
	}

	@ResponseBody
	@RequestMapping(value = "/saveEmploy.do", method = RequestMethod.POST)
	public Object saveEmploy(HttpServletRequest request, String params) {
		StaffEmploymentInfo staffEmploymentInfo = (StaffEmploymentInfo) GsonUtil.jsonToBean(params, StaffEmploymentInfo.class, DateFormatterConstants.SYS_FORMAT,
				DateFormatterConstants.SYS_T_FORMAT, DateFormatterConstants.SYS_T_FORMAT2, DateFormatterConstants.SYS_FORMAT2);
		ServiceResult<Object> result = staffService.saveEmploymentRequest(staffEmploymentInfo, getCurrentUser(request), request);
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	@ResponseBody
	@RequestMapping(value = "/cancelEmploy.do", method = RequestMethod.POST)
	public Object cancelEmploy(HttpServletRequest request, String id) {
		ServiceResult<Object> result = staffService.updateEmploymentRequest(id, getCurrentUser(request));
		return ResponseUtils.sendMsg(result.getCode(), result.getMsg());
	}
}
