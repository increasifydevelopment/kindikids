package com.aoyuntek.aoyun.controller.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aoyuntek.aoyun.condtion.CenterCondition;
import com.aoyuntek.aoyun.condtion.MenuCondition;
import com.aoyuntek.aoyun.constants.DateFormatterConstants;
import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.RoomGroupInfoMapper;
import com.aoyuntek.aoyun.dao.RoomInfoMapper;
import com.aoyuntek.aoyun.dao.WeekNutrionalMenuInfoMapper;
import com.aoyuntek.aoyun.entity.po.RoomGroupInfo;
import com.aoyuntek.aoyun.entity.po.RoomInfo;
import com.aoyuntek.aoyun.entity.po.WeekNutrionalMenuInfo;
import com.aoyuntek.aoyun.entity.vo.CenterInfoVo;
import com.aoyuntek.aoyun.entity.vo.GroupInfoVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.WeekMenuVo;
import com.aoyuntek.aoyun.entity.vo.WeekNutrionalMenuVo;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.service.ICenterService;
import com.aoyuntek.aoyun.service.IWeekNutrionalMenuService;
import com.aoyuntek.aoyun.uitl.GsonUtil;
import com.aoyuntek.framework.annotation.UserAuthority;
import com.aoyuntek.framework.condtion.BaseCondition;
import com.aoyuntek.framework.model.Pager;
import com.google.gson.reflect.TypeToken;
import com.theone.common.util.ServiceResult;
import com.theone.json.util.JsonUtil;
import com.theone.string.util.StringUtil;
import com.theone.web.util.ResponseUtils;

/**
 * 
 * @description 
 * @author mingwang
 * @create 2017年6月1日上午11:08:57
 * @version 1.0
 */
@Controller
@RequestMapping(value = "/menu")
public class MenuController extends BaseWebController {

    /**
     * log
     */
    private static Logger log = Logger.getLogger(MenuController.class);
    
    @Autowired
    private WeekNutrionalMenuInfoMapper weekNutrionalMenuInfoMapper;
    
    @Autowired
    private IWeekNutrionalMenuService weekNutrionalMenuService;
    
    /**
     * 
     * @description 新增菜单
     * @author mingwang
     * @create 2017年6月1日下午1:42:29
     * @param request
     * @param params
     * @return
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/addMenu.do", method = RequestMethod.POST)
    public Object addMenu(HttpServletRequest request, String params) {
        log.info("addMenu|start");
        WeekNutrionalMenuInfo menuInfo = (WeekNutrionalMenuInfo) GsonUtil.jsonToBean(params, WeekNutrionalMenuInfo.class, DateFormatterConstants.SYS_FORMAT2,
                DateFormatterConstants.SYS_FORMAT, DateFormatterConstants.SYS_T_FORMAT, DateFormatterConstants.SYS_T_FORMAT2);
        ServiceResult<Object> result = weekNutrionalMenuService.addOrUpdateMenu(menuInfo, getCurrentUser(request));
        log.info("addMenu|end");
        return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
    }
   
    /**
     * 
     * @description 获取菜单列表
     * @author mingwang
     * @create 2017年6月1日下午2:44:46
     * @param request
     * @param condition
     * @return
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/getMenuList.do", method = RequestMethod.POST)
    public Object list(HttpServletRequest request, String params) {
        log.info("getMenuList|start");
        MenuCondition condition = (MenuCondition) GsonUtil.jsonToBean(params, MenuCondition.class, DateFormatterConstants.SYS_FORMAT2,
                DateFormatterConstants.SYS_FORMAT, DateFormatterConstants.SYS_T_FORMAT, DateFormatterConstants.SYS_T_FORMAT2);
        // 获取当前登录用户
        UserInfoVo currentUser = getCurrentUser(request);
        ServiceResult<Pager<WeekNutrionalMenuVo, MenuCondition>> result = weekNutrionalMenuService.getMenuList(condition, currentUser);
        log.info("getMenuList|end");
        return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
    }
    
    /**
     * 
     * @description 获取下拉框菜单列表
     * @author mingwang
     * @create 2017年6月5日下午2:11:37
     * @param request
     * @return
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/getSelectMenus.do", method = RequestMethod.POST)
    public Object getSelectList(HttpServletRequest request) {
        log.info("getSelectMenus|start");
        ServiceResult<List<WeekMenuVo>> result = weekNutrionalMenuService.getSelectMenuList();
        log.info("getSelectMenus|end");
        return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
    }
    
    /**
     * 
     * @description 获取当前room选择的菜单
     * @author mingwang
     * @create 2017年6月5日下午2:52:03
     * @param request
     * @param roomId
     * @param index
     * @return
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/getCurrentMenu.do", method = RequestMethod.POST)
    public Object getCurrentMenu(HttpServletRequest request, String roomId, String index) {
        log.info("getSelectMenus|start");
        ServiceResult<Object> result = weekNutrionalMenuService.getCurrentMenu(roomId, index);
        log.info("getSelectMenus|end");
        return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
    }
    
    /**
     * 
     * @description 建立room和Menu的关系
     * @author mingwang
     * @create 2017年6月5日下午2:28:46
     * @version 1.0
     * @param request
     * @param roomId
     * @param menuId
     * @return
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/saveMenu.do", method = RequestMethod.POST)
    public Object saveMenu(HttpServletRequest request, String roomId, String menuId, String index) {
        log.info("getSelectMenus|start");
        ServiceResult<List<WeekNutrionalMenuVo>> result = weekNutrionalMenuService.saveMenu(roomId, menuId, index);
        log.info("getSelectMenus|end");
        return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
    }

    /**
     * 
     * @description 获取菜单详情
     * @author mingwang
     * @create 2017年6月1日下午6:37:09
     * @param request
     * @param name
     * @return
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/getMenuInfo.do", method = RequestMethod.POST)
    public Object getMenuInfo(HttpServletRequest request, String menuId) {
        ServiceResult<Object> result = weekNutrionalMenuService.getMenuInfo(menuId);
        return ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
    }
    
    /**
     * 
     * @description 更新菜单信息
     * @author mingwang
     * @create 2017年6月1日下午6:37:23
     * @param request
     * @param params
     * @return
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/saveMenuInfo.do", method = RequestMethod.POST)
    public Object saveMenuInfo(HttpServletRequest request, String params) {
        log.info("saveMenuInfo|start");
//        List<WeekNutrionalMenuVo> weekMenuList = JsonUtil.jsonToList(params, new TypeToken<List<WeekNutrionalMenuVo>>() {
//        }.getType(), DateFormatterConstants.SYS_T_FORMAT);
        WeekMenuVo weekMenuVo = (WeekMenuVo) GsonUtil.jsonToBean(params, WeekMenuVo.class, DateFormatterConstants.SYS_FORMAT2,
                DateFormatterConstants.SYS_FORMAT, DateFormatterConstants.SYS_T_FORMAT, DateFormatterConstants.SYS_T_FORMAT2);
        ServiceResult<Object> result = weekNutrionalMenuService.updateWeekMenu(weekMenuVo, getCurrentUser(request));
        log.info("saveMenuInfo|end");
        return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
    }

    /**
     * 
     * @description 刪除菜单
     * @author mingwang
     * @create 2017年6月2日上午9:09:40
     * @param request
     * @param name
     * @return
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/deleteMenu.do", method = RequestMethod.POST)
    public Object deleteMenu(HttpServletRequest request, String menuId) {
        log.info("deleteMenu|start");
        ServiceResult<Object> result = weekNutrionalMenuService.deleteMenu(menuId);
        log.info("deleteMenu|end");
        return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
    }

    /**
     * 
     * @description 验证menuName是否重复
     * @author mingwang
     * @create 2017年6月1日上午11:29:25
     * @param request
     * @param response
     * @param menuName
     * @param menuId
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @RequestMapping(value = "/validateMenuName.do", method = RequestMethod.POST)
    public void validateCenterName(HttpServletRequest request, HttpServletResponse response, String menuName, String menuId) {
        if (StringUtil.isEmpty(menuName)) {
            wrightJson(response, true);
            return;
        }
        log.info("validateMenuName|name=" + menuName + "|menuId=" + menuId);
        int count = weekNutrionalMenuInfoMapper.validateName(menuName, menuId);
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        boolean result = count > 0 ? false : true;
        wrightJson(response, result);
    }

    private void wrightJson(HttpServletResponse response, boolean result) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.append("{\"valid\":" + result + "}");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }
    

}
