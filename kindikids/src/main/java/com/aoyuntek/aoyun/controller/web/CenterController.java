package com.aoyuntek.aoyun.controller.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aoyuntek.aoyun.condtion.CenterCondition;
import com.aoyuntek.aoyun.constants.DateFormatterConstants;
import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.RoomGroupInfoMapper;
import com.aoyuntek.aoyun.dao.RoomInfoMapper;
import com.aoyuntek.aoyun.entity.po.RoomGroupInfo;
import com.aoyuntek.aoyun.entity.po.RoomInfo;
import com.aoyuntek.aoyun.entity.vo.CenterInfoVo;
import com.aoyuntek.aoyun.entity.vo.GroupInfoVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.WeekNutrionalMenuVo;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.service.ICenterService;
import com.aoyuntek.aoyun.uitl.GsonUtil;
import com.aoyuntek.framework.annotation.UserAuthority;
import com.aoyuntek.framework.condtion.BaseCondition;
import com.aoyuntek.framework.model.Pager;
import com.google.gson.reflect.TypeToken;
import com.theone.common.util.ServiceResult;
import com.theone.json.util.JsonUtil;
import com.theone.string.util.StringUtil;
import com.theone.web.util.ResponseUtils;

/**
 * 
 * @description 园区管理控制器
 * @author gfwang
 * @create 2016年8月18日上午8:37:01
 * @version 1.0
 */
@Controller
@RequestMapping(value = "/center")
public class CenterController extends BaseWebController {

    /**
     * log
     */
    private static Logger log = Logger.getLogger(FamilyController.class);
    /**
     * centerService
     */
    @Autowired
    private ICenterService centerService;

    /**
     * centersInfoMapper
     */
    @Autowired
    private CentersInfoMapper centersInfoMapper;

    /**
     * roomInfoMapper
     */
    @Autowired
    private RoomInfoMapper roomInfoMapper;

    /**
     * roomGroupInfoMapper
     */
    @Autowired
    private RoomGroupInfoMapper roomGroupInfoMapper;

    /**
     * 
     * @description 获取园区列表
     * @author mingwang
     * @create 2016年8月18日下午1:44:35
     * @version 1.0
     * @param request
     *            HttpServletRequest
     * @param condition
     *            查询条件
     * @return
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/list.do", method = RequestMethod.POST)
    public Object list(HttpServletRequest request, CenterCondition condition) {
        log.info("list|start");
        // 获取当前登录用户
        UserInfoVo currentUser = getCurrentUser(request);
        ServiceResult<Pager<CenterInfoVo, BaseCondition>> result = centerService.getCenterList(condition, currentUser);
        log.info("list|end");
        return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
    }

    /**
     * 
     * @description 新增/更新园区
     * @author mingwang
     * @create 2016年8月18日上午9:59:30
     * @version 1.0
     * @param request
     *            HttpServletRequest
     * @return
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/addCenter.do", method = RequestMethod.POST)
    public Object addCenter(HttpServletRequest request, String params) {
        log.info("addCenter|start");
        CenterInfoVo centersInfo = (CenterInfoVo) GsonUtil.jsonToBean(params, CenterInfoVo.class, DateFormatterConstants.SYS_FORMAT2,
                DateFormatterConstants.SYS_FORMAT, DateFormatterConstants.SYS_T_FORMAT, DateFormatterConstants.SYS_T_FORMAT2);
        ServiceResult<Object> result = centerService.addOrUpdateCenter(centersInfo, getCurrentUser(request));
        log.info("addCenter|end");
        return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
    }

    /**
     * 
     * @description 新增/更新room
     * @author mingwang
     * @create 2016年8月19日下午3:10:49
     * @version 1.0
     * @param request
     * @return
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/addRoom.do", method = RequestMethod.POST)
    public Object addRoom(HttpServletRequest request, String params) {
        log.info("addRoom|start");
        // 获取当前登录用户
        RoomInfo RoomInfo = (RoomInfo) GsonUtil.jsonToBean(params, RoomInfo.class, DateFormatterConstants.SYS_FORMAT2,
                DateFormatterConstants.SYS_FORMAT, DateFormatterConstants.SYS_T_FORMAT, DateFormatterConstants.SYS_T_FORMAT2);
        ServiceResult<Object> result = centerService.addOrUpdateRoom(RoomInfo, getCurrentUser(request));
        log.info("addRoom|end");
        return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
    }

    /**
     * 
     * @description 新增/更新分组
     * @author gfwang
     * @create 2016年8月20日下午3:10:19
     * @version 1.0
     * @param request
     * @param groupInfo
     * @return 成功或失败
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/addGroup.do", method = RequestMethod.POST)
    public Object addGroup(HttpServletRequest request, RoomGroupInfo groupInfo) {
        log.info("addGroup|start");
        ServiceResult<Object> result = centerService.addOrUpdateGroup(groupInfo, getCurrentUser(request));
        log.info("addGroup|end");
        return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
    }

    /**
     * 
     * @description 获取园区基本信息
     * @author gfwang
     * @create 2016年8月22日上午9:21:20
     * @version 1.0
     * @param request
     * @param centerId
     *            园区ID
     * @return 成功或失败
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/info.do", method = RequestMethod.POST)
    public Object getCenterInfo(HttpServletRequest request, String centerId) {
        log.info("getCenterInfo|controller|start|centerId=" + centerId);
        if (StringUtil.isEmpty(centerId)) {
            return sendFailResponse(getTipMsg("no.permissions"));
        }
        CenterInfoVo centers = centerService.getCenterInfo(centerId).getReturnObj();
        log.info("getCenterInfo|controller|end|centerId=" + centerId);
        return sendSuccessResponse(centers);
    }

    /**
     * 
     * @description 获取每周菜单
     * @author gfwang
     * @create 2016年8月24日上午9:50:04
     * @version 1.0
     * @param request
     * @param weekNum
     *            weekNum
     * @param centerId
     *            centerId
     * @return menus
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/getNutMenu.do", method = RequestMethod.POST)
    public Object getNutMenu(HttpServletRequest request, Short weekNum, String centerId) {
        log.info("getNutMenu|start");
        List<WeekNutrionalMenuVo> menus = centerService.getWeekMenuByCenter(centerId, weekNum);
        log.info("getNutMenu|end");
        return sendSuccessResponse(menus);
    }

    /**
     * 
     * @description 归档/解除归档center
     * @author gfwang
     * @create 2016年8月22日上午9:24:05
     * @version 1.0
     * @param request
     * @param roomId
     *            roomId
     * @param flag
     *            0归档操作，1解除归档操作
     * @return 成功|失败
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/archiveCenter.do", method = RequestMethod.POST)
    public Object archiveCenter(HttpServletRequest request, String centerId, boolean flag) {
        log.info("archiveCenter|start");
        // 获取当前登录用户
        ServiceResult<Object> result = centerService.updateArchiveCenter(centerId, flag, getCurrentUser(request));
        log.info("archiveCenter|end");
        return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
    }

    /**
     * 
     * @description 归档/解除归档room
     * @author gfwang
     * @create 2016年8月22日上午9:24:05
     * @version 1.0
     * @param request
     * @param roomId
     *            roomId
     * @param flag
     *            0归档操作，1解除归档操作
     * @return 成功|失败
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/archiveRoom.do", method = RequestMethod.POST)
    public Object archiveRoom(HttpServletRequest request, String roomId, boolean flag) {
        log.info("archiveRoom|start");
        ServiceResult<Object> result = centerService.updateArchiveRoom(roomId, flag, getCurrentUser(request));
        log.info("archiveRoom|end");
        return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
    }

    /**
     * 
     * @description 归档/解除归档group
     * @author gfwang
     * @create 2016年8月22日上午9:24:05
     * @version 1.0
     * @param request
     * @param roomId
     *            roomId
     * @param flag
     *            0归档操作，1解除归档操作
     * @return 成功|失败
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/archiveGroup.do", method = RequestMethod.POST)
    public Object archiveGroup(HttpServletRequest request, String groupId, boolean flag) {
        log.info("archiveGroup|start");
        ServiceResult<Object> result = centerService.updateArchiveGroup(groupId, flag, getCurrentUser(request));
        log.info("archiveGroup|end");
        return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
    }

    /**
     * 
     * @description 新增菜单
     * @author mingwang
     * @create 2016年8月23日上午9:25:15
     * @version 1.0
     * @param request
     * @return
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/addWeekMenu.do", method = RequestMethod.POST)
    public Object addWeekMenu(HttpServletRequest request, String params) {
        log.info("addWeekMenu|start");
        // 获取当前登录用户
        List<WeekNutrionalMenuVo> weekMenuList = JsonUtil.jsonToList(params, new TypeToken<List<WeekNutrionalMenuVo>>() {
        }.getType(), DateFormatterConstants.SYS_T_FORMAT);
        ServiceResult<Object> result = centerService.updateWeekMenu(weekMenuList, getCurrentUser(request));
        log.info("addWeekMenu|end");
        return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
    }

    /**
     * 
     * @description 获取分组信息
     * @author gfwang
     * @create 2016年8月22日下午2:17:33
     * @version 1.0
     * @param request
     * @param groupId
     * @return
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/getGroupInfo.do", method = RequestMethod.POST)
    public Object getGroupInfo(HttpServletRequest request, String groupId) {
        log.info("getGroupInfo|start");
        GroupInfoVo group = centerService.getGroupInfo(groupId);
        log.info("getGroupInfo|end");
        return sendSuccessResponse(group);
    }

    /**
     * 
     * @description menu
     * @author gfwang
     * @create 2016年8月23日上午9:39:50
     * @version 1.0
     * @param request
     * @return
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/getCenterMenu.do", method = RequestMethod.POST)
    public Object getCenterMenu(HttpServletRequest request) {
        ServiceResult<Object> result = centerService.getCenterMenuList(getCurrentUser(request));
        return ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
    }

    /**
     * @description 验证centerName是否重复
     * @author gfwang
     * @create 2015年12月28日下午8:12:13
     * @version 1.0
     * @param request
     *            HttpServletRequest对象
     * @param response
     *            HttpServletResponse对象
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @RequestMapping(value = "/validateCenterName.do", method = RequestMethod.POST)
    public void validateCenterName(HttpServletRequest request, HttpServletResponse response, String centerName, String centerId) {
        if (StringUtil.isEmpty(centerName)) {
            wrightJson(response, true);
            return;
        }
        log.info("validateCenterName|name=" + centerName + "|centerId=" + centerId);
        int count = centersInfoMapper.validateName(centerName, centerId);
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        boolean result = count > 0 ? false : true;
        wrightJson(response, result);
    }

    private void wrightJson(HttpServletResponse response, boolean result) {
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.append("{\"valid\":" + result + "}");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }

    /**
     * @description 验证roomName是否重复
     * @author gfwang
     * @create 2015年12月28日下午8:12:13
     * @version 1.0
     * @param request
     *            HttpServletRequest对象
     * @param response
     *            HttpServletResponse对象
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @RequestMapping(value = "/validateRoomName.do", method = RequestMethod.POST)
    public void validateRoomName(HttpServletRequest request, HttpServletResponse response) {
        String name = ServletRequestUtils.getStringParameter(request, "roomName", "");
        String centerId = ServletRequestUtils.getStringParameter(request, "centerId", "");
        String roomId = ServletRequestUtils.getStringParameter(request, "roomId", "");
        log.info("validateRoomName|name=" + name + "|centerId=" + centerId + "|roomId=" + roomId);
        int count = roomInfoMapper.validateName(centerId.trim(), roomId.trim(), name.trim());
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        boolean result = count > 0 ? false : true;
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.append("{\"valid\":" + result + "}");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }

    /**
     * @description 验证groupName是否重复
     * @author gfwang
     * @create 2015年12月28日下午8:12:13
     * @version 1.0
     * @param request
     *            HttpServletRequest对象
     * @param response
     *            HttpServletResponse对象
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @RequestMapping(value = "/validateGroupName.do", method = RequestMethod.POST)
    public void validateGroupName(HttpServletRequest request, HttpServletResponse response) {
        String name = ServletRequestUtils.getStringParameter(request, "groupName", "");
        String roomId = ServletRequestUtils.getStringParameter(request, "roomId", "");
        String groupId = ServletRequestUtils.getStringParameter(request, "groupId", "");
        log.info("validateRoomName|name=" + name + "|groupId=" + groupId + "|roomId=" + roomId);
        int count = roomGroupInfoMapper.validateName(roomId.trim(), groupId.trim(), name.trim());
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        boolean result = count > 0 ? false : true;
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.append("{\"valid\":" + result + "}");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }

    @ResponseBody
    @RequestMapping(value = "/check.do", method = RequestMethod.POST)
    public void checkRoomTask(HttpServletRequest request, String id, boolean check) {
        centerService.saveCheckRoomTask(id, check);
    }
}
