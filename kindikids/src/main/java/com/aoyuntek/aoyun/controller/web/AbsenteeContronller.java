package com.aoyuntek.aoyun.controller.web;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aoyuntek.aoyun.constants.DateFormatterConstants;
import com.aoyuntek.aoyun.dao.AbsenteeRequestInfoMapper;
import com.aoyuntek.aoyun.entity.po.AbsenteeRequestInfoWithBLOBs;
import com.aoyuntek.aoyun.enums.ChangeAttendanceRequestState;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.service.attendance.IAbsenteeService;
import com.aoyuntek.aoyun.uitl.GsonUtil;
import com.aoyuntek.framework.annotation.UserAuthority;
import com.theone.common.util.ServiceResult;
import com.theone.web.util.ResponseUtils;

@RequestMapping("/Absentee")
@Controller
public class AbsenteeContronller extends BaseWebController {
    @Autowired
    private IAbsenteeService absenteeService;
    @Autowired
    private AbsenteeRequestInfoMapper absenteeRequestInfoMapper;
    

    /**
     * @description 获取请假信息
     * @author hxzhang
     * @create 2016年8月26日下午4:07:48
     * @version 1.0
     * @param request
     * @return
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Parent, Role.Cook })
    @ResponseBody
    @RequestMapping(value = "/getAbsentee.do", method = RequestMethod.GET)
    public Object getAbsenteeRequestInfo(HttpServletRequest request, String id, String userId) {
        ServiceResult<Object> result = absenteeService.getAbsentee(id, userId);
        return ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
    }

    /**
     * @description 新增或编辑请假信息
     * @author hxzhang
     * @create 2016年8月26日下午5:04:03
     * @version 1.0
     * @param request
     * @return
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge, Role.Parent })
    @ResponseBody
    @RequestMapping(value = "/saveAbsentee.do", method = RequestMethod.POST)
    public Object saveAbsenteeRequestInfo(HttpServletRequest request, boolean isSure) {
        String jsonStr = ServletRequestUtils.getStringParameter(request, "params", "");
        AbsenteeRequestInfoWithBLOBs ariWithBLOBs = (AbsenteeRequestInfoWithBLOBs) GsonUtil.jsonToBean(jsonStr, AbsenteeRequestInfoWithBLOBs.class,
                DateFormatterConstants.SYS_T_FORMAT, DateFormatterConstants.SYS_FORMAT2);
        ServiceResult<Object> result = absenteeService.addOrUpdateAbsentee(ariWithBLOBs, getCurrentUser(request), isSure);
        return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
    }

    /**
     * @description 家长取消请假申请
     * @author hxzhang
     * @create 2016年8月26日下午5:51:11
     * @version 1.0
     * @param request
     * @param id
     * @return
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge, Role.Parent })
    @ResponseBody
    @RequestMapping(value = "/cancelAbsentee.do", method = RequestMethod.GET)
    public Object cancelAbsenteeRequestInfo(HttpServletRequest request, String id) {
        ServiceResult<Object> result = absenteeService.updateOperaAbsenteeRequestInfo(id, ChangeAttendanceRequestState.Cancel.getValue(),
                getCurrentUser(request));
        return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg());
    }

    /**
     * @description 管理员拒绝请假申请
     * @author hxzhang
     * @create 2016年8月28日下午12:19:48
     * @version 1.0
     * @param request
     * @param id
     * @return
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge })
    @ResponseBody
    @RequestMapping(value = "/rejectAbsentee.do", method = RequestMethod.GET)
    public Object rejectAbsenteeRequestInfo(HttpServletRequest request, String id) {
        ServiceResult<Object> result = absenteeService.updateOperaAbsenteeRequestInfo(id, ChangeAttendanceRequestState.Discard.getValue(),
                getCurrentUser(request));
        return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg());
    }

    /**
     * @description 是否有request的Absentee
     * @author hxzhang
     * @create 2016年8月28日下午3:42:05
     * @version 1.0
     * @param request
     * @param id
     * @return
     */
    @UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Parent, Role.Cook })
    @ResponseBody
    @RequestMapping(value = "/haveAbsentee.do", method = RequestMethod.GET)
    public Object haveAbsentee(HttpServletRequest request, String id) {
        int count = absenteeRequestInfoMapper.getCountByChildId(id);
        return ResponseUtils.sendMsg(0, count);
    }

    /**
     * @description 获取该小孩本次有效请假天数,及请假有效总天数
     * @author hxzhang
     * @create 2016年12月6日下午5:01:15
     */
    @ResponseBody
    @RequestMapping(value = "/getAbsenteeDays.do", method = RequestMethod.POST)
    public Object getAbsenteeDays(HttpServletRequest request, String params) {
        AbsenteeRequestInfoWithBLOBs ariWithBLOBs = (AbsenteeRequestInfoWithBLOBs) GsonUtil.jsonToBean(params, AbsenteeRequestInfoWithBLOBs.class,
                DateFormatterConstants.SYS_T_FORMAT, DateFormatterConstants.SYS_FORMAT2);
        ServiceResult<Object> result = absenteeService.getAbsenteeDays(ariWithBLOBs);
        return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
    }
}
