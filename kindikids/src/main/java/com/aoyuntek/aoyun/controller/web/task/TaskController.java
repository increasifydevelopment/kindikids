package com.aoyuntek.aoyun.controller.web.task;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aoyuntek.aoyun.condtion.AttendanceCondtion;
import com.aoyuntek.aoyun.condtion.TaskCondtion;
import com.aoyuntek.aoyun.constants.DateFormatterConstants;
import com.aoyuntek.aoyun.controller.web.BaseWebController;
import com.aoyuntek.aoyun.entity.po.task.TaskFollowUpInfo;
import com.aoyuntek.aoyun.entity.vo.task.PersonSelectVo;
import com.aoyuntek.aoyun.entity.vo.task.TaskInfoVo;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.enums.TaskCategory;
import com.aoyuntek.aoyun.enums.TaskFrequenceRepeatType;
import com.aoyuntek.aoyun.service.program.IProgramTaskExgrscService;
import com.aoyuntek.aoyun.service.program.IWeeklyEvaluationService;
import com.aoyuntek.aoyun.service.task.ITaskService;
import com.aoyuntek.aoyun.uitl.GsonUtil;
import com.aoyuntek.framework.annotation.UserAuthority;
import com.google.gson.reflect.TypeToken;
import com.theone.common.util.ServiceResult;
import com.theone.date.util.DateUtil;
import com.theone.json.util.JsonUtil;
import com.theone.list.util.ListUtil;
import com.theone.regex.util.RegexUtil;
import com.theone.string.util.StringUtil;
import com.theone.web.util.ResponseUtils;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

/**
 * @description
 * @author Hxzhang 2016年9月22日下午3:55:19
 * @version 1.0
 */
@Controller
@RequestMapping("/task")
public class TaskController extends BaseWebController {
	@Autowired
	private ITaskService taskService;
	@Autowired
	private IProgramTaskExgrscService programTaskExgrscService;
	@Autowired
	private IWeeklyEvaluationService weeklyEvaluationService;

	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Cook })
	@ResponseBody
	@RequestMapping(value = "/obsoleteTask.do", method = RequestMethod.POST)
	public Object obsoleteTask(HttpServletRequest request, String taskInstanceId, Boolean followUpFlag, Date day) throws Exception {
		if (StringUtil.isEmpty(taskInstanceId)) {
			return ResponseUtils.sendMsg(-1, getTipMsg("illegal.request"));
		}
		ServiceResult<Object> result = taskService.dealObsoleteTask(getCurrentUser(request), taskInstanceId, followUpFlag, day);
		return serviceResult(result);
	}

	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Cook })
	@ResponseBody
	@RequestMapping(value = "/saveWhenRequiredTask.do", method = RequestMethod.POST)
	public Object saveWhenRequiredTask(HttpServletRequest request, String taskId, String json, Date day) throws Exception {
		List<PersonSelectVo> list = JsonUtil.jsonToList(json, new TypeToken<List<PersonSelectVo>>() {
		}.getType());
		if (ListUtil.isEmpty(list)) {
			return ResponseUtils.sendMsg(-1, getTipMsg("task.save.select.empty2"));
		}
		ServiceResult<Object> result = taskService.saveWhenRequiredTask(getCurrentUser(request), taskId, list, day);
		return serviceResult(result);
	}

	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Cook })
	@ResponseBody
	@RequestMapping(value = "/getWhenRequiredTasks.do", method = RequestMethod.GET)
	public Object getWhenRequiredTasks(HttpServletRequest request) {
		ServiceResult<Object> result = taskService.getWhenRequiredTasks(getCurrentUser(request));
		return ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
	}

	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Cook })
	@ResponseBody
	@RequestMapping(value = "/getWhenRequiredTasks2.do", method = RequestMethod.GET)
	public Object getWhenRequiredTasks2(HttpServletRequest request) {
		ServiceResult<Object> result = taskService.getWhenRequiredTasks2(getCurrentUser(request));
		return ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
	}

	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Cook })
	@ResponseBody
	@RequestMapping(value = "/getWhenRequiredSelects.do", method = RequestMethod.GET)
	public Object getWhenRequiredSelects(HttpServletRequest request, String taskId) {
		ServiceResult<Object> result = taskService.getWhenRequiredSelects(getCurrentUser(request), taskId);
		return ResponseUtils.sendMsg((int) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Cook })
	@ResponseBody
	@RequestMapping(value = "/saveTaskFormValue.do", method = RequestMethod.POST)
	public Object saveTaskFormValue(HttpServletRequest request, String id, int type, int statu, String json) throws Exception {
		ServiceResult<Object> result = taskService.saveTaskFormValue(getCurrentUser(request), id, type, statu, json);
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Cook })
	@ResponseBody
	@RequestMapping(value = "/getCustmerTaskInstance.do", method = RequestMethod.POST)
	public Object getCustmerTaskInstance(HttpServletRequest request, String taskInstanceId) {
		ServiceResult<Object> result = taskService.getCustmerTaskInstance(getCurrentUser(request), taskInstanceId);
		return ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
	}

	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Cook })
	@ResponseBody
	@RequestMapping(value = "/getTodayNote.do", method = RequestMethod.POST)
	public Object getTodayNote(HttpServletRequest request, String json) {
		TaskCondtion condtion = (TaskCondtion) GsonUtil.jsonToBean(json, TaskCondtion.class, DateFormatterConstants.SYS_T_FORMAT, DateFormatterConstants.SYS_FORMAT2);
		ServiceResult<Object> result = taskService.getTodayNote(getCurrentUser(request), condtion);
		return ResponseUtils.sendMsg(result.getCode(), result.getReturnObj());
	}

	/**
	 * @description 获取日历信息
	 * @author hxzhang
	 * @create 2016年9月25日上午11:50:34
	 * @version 1.0
	 * @param request
	 * @param params
	 * @return
	 */
	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Cook })
	@ResponseBody
	@RequestMapping(value = "/getCalendar.do", method = RequestMethod.POST)
	public Object getCalendar(HttpServletRequest request, String params) {
		AttendanceCondtion attendanceCondtion = (AttendanceCondtion) GsonUtil.jsonToBean(params, AttendanceCondtion.class, DateFormatterConstants.SYS_FORMAT,
				DateFormatterConstants.SYS_FORMAT2, DateFormatterConstants.SYS_T_FORMAT, DateFormatterConstants.SYS_T_FORMAT2);
		ServiceResult<Object> result = taskService.getCalendar(getCurrentUser(request), attendanceCondtion);
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	@UserAuthority(roles = { Role.CEO })
	@ResponseBody
	@RequestMapping(value = "/getSelectList.do", method = RequestMethod.GET)
	public Object getSelectList(HttpServletRequest request, int category, String taskId, String findText) {
		ServiceResult<List> serviceResult = taskService.getSelectList(category, getCurrentUser(request), taskId, findText);
		return serviceResult.getReturnObj();
	}

	@UserAuthority(roles = { Role.CEO })
	@ResponseBody
	@RequestMapping(value = "/getResponsibleList.do", method = RequestMethod.GET)
	public Object getResponsibleList(HttpServletRequest request, int category, String selectJson, String taskId, String findText) throws ParseException {
		List<PersonSelectVo> list = JsonUtil.jsonToList(selectJson, new TypeToken<List<PersonSelectVo>>() {
		}.getType());
		if (ListUtil.isEmpty(list)) {
			return new ArrayList<PersonSelectVo>();
		}
		ServiceResult<List> serviceResult = taskService.getResponsibleList(list, category, getCurrentUser(request), taskId, findText);
		return serviceResult.getReturnObj();
	}

	@UserAuthority(roles = { Role.CEO })
	@ResponseBody
	@RequestMapping(value = "/getImplementersList.do", method = RequestMethod.GET)
	public Object getImplementersList(HttpServletRequest request, int category, String selectJson, String taskId, String findText) throws ParseException {
		List<PersonSelectVo> list = JsonUtil.jsonToList(selectJson, new TypeToken<List<PersonSelectVo>>() {
		}.getType());
		if (ListUtil.isEmpty(list)) {
			return new ArrayList<PersonSelectVo>();
		}
		ServiceResult<List> serviceResult = taskService.getImplementersList(list, category, getCurrentUser(request), taskId, findText);
		return serviceResult.getReturnObj();
	}

	@UserAuthority(roles = { Role.CEO })
	@ResponseBody
	@RequestMapping(value = "/saveTask.do", method = RequestMethod.POST)
	public Object saveTask(HttpServletRequest request, String json) throws Exception {
		TaskInfoVo obj = (TaskInfoVo) GsonUtil.jsonToBean(json, TaskInfoVo.class, DateFormatterConstants.SYS_T_FORMAT, DateFormatterConstants.SYS_FORMAT2);

		if (!mustInput(obj.getTaskName())) {
			return ResponseUtils.sendMsg(-1, getTipMsg("task.save.TaskName.empty"));
		}
		// add gfwang nqs require
		if (ListUtil.isEmpty(obj.getNqsList())) {
			return ResponseUtils.sendMsg(-1, getTipMsg("msg_nqs_empty"));
		}

		if (ListUtil.isEmpty(obj.getSelectTaskVisibles())) {
			return ResponseUtils.sendMsg(-1, getTipMsg("task.save.select.empty"));
		}

		if (BooleanUtils.isNotTrue(obj.getShiftFlag())) {
			if (obj.getTaskFrequencyInfo() == null) {
				return ResponseUtils.sendMsg(-1, getTipMsg("task.save.TaskFrequency.empty"));
			}
			Short shortVal = obj.getTaskFrequencyInfo().getRepeatType();
			if (shortVal == null) {
				return ResponseUtils.sendMsg(-1, getTipMsg("task.save.TaskFrequency.empty"));
			}

			TaskFrequenceRepeatType frequenceRepeatType = TaskFrequenceRepeatType.getType(shortVal);

			// getStartsDate时间必须大于等于当前时间
			if (obj.getTaskFrequencyInfo().getStartsDate() != null
					&& com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(new Date(), obj.getTaskFrequencyInfo().getStartsDate()) < 0) {
				return ResponseUtils.sendMsg(-1, getTipMsg("task.save.TaskFrequency.start.now"));
			}

			// 判断吐过end选择的话 那么Starts <= Ends
			if (obj.getTaskFrequencyInfo().getEndsDate() != null) {
				if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(obj.getTaskFrequencyInfo().getStartsDate(), obj.getTaskFrequencyInfo().getEndsDate()) < 0) {
					return ResponseUtils.sendMsg(-1, getTipMsg("task.save.endstart.date"));
				}
			}

			switch (frequenceRepeatType) {
			case Daily:
			case Quarterly:
				if (!mustInput(obj.getTaskFrequencyInfo().getStartsDate())) {
					return ResponseUtils.sendMsg(-1, getTipMsg("task.save.TaskFrequency.form.empty"));
				}
				break;
			case Weekly:
			case Monthly:
			case Yearly:
			case OnceOff:
				if (!mustInput(obj.getTaskFrequencyInfo().getStartsDate(), obj.getTaskFrequencyInfo().getDayToStart(), obj.getTaskFrequencyInfo().getDayToEnd())) {
					return ResponseUtils.sendMsg(-1, getTipMsg("task.save.TaskFrequency.form.empty"));
				}
				break;
			default:
				break;
			}

			switch (frequenceRepeatType) {
			case Weekly: {
				if (Integer.parseInt(obj.getTaskFrequencyInfo().getDayToStart()) > Integer.parseInt(obj.getTaskFrequencyInfo().getDayToEnd())) {
					return ResponseUtils.sendMsg(-1, getTipMsg("task.save.TaskFrequency.weekly.start.due"));
				}
			}
				break;
			case Monthly: {
				if (Integer.parseInt(obj.getTaskFrequencyInfo().getDayToStart()) > Integer.parseInt(obj.getTaskFrequencyInfo().getDayToEnd())) {
					return ResponseUtils.sendMsg(-1, getTipMsg("task.save.TaskFrequency.monthy.start.due"));
				}
			}
				break;
			case Yearly: {
				String startStr = obj.getTaskFrequencyInfo().getDayToStart() + "/2016";
				String endStr = obj.getTaskFrequencyInfo().getDayToEnd() + "/2016";
				Date dayToStart = DateUtil.parse(startStr, DateFormatterConstants.SYS_FORMAT2);
				Date dayToDue = DateUtil.parse(endStr, DateFormatterConstants.SYS_FORMAT2);
				if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(dayToStart, dayToDue) < 0) {
					return ResponseUtils.sendMsg(-1, getTipMsg("task.save.TaskFrequency.YearlyOnceOff.start.due"));
				}
			}
				break;
			case OnceOff: {
				Date dayToStart = DateUtil.parse(obj.getTaskFrequencyInfo().getDayToStart(), DateFormatterConstants.SYS_FORMAT2);
				Date dayToDue = DateUtil.parse(obj.getTaskFrequencyInfo().getDayToEnd(), DateFormatterConstants.SYS_FORMAT2);
				if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(dayToStart, dayToDue) < 0) {
					return ResponseUtils.sendMsg(-1, getTipMsg("task.save.TaskFrequency.YearlyOnceOff.start.due"));
				}
			}
				break;
			default:
				break;
			}

			if (ListUtil.isEmpty(obj.getResponsibleTaskVisibles()) && obj.getTaskCategory() != TaskCategory.Individual.getValue()) {
				return ResponseUtils.sendMsg(-1, getTipMsg("task.save.responsible.empty"));
			}
			if (ListUtil.isEmpty(obj.getTaskImplementersVisibles()) && obj.getTaskCategory() != TaskCategory.Individual.getValue()) {
				return ResponseUtils.sendMsg(-1, getTipMsg("task.save.implementer.empty"));
			}
		}

		// 新增的时候，workflow必填
		if (StringUtil.isEmpty(obj.getFormId()) && obj.getTaskFormObj().getAttributes().size() <= 0) {
			return ResponseUtils.sendMsg(-1, getTipMsg("task.save.form.empty"));
		}
		// 编辑workflow时，不能完全删除form
		if (obj.getTaskFormObj() != null && obj.getTaskFormObj().getAttributes().size() <= 0) {
			return ResponseUtils.sendMsg(-1, getTipMsg("task.save.form.empty"));
		}

		if (ListUtil.isNotEmpty(obj.getFollowUpInfos())) {
			for (TaskFollowUpInfo taskFollowUpInfo : obj.getFollowUpInfos()) {
				if (taskFollowUpInfo.getDurationStep() == null || !RegexUtil.isInt(taskFollowUpInfo.getDurationStep().toString())
						|| taskFollowUpInfo.getDurationStep() <= 0) {
					return ResponseUtils.sendMsg(-1, getTipMsg("task.save.TaskFollowUpInfo.durationstep"));
				}

				// 新增
				if (StringUtil.isEmpty(taskFollowUpInfo.getFormId()) && taskFollowUpInfo.getTaskFormObj() == null) {
					return ResponseUtils.sendMsg(-1, getTipMsg("task.save.TaskFollowUpInfo.empty"));
				}

				if (taskFollowUpInfo.getTaskFormObj() != null && taskFollowUpInfo.getTaskFormObj().getAttributes().size() <= 0) {
					return ResponseUtils.sendMsg(-1, getTipMsg("task.save.TaskFollowUpInfo.empty"));
				}
				if (ListUtil.isEmpty(taskFollowUpInfo.getResponsibleTaskVisibles()) && obj.getTaskCategory() != TaskCategory.Individual.getValue()) {
					return ResponseUtils.sendMsg(-1, getTipMsg("task.save.responsible.empty"));
				}
				if (ListUtil.isEmpty(taskFollowUpInfo.getTaskImplementersVisibles()) && obj.getTaskCategory() != TaskCategory.Individual.getValue()) {
					return ResponseUtils.sendMsg(-1, getTipMsg("task.save.implementer.empty"));
				}
			}
		}

		ServiceResult<Object> serviceResult = taskService.saveOrUpdateTask(obj, getCurrentUser(request));
		return ResponseUtils.sendMsg((int) serviceResult.getCode(), serviceResult.getMsg(), serviceResult.getReturnObj());
	}

	public static void main(String[] args) {
		System.out.println("27/09/2016".substring(0, "27/09/2016".lastIndexOf("/")));
	}

	@UserAuthority(roles = { Role.CEO })
	@ResponseBody
	@RequestMapping(value = "/updateStatu.do", method = RequestMethod.POST)
	public Object updateStatu(HttpServletRequest request, String taskId, int statu) throws ParseException {
		ServiceResult<Object> serviceResult = taskService.updateStatu(taskId, statu, getCurrentUser(request));
		return ResponseUtils.sendMsg((int) serviceResult.getCode(), serviceResult.getMsg(), serviceResult.getReturnObj());
	}

	@UserAuthority(roles = { Role.CEO })
	@ResponseBody
	@RequestMapping(value = "/getTaskList.do", method = RequestMethod.POST)
	public Object getTaskList(HttpServletRequest request, TaskCondtion condtion) throws ParseException {
		ServiceResult<Object> serviceResult = taskService.getTaskList(condtion, getCurrentUser(request));
		return ResponseUtils.sendMsg((int) serviceResult.getCode(), serviceResult.getReturnObj());
	}

	@UserAuthority(roles = { Role.CEO })
	@ResponseBody
	@RequestMapping(value = "/getTask.do", method = RequestMethod.POST)
	public Object getTask(HttpServletRequest request, String taskId) throws Exception {
		ServiceResult<Object> serviceResult = taskService.getTask(taskId, getCurrentUser(request));
		return ResponseUtils.sendMsg((int) serviceResult.getCode(), serviceResult.getReturnObj());
	}

	@UserAuthority(roles = { Role.CEO, Role.CentreManager, Role.Educator, Role.Cook })
	@ResponseBody
	@RequestMapping(value = "/getUserTaskList.do", method = RequestMethod.POST)
	public Object getUserTaskList(HttpServletRequest request, String json) throws Exception {
		TaskCondtion condtion = (TaskCondtion) GsonUtil.jsonToBean(json, TaskCondtion.class, DateFormatterConstants.SYS_T_FORMAT, DateFormatterConstants.SYS_FORMAT2);
		// 调用接口去生成
		// TODO big 点击全部的时候生成可能存在问题 后期可以考虑用异步方式 将生成task的任务抛给消息处理器
		CacheManager manager = CacheManager.create();
		// 通过manager可以生成指定名称的Cache对象
		Cache cache = manager.getCache("taskCache");
		// 缓存key
		String key = DateUtil.format(condtion.getDay(), "yyyy-MM-dd");
		Element el = cache.get(key);
		try {
			if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(condtion.getDay(), new Date()) < 0 && null == el) {
				// 存入缓存
				cache.put(new Element(key, ""));
				programTaskExgrscService.createProgramTaskExgrscTimedTask(condtion.getDay());
				weeklyEvaluationService.createWeeklyEvaluationOnFridayTimedTask(condtion.getDay());
				// 清除缓存
				cache.remove(key);
			}
		} catch (Exception e) {
			log.error("programTaskExgrscService", e);
		}

		// 调用接口去获取task
		ServiceResult<Object> serviceResult = taskService.getUserTaskList(condtion, getCurrentUser(request));
		return ResponseUtils.sendMsg((int) serviceResult.getCode(), serviceResult.getReturnObj());
	}

	@ResponseBody
	@RequestMapping(value = "/taskTableView.do", method = RequestMethod.POST)
	public Object taskTableView(HttpServletRequest request, String json) {
		TaskCondtion condtion = (TaskCondtion) GsonUtil.jsonToBean(json, TaskCondtion.class, DateFormatterConstants.SYS_T_FORMAT, DateFormatterConstants.SYS_FORMAT2);
		// 调用接口去获取task
		ServiceResult<Object> serviceResult = taskService.getTaskTableView(condtion, getCurrentUser(request));
		return ResponseUtils.sendMsg((int) serviceResult.getCode(), serviceResult.getReturnObj());
	}

	@ResponseBody
	@RequestMapping(value = "/taskName.do", method = RequestMethod.GET)
	public Object selectTaskName(HttpServletRequest request, String p) {
		return taskService.getTaskNames(p, getCurrentUser(request));
	}

	/**
	 * 
	 * @description 验证task名称重复
	 * @author gfwang
	 * @create 2016年9月25日上午10:10:59
	 * @version 1.0
	 * @param response
	 * @param taskName
	 * @param taskId
	 */
	@UserAuthority(roles = { Role.CEO })
	@ResponseBody
	@RequestMapping(value = "/validataTaskName.do", method = RequestMethod.POST)
	public void validataTaskName(HttpServletResponse response, String taskName, String taskId) {
		boolean result = false;
		result = taskService.validateTaskName(taskName, taskId);
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json; charset=utf-8");
		PrintWriter out = null;
		try {
			out = response.getWriter();
			out.append("{\"valid\":" + result + "}");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (out != null) {
				out.close();
			}
		}
	}

	/**
	 * 
	 * @description 获取task的个数
	 * @author gfwang
	 * @create 2016年11月30日上午8:41:41
	 * @version 1.0
	 * @param request
	 * @return 所有人可见
	 */
	@ResponseBody
	@RequestMapping(value = "/getTaskCount.do", method = RequestMethod.POST)
	public Object getTaskCount(HttpServletRequest request, String json) {
		TaskCondtion condtion = (TaskCondtion) GsonUtil.jsonToBean(json, TaskCondtion.class, DateFormatterConstants.SYS_T_FORMAT, DateFormatterConstants.SYS_FORMAT2);
		return sendSuccessResponse(taskService.getTaskCount(condtion, getCurrentUser(request)).getReturnObj());
	}

	@ResponseBody
	@RequestMapping(value = "/export.do", method = RequestMethod.POST)
	public Object exportData(HttpServletRequest request, String params) {
		TaskCondtion condtion = (TaskCondtion) GsonUtil.jsonToBean(params, TaskCondtion.class, DateFormatterConstants.SYS_T_FORMAT, DateFormatterConstants.SYS_FORMAT2);
		// 调用接口去获取task
		ServiceResult<Object> serviceResult = taskService.getExportData(condtion, getCurrentUser(request));
		String uuid = UUID.randomUUID().toString();
		request.getSession().setAttribute(uuid, serviceResult.getReturnObj());
		return ResponseUtils.sendMsg(serviceResult.getCode(), (Object) uuid);
	}

	@ResponseBody
	@RequestMapping(value = "/csv.do", method = RequestMethod.GET)
	public void exportCsv(HttpServletRequest request, HttpServletResponse response, String uuid) {
		List<Map<String, String>> list = (List<Map<String, String>>) request.getSession().getAttribute(uuid);
		request.removeAttribute(uuid);
		taskService.exportCsv(response, list);
	}
}
