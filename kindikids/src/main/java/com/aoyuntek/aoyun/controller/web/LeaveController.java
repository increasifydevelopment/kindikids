package com.aoyuntek.aoyun.controller.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aoyuntek.aoyun.condtion.LeaveCondition;
import com.aoyuntek.aoyun.constants.DateFormatterConstants;
import com.aoyuntek.aoyun.entity.po.LeaveInfo;
import com.aoyuntek.aoyun.entity.po.LeavePaidInfo;
import com.aoyuntek.aoyun.entity.vo.LeaveListVo;
import com.aoyuntek.aoyun.entity.vo.LeaveVo;
import com.aoyuntek.aoyun.factory.UserFactory;
import com.aoyuntek.aoyun.service.attendance.ILeaveService;
import com.aoyuntek.aoyun.uitl.GsonUtil;
import com.aoyuntek.framework.model.Pager;
import com.google.gson.reflect.TypeToken;
import com.theone.common.util.ServiceResult;
import com.theone.json.util.JsonUtil;
import com.theone.web.util.ResponseUtils;

/**
 * @description 请假控制层
 * @author hxzhang
 * @create 2016年8月16日上午9:09:29
 * @version 1.0
 */
@Controller
@RequestMapping(value = "/leave")
public class LeaveController extends BaseWebController {
	/**
	 * 日志
	 */
	private static Logger log = Logger.getLogger(LeaveController.class);
	/**
	 * ILeaveService
	 */
	@Autowired
	private ILeaveService leaveService;

	/**
	 * @description 提交请假条
	 * @author hxzhang
	 * @create 2016年8月16日上午9:13:12
	 * @version 1.0
	 * @param request
	 *            HttpServletRequest
	 * @return 返回结果
	 */
	@ResponseBody
	@RequestMapping(value = "/saveLeave.do", method = RequestMethod.POST)
	public Object leave(HttpServletRequest request, String params) {
		LeaveVo leaveVo = (LeaveVo) GsonUtil.jsonToBean(params, LeaveVo.class, DateFormatterConstants.SYS_T_FORMAT2);
		ServiceResult<Object> result = leaveService.saveLeaveInfo(leaveVo, getCurrentUser(request));
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	/**
	 * @description 提交自己的请假条
	 * @author hxzhang
	 * @create 2016年8月23日上午10:34:27
	 * @version 1.0
	 * @param request
	 * @param id
	 * @param startDate
	 * @param endDate
	 * @param type
	 * @param isAdmin
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/saveMyLeave.do", method = RequestMethod.POST)
	public Object myLeave(HttpServletRequest request, String params) {
		LeaveVo leaveVo = (LeaveVo) GsonUtil.jsonToBean(params, LeaveVo.class, DateFormatterConstants.SYS_T_FORMAT2);
		ServiceResult<Object> result = leaveService.saveLeaveInfo(leaveVo, getCurrentUser(request));
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg(), result.getReturnObj());
	}

	@ResponseBody
	@RequestMapping(value = "/goRequestState.do", method = RequestMethod.POST)
	public Object goRequestState(String leaveId) {
		leaveService.updateRequestState(leaveId);
		return sendSuccessResponse();
	}

	/**
	 * @description 获取请假条列表
	 * @author hxzhang
	 * @create 2016年8月16日下午2:36:45
	 * @version 1.0
	 * @param request
	 *            HttpServletRequest
	 * @return 返回结果
	 */
	@ResponseBody
	@RequestMapping(value = "/getLeaveList.do", method = RequestMethod.POST)
	public Object leaveList(HttpServletRequest request, String params) {
		LeaveCondition condition = (LeaveCondition) GsonUtil.jsonToBean(params, LeaveCondition.class, DateFormatterConstants.SYS_FORMAT2,
				DateFormatterConstants.SYS_T_FORMAT);
		ServiceResult<Pager<LeaveListVo, LeaveCondition>> result = leaveService.getLeaveList(condition, getCurrentUser(request));
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getReturnObj());
	}

	/**
	 * @description 获取自己的请假列表
	 * @author hxzhang
	 * @create 2016年8月23日上午10:35:42
	 * @version 1.0
	 * @param request
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/getMyLeaveList.do", method = RequestMethod.POST)
	public Object myleaveList(HttpServletRequest request, String params) {
		LeaveCondition condition = (LeaveCondition) JsonUtil.jsonToBean(params, LeaveCondition.class, DateFormatterConstants.SYS_T_FORMAT);
		ServiceResult<Pager<LeaveListVo, LeaveCondition>> result = leaveService.getLeaveList(condition, getCurrentUser(request));
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getReturnObj());
	}

	@Autowired
	private UserFactory userFactory;

	/**
	 * @description 对请假条进行操作
	 * @author hxzhang
	 * @create 2016年8月16日下午5:49:08
	 * @version 1.0
	 * @param request
	 *            HttpServletRequest
	 * @param operaType
	 *            操作方式
	 * @param reason
	 *            事由
	 * @return 返回操作结果
	 */
	@ResponseBody
	@RequestMapping(value = "/operaLeave.do", method = RequestMethod.POST)
	public Object operaLeave(HttpServletRequest request, String leaveId, int operaType, String reason, String params, boolean confirm) {
		List<LeavePaidInfo> lpiList = JsonUtil.jsonToList(params, new TypeToken<List<LeavePaidInfo>>() {
		}.getType(), DateFormatterConstants.SYS_T_FORMAT);

		LeaveInfo leaveInfo = leaveService.selectByPrimaryKey(leaveId);
		if (!userFactory.validatePermiss(null, getCurrentUser(request), leaveInfo.getCentreId())) {
			return sendFailResponse(getTipMsg("no.permissions"));
		}

		ServiceResult<Object> result = leaveService.saveOperaLeave(lpiList, leaveId, (short) operaType, reason, confirm, getCurrentUser(request));
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg());
	}

	/**
	 * @description 自己对请假条进行操作
	 * @author hxzhang
	 * @create 2016年8月23日上午11:49:57
	 * @version 1.0
	 * @param request
	 * @param leaveId
	 * @param operaType
	 * @param reason
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/myOperaLeave.do", method = RequestMethod.POST)
	public Object myOperaLeave(HttpServletRequest request, String leaveId, int operaType, String reason, String params, boolean confirm) {
		List<LeavePaidInfo> lpiList = JsonUtil.jsonToList(params, new TypeToken<List<LeavePaidInfo>>() {
		}.getType(), DateFormatterConstants.SYS_FORMAT2);
		ServiceResult<Object> result = leaveService.saveOperaLeave(lpiList, leaveId, (short) operaType, reason, confirm, getCurrentUser(request));
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getMsg());
	}

	/**
	 * @description 获取请假时间明细
	 * @author hxzhang
	 * @create 2016年8月17日上午11:47:14
	 * @version 1.0
	 * @param request
	 *            HttpServletRequest
	 * @param leaveId
	 *            请假条 ID
	 * @return 返回结果
	 */
	@ResponseBody
	@RequestMapping(value = "/leaveDate.do", method = RequestMethod.GET)
	public Object leaveDate(HttpServletRequest request, String leaveId) {
		ServiceResult<Object> result = leaveService.getLeaveDate(leaveId, getCurrentUser(request));
		return ResponseUtils.sendMsg((Integer) result.getCode(), result.getReturnObj());
	}

}
