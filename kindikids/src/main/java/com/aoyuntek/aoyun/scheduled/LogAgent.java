package com.aoyuntek.aoyun.scheduled;

import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class LogAgent {

    private static Logger info = Logger.getLogger(LogAgent.class);
    private static Logger VisitUrlLog = Logger.getLogger("VisitUrlLog");
    private static Logger MethodLog = Logger.getLogger("MethodLog");

    @Scheduled(cron = "10 0 0 * * ?")
    public void updateJob() {
        info.warn("----------------------------------");
        info.info("----------------------------------");
        info.debug("----------------------------------");
        info.error("----------------------------------");
        VisitUrlLog.info("----------------------------------");
        MethodLog.info("----------------------------------");
    }
}
