package com.aoyuntek.aoyun.interceptor;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.framework.annotation.UserAuthority;

/**
 * 
 * @description 权限拦截
 * @author gfwang
 * @create 2016年6月28日下午4:08:16
 * @version 1.0
 */
public class AuthorityAnnotationInterceptor extends HandlerInterceptorAdapter {
    /**
     * logger
     */
    private final Logger logger = Logger.getLogger(AuthorityAnnotationInterceptor.class);

    /**
     * tipProperties
     */
    @Value("#{tip}")
    private Properties tipProperties;

    /**
     * sysProperties
     */
    @Value("#{sys}")
    private Properties sysProperties;

    /**
     * userInfoMapper
     */
    @Autowired
    private UserInfoMapper userInfoMapper;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        logger.debug("权限过滤begin......");

        HandlerMethod handler2 = (HandlerMethod) handler;
        UserAuthority fireAuthority = handler2.getMethodAnnotation(UserAuthority.class);

        if (null == fireAuthority) {
            logger.info("没有声明权限,放行");
            return true;
        }
        return true;
   //     logger.info("fireAuthority" + fireAuthority.toString());

   /*     List<RoleInfoVo> roleList = null;
        if (SessionUtil.haveSession(request, SessionConts.USERSESSION)) {
            UserInfoVo userInfo = (UserInfoVo) SessionUtil.getModel(request, SessionConts.USERSESSION);
            roleList = userInfo.getRoleInfoVoList();
        } else {
            String account = RequestUtil.getStringParameter(request, "account", "-1");
            // 根据account获取用户信息
            UserCondition condition = new UserCondition();
            condition.setAccount(account);
            List<UserInfoVo> userList = userInfoMapper.getUserList(condition);
            if (null != userList && userList.size() != 0) {
                roleList = userList.get(0).getRoleInfoVoList();
            }
        }

        if (null == roleList || roleList.size() == 0) {
            // 如果是ajax请求 那么返回错误json
            if (RequestUtil.isAsynRequest(request)) {
                // CSOFF: MagicNumber
                response.setStatus(500);
                // CSOFF: MagicNumber
                ResponseUtils.sendJson(new ResponseVo(1, "SessionLost"), response);
            } else {
                ResponseUtils.gotoLogin(response, PropertiesCacheUtil.getValue("system.login.url", PropertieNameConts.SYSTEM));
            }
            logger.info("该用户无任何权限");
            return false;
        }

        // 循环
        for (Role userType : fireAuthority.roles()) {
            for (RoleInfo roleInfo : roleList) {
                // 如果当前用户是权限所允许的 那么放行
                if (userType.getName().toLowerCase().equals(roleInfo.getName().toLowerCase())) {
                    logger.info("具备权限,放行");
                    return true;
                }
            }
        }

        // 返回客户端没有权限访问的信息
        // 如果是异步请求 则返回json数据
        if (RequestUtil.isAsynRequest(request)) {
            ResponseUtils.sendJson(new ResponseVo(1, tipProperties.getProperty("user.authority.null")), response);
        } else {
            ResponseUtil.gotoLogin(response, sysProperties.getProperty("system.login.url"));
        }
        return false;*/
    }
}