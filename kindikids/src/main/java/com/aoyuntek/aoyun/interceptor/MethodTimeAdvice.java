package com.aoyuntek.aoyun.interceptor;

import java.io.OutputStream;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StopWatch;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.theone.json.util.JsonUtil;

/**
 * 方法执行时长以及日志参数打印
 * 
 * @author dlli5 at 2016年9月12日下午8:33:33
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public class MethodTimeAdvice implements MethodInterceptor {

	private static final Logger Log = LoggerFactory.getLogger("MethodLog");

	private static final String FORMAT = "=====>>>>> Takes:{0} ms [{1}.{2}({3})\r\n({4})]";

	@Override
	public Object invoke(MethodInvocation invocation) throws Throwable {
		// 用 commons-lang 提供的 StopWatch 计时
		StopWatch clock = new StopWatch();
		clock.start(); // 计时开始
		Object result = invocation.proceed();
		clock.stop(); // 计时结束
		// 方法参数类型，转换成简单类型
		Class[] params = invocation.getMethod().getParameterTypes();
		String[] simpleParams = new String[params.length];
		for (int i = 0; i < params.length; i++) {
			simpleParams[i] = params[i].getSimpleName();
		}
		Object[] args = invocation.getArguments();
		List<Object> array = new ArrayList<Object>();
		for (Object obj : args) {
			if (obj instanceof Logger) {
				continue;
			}
			if (obj instanceof OutputStream) {
				continue;
			}
			if (obj instanceof org.apache.log4j.Logger) {
				continue;
			}
			if (obj instanceof MultipartHttpServletRequest) {
				continue;
			}

			if (obj instanceof HttpServletRequest) {
				continue;
			}
			if (obj instanceof HttpServletResponse) {
				continue;
			}
			if (obj != null) {
				array.add(JsonUtil.objectToJson(obj));
			} else {
				array.add(obj);
			}
		}
		Log.info(MessageFormat.format(FORMAT, clock.getTotalTimeMillis(), invocation.getThis().getClass().getName(), invocation.getMethod().getName(),
				StringUtils.join(simpleParams, ","), StringUtils.join(array.toArray(), ",")));
		return result;
	}

}
