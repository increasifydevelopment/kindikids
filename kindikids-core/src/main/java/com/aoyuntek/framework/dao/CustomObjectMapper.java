package com.aoyuntek.framework.dao;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializerProvider;
import org.codehaus.jackson.map.ser.CustomSerializerFactory;

import com.aoyuntek.aoyun.constants.DateFormatterConstants;

/**
 * 解决Date类型返回json格式为自定义格式
 * 
 * @author big at 2014年2月26日下午3:45:39
 * @Email lidaliang12345@126.com
 * @QQ 386115312
 */
public class CustomObjectMapper extends ObjectMapper {

    public CustomObjectMapper() {
        CustomSerializerFactory factory = new CustomSerializerFactory();
        factory.addGenericMapping(Date.class, new JsonSerializer<Date>() {
            @Override
            public void serialize(Date value, JsonGenerator jsonGenerator, SerializerProvider provider) throws IOException, JsonProcessingException {
                // 出去
                SimpleDateFormat sdf = new SimpleDateFormat(DateFormatterConstants.SYS_T_FORMAT);
                jsonGenerator.writeString(sdf.format(value));
            }
        });
        // factory.findPropertyTypeSerializer(baseType, config, accessor, property)
        this.setSerializerFactory(factory);
    }
}