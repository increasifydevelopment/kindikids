package com.aoyuntek.framework.service.impl;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.aoyuntek.framework.condtion.BaseCondition;
import com.aoyuntek.framework.controller.BaseController;
import com.aoyuntek.framework.dao.BaseMapper;
import com.aoyuntek.framework.service.IBaseService;
import com.aoyuntek.framework.spring.SpringApplicationContextHolder;
import com.theone.common.util.ServiceResult;

/**
 * @description 业务逻辑层是实现
 * @author xdwang
 * @create 2015年12月13日下午2:08:25
 * @version 1.0
 * @param <T>
 *            实体
 * @param <M>
 *            实体Mapper
 */
public class BaseService<T, M> implements IBaseService<T, M> {

	/**
	 * 日志实例
	 */
	protected static final Log log = LogFactory.getLog(BaseController.class);

	/**
	 * 数据访问层接口
	 */
	private BaseMapper<T> baseMapper;

	/**
	 * Class
	 */
	private Class<T> clazz;

	@SuppressWarnings("unchecked")
	public BaseService() {
		if (clazz == null) {
			clazz = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[1];
		}
		if (baseMapper == null) {
			baseMapper = (BaseMapper<T>) SpringApplicationContextHolder.getWebApplicationContext().getBean(clazz);
		}
	}

	@Override
	public int insert(T record) {
		return baseMapper.insert(record);
	}

	@Override
	public int insertSelective(T record) {
		return baseMapper.insertSelective(record);
	}

	@Override
	public T selectByPrimaryKey(String id) {
		return baseMapper.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(T record) {
		return baseMapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(T record) {
		return baseMapper.updateByPrimaryKey(record);
	}

	@Override
	public List<T> list() {
		return baseMapper.list();
	}

	@Override
	public List<T> getList(BaseCondition conditon) {
		return baseMapper.getList(conditon);
	}

	@Override
	public int getListCount(BaseCondition conditon) {
		return baseMapper.getListCount(conditon);
	}

	@Override
	public int delete(List<String> ids) {
		return baseMapper.delete(ids);
	}

	@Override
	public int deleteByPrimaryKey(String id) {
		return baseMapper.deleteByPrimaryKey(id);
	}

	/**
	 * 
	 * @author dlli5 at 2016年5月4日上午9:50:49
	 * @param obj
	 * @param msg
	 * @return
	 */
	public <O> ServiceResult<O> successResult(String msg, O obj) {
		ServiceResult<O> result = new ServiceResult<O>();
		result.setCode(0);
		result.setSuccess(true);
		result.setReturnObj(obj);
		result.setMsg(msg);
		return result;
	}

	/**
	 * 
	 * @author dlli5 at 2016年5月4日上午9:51:14
	 * @param obj
	 * @return
	 */
	public <O> ServiceResult<O> successResult(O obj) {
		return successResult("", obj);
	}

	/**
	 * 
	 * @author dlli5 at 2016年5月4日上午9:51:32
	 * @param msg
	 * @return
	 */
	public <O> ServiceResult<O> successResult(String msg) {
		return successResult(msg, null);
	}

	public <O> ServiceResult<O> successResult2(Object code, String msg) {
		ServiceResult<O> result = new ServiceResult<O>();
		result.setCode(code);
		result.setSuccess(true);
		result.setMsg(msg);
		return result;
	}

	/**
	 * 
	 * @author dlli5 at 2016年5月4日上午9:51:47
	 * @return
	 */
	public <O> ServiceResult<O> successResult() {
		return successResult(null, null);
	}

	/**
	 * 
	 * @author dlli5 at 2016年5月4日上午9:52:52
	 * @param obj
	 * @param msg
	 * @return
	 */
	public <O> ServiceResult<O> failResult(Object code, String msg, O obj) {
		ServiceResult<O> result = new ServiceResult<O>();
		result.setSuccess(false);
		result.setCode(code);
		result.setReturnObj(obj);
		result.setMsg(msg);
		return result;
	}

	public <O> ServiceResult<O> failResult(Object code, O obj) {
		return failResult(code, null, obj);
	}

	/**
	 * 
	 * @author dlli5 at 2016年5月4日上午9:53:33
	 * @param obj
	 * @return
	 */
	public <O> ServiceResult<O> failResult(Object code) {
		return failResult(code, null, null);
	}

	/**
	 * 
	 * @author dlli5 at 2016年5月4日上午9:53:33
	 * @param obj
	 * @return
	 */
	public <O> ServiceResult<O> failResult2(O data) {
		return failResult(1, null, data);
	}

	/**
	 * 
	 * @description
	 * @author bbq
	 * @create 2016年6月17日下午5:45:44
	 * @version 1.0
	 * @param msg
	 * @return
	 */
	public <O> ServiceResult<O> failResult(String msg) {
		return failResult(1, msg, null);
	}

	/**
	 * 
	 * @author dlli5 at 2016年5月4日上午9:53:54
	 * @param msg
	 * @return
	 */
	public <O> ServiceResult<O> failResult(Object code, String msg) {
		return failResult(code, msg, null);
	}

}
