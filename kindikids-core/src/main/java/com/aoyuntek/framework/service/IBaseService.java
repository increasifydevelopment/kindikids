package com.aoyuntek.framework.service;

import java.util.List;

import com.aoyuntek.framework.condtion.BaseCondition;

/**
 * @description Serviet基类
 * @author xdwang
 * @create 2015年11月16日下午4:09:00
 * @version 1.0
 * @param <T>
 *            实体
 * @param <M>
 *            实体Mapper
 */
public interface IBaseService<T, M> {
	/**
	 * @description 插入方法
	 * @author xdwang
	 * @create 2015年11月16日下午3:58:33
	 * @version 1.0
	 * @param record
	 *            需要插入的实体
	 * @return 返回插入的id
	 */
	int insert(T record);

	/**
	 * @description 插入方法，为空字段不插入
	 * @author xdwang
	 * @create 2015年11月16日下午3:57:55
	 * @version 1.0
	 * @param record
	 *            需要插入的实体
	 * @return 返回插入的id
	 */
	int insertSelective(T record);

	/**
	 * @description 根据主键id获取实体
	 * @author xdwang
	 * @create 2015年11月16日下午3:58:49
	 * @version 1.0
	 * @param id
	 *            主键id
	 * @return 对应的实体
	 */
	T selectByPrimaryKey(String id);

	/**
	 * @description 更新实体，为空字段不更新
	 * @author xdwang
	 * @create 2015年11月16日下午3:59:12
	 * @version 1.0
	 * @param record
	 *            更新的实体
	 * @return 返回影响行数
	 */
	int updateByPrimaryKeySelective(T record);

	/**
	 * @description 更新实体
	 * @author xdwang
	 * @create 2015年11月16日下午4:05:28
	 * @version 1.0
	 * @param record
	 *            更新的实体
	 * @return 返回影响行数
	 */
	int updateByPrimaryKey(T record);

	/**
	 * @description 获取所有数据集合
	 * @author xdwang
	 * @create 2015年11月16日下午4:06:03
	 * @version 1.0
	 * @return 返回所有数据集合
	 */
	List<T> list();

	/**
	 * @description 获取符合条件的数据集合
	 * @author xdwang
	 * @create 2015年11月16日下午4:06:30
	 * @version 1.0
	 * @param conditon
	 *            查询条件对象
	 * @return 返回符合条件的数据集合
	 */
	List<T> getList(BaseCondition conditon);

	/**
	 * @description 获取符合条件的数据数量
	 * @author xdwang
	 * @create 2015年11月16日下午4:07:13
	 * @version 1.0
	 * @param conditon
	 *            查询条件对象
	 * @return 返回符合条件的数据数量
	 */
	int getListCount(BaseCondition conditon);

	/**
	 * @description 批量删除实体
	 * @author xdwang
	 * @create 2015年11月16日下午4:08:06
	 * @version 1.0
	 * @param ids
	 *            主键id集合
	 * @return 影响行数
	 */
	int delete(List<String> ids);

	/**
	 * @description 根据主键id删除实体
	 * @author xdwang
	 * @create 2015年11月16日下午4:07:44
	 * @version 1.0
	 * @param id
	 *            主键id
	 * @return 返回影响行数
	 */
	int deleteByPrimaryKey(String id);
}
