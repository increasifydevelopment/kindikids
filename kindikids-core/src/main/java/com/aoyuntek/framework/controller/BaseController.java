package com.aoyuntek.framework.controller;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @description 基类
 * @author xdwang
 * @create 2015年12月10日下午7:52:02
 * @version 1.0
 */
public class BaseController {

    /**
     * Logger
     */
    public static Log log = LogFactory.getLog(BaseController.class);

    public void logRequest(HttpServletRequest request) {
        @SuppressWarnings("unchecked")
        Enumeration<String> enumeration = request.getParameterNames();
        StringBuilder sb = new StringBuilder();
        while (enumeration.hasMoreElements()) {
            String string = (String) enumeration.nextElement();
            sb.append("{name:" + string + ",value:" + request.getParameter(string) + "}|");
        }
        log.info(sb.toString());
    }

}
