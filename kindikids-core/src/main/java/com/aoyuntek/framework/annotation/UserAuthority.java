package com.aoyuntek.framework.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.aoyuntek.aoyun.enums.Role;

/**
 * 用户权限注解
 * @author dlli5 at 2016年4月28日上午8:48:27
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface UserAuthority {
	Role[] roles();
	/*ResultTypeEnum resultType() default ResultTypeEnum.page;*/
}