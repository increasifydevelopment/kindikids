package com.aoyuntek.framework.spring;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @description Spring应用上下文环境
 * @author xdwang
 * @create 2015年11月22日上午10:25:09
 * @version 1.0
 */
@Component
public class SpringApplicationContextHolder implements ApplicationContextAware {

    /**
     * 日志
     */
    protected static Log logger = LogFactory.getLog(SpringApplicationContextHolder.class);

    /**
     * Spring应用上下文环境
     */
    private static ApplicationContext applicationContext;

    public static ApplicationContext getWebApplicationContext() {
        return applicationContext;
    }

    @Override
    public void setApplicationContext(ApplicationContext arg0) throws BeansException {
        applicationContext = arg0;
    }
}
