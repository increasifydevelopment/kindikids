package com.aoyuntek.aoyun.uitl;

import java.io.Serializable;
import java.util.List;

public class OutPutUtil<T> implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void printOutList(List<T> list) {
		for (T obj : list) {
			System.err.println(obj);
		}
	}

	public void printOutList2(List list) {
		for (Object obj : list) {
			System.err.println(obj);
		}
	}

	public void printModel(T model) {
		System.err.println(model);
	}
}
