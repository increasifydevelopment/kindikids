package com.aoyuntek.aoyun.uitl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import org.xhtmlrenderer.context.StyleReference;
import org.xhtmlrenderer.pdf.ITextFontResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.lowagie.text.DocumentException;

/**
 * @description PDF 工具类
 * @author xdwang
 * @create 2016年3月24日上午10:51:57
 * @version 1.0
 */
public class PDFUtil {

    /**
     * ITextRenderer
     */
    static ITextRenderer renderer = new ITextRenderer();

    /**
     * ITextFontResolver
     */
    static ITextFontResolver fontResolver = renderer.getFontResolver();

    // static {
    // try {
    // fontResolver.addFont("C:\\Windows\\fonts\\simsun.ttc",
    // BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
    // } catch (DocumentException e) {
    // e.printStackTrace();
    // } catch (IOException e) {
    // e.printStackTrace();
    // }
    // }

    /**
     * @description 根据html生成pdf
     * @author xdwang
     * @create 2016年3月24日上午10:52:27
     * @version 1.0
     * @param html
     *            html内容
     * @param outStream
     *            OutputStream
     * @throws DocumentException
     *             异常
     * @throws IOException
     *             异常
     */
    public static void convertPDF(String html, OutputStream outStream) throws DocumentException, IOException {
        // DOCTYPE 必需写否则类似于 这样的字符解析会出现错误
        renderer.setDocumentFromString(html.toString());
        // 解决图片的相对路径问题
        renderer.layout();
        renderer.createPDF(outStream);
        outStream.close();
    }

    /**
     * 
     * @description 多html转pdf
     * @author gfwang
     * @create 2016年5月8日上午10:06:17
     * @version 1.0
     * @param files
     *            html文件
     * @param os
     *            输出流
     * @throws Exception
     *             异常
     */
    public static void htmls2Pdf(final List<File> files, final OutputStream os) throws Exception {
        ITextRenderer renderer = new ITextRenderer();
        StyleReference css = new StyleReference(renderer.getSharedContext().getUac());
        renderer.getSharedContext().setCss(css);
        int htmlCount = files.size();
        for (int i = 0; i < htmlCount; i++) {
            File file = files.get(i);
            renderer.setDocument(file);
            renderer.layout();
            // if (i != (htmlCount - 1)) {
            renderer.createPDF(os, true);
            // } else {
            // CSOFF: MagicNumber
            // renderer.writeNextDocument(15);
            // CSOFF: MagicNumber
            renderer.finishPDF();
            // }
            if (file.exists()) {
                file.delete();
            }
        }
        os.flush();
        os.close();
    }

    /**
     * 
     * @description 单html转pdf
     * @author gfwang
     * @create 2016年5月8日上午10:06:17
     * @version 1.0
     * @param files
     *            html文件
     * @param outputName
     *            输出名
     * @throws Exception
     *             异常
     */
    public static void singleConvertPDF(final File file, final String outputName) throws Exception {
        OutputStream os = new FileOutputStream(outputName);
        ITextRenderer renderer = new ITextRenderer();
        StyleReference css = new StyleReference(renderer.getSharedContext().getUac());
        renderer.getSharedContext().setCss(css);
        renderer.setDocument(file);
        renderer.layout();
        renderer.createPDF(os, false);
        renderer.finishPDF();
        file.deleteOnExit();
        os.flush();
        os.close();
    }
}
