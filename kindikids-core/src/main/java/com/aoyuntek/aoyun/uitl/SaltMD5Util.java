package com.aoyuntek.aoyun.uitl;

import com.theone.secure.util.SHAUtil;

public class SaltMD5Util {
    public static String saltMd5(String salt, String pwd) {
        //update tbl_account set psw='AAF17874D420EB6606B298E010F3E2A4DEC42C5D2C5A93CE81893F6FE9F0138713C80E2900CC770EE0A6E3F786F70EC3582ED1BEED211CDA3200D1DED3EA2FB2
        try {
            return SHAUtil.encodeSHA512((pwd + salt).getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
