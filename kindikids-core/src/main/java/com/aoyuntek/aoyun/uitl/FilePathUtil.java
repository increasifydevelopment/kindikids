package com.aoyuntek.aoyun.uitl;

public class FilePathUtil {
    /**
     * 临时目录
     */
    public static final String TEMP_PATH = "temp/";
    /**
     * 主目录
     */
    public static final String MAIN_PATH = "main/";
    /**
     * 主目录
     */
    public static final String CENTER_MANAGER = "center/";
    /**
     * 主目录
     */
    public static final String PROGRAM_PATH = "program/";
    /**
     * 图片目录
     */
    public static final String IMG_PATH = "img/";
    /**
     * 表单目录
     */
    public static final String FORM_PATH = "form/";

    /**
     * policy
     */
    public static final String POLICY_PATH = "policy/";

    /**
     * event
     */
    public static final String EVENT_PATH = "event/";

    public static final String NewsFeed_PATH = "newsfeed/";

    public static final String MESSAGE_PATH = "message/";

    public static final String STAFF_PATH = "staff/";

    public static final String FAMILY_PATH = "family/";

    public static final String AVATAR_CHILD = "avatar_child/";
    public static final String AVATAR_STAFF = "avatar_staff/";

    public static String getTempPath(String fileIdName) {
        return TEMP_PATH + fileIdName;
    }

    /**
     * 
     * @description 临时目录转主目录
     * @author gfwang
     * @create 2016年9月21日上午9:13:26
     * @version 1.0
     * @param mainPath
     *            目标主目录
     * @param tempRelativePath
     *            临时目录
     * @return
     */
    public static String getMainPath(String mainPath, String tempRelativePath) {
        String fileName = tempRelativePath.substring(tempRelativePath.lastIndexOf("/") + 1);
        return mainPath + fileName;
    }

    public static String getCenterPath(String tempRelativePath) {
        String fileName = tempRelativePath.substring(tempRelativePath.lastIndexOf("/") + 1);
        return CENTER_MANAGER + fileName;
    }

    public static String getImgPath(String imgIdName) {
        return IMG_PATH + imgIdName;
    }
}
