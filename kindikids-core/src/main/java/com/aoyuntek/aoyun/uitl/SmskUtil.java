package com.aoyuntek.aoyun.uitl;

import org.apache.log4j.Logger;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.theone.aws.util.SmsUtil;
import com.theone.string.util.StringUtil;

public class SmskUtil {
    // 默认大小基数
    private final int baseLength = 700;
    private static Logger log = Logger.getLogger("SmskUtil");

    public void splitSendMessage(String message, String phoneNumber) {
        StringBuffer sb = new StringBuffer();
        if (StringUtil.isEmpty(message)) {
            log.warn("message is empty|phoneNumber=" + phoneNumber);
            return;
        }
        message = message.trim();
        int length = message.length();
        log.info("message.length=" + length);
        log.info("send message start|message=" + message + "|phoneNumber=" + phoneNumber);
        // 如果在1条短信内容范围直接发送
        if (length < baseLength) {
            SmsUtil sms = new SmsUtil(Region.getRegion(Regions.AP_SOUTHEAST_2));
            sms.sendSms(message, phoneNumber);
            log.info("send message success 1");
            return;
        }
        int size = getUpNumber(length, baseLength);
        int startIndex = 0;
        for (int i = 1; i <= size; i++) {
            SmsUtil sms = new SmsUtil(Region.getRegion(Regions.AP_SOUTHEAST_2));
            int endIndex = baseLength * i;
            String content = null;
            if (endIndex > length) {
                content = message.substring(startIndex, length);
            } else {
                content = message.substring(startIndex, endIndex);
            }
            sms.sendSms(content, phoneNumber);
            log.info("------------send " + i + " ok--------------");
            log.info(content);
            log.info("--------------------------------------------");
            sb.append(content);
            startIndex = endIndex;
        }
        log.info("send message success 2|" + sb.toString());
    }

    private int getUpNumber(int num1, int num2) {
        return (int) Math.ceil(num1 * 1.0 / num2);
    }
}
