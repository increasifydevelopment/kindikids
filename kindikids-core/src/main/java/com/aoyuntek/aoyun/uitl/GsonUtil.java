package com.aoyuntek.aoyun.uitl;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

public class GsonUtil{
	

	private static Gson gson = null;
	static {
		if (gson == null) {
			gson = new Gson();
		}
	}
	
	public static Object jsonToBean(String jsonStr, Type type, final String... pattern) {
		Object obj = null;
		gson = new GsonBuilder().registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
			public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
				for (String string : pattern) {
					SimpleDateFormat format = new SimpleDateFormat(string);
					String dateStr = json.getAsString();
					try {
						return format.parse(dateStr);
					} catch (ParseException e) {
						//e.printStackTrace();
					}
				}
				return null;
			}
		}).create();
		if (gson != null) {
			obj = gson.fromJson(jsonStr, type);
		}
		return obj;
	}
}
