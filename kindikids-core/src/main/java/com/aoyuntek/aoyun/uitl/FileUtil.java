package com.aoyuntek.aoyun.uitl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;

/**
 * @description 文件操作
 * @author xdwang
 * @create 2015年12月22日下午9:39:06
 * @version 1.0
 */
public class FileUtil {

    /**
     * 日志
     */
    public static Logger logger = Logger.getLogger(FileUtil.class);

    /**
     * @description 获取文件后缀名
     * @author xdwang
     * @create 2015年12月22日下午9:39:20
     * @version 1.0
     * @param filename
     *            文件名
     * @return 后缀名
     */
    public static String getExtensionName(String filename) {
        if ((filename != null) && (filename.length() > 0)) {
            int dot = filename.lastIndexOf('.');
            if ((dot > -1) && (dot < (filename.length() - 1))) {
                return filename.substring(dot + 1);
            }
        }
        return filename;
    }

    /**
     * @description 递归建文件夹
     * @author xdwang
     * @create 2016年1月5日下午5:50:29
     * @version 1.0
     * @param file
     *            File对象
     */
    public static void mkDir(File file) {
        if (file.getParentFile().exists()) {
            file.mkdir();
        } else {
            mkDir(file.getParentFile());
            file.mkdir();
        }
    }

    /**
     * @description 读取文件文本
     * @author xdwang
     * @create 2015年12月22日下午9:39:42
     * @version 1.0
     * @param filePath
     *            文件路径
     * @return 文件内容
     * @throws IOException
     *             异常
     */
    public static String readTxt(String filePath) throws IOException {
        StringBuilder sb = new StringBuilder();
        // 构造一个BufferedReader类来读取文件
        BufferedReader br = new BufferedReader(new FileReader(filePath));
        String s = null;
        try {
            // 使用readLine方法，一次读一行
            int count = 0;
            while ((s = br.readLine()) != null) {
                if (count == 0) {
                    sb.append(s);
                } else {
                    sb.append("\n" + s);
                }
                count++;
            }
        } catch (Exception e) {
            logger.error(e.toString());
        } finally {
            br.close();
        }
        return sb.toString();
    }

    /**
     * 
     * @description html字符串转html文件
     * @author gfwang
     * @create 2016年5月8日上午10:54:50
     * @version 1.0
     * @param html
     *            html字符串
     * @param filePath
     *            文件路径
     * @return File对象
     * @throws Exception
     *             文件写入异常
     */
    public static File getFile(String html, String filePath) throws Exception {
        String baseDir = filePath + UUID.randomUUID().toString() + ".html";
        // 建立文件输出流
        FileOutputStream fileoutputstream = new FileOutputStream(baseDir);
        fileoutputstream.write(html.getBytes());
        fileoutputstream.close();
        return new File(baseDir);
    }

    public static void writeFile1(String pcStr, String pcStrDB) {
        // StringBuffer sb = new StringBuffer("\r\n[");
        // sb.append(DateUtil.format(new Date(), DateUtil.yyyyMMddHHmmssSpt));
        // sb.append("]\t");
        // sb.append(txt);
        // try {
        // FileWriter w = new FileWriter(filePath, isAppend);
        // w.write(sb.toString());
        // w.close();
        // } catch (IOException e) {
        // System.err.println("writeFile error");
        // e.printStackTrace();
        // }
        String[] pcs = pcStr.split(",");
        String[] pcsDB = pcStrDB.split(",");
        List<String> pcsDBList = new ArrayList<String>();
        Collections.addAll(pcsDBList, pcsDB);
        for (int i = 0; i < pcs.length; i++) {
            if (!pcsDBList.contains(pcs[i])) {
                pcsDBList.add(pcs[i]);
            }
        }
        StringBuffer newPcs = new StringBuffer();
        for (int i = 0; i < pcsDBList.size(); i++) {
            newPcs.append(pcsDBList.get(i));
        }

    }

    /**
     * @description 删除子目录
     * @author hxzhang
     * @create 2016年7月25日下午1:41:14
     * @version 1.0
     * @param path
     *            路径
     */
    public static void deleteAllFilesOfDir(File path) {
        if (!path.exists())
            return;
        if (path.isFile()) {
            path.delete();
            return;
        }
        File[] files = path.listFiles();
        for (int i = 0; i < files.length; i++) {
            deleteAllFilesOfDir(files[i]);
        }
        path.delete();
    }
}
