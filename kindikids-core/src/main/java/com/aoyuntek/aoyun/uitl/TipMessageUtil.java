package com.aoyuntek.aoyun.uitl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.aoyuntek.aoyun.constants.DateFormatterConstants;
import com.aoyuntek.aoyun.constants.TipMsgConstants;
import com.aoyuntek.aoyun.entity.po.ChangeAttendanceRequestInfo;
import com.aoyuntek.aoyun.entity.po.ChildAttendanceInfo;
import com.aoyuntek.aoyun.entity.po.SubtractedAttendance;
import com.aoyuntek.aoyun.entity.po.TemporaryAttendanceInfo;
import com.aoyuntek.aoyun.enums.AttendanceRequestType;
import com.aoyuntek.aoyun.enums.ChangeAttendanceRequestDayState;
import com.aoyuntek.aoyun.enums.TemporaryType;
import com.theone.date.util.DateUtil;
import com.theone.string.util.StringUtil;

@Component
public class TipMessageUtil {

    @Value("#{tip}")
    private Properties tipProperties;
    @Value("#{sys}")
    private Properties sysProperties;
    @Value("#{email}")
    private Properties emailProperties;

    public String getSystemMsg(String tipKey) {
        return sysProperties.getProperty(tipKey);
    }

    public String getTipMsg(String tipKey) {
        return tipProperties.getProperty(tipKey);
    }

    /**
     * 
     * @author dlli5 at 2016年9月10日上午10:56:22
     * @param attendanceRequestInfo
     * @return
     */
    public String getRequestLogStr(ChangeAttendanceRequestInfo attendanceRequestInfo,String centreName,String roomName) {
        List<String> list = new ArrayList<String>();
        if (attendanceRequestInfo.getMonday() == ChangeAttendanceRequestDayState.Check.getValue()) {
            list.add(TipMsgConstants.WEEKITEMS[0]);
        }
        if (attendanceRequestInfo.getTuesday() == ChangeAttendanceRequestDayState.Check.getValue()) {
            list.add(TipMsgConstants.WEEKITEMS[1]);
        }
        if (attendanceRequestInfo.getWednesday() == ChangeAttendanceRequestDayState.Check.getValue()) {
            list.add(TipMsgConstants.WEEKITEMS[2]);
        }
        if (attendanceRequestInfo.getThursday() == ChangeAttendanceRequestDayState.Check.getValue()) {
            list.add(TipMsgConstants.WEEKITEMS[3]);
        }
        if (attendanceRequestInfo.getFriday() == ChangeAttendanceRequestDayState.Check.getValue()) {
            list.add(TipMsgConstants.WEEKITEMS[4]);
        }
        String dateStr=DateUtil.format(attendanceRequestInfo.getChangeDate(), DateFormatterConstants.SYS_FORMAT2);
        if (attendanceRequestInfo.getType() == AttendanceRequestType.C.getValue()) {
            return MessageFormat.format(getSystemMsg("RequestLogType.ChangeAttendanceRequest.c"), centreName,dateStr);
        } else if (attendanceRequestInfo.getType() == AttendanceRequestType.R.getValue()) {
            return MessageFormat.format(getSystemMsg("RequestLogType.ChangeAttendanceRequest.r"), roomName,dateStr);
        } else {
            return MessageFormat.format(getSystemMsg("RequestLogType.ChangeAttendanceRequest.k"), dealWeekCheckStr(list),dateStr);
        }
    }

    /**
     * 
     * @author dlli5 at 2016年9月10日上午10:56:26
     * @param weekCheckList
     * @return
     */
    private String dealWeekCheckStr(List<String> weekCheckList) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < weekCheckList.size(); i++) {
            if (i == weekCheckList.size() - 2) {
                sb.append(weekCheckList.get(i) + " and ");
            } else {
                sb.append(weekCheckList.get(i) + ",");
            }
        }
        return StringUtil.trimEnd(sb.toString(), ",");
    }

    /**
     * 
     * @author dlli5 at 2016年9月10日上午10:56:33
     * @param days
     * @return
     */
    private String getAlertStr(boolean... days) {
        List<String> weekCheckList = new ArrayList<String>();
        for (int i = 0; i < days.length; i++) {
            if (days[i]) {
                weekCheckList.add(TipMsgConstants.WEEKNAMES[i]);
            }
        }
        String temp = dealWeekCheckStr(weekCheckList);
        if (temp.endsWith(",")) {
            temp = StringUtil.trimEnd(temp, ",");
        }
        return MessageFormat.format(getTipMsg("submitChangeAttendance.deal.week.attend"), temp);
    }

    /**
     * 
     * @author dlli5 at 2016年9月10日上午10:56:36
     * @param attendanceInfo
     * @return
     */
    public String getAlertForAttendance(ChildAttendanceInfo attendanceInfo, short dayOfWeek) {
        boolean[] array = new boolean[5];
        if (attendanceInfo.getMonday() && dayOfWeek != 1) {
            array[0] = true;
        }
        if (attendanceInfo.getTuesday() && dayOfWeek != 2) {
            array[1] = true;
        }
        if (attendanceInfo.getWednesday() && dayOfWeek != 3) {
            array[2] = true;
        }
        if (attendanceInfo.getThursday() && dayOfWeek != 4) {
            array[3] = true;
        }
        if (attendanceInfo.getFriday() && dayOfWeek != 5) {
            array[4] = true;
        }
        return getAlertStr(array);
    }

    public String getRequestLogStr(TemporaryAttendanceInfo temp) {
        if (temp.getType() == TemporaryType.Temporary.getValue()) {
            return MessageFormat.format(getSystemMsg("RequestLogType.ChangeAttendanceRequest.TemporaryAttendanceInfo.Temporary"),
                    TipMsgConstants.WEEKITEMS[temp.getDayWeek() - 1], DateUtil.format(temp.getBeginTime(), DateFormatterConstants.SYS_FORMAT2),
                    DateUtil.format(temp.getEndTime(), DateFormatterConstants.SYS_FORMAT2));
        } else {
            return MessageFormat.format(getSystemMsg("RequestLogType.ChangeAttendanceRequest.TemporaryAttendanceInfo.All"),
                    TipMsgConstants.WEEKITEMS[temp.getDayWeek() - 1], DateUtil.format(temp.getBeginTime(), DateFormatterConstants.SYS_FORMAT2));
        }
    }

    public String getRequestLogStr(SubtractedAttendance subtractedAttendance) {
        if (subtractedAttendance.getEndTime() != null) {
            return MessageFormat.format(getSystemMsg("RequestLogType.ChangeAttendanceRequest.SubtractedAttendance.Temporary"),
                    TipMsgConstants.WEEKITEMS[subtractedAttendance.getDayWeek() - 1],
                    DateUtil.format(subtractedAttendance.getBeginTime(), DateFormatterConstants.SYS_FORMAT2),
                    DateUtil.format(subtractedAttendance.getEndTime(), DateFormatterConstants.SYS_FORMAT2));
        } else {
            return MessageFormat.format(getSystemMsg("RequestLogType.ChangeAttendanceRequest.SubtractedAttendance.All"),
                    TipMsgConstants.WEEKITEMS[subtractedAttendance.getDayWeek() - 1],
                    DateUtil.format(subtractedAttendance.getBeginTime(), DateFormatterConstants.SYS_FORMAT2));
        }
    }
}
