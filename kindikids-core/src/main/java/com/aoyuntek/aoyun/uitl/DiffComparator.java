package com.aoyuntek.aoyun.uitl;

public interface DiffComparator<T, V> {

	int compare(T t, V v);

	boolean equals(T t,V v) ;

}
