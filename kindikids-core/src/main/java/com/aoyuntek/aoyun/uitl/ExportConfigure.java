package com.aoyuntek.aoyun.uitl;

import java.util.ArrayList;
import java.util.List;

/**
 * @description 序列化excel模版类
 * @author hxzhang
 * @create 2016年3月3日下午8:39:53
 * @version 1.0
 */
public class ExportConfigure {
	/**
	 * 游客签到模版
	 */
	public static final List<String> VisitorSign = new ArrayList<String>() {
		/**
		 * 序列化
		 */
		private static final long serialVersionUID = 1L;

		{
			add("firstName");
			add("lastName");
			add("reason");
			add("centre");
			add("organisation");
			add("contactNumber");
			add("date");
			add("signinTime");
			add("signoutTime");
			add("signinName");
			add("signoutName");
		}
	};
	/**
	 * 小孩签到模版
	 */
	public static final List<String> ChildSign = new ArrayList<String>() {
		/**
		 * 序列化
		 */
		private static final long serialVersionUID = 1L;

		{
			add("firstName");
			add("middleName");
			add("lastName");
			add("centre");
			add("room");
			add("sunscreenApp");
			add("date");
			add("signinTime");
			add("signoutTime");
			add("signinName");
			add("signoutName");
		}
	};
	/**
	 * 员工签到模版
	 */
	public static final List<String> StaffSign = new ArrayList<String>() {
		/**
		 * 序列化
		 */
		private static final long serialVersionUID = 1L;

		{
			add("firstName");
			add("middleName");
			add("lastName");
			add("centre");
			add("date");
			add("signinTime");
			add("signoutTime");
			add("RosteredSignIn");
			add("RosteredSignout");
			add("TotalRosteredHours");
			add("type");
		}
	};
	/**
	 * Task Table
	 */
	public static final String[] TaskTable = {
			"Dates",
			"Centre",
			"Room",
			"Task Name",
			"Responsible Person",
			"Task Implementers",
			"Subject Person",
			"NQS",
			"Task Status"
	};
		
}
