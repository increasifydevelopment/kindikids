package com.aoyuntek.aoyun.uitl;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFRelation;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.theone.string.util.StringUtil;

import sun.misc.BASE64Decoder;

/**
 * @description 数据写入
 * @author hxzhang
 * @create 2016年3月3日下午7:43:17
 * @version 1.0
 */
@SuppressWarnings("rawtypes")
public class ExcelHandle {
    /**
     * tempFileMap
     */
    private Map<String, HashMap[]> tempFileMap = new HashMap<String, HashMap[]>();
    /**
     * cellMap
     */
    private Map<String, Map<String, Cell>> cellMap = new HashMap<String, Map<String, Cell>>();
    /**
     * tempStream
     */
    private Map<String, FileInputStream> tempStream = new HashMap<String, FileInputStream>();
    /**
     * tempWorkbook
     */
    private Map<String, SXSSFWorkbook> tempWorkbook = new HashMap<String, SXSSFWorkbook>();
    /**
     * dataWorkbook
     */
    private Map<String, XSSFWorkbook> dataWorkbook = new HashMap<String, XSSFWorkbook>();

    /**
     * 单无格类
     * 
     * @author xiliang.xiao
     * 
     */
    class Cell {
        /**
         * 列
         */
        private int column;
        /**
         * 行
         */
        private int line;
        /**
         * 样式
         */
        private CellStyle cellStyle;

        public int getColumn() {
            return column;
        }

        public void setColumn(int column) {
            this.column = column;
        }

        public int getLine() {
            return line;
        }

        public void setLine(int line) {
            this.line = line;
        }

        public CellStyle getCellStyle() {
            return cellStyle;
        }

        public void setCellStyle(CellStyle cellStyle) {
            this.cellStyle = cellStyle;
        }
    }

    private Sheet wsheet;

    private SXSSFWorkbook temWorkbook;

    /**
     * @description 向Excel中输入相同title的多条数据
     * @author hxzhang
     * @create 2016年3月3日下午7:51:15
     * @version 1.0
     * @param tempFilePath
     *            excel模板文件路径
     * @param cellList
     *            需要填充的数据（模板<!%后的字符串）
     * @param list
     *            填充的数据
     * @param sheet
     *            填充的excel sheet,从0开始
     * @throws IOException
     *             异常
     */
    public void writeListData(String tempFilePath, List<String> cellList, List<Map<String, String>> list, int sheet) throws IOException {
        final String signNameColumn = "signinName;signoutName;";

        // 获取模板填充格式位置等数据
        HashMap temp = getTemp(tempFilePath, sheet);
        // 按模板为写入板
        // XSSFWorkbook wb = getTempWorkbook(tempFilePath);
        temWorkbook = getTempWorkbook(tempFilePath);
        // 获取数据填充开始行
        int startCell = 1;
        // 数据填充的sheet
        wsheet = temWorkbook.getSheetAt(sheet);
        if (list != null && list.size() > 0) {
            for (Map<String, String> map : list) {
                for (String cell : cellList) {
                    // 获取对应单元格数据
                    Cell c = getCell(cell, temp, temWorkbook, tempFilePath);
                    // 取消文字换行
                    c.getCellStyle().setWrapText(false);
                    // 写入数据
                    if (!signNameColumn.contains(cell + ";")) {
                        ExcelTemplate.setValue(wsheet, startCell, c.getColumn(), map.get(cell), c.getCellStyle());
                        continue;
                    }
                    // 写入base64图片
                    String signNameBase64 = map.get(cell);
                    if (StringUtil.isNotEmpty(signNameBase64)) {
                        setValue(startCell, c.getColumn(), c.getCellStyle(), signNameBase64);
                    }
                }
                startCell++;
            }
        }
    }

    /**
     * 
     * @description
     * @author gfwang
     * @create 2017年2月21日下午5:22:51
     * @version 1.0
     * @param row
     *            开始坐标点（单元格左上角的坐标y轴）
     * @param cell
     *            开始坐标点（单元格左上角的坐标x轴）
     * @param cellStyle
     * @param base64Str
     */
    private void setValue(int row, int cell, CellStyle cellStyle, String base64Str) {
        Row rowIn = wsheet.getRow(row);
        if (rowIn == null) {
            rowIn = wsheet.createRow(row);
            if (row > 0) {
                rowIn.setHeight((short) 650);
            }
        }
        org.apache.poi.ss.usermodel.Cell cellIn = rowIn.getCell(cell);
        if (cellIn == null) {
            cellIn = rowIn.createCell(cell);
        }
        if (cellStyle != null) {
            cellIn.setCellStyle(cellStyle);
        }
        setPic(base64Str, cell, row);
    }

    private int picIndex = 0;

    private void setPic(String baseStr, int startX, int satrtY) {
        int size = 1000;
        baseStr = baseStr.split(";base64,")[1];
        temWorkbook.addPicture(getByte4Base64(baseStr), Workbook.PICTURE_TYPE_PNG);
        // 前两个00，是代表单元格内起始位置，这里表示左上角的点；Integer.MAX_VALUE，表示图片的大小，这里应该是撑满整个单元格;
        XSSFClientAnchor anchor = new XSSFClientAnchor(0, 0, size, size, startX, satrtY, startX + 1, satrtY + 1);
        Drawing patri = wsheet.createDrawingPatriarch();
        patri.createPicture(anchor, picIndex);
        picIndex++;
    }

    public byte[] getByte4Base64(String baseStr) {
        BASE64Decoder decode = new BASE64Decoder();
        try {
            byte[] b = decode.decodeBuffer(baseStr);
            for (int i = 0; i < b.length; ++i) {
                if (b[i] < 0) {// 调整异常数据
                    b[i] += 256;
                }
            }
            return b;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Mapped File way MappedByteBuffer 可以在处理大文件时，提升性能
     * 
     * @param filename
     * @return
     * @throws IOException
     */
    public static byte[] toByteArray3(String filename) throws IOException {

        FileChannel fc = null;
        try {
            fc = new RandomAccessFile(filename, "r").getChannel();
            MappedByteBuffer byteBuffer = fc.map(MapMode.READ_ONLY, 0, fc.size()).load();
            System.out.println(byteBuffer.isLoaded());
            byte[] result = new byte[(int) fc.size()];
            if (byteBuffer.remaining() > 0) {
                byteBuffer.get(result, 0, byteBuffer.remaining());
            }
            return result;
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } finally {
            try {
                fc.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @description 获得模板输入流
     * @author hxzhang
     * @create 2016年3月3日下午7:55:07
     * @version 1.0
     * @param tempFilePath
     *            excel模版路径
     * @return 返回流
     * @throws FileNotFoundException
     *             异常
     */
    private FileInputStream getFileInputStream(String tempFilePath) throws FileNotFoundException {
        if (!tempStream.containsKey(tempFilePath)) {
            tempStream.put(tempFilePath, new FileInputStream(tempFilePath));
        }

        return tempStream.get(tempFilePath);
    }

    /**
     * @description 获得输入工作区
     * @author hxzhang
     * @create 2016年3月3日下午7:56:14
     * @version 1.0
     * @param tempFilePath
     *            excel模版路径
     * @return 返回工作表
     * @throws FileNotFoundException
     *             异常
     * @throws IOException
     *             异常
     */
    private SXSSFWorkbook getTempWorkbook(String tempFilePath) throws FileNotFoundException, IOException {
        if (!tempWorkbook.containsKey(tempFilePath)) {
            XSSFWorkbook xwb = new XSSFWorkbook(getFileInputStream(tempFilePath));
            // 获取excel sheet数量
            int sheet = xwb.getNumberOfSheets();
            for (int i = 0; i < sheet; i++) {
                // 获取excel第i页签
                XSSFSheet tempSheet = xwb.getSheetAt(i);
                // 删除excel第i页签的第一行模版
                tempSheet.removeRow(tempSheet.getRow(1));
            }

            tempWorkbook.put(tempFilePath, new SXSSFWorkbook(xwb));
        }
        return tempWorkbook.get(tempFilePath);
    }

    /**
     * @description 获取对应单元格样式等数据数据
     * @author hxzhang
     * @create 2016年3月3日下午7:57:25
     * @version 1.0
     * @param cell
     *            单元格
     * @param tem
     *            map
     * @param wbModule
     *            XSSFWorkbook对象
     * @param tempFilePath
     *            excel模版路径
     * @return 返回结果
     */
    private Cell getCell(String cell, HashMap tem, SXSSFWorkbook wbModule, String tempFilePath) {
        if (!cellMap.get(tempFilePath).containsKey(cell)) {
            Cell c = new Cell();

            int[] pos = ExcelTemplate.getPos(tem, cell);
            if (pos.length > 1) {
                c.setLine(pos[1]);
            }
            c.setColumn(pos[0]);
            c.setCellStyle((ExcelTemplate.getStyle(tem, cell, wbModule)));
            cellMap.get(tempFilePath).put(cell, c);
        }
        return cellMap.get(tempFilePath).get(cell);
    }

    /**
     * @description 获取模板数据
     * @author hxzhang
     * @create 2016年3月3日下午7:58:40
     * @version 1.0
     * @param tempFilePath
     *            模板文件路径
     * @param sheet
     *            sheet
     * @return 返回结果
     * @throws IOException
     *             异常
     */
    private HashMap getTemp(String tempFilePath, int sheet) throws IOException {
        if (!tempFileMap.containsKey(tempFilePath)) {
            tempFileMap.put(tempFilePath, ExcelTemplate.getTemplateFile(tempFilePath));
            cellMap.put(tempFilePath, new HashMap<String, Cell>());
        }
        return tempFileMap.get(tempFilePath)[sheet];
    }

    /**
     * 资源关闭
     * 
     * @param tempFilePath
     *            模板文件路径
     * @param os
     *            输出流
     * @throws IOException
     * @throws FileNotFoundException
     */
    /**
     * @description 资源关闭
     * @author hxzhang
     * @create 2016年3月3日下午7:59:34
     * @version 1.0
     * @param tempFilePath
     *            模板文件路径
     * @param os
     *            输出流
     * @throws FileNotFoundException
     *             异常
     * @throws IOException
     *             异常
     */
    public void writeAndClose(String tempFilePath, OutputStream os) throws FileNotFoundException, IOException {
        if (getTempWorkbook(tempFilePath) != null) {
            getTempWorkbook(tempFilePath).write(os);
            tempWorkbook.remove(tempFilePath);
        }
        if (getFileInputStream(tempFilePath) != null) {
            getFileInputStream(tempFilePath).close();
            tempStream.remove(tempFilePath);
        }
    }

    /**
     * 读取数据后关闭
     * 
     * @param tempFilePath
     */
    /**
     * @description 读取数据后关闭
     * @author hxzhang
     * @create 2016年3月3日下午8:01:02
     * @version 1.0
     * @param tempFilePath
     *            模版路径
     */
    public void readClose(String tempFilePath) {
        dataWorkbook.remove(tempFilePath);
    }

}
