package com.aoyuntek.aoyun.uitl;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.internet.MimeBodyPart;

import org.apache.log4j.Logger;
import org.springframework.web.context.ContextLoader;

import com.aoyuntek.framework.constants.PropertieNameConts;
import com.sendgrid.SendGrid;
import com.sendgrid.SendGrid.Email;
import com.sendgrid.SendGridException;
import com.theone.resource.util.PropertiesCacheUtil;
import com.theone.string.util.StringUtil;

public class EmailUtil {
    public static Logger LOGGER = Logger.getLogger(EmailUtil.class);
    private String emailKey;
    private static SendGrid sendgrid;
    private String senderAddress;
    private String senderName;

    /**
     * @description 获取邮件模版Html
     * @author hxzhang
     * @create 2016年12月29日下午2:15:17
     */
    public static String getEmailHtml() {
        String html = "";
        String filePath = "";
        try {
            filePath = ContextLoader.getCurrentWebApplicationContext().getServletContext().getRealPath("/") + "templet" + File.separator + "email"
                    + File.separator + "email.html";
        } catch (Exception e) {
            filePath = System.getProperty("user.dir") + File.separator + "templet" + File.separator + "email.html";
        }
        try {
            html = FileUtil.readTxt(filePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return html;
    }

    /**
     * 
     * @description 获取占位符
     * @author gfwang
     * @create 2016年4月18日下午5:13:16
     * @version 1.0
     * @param tagName
     *            名称
     * @return name
     */
    public static String getHtmlTag(String tagName) {
        return "\\$\\{" + tagName + "\\}";
    }

    public static String replaceHtml(String title, String content) {
        String html = getEmailHtml();
        html = html.replaceAll(getHtmlTag("title"), title);
        System.err.println(html);
        html = html.replaceAll(getHtmlTag("content"), content);
        html = html.replaceAll(getHtmlTag("rootUrl"), PropertiesCacheUtil.getValue("rootUrl", PropertieNameConts.SYSTEM));
        return html;
    }

    public static void sendgrid(String senderAddress, String senderName, String receiverAddress, String receiverName, String sub, String msg,
            boolean isHtml, String replyTos, Collection attachments, String emailKey, String[] bccs) throws SendGridException {
        LOGGER.info(">>>>>>>>>>>>>>>>send start " + receiverAddress);
        init(emailKey);
        Email email = new Email();
        // 添加秘密抄送人
        email.addBcc(bccs);
        // 收件人只支持同时发1人
        email.addTo(receiverAddress, StringUtil.isEmpty(receiverName) ? "" : receiverName);
        try {
            // 附件
            if (null != attachments) {
                MimeBodyPart attachPart;
                for (Iterator it = attachments.iterator(); it.hasNext();) {
                    attachPart = new MimeBodyPart();
                    FileDataSource fds = new FileDataSource(it.next().toString().trim());
                    attachPart.setDataHandler(new DataHandler(fds));
                    if (fds.getName().indexOf("$") != -1) {
                        attachPart.setFileName(fds.getName().substring(fds.getName().indexOf("$") + 1, fds.getName().length()));
                    } else {
                        attachPart.setFileName(fds.getName());
                    }
                    email.addAttachment(attachPart.getFileName(), attachPart.getInputStream());
                }
            }
            // 发件人
            email.setFrom(senderAddress);
            // 发件人名
            email.setFromName(senderName);
            // 主题
            email.setSubject(sub);
            // 是否为html文本
            if (isHtml) {
                email.setHtml(msg);
            } else {
                email.setText(msg);
            }
            // 答复人
            if (StringUtil.isNotEmpty(replyTos)) {
                email.setReplyTo(replyTos);
            }

        } catch (Exception e) {
            LOGGER.error(">>>>>>>>>>>>>>>>send mail error", e);
        }
        sendgrid.send(email);
        LOGGER.info(">>>>>>>>>>>>>>>>发送邮件成功end");
    }

    private static void init(String emailKey) {
        if (sendgrid == null) {
            sendgrid = new SendGrid(emailKey);
        }
    }
}
