package com.aoyuntek.aoyun.uitl;

import java.util.Calendar;
import java.util.Date;

import com.theone.string.util.StringUtil;

/**
 * @description 计算工具
 * @author hxzhang
 * @create 2016年3月3日下午8:37:19
 * @version 1.0
 */
public class CalcTotalUtil {

    /**
     * @description 计算两个数的和
     * @author hxzhang
     * @create 2016年3月3日下午8:37:46
     * @version 1.0
     * @param arg0
     *            第一个数
     * @param arg1
     *            第二个数
     * @return 和
     */
    public static String calcTotal(String arg0, String arg1) {
        double total = 0;
        double a = 0;
        double b = 0;
        try {
            if (StringUtil.isEmpty(arg0)) {
                a = 0.00;
            } else {
                a = Double.parseDouble(arg0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if (StringUtil.isEmpty(arg1)) {
                b = 0.00;
            } else {
                b = Double.parseDouble(arg1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        total = a + b;

        return String.format("%.2f", total);
    }

    /**
     * @description 计算四个数的和
     * @author hxzhang
     * @create 2016年3月3日下午8:38:14
     * @version 1.0
     * @param arg0
     *            第一个数
     * @param arg1
     *            第二个数
     * @param arg2
     *            第三个数
     * @param arg3
     *            第四个数
     * @return 和
     */
    public static String calcTotal(String arg0, String arg1, String arg2, String arg3) {
        return calcTotal(calcTotal(arg0, arg1), calcTotal(arg2, arg3));
    }

    /**
     * @description
     * @author hxzhang
     * @create 2016年3月3日下午8:38:56
     * @version 1.0
     * @param obj
     *            Object对象
     * @return 返回结果
     */
    public static boolean isCanParseDouble(Object obj) {
        String str = obj.toString();
        if (str.endsWith("d") || str.endsWith("D") || str.endsWith("f") || str.endsWith("F")) {
            return false;
        }
        try {
            if (str.length() >= 2) {
                String f1 = str.substring(0, 1);
                String f2 = str.substring(1, 2);
                if ("0".equals(f1)) {
                    if (!".".equals(f2)) {
                        return false;
                    }
                }
            }
            Double.parseDouble(str);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * 
     * @description 判断时间为周几
     * @author gfwang
     * @create 2016年4月15日下午2:05:04
     * @version 1.0
     * @param date
     *            日期
     * @return 周1234567
     */
    public static int dayForWeek(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        int dayForWeek = 0;
        if (c.get(Calendar.DAY_OF_WEEK) == 1) {
            // CSOFF: MagicNumber
            dayForWeek = 7;
            // CSOFF: MagicNumber
        } else {
            dayForWeek = c.get(Calendar.DAY_OF_WEEK) - 1;
        }
        return dayForWeek;
    }
}
