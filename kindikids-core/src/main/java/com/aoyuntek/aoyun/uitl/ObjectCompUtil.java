package com.aoyuntek.aoyun.uitl;

import java.lang.reflect.Field;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import com.aoyuntek.aoyun.entity.po.LogsDetaileInfo;
import com.theone.string.util.StringUtil;

public class ObjectCompUtil {
    public static List<LogsDetaileInfo> getResult(Object oldObj, Object newObj, String dateFormatter, String logId, String keyIndex,String tagsIndex,String classType,
            Properties feildsProperties) throws IllegalArgumentException, IllegalAccessException {
        Map<String, String> oldMap = getMap(oldObj, dateFormatter);
        Map<String, String> newMap = getMap(newObj, dateFormatter);
        List<LogsDetaileInfo> result = new ArrayList<LogsDetaileInfo>();
        for (Map.Entry<String, String> entry : oldMap.entrySet()) {
            String key = entry.getKey();
            String oldValue = entry.getValue();
            String newValue = newMap.get(key);
            if (oldValue.equals(newValue)) {
                continue;
            }            
            if(StringUtil.isNotEmpty(classType)){
            	key=classType+"_"+key;
            }
            if(StringUtil.isNotEmpty(keyIndex)){
            	key=key+keyIndex;
            }
            String tags = feildsProperties.getProperty(key);
            if(StringUtil.isEmpty(tags)){
            	 continue;
            }
            if(StringUtil.isNotEmpty(tagsIndex)){
            	tags=MessageFormat.format(tags, tagsIndex);
            }
            result.add(new LogsDetaileInfo(UUID.randomUUID().toString(), key, oldValue, newValue, tags,logId));
        }
        return result;
    }

    private static Map<String, String> getMap(Object obj, String dateFormatter) throws IllegalArgumentException, IllegalAccessException {
        Map<String, String> map = new HashMap<String, String>();
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormatter);
        // 得到类对象
        @SuppressWarnings("rawtypes")
        Class clazz = obj.getClass();
        Field[] fileds = clazz.getDeclaredFields();
        for (Field field : fileds) {
            field.setAccessible(true); // 设置些属性是可以访问的
            Object valTemp = field.get(obj);// 得到此属性的值
            String type = field.getGenericType().toString();
            String value = "";
            String name = field.getName();
            if (valTemp == null) {
                map.put(name, value);
                continue;
            }
            if ("class java.util.Date".equals(type)) {
                value = sdf.format(valTemp);
            } else {
                value = valTemp.toString();
            }
            if ("class java.util.List".equals(type)) {
                continue;
            }
            map.put(name, value);
        }
        return map;
    }

}
