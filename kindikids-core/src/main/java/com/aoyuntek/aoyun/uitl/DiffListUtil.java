package com.aoyuntek.aoyun.uitl;

import java.util.ArrayList;
import java.util.List;

public class DiffListUtil<T, V> {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	/**
	 * @param lt T类型对象List
	 * @param lv V类型对象List
	 * @param d 比较器
	 * @param addList 新增List
	 * @param removeList 移除List
	 * @param updateList 更新List
	 */
	public void decompose(List<T> lt, List<V> lv, DiffComparator<T,V> d, List<T> addList, List<V> removeList, List<T> updateList){
		Boolean disposed;
		for (T t : lt) {
			disposed = false;
			for(V v : lv) {
				if(d.equals(t, v)){
					updateList.add(t);
					disposed = true;
					lv.remove(v);
					break;
				}
			}
			if(!disposed){
				addList.add(t);
			}
		}
		removeList.addAll(lv);
	}
	
	public List<T> intersectionT(List<T> lt, List<V> lv, DiffComparator<T,V> d){
		List<T> updateList = new ArrayList<T>();
		for (T t : lt) {
			for(V v : lv) {
				if(d.equals(t, v)){
					updateList.add(t);
				}
			}
		}
		return updateList;
	}
	
	public List<V> intersectionV(List<T> lt, List<V> lv, DiffComparator<T,V> d){
		List<V> updateList = new ArrayList<V>();
		for (V v : lv) {
			for(T t : lt) {
				if(d.equals(t, v)){
					updateList.add(v);
				}
			}
		}
		return updateList;
	}
	
	public List<T> complementLeft(List<T> lt, List<V> lv, DiffComparator<T,V> d){
		for (V v : lv) {
			for(T t : lt) {
				if(d.equals(t, v)){
					lt.remove(t);
				}
			}
		}
		return lt;
	}
	
	public List<V> complementRight(List<T> lt, List<V> lv, DiffComparator<T,V> d){
		for (T t : lt) {
			for(V v : lv) {
				if(d.equals(t, v)){
					lv.remove(v);
				}
			}
		}
		return lv;
	}
}
