package com.aoyuntek.aoyun.uitl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * @description excel工具类
 * @author hxzhang
 * @create 2016年3月3日下午8:16:52
 * @version 1.0
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
public class ExcelTemplate {
    /************************************ XSSF *********************************************/

    /**
     * @description 取得指定单元格行和列
     * @author hxzhang
     * @create 2016年3月3日下午8:35:20
     * @version 1.0
     * @param keyMap
     *            所有单元格行、列集合
     * @param key
     *            单元格标识
     * @return 0：列 1：行（列表型数据不记行，即1无值）
     */
    public static int[] getPos(HashMap keyMap, String key) {
        int[] ret = new int[0];

        String val = (String) keyMap.get(key);

        if (val == null || val.length() == 0) {
            return ret;
        }

        String[] pos = val.split(",");

        if (pos.length == 1 || pos.length == 2) {
            ret = new int[pos.length];
            for (int i0 = 0; i0 < pos.length; i0++) {
                if (pos[i0] != null && pos[i0].trim().length() > 0) {
                    ret[i0] = Integer.parseInt(pos[i0].trim());
                } else {
                    ret[i0] = 0;
                }
            }
        }
        return ret;
    }

    /**
     * @description 读数据模板
     * @author hxzhang
     * @create 2016年3月3日下午8:34:36
     * @version 1.0
     * @param templateFileName
     *            文件路径
     * @return 返回结果
     * @throws IOException
     *             异常
     */
    @SuppressWarnings("deprecation")
    public static HashMap[] getTemplateFile(String templateFileName) throws IOException {
        FileInputStream fis = new FileInputStream(templateFileName);
        File file = new File(templateFileName);
        XSSFWorkbook wbPartModule = new XSSFWorkbook(file.getAbsolutePath());
        int numOfSheet = wbPartModule.getNumberOfSheets();
        HashMap[] templateMap = new HashMap[numOfSheet];
        for (int i = 0; i < numOfSheet; i++) {
            Sheet sheet = wbPartModule.getSheetAt(i);
            templateMap[i] = new HashMap();
            readSheet(templateMap[i], sheet);
        }
        fis.close();
        return templateMap;
    }

    /**
     * @description 读模板数据的样式值置等信息
     * @author hxzhang
     * @create 2016年3月3日下午8:17:38
     * @version 1.0
     * @param keyMap
     *            HashMap对象
     * @param sheet
     *            XSSFSheet对象
     */
    private static void readSheet(HashMap keyMap, Sheet sheet) {
        int firstRowNum = sheet.getFirstRowNum();
        int lastRowNum = sheet.getLastRowNum();

        for (int j = firstRowNum; j <= lastRowNum; j++) {
            Row rowIn = sheet.getRow(j);
            if (rowIn == null) {
                continue;
            }
            int firstCellNum = rowIn.getFirstCellNum();
            int lastCellNum = rowIn.getLastCellNum();
            for (int k = firstCellNum; k <= lastCellNum; k++) {
                // XSSFCell cellIn = rowIn.getCell((short) k);
                Cell cellIn = rowIn.getCell(k);
                if (cellIn == null) {
                    continue;
                }

                int cellType = cellIn.getCellType();
                if (XSSFCell.CELL_TYPE_STRING != cellType) {
                    continue;
                }
                String cellValue = cellIn.getStringCellValue();
                if (cellValue == null) {
                    continue;
                }
                // CSOFF: MagicNumber
                cellValue = cellValue.trim();
                if (cellValue.length() > 2 && "<%".equals(cellValue.substring(0, 2))) {
                    String key = cellValue.substring(2, cellValue.length());
                    String keyPos = Integer.toString(k) + "," + Integer.toString(j);
                    keyMap.put(key, keyPos);
                    keyMap.put(key + "CellStyle", cellIn.getCellStyle());
                } else if (cellValue.length() > 3 && "<!%".equals(cellValue.substring(0, 3))) {
                    String key = cellValue.substring(3, cellValue.length());
                    keyMap.put("STARTCELL", Integer.toString(j));
                    keyMap.put(key, Integer.toString(k));
                    keyMap.put(key + "CellStyle", cellIn.getCellStyle());
                }
            }
        }
    }

    /**
     * @description 获取格式，不适于循环方法中使用，wb.createCellStyle()次数超过4000将抛异常
     * @author hxzhang
     * @create 2016年3月3日下午8:30:57
     * @version 1.0
     * @param keyMap
     *            HashMap
     * @param key
     *            key
     * @param wb
     *            XSSFWorkbook
     * @return 返回结果
     */
    public static CellStyle getStyle(HashMap keyMap, String key, SXSSFWorkbook wb) {
        CellStyle cellStyle = null;

        cellStyle = (CellStyle) keyMap.get(key + "CellStyle");
        // 当字符超出时换行
        cellStyle.setWrapText(true);
        CellStyle newStyle = wb.createCellStyle();
        newStyle.cloneStyleFrom(cellStyle);
        return newStyle;
    }

    /**
     * @description Excel单元格输出
     * @author hxzhang
     * @create 2016年3月3日下午8:29:43
     * @version 1.0
     * @param sheet
     *            XSSFSheet
     * @param row
     *            行
     * @param cell
     *            单元格
     * @param value
     *            Object
     * @param cellStyle
     *            样式
     */
    public static void setValue(Sheet sheet, int row, int cell, Object value, CellStyle cellStyle) {
        Row rowIn = sheet.getRow(row);
        if (rowIn == null) {
            rowIn = sheet.createRow(row);
            if (row > 0) {
                rowIn.setHeight((short) 650);
            }
        }
        Cell cellIn = rowIn.getCell(cell);
        if (cellIn == null) {
            cellIn = rowIn.createCell(cell);
        }
        if (cellStyle != null) {
            cellIn.setCellStyle(cellStyle);
        }
        // 对时间格式进行单独处理
        if (value == null || "".equals(value)) {
            cellIn.setCellValue("");
        } else {
            if (isCellDateFormatted(cellStyle)) {
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                Date date = null;
                try {
                    date = format.parse(value.toString());
                } catch (ParseException e) {
                    cellIn.setCellValue(new XSSFRichTextString(value.toString()));
                }
                if (date == null) {
                    cellIn.setCellValue(value.toString());
                } else {
                    cellIn.setCellValue(date);
                }

            } else {
                cellIn.setCellValue(new XSSFRichTextString(value.toString()));
            }
        }
    }

    /**
     * @description 根据表格样式判断是否为日期格式
     * @author hxzhang
     * @create 2016年3月3日下午8:28:54
     * @version 1.0
     * @param cellStyle
     *            样式
     * @return 返回结果
     */
    public static boolean isCellDateFormatted(CellStyle cellStyle) {
        if (cellStyle == null) {
            return false;
        }
        int i = cellStyle.getDataFormat();
        String f = cellStyle.getDataFormatString();

        return org.apache.poi.ss.usermodel.DateUtil.isADateFormat(i, f);
    }

    /************************************ XSSF *******************************************/

}
