package com.aoyuntek.aoyun.uitl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.aoyuntek.aoyun.entity.vo.CusDateVo;

/**
 * 获取时间段
 * 
 * @author rick
 * 
 */
public class DateUtil extends com.theone.date.util.DateUtil {

	public static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	/**
	 * 返回格式yyyyMMddHHmm
	 * 
	 * @author dlli5 at 2017年1月11日上午8:59:41
	 * @param day
	 * @param Time
	 * @return
	 */
	public static Date merge2DateTime(Date day, Date Time) {
		return DateUtil.parse(DateUtil.format(day, DateUtil.yyyyMMdd) + DateUtil.format(Time, DateUtil.HHmm), DateUtil.yyyyMMddHHmm);
	}

	/**
	 * yyyyMMddHHmmss 此时比上一个多了秒的精确 此方法重复 后续移到TheOneUtil中
	 * 
	 * @author dlli5 at 2017年1月11日上午8:59:44
	 * @param day
	 * @param Time
	 * @return
	 */
	public static Date merge2DateTime2(Date day, Date Time) {
		return DateUtil.parse(DateUtil.format(day, DateUtil.yyyyMMdd) + DateUtil.format(Time, DateUtil.HHmmss), DateUtil.yyyyMMddHHmmss);
	}

	/**
	 * 
	 * @author dlli5 at 2016年12月13日下午7:20:44
	 * @param dateChange
	 * @param dayOfWeeks
	 * @return
	 */
	public static Date getBeginDateOfDateWeek(Date dateChange, int dayOfWeeks) {
		int changeDayOfWeek = DateUtil.getDayOfWeek(dateChange) - 1;
		if (dayOfWeeks < changeDayOfWeek) {
			return DateUtil.addDay(dateChange, 7 - changeDayOfWeek + dayOfWeeks);
		}
		if (dayOfWeeks > changeDayOfWeek) {
			return DateUtil.addDay(dateChange, (dayOfWeeks - changeDayOfWeek));
		}
		return dateChange;
	}

	/**
	 * 判断两个时间段是否有交集
	 * 
	 * @author dlli5 at 2016年10月17日上午9:29:47
	 * @param leftStartDate
	 * @param leftEndDate
	 * @param rightStartDate
	 * @param rightEndDate
	 * @return
	 */
	public static boolean isOverlap(Date leftStartDate, Date leftEndDate, Date rightStartDate, Date rightEndDate) {
		return ((leftStartDate.getTime() >= rightStartDate.getTime()) && leftStartDate.getTime() < rightEndDate.getTime())
				|| ((rightStartDate.getTime() >= leftStartDate.getTime()) && rightStartDate.getTime() < leftEndDate.getTime())
				|| ((leftStartDate.getTime() < rightStartDate.getTime()) && leftEndDate.getTime() > rightStartDate.getTime())
				|| ((rightStartDate.getTime() < leftStartDate.getTime()) && rightEndDate.getTime() > leftStartDate.getTime());
	}

	public static void main(String[] args) {
		/*
		 * Date a = com.theone.date.util.DateUtil.parse("201601040529",
		 * com.theone.date.util.DateUtil.yyyyMMddHHmm); Date b =
		 * com.theone.date.util.DateUtil.parse("201601040530",
		 * com.theone.date.util.DateUtil.yyyyMMddHHmm); Date c =
		 * com.theone.date.util.DateUtil.parse("201601040528",
		 * com.theone.date.util.DateUtil.yyyyMMddHHmm); Date d =
		 * com.theone.date.util.DateUtil.parse("201601040530",
		 * com.theone.date.util.DateUtil.yyyyMMddHHmm);
		 * System.out.println(isOverlap(a, b, c, d));
		 */
		Date startDate = parse("20180630", "yyyyMMdd");
		Date endDate = parse("20170607", "yyyyMMdd");

		System.out.println(getDaysIntervalNew(startDate, endDate));
	}

	/**
	 * 当前季度的开始时间，即2012-01-1 00:00:00
	 * 
	 * @return
	 */
	public static Date getFristDayOfQuarter(Date day) {
		Calendar c = Calendar.getInstance();
		c.setTime(day);
		int currentMonth = c.get(Calendar.MONTH) + 1;
		Date now = null;
		try {
			if (currentMonth >= 1 && currentMonth <= 3)
				c.set(Calendar.MONTH, 0);
			else if (currentMonth >= 4 && currentMonth <= 6)
				c.set(Calendar.MONTH, 3);
			else if (currentMonth >= 7 && currentMonth <= 9)
				c.set(Calendar.MONTH, 6);
			else if (currentMonth >= 10 && currentMonth <= 12)
				c.set(Calendar.MONTH, 9);
			c.set(Calendar.DATE, 1);
			now = parse(format(c.getTime(), yyyyMMdd), yyyyMMdd);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return now;
	}

	/**
	 * 当前季度的结束时间，即2012-03-31 23:59:59
	 * 
	 * @return
	 */
	public static Date getEndDayOfQuarter(Date day) {
		Calendar c = Calendar.getInstance();
		c.setTime(day);
		int currentMonth = c.get(Calendar.MONTH) + 1;
		Date now = null;
		try {
			if (currentMonth >= 1 && currentMonth <= 3) {
				c.set(Calendar.MONTH, 2);
				c.set(Calendar.DATE, 31);
			} else if (currentMonth >= 4 && currentMonth <= 6) {
				c.set(Calendar.MONTH, 5);
				c.set(Calendar.DATE, 30);
			} else if (currentMonth >= 7 && currentMonth <= 9) {
				c.set(Calendar.MONTH, 8);
				c.set(Calendar.DATE, 30);
			} else if (currentMonth >= 10 && currentMonth <= 12) {
				c.set(Calendar.MONTH, 11);
				c.set(Calendar.DATE, 31);
			}
			now = parse(format(c.getTime(), yyyyMMdd), yyyyMMdd);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return now;
	}

	public static String[] dateArr = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October",
			"November", "December" };

	public static List<CusDateVo> getDateSlot() {
		List<CusDateVo> list = new ArrayList<CusDateVo>();
		list.add(new CusDateVo("All Time", "All Time"));
		list.add(new CusDateVo("This Week", getThisWeek()));
		list.add(new CusDateVo("This Month", getThisMonth()));
		// list.add(new CusDateVo("Last month", getLastMonth()));
		// list.add(new CusDateVo(getMonthByIndex(-2), getTimeSlotByMonth(-2)));
		// list.add(new CusDateVo(getMonthByIndex(-3), getTimeSlotByMonth(-3)));
		// list.add(new CusDateVo(getMonthByIndex(-4), getTimeSlotByMonth(-4)));
		return list;
	}

	public static String getMonthByIndex(int num) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, num);
		return dateArr[cal.get(Calendar.MONTH)];
	}

	public static String getTimeSlotByMonth(int num) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, 1);

		cal.add(Calendar.MONTH, num);
		String str1 = sdf.format(cal.getTime());

		cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		String str2 = sdf.format(cal.getTime());
		return str1 + "-" + str2;
	}

	/**
	 * 获取本周
	 * 
	 * @author rick
	 * @date 2016年7月19日 上午10:00:20
	 * @return
	 */
	public static String getThisWeek() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Calendar cal = Calendar.getInstance();
		cal.setFirstDayOfWeek(Calendar.MONDAY);

		cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		String str1 = sdf.format(cal.getTime());

		cal.set(Calendar.DAY_OF_WEEK, 1);
		String str2 = sdf.format(cal.getTime());
		return str1 + "-" + str2;
	}

	/**
	 * 获取上周
	 * 
	 * @author rick
	 * @date 2016年7月19日 上午10:00:28
	 * @return
	 */
	public static String getLastWeek() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Calendar cal = Calendar.getInstance();
		cal.setFirstDayOfWeek(Calendar.MONDAY);

		cal.add(Calendar.DATE, -7);
		cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		String str1 = sdf.format(cal.getTime());

		cal.set(Calendar.DAY_OF_WEEK, 1);
		String str2 = sdf.format(cal.getTime());
		return str1 + "-" + str2;
	}

	/**
	 * 获取本月
	 * 
	 * @author rick
	 * @date 2016年7月19日 上午10:00:36
	 * @return
	 */
	public static String getThisMonth() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, 1);

		cal.add(Calendar.MONTH, 0);
		String str1 = sdf.format(cal.getTime());

		cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		String str2 = sdf.format(cal.getTime());
		return str1 + "-" + str2;
	}

	/**
	 * 获取上月
	 * 
	 * @author rick
	 * @date 2016年7月19日 上午10:00:45
	 * @return
	 */
	public static String getLastMonth() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, 1);

		cal.add(Calendar.MONTH, -1);
		String str1 = sdf.format(cal.getTime());

		cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		String str2 = sdf.format(cal.getTime());
		return str1 + "-" + str2;
	}

	/**
	 * 获取上月与本月
	 * 
	 * @author rick
	 * @date 2016年7月19日 上午10:00:51
	 * @return
	 */
	public static String getAllTime() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, 1);

		cal.add(Calendar.MONTH, -1);
		String str1 = sdf.format(cal.getTime());

		cal.add(Calendar.MONTH, 1);
		cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		String str2 = sdf.format(cal.getTime());
		return str1 + "-" + str2;
	}

	/**
	 * 
	 * @description 获取两个时间段内的所有日期
	 * @author gfwang
	 * @create 2016年9月29日下午4:15:10
	 * @version 1.0
	 * @param beginDate
	 *            开始时间
	 * @param endDate
	 *            结束时间
	 * @return
	 */
	public static List<Date> getDatesBetweenTwoDate(Date beginDate, Date endDate) {
		List<Date> lDate = new ArrayList<Date>();
		lDate.add(beginDate);// 把开始时间加入集合
		Calendar cal = Calendar.getInstance();
		// 使用给定的 Date 设置此 Calendar 的时间
		cal.setTime(beginDate);
		boolean bContinue = true;
		while (bContinue) {
			// 根据日历的规则，为给定的日历字段添加或减去指定的时间量
			cal.add(Calendar.DAY_OF_MONTH, 1);
			// 测试此日期是否在指定日期之后
			if (endDate.after(cal.getTime())) {
				lDate.add(cal.getTime());
			} else {
				break;
			}
		}
		lDate.add(endDate);// 把结束时间加入集合
		return lDate;
	}

	public static int getDaysIntervalNew(Date d1, Date d2) {
		if (d1 == null || d2 == null)
			return 0;

		int tt = 1;
		if (d1.compareTo(d2) > 0) {
			Date temp = d1;
			d1 = d2;
			d2 = temp;
			tt = -1;
		}
		Calendar c = Calendar.getInstance();
		c.setTime(d1);
		int beginYear = c.get(Calendar.YEAR);
		int dayBegin = c.get(Calendar.DAY_OF_YEAR);
		c.setTime(d2);
		int endYear = c.get(Calendar.YEAR);
		int dayEnd = c.get(Calendar.DAY_OF_YEAR);
		// 同一年两个时间差值
		if (beginYear == endYear) {
			return (dayEnd - dayBegin) * tt;
		}

		// 第一年还剩多少
		int total = getYearDays(beginYear) - dayBegin;

		// 年相差
		for (int i = beginYear + 1; i < endYear; i++) {
			total += getYearDays(i);
		}

		// 第二年已开始多少
		total += dayEnd;
		return total * tt;
	}

	private static int getYearDays(int year) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		return cal.getActualMaximum(Calendar.DAY_OF_YEAR);
	}

	/**
	 * 
	 * @description 判断是否属于
	 * @author gfwang
	 * @create 2016年12月29日下午6:58:26
	 * @version 1.0
	 * @param date
	 * @param strDateBegin
	 *            00:00:00
	 * @param strDateEnd
	 *            00:05:00
	 * @return
	 */
	public static boolean isInDate(Date date, String strDateBegin, String strDateEnd) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String strDate = sdf.format(date);
		// 截取当前时间时分秒
		int strDateH = Integer.parseInt(strDate.substring(11, 13));
		int strDateM = Integer.parseInt(strDate.substring(14, 16));
		int strDateS = Integer.parseInt(strDate.substring(17, 19));
		// 截取开始时间时分秒
		int strDateBeginH = Integer.parseInt(strDateBegin.substring(0, 2));
		int strDateBeginM = Integer.parseInt(strDateBegin.substring(3, 5));
		int strDateBeginS = Integer.parseInt(strDateBegin.substring(6, 8));
		// 截取结束时间时分秒
		int strDateEndH = Integer.parseInt(strDateEnd.substring(0, 2));
		int strDateEndM = Integer.parseInt(strDateEnd.substring(3, 5));
		int strDateEndS = Integer.parseInt(strDateEnd.substring(6, 8));
		if ((strDateH >= strDateBeginH && strDateH <= strDateEndH)) {
			// 当前时间小时数在开始时间和结束时间小时数之间
			if (strDateH > strDateBeginH && strDateH < strDateEndH) {
				return true;
				// 当前时间小时数等于开始时间小时数，分钟数在开始和结束之间oo
			} else if (strDateH == strDateBeginH && strDateM >= strDateBeginM && strDateM <= strDateEndM) {
				return true;
				// 当前时间小时数等于开始时间小时数，分钟数等于开始时间分钟数，秒数在开始和结束之间
			} else if (strDateH == strDateBeginH && strDateM == strDateBeginM && strDateS >= strDateBeginS && strDateS <= strDateEndS) {
				return true;
			}
			// 当前时间小时数大等于开始时间小时数，等于结束时间小时数，分钟数小等于结束时间分钟数
			else if (strDateH >= strDateBeginH && strDateH == strDateEndH && strDateM <= strDateEndM) {
				return true;
				// 当前时间小时数大等于开始时间小时数，等于结束时间小时数，分钟数等于结束时间分钟数，秒数小等于结束时间秒数
			} else if (strDateH >= strDateBeginH && strDateH == strDateEndH && strDateM == strDateEndM && strDateS <= strDateEndS) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public static Integer[] calculateAge(Date birthday, Date endDate) {// 年龄：6年9月11天
		Calendar bir = Calendar.getInstance();
		bir.setTime(birthday);
		Calendar end = Calendar.getInstance();
		end.setTime(endDate == null ? new Date() : endDate);

		if (!bir.after(end)) {
			int day = end.get(Calendar.DAY_OF_MONTH) - bir.get(Calendar.DAY_OF_MONTH);
			int month = end.get(Calendar.MONTH) - bir.get(Calendar.MONTH);
			int year = end.get(Calendar.YEAR) - bir.get(Calendar.YEAR);
			// 按照减法原理，先day相减，不够向month借；然后month相减，不够向year借；最后year相减。

			if (day < 0) {
				month -= 1;
				end.add(Calendar.MONTH, -1);// 得到上一个月，用来得到上个月的天数。
				day = day + end.getActualMaximum(Calendar.DAY_OF_MONTH);
			}
			if (month < 0) {
				month = (month + 12) % 12;
				year--;
			}
			return new Integer[] { year, month, day };
		}
		int day = (bir.get(Calendar.DAY_OF_MONTH) - end.get(Calendar.DAY_OF_MONTH));
		int month = (bir.get(Calendar.MONTH) - end.get(Calendar.MONTH));
		int year = (bir.get(Calendar.YEAR) - end.get(Calendar.YEAR));
		// 按照减法原理，先day相减，不够向month借；然后month相减，不够向year借；最后year相减。

		if (day < 0) {
			month -= 1;
			end.add(Calendar.MONTH, -1);// 得到上一个月，用来得到上个月的天数。
			day = day + end.getActualMaximum(Calendar.DAY_OF_MONTH);
		}
		if (month < 0) {
			month = (month + 12) % 12;
			year--;
		}
		day = day != 0 ? -day : day;
		month = month != 0 ? -month : month;
		year = year != 0 ? -year : year;
		return new Integer[] { year, month, day };
		// System.out.println("年龄：" + year + "年" + month + "月" + day + "天");
	}
}
