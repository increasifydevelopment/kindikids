package com.aoyuntek.aoyun.uitl;

import com.theone.string.util.StringUtil;

/**
 * @description sql工具类
 * @author xdwang
 * @create 2016年3月22日下午1:55:27
 * @version 1.0
 */
public class SqlUtil {

    /**
     * @description 转义sql like
     * @author xdwang
     * @create 2016年3月22日下午1:55:35
     * @version 1.0
     * @param str
     *            参数
     * @return 转义后的参数
     */
    public static String likeEscape(String str) {
        if (StringUtil.isBlank(str)) {
            return null;
        }
        StringBuffer buffer = new StringBuffer();
        int len = str.length();
        for (int i = 0; i < len; i++) {
            char c = str.charAt(i);
            switch (c) {
                case '_':
                    buffer.append("\\_");
                    break;
                case '%':
                    buffer.append("\\%");
                    break;
                case '\\':
                    buffer.append("\\\\");
                    break;
                default:
                    buffer.append(c);
            }
        }
        return buffer.toString();
    }

}
