package com.aoyuntek.aoyun.uitl;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.google.gson.reflect.TypeToken;

public class MyListUtil<T> {
    /**
     * 
     * @description list转set
     * @author gfwang
     * @create 2016年7月20日下午8:38:25
     * @version 1.0
     * @param list
     * @return
     */
    public Set<T> list2Set(List<T> list) {
        Set<T> sets = new LinkedHashSet<T>();
        if (list == null) {
            return sets;
        }
        sets.addAll(list);
        return sets;
    }

    /**
     * 
     * @description set转list
     * @author gfwang
     * @create 2016年7月20日下午8:38:25
     * @version 1.0
     * @param list
     * @return
     */
    public List<T> set2List(Set<T> sets) {
        List<T> list = new ArrayList<T>();
        if (sets == null) {
            return list;
        }
        list.addAll(sets);
        return list;
    }

    /**
     * 
     * @description 获取list type
     * @author gfwang
     * @create 2016年10月8日下午2:49:24
     * @version 1.0
     * @return
     */
    public Type getListType() {
        return new TypeToken<List<T>>() {
        }.getType();
    }
}
