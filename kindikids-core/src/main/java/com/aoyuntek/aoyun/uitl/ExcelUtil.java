package com.aoyuntek.aoyun.uitl;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.ContextLoader;

/**
 * @description 获取数据写入excel工具类
 * @author Hxzhang 2016年9月19日上午10:41:32
 * @version 1.0
 */
public class ExcelUtil {
	public void exportExcel(HttpServletResponse response, String fileName, List<String> cellList, List<Map<String, String>> list) {
		// 获取excel模版路径
		String tempFilePath = ContextLoader.getCurrentWebApplicationContext().getServletContext().getRealPath("/") + "templet/excel/"
				+ fileName + ".xlsx";
		try {
			fileName = fileName + ".xlsx";
			ExcelHandle handle = new ExcelHandle();
			handle.writeListData(tempFilePath, cellList, list, 0);
			// 写到输出流并关闭资源
			OutputStream output = response.getOutputStream();
			response.reset();
			fileName = new String(fileName.getBytes("gbk"), "iso-8859-1");
			response.setHeader("Content-Disposition", "attachment;filename=\"".concat(fileName + "\""));
			response.setHeader("Connection", "close");
			response.setContentType("multipart/form-data");
			handle.writeAndClose(tempFilePath, output);
			handle.readClose(tempFilePath);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void exportCsv(HttpServletResponse response, String fileName, List<String> cellList, List<Map<String, String>> list) {
		// 获取excel模版路径
		String tempFilePath = ContextLoader.getCurrentWebApplicationContext().getServletContext().getRealPath("/") + "templet/excel/"
				+ fileName + ".csv";
		try {
			fileName = fileName + ".csv";
			ExcelHandle handle = new ExcelHandle();
			handle.writeListData(tempFilePath, cellList, list, 0);
			// 写到输出流并关闭资源
			OutputStream output = response.getOutputStream();
			response.reset();
			fileName = new String(fileName.getBytes("gbk"), "iso-8859-1");
			response.setHeader("Content-Disposition", "attachment;filename=\"".concat(fileName + "\""));
			response.setHeader("Connection", "close");
			response.setContentType("multipart/form-data");
			handle.writeAndClose(tempFilePath, output);
			handle.readClose(tempFilePath);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
