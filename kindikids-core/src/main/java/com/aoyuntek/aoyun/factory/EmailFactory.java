package com.aoyuntek.aoyun.factory;

import java.text.MessageFormat;

import org.springframework.stereotype.Component;

import com.aoyuntek.aoyun.constants.DateFormatterConstants;
import com.aoyuntek.aoyun.entity.po.LeaveInfo;
import com.aoyuntek.aoyun.enums.EmailType;
import com.aoyuntek.aoyun.enums.LeaveType;
import com.aoyuntek.aoyun.uitl.EmailUtil;
import com.theone.date.util.DateUtil;

@Component
public class EmailFactory extends BaseFactory {
	/**
	 * 
	 * @description CEO/园长替员工请假创建成功，给Staff发送Approved邮件；
	 * @author gfwang
	 * @create 2017年1月4日上午9:14:08
	 * @version 1.0
	 * @return
	 */
	public String initEmailHtml(LeaveInfo leave, String staffName, String title, String content) {
		String beginDate = DateUtil.format(leave.getStartDate(), DateFormatterConstants.SYS_FORMAT2);
		String endDate = DateUtil.format(leave.getEndDate(), DateFormatterConstants.SYS_FORMAT2);
		switch (leave.getLeaveType()) {
		case 10:
		case 11:
			beginDate = DateUtil.format(leave.getStartDate(), "dd/MM/yyyy HH:mm");
			endDate = DateUtil.format(leave.getEndDate(), "dd/MM/yyyy HH:mm");
			break;
		}
		String leaveType = LeaveType.getDescByValue(leave.getLeaveType());
		content = MessageFormat.format(content, staffName, leaveType, beginDate + "-" + endDate);

		return EmailUtil.replaceHtml(title, content);
	}

	public String initEmail(LeaveInfo leave, String ceoName, String staffName, String title, String content) {
		String beginDate = DateUtil.format(leave.getStartDate(), DateFormatterConstants.SYS_FORMAT2);
		String endDate = DateUtil.format(leave.getEndDate(), DateFormatterConstants.SYS_FORMAT2);
		String leaveType = LeaveType.getDescByValue(leave.getLeaveType());
		content = MessageFormat.format(content, ceoName, staffName, leaveType, beginDate + "-" + endDate);
		return EmailUtil.replaceHtml(title, content);
	}

	public String initStaffLeaveChangeState(LeaveInfo leave, String staffName, String title, String content, EmailType emailType) {
		String beginDate = DateUtil.format(leave.getStartDate(), DateFormatterConstants.SYS_FORMAT2);
		String endDate = DateUtil.format(leave.getEndDate(), DateFormatterConstants.SYS_FORMAT2);
		switch (emailType) {
		case staff_leave_ceo_approve:
			return EmailUtil.replaceHtml(title, MessageFormat.format(content, staffName, beginDate + "-" + endDate));
		case staff_leave_ceo_cancle:
		case staff_leave_ceo_rejected:
			return EmailUtil.replaceHtml(title, MessageFormat.format(content, staffName, beginDate + "-" + endDate, leave.getReason()));
		default:
			return null;
		}
	}
}
