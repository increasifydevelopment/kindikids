package com.aoyuntek.aoyun.factory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aoyuntek.aoyun.dao.AttendanceHistoryInfoMapper;
import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.ChangeAttendanceRequestInfoMapper;
import com.aoyuntek.aoyun.dao.RoomInfoMapper;
import com.aoyuntek.aoyun.entity.po.ChangeAttendanceRequestInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.vo.AttendanceHistoryVo;
import com.aoyuntek.aoyun.entity.vo.child.AttendanceHistoryGroupVo;
import com.aoyuntek.aoyun.entity.vo.child.HistoryVo;
import com.aoyuntek.aoyun.enums.AttendanceRequestType;

@Component
public class AttendanceHistoryFactory extends BaseFactory {

    @Autowired
    private CentersInfoMapper centersInfoMapper;
    @Autowired
    private RoomInfoMapper roomInfoMapper;
    @Autowired
    private AttendanceHistoryInfoMapper attendanceHistoryInfoMapper;
    @Autowired
    private ChangeAttendanceRequestInfoMapper changeAttendanceRequestInfoMapper;

    /**
     * 
     * @author dlli5 at 2016年12月19日上午10:56:55
     * @param userInfo
     * @param accountId
     * @param leaveDate
     * @return
     */
    public HistoryVo getHistoryVo(UserInfo userInfo, String accountId, Date leaveDate) {
        List<AttendanceHistoryGroupVo> historyGroupVos = attendanceHistoryInfoMapper.getCentreGroupByChildId(accountId);

        List<ChangeAttendanceRequestInfo> attendanceRequestInfos = changeAttendanceRequestInfoMapper.getListByChildAndDateScope(accountId,
                new Date(), leaveDate);

        List<AttendanceHistoryVo> attendanceHistoryVo = new ArrayList<AttendanceHistoryVo>();
        for (ChangeAttendanceRequestInfo changeAttendanceRequestInfo : attendanceRequestInfos) {
            if (changeAttendanceRequestInfo.getType() != AttendanceRequestType.K.getValue()) {
                AttendanceHistoryVo item = new AttendanceHistoryVo();
                item.setCenterName(centersInfoMapper.selectByPrimaryKey(changeAttendanceRequestInfo.getNewCentreId()).getName());
                item.setRoomName(roomInfoMapper.selectByPrimaryKey(changeAttendanceRequestInfo.getNewRoomId()).getName());
                item.setBeginTime(changeAttendanceRequestInfo.getChangeDate());
                attendanceHistoryVo.add(item);
            }
        }
        return new HistoryVo(historyGroupVos, attendanceHistoryVo);
    }
}
