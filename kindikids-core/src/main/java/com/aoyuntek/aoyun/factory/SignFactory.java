package com.aoyuntek.aoyun.factory;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aoyuntek.aoyun.dao.SigninInfoMapper;

@Component
public class SignFactory extends BaseFactory {

    @Autowired
    private SigninInfoMapper signinInfoMapper;

    /**
     * 改变签到空间信息
     * 
     * @author dlli5 at 2016年10月27日上午9:10:23
     * @param accountId
     *            账号id
     * @param centreId
     *            园id
     * @param roomId
     *            room id
     * @param groupId
     * @param day
     */
    public void changeSignRoom(String accountId, String centreId, String roomId, Date day) {

        signinInfoMapper.updateSignCentreId(accountId, centreId, roomId, day);

    }
}
