package com.aoyuntek.aoyun.factory;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.RoomInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskResponsibleLogInfoMapper;
import com.aoyuntek.aoyun.entity.po.task.TaskResponsibleLogInfo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.task.TaskInfoVo;
import com.aoyuntek.aoyun.enums.TaskCategory;

@Component
public class TaskCoreFactory {

    @Autowired
    private RoomInfoMapper roomInfoMapper;
    @Autowired
    private UserInfoMapper userInfoMapper;
    @Autowired
    private CentersInfoMapper centersInfoMapper;
    @Autowired
    private TaskCentreFactory taskCentreFactory;
    @Autowired
    private TaskRoomFactory taskRoomFactory;
    @Autowired
    private TaskChildFactory taskChildFactory;
    @Autowired
    private TaskStaffFactory taskStaffFactory;
    @Autowired
    private TaskResponsibleLogInfoMapper taskResponsibleLogInfoMapper;

    public boolean needShow(TaskInfoVo taskInfoVo, UserInfoVo currentUser) {
        TaskCategory taskCategory = TaskCategory.getType((short) taskInfoVo.getTaskCategory());
        switch (taskCategory) {
        case CENTER: {
            // 可选责任人 Centre Managers 默认 Specific staff member（单独员工）
            boolean isHave01 = taskCentreFactory.isInPersonLiableOfCentre(taskInfoVo, currentUser);
            // 可选填写人 All Staff in the centre 默认 Centre Managers
            // Educators
            // Cooks Casuals Specific staff member（单独员工）
            boolean isHave02 = taskCentreFactory.isInImplementersOfCentre(taskInfoVo, currentUser);
            if (isHave01 || isHave02) {
                return true;
            }
        }
            break;
        case ROOM: {
            // 可选责任人 All Educators in the room 默认 Staff Name（可直接指定某员工）
            boolean isHave01 = taskRoomFactory.isInPersonLiableOfRoom(taskInfoVo, currentUser);
            // 可选填写人 All Staff in the centre 默认 Staff Name（可直接指定某员工）
            boolean isHave02 = taskRoomFactory.isInImplementersOfRoom(taskInfoVo, currentUser);
            if (isHave01 || isHave02) {
                return true;
            }
        }
            break;
        case CHILDRRENN: {
            // 可选责任人 All Educators in the room 默认 Staff Name（可直接指定某员工）
            boolean isHave01 = taskChildFactory.isInPersonLiableOfChild(taskInfoVo, currentUser);
            // 可选填写人 All Staff in the centre 默认 Staff Name（可直接指定某员工）
            boolean isHave02 = taskChildFactory.isInImplementersOfChild(taskInfoVo, currentUser);
            if (isHave01 || isHave02) {
                return true;
            }
        }
            break;
        case STAFF: {
            // 可选责任人 Directors Centre Managerss（默认） Staff
            // Names（当前园的Staff）
            boolean isHave01 = taskStaffFactory.isInPersonLiableOfStaff(taskInfoVo, currentUser);
            // 可选填写人 All Staff in the centre Centre Managerss（默认）
            // Specific
            // Staff Members（当前园的Staff，单个人员）
            boolean isHave02 = taskStaffFactory.isInImplementersOfStaff(taskInfoVo, currentUser);
            if (isHave01 || isHave02) {
                return true;
            }
        }
            break;
        case Individual: {
            // 可选责任人 Directors Centre Managerss（默认） Staff
            // // Names（当前园的Staff）
            // boolean isHave01 =
            // taskStaffFactory.isInPersonLiableOfStaff(taskInfoVo,
            // currentUser);
            // // 可选填写人 All Staff in the centre Centre Managerss（默认）
            // // Specific
            // // Staff Members（当前园的Staff，单个人员）
            // boolean isHave02 =
            // taskStaffFactory.isInImplementersOfStaff(taskInfoVo,
            // currentUser);
            // if (isHave01 || isHave02) {
            // return true;
            // }

            // 因为IndividualTask没有存储责任人凭证信息,而是直接固化记录,所以查询时不能够通过凭证查询,需直接通过固化的信息查询可见,操作权限
            String taskInstanceId = taskInfoVo.getTemp1();
            List<String> accountIds = taskResponsibleLogInfoMapper.getByInstanceId(taskInstanceId);
            for (String accountId : accountIds) {
                if (accountId.equals(currentUser.getAccountInfo().getId())) {
                    return true;
                }
            }
        }
            break;
        default:
            break;
        }
        return false;
    }
}
