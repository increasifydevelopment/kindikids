package com.aoyuntek.aoyun.factory;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aoyuntek.aoyun.condtion.RosterCondition;
import com.aoyuntek.aoyun.condtion.UserCondition;
import com.aoyuntek.aoyun.dao.AccountRoleInfoMapper;
import com.aoyuntek.aoyun.dao.RoleInfoMapper;
import com.aoyuntek.aoyun.dao.SigninInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.dao.roster.RosterInfoMapper;
import com.aoyuntek.aoyun.dao.roster.RosterShiftInfoMapper;
import com.aoyuntek.aoyun.dao.roster.RosterShiftTaskInfoMapper;
import com.aoyuntek.aoyun.dao.roster.RosterShiftTimeInfoMapper;
import com.aoyuntek.aoyun.dao.roster.RosterStaffInfoMapper;
import com.aoyuntek.aoyun.dao.roster.ShiftWeekInfoMapper;
import com.aoyuntek.aoyun.dao.roster.TempRosterCenterInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskFormRelationInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskInstanceInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskVisibleInfoMapper;
import com.aoyuntek.aoyun.entity.po.AccountRoleInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.po.roster.RosterInfo;
import com.aoyuntek.aoyun.entity.po.roster.RosterShiftTimeInfo;
import com.aoyuntek.aoyun.entity.po.roster.RosterStaffInfo;
import com.aoyuntek.aoyun.entity.po.roster.TempRosterCenterInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskVisibleInfo;
import com.aoyuntek.aoyun.entity.vo.RoleInfoVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.base.UserVo;
import com.aoyuntek.aoyun.entity.vo.roster.RosterRoleDayVo;
import com.aoyuntek.aoyun.entity.vo.roster.RosterShiftTimeVo;
import com.aoyuntek.aoyun.entity.vo.roster.RosterShiftVo;
import com.aoyuntek.aoyun.entity.vo.roster.RosterStaffShiftTimeVo;
import com.aoyuntek.aoyun.entity.vo.roster.RosterStaffVo;
import com.aoyuntek.aoyun.entity.vo.roster.RosterWeekVo;
import com.aoyuntek.aoyun.entity.vo.task.TaskInfoVo;
import com.aoyuntek.aoyun.enums.ArchivedStatus;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.enums.TaskCategory;
import com.aoyuntek.aoyun.enums.TaskVisibleType;
import com.aoyuntek.aoyun.enums.UserType;
import com.aoyuntek.aoyun.enums.roster.RosterStaffType;
import com.aoyuntek.aoyun.enums.roster.TempRosterCenterState;
import com.aoyuntek.aoyun.service.task.ITaskCoreService;
import com.aoyuntek.aoyun.uitl.EmailUtil;
import com.theone.aws.util.AwsEmailUtil;
import com.theone.date.util.DateUtil;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

@Component
public class RosterFactory extends BaseFactory {

	@Autowired
	private RosterShiftInfoMapper rosterShiftInfoMapper;
	@Autowired
	private RosterShiftTimeInfoMapper rosterShiftTimeInfoMapper;
	@Autowired
	private ShiftWeekInfoMapper shiftWeekInfoMapper;
	@Autowired
	private RosterShiftTaskInfoMapper rosterShiftTaskInfoMapper;
	@Autowired
	private RosterInfoMapper rosterInfoMapper;
	@Autowired
	private TaskInfoMapper taskInfoMapper;
	@Autowired
	private RosterStaffInfoMapper rosterStaffInfoMapper;
	@Autowired
	private SigninInfoMapper signinInfoMapper;
	@Autowired
	private UserInfoMapper userInfoMapper;
	@Autowired
	private RoleInfoMapper roleInfoMapper;
	@Autowired
	private AccountRoleInfoMapper accountRoleInfoMapper;
	@Autowired
	private TaskVisibleInfoMapper taskVisibleInfoMapper;
	@Autowired
	private TaskCentreFactory taskCentreFactory;
	@Autowired
	private ITaskCoreService taskCoreService;
	@Autowired
	private TaskInstanceInfoMapper taskInstanceInfoMapper;
	@Autowired
	private TaskFactory taskFactory;
	@Autowired
	private TaskFormRelationInfoMapper taskFormRelationInfoMapper;
	@Autowired
	private UserFactory userFactory;
	@Autowired
	private SignFactory signFactory;
	@Autowired
	private TempRosterCenterInfoMapper tempRosterCenterInfoMapper;

	/**
	 * 
	 * @author dlli5 at 2017年1月11日下午3:26:03
	 * @param oldRosterShiftTimeInfo
	 */
	public void dealChangeTimeOfCasual(RosterShiftTimeInfo oldRosterShiftTimeInfo) {
		// 找到所有用此shift的 roster
		// List<RosterInfo> rosterInfos = rosterInfoMapper.getList(condition)
		// 查找shiftCode 对应的roster的员工安排
		// List<RosterStaffInfo> rosterStaffInfos = rosterStaffInfoMapper.get
		System.out.println("sssss");
		// TODO big 这里影响的是下周 所以可以不镜像
	}

	/**
	 * 处理改变兼职排班时间的问题
	 * 
	 * @author dlli5 at 2017年1月11日下午1:30:31
	 * @param old
	 * @param rosterInfo
	 */
	public void dealChangeTimeOfCasual(String modelId, Date newBeginTime, Date newEndTime, RosterInfo rosterInfo) {
		Date now = new Date();
		RosterStaffInfo old = rosterStaffInfoMapper.selectByPrimaryKey(modelId);
		// 如果依照的是shift的时间 那么单个修改对此无影响
		if (StringUtil.isNotEmpty(old.getShiftTimeCode())) {
			return;
		}
		// 如果时间没有发生任何变化 那么不用处理
		if ((old.getStartTime().getTime() == newBeginTime.getTime()) && (old.getEndTime().getTime() == newEndTime.getTime())) {
			return;
		}

		// 配合流程图
		boolean isA = old.getEndTime().getTime() < now.getTime();
		boolean isB = old.getEndTime().getTime() > now.getTime() && old.getStartTime().getTime() < now.getTime();
		boolean isC = old.getStartTime().getTime() > now.getTime();

		boolean willA = newEndTime.getTime() < now.getTime();
		boolean willB = newEndTime.getTime() > now.getTime() && newBeginTime.getTime() < now.getTime();
		boolean willC = newBeginTime.getTime() > now.getTime();

		Role role = getRoleOfRosterStaffType(old.getStaffType());
		if (isA) {
			// 如果是A=>B 或者A=>C
			if (willB || willC) {
				TempRosterCenterInfo record = new TempRosterCenterInfo();
				record.setCentreId(rosterInfo.getCenterId());
				record.setDeleteFalg(false);
				record.setRoleId(roleInfoMapper.selectRoleIdByValue(role.getValue()));
				record.setRosterStaffId(old.getId());
				record.setState(TempRosterCenterState.Default.getValue());
				tempRosterCenterInfoMapper.insert(record);
			}
		}

		if (isB) {
			// 如果是B=>C
			if (willC) {
				// 给她的信息去掉园的信息
				UserInfo userInfo = userInfoMapper.getUserInfoByAccountId(old.getStaffAccountId());
				userInfo.setCentersId(null);
				userInfoMapper.updateByPrimaryKey(userInfo);

				// 删除所有临时角色
				accountRoleInfoMapper.deleteTempFlag(old.getStaffAccountId());

				// 更新状态为-2
				tempRosterCenterInfoMapper.updateStateByRosterStaffId(old.getId(), TempRosterCenterState.BC.getValue());

				// 生成新的
				TempRosterCenterInfo record = new TempRosterCenterInfo();
				record.setCentreId(rosterInfo.getCenterId());
				record.setDeleteFalg(false);
				record.setRoleId(roleInfoMapper.selectRoleIdByValue(role.getValue()));
				record.setRosterStaffId(old.getId());
				record.setState(TempRosterCenterState.Default.getValue());
				tempRosterCenterInfoMapper.insert(record);
			}
		}

		if (isC) {
			if (willA) {
				// 更新状态为-2
				tempRosterCenterInfoMapper.updateStateByRosterStaffId(old.getId(), TempRosterCenterState.CA.getValue());
			}
		}
	}

	/**
	 * 是否包含角色 只要包含任意则返回true
	 * 
	 * @author dlli5 at 2017年1月11日下午1:29:37
	 * @param roleList
	 *            角色列表
	 * @param roles
	 *            角色 多参
	 * @return 只要有一个存在 则返回true 否则返回false
	 */
	public boolean containRoles(List<RoleInfoVo> roleList, Role... roles) {
		boolean result = false;
		for (RoleInfoVo allRole : roleList) {
			for (Role role : roles) {
				if (allRole.getValue() == role.getValue()) {
					result = true;
				}
			}
		}
		return result;
	}

	/**
	 * 
	 * @author dlli5 at 2016年12月27日上午11:12:18
	 * @param rosterShiftTimeVo
	 * @return
	 */
	public String getTitle(RosterShiftTimeVo rosterShiftTimeVo) {
		return DateUtil.formatDate(rosterShiftTimeVo.getShiftStartTime(), "hh:mmaaa", Locale.ENGLISH) + "-"
				+ DateUtil.formatDate(rosterShiftTimeVo.getShiftEndTime(), "hh:mmaaa", Locale.ENGLISH);
	}

	/**
	 * 
	 * @author dlli5 at 2016年12月27日上午11:12:15
	 * @param centreId
	 * @param rosterShiftId
	 * @param day
	 * @return
	 */
	public RosterWeekVo initEmptyRsoter(String centreId, String rosterShiftId, Date day) {
		Date weekStart = DateUtil.getWeekStart(day);
		RosterWeekVo result = new RosterWeekVo();
		// 组装角色信息
		List<RosterRoleDayVo> roleDayVos = new ArrayList<RosterRoleDayVo>();

		// 获取本周签到的员工
		List<RosterStaffVo> rosterStaffVos = signinInfoMapper.getStaffSignByCentre(centreId, weekStart);

		RosterStaffType[] roles = RosterStaffType.values();
		for (int i = 0; i < 7; i++) {
			if (i == 0 || i == 6) {
				continue;
			}
			Date tempDay = DateUtil.addDay(weekStart, i);
			Map<Short, List<RosterStaffVo>> roleRosterStaffMap = new HashMap<Short, List<RosterStaffVo>>();

			for (RosterStaffType rosterStaffType : roles) {
				if (rosterStaffType == RosterStaffType.Sign) {
					roleRosterStaffMap.put(rosterStaffType.getValue(), getRosterStaffListOfDay(rosterStaffVos, i));
				} else {
					roleRosterStaffMap.put(rosterStaffType.getValue(), new ArrayList<RosterStaffVo>());
				}
			}

			roleDayVos.add(new RosterRoleDayVo(roleRosterStaffMap, tempDay));
		}
		result.setRoleDayVos(roleDayVos);

		// 组装时间段
		RosterShiftVo rosterShiftVo = rosterShiftInfoMapper.selectVoById(rosterShiftId);
		List<RosterStaffShiftTimeVo> rosterStaffShiftTimeVos = new ArrayList<RosterStaffShiftTimeVo>();

		ArrayList<RosterStaffVo> empty = new ArrayList<RosterStaffVo>();
		for (int i = 0; i < 7; i++) {
			if (i == 0 || i == 6) {
				continue;
			}
			Date tempDay = DateUtil.addDay(weekStart, i);
			empty.add(new RosterStaffVo(tempDay));
		}

		List<RosterShiftTimeVo> rosterShiftTimeVos = rosterShiftVo.getRosterShiftTimeVos();
		for (RosterShiftTimeVo rosterShiftTimeVo : rosterShiftTimeVos) {
			RosterStaffShiftTimeVo rosterStaffShiftTimeVo = new RosterStaffShiftTimeVo();
			rosterStaffShiftTimeVo.setTitle(getTitle(rosterShiftTimeVo));
			rosterStaffShiftTimeVo.setCode(rosterShiftTimeVo.getCode());
			rosterStaffShiftTimeVo.setRosterStaffVos(empty);
			rosterStaffShiftTimeVo.setShiftWeekVos(rosterShiftTimeVo.getShiftWeekVos());
			rosterStaffShiftTimeVos.add(rosterStaffShiftTimeVo);
		}
		result.setRosterStaffShiftTimeVos(rosterStaffShiftTimeVos);

		return result;
	}

	private List<RosterStaffVo> getRosterStaffListOfDay(List<RosterStaffVo> rosterStaffVos, int dayOfWeek) {
		List<RosterStaffVo> dayRosterStaff = new ArrayList<RosterStaffVo>();

		for (RosterStaffVo rosterStaffVo : rosterStaffVos) {
			if (DateUtil.getDayOfWeek(rosterStaffVo.getDay()) == dayOfWeek) {
				dayRosterStaff.add(rosterStaffVo);
			}
		}

		return dayRosterStaff;
	}

	public List<RosterStaffVo> getRosterStaffListOfDay(List<RosterStaffVo> rosterStaffSigns, int dayOfWeek, List<RosterStaffVo> allWeekRosterStaff) {
		List<RosterStaffVo> dayRosterStaff = new ArrayList<RosterStaffVo>();

		for (RosterStaffVo rosterStaffVo : rosterStaffSigns) {
			// 员工签到的时间和周期符合周期的
			if (DateUtil.getDayOfWeek(rosterStaffVo.getDay()) - 1 == dayOfWeek) {
				// 且不没有安排工作
				if (!isHaveRosterOfStaff(allWeekRosterStaff, rosterStaffVo.getStaffAccountId(), dayOfWeek)) {
					dayRosterStaff.add(rosterStaffVo);
				}
			}
		}

		return dayRosterStaff;
	}

	/**
	 * 
	 * @author dlli5 at 2016年12月27日上午11:12:03
	 * @param allWeekRosterStaff
	 * @param staffAccountId
	 * @param dayOfWeek
	 * @return
	 */
	private boolean isHaveRosterOfStaff(List<RosterStaffVo> allWeekRosterStaff, String staffAccountId, int dayOfWeek) {
		if (ListUtil.isEmpty(allWeekRosterStaff)) {
			return false;
		}
		for (RosterStaffVo rosterStaffVo : allWeekRosterStaff) {
			if (rosterStaffVo.getStaffAccountId().equals(staffAccountId) && DateUtil.getDayOfWeek(rosterStaffVo.getDay()) - 1 == dayOfWeek) {
				return true;
			}
		}
		return false;
	}

	public Role getRoleOfRosterStaffType(Short rosterStaffTypeVal) {
		RosterStaffType rosterStaffType = RosterStaffType.getType(rosterStaffTypeVal);
		switch (rosterStaffType) {
		case AdditionalStaff: {
			return Role.Educator;
		}
		case CenterManager: {
			return Role.CentreManager;
		}
		case Cook: {
			return Role.Cook;
		}
		case Sign: {
			return Role.Educator;
		}
		default:
			return null;
		}
	}

	/**
	 * 
	 * @author dlli5 at 2016年12月27日上午11:12:00
	 * @param rosterInfo
	 * @param rosterStaffInfo
	 * @param currentUser
	 * @return
	 */
	public boolean approveTempRole(RosterInfo rosterInfo, RosterStaffInfo rosterStaffInfo, UserInfoVo currentUser) {
		// 加角色
		RosterStaffType rosterStaffType = RosterStaffType.getType(rosterStaffInfo.getStaffType());
		// 获取这个人的角色信息
		List<RoleInfoVo> roleList = userInfoMapper.getRoleByAccount(rosterStaffInfo.getStaffAccountId());
		boolean haveRole = true;
		Role role = getRoleOfRosterStaffType(rosterStaffInfo.getStaffType());
		switch (rosterStaffType) {
		case AdditionalStaff: {
			haveRole = containRoles(roleList, Role.Educator);
		}
			break;
		case CenterManager: {
			haveRole = containRoles(roleList, Role.CentreManager);
		}
			break;
		case Cook: {
			haveRole = containRoles(roleList, Role.Cook);
		}
			break;
		case Sign: {
			haveRole = containRoles(roleList, Role.Educator);
		}
			break;
		default:
			break;
		}
		if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(new Date(), rosterStaffInfo.getDay()) == 0) {
			boolean isCasual = containRoles(roleList, Role.Casual);
			if (isCasual) {
				TempRosterCenterInfo tempRosterCenterInfo = new TempRosterCenterInfo();
				tempRosterCenterInfo.setCentreId(rosterInfo.getCenterId());
				tempRosterCenterInfo.setDeleteFalg(false);
				tempRosterCenterInfo.setState((short) 0);

				Date beginDate = null;
				Date endDate = null;
				if (rosterStaffInfo.getStartTime() != null) {
					beginDate = com.aoyuntek.aoyun.uitl.DateUtil.merge2DateTime(rosterStaffInfo.getDay(), rosterStaffInfo.getStartTime());
					endDate = com.aoyuntek.aoyun.uitl.DateUtil.merge2DateTime(rosterStaffInfo.getDay(), rosterStaffInfo.getEndTime());
					tempRosterCenterInfo.setStartTime(beginDate);
					tempRosterCenterInfo.setEndTime(endDate);
				}

				tempRosterCenterInfo.setRoleId(roleInfoMapper.selectRoleIdByValue(role.getValue()));
				tempRosterCenterInfo.setRosterStaffId(rosterStaffInfo.getId());
				if (StringUtil.isNotEmpty(rosterStaffInfo.getShiftTimeCode())) {
					RosterShiftTimeInfo rosterShiftTimeInfo = rosterShiftTimeInfoMapper.getByCode(rosterInfo.getShiftId(), rosterStaffInfo.getShiftTimeCode());
					beginDate = com.aoyuntek.aoyun.uitl.DateUtil.merge2DateTime(rosterStaffInfo.getDay(), rosterShiftTimeInfo.getShiftStartTime());
					endDate = com.aoyuntek.aoyun.uitl.DateUtil.merge2DateTime(rosterStaffInfo.getDay(), rosterShiftTimeInfo.getShiftEndTime());
					tempRosterCenterInfo.setStartTime(beginDate);
					tempRosterCenterInfo.setEndTime(endDate);
				} else {

				}
				tempRosterCenterInfoMapper.insert(tempRosterCenterInfo);
			} else {
				if (!haveRole) {
					AccountRoleInfo accountRoleInfo = new AccountRoleInfo();
					accountRoleInfo.setId(UUID.randomUUID().toString());
					accountRoleInfo.setAccountId(rosterStaffInfo.getStaffAccountId());
					accountRoleInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
					accountRoleInfo.setCreateTime(new Date());
					accountRoleInfo.setDeleteFlag(DeleteFlag.Default.getValue());
					accountRoleInfo.setRoleId(roleInfoMapper.selectRoleIdByValue(role.getValue()));
					accountRoleInfo.setTempFlag(true);
					accountRoleInfoMapper.insert(accountRoleInfo);

				}
			}
			// 看看是否是兼职老师
			// if (isCasual) {
			// // 给她的信息加园的信息
			// UserInfo userInfo =
			// userInfoMapper.getUserInfoByAccountId(rosterStaffInfo.getStaffAccountId());
			// userInfo.setCentersId(rosterInfo.getCenterId());
			// userInfoMapper.updateByPrimaryKey(userInfo);
			// // 如果有签到信息 那么更新签到信息到这个园s
			// signFactory.changeSignRoom(rosterStaffInfo.getStaffAccountId(),
			// rosterInfo.getCenterId(), null, rosterStaffInfo.getDay());
			// }
			// 如果是自己那么
			if (rosterStaffInfo.getStaffAccountId().equals(currentUser.getAccountInfo().getId())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * @author dlli5 at 2016年12月27日上午11:11:45
	 * @param rosterInfo
	 * @param currentUser
	 * @param day
	 * @return
	 * @throws Exception
	 */
	public boolean approvedRoster(RosterInfo rosterInfo, UserInfoVo currentUser, Date day) throws Exception {
		boolean needFlushSesson = false;
		// 搜索所有的今天的排班人员
		List<RosterStaffVo> rosterStaffVos = rosterStaffInfoMapper.getByRosterId(rosterInfo.getId());
		if (ListUtil.isEmpty(rosterStaffVos)) {
			return needFlushSesson;
		}
		for (RosterStaffVo rosterStaffVo : rosterStaffVos) {
			if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(rosterStaffVo.getDay(), day) == 0) {
				if (approveTempRole(rosterInfo, rosterStaffVo, currentUser)) {
					needFlushSesson = true;
				}
			}
		}

		// 生成task
		RosterShiftVo rosterShiftVo = rosterShiftInfoMapper.selectVoById(rosterInfo.getShiftId());
		List<RosterShiftTimeVo> rosterShiftTimeVos = rosterShiftVo.getRosterShiftTimeVos();
		if (ListUtil.isEmpty(rosterShiftTimeVos)) {
			return needFlushSesson;
		}

		List<String> taskIds = new ArrayList<String>();
		for (RosterShiftTimeVo rosterShiftTimeVo : rosterShiftTimeVos) {
			List<TaskInfo> taskInfos = rosterShiftTimeVo.getRosterShiftTaskVos();
			if (ListUtil.isEmpty(taskInfos)) {
				continue;
			}
			// needFlushSesson = true;
			for (TaskInfo taskInfo : taskInfos) {
				if (!taskIds.contains(taskInfo.getId())) {
					taskIds.add(taskInfo.getId());
				}
			}
		}

		for (String taskId : taskIds) {
			TaskInfo taskInfo = taskInfoMapper.getVo(taskId);
			// 伪造可填写人
			TaskVisibleInfo visibleInfoT = new TaskVisibleInfo();
			visibleInfoT.setObjId("0");
			visibleInfoT.setSourceId(taskInfo.getTaskImplementersId());
			visibleInfoT.setType(TaskVisibleType.CENTER.getValue());
			// 看看有没有这条记录
			int size = taskVisibleInfoMapper.selectByModelCount(visibleInfoT);
			if (size == 0) {
				taskVisibleInfoMapper.insert(visibleInfoT);
			}
			// 创建task实例
			TaskInfoVo taskInfoVo = taskInfoMapper.getVo(taskId);
			TaskCategory taskCategory = TaskCategory.getType(taskInfoVo.getTaskCategory());
			// 判断数据库中是否已经生成
			if (taskInstanceInfoMapper.haveTaskInstance(taskInfoVo.getId(), day, day, taskCategory.getValue(), rosterInfo.getCenterId()) > 0) {
				continue;
			}
			taskInfoVo.setFormId(taskFormRelationInfoMapper.getByTaskId(taskInfo.getId()));
			taskCoreService.dealTaskInstance(taskInfoVo, currentUser.getAccountInfo().getId(), rosterInfo.getCenterId(), null, null, day);
		}
		return needFlushSesson;
	}

	/**
	 * 
	 * @author dlli5 at 2016年12月27日上午11:11:52
	 * @param rosterInfo
	 * @param currentUser
	 * @param day
	 */
	public void sendEmailOfRoster(RosterInfo rosterInfo, UserInfoVo currentUser, Date day) {
		List<RosterStaffVo> rosterStaffVos = rosterStaffInfoMapper.getByRosterId(rosterInfo.getId());
		if (ListUtil.isEmpty(rosterStaffVos)) {
			return;
		}
		List<String> accountIds = new ArrayList<String>();
		for (RosterStaffVo rosterStaffVo : rosterStaffVos) {
			if (!accountIds.contains(rosterStaffVo.getStaffAccountId())) {
				accountIds.add(rosterStaffVo.getStaffAccountId());
			}
		}
		UserCondition userCondition = new UserCondition();
		userCondition.initAll();
		userCondition.setUserType(UserType.Staff.getValue());
		userCondition.setStatus(ArchivedStatus.UnArchived.getValue());
		userCondition.setAccountIds(accountIds);
		List<UserVo> list = userInfoMapper.getListByCondtion(userCondition);
		final List<UserVo> resultList = userFactory.duplicate(list);
		final String emailKey = getSystemMsg("email_key");
		final String sendAddress = getSystemMsg("mailHostAccount");
		final String sendName = getSystemMsg("email_name");
		final AwsEmailUtil awsEmailUtil = new AwsEmailUtil();
		new Thread(new Runnable() {
			@Override
			public void run() {
				for (UserVo userVo : resultList) {
					try {
						String userName = userVo.getName();
						String title = getSystemEmailMsg("roster_appreve_title");
						String content = MessageFormat.format(getSystemEmailMsg("roster_approve_content"), userName, getSystemMsg("rootUrl"));
						String email = EmailUtil.replaceHtml(title, content);
						awsEmailUtil.sendgrid(sendAddress, sendName, userVo.getEmail(), userName, title, email, true, "", null, emailKey);
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			}
		}).start();

	}

	/**
	 * 按照规则获取需要的ECT员工
	 * 
	 * @author dlli5 at 2016年12月23日下午4:13:16
	 * @param count
	 * @return
	 */
	public int getNeedEctStaff(int count) {
		/*
		 * 1-39:1; 40-59:2; 60-89:3; 90+:4
		 */
		if (count >= 90) {
			return 4;
		}
		if (count >= 60) {
			return 3;
		}
		if (count >= 40) {
			return 2;
		}
		if (count >= 1) {
			return 1;
		}
		return 0;
	}

	public boolean havaRoster(UserInfoVo user) {
		List<TempRosterCenterInfo> list = tempRosterCenterInfoMapper.getTempRoster(user.getAccountInfo().getId(), new Date());
		if (ListUtil.isNotEmpty(list)) {
			return true;
		}
		return false;
	}
}
