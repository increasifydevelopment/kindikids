package com.aoyuntek.aoyun.factory;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aoyuntek.aoyun.dao.AccountRoleInfoMapper;
import com.aoyuntek.aoyun.dao.RoleInfoMapper;
import com.aoyuntek.aoyun.dao.SigninInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.dao.roster.RosterInfoMapper;
import com.aoyuntek.aoyun.dao.roster.RosterShiftInfoMapper;
import com.aoyuntek.aoyun.dao.roster.RosterShiftTaskInfoMapper;
import com.aoyuntek.aoyun.dao.roster.RosterShiftTimeInfoMapper;
import com.aoyuntek.aoyun.dao.roster.RosterStaffInfoMapper;
import com.aoyuntek.aoyun.dao.roster.ShiftWeekInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskInstanceInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskVisibleInfoMapper;
import com.aoyuntek.aoyun.entity.po.roster.RosterShiftInfo;
import com.aoyuntek.aoyun.entity.po.roster.RosterShiftTaskInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskInfo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.roster.RosterShiftTimeVo;
import com.aoyuntek.aoyun.entity.vo.roster.RosterShiftVo;
import com.aoyuntek.aoyun.entity.vo.roster.ShiftWeekVo;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.service.task.ITaskCoreService;
import com.theone.date.util.DateUtil;
import com.theone.list.util.ListUtil;

@Component
public class ShiftFactory {

	@Autowired
	private RosterShiftInfoMapper rosterShiftInfoMapper;
	@Autowired
	private RosterShiftTimeInfoMapper rosterShiftTimeInfoMapper;
	@Autowired
	private ShiftWeekInfoMapper shiftWeekInfoMapper;
	@Autowired
	private RosterShiftTaskInfoMapper rosterShiftTaskInfoMapper;
	@Autowired
	private RosterInfoMapper rosterInfoMapper;
	@Autowired
	private TaskInfoMapper taskInfoMapper;
	@Autowired
	private RosterStaffInfoMapper rosterStaffInfoMapper;
	@Autowired
	private SigninInfoMapper signinInfoMapper;
	@Autowired
	private UserInfoMapper userInfoMapper;
	@Autowired
	private RoleInfoMapper roleInfoMapper;
	@Autowired
	private AccountRoleInfoMapper accountRoleInfoMapper;
	@Autowired
	private TaskVisibleInfoMapper taskVisibleInfoMapper;
	@Autowired
	private TaskCentreFactory taskCentreFactory;
	@Autowired
	private ITaskCoreService taskCoreService;
	@Autowired
	private TaskInstanceInfoMapper taskInstanceInfoMapper;
	@Autowired
	private TaskFactory taskFactory;

	private String reSaveNewTask(String taskId, String newTaskId, RosterShiftVo vo, UserInfoVo currentUser) {
		Date now = new Date();
		vo.setCreateAccountId(currentUser.getAccountInfo().getId());
		vo.setCreateTime(now);
		vo.setDeleteFlag(DeleteFlag.Default.getValue());
		vo.setId(UUID.randomUUID().toString());
		vo.setCurrentFlag(vo.getCurrentFlag());
		rosterShiftInfoMapper.insert(vo);
		for (RosterShiftTimeVo rosterShiftTimeInfo : vo.getRosterShiftTimeVos()) {
			rosterShiftTimeInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
			// rosterShiftTimeInfo.setCode(UUID.randomUUID().toString());
			rosterShiftTimeInfo.setCreateTime(now);
			rosterShiftTimeInfo.setDeleteFlag(DeleteFlag.Default.getValue());
			rosterShiftTimeInfo.setId(UUID.randomUUID().toString());
			rosterShiftTimeInfo.setShiftId(vo.getId());

			for (ShiftWeekVo shiftWeekVo : rosterShiftTimeInfo.getShiftWeekVos()) {
				shiftWeekVo.setId(UUID.randomUUID().toString());
				shiftWeekVo.setShiftTimeId(rosterShiftTimeInfo.getId());
				shiftWeekInfoMapper.insert(shiftWeekVo);
			}

			for (TaskInfo taskInfo : rosterShiftTimeInfo.getRosterShiftTaskVos()) {
				RosterShiftTaskInfo rosterShiftTaskInfo = new RosterShiftTaskInfo();
				rosterShiftTaskInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
				rosterShiftTaskInfo.setCreateTime(now);
				rosterShiftTaskInfo.setDeleteFlag(DeleteFlag.Default.getValue());
				rosterShiftTaskInfo.setShiftTimeId(rosterShiftTimeInfo.getId());
				rosterShiftTaskInfo.setTaskId(taskInfo.getId().equals(taskId) ? newTaskId : taskInfo.getId());
				rosterShiftTaskInfoMapper.insert(rosterShiftTaskInfo);
			}
			rosterShiftTimeInfoMapper.insert(rosterShiftTimeInfo);
		}
		return vo.getId();
	}

	public void imageWithTask(String taskId, String newTaskId, UserInfoVo currentUser) {
		Date todayWeelStart = DateUtil.getWeekStart(new Date());
		// 找到task对应所有shift 本周以及老的roster
		List<String> shiftIds = rosterShiftInfoMapper.getWillImageShiftIds(taskId, todayWeelStart);
		if (ListUtil.isNotEmpty(shiftIds)) {
			// 镜像shift
			for (String shiftId : shiftIds) {
				RosterShiftVo vo = rosterShiftInfoMapper.selectVoById(shiftId);
				// 记录老的shiftId 以便后续更新
				String oldShiftId = vo.getId();
				String newShiftId = reSaveNewTask(taskId, newTaskId, vo, currentUser);
				// 更新本周之后的roster的shift 为最新的
				rosterInfoMapper.updateFutureShiftId(todayWeelStart, oldShiftId, newShiftId);
				// 更新老的shift 的当前标识为不是当前的
				RosterShiftInfo rosterShiftInfo = rosterShiftInfoMapper.selectByPrimaryKey(oldShiftId);
				rosterShiftInfo.setCurrentFlag(false);
				rosterShiftInfoMapper.updateByPrimaryKey(rosterShiftInfo);
				// 更新现有的shift的taskId
				// rosterShiftTaskInfoMapper.updateNewTask(newShiftId,);
				// add by big
				// 修改本周未Approved和以后的的roster shiftid 为最新
				// rosterInfoMapper.updateFutureShiftId(todayWeelStart,
				// oldShiftId,newShiftId);
			}

		}
	}
}
