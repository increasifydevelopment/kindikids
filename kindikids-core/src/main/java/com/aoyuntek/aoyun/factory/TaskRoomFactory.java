package com.aoyuntek.aoyun.factory;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aoyuntek.aoyun.condtion.RoomCondition;
import com.aoyuntek.aoyun.constants.Select2Constants;
import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.RoomInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.dao.program.ProgramIntobsleaInfoMapper;
import com.aoyuntek.aoyun.dao.program.TaskProgramFollowUpInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskFollowUpInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskFrequencyInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskResponsibleLogInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskVisibleInfoMapper;
import com.aoyuntek.aoyun.entity.po.CentersInfo;
import com.aoyuntek.aoyun.entity.po.RoomInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskVisibleInfo;
import com.aoyuntek.aoyun.entity.vo.SelecterGroupVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.base.UserVo;
import com.aoyuntek.aoyun.entity.vo.task.PersonSelectVo;
import com.aoyuntek.aoyun.entity.vo.task.TaskInfoVo;
import com.aoyuntek.aoyun.enums.ArchivedStatus;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.enums.TaskVisibleType;
import com.theone.string.util.StringUtil;

@Component
public class TaskRoomFactory extends TaskFactory {

	@Autowired
	private RoomInfoMapper roomInfoMapper;
	@Autowired
	private UserInfoMapper userInfoMapper;
	@Autowired
	private CentersInfoMapper centersInfoMapper;
	@Autowired
	private ProgramIntobsleaInfoMapper programIntobsleaInfoMapper;
	@Autowired
	private TaskProgramFollowUpInfoMapper taskProgramFollowUpInfoMapper;
	@Autowired
	private TaskVisibleInfoMapper taskVisibleInfoMapper;
	@Autowired
	private TaskInfoMapper taskInfoMapper;
	@Autowired
	private TaskResponsibleLogInfoMapper taskResponsibleLogInfoMapper;
	@Autowired
	private UserFactory userFactory;
	@Autowired
	private TaskFrequencyInfoMapper taskFrequencyInfoMapper;
	@Autowired
	private TaskFollowUpInfoMapper taskFollowUpInfoMapper;

	public List dealRoomOfSelect(List<CentersInfo> list, List<RoomInfo> roomList, List<TaskVisibleInfo> taskVisibleInfos, String findText) {
		if (list == null) {
			list = new ArrayList<CentersInfo>();
		}
		List<SelecterGroupVo> selecterPos = new ArrayList<SelecterGroupVo>();

		SelecterGroupVo all = new SelecterGroupVo();
		all.setText(null);
		List<PersonSelectVo> allItem = new ArrayList<PersonSelectVo>();

		PersonSelectVo allRoom = new PersonSelectVo(TaskVisibleType.CENTER.getValue(), "0", Select2Constants.AllCentreRooms);
		// 是否选中
		allRoom.setSelected(isSelected(allRoom.getId(), allRoom.getType(), taskVisibleInfos));
		allItem.add(allRoom);
		all.setChildren(allItem);
		selecterPos.add(all);

		for (CentersInfo centersInfo : list) {
			SelecterGroupVo selecterGroupVo = new SelecterGroupVo();
			selecterGroupVo.setText(centersInfo.getName());

			List<PersonSelectVo> items = new ArrayList<PersonSelectVo>();
			// 添加all center one room
			PersonSelectVo allCenterRoom = new PersonSelectVo(TaskVisibleType.CENTER.getValue(), centersInfo.getId(),
					MessageFormat.format(Select2Constants.AllXRooms, centersInfo.getName()));
			// 看看是否被选中
			allCenterRoom.setSelected(isSelected(allCenterRoom.getId(), allCenterRoom.getType(), taskVisibleInfos));
			items.add(allCenterRoom);

			for (RoomInfo room : roomList) {
				if (room.getCentersId().equals(centersInfo.getId())) {
					PersonSelectVo select = new PersonSelectVo(TaskVisibleType.ROOM.getValue(), room.getId(),
							MessageFormat.format("{0} - {1}", centersInfo.getName(), room.getName()));
					// 是否选中
					select.setSelected(isSelected(select.getId(), select.getType(), taskVisibleInfos));
					items.add(select);
				}
			}
			selecterGroupVo.setChildren(items);
			selecterPos.add(selecterGroupVo);
		}
		return filterGroupSelect(selecterPos, findText);
	}

	public List<String> getSelectRoomIds(List<PersonSelectVo> selectList) {
		if (selectList == null) {
			selectList = new ArrayList<PersonSelectVo>();
		}
		List<String> array = new ArrayList<String>();
		for (PersonSelectVo personSelectVo : selectList) {
			if ((personSelectVo.getType() == TaskVisibleType.ROOM.getValue()) && (!"0".equals(personSelectVo.getId()))) {
				// 根据roomId 找到 centreId
				String roomId = personSelectVo.getId();
				array.add(roomId);
			}
		}
		return array;
	}

	public List dealRoomOfResponsible(List<UserVo> userInfos, List<TaskVisibleInfo> taskVisibleInfos, String findText) {
		if (userInfos == null) {
			userInfos = new ArrayList<UserVo>();
		}
		List<PersonSelectVo> selecterPos = new ArrayList<PersonSelectVo>();

		PersonSelectVo ordinator = new PersonSelectVo(TaskVisibleType.ROLE.getValue(), Role.Educator.getValue() + "",
				Select2Constants.AllEducatorsInThRoom);
		ordinator.setSelected(isSelected(ordinator.getId(), ordinator.getType(), taskVisibleInfos));
		selecterPos.add(ordinator);

		for (UserVo userInfo : userInfos) {
			PersonSelectVo select = new PersonSelectVo(TaskVisibleType.USER.getValue(), userInfo.getAccountId(), userInfo.getFullName());
			// 是否选中
			select.setSelected(isSelected(select.getId(), select.getType(), taskVisibleInfos));
			selecterPos.add(select);
		}
		return filterSelect(selecterPos, findText);
	}

	public List dealRoomOfImplementers(List<UserVo> userInfos, List<TaskVisibleInfo> taskVisibleInfos, String findText) {
		if (userInfos == null) {
			userInfos = new ArrayList<UserVo>();
		}
		List<PersonSelectVo> selecterPos = new ArrayList<PersonSelectVo>();

		PersonSelectVo ordinatorAllStaffOfCentre = new PersonSelectVo(TaskVisibleType.CENTER.getValue(), "0",
				Select2Constants.AllStaffInTheCentre);
		ordinatorAllStaffOfCentre
				.setSelected(isSelected(ordinatorAllStaffOfCentre.getId(), ordinatorAllStaffOfCentre.getType(), taskVisibleInfos));
		selecterPos.add(ordinatorAllStaffOfCentre);
		for (UserVo userInfo : userInfos) {
			if (StringUtil.isNotEmpty(findText) && !userInfo.getFullName().toLowerCase().contains(findText.toLowerCase())) {
				continue;
			}
			PersonSelectVo select = new PersonSelectVo(TaskVisibleType.USER.getValue(), userInfo.getAccountId(), userInfo.getFullName());
			// 是否选中
			select.setSelected(isSelected(select.getId(), select.getType(), taskVisibleInfos));
			selecterPos.add(select);
		}

		return filterSelect(selecterPos, findText);
	}

	public boolean isInImplementersOfRoom(TaskInfoVo taskInfoVo, UserInfoVo currentUser) {
		for (TaskVisibleInfo items : taskInfoVo.getTaskImplementersVisibles()) {
			// All Staff in the centre
			if (items.getType() == TaskVisibleType.CENTER.getValue()) {
				if (isInRoom(taskInfoVo.getSelectTaskVisibles(), currentUser)) {
					return true;
				}
			}
			// Staff Name（可直接指定某员工）
			else if (items.getType() == TaskVisibleType.USER.getValue()) {
				if (items.getObjId().equals(currentUser.getAccountInfo().getId())) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean isInPersonLiableOfRoom(TaskInfoVo taskInfoVo, UserInfoVo currentUser) {
		for (TaskVisibleInfo items : taskInfoVo.getResponsibleTaskVisibles()) {
			// All Educators in the room
			if (items.getType() == TaskVisibleType.ROLE_GROUP.getValue()) {
				boolean inRole = containRoles(currentUser.getRoleInfoVoList(), currentUser.getHistoryRoles(), Role.Educator,
						Role.EducatorECT);
				if (inRole && isInRoom(taskInfoVo.getSelectTaskVisibles(), currentUser)) {
					return true;
				}
			}
			// 单独员工
			else if (items.getType() == TaskVisibleType.USER.getValue()) {
				if (items.getObjId().equals(currentUser.getAccountInfo().getId())) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean isInRoom(List<TaskVisibleInfo> selectTaskVisibles, UserInfoVo currentUser) {
		for (TaskVisibleInfo taskVisibleInfo : selectTaskVisibles) {
			// all center room
			if (taskVisibleInfo.getType() == TaskVisibleType.CENTER.getValue() && "0".equals(taskVisibleInfo.getObjId())) {
				return true;
			}
			// all centre one room
			// 兼职+二级园长
			if (StringUtil.isNotEmpty(currentUser.getUserInfo().getCentersId())
					&& taskVisibleInfo.getType() == TaskVisibleType.CENTER.getValue()
					&& currentUser.getUserInfo().getCentersId().equals(taskVisibleInfo.getObjId())) {
				return true;
			}

			// center one room01
			if (taskVisibleInfo.getType() == TaskVisibleType.ROOM.getValue()) {
				// TODO big 这里可以优化
				RoomInfo roomInfo = roomInfoMapper.selectByPrimaryKey(taskVisibleInfo.getObjId());
				if (roomInfo.getCentersId().equals(currentUser.getUserInfo().getCentersId())) {
					return true;
				}
			}
		}
		return false;
	}

	public List<RoomInfo> getRoomOfSelectRoom(List<TaskVisibleInfo> selectTaskVisibles) {
		List<RoomInfo> result = new ArrayList<RoomInfo>();
		List<String> roomIds = new ArrayList<String>();
		for (TaskVisibleInfo taskVisibleInfo : selectTaskVisibles) {
			// all center room
			if (taskVisibleInfo.getType() == TaskVisibleType.CENTER.getValue() && "0".equals(taskVisibleInfo.getObjId())) {
				result.clear();
				roomIds.clear();
				RoomCondition roomCondition = new RoomCondition();
				roomCondition.initAll();
				roomCondition.setStatus(ArchivedStatus.UnArchived.getValue());
				List<RoomInfo> roomList = roomInfoMapper.getList(roomCondition);
				return roomList;
			}
			// all centre one room
			if (taskVisibleInfo.getType() == TaskVisibleType.CENTER.getValue()) {
				RoomCondition roomCondition = new RoomCondition();
				List<String> centreIds = new ArrayList<String>();
				roomCondition.initAll();
				centreIds.add(taskVisibleInfo.getObjId());
				roomCondition.setCentreIds(centreIds);
				roomCondition.setStatus(ArchivedStatus.UnArchived.getValue());
				List<RoomInfo> roomInfoVos = roomInfoMapper.getList(roomCondition);

				for (RoomInfo roomInfoVo : roomInfoVos) {
					if (!roomIds.contains(roomInfoVo.getId())) {
						result.add(roomInfoVo);
						roomIds.add(roomInfoVo.getId());
					}
				}
			}

			// center one room01
			if (taskVisibleInfo.getType() == TaskVisibleType.ROOM.getValue()) {
				RoomInfo roomInfo = roomInfoMapper.selectByPrimaryKey(taskVisibleInfo.getObjId());
				if (!roomIds.contains(roomInfo.getId())) {
					result.add(roomInfo);
				}
			}
		}
		return result;
	}

	public boolean haveOnlySelectOneRoom(List<PersonSelectVo> selectList) {
		if (selectList == null) {
			selectList = new ArrayList<PersonSelectVo>();
		}

		// 选一个 并且选择到具体园
		if (selectList.size() == 1 && selectList.get(0).getType() == TaskVisibleType.ROOM.getValue()) {
			return true;
		}
		return false;
	}

}
