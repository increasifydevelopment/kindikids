package com.aoyuntek.aoyun.factory;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aoyuntek.aoyun.constants.ChildDietaryRequirConstants;
import com.aoyuntek.aoyun.constants.ChildMedicalConstants;
import com.aoyuntek.aoyun.constants.DateFormatterConstants;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.entity.po.ChildDietaryRequireInfo;
import com.aoyuntek.aoyun.entity.po.ChildMedicalInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.po.event.EventInfo;
import com.aoyuntek.aoyun.entity.po.program.TimeMedicationInfo;
import com.aoyuntek.aoyun.entity.vo.ProgramMedicationInfoVo;
import com.aoyuntek.aoyun.entity.vo.family.ChildCustodyArrangeVo;
import com.aoyuntek.aoyun.entity.vo.todaynote.NoteKeyValueVo;
import com.aoyuntek.aoyun.entity.vo.todaynote.TodayNoteVo2;
import com.aoyuntek.aoyun.enums.TodayNoteType;
import com.theone.date.util.DateUtil;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

@Component
public class TaskNoteFactory {

    @Autowired
    private UserInfoMapper userInfoMapper;

    private StringBuilder trimString(Boolean otherChoose, String OtherValue, StringBuilder sb) {
        final String BR = "</br>";
        if (BooleanUtils.isTrue(otherChoose)) {
            sb.append(MessageFormat.format("{0}"+BR, OtherValue));
            return sb;
        } else {
            if(sb.toString().endsWith(BR)){
                return sb;
            }
            String temp = StringUtil.trimEnd(sb.toString().trim(), ",");
            if (StringUtil.isEmpty(temp)) {
                return new StringBuilder();
            }
            return new StringBuilder(temp + BR);
        }
    }

    private NoteKeyValueVo getDietaryRequireStr(ChildDietaryRequireInfo childDietaryRequireInfo) {
        UserInfo userInfo = userInfoMapper.selectByPrimaryKey(childDietaryRequireInfo.getUserId());
        StringBuilder sb = new StringBuilder();
        Boolean[] check = { childDietaryRequireInfo.getDairy1(), childDietaryRequireInfo.getDairy2(), childDietaryRequireInfo.getDairy3(),
                childDietaryRequireInfo.getNuts1(), childDietaryRequireInfo.getNuts2(), childDietaryRequireInfo.getNuts3(),
                childDietaryRequireInfo.getNuts4(), childDietaryRequireInfo.getEggs1(), childDietaryRequireInfo.getEggs2(),
                childDietaryRequireInfo.getEggs3(), childDietaryRequireInfo.getMeat1(), childDietaryRequireInfo.getMeat2(),
                childDietaryRequireInfo.getMeat3(), childDietaryRequireInfo.getMeat4(), childDietaryRequireInfo.getMeat5() };
        for (int i = 0; i < check.length; i++) {
            if (BooleanUtils.isTrue(check[i])) {
                sb.append(MessageFormat.format("{0}, ", ChildDietaryRequirConstants.ChildDietaryRequirs[i]));
            }
            if (i == 2) {
                sb = trimString(childDietaryRequireInfo.getDairyOther(), childDietaryRequireInfo.getDairyOherSpecify(), sb);
            } else if (i == 6) {
                sb = trimString(childDietaryRequireInfo.getNutsOther(), childDietaryRequireInfo.getNutsOtherSpecify(), sb);
            } else if (i == 9) {
                sb = trimString(childDietaryRequireInfo.getEggsOther(), childDietaryRequireInfo.getEggsOtherSpecify(), sb);
            } else if (i == 14) {
                sb = trimString(childDietaryRequireInfo.getMeatOther(), childDietaryRequireInfo.getMeatOtherSpecify(), sb);
            }
            /*
             * if (i == 2 || i == 6 || i == 9 || i == 14) {
             * 
             * 
             * 
             * 
             * String title = ""; if
             * (BooleanUtils.isTrue(childDietaryRequireInfo.getDairyOther()) &&
             * i == 2) { title = childDietaryRequireInfo.getDairyOherSpecify();
             * } else if
             * (BooleanUtils.isTrue(childDietaryRequireInfo.getNutsOther()) && i
             * == 6) { title = childDietaryRequireInfo.getNutsOtherSpecify(); }
             * else if
             * (BooleanUtils.isTrue(childDietaryRequireInfo.getEggsOther()) && i
             * == 9) { title = childDietaryRequireInfo.getEggsOtherSpecify(); }
             * else if
             * (BooleanUtils.isTrue(childDietaryRequireInfo.getMeatOther()) && i
             * == 14) { title = childDietaryRequireInfo.getMeatOtherSpecify(); }
             * else { sb = new
             * StringBuilder(StringUtil.trimEnd(sb.toString().trim(), ",")); }
             * if (BooleanUtils.isTrue(check[i]) ||
             * StringUtil.isNotEmpty(title)) { if (StringUtil.isNotEmpty(sb)) {
             * str = sb.toString().trim(); str = str.substring(str.length() - 1,
             * str.length()).equals(",") ? str.substring(0, str.length() - 1) :
             * str; sb = StringUtil.isEmpty(title) ? new StringBuilder(str) :
             * sb; } if (StringUtil.isNotEmpty(title)) {
             * sb.append(MessageFormat.format("{0}</br>", title)); } else {
             * sb.append(MessageFormat.format("{0}", "</br>")); } str =
             * sb.toString().trim(); } } else { if (StringUtil.isNotEmpty(sb)) {
             * str = sb.toString().trim(); str = str.substring(str.length() - 1,
             * str.length()).equals(",") ? str.substring(0, str.length() - 1) :
             * str; } }
             */
        }
        return new NoteKeyValueVo(userInfo.getFullName(), sb.toString());
    }

    private NoteKeyValueVo getMedicalStr(ChildMedicalInfo childMedicalInfo) {
        UserInfo userInfo = userInfoMapper.selectByPrimaryKey(childMedicalInfo.getUserId());
        StringBuilder sb = new StringBuilder();
        Boolean[] check = { childMedicalInfo.getApplicableEpilepsy(), childMedicalInfo.getApplicableAsthma(), childMedicalInfo.getApplicableEczema() };
        for (int i = 0; i < check.length; i++) {
            if (BooleanUtils.isTrue(check[i])) {
                sb.append(MessageFormat.format("{0}, ", ChildMedicalConstants.ChildMedicals[i]));
            }
        }
        if (BooleanUtils.isTrue(childMedicalInfo.getApplicableOther())) {
            sb.append(MessageFormat.format("{0}, ", childMedicalInfo.getApplicableOtherSpecify()));
        }
        return new NoteKeyValueVo(userInfo.getFullName(), StringUtil.trimEnd(sb.toString().trim(), ","));
    }

    public List<TodayNoteVo2> dealTodayNote(List<ChildDietaryRequireInfo> childDietaryRequireInfos, List<EventInfo> eventList,
            List<UserInfo> willCome, List<UserInfo> willLeave, List<ProgramMedicationInfoVo> medicationInfos, int size,
            List<ChildCustodyArrangeVo> custodyArrangeInfos, List<ChildMedicalInfo> childMedicalInfos) {
        // List<String> notes = new ArrayList<String>();
        List<TodayNoteVo2> list = new ArrayList<TodayNoteVo2>();
        if (ListUtil.isNotEmpty(childDietaryRequireInfos)) {
            List<NoteKeyValueVo> notes1 = new ArrayList<NoteKeyValueVo>();
            for (ChildDietaryRequireInfo childDietaryRequireInfo : childDietaryRequireInfos) {
                notes1.add(getDietaryRequireStr(childDietaryRequireInfo));
            }
            list.add(new TodayNoteVo2(TodayNoteType.Dietary_Requirement.getValue(), notes1));
        }
        if (ListUtil.isNotEmpty(childMedicalInfos)) {
            List<NoteKeyValueVo> notes2 = new ArrayList<NoteKeyValueVo>();
            for (ChildMedicalInfo childMedicalInfo : childMedicalInfos) {
                notes2.add(getMedicalStr(childMedicalInfo));
            }
            list.add(new TodayNoteVo2(TodayNoteType.Medical.getValue(), notes2));
        }
        if (ListUtil.isNotEmpty(custodyArrangeInfos) && StringUtil.isNotEmpty(custodyArrangeInfos.get(0).getId())) {
            List<NoteKeyValueVo> notes6 = new ArrayList<NoteKeyValueVo>();
            for (ChildCustodyArrangeVo childCustodyArrangeVo : custodyArrangeInfos) {
                /*
                 * notes.add(MessageFormat.format("{0}''s Custody Arrangements:{1}"
                 * , childCustodyArrangeVo.getName(),
                 * childCustodyArrangeVo.getGetChildLaw()));
                 */
                String title = childCustodyArrangeVo.getGetChildLaw();
                notes6.add(new NoteKeyValueVo(childCustodyArrangeVo.getName(), title));
            }
            list.add(new TodayNoteVo2(TodayNoteType.Custody_Arrangements.getValue(), notes6));
        }
        List<NoteKeyValueVo> notes = new ArrayList<NoteKeyValueVo>();
        if (ListUtil.isNotEmpty(willCome)) {
            for (UserInfo userInfo : willCome) {
                String title = MessageFormat.format("will come {1} on {0}",
                        DateUtil.format(userInfo.getCreateTime(), DateFormatterConstants.SYS_FORMAT2), userInfo.getRoomId());
                notes.add(new NoteKeyValueVo(userInfo.getFullName(), title));
            }
        }

        if (ListUtil.isNotEmpty(willLeave)) {
            for (UserInfo userInfo : willLeave) {
                String title = MessageFormat.format("will leave {1} on {0}",
                        DateUtil.format(userInfo.getCreateTime(), DateFormatterConstants.SYS_FORMAT2), userInfo.getRoomId());
                notes.add(new NoteKeyValueVo(userInfo.getFullName(), title));
            }
        }
        if (ListUtil.isNotEmpty(notes)) {
            list.add(new TodayNoteVo2(TodayNoteType.Change_Attend.getValue(), notes));
        }
        // todayNoteVo.setMedicationInfos(medicationInfos);

        if (ListUtil.isNotEmpty(medicationInfos)) {
            List<NoteKeyValueVo> notes7 = new ArrayList<NoteKeyValueVo>();
            for (ProgramMedicationInfoVo programMedicationInfoVo : medicationInfos) {
                String childName = programMedicationInfoVo.getUserName();
                StringBuilder sb2 = new StringBuilder();
                for (TimeMedicationInfo item : programMedicationInfoVo.getTimeMedicationInfos()) {
                    if (BooleanUtils.isFalse(item.getChooseFlag()) || null == item.getChooseFlag()) {
                        String html = "<span class='redColor'>" + parsrTime(item.getDay()) + "</span>, ";
                        sb2.append(html);
                        continue;
                    }
                    sb2.append(parsrTime(item.getDay()) + ", ");
                }
                notes7.add(new NoteKeyValueVo(childName, StringUtil.trimEnd(sb2.toString().trim(), ",")));
            }
            list.add(new TodayNoteVo2(TodayNoteType.Medical_Task.getValue(), notes7));
        }
        // edit by gfwang 01/10/2017 新需求
        /*
         * if (size > 0) {
         * notes.add(MessageFormat.format("You have {0} tasks overdue today.",
         * size)); }
         */

        if (ListUtil.isNotEmpty(eventList)) {
            List<NoteKeyValueVo> notes3 = new ArrayList<NoteKeyValueVo>();
            for (EventInfo eventInfo : eventList) {
                StringBuilder sb = new StringBuilder();
                notes3.add(new NoteKeyValueVo(eventInfo.getTitle()));
            }
            list.add(new TodayNoteVo2(TodayNoteType.Event.getValue(), notes3));
        }

        return list;
    }

    private String parsrTime(Date date) {
        String patten = "hh:mm a";
        SimpleDateFormat sdf = new SimpleDateFormat(patten, Locale.ENGLISH);
        return sdf.format(date);
    }

}
