package com.aoyuntek.aoyun.factory;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aoyuntek.aoyun.constants.TipMsgConstants;
import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.ChangeAttendanceRequestInfoMapper;
import com.aoyuntek.aoyun.dao.ChildAttendanceInfoMapper;
import com.aoyuntek.aoyun.dao.RoomInfoMapper;
import com.aoyuntek.aoyun.dao.SigninInfoMapper;
import com.aoyuntek.aoyun.dao.TemporaryAttendanceInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.dao.child.RoomChangeInfoMapper;
import com.aoyuntek.aoyun.entity.po.AttendanceHistoryInfo;
import com.aoyuntek.aoyun.entity.po.CentersInfo;
import com.aoyuntek.aoyun.entity.po.ChangeAttendanceRequestInfo;
import com.aoyuntek.aoyun.entity.po.ChildAttendanceInfo;
import com.aoyuntek.aoyun.entity.po.TemporaryAttendanceInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.po.child.RoomChangeInfo;
import com.aoyuntek.aoyun.entity.vo.AttendanceChildVo;
import com.aoyuntek.aoyun.entity.vo.EmailVo;
import com.aoyuntek.aoyun.entity.vo.LeaveListVo;
import com.aoyuntek.aoyun.entity.vo.RoleInfoVo;
import com.aoyuntek.aoyun.entity.vo.SigninVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.child.AttendancePostion;
import com.aoyuntek.aoyun.enums.AttendanceRequestType;
import com.aoyuntek.aoyun.enums.ChangeAttendanceRequestDayState;
import com.aoyuntek.aoyun.enums.ChangeAttendanceRequestState;
import com.aoyuntek.aoyun.enums.ChangeAttendanceRequestType;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.EmailPlanType;
import com.aoyuntek.aoyun.enums.EmailType;
import com.aoyuntek.aoyun.enums.EnrolledState;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.enums.SigninState;
import com.aoyuntek.aoyun.enums.TemporaryType;
import com.aoyuntek.aoyun.enums.Week;
import com.aoyuntek.aoyun.enums.child.RoomChangeOperate;
import com.aoyuntek.aoyun.service.IMsgEmailInfoService;
import com.aoyuntek.aoyun.uitl.EmailUtil;
import com.theone.date.util.DateUtil;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

@Component
public class AttendanceFactory extends BaseFactory {

	@Autowired
	private UserInfoMapper userInfoMapper;
	@Autowired
	private CentersInfoMapper centersInfoMapper;
	@Autowired
	private RoomInfoMapper roomInfoMapper;
	@Autowired
	private ChildAttendanceInfoMapper childAttendanceInfoMapper;
	@Autowired
	private SigninInfoMapper signinInfoMapper;
	@Autowired
	private RoomChangeInfoMapper roomChangeInfoMapper;
	@Autowired
	private ChangeAttendanceRequestInfoMapper changeAttendanceRequestInfoMapper;
	@Autowired
	private TemporaryAttendanceInfoMapper temporaryAttendanceInfoMapper;
	@Autowired
	private UserFactory userFactory;
	@Autowired
	private IMsgEmailInfoService emailService;

	/**
	 * 
	 * @author dlli5 at 2016年12月19日上午9:35:41
	 * @param userInfo
	 * @param re
	 * @param currentUser
	 */
	public void dealChangeRoomWithEmail(UserInfo userInfo, ChangeAttendanceRequestInfo re, UserInfoVo currentUser) {
		// 上课周期的改变
		if (re.getType() == AttendanceRequestType.K.getValue()) {
			return;
		}
		// 园长申请转园
		boolean isCentreManagerRequest = false;
		if (StringUtil.isEmpty(re.getId()) && re.getSourceType() == ChangeAttendanceRequestType.CentreManagerRequest.getValue()) {
			isCentreManagerRequest = true;
		}

		if (!(re.getSourceType() == ChangeAttendanceRequestType.CentreManagerRequest.getValue()
				|| re.getSourceType() == ChangeAttendanceRequestType.CeoChangeCentre.getValue()
				|| re.getSourceType() == ChangeAttendanceRequestType.ChangeRoom.getValue())) {
			return;
		}

		boolean changeCentre = !userInfo.getCentersId().equals(re.getNewCentreId());
		boolean changeRoom = isChangRoom(userInfo, re);
		if (changeCentre || changeRoom) {
			List<UserInfo> parents = userInfoMapper.getParentInfoByFamilyId(userInfo.getFamilyId());
			CentersInfo centersInfo = centersInfoMapper.selectByPrimaryKey(re.getNewCentreId());
			CentersInfo oldCentersInfo = centersInfoMapper.getCenterByChildAccountId(userInfo.getAccountId());
			String childFullName = userFactory.getUserName(userInfo);
			String[] ceoEmails = userInfoMapper.getCeoEmails();

			if (isCentreManagerRequest) {
				// 发送Request邮件
				List<UserInfo> ceosAndcenterMangers = userInfoMapper.getCEOAndMangersByCenterId(re.getNewCentreId());
				// 给园长发送邮件
				List<EmailVo> emails = new ArrayList<EmailVo>();
				for (UserInfo centerManger : ceosAndcenterMangers) {
					String oldName = oldCentersInfo.getName();
					String newName = centersInfo.getName();
					String sub = getSystemEmailMsg("request_changCentre_sub");
					String title = getSystemEmailMsg("request_changCentre_title");
					String content = MessageFormat.format(getSystemEmailMsg("request_changCentre_content"), userFactory.getUserName(centerManger), childFullName, oldName,
							newName);
					String html = EmailUtil.replaceHtml(title, content);
					emails.add(new EmailVo(centerManger.getEmail(), userFactory.getUserName(centerManger), sub, html, true));
				}
				emailService.batchSendEamil(emails);
				List<EmailVo> emails2 = new ArrayList<EmailVo>();
				// 给家长发送邮件
				for (UserInfo parent : parents) {
					String oldName = oldCentersInfo.getName();
					String newName = centersInfo.getName();
					String sub = getSystemEmailMsg("request_changCentre_sub");
					String title = getSystemEmailMsg("request_changCentre_title");
					String content = MessageFormat.format(getSystemEmailMsg("request_changCentre_content"), userFactory.getUserName(parent), childFullName, oldName,
							newName);
					String html = EmailUtil.replaceHtml(title, content);
					emails2.add(new EmailVo(parent.getEmail(), userFactory.getUserName(parent), sub, html, true));
				}
				emailService.batchSendEamil(emails2, ceoEmails);
			} else {
				// 发送ChangRoom的Approve邮件
				if (changeRoom && re.getSourceType() != ChangeAttendanceRequestType.CeoChangeCentre.getValue()) {
					String title = getSystemEmailMsg("approve_attendance_title");
					String content = getSystemEmailMsg("approve_attendance_content");
					ChildAttendanceInfo oldAttendanceInfo = childAttendanceInfoMapper.selectByUserId(userInfo.getId());
					oldAttendanceInfo.setRequestInfo(re);
					oldAttendanceInfo.setNewMonday(re.getMondayBool());
					oldAttendanceInfo.setNewTuesday(re.getTuesdayBool());
					oldAttendanceInfo.setNewWednesday(re.getWednesdayBool());
					oldAttendanceInfo.setNewThursday(re.getThursdayBool());
					oldAttendanceInfo.setNewFriday(re.getFridayBool());
					oldAttendanceInfo.setPlan(re.isPlan());
					oldAttendanceInfo.setRequestId(re.getId());
					userFactory.sendParentEmailByChild(title, content, userInfo.getAccountId(), true, EmailType.changeRoom.getValue(), null, null, oldAttendanceInfo,
							oldAttendanceInfo.isPlan());
				} else {
					// 发送Approve邮件
					// 如果是ceo Approve 只给家长发送邮件
					if (!isRole(currentUser, Role.CEO.getValue())) {
						List<UserInfo> ceosAndcenterMangers = userInfoMapper.getCEOAndMangersByCenterId(userInfo.getCentersId());
						// 给园长发送邮件
						List<EmailVo> emails = new ArrayList<EmailVo>();
						for (UserInfo centerManger : ceosAndcenterMangers) {
							String newName = centersInfo.getName();
							String sub = getSystemEmailMsg("approve_changCentre_sub");
							String title = getSystemEmailMsg("approve_changCentre_title");
							String str = DateUtil.formatDate(re.getChangeDate(), "d MMM yyyy", Locale.ENGLISH);
							ChildAttendanceInfo oldAttendanceInfo = childAttendanceInfoMapper.selectByUserId(userInfo.getId());
							String content = MessageFormat.format(getSystemEmailMsg("approve_changCentre_content"), userFactory.getUserName(centerManger), childFullName,
									newName, str, getDaysOfAttendance(oldAttendanceInfo));
							String html = EmailUtil.replaceHtml(title, content);
							emails.add(new EmailVo(centerManger.getEmail(), userFactory.getUserName(centerManger), sub, html, true));
						}
						emailService.batchSendEamil(emails);
					}
					List<EmailVo> emails2 = new ArrayList<EmailVo>();
					// 给家长发送邮件
					for (UserInfo parent : parents) {
						String newName = centersInfo.getName();
						String sub = getSystemEmailMsg("approve_changCentre_sub");
						String title = getSystemEmailMsg("approve_changCentre_title");
						String str = DateUtil.formatDate(re.getChangeDate(), "d MMM yyyy", Locale.ENGLISH);
						String content = MessageFormat.format(getSystemEmailMsg("approve_changCentre_content"), userFactory.getUserName(parent), childFullName, newName,
								str, getDaysOfAttendance(re.getMonday(), re.getTuesday(), re.getWednesday(), re.getThursday(), re.getFriday()));
						String html = EmailUtil.replaceHtml(title, content);
						emails2.add(new EmailVo(parent.getEmail(), userFactory.getUserName(parent), sub, html, true));
					}
					if (re.isPlan()) {
						userFactory.dealPlanEmail(re.getId(), emails2, EmailPlanType.C.getValue(), userInfo.getId());
					} else {
						emailService.batchSendEamil(emails2, ceoEmails);
					}
				}
			}
		}
	}

	private String getDaysOfAttendance(ChildAttendanceInfo att) {
		return getDaysOfAttendance(att.getMonday(), att.getTuesday(), att.getWednesday(), att.getThursday(), att.getFriday());
	}

	/**
	 * @description 获取孩子的上课周期
	 * @author hxzhang
	 * @create 2017年1月4日上午11:56:27
	 */
	private String getDaysOfAttendance(Boolean... days) {
		String daysOfAttendance = "";
		List<String> weekCheckList = new ArrayList<String>();
		for (int i = 0; i < days.length; i++) {
			if (days[i]) {
				weekCheckList.add(TipMsgConstants.WEEKNAMES[i]);
			}
		}
		for (String str : weekCheckList) {
			daysOfAttendance += str + ", ";
		}
		if (StringUtil.isEmpty(daysOfAttendance)) {
			return " ";
		}
		return daysOfAttendance.substring(0, daysOfAttendance.length() - 2);
	}

	private boolean isChangRoom(UserInfo userInfo, ChangeAttendanceRequestInfo re) {
		if (StringUtil.isEmpty(userInfo.getRoomId())) {
			return false;
		}
		String oldCentreId = roomInfoMapper.selectCenterIdByRoom(userInfo.getRoomId());
		String newCentreId = roomInfoMapper.selectCenterIdByRoom(re.getNewRoomId());
		if (oldCentreId.equals(newCentreId) && !userInfo.getRoomId().equals(re.getNewRoomId())) {
			return true;
		}
		return false;
	}

	/**
	 * @description 获取孩子的上课周期
	 * @author hxzhang
	 * @create 2017年1月4日上午11:56:27
	 */
	private String getDaysOfAttendance(short... days) {
		String daysOfAttendance = "";
		List<String> weekCheckList = new ArrayList<String>();
		for (int i = 0; i < days.length; i++) {
			if (days[i] == 1) {
				weekCheckList.add(TipMsgConstants.WEEKNAMES[i]);
			}
		}
		for (String str : weekCheckList) {
			daysOfAttendance += str + ", ";
		}
		return daysOfAttendance.substring(0, daysOfAttendance.length() - 2);
	}

	/**
	 * 按照小孩的lastName排序
	 * 
	 * @author dlli5 at 2016年9月6日下午7:31:08
	 * @param attendanceChilds
	 */
	public void sortChildArrendance(List<SigninVo> signinInfos) {
		Collections.sort(signinInfos, new Comparator<SigninVo>() {
			public int compare(SigninVo arg0, SigninVo arg1) {
				// if (StringUtil.isEmpty(arg0.getLastName())) {
				// arg0.setLastName("");
				// }
				// if (StringUtil.isEmpty(arg1.getLastName())) {
				// arg1.setLastName("");
				// }
				// return arg0.getLastName().compareTo(arg1.getLastName());
				return arg0.getBirthday().compareTo(arg1.getBirthday());
			}
		});
	}

	/**
	 * 
	 * @author dlli5 at 2016年9月20日上午10:36:17
	 * @param day
	 * @param centresId
	 * @param roomId
	 * @param childId
	 * @param createAccountId
	 * @param dayOfWeek
	 * @return
	 */
	private TemporaryAttendanceInfo dealTemporaryAttendanceInfo(Date day, String centresId, String roomId, String childId, String createAccountId, int dayOfWeek) {
		// 添加任务到永久安排表
		TemporaryAttendanceInfo temporary = new TemporaryAttendanceInfo();
		temporary.setBeginTime(day);
		temporary.setCentreId(centresId);// child.getCentersId()
		temporary.setCreateAccountId(createAccountId);
		temporary.setCreateTime(new Date());
		temporary.setDayWeek((short) dayOfWeek);
		temporary.setDeleteFlag(DeleteFlag.Default.getValue());
		temporary.setEndTime(null);
		temporary.setId(UUID.randomUUID().toString());
		temporary.setRoomId(roomId);
		temporary.setType(TemporaryType.All.getValue());
		temporary.setChildId(childId);
		return temporary;
	}

	/**
	 * 
	 * @author dlli5 at 2016年9月1日下午9:12:15
	 * @param changeAttendanceRequestInfo
	 * @param dayOfWeek
	 */
	public List<TemporaryAttendanceInfo> dealAttendance(ChangeAttendanceRequestInfo changeAttendanceRequestInfo, int dayOfWeek, boolean dealAll, Date day,
			String centresId, String roomId, String childId, String createAccountId) {
		List<TemporaryAttendanceInfo> temporaryAttendanceInfos = new ArrayList<TemporaryAttendanceInfo>();
		switch (dayOfWeek) {
		case 1:
			changeAttendanceRequestInfo.setMonday(ChangeAttendanceRequestDayState.Deal.getValue());
			break;
		case 2:
			changeAttendanceRequestInfo.setTuesday(ChangeAttendanceRequestDayState.Deal.getValue());
			break;
		case 3:
			changeAttendanceRequestInfo.setWednesday(ChangeAttendanceRequestDayState.Deal.getValue());
			break;
		case 4:
			changeAttendanceRequestInfo.setThursday(ChangeAttendanceRequestDayState.Deal.getValue());
			break;
		case 5:
			changeAttendanceRequestInfo.setFriday(ChangeAttendanceRequestDayState.Deal.getValue());
			break;
		default:
			break;
		}
		if (dealAll) {
			if (changeAttendanceRequestInfo.getMonday() == ChangeAttendanceRequestDayState.Check.getValue()) {
				changeAttendanceRequestInfo.setMonday(ChangeAttendanceRequestDayState.Deal.getValue());
				temporaryAttendanceInfos.add(dealTemporaryAttendanceInfo(day, centresId, roomId, childId, createAccountId, 1));
			}
			if (changeAttendanceRequestInfo.getTuesday() == ChangeAttendanceRequestDayState.Check.getValue()) {
				changeAttendanceRequestInfo.setTuesday(ChangeAttendanceRequestDayState.Deal.getValue());
				temporaryAttendanceInfos.add(dealTemporaryAttendanceInfo(day, centresId, roomId, childId, createAccountId, 2));
			}
			if (changeAttendanceRequestInfo.getWednesday() == ChangeAttendanceRequestDayState.Check.getValue()) {
				changeAttendanceRequestInfo.setWednesday(ChangeAttendanceRequestDayState.Deal.getValue());
				temporaryAttendanceInfos.add(dealTemporaryAttendanceInfo(day, centresId, roomId, childId, createAccountId, 3));
			}
			if (changeAttendanceRequestInfo.getThursday() == ChangeAttendanceRequestDayState.Check.getValue()) {
				changeAttendanceRequestInfo.setThursday(ChangeAttendanceRequestDayState.Deal.getValue());
				temporaryAttendanceInfos.add(dealTemporaryAttendanceInfo(day, centresId, roomId, childId, createAccountId, 4));
			}
			if (changeAttendanceRequestInfo.getFriday() == ChangeAttendanceRequestDayState.Check.getValue()) {
				changeAttendanceRequestInfo.setFriday(ChangeAttendanceRequestDayState.Deal.getValue());
				temporaryAttendanceInfos.add(dealTemporaryAttendanceInfo(day, centresId, roomId, childId, createAccountId, 5));
			}

		} else {
			temporaryAttendanceInfos.add(dealTemporaryAttendanceInfo(day, centresId, roomId, childId, createAccountId, dayOfWeek));
		}
		return temporaryAttendanceInfos;
	}

	/**
	 * 处理小孩ChildAttendanceInfo的周期信息
	 * 将ChangeAttendanceRequestInfo同步到ChildAttendanceInfo
	 * 
	 * @author dlli5 at 2016年9月18日上午10:52:31
	 * @param attendanceInfo
	 * @param changeAttendanceRequestInfo
	 */
	public void dealAttendance(ChildAttendanceInfo attendanceInfo, ChangeAttendanceRequestInfo changeAttendanceRequestInfo) {
		attendanceInfo.setMonday(false);
		attendanceInfo.setTuesday(false);
		attendanceInfo.setWednesday(false);
		attendanceInfo.setThursday(false);
		attendanceInfo.setFriday(false);
		if (changeAttendanceRequestInfo.getMonday() == ChangeAttendanceRequestDayState.Check.getValue()) {
			attendanceInfo.setMonday(true);
		}
		if (changeAttendanceRequestInfo.getTuesday() == ChangeAttendanceRequestDayState.Check.getValue()) {
			attendanceInfo.setTuesday(true);
		}
		if (changeAttendanceRequestInfo.getWednesday() == ChangeAttendanceRequestDayState.Check.getValue()) {
			attendanceInfo.setWednesday(true);
		}
		if (changeAttendanceRequestInfo.getThursday() == ChangeAttendanceRequestDayState.Check.getValue()) {
			attendanceInfo.setThursday(true);
		}
		if (changeAttendanceRequestInfo.getFriday() == ChangeAttendanceRequestDayState.Check.getValue()) {
			attendanceInfo.setFriday(true);
		}
	}

	/**
	 * 
	 * @author dlli5 at 2016年9月1日下午9:10:14
	 * @param attendanceInfo
	 * @param dayOfWeek
	 */
	public void dealAttendance(ChildAttendanceInfo attendanceInfo, int dayOfWeek) {
		switch (dayOfWeek) {
		case 1:
			attendanceInfo.setMonday(true);
			break;
		case 2:
			attendanceInfo.setTuesday(true);
			break;
		case 3:
			attendanceInfo.setWednesday(true);
			break;
		case 4:
			attendanceInfo.setThursday(true);
			break;
		case 5:
			attendanceInfo.setFriday(true);
			break;
		default:
			break;
		}
	}

	/**
	 * 处理当天的排课算法
	 * 
	 * @author dlli5 at 2016年8月31日下午4:56:32
	 * @param signinInfos
	 *            本应上课的
	 * @param temporarySigninInfos
	 *            零时上课的
	 * @param allTemporarySigninInfos
	 *            永久安排的
	 * @param subtractedAttendanceList
	 *            不想上课的
	 * @return
	 */
	public List<SigninVo> dealTodayAttendance(List<SigninVo> signinInfos, List<SigninVo> temporarySigninInfos, List<SigninVo> allTemporarySigninInfos,
			List<SigninVo> subtractedAttendanceList) {

		List<SigninVo> result = new ArrayList<SigninVo>();

		if (ListUtil.isNotEmpty(signinInfos)) {
			// 查找在已经安排的list中是否存在临时和永久上课的可能性,如果存在零时和永久上课的可能性 那么直接忽略掉
			for (SigninVo signinVo : signinInfos) {
				dealTemporary(signinVo, temporarySigninInfos);
				dealTemporary(signinVo, allTemporarySigninInfos);
			}
		}

		// 如果临时存在而原来安排的不在 那么将临时的加入到上课队列
		if (ListUtil.isNotEmpty(temporarySigninInfos)) {
			for (SigninVo signinVo : temporarySigninInfos) {
				signinInfos.add(signinVo);
			}
		}
		if (ListUtil.isNotEmpty(allTemporarySigninInfos)) {
			for (SigninVo signinVo : allTemporarySigninInfos) {
				signinInfos.add(signinVo);
			}
		}

		// 减去不想上课的
		if (ListUtil.isNotEmpty(subtractedAttendanceList)) {
			for (SigninVo subtracted : subtractedAttendanceList) {
				for (SigninVo item : signinInfos) {
					if (item.getAccountId().equals(subtracted.getAccountId())) {
						signinInfos.remove(item);
						break;
					}
				}
			}
		}

		// 处理如果临时中存在被替换的对象 那么 被替换的对象不显示
		/*
		 * for (SigninVo have : signinInfos) { if (!have.isLeave()) {
		 * result.add(have); } }
		 */
		// 处理如果临时中存在被替换的对象 那么 被替换的对象不显示
		for (SigninVo have : signinInfos) {
			if (!have.isLeave()) {
				result.add(have);
			} else if (!haveRepalceChild(have, signinInfos)) {
				result.add(have);
			}
		}

		return result;
	}

	private void dealTemporary(SigninVo signinVo, List<SigninVo> temporarySigninInfos) {
		if (ListUtil.isEmpty(temporarySigninInfos)) {
			return;
		}

		for (SigninVo item : temporarySigninInfos) {
			if (item.getAccountId().equals(signinVo.getAccountId())) {
				temporarySigninInfos.remove(item);
				return;
			}
		}

	}

	/**
	 * 处理未来时间排课算法
	 * 
	 * @author dlli5 at 2016年8月31日下午4:58:27
	 * @param signinInfos
	 *            本应上课的
	 * @param temporarySigninInfos
	 *            临时上课的
	 * @param allTemporarySigninInfos
	 *            永久安排上课的
	 * @param attendanceRequests
	 *            申请队列中的 申请队列的要取上一个时间最近的安排
	 * @param subtractedAttendanceList
	 *            不想上课的
	 * @return
	 */
	public List<SigninVo> dealFutureAttendance(List<SigninVo> signinInfos, List<SigninVo> temporarySigninInfos, List<SigninVo> allTemporarySigninInfos,
			List<SigninVo> attendanceRequests, List<SigninVo> subtractedAttendanceList) {

		List<SigninVo> result = new ArrayList<SigninVo>();

		// 原本有的逻辑是 找符合条件的小孩是否有临近的上课安排 、
		// 如果有 不管是否发生了空间变化 那么都舍去

		if (ListUtil.isNotEmpty(signinInfos)) {
			// 查找在已经安排的list中是否存在临时和永久上课的可能性,如果存在零时和永久上课的可能性 那么直接忽略掉
			for (SigninVo signinVo : signinInfos) {
				dealTemporary(signinVo, temporarySigninInfos);
				dealTemporary(signinVo, allTemporarySigninInfos);
			}
		}

		// 如果临时存在而原来安排的不在 那么将临时的加入到上课队列
		if (ListUtil.isNotEmpty(temporarySigninInfos)) {
			for (SigninVo signinVo : temporarySigninInfos) {
				signinInfos.add(signinVo);
			}
		}
		if (ListUtil.isNotEmpty(allTemporarySigninInfos)) {
			for (SigninVo signinVo : allTemporarySigninInfos) {
				signinInfos.add(signinVo);
			}
		}

		// 处理有队列未来的上课安排的 如果发现目前之上的安排中有 那么干掉
		if (ListUtil.isNotEmpty(attendanceRequests)) {
			for (SigninVo request : attendanceRequests) {
				for (SigninVo old : signinInfos) {
					if (old.getAccountId().equals(request.getAccountId())) {
						signinInfos.remove(old);
						break;
					}
				}
			}
			for (SigninVo signinVo : attendanceRequests) {
				signinInfos.add(signinVo);
			}
		}

		// 减去不想上课的
		if (ListUtil.isNotEmpty(subtractedAttendanceList)) {
			for (SigninVo subtracted : subtractedAttendanceList) {
				for (SigninVo item : signinInfos) {
					if (item.getAccountId().equals(subtracted.getAccountId())) {
						signinInfos.remove(item);
						break;
					}
				}
			}
		}

		// 处理如果临时中存在被替换的对象 那么 被替换的对象不显示
		for (SigninVo have : signinInfos) {
			if (!have.isLeave()) {
				result.add(have);
			} else if (!haveRepalceChild(have, signinInfos)) {
				result.add(have);
			}
		}

		return result;
	}

	/**
	 * 是否有替换的小孩
	 * 
	 * @author dlli5 at 2016年12月23日上午10:38:37
	 * @param child
	 * @param signinInfos
	 * @return
	 */
	private boolean haveRepalceChild(SigninVo child, List<SigninVo> signinInfos) {
		for (SigninVo signinVo : signinInfos) {
			if (StringUtil.isNotEmpty(signinVo.getReplaceId()) && signinVo.getReplaceId().equals(child.getAccountId())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * @author dlli5 at 2016年12月23日上午10:38:50
	 * @param value
	 * @param oldChecked
	 * @return
	 */
	private boolean checkOldCheck(short value, boolean oldChecked) {
		if ((value == ChangeAttendanceRequestDayState.Check.getValue()) && oldChecked) {
			return true;
		}
		return false;
	}

	/**
	 * 处理家长申请
	 * 
	 * @author dlli5 at 2016年12月23日上午10:39:01
	 * @param childAttendanceInfo
	 * @param changeAttendanceRequestInfo
	 */
	public void dealParentAttendance(ChildAttendanceInfo childAttendanceInfo, ChangeAttendanceRequestInfo changeAttendanceRequestInfo) {
		if (checkOldCheck(changeAttendanceRequestInfo.getMonday(), childAttendanceInfo.getMonday())) {
			changeAttendanceRequestInfo.setMonday(ChangeAttendanceRequestDayState.UnCheck.getValue());
		}
		if (checkOldCheck(changeAttendanceRequestInfo.getTuesday(), childAttendanceInfo.getTuesday())) {
			changeAttendanceRequestInfo.setTuesday(ChangeAttendanceRequestDayState.UnCheck.getValue());
		}
		if (checkOldCheck(changeAttendanceRequestInfo.getWednesday(), childAttendanceInfo.getWednesday())) {
			changeAttendanceRequestInfo.setWednesday(ChangeAttendanceRequestDayState.UnCheck.getValue());
		}
		if (checkOldCheck(changeAttendanceRequestInfo.getThursday(), childAttendanceInfo.getThursday())) {
			changeAttendanceRequestInfo.setThursday(ChangeAttendanceRequestDayState.UnCheck.getValue());
		}
		if (checkOldCheck(changeAttendanceRequestInfo.getFriday(), childAttendanceInfo.getFriday())) {
			changeAttendanceRequestInfo.setFriday(ChangeAttendanceRequestDayState.UnCheck.getValue());
		}
	}

	/**
	 * 
	 * @author dlli5 at 2016年9月6日下午7:33:39
	 * @param signinInfos
	 * @return
	 */
	public List<AttendanceChildVo> convertToSigninVo(List<SigninVo> signinInfos, Date day, int dayOfWeek) {
		List<SigninVo> result = new ArrayList<SigninVo>();
		// 处理如果临时中存在被替换的对象 那么 被替换的对象不显示
		for (SigninVo have : signinInfos) {
			if (!have.isLeave()) {
				result.add(have);
			} else {
				// 找到替换的人
				List<TemporaryAttendanceInfo> temporaryAttendanceInfos = temporaryAttendanceInfoMapper.getReaplaceChilds(have.getAccountId(), day, dayOfWeek);
				have.setReplaceId(getRepalceChild(signinInfos, temporaryAttendanceInfos));
				if (StringUtil.isEmpty(have.getReplaceId())) {
					result.add(have);
				}
			}
		}

		List<AttendanceChildVo> list = new ArrayList<AttendanceChildVo>();
		for (SigninVo signinVo : result) {
			AttendanceChildVo temp = new AttendanceChildVo();
			temp.setCentersId(signinVo.getCentreId());
			temp.setRoomId(signinVo.getRoomId());
			temp.setChildId(signinVo.getAccountId());
			temp.setImg(signinVo.getImg());
			temp.setLeave(signinVo.isLeave());
			temp.setName(signinVo.getName());
			temp.setLastName(signinVo.getLastName());
			temp.setPersonColor(signinVo.getPersonColor());
			temp.setAbsenteeId(signinVo.getAbsenteeId());
			temp.setLastName(signinVo.getLastName());
			temp.setBirthday(signinVo.getBirthday());
			temp.setAge(com.aoyuntek.aoyun.uitl.DateUtil.calculateAge(signinVo.getBirthday(), day));
			temp.setInterim(signinVo.isInterim());
			if (signinVo.getState() != null) {
				temp.setSgin(signinVo.getState() == SigninState.Signin.getValue());
				// 如果签到了 那么这个人如果请假了 就不在显示这个人的可交换按钮了
				if (temp.isSgin()) {
					temp.setLeave(false);
				}
				if (signinVo.getState() == SigninState.NoSignin.getValue()) {
					temp.setLeave(false);
				}
			}
			if (signinVo.getSignOutState() != null) {
				temp.setSignOut(signinVo.getSignOutState() == SigninState.Signin.getValue());
			}
			list.add(temp);
		}
		return list;
	}

	/**
	 * 获取替换的小孩
	 * 
	 * @author dlli5 at 2016年12月23日上午10:39:22
	 * @param temporaryAttendanceInfos
	 * @return
	 */
	private String getRepalceChild(List<SigninVo> signinInfos, List<TemporaryAttendanceInfo> temporaryAttendanceInfos) {
		if (ListUtil.isEmpty(temporaryAttendanceInfos)) {
			return null;
		}

		String replaceId = null;
		for (TemporaryAttendanceInfo temporaryAttendanceInfo : temporaryAttendanceInfos) {
			replaceId = temporaryAttendanceInfo.getChildId();
			break;
		}

		if (StringUtil.isEmpty(replaceId)) {
			return null;
		}

		for (SigninVo signinVo : signinInfos) {
			if (signinVo.getAccountId().equals(replaceId)) {
				return replaceId;
			}
		}
		return null;
	}

	/**
	 * 
	 * @author dlli5 at 2016年9月13日下午6:42:57
	 * @param signinInfos
	 * @param findStr
	 * @return
	 */
	public List<AttendanceChildVo> convertToTodaySignInChilds(List<SigninVo> signinInfos, String findStr) {
		List<AttendanceChildVo> list = new ArrayList<AttendanceChildVo>();
		for (SigninVo signinVo : signinInfos) {
			if (signinVo.getState() != null) {
				continue;
			}
			if (StringUtil.isNotEmpty(findStr) && !signinVo.getName().toLowerCase().contains(findStr.trim().toLowerCase())) {
				continue;
			}
			AttendanceChildVo temp = new AttendanceChildVo();
			temp.setCentersId(signinVo.getCentreId());
			temp.setRoomId(signinVo.getRoomId());
			temp.setChildId(signinVo.getAccountId());
			temp.setImg(signinVo.getImg());
			temp.setLeave(signinVo.isLeave());
			temp.setName(signinVo.getName());
			temp.setLastName(signinVo.getLastName());
			temp.setPersonColor(signinVo.getPersonColor());
			temp.setAbsenteeId(signinVo.getAbsenteeId());
			temp.setLastName(signinVo.getLastName());
			temp.setSgin(false);
			list.add(temp);
		}
		return list;
	}

	/**
	 * 转换今天的簽出
	 * 
	 * @author dlli5 at 2016年12月23日上午10:39:34
	 * @param signinInfos
	 * @param findStr
	 * @return
	 */
	public List<AttendanceChildVo> convertToTodaySignOutChilds(List<SigninVo> signinInfos, String findStr) {
		List<AttendanceChildVo> list = new ArrayList<AttendanceChildVo>();
		for (SigninVo signinVo : signinInfos) {
			if (StringUtil.isNotEmpty(findStr) && !signinVo.getName().toLowerCase().contains(findStr.trim().toLowerCase())) {
				continue;
			}
			AttendanceChildVo temp = new AttendanceChildVo();
			temp.setCentersId(signinVo.getCentreId());
			temp.setRoomId(signinVo.getRoomId());
			temp.setChildId(signinVo.getAccountId());
			temp.setImg(signinVo.getImg());
			temp.setLeave(signinVo.isLeave());
			temp.setName(signinVo.getName());
			temp.setLastName(signinVo.getLastName());
			temp.setPersonColor(signinVo.getPersonColor());
			temp.setAbsenteeId(signinVo.getAbsenteeId());
			temp.setLastName(signinVo.getLastName());
			temp.setSgin(false);
			list.add(temp);
		}
		return list;
	}

	/**
	 * 处理 如果减去永久 那么需要对已有的Attendance做除去勾除操作
	 * 
	 * @author dlli5 at 2016年9月7日下午5:45:41
	 * @param attendanceInfo
	 * @param day
	 * @return
	 */
	public boolean dealSubtractedAttendance(ChildAttendanceInfo attendanceInfo, Date day) {
		int dayOfWeek = DateUtil.getDayOfWeek(day) - 1;
		switch (dayOfWeek) {
		case 1:
			if (attendanceInfo.getMonday()) {
				attendanceInfo.setMonday(false);
				return true;
			}
			break;
		case 2:
			if (attendanceInfo.getTuesday()) {
				attendanceInfo.setTuesday(false);
				return true;
			}
			break;
		case 3:
			if (attendanceInfo.getWednesday()) {
				attendanceInfo.setWednesday(false);
				return true;
			}
			break;
		case 4:
			if (attendanceInfo.getThursday()) {
				attendanceInfo.setThursday(false);
				return true;
			}
			break;
		case 5:
			if (attendanceInfo.getFriday()) {
				attendanceInfo.setFriday(false);
				return true;
			}
			break;
		}
		return false;
	}

	/**
	 * 是否处理全部
	 * 
	 * @author dlli5 at 2016年12月23日上午10:40:05
	 * @param changeAttendanceRequestInfo
	 * @return
	 */
	public boolean isDealAll(ChangeAttendanceRequestInfo changeAttendanceRequestInfo) {
		ChildAttendanceInfo attendanceInfo = childAttendanceInfoMapper.selectByAccountId(changeAttendanceRequestInfo.getChildId());
		if (changeAttendanceRequestInfo.getMonday() == ChangeAttendanceRequestDayState.Check.getValue() && !attendanceInfo.getMonday()) {
			return false;
		}
		if (changeAttendanceRequestInfo.getTuesday() == ChangeAttendanceRequestDayState.Check.getValue() && !attendanceInfo.getTuesday()) {
			return false;
		}
		if (changeAttendanceRequestInfo.getWednesday() == ChangeAttendanceRequestDayState.Check.getValue() && !attendanceInfo.getWednesday()) {
			return false;
		}
		if (changeAttendanceRequestInfo.getThursday() == ChangeAttendanceRequestDayState.Check.getValue() && !attendanceInfo.getThursday()) {
			return false;
		}
		if (changeAttendanceRequestInfo.getFriday() == ChangeAttendanceRequestDayState.Check.getValue() && !attendanceInfo.getFriday()) {
			return false;
		}
		// 判断是否有取消上课周期
		if (changeAttendanceRequestInfo.getMonday() == ChangeAttendanceRequestDayState.Cancel.getValue()) {
			if (changeAttendanceRequestInfo.getDayOfWeek() != Week.Monday.getValue()) {
				return false;
			}
		}
		if (changeAttendanceRequestInfo.getTuesday() == ChangeAttendanceRequestDayState.Cancel.getValue()) {
			if (changeAttendanceRequestInfo.getDayOfWeek() != Week.Tuesday.getValue()) {
				return false;
			}
		}
		if (changeAttendanceRequestInfo.getWednesday() == ChangeAttendanceRequestDayState.Cancel.getValue()) {
			if (changeAttendanceRequestInfo.getDayOfWeek() != Week.Wednesday.getValue()) {
				return false;
			}
		}
		if (changeAttendanceRequestInfo.getThursday() == ChangeAttendanceRequestDayState.Cancel.getValue()) {
			if (changeAttendanceRequestInfo.getDayOfWeek() != Week.Thursday.getValue()) {
				return false;
			}
		}
		if (changeAttendanceRequestInfo.getFriday() == ChangeAttendanceRequestDayState.Cancel.getValue()) {
			if (changeAttendanceRequestInfo.getDayOfWeek() != Week.Friday.getValue()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 处理家长申请取消上课周期
	 * 
	 * @param changeAttendanceRequestInfo
	 */
	public void dealParentCancel(ChangeAttendanceRequestInfo changeAttendanceRequestInfo) {
		ChildAttendanceInfo attendanceInfo = childAttendanceInfoMapper.selectByAccountId(changeAttendanceRequestInfo.getChildId());
		if (changeAttendanceRequestInfo.getMonday() == ChangeAttendanceRequestDayState.UnCheck.getValue() && attendanceInfo.getMonday()) {
			changeAttendanceRequestInfo.setMonday(ChangeAttendanceRequestDayState.Cancel.getValue());
		}
		if (changeAttendanceRequestInfo.getTuesday() == ChangeAttendanceRequestDayState.UnCheck.getValue() && attendanceInfo.getTuesday()) {
			changeAttendanceRequestInfo.setTuesday(ChangeAttendanceRequestDayState.Cancel.getValue());
		}
		if (changeAttendanceRequestInfo.getWednesday() == ChangeAttendanceRequestDayState.UnCheck.getValue() && attendanceInfo.getWednesday()) {
			changeAttendanceRequestInfo.setWednesday(ChangeAttendanceRequestDayState.Cancel.getValue());
		}
		if (changeAttendanceRequestInfo.getThursday() == ChangeAttendanceRequestDayState.UnCheck.getValue() && attendanceInfo.getThursday()) {
			changeAttendanceRequestInfo.setThursday(ChangeAttendanceRequestDayState.Cancel.getValue());
		}
		if (changeAttendanceRequestInfo.getFriday() == ChangeAttendanceRequestDayState.UnCheck.getValue() && attendanceInfo.getFriday()) {
			changeAttendanceRequestInfo.setFriday(ChangeAttendanceRequestDayState.Cancel.getValue());
		}
	}

	/**
	 * 
	 * @author dlli5 at 2016年12月23日上午10:40:15
	 * @param attendanceRequestInfo
	 * @param attendanceInfo
	 * @return
	 */
	public boolean isOldContain(ChangeAttendanceRequestInfo attendanceRequestInfo, ChildAttendanceInfo attendanceInfo) {
		if (attendanceRequestInfo.getMonday() == ChangeAttendanceRequestDayState.Check.getValue()) {
			if (!attendanceInfo.getMonday()) {
				return false;
			}
		}
		if (attendanceRequestInfo.getTuesday() == ChangeAttendanceRequestDayState.Check.getValue()) {
			if (!attendanceInfo.getTuesday()) {
				return false;
			}
		}
		if (attendanceRequestInfo.getWednesday() == ChangeAttendanceRequestDayState.Check.getValue()) {
			if (!attendanceInfo.getWednesday()) {
				return false;
			}
		}
		if (attendanceRequestInfo.getThursday() == ChangeAttendanceRequestDayState.Check.getValue()) {
			if (!attendanceInfo.getThursday()) {
				return false;
			}
		}
		if (attendanceRequestInfo.getFriday() == ChangeAttendanceRequestDayState.Check.getValue()) {
			if (!attendanceInfo.getFriday()) {
				return false;
			}
		}
		return true;
	}

	public List<Short> getChangeDayOfAttendance(ChildAttendanceInfo attendanceInfo) {
		List<Short> dayOfWeeks = new ArrayList<Short>();
		if (BooleanUtils.isTrue(attendanceInfo.getMonday())) {
			dayOfWeeks.add((short) 1);
		}
		if (BooleanUtils.isTrue(attendanceInfo.getTuesday())) {
			dayOfWeeks.add((short) 2);
		}
		if (BooleanUtils.isTrue(attendanceInfo.getWednesday())) {
			dayOfWeeks.add((short) 3);
		}
		if (BooleanUtils.isTrue(attendanceInfo.getThursday())) {
			dayOfWeeks.add((short) 4);
		}
		if (BooleanUtils.isTrue(attendanceInfo.getFriday())) {
			dayOfWeeks.add((short) 5);
		}
		return dayOfWeeks;
	}

	/**
	 * 
	 * @author dlli5 at 2016年9月10日下午3:27:10
	 * @param attendanceInfo
	 * @return
	 */
	public List<Short> getChangeDayOfAttendance(String userId, ChildAttendanceInfo attendanceInfo) {
		List<Short> dayOfWeeks = new ArrayList<Short>();
		if (StringUtil.isEmpty(userId)) {
			if (BooleanUtils.isTrue(attendanceInfo.getMonday())) {
				dayOfWeeks.add((short) 1);
			}
			if (BooleanUtils.isTrue(attendanceInfo.getTuesday())) {
				dayOfWeeks.add((short) 2);
			}
			if (BooleanUtils.isTrue(attendanceInfo.getWednesday())) {
				dayOfWeeks.add((short) 3);
			}
			if (BooleanUtils.isTrue(attendanceInfo.getThursday())) {
				dayOfWeeks.add((short) 4);
			}
			if (BooleanUtils.isTrue(attendanceInfo.getFriday())) {
				dayOfWeeks.add((short) 5);
			}
		} else {
			ChildAttendanceInfo old = childAttendanceInfoMapper.selectByUserId(userId);
			if (BooleanUtils.isTrue(attendanceInfo.getMonday()) && (old.getEnrolled() == EnrolledState.Default.getValue() && BooleanUtils.isFalse(old.getMonday()))) {
				dayOfWeeks.add((short) 1);
			}
			if (BooleanUtils.isTrue(attendanceInfo.getTuesday()) && (old.getEnrolled() == EnrolledState.Default.getValue() && BooleanUtils.isFalse(old.getTuesday()))) {
				dayOfWeeks.add((short) 2);
			}
			if (BooleanUtils.isTrue(attendanceInfo.getWednesday())
					&& (old.getEnrolled() == EnrolledState.Default.getValue() && BooleanUtils.isFalse(old.getWednesday()))) {
				dayOfWeeks.add((short) 3);
			}
			if (BooleanUtils.isTrue(attendanceInfo.getThursday()) && (old.getEnrolled() == EnrolledState.Default.getValue() && BooleanUtils.isFalse(old.getThursday()))) {
				dayOfWeeks.add((short) 4);
			}
			if (BooleanUtils.isTrue(attendanceInfo.getFriday()) && (old.getEnrolled() == EnrolledState.Default.getValue() && BooleanUtils.isFalse(old.getFriday()))) {
				dayOfWeeks.add((short) 5);
			}
		}

		return dayOfWeeks;
	}

	/**
	 * 获取排课天index
	 * 
	 * @author dlli5 at 2016年12月23日上午10:40:19
	 * @param userId
	 * @param re
	 * @return
	 */
	public List<Short> getDayOfWeeks(String userId, ChangeAttendanceRequestInfo re) {
		List<Short> dayOfWeeks = new ArrayList<Short>();
		boolean checkAll = false;
		if (StringUtil.isEmpty(userId)) {
			checkAll = true;
		} else {
			UserInfo userInfo = userInfoMapper.selectByPrimaryKey(userId);
			if (StringUtil.isNotEmpty(userInfo.getRoomId()) && (!userInfo.getRoomId().equals(re.getNewRoomId()))) {
				checkAll = true;
			}
		}
		if (checkAll) {
			if (re.getMonday() != null && re.getMonday() == ChangeAttendanceRequestDayState.Check.getValue()) {
				dayOfWeeks.add((short) 1);
			}
			if (re.getTuesday() != null && re.getTuesday() == ChangeAttendanceRequestDayState.Check.getValue()) {
				dayOfWeeks.add((short) 2);
			}
			if (re.getWednesday() != null && re.getWednesday() == ChangeAttendanceRequestDayState.Check.getValue()) {
				dayOfWeeks.add((short) 3);
			}
			if (re.getThursday() != null && re.getThursday() == ChangeAttendanceRequestDayState.Check.getValue()) {
				dayOfWeeks.add((short) 4);
			}
			if (re.getFriday() != null && re.getFriday() == ChangeAttendanceRequestDayState.Check.getValue()) {
				dayOfWeeks.add((short) 5);
			}
		} else {
			AttendancePostion attendancePostion = getChildPostion(re.getChildId(), re.getChangeDate());
			if (re.getMonday() != null && re.getMonday() == ChangeAttendanceRequestDayState.Check.getValue() && (BooleanUtils.isFalse(attendancePostion.getMonday()))) {
				dayOfWeeks.add((short) 1);
			}
			if (re.getTuesday() != null && re.getTuesday() == ChangeAttendanceRequestDayState.Check.getValue()
					&& (BooleanUtils.isFalse(attendancePostion.getTuesday()))) {
				dayOfWeeks.add((short) 2);
			}
			if (re.getWednesday() != null && re.getWednesday() == ChangeAttendanceRequestDayState.Check.getValue()
					&& (BooleanUtils.isFalse(attendancePostion.getWednesday()))) {
				dayOfWeeks.add((short) 3);
			}
			if (re.getThursday() != null && re.getThursday() == ChangeAttendanceRequestDayState.Check.getValue()
					&& (BooleanUtils.isFalse(attendancePostion.getThursday()))) {
				dayOfWeeks.add((short) 4);
			}
			if (re.getFriday() != null && re.getFriday() == ChangeAttendanceRequestDayState.Check.getValue() && (BooleanUtils.isFalse(attendancePostion.getFriday()))) {
				dayOfWeeks.add((short) 5);
			}
		}
		return dayOfWeeks;
	}

	/**
	 * 
	 * @author dlli5 at 2016年9月13日下午9:03:54
	 * @param adminAttendanceRequestInfo
	 * @param changeAttendanceRequestInfo
	 */
	public void dealParentAttendanceWithAdminChange(ChangeAttendanceRequestInfo adminAttendanceRequestInfo, ChangeAttendanceRequestInfo changeAttendanceRequestInfo) {
		// 判断管理员申请是否全覆盖家长申请
		boolean dealFullCoverage = true;
		if (!sameCheck(adminAttendanceRequestInfo.getMonday(), changeAttendanceRequestInfo.getMonday())) {
			dealFullCoverage = false;
		}
		if (!sameCheck(adminAttendanceRequestInfo.getTuesday(), changeAttendanceRequestInfo.getTuesday())) {
			dealFullCoverage = false;
		}
		if (!sameCheck(adminAttendanceRequestInfo.getWednesday(), changeAttendanceRequestInfo.getWednesday())) {
			dealFullCoverage = false;
		}
		if (!sameCheck(adminAttendanceRequestInfo.getThursday(), changeAttendanceRequestInfo.getThursday())) {
			dealFullCoverage = false;
		}
		if (!sameCheck(adminAttendanceRequestInfo.getFriday(), changeAttendanceRequestInfo.getFriday())) {
			dealFullCoverage = false;
		}

		// 处理家长取消的上课周期
		dealParentCancelAttends(adminAttendanceRequestInfo, changeAttendanceRequestInfo);

		boolean dealSame = false;
		if (adminAttendanceRequestInfo.getMonday() != ChangeAttendanceRequestDayState.UnCheck.getValue()) {
			if (changeAttendanceRequestInfo.getMonday() == ChangeAttendanceRequestDayState.Check.getValue()) {
				changeAttendanceRequestInfo.setMonday(ChangeAttendanceRequestDayState.Deal.getValue());
				dealSame = true;
			}
		}
		if (adminAttendanceRequestInfo.getTuesday() != ChangeAttendanceRequestDayState.UnCheck.getValue()) {
			if (changeAttendanceRequestInfo.getTuesday() == ChangeAttendanceRequestDayState.Check.getValue()) {
				changeAttendanceRequestInfo.setTuesday(ChangeAttendanceRequestDayState.Deal.getValue());
				dealSame = true;
			}
		}
		if (adminAttendanceRequestInfo.getWednesday() != ChangeAttendanceRequestDayState.UnCheck.getValue()) {
			if (changeAttendanceRequestInfo.getWednesday() == ChangeAttendanceRequestDayState.Check.getValue()) {
				changeAttendanceRequestInfo.setWednesday(ChangeAttendanceRequestDayState.Deal.getValue());
				dealSame = true;
			}
		}
		if (adminAttendanceRequestInfo.getThursday() != ChangeAttendanceRequestDayState.UnCheck.getValue()) {
			if (changeAttendanceRequestInfo.getThursday() == ChangeAttendanceRequestDayState.Check.getValue()) {
				changeAttendanceRequestInfo.setThursday(ChangeAttendanceRequestDayState.Deal.getValue());
				dealSame = true;
			}
		}
		if (adminAttendanceRequestInfo.getFriday() != ChangeAttendanceRequestDayState.UnCheck.getValue()) {
			if (changeAttendanceRequestInfo.getFriday() == ChangeAttendanceRequestDayState.Check.getValue()) {
				changeAttendanceRequestInfo.setFriday(ChangeAttendanceRequestDayState.Deal.getValue());
				dealSame = true;
			}
		}

		// 如果全部处理了 那么将家长的申请状态改为已处理
		if (dealFullCoverage) {
			changeAttendanceRequestInfo.setState(ChangeAttendanceRequestState.Over.getValue());
		} else if (dealSame) {
			changeAttendanceRequestInfo.setState(ChangeAttendanceRequestState.PartialAproved.getValue());
		}
	}

	private boolean sameCheck(short adminRequest, short parnetRequest) {
		short unCheck = ChangeAttendanceRequestDayState.UnCheck.getValue();
		short check = ChangeAttendanceRequestDayState.Check.getValue();
		short cancel = ChangeAttendanceRequestDayState.Cancel.getValue();
		if (adminRequest == unCheck && parnetRequest == check) {
			return false;
		}
		if (adminRequest == check && (parnetRequest == unCheck || parnetRequest == cancel)) {
			return false;
		}
		return true;
	}

	/**
	 * 处理管理员覆盖的家长取消的上课周期
	 * 
	 * @param adminAtt
	 * @param parentAtt
	 */
	private void dealParentCancelAttends(ChangeAttendanceRequestInfo adminAtt, ChangeAttendanceRequestInfo parentAtt) {
		if (parentAtt.getMonday() == ChangeAttendanceRequestDayState.Cancel.getValue() && adminAtt.getMonday() == ChangeAttendanceRequestDayState.UnCheck.getValue()) {
			parentAtt.setMonday(ChangeAttendanceRequestDayState.UnCheck.getValue());
		}
		if (parentAtt.getTuesday() == ChangeAttendanceRequestDayState.Cancel.getValue() && adminAtt.getTuesday() == ChangeAttendanceRequestDayState.UnCheck.getValue()) {
			parentAtt.setTuesday(ChangeAttendanceRequestDayState.UnCheck.getValue());
		}
		if (parentAtt.getWednesday() == ChangeAttendanceRequestDayState.Cancel.getValue()
				&& adminAtt.getWednesday() == ChangeAttendanceRequestDayState.UnCheck.getValue()) {
			parentAtt.setWednesday(ChangeAttendanceRequestDayState.UnCheck.getValue());
		}
		if (parentAtt.getThursday() == ChangeAttendanceRequestDayState.Cancel.getValue()
				&& adminAtt.getThursday() == ChangeAttendanceRequestDayState.UnCheck.getValue()) {
			parentAtt.setThursday(ChangeAttendanceRequestDayState.UnCheck.getValue());
		}
		if (parentAtt.getFriday() == ChangeAttendanceRequestDayState.Cancel.getValue() && adminAtt.getFriday() == ChangeAttendanceRequestDayState.UnCheck.getValue()) {
			parentAtt.setFriday(ChangeAttendanceRequestDayState.UnCheck.getValue());
		}
	}

	/**
	 * 
	 * @author dlli5 at 2016年12月23日上午10:40:43
	 * @param attendanceHistoryInfo
	 * @param changeAttendanceRequestInfo
	 */
	public void dealAttendanceHistory(AttendanceHistoryInfo attendanceHistoryInfo, ChangeAttendanceRequestInfo changeAttendanceRequestInfo) {
		if (changeAttendanceRequestInfo.getMonday() != null && changeAttendanceRequestInfo.getMonday() != ChangeAttendanceRequestDayState.UnCheck.getValue()) {
			attendanceHistoryInfo.setMonday(true);
		}
		if (changeAttendanceRequestInfo.getTuesday() != null && changeAttendanceRequestInfo.getTuesday() != ChangeAttendanceRequestDayState.UnCheck.getValue()) {
			attendanceHistoryInfo.setTuesday(true);
		}
		if (changeAttendanceRequestInfo.getWednesday() != null && changeAttendanceRequestInfo.getWednesday() != ChangeAttendanceRequestDayState.UnCheck.getValue()) {
			attendanceHistoryInfo.setWednesday(true);
		}
		if (changeAttendanceRequestInfo.getThursday() != null && changeAttendanceRequestInfo.getThursday() != ChangeAttendanceRequestDayState.UnCheck.getValue()) {
			attendanceHistoryInfo.setThursday(true);
		}
		if (changeAttendanceRequestInfo.getFriday() != null && changeAttendanceRequestInfo.getFriday() != ChangeAttendanceRequestDayState.UnCheck.getValue()) {
			attendanceHistoryInfo.setFriday(true);
		}
	}

	/**
	 * 
	 * @author dlli5 at 2016年12月23日上午10:40:47
	 * @param attendanceRequestInfo
	 * @param childAttendanceInfo
	 */
	public void dealAttendanceRequest(ChangeAttendanceRequestInfo attendanceRequestInfo, ChildAttendanceInfo childAttendanceInfo) {
		if (childAttendanceInfo.getMonday() != null && childAttendanceInfo.getMonday()) {
			attendanceRequestInfo.setMonday(ChangeAttendanceRequestDayState.Check.getValue());
		}
		if (childAttendanceInfo.getTuesday() != null && childAttendanceInfo.getTuesday()) {
			attendanceRequestInfo.setTuesday(ChangeAttendanceRequestDayState.Check.getValue());
		}
		if (childAttendanceInfo.getWednesday() != null && childAttendanceInfo.getWednesday()) {
			attendanceRequestInfo.setWednesday(ChangeAttendanceRequestDayState.Check.getValue());
		}
		if (childAttendanceInfo.getThursday() != null && childAttendanceInfo.getThursday()) {
			attendanceRequestInfo.setThursday(ChangeAttendanceRequestDayState.Check.getValue());
		}
		if (childAttendanceInfo.getFriday() != null && childAttendanceInfo.getFriday()) {
			attendanceRequestInfo.setFriday(ChangeAttendanceRequestDayState.Check.getValue());
		}
	}

	/**
	 * 處理合併
	 * 
	 * @author dlli5 at 2016年12月23日上午10:40:51
	 * @param signinInfos
	 * @param oldSigninfos
	 * @return
	 */
	public List<SigninVo> dealMergeAttendance(List<SigninVo> signinInfos, List<SigninVo> oldSigninfos) {
		for (SigninVo old : oldSigninfos) {
			if (old.getState() != null && old.getState() == SigninState.Signin.getValue()) {
				boolean haveOld = false;
				for (SigninVo today : signinInfos) {
					if (today.getAccountId().equals(old.getAccountId())) {
						if (today.getRoomId().equals(old.getRoomId())) {
							haveOld = true;
						}

					}
				}
				if (!haveOld) {
					signinInfos.add(old);
				}
			}
		}
		return signinInfos;
	}

	/**
	 * 是否仅仅变更了Group
	 * 
	 * @author dlli5 at 2016年12月23日上午10:40:58
	 * @param childInfo
	 * @param attendanceRequestInfo
	 * @return
	 */
	public boolean justChangeGroup(UserInfo childInfo, ChangeAttendanceRequestInfo attendanceRequestInfo) {
		if (StringUtil.isNotEmpty(childInfo.getCentersId()) && StringUtil.isNotEmpty(attendanceRequestInfo.getNewCentreId())
				&& childInfo.getCentersId().equals(attendanceRequestInfo.getNewCentreId())) {
			if (StringUtil.isNotEmpty(childInfo.getRoomId()) && StringUtil.isNotEmpty(attendanceRequestInfo.getNewRoomId())
					&& childInfo.getRoomId().equals(attendanceRequestInfo.getNewRoomId())) {
				if (StringUtil.isNotEmpty(childInfo.getGroupId()) && StringUtil.isNotEmpty(attendanceRequestInfo.getNewGroupId())
						&& !childInfo.getGroupId().equals(attendanceRequestInfo.getNewGroupId())) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * 转换小孩上课为天 的index
	 * 
	 * @author dlli5 at 2016年12月23日上午10:41:17
	 * @param childAttendanceInfo
	 * @return
	 */
	public List<Integer> convertChildAttendanceInfo(ChildAttendanceInfo childAttendanceInfo) {
		List<Integer> attendanceDays = new ArrayList<Integer>();
		if (BooleanUtils.isTrue(childAttendanceInfo.getMonday())) {
			attendanceDays.add(1);
		}
		if (BooleanUtils.isTrue(childAttendanceInfo.getTuesday())) {
			attendanceDays.add(2);
		}
		if (BooleanUtils.isTrue(childAttendanceInfo.getWednesday())) {
			attendanceDays.add(3);
		}
		if (BooleanUtils.isTrue(childAttendanceInfo.getThursday())) {
			attendanceDays.add(4);
		}
		if (BooleanUtils.isTrue(childAttendanceInfo.getFriday())) {
			attendanceDays.add(5);
		}
		return attendanceDays;
	}

	/**
	 * 轉換上課天數為 ChildAttendanceInfo
	 * 
	 * @author dlli5 at 2016年12月23日上午10:41:41
	 * @param weekDays
	 * @return
	 */
	public ChildAttendanceInfo convertChildAttendanceInfo(List<Integer> weekDays) {
		ChildAttendanceInfo childAttendanceInfo = new ChildAttendanceInfo();
		if (weekDays.contains(1)) {
			childAttendanceInfo.setMonday(true);
		}
		if (weekDays.contains(2)) {
			childAttendanceInfo.setTuesday(true);
		}
		if (weekDays.contains(3)) {
			childAttendanceInfo.setWednesday(true);
		}
		if (weekDays.contains(4)) {
			childAttendanceInfo.setThursday(true);
		}
		if (weekDays.contains(5)) {
			childAttendanceInfo.setFriday(true);
		}
		return childAttendanceInfo;
	}

	/**
	 * 判断未来是否有空间改变的安排
	 * 
	 * @author dlli5 at 2016年12月8日上午11:37:38
	 * @param accountId
	 * @param attendanceRequestInfos
	 * @return
	 */
	public ChangeAttendanceRequestInfo haveFutureCentreChange(String accountId, List<ChangeAttendanceRequestInfo> attendanceRequestInfos) {
		// 获取目前小孩的空间信息
		UserInfo userInfo = userInfoMapper.getUserInfoByAccountId(accountId);
		if (ListUtil.isNotEmpty(attendanceRequestInfos)) {
			for (ChangeAttendanceRequestInfo req : attendanceRequestInfos) {
				if (StringUtil.isNotEmpty(userInfo.getCentersId()) && !req.getNewCentreId().equals(userInfo.getCentersId())) {
					return req;
				}
			}
		}
		return null;
	}

	/**
	 * 转换为请求源
	 * 
	 * @author dlli5 at 2016年12月23日上午10:42:22
	 * @param re
	 */
	public void convertSourceType(ChangeAttendanceRequestInfo re) {
		List<Short> source = new ArrayList<Short>();
		source.add(ChangeAttendanceRequestType.ParentRequest.getValue());
		source.add(ChangeAttendanceRequestType.ChangeAttendance.getValue());
		if (source.contains(re.getSourceType())) {
			re.setType(AttendanceRequestType.K.getValue());
			return;
		}
		source.clear();
		source.add(ChangeAttendanceRequestType.ChangeRoom.getValue());
		if (source.contains(re.getSourceType())) {
			re.setType(AttendanceRequestType.R.getValue());
			return;
		}
		re.setType(AttendanceRequestType.C.getValue());
	}

	/**
	 * 获取今天的排课小孩
	 * 
	 * @author dlli5 at 2016年12月23日上午10:42:34
	 * @param signinInfos
	 * @param roomChangeInfos
	 * @param oldSigninfos
	 * @param day
	 * @return
	 */
	public List<SigninVo> getTodayAttendanceChild(List<SigninVo> signinInfos, List<RoomChangeInfo> roomChangeInfos, List<SigninVo> oldSigninfos, Date day) {
		if (ListUtil.isNotEmpty(roomChangeInfos)) {
			Map<String, List<RoomChangeInfo>> userChange = convertTOUserMap(roomChangeInfos);
			for (String userId : userChange.keySet()) {
				int lastRsult = 0;
				List<RoomChangeInfo> roomChangeMap = userChange.get(userId);
				for (RoomChangeInfo change : roomChangeMap) {
					RoomChangeOperate operate = RoomChangeOperate.getType(change.getOperate());
					switch (operate) {
					case REMOVEALL:
					case REMOVEDAY: {
						lastRsult--;
					}
						break;
					case ADDALL:
					case ADDDAY: {
						lastRsult++;
					}
						break;
					default:
						break;
					}
				}
				if (lastRsult > 0) {
					boolean needAdd = true;
					for (SigninVo signinVo : signinInfos) {
						if (signinVo.getAccountId().equals(userId)) {
							needAdd = false;
							break;
						}
					}
					if (needAdd) {
						// 找到这个人的签到信息
						SigninVo signLog = signinInfoMapper.getChildSigninVo(userId, day);
						if (signLog != null) {
							signinInfos.add(signLog);
						}
					}
				} else if (lastRsult < 0) {
					for (SigninVo signinVo : signinInfos) {
						if (signinVo.getAccountId().equals(userId)) {
							signinInfos.remove(signinVo);
							break;
						}
					}
				}
			}
		}

		if (ListUtil.isNotEmpty(oldSigninfos)) {
			for (SigninVo signinVo : oldSigninfos) {
				boolean have = false;
				for (SigninVo temp : signinInfos) {
					if (temp.getAccountId().equals(signinVo.getAccountId())) {
						have = true;
						break;
					}
				}
				if (!have) {
					signinInfos.add(signinVo);
				}
			}
		}
		return signinInfos;
	}

	public Map<String, List<RoomChangeInfo>> convertTOUserMap(List<RoomChangeInfo> changeInfos) {
		Map<String, List<RoomChangeInfo>> userChange = new HashMap<String, List<RoomChangeInfo>>();
		for (RoomChangeInfo change : changeInfos) {
			if (userChange.containsKey(change.getChildId())) {
				userChange.get(change.getChildId()).add(change);
			} else {
				List<RoomChangeInfo> temp = new ArrayList<RoomChangeInfo>();
				temp.add(change);
				userChange.put(change.getChildId(), temp);
			}
		}
		return userChange;
	}

	/**
	 * 模拟推进
	 * 
	 * @author dlli5 at 2016年12月14日上午11:11:56
	 * @param roomChilds
	 * @param changeInfos
	 * @param day
	 * @return
	 */
	public List<SigninVo> getFutureAttendanceChild(List<String> roomChilds, List<RoomChangeInfo> changeInfos, Date day) {
		Map<String, List<RoomChangeInfo>> userChange = convertTOUserMap(changeInfos);

		for (String userId : userChange.keySet()) {
			int lastRsult = 0;
			List<RoomChangeInfo> roomChangeMap = userChange.get(userId);
			// ++++++++++++++++++++++++++++++++++++
			if (roomChilds.contains(userId)) {
				lastRsult++;
			}
			// ++++++++++++++++++++++++++++++++++++
			for (RoomChangeInfo change : roomChangeMap) {

				RoomChangeOperate operate = RoomChangeOperate.getType(change.getOperate());
				switch (operate) {
				case REMOVEALL: {
					if (lastRsult > 0)
						lastRsult--;
				}
					break;
				case REMOVEDAY: {
					if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(change.getDay(), day) == 0) {
						lastRsult--;
					}
				}
					break;
				case ADDALL: {
					lastRsult++;
				}
					break;
				case ADDDAY: {
					if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(change.getDay(), day) == 0) {
						lastRsult++;
					}
				}
					break;
				default:
					break;
				}
			}
			if (lastRsult > 0) {
				if (!roomChilds.contains(userId)) {
					roomChilds.add(userId);
				}
			} else if (lastRsult <= 0) {
				if (roomChilds.contains(userId)) {
					roomChilds.remove(userId);
				}
			}
		}

		if (ListUtil.isEmpty(roomChilds)) {
			return new ArrayList<SigninVo>();
		}
		return signinInfoMapper.getChildSigninVos(roomChilds, day);
	}

	/**
	 * 
	 * @author dlli5 at 2016年12月23日上午10:42:52
	 * @param re
	 * @return
	 */
	public List<RoomChangeInfo> getRoomChangeInfoList(ChangeAttendanceRequestInfo re) {
		List<Integer> dayOfWeeks = new ArrayList<Integer>();
		if (re.getMonday() != null && re.getMonday() == ChangeAttendanceRequestDayState.Check.getValue()) {
			dayOfWeeks.add(1);
		}
		if (re.getTuesday() != null && re.getTuesday() == ChangeAttendanceRequestDayState.Check.getValue()) {
			dayOfWeeks.add(2);
		}
		if (re.getWednesday() != null && re.getWednesday() == ChangeAttendanceRequestDayState.Check.getValue()) {
			dayOfWeeks.add(3);
		}
		if (re.getThursday() != null && re.getThursday() == ChangeAttendanceRequestDayState.Check.getValue()) {
			dayOfWeeks.add(4);
		}
		if (re.getFriday() != null && re.getFriday() == ChangeAttendanceRequestDayState.Check.getValue()) {
			dayOfWeeks.add(5);
		}
		List<RoomChangeInfo> changeInfos = new ArrayList<RoomChangeInfo>();
		for (int dayOfWeek : dayOfWeeks) {
			RoomChangeInfo roomChangeInfo = new RoomChangeInfo();
			roomChangeInfo.setCentreId(re.getNewCentreId());
			roomChangeInfo.setChildId(re.getChildId());
			roomChangeInfo.setOperate(RoomChangeOperate.ADDALL.getValue());
			roomChangeInfo.setReqId(re.getId());
			roomChangeInfo.setRoomId(re.getNewRoomId());
			roomChangeInfo.setDayWeek((short) dayOfWeek);
			roomChangeInfo.setDay(com.aoyuntek.aoyun.uitl.DateUtil.getBeginDateOfDateWeek(re.getChangeDate(), dayOfWeek));
			changeInfos.add(roomChangeInfo);
		}
		return changeInfos;
	}

	/**
	 * 
	 * @author dlli5 at 2016年12月23日上午10:42:57
	 * @param childId
	 * @param reqId
	 * @param attendancePostion
	 * @param changeDate
	 * @return
	 */
	public List<RoomChangeInfo> getRemoveRoomChangeInfoList(String childId, String reqId, AttendancePostion attendancePostion, Date changeDate) {
		List<Integer> dayOfWeeks = new ArrayList<Integer>();
		dayOfWeeks.add(1);
		dayOfWeeks.add(2);
		dayOfWeeks.add(3);
		dayOfWeeks.add(4);
		dayOfWeeks.add(5);
		List<RoomChangeInfo> changeInfos = new ArrayList<RoomChangeInfo>();
		for (int dayOfWeek : dayOfWeeks) {
			RoomChangeInfo roomChangeInfo = new RoomChangeInfo();
			roomChangeInfo.setCentreId(attendancePostion.getCentreId());
			roomChangeInfo.setChildId(childId);
			roomChangeInfo.setOperate(RoomChangeOperate.REMOVEALL.getValue());
			roomChangeInfo.setReqId(reqId);
			roomChangeInfo.setRoomId(attendancePostion.getRoomId());
			roomChangeInfo.setDayWeek((short) dayOfWeek);
			roomChangeInfo.setDay(com.aoyuntek.aoyun.uitl.DateUtil.getBeginDateOfDateWeek(changeDate, dayOfWeek));
			changeInfos.add(roomChangeInfo);
		}
		return changeInfos;
	}

	/**
	 * 獲取下一次變更
	 * 
	 * @author dlli5 at 2016年12月23日上午10:43:01
	 * @param accountId
	 * @param changeDate
	 * @return
	 */
	public ChangeAttendanceRequestInfo getNextChange(String accountId, Date changeDate) {
		Date now = new Date();
		// 获取这段时间改变的changeAttendance
		List<ChangeAttendanceRequestInfo> attendanceRequestInfos = changeAttendanceRequestInfoMapper.getListByChildAndDateScope(accountId, now, null);
		if (ListUtil.isEmpty(attendanceRequestInfos)) {
			return null;
		}
		for (ChangeAttendanceRequestInfo re : attendanceRequestInfos) {
			if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(changeDate, re.getChangeDate()) > 0) {
				return re;
			}
		}
		return null;
	}

	public void dealAddOrRemove(AttendancePostion postion, RoomChangeInfo re, boolean add) {
		switch (re.getDayWeek()) {
		case 1:
			postion.setMonday(add);
			break;
		case 2:
			postion.setTuesday(add);
			break;
		case 3:
			postion.setWednesday(add);
			break;
		case 4:
			postion.setThursday(add);
			break;
		case 5:
			postion.setFriday(add);
			break;
		default:
			break;
		}
	}

	public AttendancePostion getChildPostion(String accountId, Date changeDate) {
		Date now = new Date();
		// 获取这段时间内正常上课天数
		UserInfo userInfo = userInfoMapper.getUserInfoByAccountId(accountId);
		ChildAttendanceInfo attendanceInfo = childAttendanceInfoMapper.selectByAccountId(accountId);
		// 获取这段时间改变的changeAttendance
		List<ChangeAttendanceRequestInfo> attendanceRequestInfos = changeAttendanceRequestInfoMapper.getListByChildAndDateScope(accountId, now, changeDate);
		AttendancePostion postion = new AttendancePostion(userInfo.getCentersId(), userInfo.getRoomId(), userInfo.getGroupId());
		postion.setMonday(attendanceInfo.getMonday());
		postion.setTuesday(attendanceInfo.getTuesday());
		postion.setWednesday(attendanceInfo.getWednesday());
		postion.setThursday(attendanceInfo.getThursday());
		postion.setFriday(attendanceInfo.getFriday());
		if (ListUtil.isNotEmpty(attendanceRequestInfos)) {
			for (ChangeAttendanceRequestInfo re : attendanceRequestInfos) {
				AttendanceRequestType type = AttendanceRequestType.getType(re.getType());
				if (type == AttendanceRequestType.K) {
					continue;
				}
				if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(re.getChangeDate(), changeDate) < 0) {
					break;
				}
				postion.setCentreId(re.getNewCentreId());
				postion.setRoomId(re.getNewRoomId());
				postion.setGroupId(re.getNewGroupId());
			}
		}

		List<RoomChangeInfo> roomChangeInfos = roomChangeInfoMapper.getListByChild(accountId, now, changeDate);
		if (ListUtil.isNotEmpty(roomChangeInfos)) {
			for (RoomChangeInfo re : roomChangeInfos) {
				RoomChangeOperate operate = RoomChangeOperate.getType(re.getOperate());
				switch (operate) {
				case REMOVEALL: {
					dealAddOrRemove(postion, re, false);
				}
					break;
				case REMOVEDAY: {
					if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(re.getDay(), changeDate) == 0) {
						dealAddOrRemove(postion, re, false);
					}
				}
					break;
				case ADDALL: {
					dealAddOrRemove(postion, re, true);
				}
					break;
				case ADDDAY: {
					if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(re.getDay(), changeDate) == 0) {
						dealAddOrRemove(postion, re, true);
					}
				}
					break;
				}
			}
		}
		return postion;
	}

	/**
	 * 判断未来是否满园
	 * 
	 * @author dlli5 at 2016年12月14日上午11:17:59
	 * @param centersId
	 * @param addDay
	 * @param dayOfWeek
	 * @param roomId
	 * @return
	 */
	public boolean futureRoomFull(String centersId, String roomId, Date addDay, short dayOfWeek) {
		Date dateChange = com.aoyuntek.aoyun.uitl.DateUtil.getBeginDateOfDateWeek(addDay, dayOfWeek);

		int size = roomInfoMapper.selectByPrimaryKey(roomId).getChildCapacity();
		// 找到有关于这个room 周几 的事件-now~未来所有
		List<RoomChangeInfo> changeInfos = roomChangeInfoMapper.getListByDateScope(centersId, roomId, dayOfWeek, new Date(), null);
		// 查询本周几应该上课的所有小孩数量
		List<String> roomChilds = userInfoMapper.getCurrentChildIdsOfRoom(centersId, roomId, dayOfWeek);
		if (ListUtil.isEmpty(changeInfos)) {
			return (roomChilds.size() + 1) > size;
		}

		// 模拟推进
		int tempRoomSize = roomChilds.size();
		for (RoomChangeInfo change : changeInfos) {
			// 如果变化的时间在插入时间之前了 说明
			if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(dateChange, change.getDay()) > 0) {
				if ((tempRoomSize + 1) > size) {
					return true;
				}
			}
			RoomChangeOperate operate = RoomChangeOperate.getType(change.getOperate());
			switch (operate) {
			case REMOVEALL: {
				if (roomChilds.contains(change.getChildId())) {
					roomChilds.remove(change.getChildId());
				}
			}
				break;
			case ADDALL: {
				if (!roomChilds.contains(change.getChildId())) {
					roomChilds.add(change.getChildId());
				}
			}
				break;
			}
			tempRoomSize = roomChilds.size();
			switch (operate) {
			case REMOVEDAY: {
				if (roomChilds.contains(change.getChildId())) {
					tempRoomSize = tempRoomSize - 1;
				}
			}
				break;
			case ADDDAY: {
				if (!roomChilds.contains(change.getChildId())) {
					tempRoomSize = tempRoomSize + 1;
				}
			}
				break;
			}
		}
		if ((tempRoomSize + 1) > size) {
			return true;
		}
		return false;
	}

	/**
	 * 获取请假的小孩
	 * 
	 * @author dlli5 at 2016年12月17日下午7:01:59
	 * @param signinInfos
	 * @return
	 */
	public List<LeaveListVo> getLeaveChilds(List<SigninVo> signinInfos) {
		List<LeaveListVo> leaves = new ArrayList<LeaveListVo>();
		for (SigninVo signinVo : signinInfos) {
			if (signinVo.isLeave()) {
				leaves.add(new LeaveListVo(signinVo.getName(), signinVo.getImg(), signinVo.getPersonColor()));
			}
		}
		return leaves;
	}

	/**
	 * 
	 * @author dlli5 at 2016年12月21日下午1:37:25
	 * @param roomChildSize
	 * @param attendanceChilds
	 */
	public void fillEmpty(int roomChildSize, List<AttendanceChildVo> attendanceChilds) {
		// 补充大小
		if (roomChildSize != 0 && attendanceChilds.size() < roomChildSize) {
			int emptySize = roomChildSize - attendanceChilds.size();
			for (int j = 0; j < emptySize; j++) {
				AttendanceChildVo temp = new AttendanceChildVo();
				temp.setChildId(null);
				attendanceChilds.add(0, temp);
			}
		}
	}

	/**
	 * 判断当前天是否需要上课
	 * 
	 * @author dlli5 at 2016年12月22日下午3:24:49
	 * @param attendancePostion
	 * @param day
	 * @return
	 */
	public boolean haveAttendanceOfDay(AttendancePostion attendancePostion, Date day) {
		int dayOfWeek = DateUtil.getDayOfWeek(day) - 1;
		switch (dayOfWeek) {
		case 1: {
			return BooleanUtils.isTrue(attendancePostion.getMonday());
		}
		case 2: {
			return BooleanUtils.isTrue(attendancePostion.getTuesday());
		}
		case 3: {
			return BooleanUtils.isTrue(attendancePostion.getWednesday());
		}
		case 4: {
			return BooleanUtils.isTrue(attendancePostion.getThursday());
		}
		case 5: {
			return BooleanUtils.isTrue(attendancePostion.getFriday());
		}
		}
		return false;
	}

	public void cancelChangCentreSendEmail(ChangeAttendanceRequestInfo ar, UserInfoVo currentUser) {
		UserInfo child = userInfoMapper.getUserInfoByAccountId(ar.getChildId());
		List<UserInfo> parents = userInfoMapper.getParentInfoByFamilyId(child.getFamilyId());
		CentersInfo oldCentre = centersInfoMapper.selectByPrimaryKey(child.getCentersId());
		String childFullName = userFactory.getUserName(child);
		String[] ceoEmails = userInfoMapper.getCeoEmails();
		// 发送Request邮件
		List<UserInfo> ceos = userInfoMapper.getAllCeos();
		List<UserInfo> newCentreManger = userInfoMapper.getCenterMangersByCenterId(ar.getNewCentreId());
		List<UserInfo> oldCentreManger = userInfoMapper.getCenterMangersByCenterId(child.getCentersId());
		// CEO取消申请给两方园长都发送邮件
		if (isRole(currentUser, Role.CEO.getValue())) {
			ceos.addAll(oldCentreManger);
			ceos.addAll(newCentreManger);
		}
		boolean isCenterManger = isCenterManger(currentUser);
		// 申请园长取消申请给被申请园长和CEO发送邮件
		if (isCenterManger && currentUser.getUserInfo().getCentersId().equals(child.getCentersId())) {
			ceos.addAll(newCentreManger);
		}
		// 被申请园长取消申请给申请园长和CEO发送邮件
		if (isCenterManger && currentUser.getUserInfo().getCentersId().equals(ar.getNewCentreId())) {
			ceos.addAll(oldCentreManger);
		}
		// 申请家长取消申请
		if (isRole(currentUser, Role.Parent.getValue())) {
			ceos.addAll(newCentreManger);
		}
		// 给园长发送邮件
		List<EmailVo> emails = new ArrayList<EmailVo>();
		for (UserInfo centerManger : ceos) {
			String oldName = oldCentre.getName();
			String sub = getSystemEmailMsg("cancel_changCentre_sub");
			String title = getSystemEmailMsg("cancel_changCentre_title");
			String content = MessageFormat.format(getSystemEmailMsg("cancel_changCentre_content"), userFactory.getUserName(centerManger), childFullName, oldName);
			String html = EmailUtil.replaceHtml(title, content);
			emails.add(new EmailVo(centerManger.getEmail(), userFactory.getUserName(centerManger), sub, html, true));
		}
		emailService.batchSendEamil(emails);
		List<EmailVo> emails2 = new ArrayList<EmailVo>();
		// 给家长发送邮件
		for (UserInfo parent : parents) {
			String oldName = oldCentre.getName();
			String sub = getSystemEmailMsg("cancel_changCentre_sub");
			String title = getSystemEmailMsg("cancel_changCentre_title");
			String content = MessageFormat.format(getSystemEmailMsg("cancel_changCentre_content"), userFactory.getUserName(parent), childFullName, oldName);
			String html = EmailUtil.replaceHtml(title, content);
			emails2.add(new EmailVo(parent.getEmail(), userFactory.getUserName(parent), sub, html, true));
		}
		boolean isAdmin = isRole(currentUser, Role.CEO.getValue()) || isRole(currentUser, Role.CentreManager.getValue())
				|| isRole(currentUser, Role.EducatorSecondInCharge.getValue());
		if (!(isAdmin && userFactory.havePlanEmail(ar.getId()))) {
			emailService.batchSendEamil(emails2, ceoEmails);
		}
	}

	private boolean isCenterManger(UserInfoVo currentUser) {
		if (isRole(currentUser, Role.CentreManager.getValue()) || isRole(currentUser, Role.EducatorSecondInCharge.getValue())) {
			return true;
		}
		return false;
	}

	/**
	 * @description 判断用户角色
	 * @author hxzhang
	 * @create 2016年10月20日下午3:01:39
	 */
	private boolean isRole(UserInfoVo currentUser, Short role) {
		List<RoleInfoVo> roleList = currentUser.getRoleInfoVoList();
		for (RoleInfoVo roleInfoVo : roleList) {
			if (role.compareTo(roleInfoVo.getValue()) == 0) {
				return true;
			}
		}
		return false;
	}

}
