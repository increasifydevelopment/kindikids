package com.aoyuntek.aoyun.factory;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aoyuntek.aoyun.condtion.RosterCondition;
import com.aoyuntek.aoyun.condtion.UserCondition;
import com.aoyuntek.aoyun.constants.DateFormatterConstants;
import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.RoomInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.dao.program.ProgramIntobsleaInfoMapper;
import com.aoyuntek.aoyun.dao.program.TaskProgramFollowUpInfoMapper;
import com.aoyuntek.aoyun.dao.roster.RosterInfoMapper;
import com.aoyuntek.aoyun.dao.roster.RosterShiftTimeInfoMapper;
import com.aoyuntek.aoyun.dao.roster.RosterStaffInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskFollowUpInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskFrequencyInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskInstanceInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskResponsibleLogInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskVisibleInfoMapper;
import com.aoyuntek.aoyun.entity.po.CentersInfo;
import com.aoyuntek.aoyun.entity.po.RoomInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramIntobsleaInfo;
import com.aoyuntek.aoyun.entity.po.program.TaskProgramFollowUpInfo;
import com.aoyuntek.aoyun.entity.po.roster.RosterInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskFollowUpInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskFrequencyInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskInstanceInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskVisibleInfo;
import com.aoyuntek.aoyun.entity.vo.RoleInfoVo;
import com.aoyuntek.aoyun.entity.vo.SelecterGroupVo;
import com.aoyuntek.aoyun.entity.vo.UserAvatarVo;
import com.aoyuntek.aoyun.entity.vo.base.UserVo;
import com.aoyuntek.aoyun.entity.vo.roster.RosterStaffVo;
import com.aoyuntek.aoyun.entity.vo.task.PersonSelectVo;
import com.aoyuntek.aoyun.entity.vo.task.TaskListVo;
import com.aoyuntek.aoyun.enums.ArchivedStatus;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.enums.TaskCategory;
import com.aoyuntek.aoyun.enums.TaskFrequenceRepeatType;
import com.aoyuntek.aoyun.enums.TaskType;
import com.aoyuntek.aoyun.enums.TaskVisibleType;
import com.aoyuntek.aoyun.enums.UserType;
import com.aoyuntek.aoyun.enums.roster.RosterStatus;
import com.aoyuntek.aoyun.service.meeting.IMeetingService;
import com.theone.date.util.DateUtil;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

@Component
public class TaskFactory {

	@Autowired
	private RoomInfoMapper roomInfoMapper;
	@Autowired
	private UserInfoMapper userInfoMapper;
	@Autowired
	private CentersInfoMapper centersInfoMapper;
	@Autowired
	private ProgramIntobsleaInfoMapper programIntobsleaInfoMapper;
	@Autowired
	private TaskProgramFollowUpInfoMapper taskProgramFollowUpInfoMapper;
	@Autowired
	private TaskVisibleInfoMapper taskVisibleInfoMapper;
	@Autowired
	private TaskInfoMapper taskInfoMapper;
	@Autowired
	private TaskResponsibleLogInfoMapper taskResponsibleLogInfoMapper;
	@Autowired
	private UserFactory userFactory;
	@Autowired
	private TaskFrequencyInfoMapper taskFrequencyInfoMapper;
	@Autowired
	private TaskFollowUpInfoMapper taskFollowUpInfoMapper;
	@Autowired
	private IMeetingService meetingService;
	@Autowired
	private RosterStaffInfoMapper rosterStaffInfoMapper;
	@Autowired
	private RosterShiftTimeInfoMapper rosterShiftTimeInfoMapper;
	@Autowired
	private RosterInfoMapper rosterInfoMapper;
	@Autowired
	private TaskInstanceInfoMapper taskInstanceInfoMapper;

	public Date[] dealDate(TaskType taskType, String taskModelId, Date day) {
		TaskInstanceInfo model = new TaskInstanceInfo();
		if (taskType != TaskType.CustomTask) {
			model.setBeginDate(day);
			model.setEndDate(day);
		} else {
			TaskFrequencyInfo frequencyInfo = taskFrequencyInfoMapper.selectByTaskId(taskModelId);
			TaskFrequenceRepeatType frequenceRepeatType = null;
			if (frequencyInfo == null) {
				frequenceRepeatType = TaskFrequenceRepeatType.Daily;
			} else {
				frequenceRepeatType = TaskFrequenceRepeatType.getType(frequencyInfo.getRepeatType());
			}
			switch (frequenceRepeatType) {
			case Daily: {
				model.setBeginDate(day);
				model.setEndDate(day);
			}
				break;
			case Weekly: {
				Date weekStart = DateUtil.getWeekStart(day);
				model.setBeginDate(DateUtil.addDay(weekStart, Integer.valueOf(frequencyInfo.getDayToStart())));
				model.setEndDate(DateUtil.addDay(weekStart, Integer.valueOf(frequencyInfo.getDayToEnd())));
			}
				break;
			case Monthly: {
				Date weekStart = DateUtil.getFirstDayOfMonth(day);
				model.setBeginDate(DateUtil.addDay(weekStart, Integer.valueOf(frequencyInfo.getDayToStart()) - 1));
				Date endDate = DateUtil.addDay(weekStart, Integer.valueOf(frequencyInfo.getDayToEnd()) - 1);
				if (DateUtil.getMonth(endDate) != DateUtil.getMonth(day)) {
					model.setEndDate(DateUtil.getLastDayOfMonth(day));
				} else {
					model.setEndDate(endDate);
				}

			}
				break;
			case Quarterly: {
				model.setBeginDate(com.aoyuntek.aoyun.uitl.DateUtil.getFristDayOfQuarter(day));
				model.setEndDate(com.aoyuntek.aoyun.uitl.DateUtil.getEndDayOfQuarter(day));
			}
				break;
			case Yearly: {
				int year = DateUtil.getCalendar(day).get(Calendar.YEAR);
				model.setBeginDate(DateUtil.parse(frequencyInfo.getDayToStart() + "/" + year, DateFormatterConstants.SYS_FORMAT2));
				model.setEndDate(DateUtil.parse(frequencyInfo.getDayToEnd() + "/" + year, DateFormatterConstants.SYS_FORMAT2));
			}
				break;
			case OnceOff: {
				model.setBeginDate(DateUtil.parse(frequencyInfo.getDayToStart(), DateFormatterConstants.SYS_FORMAT2));
				model.setEndDate(DateUtil.parse(frequencyInfo.getDayToEnd(), DateFormatterConstants.SYS_FORMAT2));
			}
				break;
			case WhenRequired:
				model.setBeginDate(day);
				model.setEndDate(null);
				break;
			default:
				break;
			}
		}
		return new Date[] { model.getBeginDate(), model.getEndDate() };
	}

	public Boolean isSelected(String roomId, short type, List<TaskVisibleInfo> taskVisibleInfos) {
		if (ListUtil.isEmpty(taskVisibleInfos)) {
			return false;
		}
		for (TaskVisibleInfo taskVisibleInfo : taskVisibleInfos) {
			if (roomId.split(",")[0].equals(taskVisibleInfo.getObjId()) && (taskVisibleInfo.getType() == type)) {
				return true;
			}
		}
		return false;
	}

	public List filterGroupSelect(List<SelecterGroupVo> selecterPos, String findText) {
		List<SelecterGroupVo> result = new ArrayList<SelecterGroupVo>();
		for (SelecterGroupVo selecterGroupVo : selecterPos) {
			List<PersonSelectVo> items = selecterGroupVo.getChildren();
			List<PersonSelectVo> resultItems = new ArrayList<PersonSelectVo>();
			for (PersonSelectVo selectVo : items) {
				if (StringUtil.isNotEmpty(findText) && !selectVo.getText().toLowerCase().contains(findText.toLowerCase())) {
					continue;
				}
				resultItems.add(selectVo);
			}
			if (ListUtil.isNotEmpty(resultItems)) {
				selecterGroupVo.setChildren(resultItems);
				result.add(selecterGroupVo);
			}
		}
		return result;
	}

	public List filterSelect(List<PersonSelectVo> selecterPos, String findText) {
		List<PersonSelectVo> result = new ArrayList<PersonSelectVo>();
		for (PersonSelectVo personSelectVo : selecterPos) {
			if (StringUtil.isNotEmpty(findText) && !personSelectVo.getText().toLowerCase().contains(findText.toLowerCase())) {
				continue;
			}
			result.add(personSelectVo);
		}
		return result;
	}

	public List<PersonSelectVo> convertTaskVisible(List<TaskVisibleInfo> taskVisibleInfos) {
		if (ListUtil.isEmpty(taskVisibleInfos)) {
			return new ArrayList<PersonSelectVo>();
		}
		List<PersonSelectVo> result = new ArrayList<PersonSelectVo>();
		for (TaskVisibleInfo taskVisibleInfo : taskVisibleInfos) {
			String objId = taskVisibleInfo.getObjId().contains(",") ? taskVisibleInfo.getObjId().split(",")[0] : taskVisibleInfo.getObjId();
			PersonSelectVo item = new PersonSelectVo(taskVisibleInfo.getType(), objId, "");
			item.setId(objId);
			result.add(item);
		}
		return result;
	}

	public Object filterSelected(List returnObj) {
		if (ListUtil.isEmpty(returnObj)) {
			return new ArrayList<PersonSelectVo>();
		}
		if (returnObj.get(0) instanceof PersonSelectVo) {
			List<PersonSelectVo> result = new ArrayList<PersonSelectVo>();
			for (Object obj : returnObj) {
				PersonSelectVo select = (PersonSelectVo) obj;
				if (select.getSelected()) {
					result.add(select);
				}
			}
			return result;
		} else {
			List<SelecterGroupVo> result = new ArrayList<SelecterGroupVo>();
			for (Object groupObj : returnObj) {
				SelecterGroupVo selecterGroupVo = (SelecterGroupVo) groupObj;
				List<PersonSelectVo> items = selecterGroupVo.getChildren();
				List<PersonSelectVo> resultItems = new ArrayList<PersonSelectVo>();
				for (PersonSelectVo selectVo : items) {
					if (!selectVo.getSelected()) {
						continue;
					}
					resultItems.add(selectVo);
				}
				if (ListUtil.isNotEmpty(resultItems)) {
					selecterGroupVo.setChildren(resultItems);
					result.add(selecterGroupVo);
				}
			}
			return result;
		}
	}

	public boolean containRoles(List<RoleInfoVo> roleList, Role... roles) {
		boolean result = false;
		for (RoleInfoVo allRole : roleList) {
			for (Role role : roles) {
				if (allRole.getValue() == role.getValue()) {
					result = true;
				}
			}
		}
		return result;
	}

	public boolean containRoles(List<RoleInfoVo> list1, List<RoleInfoVo> list2, Role... roles) {
		boolean result = false;
		for (RoleInfoVo allRole : list1) {
			for (Role role : roles) {
				if (allRole.getValue() == role.getValue()) {
					result = true;
				}
			}
		}
		for (RoleInfoVo allRole : list2) {
			for (Role role : roles) {
				if (allRole.getValue() == role.getValue()) {
					result = true;
				}
			}
		}
		return result;
	}

	/**
	 * 获取可填写人
	 * 
	 * @author hxzhang at 2018年4月26日上午9:20:20
	 * @param taskInfo
	 * @param taskInstanceInfo
	 * @return
	 */
	public List<String> getImplementersPeople(TaskInfo taskInfo, TaskListVo taskInstanceInfo) {
		List<String> userIds = new ArrayList<String>();
		List<TaskVisibleInfo> implementersPeople = new ArrayList<TaskVisibleInfo>();
		implementersPeople = taskVisibleInfoMapper.selectBySourceId(taskInfo.getTaskImplementersId());
		TaskCategory taskCategory = TaskCategory.getType(taskInfo.getTaskCategory());
		switch (taskCategory) {
		case CENTER: {
			// Specific staff member（单独员工）
			for (TaskVisibleInfo items : implementersPeople) {
				// All Staff In The Centre
				if (items.getType() == TaskVisibleType.CENTER.getValue()) {
					List<String> centreIds = new ArrayList<String>();
					centreIds.add(taskInstanceInfo.getCentreId());

					UserCondition userCondition = new UserCondition();
					userCondition.setCentreIds(centreIds);
					dealUserVoListToUserIds(userCondition, userIds);

				}
				// Centre Managers
				else if (items.getType() == TaskVisibleType.ROLE_GROUP.getValue()) {
					List<String> centreIds = new ArrayList<String>();
					List<Short> roleTypes = new ArrayList<Short>();
					roleTypes.add(Role.CentreManager.getValue());
					roleTypes.add(Role.EducatorSecondInCharge.getValue());
					centreIds.add(taskInstanceInfo.getCentreId());

					UserCondition userCondition = new UserCondition();
					userCondition.setCentreIds(centreIds);
					userCondition.setRoleTypes(roleTypes);
					dealUserVoListToUserIds(userCondition, userIds);
				}
				// Educators,Cooks,Casuals
				else if (items.getType() == TaskVisibleType.ROLE.getValue()) {
					List<String> centreIds = new ArrayList<String>();
					List<Short> roleTypes = new ArrayList<Short>();
					roleTypes.add(Short.valueOf(items.getObjId()));
					centreIds.add(taskInstanceInfo.getCentreId());

					UserCondition userCondition = new UserCondition();
					userCondition.setCentreIds(centreIds);
					userCondition.setRoleTypes(roleTypes);
					dealUserVoListToUserIds(userCondition, userIds);
				}
				// 单独员工
				else if (items.getType() == TaskVisibleType.USER.getValue()) {
					if (!userIds.contains(items.getObjId())) {
						userIds.add(items.getObjId());
					}
				}
			}
		}
			break;
		case ROOM: {
			for (TaskVisibleInfo items : implementersPeople) {
				// All Staff In The Centre
				if (items.getType() == TaskVisibleType.CENTER.getValue()) {
					List<String> centreIds = new ArrayList<String>();
					centreIds.add(taskInstanceInfo.getCentreId());

					UserCondition userCondition = new UserCondition();
					userCondition.setCentreIds(centreIds);
					dealUserVoListToUserIds(userCondition, userIds);
				}
				// 单独员工
				else if (items.getType() == TaskVisibleType.USER.getValue()) {
					if (!userIds.contains(items.getObjId())) {
						userIds.add(items.getObjId());
					}
				}
			}
		}
			break;
		case CHILDRRENN: {
			for (TaskVisibleInfo items : implementersPeople) {
				// All Staff In The Centre
				if (items.getType() == TaskVisibleType.CENTER.getValue()) {
					List<String> centreIds = new ArrayList<String>();
					centreIds.add(taskInstanceInfo.getCentreId());

					UserCondition userCondition = new UserCondition();
					userCondition.setCentreIds(centreIds);
					dealUserVoListToUserIds(userCondition, userIds);
				}
				// 单独员工
				else if (items.getType() == TaskVisibleType.USER.getValue()) {
					if (!userIds.contains(items.getObjId())) {
						userIds.add(items.getObjId());
					}
				}
			}
		}
			break;
		case STAFF: {
			for (TaskVisibleInfo items : implementersPeople) {
				// All Staff In The Centre
				if (items.getType() == TaskVisibleType.CENTER.getValue()) {
					List<String> centreIds = new ArrayList<String>();
					centreIds.add(taskInstanceInfo.getCentreId());

					UserCondition userCondition = new UserCondition();
					userCondition.setCentreIds(centreIds);
					dealUserVoListToUserIds(userCondition, userIds);
				}
				// Centre Managerss（默认）
				if (items.getType() == TaskVisibleType.ROLE_GROUP.getValue()) {
					List<Short> roleTypes = new ArrayList<Short>();
					List<String> centreIds = new ArrayList<String>();
					roleTypes.add(Role.CentreManager.getValue());
					roleTypes.add(Role.EducatorSecondInCharge.getValue());
					centreIds.add(taskInstanceInfo.getCentreId());

					UserCondition userCondition = new UserCondition();
					userCondition.setCentreIds(centreIds);
					userCondition.setRoleTypes(roleTypes);
					dealUserVoListToUserIds(userCondition, userIds);
				}

				// 单独员工
				else if (items.getType() == TaskVisibleType.USER.getValue()) {
					if (!userIds.contains(items.getObjId())) {
						userIds.add(items.getObjId());
					}
				}
			}
		}
			break;
		}
		return userIds;
	}

	/**
	 * 处理员工
	 * 
	 * @author hxzhang at 2018年4月26日上午11:10:59
	 * @param userCondition
	 * @param userIds
	 */
	private void dealUserVoListToUserIds(UserCondition userCondition, List<String> userIds) {
		userCondition.setPageSize(Integer.MAX_VALUE);
		userCondition.setPageIndex(0);
		userCondition.setUserType(UserType.Staff.getValue());
		userCondition.setStatus(ArchivedStatus.UnArchived.getValue());
		userCondition.setSortName("v.value");
		userCondition.setSortOrder("desc");
		List<UserVo> list = userInfoMapper.getListByCondtion(userCondition);
		for (UserVo userVo : list) {
			if (!userIds.contains(userVo.getAccountId())) {
				userIds.add(userVo.getAccountId());
			}
		}
	}

	// TODO big 这个想下简化
	public List<String> getResponsiblePeople(TaskInfo taskInfo, TaskListVo taskInstanceInfo) {
		List<String> userIds = new ArrayList<String>();
		List<TaskVisibleInfo> responsiblePeople = new ArrayList<TaskVisibleInfo>();
		String responsiblePeopleId = taskInfo.getResponsiblePeopleId();
		if (taskInstanceInfo.getDataType().intValue() == 2) {
			TaskFollowUpInfo followUpInfo = taskFollowUpInfoMapper.selectByPrimaryKey(taskInstanceInfo.getTaskModelId());
			responsiblePeopleId = followUpInfo.getResponsiblePersonId();
		}
		responsiblePeople = taskVisibleInfoMapper.selectBySourceId(responsiblePeopleId);
		TaskCategory taskCategory = TaskCategory.getType(taskInfo.getTaskCategory());
		switch (taskCategory) {
		case CENTER: {
			// Centre Managers 默认
			// Specific staff member（单独员工）
			for (TaskVisibleInfo items : responsiblePeople) {
				// 如果是Co-ordinato 那么判断自己是否是这些角色其中的一个
				if (items.getType() == TaskVisibleType.ROLE_GROUP.getValue()) {
					List<String> centreIds = new ArrayList<String>();
					List<Short> roleTypes = new ArrayList<Short>();
					roleTypes.add(Role.CentreManager.getValue());
					roleTypes.add(Role.EducatorSecondInCharge.getValue());
					centreIds.add(taskInstanceInfo.getCentreId());

					UserCondition userCondition = new UserCondition();
					userCondition.setPageSize(Integer.MAX_VALUE);
					userCondition.setPageIndex(0);
					userCondition.setCentreIds(centreIds);
					userCondition.setUserType(UserType.Staff.getValue());
					userCondition.setRoleTypes(roleTypes);
					userCondition.setStatus(ArchivedStatus.UnArchived.getValue());
					userCondition.setSortName("v.value");
					userCondition.setSortOrder("desc");
					List<UserVo> list = userInfoMapper.getListByCondtion(userCondition);
					for (UserVo userVo : list) {
						if (!userIds.contains(userVo.getAccountId())) {
							userIds.add(userVo.getAccountId());
						}
					}
				}
				// 单独员工
				else {
					if (!userIds.contains(items.getObjId())) {
						userIds.add(items.getObjId());
					}
				}
			}
		}
			break;
		case ROOM: {
			for (TaskVisibleInfo items : responsiblePeople) {
				// All Educators in the room
				if (items.getType() == TaskVisibleType.ROLE.getValue()) {
					List<String> centreIds = new ArrayList<String>();
					List<String> roomIds = new ArrayList<String>();
					List<Short> roleTypes = new ArrayList<Short>();
					roleTypes.add(Role.Educator.getValue());
					roomIds.add(taskInstanceInfo.getRoomId());

					UserCondition userCondition = new UserCondition();
					userCondition.setPageSize(Integer.MAX_VALUE);
					userCondition.setPageIndex(0);
					userCondition.setCentreIds(centreIds);
					userCondition.setRoomIds(roomIds);
					userCondition.setUserType(UserType.Staff.getValue());
					userCondition.setRoleTypes(roleTypes);
					userCondition.setStatus(ArchivedStatus.UnArchived.getValue());
					userCondition.setSortName("v.value");
					userCondition.setSortOrder("desc");
					List<UserVo> list = userInfoMapper.getListByCondtion(userCondition);
					for (UserVo userVo : list) {
						if (!userIds.contains(userVo.getAccountId())) {
							userIds.add(userVo.getAccountId());
						}
					}
				}
				// 单独员工
				else if (items.getType() == TaskVisibleType.USER.getValue()) {
					if (!userIds.contains(items.getObjId())) {
						userIds.add(items.getObjId());
					}
				}
			}
		}
			break;
		case CHILDRRENN: {
			for (TaskVisibleInfo items : responsiblePeople) {
				// All Educators in the room
				if (items.getType() == TaskVisibleType.ROLE.getValue()) {
					List<String> centreIds = new ArrayList<String>();
					List<String> roomIds = new ArrayList<String>();
					List<Short> roleTypes = new ArrayList<Short>();
					roleTypes.add(Role.Educator.getValue());
					roomIds.add(taskInstanceInfo.getRoomId());
					UserCondition userCondition = new UserCondition();
					userCondition.setPageSize(Integer.MAX_VALUE);
					userCondition.setPageIndex(0);
					userCondition.setCentreIds(centreIds);
					userCondition.setRoomIds(roomIds);
					userCondition.setUserType(UserType.Staff.getValue());
					userCondition.setRoleTypes(roleTypes);
					userCondition.setStatus(ArchivedStatus.UnArchived.getValue());
					userCondition.setSortName("v.value");
					userCondition.setSortOrder("desc");
					List<UserVo> list = userInfoMapper.getListByCondtion(userCondition);
					for (UserVo userVo : list) {
						if (!userIds.contains(userVo.getAccountId())) {
							userIds.add(userVo.getAccountId());
						}
					}
				}
				// 单独员工
				else if (items.getType() == TaskVisibleType.USER.getValue()) {
					if (!userIds.contains(items.getObjId())) {
						userIds.add(items.getObjId());
					}
				}
			}
		}
			break;
		case STAFF: {
			for (TaskVisibleInfo items : responsiblePeople) {
				// Directors
				if (items.getType() == TaskVisibleType.ROLE.getValue()) {
					List<Short> roleTypes = new ArrayList<Short>();
					List<String> centreIds = new ArrayList<String>();
					roleTypes.add(Role.CEO.getValue());
					UserCondition userCondition = new UserCondition();
					userCondition.setPageSize(Integer.MAX_VALUE);
					userCondition.setPageIndex(0);
					userCondition.setCentreIds(centreIds);
					userCondition.setUserType(UserType.Staff.getValue());
					userCondition.setRoleTypes(roleTypes);
					userCondition.setStatus(ArchivedStatus.UnArchived.getValue());
					userCondition.setSortName("v.value");
					userCondition.setSortOrder("desc");
					List<UserVo> list = userInfoMapper.getListByCondtion(userCondition);
					for (UserVo userVo : list) {
						if (!userIds.contains(userVo.getAccountId())) {
							userIds.add(userVo.getAccountId());
						}
					}
				}
				// Centre Managerss（默认）
				if (items.getType() == TaskVisibleType.ROLE_GROUP.getValue()) {
					List<Short> roleTypes = new ArrayList<Short>();
					List<String> centreIds = new ArrayList<String>();
					roleTypes.add(Role.CentreManager.getValue());
					roleTypes.add(Role.EducatorSecondInCharge.getValue());
					centreIds.add(taskInstanceInfo.getCentreId());
					UserCondition userCondition = new UserCondition();
					userCondition.setPageSize(Integer.MAX_VALUE);
					userCondition.setPageIndex(0);
					userCondition.setCentreIds(centreIds);
					userCondition.setUserType(UserType.Staff.getValue());
					userCondition.setRoleTypes(roleTypes);
					userCondition.setStatus(ArchivedStatus.UnArchived.getValue());
					userCondition.setSortName("v.value");
					userCondition.setSortOrder("desc");
					List<UserVo> list = userInfoMapper.getListByCondtion(userCondition);
					for (UserVo userVo : list) {
						if (!userIds.contains(userVo.getAccountId())) {
							userIds.add(userVo.getAccountId());
						}
					}
				}

				// 单独员工
				else if (items.getType() == TaskVisibleType.USER.getValue()) {
					if (!userIds.contains(items.getObjId())) {
						userIds.add(items.getObjId());
					}
				}

			}
		}
			break;
		case Individual: {
			for (TaskVisibleInfo items : responsiblePeople) {
				// Directors
				if (items.getType() == TaskVisibleType.ROLE.getValue()) {
					List<Short> roleTypes = new ArrayList<Short>();
					List<String> centreIds = new ArrayList<String>();
					roleTypes.add(Role.CEO.getValue());
					UserCondition userCondition = new UserCondition();
					userCondition.setPageSize(Integer.MAX_VALUE);
					userCondition.setPageIndex(0);
					userCondition.setCentreIds(centreIds);
					userCondition.setUserType(UserType.Staff.getValue());
					userCondition.setRoleTypes(roleTypes);
					userCondition.setStatus(ArchivedStatus.UnArchived.getValue());
					userCondition.setSortName("v.value");
					userCondition.setSortOrder("desc");
					List<UserVo> list = userInfoMapper.getListByCondtion(userCondition);
					for (UserVo userVo : list) {
						if (!userIds.contains(userVo.getAccountId())) {
							userIds.add(userVo.getAccountId());
						}
					}
				}
				// Centre Managerss（默认）
				if (items.getType() == TaskVisibleType.ROLE_GROUP.getValue()) {
					List<Short> roleTypes = new ArrayList<Short>();
					List<String> centreIds = new ArrayList<String>();
					roleTypes.add(Role.CentreManager.getValue());
					roleTypes.add(Role.EducatorSecondInCharge.getValue());
					centreIds.add(taskInstanceInfo.getCentreId());
					UserCondition userCondition = new UserCondition();
					userCondition.setPageSize(Integer.MAX_VALUE);
					userCondition.setPageIndex(0);
					userCondition.setCentreIds(centreIds);
					userCondition.setUserType(UserType.Staff.getValue());
					userCondition.setRoleTypes(roleTypes);
					userCondition.setStatus(ArchivedStatus.UnArchived.getValue());
					userCondition.setSortName("v.value");
					userCondition.setSortOrder("desc");
					List<UserVo> list = userInfoMapper.getListByCondtion(userCondition);
					for (UserVo userVo : list) {
						if (!userIds.contains(userVo.getAccountId())) {
							userIds.add(userVo.getAccountId());
						}
					}
				}

				// 单独员工
				else if (items.getType() == TaskVisibleType.USER.getValue()) {
					if (!userIds.contains(items.getObjId())) {
						userIds.add(items.getObjId());
					}
				}

			}
		}
			break;
		}
		return userIds;
	}

	public List<UserVo> getImplementersPeople(TaskListVo taskListVo) {
		if (taskListVo.getTaskType() != TaskType.CustomTask.getValue()) {
			return new ArrayList<UserVo>();
		}
		String taskModelId = taskListVo.getTaskModelId();
		// FollowUp
		if (taskListVo.getDataType().intValue() == 2) {
			TaskFollowUpInfo followUpInfo = taskFollowUpInfoMapper.selectByPrimaryKey(taskModelId);
			taskModelId = followUpInfo.getTaskId();
		}
		// 自定义
		TaskInfo taskInfo = taskInfoMapper.selectByPrimaryKey(taskModelId);
		if (taskInfo == null) {
			return new ArrayList<UserVo>();
		}
		// shift任务
		if (taskInfo.getShiftFlag()) {
			return new ArrayList<UserVo>();
		}

		List<String> userList = getImplementersPeople(taskInfo, taskListVo);
		UserCondition userCondition = new UserCondition();
		userCondition.setPageSize(Integer.MAX_VALUE);
		userCondition.setPageIndex(0);
		userCondition.setUserType(UserType.Staff.getValue());
		userCondition.setStatus(ArchivedStatus.UnArchived.getValue());
		userCondition.setAccountIds(userList);
		List<UserVo> list = new ArrayList<UserVo>();
		if (ListUtil.isNotEmpty(userList)) {
			list = userInfoMapper.getListByCondtion(userCondition);
		}
		return list;
	}

	public List<UserVo> getResponsiblePeople(TaskListVo taskListVo, Date day) {
		TaskType taskType = TaskType.getType(taskListVo.getTaskType());
		switch (taskType) {
		case Interest:
		case Observation:
		case LearningStory: {
			ProgramIntobsleaInfo model = programIntobsleaInfoMapper.selectByPrimaryKey(taskListVo.getValueId());
			if (model == null) {
				break;
			}
			// group 老师
			UserCondition userCondition = new UserCondition();
			userCondition.setPageSize(Integer.MAX_VALUE);
			userCondition.setPageIndex(0);
			userCondition.setGroupId(StringUtil.isEmpty(taskListVo.getGroupId()) ? UUID.randomUUID().toString() : taskListVo.getGroupId());
			userCondition.setUserType(UserType.Staff.getValue());
			userCondition.setRoleType(Role.Educator.getValue());
			userCondition.setStatus(ArchivedStatus.UnArchived.getValue());
			List<UserVo> list = userInfoMapper.getListByCondtion(userCondition);
			return list;
		}
		case InterestFollowup:
		case ObservationFollowup:
		case LearningStoryFollowup: {
			TaskProgramFollowUpInfo model = taskProgramFollowUpInfoMapper.selectByPrimaryKey(taskListVo.getValueId());
			if (model == null) {
				return new ArrayList<UserVo>();
			}
			ProgramIntobsleaInfo taskModel = programIntobsleaInfoMapper.selectByPrimaryKey(model.getTaskId());
			if (taskModel == null) {
				return new ArrayList<UserVo>();
			}
			// group 老师
			UserCondition userCondition = new UserCondition();
			userCondition.setPageSize(Integer.MAX_VALUE);
			userCondition.setPageIndex(0);
			userCondition.setGroupId(StringUtil.isEmpty(taskListVo.getGroupId()) ? UUID.randomUUID().toString() : taskListVo.getGroupId());
			userCondition.setUserType(UserType.Staff.getValue());
			userCondition.setRoleType(Role.Educator.getValue());
			userCondition.setStatus(ArchivedStatus.UnArchived.getValue());
			List<UserVo> list = userInfoMapper.getListByCondtion(userCondition);
			return list;
		}
		case CustomTask: {
			String taskModelId = taskListVo.getTaskModelId();
			// FollowUp
			if (taskListVo.getDataType().intValue() == 2) {
				TaskFollowUpInfo followUpInfo = taskFollowUpInfoMapper.selectByPrimaryKey(taskModelId);
				taskModelId = followUpInfo.getTaskId();
			}
			// 自定义
			TaskInfo taskInfo = taskInfoMapper.selectByPrimaryKey(taskModelId);
			if (taskInfo == null) {
				break;
			}
			// shift任务
			if (!taskInfo.getShiftFlag()) {
				List<String> userList = getResponsiblePeople(taskInfo, taskListVo);
				UserCondition userCondition = new UserCondition();
				userCondition.setPageSize(Integer.MAX_VALUE);
				userCondition.setPageIndex(0);
				userCondition.setUserType(UserType.Staff.getValue());
				userCondition.setStatus(ArchivedStatus.UnArchived.getValue());
				userCondition.setAccountIds(userList);
				List<UserVo> list = new ArrayList<UserVo>();
				if (ListUtil.isNotEmpty(userList)) {
					list = userInfoMapper.getListByCondtion(userCondition);
				}
				return list;
			}
			// 获取shift的责任人 当前园的排班人员
			String centreId = taskListVo.getCentreId();
			RosterCondition rosterCondition = new RosterCondition();
			rosterCondition.initAll();
			rosterCondition.setCenterId(centreId);
			Date weekStart = DateUtil.getWeekStart(day);
			rosterCondition.setStartDate(weekStart);
			rosterCondition.setState(RosterStatus.Approved.getValue());
			List<RosterInfo> rosterInfos = rosterInfoMapper.getList(rosterCondition);
			if (ListUtil.isEmpty(rosterInfos)) {
				return new ArrayList<UserVo>();
			}
			// 获取此task对应的shittime 集合 以便于找出安排的人员信息
			List<String> shiftTimeCodes = rosterShiftTimeInfoMapper.getShiftTimeCodes(taskListVo.getTaskModelId(), rosterInfos.get(0).getShiftId());
			if (ListUtil.isEmpty(shiftTimeCodes)) {
				return new ArrayList<UserVo>();
			}

			List<RosterStaffVo> rosterStaffVos = rosterStaffInfoMapper.getShiftTimeStaff(rosterInfos.get(0).getId(), shiftTimeCodes, day);
			List<String> userList = new ArrayList<String>();
			for (RosterStaffVo rosterStaffVo : rosterStaffVos) {
				userList.add(rosterStaffVo.getStaffAccountId());
			}
			UserCondition userCondition = new UserCondition();
			userCondition.setPageSize(Integer.MAX_VALUE);
			userCondition.setPageIndex(0);
			userCondition.setUserType(UserType.Staff.getValue());
			userCondition.setStatus(ArchivedStatus.UnArchived.getValue());
			userCondition.setAccountIds(userList);
			List<UserVo> list = new ArrayList<UserVo>();
			if (ListUtil.isNotEmpty(userList)) {
				list = userInfoMapper.getListByCondtion(userCondition);
			}
			return list;
		}
		case MeetingTask: {
			List<String> accountIds = new ArrayList<String>();
			accountIds.add(meetingService.getLiabler(taskListVo.getValueId()));
			UserCondition userCondition = new UserCondition();
			userCondition.setPageSize(Integer.MAX_VALUE);
			userCondition.setPageIndex(0);
			userCondition.setUserType(UserType.Staff.getValue());
			userCondition.setAccountIds(accountIds);
			List<UserVo> list = userInfoMapper.getListByCondtion(userCondition);
			return list;
		}
		case DepositTask:
		case HatTask: {
			return userInfoMapper.getManagerByCenterId(taskListVo.getCentreId());
		}

		default: {
			// room老师
			UserCondition userCondition = new UserCondition();
			userCondition.setPageSize(Integer.MAX_VALUE);
			userCondition.setPageIndex(0);
			userCondition.setRoomId(taskListVo.getRoomId());
			userCondition.setUserType(UserType.Staff.getValue());
			userCondition.setRoleType(Role.Educator.getValue());
			userCondition.setStatus(ArchivedStatus.UnArchived.getValue());
			List<UserVo> list = userInfoMapper.getListByCondtion(userCondition);
			return list;
		}
		}
		return new ArrayList<UserVo>();
	}

	public void dealImplementersAvatar(TaskListVo taskListVo) {
		List<UserVo> list = getImplementersPeople(taskListVo);
		list = userFactory.duplicate(list);
		List<UserAvatarVo> avatars = new ArrayList<UserAvatarVo>();
		for (UserVo userVo : list) {
			avatars.add(new UserAvatarVo(userVo.getAvatar(), userVo.getPersonColor(), userVo.getName()));
		}
		taskListVo.setImplementersAvatar(avatars);
	}

	/**
	 * 加载头像
	 * 
	 * @author dlli5 at 2016年9月23日下午1:47:06
	 * @param taskListVos
	 */
	public void dealAvatar(List<TaskListVo> taskListVos) {
		Date now = new Date();
		for (TaskListVo taskListVo : taskListVos) {
			List<UserVo> list = new ArrayList<UserVo>();
			if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(taskListVo.getBeginDate(), now) > 0) {
				String taskInstanceId = taskListVo.getId();
				// 如果是task 的 followup 那么责任人取值为value
				if (taskListVo.getDataType().intValue() == 2) {
					taskInstanceId = taskListVo.getValueId();
				}
				// 获取历史记录表中的
				List<String> accountIds = taskResponsibleLogInfoMapper.getByInstanceId(taskInstanceId);
				if (ListUtil.isNotEmpty(accountIds)) {
					UserCondition userCondition = new UserCondition();
					userCondition.setPageSize(Integer.MAX_VALUE);
					userCondition.setPageIndex(0);
					userCondition.setAccountIds(accountIds);
					list = userInfoMapper.getListByCondtion(userCondition);
				}
			} else {
				list = getResponsiblePeople(taskListVo, now);
			}

			list = userFactory.duplicate(list);
			List<UserAvatarVo> avatars = new ArrayList<UserAvatarVo>();
			for (UserVo userVo : list) {
				avatars.add(new UserAvatarVo(userVo.getAvatar(), userVo.getPersonColor(), userVo.getName()));
			}
			taskListVo.setAvatar(avatars);
		}
	}

	/**
	 * 加载头像
	 * 
	 * @author dlli5 at 2016年9月23日下午1:47:06
	 * @param taskListVos
	 */
	public void dealAvatar(TaskListVo taskListVo) {
		Date now = new Date();
		List<UserVo> list = new ArrayList<UserVo>();
		boolean isIndividualTask = false;
		if ((taskListVo.getTaskType() == TaskType.CustomTask.getValue() || taskListVo.getTaskType() == TaskType.CustomTaskFollowUp.getValue())
				&& taskListVo.getObjType() == TaskCategory.Individual.getValue()) {
			isIndividualTask = true;
		}
		if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(taskListVo.getEndDate(), now) > 0 || isIndividualTask) {
			String taskInstanceId = taskListVo.getId();
			// 如果是task 的 followup 那么责任人取值为value
			if (taskListVo.getDataType().intValue() == 2) {
				taskInstanceId = taskListVo.getValueId();
			}

			// 获取历史记录表中的
			List<String> accountIds = taskResponsibleLogInfoMapper.getByInstanceId(taskInstanceId);

			if (ListUtil.isNotEmpty(accountIds)) {
				UserCondition userCondition = new UserCondition();
				userCondition.setPageSize(Integer.MAX_VALUE);
				userCondition.setPageIndex(0);
				userCondition.setAccountIds(accountIds);
				list = userInfoMapper.getListByCondtion(userCondition);
			}
		} else {
			list = getResponsiblePeople(taskListVo, now);
		}

		list = userFactory.duplicate(list);
		List<UserAvatarVo> avatars = new ArrayList<UserAvatarVo>();
		for (UserVo userVo : list) {
			avatars.add(new UserAvatarVo(userVo.getAvatar(), userVo.getPersonColor(), userVo.getName()));
		}
		taskListVo.setAvatar(avatars);
	}

	public boolean isLiabler(String valueId, String accountId) {
		boolean isLiabler = false;
		Date now = new Date();
		List<UserVo> list = new ArrayList<UserVo>();
		TaskListVo taskListVo = taskInstanceInfoMapper.getTaskListVoByValueId(valueId);
		if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(taskListVo.getEndDate(), now) > 0) {
			String taskInstanceId = taskListVo.getId();
			// 如果是task 的 followup 那么责任人取值为value
			if (taskListVo.getDataType().intValue() == 2) {
				taskInstanceId = taskListVo.getValueId();
			}
			// 获取历史记录表中的
			List<String> accountIds = taskResponsibleLogInfoMapper.getByInstanceId(taskInstanceId);
			if (ListUtil.isNotEmpty(accountIds) && accountIds.contains(accountId)) {
				isLiabler = true;
			}
		} else {
			list = getResponsiblePeople(taskListVo, now);
			for (UserVo vo : list) {
				if (accountId.equals(vo.getAccountId())) {
					isLiabler = true;
				}
			}
		}
		return isLiabler;
	}

	public boolean isSelectAllCenter(List<TaskVisibleInfo> selectObjs) {
		for (TaskVisibleInfo taskVisibleInfo : selectObjs) {
			if ((taskVisibleInfo.getType() == TaskVisibleType.CENTER.getValue()) && ("0".equals(taskVisibleInfo.getObjId()))) {
				return true;
			}
		}
		return false;
	}

	public List<PersonSelectVo> convertCentersInfo(List<CentersInfo> list) {
		List<PersonSelectVo> selects = new ArrayList<PersonSelectVo>();
		for (CentersInfo centersInfo : list) {
			selects.add(new PersonSelectVo(TaskVisibleType.CENTER.getValue(), centersInfo.getId(), centersInfo.getName()));
		}
		return selects;
	}

	public boolean isSelectAllCentreRoom(List<TaskVisibleInfo> selectObjs) {
		for (TaskVisibleInfo taskVisibleInfo : selectObjs) {
			if ((taskVisibleInfo.getType() == TaskVisibleType.CENTER.getValue()) && ("0".equals(taskVisibleInfo.getObjId()))) {
				return true;
			}
		}
		return false;
	}

	public List<PersonSelectVo> convertRoomsInfo(List<RoomInfo> roomList) {
		List<PersonSelectVo> selects = new ArrayList<PersonSelectVo>();
		for (RoomInfo roomInfo : roomList) {
			selects.add(new PersonSelectVo(TaskVisibleType.ROOM.getValue(), roomInfo.getId(), roomInfo.getName()));
		}
		return selects;
	}

	public boolean isSelectAllChildrenRoom(List<TaskVisibleInfo> selectObjs) {
		for (TaskVisibleInfo taskVisibleInfo : selectObjs) {
			if ((taskVisibleInfo.getType() == TaskVisibleType.CENTER.getValue()) && ("0".equals(taskVisibleInfo.getObjId()))) {
				return true;
			}
		}
		return false;
	}

	public List<PersonSelectVo> convertUserInfo(List<UserVo> userInfos) {
		List<PersonSelectVo> selects = new ArrayList<PersonSelectVo>();
		for (UserVo userVo : userInfos) {
			PersonSelectVo model = new PersonSelectVo(TaskVisibleType.USER.getValue(), userVo.getAccountId(), userVo.getFullName());
			model.setPersonColor(model.getPersonColor());
			selects.add(model);
		}
		return selects;
	}

	public boolean isSelectAllStaff(List<TaskVisibleInfo> selectObjs) {
		for (TaskVisibleInfo taskVisibleInfo : selectObjs) {
			if ((taskVisibleInfo.getType() == TaskVisibleType.CENTER.getValue()) && ("0".equals(taskVisibleInfo.getObjId()))) {
				return true;
			}
		}
		return false;
	}

}
