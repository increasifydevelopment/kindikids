package com.aoyuntek.aoyun.factory;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.context.ContextLoader;

import com.aoyuntek.aoyun.uitl.FileUtil;

public class BaseFactory {

    @Value("#{sys}")
    private Properties sysProperties;
    @Value("#{email}")
    private Properties emailProperties;

    public String getSystemMsg(String tipKey) {
        return sysProperties.getProperty(tipKey);
    }

    public String getSystemEmailMsg(String tipKey) {
        return emailProperties.getProperty(tipKey);
    }

    /**
     * @description 获取邮件模版Html
     * @author hxzhang
     * @create 2016年12月29日下午2:15:17
     */
    public String getEmailHtml() {
        String html = "";
        String filePath = ContextLoader.getCurrentWebApplicationContext().getServletContext().getRealPath("/") + "templet" + File.separator + "email"
                + File.separator + "email.html";
        try {
            html = FileUtil.readTxt(filePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return html;
    }

    /**
     * 
     * @description 获取占位符
     * @author gfwang
     * @create 2016年4月18日下午5:13:16
     * @version 1.0
     * @param tagName
     *            名称
     * @return name
     */
    public String getHtmlTag(String tagName) {
        return "\\$\\{" + tagName + "\\}";
    }
    
}
