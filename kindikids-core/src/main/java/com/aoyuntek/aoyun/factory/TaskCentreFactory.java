package com.aoyuntek.aoyun.factory;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aoyuntek.aoyun.condtion.CenterCondition;
import com.aoyuntek.aoyun.constants.Select2Constants;
import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.RoomInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.dao.program.ProgramIntobsleaInfoMapper;
import com.aoyuntek.aoyun.dao.program.TaskProgramFollowUpInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskFollowUpInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskFrequencyInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskResponsibleLogInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskVisibleInfoMapper;
import com.aoyuntek.aoyun.entity.po.CentersInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskVisibleInfo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.base.UserVo;
import com.aoyuntek.aoyun.entity.vo.task.PersonSelectVo;
import com.aoyuntek.aoyun.entity.vo.task.TaskInfoVo;
import com.aoyuntek.aoyun.enums.ArchivedStatus;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.enums.TaskVisibleType;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

@Component
public class TaskCentreFactory extends TaskFactory {

	@Autowired
	private RoomInfoMapper roomInfoMapper;
	@Autowired
	private UserInfoMapper userInfoMapper;
	@Autowired
	private CentersInfoMapper centersInfoMapper;
	@Autowired
	private ProgramIntobsleaInfoMapper programIntobsleaInfoMapper;
	@Autowired
	private TaskProgramFollowUpInfoMapper taskProgramFollowUpInfoMapper;
	@Autowired
	private TaskVisibleInfoMapper taskVisibleInfoMapper;
	@Autowired
	private TaskInfoMapper taskInfoMapper;
	@Autowired
	private TaskResponsibleLogInfoMapper taskResponsibleLogInfoMapper;
	@Autowired
	private UserFactory userFactory;
	@Autowired
	private TaskFrequencyInfoMapper taskFrequencyInfoMapper;
	@Autowired
	private TaskFollowUpInfoMapper taskFollowUpInfoMapper;

	/**
	 * 
	 * @author dlli5 at 2016年11月28日上午10:04:09
	 * @param taskSelect
	 * @param centreIds
	 * @return
	 */
	public boolean alsoContainCentre(List<TaskVisibleInfo> taskSelect, List<String> centreIds) {
		boolean haveAll = isSelectAllCenter(taskSelect);
		if (!haveAll) {
			List<String> selectCentreId = new ArrayList<String>();
			List<TaskVisibleInfo> selectVisibleInfos = taskSelect;
			if (ListUtil.isNotEmpty(selectVisibleInfos)) {
				for (TaskVisibleInfo taskVisibleInfo : selectVisibleInfos) {
					selectCentreId.add(taskVisibleInfo.getObjId());
				}
			}
			if (ListUtil.isNotEmpty(centreIds)) {
				for (String centreId : centreIds) {
					if (!selectCentreId.contains(centreId)) {
						return false;
					}
				}
			}
		}
		return true;
	}

	/**
	 * 
	 * @author dlli5 at 2016年9月20日下午6:33:23
	 * @param centreId
	 * @param taskVisibleInfos
	 * @return
	 */
	private boolean isSelectOfCentre(String centreId, List<TaskVisibleInfo> taskVisibleInfos) {
		if (ListUtil.isEmpty(taskVisibleInfos)) {
			return false;
		}
		for (TaskVisibleInfo taskVisibleInfo : taskVisibleInfos) {
			if (centreId.equals(taskVisibleInfo.getObjId())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * @author dlli5 at 2016年9月20日下午6:33:26
	 * @param list
	 * @param taskVisibleInfos
	 * @return
	 */
	public List dealCenterOfSelect(List<CentersInfo> list, List<TaskVisibleInfo> taskVisibleInfos, String findText) {
		if (list == null) {
			list = new ArrayList<CentersInfo>();
		}
		List<PersonSelectVo> selecterPos = new ArrayList<PersonSelectVo>();

		CentersInfo all = new CentersInfo();
		all.setName(Select2Constants.AllCentres);
		all.setId("0");
		list.add(0, all);

		for (CentersInfo centersInfo : list) {
			PersonSelectVo select = new PersonSelectVo(TaskVisibleType.CENTER.getValue(), centersInfo.getId(), centersInfo.getName());
			// 是否选中
			select.setSelected(isSelectOfCentre(centersInfo.getId(), taskVisibleInfos));
			selecterPos.add(select);
		}

		return filterSelect(selecterPos, findText);
	}

	public boolean haveOnlySelectOneCenter(List<PersonSelectVo> selectList) {
		if (selectList == null) {
			selectList = new ArrayList<PersonSelectVo>();
		}
		if (selectList.size() == 1) {
			if ((selectList.get(0).getType() == TaskVisibleType.CENTER.getValue()) && (!"0".equals(selectList.get(0).getId()))) {
				return true;
			}
		}
		return false;
	}

	public boolean haveOnlySelectAllCenter(List<PersonSelectVo> selectList) {
		if (selectList == null) {
			selectList = new ArrayList<PersonSelectVo>();
		}
		if (selectList.size() == 1) {
			if ((selectList.get(0).getType() == TaskVisibleType.CENTER.getValue()) && (!"0".equals(selectList.get(0).getId()))) {
				return true;
			}
		}
		return false;
	}

	public List<String> getSelectCentreIds(List<PersonSelectVo> selectList) {
		if (selectList == null) {
			selectList = new ArrayList<PersonSelectVo>();
		}
		List<String> array = new ArrayList<String>();
		for (PersonSelectVo personSelectVo : selectList) {
			if ((personSelectVo.getType() == TaskVisibleType.CENTER.getValue()) && (!"0".equals(personSelectVo.getId()))) {
				array.add(personSelectVo.getId());
			}
		}
		return array;
	}

	public List dealCenterOfResponsible(List<UserVo> userInfos, List<TaskVisibleInfo> taskVisibleInfos, String findText) {
		if (userInfos == null) {
			userInfos = new ArrayList<UserVo>();
		}
		List<PersonSelectVo> selecterPos = new ArrayList<PersonSelectVo>();

		PersonSelectVo ordinator = new PersonSelectVo(TaskVisibleType.ROLE_GROUP.getValue(), "0", Select2Constants.CentreManagers);
		ordinator.setSelected(isSelected(ordinator.getId(), ordinator.getType(), taskVisibleInfos));
		selecterPos.add(ordinator);

		for (UserVo userInfo : userInfos) {
			PersonSelectVo select = new PersonSelectVo(TaskVisibleType.USER.getValue(), userInfo.getAccountId(), userInfo.getFullName());
			// 是否选中
			select.setSelected(isSelected(select.getId(), select.getType(), taskVisibleInfos));
			selecterPos.add(select);
		}
		return filterSelect(selecterPos, findText);
	}

	public List dealCenterOfImplementers(List<UserVo> userInfos, List<TaskVisibleInfo> taskVisibleInfos, String findText) {
		if (userInfos == null) {
			userInfos = new ArrayList<UserVo>();
		}
		List<PersonSelectVo> selecterPos = new ArrayList<PersonSelectVo>();

		PersonSelectVo ordinatorAllStaffOfCentre = new PersonSelectVo(TaskVisibleType.CENTER.getValue(), "0",
				Select2Constants.AllStaffInTheCentre);
		ordinatorAllStaffOfCentre
				.setSelected(isSelected(ordinatorAllStaffOfCentre.getId(), ordinatorAllStaffOfCentre.getType(), taskVisibleInfos));
		selecterPos.add(ordinatorAllStaffOfCentre);

		PersonSelectVo ordinator = new PersonSelectVo(TaskVisibleType.ROLE_GROUP.getValue(), "0", Select2Constants.CentreManagers);
		ordinator.setSelected(isSelected(ordinator.getId(), ordinator.getType(), taskVisibleInfos));
		selecterPos.add(ordinator);

		PersonSelectVo ordinatorEducators = new PersonSelectVo(TaskVisibleType.ROLE.getValue(), Role.Educator.getValue() + "", "Educators");
		ordinatorEducators.setSelected(isSelected(ordinatorEducators.getId(), ordinatorEducators.getType(), taskVisibleInfos));
		selecterPos.add(ordinatorEducators);

		PersonSelectVo ordinatorCooks = new PersonSelectVo(TaskVisibleType.ROLE.getValue(), Role.Cook.getValue() + "", "Cooks");
		ordinatorCooks.setSelected(isSelected(ordinatorCooks.getId(), ordinatorCooks.getType(), taskVisibleInfos));
		selecterPos.add(ordinatorCooks);

		PersonSelectVo ordinatorCasuals = new PersonSelectVo(TaskVisibleType.ROLE.getValue(), Role.Casual.getValue() + "", "Casuals");
		ordinatorCasuals.setSelected(isSelected(ordinatorCasuals.getId(), ordinatorCasuals.getType(), taskVisibleInfos));
		selecterPos.add(ordinatorCasuals);

		for (UserVo userInfo : userInfos) {
			if (StringUtil.isNotEmpty(findText) && !userInfo.getFullName().toLowerCase().contains(findText.toLowerCase())) {
				continue;
			}
			PersonSelectVo select = new PersonSelectVo(TaskVisibleType.USER.getValue(), userInfo.getAccountId(), userInfo.getFullName());
			// 是否选中
			select.setSelected(isSelected(select.getId(), select.getType(), taskVisibleInfos));
			selecterPos.add(select);
		}

		return filterSelect(selecterPos, findText);
	}

	public boolean isInImplementersOfCentre(TaskInfoVo taskInfoVo, UserInfoVo currentUser) {
		for (TaskVisibleInfo items : taskInfoVo.getTaskImplementersVisibles()) {
			// All Staff in the centre
			if (items.getType() == TaskVisibleType.CENTER.getValue()) {
				if (isInCentre(taskInfoVo.getSelectTaskVisibles(), currentUser)) {
					return true;
				}
			} else if (items.getType() == TaskVisibleType.ROLE_GROUP.getValue()) {
				boolean inRole = containRoles(currentUser.getRoleInfoVoList(), currentUser.getHistoryRoles(), Role.CentreManager,
						Role.EducatorSecondInCharge);
				if (inRole && isInCentre(taskInfoVo.getSelectTaskVisibles(), currentUser)) {
					return true;
				}
			} else if (items.getType() == TaskVisibleType.ROLE.getValue()) {
				boolean inRole = containRoles(currentUser.getRoleInfoVoList(), currentUser.getHistoryRoles(),
						Role.getType(Short.parseShort(items.getObjId())));
				if (inRole && isInCentre(taskInfoVo.getSelectTaskVisibles(), currentUser)) {
					return true;
				}
			} else if (items.getType() == TaskVisibleType.USER.getValue()) {
				if (items.getObjId().equals(currentUser.getAccountInfo().getId())) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * 是否
	 * 
	 * @author dlli5 at 2016年9月22日下午4:29:53
	 * @param taskInfoVo
	 * @param currentUser
	 * @return
	 */
	public boolean isInPersonLiableOfCentre(TaskInfoVo taskInfoVo, UserInfoVo currentUser) {
		for (TaskVisibleInfo items : taskInfoVo.getResponsibleTaskVisibles()) {
			// 如果是Co-ordinato 那么判断自己是否是这些角色其中的一个
			if (items.getType() == TaskVisibleType.ROLE_GROUP.getValue()) {
				boolean inRole = containRoles(currentUser.getRoleInfoVoList(), currentUser.getHistoryRoles(), Role.CentreManager,
						Role.EducatorSecondInCharge);
				if (inRole && isInCentre(taskInfoVo.getSelectTaskVisibles(), currentUser)) {
					return true;
				}
			}
			// 单独员工
			else {
				if (items.getObjId().equals(currentUser.getAccountInfo().getId())) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean isInCentre(List<TaskVisibleInfo> selectTaskVisibles, UserInfoVo currentUser) {
		// TODO big 此等应该在数据库查询
		for (TaskVisibleInfo taskVisibleInfo : selectTaskVisibles) {
			if ("0".equals(taskVisibleInfo.getObjId())) {
				return true;
			}
			if (StringUtil.isNotEmpty(currentUser.getUserInfo().getCentersId())
					&& taskVisibleInfo.getObjId().equals(currentUser.getUserInfo().getCentersId())) {
				return true;
			}
			// hxzhang 如果是兼职根据其排班园区历史信息来获取task
			if (containRoles(currentUser.getRoleInfoVoList(), Role.Casual)
					&& currentUser.getRosterCentreIds().contains(taskVisibleInfo.getObjId())) {
				return true;
			}
		}
		return false;
	}

	public List<String> getCentreOfSelectCentre(List<TaskVisibleInfo> selectTaskVisibles) {
		List<String> result = new ArrayList<String>();
		for (TaskVisibleInfo taskVisibleInfo : selectTaskVisibles) {
			if ("0".equals(taskVisibleInfo.getObjId())) {
				result.clear();
				CenterCondition condition = new CenterCondition();
				condition.setPageIndex(0);
				condition.setPageSize(Integer.MAX_VALUE);
				condition.setStatus(ArchivedStatus.UnArchived.getValue());
				List<CentersInfo> centersInfos = centersInfoMapper.getCenterList(condition);
				for (CentersInfo centersInfo : centersInfos) {
					result.add(centersInfo.getId());
				}
				return result;
			} else {
				result.add(taskVisibleInfo.getObjId());
			}
		}
		return result;
	}

}
