package com.aoyuntek.aoyun.factory;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aoyuntek.aoyun.condtion.UserDetailCondition;
import com.aoyuntek.aoyun.constants.Select2Constants;
import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.RoomInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.dao.program.ProgramIntobsleaInfoMapper;
import com.aoyuntek.aoyun.dao.program.TaskProgramFollowUpInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskFollowUpInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskFrequencyInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskResponsibleLogInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskVisibleInfoMapper;
import com.aoyuntek.aoyun.entity.po.CentersInfo;
import com.aoyuntek.aoyun.entity.po.RoomInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskVisibleInfo;
import com.aoyuntek.aoyun.entity.vo.SelecterGroupVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.base.UserVo;
import com.aoyuntek.aoyun.entity.vo.task.PersonSelectVo;
import com.aoyuntek.aoyun.entity.vo.task.TaskInfoVo;
import com.aoyuntek.aoyun.enums.ArchivedStatus;
import com.aoyuntek.aoyun.enums.EnrolledState;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.enums.TaskVisibleType;
import com.aoyuntek.aoyun.enums.UserType;
import com.theone.string.util.StringUtil;

@Component
public class TaskChildFactory extends TaskFactory {

	@Autowired
	private RoomInfoMapper roomInfoMapper;
	@Autowired
	private UserInfoMapper userInfoMapper;
	@Autowired
	private CentersInfoMapper centersInfoMapper;
	@Autowired
	private ProgramIntobsleaInfoMapper programIntobsleaInfoMapper;
	@Autowired
	private TaskProgramFollowUpInfoMapper taskProgramFollowUpInfoMapper;
	@Autowired
	private TaskVisibleInfoMapper taskVisibleInfoMapper;
	@Autowired
	private TaskInfoMapper taskInfoMapper;
	@Autowired
	private TaskResponsibleLogInfoMapper taskResponsibleLogInfoMapper;
	@Autowired
	private UserFactory userFactory;
	@Autowired
	private TaskFrequencyInfoMapper taskFrequencyInfoMapper;
	@Autowired
	private TaskFollowUpInfoMapper taskFollowUpInfoMapper;

	public List dealChildOfImplementers(List<UserVo> userInfos, List<TaskVisibleInfo> taskVisibleInfos, String findText) {
		if (userInfos == null) {
			userInfos = new ArrayList<UserVo>();
		}
		List<PersonSelectVo> selecterPos = new ArrayList<PersonSelectVo>();

		PersonSelectVo ordinatorAllStaffOfCentre = new PersonSelectVo(TaskVisibleType.CENTER.getValue(), "0",
				Select2Constants.AllStaffInTheCentre);
		ordinatorAllStaffOfCentre
				.setSelected(isSelected(ordinatorAllStaffOfCentre.getId(), ordinatorAllStaffOfCentre.getType(), taskVisibleInfos));
		selecterPos.add(ordinatorAllStaffOfCentre);
		for (UserVo userInfo : userInfos) {
			if (StringUtil.isNotEmpty(findText) && !userInfo.getFullName().toLowerCase().contains(findText.toLowerCase())) {
				continue;
			}
			PersonSelectVo select = new PersonSelectVo(TaskVisibleType.USER.getValue(), userInfo.getAccountId(), userInfo.getFullName());
			// 是否选中
			select.setSelected(isSelected(select.getId(), select.getType(), taskVisibleInfos));
			selecterPos.add(select);
		}

		return filterSelect(selecterPos, findText);
	}

	public List dealChildrenOfResponsible(List<UserVo> userInfos, List<TaskVisibleInfo> taskVisibleInfos, String findText) {
		if (userInfos == null) {
			userInfos = new ArrayList<UserVo>();
		}
		List<PersonSelectVo> selecterPos = new ArrayList<PersonSelectVo>();

		PersonSelectVo ordinator = new PersonSelectVo(TaskVisibleType.ROLE.getValue(), Role.Educator.getValue() + "",
				Select2Constants.AllEducatorsInThRoom);
		ordinator.setSelected(isSelected(ordinator.getId(), ordinator.getType(), taskVisibleInfos));
		selecterPos.add(ordinator);

		for (UserVo userInfo : userInfos) {
			PersonSelectVo select = new PersonSelectVo(TaskVisibleType.USER.getValue(), userInfo.getAccountId(), userInfo.getFullName());
			// 是否选中
			select.setSelected(isSelected(select.getId(), select.getType(), taskVisibleInfos));
			selecterPos.add(select);
		}
		return filterSelect(selecterPos, findText);
	}

	private boolean isInChild(List<TaskVisibleInfo> selectTaskVisibles, UserInfoVo currentUser) {
		for (TaskVisibleInfo taskVisibleInfo : selectTaskVisibles) {
			// all children
			if (taskVisibleInfo.getType() == TaskVisibleType.CENTER.getValue() && "0".equals(taskVisibleInfo.getObjId())) {
				return true;
			}
			// all center one children
			if (StringUtil.isNotEmpty(currentUser.getUserInfo().getCentersId())
					&& taskVisibleInfo.getType() == TaskVisibleType.CENTER.getValue()
					&& currentUser.getUserInfo().getCentersId().equals(taskVisibleInfo.getObjId())) {
				return true;
			}
			// all room one children
			if (taskVisibleInfo.getType() == TaskVisibleType.ROOM.getValue()) {
				// TODO big 这里可以优化
				RoomInfo roomInfo = roomInfoMapper.selectByPrimaryKey(taskVisibleInfo.getObjId());
				if (roomInfo.getCentersId().equals(currentUser.getUserInfo().getCentersId())) {
					return true;
				}
			}
			// child one
			if (taskVisibleInfo.getType() == TaskVisibleType.USER.getValue()) {
				// TODO big 这里可以优化
				// TODO big 这里要取固话的
				UserInfo userInfo = userInfoMapper.getUserInfoByAccountId(taskVisibleInfo.getObjId());
				if (userInfo.getCentersId().equals(currentUser.getUserInfo().getCentersId())) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean isInImplementersOfChild(TaskInfoVo taskInfoVo, UserInfoVo currentUser) {
		for (TaskVisibleInfo items : taskInfoVo.getTaskImplementersVisibles()) {
			// All Staff in the centre
			if (items.getType() == TaskVisibleType.CENTER.getValue()) {
				if (isInChild(taskInfoVo.getSelectTaskVisibles(), currentUser)) {
					return true;
				}
			}
			// Staff Name（可直接指定某员工）
			else if (items.getType() == TaskVisibleType.USER.getValue()) {
				if (items.getObjId().equals(currentUser.getAccountInfo().getId())) {
					return true;
				}
			}
		}
		return false;
	}

	public List<UserVo> getChildOfSelectChild(List<TaskVisibleInfo> selectTaskVisibles) {
		List<UserVo> result = new ArrayList<UserVo>();
		List<String> userIds = new ArrayList<String>();
		for (TaskVisibleInfo taskVisibleInfo : selectTaskVisibles) {
			// all children
			if (taskVisibleInfo.getType() == TaskVisibleType.CENTER.getValue() && "0".equals(taskVisibleInfo.getObjId())) {
				UserDetailCondition userCondtion = new UserDetailCondition();
				userCondtion.initAll();
				userCondtion.setUserType(UserType.Child.getValue());
				userCondtion.setStatus(ArchivedStatus.UnArchived.getValue());
				userCondtion.setEnrolled(EnrolledState.Enrolled.getValue());
				List<UserVo> userInfos = userInfoMapper.getListOfUserVo(userCondtion);
				return userInfos;
			}
			// all center one children
			if (taskVisibleInfo.getType() == TaskVisibleType.CENTER.getValue()) {
				UserDetailCondition userCondtion = new UserDetailCondition();
				userCondtion.initAll();
				userCondtion.setUserType(UserType.Child.getValue());
				userCondtion.setStatus(ArchivedStatus.UnArchived.getValue());
				userCondtion.setEnrolled(EnrolledState.Enrolled.getValue());
				List<String> centreIds = new ArrayList<String>();
				centreIds.add(taskVisibleInfo.getObjId());
				userCondtion.setCentreIds(centreIds);
				List<UserVo> userInfos = userInfoMapper.getListOfUserVo(userCondtion);
				for (UserVo userVo : userInfos) {
					if (!userIds.contains(userVo.getAccountId())) {
						result.add(userVo);
						userIds.add(userVo.getAccountId());
					}
				}
			}
			// all room one children
			if (taskVisibleInfo.getType() == TaskVisibleType.ROOM.getValue()) {
				UserDetailCondition userCondtion = new UserDetailCondition();
				userCondtion.initAll();
				userCondtion.setUserType(UserType.Child.getValue());
				userCondtion.setStatus(ArchivedStatus.UnArchived.getValue());
				userCondtion.setEnrolled(EnrolledState.Enrolled.getValue());
				userCondtion.setRoomId(taskVisibleInfo.getObjId());
				List<UserVo> userInfos = userInfoMapper.getListOfUserVo(userCondtion);
				for (UserVo userVo : userInfos) {
					if (!userIds.contains(userVo.getAccountId())) {
						result.add(userVo);
						userIds.add(userVo.getAccountId());
					}
				}
			}
			// child one
			if (taskVisibleInfo.getType() == TaskVisibleType.USER.getValue()) {
				UserDetailCondition userCondtion = new UserDetailCondition();
				userCondtion.initAll();
				userCondtion.setUserType(UserType.Child.getValue());
				userCondtion.setStatus(ArchivedStatus.UnArchived.getValue());
				userCondtion.setEnrolled(EnrolledState.Enrolled.getValue());
				userCondtion.setAccountId(taskVisibleInfo.getObjId());
				List<UserVo> userInfos = userInfoMapper.getListOfUserVo(userCondtion);
				for (UserVo userVo : userInfos) {
					if (!userIds.contains(userVo.getAccountId())) {
						result.add(userVo);
						userIds.add(userVo.getAccountId());
					}
				}
			}
		}
		return result;
	}

	public boolean isInPersonLiableOfChild(TaskInfoVo taskInfoVo, UserInfoVo currentUser) {
		for (TaskVisibleInfo items : taskInfoVo.getResponsibleTaskVisibles()) {
			// All Educators in the room
			if (items.getType() == TaskVisibleType.ROLE.getValue()) {
				boolean inRole = containRoles(currentUser.getRoleInfoVoList(), currentUser.getHistoryRoles(), Role.Educator,
						Role.EducatorECT);
				if (inRole && isInChild(taskInfoVo.getSelectTaskVisibles(), currentUser)) {
					return true;
				}
			}
			// 单独员工
			else if (items.getType() == TaskVisibleType.USER.getValue()) {
				if (items.getObjId().equals(currentUser.getAccountInfo().getId())) {
					return true;
				}
			}
		}
		return false;
	}

	public List dealChildOfSelect(List<CentersInfo> list, List<RoomInfo> roomList, List<UserVo> userInfos,
			List<TaskVisibleInfo> taskVisibleInfos, String findText) {
		if (list == null) {
			list = new ArrayList<CentersInfo>();
		}
		List<SelecterGroupVo> selecterPos = new ArrayList<SelecterGroupVo>();

		SelecterGroupVo all = new SelecterGroupVo();
		all.setText(null);

		List<PersonSelectVo> allItem = new ArrayList<PersonSelectVo>();
		PersonSelectVo allRoom = new PersonSelectVo(TaskVisibleType.CENTER.getValue(), "0", Select2Constants.AllChildren);
		// 是否选中
		allRoom.setSelected(isSelected(allRoom.getId(), allRoom.getType(), taskVisibleInfos));
		allItem.add(allRoom);
		all.setChildren(allItem);
		selecterPos.add(all);

		for (CentersInfo centersInfo : list) {
			SelecterGroupVo selecterGroupVo = new SelecterGroupVo();
			selecterGroupVo.setText(centersInfo.getName());

			List<PersonSelectVo> items = new ArrayList<PersonSelectVo>();
			// 添加all center one children
			PersonSelectVo allCenterRoom = new PersonSelectVo(TaskVisibleType.CENTER.getValue(), centersInfo.getId(),
					MessageFormat.format(Select2Constants.AllXChildren, centersInfo.getName()));
			// 看看是否被选中
			allCenterRoom.setSelected(isSelected(allCenterRoom.getId(), allCenterRoom.getType(), taskVisibleInfos));
			items.add(allCenterRoom);

			for (RoomInfo room : roomList) {
				if (room.getCentersId().equals(centersInfo.getId())) {
					PersonSelectVo select = new PersonSelectVo(TaskVisibleType.ROOM.getValue(), room.getId(),
							MessageFormat.format(Select2Constants.AllXChildren, room.getName()));
					// 是否选中
					select.setSelected(isSelected(select.getId(), select.getType(), taskVisibleInfos));
					items.add(select);
				}
			}

			// 添加这个centre下的小孩
			for (UserVo child : userInfos) {
				if (child.getCentersId().equals(centersInfo.getId())) {
					PersonSelectVo select = new PersonSelectVo(TaskVisibleType.USER.getValue(), child.getAccountId(), child.getFullName());
					// 是否选中
					select.setSelected(isSelected(select.getId(), select.getType(), taskVisibleInfos));
					items.add(select);
				}
			}
			selecterGroupVo.setChildren(items);
			selecterPos.add(selecterGroupVo);
		}

		return filterGroupSelect(selecterPos, findText);
	}

	public List<String> getSelectRoomIds(List<PersonSelectVo> selectList) {
		if (selectList == null) {
			selectList = new ArrayList<PersonSelectVo>();
		}
		List<String> array = new ArrayList<String>();
		for (PersonSelectVo personSelectVo : selectList) {
			if ((personSelectVo.getType() == TaskVisibleType.ROOM.getValue()) && (!"0".equals(personSelectVo.getId()))) {
				// 根据roomId 找到 centreId
				String roomId = personSelectVo.getId();
				array.add(roomId);
			}

			if ((personSelectVo.getType() == TaskVisibleType.USER.getValue()) && (!"0".equals(personSelectVo.getId()))) {
				// 根据roomId 找到 centreId
				String childrenAccountId = personSelectVo.getId();
				UserInfo userInfo = userInfoMapper.getUserInfoByAccountId(childrenAccountId);
				array.add(userInfo.getRoomId());
			}
		}
		return array;
	}

	public boolean haveOnlySelectChildrenInSameRoom(List<PersonSelectVo> selectList) {
		if (selectList == null) {
			selectList = new ArrayList<PersonSelectVo>();
		}
		// 只是单独的选择了一个all room {0} children
		if (selectList.size() == 1 && selectList.get(0).getType() == TaskVisibleType.ROOM.getValue()) {
			return true;
		}

		// 只是单独的选择了child 且 child同属于一个room下
		boolean onlySelectChild = true;
		List<String> roomIds = new ArrayList<String>();
		for (PersonSelectVo personSelectVo : selectList) {
			if ((personSelectVo.getType() != TaskVisibleType.USER.getValue())) {
				onlySelectChild = false;
				break;
			} else {
				// 是否是同一个room的
				UserInfo userInfo = userInfoMapper.getUserInfoByAccountId(personSelectVo.getId());
				if (!roomIds.contains(userInfo.getRoomId()) && roomIds.size() == 1) {
					onlySelectChild = false;
					break;
				}
				if (!roomIds.contains(userInfo.getRoomId())) {
					roomIds.add(userInfo.getRoomId());
				}
			}
		}
		if (onlySelectChild) {
			return true;
		}

		// 看看如果选择all room {0} children一个 且多个child 混合的 那么要求选择的child都是属于这个room的
		boolean flag = true;
		roomIds = new ArrayList<String>();
		for (PersonSelectVo personSelectVo : selectList) {
			if ((personSelectVo.getType() == TaskVisibleType.ROOM.getValue())) {
				if (roomIds.size() == 1) {
					flag = false;
					break;
				}
				roomIds.add(personSelectVo.getId());
			} else if (personSelectVo.getType() == TaskVisibleType.USER.getValue()) {
				// 是否是同一个room的
				UserInfo userInfo = userInfoMapper.getUserInfoByAccountId(personSelectVo.getId());
				if (!roomIds.contains(userInfo.getRoomId()) && roomIds.size() == 1) {
					flag = false;
					break;
				}
				if (!roomIds.contains(userInfo.getRoomId())) {
					roomIds.add(userInfo.getRoomId());
				}
			} else {
				flag = false;
				break;
			}
		}
		if (flag) {
			return true;
		}
		return false;
	}
}
