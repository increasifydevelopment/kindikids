package com.aoyuntek.aoyun.factory;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aoyuntek.aoyun.condtion.UserCondition;
import com.aoyuntek.aoyun.constants.TipMsgConstants;
import com.aoyuntek.aoyun.dao.AccountInfoMapper;
import com.aoyuntek.aoyun.dao.AttendanceHistoryInfoMapper;
import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.ChangeAttendanceRequestInfoMapper;
import com.aoyuntek.aoyun.dao.ChildAttendanceInfoMapper;
import com.aoyuntek.aoyun.dao.EmailMsgMapper;
import com.aoyuntek.aoyun.dao.EmailPlanInfoMapper;
import com.aoyuntek.aoyun.dao.GivingNoticeInfoMapper;
import com.aoyuntek.aoyun.dao.MsgEmailInfoMapper;
import com.aoyuntek.aoyun.dao.RequestLogInfoMapper;
import com.aoyuntek.aoyun.dao.RoomInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.dao.child.RoomChangeInfoMapper;
import com.aoyuntek.aoyun.entity.po.AccountInfo;
import com.aoyuntek.aoyun.entity.po.CentersInfo;
import com.aoyuntek.aoyun.entity.po.ChangeAttendanceRequestInfo;
import com.aoyuntek.aoyun.entity.po.ChildAttendanceInfo;
import com.aoyuntek.aoyun.entity.po.EmailMsg;
import com.aoyuntek.aoyun.entity.po.EmailPlanInfo;
import com.aoyuntek.aoyun.entity.po.MsgEmailInfo;
import com.aoyuntek.aoyun.entity.po.OutFamilyPo;
import com.aoyuntek.aoyun.entity.po.RoomInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.vo.EmailVo;
import com.aoyuntek.aoyun.entity.vo.RoleInfoVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.base.UserVo;
import com.aoyuntek.aoyun.enums.ArchivedStatus;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.EmailMsgType;
import com.aoyuntek.aoyun.enums.EmailPlanType;
import com.aoyuntek.aoyun.enums.EmailType;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.service.IMsgEmailInfoService;
import com.aoyuntek.aoyun.service.attendance.IAttendanceService;
import com.aoyuntek.aoyun.service.task.ITaskInstance;
import com.aoyuntek.aoyun.uitl.DateUtil;
import com.aoyuntek.aoyun.uitl.EmailUtil;
import com.aoyuntek.framework.constants.SessionConts;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

@Component
public class UserFactory extends BaseFactory {
	public static Logger logger = Logger.getLogger(UserFactory.class);
	@Autowired
	private UserInfoMapper userInfoMapper;
	@Autowired
	private AccountInfoMapper accountInfoMapper;
	@Autowired
	private IMsgEmailInfoService emailService;
	@Autowired
	private MsgEmailInfoMapper emailInfoMapper;
	@Autowired
	private RequestLogInfoMapper requestLogInfoMapper;
	@Autowired
	private ITaskInstance taskInstance;
	@Autowired
	private AttendanceHistoryInfoMapper attendanceHistoryInfoMapper;
	@Autowired
	private RoomChangeInfoMapper roomChangeInfoMapper;
	@Autowired
	private CentersInfoMapper centersInfoMapper;
	@Autowired
	private ChildAttendanceInfoMapper childAttendanceInfoMapper;
	@Autowired
	private GivingNoticeInfoMapper givingNoticeInfoMapper;
	@Autowired
	private EmailMsgMapper emailMsgMapper;
	@Autowired
	private RoomInfoMapper roomInfoMapper;
	@Autowired
	private ChangeAttendanceRequestInfoMapper changeAttendanceRequestInfoMapper;
	@Autowired
	private EmailPlanInfoMapper emailPlanInfoMapper;
	@Autowired
	private IAttendanceService attendanceService;

	private OutFamilyPo outFamily;

	/**
	 * 
	 * @author dlli5 at 2016年12月21日下午3:01:05
	 * @param userInfo
	 * @param currentUser
	 * @param leaveDate
	 */
	public void unArchivedChild(UserInfo userInfo, UserInfoVo currentUser, Date leaveDate) {

	}

	/**
	 * 
	 * @author dlli5 at 2016年12月21日下午1:45:47
	 * @param userInfo
	 * @param currentUser
	 * @param leaveDate
	 */
	public void archivedChild(UserInfo userInfo, UserInfoVo currentUser, Date leaveDate, short emailTypeValue, boolean isHand) {
		AccountInfo accountInfo = accountInfoMapper.selectAccountInfoByUserId(userInfo.getId());
		accountInfo.setStatus(ArchivedStatus.Archived.getValue());
		accountInfoMapper.updateByPrimaryKey(accountInfo);

		// 同步最后的状态到历史记录
		requestLogInfoMapper.dealSynLog(accountInfo.getId());

		// 更新tbl_attendance_history 的最后的时间
		attendanceHistoryInfoMapper.updateEndTime(accountInfo.getId(), leaveDate);

		// 删除排课
		roomChangeInfoMapper.deleteByChild(accountInfo.getId());

		// 给家长发送离园邮件
		sendParentEmailByChild(getSystemEmailMsg("leave_center_title"), getSystemEmailMsg("leave_center_content"), accountInfo.getId(), isHand, emailTypeValue, null,
				leaveDate, null, false);
	}

	@SuppressWarnings("unchecked")
	public List<UserVo> duplicate(List<UserVo>... lists) {
		List<UserVo> result = new ArrayList<UserVo>();
		List<String> userList = new ArrayList<String>();
		for (List<UserVo> list : lists) {
			for (UserVo userVo : list) {
				if (userList.contains(userVo.getAccountId())) {
					continue;
				}
				result.add(userVo);
				userList.add(userVo.getAccountId());
			}
		}
		return result;
	}

	public List<UserInfo> distinctUserInfo(List<UserInfo> willCome) {
		List<UserInfo> result = new ArrayList<UserInfo>();
		List<String> userIds = new ArrayList<String>();
		for (UserInfo userInfo : willCome) {
			if (!userIds.contains(userInfo.getId())) {
				result.add(userInfo);
				userIds.add(userInfo.getId());
			}
		}
		return result;
	}

	public List<UserInfo> getParentList(String familyId) {
		return userInfoMapper.getParentInfoByFamilyId(familyId);
	}

	public String getFamilyIdByUserId(String userId) {
		return userInfoMapper.selectByPrimaryKey(userId).getFamilyId();
	}

	/**
	 * 
	 * @description 发送邮件给小孩家长
	 * @author gfwang
	 * @create 2016年10月28日下午1:25:14
	 * @version 1.0
	 * @param title
	 * @param content
	 * @param childAccountId
	 */
	public void sendParentEmailByChild(String title, String content, String childAccountId, boolean isHand, short emailTypeValue, Date beginDate, Date endDate,
			ChildAttendanceInfo att, OutFamilyPo outFamily) {
		this.outFamily = outFamily;
		sendParentEmailByChild(title, content, childAccountId, isHand, emailTypeValue, beginDate, endDate, att, att.isPlan());
	}

	public void sendParentEmailForAppliction(String title, String content, boolean isHand, OutFamilyPo out) {
		List<EmailVo> emails = new ArrayList<EmailVo>();
		try {
			for (UserInfo parent : out.getParentList()) {
				if (StringUtil.isEmpty(parent.getEmail())) {
					continue;
				}
				String sub = title;
				String msg = content;
				UserInfo outChild = out.getChild();
				String centersStr = dealCenters(out.getCentreList());
				String parentName = getName(parent.getFirstName(), parent.getMiddleName(), parent.getLastName());
				msg = MessageFormat.format(content, parentName, getUserName(outChild), centersStr);
				String html = EmailUtil.replaceHtml(sub, msg);
				emails.add(new EmailVo(parent.getEmail(), parentName, sub, html, true));
			}
			emailService.batchSendEamil(emails);
		} catch (Exception e) {
			logger.error("sendParentEmailForAppliction email fail", e);
		}
		// 创建email msg
		createEmailMsg(out.getChild(), out.getParentList(), EmailMsgType.msg_out_add.getValue());
	}

	public void sendParentEmailByChild(String title, String content, String childAccountId, boolean isHand, short emailTypeValue, Date beginDate, Date endDate,
			Object obj, boolean isPlan) {
		String objId = null;
		Short emailPlanType = null;
		logger.info("sendParentEmailByChild|start|childId=" + childAccountId + ",isHand=" + isHand + ",emailTypeValue=" + emailTypeValue);
		try {
			List<UserInfo> parentList = userInfoMapper.getParentsByChildAccountId(childAccountId);
			UserInfo child = userInfoMapper.getUserInfoByAccountId(childAccountId);
			List<EmailVo> emails = new ArrayList<EmailVo>();
			List<MsgEmailInfo> msgEmails = new ArrayList<MsgEmailInfo>();
			EmailType emailType = EmailType.getType(emailTypeValue);
			String[] ceoEmails = userInfoMapper.getCeoEmails();
			boolean addBcc = false;
			for (UserInfo parent : parentList) {
				String sub = title;
				String msg = content;
				String email = parent.getEmail();
				String parentName = getName(parent.getFirstName(), parent.getMiddleName(), parent.getLastName());
				CentersInfo center = new CentersInfo();
				if (emailTypeValue == EmailType.inside.getValue()) {
					ChildAttendanceInfo att = (ChildAttendanceInfo) obj;
					if (StringUtil.isEmpty(att.getRoomId())) {
						center = centersInfoMapper.selectByPrimaryKey(att.getCenterId());
					} else {
						center = centersInfoMapper.selectByPrimaryKey(centersInfoMapper.getCenterIdByRoomId(att.getRoomId()));
					}
				} else {
					center = centersInfoMapper.getCenterByChildAccountId(childAccountId);
				}
				String html = null;
				switch (emailType) {
				case inside: {
					ChildAttendanceInfo att = (ChildAttendanceInfo) obj;
					objId = childAccountId;
					emailPlanType = EmailPlanType.B.getValue();
					String centerStr = center.getName() + (StringUtil.isNotEmpty(center.getCentreAddress()) ? " " + center.getCentreAddress() : "");
					String daysOfAttendance = getDaysOfAttendance(att.getMonday(), att.getTuesday(), att.getWednesday(), att.getThursday(), att.getFriday());
					String dateStr = DateUtil.format(att.getEnrolmentDate(), "dd/MM/yyyy");
					String phoneNum = StringUtil.isEmpty(center.getCentreContact()) ? "(phone number)" : center.getCentreContact();
					msg = MessageFormat.format(content, getUserName(parent), getUserName(child), centerStr, child.getFirstName(), daysOfAttendance, dateStr,
							getOriDateStr(att), phoneNum);
					html = EmailUtil.replaceHtml(sub, msg);
				}
					break;
				case intoCenter:
					sub = title;
					objId = childAccountId;
					String centerName = center.getName();
					msg = MessageFormat.format(content, getUserName(parent), getUserName(child), centerName, child.getFirstName());
					html = EmailUtil.replaceHtml(sub, msg);
					break;
				case leaveCenter:
					msg = MessageFormat.format(content, getUserName(parent), getUserName(child), DateUtil.format(endDate, "dd/MM/yyyy"));
					html = EmailUtil.replaceHtml(sub, msg);
					break;
				case archiveChild:
					msg = MessageFormat.format(content, getUserName(parent), getUserName(child), DateUtil.format(new Date(), "dd/MM/yyyy"));
					html = EmailUtil.replaceHtml(sub, msg);
					break;
				case givenoticeApproved: {
					objId = (String) obj;
					addBcc = true;
					center = centersInfoMapper.getCenterByChildAccountId(childAccountId);
					String endDateStr = DateUtil.formatDate(endDate, "d MMM yyyy", Locale.ENGLISH);
					msg = MessageFormat.format(content, getUserName(parent), getUserName(child), center.getName(), endDateStr);
					html = EmailUtil.replaceHtml(sub, msg);
					sub = getSystemEmailMsg("givenotice_approved_sub");
				}
					break;
				case absentee_approved_email: {
					addBcc = true;
					String beginDateStr = DateUtil.formatDate(beginDate, "d MMM yyyy", Locale.ENGLISH);
					String endDateStr = DateUtil.formatDate(endDate, "d MMM yyyy", Locale.ENGLISH);
					msg = MessageFormat.format(content, parentName, getUserName(child), beginDateStr, endDateStr,
							dealDateInterval(child.getAccountId(), beginDate, endDate));
					html = EmailUtil.replaceHtml(sub, msg);
					sub = getSystemEmailMsg("absentee_byParent_email_sub");
				}
					break;
				case absent_over_42days:
					msg = MessageFormat.format(content, parentName, getUserName(child));
					html = EmailUtil.replaceHtml(sub, msg);
					break;
				case out_add_child:
					UserInfo outChild = outFamily.getChild();
					String centersStr = dealCenters(outFamily.getCentreList());
					msg = MessageFormat.format(content, parentName, getUserName(outChild), centersStr);
					html = EmailUtil.replaceHtml(sub, msg);
					break;
				case request_attend: {
					ChildAttendanceInfo att = (ChildAttendanceInfo) obj;
					addBcc = true;
					msg = MessageFormat.format(content, getUserName(parent), getUserName(child), getDaysOfAttendance(att), getNewDaysOfAttendance(att));
					html = EmailUtil.replaceHtml(sub, msg);
					sub = getSystemEmailMsg("request_attendance_sub");
				}
					break;
				case approve_attendance: {
					ChildAttendanceInfo att = (ChildAttendanceInfo) obj;
					objId = att.getRequestId();
					emailPlanType = EmailPlanType.C.getValue();
					addBcc = true;
					RoomInfo roomInfo = roomInfoMapper.selectByPrimaryKey(child.getRoomId());
					ChangeAttendanceRequestInfo re = changeAttendanceRequestInfoMapper.selectByPrimaryKey(att.getRequestId());
					String str = DateUtil.formatDate(re.getChangeDate(), "d MMM yyyy", Locale.ENGLISH);
					String state = re.getState() == 2 ? "Partially Approved" : "Approved";
					sub = MessageFormat.format(sub, state);
					msg = MessageFormat.format(content, getUserName(parent), getUserName(child), getDaysOfAttendance(att), roomInfo.getName(),
							getNewDaysOfAttendance(att), roomInfo.getName(), str);
					html = EmailUtil.replaceHtml(sub, msg);
					sub = getSystemEmailMsg("approve_attendance_sub");
				}
					break;
				case changeRoom: {
					ChildAttendanceInfo att = (ChildAttendanceInfo) obj;
					objId = att.getRequestId();
					emailPlanType = EmailPlanType.C.getValue();
					addBcc = true;
					ChangeAttendanceRequestInfo requestInfo = att.getRequestInfo();
					RoomInfo roomInfo = roomInfoMapper.selectByPrimaryKey(child.getRoomId());
					RoomInfo newRoomInfo = roomInfoMapper.selectByPrimaryKey(requestInfo.getNewRoomId());
					String str = DateUtil.formatDate(requestInfo.getChangeDate(), "d MMM yyyy", Locale.ENGLISH);
					sub = MessageFormat.format(sub, "Approved");
					msg = MessageFormat.format(content, getUserName(parent), getUserName(child), getDaysOfAttendance(att), roomInfo.getName(),
							getNewDaysOfAttendance(att), newRoomInfo.getName(), str);
					html = EmailUtil.replaceHtml(sub, msg);
					sub = getSystemEmailMsg("approve_attendance_sub");
				}
					break;
				case cancel_attendance: {
					ChildAttendanceInfo att = (ChildAttendanceInfo) obj;
					addBcc = true;
					msg = MessageFormat.format(content, getUserName(parent), getUserName(child), getDaysOfAttendance(att), getNewDaysOfAttendance(att));
					html = EmailUtil.replaceHtml(sub, msg);
					sub = getSystemEmailMsg("cancel_attendance_sub");
				}
					break;
				case request_givenotice: {
					addBcc = true;
					CentersInfo centersInfo = centersInfoMapper.selectByPrimaryKey(child.getCentersId());
					msg = MessageFormat.format(content, parentName, getUserName(child), centersInfo.getName(),
							DateUtil.formatDate(endDate, "d MMM yyyy", Locale.ENGLISH));
					html = EmailUtil.replaceHtml(sub, msg);
					sub = getSystemEmailMsg("request_givenotice_sub");
				}
					break;
				case cancel_givenotice:
					addBcc = true;
					CentersInfo centersInfo = centersInfoMapper.selectByPrimaryKey(child.getCentersId());
					msg = MessageFormat.format(content, parentName, getUserName(child), centersInfo.getName(),
							DateUtil.formatDate(endDate, "d MMM yyyy", Locale.ENGLISH));
					html = EmailUtil.replaceHtml(sub, msg);
					sub = getSystemEmailMsg("cancel_givenotice_sub");
					break;
				case request_absentee: {
					addBcc = true;
					String beginDateStr = DateUtil.formatDate(beginDate, "d MMM yyyy", Locale.ENGLISH);
					String endDateStr = DateUtil.formatDate(endDate, "d MMM yyyy", Locale.ENGLISH);
					msg = MessageFormat.format(content, parentName, getUserName(child), beginDateStr, endDateStr,
							dealDateInterval(child.getAccountId(), beginDate, endDate));
					html = EmailUtil.replaceHtml(sub, msg);
					sub = getSystemEmailMsg("request_absentee_sub");
				}
					break;
				case cancel_absentee: {
					addBcc = true;
					String beginDateStr = DateUtil.formatDate(beginDate, "d MMM yyyy", Locale.ENGLISH);
					String endDateStr = DateUtil.formatDate(endDate, "d MMM yyyy", Locale.ENGLISH);
					msg = MessageFormat.format(content, parentName, getUserName(child), beginDateStr, endDateStr);
					html = EmailUtil.replaceHtml(sub, msg);
					sub = getSystemEmailMsg("cancel_absentee_sub");
				}
					break;
				case lapsed_absentee:
					addBcc = true;
					String beginDateStr = DateUtil.formatDate(beginDate, "d MMM yyyy", Locale.ENGLISH);
					String endDateStr = DateUtil.formatDate(endDate, "d MMM yyyy", Locale.ENGLISH);
					msg = MessageFormat.format(content, parentName, getUserName(child), beginDateStr, endDateStr);
					html = EmailUtil.replaceHtml(sub, msg);
					sub = getSystemEmailMsg("lapsed_absentee_sub");
					break;
				default:
					break;
				}

				emails.add(new EmailVo(email, parentName, sub, html, true));
				msgEmails.add(new MsgEmailInfo(UUID.randomUUID().toString(), getSystemMsg("mailHostAccount"), getSystemMsg("email_name"), email, parentName, sub, null,
						new Date(), (short) 0, (short) 1, DeleteFlag.Default.getValue(), html));
			}
			switch (emailType) {
			case inside:
				createEmailMsg(child, parentList, EmailMsgType.msg_out_inside.getValue());
				break;
			case intoCenter:
				createEmailMsg(child, parentList, EmailMsgType.msg_into_center.getValue());
				break;
			case leaveCenter:
				createEmailMsg(child, parentList, EmailMsgType.msg_leave_center.getValue());
				break;
			case givenoticeApproved:
				createEmailMsg(child, parentList, EmailMsgType.msg_approve_give.getValue());
				break;
			case absent_over_42days:
				createEmailMsg(child, parentList, EmailMsgType.msg_more_42.getValue());
				break;
			case out_add_child:
				createEmailMsg(child, parentList, EmailMsgType.msg_out_add.getValue());
				break;

			default:
				break;
			}
			if (ListUtil.isEmpty(emails)) {
				logger.info("sendParentEmailByChild|noparent|childId=" + childAccountId);
				return;
			}
			if (isHand) { // 手动直接发送.
				if (isPlan) {
					dealPlanEmail(objId, emails, emailPlanType, child.getId());
				} else {
					if (addBcc) {
						emailService.batchSendEamil(emails, ceoEmails);
					} else {
						emailService.batchSendEamil(emails);
					}
				}
			} else { // 定时任务过来的存入数据库
				emailInfoMapper.batchInsert(msgEmails);
			}

		} catch (Exception e) {
			logger.error("handle approve email fail", e);
		}
		logger.info("sendParentEmailByChild|end|childId=" + childAccountId);
	}

	private String dealCenters(List<String> centerList) {
		String name = "";
		for (String centerId : centerList) {
			name += "Kindikids Early Learning " + centersInfoMapper.getCenterNameById(centerId) + ", ";
		}
		return name.substring(0, name.length() - 2);
	}

	private String getOriDateStr(ChildAttendanceInfo att) {
		if (att.getOrientation() == null) {
			return " ";
		}
		String str = DateUtil.formatDate(att.getOrientation(), "hh:mma", java.util.Locale.ENGLISH) + " on " + DateUtil.format(att.getOrientation(), "dd/MM/yyyy");
		return str;
	}

	private int dealDateInterval(String accountId, Date startDate, Date endDate) {
		startDate = DateUtil.addDay(startDate, 1);
		endDate = DateUtil.addDay(endDate, -1);
		return attendanceService.getChildAttendanceDays(accountId, startDate, endDate);
	}

	@SuppressWarnings("deprecation")
	private int dealDateInterval2(Date beginDate, Date endDate) {
		beginDate = dealDate2(beginDate, true);
		endDate = dealDate2(endDate, false);
		int count = 0;
		while (beginDate.compareTo(endDate) <= 0) {
			if (beginDate.getDay() != 6 && beginDate.getDay() != 0) {
				count++;
			}
			beginDate.setDate(beginDate.getDate() + 1);
		}
		return count;
	}

	private Date dealDate2(Date date, boolean isAdd) {
		if (isAdd) {
			date = DateUtil.addDay(date, 1);
		} else {
			date = DateUtil.addDay(date, -1);
		}

		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		// 如果是星期六则再加2天为下个星期一
		if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
			if (isAdd) {
				date = DateUtil.addDay(date, 2);
			} else {
				date = DateUtil.addDay(date, -1);
			}
		}
		// 如果是星期日则再加1天为下个星期一
		if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
			if (isAdd) {
				date = DateUtil.addDay(date, 1);
			} else {
				date = DateUtil.addDay(date, -2);
			}

		}
		return date;
	}

	private String dealDate(Date date, boolean isAdd) {
		if (isAdd) {
			date = DateUtil.addDay(date, 1);
		} else {
			date = DateUtil.addDay(date, -1);
		}

		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		// 如果是星期六则再加2天为下个星期一
		if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
			if (isAdd) {
				date = DateUtil.addDay(date, 2);
			} else {
				date = DateUtil.addDay(date, -1);
			}
		}
		// 如果是星期日则再加1天为下个星期一
		if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
			if (isAdd) {
				date = DateUtil.addDay(date, 1);
			} else {
				date = DateUtil.addDay(date, -2);
			}

		}
		return DateUtil.formatDate(date, "d MMM yyyy", Locale.ENGLISH);
	}

	/**
	 * @description 添加发送email详情
	 * @author hxzhang
	 * @create 2017年5月24日上午9:44:37
	 */
	public void createEmailMsg(UserInfo child, List<UserInfo> parentList, short type) {
		String strAll = "";
		for (UserInfo parent : parentList) {
			String email = parent.getEmail();
			if (StringUtil.isEmpty(email)) {
				continue;
			}
			String name = parent.getFirstName() + (StringUtil.isEmpty(parent.getMiddleName()) ? "" : " " + parent.getMiddleName()) + " " + parent.getLastName();
			String str = name + " ( <span style='color:blue' >" + email + "</span> ) , ";
			strAll += str;
		}
		strAll = strAll.trim();
		strAll = strAll.substring(0, strAll.length() - 1).trim();
		String dateStr = getDateStr();
		String msg = "";
		switch (type) {
		case 1:
			msg = "Waitlist Successfully Received email has been sent to " + strAll + " " + dateStr;
			break;
		case 2:
			msg = "Congratulations email sent to " + strAll + " " + dateStr;
			break;
		case 3:
			msg = "Confirmation of Enrolment email sent to " + strAll + " " + dateStr;
			break;
		case 4:
			msg = "Your absence has reached over 42 days Email sent to " + strAll + " " + dateStr;
			break;
		case 5:
			msg = "Request to give notice Approval email sent to " + strAll + " " + dateStr;
			break;
		case 6:
			msg = "Your Child's last day email sent to " + strAll + " " + dateStr;
			break;
		case 7:
			msg = "Welcome to KindiView email sent to " + strAll + " " + dateStr;
			break;
		default:
			break;
		}

		EmailMsg dbEmailMsg = emailMsgMapper.getByUserIdAndType(child.getId(), type);
		if (dbEmailMsg == null) {
			EmailMsg emailMsg = new EmailMsg();
			emailMsg.setId(UUID.randomUUID().toString());
			emailMsg.setUserId(child.getId());
			emailMsg.setMsg(msg);
			emailMsg.setType(type);
			emailMsg.setDeleteFlag(DeleteFlag.Default.getValue());
			emailMsgMapper.insertSelective(emailMsg);
		} else {
			dbEmailMsg.setMsg(msg);
			emailMsgMapper.updateByPrimaryKeySelective(dbEmailMsg);
		}

	}

	private String getFridayDateStr(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int i = cal.get(Calendar.DAY_OF_WEEK);
		switch (i) {
		case 1:
			date = DateUtil.addDay(date, 5);
			break;
		case 2:
			date = DateUtil.addDay(date, 4);
			break;
		case 3:
			date = DateUtil.addDay(date, 3);
			break;
		case 4:
			date = DateUtil.addDay(date, 2);
			break;
		case 5:
			date = DateUtil.addDay(date, 1);
			break;
		case 6:
			date = DateUtil.addDay(date, 0);
			break;
		case 7:
			date = DateUtil.addDay(date, -1);
			break;
		default:
			break;
		}
		SimpleDateFormat format = new SimpleDateFormat("d");
		String temp = format.format(date);
		String str = "";
		if (temp.endsWith("1") && !temp.endsWith("11")) {
			str = DateUtil.formatDate(date, "dd'st' MMM yyyy", java.util.Locale.ENGLISH);
		} else if (temp.endsWith("2") && !temp.endsWith("12")) {
			str = DateUtil.formatDate(date, "dd'nd' MMM yyyy", java.util.Locale.ENGLISH);
		} else if (temp.endsWith("3") && !temp.endsWith("13")) {
			str = DateUtil.formatDate(date, "dd'rd' MMM yyyy", java.util.Locale.ENGLISH);
		} else {
			str = DateUtil.formatDate(date, "dd'th' MMM yyyy", java.util.Locale.ENGLISH);
		}
		return str;
	}

	/**
	 * @description 获取时间字符串
	 * @author hxzhang
	 * @create 2017年5月23日上午10:33:54
	 */
	private String getDateStr() {
		SimpleDateFormat format = new SimpleDateFormat("d");
		Date now = new Date();
		String temp = format.format(now);
		String str = "";
		if (temp.endsWith("1") && !temp.endsWith("11")) {
			str = DateUtil.formatDate(now, "dd'st' MMM yyyy", java.util.Locale.ENGLISH);
		} else if (temp.endsWith("2") && !temp.endsWith("12")) {
			str = DateUtil.formatDate(now, "dd'nd' MMM yyyy", java.util.Locale.ENGLISH);
		} else if (temp.endsWith("3") && !temp.endsWith("13")) {
			str = DateUtil.formatDate(now, "dd'rd' MMM yyyy", java.util.Locale.ENGLISH);
		} else {
			str = DateUtil.formatDate(now, "dd'th' MMM yyyy", java.util.Locale.ENGLISH);
		}
		String str2 = DateUtil.formatDate(now, "hh:mm:ssa", java.util.Locale.ENGLISH);
		return str + ", " + str2;
	}

	private String getNewDaysOfAttendance(ChildAttendanceInfo att) {
		return getDaysOfAttendance(att.getNewMonday(), att.getNewTuesday(), att.getNewWednesday(), att.getNewThursday(), att.getNewFriday());
	}

	private String getDaysOfAttendance(ChildAttendanceInfo att) {
		return getDaysOfAttendance(att.getMonday(), att.getTuesday(), att.getWednesday(), att.getThursday(), att.getFriday());
	}

	/**
	 * @description 获取孩子的上课周期
	 * @author hxzhang
	 * @create 2017年1月4日上午11:56:27
	 */
	private String getDaysOfAttendance(Boolean... days) {
		String daysOfAttendance = "";
		List<String> weekCheckList = new ArrayList<String>();
		for (int i = 0; i < days.length; i++) {
			if (days[i]) {
				weekCheckList.add(TipMsgConstants.WEEKNAMES[i]);
			}
		}
		for (String str : weekCheckList) {
			daysOfAttendance += str + ", ";
		}
		if (StringUtil.isEmpty(daysOfAttendance)) {
			return " ";
		}
		return daysOfAttendance.substring(0, daysOfAttendance.length() - 2);
	}

	private String getName(String firstName, String middleName, String lastName) {
		if (StringUtil.isEmpty(middleName)) {
			return firstName + ' ' + lastName;
		}
		return firstName + ' ' + middleName + ' ' + lastName;
	}

	/**
	 * 
	 * @description 更新session
	 * @author gfwang
	 * @create 2016年10月28日下午1:24:47
	 * @version 1.0
	 * @param accountId
	 * @param request
	 */
	public void updateSession(HttpServletRequest request) {
		String accountId = getCurrentUser(request).getAccountInfo().getId();
		logger.info("updateSession|start|accountId=" + accountId);
		if (StringUtil.isEmpty(accountId)) {
			return;
		}
		UserCondition condition = new UserCondition();
		condition.setAccountId(accountId);
		List<UserInfoVo> userList = userInfoMapper.getUserList(condition);

		if (ListUtil.isEmpty(userList)) {
			logger.info("updateSession|end|accountId=" + accountId + ",listempty");
			return;
		}
		UserInfoVo user = userList.get(0);
		user.setRoleInfoVoList(userInfoMapper.getRoleByAccount(accountId));
		request.getSession().setAttribute(SessionConts.USERSESSION, userList.get(0));
		logger.info("updateSession|end|accountId=" + accountId);
	}

	/**
	 * @description 获取当前用户信息
	 * @author xdwang
	 * @create 2015年12月10日下午7:53:40
	 * @version 1.0
	 * @param request
	 *            HttpServletRequest对象
	 * @return 获取当前用户信息
	 */
	public UserInfoVo getCurrentUser(HttpServletRequest request) {
		Object object = request.getSession().getAttribute(SessionConts.USERSESSION);
		if (object != null) {
			return (UserInfoVo) object;
		} else {
			return null;
		}
	}

	public boolean validatePermiss(String accountId, UserInfoVo currtenUser, String centerId) {
		if (StringUtil.isEmpty(centerId)) {
			UserInfo user = userInfoMapper.getUserInfoByAccountId(accountId);
			if (user == null) {
				return true;
			}
			centerId = StringUtil.isEmpty(user.getCentersId()) ? "" : user.getCentersId();
		}
		boolean isCeo = containRoles(currtenUser.getRoleInfoVoList(), Role.CEO);
		String currtenCentreId = StringUtil.isEmpty(currtenUser.getUserInfo().getCentersId()) ? null : currtenUser.getUserInfo().getCentersId();
		if (!isCeo && !centerId.equals(currtenCentreId)) {
			return false;
		}
		return true;
	}

	/**
	 * 
	 * @description have roles
	 * @author gfwang
	 * @create 2016年9月8日下午1:59:16
	 * @version 1.0
	 * @param roleList
	 * @param roles
	 * @return
	 */
	private boolean containRoles(List<RoleInfoVo> roleList, Role... roles) {
		boolean result = false;
		for (RoleInfoVo allRole : roleList) {
			for (Role role : roles) {
				if (allRole.getValue() == role.getValue()) {
					result = true;
				}
			}
		}
		return result;
	}

	public String getUserName(UserInfo user) {
		return getUserName(user.getFirstName(), user.getMiddleName(), user.getLastName());
	}

	public String getUserName(String firstName, String middleName, String lastName) {
		firstName = StringUtil.isEmpty(firstName) ? "" : firstName + " ";
		middleName = StringUtil.isEmpty(middleName) ? "" : middleName + " ";
		lastName = StringUtil.isEmpty(lastName) ? "" : lastName;
		return firstName + middleName + lastName;
	}

	/**
	 * @description 给CEO,园长发送邮件.
	 * @author hxzhang
	 * @create 2017年6月6日上午10:49:06
	 */
	public void sendEmailCeoManager(String title, String content, String childAccountId, short emailTypeValue, Date startDate, Date lastDate, ChildAttendanceInfo att) {
		UserInfo userInfo = userInfoMapper.getUserInfoByAccountId(childAccountId);
		List<UserInfo> ceoManagers = userInfoMapper.getCEOAndMangersByCenterId(userInfo.getCentersId());
		UserInfo child = userInfoMapper.getUserInfoByAccountId(childAccountId);
		List<EmailVo> emails = new ArrayList<EmailVo>();
		EmailType emailType = EmailType.getType(emailTypeValue);
		for (UserInfo u : ceoManagers) {
			String sub = title;
			String msg = content;
			String email = u.getEmail();
			String userName = getName(u.getFirstName(), u.getMiddleName(), u.getLastName());
			String html = null;
			switch (emailType) {
			case request_attend: {
				msg = MessageFormat.format(content, getUserName(u), getUserName(child), getDaysOfAttendance(att), getNewDaysOfAttendance(att));
				html = EmailUtil.replaceHtml(sub, msg);
				sub = getSystemEmailMsg("request_attendance_sub");
			}
				break;
			case approve_attendance: {
				RoomInfo roomInfo = roomInfoMapper.selectByPrimaryKey(child.getRoomId());
				ChangeAttendanceRequestInfo re = changeAttendanceRequestInfoMapper.selectByPrimaryKey(att.getRequestId());
				String str = DateUtil.formatDate(re.getChangeDate(), "d MMM yyyy", Locale.ENGLISH);
				String state = re.getState() == 2 ? "Partially Approved" : "Approved";
				sub = MessageFormat.format(sub, state);
				msg = MessageFormat.format(content, getUserName(u), getUserName(child), getDaysOfAttendance(att), roomInfo.getName(), getNewDaysOfAttendance(att),
						roomInfo.getName(), str);
				html = EmailUtil.replaceHtml(sub, msg);

				sub = getSystemEmailMsg("approve_attendance_sub");
			}
				break;
			case cancel_attendance: {
				msg = MessageFormat.format(content, getUserName(u), getUserName(child), getDaysOfAttendance(att), getNewDaysOfAttendance(att));
				html = EmailUtil.replaceHtml(sub, msg);
				sub = getSystemEmailMsg("cancel_attendance_sub");
			}
				break;
			case request_givenotice: {
				CentersInfo centersInfo = centersInfoMapper.selectByPrimaryKey(child.getCentersId());
				msg = MessageFormat.format(content, userName, getUserName(child), centersInfo.getName(), DateUtil.formatDate(lastDate, "d MMM yyyy", Locale.ENGLISH));
				html = EmailUtil.replaceHtml(sub, msg);
				sub = getSystemEmailMsg("request_givenotice_sub");
			}
				break;
			case cancel_givenotice: {
				CentersInfo centersInfo = centersInfoMapper.selectByPrimaryKey(child.getCentersId());
				msg = MessageFormat.format(content, userName, getUserName(child), centersInfo.getName(), DateUtil.formatDate(lastDate, "d MMM yyyy", Locale.ENGLISH));
				html = EmailUtil.replaceHtml(sub, msg);
				sub = getSystemEmailMsg("cancel_givenotice_sub");
			}
				break;
			case request_absentee: {
				String beginDateStr = DateUtil.formatDate(startDate, "d MMM yyyy", Locale.ENGLISH);
				String endDateStr = DateUtil.formatDate(lastDate, "d MMM yyyy", Locale.ENGLISH);
				msg = MessageFormat.format(content, userName, getUserName(child), beginDateStr, endDateStr, dealDateInterval(child.getAccountId(), startDate, lastDate));
				html = EmailUtil.replaceHtml(sub, msg);
				sub = getSystemEmailMsg("request_absentee_sub");
			}
				break;
			case cancel_absentee: {
				String beginDateStr = DateUtil.formatDate(startDate, "d MMM yyyy", Locale.ENGLISH);
				String endDateStr = DateUtil.formatDate(lastDate, "d MMM yyyy", Locale.ENGLISH);
				msg = MessageFormat.format(content, userName, getUserName(child), beginDateStr, endDateStr);
				html = EmailUtil.replaceHtml(sub, msg);
				sub = getSystemEmailMsg("cancel_absentee_sub");
			}
				break;
			case lapsed_absentee: {
				String beginDateStr = DateUtil.formatDate(startDate, "d MMM yyyy", Locale.ENGLISH);
				String endDateStr = DateUtil.formatDate(lastDate, "d MMM yyyy", Locale.ENGLISH);
				msg = MessageFormat.format(content, userName, getUserName(child), beginDateStr, endDateStr);
				html = EmailUtil.replaceHtml(sub, msg);
				sub = getSystemEmailMsg("lapsed_absentee_sub");
			}
				break;
			default:
				break;
			}
			emails.add(new EmailVo(email, userName, sub, html, true));
		}
		emailService.batchSendEamil(emails);
	}

	public void sendSpecialEmail(EmailVo emailVo) {
		List<EmailVo> emails = new ArrayList<EmailVo>();
		String html = EmailUtil.replaceHtml(emailVo.getSub(), emailVo.getMsg());
		emailVo.setMsg(html);
		emails.add(emailVo);
		emailService.batchSendEamil(emails, emailVo.getBccs());
	}

	public void dealPlanEmail(String objId, List<EmailVo> emails, Short type, String userId) {
		for (EmailVo vo : emails) {
			EmailPlanInfo emailPlanInfo = new EmailPlanInfo();
			emailPlanInfo.setSendername(vo.getSendName());
			emailPlanInfo.setSenderaddress(vo.getSendAddress());
			emailPlanInfo.setReceivername(vo.getReceiverName());
			emailPlanInfo.setReceiveraddress(vo.getReceiverAddress());
			emailPlanInfo.setSub(vo.getSub());
			emailPlanInfo.setMsg(vo.getMsg());
			emailPlanInfo.setObjId(objId);
			emailPlanInfo.setType(type);
			emailPlanInfo.setUserId(userId);
			emailPlanInfo.setStatus((short) 0);
			emailPlanInfoMapper.insert(emailPlanInfo);
		}
	}

	public boolean havePlanEmail(String objId) {
		if (emailPlanInfoMapper.getCountByObjId(objId) != 0) {
			return true;
		}
		return false;
	}

	public void removePlanEmail(String objId) {
		emailPlanInfoMapper.remove(objId);
	}
}
