package com.aoyuntek.aoyun.factory;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aoyuntek.aoyun.condtion.UserDetailCondition;
import com.aoyuntek.aoyun.constants.Select2Constants;
import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.RoomInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.dao.program.ProgramIntobsleaInfoMapper;
import com.aoyuntek.aoyun.dao.program.TaskProgramFollowUpInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskFollowUpInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskFrequencyInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskResponsibleLogInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskVisibleInfoMapper;
import com.aoyuntek.aoyun.entity.po.CentersInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskVisibleInfo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.base.UserVo;
import com.aoyuntek.aoyun.entity.vo.task.PersonSelectVo;
import com.aoyuntek.aoyun.entity.vo.task.TaskInfoVo;
import com.aoyuntek.aoyun.enums.ArchivedStatus;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.enums.TaskVisibleType;
import com.aoyuntek.aoyun.enums.UserType;
import com.theone.string.util.StringUtil;

@Component
public class TaskStaffFactory extends TaskFactory {

	@Autowired
	private RoomInfoMapper roomInfoMapper;
	@Autowired
	private UserInfoMapper userInfoMapper;
	@Autowired
	private CentersInfoMapper centersInfoMapper;
	@Autowired
	private ProgramIntobsleaInfoMapper programIntobsleaInfoMapper;
	@Autowired
	private TaskProgramFollowUpInfoMapper taskProgramFollowUpInfoMapper;
	@Autowired
	private TaskVisibleInfoMapper taskVisibleInfoMapper;
	@Autowired
	private TaskInfoMapper taskInfoMapper;
	@Autowired
	private TaskResponsibleLogInfoMapper taskResponsibleLogInfoMapper;
	@Autowired
	private UserFactory userFactory;
	@Autowired
	private TaskFrequencyInfoMapper taskFrequencyInfoMapper;
	@Autowired
	private TaskFollowUpInfoMapper taskFollowUpInfoMapper;

	public List dealStaffOfImplementers(List<UserVo> userInfos, List<TaskVisibleInfo> taskVisibleInfos, String findText) {
		if (userInfos == null) {
			userInfos = new ArrayList<UserVo>();
		}
		List<PersonSelectVo> selecterPos = new ArrayList<PersonSelectVo>();

		PersonSelectVo ordinatorAllStaffOfCentre = new PersonSelectVo(TaskVisibleType.CENTER.getValue(), "0",
				Select2Constants.AllStaffInTheCentre);
		ordinatorAllStaffOfCentre
				.setSelected(isSelected(ordinatorAllStaffOfCentre.getId(), ordinatorAllStaffOfCentre.getType(), taskVisibleInfos));
		selecterPos.add(ordinatorAllStaffOfCentre);

		PersonSelectVo ordinator = new PersonSelectVo(TaskVisibleType.ROLE_GROUP.getValue(), "0", Select2Constants.CentreManagers);
		ordinator.setSelected(isSelected(ordinator.getId(), ordinator.getType(), taskVisibleInfos));
		selecterPos.add(ordinator);

		for (UserVo userInfo : userInfos) {
			if (StringUtil.isNotEmpty(findText) && !userInfo.getFullName().toLowerCase().contains(findText.toLowerCase())) {
				continue;
			}
			PersonSelectVo select = new PersonSelectVo(TaskVisibleType.USER.getValue(), userInfo.getAccountId(), userInfo.getFullName());
			// 是否选中
			select.setSelected(isSelected(select.getId(), select.getType(), taskVisibleInfos));
			selecterPos.add(select);
		}

		return filterSelect(selecterPos, findText);
	}

	public List dealStaffOfSelect(List<CentersInfo> list, List<TaskVisibleInfo> taskVisibleInfos, String findText) {
		if (list == null) {
			list = new ArrayList<CentersInfo>();
		}
		List<PersonSelectVo> selecterPos = new ArrayList<PersonSelectVo>();
		PersonSelectVo allCentres = new PersonSelectVo(TaskVisibleType.CENTER.getValue(), "0", Select2Constants.AllCentresStaff);
		allCentres.setSelected(isSelected(allCentres.getId(), allCentres.getType(), taskVisibleInfos));

		selecterPos.add(allCentres);
		for (CentersInfo centersInfo : list) {
			PersonSelectVo select = new PersonSelectVo(TaskVisibleType.CENTER.getValue(), centersInfo.getId(),
					MessageFormat.format(Select2Constants.AllXStaffs, centersInfo.getName()));
			// 是否选中
			select.setSelected(isSelected(select.getId(), select.getType(), taskVisibleInfos));
			selecterPos.add(select);
		}

		return filterSelect(selecterPos, findText);
	}

	public List deaStaffOfResponsible(List<UserVo> userInfos, List<TaskVisibleInfo> taskVisibleInfos, String findText) {
		if (userInfos == null) {
			userInfos = new ArrayList<UserVo>();
		}
		List<PersonSelectVo> selecterPos = new ArrayList<PersonSelectVo>();

		PersonSelectVo directors = new PersonSelectVo(TaskVisibleType.ROLE.getValue(), Role.CEO.getValue() + "",
				Select2Constants.Directors);
		directors.setSelected(isSelected(directors.getId(), directors.getType(), taskVisibleInfos));
		selecterPos.add(directors);

		PersonSelectVo ordinator = new PersonSelectVo(TaskVisibleType.ROLE_GROUP.getValue(), "0", Select2Constants.CentreManagers);
		ordinator.setSelected(isSelected(ordinator.getId(), ordinator.getType(), taskVisibleInfos));
		selecterPos.add(ordinator);

		for (UserVo userInfo : userInfos) {
			PersonSelectVo select = new PersonSelectVo(TaskVisibleType.USER.getValue(), userInfo.getAccountId(), userInfo.getFullName());
			// 是否选中
			select.setSelected(isSelected(select.getId(), select.getType(), taskVisibleInfos));
			selecterPos.add(select);
		}
		return filterSelect(selecterPos, findText);
	}

	private boolean isInStaff(List<TaskVisibleInfo> selectTaskVisibles, UserInfoVo currentUser) {
		for (TaskVisibleInfo taskVisibleInfo : selectTaskVisibles) {
			// all center staff
			if (taskVisibleInfo.getType() == TaskVisibleType.CENTER.getValue() && "0".equals(taskVisibleInfo.getObjId())) {
				return true;
			}
			// all center one staff
			if (StringUtil.isNotEmpty(currentUser.getUserInfo().getCentersId())
					&& taskVisibleInfo.getType() == TaskVisibleType.CENTER.getValue()
					&& currentUser.getUserInfo().getCentersId().equals(taskVisibleInfo.getObjId())) {
				return true;
			}
		}
		return false;
	}

	public boolean isInImplementersOfStaff(TaskInfoVo taskInfoVo, UserInfoVo currentUser) {
		for (TaskVisibleInfo items : taskInfoVo.getTaskImplementersVisibles()) {
			// All Staff in the centre
			if (items.getType() == TaskVisibleType.CENTER.getValue()) {
				if (isInStaff(taskInfoVo.getSelectTaskVisibles(), currentUser)) {
					return true;
				}
			}

			// Centre Managerss（默认）
			if (items.getType() == TaskVisibleType.ROLE_GROUP.getValue()) {
				boolean inRole = containRoles(currentUser.getRoleInfoVoList(), currentUser.getHistoryRoles(), Role.CentreManager,
						Role.EducatorSecondInCharge);
				if (inRole && isInStaff(taskInfoVo.getSelectTaskVisibles(), currentUser)) {
					return true;
				}
			}

			// Staff Name（可直接指定某员工）
			else if (items.getType() == TaskVisibleType.USER.getValue()) {
				if (items.getObjId().equals(currentUser.getAccountInfo().getId())) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean isInPersonLiableOfStaff(TaskInfoVo taskInfoVo, UserInfoVo currentUser) {
		for (TaskVisibleInfo items : taskInfoVo.getResponsibleTaskVisibles()) {
			// Directors
			if (items.getType() == TaskVisibleType.ROLE.getValue()) {
				boolean inRole = containRoles(currentUser.getRoleInfoVoList(), currentUser.getHistoryRoles(), Role.CEO);
				if (inRole) {
					return true;
				}
			}
			// Centre Managerss（默认）
			if (items.getType() == TaskVisibleType.ROLE_GROUP.getValue()) {
				boolean inRole = containRoles(currentUser.getRoleInfoVoList(), currentUser.getHistoryRoles(), Role.CentreManager,
						Role.EducatorSecondInCharge);
				if (inRole && isInStaff(taskInfoVo.getSelectTaskVisibles(), currentUser)) {
					return true;
				}
			}

			// 单独员工
			else if (items.getType() == TaskVisibleType.USER.getValue()) {
				if (items.getObjId().equals(currentUser.getAccountInfo().getId())) {
					return true;
				}
			}
		}
		return false;
	}

	public List<UserVo> getStaffOfSelectStaff(List<TaskVisibleInfo> selectTaskVisibles) {
		List<UserVo> result = new ArrayList<UserVo>();
		List<String> userIds = new ArrayList<String>();
		for (TaskVisibleInfo taskVisibleInfo : selectTaskVisibles) {
			// all center staff
			if (taskVisibleInfo.getType() == TaskVisibleType.CENTER.getValue() && "0".equals(taskVisibleInfo.getObjId())) {
				UserDetailCondition userCondtion = new UserDetailCondition();
				userCondtion.initAll();
				userCondtion.setUserType(UserType.Staff.getValue());
				userCondtion.setStatus(ArchivedStatus.UnArchived.getValue());
				List<UserVo> userInfos = userInfoMapper.getListOfUserVo(userCondtion);
				return userInfos;
			}
			// all center one staff
			if (taskVisibleInfo.getType() == TaskVisibleType.CENTER.getValue()) {
				UserDetailCondition userCondtion = new UserDetailCondition();
				userCondtion.initAll();
				userCondtion.setUserType(UserType.Staff.getValue());
				userCondtion.setStatus(ArchivedStatus.UnArchived.getValue());
				List<String> centreIds = new ArrayList<String>();
				centreIds.add(taskVisibleInfo.getObjId());
				userCondtion.setCentreIds(centreIds);
				List<UserVo> userInfos = userInfoMapper.getListOfUserVo(userCondtion);
				for (UserVo userVo : userInfos) {
					if (!userIds.contains(userVo.getAccountId())) {
						result.add(userVo);
						userIds.add(userVo.getAccountId());
					}
				}
			}
		}
		return result;
	}

	public List<String> getSelectCentreIds(List<PersonSelectVo> selectList) {
		if (selectList == null) {
			selectList = new ArrayList<PersonSelectVo>();
		}
		List<String> array = new ArrayList<String>();
		for (PersonSelectVo personSelectVo : selectList) {
			if ((personSelectVo.getType() == TaskVisibleType.CENTER.getValue()) && (!"0".equals(personSelectVo.getId()))) {
				array.add(personSelectVo.getId());
			}
		}
		return array;
	}

	public boolean haveOnlySelectOneCenter(List<PersonSelectVo> selectList) {
		if (selectList == null) {
			selectList = new ArrayList<PersonSelectVo>();
		}
		if (selectList.size() == 1) {
			if ((selectList.get(0).getType() == TaskVisibleType.CENTER.getValue()) && (!"0".equals(selectList.get(0).getId()))) {
				return true;
			}
		}
		return false;
	}
}
