package com.aoyuntek.aoyun.dao;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.oldIdSignature;

public interface oldIdSignatureMapper {
    int deleteByPrimaryKey(String id);

    int insert(oldIdSignature record);

    int insertSelective(oldIdSignature record);

    oldIdSignature selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(oldIdSignature record);

    int updateByPrimaryKeyWithBLOBs(oldIdSignature record);

    int updateByPrimaryKey(oldIdSignature record);

    List<oldIdSignature> getList();
}