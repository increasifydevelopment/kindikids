package com.aoyuntek.aoyun.dao;

import com.aoyuntek.aoyun.entity.po.ChildBackgroundInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface ChildBackgroundInfoMapper extends BaseMapper<ChildBackgroundInfo> {
    ChildBackgroundInfo getChildBackgroundInfoByUserId(String userId);
}