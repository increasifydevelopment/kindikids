package com.aoyuntek.aoyun.dao;

import com.aoyuntek.aoyun.entity.po.ClosurePeriodCentres;
import com.aoyuntek.framework.dao.BaseMapper;

public interface ClosurePeriodCentresMapper extends BaseMapper<ClosurePeriodCentres> {

	void removeById(String id);

	void removeClosurePeriodCentres(String id);
}