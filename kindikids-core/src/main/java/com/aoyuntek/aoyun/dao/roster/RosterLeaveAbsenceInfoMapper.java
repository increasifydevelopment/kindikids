package com.aoyuntek.aoyun.dao.roster;

import com.aoyuntek.aoyun.entity.po.roster.RosterLeaveAbsenceInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface RosterLeaveAbsenceInfoMapper extends BaseMapper<RosterLeaveAbsenceInfo> {

    RosterLeaveAbsenceInfo getLeaveInfoByAccountId(String accountId);
    void  deleteRosterLeave(String id);

    
}