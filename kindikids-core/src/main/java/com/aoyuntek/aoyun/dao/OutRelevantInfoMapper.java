package com.aoyuntek.aoyun.dao;

import com.aoyuntek.aoyun.entity.po.OutRelevantInfo;

public interface OutRelevantInfoMapper {
	int deleteByPrimaryKey(String id);

	int insert(OutRelevantInfo record);

	int insertSelective(OutRelevantInfo record);

	OutRelevantInfo selectByPrimaryKey(String id);

	int updateByPrimaryKeySelective(OutRelevantInfo record);

	int updateByPrimaryKey(OutRelevantInfo record);

	OutRelevantInfo getInfo(String id);
}