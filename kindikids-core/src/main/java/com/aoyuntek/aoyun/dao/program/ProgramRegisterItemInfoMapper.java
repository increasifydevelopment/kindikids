package com.aoyuntek.aoyun.dao.program;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.program.ProgramRegisterItemInfo;
import com.aoyuntek.aoyun.entity.vo.ProgramRegisterItemInfoVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface ProgramRegisterItemInfoMapper extends BaseMapper<ProgramRegisterItemInfo> {

    /**
     * 
     * @description 批量插入ProgramRegisterItemInfo
     * @author mingwang
     * @create 2016年9月22日下午9:18:52
     * @version 1.0
     * @param registerItems
     */
    void batchInsertRegisterItems(List<ProgramRegisterItemInfo> registerItems);

    /**
     * 
     * @description 批量更新ProgramRegisterItemInfo
     * @author mingwang
     * @create 2016年9月22日下午10:18:26
     * @version 1.0
     * @param registerItems
     */
    void batchUpdateRegisterItems(List<ProgramRegisterItemInfo> registerItems);

    /**
     * 
     * @description 获取ProgramRegisterTaskInfo
     * @author mingwang
     * @create 2016年9月23日下午2:08:15
     * @version 1.0
     * @param id
     * @return
     */
    List<ProgramRegisterItemInfoVo> getRegisterItems(String id);

    /**
     * 
     * @description 通过taskId从数据库获取ProgramRegisterItemInfo集合
     * @author mingwang
     * @create 2016年9月23日下午4:40:38
     * @version 1.0
     * @param taskId
     * @return
     */
    List<ProgramRegisterItemInfo> getRegisterItemsByTaskId(String taskId);

    /**
     * 
     * @description 更新小孩的newsfeedId
     * @author gfwang
     * @create 2016年9月25日下午10:08:35
     * @version 1.0
     * @param id
     * @return
     */
    int updateNewsFeedId(@Param("id") String id, @Param("newsFeedId") String newsFeedId);

    /**
     * 
     * @description 通过taskId获取childid
     * @author mingwang
     * @create 2016年9月29日下午3:29:52
     * @version 1.0
     * @param taskId
     * @return
     */
    List<String> getChildIdByTaskId(String taskId);

    /**
     * 
     * @description 批量删除RegisterItems(修改delete_flag状态)
     * @author mingwang
     * @create 2016年10月10日下午4:04:31
     * @version 1.0
     * @param registerItems_remove
     */
    int batchDeleteById(List<String> registerItems_remove);

    /**
     * 通过item的id获取task的id
     */
    String getTaskIdByItemId(String id);

    int updateSleepNewsFeedId(@Param("childId") String childId, @Param("newsfeedId") String newsfeedId);

    void updateItemTime(ProgramRegisterItemInfo registerItem);

    /**
     * 
     * @description 通过taskId删除items
     * @author mingwang
     * @create 2017年2月4日上午11:43:18
     * @version 1.0
     * @param taskId
     */
    void updateRemoveByTaskId(String taskId);

}