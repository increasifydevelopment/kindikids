package com.aoyuntek.aoyun.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.condtion.MessageCondition;
import com.aoyuntek.aoyun.entity.po.MessageInfo;
import com.aoyuntek.aoyun.entity.po.MessageLookInfo;
import com.aoyuntek.aoyun.entity.vo.MessageInfoVo;
import com.aoyuntek.aoyun.entity.vo.MessageListVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface MessageInfoMapper extends BaseMapper<MessageInfo> {
    /**
     * 
     * @description 获取收件箱信息
     * @author bbq
     * @create 2016年6月27日上午11:49:26
     * @version 1.0
     * @param condition
     *            站内信查询条件
     * @return
     */
    List<MessageListVo> getPagerList(MessageCondition condition);

    /**
     * 
     * @description 删除收件箱MSG
     * @author bbq
     * @create 2016年6月27日下午1:52:08
     * @version 1.0
     * @param msgId
     *            信息Id
     * @return 影响行数
     */
    int updateInboxByMsgId(String msgId);

    /**
     * 
     * @description 删除发件箱MSG
     * @author bbq
     * @create 2016年6月27日上午11:49:42
     * @version 1.0
     * @param msgId
     *            信息Id
     * @return int 影响行数
     */
    int updateOutboxByMsgId(String msgId);

    /**
     * 
     * @description 获取未读消息数量
     * @author bbq
     * @create 2016年6月27日下午2:35:18s
     * @version 1.0
     * @param accountId
     *            当前用户id
     * @return 未读消息数量
     */
    int getUnreadCount(String accountId);

    /**
     * 
     * @description 获取分页总记录
     * @author bbq
     * @create 2016年6月30日下午2:30:55
     * @version 1.0
     * @param condition
     *            站内信查询条件
     * @return 总记录
     */
    int getPagerListCount(MessageCondition condition);

    /**
     * @description 通过messageId获取messageVo
     * @author hxzhang
     * @create 2016年6月30日下午2:39:23
     * @version 1.0
     * @param messageId
     *            messageId
     * @return 返回结果
     */
    List<MessageInfoVo> getMessageInfoAndNqsInfos(@Param("messageId") String messageId, @Param("accountId") String accountId,@Param("msgSenderId") String msgSenderId);
    
    /**
     * 根据老id获取新Id
     * @param oldId
     * @return
     */
    
    String getIdByOldId(@Param("oldId") String oldId);
    /**
     * 批量增加
     */
   void insertBatch(List<MessageInfo> messageInfos);
   
   List<String> getAccountIdsByParentmsgId(String msgId);
   
   String getAccountIdBymsgId(String msgId);
   
}