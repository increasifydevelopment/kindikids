package com.aoyuntek.aoyun.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.BackgroundPetsInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface BackgroundPetsInfoMapper extends BaseMapper<BackgroundPetsInfo> {
    /**
     * @description 批量插入BackgroundPetsInfo
     * @author hxzhang
     * @create 2016年7月28日下午7:10:20
     * @version 1.0
     * @param petList
     *            List<BackgroundPetsInfo>
     */
    void insterBatchBackgroundPetsInfo(@Param("petList") List<BackgroundPetsInfo> petList);

    /**
     * @description 通过backgroundId删除BackgroundPetsInfo记录
     * @author hxzhang
     * @create 2016年7月29日下午1:50:08
     * @version 1.0
     * @param backgroundId
     *            backgroundId
     */
    int removeBackgroundPetsInfoByBackgroundId(String backgroundId);

    List<BackgroundPetsInfo> getBackgroundPetsInfoByBackgroundId(String backgroundId);
}