package com.aoyuntek.aoyun.dao.impl;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.aoyuntek.aoyun.dao.IVisitLogDao;
import com.aoyuntek.aoyun.entity.po.VisitLogInfo;

/**
 * @description 日志数据访问层实现
 * @author xdwang
 * @create 2015年12月10日下午7:59:44
 * @version 1.0
 */
@Repository
public class VisitLogDao implements IVisitLogDao {

    /**
     * JdbcTemplate实例
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;

    // CSOFF: MagicNumber
    @Override
    public void saveBacth(List<VisitLogInfo> logInfos) {
        final List<VisitLogInfo> tempLogInfos = logInfos;
        String sql = " insert into t_visitlog (ip,user_id,user_name,url,visit_time,create_time,creater_id,creater_name,is_delete)" + " values (?,?,?,?,?,?,?,?,?) ";
        jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {

            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                VisitLogInfo item = tempLogInfos.get(i);
                ps.setString(1, item.getIp());
                ps.setString(2, item.getUserId());
                ps.setString(3, item.getUserName());
                ps.setString(4, item.getUrl());
                ps.setString(5, item.getDate());
                ps.setDate(6, new Date(new java.util.Date().getTime()));
                ps.setString(7, "");
                ps.setString(8, "system");
                ps.setBoolean(9, false);
            }

            @Override
            public int getBatchSize() {
                return tempLogInfos.size();
            }
        });
    }
}
