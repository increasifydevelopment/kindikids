package com.aoyuntek.aoyun.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.NqsInfo;
import com.aoyuntek.aoyun.entity.vo.NqsVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface NqsInfoMapper extends BaseMapper<NqsInfo> {
    /**
     * @description 获取有效的NQS集合
     * @author hxzhang
     * @create 2016年7月5日下午2:22:17
     * @version 1.0
     * @param nqsVersions
     *            需要验证的NQS集合
     * @return 返回NQS集合
     */
    List<String> getDbNqs(@Param("nqsVoList") List<NqsVo> nqsVoList);

    NqsVo geNqsVo(@Param("version") String version,@Param("tag") Short tag);

    List<NqsVo> queryChildNqsVo(@Param("version")String version,@Param("tag") Short tag);

    List<String> getRootNqs(@Param("tag") Short tag);

    List<NqsInfo> getNqsInfoListByNewsId(String newsId);
    
    List<NqsVo> getNqsVoListByObjId(@Param("objId")String objId);

}