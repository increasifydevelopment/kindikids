package com.aoyuntek.aoyun.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.AttachmentInfo;
import com.aoyuntek.aoyun.entity.vo.FileVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface AttachmentInfoMapper extends BaseMapper<AttachmentInfo> {
    /**
     * @description 批量添加附件
     * @author hxzhang
     * @create 2016年7月11日下午4:30:03
     * @version 1.0
     * @param attachList
     *            附件对象集合
     */
    void insterBatchAttachmentInfo(@Param("attachList") List<AttachmentInfo> attachList);

    /**
     * @description 根据sourceId获取AttachmentInfo
     * @author hxzhang
     * @create 2016年7月13日下午9:37:53
     * @version 1.0
     * @param sourceId
     *            附件源ID
     * @return 返回List<AttachmentInfo>
     */
    List<AttachmentInfo> getAttachmentInfoBySourceId(String sourceId);

    /**
     * @description 批量删除附件
     * @author hxzhang
     * @create 2016年7月14日下午4:52:58
     * @version 1.0
     * @param unDelList
     *            附件集合
     * @param sourceId
     *            附件源ID
     */
    void deleteBatchAttachmentInfo(@Param("unDelList") List<FileVo> unDelList, @Param("sourceId") String sourceId);
    
    /**
     * 
     * @description sourceId获取FileVoList
     * @author gfwang
     * @create 2016年8月3日下午8:36:50
     * @version 1.0
     * @param sourceId
     * @return
     */
    List<FileVo> getFileListVo(@Param("sourceId") String sourceId);

    /**
     * 
     * @description sourceId获取FileVoList
     * @author gfwang
     * @create 2016年8月3日下午8:36:50
     * @version 1.0
     * @param sourceId
     * @return
     */
    List<FileVo> getNotTempFileListVo(@Param("sourceId") String sourceId);
    
    /**
     * 
     * @description sourceId删除
     * @author gfwang
     * @create 2016年8月24日下午1:27:20
     * @version 1.0
     * @param sourceId
     *            sourceId
     * @return
     */
    int updateDeleteBySourceId(@Param("sourceId") String sourceId);

    /**
     * 
     * @description 虚拟删除文件
     * @author gfwang
     * @create 2016年9月25日下午1:48:22
     * @version 1.0
     * @param attachId
     * @return
     */
    int deleteFileByList(@Param("list") List<String> list);
    
    /**
     * 
     * @description 虚拟删除文件
     * @author gfwang
     * @create 2016年9月25日下午1:48:22
     * @version 1.0
     * @param id
     * @return
     */
    int deleteFileByIds(@Param("list") List<String> list);
    /**
     * 批量增加文件
     */
    void insertBatch(List<AttachmentInfo> attachmentInfoList);
    int updateAttachIdById(@Param("attachId") String attachId,@Param("id") String id);
    List<AttachmentInfo> selectAll(@Param("keyName")String keyName);

    /**
     * 
     * @description 
     * @author mingwang
     * @create 2017年1月21日下午3:03:47
     * @param id
     */
    void updateDeleteById(String id);

    List<AttachmentInfo> selectCenter();
    
}