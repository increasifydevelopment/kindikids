package com.aoyuntek.aoyun.dao.roster;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.condtion.PayrollCondition;
import com.aoyuntek.aoyun.entity.po.roster.RosterStaffInfo;
import com.aoyuntek.aoyun.entity.vo.PayrollVo;
import com.aoyuntek.aoyun.entity.vo.roster.RosterStaffVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface RosterStaffInfoMapper extends BaseMapper<RosterStaffInfo> {

	List<PayrollVo> getListForPayroll(PayrollCondition condition);

	List<RosterStaffInfo> getListByCondition(@Param("accountId") String accountId, @Param("centreId") String centreId, @Param("region") Short region);

	/**
	 * 
	 * @author dlli5 at 2016年12月23日下午4:21:47
	 * @param id
	 * @param day
	 * @return
	 */
	int haveStaff(@Param("id") String id, @Param("day") Date day);

	/**
	 * 
	 * @author dlli5 at 2016年12月23日下午4:21:50
	 * @param rosterId
	 * @return
	 */
	List<RosterStaffVo> getByRosterId(@Param("rosterId") String rosterId);

	/**
	 * 
	 * @author dlli5 at 2016年12月23日下午4:21:54
	 * @param rosterId
	 * @param day
	 * @return
	 */
	List<RosterStaffVo> getByRosterIdAndDay(@Param("rosterId") String rosterId, @Param("day") Date day);

	List<RosterStaffVo> getShiftListByRosterIdAndDay(@Param("rosterId") String rosterId, @Param("day") Date day);

	/**
	 * 
	 * @author dlli5 at 2016年12月23日下午4:21:58
	 * @param rosterId
	 * @param shiftTimeCodes
	 * @param day
	 * @return
	 */
	List<RosterStaffVo> getShiftTimeStaff(@Param("rosterId") String rosterId, @Param("shiftTimeCodes") List<String> shiftTimeCodes, @Param("day") Date day);

	/**
	 * 
	 * @author dlli5 at 2016年12月23日下午4:22:01
	 * @param id
	 * @return
	 */
	RosterStaffVo getVo(@Param("id") String id);

	/**
	 * 
	 * @author dlli5 at 2016年12月23日下午4:22:04
	 * @param accountId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	int getCountByAccountIdAndDate(@Param("accountId") String accountId, @Param("startDate") Date startDate, @Param("endDate") Date endDate);

	/**
	 * 
	 * @author dlli5 at 2016年12月23日下午4:22:07
	 * @param id
	 * @return
	 */
	int getCountByAccountIdFutureDate(String id);

	int havaRoster(@Param("accountId") String accountId, @Param("day") Date day);

	List<RosterStaffInfo> getListByAccountIdFutureDate(String id);

	List<RosterStaffInfo> getListByCentreIdDate(@Param("centres") List<String> centres, @Param("startDate") Date startDate, @Param("endDate") Date endDate);

	/**
	 * 
	 * @author dlli5 at 2016年12月23日下午4:22:10
	 * @param rosterId
	 * @param code
	 * @param day
	 * @return
	 */
	int haveStaffWithDay(@Param("rosterId") String rosterId, @Param("shiftTimeCode") String code, @Param("day") Date day);

	/**
	 * 
	 * @author dlli5 at 2016年12月23日下午4:22:12
	 * @param accountId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	int removRosterStaffInfo(@Param("accountId") String accountId, @Param("startDate") Date startDate, @Param("endDate") Date endDate);

	/**
	 * 
	 * @author dlli5 at 2016年12月23日下午4:22:17
	 * @param deleteShiftTimeCode
	 * @param shiftId
	 */
	void deleteFutureRosterStaff(@Param("shiftTimeCodes") List<String> deleteShiftTimeCode, @Param("shiftId") String shiftId);

	/**
	 * 
	 * @author dlli5 at 2016年12月23日下午4:22:21
	 * @param centreId
	 * @param accountId
	 * @param day
	 * @return
	 */
	int getStaffAttendance(@Param("centreId") String centreId, @Param("accountId") String accountId, @Param("day") Date day);

	/**
	 * 
	 * @author dlli5 at 2016年12月23日下午4:22:25
	 * @param accountId
	 * @param day
	 * @return
	 */
	RosterStaffInfo getStaffAttendanceInfo(@Param("accountId") String accountId, @Param("day") Date day);

	/**
	 * 
	 * @author dlli5 at 2016年12月23日下午4:22:28
	 * @param weekBegin
	 * @param code
	 */
	void deleteByShiftTimeCode(@Param("todayWeekStart") Date weekBegin, @Param("code") String code);

	/**
	 * 
	 * @author dlli5 at 2016年12月23日下午4:22:36
	 * @param rosterId
	 * @param id
	 * @param day
	 * @return
	 */
	RosterStaffInfo getFristOfRosterTime(@Param("rosterId") String rosterId, @Param("shiftTimeCode") String id, @Param("day") Date day);

	/**
	 * 
	 * @author dlli5 at 2016年12月23日下午4:22:39
	 * @param rosterId
	 * @param weekStart
	 * @param educatorect
	 * @return
	 */
	int getRosterStaffCountWithRole(@Param("rosterId") String rosterId, @Param("day") Date day, @Param("roleVal") short educatorect);

	/**
	 * 
	 * @author dlli5 at 2016年12月27日下午8:06:19
	 * @param staffAccountId
	 * @param day
	 * @return
	 */
	List<RosterStaffVo> getByStaffIdAndDay(@Param("staffAccountId") String staffAccountId, @Param("day") Date day);
}