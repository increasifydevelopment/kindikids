package com.aoyuntek.aoyun.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.RoleInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface RoleInfoMapper extends BaseMapper<RoleInfo> {

	List<RoleInfo> firstRolesByAccountId(String accountId);

	void removeByAccountId(String accountId);

	List<RoleInfo> getRoleListByAccountId(String accountId);

	/**
	 * @description 根据角色值获取RoleId
	 * @author hxzhang
	 * @create 2016年7月29日下午5:57:46
	 * @version 1.0
	 * @param value
	 *            角色值
	 * @return RoleId
	 */
	String selectRoleIdByValue(short value);

	/**
	 * @description
	 * @author hxzhang
	 * @create 2016年8月5日下午3:06:55
	 * @version 1.0
	 * @return
	 */
	List<RoleInfo> getAllRoleInfo();

	/**
	 * 
	 * @author dlli5 at 2016年11月15日上午9:29:56
	 * @param accountId
	 * @param temp
	 * @return
	 */
	List<RoleInfo> getRoleInfoByAccountId(@Param("accountId") String accountId, @Param("temp") Boolean temp);

	/**
	 * 
	 * @description 通过id获取RoleType
	 * @author mingwang
	 * @create 2016年11月30日下午2:09:23
	 * @version 1.0
	 * @param id
	 * @return
	 */
	int getRoleTypeById(String id);

	/**
	 * 
	 * @description 通过id获取roleName
	 * @author mingwang
	 * @create 2016年11月30日下午2:28:14
	 * @version 1.0
	 * @param oldValue
	 * @return
	 */
	String getNameById(String id);
}