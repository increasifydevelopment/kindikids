package com.aoyuntek.aoyun.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.NewsAccountsInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface NewsAccountsInfoMapper extends BaseMapper<NewsAccountsInfo> {

	void updateUserNameByAccountId(@Param("accountId") String accountId, @Param("name") String name);

	/**
	 * 通过AccountId获取用户名字
	 * 
	 * @author hxzhang 2018年10月24日
	 * @param accountId
	 * @return
	 */
	String getUserNameByAccountId(String accountId);

	/**
	 * @description 批量添加NewsAccountsInfo
	 * @author hxzhang
	 * @create 2016年7月12日上午9:54:37
	 * @version 1.0
	 * @param newsAccountsInfos
	 *            List<NewsAccountsInfo>
	 */
	void insterBatchNewsAccountsInfo(@Param("newsAccountsInfos") List<NewsAccountsInfo> newsAccountsInfos);

	/**
	 * @description 根据newsId更新NewsAccountsInfo
	 * @author hxzhang
	 * @create 2016年7月12日下午2:11:30
	 * @version 1.0
	 * @param newsId
	 *            微博ID
	 */
	int updateNewsAccountsInfoByNewsId(String newsId);

	/**
	 * @description 删除微博Tag的小孩
	 * @author Hxzhang
	 * @create 2016年9月28日下午6:45:02
	 */
	int removeNewsAccountsInfoByNewsId(String newsId);
}