package com.aoyuntek.aoyun.dao.roster;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.roster.RosterShiftInfo;
import com.aoyuntek.aoyun.entity.vo.roster.RosterShiftVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface RosterShiftInfoMapper extends BaseMapper<RosterShiftInfo> {

    RosterShiftVo selectVoByCentreId(String centreId);

    RosterShiftVo selectVoById(String id);

    List<String> getWillImageShiftIds(@Param("taskId") String taskId, @Param("todayWeekStart") Date todayWeekStart);

    void updateOtherCurrentFlag(@Param("centerId")String centerId, @Param("id")String id);

    List<String> getUseCentreIds(@Param("shiftId")String shiftId,@Param("currentFlag")Boolean currentFlag);
}