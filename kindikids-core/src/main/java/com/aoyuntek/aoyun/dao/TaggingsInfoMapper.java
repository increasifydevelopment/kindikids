package com.aoyuntek.aoyun.dao;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.TaggingsInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface TaggingsInfoMapper extends BaseMapper<TaggingsInfo> {

	List<TaggingsInfo> getListByEmployId(String employId);
}