package com.aoyuntek.aoyun.dao.event;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.event.EventChildInfo;

public interface EventChildInfoMapper {
    int insert(EventChildInfo record);

    int insertSelective(EventChildInfo record);
    
    /**
     * 批量插入附件主表与小孩关系
     * @param eventDocChildList
     * @return
     */
    int insterBatchEventDocChildInfo(@Param("eventChildList") List<EventChildInfo> eventChildInfo);
    
    /**
     * 获取EventChilds
     * @param docId
     * @return
     */
    List<EventChildInfo> getListByDocId(String docId);
    
    /**
     * 批量虚拟删除文档与tag小孩的关系
     * @param eventChildInfo
     * @return
     */
    int deleteDocByEventChildInfos(@Param("eventChildList") List<EventChildInfo> eventChildInfo);
    
    /**
     * 
     * @description 虚拟删除文件
     * @author abliu
     * @create 2016年9月25日下午1:48:22
     * @version 1.0
     * @param id
     * @return
     */
    int deleteDocChildsByIds(@Param("list") List<String> list);
}