package com.aoyuntek.aoyun.dao;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.condtion.StaffCondition;
import com.aoyuntek.aoyun.condtion.UserBySelectCondition;
import com.aoyuntek.aoyun.condtion.UserCondition;
import com.aoyuntek.aoyun.condtion.UserDetailCondition;
import com.aoyuntek.aoyun.entity.po.AccountInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.vo.LeaveListVo;
import com.aoyuntek.aoyun.entity.vo.RoleInfoVo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.aoyun.entity.vo.UserDetail;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.base.UserVo;
import com.aoyuntek.framework.condtion.BaseCondition;
import com.aoyuntek.framework.dao.BaseMapper;

public interface UserInfoMapper extends BaseMapper<UserInfo> {

	int getCountByIdCrn(@Param("id") String id, @Param("crn") String crn);

	List<String> getFullPartStaff(String centreId);

	UserInfo getChildByHubworksId(String hubworksId);

	void syncHubworksIdByCrn(@Param("id") String id, @Param("crn") String crn);

	List<SelecterPo> getSelecterPoByAccountIds(@Param("accountIds") List<String> accountIds);

	List<UserInfo> getChildListByFamilyId(String id);

	List<UserInfo> getChildListForNotes(@Param("name") String name, @Param("birthday") Date birthday);

	void removeUserById(String id);

	void deleteUserById(String id);

	List<UserVo> getResponsibleUsers(@Param("taskInstanceId") String taskInstanceId);

	UserInfo getUserInfoByEmail(String email);

	String getFullNameByAccountId(String accountId);

	void saveSignature(@Param("id") String id, @Param("oldId") String oldId, @Param("signature") String signature);

	int haveRelation(String userId);

	void setIdAndOldId(@Param("id") String id, @Param("oldId") String oldId);

	List<UserInfo> getImportChild();

	String[] getCeoEmails();

	int updateStatusByUserId(@Param("status") short status, @Param("userId") String userId);

	List<UserInfo> getUserByCondition(UserCondition condition);

	List<UserInfo> getChildByNameFamilyId(@Param("firstName") String firstName, @Param("lastName") String lastName, @Param("familyId") String familyId);

	void updateOutUserTime(@Param("userId") String userId, @Param("time") Date time, @Param("oldId") String oldId);

	UserInfo getOutChildByName(@Param("firstName") String firstName, @Param("lastName") String lastName);

	List<UserInfo> getCenterMangersByCenterId(String centerId);

	List<UserInfo> getCEOAndMangersByCenterId(String centerId);

	List<UserInfo> getAllCeos();

	List<SelecterPo> getMapId();

	UserInfo getUserInfoByOldId(String oldId);

	/**
	 * 
	 * @author dlli5 at 2016年9月21日下午3:23:23
	 * @param condition
	 * @return
	 */
	public List<UserVo> getListOfUserVo(BaseCondition condition);

	/**
	 * 
	 * @description 获取用户信息
	 * @author bbq
	 * @create 2016年6月16日下午3:42:32
	 * @version 1.0
	 * @param condition
	 * @return List<UserInfoVO>集合
	 */
	public List<UserInfoVo> getUserList(BaseCondition condition);

	/**
	 * @description 通过accountInfos获取对应的List<UserInfo>
	 * @author hxzhang
	 * @create 2016年6月30日下午4:33:50
	 * @version 1.0
	 * @param accountInfos
	 *            List<AccountInfo>
	 * @return 返回结果
	 */
	List<UserInfo> getUserInfosByAccountInfos(List<AccountInfo> accountInfos);

	/**
	 * 
	 * @description 获取下拉框人
	 * @author gfwang
	 * @create 2016年7月11日下午9:41:48
	 * @version 1.0
	 * @param condition
	 *            条件
	 * @return 结果
	 */
	Set<SelecterPo> getReceiveUser(UserBySelectCondition condition);

	/**
	 * 
	 * @description 获取当前园区下小孩对应的家长(用于下拉选择)
	 * @author gfwang
	 * @create 2016年7月20日下午4:26:15
	 * @version 1.0
	 * @param centerId
	 * @return
	 */
	List<SelecterPo> getParentByKidCenter(@Param("centerId") String centerId, @Param("name") String name);

	/**
	 * 
	 * @description 获取当前 家长所有小孩的所属的园区集合
	 * @author gfwang
	 * @create 2016年7月21日上午9:28:15
	 * @version 1.0
	 * @param parentAccountId
	 *            家长id
	 * @return 园区集合
	 */
	List<String> getCenterByMyKid(String parentAccountId);

	/**
	 * 
	 * @description 园区家长id
	 * @author gfwang
	 * @create 2016年7月22日下午2:17:10
	 * @version 1.0
	 * @param parentAccountId
	 *            家长
	 */
	List<SelecterPo> getCenterMangersByParent(@Param("parentAccountId") String parentAccountId, @Param("name") String name);

	/**
	 * @description 通过familyId获取小孩子的信息
	 * @author hxzhang
	 * @create 2016年8月1日上午11:10:10
	 * @version 1.0
	 * @param familyId
	 *            FamilyID
	 * @return 返回查询结果
	 */
	List<UserInfo> getChildInfoByFamilyId(String familyId);

	List<UserInfo> getChilds(String familyId);

	/**
	 * @description 通过familyId获取家长信息
	 * @author hxzhang
	 * @create 2016年8月1日上午11:13:22
	 * @version 1.0
	 * @param familyId
	 *            FamilyID
	 * @return 返回查询结果
	 */
	List<UserInfo> getParentInfoByFamilyId(String familyId);

	List<UserInfo> getParents(String familyId);

	/**
	 * 
	 * @description 根据条件查询
	 * @author gfwang
	 * @create 2016年8月1日下午2:55:20
	 * @version 1.0
	 * @param condition
	 * @return
	 */
	List<UserInfo> getByCondition(UserDetailCondition condition);

	/**
	 * @description 通过小孩ID删除该小孩
	 * @author hxzhang
	 * @create 2016年8月1日下午5:43:28
	 * @version 1.0
	 * @param userId
	 *            小孩ID
	 * @return 返回操作结果
	 */
	int deleteChild(String accountId);

	/**
	 * @description 通过家长ID删除该家长
	 * @author hxzhang
	 * @create 2016年8月1日下午5:58:00
	 * @version 1.0
	 * @param userId
	 *            家长ID
	 * @return 返回操作结果
	 */
	int deleteParent(String userId);

	/**
	 * @description 通过用户ID获取该用户所在家庭的小孩子或家长数量
	 * @author hxzhang
	 * @create 2016年8月3日上午9:11:20
	 * @version 1.0
	 * @param userId
	 *            用户ID
	 * @param userType
	 *            获取的用户类型
	 * @return 返回查询结果
	 */
	int getUserNumByUserId(@Param("userId") String userId, @Param("userType") short userType);

	/**
	 * @description 通过familyId获取用户信息
	 * @author hxzhang
	 * @create 2016年8月4日下午2:29:55
	 * @version 1.0
	 * @param familyId
	 *            familyId
	 * @param userType
	 *            userType
	 * @return List<UserInfoVo>
	 */
	List<UserInfoVo> getUserByFamilyId(@Param("familyId") String familyId, @Param("userType") short userType);

	/**
	 * @description 通过familyId获取该家庭所有人员
	 * @author hxzhang
	 * @create 2016年8月10日下午3:43:46
	 * @version 1.0
	 * @param familyId
	 *            familyId
	 * @return List<UserInfo>
	 */
	List<UserInfo> getAllUsersByFamilyId(String familyId);

	/**
	 * @description 通过familyId获取家长信息,并按照性别创建时间排序
	 * @author hxzhang
	 * @create 2016年8月12日上午9:44:20
	 * @version 1.0
	 * @param familyId
	 *            familyId
	 * @return 返回查询结果
	 */
	List<UserInfo> getParentInfoByFamilyIdOrd(String familyId);

	/**
	 * @description 根据查询条件获取用户信息
	 * @author hxzhang
	 * @create 2016年8月16日上午10:22:15
	 * @version 1.0
	 * @param condition
	 *            UserBySelectCondition
	 * @return 返回查询结果
	 */
	List<SelecterPo> getStaff(UserBySelectCondition condition);

	List<SelecterPo> getChild(UserBySelectCondition condition);

	List<SelecterPo> getStaffAndCasual(UserBySelectCondition condition);

	/**
	 * @description 通过accountId获取用户信息
	 * @author hxzhang
	 * @create 2016年8月17日下午2:10:41
	 * @version 1.0
	 * @param accountId
	 *            账户ID
	 * @return 返回结果
	 */
	UserInfo getUserInfoByAccountId(String accountId);

	/**
	 * @description 通过familyId获取为归档的小孩
	 * @author hxzhang
	 * @create 2016年8月20日上午11:04:51
	 * @version 1.0
	 * @param familyId
	 *            familyId
	 * @return 返回查询结果
	 */
	List<UserInfo> getUnArchiveChildByFamilyId(String familyId);

	/**
	 * @description 通过familyId获取已入园的小孩
	 * @author hxzhang
	 * @create 2016年8月24日下午3:49:14
	 * @version 1.0
	 * @param familyId
	 *            familyId
	 * @return 返回结果
	 */
	List<UserInfo> getEnrolledChildByFamilyId(String familyId);

	List<UserInfo> getEnrolledChildByFamilyId2(String familyId);

	/**
	 * 
	 * @author dlli5 at 2016年8月30日下午4:51:14
	 * @param now
	 * @return
	 */
	List<LeaveListVo> getOverdueLeaveInfo(Date now);

	/**
	 * 
	 * @author dlli5 at 2016年8月30日下午4:51:12
	 * @param roomId
	 * @return
	 */
	public int getRoomChildCount(String roomId);

	/**
	 * 
	 * @description 获取下一个员工/家长/小孩的personColor
	 * @author mingwang
	 * @create 2016年8月30日下午3:42:35
	 * @version 1.0
	 * @param userInfo
	 * @return
	 */
	String getPersonColorByUserInfo(UserInfo userInfo);

	/**
	 * 
	 * @author dlli5 at 2016年9月7日下午11:01:19
	 * @param centersId
	 * @return
	 */
	public List<UserInfo> getCenterSecondMangers(String centersId);

	/**
	 * @description 通过园区ID获取员工
	 * @author hxzhang
	 * @create 2016年9月12日下午2:20:26
	 * @version 1.0
	 * @param id
	 * @return
	 */
	public List<UserInfo> getStaffByCentreId(@Param("id") String id, @Param("name") String name, @Param("flag") int flag);

	/**
	 * @description 通过小孩的名字进行搜索
	 * @author hxzhang
	 * @create 2016年9月14日上午10:25:42
	 * @version 1.0
	 * @param name
	 * @param type
	 * @return
	 */
	public List<UserInfo> searchSignChild(@Param("name") String name, @Param("centreId") String centreId);

	/**
	 * @description 获取未签出的小孩
	 * @author hxzhang
	 * @create 2016年9月19日下午5:02:36
	 * @version 1.0
	 * @return
	 */
	public List<UserDetail> getUnSignoutChild(Date now);

	/**
	 * @description 通过familyId获取该家庭未激活的家长
	 * @author hxzhang
	 * @create 2016年9月20日上午10:12:52
	 * @version 1.0
	 * @return
	 */
	public List<UserInfo> getUnActiveParentByFamilyId(String familyId);

	/**
	 * 
	 * @description 跟据园区获取staff，的下拉对象集合
	 * @author gfwang
	 * @create 2016年9月22日上午11:17:35
	 * @version 1.0
	 * @param centerId
	 *            園區Id
	 * @return
	 */
	List<SelecterPo> getSelectStaff(StaffCondition condition);

	/**
	 * 
	 * @author dlli5 at 2016年9月23日下午2:51:48
	 * @param userCondition
	 * @return
	 */
	public List<UserVo> getListByCondtion(UserCondition userCondition);

	/**
	 * 
	 * @author abliu at 2016年9月23日下午2:51:48
	 * @param birthday
	 * @return
	 */
	public List<UserVo> getListByBirthday(@Param("birthday") Date birthday);

	/**
	 * 获取要即将离园的小孩
	 * 
	 * @author dlli5 at 2016年9月26日下午6:45:37
	 * @param centersId
	 * @param roomId
	 * @param day
	 * @return
	 */
	public List<UserInfo> getWillLeaveChild(@Param("centersId") String centersId, @Param("roomId") String roomId, @Param("day") Date day);

	/**
	 * 
	 * @author dlli5 at 2016年12月27日下午1:24:59
	 * @param centersId
	 * @param roomId
	 * @param day
	 * @return
	 */
	public List<UserInfo> getWillComeChild(@Param("centersId") String centersId, @Param("roomId") String roomId, @Param("day") Date day);

	/**
	 * @description
	 * @author Hxzhang
	 * @create 2016年9月26日下午8:26:37
	 */
	public UserInfo getUserByIntobsleaId(String id);

	/**
	 * @description 获取已入园且已激活的小孩
	 * @author Hxzhang
	 * @create 2016年9月28日上午9:53:44
	 */
	List<SelecterPo> getEnrolledChildByRoomId(@Param("roomId") String roomId, @Param("params") String params);

	/**
	 * @description 通过newsId获取Tag的小孩
	 * @author Hxzhang
	 * @create 2016年9月28日下午5:20:41
	 */
	UserInfo getTagChildByNewsId(String newsId);

	/**
	 * @description 获取newsId关联的小孩
	 * @author Hxzhang
	 * @create 2016年9月28日下午8:26:15
	 */
	List<String> getNameList(String newsId);

	/**
	 * @description 通过centerId获取园长及二级园长
	 * @author Hxzhang
	 * @create 2016年10月9日上午9:13:39
	 */
	List<UserVo> getManagerByCenterId(String centerId);

	/**
	 * @description 获取所有的CEO
	 * @author Hxzhang
	 * @create 2016年10月9日下午1:18:31
	 */
	List<UserVo> getCEO();

	/**
	 * abliu 通过family获取childId
	 * 
	 * @param familyId
	 * @return
	 */
	List<String> getChildIdByFamilyId(String familyId);

	/**
	 * 
	 * @description 依据toilet和nappy筛选小孩
	 * @author mingwang
	 * @create 2016年10月20日下午6:28:06
	 * @version 1.0
	 * @param type
	 * @return
	 */
	public List<SelecterPo> getChildByWayTlilet(short type);

	/**
	 * @description 获取一个家庭里所有小孩集合
	 * @author hxzhang
	 * @create 2016年10月24日下午2:34:44
	 */
	List<SelecterPo> getChildListForSelect2(@Param("id") String id, @Param("p") String p);

	/**
	 * 
	 * @author dlli5 at 2016年10月25日下午8:11:00
	 * @param id
	 * @param date
	 * @return
	 */
	public List<UserInfo> getRosterUserList(@Param("rosterId") String id, @Param("day") Date date);

	/**
	 * @description 通过小孩的accountId获取其家庭里所有家长
	 * @author hxzhang
	 * @create 2016年10月25日下午7:31:13
	 */
	List<UserInfo> getParentsByChildAccountId(String id);

	List<RoleInfoVo> getRoleByAccount(String accountId);

	/**
	 * @description 更新更新时间
	 * @author hxzhang
	 * @create 2016年11月17日上午10:57:36
	 */
	int updateTimeByUserId(@Param("userId") String userId, @Param("now") Date now);

	/**
	 * 
	 * @author dlli5 at 2016年12月13日上午9:44:17
	 * @param roomId
	 * @param dayOfWeek
	 * @return
	 */
	public List<String> getCurrentChildIdsOfRoom(@Param("centreId") String centreId, @Param("roomId") String roomId, @Param("dayOfWeek") Short dayOfWeek);

	String getAccountIdByUserOldId(String oldId);

	List<SelecterPo> getSelecterPoByaccountId(String accountId);

	List<SelecterPo> getCasualStaff(@Param("likeName") String likeName);

	/**
	 * 
	 * @description 获取所有院长和CEO
	 * @author gfwang
	 * @create 2017年1月4日下午4:04:14
	 * @version 1.0
	 * @return
	 */
	List<UserInfo> getCentreManagerAndCeo();

	List<UserInfo> selectAll(@Param("keyName") String keyName, @Param("tempTag") String tempTag);

	void updateAvatar(@Param("avatar") String avata, @Param("id") String id);

	List<UserInfo> selectTest();

	/**
	 * @description 获取指定room下的孩子,并支持模糊查询.
	 * @author hxzhang
	 * @create 2017年3月14日上午9:35:55
	 */
	List<SelecterPo> getChildsByRoom(@Param("roomId") String roomId, @Param("p") String p);

	/**
	 * @description 获取家庭里所有家长的AccountId
	 * @author hxzhang
	 * @create 2017年5月2日下午1:52:51
	 */
	List<String> getAccountIdsByFamilyId(String familyId);

}