package com.aoyuntek.aoyun.dao;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.condtion.FamilyCondition;
import com.aoyuntek.aoyun.entity.po.FamilyInfo;
import com.aoyuntek.aoyun.entity.vo.EylfInfoVo;
import com.aoyuntek.aoyun.entity.vo.FileListVo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.aoyun.entity.vo.TaskVo;
import com.aoyuntek.aoyun.entity.vo.UnActiveParentVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface FamilyInfoMapper extends BaseMapper<FamilyInfo> {

	void removeFamilyById(String id);

	FamilyInfo getFamilyInfo(String id);

	List<TaskVo> getTaskList(@Param("accountId") String accountId, @Param("strs") String[] strs);

	List<FileListVo> getPagerList(FamilyCondition condition);

	int getPagerListCount(FamilyCondition condition);

	List<SelecterPo> selectParentChilds(@Param("p") String p);

	/**
	 * @description 通过用户ID获取家庭信息
	 * @author hxzhang
	 * @create 2016年8月10日上午11:07:33
	 * @version 1.0
	 * @param userId
	 *            用户ID
	 * @return 家庭信息
	 */
	FamilyInfo getFamilyInfoByUserId(String userId);

	/**
	 * @description 获取归档小孩的家庭
	 * @author hxzhang
	 * @create 2016年8月19日下午5:13:46
	 * @version 1.0
	 * @return List<FamilyInfo>
	 */
	List<FamilyInfo> getFamilyByArchive();

	/**
	 * @description 获取小孩归档的家庭
	 * @author hxzhang
	 * @create 2016年8月20日上午11:56:22
	 * @version 1.0
	 * @return 返回查询结果
	 */
	int updateParentByTimedTask(Date now);

	/**
	 * @description 通过familyId更新归档时间
	 * @author hxzhang
	 * @create 2016年8月23日上午9:52:34
	 * @version 1.0
	 * @param familyId
	 * @param now
	 */
	void updateArchiveTimeByFamilyId(@Param("familyId") String familyId, @Param("now") Date now);

	/**
	 * @description 清除归档时间
	 * @author hxzhang
	 * @create 2016年8月25日下午4:01:52
	 * @version 1.0
	 * @param familyId
	 *            familyId
	 */
	void clearArchiveTimeByFamilyId(String familyId);

	/**
	 * @description 获取家长未激活的家庭
	 * @author hxzhang
	 * @create 2016年8月30日上午9:08:59
	 * @version 1.0
	 * @return
	 */
	List<UnActiveParentVo> getParentUnActive(Date now);

	/**
	 * 
	 * @description 獲取所有小孩
	 * @author gfwang
	 * @create 2016年9月18日上午9:32:28
	 * @version 1.0
	 * @return
	 */
	List<SelecterPo> getAllChilds(@Param("p") String p, @Param("orgId") String orgId, @Param("type") int type);

	/**
	 * @description 获取有未激活家长的家庭
	 * @author hxzhang
	 * @create 2016年9月19日下午9:46:46
	 * @version 1.0
	 * @return
	 */
	List<FamilyInfo> getHaveUnActiveParentEnrolledChild();

	/**
	 * 
	 * @description 得到指定小孩的年龄段
	 * @author sjwang
	 * @create 2016年11月1日上午10:52:58
	 * @version 1.0
	 * @param id
	 * @returntab_child_eylf
	 */
	List<Integer> getAgeStage();

	/**
	 * 
	 * @description 得到小孩指定年龄段的能力
	 * @author sjwang
	 * @create 2016年11月1日下午1:59:35
	 * @version 1.0
	 * @return
	 */
	List<EylfInfoVo> getChildScopeEylf(@Param("ageScope") Integer ageScope, @Param("childId") String childId);

	/**
	 * @description 通过familyId获取小孩的centerId
	 * @author hxzhang
	 * @create 2017年2月27日下午4:22:49
	 */
	List<String> getChildsByFamilyId(String familyId);
}