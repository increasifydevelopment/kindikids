package com.aoyuntek.aoyun.dao;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.ValueTextInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface ValueTextInfoMapper extends BaseMapper<ValueTextInfo> {
    int insert(ValueTextInfo record);

    int insertSelective(ValueTextInfo record);
    
    List<ValueTextInfo> selectByInstanceId(String instanceId);
    
    int deleteByInstanceId(String instanceId);
}