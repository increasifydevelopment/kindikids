package com.aoyuntek.aoyun.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.condtion.EmailListCondition;
import com.aoyuntek.aoyun.entity.po.EmailPlanInfo;
import com.aoyuntek.aoyun.entity.vo.EmailListVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface EmailPlanInfoMapper extends BaseMapper<EmailPlanInfo> {
	int insert(EmailPlanInfo record);

	int insertSelective(EmailPlanInfo record);

	int getCountByObjId(String id);

	void remove(String objId);

	List<EmailPlanInfo> getList();

	List<EmailListVo> getPagerList(EmailListCondition condition);

	int getPagerListCount(EmailListCondition condition);

	List<String> getAllIds(EmailListCondition condition);

	List<EmailPlanInfo> getListByIds(@Param("ids") List<String> ids);

	void updateStatus(@Param("ids") List<String> ids);
}