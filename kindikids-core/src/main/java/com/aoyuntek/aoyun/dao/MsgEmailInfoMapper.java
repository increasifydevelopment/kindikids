package com.aoyuntek.aoyun.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.MsgEmailInfo;
import com.aoyuntek.aoyun.entity.vo.EmailVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface MsgEmailInfoMapper extends BaseMapper<MsgEmailInfo> {
    /**
     * 
     * @description 获取List
     * @author gfwang
     * @create 2016年10月10日上午10:06:18
     * @version 1.0
     * @param category
     *            0email，1电话 类型
     * @return
     */
    List<MsgEmailInfo> getList(@Param("category") Short category);

    /**
     * 
     * @description 逻辑删除
     * @author gfwang
     * @create 2016年10月10日上午10:08:02
     * @version 1.0
     * @param id
     * @return
     */
    int deleteById(@Param("id") String id);

    /**
     * @description 批量插入数据
     * @author hxzhang
     * @create 2016年12月7日上午9:11:10
     */
    void batchInsert(@Param("emails") List<MsgEmailInfo> emails);

}