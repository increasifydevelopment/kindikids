package com.aoyuntek.aoyun.dao;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.VisitLogInfo;

/**
 * @description 日志数据访问层接口
 * @author xdwang
 * @create 2015年12月10日下午8:02:29
 * @version 1.0
 */
public interface IVisitLogDao {

    /**
     * 批量添加访问日志
     * 
     * @param logInfos
     *            反问日志集合
     */
    public void saveBacth(List<VisitLogInfo> logInfos);
}
