package com.aoyuntek.aoyun.dao.roster;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.roster.RosterShiftTaskInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface RosterShiftTaskInfoMapper extends BaseMapper<RosterShiftTaskInfo> {

    int haveTask(@Param("taskId") String taskId, @Param("shiftTimeId") String shiftTimeId);

    void deleteOther(@Param("shiftTimeId") String shiftTimeId, @Param("shiftTaskIds") List<String> shiftTaskIds);

    void deleteByOtherShiftTimeId(@Param("shiftId") String string, @Param("shiftTimeIds") List<String> shiftTimeIds);

    int haveUseShiftTask(@Param("taskId") String taskId);
}