package com.aoyuntek.aoyun.dao;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.AttendanceHistoryInfo;
import com.aoyuntek.aoyun.entity.vo.AttendanceHistoryVo;
import com.aoyuntek.aoyun.entity.vo.child.AttendanceHistoryGroupVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface AttendanceHistoryInfoMapper extends BaseMapper<AttendanceHistoryInfo> {

	void updateEndTime(@Param("childId") String childId, @Param("endTime") Date changeDate);

	List<AttendanceHistoryVo> getByChildId(String childId);

	List<AttendanceHistoryInfo> getChangeCentreHistory(@Param("intervalDay") int intervalDay, @Param("day") Date day);

	AttendanceHistoryInfo getUpHistory(@Param("childId") String childId, @Param("day") Date day);

	List<AttendanceHistoryGroupVo> getCentreGroupByChildId(String childId);

	List<AttendanceHistoryGroupVo> getCentreGroupByIds(@Param("ids") List<String> ids);

	AttendanceHistoryInfo getCurrentGroupNo(String childId);

	void removeHistoryByChildId(@Param("childId") String childId, @Param("date") Date date, @Param("accountId") String accountId);

	List<AttendanceHistoryInfo> getListByChild(@Param("accountId") String accountId);

}