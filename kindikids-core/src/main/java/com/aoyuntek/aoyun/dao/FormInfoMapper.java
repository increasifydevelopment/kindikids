package com.aoyuntek.aoyun.dao;

import com.aoyuntek.aoyun.entity.po.FormInfo;
import com.aoyuntek.aoyun.entity.vo.FormInfoDataVO;
import com.aoyuntek.framework.dao.BaseMapper;

public interface FormInfoMapper extends BaseMapper<FormInfo>{

	FormInfoDataVO selectVoByCode(String code);
	
    int deleteByPrimaryKey(String formId);

    int insert(FormInfo record);

    int insertSelective(FormInfo record);

    FormInfo selectByPrimaryKey(String formId);

    int updateByPrimaryKeySelective(FormInfo record);

    int updateByPrimaryKey(FormInfo record);
}