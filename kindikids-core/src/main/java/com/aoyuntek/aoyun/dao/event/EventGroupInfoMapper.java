package com.aoyuntek.aoyun.dao.event;

import com.aoyuntek.aoyun.entity.po.event.EventGroupInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface EventGroupInfoMapper extends BaseMapper<EventGroupInfo> {
}