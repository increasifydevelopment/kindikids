package com.aoyuntek.aoyun.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.FormAttributeInfo;
import com.aoyuntek.aoyun.entity.po.InstanceAttributeInfo;
import com.aoyuntek.aoyun.entity.vo.FormAttributeInfoDataVO;
import com.aoyuntek.framework.dao.BaseMapper;

public interface FormAttributeInfoMapper extends BaseMapper<FormAttributeInfo> {

	List<FormAttributeInfoDataVO> selectFormAttrVoByFormId(@Param("formId") String formId,@Param("instanceId") String instanceId);
	
	List<InstanceAttributeInfo> selectInstanceAttrByFormAttrId(Map map);
	
    List<FormAttributeInfo> selectByFormId(String formId);
	
	int deleteByPrimaryKey(String attrId);

    int insert(FormAttributeInfo record);

    int insertSelective(FormAttributeInfo record);

    FormAttributeInfo selectByPrimaryKey(String attrId);

    int updateByPrimaryKeySelective(FormAttributeInfo record);

    int updateByPrimaryKey(FormAttributeInfo record);
}