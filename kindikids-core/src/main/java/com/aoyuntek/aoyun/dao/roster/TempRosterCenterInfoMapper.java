package com.aoyuntek.aoyun.dao.roster;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.roster.TempRosterCenterInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface TempRosterCenterInfoMapper extends BaseMapper<TempRosterCenterInfo> {

	List<TempRosterCenterInfo> getBetweenChangeList(@Param("currtenDate") Date currtenDate);

	List<TempRosterCenterInfo> getAfterChangelist(@Param("currtenDate") Date currtenDate);

	int deleteBetweenItem(@Param("currtenDate") Date currtenDate);

	int deleteAfterChange(@Param("currtenDate") Date currtenDate);

	void deleteByRosterStaffId(String rosterStaffId);

	void updateStateByRosterStaffId(@Param("rosterStaffId") String rosterStaffId, @Param("state") Short state);

	List<TempRosterCenterInfo> getTempRoster(@Param("rosterStaffId") String rosterStaffId, @Param("day") Date day);
}