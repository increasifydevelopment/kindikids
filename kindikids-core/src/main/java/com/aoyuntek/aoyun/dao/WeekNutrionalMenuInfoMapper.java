package com.aoyuntek.aoyun.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.condtion.MenuCondition;
import com.aoyuntek.aoyun.condtion.WeekMenuCondition;
import com.aoyuntek.aoyun.entity.po.WeekNutrionalMenuInfo;
import com.aoyuntek.aoyun.entity.vo.WeekMenuVo;
import com.aoyuntek.aoyun.entity.vo.WeekNutrionalMenuVo;
import com.aoyuntek.framework.dao.BaseMapper;

/**
 * 
 * @description 园区菜单
 * @author gfwang
 * @create 2016年8月17日下午4:44:47
 * @version 1.0
 */
public interface WeekNutrionalMenuInfoMapper extends BaseMapper<WeekNutrionalMenuInfo> {
    /**
     * 
     * @description 条件获取菜单
     * @author gfwang
     * @create 2016年8月20日下午1:52:27
     * @version 1.0
     * @param condition
     *            condition
     * @return List<WeekNutrionalMenuInfo>
     */
    List<WeekNutrionalMenuVo> getMenuList(WeekMenuCondition condition);

    /**
     * 
     * @description 初始化weekNutrionalMenu
     * @author mingwang
     * @create 2016年8月22日下午5:15:23
     * @version 1.0
     * @param list
     */
    void initMenuInfo(ArrayList<WeekNutrionalMenuInfo> list);

    /**
     * 
     * @description 更新菜单信息
     * @author mingwang
     * @create 2016年8月23日上午10:13:28
     * @version 1.0
     * @param weekMenuList
     */
    void updateMenuInfo(List<WeekNutrionalMenuVo> weekMenuList);

    /**
     * 
     * @description 获取当天菜单
     * @author mingwang
     * @create 2016年8月24日上午9:04:08
     * @version 1.0
     * @param weekNum
     * @param weekToday
     * @return
     */
    WeekNutrionalMenuInfo getTodayMenu(WeekMenuCondition condition);

    /**
     * 
     * @description 校验menuName是否重复
     * @author mingwang
     * @create 2017年6月1日上午11:16:52
     * @version 1.0
     * @param menuName
     * @param menuId
     * @return
     */
    int validateName(@Param("menuName")String menuName, @Param("menuId")String menuId);

    /**
     * 
     * @description 获取所有菜单列表
     * @author mingwang
     * @create 2017年6月1日下午2:56:01
     * @param condition
     * @return
     */
    List<WeekNutrionalMenuVo> getweekNutrionalMenuList(MenuCondition condition);

    /**
     * 
     * @description 
     * @author mingwang
     * @create 2017年6月1日下午5:35:31
     * @param list
     */
    void initWeekMenuInfo(List<WeekNutrionalMenuInfo> list);

    /**
     * 
     * @description 获取菜单详情
     * @author mingwang
     * @create 2017年6月1日下午6:17:08
     * @param name
     * @return
     */
    List<WeekNutrionalMenuVo> getMenuInfo(String menuId);

    /**
     * 
     * @description 删除menu
     * @author mingwang
     * @create 2017年6月1日下午7:00:01
     * @param name
     */
    void deleteMenuByName(String menuId);

    /**
     * 
     * @description 更新menuName
     * @author mingwang
     * @create 2017年6月2日上午9:38:27
     * @param menuId
     * @param menuName
     */
    void updateMenuName(@Param("menuId")String menuId, @Param("menuName")String menuName);

    /**
     * 
     * @description 获取menu总数量
     * @author mingwang
     * @create 2017年6月2日下午1:53:56
     * @param condition
     * @return
     */
    int getMenuListCount(MenuCondition condition);

    List<WeekMenuVo> getSelectMenus();

    void insertRoomMenu(@Param("roomId")String roomId, @Param("menuId")String menuId, @Param("index")String index);

    void deleteRoomMenu(@Param("roomId")String roomId, @Param("index")String index);

    /**
     * 
     * @description 获取room下第几周的菜单Id
     * @author mingwang
     * @create 2017年6月5日下午2:54:07
     * @param roomId
     * @param index
     * @return
     */
    String getMenuId(@Param("roomId")String roomId, @Param("index")String index);

    List<String> getRoomList(short weekNum);

    WeekNutrionalMenuInfo getSendMenu(@Param("roomId")String roomId, @Param("weekNum")short weekNum, @Param("weekToday")short weekToday);

    /**
     * 
     * @description 菜单是否被room关联
     * @author mingwang
     * @create 2017年6月7日下午3:41:06
     * @param menuId
     * @return
     */
    int getRelationCount(String menuId);
}