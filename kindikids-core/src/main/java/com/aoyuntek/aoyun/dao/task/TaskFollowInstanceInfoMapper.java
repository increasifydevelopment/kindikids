package com.aoyuntek.aoyun.dao.task;

import java.util.Date;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.task.TaskFollowInstanceInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface TaskFollowInstanceInfoMapper extends BaseMapper<TaskFollowInstanceInfo> {

    TaskFollowInstanceInfo getByFollowUpId(@Param("followUpId")String followUpId,@Param("taskInstanceId")String taskInstanceId);

    void deleteDoingByModelId(String taskModelId);

    void updateFollowUpId(@Param("oldId") String oldId, @Param("newId") String newId);

    void updateStatu(@Param("day") Date day);
}