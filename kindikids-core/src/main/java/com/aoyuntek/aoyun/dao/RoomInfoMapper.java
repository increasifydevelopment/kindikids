package com.aoyuntek.aoyun.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.RoomInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.vo.RoomInfoVo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface RoomInfoMapper extends BaseMapper<RoomInfo> {

	List<String> getRoomIdsByCenterId(String centerId);

	/**
	 * @description 通过孩子获取room
	 * @author hxzhang
	 * @create 2017年1月4日下午8:01:24
	 */
	RoomInfo getRoomInfoByUserId(String userId);

	/**
	 * 
	 * @author dlli5 at 2016年9月20日下午6:50:15
	 * @param centerId
	 * @param roomId
	 * @param p
	 * @return
	 */
	List<SelecterPo> selecterRooms(@Param("centerId") String centerId, @Param("roomId") String roomId, @Param("p") String p);

	/**
	 * @description 通过园区ID获取room信息
	 * @author hxzhang
	 * @create 2016年8月5日下午3:24:41
	 * @version 1.0
	 * @param centerId
	 *            园区ID
	 * @return List<RoomInfo>
	 */
	List<RoomInfo> getRoomInfo();

	/**
	 * 
	 * @description 通过园区id获取room信息
	 * @author mingwang
	 * @create 2016年8月19日下午5:16:37
	 * @version 1.0
	 * @param centerId
	 * @return
	 */
	List<RoomInfoVo> selectRoomsByCenterId(@Param("centerId") String centerId);

	/**
	 * 
	 * @description 校验当前园区下room名称是否重复
	 * @author gfwang
	 * @create 2016年8月20日上午11:08:43
	 * @version 1.0
	 * @param centerId
	 *            园区id
	 * @param roomId
	 *            roomid
	 * @param name
	 *            room名称
	 * @return 个数
	 */
	int validateName(@Param("centerId") String centerId, @Param("roomId") String roomId, @Param("name") String name);

	/**
	 * 
	 * @description 判断当前room下所有的group是否归档
	 * @author mingwang
	 * @create 2016年8月22日下午2:01:18
	 * @version 1.0
	 * @param roomId
	 * @return
	 */
	int isAllArchived(@Param("roomId") String roomId);

	/**
	 * 
	 * @description 归档/解除归档room
	 * @author mingwang
	 * @create 2016年8月22日下午2:09:58
	 * @version 1.0
	 * @param roomId
	 * @param value
	 * @return
	 */
	int updateStatusByRoomId(@Param("roomId") String roomId, @Param("status") short status);

	/**
	 * 
	 * @description 获取当前room下所有人的userId
	 * @author mingwang
	 * @create 2016年8月25日下午4:55:10
	 * @version 1.0
	 * @param roomId
	 * @return
	 */
	List<String> getUserListByRoomId(@Param("roomId") String roomId);

	/**
	 * 
	 * @description 根据userId判断room是否归档
	 * @author mingwang
	 * @create 2016年8月29日上午11:04:58
	 * @version 1.0
	 * @param userId
	 * @return
	 */
	int isRoomAchived(@Param("userId") String userId);

	/**
	 * 
	 * @description 根据roomId查找centerId
	 * @author mingwang
	 * @create 2016年9月1日下午3:58:37
	 * @version 1.0
	 * @param roomId
	 * @return
	 */
	String selectCenterIdByRoom(@Param("roomId") String roomId);

	/**
	 * 
	 * @author dlli5 at 2016年9月23日上午9:34:01
	 * @param roomId
	 * @return
	 */
	int isRoomAchivedById(@Param("roomId") String roomId);

	/**
	 * 
	 * @description 获取rooms
	 * @author gfwang
	 * @create 2016年9月8日下午2:10:46
	 * @version 1.0
	 * @param parentUserId
	 * @return
	 */
	List<String> getRoomsByChild(@Param("userId") String parentUserId);

	/**
	 * 
	 * @description 根据小孩信息判断所在room是否存在
	 * @author mingwang
	 * @create 2016年9月8日下午5:35:08
	 * @version 1.0
	 * @param child
	 * @return
	 */
	int isRoomExist(UserInfo child);

	/**
	 * 
	 * @description 获取所有roomId
	 * @author mingwang
	 * @create 2016年9月25日下午2:11:24
	 * @version 1.0
	 * @return
	 */
	List<String> getRoomIds();

	/**
	 * @description 获取所有room
	 * @author hxzhang
	 * @create 2016年9月25日下午4:20:57
	 * @version 1.0
	 * @return
	 */
	List<RoomInfo> getAllRoom();

	/**
	 * 
	 * @description 通过Id获取roomName
	 * @author mingwang
	 * @create 2016年11月30日下午1:31:48
	 * @version 1.0
	 * @param id
	 * @return
	 */
	String getRoomNameById(String id);

	/**
	 * @description 通过Room Name 获取Room Id
	 * @author hxzhang
	 * @create 2016年12月15日上午9:08:02
	 */
	String getRoomIdByOldId(String oldId);

	String getCenterIdByRoomOldId(String oldId);

}