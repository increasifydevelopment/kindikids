package com.aoyuntek.aoyun.dao.program;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.program.ProgramTagsInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface ProgramTagsInfoMapper extends BaseMapper<ProgramTagsInfo> {
	int insert(ProgramTagsInfo record);

	int insertSelective(ProgramTagsInfo record);

	void removeTags(String taskId);

	List<String> getTagsAccountIds(String taskId);
}