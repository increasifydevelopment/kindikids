package com.aoyuntek.aoyun.dao;

import com.aoyuntek.aoyun.entity.po.RoleFunctionInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface RoleFunctionInfoMapper extends BaseMapper<RoleFunctionInfo> {

}