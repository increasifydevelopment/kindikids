package com.aoyuntek.aoyun.dao;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.AbsenteeRequestInfoWithBLOBs;
import com.aoyuntek.framework.dao.BaseMapper;

public interface AbsenteeRequestInfoMapper extends BaseMapper<AbsenteeRequestInfoWithBLOBs> {

	/**
	 * @description 更新AbsenteeRequestInfo
	 * @author hxzhang
	 * @create 2016年8月26日下午5:58:26
	 * @version 1.0
	 * @param id
	 * @param opera
	 * @param now
	 * @return
	 */
	int updateAbsenteeRequestInfoById(@Param("id") String id, @Param("opera") short opera, @Param("now") Date now);

	/**
	 * @description 通过小孩子的ID获取其请假的数量
	 * @author hxzhang
	 * @create 2016年8月28日下午12:02:52
	 * @version 1.0
	 * @param id
	 * @return
	 */
	int getCountByChildId(String id);

	/**
	 * @description
	 * @author hxzhang
	 * @create 2016年8月28日下午2:41:15
	 * @version 1.0
	 * @param absenteeRequestInfoWithBLOBs
	 * @return
	 */
	int updateByPrimaryKeyWithBLOBs(AbsenteeRequestInfoWithBLOBs absenteeRequestInfoWithBLOBs);

	/**
	 * @description 定时任务
	 * @author hxzhang
	 * @create 2016年8月28日下午5:45:27
	 * @version 1.0
	 * @param now
	 * @return
	 */
	int absenteeTimedTask(Date now);

	/**
	 * @description 取消小孩的请假申请
	 * @author hxzhang
	 * @create 2016年9月5日下午3:26:36
	 * @version 1.0
	 */
	void cancelChildAbsenteeRequest(String childId);

	/**
	 * @description 冻结小孩已审批的请假申请,使其不能再操作
	 * @author hxzhang
	 * @create 2016年9月8日下午5:44:29
	 * @version 1.0
	 * @param accountId
	 * @return
	 */
	int frozenARRequestLog(String accountId);

	/**
	 * 
	 * @description 判断两个时间段内是否有交集
	 * @author gfwang
	 * @create 2016年10月8日下午4:52:48
	 * @version 1.0
	 * @param startDate
	 *            开始时间
	 * @param endDate
	 *            结束时间
	 * @return
	 */
	int getDateRepeatCount(@Param("childId") String childId, @Param("startDate") Date startDate, @Param("endDate") Date endDate);

	/**
	 * @description 获取两日期之间小孩请假信息
	 * @author hxzhang
	 * @create 2016年10月26日下午3:47:10
	 */
	List<AbsenteeRequestInfoWithBLOBs> getAbsenteeList(@Param("id") String id, @Param("startDate") Date startDate, @Param("endDate") Date endDate);

	/**
	 * @description 根据小孩的accountId获取时间段内请假的总天数
	 * @author hxzhang
	 * @create 2016年12月5日上午11:41:40
	 */
	int getTotalDay(@Param("accountId") String accountId, @Param("startDate") Date startDate, @Param("endDate") Date endDate);

	/**
	 * @description 更新请假时间
	 * @author hxzhang
	 * @create 2016年12月12日下午2:16:31
	 */
	int updateAbsentNum(@Param("bs") AbsenteeRequestInfoWithBLOBs bs, @Param("minuend") int minuend, @Param("startDate") Date startDate, @Param("endDate") Date endDate);

	/**
	 * @description 查看小孩是否有请假记录
	 * @author hxzhang
	 * @create 2016年12月20日下午5:09:34
	 */
	List<AbsenteeRequestInfoWithBLOBs> getAbsenteesByDate(@Param("childId") String childId, @Param("date") Date date);

	List<AbsenteeRequestInfoWithBLOBs> getAbsenteesByChild(@Param("childId") String childId);

	/**
	 * 
	 * @author dlli5 at 2016年12月20日下午7:52:52
	 * @param childId
	 * @param date
	 * @return
	 */
	List<AbsenteeRequestInfoWithBLOBs> getAbsenteesAfterDate(@Param("childId") String childId, @Param("date") Date date);
}