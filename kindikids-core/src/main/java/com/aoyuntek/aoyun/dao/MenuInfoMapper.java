package com.aoyuntek.aoyun.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.MenuInfo;
import com.aoyuntek.aoyun.entity.vo.RoleInfoVo;
import com.aoyuntek.framework.dao.BaseMapper;

/**
 * 
 * @description
 * @author gfwang
 * @create 2016年9月14日下午1:46:49
 * @version 1.0
 */
public interface MenuInfoMapper extends BaseMapper<MenuInfo> {

    /**
     * 
     * @description
     * @author gfwang
     * @create 2016年9月14日下午1:47:34
     * @version 1.0
     * @param roleInfoVoList
     * @return
     */
    List<MenuInfo> getMenuList(@Param("roleInfoVoList") List<RoleInfoVo> roleInfoVoList);
}