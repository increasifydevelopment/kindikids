package com.aoyuntek.aoyun.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.YelfInfo;
import com.aoyuntek.aoyun.entity.vo.EylfInfoVo;
import com.aoyuntek.aoyun.entity.vo.FollowupEylfVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface YelfInfoMapper extends BaseMapper<YelfInfo> {
    /**
     * @description 获取全部eylf条款
     * @author hxzhang
     * @create 2016年9月21日下午4:42:55
     * @version 1.0
     * @return
     */
    List<FollowupEylfVo> getAllEylf();

    /**
     * @description 通过followUpId获取对应的Eylf条款
     * @author hxzhang
     * @create 2016年9月22日上午10:45:24
     * @version 1.0
     * @param id
     * @return
     */
    List<FollowupEylfVo> getFollowupEylfVoByFollowUpId(String id);

    /**
     * @description 获取已经选择的Eylf
     * @author Hxzhang
     * @create 2016年9月27日上午10:28:55
     */
    List<YelfInfo> getCilkEylfByFollowupId(String id);

    /**
     * 
     * @description
     * @author gfwang
     * @create 2016年9月23日上午8:56:54
     * @version 1.0
     * @return
     */
    List<EylfInfoVo> getRootNode();

    List<YelfInfo> getYelfInfoByParent(@Param("parentNode") String parentNode);

    /**
     * @description 获取news选择的YelfInfo
     * @author Hxzhang
     * @create 2016年9月28日下午5:52:35
     */
    List<YelfInfo> getYelfInfoByNewsId(String newsId);

}