package com.aoyuntek.aoyun.dao;

import java.util.Date;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.HolidaypayInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface HolidaypayInfoMapper extends BaseMapper<HolidaypayInfo> {

	HolidaypayInfo getAccountIdDay(@Param("accountId") String accountId, @Param("date") Date date);

}