package com.aoyuntek.aoyun.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.SignSelationInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface SignSelationInfoMapper extends BaseMapper<SignSelationInfo> {

    void removeSignById(String signId);

    void insertBatchSignSelationInfo(@Param("list") List<SignSelationInfo> list);

}