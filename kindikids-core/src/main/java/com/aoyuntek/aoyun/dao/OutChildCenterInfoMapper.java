package com.aoyuntek.aoyun.dao;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.CentersInfo;
import com.aoyuntek.aoyun.entity.po.OutChildCenterInfo;

public interface OutChildCenterInfoMapper {
	int deleteByPrimaryKey(String id);

	int insert(OutChildCenterInfo record);

	int insertSelective(OutChildCenterInfo record);

	OutChildCenterInfo selectByPrimaryKey(String id);

	int updateByPrimaryKeySelective(OutChildCenterInfo record);

	int updateByPrimaryKey(OutChildCenterInfo record);

	String getCenterIdsByUserId(String userId);

	List<CentersInfo> getCenterNameByUserId(String id);

	List<String> getCentreIdListByuserId(String id);

	List<OutChildCenterInfo> getListByUserId(String id);

	void deleteCenterByUserId(String userId);
}