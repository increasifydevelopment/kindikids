package com.aoyuntek.aoyun.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.EmailMsg;

public interface EmailMsgMapper {
	int deleteByPrimaryKey(String id);

	int insert(EmailMsg record);

	int insertSelective(EmailMsg record);

	EmailMsg selectByPrimaryKey(String id);

	int updateByPrimaryKeySelective(EmailMsg record);

	int updateByPrimaryKey(EmailMsg record);

	List<EmailMsg> getEmailMsgListByUserId(String userId);

	void removeEmailMsgByUserId(String userId);

	List<EmailMsg> getEmailMsgListByAppId(String appId);

	List<EmailMsg> getListByIds(@Param("ids") List<String> ids);

	EmailMsg getByUserIdAndType(@Param("userId") String userId, @Param("type") Short type);
}