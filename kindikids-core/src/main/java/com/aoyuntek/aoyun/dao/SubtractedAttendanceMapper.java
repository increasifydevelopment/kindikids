package com.aoyuntek.aoyun.dao;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.SubtractedAttendance;
import com.aoyuntek.aoyun.entity.vo.SigninVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface SubtractedAttendanceMapper extends BaseMapper<SubtractedAttendance> {

	List<SigninVo> getSubtractedAttendance(@Param("centreId") String centreId, @Param("roomId") String roomId, @Param("dayOfWeek") int dayOfWeek,
			@Param("signDate") Date day);

	void deleteOld(String childId);

	List<SubtractedAttendance> getSubtractedAttendanceOfDate(Date day);

	List<SubtractedAttendance> getListByChildAndDateScope(@Param("accountId") String accountId, @Param("beginDate") Date beginDate, @Param("endDate") Date endDate);

	void deleteConflict(@Param("childId") String childId, @Param("newRoomId") String newRoomId, @Param("changeDate") Date changeDate,
			@Param("nextRoomChangeDate") Date nextRoomChangeDate);

	List<SubtractedAttendance> getListByChild(@Param("accountId") String accountId);
}