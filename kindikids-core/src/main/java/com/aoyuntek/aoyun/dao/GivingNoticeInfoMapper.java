package com.aoyuntek.aoyun.dao;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.GivingNoticeInfo;
import com.aoyuntek.aoyun.entity.po.GivingNoticeInfoWithBLOBs;
import com.aoyuntek.framework.dao.BaseMapper;

public interface GivingNoticeInfoMapper extends BaseMapper<GivingNoticeInfoWithBLOBs> {
	
	
    /**
     * @description 获取孩子的离园记录
     * @author hxzhang
     * @create 2017年1月4日上午9:10:32
     */
    List<GivingNoticeInfo> getGivingNoticeInfoWithBLOBsByChildId(String childId);

    /**
     * @description 更新GivingNoticeInfo
     * @author hxzhang
     * @create 2016年8月26日下午2:46:09
     * @version 1.0
     * @param id
     * @param opera
     * @param now
     */
    int updateGivingNoticeInfoById(@Param("id") String id, @Param("opera") short opera, @Param("now") Date now);

    /**
     * @description 通过小孩ID获取离园申请个数
     * @author hxzhang
     * @create 2016年8月26日下午3:25:54
     * @version 1.0
     * @param id
     * @return
     */
    int getCountByChildId(String id);

    /**
     * @description
     * @author hxzhang
     * @create 2016年8月28日下午2:30:42
     * @version 1.0
     * @param givingNoticeInfoWithBLOBs
     * @return
     */
    int updateByPrimaryKeyWithBLOBs(GivingNoticeInfoWithBLOBs givingNoticeInfoWithBLOBs);

    /**
     * @description 定时任务
     * @author hxzhang
     * @create 2016年8月28日下午5:13:09
     * @version 1.0
     * @param now
     * @return
     */
    int givingNoticeTimedTask(Date now);

    /**
     * @description 取消小孩的离园申请
     * @author hxzhang
     * @create 2016年9月5日下午3:21:56
     * @version 1.0
     * @param childId
     */
    void cancelChildGivingNotice(String childId);

    /**
     * @description 将到期且未审批的离园申请更新为失效状态
     * @author hxzhang
     * @create 2016年9月7日下午8:15:30
     * @version 1.0
     * @param now
     * @return
     */
    int givingNoticeStatusTimedTask(Date now);

    /**
     * @description 冻结已审批的离园申请RequestLog
     * @author hxzhang
     * @create 2016年9月8日下午5:36:50
     * @version 1.0
     * @param accountId
     * @return
     */
    int frozenGNRequestLog(String accountId);

    /**
     * @description
     * @author hxzhang
     * @create 2016年12月20日下午7:44:58
     */
    List<GivingNoticeInfoWithBLOBs> haveGivingNotice(@Param("childId") String childId, @Param("endDate") Date endDate);
}