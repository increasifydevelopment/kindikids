package com.aoyuntek.aoyun.dao.meeting;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.meeting.MeetingRowInfo;
import com.aoyuntek.aoyun.entity.vo.meeting.TableColRowVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface MeetingRowInfoMapper extends BaseMapper<MeetingRowInfo> {
    /**
     * @description 批量插入信息
     * @author hxzhang
     * @create 2016年10月19日上午11:05:48
     */
    void insertBatchMeetingRowInfo(List<MeetingRowInfo> rowList);

    /**
     * @description 删除会议表格数据
     * @author hxzhang
     * @create 2016年10月19日下午3:06:19
     */
    void deleteMeetingRowInfoByMeetingId(String id);

    /**
     * @description 通过rowId获取该行数据
     * @author hxzhang
     * @create 2016年10月23日下午2:30:57
     */
    List<TableColRowVo> getRowInfoByRowId(String id);

    /**
     * @description 通过RowId获取责任人
     * @author hxzhang
     * @create 2016年10月23日下午3:13:18
     */
    String getLiablerByRowId(String id);
}