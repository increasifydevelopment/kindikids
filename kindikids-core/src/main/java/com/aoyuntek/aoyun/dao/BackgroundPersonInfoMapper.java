package com.aoyuntek.aoyun.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.BackgroundPersonInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface BackgroundPersonInfoMapper extends BaseMapper<BackgroundPersonInfo> {
    /**
     * @description 批量插入BackgroundPersonInfo
     * @author hxzhang
     * @create 2016年7月28日下午7:17:29
     * @version 1.0
     * @param personList
     *            List<BackgroundPersonInfo>
     */
    void insterBatchBackgroundPersonInfo(@Param("personList") List<BackgroundPersonInfo> personList);

    /**
     * @description 通过backgroundId删除BackgroundPersonInfo记录
     * @author hxzhang
     * @create 2016年7月29日下午1:57:51
     * @version 1.0
     * @param backgroundId
     *            backgroundId
     */
    int reomveBackgroundPersonInfoByBackgroundId(String backgroundId);

    List<BackgroundPersonInfo> getBackgroundPersonInfoByBackgroundId(String backgroundId);
}