package com.aoyuntek.aoyun.dao;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.ValueStringInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface ValueStringInfoMapper extends BaseMapper<ValueStringInfo> {
    int insert(ValueStringInfo record);

    int insertSelective(ValueStringInfo record);

    List<ValueStringInfo> selectByInstanceId(String instanceId);

    int deleteByInstanceId(String instanceId);
}