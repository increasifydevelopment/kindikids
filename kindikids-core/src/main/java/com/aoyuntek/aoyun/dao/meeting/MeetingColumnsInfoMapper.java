package com.aoyuntek.aoyun.dao.meeting;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.meeting.MeetingColumnsInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface MeetingColumnsInfoMapper extends BaseMapper<MeetingColumnsInfo> {
    /**
     * @description 批量插入数据
     * @author hxzhang
     * @create 2016年10月14日下午4:13:24
     */
    void insertBatchMeetingColumnsInfo(@Param("list") List<MeetingColumnsInfo> list);

    /**
     * @description 删除多余数据
     * @author hxzhang
     * @create 2016年10月14日下午4:41:18
     */
    int removeMeetingColumnsInfo(@Param("tempId") String tempId, @Param("list") List<MeetingColumnsInfo> list);

}