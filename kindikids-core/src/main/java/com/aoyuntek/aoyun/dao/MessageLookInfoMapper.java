package com.aoyuntek.aoyun.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.MessageLookInfo;
import com.aoyuntek.aoyun.entity.vo.ReceiverVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface MessageLookInfoMapper extends BaseMapper<MessageLookInfo> {
    /**
     * @description 批量插入MessageReceiveInfo
     * @author hxzhang
     * @create 2016年6月23日下午8:41:21
     * @version 1.0
     * @param messageReceiveInfoList
     *            MessageReceiveInfo集合
     */
    void insterBatchMessageReceiveInfo(@Param("messageReceiveInfoList")
    List<MessageLookInfo> messageReceiveInfoList);

    /**
     * @description 通过messageId和当前登用户获取收件人信息
     * @author hxzhang
     * @create 2016年6月29日下午2:14:31
     * @version 1.0
     * @param messageId
     *            Message ID
     * @param accountId
     *            account ID
     * @return 返回收件人信息
     */
    MessageLookInfo getMessageReceiveInfoByMessageId(@Param("messageId") String messageId, @Param("accountId") String accountId);

    /**
     * @description 获取收件人信息
     * @author bbq
     * @create 2016年7月14日上午10:12:55
     * @version 1.0
     * @param messageId msgId
     * @return 满足条件的所有记录
     */
    List<MessageLookInfo> getMessageLookInfoByMessageIdAndAccountId(String messageId);
    
    /**
     * @description 获取收件人VO
     * @author bbq
     * @create 2016年7月14日上午10:14:00
     * @version 1.0
     * @param msgId msgId
     * @return 满足条件的所有记录
     */
    List<ReceiverVo> getReceiversByMsgId(String msgId);

}