package com.aoyuntek.aoyun.dao;

import com.aoyuntek.aoyun.entity.po.PreviousEnrolmentRelation;
import com.aoyuntek.framework.dao.BaseMapper;

public interface PreviousEnrolmentRelationMapper extends BaseMapper<PreviousEnrolmentRelation> {
	
}