package com.aoyuntek.aoyun.dao.program;

import com.aoyuntek.aoyun.entity.po.program.TaskPersonLiableInfo;

public interface TaskPersonLiableInfoMapper {
    int insert(TaskPersonLiableInfo record);

    int insertSelective(TaskPersonLiableInfo record);
}