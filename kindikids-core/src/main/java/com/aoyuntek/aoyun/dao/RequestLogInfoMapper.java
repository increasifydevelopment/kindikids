package com.aoyuntek.aoyun.dao;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.condtion.AttendanceManagementCondition;
import com.aoyuntek.aoyun.condtion.ExternalListCondition;
import com.aoyuntek.aoyun.condtion.waitingList.WaitingListCondition;
import com.aoyuntek.aoyun.entity.po.RequestLogInfo;
import com.aoyuntek.aoyun.entity.vo.ExternalListVo;
import com.aoyuntek.aoyun.entity.vo.RequestLogVo;
import com.aoyuntek.aoyun.entity.vo.waiting.WaitingListVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface RequestLogInfoMapper extends BaseMapper<RequestLogInfo> {

	List<RequestLogVo> getListByIds(@Param("ids") List<String> ids);

	List<RequestLogInfo> getListByChild(String accountId);

	void removeLogsByChild(String accountId);

	/**
	 * 
	 * @author dlli5 at 2016年8月23日上午11:31:57
	 * @param childId
	 * @return
	 */
	List<RequestLogVo> selectLogVo(String childId);

	/**
	 * 
	 * @author dlli5 at 2016年9月13日下午5:38:16
	 * @param childId
	 */
	void dealSynLog(String childId);

	/**
	 * 
	 * @author dlli5 at 2016年12月21日下午6:23:28
	 * @param childId
	 */
	void dealUnArchived(String childId);

	List<RequestLogVo> getPagarList(AttendanceManagementCondition condition);

	int getPagarListCount(AttendanceManagementCondition condition);

	RequestLogInfo getRequestLogInfoByObjId(String objId);

	List<ExternalListVo> getExternalList(ExternalListCondition condition);

	List<WaitingListVo> getExternalSiblingList(WaitingListCondition condition);

	int getExternalSiblingListCount(WaitingListCondition condition);

	int getExternalListCount(ExternalListCondition condition);

	void updatePositionDate(@Param("id") String id, @Param("date") Date date);

	void updateSiblingFlag(@Param("id") String id, @Param("date") Date date, @Param("accountId") String accountId, @Param("flag") boolean flag);
}