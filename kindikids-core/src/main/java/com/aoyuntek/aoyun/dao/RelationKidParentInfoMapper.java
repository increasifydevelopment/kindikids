package com.aoyuntek.aoyun.dao;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.RelationKidParentInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface RelationKidParentInfoMapper extends BaseMapper<RelationKidParentInfo> {

	void removeRelation(String id);

	/**
	 * @description 通过familyId获取关系
	 * @author hxzhang
	 * @create 2016年8月3日下午5:59:29
	 * @version 1.0
	 * @param familyId
	 *            familyId
	 * @return RelationKidParentInfo
	 */
	RelationKidParentInfo getRelationKidParentInfoByFamilyId(String familyId);

	/**
	 * @description 通过小孩的AccountId获取该小孩的关系
	 * @author hxzhang
	 * @create 2016年8月4日下午1:54:01
	 * @version 1.0
	 * @param kidAccountId
	 *            小孩AccountId
	 * @return List<RelationKidParentInfo>
	 */
	List<RelationKidParentInfo> getRkpListByKidAccountId(String kidAccountId);

	/**
	 * @description 通过accountId删除关系
	 * @author hxzhang
	 * @create 2016年8月4日下午5:52:56
	 * @version 1.0
	 * @param accountId
	 *            被删除用户的accountId
	 */
	void updateRelationBykidAccountId(String accountId);
}