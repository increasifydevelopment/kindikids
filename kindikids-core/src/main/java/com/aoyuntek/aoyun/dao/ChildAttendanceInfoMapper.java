package com.aoyuntek.aoyun.dao;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.ChildAttendanceInfo;
import com.aoyuntek.framework.dao.BaseMapper;

/**
 * 
 * @author dlli5 at 2016年8月22日上午8:57:15
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public interface ChildAttendanceInfoMapper extends BaseMapper<ChildAttendanceInfo> {

	void setEnrolledFlag(String id);

	void removeChildAtten(String id);

	void updateRequestDateById(@Param("date") Date date, @Param("userId") String userId);

	void updateOutChildTime(@Param("userId") String userId, @Param("time") Date time);

	ChildAttendanceInfo getChildAttendanceByUserId(String userId);

	List<ChildAttendanceInfo> haveChildAttendance(@Param("userId") String userId, @Param("endDate") Date endDate);

	/**
	 * 
	 * @author dlli5 at 2016年8月29日上午10:35:37
	 * @param userId
	 * @return
	 */
	ChildAttendanceInfo selectByUserId(String userId);

	/**
	 * 
	 * @author dlli5 at 2016年11月24日下午4:26:32
	 * @param accountId
	 * @return
	 */
	ChildAttendanceInfo selectByAccountId(String accountId);

	/**
	 * 
	 * @author dlli5 at 2016年8月29日上午10:37:10
	 * @param familyId
	 * @return
	 */
	int getEnrolledCount(String familyId);

	/**
	 * @description 根据小孩Id更新leaveDate
	 * @author hxzhang
	 * @create 2016年8月29日下午5:55:40
	 * @version 1.0
	 */
	void updateLeaveDateById(@Param("date") Date date, @Param("userId") String userId);

	/**
	 * 
	 * @description 入园小孩
	 * @author gfwang
	 * @create 2016年8月29日下午10:00:02
	 * @version 1.0
	 * @param userId
	 *            用户id
	 * @return
	 */
	int enrolChild(String userId);

	/**
	 * 获取今天该入院的
	 * 
	 * @author dlli5 at 2016年9月1日上午9:29:01
	 * @param now
	 * @return
	 */
	List<ChildAttendanceInfo> getEnrolledChild(Date enrolmentDate);

	/**
	 * 
	 * @author dlli5 at 2016年9月1日上午9:55:31
	 * @param leaveDate
	 * @return
	 */
	List<ChildAttendanceInfo> getLeaveChild(Date leaveDate);

	/**
	 * @description 只获取当天离园的小孩
	 * @author hxzhang
	 * @create 2016年10月28日上午10:24:00
	 */
	List<ChildAttendanceInfo> getLeaveChildInNow(Date leaveDate);

	/**
	 * @description 清除离园日期
	 * @author hxzhang
	 * @create 2016年9月13日上午9:36:30
	 * @version 1.0
	 * @param userId
	 */
	void removeLeaveDateByUserId(String userId);

	/**
	 * @description 通过familyId获取该家庭小孩的入园信息
	 * @author hxzhang
	 * @create 2016年9月20日上午9:58:21
	 * @version 1.0
	 * @param familyId
	 * @return
	 */
	List<ChildAttendanceInfo> getChildAttendanceInfoByFamilyId(String familyId);

	/**
	 * @description 获取离园一星期的小孩
	 * @author hxzhang
	 * @create 2016年10月26日下午1:08:14
	 */
	List<ChildAttendanceInfo> getOneWeekAgoLeaveChild(Date date);

	/**
	 * 
	 * @description 是否排课
	 * @author gfwang
	 * @create 2016年11月30日下午2:04:34
	 * @version 1.0
	 * @param attendFlag
	 * @return
	 */
	int attendChangeByOut(@Param("attendFlag") boolean attendFlag, @Param("userId") String userId);
}