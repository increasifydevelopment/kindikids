package com.aoyuntek.aoyun.dao.event;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.event.EventDocumentsInfo;
import com.aoyuntek.aoyun.entity.po.policy.PolicyDocumentsInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface EventDocumentsInfoMapper extends BaseMapper<EventDocumentsInfo>{
	
	  /**
     * 
     * @description 虚拟删除文件
     * @author abliu
     * @create 2016年9月25日下午1:48:22
     * @version 1.0
     * @param id
     * @return
     */
    int deleteDocByIds(@Param("list") List<String> list);
    
    /**
     * 批量插入附件主表
     * @param eventDocList
     * @return
     */
    int insterBatchEventDocInfo(@Param("eventDocList") List<EventDocumentsInfo> eventDocumentsInfo);
}