package com.aoyuntek.aoyun.dao.program;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.condtion.TaskCondtion;
import com.aoyuntek.aoyun.entity.po.program.ProgramTaskExgrscInfo;
import com.aoyuntek.aoyun.entity.vo.ActivityVo;
import com.aoyuntek.aoyun.entity.vo.task.TaskListVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface ProgramTaskExgrscInfoMapper extends BaseMapper<ProgramTaskExgrscInfo> {
    /**
     * @description 获取关联活动的小孩信息
     * @author hxzhang
     * @create 2016年9月20日下午8:54:05
     * @version 1.0
     * @param roomId
     * @param type
     * @return
     */
    List<ActivityVo> getActivityManagement(@Param("roomId") String roomId, @Param("type") short type, @Param("category") short category,
            @Param("taskDay") Date taskDay);

    /**
     * @description 获取Task和Followup关联的活动
     * @author hxzhang
     * @create 2016年10月11日下午7:30:46
     */
    List<ActivityVo> getTaskAndFollowupActivity(@Param("roomId") String roomId, @Param("type") short type, @Param("category") short category,
            @Param("taskDay") Date taskDay);

    /**
     * @description 获取followup关联小孩活动信息
     * @author Hxzhang
     * @create 2016年9月30日下午2:19:48
     */
    List<ActivityVo> getFollowupActivityManagement(@Param("roomId") String roomId, @Param("type") short type, @Param("category") short category,
            @Param("taskDay") Date taskDay);

    /**
     * @description 获取ProgramTaskExgrscInfoList
     * @author hxzhang
     * @create 2016年9月22日下午7:37:46
     * @version 1.0
     * @param condtion
     * @return
     */
    List<TaskListVo> getProgramTaskExgrscInfoList(TaskCondtion condtion);

    /**
     * @description 更新
     * @author hxzhang
     * @create 2016年9月22日下午10:02:40
     * @version 1.0
     * @param id
     * @param newsId
     * @return
     */
    int updateNewsfeedId(@Param("id") String id, @Param("newsfeedId") String newsfeedId);

    /**
     * @description 获取每个Room下每天只有一条的ProgramTaskExgrscInfo对象
     * @author hxzhang
     * @create 2016年9月25日下午2:43:50
     * @version 1.0
     * @param type
     * @return
     */
    ProgramTaskExgrscInfo getOnliyOneExgrscByRoomIdType(@Param("roomId") String roomId, @Param("type") short type, @Param("now") Date now);

    /**
     * @description 获取过期的ProgramTaskExgrsc
     * @author hxzhang
     * @create 2016年9月25日下午7:29:10
     * @version 1.0
     * @param now
     * @return
     */
    List<String> getOverDueProgramTaskExgrsc(Date now);

    /**
     * @description 将过期的记录更新为过期状态
     * @author hxzhang
     * @create 2016年9月25日下午7:38:48
     * @version 1.0
     * @param now
     * @return
     */
    int updateOverDueProgramTaskExgrsc(Date now);
}