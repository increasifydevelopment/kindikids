package com.aoyuntek.aoyun.dao.program;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.program.ProgramRegisterSleepInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface ProgramRegisterSleepInfoMapper extends BaseMapper<ProgramRegisterSleepInfo> {

    /**
     * 
     * @description 批量插入ProgramRegisterSleepInfo
     * @author mingwang
     * @create 2016年9月22日下午9:53:32
     * @version 1.0
     * @param registerSleeps
     */
    void batchInsertRegisterSleeps(List<ProgramRegisterSleepInfo> registerSleeps);

    /**
     * 
     * @description 批量更新ProgramRegisterSleepInfo
     * @author mingwang
     * @create 2016年9月22日下午10:18:54
     * @version 1.0
     * @param registerSleeps
     */
    void batchUpdateRegisterSleeps(List<ProgramRegisterSleepInfo> registerSleeps);

    /**
     * 
     * @description 批量删除RegisterSleep（直接删除）
     * @author mingwang
     * @create 2016年10月10日下午4:42:16
     * @version 1.0
     * @param remove_ids
     * @return
     */
    int batchDeleteById(List<String> remove_ids);

}