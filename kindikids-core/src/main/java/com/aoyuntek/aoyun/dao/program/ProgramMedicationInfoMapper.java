package com.aoyuntek.aoyun.dao.program;


import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.program.ProgramMedicationInfoWithBLOBs;
import com.aoyuntek.aoyun.entity.vo.ProgramMedicationInfoVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface ProgramMedicationInfoMapper extends BaseMapper<ProgramMedicationInfoWithBLOBs> {

    /**
     * 
     * @description 通过timeMedicationId删除ProgramMedicationInfo
     * @author mingwang
     * @create 2016年9月26日下午6:07:37
     * @version 1.0
     * @param timeMedicationId
     */
    void deleteByTimeMedicationId(String timeMedicationId);

    /**
     * 
     * @description 获取ProgramMedicationInfoVo
     * @author mingwang
     * @create 2016年9月26日下午8:11:20
     * @version 1.0
     * @param id
     * @return
     */
    ProgramMedicationInfoVo getMedicationInfoVo(String id);

    /**
     * 
     * @author dlli5 at 2016年9月26日下午9:01:09
     * @param centersId
     * @param roomId
     * @param day
     * @return
     */
    List<ProgramMedicationInfoVo> getWillTakeMedicine(@Param("centersId")String centersId, @Param("roomId")String roomId, @Param("day")Date day);

    /**
     * 
     * @description 获取过期的Medication
     * @author mingwang
     * @create 2016年9月28日下午3:02:35
     * @version 1.0
     * @param now
     * @return
     */
    List<String> getOverDueMedication(Date now);

    /**
     * 
     * @description 将过期Medication状态更新为overDue
     * @author mingwang
     * @create 2016年9月28日下午3:02:48
     * @version 1.0
     * @param now
     * @return
     */
    int updateOverDueMedication(Date now);

    /**
     * 
     * @description 删除medicationId
     * @author mingwang
     * @create 2016年9月30日下午5:15:30
     * @version 1.0
     * @param dbTimeMedicationId
     */
    void updateMedicationId(String dbTimeMedicationId);

    /**
     * 
     * @description 设置medicationId
     * @author mingwang
     * @create 2016年9月30日下午5:20:59
     * @version 1.0
     * @param id
     * @param timeMedicationId
     */
    void updateMedicationIdById(@Param("id")String id,@Param("timeMedicationId") String timeMedicationId);

    /**
     * 
     * @description 更新
     * @author mingwang
     * @create 2016年10月12日下午1:57:25
     * @version 1.0
     * @param dbMedicationInfoWithBLOBs
     */
    void updateByPrimaryKeyWithBLOBs(ProgramMedicationInfoWithBLOBs dbMedicationInfoWithBLOBs);
}