package com.aoyuntek.aoyun.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.EmergencyContactInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface EmergencyContactInfoMapper extends BaseMapper<EmergencyContactInfo> {
    /**
     * @description 通过用户ID获取紧急联系人集合
     * @author hxzhang
     * @create 2016年7月28日下午6:06:45
     * @version 1.0
     * @param userId
     *            用户ID
     * @return 返回查询集合
     */
    List<EmergencyContactInfo> selectByUserId(String userId);

    /**
     * @description 批量插入紧急联系人对象
     * @author hxzhang
     * @create 2016年7月28日下午6:08:57
     * @version 1.0
     * @param emergencyContactList
     *            List<EmergencyContactInfo>
     */
    void insterBatchEmergencyContactInfo(@Param("emergencyContactList") List<EmergencyContactInfo> emergencyContactList);

    /**
     * @description 通过用户ID删除EmergencyContactInfo记录
     * @author hxzhang
     * @create 2016年7月29日上午9:05:57
     * @version 1.0
     * @param userId
     *            用户ID
     */
    void removeEmergencyContactInfoByUserId(String userId);
}