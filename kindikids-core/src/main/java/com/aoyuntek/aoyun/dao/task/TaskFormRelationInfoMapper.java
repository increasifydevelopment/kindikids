package com.aoyuntek.aoyun.dao.task;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.task.TaskFormRelationInfo;

public interface TaskFormRelationInfoMapper {

    int insert(TaskFormRelationInfo record);

    int insertSelective(TaskFormRelationInfo record);

    void deleteByTaskId(@Param("taskId") String taskId);

    String getByTaskId(@Param("taskId") String taskId);
}