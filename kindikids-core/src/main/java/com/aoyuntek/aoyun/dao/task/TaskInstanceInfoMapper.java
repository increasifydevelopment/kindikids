package com.aoyuntek.aoyun.dao.task;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.condtion.TaskCondtion;
import com.aoyuntek.aoyun.entity.po.task.TaskInstanceInfo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.aoyun.entity.vo.task.TaskListVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface TaskInstanceInfoMapper extends BaseMapper<TaskInstanceInfo> {

	List<SelecterPo> getTaskNames(TaskCondtion condtion);

	List<String> getTaskIdByByInstanceId(@Param("list") List<String> list);

	List<TaskListVo> getTaskListForTable(TaskCondtion condtion);

	List<String> nqsSelectVer(String selectNqs);

	void updateSetEndDay(String taskModelId, Date day);

	void deleteDoingByTaskModelId(String taskModelId);

	List<TaskListVo> getListVo(TaskCondtion condtion);

	List<TaskListVo> getListVoForTable(TaskCondtion condtion);

	TaskListVo getTaskListVoByValueId(String valueId);

	int getListVoCount(TaskCondtion condtion);

	int getListVoCountForTable(TaskCondtion condtion);

	int getTaskInstanceSizeOfStatu(@Param("taskModelId") String taskModelId, @Param("statu") Short statu,
			@Param("include") Boolean include);

	int haveTaskInstance(@Param("taskModelId") String taskModelId, @Param("beginDate") Date beginDate, @Param("endDate") Date endDate,
			@Param("objType") Short obj_type, @Param("objId") String objId);

	void updateStatu(@Param("day") Date day);

	int updateStatuByValueIds(@Param("valueIds") List<String> valueIds);

	int getMyOverDueTaskInstanceSize(String acountId);

	void deleteTaskInstanceByValueId(@Param("valueId") String valueId);

	void updateModelId(@Param("oldId") String oldId, @Param("newId") String newId);

	void updateGroupId(@Param("childId") String childId, @Param("newGroupId") String newGroupId);

	void updateTaskInstanceForDay(@Param("valueId") String valueId, @Param("day") Date day);

	void updateTaskStatus(@Param("valueId") String valueId, @Param("taskType") Short taskType, @Param("statu") short statu);

	void updateStatus(@Param("valueId") String valueId, @Param("statu") short statu, @Param("updateAccountId") String updateAccountId,
			@Param("updateTime") Date updateTime);

	TaskInstanceInfo getByValueId(@Param("valueId") String valueId);

	TaskInstanceInfo getTaskInstanceInfoByValueId(String id);

	int getHatMeetingCountInNow(String id);

	/**
	 * 
	 * @author dlli5 at 2016年11月4日下午2:44:53
	 * @param condtion
	 * @return
	 */
	List<TaskListVo> getAllListVo(TaskCondtion condtion);

	void insertBatch(List<TaskInstanceInfo> TaskInstanceList);

	int getTaskInstanceInfoList(@Param("valueId") String valueId, @Param("type") short type, @Param("date") Date date);

	List<String> getIndividualTaskIdByAccountId(@Param("day") Date day, @Param("accountId") String accountId);

	List<String> getIndividualTaskFollowupIdByAccountId(@Param("day") Date day, @Param("accountId") String accountId);
}