package com.aoyuntek.aoyun.dao;

import com.aoyuntek.aoyun.entity.po.ChangePswRecordInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface ChangePswRecordInfoMapper extends BaseMapper<ChangePswRecordInfo> {

	/**
	 * 
	 * @description 根据token获取密码重置记录
	 * @author bbq
	 * @create 2016年6月16日下午7:42:00
	 * @version 1.0
	 * @param token
	 * @return ChangePswRecordInfo对象
	 */
	ChangePswRecordInfo getRecordByToken(String token);

	/**
	 * 根据accountId删除过时邮件
	 * 
	 * @author rick
	 * @date 2016年7月22日 下午4:40:59
	 * @param accountId
	 */
	void deleteByAccount(String accountId);

}