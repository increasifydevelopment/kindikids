package com.aoyuntek.aoyun.dao;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.condtion.SignCondition;
import com.aoyuntek.aoyun.entity.po.ReplaceChildVo;
import com.aoyuntek.aoyun.entity.po.SigninInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.aoyun.entity.vo.SigninVo;
import com.aoyuntek.aoyun.entity.vo.StaffSignInfoVo;
import com.aoyuntek.aoyun.entity.vo.roster.RosterStaffVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface SigninInfoMapper extends BaseMapper<SigninInfo> {

	List<SigninVo> getInterimList(@Param("centerId") String centerId, @Param("roomId") String roomId, @Param("day") Date day);

	List<ReplaceChildVo> getChildListByCenterId(String centerId);

	int updateSignDateById(@Param("id") String id, @Param("signDate") Date signDate);

	List<SigninInfo> getSigninOrout(@Param("accountId") String accountId, @Param("date") Date date, @Param("type") short type);

	/**
	 * @description 判断孩子当天是否有签入或签出信息
	 * @author hxzhang
	 * @create 2017年1月10日上午10:17:12
	 */
	SigninInfo havaSignForToday(@Param("accountId") String accountId, @Param("type") short type);

	/**
	 * 
	 * @author dlli5 at 2016年9月5日下午7:22:11
	 * @param centreId
	 * @param roomId
	 * @param day
	 * @return
	 */
	List<SigninVo> getAttendanceLogs(@Param("centreId") String centreId, @Param("roomId") String roomId, @Param("signDate") Date day);

	/**
	 * 
	 * @author dlli5 at 2016年8月24日下午1:58:28
	 * @param roomId
	 * @return
	 */
	List<SigninVo> getListByWhere(@Param("centersId") String centreId, @Param("roomId") String roomId, @Param("dayOfWeek") int dayOfWeek, @Param("signDate") Date day);

	/**
	 * 
	 * @author dlli5 at 2016年8月29日下午7:45:43
	 * @param centreId
	 * @param roomId
	 * @param dayOfWeek
	 * @param day
	 * @return
	 */
	List<ReplaceChildVo> getInternalReplaceChilds(@Param("centreId") String centreId, @Param("roomId") String roomId, @Param("dayOfWeek") int dayOfWeek,
			@Param("signDate") Date day);

	/**
	 * 
	 * @author dlli5 at 2016年8月31日下午8:25:30
	 * @param centreId
	 * @param roomId
	 * @param dayOfWeek
	 * @return
	 */
	List<ReplaceChildVo> getSiblingsReplaceChilds(@Param("centreId") String centreId, @Param("roomId") String roomId, @Param("dayOfWeek") int dayOfWeek,
			@Param("signDate") Date day);

	/**
	 * 
	 * @author dlli5 at 2016年8月31日下午8:25:35
	 * @param centreId
	 * @param roomId
	 * @return
	 */
	List<ReplaceChildVo> getExternalsReplaceChilds(@Param("centreId") String centreId, @Param("roomId") String roomId, @Param("dayOfWeek") int dayOfWeek,
			@Param("signDate") Date day);

	/**
	 * @description 通过游客签到ID获取主表签到信息
	 * @author hxzhang
	 * @create 2016年9月1日下午4:24:15
	 * @version 1.0
	 * @param id
	 * @return
	 */
	SigninInfo getSigninInfoBySignVisitorId(String id);

	/**
	 * 
	 * @author dlli5 at 2016年9月5日下午7:18:51
	 * @param centersId
	 * @param day
	 * @return
	 */
	List<SigninVo> getVisitorAttendanceLogs(@Param("centersId") String centersId, @Param("signDate") Date day);

	/**
	 * @description 获取员工签到信息
	 * @author hxzhang
	 * @create 2016年9月19日下午7:44:26
	 * @version 1.0
	 * @param centersId
	 * @param day
	 * @return
	 */
	List<SigninVo> getStaffAttendanceLogs(@Param("centersId") String centersId, @Param("roomId") String roomId, @Param("signDate") Date day);

	/**
	 * @description 通过小孩签到ID获取主表签到信息
	 * @author hxzhang
	 * @create 2016年9月12日下午3:35:08
	 * @version 1.0
	 * @param id
	 * @return
	 */
	SigninInfo getSigninInfoBySignChildId(String id);

	/**
	 * 
	 * @author dlli5 at 2016年9月13日下午6:39:14
	 * @param centersId
	 * @param day
	 * @return
	 */
	List<SigninVo> getTodaySignOutChilds(@Param("centersId") String centersId, @Param("signDate") Date day);

	/**
	 * @description 根据小孩签到子表ID获取签入信息或签出信息
	 * @author hxzhang
	 * @create 2016年9月18日上午8:38:13
	 * @version 1.0
	 * @param id
	 * @return
	 */
	SigninInfo getSignInfoBySignChildId(@Param("id") String id, @Param("type") short type);

	/**
	 * @description 通过accountId获取员工的签入签出信息
	 * @author hxzhang
	 * @create 2016年9月20日上午11:23:29
	 * @version 1.0
	 * @param accountId
	 * @return
	 */
	StaffSignInfoVo getStaffSignByAccountId(@Param("accountId") String accountId, @Param("date") Date date, @Param("centerId") String centerId);

	/**
	 * @description 获取员工签到导出数据
	 * @author hxzhang
	 * @create 2016年9月20日下午4:00:11
	 * @version 1.0
	 * @param condition
	 * @return
	 */
	List<StaffSignInfoVo> getStaffExport(SignCondition condition);

	/**
	 * 
	 * @description 获取当天应到的小孩
	 * @author gfwang
	 * @create 2016年9月26日下午9:56:40
	 * @version 1.0
	 * @param date
	 * @return
	 */
	List<SelecterPo> getTodayInChild(@Param("signDate") Date date, @Param("roomId") String roomId);

	/**
	 * @description
	 * @author Hxzhang
	 * @create 2016年9月29日下午11:49:00
	 */
	int removeSignById(String id);

	/**
	 * 
	 * @author dlli5 at 2016年10月9日上午8:52:02
	 * @param childId
	 * @param newCentreId
	 * @param newRoomId
	 */
	int updateChildSignOfToday(@Param("childId") String childId, @Param("newCentreId") String newCentreId, @Param("newRoomId") String newRoomId);

	/**
	 * @description 更新签到信息
	 * @author hxzhang
	 * @create 2016年10月9日下午3:18:21
	 */
	int updateCentreIdByAccountId(@Param("centreId") String centreId, @Param("accountId") String accountId);

	/**
	 * @description
	 * @author hxzhang
	 * @create 2016年10月9日下午4:00:02
	 */
	SigninInfo getSigninInfoByAccountIdAndDate(@Param("accountId") String accountId, @Param("date") Date date, @Param("centerId") String centerId);

	/**
	 * 獲取員工簽到信息
	 * 
	 * @author dlli5 at 2016年10月18日上午11:10:58
	 * @param centreId
	 * @param weekStart
	 * @return
	 */
	List<RosterStaffVo> getStaffSignByCentre(@Param("centreId") String centreId, @Param("weekStart") Date weekStart);

	/**
	 * 
	 * @author dlli5 at 2016年10月27日下午3:11:03
	 * @param centreId
	 * @param day
	 * @return
	 */
	List<RosterStaffVo> getStaffSignByCentreAndDay(@Param("centreId") String centreId, @Param("day") Date day);

	/**
	 * 
	 * @author dlli5 at 2016年10月24日下午5:09:21
	 * @param accountId
	 * @param day
	 */
	void deleteNoSign(@Param("accountId") String accountId, @Param("day") Date day);

	/**
	 * 
	 * @author dlli5 at 2016年12月28日下午4:43:23
	 * @param accountId
	 * @param day
	 * @param centreId
	 */
	void deleteNoSignOfCentre(@Param("accountId") String accountId, @Param("day") Date day, @Param("centreId") String centreId);

	/**
	 * 
	 * @author dlli5 at 2016年10月27日上午8:59:11
	 * @param staffAccountId
	 * @param centerId
	 * @param groupId
	 * @param roomId
	 * @param day
	 */
	void updateSignCentreId(@Param("staffAccountId") String staffAccountId, @Param("centerId") String centerId, @Param("roomId") String roomId, @Param("day") Date day);

	/**
	 * 
	 * @author dlli5 at 2016年12月23日下午4:05:23
	 * @param accountId
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	Integer getUserSignSize(@Param("accountId") String accountId, @Param("beginDate") Date beginDate, @Param("endDate") Date endDate);

	/**
	 * 
	 * @author dlli5 at 2016年12月23日下午4:05:28
	 * @param childId
	 * @param day
	 * @return
	 */
	SigninVo getChildSigninVo(@Param("accountId") String childId, @Param("day") Date day);

	/**
	 * 
	 * @author dlli5 at 2016年12月23日下午4:05:31
	 * @param roomChilds
	 * @param day
	 * @return
	 */
	List<SigninVo> getChildSigninVos(@Param("accountIds") List<String> roomChilds, @Param("day") Date day);

	/**
	 * 
	 * @author dlli5 at 2016年12月23日下午4:05:37
	 * @param list
	 */
	void insertBatchSigninInfo(@Param("list") List<SigninInfo> list);

	List<UserInfo> getStaffListByKeyword(@Param("centreId") String centreId, @Param("name") String name);

	/**
	 * 获取小孩记录数 园区
	 * 
	 * @author dlli5 at 2016年12月23日下午4:06:56
	 * @param centreId
	 *            园区
	 * @param day
	 *            天
	 * @return
	 */
	int getCentreChildLogCount(@Param("centreId") String centreId, @Param("day") Date day);
}