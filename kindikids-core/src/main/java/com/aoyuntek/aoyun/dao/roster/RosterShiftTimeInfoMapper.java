package com.aoyuntek.aoyun.dao.roster;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.roster.RosterShiftTimeInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface RosterShiftTimeInfoMapper extends BaseMapper<RosterShiftTimeInfo> {

	void deleteOther(@Param("shiftId") String shiftId, @Param("shiftTimeIds") List<String> shiftTimeIds);

	RosterShiftTimeInfo getByCode(@Param("shiftId") String shiftId, @Param("code") String shiftTimeCode);

	List<String> getShiftTimeCodes(@Param("taskId") String taskModelId, @Param("shiftId") String shiftId);

	int getRepeatShiftCount(@Param("startTime") Date startTime, @Param("endTime") Date endTime, @Param("accountId") String accountId, @Param("centreId") String centreId,
			@Param("day") Date day);
}