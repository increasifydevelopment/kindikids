package com.aoyuntek.aoyun.dao.policy;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.policy.PolicyDocumentsInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface PolicyDocumentsInfoMapper extends BaseMapper<PolicyDocumentsInfo> {
	  /**
     * 
     * @description 虚拟删除文件
     * @author abliu
     * @create 2016年9月25日下午1:48:22
     * @version 1.0
     * @param id
     * @return
     */
    int deleteDocByIds(@Param("list") List<String> list);
    
    /**
     * 批量插入附件主表
     * @param policyDocumentsInfos
     * @return
     */
    int insterBatchPolicyDocInfo(@Param("policyDocList") List<PolicyDocumentsInfo> policyDocumentsInfos);
}