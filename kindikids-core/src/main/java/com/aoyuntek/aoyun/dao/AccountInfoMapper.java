package com.aoyuntek.aoyun.dao;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.AccountInfo;
import com.aoyuntek.aoyun.entity.vo.ReceiverVo;
import com.aoyuntek.aoyun.entity.vo.UserInGroupInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface AccountInfoMapper extends BaseMapper<AccountInfo> {

	int getCountByCardId(String cardId);

	void addErrorNum(@Param("num") int num, @Param("account") String account);

	Integer getErrorNumByAccount(String account);

	void completeById(String id);

	void removeAccountByUserId(String userId);

	void updateOutAccountTime(String userId, Date time);

	/**
	 * @description 查询有无签出
	 * @author hxzhang
	 * @create 2016年12月23日上午10:28:54
	 */
	int notHaveSignOutByAccountId(String accountId);

	/**
	 * @description 移除未签出标识
	 * @author hxzhang
	 * @create 2016年12月23日上午10:41:35
	 */
	void removeNotHaveSignOut(String accountId);

	/**
	 * @description 更新未签出标识
	 * @author hxzhang
	 * @create 2016年12月23日上午10:27:47
	 */
	void updateNotSignOutByAccountId(@Param("now") Date now, @Param("list") List<String> list);

	/**
	 * 
	 * @description动态执行sql
	 * @author gfwang
	 * @create 2016年6月25日下午11:10:13
	 * @version 1.0
	 * @param sql
	 *            动态执行的sql
	 * @return 获取的结果
	 */
	List<UserInGroupInfo> getUserByGroupSql(@Param("sql") String sql);

	/**
	 * @description 通过accountIds获取对应的accountInfo
	 * @author hxzhang
	 * @create 2016年6月30日下午3:43:08
	 * @version 1.0
	 * @param accountIds
	 *            accountIds
	 * @return 返回结果
	 */
	List<AccountInfo> getAccountInfosByAccountId(@Param("accountIds") List<String> accountIds);

	/**
	 * 
	 * @description 通过accountIds获取对应的ReceiverVo
	 * @author bbq
	 * @create 2016年7月9日下午3:14:46
	 * @version 1.0
	 * @param ids
	 *            id集合
	 * @return List<ReceiverVo>
	 */
	List<ReceiverVo> getNamesByIds(@Param("accountIds") List<String> ids);

	/**
	 * @description 通过用户ID获取AccountInfo
	 * @author hxzhang
	 * @create 2016年7月29日下午3:15:52
	 * @version 1.0
	 * @param userId
	 *            用户ID
	 * @return AccountInfo
	 */
	AccountInfo selectAccountInfoByUserId(String userId);

	/**
	 * @description 通过用户ID更改账户状态
	 * @author hxzhang
	 * @create 2016年8月2日下午2:34:55
	 * @version 1.0
	 * @param userId
	 *            用户ID
	 * @param status
	 *            状态值
	 * @return 返回操作结果
	 */
	int updateStatusByUserId(@Param("userId") String userId, @Param("status") short status);

	/**
	 * @description 通过用户ID获取账户信息
	 * @author hxzhang
	 * @create 2016年8月4日上午11:05:15
	 * @version 1.0
	 * @param userId
	 *            用户ID
	 * @return AccountInfo
	 */
	AccountInfo getAccountInfoByUserId(String userId);

	/**
	 * @description 删除账户信息
	 * @author hxzhang
	 * @create 2016年8月10日下午4:07:44
	 * @version 1.0
	 * @param userId
	 *            用户ID
	 */
	void removeAccountInfoByUserId(String userId);

	/**
	 * @description 通过familyId获取用户的账户信息
	 * @author hxzhang
	 * @create 2016年8月11日下午4:00:06
	 * @version 1.0
	 * @param familyId
	 *            familyId
	 * @param userType
	 *            userType
	 * @return 返回查询结果
	 */
	List<AccountInfo> getAccountByFamilyId(@Param("familyId") String familyId, @Param("userType") short userType);

	/**
	 * @description 通过accountId获取角色名称
	 * @author hxzhang
	 * @create 2016年8月24日上午8:59:15
	 * @version 1.0
	 * @param accountId
	 *            accountId
	 * @return 返回查询结果
	 */
	String getRoleNameByAccountId(String accountId);

	/**
	 * @description 取消发送激活邮件
	 * @author hxzhang
	 * @create 2016年8月30日下午3:53:24
	 * @version 1.0
	 * @param userId
	 * @return
	 */
	int updatePswByUserId(String userId);

	/**
	 * @description 通过小孩的accountId归档小孩
	 * @author hxzhang
	 * @create 2016年9月7日下午8:53:19
	 * @version 1.0
	 * @param accountId
	 * @return
	 */
	int updateArchiveChildByAccountId(String accountId);

	/**
	 * @description 通过用户ID和psw获取用户计数
	 * @author hxzhang
	 * @create 2016年9月12日上午11:40:41
	 * @version 1.0
	 * @param userId
	 * @param psw
	 * @return
	 */
	int getCountByUserIdPsw(@Param("userId") String userId, @Param("psw") String psw);

	/**
	 * @description 同意条款
	 * @author hxzhang
	 * @create 2016年10月21日下午4:35:27
	 */
	int saveAgree(String id);

	/**
	 * 
	 * @author dlli5 at 2016年10月25日上午10:27:27
	 * @param centreId
	 * @param value
	 * @return
	 */
	public List<AccountInfo> getListOfCentreRole(@Param("centreId") String centreId, @Param("value") short value);

}