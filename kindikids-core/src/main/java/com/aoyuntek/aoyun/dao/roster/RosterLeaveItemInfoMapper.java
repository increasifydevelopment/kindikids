package com.aoyuntek.aoyun.dao.roster;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.roster.RosterLeaveItemInfo;
import com.aoyuntek.aoyun.entity.vo.roster.RosterLeaveVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface RosterLeaveItemInfoMapper extends BaseMapper<RosterLeaveItemInfo> {
   List<RosterLeaveVo> selectRosterLeaveVo();
 String getRosterLeaveIdById(String id);
   int selectCountById(String id);
   void  deleteRosterLeave(String id);
   
}