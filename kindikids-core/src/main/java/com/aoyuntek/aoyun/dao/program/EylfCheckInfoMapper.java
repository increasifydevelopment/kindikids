package com.aoyuntek.aoyun.dao.program;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.program.EylfCheckInfo;
import com.aoyuntek.aoyun.entity.vo.ChildEylfVo;
import com.aoyuntek.aoyun.entity.vo.EylfInfoVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface EylfCheckInfoMapper extends BaseMapper<EylfCheckInfo> {
    /**
     * @description 获取该小孩年龄所在的FollowupEylfcheck
     * @author hxzhang
     * @create 2016年9月21日下午5:07:10
     * @version 1.0
     * @param age
     * @return
     */
    List<ChildEylfVo> getFollowupEylfcheckByAge(int age);

    /**
     * @description 获取小孩EylfCheck的历史信息
     * @author hxzhang
     * @create 2016年9月22日上午11:26:17
     * @version 1.0
     * @param accountId
     * @param age
     * @param falg
     * @return
     */
    List<ChildEylfVo> getFollowupEylfcheckLog(@Param("accountId") String accountId, @Param("age") int age, @Param("falg") boolean falg);
    
   List<ChildEylfVo> getYelfInfoByParent(@Param("parentNode")String parentNode,@Param("ageScope")Integer ageScope,@Param("childId") String childId,@Param("flag") boolean flag);

   List<String> getIds();
}