package com.aoyuntek.aoyun.dao.task;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.task.TaskFollowUpInfo;
import com.aoyuntek.aoyun.entity.vo.task.TaskFollowUpInfoVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface TaskFollowUpInfoMapper extends BaseMapper<TaskFollowUpInfo> {

    void deleteByTaskId(String taskId);

    List<TaskFollowUpInfoVo> getTaskFollowUpsOfDate(@Param("day") Date day);

    TaskFollowUpInfoVo getVo(String id);

    String getTaskInstanceId(String id);

}