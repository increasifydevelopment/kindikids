package com.aoyuntek.aoyun.dao.dashboard;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.condtion.dashboard.DashboardCondition;
import com.aoyuntek.aoyun.condtion.dashboard.SelecterCondition;
import com.aoyuntek.aoyun.entity.po.NqsInfo;
import com.aoyuntek.aoyun.entity.vo.dashboard.DashboardGroup;
import com.aoyuntek.aoyun.entity.vo.dashboard.DashboardVo;
import com.aoyuntek.aoyun.entity.vo.dashboard.NqsDashboardVo;
import com.aoyuntek.aoyun.entity.vo.task.PersonSelectVo;

public interface DashboardMapper {
    /**
     * 
     * @description 獲取员工和小孩
     * @author gfwang
     * @create 2016年11月9日下午1:22:41
     * @version 1.0
     * @param condition
     * @return
     */
    List<PersonSelectVo> getSelectStaffList(SelecterCondition condition);

    /**
     * 
     * @description 獲取小孩
     * @author gfwang
     * @create 2016年11月17日上午11:13:50
     * @version 1.0
     * @param condition
     * @return
     */
    List<PersonSelectVo>  getChildList(SelecterCondition condition);
    
    /**
     * 
     * @description 获取园区信息
     * @author gfwang
     * @create 2016年11月9日下午1:22:54
     * @version 1.0
     * @param condition
     * @return
     */
    List<PersonSelectVo> getCenterList(SelecterCondition condition);

    /**
     * 
     * @description
     * @author gfwang
     * @create 2016年11月9日下午1:31:00
     * @version 1.0
     * @param condition
     * @return
     */
    List<DashboardGroup> getRoomList(SelecterCondition condition);

    /**
     * @description 获取已完成Task总数
     * @author hxzhang
     * @create 2016年11月9日下午3:09:30
     */
    int getCompleteTaskTotal(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

    /**
     * @description 分别获取Centre Tasks,Room Tasks,Children Tasks,Other Tasks数量
     * @author hxzhang
     * @create 2016年11月9日下午3:48:53
     */
    int getCount(@Param("startDate") Date startDate, @Param("endDate") Date endDate, @Param("id") String id, @Param("type") short type,
            @Param("taskType") short taskType, @Param("selectType") short selectType);

    /**
     * @description 获取center,room下所有的staff数量
     * @author hxzhang
     * @create 2016年11月9日下午4:22:40
     */
    int getStaffCount(@Param("id") String id, @Param("startDate") Date startDate, @Param("endDate") Date endDate, @Param("type") short type);

    /**
     * 
     * @description 获取小孩，center，room等Activities的总数量
     * @author mingwang
     * @create 2016年11月10日下午1:13:35
     * @version 1.0
     * @param type
     * @param startDate
     * @param endDate
     * @return
     */
    int getActivitiesTotalCount(@Param("type") short type, @Param("id") String id, @Param("startDate") Date startDate, @Param("endDate") Date endDate);

    /**
     * 
     * @description 获取小孩，center，room等单个Activities的数量
     * @author mingwang
     * @create 2016年11月10日下午2:23:49
     * @version 1.0
     * @param activityType
     * @param type
     * @param id
     * @param satrtDate
     * @param endDate
     * @return
     */
    int getActivitiesCount(@Param("activityType") short activityType, @Param("type") short type, @Param("id") String id,
            @Param("startDate") Date startDate, @Param("endDate") Date endDate);

    /**
     * 
     * @description
     * @author gfwang
     * @create 2016年11月10日下午6:07:48
     * @version 1.0
     * @param condition
     * @return
     */
    List<NqsDashboardVo> getNqsStatistics(DashboardCondition condition);

    int getNqsStatisticsCount(DashboardCondition condition);

    List<NqsInfo> getRootNqs(@Param("tag")Short tag);

    /**
     * 
     * @description 获取园区中所有小孩数量
     * @author mingwang
     * @create 2016年11月11日下午3:11:47
     * @version 1.0
     * @param id
     * @param type
     * @return
     */
    int getChildCount(@Param("id") String id, @Param("type") short type, @Param("startDate") Date startDate, @Param("endDate") Date endDate);

    /**
     * 
     * @description 获取staff的评价
     * @author sjwang
     * @create 2016年11月10日上午10:59:27
     * @version 1.0
     * @param condition
     * @return
     */
    List<DashboardVo> getStaffList(@Param("condition") DashboardCondition condition, @Param("id") String id);

    /**
     * 
     * @description 获取centers的评价
     * @author sjwang
     * @create 2016年11月11日下午2:50:57
     * @version 1.0
     * @param condition
     * @param id
     * @return
     */
    List<DashboardVo> getCentersList(@Param("condition") DashboardCondition condition, @Param("id") String id);

    /**
     * 
     * @description 获取room的评价
     * @author sjwang
     * @create 2016年11月11日下午2:51:33
     * @version 1.0
     * @param condition
     * @param id
     * @return
     */
    List<DashboardVo> getRoomVoList(@Param("condition") DashboardCondition condition, @Param("id") String id);

    /**
     * 
     * @description 获取Orgnize的评价
     * @author sjwang
     * @create 2016年11月11日下午3:15:27
     * @version 1.0
     * @param condition
     * @param id
     * @return
     */
    List<DashboardVo> getOrgnizeList(@Param("condition") DashboardCondition condition, @Param("id") String id);

    /**
     * @description 获取完成,逾期完成和过期的task总数
     * @author hxzhang
     * @create 2016年11月15日上午9:52:51
     */
    int getCompleteOverDueTaskTotal(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

    /**
     * @description 获取OnTime,LateCompletion,Overdue各个的数量
     * @author hxzhang
     * @create 2016年11月15日下午1:40:22
     */
    int getProgressCount(@Param("startDate") Date startDate, @Param("endDate") Date endDate, @Param("type") short type,
            @Param("selectType") short selectType, @Param("id") String id);

    /**
     * 
     * @description 获取单个score数量
     * @author mingwang
     * @create 2016年11月16日上午10:30:31
     * @version 1.0
     * @param condition
     * @param id
     * @param type
     * @param score
     * @return
     */
    int getScoreCount(@Param("condition") DashboardCondition condition, @Param("id") String id, @Param("type") short type, @Param("score") short score);

    /**
     * 
     * @description 获取score总数量
     * @author mingwang
     * @create 2016年11月16日上午10:30:35
     * @version 1.0
     * @param condition
     * @param id
     * @param type
     * @param score
     * @return
     */
    int getTotalCount(@Param("condition") DashboardCondition condition, @Param("id") String id, @Param("type") short type, @Param("score") short score);
}