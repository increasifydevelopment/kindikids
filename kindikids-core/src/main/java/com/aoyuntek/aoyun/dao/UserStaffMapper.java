package com.aoyuntek.aoyun.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.condtion.MenuOrgsCondition;
import com.aoyuntek.aoyun.condtion.StaffCondition;
import com.aoyuntek.aoyun.entity.po.AccountInfo;
import com.aoyuntek.aoyun.entity.po.AccountRoleInfo;
import com.aoyuntek.aoyun.entity.po.CentersInfo;
import com.aoyuntek.aoyun.entity.po.RoleInfo;
import com.aoyuntek.aoyun.entity.po.RoomGroupInfo;
import com.aoyuntek.aoyun.entity.po.RoomInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.aoyun.entity.vo.StaffListVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface UserStaffMapper extends BaseMapper<UserInfo> {

	void clearEmploymentEndDay(String id);

	/**
	 * 分页显示员工
	 * 
	 * @author rick
	 * @date 2016年7月26日 下午6:15:35
	 * @param condition
	 * @return
	 */
	List<StaffListVo> getStaffPagerList(StaffCondition condition);

	/**
	 * 分页显示员工总数
	 * 
	 * @author rick
	 * @date 2016年7月26日 下午6:15:35
	 * @param condition
	 * @return
	 */
	int getStaffPagerListCount(StaffCondition condition);

	/**
	 * 
	 * @author rick
	 * @date 2016年7月29日 上午9:18:50
	 * @param condition
	 * @return
	 */
	List<SelecterPo> selectStaffByRole(StaffCondition condition);

	/**
	 * 
	 * @author rick
	 * @date 2016年8月1日 下午2:44:18
	 * @param id
	 * @return
	 */
	UserInfo selectUserById(String id);

	/**
	 * 
	 * @author rick
	 * @date 2016年7月28日 上午10:37:01
	 * @param userId
	 * @return
	 */
	AccountInfo selectAccountByUserId(String userId);

	/**
	 * 检查账号重复
	 * 
	 * @author rick
	 * @date 2016年8月4日 上午8:35:53
	 * @param account
	 * @param userId
	 *            排除
	 * @return
	 */
	int selectAccount(@Param("account") String account, @Param("userId") String userId);

	/**
	 * 通过账户id查询角色列表
	 * 
	 * @author rick
	 * @date 2016年7月28日 上午10:44:28
	 * @param accountId
	 * @return
	 */
	List<AccountRoleInfo> selectAccountRoleByAccountId(String accountId);

	/**
	 * 
	 * @author rick
	 * @date 2016年7月28日 上午11:15:37
	 * @return
	 */
	List<RoleInfo> selectRole();

	/**
	 * 
	 * @author rick
	 * @date 2016年7月28日 上午11:15:40
	 * @return
	 */
	List<RoomInfo> selectRoom(MenuOrgsCondition condition);

	/**
	 * 
	 * @author rick
	 * @date 2016年7月28日 上午11:15:42
	 * @return
	 */
	List<CentersInfo> selectCenters(MenuOrgsCondition condition);

	/**
	 * 
	 * @author rick
	 * @date 2016年8月9日 下午2:19:29
	 * @return
	 */
	List<RoomGroupInfo> selectGroup();

	/**
	 * 逻辑删除
	 * 
	 * @author rick
	 * @date 2016年7月28日 上午11:22:58
	 * @param userId
	 */
	void deleteStaffUser(String userId);

	/**
	 * 
	 * @author rick
	 * @date 2016年8月9日 下午3:24:07
	 * @param userId
	 */
	void deleteStaffAccount(String userId);

	/**
	 * 
	 * @author rick
	 * @date 2016年8月2日 上午8:59:46
	 * @param userId
	 */
	void updateAchive(@Param("userId") String userId, @Param("type") String type);

	/**
	 * 
	 * @author rick
	 * @date 2016年8月2日 上午10:07:39
	 * @param userId
	 */
	void updateUnAchive(String userId);

	/**
	 * 
	 * @author rick
	 * @date 2016年8月4日 上午8:55:58
	 * @param userId
	 */
	void deleteMedical(String userId);

	/**
	 * 
	 * @author rick
	 * @date 2016年8月4日 上午8:55:58
	 * @param userId
	 */
	void deleteMedicalImmu(String userId);

	/**
	 * 
	 * @author rick
	 * @date 2016年8月4日 上午8:55:58
	 * @param userId
	 */
	void deleteEmergency(String userId);

	/**
	 * 
	 * @author rick
	 * @date 2016年8月4日 上午8:55:58
	 * @param userId
	 */
	void deleteCredentials(String userId);

	/**
	 * 
	 * @author rick
	 * @date 2016年8月4日 上午8:55:58
	 * @param userId
	 */
	void deleteAccountRole(String accountId);

	/**
	 * 
	 * @description 更新staff信息
	 * @author mingwang
	 * @create 2016年8月28日下午5:18:22
	 * @version 1.0
	 * @param userId
	 * @param centerId
	 * @param roomId
	 * @param groupId
	 */
	void updateStaffInfo(@Param("userId") String userId, @Param("centerId") String centerId, @Param("roomId") String roomId, @Param("groupId") String groupId);

	/**
	 * 
	 * @description 清除员工的centerId、roomId、groupId
	 * @author mingwang
	 * @create 2016年8月29日上午11:21:02
	 * @version 1.0
	 * @param centersId
	 * @param roomId
	 * @param groupId
	 * @param string
	 * @return
	 */
	int clearStaffCenterInfo(@Param("centersId") String centersId, @Param("roomId") String roomId, @Param("groupId") String groupId, @Param("userId") String userId);

	/**
	 * @description 获取未激活的员工
	 * @author hxzhang
	 * @create 2016年9月2日下午2:41:13
	 * @version 1.0
	 * @return
	 */
	List<UserInfo> getUnActiveStaff();

	List<String> getCentersByFamilyId(@Param("familyId") String familyId);

	List<String> getRoomsByFamilyId(@Param("familyId") String familyId);

	/**
	 * 
	 * @author dlli5 at 2016年10月26日下午12:54:14
	 * @param condition
	 * @return
	 */
	List<RoomInfo> selectRoomWithAttendance(MenuOrgsCondition condition);

	List<CentersInfo> selectCentersWithRoster(MenuOrgsCondition condition);

	/**
	 * 
	 * @description 获取老数据员工的新Id
	 * @author mingwang
	 * @create 2016年12月13日下午3:11:19
	 * @version 1.0
	 * @param create_Account_oldid
	 * @return
	 */
	String getIdByOldId(String oldId);

	/**
	 * 获取全部Staff AccountId
	 * 
	 * @author hxzhang 2019年3月14日
	 * @return
	 */
	List<String> getAllStaffAccountId();

	/**
	 * 
	 * @author hxzhang 2019年3月14日
	 */
	void savaCardIdById(@Param("accountId") String accountId, @Param("cardId") String cardId);

}
