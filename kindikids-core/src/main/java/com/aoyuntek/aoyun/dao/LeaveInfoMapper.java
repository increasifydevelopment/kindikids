package com.aoyuntek.aoyun.dao;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.condtion.AttendanceCondtion;
import com.aoyuntek.aoyun.condtion.LeaveCondition;
import com.aoyuntek.aoyun.entity.po.LeaveInfo;
import com.aoyuntek.aoyun.entity.vo.LeaveListVo;
import com.aoyuntek.framework.dao.BaseMapper;

/**
 * 
 * @description 员工请假模块
 * @author gfwang
 * @create 2016年8月15日上午9:48:02
 * @version 1.0
 */
public interface LeaveInfoMapper extends BaseMapper<LeaveInfo> {

	List<LeaveInfo> getLeaves(@Param("accountId") String accountId, @Param("day") Date day);

	/**
	 * @description 分页获取Leave列表
	 * @author hxzhang
	 * @create 2016年8月16日下午3:21:07
	 * @version 1.0
	 * @param condition
	 *            查询条件
	 * @return 返回查询结果
	 */
	List<LeaveListVo> getLeaveList(LeaveCondition condition);

	/**
	 * @description 日历获取Leave列表
	 * @author abliu
	 * @create 2016年8月16日下午3:21:07
	 * @version 1.0
	 * @param
	 * @return 返回查询结果
	 */
	List<LeaveListVo> getLeaveListByCalendar(LeaveCondition condition);

	/**
	 * @description 获取Leave数量
	 * @author hxzhang
	 * @create 2016年8月16日下午4:01:32
	 * @version 1.0
	 * @param condition
	 *            查询条件
	 * @return 返回查询结果
	 */
	int getLeaveListCount(LeaveCondition condition);

	/**
	 * @description 定时任务更新leave状态
	 * @author hxzhang
	 * @create 2016年8月19日上午11:24:45
	 * @version 1.0
	 * @param lapsed
	 *            失效状态
	 * @param now
	 *            当前时间
	 * @return 返回更新条数
	 */
	int updateLeaveByTimedTask(@Param("status") short status, @Param("now") Date now);

	/**
	 * @description 取消待审批的请假记录
	 * @author hxzhang
	 * @create 2016年8月24日上午11:11:55
	 * @version 1.0
	 * @param accountId
	 *            accountId
	 * @param reason
	 *            取消原因
	 */
	int cancelLeaveByAccountId(@Param("accountId") String accountId, @Param("reason") String reason);

	/**
	 * @description 通过用户ID更新过期的请假申请
	 * @author hxzhang
	 * @create 2016年8月25日下午5:39:55
	 * @version 1.0
	 * @param userId
	 *            用户ID
	 * @param now
	 *            当前时间
	 * @return 返回结果
	 */
	int updateOverdueLeaveInfo(@Param("id") String id);

	/**
	 * 
	 * @description 判断两个时间段内是否有交集
	 * @author gfwang
	 * @create 2016年10月8日下午5:29:31
	 * @version 1.0
	 * @param accountId
	 *            员工Id
	 * @param startDate
	 *            开始日期
	 * @param endDate
	 *            结束日期
	 * @return
	 */
	int getDateRepeatCount(@Param("id") String id, @Param("accountId") String accountId, @Param("startDate") Date startDate, @Param("endDate") Date endDate);

	int getTimeRepeatCount(@Param("id") String id, @Param("accountId") String accountId, @Param("startDate") Date startDate, @Param("endDate") Date endDate);

	/**
	 * 
	 * @author dlli5 at 2016年12月30日下午2:03:35
	 * @param accountId
	 * @param startDate
	 * @return
	 */
	int haveLeave(@Param("accountId") String accountId, @Param("day") Date day);

	int haveLeaveAfterDay(@Param("accountId") String accountId, @Param("day") Date day);

	/**
	 * @description 获取员工的请假信息
	 * @author hxzhang
	 * @create 2016年10月10日上午9:58:17
	 */
	List<LeaveListVo> getStaffLeaves(AttendanceCondtion attendanceCondtion);

	/**
	 * @description 获取小孩请假List
	 * @author hxzhang
	 * @create 2016年12月1日上午10:37:57
	 */
	List<LeaveListVo> getChildAbsenteeList(AttendanceCondtion attendanceCondtion);

}