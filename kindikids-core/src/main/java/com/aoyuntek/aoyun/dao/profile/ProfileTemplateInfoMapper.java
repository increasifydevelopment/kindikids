package com.aoyuntek.aoyun.dao.profile;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.condtion.ProfileTempCondition;
import com.aoyuntek.aoyun.entity.po.profile.ProfileTemplateInfo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface ProfileTemplateInfoMapper extends BaseMapper<ProfileTemplateInfo> {
    /**
     * @description 获取分页集合
     * @author hxzhang
     * @create 2016年10月21日上午10:39:38
     */
    List<ProfileTemplateInfo> getPagerList(ProfileTempCondition condition);

    /**
     * @description 获取总数量
     * @author hxzhang
     * @create 2016年10月21日上午10:48:08
     */
    int getPagerListCount(ProfileTempCondition condition);
    
    /**
     * 
     * @description 
     * @author gfwang
     * @create 2016年10月21日下午12:59:06
     * @version 1.0
     * @param type
     * @return
     */
    List<SelecterPo> getSelectList(Short type);

    /**
     * @description 模版名称唯一性验证
     * @author hxzhang
     * @create 2016年10月21日下午1:11:36
     */
    int validateTempName(@Param("id") String id, @Param("name") String name, @Param("type") short type);

    /**
     * @description
     * @author hxzhang
     * @create 2016年10月21日下午1:59:24
     */
    ProfileTemplateInfo getProfileTempByUserId(@Param("userId")String userId,@Param("templateId")String templateId);
}