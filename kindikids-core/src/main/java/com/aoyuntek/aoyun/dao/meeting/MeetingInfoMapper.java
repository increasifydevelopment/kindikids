package com.aoyuntek.aoyun.dao.meeting;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.condtion.MeetingCondition;
import com.aoyuntek.aoyun.entity.po.meeting.AgendaItemInfo;
import com.aoyuntek.aoyun.entity.po.meeting.MeetingInfo;
import com.aoyuntek.aoyun.entity.vo.meeting.MeetingInfoVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface MeetingInfoMapper extends BaseMapper<MeetingInfo> {
    /**
     * @description
     * @author hxzhang
     * @create 2016年10月17日下午3:07:33
     */
    int getMeetingCountByTempId(@Param("tempId") String tempId);

    /**
     * @description
     * @author hxzhang
     * @create 2016年11月1日下午7:33:23
     */
    int getMeetingCountByTempIdAndDate(@Param("id") String id, @Param("day") Date day);

    /**
     * 
     * @description 通过agendaId获取meetingInfo的数量
     * @author mingwang
     * @create 2016年10月17日下午2:34:47
     * @version 1.0
     * @param agendaId
     * @return
     */
    int selectByAgendaId(String agendaId);

    /**
     * @description 获取会议列表
     * @author hxzhang
     * @create 2016年10月17日下午4:49:53
     */
    List<MeetingInfo> getMeetingManaList(MeetingCondition condition);

    /**
     * @description 获取分页数量
     * @author hxzhang
     * @create 2016年10月17日下午4:58:02
     */
    int getMeetingManaCount(MeetingCondition condition);

    /**
     * 
     * @description 获取个数
     * @author gfwang
     * @create 2016年10月17日下午4:27:26
     * @version 1.0
     * @param id
     * @param name
     * @return
     */
    int getCountByName(@Param("id") String id, @Param("name") String name);

    /**
     * @description 获取会议相关信息
     * @author hxzhang
     * @create 2016年10月18日上午10:27:49
     */
    List<AgendaItemInfo> getItemsByMeetingId(String id);

    /**
     * @description 获取会议表格数据
     * @author hxzhang
     * @create 2016年10月18日上午11:09:06
     */
    MeetingInfoVo getMeetingTable(String id);

    /**
     * @description
     * @author hxzhang
     * @create 2016年10月18日下午5:27:56
     */
    MeetingInfoVo getNqsAndCol(String id);

    /**
     * 
     * @description 删除meeting
     * @author mingwang
     * @create 2016年10月19日上午11:20:34
     * @version 1.0
     * @param meetingId
     */
    void updateDeleteByPrimaryKey(String meetingId);

    /**
     * @description 通过模版ID获取会议数量
     * @author hxzhang
     * @create 2016年10月20日下午1:17:47
     */
    int getMeetingByTempId(String id);

    /**
     * @description 通过会议模版ID删除会议记录
     * @author hxzhang
     * @create 2016年10月24日下午7:37:02
     */
    int removeMeetingByTempId(@Param("id") String id, @Param("day") Date day);

    /**
     * @param rowId
     * @return
     */
    String getTempIdByRowId(String rowId);
}