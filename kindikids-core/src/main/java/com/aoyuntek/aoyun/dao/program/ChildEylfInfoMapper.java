package com.aoyuntek.aoyun.dao.program;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.program.ChildEylfInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface ChildEylfInfoMapper extends BaseMapper<ChildEylfInfo> {
    /**
     * @description 通过小孩accountId,年龄获取Eylf选择的历史信息
     * @author hxzhang
     * @create 2016年9月21日下午9:50:57
     * @version 1.0
     * @param accountId
     * @return
     */
    List<ChildEylfInfo> getChildEylfInfoByChildIdAge(@Param("accountId") String accountId, @Param("age") int age);

    /**
     * @description 批量插入
     * @author hxzhang
     * @create 2016年9月22日下午3:07:38
     * @version 1.0
     * @param ceList
     * @return
     */
    int batchInsertChildEylfInfo(@Param("ceList") List<ChildEylfInfo> ceList);

    /**
     * @description 删除记录
     * @author hxzhang
     * @create 2016年9月22日下午3:29:45
     * @version 1.0
     * @param accountId
     * @param age
     * @return
     */
    int removeChildEylfInfoByAccountIdAge(@Param("accountId") String accountId, @Param("age") int age);

}