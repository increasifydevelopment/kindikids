package com.aoyuntek.aoyun.dao;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.ValueTableInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface ValueTableInfoMapper extends BaseMapper<ValueTableInfo> {
    int insert(ValueTableInfo record);

    int insertSelective(ValueTableInfo record);
    
    List<ValueTableInfo> selectByInstanceId(String instanceId);
    
    int deleteByInstanceId(String instanceId);
}