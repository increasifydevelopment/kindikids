package com.aoyuntek.aoyun.dao;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.StaffEmploymentInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface StaffEmploymentInfoMapper extends BaseMapper<StaffEmploymentInfo> {

	void cancelRequest(String id);

	void cancelRequestByAccountId(String accountId);

	StaffEmploymentInfo getCurrentEmploy(String accountId);

	int getRequestCount(String accountId);

	void removeCurrentEmploy(String accountId);

	List<StaffEmploymentInfo> getList(String accountId);

	List<StaffEmploymentInfo> getEmployByEffectiveDay(Date effectiveDate);

	List<StaffEmploymentInfo> getEmployByLastDay(Date lastDay);

	void cancelCurrentEmploy(String accountId);

	List<StaffEmploymentInfo> getListByAccountIdEffectiveDay(@Param("accountId") String accountId, @Param("date") Date date);

	List<String> getLostTags();
}