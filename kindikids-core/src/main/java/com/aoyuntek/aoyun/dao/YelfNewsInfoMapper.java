package com.aoyuntek.aoyun.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.YelfNewsInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface YelfNewsInfoMapper extends BaseMapper<YelfNewsInfo> {
    /**
     * @description 批量插入YelfNewsInfo
     * @author hxzhang
     * @create 2016年9月21日下午7:45:13
     * @version 1.0
     * @param eylfNewsList
     */
    void insterBatchYelfNewsInfo(@Param("eylfNewsList") List<YelfNewsInfo> eylfNewsList);

    /**
     * @description 通过newsId删除关系
     * @author hxzhang
     * @create 2016年9月21日下午7:51:25
     * @version 1.0
     * @param newsId
     */
    void updateYelfNewsInfoByNewsId(String newsId);
}