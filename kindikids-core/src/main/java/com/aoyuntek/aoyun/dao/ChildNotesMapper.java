package com.aoyuntek.aoyun.dao;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.ChildNotes;
import com.aoyuntek.framework.dao.BaseMapper;

public interface ChildNotesMapper extends BaseMapper<ChildNotes> {
	/**
	 * @author hxzhang 2018年8月18日 上午11:30:39
	 *
	 * @param userId
	 * @return
	 */
	List<ChildNotes> getNotesByUserId(String userId);
}