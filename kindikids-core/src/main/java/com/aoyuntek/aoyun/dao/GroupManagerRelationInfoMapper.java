package com.aoyuntek.aoyun.dao;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.GroupManagerRelationInfo;
import com.aoyuntek.aoyun.entity.vo.UserAvatarVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface GroupManagerRelationInfoMapper extends BaseMapper<GroupManagerRelationInfo> {

	List<UserAvatarVo> getGroupManagers(String groupId);

}