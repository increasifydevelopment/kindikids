package com.aoyuntek.aoyun.dao.task;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.task.TaskVisibleInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface TaskVisibleInfoMapper extends BaseMapper<TaskVisibleInfo>{

    void deleteBySourceId(String sourceId);

    List<TaskVisibleInfo> selectBySourceId(String sourceId);

    int selectByModelCount(TaskVisibleInfo visibleInfoT);
  
}