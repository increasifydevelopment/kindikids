package com.aoyuntek.aoyun.dao.child;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.child.RoomChangeInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface RoomChangeInfoMapper extends BaseMapper<RoomChangeInfo> {

    /**
     * 
     * @author dlli5 at 2016年12月14日上午9:06:06
     * @param childId
     */
    void deleteByChild(String childId);

    /**
     * 
     * @author dlli5 at 2016年12月14日上午9:06:03
     * @param centersId
     * @param roomId
     * @param dayOfWeek
     * @param beginDate
     * @param endDate
     * @return
     */
    List<RoomChangeInfo> getListByDateScope(@Param("centreId") String centersId, @Param("roomId") String roomId, @Param("dayOfWeek") int dayOfWeek,
            @Param("beginDate") Date beginDate, @Param("endDate") Date endDate);

    /**
     * 
     * @author dlli5 at 2016年12月14日上午9:05:59
     * @param id
     * @param operate
     */
    void deleteByReqId(@Param("reqId") String id, @Param("operate") Short operate);

    /**
     * 
     * @author dlli5 at 2016年12月14日上午9:05:57
     * @param childId
     * @param newRoomId
     * @param changeDate
     * @param nextChangeDate
     */
    void deleteConflictWithTemporary(@Param("childId") String childId, @Param("newRoomId") String newRoomId, @Param("changeDate") Date changeDate,
            @Param("nextRoomChangeDate") Date nextRoomChangeDate);

    /**
     * 
     * @author dlli5 at 2016年12月14日上午9:13:30
     * @param childId
     * @param newRoomId
     * @param changeDate
     * @param nextRoomChangeDate
     */
    void deleteConflictWithSubtracted(@Param("childId") String childId, @Param("newRoomId") String newRoomId, @Param("changeDate") Date changeDate,
            @Param("nextRoomChangeDate") Date nextRoomChangeDate);

    /**
     * 
     * @author dlli5 at 2016年12月22日下午3:57:58
     * @param roomId
     * @param childId
     * @param day
     * @param operate
     * @return
     */
    List<RoomChangeInfo> getListByRoomAndDay(@Param("roomId") String roomId, @Param("childId") String childId, @Param("day") Date day,
            @Param("operate") Short operate);

    /**
     * 
     * @author dlli5 at 2016年12月22日下午3:58:04
     * @param childId
     * @param beginDate
     * @param endDate
     * @return
     */
    List<RoomChangeInfo> getListByChild(@Param("childId") String childId, @Param("beginDate") Date beginDate, @Param("endDate") Date endDate);
}