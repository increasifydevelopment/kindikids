package com.aoyuntek.aoyun.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.ChildHealthMedicalDetailInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface ChildHealthMedicalDetailInfoMapper extends BaseMapper<ChildHealthMedicalDetailInfo> {

	void removeHealthMedicalDetail(String id);

	/**
	 * @description 批量插入ChildHealthMedicalDetailInfo
	 * @author hxzhang
	 * @create 2016年7月28日下午7:36:18
	 * @version 1.0
	 * @param healthMedicalDetailList
	 *            List<ChildHealthMedicalDetailInfo>
	 */
	void insterBatchChildHealthMedicalDetailInfo(
			@Param("healthMedicalDetailList") List<ChildHealthMedicalDetailInfo> healthMedicalDetailList);

	/**
	 * @description 通过用户ID删除ChildHealthMedicalDetailInfo记录
	 * @author hxzhang
	 * @create 2016年7月29日下午2:05:08
	 * @version 1.0
	 * @param userId
	 *            用户ID
	 */
	void removeChildHealthMedicalDetailInfoByUserId(String userId);

	List<ChildHealthMedicalDetailInfo> getChildHealthMedicalDetailInfoByUserId(String userId);
}