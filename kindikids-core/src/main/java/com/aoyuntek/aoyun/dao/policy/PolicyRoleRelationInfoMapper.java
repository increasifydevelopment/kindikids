package com.aoyuntek.aoyun.dao.policy;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.policy.PolicyRoleRelationInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface PolicyRoleRelationInfoMapper extends BaseMapper<PolicyRoleRelationInfo> {
    int insert(PolicyRoleRelationInfo record);

    int insertSelective(PolicyRoleRelationInfo record);
    
    /**
     * 批量保存PolicyRoleRelation
     * @param record
     * @return
     */
    int insterBatchVisiblityRelationInfo(List<PolicyRoleRelationInfo> record);
    
    /**
     * 通过policyId删除Role的relation
     * @param policyId
     * @return
     */
    int deleteByPolicyKey(String policyId);
    
}