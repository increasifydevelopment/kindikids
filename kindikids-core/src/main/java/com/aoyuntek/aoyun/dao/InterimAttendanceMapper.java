package com.aoyuntek.aoyun.dao;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.InterimAttendance;
import com.aoyuntek.framework.dao.BaseMapper;

public interface InterimAttendanceMapper extends BaseMapper<InterimAttendance> {

	InterimAttendance getInterimAttend(@Param("accountId") String accountId, @Param("day") Date day);

	void removeForDay(@Param("accountId") String accountId, @Param("day") Date day);

	void removeForAll(@Param("accountId") String accountId);

	List<InterimAttendance> getNowAndFutureList(@Param("accountId") String accountId, @Param("day") Date day);
}