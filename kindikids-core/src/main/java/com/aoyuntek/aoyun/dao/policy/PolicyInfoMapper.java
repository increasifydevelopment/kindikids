package com.aoyuntek.aoyun.dao.policy;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.condtion.PolicyCondition;
import com.aoyuntek.aoyun.entity.po.policy.PolicyInfo;
import com.aoyuntek.aoyun.entity.vo.policy.PolicyInfoVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface PolicyInfoMapper extends BaseMapper<PolicyInfo> {

	PolicyInfoVo selectPolicyInfoVoById(String policyId);
	
	List<PolicyInfoVo> selectPolicyInfoVoList(PolicyCondition condition);
	
	int selectPolicyInfoVoListCount(PolicyCondition condition);
	
	int selectPolicyCountVoByCategoryId(@Param("categoryId")String categoryId);
}