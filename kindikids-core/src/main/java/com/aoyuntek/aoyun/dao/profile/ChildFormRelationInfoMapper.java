package com.aoyuntek.aoyun.dao.profile;

import com.aoyuntek.aoyun.entity.po.profile.ChildFormRelationInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface ChildFormRelationInfoMapper extends BaseMapper<ChildFormRelationInfo> {
    /**
     * @description 通过模版ID判断是否被使用
     * @author hxzhang
     * @create 2016年10月21日下午5:06:53
     */
    int getCountByTempId(String id);
    
    ChildFormRelationInfo getByUserId(String userId);
    
    int updateByUserId(ChildFormRelationInfo record);
}