package com.aoyuntek.aoyun.dao;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.PreviousEnrolment;
import com.aoyuntek.framework.dao.BaseMapper;

public interface PreviousEnrolmentMapper extends BaseMapper<PreviousEnrolment> {
	List<PreviousEnrolment> getListByChild(String userId);

	List<String> getObjIds(String id);
}