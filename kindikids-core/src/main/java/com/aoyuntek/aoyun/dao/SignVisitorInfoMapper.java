package com.aoyuntek.aoyun.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.condtion.SignCondition;
import com.aoyuntek.aoyun.entity.po.SignVisitorInfo;
import com.aoyuntek.aoyun.entity.po.SignVisitorInfoWithBLOBs;
import com.aoyuntek.aoyun.entity.vo.SigninInfoVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface SignVisitorInfoMapper extends BaseMapper<SignVisitorInfoWithBLOBs> {
    /**
     * @description 通过园区ID获取之签入未签出的签到
     * @author hxzhang
     * @create 2016年9月1日下午3:59:33
     * @version 1.0
     * @param centreId
     * @return
     */
    List<SignVisitorInfo> getUnSignOutList(@Param("centreId") String centreId, @Param("params") String params);

    /**
     * @description
     * @author hxzhang
     * @create 2016年9月2日下午4:07:37
     * @version 1.0
     * @param svi
     * @return
     */
    int updateByPrimaryKeyWithBLOBs(SignVisitorInfoWithBLOBs svi);

    /**
     * @description 获取签到信息
     * @author hxzhang
     * @create 2016年9月6日上午8:59:01
     * @version 1.0
     * @param id
     * @return
     */
    SigninInfoVo getSigninInfoVo(String id);

    /**
     * @description 更新签到信息
     * @author hxzhang
     * @create 2016年9月6日上午10:54:33
     * @version 1.0
     * @param signinInfoVo
     * @return
     */
    int updateSigninInfoVo(SigninInfoVo signinInfoVo);

    /**
     * @description 获取导出数据
     * @author hxzhang
     * @create 2016年9月6日下午8:33:39
     * @version 1.0
     * @param condition
     * @return
     */
    List<SigninInfoVo> getExport(SignCondition condition);

    void insertBatchSignVisitorInfo(@Param("list") List<SignVisitorInfoWithBLOBs> list);
}