package com.aoyuntek.aoyun.dao.meeting;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.meeting.AgendaTypeInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface AgendaTypeInfoMapper extends BaseMapper<AgendaTypeInfo> {

    /**
     * 
     * @description 获取所有的AgendaTypeInfo
     * @author mingwang
     * @create 2016年10月13日上午10:37:09
     * @version 1.0
     * @return
     */
    List<AgendaTypeInfo> getAll();

    /**
     * 
     * @description 删除数据库中所有记录（修改deleteFlag状态）
     * @author mingwang
     * @create 2016年10月13日下午1:06:03
     * @version 1.0
     * @return
     */
    int updateDelete();

    /**
     * 
     * @description 根据Id批量删除数据库中记录
     * @author mingwang
     * @create 2016年10月13日下午1:36:35
     * @version 1.0
     * @param agendaTypeList_remove
     * @return
     */
    int batchDeleteAgendaType(List<String> agendaTypeList_remove);

    /**
     * 
     * @description 批量增加agendaType
     * @author mingwang
     * @create 2016年10月13日下午1:44:23
     * @version 1.0
     * @param agendaTypeList
     * @return
     */
    int batchInsertAgendaType(List<AgendaTypeInfo> agendaTypeList);

    /**
     * 
     * @description 批量更新agendaType
     * @author mingwang
     * @create 2016年10月13日下午1:53:14
     * @version 1.0
     * @param agendaTypeList
     * @return
     */
    int batchUpdateAgendaType(List<AgendaTypeInfo> agendaTypeList);
}