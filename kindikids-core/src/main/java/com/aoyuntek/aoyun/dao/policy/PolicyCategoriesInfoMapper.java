package com.aoyuntek.aoyun.dao.policy;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.policy.PolicyCategoriesInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface PolicyCategoriesInfoMapper extends BaseMapper<PolicyCategoriesInfo> {
	/**
	 * 获取分类信息，获取id相同或者delete_flag为0的数据
	 * @param id
	 * @return
	 */
	 List<PolicyCategoriesInfo> selectCategoriesByIdOrDeleteFlag(String id);
}