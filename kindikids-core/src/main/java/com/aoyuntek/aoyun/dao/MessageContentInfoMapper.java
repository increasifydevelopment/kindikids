package com.aoyuntek.aoyun.dao;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.MessageContentInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface MessageContentInfoMapper extends BaseMapper<MessageContentInfo> {
    /**
     * 批量增加
     */
   void insertBatch(List<MessageContentInfo> messageContentInfos);
}