package com.aoyuntek.aoyun.dao;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.NqsRelationInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface NqsRelationInfoMapper extends BaseMapper<NqsRelationInfo> {
    /**
     * @description 批量插入NqsMessageInfo
     * @author hxzhang
     * @create 2016年6月23日下午9:01:50
     * @version 1.0
     * @param nqsMessageInfoList
     *            NqsMessageInfo集合
     */
    void insterBatchNqsRelationInfo(List<NqsRelationInfo> nqsRelationInfoList);

    void removeNqsRelationByObjId(String objId);

}