package com.aoyuntek.aoyun.dao.meeting;

import com.aoyuntek.aoyun.entity.po.meeting.TemplateFrequencyInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface TemplateFrequencyInfoMapper extends BaseMapper<TemplateFrequencyInfo> {
}