package com.aoyuntek.aoyun.dao.program;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.condtion.TaskCondtion;
import com.aoyuntek.aoyun.entity.po.program.ProgramLessonInfo;
import com.aoyuntek.aoyun.entity.vo.ProgramLessonInfoVo;
import com.aoyuntek.aoyun.entity.vo.task.TaskListVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface ProgramLessonInfoMapper extends BaseMapper<ProgramLessonInfo> {

    /**
     * 
     * @description 删除lesson信息
     * @author mingwang
     * @create 2016年9月20日下午2:25:27
     * @version 1.0
     * @param lessonId
     */
    int deleteLesson(@Param("lessonId") String lessonId);

    /**
     * 
     * @description 获取programLessonInfo信息
     * @author mingwang
     * @create 2016年9月21日下午5:52:39
     * @version 1.0
     * @param lessonId
     * @return
     */
    ProgramLessonInfoVo getProgramLessonInfo(@Param("lessonId") String lessonId);

    /**
     * 
     * @description 更新newsfeedId
     * @author mingwang
     * @create 2016年9月22日下午4:43:21
     * @version 1.0
     * @param lessonId
     * @return
     */
    int updateNewsfeedId(@Param("lessonId") String lessonId, @Param("newsfeedId") String newsfeedId);

    /**
     * @description 获取ProgramLessonInfoList
     * @author hxzhang
     * @create 2016年9月22日下午8:20:16
     * @version 1.0
     * @param condtion
     * @return
     */
    List<TaskListVo> getProgramLessonInfoList(TaskCondtion condtion);

    /**
     * 
     * @description 获取过期的lessonId
     * @author mingwang
     * @create 2016年9月28日上午10:12:36
     * @version 1.0
     * @param now
     * @return
     */
    List<String> getOverDueLesson(Date now);

    /**
     * 
     * @description 将过期lesson状态更新为overDue
     * @author mingwang
     * @create 2016年9月28日上午10:22:59
     * @version 1.0
     * @param now
     * @return
     */
    int updateOverDueLesson(Date now);

}