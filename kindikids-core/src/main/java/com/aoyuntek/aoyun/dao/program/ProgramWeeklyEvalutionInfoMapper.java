package com.aoyuntek.aoyun.dao.program;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.program.ProgramWeeklyEvalutionInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface ProgramWeeklyEvalutionInfoMapper extends BaseMapper<ProgramWeeklyEvalutionInfo> {
    /**
     * @description 删除ProgramWeeklyEvalutionInfo记录
     * @author hxzhang
     * @create 2016年9月23日下午3:08:41
     * @version 1.0
     * @param id
     * @return
     */
    int removeProgramWeeklyEvalutionInfo(String id);

    /**
     * @description 获取星期五每个room的WeeklyEvalution对象
     * @author hxzhang
     * @create 2016年9月25日下午3:58:04
     * @version 1.0
     * @param roomId
     * @return
     */
    ProgramWeeklyEvalutionInfo getFridayWeeklyEvalutionByRoomIdAndType(@Param("roomId") String roomId, @Param("now") Date now,
            @Param("type") short type);

    /**
     * @description 获取过期的WeeklyEvalution
     * @author hxzhang
     * @create 2016年9月25日下午8:44:52
     * @version 1.0
     * @param now
     * @return
     */
    List<String> getOverDueWeeklyEvalution(Date now);

    /**
     * @description 将过期记录状态更新为overDue
     * @author hxzhang
     * @create 2016年9月25日下午8:52:34
     * @version 1.0
     * @param now
     * @return
     */
    int updateOverDueWeeklyEvalution(Date now);

    /**
     * 
     * @description 更新newsfeedId
     * @author gfwang
     * @create 2016年9月23日下午9:48:52
     * @version 1.0
     * @param newsfeedId
     * @param id
     * @return
     */
    int updateNewsFeed(@Param("newsfeedId") String newsfeedId, @Param("id") String id);
    void insertList(List<ProgramWeeklyEvalutionInfo> programWeeklyEvalutionInfos);
}