package com.aoyuntek.aoyun.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.condtion.AuthGroupCondition;
import com.aoyuntek.aoyun.entity.po.AuthorityGroupInfo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface AuthorityGroupInfoMapper extends BaseMapper<AuthorityGroupInfo> {

    /**
     * 
     * @description 更新分组名称
     * @author gfwang
     * @create 2016年9月12日下午6:39:45
     * @version 1.0
     * @param newGroupName
     * @param oldGroupName
     * @return
     */
    int updateGroupName(@Param("newGroupName") String newGroupName, @Param("oldGroupName") String oldGroupName);

    /**
     * 
     * @description 获取所有分组
     * @author gfwang
     * @create 2016年9月13日上午9:40:08
     * @version 1.0
     * @return
     */
    List<String> getAllGroups();

    /**
     * 
     * @description 获取form权限组的 下拉框对象
     * @author gfwang
     * @create 2016年9月24日下午4:07:25
     * @version 1.0
     * @param condition
     *            condition
     * @return
     */
    List<SelecterPo> getSelecterGroup(AuthGroupCondition condition);

    /**
     * @description
     * @author gfwang
     * @create 2016年9月24日下午6:30:22
     * @version 1.0
     * @param sql
     * @return
     */
    List<SelecterPo> getSelecterByGroupSql(@Param("sql") String sql);

    /**
     * 
     * @description 此人是否在这个集合里
     * @author gfwang
     * @create 2016年10月26日下午4:06:08
     * @version 1.0
     * @param sql
     * @param accountId
     *            用户
     * @return
     */
    int isInGroup(@Param("sql") String sql, @Param("accountId") String accountId);

    /**
     * 
     * @description groupIds獲取所有sql
     * @author gfwang
     * @create 2016年9月24日下午6:35:48
     * @version 1.0
     * @param list
     * @return
     */
    List<String> getSqlsGroupIds(AuthGroupCondition condition);

    List<AuthorityGroupInfo> getGropList(Short type);

    /**
     * 
     * @description 更改状态
     * @author mingwang
     * @create 2016年11月8日上午11:11:40
     * @version 1.0
     * @param centerId
     * @param value
     */
    void updateAuthorityGroupStatus(@Param("centerId") String centerId, @Param("value") Short value);

    /**
     * 
     * @description 获取id
     * @author mingwang
     * @create 2016年12月14日下午1:35:14
     * @param group_name
     * @return
     */
    String getIdByGroupName(String groupName);

    Integer inGroupTest(@Param("sql")String sql);
    
    /**
     * 
     * @description 
     * @author gfwang
     * @create 2017年5月31日下午10:25:21
     * @version 1.0
     * @param id
     * @return
     */
    String getCenterId(String id);
}