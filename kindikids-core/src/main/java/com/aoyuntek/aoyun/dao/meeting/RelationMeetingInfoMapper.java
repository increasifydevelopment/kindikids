package com.aoyuntek.aoyun.dao.meeting;

import com.aoyuntek.aoyun.entity.po.meeting.RelationMeetingInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface RelationMeetingInfoMapper extends BaseMapper<RelationMeetingInfo> {
    /**
     * @description 获取会议模版使用数量
     * @author hxzhang
     * @create 2016年10月13日下午4:51:10
     */
    public int getRelationMeetingCountByTempId(String id);
}