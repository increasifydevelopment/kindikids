package com.aoyuntek.aoyun.dao.event;

import com.aoyuntek.aoyun.entity.po.event.EventAccountInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface EventAccountInfoMapper extends BaseMapper<EventAccountInfo> {
}