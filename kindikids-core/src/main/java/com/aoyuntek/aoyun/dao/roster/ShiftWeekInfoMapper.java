package com.aoyuntek.aoyun.dao.roster;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.roster.ShiftWeekInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface ShiftWeekInfoMapper extends BaseMapper<ShiftWeekInfo> {

    void deleteByOtherShiftTimeId(@Param("shiftId") String shiftId, @Param("shiftTimeIds") List<String> shiftTimeIds);

}