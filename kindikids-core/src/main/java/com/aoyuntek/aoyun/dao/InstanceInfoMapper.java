package com.aoyuntek.aoyun.dao;

import com.aoyuntek.aoyun.entity.po.InstanceInfo;
import com.aoyuntek.aoyun.entity.vo.InstanceInfoDataVO;
import com.aoyuntek.framework.dao.BaseMapper;

public interface InstanceInfoMapper extends BaseMapper<InstanceInfo> {

	InstanceInfoDataVO selectVOByPrimaryKey(String instanceId);
	
	int getCountByInstanceId(String formId);
	
    int deleteByPrimaryKey(String instanceId);

    int insert(InstanceInfo record);

    int insertSelective(InstanceInfo record);

    InstanceInfo selectByPrimaryKey(String instanceId);

    int updateByPrimaryKeySelective(InstanceInfo record);

    int updateByPrimaryKey(InstanceInfo record);
}