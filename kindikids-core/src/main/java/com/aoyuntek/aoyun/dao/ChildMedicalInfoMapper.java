package com.aoyuntek.aoyun.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.ChildMedicalInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface ChildMedicalInfoMapper extends BaseMapper<ChildMedicalInfo> {

	ChildMedicalInfo getChildMedicalByUserId(@Param("userId") String userId);

	List<ChildMedicalInfo> getHaveMedical(@Param("accountIds") List<String> childAccountIds);

	void removeMedical(String id);
}