package com.aoyuntek.aoyun.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.AccountRoleInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface AccountRoleInfoMapper extends BaseMapper<AccountRoleInfo> {

    void insertBatch(@Param("roleInfoList") List<AccountRoleInfo> roleInfos);

    void deleteTempFlag(@Param("staffAccountId") String staffAccountId);

    void deleteTempRole();

    List<String> getTempRoleAccountIds();

    List<AccountRoleInfo> getRoleInfoByAccountId(String id);

    void removeTempRoleByAccountId(String id);
}