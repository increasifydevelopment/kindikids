package com.aoyuntek.aoyun.dao;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.ValueSelectInfo;

public interface ValueSelectInfoMapper {
    int insert(ValueSelectInfo record);

    int insertSelective(ValueSelectInfo record);

    List<ValueSelectInfo> selectByInstanceId(String instanceId);

    int deleteByInstanceId(String instanceId);
}