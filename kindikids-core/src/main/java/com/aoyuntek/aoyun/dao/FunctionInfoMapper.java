package com.aoyuntek.aoyun.dao;

import java.util.List;
import java.util.Set;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.FunctionInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface FunctionInfoMapper extends BaseMapper<FunctionInfo> {
    /**
     * 
     * @description 获取function集合
     * @author bbq
     * @create 2016年6月22日下午12:42:32
     * @version 1.0
     * @param roleId
     *            用户角色id
     * @param url
     *            URL
     * @return function集合
     */
    Set<FunctionInfo> getFunctionList(@Param("roleId") String roleId, @Param("url") String url);

    /**
     * 
     * @description 根据角色，访问url获取是否有配置权限
     * @author gfwang
     * @create 2016年7月19日下午9:01:56
     * @version 1.0
     * @param roleId
     *            角色
     * @param requestUrl
     *            访问url
     * @return
     */
    int getCountByUrlRole(@Param("roleId") String roleId, @Param("requestUrl") String requestUrl);

    /**
     * 
     * @description 根据类型获取function
     * @author gfwang
     * @create 2016年10月17日上午9:34:13
     * @version 1.0
     * @param functionType
     * @return
     */
    int haveFunctionUrlByType(@Param("functionType") Short functionType,@Param("functionUrl")String functionUrl);
}