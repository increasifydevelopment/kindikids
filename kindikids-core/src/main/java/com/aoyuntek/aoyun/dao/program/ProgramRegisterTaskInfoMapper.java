package com.aoyuntek.aoyun.dao.program;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.program.ProgramRegisterTaskInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface ProgramRegisterTaskInfoMapper extends BaseMapper<ProgramRegisterTaskInfo> {

    /**
     * 
     * @description 批量插入ProgramRegisterTaskInfo
     * @author mingwang
     * @create 2016年9月25日下午3:04:23
     * @version 1.0
     * @param taskInfos
     * @return
     */
    int batchInsert(List<ProgramRegisterTaskInfo> taskInfos);

    /**
     * 
     * @description 通过roomId和taskType获取id
     * @author mingwang
     * @create 2016年9月27日上午11:18:53
     * @version 1.0
     * @param roomId
     * @param taskType
     * @return
     */
    String getIdByRoomAndType(@Param("roomId") String roomId, @Param("taskType") short taskType, @Param("day") Date date);

    /**
     * 
     * @description 获取过期的Register
     * @author mingwang
     * @create 2016年9月28日下午2:21:06
     * @version 1.0
     * @param now
     * @return
     */
    List<String> getOverDueRegister(@Param("now") Date now, @Param("taskType") short taskType);

    /**
     * 
     * @description 将过期Register状态更新为overDue
     * @author mingwang
     * @create 2016年9月28日下午2:21:15
     * @version 1.0
     * @param now
     * @return
     */
    int updateOverDueRegister(@Param("now") Date now, @Param("taskType") short taskTypes);

    /**
     * @description 修改状态
     * @author gfwang
     * @create 2016年9月28日下午8:27:12
     * @version 1.0
     * @param state
     * @param itemId
     * @return
     */
    int updateStateByItem(@Param("state") Short state, @Param("id") String id);

    /**
     * 
     * @description 获取当天创建的数量
     * @author mingwang
     * @create 2016年9月30日下午4:21:58
     * @version 1.0
     * @param id
     * @param taskType
     * @return
     */
    int getCountByRoomAndType(@Param("roomId") String roomId, @Param("taskType") short taskType, @Param("day") Date day);
}