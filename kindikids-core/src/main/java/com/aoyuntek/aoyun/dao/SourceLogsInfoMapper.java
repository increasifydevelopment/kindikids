package com.aoyuntek.aoyun.dao;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.SourceLogsInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface SourceLogsInfoMapper extends BaseMapper<SourceLogsInfo> {

	List<SourceLogsInfo> getInfoBySourceId(String sourceId);

}