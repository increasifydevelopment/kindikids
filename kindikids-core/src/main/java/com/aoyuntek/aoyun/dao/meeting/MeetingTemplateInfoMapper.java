package com.aoyuntek.aoyun.dao.meeting;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.condtion.MeetingCondition;
import com.aoyuntek.aoyun.entity.po.meeting.MeetingTemplateInfo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.aoyun.entity.vo.meeting.MeetingTempVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface MeetingTemplateInfoMapper extends BaseMapper<MeetingTemplateInfo> {
    /**
     * @description 获取会议模版列表
     * @author hxzhang
     * @create 2016年10月13日上午11:04:19
     */
    public List<MeetingTemplateInfo> getMeetingTempList(MeetingCondition condition);

    /**
     * @description 获取会议模版列表总数量
     * @author hxzhang
     * @create 2016年10月13日下午3:43:54
     */
    public int getMeetingTempListCount(MeetingCondition condition);

    /**
     * @description 获取会议模版信息
     * @author hxzhang
     * @create 2016年10月13日下午3:45:07
     */
    public MeetingTempVo getMeetingTempInfo(String id);

    /**
     * @description 验证会议模版名称是否重复
     * @author hxzhang
     * @create 2016年10月14日上午9:42:57
     */
    public int validateMeetingTempName(@Param("name") String name, @Param("id") String id);

    /**
     * @description 禁用启用会议模版
     * @author hxzhang
     * @create 2016年10月14日上午11:36:31
     */
    public int updateMeetingTempStateById(@Param("id") String id, @Param("state") short state);

    /**
     * @description 获取所有模版
     * @author hxzhang
     * @create 2016年10月18日上午9:14:55
     */
    public List<MeetingTempVo> getAllMeetingTemp();

    /**
     * @description 以SelecterPo查询最新模版
     * @author hxzhang
     * @create 2016年10月18日上午9:16:08
     */
    public List<SelecterPo> getMeetingTempForSelecterPo();

}