package com.aoyuntek.aoyun.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.condtion.AuthGroupCondition;
import com.aoyuntek.aoyun.entity.po.RoleGroupRelationInfo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface RoleGroupRelationInfoMapper extends BaseMapper<RoleGroupRelationInfo> {

    /**
     * 
     * @description 批量插入
     * @author gfwang
     * @create 2016年9月12日下午2:35:05
     * @version 1.0
     * @param relations
     *            relations
     * @return
     */
    int inserBatch(@Param("relations") List<RoleGroupRelationInfo> relations);

    /**
     * 
     * @description 根据roleId 获取当前角色所拥有的分组
     * @author gfwang
     * @create 2016年9月12日下午3:25:50
     * @version 1.0
     * @param roleId
     * @param centerId
     * @return
     */
    List<SelecterPo> getGroupsByRoleId(AuthGroupCondition condition);

    /**
     * 
     * @description 根据分组名称获取sql
     * @author gfwang
     * @create 2016年9月12日下午4:03:04
     * @version 1.0
     * @param groupName
     *            分组名称
     * @return String
     */
    String getSqlByGroup(@Param("groupId") String groupId);
}