package com.aoyuntek.aoyun.dao;

import com.aoyuntek.aoyun.entity.po.ChildHealthMedicalInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface ChildHealthMedicalInfoMapper extends BaseMapper<ChildHealthMedicalInfo> {
    ChildHealthMedicalInfo getChildHealthMedicalInfoByUserId(String userId);
}