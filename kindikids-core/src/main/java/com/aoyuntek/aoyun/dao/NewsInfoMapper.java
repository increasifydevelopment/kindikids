package com.aoyuntek.aoyun.dao;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.condtion.NewsCondition;
import com.aoyuntek.aoyun.condtion.NewsFeedAuthorityCondition;
import com.aoyuntek.aoyun.entity.po.NewsInfo;
import com.aoyuntek.aoyun.entity.vo.NewsFeedInfoVo;
import com.aoyuntek.aoyun.entity.vo.NewsInfoListVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface NewsInfoMapper extends BaseMapper<NewsInfo> {

	void setParentComment(@Param("newsId") String newsId, @Param("flag") boolean flag);

	List<String> newFeedbackCeo(NewsCondition condition);

	List<String> newFeedbackCM(NewsCondition condition);

	List<String> newFeedbackEdu(NewsCondition condition);

	List<String> newFeedbackCook(NewsCondition condition);

	List<String> getNewsIdByVisibility(@Param("centerId") String centerId);

	/**
	 * 
	 * @description 根据查询条件获取微博分页
	 * @author bbq
	 * @create 2016年6月27日下午4:00:41
	 * @version 1.0
	 * @param condition
	 *            查询条件
	 * @return 微博集合
	 */
	List<NewsInfoListVo> getPagerList(NewsCondition condition);

	List<NewsInfoListVo> getPagerListCEO(NewsCondition condition);

	int getPagerListCountCEO(NewsCondition condition);

	List<NewsInfoListVo> getPagerListCM(NewsCondition condition);

	int getPagerListCountCM(NewsCondition condition);

	List<NewsInfoListVo> getPagerListEdu(NewsCondition condition);

	int getPagerListCountEdu(NewsCondition condition);

	List<NewsInfoListVo> getPagerListCook(NewsCondition condition);

	int getPagerListCountCook(NewsCondition condition);

	List<NewsInfoListVo> getPagerListCasual(NewsCondition condition);

	int getPagerListCountCasual(NewsCondition condition);

	List<NewsInfoListVo> getPagerListParent(NewsCondition condition);

	int getPagerListCountParent(NewsCondition condition);

	List<String> getNewsIdByFamilyId(String familyId);

	/**
	 * 
	 * @description 打获取一条微博及其评论
	 * @author bbq
	 * @create 2016年6月27日下午4:30:48
	 * @version 1.0
	 * @param newsId
	 *            微博Id
	 * @param centersId
	 *            当前园区Id
	 * @return NewsInfoVO
	 */
	NewsFeedInfoVo getNews(@Param("newsId") String newsId);

	/**
	 * 
	 * @description 删除微博及其评论
	 * @author bbq
	 * @create 2016年6月27日下午5:29:47
	 * @version 1.0
	 * @param newsId
	 * @return
	 */
	int updateNewsAndConmments(@Param("newsId") String newsId, @Param("accountId") String account, @Param("date") Date date);

	/**
	 * 
	 * @description 删除评论
	 * @author bbq
	 * @create 2016年6月27日下午5:48:58
	 * @version 1.0
	 * @param newsCommentId
	 *            评论Id
	 * @return 影响行数
	 */
	int updateNewsComment(@Param("newsCommentId") String newsCommentId, @Param("accountId") String account, @Param("date") Date date);

	/**
	 * 
	 * @description 获取分页总数
	 * @author bbq
	 * @create 2016年6月30日下午2:09:40
	 * @version 1.0
	 * @param condition
	 *            查询条件
	 * @return 记录总条数
	 */
	int getPagerListCount(NewsCondition condition);

	/**
	 * @description 通过评论ID获取微博
	 * @author hxzhang
	 * @create 2016年7月20日下午4:02:48
	 * @version 1.0
	 * @param id
	 *            评论ID
	 * @return 返回查询结果
	 */
	NewsInfo getNewsInfoByNewsCommentId(String id);

	/**
	 * 
	 * @description 获取当前人可查看news的
	 * @author gfwang
	 * @create 2016年7月21日上午8:39:57
	 * @version 1.0
	 * @param condition
	 *            条件
	 * @return ids
	 */
	List<String> getNewsIdByCondition(NewsFeedAuthorityCondition condition);

	/**
	 * 通过tag小孩的id 查找今天是否创建过birthday
	 * 
	 * @param accountId
	 * @param roomId
	 * @param createTime
	 * @param centreId
	 * @return
	 */
	int getBirthdayListCount(@Param("accountId") String accountId, @Param("centreId") String centreId, @Param("roomId") String roomId,
			@Param("createTime") Date createTime);

	/**
	 * @description 获取微博内容
	 * @author Hxzhang
	 * @create 2016年9月28日下午7:19:22
	 */
	String getContentByNewsId(String newsId);

	/**
	 * @description 通过newsId删除该news的评论
	 * @author Hxzhang
	 * @create 2016年10月8日下午3:22:20
	 */
	int removeCommentByNewsId(String id);

	/**
	 * 
	 * @description 判断当天的newsfeed是否已发送
	 * @author mingwang
	 * @create 2016年10月11日上午10:53:11
	 * @version 1.0
	 * @param value
	 * @param date
	 * @return
	 */
	int getTodaysMenu(@Param("newsType") Short newsType, @Param("date") Date date);

	String getNewIdByOldId(@Param("oldId") String oldId);

}