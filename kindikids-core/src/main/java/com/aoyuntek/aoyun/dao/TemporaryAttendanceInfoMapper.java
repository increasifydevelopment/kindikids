package com.aoyuntek.aoyun.dao;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.TemporaryAttendanceInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface TemporaryAttendanceInfoMapper extends BaseMapper<TemporaryAttendanceInfo> {

	void deleteOld(String childId);

	List<TemporaryAttendanceInfo> getWillAttendance(@Param("type") short type, @Param("beginTime") Date beginTime);

	List<TemporaryAttendanceInfo> getListByChildAndDateScope(@Param("accountId") String accountId, @Param("beginDate") Date beginDate, @Param("endDate") Date endDate);

	void deleteConflict(@Param("childId") String childId, @Param("newRoomId") String newRoomId, @Param("changeDate") Date changeDate,
			@Param("nextRoomChangeDate") Date nextRoomChangeDate);

	List<TemporaryAttendanceInfo> getDayAttendances(@Param("beginDate") Date beginDate, @Param("childId") String childId);

	List<TemporaryAttendanceInfo> getReaplaceChilds(@Param("accountId") String accountId, @Param("day") Date day, @Param("dayOfWeek") int dayOfWeek);

	List<TemporaryAttendanceInfo> getListByChild(@Param("accountId") String accountId);
}