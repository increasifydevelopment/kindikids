package com.aoyuntek.aoyun.dao;

import com.aoyuntek.aoyun.entity.po.CalendarInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface CalendarInfoMapper extends BaseMapper<CalendarInfo> {

}