package com.aoyuntek.aoyun.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.ChildDietaryRequireInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface ChildDietaryRequireInfoMapper extends BaseMapper<ChildDietaryRequireInfo> {

	void removeDietaryRequire(String id);

	/**
	 * 
	 * @description userID获取ChildDietaryRequireInfo list
	 * @author gfwang
	 * @create 2016年8月3日下午9:07:27
	 * @version 1.0
	 * @param userId
	 * @return
	 */
	ChildDietaryRequireInfo getChildDietaryRequireByUserId(@Param("userId") String userId);

	/**
	 * 
	 * @author dlli5 at 2016年9月26日下午5:31:44
	 * @param childAccountIds
	 * @return
	 */
	List<ChildDietaryRequireInfo> getHaveDietaryRequire(@Param("accountIds") List<String> childAccountIds);
}