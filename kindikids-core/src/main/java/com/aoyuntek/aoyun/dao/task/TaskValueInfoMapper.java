package com.aoyuntek.aoyun.dao.task;

import com.aoyuntek.aoyun.entity.po.task.TaskValueInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface TaskValueInfoMapper extends BaseMapper<TaskValueInfo>{
   
}