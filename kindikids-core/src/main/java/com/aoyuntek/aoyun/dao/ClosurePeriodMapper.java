package com.aoyuntek.aoyun.dao;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.ClosurePeriod;
import com.aoyuntek.framework.dao.BaseMapper;

public interface ClosurePeriodMapper extends BaseMapper<ClosurePeriod> {

	List<ClosurePeriod> getClosurePeriod(@Param("day") Date day, @Param("centreId") String centreId);

	int getRepeatCount(@Param("formDate") Date formDate, @Param("toDate") Date toDate, @Param("id") String id, @Param("centres") List<String> centres);

	void removeClosurePeriod(String id);

	int getRepeatBycentreIdDate(@Param("centreId") String centreId, @Param("startDate") Date startDate, @Param("endDate") Date endDate);

	int getCountByCentreIdDate(@Param("centreId") String centreId, @Param("date") Date date);

	List<ClosurePeriod> getListByCentreIdDate(@Param("centreId") String centreId, @Param("date") Date date);

}