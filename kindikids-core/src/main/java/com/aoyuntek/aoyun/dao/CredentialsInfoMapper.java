package com.aoyuntek.aoyun.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.CredentialsInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface CredentialsInfoMapper extends BaseMapper<CredentialsInfo> {

	List<CredentialsInfo> selectByUserId(String userId);

	void insertBatch(@Param("credentialsList") List<CredentialsInfo> credentialsList);

}