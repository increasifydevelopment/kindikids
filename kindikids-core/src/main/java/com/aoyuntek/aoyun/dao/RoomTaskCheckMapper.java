package com.aoyuntek.aoyun.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.RoomTaskCheck;
import com.aoyuntek.framework.dao.BaseMapper;

public interface RoomTaskCheckMapper extends BaseMapper<RoomTaskCheck> {
    int deleteByPrimaryKey(String id);

    int insert(RoomTaskCheck record);

    int insertSelective(RoomTaskCheck record);

    RoomTaskCheck selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(RoomTaskCheck record);

    int updateByPrimaryKey(RoomTaskCheck record);

    void insertBatch(@Param("list") List<RoomTaskCheck> list);

    List<RoomTaskCheck> getCheckList(String roomId);

    void updateCheckById(@Param("id") String id, @Param("check") boolean check);

    List<Short> getTaskTypeByRoomId(String roomId);

    int getTaskCountByRoomIdTaskType(@Param("roomId") String roomId, @Param("taskType") short taskType);
}