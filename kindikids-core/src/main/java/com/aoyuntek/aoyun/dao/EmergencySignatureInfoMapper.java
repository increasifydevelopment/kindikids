package com.aoyuntek.aoyun.dao;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.EmergencySignatureInfo;

public interface EmergencySignatureInfoMapper {
	int insert(EmergencySignatureInfo record);

	int insertSelective(EmergencySignatureInfo record);

	EmergencySignatureInfo getInfoByUserId(String userId);

	void updateInfoByUserId(@Param("userId") String userId, @Param("emergencySignature") String emergencySignature);
}