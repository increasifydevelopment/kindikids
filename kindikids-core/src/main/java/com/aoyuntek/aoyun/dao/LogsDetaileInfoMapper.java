package com.aoyuntek.aoyun.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.condtion.LogsCondition;
import com.aoyuntek.aoyun.entity.po.LogsDetaileInfo;
import com.aoyuntek.aoyun.entity.vo.LogsDetaileVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface LogsDetaileInfoMapper extends BaseMapper<LogsDetaileInfo> {

	/**
	 * 查找logs集合
	 * 
	 * @param condition
	 * @return
	 */
	List<LogsDetaileVo> getLogsList(LogsCondition condition);

	/**
	 * 
	 * @param condition
	 * @return
	 */
	int getLogsCount(LogsCondition condition);

	LogsDetaileInfo getLogsDetaile(String sourceLogsId);
}