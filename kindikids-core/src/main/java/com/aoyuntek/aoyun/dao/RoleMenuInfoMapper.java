package com.aoyuntek.aoyun.dao;

import com.aoyuntek.aoyun.entity.po.RoleMenuInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface RoleMenuInfoMapper extends BaseMapper<RoleMenuInfo> {

}