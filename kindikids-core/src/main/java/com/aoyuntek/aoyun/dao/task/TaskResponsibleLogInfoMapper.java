package com.aoyuntek.aoyun.dao.task;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.task.TaskResponsibleLogInfo;
import com.aoyuntek.aoyun.entity.vo.base.UserVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface TaskResponsibleLogInfoMapper extends BaseMapper<TaskResponsibleLogInfo> {

	/**
	 * 
	 * @author dlli5 at 2016年9月27日下午6:59:30
	 * @param taskInstanceId
	 * @return
	 */
	List<String> getByInstanceId(@Param("taskInstanceId") String taskInstanceId);

	void deleteByInstanceId(@Param("taskInstanceId") String taskInstanceId);

	List<UserVo> getResponsibleUsers(@Param("taskInstanceId") String taskInstanceId);

	List<String> getTaskIdByByInstanceId(@Param("list") List<String> list);
}