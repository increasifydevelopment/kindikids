package com.aoyuntek.aoyun.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.ChildLogsInfo;
import com.aoyuntek.aoyun.entity.vo.ChildLogsVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface ChildLogsInfoMapper extends BaseMapper<ChildLogsInfo> {
	/**
	 * 
	 * @description 获取日志
	 * @author gfwang
	 * @create 2016年9月5日下午7:03:19
	 * @version 1.0
	 * @param userId
	 * @return
	 */
	List<ChildLogsVo> getLogList(@Param("userId") String userId);

	/**
	 * 
	 * @description logId
	 * @author gfwang
	 * @create 2016年9月5日下午7:44:15
	 * @version 1.0
	 * @param logId
	 *            logId
	 * @return int
	 */
	int deleteLogById(@Param("logId") String logId);

	void updateAttachIdNull(String logId);

	void updateNotesUserId(@Param("oldUserId") String oldUserId, @Param("newUserId") String newUserId);
}