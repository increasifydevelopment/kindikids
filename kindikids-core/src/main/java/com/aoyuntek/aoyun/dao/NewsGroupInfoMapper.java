package com.aoyuntek.aoyun.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.NewsGroupInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface NewsGroupInfoMapper extends BaseMapper<NewsGroupInfo> {
    /**
     * @description 批量插入NewsGroupInfo数据
     * @author hxzhang
     * @create 2016年7月12日上午8:56:44
     * @version 1.0
     * @param newsGroupInfos
     *            NewsGroupInfo对象集合
     */
    void insterBatchNewsGroupInfo(@Param("newsGroupInfos") List<NewsGroupInfo> newsGroupInfos);

    /**
     * @description 通过newsId跟新分组信息
     * @author hxzhang
     * @create 2016年7月12日下午2:46:16
     * @version 1.0
     * @param newsId
     *            微博ID
     */
    void updateNewsGroupInfoByNewsId(String newsId);

    /**
     * @description 根据newsId获取对应的分组信息
     * @author hxzhang
     * @create 2016年7月21日下午3:03:08
     * @version 1.0
     * @param newsId
     *            微博ID
     * @return 返回查询结果
     */
    List<String> getGroupIdByNewsId(String newsId);
}