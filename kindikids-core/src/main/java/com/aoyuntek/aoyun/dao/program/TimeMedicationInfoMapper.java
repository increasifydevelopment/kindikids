package com.aoyuntek.aoyun.dao.program;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.program.TimeMedicationInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface TimeMedicationInfoMapper extends BaseMapper<TimeMedicationInfo> {
 
    /**
     * 
     * @description 批量插入TimeMedicationInfo
     * @author mingwang
     * @create 2016年9月26日下午5:11:01
     * @version 1.0
     * @param timeMedicationInfos
     */
    void batchInsertMedications(List<TimeMedicationInfo> list);

    /**
     * 
     * @description 通过timeMedicationId删除TimeMedicationInfo
     * @author mingwang
     * @create 2016年9月26日下午6:16:36
     * @version 1.0
     * @param timeMedicationId
     */
    void deleteByProgramMedicationId(String timeMedicationId);

    /**
     * 
     * @description 通过timeMedicationId获取TimeMedicationInfo
     * @author mingwang
     * @create 2016年9月26日下午6:27:09
     * @version 1.0
     * @param timeMedicationId
     * @return
     */
    List<TimeMedicationInfo> selectByTimeMedicationId(String timeMedicationId);

    /**
     * 
     * @description 批量更新TimeMedicationInfo
     * @author mingwang
     * @create 2016年9月26日下午7:48:10
     * @version 1.0
     * @param timeMedicationInfo_update
     */
    void batchUpdateMedications(List<TimeMedicationInfo> timeMedicationInfo_update);

    /**
     * 
     * @description 删除timeMedication(通过timeMedicationId删除)
     * @author mingwang
     * @create 2016年10月10日下午5:09:21
     * @version 1.0
     * @param dbTimeMedicationId
     */
    void updateDeleteByMedicationId(String timeMedicationId);

    /**
     * 
     * @description 批量删除timeMedication（修改deleteFlag状态）
     * @author mingwang
     * @create 2016年10月10日下午5:14:28
     * @version 1.0
     * @param timeMedicationInfo_remove
     * @return
     */
    int batchDeleteTimeMedication(List<String> timeMedicationInfo_remove);

}