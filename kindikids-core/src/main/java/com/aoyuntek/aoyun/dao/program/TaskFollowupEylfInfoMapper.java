package com.aoyuntek.aoyun.dao.program;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.program.TaskFollowupEylfInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface TaskFollowupEylfInfoMapper extends BaseMapper<TaskFollowupEylfInfo> {
    /**
     * @description 通过followUpId获取TaskFollowupEylfInfo
     * @author hxzhang
     * @create 2016年9月21日下午9:20:05
     * @version 1.0
     * @param followUpId
     * @return
     */
    List<TaskFollowupEylfInfo> getFollowupEylfInfoByFollowUpId(String followUpId);

    /**
     * @description 批量插入
     * @author hxzhang
     * @create 2016年9月22日下午2:52:15
     * @version 1.0
     * @param tfeList
     */
    int batchInsertTaskFollowupEylfInfo(@Param("tfeList") List<TaskFollowupEylfInfo> tfeList);

    /**
     * @description 通过followUpId删除记录
     * @author hxzhang
     * @create 2016年9月22日下午3:21:11
     * @version 1.0
     */
    int removeFollowupEylfInfoByFollowupId(String followupId);
}