package com.aoyuntek.aoyun.dao.program;

import com.aoyuntek.aoyun.entity.po.program.TaskFollowupEylfcheckInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface TaskFollowupEylfcheckInfoMapper extends BaseMapper<TaskFollowupEylfcheckInfo> {

}