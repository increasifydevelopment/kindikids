package com.aoyuntek.aoyun.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.condtion.waitingList.WaitingListCondition;
import com.aoyuntek.aoyun.entity.po.ApplicationList;
import com.aoyuntek.aoyun.entity.vo.QueuevVo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.aoyun.entity.vo.waiting.ParentVo;
import com.aoyuntek.aoyun.entity.vo.waiting.WaitingCenterVo;
import com.aoyuntek.aoyun.entity.vo.waiting.WaitingListVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface ApplicationListMapper extends BaseMapper<ApplicationList> {

	List<WaitingListVo> getWaitingList(WaitingListCondition condition);

	List<WaitingListVo> getOutChild(WaitingListCondition condition);

	int getWaitingListCount(WaitingListCondition condition);

	ApplicationList getChildDetails(String id);

	List<ParentVo> getParents(String id);

	List<String> getAppIdsForSelectCenter(@Param("selectCenter") List<String> selectCenter);

	List<String> getAppIdsForParentName(String name);

	List<String> getAppIdsForParentMobile(String mobile);

	int getApplictionCount(WaitingListCondition condition);

	int getArchivedCount(WaitingListCondition condition);

	int getExternalCount(WaitingListCondition condition);

	int getSiblingCount(WaitingListCondition condition);

	void updatePositionDate(@Param("id") String id, @Param("date") Date date);

	void allocateType(@Param("appId") String appId, @Param("type") short type);

	List<String> getUserIdsForSelectCenter(@Param("selectCenter") List<String> selectCenter);

	List<SelecterPo> getFamilySelect(@Param("p") String p);

	int getQueue(@Param("objId") String objId, @Param("type") short type);

	List<QueuevVo> getQueueForType(@Param("type") short type);

	List<String> searchPeople(String param);

	List<String> getCenterIds(String id);

	List<WaitingCenterVo> getAllCenter();

	List<String> getObjIdBySelectCenterId(@Param("centerIds") List<String> centerIds);

	ApplicationList getRequestByUserId(String userId);
}