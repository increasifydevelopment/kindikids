package com.aoyuntek.aoyun.dao.task;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.task.TaskInfo;
import com.aoyuntek.aoyun.entity.vo.task.TaskInfoVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface TaskInfoMapper extends BaseMapper<TaskInfo> {

    /**
     * 
     * @author dlli5 at 2016年10月12日下午5:43:49
     * @param day
     * @return
     */
    List<TaskInfoVo> getUserTask(@Param("day") Date day);

    /**
     * 
     * @description 验证task名称重复
     * @author gfwang
     * @create 2016年9月25日上午10:07:08
     * @version 1.0
     * @param taskId
     * @param taskName
     */
    int validateTaskName(@Param("taskId") String taskId, @Param("taskName") String taskName);

    /**
     * 
     * @author dlli5 at 2016年10月18日下午4:27:09
     * @param centreId
     * @return
     */
    List<TaskInfo> getShiftTasks(@Param("centreId") String centreId);

    /**
     * 
     * @author dlli5 at 2016年10月20日下午2:32:52
     * @param id
     * @return
     */
    TaskInfoVo getVo(String id);

    /**
     * 是否可用
     * 
     * @author dlli5 at 2017年1月19日下午8:46:14
     * @param id
     * @return
     */
    int isAble(String id);

    List<String> getTaskIdByCode(String code);
}