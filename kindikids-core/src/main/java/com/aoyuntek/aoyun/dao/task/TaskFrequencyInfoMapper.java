package com.aoyuntek.aoyun.dao.task;

import com.aoyuntek.aoyun.entity.po.task.TaskFrequencyInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface TaskFrequencyInfoMapper extends BaseMapper<TaskFrequencyInfo>{

    void deleteByTaskId(String taskId);

    TaskFrequencyInfo selectByTaskId(String taskId);
    
}