package com.aoyuntek.aoyun.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.ChildCustodyArrangeInfo;
import com.aoyuntek.aoyun.entity.vo.family.ChildCustodyArrangeVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface ChildCustodyArrangeInfoMapper extends BaseMapper<ChildCustodyArrangeInfo> {
    /**
     * @description 通过familyId获取ChildCustodyArrangeInfo
     * @author hxzhang
     * @create 2016年8月3日下午8:38:31
     * @version 1.0
     * @param userId
     *            userId
     * @return ChildCustodyArrangeInfo
     */
    ChildCustodyArrangeInfo getChildCustodyArrangeInfoByUserId(String userId);

    /**
     * 
     * @author dlli5 at 2016年10月20日上午9:39:47
     * @param childAccountIds
     * @return
     */
    List<ChildCustodyArrangeVo> getListOfTodayNote(@Param("accountIds") List<String> childAccountIds);

}