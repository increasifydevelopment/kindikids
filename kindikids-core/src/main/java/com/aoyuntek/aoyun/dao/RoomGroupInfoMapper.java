package com.aoyuntek.aoyun.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.RoomGroupInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.vo.GroupInfoVo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface RoomGroupInfoMapper extends BaseMapper<RoomGroupInfo> {
    List<SelecterPo> selecterGroups(@Param("roomId") String roomId, @Param("groupId") String groupId, @Param("p") String p);

    /**
     * @description 通过园区ID获取分组信息
     * @author hxzhang
     * @create 2016年8月5日下午3:37:12
     * @version 1.0
     * @param centerId
     *            园区ID
     * @return List<RoomGroupInfo>
     */
    List<RoomGroupInfo> getRoomGroupInfo();

    /**
     * 
     * @description 校验当前room下 group名称是否重复
     * @author gfwang
     * @create 2016年8月20日上午11:13:19
     * @version 1.0
     * @param roomId
     *            当前room
     * @param groupId
     *            group
     * @param name
     *            名称
     * @return 个数
     */
    int validateName(@Param("roomId") String roomId, @Param("groupId") String groupId, @Param("name") String name);

    /**
     * 
     * @description 获取分组信息
     * @author gfwang
     * @create 2016年8月22日下午1:42:08
     * @version 1.0
     * @param groupId
     *            分组Id
     * @return GroupInfoVo
     */
    GroupInfoVo getGroupInfo(@Param("groupId") String groupId);

    /**
     * 
     * @description 获取分组下小孩
     * @author gfwang
     * @create 2016年8月22日下午1:50:04
     * @version 1.0
     * @param groupId
     *            分组id
     * @return UserInfo
     */
    List<UserInfo> getChildsByGroup(@Param("groupId") String groupId);

    /**
     * 
     * @description 通过groupId获取成员
     * @author mingwang
     * @create 2016年8月22日上午10:43:47
     * @version 1.0
     * @param groupId
     * @return
     */
    List<String> getMembersByGroupId(@Param("groupId") String groupId);

    /**
     * 
     * @description 判断group下的老师和学生是否全部归档
     * @author mingwang
     * @create 2016年8月22日上午11:09:44
     * @version 1.0
     * @param userIdList
     * @return
     */
    int isAllArchived(List<String> userIdList);

    /**
     * 
     * @description 归档/解除归档group
     * @author mingwang
     * @create 2016年8月22日上午11:15:48
     * @version 1.0
     * @param short1
     * @param groupId
     * @return
     */
    int updateStatusByGroupId(@Param("groupId") String groupId, @Param("status") short status);

    /**
     * 
     * @description 清除分组负责人
     * @author gfwang
     * @create 2016年8月25日上午11:19:20
     * @version 1.0
     * @param userId
     *            用户
     * @return 手影响的行数
     */
    int clearGroupEduer(@Param("userId") String userId);

    /**
     * 
     * @description 增加分组负责人
     * @author mingwang
     * @create 2016年8月26日下午4:46:09
     * @version 1.0
     * @param userId
     * @return
     */
    int addGroupEduer(@Param("userId") String userId);

    /**
     * 
     * @description 判断group中的老师是否被占用
     * @author mingwang
     * @create 2016年8月26日下午5:28:43
     * @version 1.0
     * @param userId
     * @return
     */
    String isEducatorInUse(@Param("userId") String userId);

    /**
     * 
     * @description 根据userId判断group是否归档
     * @author mingwang
     * @create 2016年8月26日下午6:07:39
     * @version 1.0
     * @param userId
     * @return
     */
    int isGroupAchived(@Param("userId") String userId);

    /**
     * 
     * @description 根据groupId获取roomId
     * @author mingwang
     * @create 2016年9月1日下午4:30:55
     * @version 1.0
     * @param id
     * @return
     */
    String selectRoomIdByGroup(@Param("groupId") String groupId);

    /**
     * 
     * @description 根据userInfo判断小孩的group是否存在
     * @author mingwang
     * @create 2016年9月7日下午3:57:05
     * @version 1.0
     * @param child
     * @return
     */
    int isGroupExist(UserInfo child);

    /**
     * 
     * @description 通过id获取GroupName
     * @author mingwang
     * @create 2016年11月30日下午1:33:45
     * @version 1.0
     * @param id
     * @return
     */
    String getGroupNameById(String id);

    /**
     * 
     * @description 根据老id获取
     * @author gfwang
     * @create 2017年1月16日上午9:08:11
     * @version 1.0
     * @return
     */
    String getByOldId(@Param("oldId") String oldId);
}