package com.aoyuntek.aoyun.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.LeavePaidInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface LeavePaidInfoMapper extends BaseMapper<LeavePaidInfo> {
    /**
     * @description 批量插入
     * @author hxzhang
     * @create 2016年8月17日上午10:51:07
     * @version 1.0
     * @param lpiList
     */
    void insertBatch(@Param("lpiList") List<LeavePaidInfo> lpiList);

    /**
     * @description 获取请假时间列表
     * @author hxzhang
     * @create 2016年8月17日上午11:38:12
     * @version 1.0
     * @param leaveId
     *            请假条ID
     * @return 返回查询结果
     */
    List<LeavePaidInfo> getLeavePaidInfoList(String leaveId);

    /**
     * @description 删除请假时间记录
     * @author hxzhang
     * @create 2016年8月18日上午8:45:31
     * @version 1.0
     * @param leaveId
     *            请假条ID
     */
    void removeLeavePaidInfoByLeaveId(String leaveId);
}