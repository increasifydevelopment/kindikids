package com.aoyuntek.aoyun.dao.roster;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.roster.RosterInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface RosterInfoMapper extends BaseMapper<RosterInfo> {

    int haveUsed(@Param("shiftId") String shiftId, @Param("dayOfWeek") Date dayOfWeek);

    void updateShiftId(@Param("oldId") String oldId, @Param("newId") String newId);

    List<RosterInfo> getRosterStaffTemplates(@Param("centerId") String centerId);

    List<RosterInfo> getListByTaskAndDate(@Param("taskId") String id, @Param("startDate") Date weekStart);

    void deleteRoster(@Param("rosterId") String rosterId);

    void updateFutureShiftId(@Param("todayWeekStart") Date todayWeelStart, @Param("oldShiftId") String oldShiftId,
            @Param("newShiftId") String newShiftId);

    RosterInfo getByName(@Param("templateName") String templateName, @Param("templateFlag") Short templateFlag,@Param("centreId")String centreId);

    int haveUsedOfShiftTime(@Param("shiftId") String shiftId, @Param("dayOfWeek") Date dayOfWeek);

    void updateFutureShiftIdOfShiftTime(@Param("todayWeekStart") Date todayWeelStart, @Param("oldShiftId") String oldShiftId,
            @Param("newShiftId") String newShiftId);
}