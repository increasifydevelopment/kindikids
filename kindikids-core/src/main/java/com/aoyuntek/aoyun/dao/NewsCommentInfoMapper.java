package com.aoyuntek.aoyun.dao;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.NewsCommentInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface NewsCommentInfoMapper extends BaseMapper<NewsCommentInfo> {
	List<NewsCommentInfo> getListByNewsId(String newsId);
}