package com.aoyuntek.aoyun.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.CentersInfo;
import com.aoyuntek.aoyun.entity.vo.CenterInfoVo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.aoyun.entity.vo.UserInGroupInfo;
import com.aoyuntek.framework.condtion.BaseCondition;
import com.aoyuntek.framework.dao.BaseMapper;

public interface CentersInfoMapper extends BaseMapper<CentersInfo> {

	Short getRegionBycentreId(String centreId);

	List<CentersInfo> getCentersByIds(@Param("ids") List<String> ids);

	/**
	 * @description 通过孩子的accountId获取其园区
	 * @author hxzhang
	 * @create 2017年1月3日下午8:35:53
	 */
	CentersInfo getCenterByChildAccountId(String accountId);

	/**
	 * 
	 * @description 根据园区编号获取园区id
	 * @author gfwang
	 * @create 2016年7月22日下午1:18:40
	 * @version 1.0
	 * @param grade
	 *            编号
	 * @return id
	 */
	String getCenterIdByGrade(@Param("centerName") String centerName);

	/**
	 * 
	 * @description 获取当前园区下小孩对应的家长(用于根据分组名称获取人)
	 * @author gfwang
	 * @create 2016年7月20日下午4:26:15
	 * @version 1.0
	 * @param centerId
	 * @return
	 */
	List<UserInGroupInfo> getParentByKidCenter(@Param("centerId") String centerId);

	List<SelecterPo> selecterCenters(@Param("centerId") String centerId, @Param("p") String p);

	/**
	 * @description 通过园区ID获取园区信息
	 * @author hxzhang
	 * @create 2016年8月5日下午3:16:41
	 * @version 1.0
	 * @param centerId
	 *            园区ID
	 * @param isActive
	 * @return List<CentersInfo>
	 */
	List<CentersInfo> getCentersInfo(@Param("centerId") String centerId, @Param("isActive") String isActive);

	/**
	 * 
	 * @description 通过园区name获取园区信息
	 * @author mingwang
	 * @create 2016年8月18日下午3:45:52
	 * @version 1.0
	 * @param centerName
	 *            园区name
	 * @return
	 */
	List<CentersInfo> getCenterInfoByName(String centerName);

	/**
	 * 
	 * @description 校验名称重复性
	 * @author gfwang
	 * @create 2016年8月19日上午9:18:48
	 * @version 1.0
	 * @param name
	 *            园区名称
	 * @param centerId
	 *            园区Id
	 * @return 个数
	 */
	int validateName(@Param("name") String name, @Param("centerId") String centerId);

	/**
	 * 
	 * @description 获取园区列表
	 * @author mingwang
	 * @create 2016年8月19日上午9:05:46
	 * @version 1.0
	 * @param condition
	 * @return
	 */
	List<CentersInfo> getCenterList(BaseCondition condition);

	/**
	 * 
	 * @description 获取分页总数
	 * @author mingwang
	 * @create 2016年8月19日上午9:08:00
	 * @version 1.0
	 * @param condition
	 * @return
	 */
	int getCentersCount(BaseCondition condition);

	/**
	 * 
	 * @description 判断当前center下所有的room是否归档
	 * @author mingwang
	 * @create 2016年8月22日下午2:19:26
	 * @version 1.0
	 * @param centerId
	 * @return
	 */
	int isAllArchived(@Param("centerId") String centerId);

	/**
	 * 
	 * @description 归档/解除归档center
	 * @author mingwang
	 * @create 2016年8月22日下午2:25:35
	 * @version 1.0
	 * @param centerId
	 * @param value
	 * @return
	 */
	int updateStatusByCenterId(@Param("centerId") String centerId, @Param("status") short status);

	/**
	 * 
	 * @description 获取园区基本信息
	 * @author gfwang
	 * @create 2016年8月22日下午4:20:54
	 * @version 1.0
	 * @param centerId
	 *            centerId
	 * @return CenterInfoVo
	 */
	CenterInfoVo getCenterInfoVo(@Param("centerId") String centerId);

	/**
	 * 
	 * @description 通过centerId获取当前园区所有的userId
	 * @author mingwang
	 * @create 2016年8月25日下午4:02:54
	 * @version 1.0
	 * @param centerId
	 * @return
	 */
	List<String> getUserListByCenterId(@Param("centerId") String centerId);

	/**
	 * 
	 * @description 判断当前园区所有user是否归档
	 * @author mingwang
	 * @create 2016年8月23日上午10:44:38
	 * @version 1.0
	 * @param staffIdList
	 * @return
	 */
	int isAllUserArchived(List<String> staffIdList);

	/**
	 * 
	 * @description 清除园区负责人
	 * @author gfwang
	 * @create 2016年8月25日上午11:16:08
	 * @version 1.0
	 * @param userId
	 *            当前负责人
	 * @return 受影响的行数
	 */
	int clearCenterLead(@Param("userId") String userId);

	/**
	 * 
	 * @description 增加园区负责人
	 * @author mingwang
	 * @create 2016年8月26日下午4:45:36
	 * @version 1.0
	 * @param userId
	 * @return
	 */
	int addCenterLead(@Param("userId") String userId);

	/**
	 * 
	 * @description 判断园区负责人是否被占用
	 * @author mingwang
	 * @create 2016年8月26日下午5:21:41
	 * @version 1.0
	 * @param userId
	 * @return
	 */
	String isCentersLeaderInUse(@Param("userId") String userId);

	/**
	 * 
	 * @description 根据userId判断center是否归档
	 * @author mingwang
	 * @create 2016年8月26日下午6:07:03
	 * @version 1.0
	 * @param userId
	 * @return
	 */
	int isCentersAchived(@Param("userId") String userId);

	/**
	 * 
	 * @description 根据centerId判断center是否归档
	 * @author mingwang
	 * @create 2016年9月6日下午6:10:42
	 * @version 1.0
	 * @param centerId
	 * @return
	 */
	int isCentersAchivedById(@Param("centerId") String centerId);

	/**
	 * 
	 * @description 根据小孩信息判断所在center是否存在
	 * @author mingwang
	 * @create 2016年9月8日下午5:35:38
	 * @version 1.0
	 * @param child
	 * @return
	 */
	int isCenterExist(String id);

	/**
	 * 
	 * @description 根据roomId判断center是否存在
	 * @author mingwang
	 * @create 2016年9月9日上午11:27:39
	 * @version 1.0
	 * @param roomId
	 * @return
	 */
	int isCentersAchivedByRoomId(@Param("roomId") String roomId);

	/**
	 * @description
	 * @author hxzhang
	 * @create 2016年9月9日下午5:49:10
	 * @version 1.0
	 * @return
	 */
	CentersInfo getCentreInfoByCentreName(String params);

	/**
	 * @description 获取所有未归档的center
	 * @author Hxzhang
	 * @create 2016年10月10日上午11:48:57
	 */
	List<CentersInfo> getAllCentreInfo();

	/**
	 * @description 通过roomId获取centerId
	 * @author hxzhang
	 * @create 2016年11月14日下午1:31:53
	 */
	String getCenterIdByRoomId(String roomId);

	/**
	 * 
	 * @description 通过groupId获取centerId
	 * @author mingwang
	 * @create 2016年11月22日下午2:25:05
	 * @version 1.0
	 * @param groupId
	 * @return
	 */
	String getCenterIdByGroupId(String groupId);

	/**
	 * 
	 * @description 通过id获取centerName
	 * @author mingwang
	 * @create 2016年11月30日下午1:27:59
	 * @version 1.0
	 * @param id
	 * @return
	 */
	String getCenterNameById(String id);

	/**
	 * 
	 * @description 老id获取新id
	 * @author gfwang
	 * @create 2016年12月6日下午2:16:36
	 * @version 1.0
	 * @param oldId
	 * @return
	 */
	String getIdByOldId(@Param("oldId") String oldId);

	/**
	 * @description 获取Center详细信息
	 * @author hxzhang
	 * @create 2016年12月8日下午1:15:25
	 */
	List<CentersInfo> getAllCentersInfos();

	/**
	 * @description 通过历史数据获取CenterId
	 * @author hxzhang
	 * @create 2016年12月14日下午5:20:14
	 */
	String getCenterIdByOldId(String id);

	List<SelecterPo> getMapId();

	/**
	 * 
	 * @description 追加centerIp
	 * @author mingwang
	 * @create 2017年2月10日下午2:07:34
	 * @version 1.0
	 * @param centerIp
	 */
	void updateCentreIp(@Param("id") String id, @Param("centerIp") String centerIp);

	/**
	 * @description 获取所有center
	 * @author hxzhang
	 * @create 2017年2月28日上午9:51:26
	 */
	List<CentersInfo> getAllCenterForEvent();

	List<String> getStaffAccountIdByCenterId(String centerId);

	Map<String, String> getAllCentreRoomGroup();

	List<String> getCentreIds(@Param("ids") List<String> ids);
}