package com.aoyuntek.aoyun.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.MedicalImmunisationInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface MedicalImmunisationInfoMapper extends BaseMapper<MedicalImmunisationInfo> {

	List<MedicalImmunisationInfo> selectByUserId(String userId);

	void insertBatch(@Param("immunisationList") List<MedicalImmunisationInfo> immunisationList);

}