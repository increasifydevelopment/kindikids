package com.aoyuntek.aoyun.dao;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.NewsReceiveInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface NewsReceiveInfoMapper extends BaseMapper<NewsReceiveInfo> {
    /**
     * @description 批量插入NewsReceiveInfo
     * @author hxzhang
     * @create 2016年6月23日下午8:41:21
     * @version 1.0
     * @param newsReceiveInfoList
     *            newsReceiveInfoList集合
     */
    void insterBatchNewsReceiveInfo(@Param("newsReceiveInfoList") List<NewsReceiveInfo> newsReceiveInfoList);

    /**
     * @description 通过newsId删除NewsReceive记录
     * @author hxzhang
     * @create 2016年6月28日下午3:14:54
     * @version 1.0
     * @param newsId
     *            newsId
     */
    void updateNewsReceiveByNewsId(String newsId);

    /**
     * @description 通过newsId和accountId判断关系是否建立
     * @author hxzhang
     * @create 2016年8月25日上午10:07:59
     * @version 1.0
     * @param newsId
     *            newsId
     * @param accountId
     *            accountId
     * @return 返回结果
     */
    int getUnNewsReceiveByNewsIdAccountId(@Param("newsId") String newsId, @Param("accountId") String accountId);

    List<String> getNewsIdByCenterId(@Param("centerId") String centerId, @Param("startDate") Date startDate, @Param("endDate") Date endDate);

    List<String> getNewsIdByRoomId(@Param("roomId") String roomId, @Param("startDate") Date startDate, @Param("endDate") Date endDate);
}