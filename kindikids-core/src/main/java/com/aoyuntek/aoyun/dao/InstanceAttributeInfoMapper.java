package com.aoyuntek.aoyun.dao;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.InstanceAttributeInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface InstanceAttributeInfoMapper extends BaseMapper<InstanceAttributeInfo> {
    int insert(InstanceAttributeInfo record);

    int insertSelective(InstanceAttributeInfo record);
    
    List<InstanceAttributeInfo> selectByInstanceId(String instanceId);
    
    int deleteByInstanceId(String instanceId);
}