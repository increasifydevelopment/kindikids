package com.aoyuntek.aoyun.dao.program;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.program.ProgramNqsInfo;
import com.aoyuntek.aoyun.entity.vo.NqsVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface ProgramNqsInfoMapper extends BaseMapper<ProgramNqsInfo> {
	int insert(ProgramNqsInfo record);

	int insertSelective(ProgramNqsInfo record);

	void removeNqsList(String taskId);

	List<NqsVo> getNqsVo(String taskId);

}