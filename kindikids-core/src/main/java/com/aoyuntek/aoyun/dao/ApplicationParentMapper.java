package com.aoyuntek.aoyun.dao;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.ApplicationParent;
import com.aoyuntek.framework.dao.BaseMapper;

public interface ApplicationParentMapper extends BaseMapper<ApplicationParent> {
	int deleteByPrimaryKey(String id);

	int insert(ApplicationParent record);

	int insertSelective(ApplicationParent record);

	ApplicationParent selectByPrimaryKey(String id);

	int updateByPrimaryKeySelective(ApplicationParent record);

	int updateByPrimaryKey(ApplicationParent record);

	List<ApplicationParent> getParentDetails(String id);

}