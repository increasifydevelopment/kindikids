package com.aoyuntek.aoyun.dao.program;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.program.TaskProgramFollowUpInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface TaskProgramFollowUpInfoMapper extends BaseMapper<TaskProgramFollowUpInfo> {

    void batchInsertFollowup(@Param("list") List<TaskProgramFollowUpInfo> list);

    /**
     * @description 通过TaskId获取followup信息
     * @author hxzhang
     * @create 2016年9月22日下午4:18:51
     * @version 1.0
     * @return
     */
    TaskProgramFollowUpInfo getTaskProgramFollowUpInfoByTaskId(String taskId);

    /**
     * 
     * @description 更新NewsfeedId
     * @author mingwang
     * @create 2016年9月23日上午11:19:24
     * @version 1.0
     * @param objId
     * @param newsfeedId
     * @return
     */
    int updateNewsfeedId(@Param("id") String id, @Param("newsfeedId") String newsfeedId);

    /**
     * @description
     * @author hxzhang
     * @create 2016年9月25日下午3:32:15
     * @version 1.0
     * @param roomId
     * @param type
     * @return
     */
    TaskProgramFollowUpInfo getOnliyOneFollowupByRoomIdType(@Param("roomId") String roomId, @Param("type") short type);

    /**
     * @description 获取过期的ProgramFollowup
     * @author hxzhang
     * @create 2016年9月25日下午7:59:35
     * @version 1.0
     * @param now
     * @return
     */
    List<String> getOverDueProgramFollowup(Date now);

    /**
     * @description 将过期的Followup更新为overDue
     * @author hxzhang
     * @create 2016年9月25日下午8:12:25
     * @version 1.0
     * @param now
     * @return
     */
    int updateOverDueProgramFollowup(Date now);

    /**
     * @description 通过newsId获取TaskProgramFollowUpInfo
     * @author Hxzhang
     * @create 2016年9月26日下午7:56:08
     */
    TaskProgramFollowUpInfo getFollowUpInfoByNewsId(String newsId);

    /**
     * @description 通过taskId删除followup
     * @author Hxzhang
     * @create 2016年9月28日上午10:36:02
     */
    int removeFollowupByTaskId(String taskId);
}