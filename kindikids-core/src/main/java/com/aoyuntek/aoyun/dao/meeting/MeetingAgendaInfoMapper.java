package com.aoyuntek.aoyun.dao.meeting;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.condtion.MeetingAgendaCondition;
import com.aoyuntek.aoyun.entity.po.meeting.MeetingAgendaInfo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.aoyun.entity.vo.meeting.MeetingAgendaInfoVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface MeetingAgendaInfoMapper extends BaseMapper<MeetingAgendaInfo> {

    /**
     * 
     * @description 获取MeetingAgendaInfoVo
     * @author mingwang
     * @create 2016年10月13日下午3:35:00
     * @version 1.0
     * @param id
     * @return
     */
    MeetingAgendaInfoVo getMeetinAgendaById(String id);

    /**
     * 
     * @description 获取MeetingAgendaList
     * @author mingwang
     * @create 2016年10月14日上午10:18:49
     * @version 1.0
     * @param condition
     * @return
     */
    List<MeetingAgendaInfo> getMeetingAgendaList(MeetingAgendaCondition condition);

    /**
     * 
     * @description 获取MeetingAgendaList总数量
     * @author mingwang
     * @create 2016年10月14日上午10:19:07
     * @version 1.0
     * @param condition
     * @return
     */
    int getMeetingAgendaListCount(MeetingAgendaCondition condition);

    /**
     * 
     * @description 验证MeetingAgendaName是否重复
     * @author mingwang
     * @create 2016年10月14日下午2:34:43
     * @version 1.0
     * @param agendaName
     * @param id
     * @return
     */
    int validateMeetingAgendaName(@Param("name") String name, @Param("id") String id);

    /**
     * 
     * @description 修改MeetingAgendaState状态
     * @author mingwang
     * @create 2016年10月14日下午4:24:37
     * @version 1.0
     * @param agendaState
     * @param id
     * @return
     */
    int updateMeetingAgendaState(@Param("agendaState") short agendaState, @Param("id") String id);

    /**
     * 
     * @description 获取agenda列表
     * @author mingwang
     * @create 2016年10月17日下午2:07:08
     * @version 1.0
     * @return
     */
    List<SelecterPo> getAgendaList();

    /**
     * 
     * @description 更改版本状态
     * @author mingwang
     * @create 2016年10月17日下午2:55:07
     * @version 1.0
     * @param value
     */
    void updateCurrentFlag(@Param("value") Short value, @Param("agendaId") String agendaId);
}