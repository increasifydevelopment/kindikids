package com.aoyuntek.aoyun.dao.meeting;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.meeting.AgendaItemInfo;
import com.aoyuntek.aoyun.entity.vo.meeting.AgendaItemInfoVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface AgendaItemInfoMapper extends BaseMapper<AgendaItemInfo> {

    /**
     * 
     * @description 获取AgendaItemInfo
     * @author mingwang
     * @create 2016年10月13日下午4:53:13
     * @version 1.0
     * @param id
     * @return
     */
    List<AgendaItemInfoVo> getItemsByAgendaId(String id);

    /**
     * 
     * @description 批量增加AgendaItemInfoVo
     * @author mingwang
     * @create 2016年10月14日上午9:35:00
     * @version 1.0
     * @param agendaItems
     * @return
     */
    int batchInsertAgendaItems(List<AgendaItemInfo> agendaItems);

    /**
     * 
     * @description 通过agendaId删除Items
     * @author mingwang
     * @create 2016年10月14日上午9:49:04
     * @version 1.0
     * @param id
     */
    void updateDeleteByAgendaId(String id);

}