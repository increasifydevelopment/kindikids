package com.aoyuntek.aoyun.dao;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.entity.po.ChangeAttendanceRequestInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface ChangeAttendanceRequestInfoMapper extends BaseMapper<ChangeAttendanceRequestInfo> {

	List<ChangeAttendanceRequestInfo> getListByChild(String childId);

	/**
	 * 
	 * @author dlli5 at 2016年8月23日上午8:54:43
	 * @param childId
	 * @param value
	 * @return
	 */
	List<ChangeAttendanceRequestInfo> getListByChildAndState(@Param("childId") String childId, @Param("value") Short value);

	/**
	 * 
	 * @author dlli5 at 2016年11月17日下午2:02:27
	 * @param childId
	 * @param type
	 * @param value
	 * @return
	 */
	List<ChangeAttendanceRequestInfo> getListByChildAndStateAndType(@Param("childId") String childId, @Param("type") Short type,
			@Param("value") Short value);

	/**
	 * 
	 * @author dlli5 at 2016年8月23日上午11:03:48
	 * @param childId
	 * @param value
	 */
	void updateState(@Param("childId") String childId, @Param("state") Short value);

	/**
	 * 
	 * @author dlli5 at 2016年8月29日上午10:14:48
	 * @param changeDate
	 * @return
	 */
	List<ChangeAttendanceRequestInfo> getAllJobRequest(Date changeDate);

	/**
	 * 
	 * @author dlli5 at 2016年8月31日上午10:12:58
	 * @param id
	 * @param value
	 * @param value2
	 */
	void updateStateForType(@Param("childId") String childId, @Param("type") Short type, @Param("state") Short state);

	/**
	 * 
	 * @author dlli5 at 2016年8月31日下午1:47:07
	 * @param childId
	 * @param type
	 * @param state
	 * @param changeDate
	 */
	void updateStateForDay(@Param("childId") String childId, @Param("type") Short type, @Param("state") Short state,
			@Param("changeDate") Date changeDate);

	/**
	 * 
	 * @author dlli5 at 2016年8月31日上午11:45:59
	 * @param id
	 * @param changeDay
	 * @return
	 */
	List<ChangeAttendanceRequestInfo> getListByChildAndDate(@Param("childId") String childId, @Param("changeDate") Date changeDate);

	/**
	 * 
	 * @author dlli5 at 2016年9月5日下午6:43:29
	 * @param childId
	 * @param state
	 * @return
	 */
	int getCountByChildAndState(@Param("childId") String childId, @Param("type") Short type, @Param("state") Short state);

	/**
	 * 
	 * @author dlli5 at 2016年9月5日下午6:43:33
	 * @param id
	 */
	void deleteOld(@Param("childId") String childId);

	/**
	 * 
	 * @author dlli5 at 2016年9月7日下午9:18:32
	 * @param childId
	 * @param beginTime
	 * @param endTime
	 * @return
	 */
	List<ChangeAttendanceRequestInfo> haveAttendanceRequestScope(@Param("childId") String childId, @Param("roomId") String roomId,
			@Param("dayOfWeek") int dayOfWeek, @Param("beginTime") Date beginTime, @Param("endTime") Date endTime);

	/**
	 * 
	 * @author dlli5 at 2016年12月20日下午7:27:48
	 * @param childId
	 * @param beginTime
	 * @param endTime
	 * @return
	 */
	List<ChangeAttendanceRequestInfo> haveChangeCentreRequest(@Param("childId") String childId, @Param("endTime") Date endTime);

	/**
	 * 
	 * @author dlli5 at 2016年12月19日下午3:32:06
	 * @param childId
	 * @param roomId
	 * @param endTime
	 * @return
	 */
	List<ChangeAttendanceRequestInfo> haveChangeCenterRequestScope(@Param("childId") String childId, @Param("roomId") String roomId,
			@Param("endTime") Date endTime);

	/**
	 * 
	 * @author dlli5 at 2016年12月19日下午3:32:04
	 * @param attendanceRequestInfo
	 * @return
	 */
	int haveSameRequest(ChangeAttendanceRequestInfo attendanceRequestInfo);

	/**
	 * 
	 * @author dlli5 at 2016年12月19日下午3:32:01
	 * @param accountId
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	List<ChangeAttendanceRequestInfo> getListByChildAndDateScope(@Param("accountId") String accountId, @Param("beginDate") Date beginDate,
			@Param("endDate") Date endDate);

	/**
	 * 
	 * @author dlli5 at 2016年12月8日下午2:50:56
	 * @param id
	 * @param value
	 * @param changeDate
	 */
	void updateStateByFutureDate(@Param("childId") String childId, @Param("state") Short state, @Param("date") Date date);

	/**
	 * 
	 * @author dlli5 at 2016年12月14日上午9:21:12
	 * @param re
	 * @return
	 */
	int getRequestOfDay(ChangeAttendanceRequestInfo re);

	/**
	 * 
	 * @author dlli5 at 2016年12月14日上午9:21:17
	 * @param re
	 * @return
	 */
	int getBeforeGreaterThanRequest(ChangeAttendanceRequestInfo re);

	/**
	 * 
	 * @author dlli5 at 2016年12月19日下午3:34:46
	 * @param re
	 * @return
	 */
	int getAfterLessGreaterThanRequest(ChangeAttendanceRequestInfo re);

}