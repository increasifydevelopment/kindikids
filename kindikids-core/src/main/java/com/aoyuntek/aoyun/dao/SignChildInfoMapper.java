package com.aoyuntek.aoyun.dao;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.condtion.SignCondition;
import com.aoyuntek.aoyun.entity.po.SignChildInfoWithBLOBs;
import com.aoyuntek.aoyun.entity.po.old.ChildSignin;
import com.aoyuntek.aoyun.entity.vo.SigninInfoVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface SignChildInfoMapper extends BaseMapper<SignChildInfoWithBLOBs> {
    /**
     * @description 获取小孩的签到信息
     * @author hxzhang
     * @create 2016年9月12日下午4:20:38
     * @version 1.0
     * @return
     */
    SigninInfoVo getSigninInfoVo(@Param("id") String id, @Param("date") Date date);

    /**
     * @description 更新小孩主表信息
     * @author hxzhang
     * @create 2016年9月18日上午9:29:55
     * @version 1.0
     * @param sci
     */
    void updateByPrimaryKeyWithBLOBs(SignChildInfoWithBLOBs sci);

    /**
     * @description 通过小孩accountId获取其签到子表信息
     * @author hxzhang
     * @create 2016年9月18日下午4:05:07
     * @version 1.0
     * @param accountId
     * @return
     */
    SignChildInfoWithBLOBs getSignChildInfoWithBLOBsByAccountId(@Param("id") String id, @Param("date") Date date);

    /**
     * @description 获取导出数据
     * @author hxzhang
     * @create 2016年9月19日上午11:41:51
     * @version 1.0
     * @param condition
     * @return
     */
    List<SigninInfoVo> getExport(SignCondition condition);

    void insertBatchSignChildInfo(@Param("list") List<SignChildInfoWithBLOBs> list);

    int insertOldBatch(@Param("list") List<ChildSignin> list);

    List<ChildSignin> getOldList();
}