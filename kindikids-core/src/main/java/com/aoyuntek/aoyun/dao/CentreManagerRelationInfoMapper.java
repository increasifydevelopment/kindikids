package com.aoyuntek.aoyun.dao;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.CentreManagerRelationInfo;
import com.aoyuntek.aoyun.entity.vo.UserAvatarVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface CentreManagerRelationInfoMapper extends BaseMapper<CentreManagerRelationInfo> {

	List<UserAvatarVo> getCentreManagers(String centreId);

}