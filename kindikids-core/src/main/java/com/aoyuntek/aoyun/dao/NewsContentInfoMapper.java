package com.aoyuntek.aoyun.dao;

import com.aoyuntek.aoyun.entity.po.NewsContentInfo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface NewsContentInfoMapper extends BaseMapper<NewsContentInfo> {
    /**
     * @description 根据newsId获取NewsContentInfo
     * @author hxzhang
     * @create 2016年7月13日下午7:14:40
     * @version 1.0
     * @param newsId
     *            微博ID
     * @return NewsContentInfo对象
     */
    NewsContentInfo getNewsContentInfoByNewsId(String newsId);
}