package com.aoyuntek.aoyun.dao.program;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.condtion.TaskCondtion;
import com.aoyuntek.aoyun.entity.po.RoomGroupInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramIntobsleaInfo;
import com.aoyuntek.aoyun.entity.vo.ProgramIntobsleaInfoVo;
import com.aoyuntek.aoyun.entity.vo.task.TaskListVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface ProgramIntobsleaInfoMapper extends BaseMapper<ProgramIntobsleaInfo> {

    String getTaskIdByOldId(String oldId);

    void batchInsertIntobsleaInfo(@Param("list") List<ProgramIntobsleaInfo> list);

    /**
     * 
     * @description 删除intobslea信息
     * @author mingwang
     * @create 2016年9月21日下午2:06:00
     * @version 1.0
     * @param intobsleaId
     * @return
     */
    int deleteIntobslea(@Param("intobsleaId") String intobsleaId);

    /**
     * 
     * @description 通过intobsleaId获取ProgramIntobsleaInfoVo
     * @author mingwang
     * @create 2016年9月22日上午10:02:05
     * @version 1.0
     * @param intobsleaId
     * @return
     */
    ProgramIntobsleaInfoVo getProgramIntobsleaInfo(@Param("intobsleaId") String intobsleaId);

    /**
     * @description 获取ProgramIntobsleaInfoList
     * @author hxzhang
     * @create 2016年9月22日下午8:37:45
     * @version 1.0
     * @param condtion
     * @return
     */
    List<TaskListVo> getProgramIntobsleaInfoList(TaskCondtion condtion);

    /**
     * 
     * @description 更新newsfeedId
     * @author mingwang
     * @create 2016年9月23日上午11:08:29
     * @version 1.0
     * @param objId
     * @param newsfeedId
     * @return
     */
    int updateNewsfeedId(@Param("id") String id, @Param("newsfeedId") String newsfeedId);

    /**
     * @description 获取需要创建ProgramIntobsleaInfo
     * @author hxzhang
     * @create 2016年9月25日下午5:05:19
     * @version 1.0
     * @return
     */
    List<ProgramIntobsleaInfo> getCreateFollowupProgramIntobsleaList(Date now);

    /**
     * 
     * @description 获取过期的Insobslea
     * @author mingwang
     * @create 2016年9月28日上午11:31:40
     * @version 1.0
     * @param now
     * @return
     */
    List<String> getOverDueInsobslea(Date now);

    /**
     * 
     * @description 将过期Insobslea状态更新为overDue
     * @author mingwang
     * @create 2016年9月28日上午11:32:20
     * @version 1.0
     * @param now
     * @return
     */
    int updateOverDueInsobslea(Date now);

    /**
     * @description 通过newsId获取ProgramIntobsleaInfo
     * @author Hxzhang
     * @create 2016年9月28日下午4:09:12
     */
    ProgramIntobsleaInfo getIntobsleaInfo(String newsId);

    /**
     * 
     * @description 获取task的roomid,centerid
     * @author gfwang
     * @create 2016年11月13日上午10:20:42
     * @version 1.0
     * @param id
     * @return
     */
    RoomGroupInfo getCenterIdAndRoomId(String id);

    /**
     * 
     * @description 获取registerItem的roomid,centerid
     * @author mingwang
     * @create 2017年1月5日下午5:14:01
     * @version 1.0
     * @param id
     * @return
     */
    RoomGroupInfo getCenterIdAndRoomIdRegister(String id);
}