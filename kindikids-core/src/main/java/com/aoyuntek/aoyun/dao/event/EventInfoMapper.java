package com.aoyuntek.aoyun.dao.event;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.aoyuntek.aoyun.condtion.EventCondition;
import com.aoyuntek.aoyun.condtion.GalleryCondition;
import com.aoyuntek.aoyun.entity.po.event.EventInfo;
import com.aoyuntek.aoyun.entity.vo.event.EventInfoVo;
import com.aoyuntek.aoyun.entity.vo.event.GalleryInfoVo;
import com.aoyuntek.framework.dao.BaseMapper;

public interface EventInfoMapper extends BaseMapper<EventInfo> {

	void removeEventById(String id);

	List<EventInfo> getListByCentreIdDate(@Param("centreIds") List<String> centreIds, @Param("startDate") Date startDate, @Param("endDate") Date endDate);

	List<EventInfo> getListByWhere2(@Param("date") Date date, @Param("visibility") Short visibility, @Param("createAccountId") String createAccountId,
			@Param("centerIds") List<String> centerIds, @Param("isCeoAndAllCenters") boolean isCeoAndAllCenters);

	List<EventInfo> getListByWhere(@Param("date") Date date, @Param("visibility") Short visibility, @Param("createAccountId") String createAccountId,
			@Param("centerId") String centerId);

	/**
	 * 通过eventId获取eventVo
	 * 
	 * @param eventId
	 * @return
	 */
	EventInfoVo selectEventInfoVoById(String eventId);

	/**
	 * 查询event集合
	 * 
	 * @param condition
	 * @return
	 */
	List<EventInfoVo> selectEventInfoVoList(EventCondition condition);

	/**
	 * 查询event个数
	 * 
	 * @param condition
	 * @return
	 */
	int selectEventInfoVoListCount(EventCondition condition);

	/**
	 * 查询gallery集合
	 * 
	 * @param condition
	 * @return
	 */
	List<GalleryInfoVo> selectGalleryInfoVoList(GalleryCondition condition);

	/**
	 * 查询gallery个数
	 * 
	 * @param condition
	 * @return
	 */
	int selectGalleryInfoVoListCount(GalleryCondition condition);

}