package com.aoyuntek.aoyun.service.program;

import java.util.Date;

import com.aoyuntek.aoyun.dao.program.ProgramMedicationInfoMapper;
import com.aoyuntek.aoyun.entity.po.program.ProgramMedicationInfo;
import com.aoyuntek.aoyun.entity.vo.ProgramMedicationInfoVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.service.IBaseWebService;
import com.theone.common.util.ServiceResult;

/**
 * 
 * @description programMedication 业务逻辑层
 * @author mingwang
 * @create 2016年9月26日下午5:04:51
 * @version 1.0
 */
public interface IProgramMedicationService extends IBaseWebService<ProgramMedicationInfo, ProgramMedicationInfoMapper>{

    /**
     * 
     * @description 新增更新小孩MedicationInfo
     * @author mingwang
     * @create 2016年9月26日下午3:48:27
     * @version 1.0
     * @param medicationInfoVo
     * @param currentUser
     * @return
     */
    ServiceResult<Object> addOrUpdateMedication(ProgramMedicationInfoVo medicationInfoVo, UserInfoVo currentUser, Date date);

    /**
     * 
     * @description 获取ProgramMedication(喂药信息)
     * @author mingwang
     * @create 2016年9月26日下午7:58:22
     * @version 1.0
     * @param id
     * @return
     */
    ServiceResult<Object> getMedicationInfo(String id);

    /**
     * 
     * @description 将Medication的过期状态更新为overDue
     * @author mingwang
     * @create 2016年9月28日下午2:58:02
     * @version 1.0
     * @param now
     */
    void updateOverDueMedicationTimedTask(Date now);
    
}
