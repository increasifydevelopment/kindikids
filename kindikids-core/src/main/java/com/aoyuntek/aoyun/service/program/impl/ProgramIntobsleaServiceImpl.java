package com.aoyuntek.aoyun.service.program.impl;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.dao.AbsenteeRequestInfoMapper;
import com.aoyuntek.aoyun.dao.AttachmentInfoMapper;
import com.aoyuntek.aoyun.dao.RoomInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.dao.program.ProgramIntobsleaInfoMapper;
import com.aoyuntek.aoyun.dao.program.TaskProgramFollowUpInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskInstanceInfoMapper;
import com.aoyuntek.aoyun.entity.po.AbsenteeRequestInfoWithBLOBs;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramIntobsleaInfo;
import com.aoyuntek.aoyun.entity.po.program.TaskProgramFollowUpInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskInstanceInfo;
import com.aoyuntek.aoyun.entity.vo.ProgramIntobsleaInfoVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.ProgramState;
import com.aoyuntek.aoyun.enums.TaskType;
import com.aoyuntek.aoyun.service.attendance.IAttendanceService;
import com.aoyuntek.aoyun.service.impl.BaseWebServiceImpl;
import com.aoyuntek.aoyun.service.program.IProgramIntobsleaService;
import com.aoyuntek.aoyun.service.program.IProgramLessonService;
import com.aoyuntek.aoyun.service.program.ITaskProgramFollowUpService;
import com.aoyuntek.aoyun.service.task.ITaskInstance;
import com.theone.common.util.ServiceResult;
import com.theone.date.util.DateUtil;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

/**
 * 
 * @description program Intobslea(interest,observasion,learningStory) 业务逻辑实现类
 * @author mingwang
 * @create 2016年9月20日下午5:49:41
 * @version 1.0
 */
@Service
public class ProgramIntobsleaServiceImpl extends BaseWebServiceImpl<ProgramIntobsleaInfo, ProgramIntobsleaInfoMapper> implements
        IProgramIntobsleaService {

    /**
     * 日志
     */
    public static Logger logger = Logger.getLogger(ProgramIntobsleaServiceImpl.class);

    @Autowired
    private ProgramIntobsleaInfoMapper intobsleaInfoMapper;
    @Autowired
    private IProgramLessonService lessonSerevice;
    @Autowired
    private AttachmentInfoMapper attachmentInfoMapper;
    @Autowired
    private RoomInfoMapper roomInfoMapper;
    @Autowired
    private ITaskInstance taskInstance;
    @Autowired
    private TaskInstanceInfoMapper taskInstanceInfoMapper;
    @Autowired
    private TaskProgramFollowUpInfoMapper taskProgramFollowUpInfoMapper;
    @Autowired
    private UserInfoMapper userInfoMapper;
    @Autowired
    private ITaskProgramFollowUpService taskProgramFollowUpService;
    @Autowired
    private IAttendanceService attendanceService;
    @Autowired
    private AbsenteeRequestInfoMapper absenteeRequestInfoMapper;

    /**
     * 新增/更新program中Intobslea
     */
    @Override
    public ServiceResult<Object> addOrUpdateintobslea(ProgramIntobsleaInfoVo programIntobsleaInfoVo, UserInfoVo currentUser, Date date,
            boolean confirm) {
        log.info("addOrUpdateintobslea|start");
        ProgramIntobsleaInfo intobsleaInfo = programIntobsleaInfoVo.getProgramIntobsleaInfo();
        // 小孩必选
        if (StringUtil.isEmpty(intobsleaInfo.getChildId())) {
            return failResult(getTipMsg("program.child.empty"));
        }
        // Educator's Planned Experience 长度验证 hxzhang
        if (StringUtil.isNotEmpty(intobsleaInfo.getEducatorExperience()) && intobsleaInfo.getEducatorExperience().length() > 1000) {
            return failResult(getTipMsg("program_Interest_Educator_Experience"));
        }
        // 定义返回ProgramIntobsleaInfo对象
        ProgramIntobsleaInfo returnIntobsleaInfo = null;
        String intobsleaId = intobsleaInfo.getId();
        // intobsleaId为空，新增programIntobsleaInfo
        if (StringUtil.isEmpty(intobsleaId)) {
            // 判断所选的孩子是否未排课请假.如果是则给出提示,提醒用户下一步操作.
            ServiceResult<Object> isTip = tipValidate(intobsleaInfo.getChildId(), intobsleaInfo.getEvaluationDate(), confirm);
            if (!isTip.isSuccess()) {
                return isTip;
            }
            intobsleaId = UUID.randomUUID().toString();
            // 保存IntobsleaInfo
            ProgramIntobsleaInfo saveIntobsleaInfo = saveprogramIntobsleaInfo(intobsleaId, intobsleaInfo, currentUser, date);
            returnIntobsleaInfo = saveIntobsleaInfo;
        } else {

            // 通过intobsleaId查询ProgramIntobsleaInfo
            ProgramIntobsleaInfo selectIntobsleaInfo = intobsleaInfoMapper.selectByPrimaryKey(intobsleaInfo.getId());
            // 判断是否更换孩子如果更换还要验证孩子是否排课及请假
            if (StringUtil.isNotEmpty(selectIntobsleaInfo.getChildId()) && !selectIntobsleaInfo.getChildId().equals(intobsleaInfo.getChildId())) {
                // 判断所选的孩子是否未排课请假.如果是则给出提示,提醒用户下一步操作.
                ServiceResult<Object> isTip = tipValidate(intobsleaInfo.getChildId(), intobsleaInfo.getEvaluationDate(), confirm);
                if (!isTip.isSuccess()) {
                    return isTip;
                }
            }
            // intobsleaId不为空，更新programIntobsleaInfo
            ProgramIntobsleaInfo updateIntobsleaInfo = updateProgramIntobsleaInfo(intobsleaInfo, selectIntobsleaInfo, currentUser);
            returnIntobsleaInfo = updateIntobsleaInfo;
        }
        // 处理图片
        lessonSerevice.initImages(intobsleaId, programIntobsleaInfoVo.getImages());
        // 创建followup
        taskProgramFollowUpService.createProgramFollowup(intobsleaInfo);
        log.info("addOrUpdateintobslea|end");
        return successResult(getTipMsg("operation_success"), (Object) returnIntobsleaInfo);
    }

    /**
     * @description 验证是否有提示信息
     * @author hxzhang
     * @create 2017年3月14日上午10:57:28
     */
    private ServiceResult<Object> tipValidate(String accountId, Date date, boolean confirm) {
        if (confirm) {
            return successResult();
        }
        int days = attendanceService.getChildAttendanceDays(accountId, date, date);
        // 判断孩子是否排课
        if (days == 0) {
            return failResult(2, getTipMsg("program_not_have_attendance"));
        }
        List<AbsenteeRequestInfoWithBLOBs> list = absenteeRequestInfoMapper.getAbsenteesByDate(accountId, date);
        // 判断是否有请假
        if (ListUtil.isNotEmpty(list)) {
            return failResult(2, getTipMsg("program_have_absentee"));
        }
        return successResult();
    }

    /**
     * 
     * @description 保存Intobslea信息
     * @author mingwang
     * @create 2016年9月20日下午8:24:13
     * @version 1.0
     * @param intobsleaId
     * @param intobsleaInfo
     * @param currentUser
     * @return
     */
    private ProgramIntobsleaInfo saveprogramIntobsleaInfo(String intobsleaId, ProgramIntobsleaInfo intobsleaInfo, UserInfoVo currentUser, Date date) {
        log.info("addOrUpdateintobslea|saveprogramIntobsleaInfo|start|intobsleaId=" + intobsleaId);
        // 初始化programIntobsleaInfo
        intobsleaInfo.setId(intobsleaId);
        intobsleaInfo.setState(ProgramState.Pending.getValue());
        intobsleaInfo.setCenterId(roomInfoMapper.selectCenterIdByRoom(intobsleaInfo.getRoomId()));
        intobsleaInfo.setDay(date);
        intobsleaInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
        intobsleaInfo.setUpdateTime(new Date());
        intobsleaInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
        intobsleaInfo.setDeleteFlag(DeleteFlag.Default.getValue());
        // 插入programIntobsleaInfo
        intobsleaInfoMapper.insertSelective(intobsleaInfo);

        UserInfo child = userInfoMapper.getUserInfoByAccountId(intobsleaInfo.getChildId());

        // 保存task信息
        ServiceResult<TaskInstanceInfo> instance = taskInstance.addTaskInstance(getTaskType(intobsleaInfo.getType()), intobsleaInfo.getCenterId(),
                intobsleaInfo.getRoomId(), child.getGroupId(), intobsleaInfo.getId(), intobsleaInfo.getCreateAccountId(), date);
        log.info("addOrUpdateintobslea|saveprogramIntobsleaInfo|end");
        intobsleaInfo.setTemp1(instance.getReturnObj().getId());
        return intobsleaInfo;
    }

    /**
     * 
     * @description 更新Intobslea信息
     * @author mingwang
     * @create 2016年9月20日下午8:39:00
     * @version 1.0
     * @param intobsleaInfo
     * @param currentUser
     * @return
     */
    private ProgramIntobsleaInfo updateProgramIntobsleaInfo(ProgramIntobsleaInfo intobsleaInfo, ProgramIntobsleaInfo selectIntobsleaInfo,
            UserInfoVo currentUser) {
        log.info("addOrUpdateintobslea|updateProgramIntobsleaInfo|start");
        // 判断EvaluationDate是否改变,修改列表记录时间
        changeEvaluationDate(selectIntobsleaInfo, intobsleaInfo);

        selectIntobsleaInfo.setChildId(intobsleaInfo.getChildId());
        selectIntobsleaInfo.setCategory(intobsleaInfo.getCategory());
        selectIntobsleaInfo.setObservation(intobsleaInfo.getObservation());
        selectIntobsleaInfo.setEvaluationDate(intobsleaInfo.getEvaluationDate());
        selectIntobsleaInfo.setLearning(intobsleaInfo.getLearning());
        selectIntobsleaInfo.setObjective(intobsleaInfo.getObjective());
        selectIntobsleaInfo.setEducatorExperience(intobsleaInfo.getEducatorExperience());
        selectIntobsleaInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
        selectIntobsleaInfo.setUpdateTime(new Date());
        // 更新programIntobsleaInfo
        intobsleaInfoMapper.updateByPrimaryKeySelective(selectIntobsleaInfo);

        log.info("addOrUpdateintobslea|updateProgramIntobsleaInfo|end");
        return selectIntobsleaInfo;
    }

    /**
     * @description 改变EvaluationDate,修改列表记录时间
     * @author hxzhang
     * @create 2016年10月11日上午10:19:44
     */
    private void changeEvaluationDate(ProgramIntobsleaInfo oldPro, ProgramIntobsleaInfo newPro) {
        if (null == oldPro.getEvaluationDate()) {
            return;
        }
        if (null != oldPro.getEvaluationDate() && null == newPro.getEvaluationDate()) {
            taskInstance.deleteTaskInstanceByValueId(oldPro.getId());
            taskProgramFollowUpInfoMapper.removeFollowupByTaskId(oldPro.getId());
            return;
        }
        if (DateUtil.getPreviousDay(oldPro.getEvaluationDate()) != DateUtil.getPreviousDay(newPro.getEvaluationDate())) {
            TaskProgramFollowUpInfo followup = taskProgramFollowUpInfoMapper.getTaskProgramFollowUpInfoByTaskId(oldPro.getId());
            if (StringUtil.isNotEmpty(followup.getId())) {
                taskInstance.updateTaskInstance(followup.getId(), newPro.getEvaluationDate());
            }
        }
    }

    /**
     * 删除program中Intobslea
     */
    @Override
    public ServiceResult<Object> removeIntobslea(String intobsleaId) {
        log.info("removeIntobslea|start|intobsleaId=" + intobsleaId);
        // 删除Intobslea
        int deleteIntobslea = intobsleaInfoMapper.deleteIntobslea(intobsleaId);
        // 删除记录
        taskInstance.deleteTaskInstanceByValueId(intobsleaInfoMapper.selectByPrimaryKey(intobsleaId).getId());
        // 获取followup
        TaskProgramFollowUpInfo followup = taskProgramFollowUpInfoMapper.getTaskProgramFollowUpInfoByTaskId(intobsleaId);
        if (!(followup == null)) {
            // 删除对应的followup
            int deleteFollowup = taskProgramFollowUpInfoMapper.removeFollowupByTaskId(intobsleaId);
            // 删除followup记录
            taskInstance.deleteTaskInstanceByValueId(followup.getId());
            log.info("removeIntobslea|deleteFollowup=" + deleteFollowup);
        }
        // 删除附件图片
        int updateDeleteBySourceId = attachmentInfoMapper.updateDeleteBySourceId(intobsleaId);
        log.info("removeIntobslea|end|deleteIntobslea=" + deleteIntobslea + "|updateDeleteBySourceId=" + updateDeleteBySourceId);
        return successResult(getTipMsg("operation_success"));
    }

    /**
     * 获取ProgramIntobsleaInfo信息
     */
    @Override
    public ServiceResult<Object> getIntobsleaInfo(String intobsleaId) {
        ProgramIntobsleaInfoVo programIntobsleaInfoVo = intobsleaInfoMapper.getProgramIntobsleaInfo(intobsleaId);
        // add for
        TaskInstanceInfo taskInstanceInfo = taskInstanceInfoMapper.getByValueId(programIntobsleaInfoVo.getProgramIntobsleaInfo().getId());
        programIntobsleaInfoVo.setTemp1(taskInstanceInfo.getId());
        // end
        return successResult(getTipMsg("operation_success"), (Object) programIntobsleaInfoVo);
    }

    /**
     * @description 通过value获取枚举对象
     * @author mingwang
     * @create 2016年9月23日下午3:52:26
     * @version 1.0
     * @param type
     * @return
     */
    private TaskType getTaskType(short type) {
        for (TaskType t : TaskType.values()) {
            if (t.getValue() == type) {
                return t;
            }
        }
        return null;
    }

    /**
     * 将Intobslea的过期状态更新为overDue
     */
    @Override
    public void updateOverDueIntobsleaTimedTask(Date now) {
        log.info("updateOverDueIntobsleaTimedTask|start");
        // 获取过期的Insobslea
        List<String> overDueIntobsleaIds = intobsleaInfoMapper.getOverDueInsobslea(now);
        // 将记录表中的记录更新为overDue
        int instanceCount = taskInstanceInfoMapper.updateStatuByValueIds(overDueIntobsleaIds);
        log.info("updateOverDueIntobsleaTimedTask|instanceCount:" + instanceCount);
        // 将过期Insobslea状态更新为overDue
        int insobsleaCount = intobsleaInfoMapper.updateOverDueInsobslea(now);
        log.info("updateOverDueIntobsleaTimedTask|end|weeklyCount:" + insobsleaCount);
    }

    @Override
    public ServiceResult<Object> hasFollowUp(String intobsleaId) {
        TaskProgramFollowUpInfo followUp = taskProgramFollowUpInfoMapper.getTaskProgramFollowUpInfoByTaskId(intobsleaId);
        if (followUp != null) {
            return successResult((Object) true);
        }
        return successResult((Object) false);
    }

}
