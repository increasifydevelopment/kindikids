package com.aoyuntek.aoyun.service.calendar;

import com.aoyuntek.aoyun.condtion.AttendanceCondtion;
import com.aoyuntek.aoyun.dao.LeaveInfoMapper;
import com.aoyuntek.aoyun.entity.po.LeaveInfo;
import com.aoyuntek.aoyun.entity.vo.CalendarListVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.service.IBaseWebService;
import com.theone.common.util.ServiceResult;

public interface ICalendarService extends IBaseWebService<LeaveInfo,LeaveInfoMapper>{

    /**
     * 
     * @author dlli5 at 2016年8月24日上午8:07:15
     * @param currentUser
     * @param centreId
     * @param roomId
     * @param weekDate
     * @return
     */
    public ServiceResult<CalendarListVo> getCalendarList(UserInfoVo currentUser, AttendanceCondtion condtion);
    
    public ServiceResult<Object> updateLeaveColor(String leaveId,String color);
    
    public ServiceResult<Object> updateEventColor(String eventId,String color);
    
}
