package com.aoyuntek.aoyun.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.aoyuntek.aoyun.condtion.MessageCondition;
import com.aoyuntek.aoyun.condtion.NewsCondition;
import com.aoyuntek.aoyun.dao.AccountInfoMapper;
import com.aoyuntek.aoyun.dao.AttachmentInfoMapper;
import com.aoyuntek.aoyun.dao.MessageContentInfoMapper;
import com.aoyuntek.aoyun.dao.MessageInfoMapper;
import com.aoyuntek.aoyun.dao.MessageLookInfoMapper;
import com.aoyuntek.aoyun.dao.NqsInfoMapper;
import com.aoyuntek.aoyun.dao.NqsRelationInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.entity.po.AttachmentInfo;
import com.aoyuntek.aoyun.entity.po.MessageContentInfo;
import com.aoyuntek.aoyun.entity.po.MessageInfo;
import com.aoyuntek.aoyun.entity.po.MessageLookInfo;
import com.aoyuntek.aoyun.entity.po.NqsRelationInfo;
import com.aoyuntek.aoyun.entity.vo.MessageInfoVo;
import com.aoyuntek.aoyun.entity.vo.MessageListVo;
import com.aoyuntek.aoyun.entity.vo.NqsVo;
import com.aoyuntek.aoyun.entity.vo.ReceiverVo;
import com.aoyuntek.aoyun.entity.vo.UserInGroupInfo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.MessageType;
import com.aoyuntek.aoyun.enums.ReadFlag;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.service.IAuthGroupService;
import com.aoyuntek.aoyun.service.IMessageInfoService;
import com.aoyuntek.aoyun.service.IUserInfoService;
import com.aoyuntek.aoyun.uitl.FilePathUtil;
import com.aoyuntek.aoyun.uitl.SqlUtil;
import com.aoyuntek.framework.constants.PropertieNameConts;
import com.aoyuntek.framework.model.Pager;
import com.theone.aws.util.FileFactory;
import com.theone.common.util.ServiceResult;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

/**
 * @description Message业务逻辑层
 * @author hxzhang
 * @create 2016年7月4日下午5:31:34
 * @version 1.0
 */
@Service
public class MessageInfoServiceImpl extends BaseWebServiceImpl<MessageInfo, MessageInfoMapper> implements IMessageInfoService {

    public static Logger logger = Logger.getLogger(MessageInfoServiceImpl.class);
    /**
     * MessageInfoMapper
     */
    @Autowired
    private MessageInfoMapper messageInfoMapper;
    /**
     * MessageReceiveInfoMapper
     */
    @Autowired
    private MessageLookInfoMapper messageLookInfoMapper;
    /**
     * NqsMessageInfoMapper
     */
    @Autowired
    private NqsRelationInfoMapper nqsRelationInfoMapper;
    /**
     * IUserInfoService
     */
    @Autowired
    private IUserInfoService userInfoService;
    /**
     * AccountInfoMapper
     */
    @Autowired
    private AccountInfoMapper accountInfoMapper;
    /**
     * UserInfoMapper
     */
    @Autowired
    private UserInfoMapper userInfoMapper;
    /**
     * NqsInfoMapper
     */
    @Autowired
    private NqsInfoMapper nqsInfoMapper;

    @Autowired
    private MessageContentInfoMapper messagContentInfoMapper;
    /**
     * AttachmentInfoMapper
     */
    @Autowired
    private AttachmentInfoMapper attachmentInfoMapper;

    @Autowired
    private IAuthGroupService authGroupService;

    @Override
    public ServiceResult<Object> addMessage(MessageCondition condition, UserInfoVo currentUser) {
        logger.info("===================================addMessage begin====================================");
        // 参数验证
        ServiceResult<Object> result = validateAddMessage(condition, currentUser);
        if (!result.isSuccess()) {
            return result;
        }
        System.err.println(condition.getContent());
        // condition.setContent(dealContent(condition.getContent()));
        System.err.println(condition.getContent());
        String currentUserId = currentUser.getAccountInfo().getId();
        logger.info("currentUserId:" + currentUserId);
        String messageId = UUID.randomUUID().toString();
        String fileId = UUID.randomUUID().toString();
        short messageType = condition.getMessageType();
        String msgId = condition.getMsgId();

        // 获取消息接收人ID及Name
        List<ReceiverVo> idsAndNames = getAllAccountIdsAndNames(condition, currentUserId);
        if (ListUtil.isEmpty(idsAndNames)) {
            return failResult(getTipMsg("msg_group_empty"));
        }

        // 保存messageInfo
        MessageInfo replyMessageInfo = saveMessageInfo(currentUser, condition, messageId, fileId);

        // 接收人集合中去除当前用户(信息接收表不添加数据)
        if (ListUtil.isNotEmpty(idsAndNames)) {
            Iterator<ReceiverVo> it = idsAndNames.iterator();
            while (it.hasNext()) {
                ReceiverVo r = it.next();
                if (r.getId().equals(currentUserId))
                    it.remove();
            }
        }

        // 保存MessageLookInfo()
        if (messageType == MessageType.Send.getValue())
            saveMessageLookInfo(messageId, idsAndNames, replyMessageInfo.getSenderTime());

        // 保存nqs
        saveNqsMessageInfo(messageId, condition.getChooseNqs());

        // 保存附件信息
        try {
            saveAttachmentInfo(fileId, condition.getFileNames(), condition.getRelativePathNames());
        } catch (Exception e) {
            logger.error("msg_file_upload_failed||", e);
            throw new RuntimeException();
        }

        // reply,replyall,forward(更新发件人发送的信息为未读)
        if (messageType != MessageType.Send.getValue()) {
            MessageInfo messageInfo = messageInfoMapper.selectByPrimaryKey(replyMessageInfo.getParentMessageId());

            if (!currentUserId.equals(messageInfo.getSendAccountId())) {
                messageInfo.setReadFlag(ReadFlag.Default.getValue());
            }
            messageInfo.setSenderTime(new Date());
            messageInfoMapper.updateByPrimaryKeySelective(messageInfo);
        }

        // forward
        if (messageType == MessageType.Forward.getValue()) {
            // 原接收人
            List<ReceiverVo> receivers = messageLookInfoMapper.getReceiversByMsgId(msgId);
            // 与新接收人比较
            Iterator<ReceiverVo> itr = idsAndNames.iterator();
            while (itr.hasNext()) {
                ReceiverVo r = itr.next();
                if (receivers.contains(r)) {
                    // 移除新接收人中已存在于原接收人的接收人
                    itr.remove();
                }
            }
            // 添加额外接收人
            saveMessageLookInfo(replyMessageInfo.getParentMessageId(), idsAndNames, replyMessageInfo.getSenderTime());
            logger.info("forward succeed");

        }

        // replyall、forward 更新所有接收人的该信息(除当前用户)
        if (messageType == MessageType.ReplyAll.getValue() || messageType == MessageType.Forward.getValue()) {
            // 该信息所有的收件人信息update
            List<MessageLookInfo> messageLookInfoList = messageLookInfoMapper.getMessageLookInfoByMessageIdAndAccountId(msgId);
            logger.info("reply succed");
            Iterator<MessageLookInfo> itm = messageLookInfoList.iterator();
            while (itm.hasNext()) {
                MessageLookInfo messageLookInfo = itm.next();
                if (!currentUserId.equals(messageLookInfo.getLookAccountId())) {
                    messageLookInfo.setReadFlag(ReadFlag.Default.getValue());
                    messageLookInfoMapper.updateByPrimaryKeySelective(messageLookInfo);
                }
            }
        }
        logger.info("===================================addMessage end====================================");
        return successResult(getTipMsg("operation_success"));
    }

    @Override
    public ServiceResult<List<MessageInfoVo>> dealMessageInfo(String messageId, UserInfoVo currentUser) {
        logger.info("===================================dealMessageInfo begin====================================");
        // 当前用户账号ID
        String currentAccountId = currentUser.getAccountInfo().getId();
        // 获取消息主体
        MessageInfo messageInfo = messageInfoMapper.selectByPrimaryKey(messageId);
        if (null == messageInfo || StringUtil.isEmpty(messageInfo.getId())) {
            return failResult(getTipMsg("msg_get_failed"));
        }
        // 通过messageId获取Message信息
        String msgSenderId = messageInfo.getSendAccountId();
        long a = System.currentTimeMillis();
        List<MessageInfoVo> messageInfoVoList = messageInfoMapper.getMessageInfoAndNqsInfos(messageId, currentAccountId, msgSenderId);
        System.err.println(System.currentTimeMillis() - a);
        // 主体消息的ID
        String accountId = messageInfo.getSendAccountId();

        // 此消息为当前用户发送的消息
        if (currentAccountId.equals(accountId)) {
            if (null == messageInfo.getReadFlag() || messageInfo.getReadFlag() != ReadFlag.Read.getValue()) {
                // 将message设成已读
                messageInfo.setReadFlag(ReadFlag.Read.getValue());
                messageInfoMapper.updateByPrimaryKeySelective(messageInfo);
            }
            // initReceive(messageInfoVoList);
            return successResult(messageInfoVoList);
        } else {
            // 更新readFlag
            MessageLookInfo messageLookInfo = messageLookInfoMapper.getMessageReceiveInfoByMessageId(messageId, currentAccountId);
            messageLookInfo.setReadFlag(ReadFlag.Read.getValue());
            messageLookInfo.setLookTime(new Date());

            // 更新已读List<ReceiverVo>
            messageLookInfoMapper.updateByPrimaryKeySelective(messageLookInfo);
            List<ReceiverVo> receivers = messageInfoVoList.get(0).getReceiverVo();
            for (ReceiverVo r : receivers) {
                if (r.getId().equals(currentAccountId)) {
                    r.setReadFlag(ReadFlag.Read.getValue());
                }
            }
            logger.info("===================================dealMessageInfo end====================================");
            // initReceive(messageInfoVoList);
            return successResult(messageInfoVoList);
        }
    }

    /**
     * 
     * @description
     * @author gfwang
     * @create 2016年8月25日下午3:45:22
     * @version 1.0
     * @param messageInfoVoList
     */
    private void initReceive(List<MessageInfoVo> messageInfoVoList) {
        List<ReceiverVo> receiverList = messageInfoVoList.get(0).getReceiverVo();
        String[] mapArr = new String[receiverList.size()];
        Map<String, ReceiverVo> map = new TreeMap<String, ReceiverVo>();
        for (ReceiverVo receive : receiverList) {
            String group = receive.getGroupName();
            if (StringUtil.isEmpty(group)) {
                // 过度值
                String key = "initReceive" + UUID.randomUUID().toString();
                map.put(key, receive);
                continue;
            }
            if (map.containsKey(group)) {
                // 过度值
                group = "initReceive" + UUID.randomUUID().toString();
                receive.setGroupName(null);
            }
            map.put(group, receive);
        }
        receiverList.clear();
        Set<String> keySet = map.keySet();
        for (String key : keySet) {
            ReceiverVo r = map.get(key);
            if (key.contains("initReceive")) {
                key = null;
            }
            r.setGroupName(key);
            receiverList.add(r);
        }
        messageInfoVoList.get(0).setReceiverVo(receiverList);
    }

    @Override
    public ServiceResult<Pager<MessageListVo, MessageCondition>> getPagerMsgs(UserInfoVo currentUser, MessageCondition condition) {
        logger.info("===================================getPagerMsgs begin====================================");
        dealCondition(condition);
        // 获取当前用户账户ID
        String accountId = currentUser.getAccountInfo().getId();
        // 设置查询条件
        condition.setAccountId(accountId);
        condition.setSortName("time");
        condition.setSortOrder("DESC");
        // 模糊查询处理特殊字符
        String param = condition.getParams();
        condition.setParams(SqlUtil.likeEscape(condition.getParams()));
        logger.info("SortName：time ,SortOrder:DESC");
        condition.setStartSize(condition.getPageIndex());
        // 获取msg list
        List<MessageListVo> messageInfoVOList = messageInfoMapper.getPagerList(condition);
        // 信息总条数
        int totalSize = messageInfoMapper.getPagerListCount(condition);
        condition.setTotalSize(totalSize);
        condition.setParams(param);
        // 站内信分页实体
        Pager<MessageListVo, MessageCondition> pager = new Pager<MessageListVo, MessageCondition>(messageInfoVOList, condition);
        logger.info("===================================getPagerMsgs end====================================");
        return successResult(pager);
    }

    private void dealCondition(MessageCondition condition) {
        if (StringUtil.isNotEmpty(condition.getNqsVersion())) {
            if (condition.getNqsVersion().toUpperCase().equals("QA1")) {
                condition.setSelectNqs("1");
            } else if (condition.getNqsVersion().toUpperCase().equals("QA2")) {
                condition.setSelectNqs("2");
            } else if (condition.getNqsVersion().toUpperCase().equals("QA3")) {
                condition.setSelectNqs("3");
            } else if (condition.getNqsVersion().toUpperCase().equals("QA4")) {
                condition.setSelectNqs("4");
            } else if (condition.getNqsVersion().toUpperCase().equals("QA5")) {
                condition.setSelectNqs("5");
            } else if (condition.getNqsVersion().toUpperCase().equals("QA6")) {
                condition.setSelectNqs("6");
            } else if (condition.getNqsVersion().toUpperCase().equals("QA7")) {
                condition.setSelectNqs("7");
            } else {
                condition.setSelectNqs(condition.getNqsVersion());
            }
        } else {
            condition.setSelectNqs(null);
        }
    }

    @Override
    public ServiceResult<Integer> removeByMsgId(String msgId, boolean isInbox) {
        int count = 0;
        // 收件箱or发件箱
        if (isInbox) {
            count = messageInfoMapper.updateInboxByMsgId(msgId);
            if (count == 0) {
                return failResult(getTipMsg("msg_delete_failed"));
            }
            return successResult(getTipMsg("msg_delete_succeed"));
        } else {
            count = messageInfoMapper.updateOutboxByMsgId(msgId);
            if (count == 0) {
                return failResult(getTipMsg("msg_delete_failed"));
            }
            return successResult(getTipMsg("msg_delete_succeed"));
        }
    }

    @Override
    public int getUnreadCount(String accountId) {
        int count = messageInfoMapper.getUnreadCount(accountId);
        return count;
    }

    /**
     * @description 获取分组以及分组以外的收件人信息
     * @author hxzhang
     * @create 2016年6月30日下午3:29:22
     * @version 1.0
     * @param messageInfoVo
     *            messageInfo
     * @return 返回查询结果
     */
    private List<ReceiverVo> getAllAccountIdsAndNames(MessageCondition condition, String currentUserId) {
        logger.info("===================================getAllAccountIdsAndNames begin====================================");
        List<String> ids = new ArrayList<String>();
        // 获取所有分组 add by gfwang
        List<String> groupNames = authGroupService.getAllGroups();
        // 获取分组及分组以外的接收对象
        List<String> receiveObjects = new ArrayList<String>(Arrays.asList(condition.getReceiverIds().split(",")));
        List<ReceiverVo> idsAndNamesInGroup = new ArrayList<ReceiverVo>();
        for (int i = receiveObjects.size() - 1; i >= 0; i--) {
            String groupName = receiveObjects.get(i);
            if (groupNames.contains(receiveObjects.get(i))) {
                // 通过分组获取List<AccountInfo>
                List<UserInGroupInfo> accounts = authGroupService.dealAccountListByGroup(receiveObjects.get(i));
                if (!ListUtil.isEmpty(accounts)) {
                    List<String> accountIds = new ArrayList<String>();
                    for (int j = 0; j < accounts.size(); j++) {
                        UserInGroupInfo currtenUser = accounts.get(j);
                        if (currtenUser.getDisabled()) {
                            continue;
                        }
                        accountIds.add(accounts.get(j).getAccountId());
                        ids.addAll(accountIds);
                        idsAndNamesInGroup.add(new ReceiverVo(currtenUser.getAccountId(), currtenUser.getName(), groupName));
                    }
                    // 删除接收对象中分组信息
                    receiveObjects.remove(i);
                }
            }
        }
        // 通过分组以外的接收对象获取List<AccountInfo>
        List<ReceiverVo> idsAndNamesOutGroup = new ArrayList<ReceiverVo>();
        if (!ListUtil.isEmpty(receiveObjects)) {
            idsAndNamesOutGroup = accountInfoMapper.getNamesByIds(receiveObjects);
        }
        /*
         * if (ids.size() != 0) { idsAndNamesInGroup =
         * accountInfoMapper.getNamesByIds(ids); }
         */
        // 去除了重复接收人
        Set<ReceiverVo> idsAndNameSet = new LinkedHashSet<ReceiverVo>();
        idsAndNameSet.addAll(idsAndNamesOutGroup);
        idsAndNameSet.addAll(idsAndNamesInGroup);
        List<ReceiverVo> idsAndNames = new ArrayList<ReceiverVo>(idsAndNameSet);
        logger.info("收件人：" + idsAndNames);
        logger.info("===================================getAllAccountIdsAndNames end====================================");
        return idsAndNames;
    }

    /**
     * @description 保存messageInfo
     * @author hxzhang
     * @create 2016年7月5日上午9:09:41
     * @version 1.0
     * @param messageInfo
     *            MessageInfo
     * @param accountInfos
     *            List<AccountInfo>
     * @param currentUser
     *            当前登录用户
     * @return 返回messageId
     */
    private MessageInfo saveMessageInfo(UserInfoVo currentUser, MessageCondition condition, String messageId, String fileId) {
        logger.info("===================================saveMessageInfo begin====================================");
        // 初始化MessageInfo信息,并插入数据库
        MessageInfo messageInfo = new MessageInfo();
        messageInfo.setId(messageId);
        messageInfo.setSendAccountId(currentUser.getAccountInfo().getId());
        messageInfo.setSendAccountName(currentUser.getUserInfo().getFirstName() + " " + currentUser.getUserInfo().getLastName());
        messageInfo.setSenderTime(new Date());
        messageInfo.setCreateTime(new Date());
        messageInfo.setSubject(condition.getSubject());
        messageInfo.setDeleteFlag(DeleteFlag.Default.getValue());
        messageInfo.setCentersId(currentUser.getUserInfo().getCentersId());
        messageInfo.setMessageType(condition.getMessageType());
        logger.info("发送类型：" + condition.getMessageType());
        if (condition.getMessageType() != MessageType.Send.getValue()) {
            messageInfo.setParentMessageId(condition.getMsgId());
        }
        messageInfo.setReadFlag(ReadFlag.Read.getValue());
        messageInfo.setFileId(fileId);
        messageInfoMapper.insertSelective(messageInfo);

        MessageContentInfo messageContentInfo = new MessageContentInfo();
        messageContentInfo.setId(UUID.randomUUID().toString());
        messageContentInfo.setContent(condition.getContent());
        messageContentInfo.setMessageId(messageId);
        messagContentInfoMapper.insertSelective(messageContentInfo);
        logger.info("===================================saveMessageInfo end====================================");
        return messageInfo;
    }

    /**
     * @description 保存MessageReceiveInfo
     * @author hxzhang
     * @create 2016年7月5日上午8:58:01
     * @version 1.0
     * @param messageId
     *            message ID
     * @param messageInfo
     *            MessageInfo
     * @param accountInfos
     *            List<AccountInfo>
     */
    private void saveMessageLookInfo(String messageId, List<ReceiverVo> idsAndNames, Date sendTime) {
        if (ListUtil.isNotEmpty(idsAndNames)) {
            logger.info("===================================saveMessageLookInfo begin====================================");
            // 初始化MessageReceiveInfo信息,并插如数据库
            List<MessageLookInfo> messageLookInfoList = new ArrayList<MessageLookInfo>();
            for (ReceiverVo item : idsAndNames) {
                MessageLookInfo messageLookInfo = new MessageLookInfo();
                messageLookInfo.setId(UUID.randomUUID().toString());
                messageLookInfo.setMessageId(messageId);
                messageLookInfo.setLookAccountId(item.getId());
                messageLookInfo.setLookUserName(item.getName());
                messageLookInfo.setLookReceiveTime(sendTime);
                sendTime = addOneSecond(sendTime);
                messageLookInfo.setGroupName(item.getGroupName());
                messageLookInfo.setReadFlag(ReadFlag.Default.getValue());
                messageLookInfo.setDeleteFlag(DeleteFlag.Default.getValue());
                messageLookInfoList.add(messageLookInfo);
            }
            messageLookInfoMapper.insterBatchMessageReceiveInfo(messageLookInfoList);
            logger.info("send succeed");
            logger.info("===================================saveMessageLookInfo end====================================");
        }
    }

    /**
     * 
     * @description 时间加1秒
     * @author bbq
     * @create 2016年7月13日下午2:40:17
     * @version 1.0
     * @param date
     * @return
     */
    private Date addOneSecond(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.SECOND, 1);
        return calendar.getTime();
    }

    /**
     * @description 保存NqsMessageInfo
     * @author hxzhang
     * @create 2016年7月5日上午8:58:46
     * @version 1.0
     * @param messageId
     *            message ID
     * @param nsqVersion
     *            nsqVersion
     */
    private void saveNqsMessageInfo(String messageId, List<NqsVo> nqsList) {
        if (ListUtil.isNotEmpty(nqsList)) {
            logger.info("===================================saveNqsMessageInfo begin====================================");
            // 初始化NqsMessageInfo信息,并插入数据库
            List<NqsRelationInfo> nqsMessageInfoList = new ArrayList<NqsRelationInfo>();
            for (NqsVo nqs : nqsList) {
                NqsRelationInfo nqsRelation = new NqsRelationInfo();
                nqsRelation.setId(UUID.randomUUID().toString());
                nqsRelation.setObjId(messageId);
                nqsRelation.setNqsVersion(nqs.getVersion());
                nqsRelation.setDeleteFlag((short) 0);
                nqsRelation.setTag(nqs.getTag());
                nqsMessageInfoList.add(nqsRelation);
            }
            nqsRelationInfoMapper.insterBatchNqsRelationInfo(nqsMessageInfoList);
            logger.info("===================================saveNqsMessageInfo end====================================");
        }
    }

    /**
     * @description 信息验证
     * @author hxzhang
     * @create 2016年7月5日下午2:47:13
     * @version 1.0
     * @param messageInfo
     *            MessageInfo
     * @param nqsVersion
     *            NQSchoseNqs
     * @return 返回结果
     */
    private ServiceResult<Object> validateAddMessage(MessageCondition condition, UserInfoVo currentUser) {
        logger.info("===================================validateAddMessage begin====================================");
        // 1.condition非空验证
        if (null == condition)
            return failResult(getTipMsg("msg_validate_failed"));
        List<NqsVo> choseNqs = condition.getChooseNqs();
        // 主题非空验证
        if (condition.getMessageType() == MessageType.Send.getValue() && StringUtil.isEmpty(condition.getSubject()))
            return failResult(getTipMsg("msg_subject"));

        if (StringUtil.isEmpty(condition.getContent())) {
            return failResult(getTipMsg("news_content_empty"));
        }
        boolean isParent = containRoles(currentUser.getRoleInfoVoList(), Role.Parent);
        // 主题非空验证
        if (condition.getMessageType() == MessageType.Send.getValue() && ListUtil.isEmpty(choseNqs) && !isParent) {
            return failResult(getTipMsg("msg_nqs_empty"));
        }

        // 2.收件人非空验证
        if (StringUtil.isEmpty(condition.getReceiverIds())) {
            if (StringUtil.isEmpty(condition.getSubject())) {
                return failResult(getTipMsg("msg_receiveUser_subject"));
            }
            return failResult(getTipMsg("msg_receiveUser"));
        }
        // 3.主题长度验证
        if (condition.getMessageType() == MessageType.Send.getValue() && (null == condition.getSubject() || condition.getSubject().length() > 500)) {
            return failResult(getTipMsg("msg_subject_long"));
        }

        // 4.NQS有效性验证
        if (!ListUtil.isEmpty(choseNqs) && !isParent) {
            List<String> dbNsqs = nqsInfoMapper.getDbNqs(choseNqs);
            for (NqsVo nqs : choseNqs) {
                if (!dbNsqs.contains(nqs.getVersion())) {
                    logger.info("NQS非法");
                    return failResult(nqs + " " + getTipMsg("msg_nqs"));
                }
            }
        }

        logger.info("===================================validateAddMessage end====================================");
        return successResult();
    }

    /**
     * @description 保存附件
     * @author hxzhang
     * @create 2016年7月11日下午3:55:49
     * @version 1.0
     * @param messageId
     *            message ID
     * @param attachName
     *            附件名称
     * @param attachId
     *            附件UUID
     * @throws Exception
     */
    private void saveAttachmentInfo(String sourceId, String attachNames, String attachIds) throws Exception {
        if (StringUtil.isNotEmpty(attachNames) && StringUtil.isNotEmpty(attachIds)) {
            logger.info("===================================saveAttachmentInfo begin====================================");
            List<AttachmentInfo> attachList = new ArrayList<AttachmentInfo>();
            String[] attNames = attachNames.split(",");
            String[] attIds = attachIds.split(",");
            FileFactory fac = null;
            fac = new FileFactory(Region.getRegion(Regions.AP_SOUTHEAST_2), PropertieNameConts.S3);
            for (int i = 0; i < attNames.length; i++) {
                AttachmentInfo attachmentInfo = new AttachmentInfo();
                attachmentInfo.setId(UUID.randomUUID().toString());
                attachmentInfo.setSourceId(sourceId);
                attachmentInfo.setAttachName(attNames[i]);
                String tempDir = attIds[i];
                String mainDir = FilePathUtil.getMainPath(FilePathUtil.MESSAGE_PATH, tempDir);
                String rePath = fac.copyFile(tempDir, mainDir);
                attachmentInfo.setAttachId(rePath);
                attachList.add(attachmentInfo);
            }
            attachmentInfoMapper.insterBatchAttachmentInfo(attachList);
            logger.info("===================================saveAttachmentInfo end====================================");
        }
    }

    @Override
    public ServiceResult<Object> updateForward(List<String> accountIds, String msgId) {
        if (ListUtil.isEmpty(accountIds)) {
            return failResult(getTipMsg("msg_forward_person"));
        }
        for (String accountId : accountIds) {
            initLookInfo(msgId, accountId);
        }
        return successResult();
    }

    private void initLookInfo(String msgId, String accountId) {
        MessageLookInfo dbLook = messageLookInfoMapper.getMessageReceiveInfoByMessageId(msgId, accountId);
        if (dbLook != null) {
            return;
        }
        MessageLookInfo lookInfo = new MessageLookInfo();
        lookInfo.setId(UUID.randomUUID().toString());
        lookInfo.setDeleteFlag(DeleteFlag.Default.getValue());
        lookInfo.setLookAccountId(accountId);
        lookInfo.setMessageId(msgId);
        lookInfo.setLookReceiveTime(new Date());
        lookInfo.setReadFlag(ReadFlag.Default.getValue());
        messageLookInfoMapper.insert(lookInfo);
    }
}
