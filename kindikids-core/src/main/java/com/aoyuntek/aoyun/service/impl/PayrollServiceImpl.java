package com.aoyuntek.aoyun.service.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.condtion.PayrollCondition;
import com.aoyuntek.aoyun.dao.AccountRoleInfoMapper;
import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.ClosurePeriodMapper;
import com.aoyuntek.aoyun.dao.HolidaypayInfoMapper;
import com.aoyuntek.aoyun.dao.RoleInfoMapper;
import com.aoyuntek.aoyun.dao.SigninInfoMapper;
import com.aoyuntek.aoyun.dao.StaffEmploymentInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.dao.roster.RosterStaffInfoMapper;
import com.aoyuntek.aoyun.entity.po.CentersInfo;
import com.aoyuntek.aoyun.entity.po.ClosurePeriod;
import com.aoyuntek.aoyun.entity.po.HolidaypayInfo;
import com.aoyuntek.aoyun.entity.po.RoleInfo;
import com.aoyuntek.aoyun.entity.po.SigninInfo;
import com.aoyuntek.aoyun.entity.po.StaffEmploymentInfo;
import com.aoyuntek.aoyun.entity.vo.PayrollVo;
import com.aoyuntek.aoyun.enums.Holiday;
import com.aoyuntek.aoyun.enums.LeaveType;
import com.aoyuntek.aoyun.enums.PayrollCategory;
import com.aoyuntek.aoyun.enums.Region;
import com.aoyuntek.aoyun.enums.SigninType;
import com.aoyuntek.aoyun.enums.StaffType;
import com.aoyuntek.aoyun.service.IPayrollService;
import com.theone.date.util.DateUtil;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

@Service
public class PayrollServiceImpl implements IPayrollService {
	@Autowired
	private RosterStaffInfoMapper rosterStaffInfoMapper;
	@Autowired
	private StaffEmploymentInfoMapper staffEmploymentInfoMapper;
	@Autowired
	private RoleInfoMapper roleInfoMapper;
	@Autowired
	private SigninInfoMapper signinInfoMapper;
	@Autowired
	private CentersInfoMapper centersInfoMapper;
	@Autowired
	private ClosurePeriodMapper closurePeriodMapper;
	@Autowired
	private UserInfoMapper userInfoMapper;
	@Autowired
	private HolidaypayInfoMapper holidaypayInfoMapper;

	private List<PayrollVo> payrollVos;

	private List<PayrollVo> repeatList;

	private BigDecimal defaultHours = BigDecimal.valueOf(0);

	@Override
	public StringBuffer payrollExport(PayrollCondition condition) {
		String[] head = { "Employee Co./Last Name", "Employee First Name", "Payroll Category", "Job", "Customer Co./Last Name", "Customer First Name", "Notes", "Date",
				"Units", "Employee Card ID", "Employee Record ID", "Start/Stop Time", "Customer Card ID", "Customer Record ID", "Total Hours (Payroll)", "Centre",
				"Roster Time Start", "Roster Time End", "Signed In", "Signed Out", "Approved by", "Employment Type", "Role/Qualifications" };
		String separator = "\r\n";
		StringBuffer buffer = new StringBuffer();
		buffer.append("{}");
		buffer.append(separator);
		buffer.append(StringUtils.join(Arrays.asList(head), ","));
		buffer.append(separator);
		// 处理数据
		this.payrollVos = rosterStaffInfoMapper.getListForPayroll(condition);
		repeatList = new ArrayList<PayrollVo>();

		for (PayrollVo vo : this.payrollVos) {
			if (repeatList.contains(vo)) {
				continue;
			}
			List<StaffEmploymentInfo> list = staffEmploymentInfoMapper.getListByAccountIdEffectiveDay(vo.getAccountId(), vo.getDay());
			dealRepeatData(vo);
			setStaffType(list, vo);
			BigDecimal hours = dealUnits(vo, list);
			// baseHours 为0或空且不存在排班和签到时间不展示
			if (isNotShowData(hours, vo)) {
				continue;
			}
			// 兼职的ROD不展示整天请假
			if (isCasualLeaveForDay(vo)) {
				continue;
			}
			// 写入数据
			append(buffer, vo.getLastName());
			append(buffer, vo.getFirstName());
			append(buffer, dealPayrollCategory(vo));
			append(buffer, dealJob(vo));
			append(buffer, "");
			append(buffer, "");
			append(buffer, dealNotes(vo));
			append(buffer, DateUtil.format(vo.getDay(), "dd/MM/yyyy"));
			if (hours == null) {
				append(buffer, defaultHours);
			} else {
				append(buffer, hours);
			}
			append(buffer, vo.getCardId());
			append(buffer, "");
			append(buffer, "");
			append(buffer, "");
			append(buffer, "");
			if (hours == null) {
				append(buffer, defaultHours);
			} else {
				append(buffer, hours);
			}
			append(buffer, StringUtil.isEmpty(vo.getCentreName()) ? "" : vo.getCentreName());
			append(buffer, vo.getType() != PayrollCategory.BaseHourly.getValue() ? "" : dealTimes(vo.getStartDate()));
			append(buffer, vo.getType() != PayrollCategory.BaseHourly.getValue() ? "" : dealTimes(vo.getEndDate()));
			append(buffer, vo.getType() != PayrollCategory.BaseHourly.getValue() ? "" : dealTimes(vo.getsInDate()));
			append(buffer, vo.getType() != PayrollCategory.BaseHourly.getValue() ? "" : dealTimes(vo.getsOutDate()));
			append(buffer, vo.getApprovalName() == null ? "" : vo.getApprovalName());
			append(buffer, dealStaffType(vo.getStaffType()));
			append(buffer, dealRole(vo.getAccountId()));

			buffer.append(separator);
		}
		return buffer;
	}

	private String dealJob(PayrollVo vo) {
		if (vo.getRegion() == null) {
			return "";
		}
		if (vo.getCentreName().equals(Region.HeadOffice.getDesc())) {
			return Region.HeadOffice.getDesc();
		}
		if (vo.getRegion() == Region.EdensorPark.getValue()) {
			return Region.EdensorPark.getDesc();
		}
		if (vo.getRegion() == Region.Ryde.getValue()) {
			return Region.Ryde.getDesc();
		}
		return "";
	}

	private boolean isNotShowData(BigDecimal hours, PayrollVo vo) {
		if (vo.getStartDate() != null || vo.getEndDate() != null || vo.getsInDate() != null || vo.getsOutDate() != null) {
			return false;
		}
		if (hours != null && hours.compareTo(defaultHours) > 0) {
			return false;
		}
		return true;
	}

	private boolean isCasualLeaveForDay(PayrollVo vo) {
		if (vo.getStaffType() == null) {
			return false;
		}
		if (vo.getStaffType() != StaffType.Casual.getValue()) {
			return false;
		}
		if (vo.getType() != PayrollCategory.Leave.getValue()) {
			return false;
		}
		if (!LeaveType.getLeaveForDay().contains(vo.getLeaveType())) {
			return false;
		}
		return true;
	}

	private PayrollVo dealRepeatData(PayrollVo vo) {
		// 通过Roster Time计算Units(存在多个排班累加)
		BigDecimal units = getHoursInterval(vo.getEndDate(), vo.getStartDate());
		for (PayrollVo p : this.payrollVos) {

			if (p.getType() != PayrollCategory.BaseHourly.getValue() || vo.getType() != PayrollCategory.BaseHourly.getValue()) {
				continue;
			}
			if (p.getDay().compareTo(vo.getDay()) != 0 || !p.getAccountId().equals(vo.getAccountId()) || p.getId().equals(vo.getId())) {
				continue;
			}
			if (p.getStartDate() != null && (vo.getStartDate() == null || times(p.getStartDate()).before(times(vo.getStartDate())))) {
				vo.setStartDate(p.getStartDate());
			}
			if (p.getEndDate() != null && (vo.getEndDate() == null || times(p.getEndDate()).after(times(vo.getEndDate())))) {
				vo.setEndDate(p.getEndDate());
			}
			this.repeatList.add(p);
			// 累加
			if (units == null) {
				units = getHoursInterval(p.getEndDate(), p.getStartDate());
			} else {
				BigDecimal u = getHoursInterval(p.getEndDate(), p.getStartDate());
				if (u != null) {
					units = units.add(u);
				}
			}
		}
		vo.setUnits(units);
		return vo;
	}

	private Date times(Date date) {
		String time = DateUtil.format(date, DateUtil.HHmm);
		String day = DateUtil.format(new Date(), DateUtil.yyyyMMdd);
		return DateUtil.parse(day + time, DateUtil.yyyyMMddHHmm);
	}

	private String dealRole(String accountId) {
		List<RoleInfo> list = roleInfoMapper.firstRolesByAccountId(accountId);
		if (ListUtil.isEmpty(list)) {
			return "";
		}
		return list.get(0).getName();
	}

	private String dealStaffType(Short type) {
		if (type == null) {
			return "";
		}
		if (type == StaffType.FullTime.getValue()) {
			return StaffType.FullTime.getDesc();
		} else if (type == StaffType.PartTime.getValue()) {
			return StaffType.PartTime.getDesc();
		} else {
			return StaffType.Casual.getDesc();
		}
	}

	private void setStaffType(List<StaffEmploymentInfo> list, PayrollVo vo) {
		if (ListUtil.isEmpty(list)) {
			dealStaffType(vo.getStaffType());
		} else {
			StaffEmploymentInfo info = list.get(0);
			vo.setStaffType(info.getStaffType());
		}
	}

	private Date dealSign(String accountId, Date date, SigninType type) {
		List<SigninInfo> list = signinInfoMapper.getSigninOrout(accountId, date, type.getValue());
		if (ListUtil.isEmpty(list)) {
			return null;
		}
		SigninInfo s = list.get(0);
		return s.getSignDate();
	}

	private String dealTimes(Date time) {
		if (time == null) {
			return "";
		}
		return new SimpleDateFormat("HH:mm a", Locale.ENGLISH).format(time);
	}

	private String dealNotes(PayrollVo vo) {
		if (StringUtil.isEmpty(vo.getLeaveReason())) {
			return "";
		}
		String notes = vo.getLeaveReason();
		notes = notes.replaceAll("\n", "");
		notes = notes.replaceAll(",", ".");
		return notes;
	}

	private BigDecimal dealUnits(PayrollVo vo, List<StaffEmploymentInfo> list) {
		if (vo.getType() == PayrollCategory.BaseHourly.getValue()) {
			return dealBaseHours(vo, list);
		} else if (vo.getType() == PayrollCategory.HolidayPay.getValue()) {
			return vo.getHours();
		} else {
			return dealLeaveHours(vo, list);
		}
	}

	private BigDecimal dealBaseHours(PayrollVo vo, List<StaffEmploymentInfo> list) {
		BigDecimal baseHours = getBaseHours(vo, list);
		BigDecimal hours = baseHours;

		Date sInDate = dealSign(vo.getAccountId(), vo.getDay(), SigninType.IN);
		Date sOutDate = dealSign(vo.getAccountId(), vo.getDay(), SigninType.OUT);
		vo.setsInDate(sInDate);
		vo.setsOutDate(sOutDate);

		if (hours == null) {
			return null;
		}

		boolean leaveForDay = false;
		for (PayrollVo p : this.payrollVos) {
			if (p.getDay().compareTo(vo.getDay()) != 0 || !p.getAccountId().equals(vo.getAccountId()) || p.getId().equals(vo.getId())) {
				continue;
			}
			if (p.getType() != PayrollCategory.Leave.getValue()) {
				continue;
			}
			if (LeaveType.getLeaveForDay().contains(p.getLeaveType())) {
				leaveForDay = true;
				hours = hours.subtract(baseHours == null ? defaultHours : baseHours);
			} else {
				hours = hours.subtract(getHoursInterval(times(p.getEndDate()), times(p.getStartDate())));
			}
		}
		// 如果是兼职并且请了整天的假期baseHours为0.
		if (vo.getStaffType() != null && vo.getStaffType() == StaffType.Casual.getValue() && leaveForDay) {
			return defaultHours;
		}

		// 处理非全职人员BaseHourly
		hours = dealBaseHourly(vo.getStaffType(), hours);

		return hours.compareTo(defaultHours) < 0 ? defaultHours : hours;
	}

	private BigDecimal getBaseHours(PayrollVo vo, List<StaffEmploymentInfo> list) {
		if (vo.getStaffType() != null && vo.getStaffType() == StaffType.PartTime.getValue()) {
			return vo.getUnits();
		}
		if (vo.getStaffType() != null && vo.getStaffType() == StaffType.Casual.getValue()) {
			return vo.getUnits();
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(vo.getDay());
		int week = calendar.get(Calendar.DAY_OF_WEEK);
		if (ListUtil.isEmpty(list)) {
			return null;
		}
		StaffEmploymentInfo employ = list.get(0);
		if (week == Calendar.MONDAY) {
			return employ.getMonHours();
		}
		if (week == Calendar.TUESDAY) {
			return employ.getTueHours();
		}
		if (week == Calendar.WEDNESDAY) {
			return employ.getWedHours();
		}
		if (week == Calendar.THURSDAY) {
			return employ.getThuHours();
		}
		if (week == Calendar.FRIDAY) {
			return employ.getFriHours();
		}
		return null;
	}

	private BigDecimal dealBaseHourly(Short type, BigDecimal baseHourly) {
		if (baseHourly == null) {
			return null;
		}
		if (type == null) {
			return baseHourly;
		}
		if (type == StaffType.FullTime.getValue()) {
			return baseHourly;
		}
		if (baseHourly.compareTo(BigDecimal.valueOf(6)) == -1) {
			return baseHourly;
		}
		return baseHourly.subtract(BigDecimal.valueOf(0.5));
	}

	private BigDecimal dealLeaveHours(PayrollVo vo, List<StaffEmploymentInfo> list) {
		if (LeaveType.getLeaveForDay().contains(vo.getLeaveType())) {
			return getBaseHours(vo, list);
		}
		if (LeaveType.getLeaveForTime().contains(vo.getLeaveType())) {
			return getHoursInterval(times(vo.getEndDate()), times(vo.getStartDate()));
		}
		return null;
	}

	private BigDecimal getHoursInterval(Date endTime, Date startTime) {
		if (endTime == null || startTime == null) {
			return null;
		}
		long diff = endTime.getTime() - startTime.getTime();
		return new BigDecimal((float) diff / (1000 * 60 * 60)).setScale(1, BigDecimal.ROUND_HALF_UP);
	}

	private void append(StringBuffer buffer, String str) {
		buffer.append(str);
		buffer.append(",");
	}

	private void append(StringBuffer buffer, BigDecimal decimal) {
		buffer.append(decimal);
		buffer.append(",");
	}

	private String dealPayrollCategory(PayrollVo vo) {
		if (vo.getType() == PayrollCategory.BaseHourly.getValue()) {
			return PayrollCategory.BaseHourly.getDesc();
		}
		if (vo.getType() == PayrollCategory.HolidayPay.getValue()) {
			return PayrollCategory.HolidayPay.getDesc();
		}
		if (vo.getLeaveType() == null) {
			return null;
		}
		if (vo.getLeaveType() == LeaveType.RDO.getValue()) {
			return "Rostered Day Off";
		}
		if (vo.getLeaveType() == LeaveType.AnnualLeave.getValue()) {
			return LeaveType.AnnualLeave.getDesc();
		}
		if (vo.getLeaveType() == LeaveType.PersonalLeave.getValue()) {
			return LeaveType.PersonalLeave.getDesc();
		}
		if (vo.getLeaveType() == LeaveType.ProfessionalDevelopmentLeave.getValue()) {
			return LeaveType.ProfessionalDevelopmentLeave.getDesc();
		}
		if (vo.getLeaveType() == LeaveType.CompassionateLeave.getValue()) {
			return LeaveType.CompassionateLeave.getDesc();
		}
		if (vo.getLeaveType() == LeaveType.TimeinLieu.getValue()) {
			return LeaveType.TimeinLieu.getDesc();
		}
		if (vo.getLeaveType() == LeaveType.LongServiceLeave.getValue()) {
			return LeaveType.LongServiceLeave.getDesc();
		}
		if (vo.getLeaveType() == LeaveType.ParentLeave.getValue()) {
			return LeaveType.ParentLeave.getDesc();
		}
		if (vo.getLeaveType() == LeaveType.CasualUnavailability.getValue()) {
			return LeaveType.CasualUnavailability.getDesc();
		}
		if (vo.getLeaveType() == LeaveType.JuryDuty.getValue()) {
			return LeaveType.JuryDuty.getDesc();
		}
		if (vo.getLeaveType() == LeaveType.Practicum.getValue()) {
			return LeaveType.Practicum.getDesc();
		}
		if (vo.getLeaveType() == LeaveType.WorkersCompensation.getValue()) {
			return LeaveType.WorkersCompensation.getDesc();
		}
		if (vo.getLeaveType() == LeaveType.OtherLeave.getValue()) {
			return LeaveType.OtherLeave.getDesc();
		}
		return null;
	}

	@Override
	public void dealClosurePeriodData(Date now) {
		// 同步上一天的所以要减一天
		now = DateUtil.addDay(now, -1);
		// 获取关园信息
		List<CentersInfo> list = centersInfoMapper.getAllCentreInfo();
		for (CentersInfo c : list) {
			List<ClosurePeriod> closurePeriods = closurePeriodMapper.getClosurePeriod(now, c.getId());
			if (ListUtil.isEmpty(closurePeriods)) {
				continue;
			}
			boolean isHoliday = false;
			for (ClosurePeriod cPeriod : closurePeriods) {
				if (cPeriod.getHoliday() == Holiday.YES.getValue()) {
					isHoliday = true;
				}
			}
			if (!isHoliday) {
				continue;
			}
			// 获取该天该园区工作人员(Full Time,Part Time)
			List<String> accountIds = userInfoMapper.getFullPartStaff(c.getId());
			for (String accountId : accountIds) {
				// 验证是否存在
				HolidaypayInfo holidaypayInfo = holidaypayInfoMapper.getAccountIdDay(accountId, now);
				if (holidaypayInfo != null) {
					continue;
				}
				List<StaffEmploymentInfo> employmentInfos = staffEmploymentInfoMapper.getListByAccountIdEffectiveDay(accountId, now);
				if (ListUtil.isEmpty(employmentInfos)) {
					continue;
				}
				StaffEmploymentInfo info = employmentInfos.get(0);
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(now);
				int weekday = calendar.get(Calendar.DAY_OF_WEEK);
				Boolean isAdd = false;
				BigDecimal hours = null;
				switch (weekday) {
				case Calendar.MONDAY:
					isAdd = info.getMonday();
					hours = info.getMonHours();
					break;
				case Calendar.TUESDAY:
					isAdd = info.getTuesday();
					hours = info.getTueHours();
					break;
				case Calendar.WEDNESDAY:
					isAdd = info.getWednesday();
					hours = info.getWedHours();
					break;
				case Calendar.THURSDAY:
					isAdd = info.getThursday();
					hours = info.getThuHours();
					break;
				case Calendar.FRIDAY:
					isAdd = info.getFriday();
					hours = info.getFriHours();
					break;
				}
				if (isAdd == null || !isAdd) {
					continue;
				}
				if (hours == null || hours.compareTo(defaultHours) == 0) {
					continue;
				}
				HolidaypayInfo h = new HolidaypayInfo();
				h.setId(UUID.randomUUID().toString());
				h.setAccountId(accountId);
				h.setEmployId(info.getId());
				h.setDay(now);
				h.setHours(hours);
				h.setRole(info.getPrimaryRole());
				h.setStaffType(info.getStaffType());
				h.setCentreId(info.getCentreId());
				h.setDeleteFlag(false);
				holidaypayInfoMapper.insertSelective(h);
			}
		}
	}
}
