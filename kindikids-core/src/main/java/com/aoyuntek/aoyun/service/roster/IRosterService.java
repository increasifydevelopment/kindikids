package com.aoyuntek.aoyun.service.roster;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.aoyuntek.aoyun.condtion.RosterStaffCondition;
import com.aoyuntek.aoyun.dao.roster.RosterStaffInfoMapper;
import com.aoyuntek.aoyun.entity.po.ClosurePeriod;
import com.aoyuntek.aoyun.entity.po.StaffEmploymentInfo;
import com.aoyuntek.aoyun.entity.po.roster.RosterStaffInfo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.roster.RosterLeaveVo;
import com.aoyuntek.aoyun.entity.vo.roster.RosterStaffVo;
import com.aoyuntek.aoyun.entity.vo.roster.RosterWeekVo;
import com.aoyuntek.aoyun.service.IBaseWebService;
import com.theone.common.util.ServiceResult;

public interface IRosterService extends IBaseWebService<RosterStaffInfo, RosterStaffInfoMapper> {

	ServiceResult<Object> deleteClosurePeriod(String id);

	ServiceResult<Object> saveClosurePeriod(ClosurePeriod closurePeriod, UserInfoVo currentUser, HttpServletRequest request);

	ServiceResult<Object> validateFutureRoster(StaffEmploymentInfo info);

	ServiceResult<RosterWeekVo> getWeekRoster(RosterStaffCondition condition, UserInfoVo currentUser);

	ServiceResult<List<SelecterPo>> getRosterStaffTemplates(String centreId);

	ServiceResult<RosterWeekVo> dealChooseTemplates(String id, Date day, UserInfoVo currentUser, HttpServletRequest request) throws Exception;

	ServiceResult<RosterWeekVo> createBlankRoster(String centreId, Date day, UserInfoVo currentUser);

	ServiceResult<List<SelecterPo>> getRosterStaffOfRole(String p, String rosterId, String centreId, int rosterStaffType, Date day, String shiftId,
			UserInfoVo currentUser);

	ServiceResult<List<SelecterPo>> getRosterStaffForToday(String accountId, Date today, String p);

	ServiceResult<RosterStaffVo> addRosterStaff(RosterStaffInfo rosterStaffInfo, UserInfoVo currentUser, Boolean alert, HttpServletRequest request);

	ServiceResult<RosterStaffVo> deleteRosterStaff(String objId, UserInfoVo currentUser, Boolean alert, HttpServletRequest request);

	ServiceResult<RosterStaffVo> getRosterStaff(String objId, UserInfoVo currentUser);

	ServiceResult<RosterStaffVo> updateRosterStatu(String rosterId, short statu, UserInfoVo currentUser, HttpServletRequest request, Boolean alert) throws Exception;

	ServiceResult<RosterStaffVo> dealRosterAsTemplate(String rosterId, boolean templateFlag, String name, UserInfoVo currentUser);

	ServiceResult<RosterStaffVo> updateRosterStaffItem(RosterStaffVo rosterStaffInfo, UserInfoVo currentUser, Boolean comfirmSure);

	ServiceResult<List<RosterLeaveVo>> getRosterLeaveVoList();

	ServiceResult<Object> saveRosterLeave(RosterLeaveVo rosterLeaveItemInfo, UserInfoVo user);

	ServiceResult<Object> deleteRosterLeave(String id);

}
