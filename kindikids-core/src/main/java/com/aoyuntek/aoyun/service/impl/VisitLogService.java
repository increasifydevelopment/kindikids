package com.aoyuntek.aoyun.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.dao.impl.VisitLogDao;
import com.aoyuntek.aoyun.entity.po.VisitLogInfo;
import com.aoyuntek.aoyun.service.IVisitLogService;

/**
 * @description 日志记录业务逻辑层实现
 * @author xdwang
 * @create 2015年12月13日下午8:10:39
 * @version 1.0
 */
@Service
public class VisitLogService implements IVisitLogService {

    /**
     * 日志数据访问层实现
     */
    @Autowired
    private VisitLogDao visitLogDao;

    @Override
    public void saveBatch(List<VisitLogInfo> logInfos) {
        visitLogDao.saveBacth(logInfos);
    }

}
