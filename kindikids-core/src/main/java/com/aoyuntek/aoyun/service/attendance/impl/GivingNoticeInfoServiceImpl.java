package com.aoyuntek.aoyun.service.attendance.impl;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.dao.AccountInfoMapper;
import com.aoyuntek.aoyun.dao.ChildAttendanceInfoMapper;
import com.aoyuntek.aoyun.dao.EmailMsgMapper;
import com.aoyuntek.aoyun.dao.GivingNoticeInfoMapper;
import com.aoyuntek.aoyun.dao.RequestLogInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.entity.po.GivingNoticeInfo;
import com.aoyuntek.aoyun.entity.po.GivingNoticeInfoWithBLOBs;
import com.aoyuntek.aoyun.entity.po.RequestLogInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.vo.LogRequestType;
import com.aoyuntek.aoyun.entity.vo.RoleInfoVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.enums.ChangeAttendanceRequestState;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.EmailType;
import com.aoyuntek.aoyun.enums.RequestLogType;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.factory.UserFactory;
import com.aoyuntek.aoyun.service.attendance.IAttendanceCoreService;
import com.aoyuntek.aoyun.service.attendance.IGivingNoticeInfoService;
import com.aoyuntek.aoyun.service.impl.BaseWebServiceImpl;
import com.aoyuntek.aoyun.service.task.ITaskService;
import com.theone.common.util.ServiceResult;
import com.theone.string.util.StringUtil;

@Service
public class GivingNoticeInfoServiceImpl extends BaseWebServiceImpl<GivingNoticeInfo, GivingNoticeInfoMapper> implements IGivingNoticeInfoService {
	public static Logger logger = Logger.getLogger(GivingNoticeInfoServiceImpl.class);
	@Autowired
	private GivingNoticeInfoMapper givingNoticeInfoMapper;
	@Autowired
	private RequestLogInfoMapper requestLogInfoMapper;
	@Autowired
	private UserInfoMapper userInfoMapper;
	@Autowired
	private ChildAttendanceInfoMapper caiMapper;
	@Autowired
	private AccountInfoMapper accountInfoMapper;
	@Autowired
	private IAttendanceCoreService systemCoreService;
	@Autowired
	private ITaskService taskService;
	@Autowired
	private UserFactory userFactory;
	@Autowired
	private EmailMsgMapper emailMsgMapper;

	@Override
	public ServiceResult<Object> getGivingNoticeInfo(String id) {
		GivingNoticeInfoWithBLOBs givingNoticeInfoWithBLOBs;
		if (StringUtil.isEmpty(id)) {
			givingNoticeInfoWithBLOBs = new GivingNoticeInfoWithBLOBs();
		} else {
			givingNoticeInfoWithBLOBs = givingNoticeInfoMapper.selectByPrimaryKey(id);
			givingNoticeInfoWithBLOBs.setCenterId(userInfoMapper.getUserInfoByAccountId(givingNoticeInfoWithBLOBs.getChildId()).getCentersId());
		}
		return successResult((Object) givingNoticeInfoWithBLOBs);
	}

	@Override
	public ServiceResult<Object> addOrUpdateGivingNoticeInfo(GivingNoticeInfoWithBLOBs givingNoticeInfoWithBLOBs, UserInfoVo currentUser) {
		// 验证
		ServiceResult<Object> result = validateGivingNoticeInfo(givingNoticeInfoWithBLOBs, currentUser);
		if (!result.isSuccess()) {
			return result;
		}
		if (StringUtil.isEmpty(givingNoticeInfoWithBLOBs.getId())) {
			// 新增
			String gniId = UUID.randomUUID().toString();
			givingNoticeInfoWithBLOBs.setId(gniId);
			givingNoticeInfoWithBLOBs.setCreateAccountId(currentUser.getAccountInfo().getId());
			Date now = new Date();
			givingNoticeInfoWithBLOBs.setCreateTime(now);
			givingNoticeInfoWithBLOBs.setUpdateTime(now);
			givingNoticeInfoWithBLOBs.setDeleteFlag(DeleteFlag.Default.getValue());
			// 如果是CEO及园长申请将直接变为approve状态
			if (containRoles(currentUser.getRoleInfoVoList(), Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge)) {
				givingNoticeInfoWithBLOBs.setState(ChangeAttendanceRequestState.Over.getValue());
				caiMapper.updateLeaveDateById(givingNoticeInfoWithBLOBs.getLastDate(),
						userInfoMapper.getUserInfoByAccountId(givingNoticeInfoWithBLOBs.getChildId()).getId());
				// approve 发送邮件 add gfwang
				userFactory.sendParentEmailByChild(getSystemEmailMsg("givenotice_approved_title"), getSystemEmailMsg("givenotice_approved_content"),
						givingNoticeInfoWithBLOBs.getChildId(), true, EmailType.givenoticeApproved.getValue(), null, givingNoticeInfoWithBLOBs.getLastDate(), gniId,
						givingNoticeInfoWithBLOBs.isPlan());
			} else {
				givingNoticeInfoWithBLOBs.setState(ChangeAttendanceRequestState.Default.getValue());
				// 家长申请给家长,CEO,园长发送邮件
				userFactory.sendParentEmailByChild(getSystemEmailMsg("request_givenotice_title"), getSystemEmailMsg("request_givenotice_content"),
						givingNoticeInfoWithBLOBs.getChildId(), true, EmailType.request_givenotice.getValue(), null, givingNoticeInfoWithBLOBs.getLastDate(), gniId,
						givingNoticeInfoWithBLOBs.isPlan());
				userFactory.sendEmailCeoManager(getSystemEmailMsg("request_givenotice_title"), getSystemEmailMsg("request_givenotice_content"),
						givingNoticeInfoWithBLOBs.getChildId(), EmailType.request_givenotice.getValue(), null, givingNoticeInfoWithBLOBs.getLastDate(), null);
			}
			givingNoticeInfoMapper.insertSelective(givingNoticeInfoWithBLOBs);
			caiMapper.updateRequestDateById(givingNoticeInfoWithBLOBs.getLastDate(),
					userInfoMapper.getUserInfoByAccountId(givingNoticeInfoWithBLOBs.getChildId()).getId());
			// 添加申请记录
			RequestLogInfo rli = new RequestLogInfo();
			rli.setId(UUID.randomUUID().toString());
			rli.setChildId(givingNoticeInfoWithBLOBs.getChildId());
			rli.setObjId(gniId);
			// TODO big
			// rli.setOperaFlag(OperaFlag.Yes.getValue());
			rli.setCreateAccountId(currentUser.getAccountInfo().getId());
			rli.setCreateTime(now);
			rli.setUpdateTime(now);
			rli.setMsg(getSystemMsg("giving_note") + " " + "(" + givingNoticeInfoWithBLOBs.getChildName() + ")");
			rli.setType(RequestLogType.GiveNoteRequest.getValue());
			rli.setDeleteFlag(DeleteFlag.Default.getValue());
			rli.setLogType(LogRequestType.GiveNoteRequest.getValue());
			requestLogInfoMapper.insertSelective(rli);
		} else {
			// 更新
			GivingNoticeInfoWithBLOBs dbGni = givingNoticeInfoMapper.selectByPrimaryKey(givingNoticeInfoWithBLOBs.getId());
			givingNoticeInfoWithBLOBs.setCreateAccountId(dbGni.getCreateAccountId());
			givingNoticeInfoWithBLOBs.setCreateTime(dbGni.getCreateTime());
			givingNoticeInfoWithBLOBs.setDeleteFlag(dbGni.getDeleteFlag());
			givingNoticeInfoWithBLOBs.setUpdateAccountId(currentUser.getAccountInfo().getId());
			givingNoticeInfoWithBLOBs.setUpdateTime(new Date());
			givingNoticeInfoWithBLOBs.setState(ChangeAttendanceRequestState.Over.getValue());
			givingNoticeInfoWithBLOBs.setParentSignatureId(dbGni.getParentSignatureId());
			givingNoticeInfoWithBLOBs.setBankParentSignatureId(dbGni.getBankParentSignatureId());
			givingNoticeInfoMapper.updateByPrimaryKeyWithBLOBs(givingNoticeInfoWithBLOBs);
			caiMapper.updateLeaveDateById(givingNoticeInfoWithBLOBs.getLastDate(), userInfoMapper.getUserInfoByAccountId(dbGni.getChildId()).getId());

			// approve 发送邮件 add gfwang
			userFactory.sendParentEmailByChild(getSystemEmailMsg("givenotice_approved_title"), getSystemEmailMsg("givenotice_approved_content"), dbGni.getChildId(), true,
					EmailType.givenoticeApproved.getValue(), null, givingNoticeInfoWithBLOBs.getLastDate(), dbGni.getId(), givingNoticeInfoWithBLOBs.isPlan());

			// 管理员审批后,如果离园日期是当天则立即将小孩归档
			// (去除当天直接归档操作,实际操作为下一天执行) hxzhang
			// if (DateUtil.isCurrentDate(dbGni.getLastDate())) {
			// UserInfo child =
			// userInfoMapper.getUserInfoByAccountId(dbGni.getChildId());
			// userFactory.archivedChild(child, currentUser, new Date(),
			// EmailType.leaveCenter.getValue(), true);
			// return successResult2(2, getTipMsg("operation_success"));
			// }

			RequestLogInfo logInfo = requestLogInfoMapper.getRequestLogInfoByObjId(dbGni.getId());
			if (logInfo != null) {
				logInfo.setUpdateTime(new Date());
				logInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
				requestLogInfoMapper.updateByPrimaryKeySelective(logInfo);
			}

		}
		return successResult(getTipMsg("operation_success"));
	}

	@Override
	public ServiceResult<Object> updateOperaGivingNotice(String id, short opera, UserInfoVo currentUser) {
		// 只能操作申请或已审批的离园申请
		GivingNoticeInfoWithBLOBs dbGni = givingNoticeInfoMapper.selectByPrimaryKey(id);
		UserInfo userInfo = userInfoMapper.getUserInfoByAccountId(dbGni.getChildId());
		if (dbGni.getState() != ChangeAttendanceRequestState.Default.getValue() && dbGni.getState() != ChangeAttendanceRequestState.Over.getValue()) {
			return failResult(getTipMsg("no.permissions"));
		}
		// 园长只能操作本园小孩的离园申请
		if (isRole(currentUser, Role.CentreManager.getValue())) {
			UserInfo child = userInfoMapper.getUserInfoByAccountId(dbGni.getChildId());
			if (!currentUser.getUserInfo().getCentersId().equals(child.getCentersId())) {
				return failResult(getTipMsg("giving_notice_centreManager"));
			}
		}
		// 对该申请进行状态更新
		int count = givingNoticeInfoMapper.updateGivingNoticeInfoById(id, opera, new Date());
		// 清空小孩的LeaveDate
		if (dbGni.getState() == ChangeAttendanceRequestState.Over.getValue()) {
			caiMapper.updateLeaveDateById(null, userInfo.getId());
			// 删除GivingNotice approved的邮件记录
			emailMsgMapper.removeEmailMsgByUserId(userInfo.getId());
		}
		// 清空request day
		caiMapper.updateRequestDateById(null, userInfo.getId());
		if (count == 0) {
			return successResult(getTipMsg("operation_failed"));
		}
		if (opera == ChangeAttendanceRequestState.Cancel.getValue()) {
			// 管理员取消GivingNotice,给家长,CEO,园长发送邮件
			if (userFactory.havePlanEmail(id)) {
				userFactory.removePlanEmail(id);
			} else {
				userFactory.sendParentEmailByChild(getSystemEmailMsg("cancel_givenotice_title"), getSystemEmailMsg("cancel_givenotice_content"), dbGni.getChildId(), true,
						EmailType.cancel_givenotice.getValue(), null, dbGni.getLastDate(), null, false);
				userFactory.sendEmailCeoManager(getSystemEmailMsg("cancel_givenotice_title"), getSystemEmailMsg("cancel_givenotice_content"), dbGni.getChildId(),
						EmailType.cancel_givenotice.getValue(), null, dbGni.getLastDate(), null);
			}
		}

		RequestLogInfo logInfo = requestLogInfoMapper.getRequestLogInfoByObjId(dbGni.getId());
		if (logInfo != null) {
			logInfo.setUpdateTime(new Date());
			logInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
			requestLogInfoMapper.updateByPrimaryKeySelective(logInfo);
		}

		return successResult(getTipMsg("operation_success"));
	}

	private ServiceResult<Object> validateGivingNoticeInfo(GivingNoticeInfoWithBLOBs gniWithBLOBs, UserInfoVo currentUser) {
		// 1.同时只能存在一条离园申请
		int count = givingNoticeInfoMapper.getCountByChildId(gniWithBLOBs.getChildId());
		if (StringUtil.isEmpty(gniWithBLOBs.getId()) && count != 0) {
			return failResult(getTipMsg("giving_notice"));
		}
		// 2.新增时家长签名非空验证
		if (StringUtil.isEmpty(gniWithBLOBs.getId()) && StringUtil.isEmpty(gniWithBLOBs.getParentSignatureId())) {
			return failResult(getTipMsg("parent_signature"));
		}
		// 4.编辑时员工签名非空验证
		if (containRoles(currentUser.getRoleInfoVoList(), Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge)
				&& StringUtil.isEmpty(gniWithBLOBs.getStaffSignatureId())) {
			return failResult(getTipMsg("staff_signature"));
		}
		if (null == gniWithBLOBs.getLastDate()) {
			return failResult(getTipMsg("giving_notice_endDate_require"));
		}
		// 5.编辑时只能批准request的记录
		GivingNoticeInfoWithBLOBs dbGni = givingNoticeInfoMapper.selectByPrimaryKey(gniWithBLOBs.getId());
		if (StringUtil.isNotEmpty(gniWithBLOBs.getId()) && dbGni.getState() != ChangeAttendanceRequestState.Default.getValue()) {
			return failResult(getTipMsg("giving_notice_un_request"));
		}
		// 6.园长只能给本园的小孩离园申请,只能审批本园的小孩的离园申请
		if (isRole(currentUser, Role.CentreManager.getValue()) || isRole(currentUser, Role.EducatorSecondInCharge.getValue())) {
			UserInfo child = userInfoMapper.getUserInfoByAccountId(gniWithBLOBs.getChildId());
			if (!currentUser.getUserInfo().getCentersId().equals(child.getCentersId())) {
				return failResult(getTipMsg("giving_notice_centreManager"));
			}
		}
		// 7.Account Name长度验证
		if (StringUtil.isNotEmpty(gniWithBLOBs.getAccountName()) && gniWithBLOBs.getAccountName().length() > 50) {
			return failResult(getTipMsg("giving_notice_account_name"));
		}
		return successResult();
	}

	@Override
	public int updateGivingNoticeTimedTask(Date now) {
		// 离园申请时间到期后将小孩更新为离园状态
		logger.info("updateGivingNoticeTimedTask==>start");
		int count = givingNoticeInfoMapper.givingNoticeTimedTask(now);
		logger.info("updateGivingNoticeTimedTask==>end:" + count);
		return count;
	}

	/**
	 * @description 判断当前登录人角色值
	 * @author hxzhang
	 * @create 2016年7月20日下午7:36:08
	 * @version 1.0
	 * @param currentUser
	 *            当前登录人
	 * @param role
	 *            角色值
	 * @return 返回结果
	 */
	private boolean isRole(UserInfoVo currentUser, Short role) {
		List<RoleInfoVo> roleList = currentUser.getRoleInfoVoList();
		for (RoleInfoVo roleInfoVo : roleList) {
			if (role.compareTo(roleInfoVo.getValue()) == 0) {
				return true;
			}
		}
		return false;
	}

	@Override
	public int updateGivingNoticeStatusTimedTask(Date now) {
		// 将到期且未审批的离园申请更新为失效
		logger.info("updateGivingNoticeStatusTimedTask==>start");
		int count = givingNoticeInfoMapper.givingNoticeStatusTimedTask(now);
		logger.info("updateGivingNoticeStatusTimedTask==>end");
		return count;
	}

}
