package com.aoyuntek.aoyun.service.attendance.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.dao.AbsenteeRequestInfoMapper;
import com.aoyuntek.aoyun.dao.AccountInfoMapper;
import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.ChangeAttendanceRequestInfoMapper;
import com.aoyuntek.aoyun.dao.ChildAttendanceInfoMapper;
import com.aoyuntek.aoyun.dao.GivingNoticeInfoMapper;
import com.aoyuntek.aoyun.dao.RequestLogInfoMapper;
import com.aoyuntek.aoyun.dao.RoomInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.entity.po.AbsenteeRequestInfo;
import com.aoyuntek.aoyun.entity.po.AbsenteeRequestInfoWithBLOBs;
import com.aoyuntek.aoyun.entity.po.ChangeAttendanceRequestInfo;
import com.aoyuntek.aoyun.entity.po.ChildAttendanceInfo;
import com.aoyuntek.aoyun.entity.po.RequestLogInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.vo.EmailVo;
import com.aoyuntek.aoyun.entity.vo.LogRequestType;
import com.aoyuntek.aoyun.entity.vo.RoleInfoVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.base.UserVo;
import com.aoyuntek.aoyun.enums.ChangeAttendanceRequestState;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.EmailType;
import com.aoyuntek.aoyun.enums.RequestLogType;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.factory.UserFactory;
import com.aoyuntek.aoyun.service.IMsgEmailInfoService;
import com.aoyuntek.aoyun.service.IUserInfoService;
import com.aoyuntek.aoyun.service.attendance.IAbsenteeService;
import com.aoyuntek.aoyun.service.attendance.IAttendanceService;
import com.aoyuntek.aoyun.service.impl.BaseWebServiceImpl;
import com.aoyuntek.aoyun.uitl.EmailUtil;
import com.theone.common.util.ServiceResult;
import com.theone.date.util.DateUtil;
import com.theone.string.util.StringUtil;

@Service
public class AbsenteeServiceImpl extends BaseWebServiceImpl<AbsenteeRequestInfo, AbsenteeRequestInfoMapper> implements IAbsenteeService {
	public static Logger logger = Logger.getLogger(AbsenteeServiceImpl.class);
	@Autowired
	private AbsenteeRequestInfoMapper absenteeRequestInfoMapper;
	@Autowired
	private RequestLogInfoMapper requestLogInfoMapper;
	@Autowired
	private UserInfoMapper userInfoMapper;
	@Autowired
	private IUserInfoService userInfoService;
	@Autowired
	private CentersInfoMapper centersInfoMapper;
	@Autowired
	private RoomInfoMapper roomInfoMapper;
	@Autowired
	private IMsgEmailInfoService msgEmailInfoService;
	@Autowired
	private UserFactory userFactory;
	@Autowired
	private IAttendanceService attendanceService;
	@Autowired
	private AccountInfoMapper accountInfoMapper;
	@Autowired
	private GivingNoticeInfoMapper givingNoticeInfoMapper;
	@Autowired
	private ChangeAttendanceRequestInfoMapper changeAttendanceRequestInfoMapper;
	@Autowired
	private ChildAttendanceInfoMapper childAttendanceInfoMapper;

	@Override
	public ServiceResult<Object> getAbsentee(String id, String userId) {
		AbsenteeRequestInfoWithBLOBs ari;
		if (StringUtil.isEmpty(id)) {
			ari = new AbsenteeRequestInfoWithBLOBs();
			ari.setChildId(accountInfoMapper.getAccountInfoByUserId(userId).getId());
			ari.setAbsentNum(getTotalDays(ari));
		} else {
			ari = absenteeRequestInfoMapper.selectByPrimaryKey(id);

		}
		return successResult((Object) ari);
	}

	@Override
	public ServiceResult<Object> addOrUpdateAbsentee(AbsenteeRequestInfoWithBLOBs ariWithBLOBs, UserInfoVo currentUser, boolean isSure) {
		// 验证
		ServiceResult<Object> result = validateAbsentee(ariWithBLOBs, currentUser);
		if (!result.isSuccess()) {
			return result;
		}
		ServiceResult<Object> r = showConfirm(ariWithBLOBs, isSure);
		if (!r.isSuccess()) {
			return r;
		}
		if (StringUtil.isEmpty(ariWithBLOBs.getId())) {
			// 新增
			String id = UUID.randomUUID().toString();
			ariWithBLOBs.setId(id);
			ariWithBLOBs.setCreateAccountId(currentUser.getAccountInfo().getId());
			Date now = new Date();
			ariWithBLOBs.setCreateTime(now);
			ariWithBLOBs.setUpdateTime(now);
			ariWithBLOBs.setDeleteFlag(DeleteFlag.Default.getValue());
			// 管理员给小孩请假,请假状态为已审批的.
			if (isRole(currentUser, Role.CEO.getValue()) || isRole(currentUser, Role.CentreManager.getValue())
					|| isRole(currentUser, Role.EducatorSecondInCharge.getValue())) {
				// approve 发送邮件 add gfwang
				userFactory.sendParentEmailByChild(getSystemEmailMsg("absentee_byParent_email_title"), getSystemEmailMsg("absentee_byParent_email_content"),
						ariWithBLOBs.getChildId(), true, EmailType.absentee_approved_email.getValue(), ariWithBLOBs.getBeginDate(), ariWithBLOBs.getEndDate(), null,
						false);

				ariWithBLOBs.setState(ChangeAttendanceRequestState.Over.getValue());
			} else {
				// 非管理员(家长)请假请假时间小于当前时间请假状态为失效,不小于当前时间请假状态为申请
				if (DateUtil.getPreviousDay(ariWithBLOBs.getEndDate()) < DateUtil.getPreviousDay(new Date())) {
					ariWithBLOBs.setState(ChangeAttendanceRequestState.lapsed.getValue());
					userFactory.sendParentEmailByChild(getSystemEmailMsg("lapsed_absentee_title"), getSystemEmailMsg("lapsed_absentee_content"),
							ariWithBLOBs.getChildId(), true, EmailType.lapsed_absentee.getValue(), ariWithBLOBs.getBeginDate(), ariWithBLOBs.getEndDate(), null, false);
					userFactory.sendEmailCeoManager(getSystemEmailMsg("lapsed_absentee_title"), getSystemEmailMsg("lapsed_absentee_content"), ariWithBLOBs.getChildId(),
							EmailType.lapsed_absentee.getValue(), ariWithBLOBs.getBeginDate(), ariWithBLOBs.getEndDate(), null);
				} else {
					ariWithBLOBs.setState(ChangeAttendanceRequestState.Default.getValue());
					userFactory.sendParentEmailByChild(getSystemEmailMsg("request_absentee_title"), getSystemEmailMsg("request_absentee_content"),
							ariWithBLOBs.getChildId(), true, EmailType.request_absentee.getValue(), ariWithBLOBs.getBeginDate(), ariWithBLOBs.getEndDate(), null, false);
					userFactory.sendEmailCeoManager(getSystemEmailMsg("request_absentee_title"), getSystemEmailMsg("request_absentee_content"), ariWithBLOBs.getChildId(),
							EmailType.request_absentee.getValue(), ariWithBLOBs.getBeginDate(), ariWithBLOBs.getEndDate(), null);
				}
			}
			absenteeRequestInfoMapper.insertSelective(ariWithBLOBs);
			// 添加请假申请记录
			RequestLogInfo rli = new RequestLogInfo();
			rli.setId(UUID.randomUUID().toString());
			rli.setChildId(ariWithBLOBs.getChildId());
			rli.setCreateAccountId(currentUser.getAccountInfo().getId());
			rli.setObjId(id);
			// TODO big
			// rli.setOperaFlag(OperaFlag.Yes.getValue());
			rli.setType(RequestLogType.AbsenteeRequest.getValue());
			rli.setCreateTime(now);
			rli.setDeleteFlag(DeleteFlag.Default.getValue());
			rli.setMsg(getSystemMsg("absentee_request") + " " + "(" + ariWithBLOBs.getChildName() + ")");
			rli.setUpdateTime(now);
			rli.setLogType(LogRequestType.AbsenteeRequest.getValue());
			requestLogInfoMapper.insertSelective(rli);
			// 判断是否发送提醒邮件
			judgeSendPromptEmail(ariWithBLOBs);
		} else {
			// approve 发送邮件 add gfwang
			userFactory.sendParentEmailByChild(getSystemEmailMsg("absentee_byParent_email_title"), getSystemEmailMsg("absentee_byParent_email_content"),
					ariWithBLOBs.getChildId(), true, EmailType.absentee_approved_email.getValue(), ariWithBLOBs.getBeginDate(), ariWithBLOBs.getEndDate(), null, false);
			AbsenteeRequestInfoWithBLOBs dbAri = absenteeRequestInfoMapper.selectByPrimaryKey(ariWithBLOBs.getId());
			ariWithBLOBs.setCreateAccountId(dbAri.getCreateAccountId());
			ariWithBLOBs.setCreateTime(dbAri.getCreateTime());
			ariWithBLOBs.setDeleteFlag(dbAri.getDeleteFlag());
			ariWithBLOBs.setState(dbAri.getState());
			ariWithBLOBs.setUpdateAccountId(currentUser.getAccountInfo().getId());
			ariWithBLOBs.setUpdateTime(new Date());
			ariWithBLOBs.setState(ChangeAttendanceRequestState.Over.getValue());
			ariWithBLOBs.setParentSignatureId(dbAri.getParentSignatureId());
			absenteeRequestInfoMapper.updateByPrimaryKeyWithBLOBs(ariWithBLOBs);

			RequestLogInfo logInfo = requestLogInfoMapper.getRequestLogInfoByObjId(dbAri.getId());
			if (logInfo != null) {
				logInfo.setUpdateTime(new Date());
				logInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
				requestLogInfoMapper.updateByPrimaryKeySelective(logInfo);
			}

		}
		return successResult(getTipMsg("operation_success"), (Object) ariWithBLOBs);
	}

	/**
	 * @description 弹出确认框
	 * @author hxzhang
	 * @create 2016年12月5日下午2:33:49
	 */
	private ServiceResult<Object> showConfirm(AbsenteeRequestInfoWithBLOBs ariWithBLOBs, boolean isSure) {
		// 超过42天提示信息
		if (!isSure && ariWithBLOBs.getAbsentTotalNum() > Integer.parseInt(getSystemMsg("totalDays"))) {
			return failResult(2, getTipMsg("absentee_more_than_42days"));
		} else {
			return successResult();
		}
	}

	/**
	 * @description 获取小孩的有效请假总天数
	 * @author hxzhang
	 * @create 2016年12月5日下午1:34:40
	 */
	private int getTotalDays(AbsenteeRequestInfoWithBLOBs ariWithBLOBs) {
		Map<String, Date> map = handDateTime(ariWithBLOBs);
		return absenteeRequestInfoMapper.getTotalDay(ariWithBLOBs.getChildId(), map.get("startDate"), map.get("endDate"));
	}

	/**
	 * @description 处理时间(判断是7月1号到第二年的6月30号)
	 * @author hxzhang
	 * @create 2016年12月12日下午4:07:36
	 */
	private Map<String, Date> handDateTime(AbsenteeRequestInfoWithBLOBs ariWithBLOBs) {
		Map<String, Date> map = new HashMap<String, Date>();
		String start = getSystemMsg("startStr");
		String end = getSystemMsg("endStr");
		int contrastValue = Integer.parseInt(getSystemMsg("contrastValue"));
		Calendar c = Calendar.getInstance();
		if (ariWithBLOBs.getBeginDate() != null) {
			c.setTime(ariWithBLOBs.getBeginDate());
		}
		int y = c.get(Calendar.YEAR);
		int m = c.get(Calendar.MONTH) + 1;
		if (m >= contrastValue) {
			start = String.valueOf(y) + start;
			end = String.valueOf((y + 1)) + end;
		} else {
			start = String.valueOf((y - 1)) + start;
			end = String.valueOf(y) + end;
		}
		Date startDate = DateUtil.parse(start, "yyyy/MM/dd");
		Date endDate = DateUtil.parse(end, "yyyy/MM/dd");
		map.put("startDate", startDate);
		map.put("endDate", endDate);
		return map;
	}

	/**
	 * @description 判断是否需要发送超过42天的提醒邮件
	 * @author hxzhang
	 * @create 2016年10月27日上午9:39:29
	 */
	private void judgeSendPromptEmail(AbsenteeRequestInfoWithBLOBs ariWithBLOBs) {
		// 小孩子在7月1日到第二年的6月30日期间请假42天需要给CEO和园长发送邮件
		// long totalDays = getAbsenteeTotalDays(ariWithBLOBs);
		logger.info("judgeSendPromptEmail---------start");
		if (ariWithBLOBs.getAbsentTotalNum() > Long.parseLong(getSystemMsg("totalDays"))) {
			UserInfo child = userInfoMapper.getUserInfoByAccountId(ariWithBLOBs.getChildId());
			List<UserVo> ceoList = userInfoMapper.getCEO();
			List<UserVo> managerList = userInfoMapper.getManagerByCenterId(child.getCentersId());
			ceoList.addAll(managerList);
			batchSendPromptEmail(ceoList, child);
			userFactory.sendParentEmailByChild(getSystemEmailMsg("absent_over_42days_toParent_title"), getSystemEmailMsg("absent_over_42days_toParent_content"),
					ariWithBLOBs.getChildId(), true, EmailType.absent_over_42days.getValue(), null, null, null, false);
		}
		logger.info("judgeSendPromptEmail---------end");
	}

	/**
	 * @description 批量发送提示邮件
	 * @author hxzhang
	 * @create 2016年10月26日下午5:05:35
	 */
	private void batchSendPromptEmail(List<UserVo> users, UserInfo child) {
		List<EmailVo> emailVoList = new ArrayList<EmailVo>();
		for (UserVo u : users) {
			String middleName = (u.getMiddleName() == null || "".equals(u.getMiddleName())) ? "" : " " + u.getMiddleName();
			String fullName = u.getFirstName() + middleName + " " + u.getLastName();
			u.setFullName(fullName);
			EmailVo emailVo = new EmailVo();
			emailVo.setSendAddress(getSystemMsg("mailHostAccount"));
			String title = getSystemEmailMsg("absent_over_42days_toCeo_title");
			emailVo.setSendName(title);
			emailVo.setEmail_key(getSystemMsg("email_key"));
			emailVo.setReceiverAddress(u.getEmail());
			emailVo.setReceiverName(userFactory.getUserName(u));
			emailVo.setSub(getSystemEmailMsg("absent_over_42days_toCeo_title"));
			// CentersInfo centersInfo =
			// centersInfoMapper.selectByPrimaryKey(child.getCentersId());
			// RoomInfo roomInfo =
			// roomInfoMapper.selectByPrimaryKey(child.getRoomId());
			String msg = MessageFormat.format(getSystemEmailMsg("absent_over_42days_toCeo_content"), u.getName(), userFactory.getUserName(child));
			String html = EmailUtil.replaceHtml(title, msg);
			emailVo.setMsg(html);
			emailVo.setHtml(true);
			emailVo.setReplyTos(null);
			emailVo.setAttachments(null);
			emailVoList.add(emailVo);
		}

		msgEmailInfoService.batchSendEamil(emailVoList);
	}

	@Override
	public ServiceResult<Object> updateOperaAbsenteeRequestInfo(String id, short opera, UserInfoVo currentUser) {
		// 请假不是申请或已审批状态是不能进行操作
		AbsenteeRequestInfoWithBLOBs dbAri = absenteeRequestInfoMapper.selectByPrimaryKey(id);
		if (dbAri.getState() != ChangeAttendanceRequestState.Default.getValue() && dbAri.getState() != ChangeAttendanceRequestState.Over.getValue()) {
			return failResult(getTipMsg("no.permissions"));
		}
		// 园长只能操作本园的请假
		if (isRole(currentUser, Role.CentreManager.getValue()) || isRole(currentUser, Role.EducatorSecondInCharge.getValue())) {
			UserInfo child = userInfoMapper.getUserInfoByAccountId(dbAri.getChildId());
			if (!currentUser.getUserInfo().getCentersId().equals(child.getCentersId())) {
				return failResult(getTipMsg("absentee_reques_centreManager"));
			}
		}
		// 更新该请假状态
		int count = absenteeRequestInfoMapper.updateAbsenteeRequestInfoById(id, opera, new Date());
		if (count == 0) {
			return successResult(getTipMsg("operation_failed"));
		}
		// 重新计算请假时间
		if (opera == ChangeAttendanceRequestState.Cancel.getValue()) {
			Map<String, Date> map = handDateTime(dbAri);
			// 获取该条记录的请假天数
			int minuend = dbAri.getAbsentTotalNum() - dbAri.getAbsentNum();
			absenteeRequestInfoMapper.updateAbsentNum(dbAri, minuend, map.get("startDate"), map.get("endDate"));
			// 取消要给家长,CEO,园长发送邮件
			userFactory.sendParentEmailByChild(getSystemEmailMsg("cancel_absentee_title"), getSystemEmailMsg("cancel_absentee_content"), dbAri.getChildId(), true,
					EmailType.cancel_absentee.getValue(), dbAri.getBeginDate(), dbAri.getEndDate(), null, false);
			userFactory.sendEmailCeoManager(getSystemEmailMsg("cancel_absentee_title"), getSystemEmailMsg("cancel_absentee_content"), dbAri.getChildId(),
					EmailType.cancel_absentee.getValue(), dbAri.getBeginDate(), dbAri.getEndDate(), null);
		}

		RequestLogInfo logInfo = requestLogInfoMapper.getRequestLogInfoByObjId(dbAri.getId());
		if (logInfo != null) {
			logInfo.setUpdateTime(new Date());
			logInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
			requestLogInfoMapper.updateByPrimaryKeySelective(logInfo);
		}

		return successResult(getTipMsg("operation_success"));
	}

	/**
	 * @description AbsenteeRequestInfo验证
	 * @author hxzhang
	 * @create 2016年8月28日上午11:55:26
	 * @version 1.0
	 * @param ariWithBLOBs
	 * @return
	 */
	private ServiceResult<Object> validateAbsentee(AbsenteeRequestInfoWithBLOBs ariWithBLOBs, UserInfoVo currentUser) {
		// 1.有且只能有一个request的Absentee
		int count = absenteeRequestInfoMapper.getCountByChildId(ariWithBLOBs.getChildId());
		if (StringUtil.isEmpty(ariWithBLOBs.getId()) && count != 0) {
			return failResult(getTipMsg("absentee_reques"));
		}
		// 2.新增时家长签字非空验证
		if (StringUtil.isEmpty(ariWithBLOBs.getId()) && StringUtil.isEmpty(ariWithBLOBs.getParentSignatureId())) {
			return failResult(getTipMsg("parent_signature"));
		}
		// 3.园长签名非空验证
		if (isRole2(currentUser, Role.CEO.getValue(), Role.CentreManager.getValue(), Role.EducatorSecondInCharge.getValue())
				&& StringUtil.isEmpty(ariWithBLOBs.getChargeSignatureId())) {
			return failResult(getTipMsg("charge_signature"));
		}
		// 开始时间必填 gfwang
		if (null == ariWithBLOBs.getBeginDate()) {
			return failResult(getTipMsg("absentee_beginDate_require"));
		}
		if (null == ariWithBLOBs.getEndDate()) {
			return failResult(getTipMsg("absentee_endDate_require"));
		}

		// 4.编辑时只能编辑request的记录
		AbsenteeRequestInfoWithBLOBs dbAri = absenteeRequestInfoMapper.selectByPrimaryKey(ariWithBLOBs.getId());
		if (StringUtil.isNotEmpty(ariWithBLOBs.getId()) && dbAri.getState() != ChangeAttendanceRequestState.Default.getValue()) {
			return failResult(getTipMsg("absentee_reques_un_request"));
		}
		// // 5.请假开始时间必须大于当天
		// if (DateUtil2.getPreviousDay(ariWithBLOBs.getBeginDate()) <
		// DateUtil2.getPreviousDay(new Date())) {
		// return failResult(getTipMsg("absentee_reques_beginDate"));
		// }
		// 6.请假开始时间必须小于结束时间
		if (DateUtil.getPreviousDay(ariWithBLOBs.getBeginDate()) > DateUtil.getPreviousDay(ariWithBLOBs.getEndDate())) {
			return failResult(getTipMsg("absentee_reques_date"));
		}
		// 7.园长只能给本园的小孩请假
		if (isRole(currentUser, Role.CentreManager.getValue()) || isRole(currentUser, Role.EducatorSecondInCharge.getValue())) {
			UserInfo child = userInfoMapper.getUserInfoByAccountId(ariWithBLOBs.getChildId());
			if (!currentUser.getUserInfo().getCentersId().equals(child.getCentersId())) {
				return failResult(getTipMsg("absentee_reques_centreManager"));
			}
		}

		// 8.请假时间段不能有重复 gfwang add
		if (isHasRepeatCount(ariWithBLOBs.getChildId(), ariWithBLOBs.getBeginDate(), ariWithBLOBs.getEndDate())) {
			return failResult(getTipMsg("absentee_request_dateRange"));
		}
		// 9.该小孩在请假时间段内存在转园申请时申请或批准请假时不能操作
		List<ChangeAttendanceRequestInfo> rs = changeAttendanceRequestInfoMapper.haveChangeCentreRequest(ariWithBLOBs.getChildId(), ariWithBLOBs.getEndDate());
		if (rs.size() != 0) {
			return failResult(getTipMsg("absentee_turn_center"));
		}
		// 10.该小孩在请假时间段内存在离园申请申请或批准请假时不能操作
		List<ChildAttendanceInfo> cas = childAttendanceInfoMapper.haveChildAttendance(userInfoMapper.getUserInfoByAccountId(ariWithBLOBs.getChildId()).getId(),
				ariWithBLOBs.getEndDate());
		if (cas.size() != 0) {
			return failResult(getTipMsg("absentee_leave_center"));
		}
		return successResult();
	}

	/**
	 * @description 计算小孩在7月1号到第二年的6月30号期间请假总天数
	 * @author hxzhang
	 * @create 2016年10月27日上午9:40:50
	 */
	private long getAbsenteeTotalDays(AbsenteeRequestInfoWithBLOBs a) {
		String start = getSystemMsg("startStr");
		String end = getSystemMsg("endStr");
		int contrastValue = Integer.parseInt(getSystemMsg("contrastValue"));
		Calendar c = Calendar.getInstance();
		int y = c.get(Calendar.YEAR);
		int m = c.get(Calendar.MONTH);
		if (m > contrastValue) {
			start = (y + "") + start;
			end = ((y + 1) + "") + end;
		} else {
			start = ((y - 1) + "") + start;
			end = (y + "") + end;
		}
		Date startDate = DateUtil.parse(start, "yyyy/MM/dd");
		Date endDate = DateUtil.parse(end, "yyyy/MM/dd");
		List<AbsenteeRequestInfoWithBLOBs> abList = absenteeRequestInfoMapper.getAbsenteeList(a.getChildId(), startDate, endDate);
		// 加上本次请假
		abList.add(a);
		long totalDays = 0;
		Date startTime = null;
		Date endTime = null;
		for (AbsenteeRequestInfo ab : abList) {
			if (ab.getBeginDate().after(startDate)) {
				startTime = ab.getBeginDate();
			}
			if (ab.getEndDate().before(endDate)) {
				endTime = ab.getEndDate();
			}
			totalDays += (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(startTime, endTime) + 1);
		}
		return totalDays;
	}

	public static void main(String[] args) {
		// String start = "2016/07/01";
		// String end = "2016/06/30";
		// // Calendar c = Calendar.getInstance();
		// // int y = c.get(Calendar.YEAR);
		// // int m = c.get(Calendar.MONTH);
		// // if (m > 7) {
		// // start = (y + "") + start;
		// // end = ((y + 1) + "") + end;
		// // } else {
		// // start = ((y - 1) + "") + start;
		// // end = (y + "") + end;
		// // }
		// System.err.println(start + " " + end);
		// Date startDate = DateUtil.parse(start, "yyyy/MM/dd");
		// Date endDate = DateUtil.parse(end, "yyyy/MM/dd");
		// long i = com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(endDate,
		// startDate);
		// System.err.println(i);

		System.err.println("Mandurah/AUSTRALIA (GREYHOUNDS)".split("/")[0]);
	}

	/**
	 * 
	 * @description 是否有重复的请假时间
	 * @author gfwang
	 * @create 2016年10月8日下午4:55:35
	 * @version 1.0
	 * @param childAccountId
	 * @return
	 */
	private boolean isHasRepeatCount(String childAccountId, Date startDate, Date endDate) {
		log.info("isHasRepeatCount|start|childId=" + childAccountId + ",startDate=" + startDate + ",endDate=" + endDate);
		int count = absenteeRequestInfoMapper.getDateRepeatCount(childAccountId, startDate, endDate);
		log.info("isHasRepeatCount|end|count=" + count);
		return count > 0 ? true : false;
	}

	@Override
	public int updateAbsenteeStatusTimedTask(Date now) {
		logger.info("updateAbsenteeTimedTask==>start");
		// 请假到期未审批将变成失效
		int count = absenteeRequestInfoMapper.absenteeTimedTask(now);
		logger.info("updateAbsenteeTimedTask==>end:" + count);
		return count;
	}

	/**
	 * @description 判断当前登录人角色值
	 * @author hxzhang
	 * @create 2016年7月20日下午7:36:08
	 * @version 1.0
	 * @param currentUser
	 *            当前登录人
	 * @param role
	 *            角色值
	 * @return 返回结果
	 */
	private boolean isRole(UserInfoVo currentUser, Short role) {
		List<RoleInfoVo> roleList = currentUser.getRoleInfoVoList();
		for (RoleInfoVo roleInfoVo : roleList) {
			if (role.compareTo(roleInfoVo.getValue()) == 0) {
				return true;
			}
		}
		return false;
	}

	private boolean isRole2(UserInfoVo currentUser, Short... roles) {
		List<RoleInfoVo> roleList = currentUser.getRoleInfoVoList();
		for (RoleInfoVo roleInfoVo : roleList) {
			for (Short r : roles) {
				if (r.compareTo(roleInfoVo.getValue()) == 0) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public ServiceResult<Object> getAbsenteeDays(AbsenteeRequestInfoWithBLOBs ariWithBLOBs) {
		if (ariWithBLOBs.getBeginDate() == null || ariWithBLOBs.getEndDate() == null) {
			return successResult((Object) ariWithBLOBs);
		}
		Date startDate = DateUtil.addDay(ariWithBLOBs.getBeginDate(), 1);
		Date endDate = DateUtil.addDay(ariWithBLOBs.getEndDate(), -1);
		int days = attendanceService.getChildAttendanceDays(ariWithBLOBs.getChildId(), startDate, endDate);
		int totalDays = getTotalDays(ariWithBLOBs);
		ariWithBLOBs.setAbsentNum(totalDays);
		ariWithBLOBs.setAbsentTotalNum(totalDays + days);
		return successResult((Object) ariWithBLOBs);
	}
}
