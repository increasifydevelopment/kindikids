package com.aoyuntek.aoyun.service.attendance.impl;

import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.RequestLogInfoMapper;
import com.aoyuntek.aoyun.dao.RoomInfoMapper;
import com.aoyuntek.aoyun.entity.po.CentersInfo;
import com.aoyuntek.aoyun.entity.po.ChangeAttendanceRequestInfo;
import com.aoyuntek.aoyun.entity.po.RequestLogInfo;
import com.aoyuntek.aoyun.entity.po.RoomInfo;
import com.aoyuntek.aoyun.entity.po.SubtractedAttendance;
import com.aoyuntek.aoyun.entity.po.TemporaryAttendanceInfo;
import com.aoyuntek.aoyun.entity.vo.LogRequestType;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.enums.AttendanceRequestType;
import com.aoyuntek.aoyun.enums.ChangeAttendanceRequestState;
import com.aoyuntek.aoyun.enums.ChangeAttendanceRequestType;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.RequestLogType;
import com.aoyuntek.aoyun.service.attendance.IRequestLogService;
import com.aoyuntek.aoyun.service.impl.BaseWebServiceImpl;
import com.aoyuntek.aoyun.uitl.TipMessageUtil;

@Service
public class RequestLogServiceImpl extends BaseWebServiceImpl<RequestLogInfo, RequestLogInfoMapper> implements IRequestLogService {

    @Autowired
    private CentersInfoMapper centersInfoMapper;
    @Autowired
    private TipMessageUtil tipMessageUtil;
    @Autowired
    private RequestLogInfoMapper requestLogInfoMapper;
    @Autowired
    private RoomInfoMapper roomInfoMapper;

    @Override
    public void dealLog(UserInfoVo currentUser, SubtractedAttendance subtractedAttendance) {
        // 產生請求消息，但是不給操作
        RequestLogInfo requestLogInfo = new RequestLogInfo();
        requestLogInfo.setId(UUID.randomUUID().toString());
        requestLogInfo.setChildId(subtractedAttendance.getChildId());
        requestLogInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
        requestLogInfo.setCreateTime(new Date());
        requestLogInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
        requestLogInfo.setUpdateTime(new Date());
        requestLogInfo.setDeleteFlag(DeleteFlag.Default.getValue());
        requestLogInfo.setMsg(tipMessageUtil.getRequestLogStr(subtractedAttendance));
        requestLogInfo.setObjId(subtractedAttendance.getId());
        requestLogInfo.setType(RequestLogType.SubtractedAttendance.getValue());
        requestLogInfo.setState(ChangeAttendanceRequestState.OnJob.getValue());
        requestLogInfo.setLogType(LogRequestType.ChangeAttendance.getValue());
        requestLogInfoMapper.insert(requestLogInfo);
    }

    @Override
    public void dealLog(UserInfoVo currentUser, TemporaryAttendanceInfo temp) {
        // 產生請求消息，但是不給操作
        RequestLogInfo requestLogInfo = new RequestLogInfo();
        requestLogInfo.setId(UUID.randomUUID().toString());
        requestLogInfo.setChildId(temp.getChildId());
        requestLogInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
        requestLogInfo.setCreateTime(new Date());
        requestLogInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
        requestLogInfo.setUpdateTime(new Date());
        requestLogInfo.setDeleteFlag(DeleteFlag.Default.getValue());
        requestLogInfo.setMsg(tipMessageUtil.getRequestLogStr(temp));
        requestLogInfo.setObjId(temp.getId());
        requestLogInfo.setType(RequestLogType.TemporaryAttendance.getValue());
        requestLogInfo.setState(ChangeAttendanceRequestState.OnJob.getValue());
        requestLogInfo.setLogType(LogRequestType.ChangeAttendance.getValue());
        requestLogInfoMapper.insert(requestLogInfo);
    }

    @Override
    public void dealLog(UserInfoVo currentUser, ChangeAttendanceRequestInfo re) {
        ChangeAttendanceRequestType type = ChangeAttendanceRequestType.getType(re.getSourceType());
        // 这里几种类型需要记录日志
        boolean isLog = (type == ChangeAttendanceRequestType.ParentRequest) || (type == ChangeAttendanceRequestType.CentreManagerRequest)
                || (type == ChangeAttendanceRequestType.CeoChangeCentre) || (type == ChangeAttendanceRequestType.ChangeRoom)
                || (type == ChangeAttendanceRequestType.ChangeAttendance);

        if (isLog) {
            // 產生請求消息，但是不給操作
            RequestLogInfo requestLogInfo = new RequestLogInfo();
            requestLogInfo.setId(UUID.randomUUID().toString());
            requestLogInfo.setChildId(re.getChildId());
            requestLogInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
            requestLogInfo.setCreateTime(new Date());
            requestLogInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
            requestLogInfo.setUpdateTime(new Date());
            requestLogInfo.setDeleteFlag(DeleteFlag.Default.getValue());
            if (re.getType() == AttendanceRequestType.C.getValue()) {
                CentersInfo centersInfo = centersInfoMapper.selectByPrimaryKey(re.getNewCentreId());
                requestLogInfo.setMsg(tipMessageUtil.getRequestLogStr(re, centersInfo.getName(), null));
                requestLogInfo.setLogType(LogRequestType.ChangeCentre.getValue());
            } else if (re.getType() == AttendanceRequestType.R.getValue()) {
                RoomInfo roomInfo = roomInfoMapper.selectByPrimaryKey(re.getNewRoomId());
                requestLogInfo.setMsg(tipMessageUtil.getRequestLogStr(re, null, roomInfo.getName()));
                requestLogInfo.setLogType(LogRequestType.ChangeRoom.getValue());
            } else {
                requestLogInfo.setMsg(tipMessageUtil.getRequestLogStr(re, null, null));
                requestLogInfo.setLogType(LogRequestType.ChangeAttendance.getValue());
            }

            requestLogInfo.setObjId(re.getId());
            requestLogInfo.setType(re.getSourceType());
            requestLogInfoMapper.insert(requestLogInfo);
        }
    }

}
