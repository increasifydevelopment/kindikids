package com.aoyuntek.aoyun.service.attendance;

import com.aoyuntek.aoyun.dao.TemporaryAttendanceInfoMapper;
import com.aoyuntek.aoyun.entity.po.TemporaryAttendanceInfo;
import com.aoyuntek.aoyun.service.IBaseWebService;

public interface ITemporaryAttendanceService extends IBaseWebService<TemporaryAttendanceInfo , TemporaryAttendanceInfoMapper>{

}
