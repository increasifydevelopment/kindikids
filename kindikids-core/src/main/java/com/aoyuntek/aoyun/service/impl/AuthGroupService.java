package com.aoyuntek.aoyun.service.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.condtion.AuthGroupCondition;
import com.aoyuntek.aoyun.dao.AccountInfoMapper;
import com.aoyuntek.aoyun.dao.AuthorityGroupInfoMapper;
import com.aoyuntek.aoyun.dao.RoleGroupRelationInfoMapper;
import com.aoyuntek.aoyun.dao.RoleInfoMapper;
import com.aoyuntek.aoyun.entity.po.AuthorityGroupInfo;
import com.aoyuntek.aoyun.entity.po.RoleGroupRelationInfo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.aoyun.entity.vo.UserInGroupInfo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.enums.AccountActiveStatus;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.GroupType;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.service.IAuthGroupService;
import com.aoyuntek.aoyun.uitl.MyListUtil;
import com.theone.date.util.DateUtil;
import com.theone.string.util.StringUtil;

/**
 * 
 * @description 权限组服务类
 * @author gfwang
 * @create 2016年8月29日下午2:55:17
 * @version 1.0
 */
@Service
public class AuthGroupService extends BaseWebServiceImpl<AuthorityGroupInfo, AuthorityGroupInfoMapper> implements IAuthGroupService {
    /**
     * log
     */
    private static Logger log = Logger.getLogger(AuthGroupService.class);
    /**
     * 数据库持久层
     */
    @Autowired
    AuthorityGroupInfoMapper auGroupInfoMapper;
    @Value("#{auth}")
    private Properties authProperties;

    @Autowired
    private RoleInfoMapper roleInfoMapper;

    @Autowired
    private RoleGroupRelationInfoMapper roleGroupRelationInfoMapper;

    @Autowired
    private AccountInfoMapper accountInfoMapper;

    @Override
    public void addOrUpdateGroup(String centerId, String centerName, String oldCenterName, boolean isAdd) {
        log.info("addOrUpdateGroup|start|centerId=" + centerId + "|centerName=" + centerName + "|oldCenterName=" + oldCenterName);
        String ftEduGroupName = MessageFormat.format(getAuthMsg("ft.educators"), centerName);
        String parentGroupName = MessageFormat.format(getAuthMsg("parent"), centerName);

        String childGroupName = MessageFormat.format(getAuthMsg("all.childs.center"), centerName);
        String staffGroupName = MessageFormat.format(getAuthMsg("all.staffs.center"), centerName);
        String eduGroupName = MessageFormat.format(getAuthMsg("all.edus.center"), centerName);
        String cookGroupName = MessageFormat.format(getAuthMsg("all.cooks.center"), centerName);

        if (isAdd) {
            Date now = new Date();
            // 新增家长组
            addGroupModel(centerId, parentGroupName, now, getAuthMsg("parent.sql"), 0);
            now = DateUtil.addSeconds(now, 1000);
            // 新增老师组
            addGroupModel(centerId, ftEduGroupName, now, getAuthMsg("ft.edu.sql"), 1);
            now = DateUtil.addSeconds(now, 1000);
            // 新增老师组
            addGroupModel(centerId, childGroupName, now, getAuthMsg("all.childs.center.sql"), 2);
            now = DateUtil.addSeconds(now, 1000);
            // 新增老师组
            addGroupModel(centerId, staffGroupName, now, getAuthMsg("all.staffs.center.sql"), 2);
            now = DateUtil.addSeconds(now, 1000);
            // 新增老师组
            addGroupModel(centerId, eduGroupName, now, getAuthMsg("all.edus.center.sql"), 2);
            now = DateUtil.addSeconds(now, 1000);
            // 新增老师组
            addGroupModel(centerId, cookGroupName, now, getAuthMsg("all.cooks.center.sql"), 2);

        } else {
            String oldEduFTGroupName = MessageFormat.format(getAuthMsg("ft.educators"), oldCenterName);
            String oldParentGroupName = MessageFormat.format(getAuthMsg("parent"), oldCenterName);

            String oldChildGroupName = MessageFormat.format(getAuthMsg("all.childs.center"), oldCenterName);
            String oldStaffsGroupName = MessageFormat.format(getAuthMsg("all.staffs.center"), oldCenterName);
            String oldEduGroupName = MessageFormat.format(getAuthMsg("all.edus.center"), oldCenterName);
            String oldCooksGroupName = MessageFormat.format(getAuthMsg("all.cooks.center"), oldCenterName);

            auGroupInfoMapper.updateGroupName(ftEduGroupName, oldEduFTGroupName);
            auGroupInfoMapper.updateGroupName(parentGroupName, oldParentGroupName);

            auGroupInfoMapper.updateGroupName(childGroupName, oldChildGroupName);
            auGroupInfoMapper.updateGroupName(staffGroupName, oldStaffsGroupName);
            auGroupInfoMapper.updateGroupName(eduGroupName, oldEduGroupName);
            auGroupInfoMapper.updateGroupName(cookGroupName, oldCooksGroupName);
        }
        log.info("addOrUpdateGroup|end|centerId=" + centerId + "|centerName=" + centerName);
    }

    /**
     * 
     * @description 新增分组
     * @author gfwang
     * @create 2016年8月29日下午3:20:09
     * @version 1.0
     * @param centerId
     *            园区id
     * @param accoutId
     *            accoutId
     * @param groupName
     *            分组名称
     */
    private void addGroupModel(String centerId, String groupName, Date now, String sql, int functionType) {
        log.info("addGroupModel|start|centerId" + centerId);
        AuthorityGroupInfo auGroup = new AuthorityGroupInfo();
        String sqlTag = "";
        // 替换占位符
        String replaceKey = "myCenterId";
        sqlTag = sql.replace(replaceKey, centerId);
        auGroup.setSqlTag(sqlTag);
        String authGroupId = UUID.randomUUID().toString();
        auGroup.setId(authGroupId);
        auGroup.setCreateTime(now);
        auGroup.setDeleteFlag(DeleteFlag.Default.getValue());
        auGroup.setStatus(AccountActiveStatus.Enabled.getValue());
        auGroup.setGroupName(groupName);
        auGroupInfoMapper.insert(auGroup);

        // 家长
        if (functionType == 0) {
            log.info("build news parent relation");
            // 建立message的parent和edu关系
            buildRelation(authGroupId, centerId, GroupType.MessageGroup, getAuthMsg("message.look.group.role").split(","));
            // 建立newfeed的parent关系
            buildRelation(authGroupId, centerId, GroupType.newsFeedGroup, getAuthMsg("newsfeed.look.group.parent.role").split(","));

            // 建立form和edu关系
            buildRelation(authGroupId, centerId, GroupType.FormGroup, new String[] { "0" });

        } else if (functionType == 1) {
            // 老师
            log.info("build news edu relation");
            // 建立message的parent和edu关系
            buildRelation(authGroupId, centerId, GroupType.MessageGroup, getAuthMsg("message.look.group.role2").split(","));

            // 建立newfeed的edu关系
            buildRelation(authGroupId, centerId, GroupType.newsFeedGroup, getAuthMsg("newsfeed.look.group.edu.role").split(","));

        } else {
            // 其它 form使用
            // 建立message的form和edu关系
            buildRelation(authGroupId, centerId, GroupType.FormGroup, new String[] { "0" });
        }
        log.info("addGroupModel|end");

    }

    /**
     * 
     * @description 建立关系
     * @author gfwang
     * @create 2016年9月12日下午3:32:36
     * @version 1.0
     * @param authGroupId
     * @param centerId
     * @param groupType
     * @param roleArr
     */
    private void buildRelation(String authGroupId, String centerId, GroupType groupType, String[] roleArr) {
        log.info("buildRelation|start|authGroupId=" + authGroupId + ",centerId=" + centerId + ",roleArr" + roleArr.toString());
        List<RoleGroupRelationInfo> relations = new ArrayList<RoleGroupRelationInfo>();
        for (String tmpRole : roleArr) {
            short role = Short.parseShort(tmpRole);
            // 如果为message
            if (groupType.getValue() == GroupType.MessageGroup.getValue()) {
                // 如果为CEO则不需要园区限制
                String roleId = roleInfoMapper.selectRoleIdByValue(role);
                // 去除园区是为了可以看见所有园区的All FT Educators
                // String insertCenterId = role == Role.CEO.getValue() ? null :
                // centerId;
                relations.add(new RoleGroupRelationInfo(roleId, authGroupId, groupType.getValue(), DeleteFlag.Default.getValue(), null, new Date()));
            } else if (groupType.getValue() == GroupType.newsFeedGroup.getValue()) {
                // 如果为newsfeed
                // 如果为CEO和兼职则不需要园区限制
                String roleId = roleInfoMapper.selectRoleIdByValue(role);
                String insertCenterId = role == Role.CEO.getValue() || role == Role.Casual.getValue() ? null : centerId;
                relations.add(new RoleGroupRelationInfo(roleId, authGroupId, groupType.getValue(), DeleteFlag.Default.getValue(), insertCenterId,
                        new Date()));
            } else {
                relations
                        .add(new RoleGroupRelationInfo(null, authGroupId, groupType.getValue(), DeleteFlag.Default.getValue(), centerId, new Date()));
            }
        }
        log.info("buildRelation|end");
        roleGroupRelationInfoMapper.inserBatch(relations);
    }

    private String getAuthMsg(String tipKey) {
        return authProperties.getProperty(tipKey);
    }

    @Override
    public List<SelecterPo> getGroupsByRole(UserInfoVo user, String keyWord, GroupType type) {
        log.info("getGroupsByRole|start");
        String centerId = user.getUserInfo().getCentersId();
        log.info("centerId=" + centerId);
        boolean messageCaualCenterManager = false;
        if (type.getValue() == GroupType.MessageGroup.getValue()) {
            // message 模块，兼职加二级园长，没有园区的情况下，不能看到家长和老师
            if (containRoles(user.getRoleInfoVoList(), Role.Casual, Role.EducatorSecondInCharge)
                    && StringUtil.isEmpty(user.getUserInfo().getCentersId())) {
                messageCaualCenterManager = true;
            }
        }
        List<SelecterPo> groups = roleGroupRelationInfoMapper.getGroupsByRoleId(new AuthGroupCondition(user.getRoleInfoVoList(), centerId, keyWord,
                type.getValue(), messageCaualCenterManager));
        log.info("getGroupsByRole|end");
        return groups;
    }

    @Override
    public List<String> isInGroup(UserInfoVo user, GroupType type) {
        boolean isParent = containRoles(user.getRoleInfoVoList(), Role.Parent);
        String parentGroupName = MessageFormat.format(getAuthMsg("parent"), "").trim();

        List<AuthorityGroupInfo> groups = auGroupInfoMapper.getGropList(type.getValue());
        List<String> result = new ArrayList<String>();
        String accountId = user.getAccountInfo().getId();
        for (AuthorityGroupInfo item : groups) {
            String id = item.getId();
            if (!isParent && item.getGroupName().startsWith(parentGroupName)) {
                continue;
            }

            if (id.equals(getAuthMsg("public_group_id"))) {
                result.add(id);
                continue;
            }
            String sql = item.getSqlTag();
            log.info("isInGroup|accountId=" + accountId + "name=" + id);
            if (StringUtil.isNotEmpty(accountId)) {
                StringBuffer sb = new StringBuffer("select count(1) from ( ");
                sb.append(sql);
                sb.append(" ) as result where result.account_id='" + accountId + "'");
                sql = sb.toString();
            }
            log.info("--->" + sql);
            long a = System.currentTimeMillis();
            int count = auGroupInfoMapper.inGroupTest(sql);
            System.err.println("----------" + id + "------->child=" + (System.currentTimeMillis() - a));
            if (count <= 0) {
                continue;
            }
            log.info("isInGroup|add name=" + id);
            result.add(id);
        }
        return result;
    }

    /**
     * 动态sql查询
     */
    @Override
    public List<UserInGroupInfo> dealAccountListByGroup(String groupId) {
        log.info("dealAccountListByGroup|start|groupId=" + groupId);
        String sql = roleGroupRelationInfoMapper.getSqlByGroup(groupId);
        List<UserInGroupInfo> list = accountInfoMapper.getUserByGroupSql(sql.trim());
        log.info("dealAccountListByGroup|end|sql=" + sql);
        return list;
    }

    public List<String> getAllGroups() {
        return auGroupInfoMapper.getAllGroups();
    }

    @Override
    public List<SelecterPo> getSelecterGroup(AuthGroupCondition condition) {
        return auGroupInfoMapper.getSelecterGroup(condition);
    }

    @Override
    public List<SelecterPo> dealSelectUsersByGroups(AuthGroupCondition condition) {
        List<String> sqlList = auGroupInfoMapper.getSqlsGroupIds(condition);
        Set<SelecterPo> sets = new LinkedHashSet<SelecterPo>();
        for (String sql : sqlList) {
            sets.addAll(auGroupInfoMapper.getSelecterByGroupSql(sql));
        }
        for (SelecterPo select : sets) {
            select.setDisabled(false);
        }
        if (StringUtil.isEmpty(condition.getKeyWord())) {
            return new MyListUtil<SelecterPo>().set2List(sets);
        }
        List<SelecterPo> list = new ArrayList<SelecterPo>();
        for (SelecterPo select : sets) {
            String text = select.getText().toLowerCase();
            if (text.contains(condition.getKeyWord().toLowerCase())) {
                list.add(select);
            }
        }
        // 这里处理排序
        Collections.sort(list, new Comparator<SelecterPo>() {
            @Override
            public int compare(SelecterPo arg0, SelecterPo arg1) {
                return arg0.getText().toLowerCase().compareTo(arg1.getText().toLowerCase());
            }
        });
        return list;
    }
}
