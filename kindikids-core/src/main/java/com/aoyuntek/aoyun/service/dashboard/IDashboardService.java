package com.aoyuntek.aoyun.service.dashboard;

import java.util.List;

import com.aoyuntek.aoyun.condtion.dashboard.DashboardCondition;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.dashboard.DashboardBaseDataVo;
import com.aoyuntek.aoyun.entity.vo.dashboard.DashboardGroup;
import com.aoyuntek.aoyun.service.IBaseWebService;
import com.theone.common.util.ServiceResult;

public interface IDashboardService extends IBaseWebService<UserInfo, UserInfoMapper> {
    /**
     * 
     * @description 获取下拉列表
     * @author gfwang
     * @create 2016年11月9日下午1:07:38
     * @version 1.0
     * @param currtenUser
     * @param p
     *            关键字
     * @return
     */
    List<DashboardGroup> getSelectList(UserInfoVo currtenUser, String p);

    /**
     * @description
     * @author hxzhang
     * @create 2016年11月9日下午2:38:12
     */
    ServiceResult<Object> getTaskCategories(DashboardCondition c);

    /**
     * 
     * @description 获取ActivitiesList
     * @author mingwang
     * @create 2016年11月9日下午3:13:06
     * @version 1.0
     * @param currentUser
     * @param condition
     * @return
     */
    DashboardBaseDataVo getActivitiesList(UserInfoVo currentUser, DashboardCondition condition);

    /**
     * 
     * @description 获取NQS
     * @author gfwang
     * @create 2016年11月9日下午5:15:27
     * @version 1.0
     * @param condition
     * @return
     */
    Object getNqs(DashboardCondition condition);

    /**
     * 
     * @description 获取QIP
     * @author sjwang
     * @create 2016年11月10日上午11:25:49
     * @version 1.0
     * @param c
     * @return
     */
    ServiceResult<Object> getQip(DashboardCondition c);

    /**
     * @description 获取TaskProgress
     * @author hxzhang
     * @create 2016年11月15日上午9:26:51
     */
    ServiceResult<Object> getTaskProgress(DashboardCondition c);
}
