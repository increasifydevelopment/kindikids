package com.aoyuntek.aoyun.service.attendance.impl;

import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.aoyuntek.aoyun.condtion.RosterCondition;
import com.aoyuntek.aoyun.condtion.SignCondition;
import com.aoyuntek.aoyun.constants.SystemConstants;
import com.aoyuntek.aoyun.dao.AccountInfoMapper;
import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.FamilyInfoMapper;
import com.aoyuntek.aoyun.dao.InterimAttendanceMapper;
import com.aoyuntek.aoyun.dao.SignChildInfoMapper;
import com.aoyuntek.aoyun.dao.SignSelationInfoMapper;
import com.aoyuntek.aoyun.dao.SignVisitorInfoMapper;
import com.aoyuntek.aoyun.dao.SigninInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.dao.roster.RosterInfoMapper;
import com.aoyuntek.aoyun.dao.roster.RosterStaffInfoMapper;
import com.aoyuntek.aoyun.entity.po.CentersInfo;
import com.aoyuntek.aoyun.entity.po.InterimAttendance;
import com.aoyuntek.aoyun.entity.po.SignChildInfoWithBLOBs;
import com.aoyuntek.aoyun.entity.po.SignSelationInfo;
import com.aoyuntek.aoyun.entity.po.SignVisitorInfo;
import com.aoyuntek.aoyun.entity.po.SignVisitorInfoWithBLOBs;
import com.aoyuntek.aoyun.entity.po.SigninInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.po.roster.RosterInfo;
import com.aoyuntek.aoyun.entity.po.roster.RosterStaffInfo;
import com.aoyuntek.aoyun.entity.vo.RoleInfoVo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.aoyun.entity.vo.SigninInfoVo;
import com.aoyuntek.aoyun.entity.vo.StaffSignInfoVo;
import com.aoyuntek.aoyun.entity.vo.UserDetail;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.roster.RosterStaffVo;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.LeaveType;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.enums.SigninState;
import com.aoyuntek.aoyun.enums.SigninType;
import com.aoyuntek.aoyun.enums.roster.RosterStatus;
import com.aoyuntek.aoyun.factory.UserFactory;
import com.aoyuntek.aoyun.service.attendance.IAttendanceService;
import com.aoyuntek.aoyun.service.attendance.ISignService;
import com.aoyuntek.aoyun.service.impl.BaseWebServiceImpl;
import com.aoyuntek.aoyun.uitl.SaltMD5Util;
import com.aoyuntek.aoyun.uitl.SmskUtil;
import com.theone.common.util.ServiceResult;
import com.theone.date.util.DateUtil;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

/**
 * @description 签到业务逻辑层
 * @author Hxzhang 2016年9月1日上午11:14:50
 * @version 1.0
 */
@Service
public class SignServiceImpl extends BaseWebServiceImpl<SigninInfo, SigninInfoMapper> implements ISignService {
	public static Logger log = Logger.getLogger(SignServiceImpl.class);
	@Autowired
	private SigninInfoMapper signinInfoMapper;
	@Autowired
	private SignVisitorInfoMapper signVisitorInfoMapper;
	@Autowired
	private SignSelationInfoMapper signSelationInfoMapper;
	@Autowired
	private CentersInfoMapper centersInfoMapper;
	@Autowired
	private AccountInfoMapper accountInfoMapper;
	@Autowired
	private UserInfoMapper userInfoMapper;
	@Autowired
	private SignChildInfoMapper signChildInfoMapper;
	@Autowired
	private FamilyInfoMapper familyInfoMapper;
	@Autowired
	private IAttendanceService attendanceService;
	@Autowired
	private RosterStaffInfoMapper rosterStaffInfoMapper;
	@Autowired
	private RosterInfoMapper rosterInfoMapper;
	@Autowired
	private DataSourceTransactionManager txManager;
	@Autowired
	private InterimAttendanceMapper interimAttendanceMapper;

	@Override
	public ServiceResult<Object> saveVisitorSignIn(SignVisitorInfoWithBLOBs sviWithBLOBs, String centreId) {
		// TODO 验证
		// 初始化签入信息
		String signinId = initSigninInfo(null, centreId, SigninType.IN.getValue(), null);
		// 初始化签入详细信息
		String signVisitorId = initOrUpdateSignVisitorInfo(sviWithBLOBs);
		// 初始化关系
		initSignSelationInfo(signinId, signVisitorId);

		return successResult(getTipMsg("signin_success"));
	}

	/**
	 * @description 初始化签入信息
	 * @author hxzhang
	 * @create 2016年9月1日下午2:46:09
	 * @version 1.0
	 * @param centreId
	 * @param currentUser
	 */
	private String initSigninInfo(String accountId, String centreId, short SigninType, String roomId) {
		SigninInfo signinInfo = new SigninInfo();
		String signinId = UUID.randomUUID().toString();
		signinInfo.setId(signinId);
		Date now = new Date();
		signinInfo.setCreateTime(now);
		signinInfo.setUpdateTime(now);
		signinInfo.setSignDate(now);
		signinInfo.setState(SigninState.Signin.getValue());
		signinInfo.setType(SigninType);
		signinInfo.setCentreId(centreId);
		signinInfo.setRoomId(roomId);
		signinInfo.setDeleteFlag(DeleteFlag.Default.getValue());
		signinInfo.setAccountId(accountId);
		signinInfoMapper.insertSelective(signinInfo);
		return signinId;
	}

	/**
	 * @description 初始化签入详细信息
	 * @author hxzhang
	 * @create 2016年9月1日下午2:47:22
	 * @version 1.0
	 * @param sviWithBLOBs
	 * @param centreId
	 * @param currentUser
	 */
	private String initOrUpdateSignVisitorInfo(SignVisitorInfoWithBLOBs sviWithBLOBs) {
		String signVisitorId = null;
		if (StringUtil.isEmpty(sviWithBLOBs.getId())) {
			signVisitorId = UUID.randomUUID().toString();
			sviWithBLOBs.setId(signVisitorId);
			Date now = new Date();
			sviWithBLOBs.setCreateTime(now);
			sviWithBLOBs.setUpdateTime(now);
			// gfwang by
			sviWithBLOBs.setPersonColor(radomColor());
			sviWithBLOBs.setDeleteFlag(DeleteFlag.Default.getValue());
			signVisitorInfoMapper.insertSelective(sviWithBLOBs);
		} else {
			SignVisitorInfoWithBLOBs dbSvi = signVisitorInfoMapper.selectByPrimaryKey(sviWithBLOBs.getId());
			signVisitorId = dbSvi.getId();
			dbSvi.setSignOutName(sviWithBLOBs.getSignOutName());
			dbSvi.setUpdateTime(new Date());
			signVisitorInfoMapper.updateByPrimaryKeyWithBLOBs(dbSvi);
		}
		return signVisitorId;
	}

	/**
	 * 
	 * @description 获取随机颜色
	 * @author gfwang
	 * @create 2016年9月7日下午8:34:00
	 * @version 1.0
	 * @return
	 */
	public String radomColor() {
		String[] colorArr = getSystemMsg("user.color").split(",");
		Integer num = (int) (Math.random() * colorArr.length);
		return colorArr[num];
	}

	/**
	 * @description 建立关系
	 * @author hxzhang
	 * @create 2016年9月1日下午3:16:01
	 * @version 1.0
	 */
	private void initSignSelationInfo(String signinId, String signVisitorId) {
		signSelationInfoMapper.removeSignById(signinId);
		SignSelationInfo ssi = new SignSelationInfo();
		ssi.setObjId(signVisitorId);
		ssi.setSignId(signinId);
		signSelationInfoMapper.insertSelective(ssi);
	}

	@Override
	public ServiceResult<Object> saveVisitorSignOut(SignVisitorInfoWithBLOBs sviWithBLOBs) {
		// 获取签入信息
		SigninInfo signinInfo = signinInfoMapper.getSigninInfoBySignVisitorId(sviWithBLOBs.getId());
		// 初始化签出主表信息
		String signinId = initSigninInfo(null, signinInfo.getCentreId(), SigninType.OUT.getValue(), null);
		// 更新签出子表信息
		String signVisitorId = initOrUpdateSignVisitorInfo(sviWithBLOBs);
		// 建立关系
		initSignSelationInfo(signinId, signVisitorId);

		return successResult(getTipMsg("signin_success"));
	}

	@Override
	public ServiceResult<Object> getVisitorSignInList(String centreId, String params) {
		List<SignVisitorInfo> sviList = signVisitorInfoMapper.getUnSignOutList(centreId, params);
		return successResult((Object) sviList);
	}

	@Override
	public ServiceResult<Object> getSignInInfo(String id) {
		SigninInfoVo signinInfoVo = signVisitorInfoMapper.getSigninInfoVo(id);
		if (null == signinInfoVo.getOutTime()) {
			signinInfoVo.setSignOutName(null);
		}
		return successResult((Object) signinInfoVo);
	}

	@Autowired
	private UserFactory userFactory;

	@Override
	public ServiceResult<Object> updateSignIn(SigninInfoVo signinInfoVo, UserInfoVo currentUser) {
		// TODO accountId不为空校验未加hxzhang
		boolean result = !userFactory.validatePermiss(null, currentUser, signinInfoVo.getCentreId());
		if (result) {
			return failResult(getTipMsg("no.permissions"));
		}
		signinInfoVo.setUpdateTime(new Date());
		signinInfoVo.setUpdateAccountId(currentUser.getAccountInfo().getId());
		// 新增或更新签出信息
		initOrUpdateSignout(null, signinInfoVo, currentUser);
		// 更新签入信息
		updateSignin(signinInfoVo, currentUser);
		// 更新签入签出子表信息
		updateSignVisitor(signinInfoVo, currentUser);
		// signVisitorInfoMapper.updateSigninInfoVo(signinInfoVo); 同时更新三条数据
		return successResult(getTipMsg("user.submit.success"));
	}

	/**
	 * @description 新增或编辑签出信息
	 * @author hxzhang
	 * @create 2016年9月6日下午5:26:53
	 * @version 1.0
	 * @param signinInfoVo
	 * @param currentUser
	 */
	private void initOrUpdateSignout(String accountId, SigninInfoVo signinInfoVo, UserInfoVo currentUser) {
		if (null == signinInfoVo.getOutTime()) {
			if (StringUtil.isNotEmpty(signinInfoVo.getSignoutId())) {
				// 删除签出信息
				signinInfoMapper.removeSignById(signinInfoVo.getSignoutId());
			}
			return;
		}
		if (StringUtil.isEmpty(signinInfoVo.getSignoutId())) {
			// 新增签出
			SigninInfo signinInfo = new SigninInfo();
			String signout = UUID.randomUUID().toString();
			signinInfo.setId(signout);
			signinInfo.setCentreId(signinInfoVo.getCentreId());
			signinInfo.setSignDate(signinInfoVo.getOutTime());
			signinInfo.setState(SigninState.Signin.getValue());
			signinInfo.setType(SigninType.OUT.getValue());
			Date now = new Date();
			signinInfo.setCreateTime(now);
			signinInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
			signinInfo.setUpdateTime(now);
			signinInfo.setDeleteFlag(DeleteFlag.Default.getValue());
			signinInfo.setAccountId(accountId);
			signinInfoMapper.insertSelective(signinInfo);
			// 建立关系
			SignSelationInfo ssi = new SignSelationInfo();
			ssi.setObjId(signinInfoVo.getId());
			ssi.setSignId(signout);
			signSelationInfoMapper.insertSelective(ssi);
		} else {
			// 更新
			SigninInfo signinInfo = signinInfoMapper.selectByPrimaryKey(signinInfoVo.getSignoutId());
			signinInfo.setSignDate(signinInfoVo.getOutTime());
			signinInfo.setCentreId(signinInfoVo.getCentreId());
			signinInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
			signinInfo.setUpdateTime(new Date());
			signinInfoMapper.updateByPrimaryKey(signinInfo);
		}
	}

	/**
	 * @description 更新签入信息
	 * @author hxzhang
	 * @create 2016年9月6日下午5:31:24
	 * @version 1.0
	 * @param signinInfoVo
	 * @param currentUser
	 */
	private void updateSignin(SigninInfoVo signinInfoVo, UserInfoVo currentUser) {
		SigninInfo signinInfo = signinInfoMapper.selectByPrimaryKey(signinInfoVo.getSigninId());
		signinInfo.setSignDate(signinInfoVo.getInTime());
		signinInfo.setCentreId(signinInfoVo.getCentreId());
		signinInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
		signinInfo.setUpdateTime(new Date());
		signinInfoMapper.updateByPrimaryKey(signinInfo);
	}

	/**
	 * @description 更新签入签出子表信息
	 * @author hxzhang
	 * @create 2016年9月6日下午5:41:39
	 * @version 1.0
	 * @param signinInfoVo
	 * @param currentUser
	 */
	private void updateSignVisitor(SigninInfoVo signinInfoVo, UserInfoVo currentUser) {
		SignVisitorInfoWithBLOBs svi = signVisitorInfoMapper.selectByPrimaryKey(signinInfoVo.getId());
		svi.setVisitReason(signinInfoVo.getReason());
		svi.setOrganisation(signinInfoVo.getOrganisation());
		svi.setContactNumber(signinInfoVo.getContactNum());
		svi.setSignInName(signinInfoVo.getSignInName());
		svi.setSignOutName(signinInfoVo.getSignOutName());
		svi.setUpdateAccountId(currentUser.getAccountInfo().getId());
		svi.setUpdateTime(new Date());
		signVisitorInfoMapper.updateByPrimaryKeyWithBLOBs(svi);
	}

	@Override
	public ServiceResult<Object> getExport(SignCondition condition, UserInfoVo currentUser) {
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		// 通过条件获取数据
		List<SigninInfoVo> signList = signVisitorInfoMapper.getExport(condition);
		for (SigninInfoVo sign : signList) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("firstName", sign.getFirstName());
			map.put("lastName", sign.getLastName());
			map.put("reason", sign.getReason());
			CentersInfo centre = centersInfoMapper.selectByPrimaryKey(sign.getCentreId());
			String centreName = "";
			// if (centre != null) {
			centreName = centre.getName();
			// }
			map.put("centre", centreName);
			map.put("organisation", sign.getOrganisation());
			map.put("contactNumber", sign.getContactNum());
			map.put("date", null != sign.getInTime() ? DateUtil.getDate(sign.getInTime(), "dd/MM/yyyy") : "");
			map.put("signinTime", null != sign.getInTime() ? DateUtil.formatDate(sign.getInTime(), "hh:mm a", Locale.ENGLISH) : "");
			map.put("signoutTime", null != sign.getOutTime() ? DateUtil.formatDate(sign.getOutTime(), "hh:mm a", Locale.ENGLISH) : "");
			map.put("signinName", sign.getSignInName());
			map.put("signoutName", sign.getSignOutName());
			resultList.add(map);
		}
		return successResult((Object) resultList);
	}

	@Override
	public ServiceResult<Object> getCentreId(String name, String currtenIp) {
		// 根据centreName获取centre信息
		CentersInfo centersInfo = centersInfoMapper.getCentreInfoByCentreName(name);
		if (null == centersInfo) {
			return failResult(getTipMsg("signout_centre_name"));
		}
		// add by gfwang ; update by mingwang
		if ("true".equals(getSystemMsg("ip.authority"))) {
			if (StringUtil.isEmpty(centersInfo.getCenterIp())) {
				return failResult(getTipMsg("sign.ip.fail"));
			}
			List<String> list = Arrays.asList(centersInfo.getCenterIp().split(","));
			if (!list.contains(currtenIp)) {
				return failResult(getTipMsg("sign.ip.fail"));
			}
		}
		return successResult((Object) centersInfo.getId());
	}

	@Override
	public ServiceResult<Object> saveStaffSigninOrSignout(String id, String centreId, String psw, short signinType) {
		if (signinType == SigninType.OUT.getValue()) {
			// 获取centre中签入还未签出的staff个数
			// update by mingwang
			List<UserInfo> staffList = (List<UserInfo>) getStaffList(centreId, "", 3).getReturnObj();
			int staffCount = staffList.size();
			// 获取centre中签入还未签出的child个数
			int childCount = attendanceService.getTodayAttendanceList(centreId, "", 1).getReturnObj().size();
			if (staffCount <= 2 && childCount > 0) {
				return failResult(2, getTipMsg("signout_child"));
			}
		}
		// 判断密码是否正确
		int count = accountInfoMapper.getCountByUserIdPsw(id, SaltMD5Util.saltMd5(getSystemMsg("salt"), psw));
		if (count == 0) {
			return failResult(getTipMsg("signin_failed_psw"));
		}
		// 保存签入或签出信息
		initSigninInfo(accountInfoMapper.getAccountInfoByUserId(id).getId(), centreId, signinType, null);
		CentersInfo c = centersInfoMapper.selectByPrimaryKey(centreId);
		return successResult((Object) c.getName());
	}

	@Override
	public ServiceResult<Object> getSignStaffList(String centreId, String name, boolean isSign) {
		RosterCondition rosterCondition = new RosterCondition();
		rosterCondition.initAll();
		rosterCondition.setCenterId(centreId);
		Date weekStart = DateUtil.getWeekStart(new Date());
		rosterCondition.setStartDate(weekStart);
		rosterCondition.setState(RosterStatus.Approved.getValue());
		if (isSign) {
			if (StringUtil.isNotEmpty(name)) {
				List<UserInfo> result = new ArrayList<UserInfo>();
				result = signinInfoMapper.getStaffListByKeyword(centreId, name);
				for (UserInfo u : result) {
					u.setRoleInfoVoList(userInfoMapper.getRoleByAccount(u.getAccountId()));
				}
				return successResult((Object) result);
			}
		}

		List<RosterInfo> rosterInfos = rosterInfoMapper.getList(rosterCondition);
		if (isSign && ListUtil.isEmpty(rosterInfos)) {
			return successResult((Object) new ArrayList<UserInfo>());
		}

		List<RosterStaffVo> rosterStaffVos = null;
		if (isSign) {
			rosterStaffVos = rosterStaffInfoMapper.getByRosterIdAndDay(rosterInfos.get(0).getId(), new Date());
		} else {
			rosterStaffVos = signinInfoMapper.getStaffSignByCentreAndDay(centreId, new Date());
		}
		List<UserInfo> staffList = new ArrayList<UserInfo>();

		boolean isAdd = false;
		List<String> signinIds = new ArrayList<String>();
		for (RosterStaffVo rosterStaffVo : rosterStaffVos) {
			isAdd = false;
			// 签到的
			if (isSign) {
				if (rosterStaffVo.getSinState() == null) {
					isAdd = true;
				}
			} else {
				if (rosterStaffVo.getSinState() != null && rosterStaffVo.getSinState() == SigninState.Signin.getValue() && rosterStaffVo.getSoutState() == null) {
					isAdd = true;
				}
			}
			if (isAdd) {
				if (!signinIds.contains(rosterStaffVo.getUserId())) {
					UserInfo userInfo = new UserInfo();
					userInfo.setId(rosterStaffVo.getUserId());
					userInfo.setCentersId(centreId);
					userInfo.setAvatar(rosterStaffVo.getAvatar());
					userInfo.setPersonColor(rosterStaffVo.getPersonColor());
					userInfo.setFullName(rosterStaffVo.getName());
					userInfo.setRoleInfoVoList(userInfoMapper.getRoleByAccount(rosterStaffVo.getStaffAccountId()));
					staffList.add(userInfo);

					signinIds.add(rosterStaffVo.getUserId());
				}
			}
		}

		if (StringUtil.isNotEmpty(name)) {
			List<UserInfo> result = new ArrayList<UserInfo>();
			for (UserInfo userInfo : staffList) {
				if (userInfo.getFullName().toLowerCase().contains(name.toLowerCase())) {
					result.add(userInfo);
				}
			}
			return successResult((Object) result);
		}

		return successResult((Object) staffList);
	}

	@Override
	public ServiceResult<Object> getStaffList(String centreId, String name, int flag) {
		// flag ：0 childSignin, 1 childSignout, 2 staffSignin, 3 staffSignout
		RosterCondition rosterCondition = new RosterCondition();
		rosterCondition.initAll();
		rosterCondition.setCenterId(centreId);
		Date weekStart = DateUtil.getWeekStart(new Date());
		rosterCondition.setStartDate(weekStart);
		rosterCondition.setState(RosterStatus.Approved.getValue());
		if (flag == 2) {
			if (StringUtil.isNotEmpty(name)) {
				List<UserInfo> result = new ArrayList<UserInfo>();
				result = signinInfoMapper.getStaffListByKeyword(centreId, name);
				return successResult((Object) result);
			}
		}
		List<RosterInfo> rosterInfos = rosterInfoMapper.getList(rosterCondition);
		// 此处不需要判断是否有排班,只查询已签入未签出的员工
		// if (ListUtil.isEmpty(rosterInfos)) {
		// return successResult((Object) new ArrayList<UserInfo>());
		// }
		List<RosterStaffVo> rosterStaffVos = null;
		if (flag == 2) {
			rosterStaffVos = rosterStaffInfoMapper.getByRosterIdAndDay(rosterInfos.get(0).getId(), new Date());
		} else {
			rosterStaffVos = signinInfoMapper.getStaffSignByCentreAndDay(centreId, new Date());
		}
		List<UserInfo> staffList = new ArrayList<UserInfo>();

		boolean isAdd = false;
		for (RosterStaffVo rosterStaffVo : rosterStaffVos) {
			isAdd = false;
			// 签到的
			if (flag == 2) {
				if (rosterStaffVo.getSinState() == null) {
					isAdd = true;
				}
			} else {
				if (rosterStaffVo.getSinState() != null && rosterStaffVo.getSinState() == SigninState.Signin.getValue() && rosterStaffVo.getSoutState() == null) {
					isAdd = true;
				}
			}
			if (isAdd) {
				UserInfo userInfo = new UserInfo();
				userInfo.setId(rosterStaffVo.getUserId());
				userInfo.setCentersId(centreId);
				userInfo.setAvatar(rosterStaffVo.getAvatar());
				userInfo.setPersonColor(rosterStaffVo.getPersonColor());
				userInfo.setFullName(rosterStaffVo.getName());
				userInfo.setRoleInfoVoList(rosterStaffVo.getRoleInfos());
				staffList.add(userInfo);
			}
		}
		return successResult((Object) staffList);
	}

	@Override
	public ServiceResult<Object> saveChildSignin(String accountId, String inName, boolean sunscreenApp) {
		log.info("saveChildSignin=====>start");
		// 判断该小孩是否已经签入
		ServiceResult<Object> result = oneSignEveryDay(accountId, SigninType.IN.getValue());
		if (!result.isSuccess()) {
			return result;
		}
		UserInfo user = userInfoMapper.getUserInfoByAccountId(accountId);
		// 初始化签到主表信息
		String signinId = initSigninInfo(accountId, user.getCentersId(), SigninType.IN.getValue(), user.getRoomId());
		// 初始化签到子表
		String siginChildId = initOrUpdateSignChildInfo(accountId, inName, null, sunscreenApp);
		// 建立关系
		initSignSelationInfo(signinId, siginChildId);
		CentersInfo c = centersInfoMapper.selectByPrimaryKey(user.getCentersId());
		log.info("saveChildSignin=====>end");
		return successResult((Object) c.getName());
	}

	/**
	 * @description 验证孩子每天只能签到一次
	 * @author hxzhang
	 * @create 2017年1月10日上午10:03:52
	 */
	private ServiceResult<Object> oneSignEveryDay(String accountId, short signType) {
		log.info("oneSignEveryDay=====>start");
		log.info("oneSignEveryDay=====>accountId" + accountId);
		log.info("oneSignEveryDay=====>signType" + signType);
		ServiceResult<Object> result = new ServiceResult<Object>();
		SigninInfo signinInfo = signinInfoMapper.havaSignForToday(accountId, signType);
		if (signinInfo != null) {
			log.info("signinInfo is not null");
			String msg = signType == SigninType.IN.getValue() ? getTipMsg("signin_is_exists") : getTipMsg("signout_is_exists");
			result.setSuccess(false);
			result.setMsg(msg);
			return result;
		}
		log.info("signinInfo is null");
		result.setSuccess(true);
		log.info("oneSignEveryDay=====>end");
		return result;
	}

	/**
	 * @description 初始化小孩签到子表
	 * @author hxzhang
	 * @create 2016年9月12日下午3:03:00
	 * @version 1.0
	 * @param sciWithBLOBs
	 * @return
	 */
	private String initOrUpdateSignChildInfo(String accountId, String inName, String outName, Boolean sunscreenApp) {
		// 通过小孩的accountId获取签到子表信息
		SignChildInfoWithBLOBs sciWithBLOBs = signChildInfoMapper.getSignChildInfoWithBLOBsByAccountId(accountId, new Date());
		String id = null;
		if (null == sciWithBLOBs) { // 签入新增,
			sciWithBLOBs = new SignChildInfoWithBLOBs();
			id = UUID.randomUUID().toString();
			sciWithBLOBs.setId(id);
			Date now = new Date();
			sciWithBLOBs.setCreateTime(now);
			sciWithBLOBs.setUpdateTime(now);
			sciWithBLOBs.setDeleteFlag(DeleteFlag.Default.getValue());
			sciWithBLOBs.setSignInName(inName);
			sciWithBLOBs.setSunscreenApp(sunscreenApp);
			signChildInfoMapper.insertSelective(sciWithBLOBs);
		} else { // 签出更新
			id = sciWithBLOBs.getId();
			sciWithBLOBs.setSignOutName(outName);
			sciWithBLOBs.setUpdateTime(new Date());
			signChildInfoMapper.updateByPrimaryKeyWithBLOBs(sciWithBLOBs);
		}
		return id;
	}

	@Override
	public ServiceResult<Object> saveChildSignout(String accountId, String outName) {
		log.info("saveChildSignout====>start");
		ServiceResult<Object> result = oneSignEveryDay(accountId, SigninType.OUT.getValue());
		if (!result.isSuccess()) {
			return result;
		}
		UserInfo user = userInfoMapper.getUserInfoByAccountId(accountId);
		// 初始化签入主表信息
		String signinId = initSigninInfo(accountId, user.getCentersId(), SigninType.OUT.getValue(), user.getRoomId());
		// 更新签到子表信息
		String signChildId = initOrUpdateSignChildInfo(accountId, null, outName, null);
		// 建立关系
		initSignSelationInfo(signinId, signChildId);
		CentersInfo c = centersInfoMapper.selectByPrimaryKey(user.getCentersId());
		log.info("saveChildSignout====>end");
		return successResult((Object) c.getName());
	}

	@Override
	public ServiceResult<Object> getChildSignInfo(String id, Date date, UserInfoVo currentUser) {
		if (date == null) {
			date = new Date();
		}
		// 通过小孩的accountId获取签到子表信息
		SignChildInfoWithBLOBs sci = signChildInfoMapper.getSignChildInfoWithBLOBsByAccountId(id, date);
		SigninInfoVo signinInfoVo = null;
		if (null != sci) {
			// 通过小孩签到信息
			signinInfoVo = signChildInfoMapper.getSigninInfoVo(sci.getId(), date);
			signinInfoVo.setGenerationSign(sci.getGenerationSign());
			signinInfoVo.setGenerationSignOut(sci.getGenerationSignOut());
			if (sci.getGenerationSign() != null && sci.getGenerationSign()) {
				signinInfoVo.setGenerationSignAccountId(sci.getGenerationSignAccountId());
				signinInfoVo.setSelecterPos(userInfoMapper.getSelecterPoByaccountId(sci.getGenerationSignAccountId()));
			}
			if (sci.getGenerationSignOut() != null && sci.getGenerationSignOut()) {
				signinInfoVo.setGenerationSignOutAccountId(sci.getGenerationSignOutAccountId());
				signinInfoVo.setSelecterPosOut(userInfoMapper.getSelecterPoByaccountId(sci.getGenerationSignOutAccountId()));
			}
		}
		if (signinInfoVo != null && (null == signinInfoVo.getOutTime())) {
			signinInfoVo.setSignOutName(null);
		}
		if (signinInfoVo == null) {
			UserInfo userInfo = userInfoMapper.getUserInfoByAccountId(id);
			signinInfoVo = new SigninInfoVo();
			signinInfoVo.setAvatar(userInfo.getAvatar());
			signinInfoVo.setFirstName(userInfo.getFirstName());
			signinInfoVo.setMiddleName(userInfo.getMiddleName());
			signinInfoVo.setLastName(userInfo.getLastName());
			signinInfoVo.setPersonColor(userInfo.getPersonColor());
		}
		return successResult((Object) signinInfoVo);
	}

	@Override
	public ServiceResult<Object> updateChildSign(SigninInfoVo signinInfoVo, UserInfoVo currentUser, boolean flag) {
		if (signinInfoVo.getInTime() == null) {
			return failResult(getTipMsg("signin_date_required"));
		}
		// 未来时间不能签到
		if (DateUtil.getPreviousDay(signinInfoVo.getInTime()) > DateUtil.getPreviousDay(new Date())) {
			return failResult(getTipMsg("signin_future_date"));
		}
		// 兼职二级园长未排班不能对小孩进行签到操作
		ServiceResult<Object> result = isCasualCentreManager(currentUser);
		if (!result.isSuccess()) {
			return result;
		}
		// 如果把签到日期改到周末,给出提示不能让其签到
		if (!flag && isWeekend(signinInfoVo.getInTime())) {
			return failResult(getTipMsg("sign_date_isweekend"));
		}
		// 如果是编辑的话
		boolean haveOld = false;
		SignChildInfoWithBLOBs sci = signChildInfoMapper.getSignChildInfoWithBLOBsByAccountId(signinInfoVo.getAccountId(), signinInfoVo.getInTime());
		if (sci != null) {
			log.info("SignChildInfoWithBLOBs=" + sci.toString());
			if (StringUtil.isEmpty(signinInfoVo.getId())) {
				haveOld = true;
				log.info("haveOld=" + haveOld);
			}
			if (StringUtil.isNotEmpty(signinInfoVo.getId()) && !signinInfoVo.getId().equals(sci.getId())) {
				haveOld = true;
				log.info("haveOld=" + haveOld);
			}
		}
		log.info("haveOld=" + haveOld);
		if (haveOld) {
			return failResult(getTipMsg("signin_date_allready_have_child"));
		}
		// ++++++++++++++++add by big++++++++++++++++++++++++
		// 如果是今天的话 看今天这个小孩在哪里上课 签到的园区改为小孩今天的园区
		if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(signinInfoVo.getInTime(), new Date()) == 0 && StringUtil.isNotEmpty(signinInfoVo.getId())) {
			SigninInfo signinInfo = signinInfoMapper.getSignInfoBySignChildId(signinInfoVo.getId(), SigninType.IN.getValue());
			if (StringUtil.isNotEmpty(signinInfo.getRoomId())) {
				UserInfo userInfo = userInfoMapper.getUserInfoByAccountId(signinInfo.getAccountId());
				if (!signinInfo.getRoomId().equals(userInfo.getRoomId())) {
					return failResult(getTipMsg("signin_date_room_change"));
				}
			}
		}
		// ++++++++++++++++++++++++++++++++++++++++++++++++++

		if (signinInfoVo.getOutTime() != null && signinInfoVo.getOutTime().getDay() != signinInfoVo.getInTime().getDay()) {
			return failResult(getTipMsg("signout_date_12"));
		}

		// 新增或编辑签到子表信息
		String signChildId = addOrUpdateSignChild(signinInfoVo, currentUser);
		// 新增或编辑签入信息
		String signinId = addOrUpdateSignin(signinInfoVo, currentUser, SigninType.IN.getValue());
		// 新增或编辑签出信息
		String signoutId = addOrUpdateSignin(signinInfoVo, currentUser, SigninType.OUT.getValue());
		// 建立关系
		if (StringUtil.isNotEmpty(signChildId) && StringUtil.isNotEmpty(signinId)) {
			initSignSelationInfo(signinId, signChildId);
		}
		if (StringUtil.isNotEmpty(signChildId) && StringUtil.isNotEmpty(signoutId)) {
			initSignSelationInfo(signoutId, signChildId);
		}
		// 删除当天可能存在的未签到信息
		// TODO big 删除当天可能存在的未签到信息
		signinInfoMapper.deleteNoSign(signinInfoVo.getAccountId(), signinInfoVo.getInTime());
		return successResult(getTipMsg("signin_success"));
	}

	/**
	 * @description 判断是否是周末
	 * @author hxzhang
	 * @create 2016年11月28日上午10:27:16
	 */
	private boolean isWeekend(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int week = cal.get(Calendar.DAY_OF_WEEK) - 1;
		if (week == 6 || week == 0) {// 0代表周日，6代表周六
			return true;
		}
		return false;
	}

	/**
	 * @description 新增或编辑小孩签到子表信息
	 * @author hxzhang
	 * @create 2016年9月14日下午5:12:44
	 * @version 1.0
	 * @param signinInfoVo
	 * @param currentUser
	 */
	private String addOrUpdateSignChild(SigninInfoVo signinInfoVo, UserInfoVo currentUser) {
		// 新增
		String id = null;
		if (StringUtil.isEmpty(signinInfoVo.getId())) {
			SignChildInfoWithBLOBs sciWithBLOBs = new SignChildInfoWithBLOBs();
			id = UUID.randomUUID().toString();
			sciWithBLOBs.setId(id);
			sciWithBLOBs.setSignInName(signinInfoVo.getSignInName());
			sciWithBLOBs.setSignOutName(signinInfoVo.getSignOutName());
			sciWithBLOBs.setCreateAccountId(currentUser.getAccountInfo().getId());
			sciWithBLOBs.setDeleteFlag(DeleteFlag.Default.getValue());
			if (StringUtil.isNotEmpty(signinInfoVo.getGenerationSignAccountId())) {
				sciWithBLOBs.setGenerationSign(signinInfoVo.getGenerationSign());
				sciWithBLOBs.setGenerationSignAccountId(signinInfoVo.getGenerationSignAccountId());
			}
			if (StringUtil.isNotEmpty(signinInfoVo.getGenerationSignOutAccountId())) {
				sciWithBLOBs.setGenerationSignOut(signinInfoVo.getGenerationSignOut());
				sciWithBLOBs.setGenerationSignOutAccountId(signinInfoVo.getGenerationSignOutAccountId());
			}
			Date now = new Date();
			sciWithBLOBs.setCreateTime(now);
			sciWithBLOBs.setUpdateTime(now);
			signChildInfoMapper.insert(sciWithBLOBs);
		} else { // 编辑
			id = signinInfoVo.getId();
			SignChildInfoWithBLOBs dbSci = signChildInfoMapper.selectByPrimaryKey(signinInfoVo.getId());
			dbSci.setUpdateAccountId(currentUser.getAccountInfo().getId());
			dbSci.setUpdateTime(new Date());
			dbSci.setSignInName(signinInfoVo.getSignInName());
			dbSci.setSignOutName(signinInfoVo.getSignOutName());
			if (StringUtil.isNotEmpty(signinInfoVo.getGenerationSignAccountId())) {
				dbSci.setGenerationSign(signinInfoVo.getGenerationSign());
				dbSci.setGenerationSignAccountId(signinInfoVo.getGenerationSignAccountId());
			}
			if (StringUtil.isNotEmpty(signinInfoVo.getGenerationSignOutAccountId())) {
				dbSci.setGenerationSignOut(signinInfoVo.getGenerationSignOut());
				dbSci.setGenerationSignOutAccountId(signinInfoVo.getGenerationSignOutAccountId());
			}
			signChildInfoMapper.updateByPrimaryKeyWithBLOBs(dbSci);
		}
		return id;
	}

	/**
	 * @description 新增或编辑签入信息
	 * @author hxzhang
	 * @create 2016年9月14日下午5:34:46
	 * @version 1.0
	 * @param signinInfoVo
	 * @param currentUser
	 * @return
	 */
	private String addOrUpdateSignin(SigninInfoVo signinInfoVo, UserInfoVo currentUser, short type) {
		// 签出时间为空则不生成签出记录
		if (SigninType.OUT.getValue() == type && null == signinInfoVo.getOutTime()) {
			if (StringUtil.isNotEmpty(signinInfoVo.getSignoutId())) {
				// 删除签出信息
				signinInfoMapper.removeSignById(signinInfoVo.getSignoutId());
				// 删除关系
				signSelationInfoMapper.removeSignById(signinInfoVo.getSignoutId());
			}
			return null;
		}
		String id = null;
		// 子表,主表信息都不存在,新增
		if (StringUtil.isEmpty(signinInfoVo.getId())) {
			id = initSignChild(signinInfoVo, currentUser, type);
		} else {
			// 获取签到主表信息
			SigninInfo signinInfo = signinInfoMapper.getSignInfoBySignChildId(signinInfoVo.getId(), type);
			// 主表信息存在则更新主表信息
			if (null != signinInfo) {
				signinInfo.setSignDate(type == SigninType.IN.getValue() ? signinInfoVo.getInTime() : signinInfoVo.getOutTime());
				signinInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
				signinInfo.setUpdateTime(new Date());
				signinInfoMapper.updateByPrimaryKey(signinInfo);
				id = signinInfo.getId();
			} else { // 主表信息不存在则新增主表信息
				id = initSignChild(signinInfoVo, currentUser, type);
			}
		}
		return id;
	}

	/**
	 * @description 初始化小孩签到主表信息
	 * @author hxzhang
	 * @create 2016年9月18日上午8:52:30
	 * @version 1.0
	 * @param signinInfoVo
	 * @param currentUser
	 * @param type
	 * @return
	 */
	private String initSignChild(SigninInfoVo signinInfoVo, UserInfoVo currentUser, short type) {
		SigninInfo signinInfo = new SigninInfo();
		String id = UUID.randomUUID().toString();
		signinInfo.setId(id);
		Date date = new Date();
		if (type == SigninType.IN.getValue() && signinInfoVo.getInTime() != null) {
			date = signinInfoVo.getInTime();
		} else if (type == SigninType.OUT.getValue() && signinInfoVo.getOutTime() != null) {
			date = signinInfoVo.getOutTime();
		}
		signinInfo.setSignDate(date);
		signinInfo.setAccountId(signinInfoVo.getAccountId());
		UserInfo user = userInfoMapper.getUserInfoByAccountId(signinInfoVo.getAccountId());
		if (signinInfoVo.isInterim()) {
			InterimAttendance interim = interimAttendanceMapper.getInterimAttend(signinInfoVo.getAccountId(), signinInfoVo.getInTime());
			signinInfo.setCentreId(interim != null ? interim.getCenterId() : user.getCentersId());
			signinInfo.setRoomId(interim != null ? interim.getRoomId() : user.getRoomId());
		} else {
			signinInfo.setCentreId(user.getCentersId());
			signinInfo.setRoomId(user.getRoomId());
		}
		signinInfo.setType(type);
		signinInfo.setState(SigninState.Signin.getValue());
		signinInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
		Date now = new Date();
		signinInfo.setCreateTime(now);
		signinInfo.setUpdateTime(now);
		signinInfo.setDeleteFlag(DeleteFlag.Default.getValue());
		signinInfoMapper.insertSelective(signinInfo);
		return id;
	}

	/**
	 * gfwang get child
	 */
	@Override
	public List<SelecterPo> getAllChilds(String p, String orgId, Integer type) {
		type = type == null ? -1 : type;
		if (type == 2) {
			return familyInfoMapper.getAllChilds(p, orgId, type);
		}
		if (StringUtil.isEmpty(p) && type == 1) {
			return new ArrayList<SelecterPo>();
		}
		return familyInfoMapper.getAllChilds(p, orgId, type);
	}

	@Override
	public ServiceResult<Object> getChildExport(SignCondition condition) {
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		List<SigninInfoVo> list = signChildInfoMapper.getExport(condition);
		for (SigninInfoVo sign : list) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("firstName", sign.getFirstName());
			map.put("middleName", sign.getMiddleName());
			map.put("lastName", sign.getLastName());
			map.put("centre", sign.getCentreName());
			map.put("room", sign.getRoomName());
			map.put("sunscreenApp", (null != sign.getSunscreenApp() && sign.getSunscreenApp() == true) ? "Yes" : "No");
			map.put("date", null != sign.getInTime() ? DateUtil.getDate(sign.getInTime(), "dd/MM/yyyy") : "");
			map.put("signinTime", null != sign.getInTime() ? DateUtil.formatDate(sign.getInTime(), "hh:mm a", Locale.ENGLISH) : "");
			map.put("signoutTime", null != sign.getOutTime() ? DateUtil.formatDate(sign.getOutTime(), "hh:mm a", Locale.ENGLISH) : "");
			map.put("signinName", sign.getSignInName());
			map.put("signoutName", sign.getSignOutName());
			resultList.add(map);
		}
		return successResult((Object) resultList);
	}

	@Override
	public void sendMsgByChildUnSignoutTimedTask(Date now) {
		List<UserDetail> childList = userInfoMapper.getUnSignoutChild(now);
		if (ListUtil.isEmpty(childList)) {
			log.info("sendMsgByChildUnSignoutTimedTask|childList empty");
			return;
		}
		Map<String, String> notSignOutChilds = new HashMap<String, String>();
		for (UserDetail user : childList) {
			String childName = userFactory.getUserName(user.getFirstName(), user.getMiddleName(), user.getLastName());
			String key = user.getCentersId() + SystemConstants.SPLITTAG + user.getCentreName();
			if (notSignOutChilds.containsKey(key)) {
				String tmp = notSignOutChilds.get(key);
				tmp += "," + childName;
				notSignOutChilds.put(key, tmp);
				continue;
			}
			notSignOutChilds.put(key, childName);
		}
		for (Map.Entry<String, String> entry : notSignOutChilds.entrySet()) {
			log.info(entry.getKey() + "," + entry.getValue());
		}
		// 获取CEO，院長,并发送短信
		List<UserInfo> managerList = userInfoMapper.getCentreManagerAndCeo();
		// 發送給院長
		for (UserInfo userManager : managerList) {
			String centreId = userManager.getCentersId();
			String managerName = userManager.getFullName();
			String phoneNumber = userManager.getMobileNumber();
			if (StringUtil.isEmpty(centreId)) {
				continue;
			}
			String contentTip = getContent(centreId, notSignOutChilds);
			if (StringUtil.isEmpty(contentTip)) {
				continue;
			}
			String content = MessageFormat.format(getSystemEmailMsg("child_not_sign_out_content"), managerName, contentTip);
			if (!StringUtil.isEmpty(phoneNumber)) {
				try {
					log.info("start send center phoneNumber=" + phoneNumber);
					log.info("start send center content=" + content);
					/*
					 * SmsUtil sms = new
					 * SmsUtil(Region.getRegion(Regions.AP_SOUTHEAST_2));
					 * sms.sendSms(content, phoneNumber);
					 */
					SmskUtil sms = new SmskUtil();
					sms.splitSendMessage(content, phoneNumber);
				} catch (Exception e) {
					log.info("connect error aoyun phoneNumber=" + phoneNumber);
				}
			}
		}
		String ceoContent = getContent(null, notSignOutChilds);
		// 发送给CEO
		for (UserInfo userManager : managerList) {
			String centreId = userManager.getCentersId();
			String managerName = userManager.getFullName();
			String phoneNumber = userManager.getMobileNumber();
			if (!StringUtil.isEmpty(centreId)) {
				continue;
			}
			// ceoContent = StringUtil.isEmpty(ceoContent) ? ceoContent :
			// ceoContent.substring(1, ceoContent.length());
			String content = MessageFormat.format(getSystemEmailMsg("child_not_sign_out_content"), managerName, ceoContent);
			if (!StringUtil.isEmpty(phoneNumber)) {
				try {
					log.info("start send ceo phoneNumber=" + phoneNumber);
					log.info("start send ceo content=" + content);
					SmskUtil sms = new SmskUtil();
					sms.splitSendMessage(content, phoneNumber);
					/*
					 * SmsUtil sms = new
					 * SmsUtil(Region.getRegion(Regions.AP_SOUTHEAST_2));
					 * sms.sendSms(content, phoneNumber);
					 */
				} catch (Exception e) {
					log.info("connect error aoyun phoneNumber=" + phoneNumber);
				}
			}
		}
	}

	private String getContent(String centreId, Map<String, String> notSignOutChilds) {
		String ceoContent = "";
		for (Map.Entry<String, String> entry : notSignOutChilds.entrySet()) {
			String[] key = entry.getKey().split(SystemConstants.SPLITTAG);
			// 小孩名称
			String value = entry.getValue();
			// 园区id
			String childCentreId = key[0];
			// 園區名稱
			String childCentreName = key[1];
			// 去除第一个，号
			if (value.startsWith(",")) {
				value = value.substring(1, value.length());
			}
			String tmpContent = MessageFormat.format(getSystemEmailMsg("child_not_sign_out_content_tip"), value, childCentreName);
			if (StringUtil.isEmpty(centreId)) {
				ceoContent += "," + tmpContent;
				continue;
			}
			if (centreId.equals(childCentreId)) {
				return tmpContent;
			}
		}
		return ceoContent.startsWith(",") ? ceoContent.substring(1, ceoContent.length()) : ceoContent;
	}

	@Override
	public ServiceResult<Object> getStaffSignInfo(String accountId, Date date, String centerId) {
		// 获取签到人角色
		List<RoleInfoVo> roleInfoVos = userInfoMapper.getRoleByAccount(accountId);
		// 判断是否是兼职,兼职可以多园区签到.
		boolean isCasual = containRoles(roleInfoVos, Role.Casual);
		if (!isCasual) {
			centerId = null;
		}

		StaffSignInfoVo staffSignInfoVo = signinInfoMapper.getStaffSignByAccountId(accountId, date, centerId);
		if (null == staffSignInfoVo) {
			UserInfo staff = userInfoMapper.getUserInfoByAccountId(accountId);
			staffSignInfoVo = new StaffSignInfoVo();
			staffSignInfoVo.setAccountId(accountId);
			staffSignInfoVo.setCentreId(staff.getCentersId());
			staffSignInfoVo.setAvatar(staff.getAvatar());
			staffSignInfoVo.setFirstName(staff.getFirstName());
			staffSignInfoVo.setMiddleName(staff.getMiddleName());
			staffSignInfoVo.setLastName(staff.getLastName());
			staffSignInfoVo.setPersonColor(staff.getPersonColor());
		}
		// 遇到补签到
		if (staffSignInfoVo != null && staffSignInfoVo.getSignInState() != null && staffSignInfoVo.getSignInState() == SigninState.NoSignin.getValue()) {
			staffSignInfoVo.setInTime(null);
		}
		return successResult((Object) staffSignInfoVo);
	}

	@Override
	public ServiceResult<Object> addOrUpdateStaffSign(StaffSignInfoVo staffSignInfoVo, UserInfoVo currentUser, Boolean comfirmSure) {
		String centerId = StringUtil.isNotEmpty(staffSignInfoVo.getCentreId()) ? staffSignInfoVo.getCentreId() : staffSignInfoVo.getLocation();
		// 获取签到人角色
		List<RoleInfoVo> roleInfoVos = userInfoMapper.getRoleByAccount(staffSignInfoVo.getAccountId());
		// 判断是否是兼职,兼职可以多园区签到.
		boolean isCasual = containRoles(roleInfoVos, Role.Casual);
		// 判断是否存在签到信息
		SigninInfo oldSign = signinInfoMapper.getSigninInfoByAccountIdAndDate(staffSignInfoVo.getAccountId(), staffSignInfoVo.getInTime(), centerId);
		// 如果是新增 那么如果没有排班 那么提示？//add by big
		if (!comfirmSure && oldSign == null) {
			// 查看是否有排班
			// update by big 2017-01-15
			int count = rosterStaffInfoMapper.getStaffAttendance(centerId, staffSignInfoVo.getAccountId(), staffSignInfoVo.getInTime());
			if (count == 0) {
				return failResult(2, getTipMsg("singn.staff.signinstaff.no.attance"));
			}
		}

		boolean flag = false;
		if (oldSign != null && oldSign.getState() != SigninState.NoSignin.getValue()) {
			if (StringUtil.isEmpty(staffSignInfoVo.getSigninId())) {
				if (isCasual && staffSignInfoVo.getCentreId().equals(oldSign.getCentreId())) {
					flag = true;
				}
				if (!isCasual) {
					flag = true;
				}
			}
			if (StringUtil.isNotEmpty(staffSignInfoVo.getSigninId()) && !staffSignInfoVo.getSigninId().equals(oldSign.getId())) {
				if (isCasual && staffSignInfoVo.getCentreId().equals(oldSign.getCentreId())) {
					flag = true;
				}
				if (!isCasual) {
					flag = true;
				}
			}
		}
		if (flag) {
			return failResult(getTipMsg("signin_date_allready_have"));
		}
		if (oldSign != null && oldSign.getState() == SigninState.NoSignin.getValue()) {
			if (!isCasual) {
				signinInfoMapper.deleteNoSign(staffSignInfoVo.getAccountId(), oldSign.getSignDate());
			} else {
				signinInfoMapper.deleteNoSignOfCentre(staffSignInfoVo.getAccountId(), oldSign.getSignDate(), centerId);
			}
		}

		// add by big
		if (!isCasual) {
			RosterStaffInfo rosterStaffInfo = rosterStaffInfoMapper.getStaffAttendanceInfo(staffSignInfoVo.getAccountId(), staffSignInfoVo.getInTime());
			if (rosterStaffInfo != null) {
				RosterInfo rosterInfo = rosterInfoMapper.selectByPrimaryKey(rosterStaffInfo.getRosterId());
				if (rosterInfo != null && !rosterInfo.getCenterId().equals(centerId)) {
					centerId = rosterInfo.getCenterId();
				}
			}
		}

		// 新增或编辑签入信息
		initOrUpdateStaffSign(staffSignInfoVo.getSigninId(), centerId, staffSignInfoVo.getAccountId(), staffSignInfoVo.getInTime(), SigninType.IN.getValue(), currentUser,
				isCasual);
		// 新增或编辑签出信息
		initOrUpdateStaffSign(staffSignInfoVo.getSignoutId(), centerId, staffSignInfoVo.getAccountId(), staffSignInfoVo.getOutTime(), SigninType.OUT.getValue(),
				currentUser, isCasual);

		return successResult();
	}

	/**
	 * @description 初始化或更新签入或签出信息
	 * @author hxzhang
	 * @create 2016年9月20日下午3:40:44
	 * @version 1.0
	 * @param id
	 * @param centreId
	 * @param accountId
	 * @param signDate
	 * @param type
	 * @param currentUser
	 */
	private void initOrUpdateStaffSign(String id, String centreId, String accountId, Date signDate, short type, UserInfoVo currentUser, boolean isCasual) {
		// add bu big
		if (signDate == null) {
			if (StringUtil.isNotEmpty(id)) {
				SigninInfo info = signinInfoMapper.selectByPrimaryKey(id);
				if (info != null) {
					info.setDeleteFlag(DeleteFlag.Delete.getValue());
					signinInfoMapper.updateByPrimaryKey(info);
				}
			}
			return;
		}

		// 新增
		if (StringUtil.isEmpty(id)) {
			if (!isCasual) {
				// 删除补签到信息
				signinInfoMapper.deleteNoSign(accountId, signDate);
			} else {
				signinInfoMapper.deleteNoSignOfCentre(accountId, signDate, centreId);
			}
			SigninInfo sign = new SigninInfo();
			sign.setId(UUID.randomUUID().toString());
			sign.setCentreId(centreId);
			sign.setAccountId(accountId);
			sign.setSignDate(signDate);
			sign.setType(type);
			sign.setState(SigninState.Signin.getValue());
			sign.setCreateAccountId(currentUser.getAccountInfo().getId());
			sign.setCreateTime(new Date());
			sign.setDeleteFlag(DeleteFlag.Default.getValue());
			signinInfoMapper.insert(sign);
		} else {// 编辑
			SigninInfo dbSign = signinInfoMapper.selectByPrimaryKey(id);
			dbSign.setCentreId(centreId);
			dbSign.setSignDate(signDate);
			dbSign.setUpdateAccountId(currentUser.getAccountInfo().getId());
			dbSign.setUpdateTime(new Date());
			signinInfoMapper.updateByPrimaryKey(dbSign);
		}

	}

	@Override
	public ServiceResult<Object> getStaffExport(SignCondition condition) {
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		List<StaffSignInfoVo> staffSignList = signinInfoMapper.getStaffExport(condition);
		for (StaffSignInfoVo ssv : staffSignList) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("firstName", ssv.getFirstName());
			map.put("middleName", ssv.getMiddleName());
			map.put("lastName", ssv.getLastName());
			map.put("centre", ssv.getCentreName());
			map.put("date", null != ssv.getInTime() ? DateUtil.getDate(ssv.getInTime(), "dd/MM/yyyy") : "");
			map.put("signinTime", null != ssv.getInTime() ? DateUtil.formatDate(ssv.getInTime(), "hh:mm a", Locale.ENGLISH) : "");
			map.put("signoutTime", null != ssv.getOutTime() ? DateUtil.formatDate(ssv.getOutTime(), "hh:mm a", Locale.ENGLISH) : "");
			map.put("RosteredSignIn", null != ssv.getStartTime() ? DateUtil.formatDate(ssv.getStartTime(), "hh:mm a", Locale.ENGLISH) : "");
			map.put("RosteredSignout", null != ssv.getEndTime() ? DateUtil.formatDate(ssv.getEndTime(), "hh:mm a", Locale.ENGLISH) : "");
			map.put("TotalRosteredHours", getHours(ssv.getStartTime(), ssv.getEndTime()));
			map.put("type", null == ssv.getType() ? "Work" : LeaveType.getDescByValue(ssv.getType()));
			resultList.add(map);
		}
		return successResult((Object) resultList);
	}

	/**
	 * @description 计算两个时间之间的小时数
	 * @author hxzhang
	 * @create 2017年4月14日上午9:36:03
	 */
	private String getHours(Date startDate, Date endDate) {
		if (null == startDate && null == endDate) {
			return "";
		}
		final int denominator = 1000 * 60 * 60;
		DecimalFormat df = new DecimalFormat("######0.00");
		String hours = df.format((double) (endDate.getTime() - startDate.getTime()) / denominator);
		return hours;
	}

	@Override
	public ServiceResult<Object> removeNotHaveSignout(String accountId) {
		accountInfoMapper.removeNotHaveSignOut(accountId);
		return successResult();
	}

	@Override
	public void dealSyncChildAttendance(String hubworksId, Date date, String signinTime, String signoutTime) {
		UserInfo userInfo = userInfoMapper.getChildByHubworksId(hubworksId);
		if (userInfo == null || StringUtil.isEmpty(signinTime)) {
			return;
		}
		// 手动创建事物,并提交.
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);// 事物隔离级别，开启新事务
		TransactionStatus status = txManager.getTransaction(def); // 获得事务状态
		SignChildInfoWithBLOBs dbBloBs = signChildInfoMapper.getSignChildInfoWithBLOBsByAccountId(userInfo.getAccountId(), date);
		if (dbBloBs == null) {
			// 初始化主表信息
			SignChildInfoWithBLOBs bloBs = new SignChildInfoWithBLOBs();
			String id = UUID.randomUUID().toString();
			bloBs.setId(id);
			bloBs.setCreateTime(date);
			bloBs.setCreateAccountId("Job");
			bloBs.setUpdateTime(date);
			bloBs.setUpdateAccountId("Job");
			bloBs.setDeleteFlag(DeleteFlag.Default.getValue());
			signChildInfoMapper.insert(bloBs);
			// 初始化子表签入信息
			addOrUpdateSignByJob(userInfo, id, date, signinTime, SigninType.IN);
			// 初始化子表签出信息
			if (StringUtil.isNotEmpty(signoutTime)) {
				addOrUpdateSignByJob(userInfo, id, date, signoutTime, SigninType.OUT);
			}
		} else {
			dbBloBs.setUpdateTime(date);
			dbBloBs.setUpdateAccountId("Job");
			signChildInfoMapper.updateByPrimaryKey(dbBloBs);
			// 更新子表签入信息
			addOrUpdateSignByJob(userInfo, dbBloBs.getId(), date, signinTime, SigninType.IN);
			// 更新子表签出信息
			if (StringUtil.isNotEmpty(signoutTime)) {
				addOrUpdateSignByJob(userInfo, dbBloBs.getId(), date, signoutTime, SigninType.OUT);
			}
		}
		txManager.commit(status);// 提交事务
	}

	private void addOrUpdateSignByJob(UserInfo userInfo, String bloBsId, Date date, String time, SigninType type) {
		SigninInfo dbSigninInfo = signinInfoMapper.getSignInfoBySignChildId(bloBsId, type.getValue());
		Date signDate = DateUtil.parseString(DateUtil.format(date, DateUtil.yyyyMMddSpt) + " " + time, DateUtil.yyyyMMddHHmmSpt);
		InterimAttendance interim = interimAttendanceMapper.getInterimAttend(userInfo.getAccountId(), date);
		if (dbSigninInfo == null) {
			SigninInfo signinInfo = new SigninInfo();
			String id = UUID.randomUUID().toString();
			signinInfo.setId(id);
			signinInfo.setAccountId(userInfo.getAccountId());
			signinInfo.setSignDate(signDate);
			signinInfo.setType(type.getValue());
			if (interim != null) {
				signinInfo.setCentreId(interim.getCenterId());
				signinInfo.setRoomId(interim.getRoomId());
			} else {
				signinInfo.setCentreId(userInfo.getCentersId());
				signinInfo.setRoomId(userInfo.getRoomId());
			}
			signinInfo.setState(SigninState.Signin.getValue());
			signinInfo.setCreateTime(date);
			signinInfo.setCreateAccountId("Job");
			signinInfo.setUpdateTime(date);
			signinInfo.setUpdateAccountId("Job");
			signinInfo.setDeleteFlag(DeleteFlag.Default.getValue());
			signinInfo.setDay(date);
			signinInfoMapper.insert(signinInfo);
			initSignSelationInfo(id, bloBsId);
		} else {
			signinInfoMapper.updateSignDateById(dbSigninInfo.getId(), signDate);
		}
	}
}
