package com.aoyuntek.aoyun.service.meeting.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.condtion.MeetingCondition;
import com.aoyuntek.aoyun.constants.DateFormatterConstants;
import com.aoyuntek.aoyun.constants.SystemConstants;
import com.aoyuntek.aoyun.dao.NqsRelationInfoMapper;
import com.aoyuntek.aoyun.dao.meeting.MeetingColumnsInfoMapper;
import com.aoyuntek.aoyun.dao.meeting.MeetingInfoMapper;
import com.aoyuntek.aoyun.dao.meeting.MeetingTemplateInfoMapper;
import com.aoyuntek.aoyun.dao.meeting.RelationMeetingInfoMapper;
import com.aoyuntek.aoyun.dao.meeting.TemplateFrequencyInfoMapper;
import com.aoyuntek.aoyun.entity.po.NqsRelationInfo;
import com.aoyuntek.aoyun.entity.po.meeting.MeetingColumnsInfo;
import com.aoyuntek.aoyun.entity.po.meeting.MeetingInfo;
import com.aoyuntek.aoyun.entity.po.meeting.MeetingTemplateInfo;
import com.aoyuntek.aoyun.entity.po.meeting.TemplateFrequencyInfo;
import com.aoyuntek.aoyun.entity.vo.NqsVo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.meeting.MeetingTempVo;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.TaskFrequenceRepeatType;
import com.aoyuntek.aoyun.enums.meeting.CurrtenFlag;
import com.aoyuntek.aoyun.enums.meeting.MeetingTempState;
import com.aoyuntek.aoyun.service.impl.BaseWebServiceImpl;
import com.aoyuntek.aoyun.service.meeting.IMeetingTempService;
import com.aoyuntek.framework.model.Pager;
import com.theone.common.util.ServiceResult;
import com.theone.date.util.DateUtil;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

/**
 * @description 会议模版业务逻辑层实现
 * @author Hxzhang 2016年10月13日上午10:40:36
 */
@Service
public class MeetingTempService extends BaseWebServiceImpl<MeetingTemplateInfo, MeetingTemplateInfoMapper> implements IMeetingTempService {
    /**
     * 日志
     */
    public static Logger logger = Logger.getLogger("MeetingTempService");
    @Autowired
    private MeetingTemplateInfoMapper meetingTemplateInfoMapper;
    @Autowired
    private NqsRelationInfoMapper nqsRelationInfoMapper;
    @Autowired
    private TemplateFrequencyInfoMapper templateFrequencyInfoMapper;
    @Autowired
    private RelationMeetingInfoMapper relationMeetingInfoMapper;
    @Autowired
    private MeetingColumnsInfoMapper meetingColumnsInfoMapper;
    @Autowired
    private MeetingInfoMapper meetingInfoMapper;

    @Override
    public ServiceResult<Object> getMeetingTempList(MeetingCondition condition) {
        // 设置起始页为当前页
        condition.setStartSize(condition.getPageIndex());
        List<MeetingTemplateInfo> mtList = meetingTemplateInfoMapper.getMeetingTempList(condition);
        int totalSize = meetingTemplateInfoMapper.getMeetingTempListCount(condition);
        condition.setTotalSize(totalSize);
        Pager<MeetingTemplateInfo, MeetingCondition> pager = new Pager<MeetingTemplateInfo, MeetingCondition>(mtList, condition);
        return successResult((Object) pager);
    }

    @Override
    public ServiceResult<Object> addOrUpdateMeetingTemp(MeetingTempVo meetingTempVo, UserInfoVo currentUser) {
        ServiceResult<Object> result = validateMeetingTemp(meetingTempVo);
        if (!result.isSuccess()) {
            return result;
        }
        // 判断该会议模版是否被使用,已使用复制之前模版并重新插入
        boolean isUse = isUse(meetingTempVo);
        // 新增或更新会议模版
        String id = initOrUpdateMeetingTemp(meetingTempVo, currentUser, isUse);
        // 保存NQS关系
        saveNqsRelation(id, meetingTempVo.getNqsList());
        // 新增或更新模版触发频率
        initOrUpdateFrequency(id, meetingTempVo.getFrequency(), currentUser, isUse);
        // 新增或编辑会议记录模版
        initOrUpdateColumn(id, meetingTempVo, currentUser, isUse);
        // 更新后先删除该模版之前在未来时间生成的会议
        meetingInfoMapper.removeMeetingByTempId(meetingTempVo.getMeetingTemplateInfo().getSimilarId(), null);
        // 根据频率生成记录
        saveMeetingTempForMeeting(new Date(), meetingTempVo, currentUser, true);
        return successResult(getTipMsg("meeting_save"), (Object) meetingTempVo);
    }

    /**
     * @description 验证
     * @author hxzhang
     * @create 2016年10月23日下午2:58:00
     */
    private ServiceResult<Object> validateMeetingTemp(MeetingTempVo meetingTempVo) {
        List<MeetingColumnsInfo> colList = meetingTempVo.getColumnList();
        // 1.新增列名唯一性验证
        for (int i = 0; i < colList.size(); i++) {
            for (int j = i + 1; j < colList.size(); j++) {
                if (colList.get(i).getColumnName().equals(colList.get(j).getColumnName())) {
                    return failResult(getTipMsg("meeting_save_column"));
                }
            }
        }
        return successResult();
    }

    /**
     * @description 新增或编辑会议记录模版
     * @author hxzhang
     * @create 2016年10月14日下午3:36:49
     */
    private void initOrUpdateColumn(String tempId, MeetingTempVo meetingTempVo, UserInfoVo currentUser, boolean isUse) {
        List<MeetingColumnsInfo> colList = meetingTempVo.getColumnList();
        Date now = new Date();
        List<MeetingColumnsInfo> newColList = new ArrayList<MeetingColumnsInfo>();
        for (MeetingColumnsInfo mc : colList) { // ADD
            if (StringUtil.isEmpty(mc.getId()) || isUse) {
                mc.setId(UUID.randomUUID().toString());
                mc.setTemplateId(tempId);
                mc.setCreateAccountId(currentUser.getAccountInfo().getId());
                mc.setCreateTime(now);
                mc.setUpdateTime(now);
                mc.setDeleteFlag(DeleteFlag.Default.getValue());
                now = new Date(now.getTime() + 1000);
                newColList.add(mc);
            } else { // UPDATE
                mc.setUpdateTime(new Date());
                mc.setUpdateAccountId(currentUser.getAccountInfo().getId());
                meetingColumnsInfoMapper.updateByPrimaryKeySelective(mc);
            }
        }
        // 插入新增数据
        if (ListUtil.isNotEmpty(newColList)) {
            meetingColumnsInfoMapper.insertBatchMeetingColumnsInfo(newColList);
        }
        // 删除数据库中多余数据
        if (ListUtil.isNotEmpty(colList)) {
            meetingColumnsInfoMapper.removeMeetingColumnsInfo(tempId, colList);
        }
    }

    /**
     * @description
     * @author hxzhang
     * @create 2016年10月13日下午4:46:52
     */
    private boolean isUse(MeetingTempVo meetingTempVo) {
        if (StringUtil.isNotEmpty(meetingTempVo.getMeetingTemplateInfo().getId())) {
            int count = meetingInfoMapper.getMeetingCountByTempId(meetingTempVo.getMeetingTemplateInfo().getId());
            if (count != 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * @description 初始化或更新会议模版
     * @author hxzhang
     * @create 2016年10月13日下午2:02:53
     */
    private String initOrUpdateMeetingTemp(MeetingTempVo meetingTempVo, UserInfoVo currentUser, boolean isUse) {
        MeetingTemplateInfo meetingTemplateInfo = meetingTempVo.getMeetingTemplateInfo();
        String id;
        if (StringUtil.isEmpty(meetingTemplateInfo.getId())) {
            // ADD
            id = UUID.randomUUID().toString();
            meetingTemplateInfo.setId(id);
            meetingTemplateInfo.setTemplateCode(UUID.randomUUID().toString());
            meetingTemplateInfo.setState(MeetingTempState.Enable.getValuse());
            meetingTemplateInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
            Date now = new Date();
            meetingTemplateInfo.setCreateTime(now);
            meetingTemplateInfo.setUpdateTime(now);
            meetingTemplateInfo.setDeleteFlag(DeleteFlag.Default.getValue());
            meetingTemplateInfo.setCurrtenFlag(CurrtenFlag.NewVersion.getValue());
            meetingTemplateInfo.setSimilarId(UUID.randomUUID().toString());
            meetingTemplateInfoMapper.insertSelective(meetingTemplateInfo);
        } else {
            // UPDATE
            MeetingTemplateInfo dbData = meetingTemplateInfoMapper.selectByPrimaryKey(meetingTemplateInfo.getId());
            id = dbData.getId();
            dbData.setTemplateName(meetingTemplateInfo.getTemplateName());
            dbData.setFormCode(meetingTemplateInfo.getFormCode());
            dbData.setUpdateAccountId(currentUser.getAccountInfo().getId());
            dbData.setUpdateTime(new Date());
            if (isUse) { // 被使用复制模版并重新插入
                // 将之前模版更新为旧模版
                dbData.setCurrtenFlag(CurrtenFlag.OldVersion.getValue());
                meetingTemplateInfoMapper.updateByPrimaryKeySelective(dbData);
                // 插入新模版
                id = UUID.randomUUID().toString();
                dbData.setId(id);
                dbData.setCurrtenFlag(CurrtenFlag.NewVersion.getValue());
                meetingTemplateInfoMapper.insertSelective(dbData);
            } else { // 未被使用则更新模版数据
                meetingTemplateInfoMapper.updateByPrimaryKey(dbData);
            }
        }

        return id;
    }

    /**
     * @description 保存NQS信息
     * @author hxzhang
     * @create 2016年10月13日下午2:22:31
     */
    private void saveNqsRelation(String objId, List<NqsVo> nqsList) {
        // 更新前先删除之前NQS信息
        nqsRelationInfoMapper.removeNqsRelationByObjId(objId);
        if (ListUtil.isEmpty(nqsList)) {
            return;
        }
        // 保存新NQS信息
        List<NqsRelationInfo> nrList = new ArrayList<NqsRelationInfo>();
        for (NqsVo n : nqsList) {
            NqsRelationInfo nr = new NqsRelationInfo();
            nr.setId(UUID.randomUUID().toString());
            nr.setObjId(objId);
            nr.setNqsVersion(n.getVersion());
            nr.setDeleteFlag(DeleteFlag.Default.getValue());
            nr.setTag(n.getTag());
            nrList.add(nr);
        }
        nqsRelationInfoMapper.insterBatchNqsRelationInfo(nrList);
    }

    /**
     * @description 新增或更新模版触发频率信息
     * @author hxzhang
     * @create 2016年10月13日下午2:41:20
     */
    private void initOrUpdateFrequency(String templateId, TemplateFrequencyInfo templateFrequencyInfo, UserInfoVo currentUser, boolean isUse) {
        if (StringUtil.isEmpty(templateFrequencyInfo.getId())) {
            // ADD
            templateFrequencyInfo.setId(UUID.randomUUID().toString());
            templateFrequencyInfo.setTemplateId(templateId);
            templateFrequencyInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
            Date now = new Date();
            templateFrequencyInfo.setCreateTime(now);
            templateFrequencyInfo.setUpdateTime(now);
            templateFrequencyInfo.setDeleteFlag(DeleteFlag.Default.getValue());
            templateFrequencyInfoMapper.insertSelective(templateFrequencyInfo);
        } else {
            // UPDATE
            TemplateFrequencyInfo dbData = templateFrequencyInfoMapper.selectByPrimaryKey(templateFrequencyInfo.getId());
            dbData.setRepeats(templateFrequencyInfo.getRepeats());
            dbData.setStartsOnDate(templateFrequencyInfo.getStartsOnDate());
            dbData.setEndOnDate(templateFrequencyInfo.getEndOnDate());
            dbData.setWeekdayStart(templateFrequencyInfo.getWeekdayStart());
            dbData.setWeekdayDue(templateFrequencyInfo.getWeekdayDue());
            dbData.setUpdateAccountId(currentUser.getAccountInfo().getId());
            dbData.setUpdateTime(new Date());
            dbData.setHaveEndDate(templateFrequencyInfo.getHaveEndDate());
            if (isUse) { // 被使用复制模版并重新插入
                dbData.setId(UUID.randomUUID().toString());
                dbData.setTemplateId(templateId);
                dbData.setCreateAccountId(currentUser.getAccountInfo().getId());
                Date now = new Date();
                dbData.setCreateTime(now);
                dbData.setUpdateTime(now);
                templateFrequencyInfoMapper.insertSelective(dbData);
            } else {// 未被使用则更新模版数据
                templateFrequencyInfoMapper.updateByPrimaryKey(dbData);
            }
        }
    }

    @Override
    public ServiceResult<Object> getMeetingTempInfo(String id) {
        MeetingTempVo mtVo = new MeetingTempVo();
        if (StringUtil.isEmpty(id)) {
            MeetingTemplateInfo mt = new MeetingTemplateInfo();
            List<NqsVo> nqsList = new ArrayList<NqsVo>();
            TemplateFrequencyInfo tf = new TemplateFrequencyInfo();
            mtVo.setMeetingTemplateInfo(mt);
            mtVo.setNqsList(nqsList);
            mtVo.setFrequency(tf);
        } else {
            mtVo = meetingTemplateInfoMapper.getMeetingTempInfo(id);
        }
        return successResult((Object) mtVo);
    }

    @Override
    public boolean validateMeetingTempName(String name, String id) {
        int count = meetingTemplateInfoMapper.validateMeetingTempName(name, id);
        return count > 0 ? false : true;
    }

    @Override
    public ServiceResult<Object> saveOperaMeetingTemp(String id, short state, UserInfoVo currentUser) {
        MeetingTemplateInfo dbData = meetingTemplateInfoMapper.selectByPrimaryKey(id);
        dbData.setState(state);
        dbData.setUpdateAccountId(currentUser.getAccountInfo().getId());
        dbData.setUpdateTime(new Date());
        meetingTemplateInfoMapper.updateByPrimaryKey(dbData);
        return successResult(getTipMsg("operation_success"));
    }

    /**
     * @description
     * @author hxzhang
     * @create 2016年10月17日下午3:50:28
     */
    private ServiceResult<Object> saveMeetingTempForMeeting(Date day, MeetingTempVo meetingTempVo, UserInfoVo currentUser, boolean unDelete) {
        TemplateFrequencyInfo frequencyInfo = meetingTempVo.getFrequency();
        MeetingTemplateInfo meetingTemplateInfo = meetingTempVo.getMeetingTemplateInfo();

        if (meetingTemplateInfo.getDeleteFlag() == DeleteFlag.Delete.getValue()) {
            return successResult();
        }

        if (frequencyInfo == null) {
            return successResult();
        }

        String createAccountId = currentUser == null ? SystemConstants.JobCreateAccountId : currentUser.getAccountInfo().getId();
        boolean needInit = false;
        Short shortVal = frequencyInfo.getRepeats();
        TaskFrequenceRepeatType frequenceRepeatType = TaskFrequenceRepeatType.getType(shortVal);

        boolean inTimeScope = false;
        if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(frequencyInfo.getStartsOnDate(), day) >= 0) {
            if (frequencyInfo.getEndOnDate() == null || com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(frequencyInfo.getEndOnDate(), day) <= 0) {
                inTimeScope = true;
            }
        }

        switch (frequenceRepeatType) {
        case Weekly: {
            if (inTimeScope) {
                // 如果开始weekday to stat 是今天 那么要生成
                if (Integer.valueOf(frequencyInfo.getWeekdayStart()) == DateUtil.getDayOfWeek(day) - 1) {
                    // if (Integer.valueOf(frequencyInfo.getWeekdayDue()) >= DateUtil.getDayOfWeek(day) - 1) {
                    needInit = true;
                    // }
                }
            }
        }
            break;
        case Monthly: {
            if (inTimeScope) {
                // 如果开始weekday to stat 是今天 那么要生成
                if ((Integer.valueOf(frequencyInfo.getWeekdayStart()) == DateUtil.getDayOfMonth(day))
                // && (Integer.valueOf(frequencyInfo.getWeekdayDue()) >= DateUtil.getDayOfMonth(day))
                ) {
                    needInit = true;
                }
            }
        }
            break;
        case Yearly: {
            if (inTimeScope) {
                int year = DateUtil.getCalendar(day).get(Calendar.YEAR);
                if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(DateUtil.parse(frequencyInfo.getWeekdayStart() + "/" + year, DateFormatterConstants.SYS_FORMAT2), day) == 0) {
                    // Date dueDateOfYear = DateUtil.parse(frequencyInfo.getWeekdayDue() + "/" + year, DateFormatterConstants.SYS_FORMAT2);
                    // if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(dueDateOfYear, day) <= 0) {
                    needInit = true;
                    // }
                }
            }
        }
            break;
        case WhenRequired:
            needInit = false;
            break;
        default:
            break;
        }

        if (!needInit) {
            return successResult();
        }

        if (unDelete) {
            // 判断是否生成存在记录
            int count = meetingInfoMapper.getMeetingCountByTempIdAndDate(meetingTemplateInfo.getSimilarId(), day);
            if (count == 0) {
                initMeeting(meetingTemplateInfo, day, createAccountId);
            }
        } else {
            // 先删除之前生成的会议
            meetingInfoMapper.removeMeetingByTempId(meetingTemplateInfo.getSimilarId(), day);
            // 模版是Enable的才创建
            if (meetingTemplateInfo.getState() == MeetingTempState.Enable.getValuse()) {
                initMeeting(meetingTemplateInfo, day, createAccountId);
            }
        }
        return successResult();
    }

    /**
     * @description 初始化meeting,并生成记录.
     * @author hxzhang
     * @create 2016年10月24日下午7:47:23
     */
    private void initMeeting(MeetingTemplateInfo meetingTemplateInfo, Date day, String createAccountId) {
        MeetingInfo meetingInfo = new MeetingInfo();
        meetingInfo.setId(UUID.randomUUID().toString());
        meetingInfo.setMeetingName(meetingTemplateInfo.getTemplateName());
        meetingInfo.setTemplateId(meetingTemplateInfo.getId());
        meetingInfo.setState(meetingTemplateInfo.getState());
        meetingInfo.setDay(day);
        meetingInfo.setCreateAccountId(createAccountId);
        meetingInfo.setCreateTime(day);
        meetingInfo.setUpdateTime(day);
        meetingInfo.setDeleteFlag(DeleteFlag.Default.getValue());
        meetingInfoMapper.insert(meetingInfo);
    }

    @Override
    public void createMeetingTempForMeetingTimeTask(Date now) {
        logger.info("createMeetingTempForMeetingTimeTask===>>>start");
        // 获取所有Meeting模版
        List<MeetingTempVo> meetingTempVoList = meetingTemplateInfoMapper.getAllMeetingTemp();
        for (MeetingTempVo meetingTempVo : meetingTempVoList) {
            saveMeetingTempForMeeting(now, meetingTempVo, null, false);
        }
        logger.info("createMeetingTempForMeetingTimeTask===>>>end");
    }

    @Override
    public List<SelecterPo> getMeetingTempForSelecterPo() {
        return meetingTemplateInfoMapper.getMeetingTempForSelecterPo();
    }
}
