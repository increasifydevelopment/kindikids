package com.aoyuntek.aoyun.service;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.aoyuntek.aoyun.condtion.StaffCondition;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.entity.po.StaffEmploymentInfo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.aoyun.entity.vo.StaffInfoVo;
import com.aoyuntek.aoyun.entity.vo.StaffListVo;
import com.aoyuntek.aoyun.entity.vo.StaffRoleInfoVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.framework.model.Pager;
import com.theone.common.util.ServiceResult;

/**
 * 
 * @author rick
 * 
 */
public interface IStaffService extends IBaseWebService<StaffListVo, UserInfoMapper> {

	void dealStaffTags();

	void dealAchiveStaff(Date now);

	void dealEmployment(Date now);

	ServiceResult<Object> updateEmploymentRequest(String id, UserInfoVo currentUser);

	ServiceResult<Object> saveEmploymentRequest(StaffEmploymentInfo staffEmploymentInfo, UserInfoVo currentUser, HttpServletRequest request);

	/**
	 * 
	 * @author rick
	 * @date 2016年7月27日 上午10:36:01
	 * @param condition
	 * @return
	 */
	ServiceResult<Pager<StaffListVo, StaffCondition>> getPagerStaffs(StaffCondition condition);

	/**
	 * 
	 * @author rick
	 * @date 2016年7月27日 上午10:36:03
	 * @param userId
	 * @return
	 */
	ServiceResult<StaffInfoVo> saveGetStaffInfo(String userId);

	/**
	 * 
	 * @author rick
	 * @date 2016年7月27日 上午10:36:05
	 * @param staffInfoVo
	 * @return
	 */
	ServiceResult<Object> updateStaffInfo(StaffInfoVo staffInfoVo, UserInfoVo currentUser, String ip) throws Exception;

	/**
	 * 
	 * @author rick
	 * @date 2016年7月28日 上午11:08:44
	 * @return
	 */
	ServiceResult<StaffRoleInfoVo> getRoleList();

	/**
	 * 
	 * @author rick
	 * @date 2016年7月28日 上午11:22:01
	 * @param userId
	 * @param currentUser
	 * @return
	 */
	ServiceResult<Object> deleteStaff(String userId, UserInfoVo currentUser);

	/**
	 * 
	 * @author rick
	 * @date 2016年8月2日 上午8:58:58
	 * @param userId
	 * @param currentUser
	 * @return
	 */
	ServiceResult<Object> updateAchive(String userId, int type, UserInfoVo currentUser);

	/**
	 * 
	 * @author rick
	 * @date 2016年8月3日 上午11:47:37
	 * @param userId
	 * @return
	 */
	ServiceResult<Object> insertWelEmail(String userId, String accountId, boolean haveCancel);

	/**
	 * 
	 * @description 更新staff的center、room、group信息
	 * @author mingwang
	 * @create 2016年8月28日下午5:14:48
	 * @version 1.0
	 * @param userId
	 * @param centerId
	 * @param roomId
	 * @param groupId
	 * @return
	 */
	ServiceResult<Object> updateStaff(String userId, String centerId, String roomId, String groupId);

	/**
	 * @description
	 * @author hxzhang
	 * @create 2016年9月2日上午11:43:03
	 * @version 1.0
	 * @param now
	 * @return
	 */
	int sendActiveEmailTimedTask(Date now);

	/**
	 * 
	 * @description 获取下拉框员工
	 * @author gfwang
	 * @create 2016年9月28日下午4:40:18
	 * @version 1.0
	 * @param condition
	 * @return
	 */
	List<SelecterPo> getSelecterStaff(StaffCondition condition);

	/**
	 * 
	 * @description 获取下拉框兼职员工
	 * @author mingwang
	 * @create 2016年12月30日下午6:00:33
	 * @version 1.0
	 * @return
	 */

	List<SelecterPo> getCasualStaff(String likeName);

	/**
	 * 处理员工Card ID
	 * 
	 * @author hxzhang 2019年3月14日
	 * @return
	 */
	void dealStaffCardId();

}
