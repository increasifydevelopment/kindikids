package com.aoyuntek.aoyun.service.program;

import java.util.Date;

import com.aoyuntek.aoyun.dao.program.ProgramTaskExgrscInfoMapper;
import com.aoyuntek.aoyun.entity.po.program.ProgramTaskExgrscInfo;
import com.aoyuntek.aoyun.entity.vo.ProgramTaskExgrscInfoVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.service.IBaseWebService;
import com.theone.common.util.ServiceResult;

/**
 * @description Exgrsc业务层接口
 * @author Hxzhang 2016年9月20日下午5:48:29
 * @version 1.0
 */
public interface IProgramTaskExgrscService extends IBaseWebService<ProgramTaskExgrscInfo, ProgramTaskExgrscInfoMapper> {
    /**
     * @description 新增或编辑 Explore Curriculum,Grow Curriculum,School Readiness
     * @author hxzhang
     * @create 2016年9月20日下午7:15:16
     * @version 1.0
     */
    ServiceResult<Object> addOrUpdatePorgramTaskExgrsc(ProgramTaskExgrscInfoVo exgrscInfoVo, UserInfoVo currentUser, short programType);

    /**
     * @description 获取该room当天的Explore Curriculum,Grow Curriculum,School Readiness
     * @author hxzhang
     * @create 2016年9月21日上午11:43:13
     * @version 1.0
     * @param id
     * @param roomId
     * @return
     */
    ServiceResult<Object> getProgramTaskExgrsc(String id, String roomId, short exgrscType);

    /**
     * @description 定时任务创建Explore Curriculum,Grow Curriculum,School Readiness
     * @author hxzhang
     * @create 2016年9月25日下午4:17:00
     * @version 1.0
     * @param now
     */
    void createProgramTaskExgrscTimedTask(Date now);

    /**
     * @description 将过期的ProgramTaskExgrsc更新为overdue
     * @author hxzhang
     * @create 2016年9月25日下午7:24:24
     * @version 1.0
     * @param now
     */
    void updateOverDueProgramTaskExgrscTimedTask(Date now);
}
