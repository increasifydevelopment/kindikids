package com.aoyuntek.aoyun.service;

import java.util.List;

import com.aoyuntek.aoyun.dao.MsgEmailInfoMapper;
import com.aoyuntek.aoyun.entity.po.MsgEmailInfo;
import com.aoyuntek.aoyun.entity.vo.EmailVo;

/**
 * 
 * @description 邮件处理业务
 * @author gfwang
 * @create 2016年10月9日上午10:52:45
 * @version 1.0
 */
public interface IMsgEmailInfoService extends IBaseWebService<MsgEmailInfo, MsgEmailInfoMapper> {

    void sendMsg();

    /**
     * @description 批量发送邮件
     * @author hxzhang
     * @create 2016年9月2日下午4:30:49
     * @version 1.0
     * @param userList
     */
    void batchSendEamil(final List<EmailVo> emailVoList);

    void batchSendEamil(final List<EmailVo> emailVoList, final String[] bccs);

}
