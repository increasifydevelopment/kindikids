package com.aoyuntek.aoyun.service.roster;

import com.aoyuntek.aoyun.dao.roster.RosterShiftInfoMapper;
import com.aoyuntek.aoyun.entity.po.roster.RosterShiftInfo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.roster.RosterShiftVo;
import com.aoyuntek.aoyun.service.IBaseWebService;
import com.theone.common.util.ServiceResult;

public interface IRosterShiftService extends IBaseWebService<RosterShiftInfo, RosterShiftInfoMapper> {

    ServiceResult<RosterShiftVo> getShiftVo(String centreId);

    ServiceResult<RosterShiftVo> saveOrUpdateShift(RosterShiftVo vo, UserInfoVo currentUser);

    ServiceResult<Object> getShiftTasks(String centreId, UserInfoVo currentUser);

    ServiceResult<RosterShiftVo> deleteShiftTime(String id, UserInfoVo currentUser);
}
