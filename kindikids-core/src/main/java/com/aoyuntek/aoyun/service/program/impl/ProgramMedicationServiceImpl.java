package com.aoyuntek.aoyun.service.program.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.dao.RoomInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.dao.program.ProgramMedicationInfoMapper;
import com.aoyuntek.aoyun.dao.program.TimeMedicationInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskInstanceInfoMapper;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramMedicationInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramMedicationInfoWithBLOBs;
import com.aoyuntek.aoyun.entity.po.program.TimeMedicationInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskInstanceInfo;
import com.aoyuntek.aoyun.entity.vo.ProgramMedicationInfoVo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.ProgramState;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.enums.TaskType;
import com.aoyuntek.aoyun.service.impl.BaseWebServiceImpl;
import com.aoyuntek.aoyun.service.program.IProgramMedicationService;
import com.aoyuntek.aoyun.service.task.ITaskInstance;
import com.theone.common.util.ServiceResult;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

/**
 * 
 * @description programMedication 业务逻辑实现类
 * @author mingwang
 * @create 2016年9月26日下午5:04:56
 * @version 1.0
 */
@Service
public class ProgramMedicationServiceImpl extends BaseWebServiceImpl<ProgramMedicationInfo, ProgramMedicationInfoMapper> implements
        IProgramMedicationService {

    @Autowired
    private ProgramMedicationInfoMapper medicationInfoMapper;
    @Autowired
    private TimeMedicationInfoMapper timeMedicationInfoMapper;
    @Autowired
    private UserInfoMapper userInfoMapper;
    @Autowired
    private ITaskInstance taskInstance;
    @Autowired
    private RoomInfoMapper roomInfoMapper;
    @Autowired
    private TaskInstanceInfoMapper taskInstanceInfoMapper;

    /**
     * 新增/更新MedicationInfo
     */
    @Override
    public ServiceResult<Object> addOrUpdateMedication(ProgramMedicationInfoVo medicationInfoVo, UserInfoVo currentUser, Date date) {
        ProgramMedicationInfoWithBLOBs medicationInfoWithBLOBs = medicationInfoVo.getProgramMedicationInfoWithBLOBs();
        // 小孩必填
        if (StringUtil.isEmpty(medicationInfoWithBLOBs.getChildAccountId())) {
            return failResult(getTipMsg("program.share.child.empty"));
        }
        String medicationId = medicationInfoWithBLOBs.getId();
        // 返回值
        ProgramMedicationInfoVo returnMedicationInfoVo = new ProgramMedicationInfoVo();
        if (StringUtil.isEmpty(medicationId)) {
            // 新增
            returnMedicationInfoVo = saveMedication(medicationInfoVo, currentUser, date);
        } else {
            // 更新
            returnMedicationInfoVo = updateMedication(medicationInfoVo, currentUser);
        }
        return successResult(getTipMsg("operation_success"), (Object) returnMedicationInfoVo);
    }

    /**
     * 
     * @description 更新medicationInfo
     * @author mingwang
     * @create 2016年9月29日下午8:15:03
     * @version 1.0
     * @param medicationInfoVo
     * @param currentUser
     * @return
     */
    private ProgramMedicationInfoVo updateMedication(ProgramMedicationInfoVo medicationInfoVo, UserInfoVo currentUser) {
        // 返回对象
        ProgramMedicationInfoVo returnMedicationInfoVo = new ProgramMedicationInfoVo();
        // 待更新medicationInfo
        ProgramMedicationInfoWithBLOBs medicationInfoWithBLOBs = medicationInfoVo.getProgramMedicationInfoWithBLOBs();
        // 待更新time
        List<TimeMedicationInfo> timeMedicationInfos = medicationInfoVo.getTimeMedicationInfos();
        // medicationInfo Id
        String medicationId = medicationInfoWithBLOBs.getId();
        // timeMedicationId
        String timeMedicationId = medicationInfoWithBLOBs.getTimeMedicationId();
        // 增加的time集合
        List<TimeMedicationInfo> timeMedicationInfo_add = new ArrayList<TimeMedicationInfo>();
        // 删除的time集合
        List<String> timeMedicationInfo_remove = new ArrayList<String>();
        // 更新的time集合
        List<TimeMedicationInfo> timeMedicationInfo_update = new ArrayList<TimeMedicationInfo>();
        // 数据库medicationInfo
        ProgramMedicationInfoWithBLOBs dbMedicationInfoWithBLOBs = medicationInfoMapper.selectByPrimaryKey(medicationId);
        // 数据库中的timeMedicationId
        String dbTimeMedicationId = dbMedicationInfoWithBLOBs.getTimeMedicationId();
        // 如果时间集合为空，删除数据库中对应的时间记录
        if (ListUtil.isEmpty(timeMedicationInfos)) {
            if (!StringUtil.isEmpty(dbTimeMedicationId)) {
                // 删除timeMedication
                timeMedicationInfoMapper.updateDeleteByMedicationId(dbTimeMedicationId);
            }
        } else {
            // 如果数据库中的TimeMedicationId为空，新增
            if (StringUtil.isEmpty(dbTimeMedicationId)) {
                timeMedicationId = UUID.randomUUID().toString();
                medicationInfoMapper.updateMedicationIdById(dbMedicationInfoWithBLOBs.getId(), timeMedicationId);
                saveTimeMedicationInfos(timeMedicationId, timeMedicationInfos, currentUser);
            } else {
                List<TimeMedicationInfo> dbTimeMedicationInfos = timeMedicationInfoMapper.selectByTimeMedicationId(dbTimeMedicationId);
                // 给需要删除的timeMedicationInfo赋值
                for (TimeMedicationInfo timeMedicationInfo : dbTimeMedicationInfos) {
                    timeMedicationInfo_remove.add(timeMedicationInfo.getId());
                }
                for (TimeMedicationInfo timeMedicationInfo : timeMedicationInfos) {
                    String timeId = timeMedicationInfo.getId();
                    // id为空，放入新增的集合中
                    if (StringUtil.isEmpty(timeId)) {
                        timeMedicationInfo_add.add(timeMedicationInfo);
                        continue;
                    }
                    // 传入的timeMedication和数据库中的对比，相同的就加入更新集合中，并将其从待删除的集合中移除
                    for (TimeMedicationInfo dbTimeMedicationInfo : dbTimeMedicationInfos) {
                        String dbTimeId = dbTimeMedicationInfo.getId();
                        if (StringUtil.compare(timeId, dbTimeId)) {
                            dbTimeMedicationInfo.setDay(timeMedicationInfo.getDay());
                            dbTimeMedicationInfo.setChooseFlag(timeMedicationInfo.getChooseFlag());
                            dbTimeMedicationInfo.setUpdateTime(new Date());
                            dbTimeMedicationInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
                            dbTimeMedicationInfo.setDeleteFlag(DeleteFlag.Default.getValue());
                            dbTimeMedicationInfo.setStaffAdministeringSignature(timeMedicationInfo.getAdministeringValue().getText());
                            dbTimeMedicationInfo.setStaffChecking(timeMedicationInfo.getCheckingValue().getText());
                            timeMedicationInfo_update.add(dbTimeMedicationInfo);
                            timeMedicationInfo_remove.remove(dbTimeId);
                        }
                    }
                }
            }
        }
        // 更新MedicationInfo
        ProgramMedicationInfoWithBLOBs updateMedication = updateMedication(timeMedicationId, dbMedicationInfoWithBLOBs, medicationInfoVo, currentUser);

        if (!ListUtil.isEmpty(timeMedicationInfo_add)) {
            // 保存时间集合
            saveTimeMedicationInfos(timeMedicationId, timeMedicationInfo_add, currentUser);
        }
        if (!ListUtil.isEmpty(timeMedicationInfo_remove)) {
            // 批量删除timeMedication（修改deleteFlag状态）
            int deleteTimeMedicationCount = timeMedicationInfoMapper.batchDeleteTimeMedication(timeMedicationInfo_remove);
            log.info("deleteTimeMedicationCount=" + deleteTimeMedicationCount);
        }
        if (!ListUtil.isEmpty(timeMedicationInfo_update)) {
            // 批量更新
            timeMedicationInfoMapper.batchUpdateMedications(timeMedicationInfo_update);
        }
        // 给返回值赋值
        returnMedicationInfoVo.setTimeMedicationInfos(timeMedicationInfos);
        returnMedicationInfoVo.setProgramMedicationInfoWithBLOBs(updateMedication);
        return returnMedicationInfoVo;
    }

    /**
     * 
     * @description 更新medicationInfo
     * @author mingwang
     * @create 2016年10月12日下午2:35:24
     * @version 1.0
     * @param dbMedicationInfoWithBLOBs
     */
    private ProgramMedicationInfoWithBLOBs updateMedication(String timeMedicationId, ProgramMedicationInfoWithBLOBs dbMedicationInfoWithBLOBs,
            ProgramMedicationInfoVo medicationInfoVo, UserInfoVo currentUser) {
        ProgramMedicationInfoWithBLOBs medicationInfoWithBLOBs = medicationInfoVo.getProgramMedicationInfoWithBLOBs();
        List<TimeMedicationInfo> timeMedicationInfos = medicationInfoVo.getTimeMedicationInfos();
        boolean statusFlag = true;
        // Time of
        // Medication中如果有时间没有勾选，statusFlag设置为FALSE；如果时间集合为空，statusFlag为TRUE，下面将设置为complete状态
        if (ListUtil.isNotEmpty(timeMedicationInfos)) {
            for (TimeMedicationInfo timeMedicationInfo : timeMedicationInfos) {
                if (timeMedicationInfo.getChooseFlag() == null) {
                    statusFlag = false;
                    break;
                }
                if (timeMedicationInfo.getChooseFlag() != null && !timeMedicationInfo.getChooseFlag()) {
                    statusFlag = false;
                    break;
                }
            }
        }
        // 更新信息
        dbMedicationInfoWithBLOBs.setTimeMedicationId(timeMedicationId);
        dbMedicationInfoWithBLOBs.setDay(medicationInfoWithBLOBs.getDay());
        dbMedicationInfoWithBLOBs.setChildAccountId(medicationInfoWithBLOBs.getChildAccountId());
        dbMedicationInfoWithBLOBs.setMedicationName(medicationInfoWithBLOBs.getMedicationName());
        dbMedicationInfoWithBLOBs.setExpiryDate(medicationInfoWithBLOBs.getExpiryDate());
        dbMedicationInfoWithBLOBs.setReasonMedication(medicationInfoWithBLOBs.getReasonMedication());
        dbMedicationInfoWithBLOBs.setDosageMedication(medicationInfoWithBLOBs.getDosageMedication());
        dbMedicationInfoWithBLOBs.setMedicationAdminitered(medicationInfoWithBLOBs.getMedicationAdminitered());
        dbMedicationInfoWithBLOBs.setDateMedicationAdminitered(medicationInfoWithBLOBs.getDateMedicationAdminitered());
        dbMedicationInfoWithBLOBs.setStaffReceivingAccountId(medicationInfoWithBLOBs.getStaffReceivingAccountId());
        dbMedicationInfoWithBLOBs.setMustAssignAccountId1(medicationInfoWithBLOBs.getMustAssignAccountId1());
        dbMedicationInfoWithBLOBs.setMustAssignAccountId2(medicationInfoWithBLOBs.getMustAssignAccountId2());
        dbMedicationInfoWithBLOBs.setUpdateAccountId(currentUser.getAccountInfo().getId());
        dbMedicationInfoWithBLOBs.setUpdateTime(new Date());
        dbMedicationInfoWithBLOBs.setParentSignatureText(medicationInfoWithBLOBs.getParentSignatureText());
        // statusFlag为FALSE时为pending状态，TRUE设置为complete状态
        if (statusFlag) {
            dbMedicationInfoWithBLOBs.setStatu(ProgramState.Complete.getValue());
        } else {
            dbMedicationInfoWithBLOBs.setStatu(ProgramState.Pending.getValue());
        }
        // 更新数据库记录
        medicationInfoMapper.updateByPrimaryKeyWithBLOBs(dbMedicationInfoWithBLOBs);
        // statusFlag为FALSE时更新taskInstance为pending状态，TRUE设置为complete状态
        if (statusFlag) {
            taskInstanceInfoMapper.updateStatus(dbMedicationInfoWithBLOBs.getId(), ProgramState.Complete.getValue(), currentUser.getAccountInfo()
                    .getId(), new Date());
        } else {
            taskInstanceInfoMapper.updateStatus(dbMedicationInfoWithBLOBs.getId(), ProgramState.Pending.getValue(), currentUser.getAccountInfo()
                    .getId(), new Date());
        }
        return dbMedicationInfoWithBLOBs;
    }

    /**
     * 
     * @description 新增保存medicationInfo
     * @author mingwang
     * @create 2016年9月29日上午11:04:37
     * @version 1.0
     * @param medicationInfoVo
     * @param currentUser
     * @return
     */
    private ProgramMedicationInfoVo saveMedication(ProgramMedicationInfoVo medicationInfoVo, UserInfoVo currentUser, Date date) {
        // 返回值
        ProgramMedicationInfoVo returnMedicationInfoVo = new ProgramMedicationInfoVo();
        // 新增的medication信息
        ProgramMedicationInfoWithBLOBs medicationInfoWithBLOBs = medicationInfoVo.getProgramMedicationInfoWithBLOBs();
        // 新增的时间集合
        List<TimeMedicationInfo> timeMedicationInfos = medicationInfoVo.getTimeMedicationInfos();
        String timeMedicationId = medicationInfoWithBLOBs.getTimeMedicationId();
        medicationInfoWithBLOBs.setId(UUID.randomUUID().toString());
        // 如果时间集合不为空，保存
        if (!ListUtil.isEmpty(timeMedicationInfos)) {
            timeMedicationId = UUID.randomUUID().toString();
            medicationInfoWithBLOBs.setTimeMedicationId(timeMedicationId);
            List<TimeMedicationInfo> saveTimeMedicationInfos = saveTimeMedicationInfos(timeMedicationId, timeMedicationInfos, currentUser);
            returnMedicationInfoVo.setTimeMedicationInfos(saveTimeMedicationInfos);
        }
        medicationInfoWithBLOBs.setCreateTime(date);
        medicationInfoWithBLOBs.setCreateAccountId(currentUser.getAccountInfo().getId());
        medicationInfoWithBLOBs.setUpdateTime(new Date());
        medicationInfoWithBLOBs.setUpdateAccountId(currentUser.getAccountInfo().getId());
        medicationInfoWithBLOBs.setDeleteFlag(DeleteFlag.Default.getValue());
        // 新增保存时，如果时间集合为空，medication直接设置为complete状态
        if (ListUtil.isEmpty(timeMedicationInfos)) {
            medicationInfoWithBLOBs.setStatu(ProgramState.Complete.getValue());
        } else {
            medicationInfoWithBLOBs.setStatu(ProgramState.Pending.getValue());
        }
        // medication信息插入数据库
        medicationInfoMapper.insertSelective(medicationInfoWithBLOBs);

        returnMedicationInfoVo.setProgramMedicationInfoWithBLOBs(medicationInfoWithBLOBs);
        UserInfo child = userInfoMapper.getUserInfoByAccountId(medicationInfoVo.getProgramMedicationInfoWithBLOBs().getChildAccountId());
        // 增加task记录
        ServiceResult<TaskInstanceInfo> result = taskInstance.addTaskInstance(TaskType.AdministrationOfMedication, roomInfoMapper
                .selectCenterIdByRoom(child.getRoomId()), child.getRoomId(), null, medicationInfoVo.getProgramMedicationInfoWithBLOBs().getId(),
                medicationInfoVo.getProgramMedicationInfoWithBLOBs().getCreateAccountId(), date);
        returnMedicationInfoVo.setTemp1(result.getReturnObj().getId());
        return returnMedicationInfoVo;
    }

    /**
     * 
     * @description 保存喂药时间
     * @author mingwang
     * @create 2016年9月26日下午4:31:44
     * @version 1.0
     * @param timeMedicationId
     * @param timeMedicationInfos
     * @param currentUser
     */
    private List<TimeMedicationInfo> saveTimeMedicationInfos(String timeMedicationId, List<TimeMedicationInfo> timeMedicationInfos,
            UserInfoVo currentUser) {
        // 循环时间集合，初始化时间信息
        for (TimeMedicationInfo timeMedicationInfo : timeMedicationInfos) {
            String timeId = UUID.randomUUID().toString();
            timeMedicationInfo.setId(timeId);
            timeMedicationInfo.setProgramMedicationId(timeMedicationId);
            Date date = new Date();
            timeMedicationInfo.setCreateTime(date);
            timeMedicationInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
            timeMedicationInfo.setUpdateTime(date);
            timeMedicationInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
            timeMedicationInfo.setDeleteFlag(DeleteFlag.Default.getValue());
            if (StringUtil.isEmpty(timeMedicationInfo.getStaffAdministeringSignature())) {
                timeMedicationInfo.setStaffAdministeringSignature(timeMedicationInfo.getAdministeringValue().getText());
            }
            if (StringUtil.isEmpty(timeMedicationInfo.getStaffChecking())) {
                timeMedicationInfo.setStaffChecking(timeMedicationInfo.getCheckingValue().getText());
            }
        }
        // 批量插入时间集合
        timeMedicationInfoMapper.batchInsertMedications(timeMedicationInfos);
        // timeMedicationInfoMapper.insertSelective(timeMedicationInfos.get(0));
        return timeMedicationInfos;
    }

    /**
     * 获取ProgramMedicationInfoVo
     */
    @Override
    public ServiceResult<Object> getMedicationInfo(String id) {
        // 定义返回值
        ProgramMedicationInfoVo medicationInfoVo = null;
        // id为空，返回一个空对象
        if (StringUtil.isEmpty(id)) {
            medicationInfoVo = new ProgramMedicationInfoVo();
            ProgramMedicationInfoWithBLOBs programMedicationInfoWithBLOBs = new ProgramMedicationInfoWithBLOBs();
            medicationInfoVo.setProgramMedicationInfoWithBLOBs(programMedicationInfoWithBLOBs);
            List<TimeMedicationInfo> timeMedicationInfos = new ArrayList<TimeMedicationInfo>();
            TimeMedicationInfo timeMedication = new TimeMedicationInfo();
            timeMedicationInfos.add(timeMedication);
            medicationInfoVo.setTimeMedicationInfos(timeMedicationInfos);
        } else {
            // 数据库获取ProgramMedicationInfoVo
            ProgramMedicationInfoVo dbMedicationInfoVo = medicationInfoMapper.getMedicationInfoVo(id);
            dbMedicationInfoVo.setChild(new ArrayList<SelecterPo>());
            dbMedicationInfoVo.setStaffReceiving(new ArrayList<SelecterPo>());
            dbMedicationInfoVo.setAssignToStaff(new ArrayList<SelecterPo>());
            // medicationInfoVo中加入SelecterPo
            medicationInfoVo = saveSelecterPo(dbMedicationInfoVo);
        }
        return successResult((Object) medicationInfoVo);
    }

    /**
     * 
     * @description medicationInfoVo中加入SelecterPo
     * @author mingwang
     * @create 2016年9月27日下午2:32:00
     * @version 1.0
     * @param dbMedicationInfoVo
     * @return
     */
    private ProgramMedicationInfoVo saveSelecterPo(ProgramMedicationInfoVo medicationInfoVo) {
        List<TimeMedicationInfo> timeMedicationInfos = medicationInfoVo.getTimeMedicationInfos();
        for (TimeMedicationInfo timeMedicationInfo : timeMedicationInfos) {
            timeMedicationInfo.getAdministeringValue().setText(timeMedicationInfo.getStaffAdministeringSignature());
            timeMedicationInfo.getCheckingValue().setText(timeMedicationInfo.getStaffChecking());
        }
        // 获取相关人员的accountId
        String childAccountId = medicationInfoVo.getProgramMedicationInfoWithBLOBs().getChildAccountId();
        String StaffReceivingAccountId = medicationInfoVo.getProgramMedicationInfoWithBLOBs().getStaffReceivingAccountId();
        String MustAssignAccountId1 = medicationInfoVo.getProgramMedicationInfoWithBLOBs().getMustAssignAccountId1();
        String MustAssignAccountId2 = medicationInfoVo.getProgramMedicationInfoWithBLOBs().getMustAssignAccountId2();
        // 根据accountId获取各自的userInfo
        UserInfo child = userInfoMapper.getUserInfoByAccountId(childAccountId);
        UserInfo StaffReceiving = userInfoMapper.getUserInfoByAccountId(StaffReceivingAccountId);
        UserInfo MustAssign1 = userInfoMapper.getUserInfoByAccountId(MustAssignAccountId1);
        UserInfo MustAssign2 = userInfoMapper.getUserInfoByAccountId(MustAssignAccountId2);
        // 如果userInfo不为空，添加到对象中
        if (child != null) {
            medicationInfoVo.getChild().add(convertObj(child, childAccountId));
        }
        if (StaffReceiving != null) {
            medicationInfoVo.getStaffReceiving().add(convertObj(StaffReceiving, StaffReceivingAccountId));
        }
        if (MustAssign1 != null) {
            medicationInfoVo.getAssignToStaff().add(convertObj(MustAssign1, MustAssignAccountId1));
        }
        if (MustAssign2 != null) {
            medicationInfoVo.getAssignToStaff().add(convertObj(MustAssign2, MustAssignAccountId2));
        }
        return medicationInfoVo;
    }

    /**
     * 
     * @description UserInfo 转换 SelecterPo
     * @author mingwang
     * @create 2016年9月27日下午3:05:49
     * @version 1.0
     * @param user
     * @param accountId
     * @return
     */
    private SelecterPo convertObj(UserInfo user, String accountId) {
        SelecterPo selecterPo = new SelecterPo();
        selecterPo.setId(accountId);
        selecterPo.setAvatar(user.getAvatar());
        selecterPo.setPersonColor(user.getPersonColor());
        String firstName = user.getFirstName() == null ? "" : user.getFirstName();
        String middleName = user.getMiddleName() == null ? "" : user.getMiddleName();
        String lastName = user.getLastName() == null ? "" : user.getLastName();
        selecterPo.setText(firstName + " " + middleName + " " + lastName);
        return selecterPo;
    }

    /**
     * 更新medication的过期状态
     */
    @Override
    public void updateOverDueMedicationTimedTask(Date now) {
        log.info("updateOverDueMedicationTimedTask|start");
        // 获取过期的Medication
        List<String> overDueIntobsleaIds = medicationInfoMapper.getOverDueMedication(now);
        // 将记录表中的记录更新为overDue
        int instanceCount = taskInstanceInfoMapper.updateStatuByValueIds(overDueIntobsleaIds);
        log.info("updateOverDueMedicationTimedTask|instanceCount:" + instanceCount);
        // 将过期Medication状态更新为overDue
        int medicationCount = medicationInfoMapper.updateOverDueMedication(now);
        log.info("updateOverDueMedicationTimedTask|end|medicationCount:" + medicationCount);

    }
}
