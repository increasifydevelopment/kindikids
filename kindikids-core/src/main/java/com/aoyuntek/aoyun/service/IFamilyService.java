package com.aoyuntek.aoyun.service;

import java.util.Date;
import java.util.List;

import com.aoyuntek.aoyun.condtion.FamilyCondition;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.entity.po.ChildAttendanceInfo;
import com.aoyuntek.aoyun.entity.po.OutFamilyPo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.vo.ChildInfoVo;
import com.aoyuntek.aoyun.entity.vo.ChildLogsVo;
import com.aoyuntek.aoyun.entity.vo.EylfInfoVo;
import com.aoyuntek.aoyun.entity.vo.FileListVo;
import com.aoyuntek.aoyun.entity.vo.MoveFamilyVo;
import com.aoyuntek.aoyun.entity.vo.OutModleVo;
import com.aoyuntek.aoyun.entity.vo.ReportVo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.aoyun.entity.vo.StaffRoleInfoVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.framework.model.Pager;
import com.theone.common.util.ServiceResult;

/**
 * @description Family业务逻辑层接口
 * @author hxzhang
 * @create 2016年7月28日下午1:56:11
 * @version 1.0
 */
public interface IFamilyService extends IBaseWebService<UserInfo, UserInfoMapper> {
	/**
	 * @description 新增或编辑小孩
	 * @author hxzhang
	 * @create 2016年7月29日下午3:46:30
	 * @version 1.0
	 * @param childInfoVo
	 *            ChildInfoVo
	 * @param currentUser
	 *            当前登录用户
	 * @return ServiceResult<Object>
	 * @throws Exception
	 */
	ServiceResult<Object> addOrUpdateChild(ChildInfoVo childInfoVo, String familyName, UserInfoVo currentUser, String ip) throws Exception;

	/**
	 * @description 新增或编辑家长
	 * @author hxzhang
	 * @create 2016年7月29日下午4:48:36
	 * @version 1.0
	 * @param parentInfoVo
	 *            parentInfoVo
	 * @param currentUser
	 *            当前登录用户
	 * @param familyId
	 *            familyId
	 * @return ServiceResult<Object>
	 */
	ServiceResult<Object> addOrUpdateParent(UserInfo parentInfo, String familyName, UserInfoVo currentUser, String ip);

	/**
	 * 
	 * @description 外部新增
	 * @author gfwang
	 * @create 2016年8月29日下午6:59:41
	 * @version 1.0
	 * @param child
	 *            小孩
	 * @param parent
	 *            家长
	 * @param familyName
	 *            家庭名称
	 * @return
	 */
	ServiceResult<Object> addFamilyOut(OutFamilyPo outFmily, OutModleVo outModleVo);

	/**
	 * @description 获取Family信息
	 * @author hxzhang
	 * @create 2016年7月31日下午2:18:10
	 * @version 1.0
	 * @param familyId
	 *            familyId
	 * @return 返回查询结果
	 */
	ServiceResult<Object> getFamilyInfo(String familyId);

	/**
	 * 
	 * @description list
	 * @author gfwang
	 * @create 2016年7月31日下午1:38:29
	 * @version 1.0
	 * @param currentUser
	 *            当前登录用户
	 * @param condition
	 *            条件
	 * @return list
	 */
	ServiceResult<Pager<FileListVo, FamilyCondition>> getPageList(UserInfoVo currentUser, FamilyCondition condition);

	List<SelecterPo> selectParentChilds(String p);

	/**
	 * @description 删除小孩
	 * @author hxzhang
	 * @create 2016年8月1日下午5:13:17
	 * @version 1.0
	 * @param userId
	 *            被删除小孩ID
	 * @param currentUser
	 *            当前登录用户
	 * @return 返回操作结果
	 */
	ServiceResult<Object> removeChild(String userId, boolean sureDelete, UserInfoVo currentUser);

	/**
	 * @description 删除家长
	 * @author hxzhang
	 * @create 2016年8月1日下午5:56:12
	 * @version 1.0
	 * @param userId
	 *            被删除家长ID
	 * @param currentUser
	 *            当前登录用户
	 * @return
	 */
	ServiceResult<Object> removeParent(String userId, UserInfoVo currentUser);

	/**
	 * @description 发送欢迎邮件
	 * @author hxzhang
	 * @create 2016年8月2日上午8:52:53
	 * @version 1.0
	 * @param userInfo
	 *            接收邮件对象
	 * @return 返回操作结果
	 */
	ServiceResult<Object> saveWelEmail(String userId);

	/**
	 * @description 批量发送欢迎邮件
	 * @author hxzhang
	 * @create 2016年9月5日下午2:44:38
	 * @version 1.0
	 * @param userList
	 * @return
	 */
	ServiceResult<Object> saveBatchWelEmail(List<UserInfo> userList, short SendEmailType, boolean isPlan, UserInfo child);

	/**
	 * @description 归档或取消归档
	 * @author hxzhang
	 * @create 2016年8月3日上午10:09:24
	 * @version 1.0
	 * @param childId
	 *            小孩ID
	 * @param falg
	 *            true 归档|false 取消归档
	 * @return 返回操作结果
	 */
	ServiceResult<Object> updateArchiveChild(String childId, boolean flag, UserInfoVo currentUser, Date lastDate);

	/**
	 * 
	 * @description 入园小孩
	 * @author gfwang
	 * @create 2016年8月29日下午9:55:30
	 * @version 1.0
	 * @param userId
	 *            用户
	 * @param accountId
	 *            账户
	 * @return ServiceResult<Object>
	 */
	ServiceResult<Object> updateEnrolChild(String userId, String accountId);

	/**
	 * 
	 * @description 获取组织信息 【ceo查询所有centers和rooms,groups】，【其它角色】【园长类登录
	 *              ，1、如果userId（空）的小孩，和currtenuser的园区一样，则查询的centers为当前此园区了；2、
	 *              如果和当前园区不一样则查询当前小孩所属园区，以及园区的rooms,groups】
	 * @author gfwang
	 * @create 2016年8月4日下午5:18:54
	 * @version 1.0
	 * @param userId
	 *            用户id
	 * @param centerId
	 *            园区
	 * @return
	 */
	StaffRoleInfoVo getFamilyOrgs();

	/**
	 * @description 保存家庭信息
	 * @author hxzhang
	 * @create 2016年8月10日上午9:01:07
	 * @version 1.0
	 * @param userId
	 */
	void saveFamilyInfo(String userId);

	/**
	 * @description 定时任务更新
	 * @author hxzhang
	 * @create 2016年8月19日下午1:48:27
	 * @version 1.0
	 * @param now
	 *            当前时间
	 */
	int updateFamilyTimedTask(Date now);

	/**
	 * @description 设置归档时间
	 * @author hxzhang
	 * @create 2016年8月23日上午9:17:22
	 * @version 1.0
	 * @param userId
	 */
	void updateArchiveTime(String userId);

	/**
	 * @description 给家长发送激活邮件
	 * @author hxzhang
	 * @create 2016年8月24日下午4:13:23
	 * @version 1.0
	 * @param familyId
	 */
	void sendEmailForParent(UserInfo child, boolean... flags);

	void dealPlanEmail(ChildAttendanceInfo childAttendanceInfo, short plan);

	/**
	 * @description 清空园区信息
	 * @author hxzhang
	 * @create 2016年8月29日上午10:37:32
	 * @version 1.0
	 * @param userId
	 */
	void delCentreInfoByUnArchive(String userId);

	/**
	 * @description 每七天发送给家长激活邮件
	 * @author hxzhang
	 * @create 2016年8月30日上午9:02:18
	 * @version 1.0
	 * @param now
	 */
	void sendActiveEmailTimedTask(Date now);

	/**
	 * 
	 * @description 获取log集合
	 * @author gfwang
	 * @create 2016年9月5日下午7:00:14
	 * @version 1.0
	 * @param userId
	 *            小孩Id
	 * @return
	 */
	List<ChildLogsVo> getLogList(String userId);

	/**
	 * 
	 * @description 新增log
	 * @author gfwang
	 * @create 2016年9月5日下午7:14:08
	 * @version 1.0
	 * @param logs
	 *            日志
	 * @param currtenUser
	 *            当前用户
	 * @return ServiceResult<Object>
	 */
	ServiceResult<Object> addLogs(ChildLogsVo childLog, UserInfoVo currtenUser);

	/**
	 * 
	 * @description 删除log
	 * @author gfwang
	 * @create 2016年9月5日下午7:42:34
	 * @version 1.0
	 * @param logId
	 * @return
	 */
	ServiceResult<Object> deleteLogs(String logId);

	/**
	 * @description 通过roomId获取已入园且已激活的小孩
	 * @author Hxzhang
	 * @create 2016年9月28日上午9:50:56
	 */
	ServiceResult<Object> getEnrolledChildByRoomId(String roomId, String params);

	/**
	 * @description 获取一个家庭里的小孩集合
	 * @author hxzhang
	 * @create 2016年10月24日下午2:32:54
	 */
	ServiceResult<Object> getChildListForSelect2(String id, String p);

	/**
	 * 
	 * @description 获取所有的年龄段
	 * @author sjwang
	 * @create 2016年11月1日上午10:06:05
	 * @version 1.0
	 * @return
	 */
	List<Integer> getAgeStage();

	/**
	 * 
	 * @description 获取ageState年龄段的能力
	 * @author sjwang
	 * @create 2016年11月1日上午10:09:56
	 * @version 1.0
	 * @param ageState
	 * @return
	 */
	List<EylfInfoVo> getEylf(Integer ageScope, String childId);

	ServiceResult<Object> updateAttendChangeState(UserInfoVo user, String userId, boolean attendFlag);

	/**
	 * @description 给家长获取task列表
	 * @author hxzhang
	 * @create 2017年3月2日上午8:57:06
	 */
	ServiceResult<Object> taskList(String accountId);

	ServiceResult<Object> getReportsHtml(ReportVo reportVo) throws Exception;

	void saveActivateParents(String userId);

	String saveOutside(OutFamilyPo outFamily);

	void removeChild(String userId);

	void removeParent(String familyId);

	void removeFamily(String familyId);

	ServiceResult<Object> saveMoveAnotherFamily(MoveFamilyVo vo, UserInfoVo currtenUser);

	ServiceResult<Object> saveMoveToOutside(String userId, boolean flag, UserInfoVo currtenUser);

	int dealNotes();

	ServiceResult<Object> saveComplete(ChildInfoVo childInfoVo) throws Exception;

	ServiceResult<Object> sendConfirmationEnrolmentEmail(String id);
}
