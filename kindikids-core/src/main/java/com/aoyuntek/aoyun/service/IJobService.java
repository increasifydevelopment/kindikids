package com.aoyuntek.aoyun.service;

import java.util.Date;

import org.apache.log4j.Logger;

import com.aoyuntek.aoyun.dao.SigninInfoMapper;
import com.aoyuntek.aoyun.entity.po.SigninInfo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;

public interface IJobService extends IBaseWebService<SigninInfo, SigninInfoMapper> {

    void dealAttendanceRequestJob(Logger logger, Date now, UserInfoVo currentUser);

    void dealEnrolledJob(Logger logger, Date now);

    void dealChildSigninJob(Logger logger, Date now);

    void dealSubtractedAttendanceJob(Logger logger, Date now);

    void dealStaffSigninJob(Logger logger, Date now, UserInfoVo currentUser);

    void dealTaskJobOfCustmer(Logger logger, Date now) throws Exception;

    void dealOverDueTaskJobOfCustmer(Logger logger, Date now);

    void dealRosterJob(Logger logger, Date now, UserInfoVo currentUser);

    void dealEnrolledJobOneWeekAgo(Date now);

    void dealChangeCenterJobOneWeekAgo(Date now);

    void updateCaualsRole();
}
