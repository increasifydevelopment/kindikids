package com.aoyuntek.aoyun.service;

import java.util.Date;
import java.util.List;

import com.aoyuntek.aoyun.condtion.MenuCondition;
import com.aoyuntek.aoyun.dao.WeekNutrionalMenuInfoMapper;
import com.aoyuntek.aoyun.entity.po.WeekNutrionalMenuInfo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.WeekMenuVo;
import com.aoyuntek.aoyun.entity.vo.WeekNutrionalMenuVo;
import com.aoyuntek.framework.model.Pager;
import com.theone.common.util.ServiceResult;

public interface IWeekNutrionalMenuService extends IBaseWebService<WeekNutrionalMenuInfo, WeekNutrionalMenuInfoMapper> {

    /**
     * 
     * @description 新增菜单
     * @author mingwang
     * @create 2017年6月1日下午1:45:02
     * @param menuInfo
     * @param currentUser
     * @return
     */
    ServiceResult<Object> addOrUpdateMenu(WeekNutrionalMenuInfo menuInfo, UserInfoVo currentUser);

    /**
     * 
     * @description 获取菜单列表
     * @author mingwang
     * @create 2017年6月1日下午2:53:07
     * @param condition
     * @param currentUser
     * @return
     */
    ServiceResult<Pager<WeekNutrionalMenuVo, MenuCondition>> getMenuList(MenuCondition condition, UserInfoVo currentUser);

    /**
     * 
     * @description 获取菜单详情
     * @author mingwang
     * @create 2017年6月1日下午6:15:24
     * @param name
     * @return
     */
    ServiceResult<Object> getMenuInfo(String menuId);

    /**
     * 
     * @description 更新菜单信息
     * @author mingwang
     * @create 2017年6月1日下午6:41:05
     * @param weekMenuList
     * @param currentUser
     * @return
     */
    ServiceResult<Object> updateWeekMenu(WeekMenuVo weekMenuVo, UserInfoVo currentUser);

    /**
     * 
     * @description 删除Menu
     * @author mingwang
     * @create 2017年6月1日下午6:59:11
     * @param name
     * @return
     */
    ServiceResult<Object> deleteMenu(String menuId);

    /**
     * 
     * @description 获取下拉框菜单
     * @author mingwang
     * @create 2017年6月5日上午10:51:04
     * @version 1.0
     * @return
     */
    ServiceResult<List<WeekMenuVo>> getSelectMenuList();

    /**
     * 
     * @description room选择菜单
     * @author mingwang
     * @create 2017年6月5日下午2:29:29
     * @version 1.0
     * @param roomId
     * @param menuId
     * @param index 
     * @return
     */
    ServiceResult<List<WeekNutrionalMenuVo>> saveMenu(String roomId, String menuId, String index);

    ServiceResult<Object> getCurrentMenu(String roomId, String index);

    void sendMenu(Date date);

}
