package com.aoyuntek.aoyun.service;

import java.util.List;
import java.util.Set;

import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.vo.BatchFileVo;
import com.aoyuntek.aoyun.entity.vo.FileVo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.aoyun.entity.vo.StaffRoleInfoVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.theone.common.util.ServiceResult;

public interface ICommonService {
	/**
	 * 
	 * @description 获取所有接收人
	 * @author gfwang
	 * @create 2016年7月12日下午8:38:10
	 * @version 1.0
	 * @param p
	 *            查询条件
	 * @param currentUser
	 *            currentUser
	 * @param isOnlyUsers
	 *            是否只获取组
	 * @return List<ReceiverVo>
	 */
	Set<SelecterPo> getReceiversMessage(String p, UserInfoVo currentUser, boolean isOnlyUsers, String defaultUser);

	/**
	 * 
	 * @description 获取所有组
	 * @author gfwang
	 * @create 2016年7月12日下午8:38:42
	 * @version 1.0
	 * @param p
	 *            查询条件
	 * @param currentUser
	 * @return List<ReceiverVo>
	 */
	List<SelecterPo> getGroupsNews(String p, UserInfoVo currentUser);

	/**
	 * 
	 * @description news获取人员
	 * @author gfwang
	 * @create 2016年7月14日下午4:15:40
	 * @version 1.0
	 * @param p
	 *            参数
	 * @param currentUser
	 *            当前用户
	 * @param isKid
	 *            标示是否只取小孩
	 * @return 结果
	 */
	Set<SelecterPo> getUsersNews(String p, UserInfoVo currentUser, int type);

	List<SelecterPo> getCenters(UserInfoVo currentUser, String p);

	List<SelecterPo> getRooms(UserInfoVo currentUser, String centerId, String p);

	List<SelecterPo> getGroups(UserInfoVo currentUser, String roomId, String p);

	List<SelecterPo> getStaff(UserInfoVo currentUser, String params, String centerId, int type);

	List<SelecterPo> getStaff2(UserInfoVo currentUser, String params, String centerId, int type);

	List<SelecterPo> getStaffAndChild(UserInfoVo currentUser, String params, String centerId, int type);

	/**
	 * 
	 * @description 获取选择园区中，staff和所有兼职
	 * @author gfwang
	 * @create 2016年10月28日上午10:31:48
	 * @version 1.0
	 * @param centerId
	 *            园区
	 * @param p
	 * @return
	 */
	List<SelecterPo> getStffByChoooseCenter(String centerId, String p);

	/**
	 * @description 初始化AttachmentInfo
	 * @author hxzhang
	 * @create 2016年7月20日下午2:43:57
	 * @version 1.0
	 * @param newsId
	 *            微博ID
	 * @param sourceId
	 *            附件源ID
	 * @param fileVoList
	 *            上传文件集合
	 * @throws Exception
	 *             异常
	 */
	void initAttachmentInfo(String sourceId, List<FileVo> fileVoList, String filePath) throws Exception;

	/**
	 * 
	 * @description 获取简写
	 * @author gfwang
	 * @create 2016年9月1日上午11:38:44
	 * @version 1.0
	 * @param userInfo
	 *            userInfo
	 * @return color
	 */
	String getPersonColor(UserInfo userInfo);

	/**
	 * 
	 * @description 根据角色获取左侧菜单的园区，room信息
	 * @author gfwang
	 * @create 2016年9月8日上午10:58:22
	 * @version 1.0
	 * @param key
	 *            标示
	 * @param currtenUser
	 *            当前用户
	 * @return
	 */
	StaffRoleInfoVo getMenuOrgs(Short type, UserInfoVo currtenUser);

	ServiceResult<BatchFileVo> uploadBatch(MultipartHttpServletRequest request, String sourceId, String dir);

	/**
	 * 
	 * @description 虚拟删除文件
	 * @author gfwang
	 * @create 2016年9月25日下午1:50:16
	 * @version 1.0
	 * @param relativePathName
	 */
	int deleteFile(List<String> relativePathNames);

	void buildUserProfileRelation(String userId, String profileId, String valueId);
}
