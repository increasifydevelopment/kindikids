package com.aoyuntek.aoyun.service.program.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.aoyuntek.aoyun.dao.AttachmentInfoMapper;
import com.aoyuntek.aoyun.dao.ClosurePeriodMapper;
import com.aoyuntek.aoyun.dao.RoomInfoMapper;
import com.aoyuntek.aoyun.dao.RoomTaskCheckMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.dao.program.ProgramNqsInfoMapper;
import com.aoyuntek.aoyun.dao.program.ProgramTagsInfoMapper;
import com.aoyuntek.aoyun.dao.program.ProgramTaskExgrscInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskInstanceInfoMapper;
import com.aoyuntek.aoyun.entity.po.RoomInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramNqsInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramTagsInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramTaskExgrscInfo;
import com.aoyuntek.aoyun.entity.vo.ActivitiesVo;
import com.aoyuntek.aoyun.entity.vo.ActivityVo;
import com.aoyuntek.aoyun.entity.vo.FileVo;
import com.aoyuntek.aoyun.entity.vo.NqsVo;
import com.aoyuntek.aoyun.entity.vo.ProgramTaskExgrscInfoVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.ProgramState;
import com.aoyuntek.aoyun.enums.TaskType;
import com.aoyuntek.aoyun.service.impl.BaseWebServiceImpl;
import com.aoyuntek.aoyun.service.program.IProgramLessonService;
import com.aoyuntek.aoyun.service.program.IProgramRegisterTaskService;
import com.aoyuntek.aoyun.service.program.IProgramTaskExgrscService;
import com.aoyuntek.aoyun.service.task.ITaskInstance;
import com.theone.common.util.ServiceResult;
import com.theone.date.util.DateUtil;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

/**
 * @description Exgrsc业务逻辑实现层
 * @author Hxzhang 2016年9月20日下午7:13:04
 * @version 1.0
 */
@Service
public class ProgramTaskExgrscServiceImpl extends BaseWebServiceImpl<ProgramTaskExgrscInfo, ProgramTaskExgrscInfoMapper> implements IProgramTaskExgrscService {
	private static Logger log = Logger.getLogger(ProgramTaskExgrscServiceImpl.class);
	@Autowired
	private ProgramTaskExgrscInfoMapper programTaskExgrscInfoMapper;
	@Autowired
	private IProgramLessonService programLessonService;
	@Autowired
	private AttachmentInfoMapper attachmentInfoMapper;
	@Autowired
	private ITaskInstance taskInstance;
	@Autowired
	private RoomInfoMapper roomInfoMapper;
	@Autowired
	private IProgramRegisterTaskService programRegisterTaskService;
	@Autowired
	private TaskInstanceInfoMapper taskInstanceInfoMapper;
	@Autowired
	private RoomTaskCheckMapper roomTaskCheckMapper;
	@Autowired
	private ProgramNqsInfoMapper programNqsInfoMapper;
	@Autowired
	private ProgramTagsInfoMapper programTagsInfoMapper;
	@Autowired
	private UserInfoMapper userInfoMapper;
	@Autowired
	private ClosurePeriodMapper closurePeriodMapper;

	@Override
	public ServiceResult<Object> addOrUpdatePorgramTaskExgrsc(ProgramTaskExgrscInfoVo exgrscInfoVo, UserInfoVo currentUser, short exgrscType) {
		log.info("addOrUpdatePorgramTaskExgrsc>>>start");
		ProgramTaskExgrscInfo exgrscInfo = exgrscInfoVo.getProgramTaskExgrscInfo();
		// 验证
		ServiceResult<Object> result = validatePorgramTaskExgrsc(exgrscInfo, exgrscType);
		if (!result.isSuccess()) {
			return result;
		}
		// 新增或编辑
		String id = initOrUpdateProgramTaskExgrscInfo(exgrscInfoVo, currentUser, exgrscType);
		// 上传附件
		try {
			programLessonService.initImages(id, exgrscInfoVo.getImages());
		} catch (Exception e) {
			e.printStackTrace();
			log.info("Upload attachments failed" + e);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			return failResult(getTipMsg("operation_failed"));
		}
		// 获取关联的Interest
		List<ActivityVo> intList = programTaskExgrscInfoMapper.getActivityManagement(exgrscInfo.getRoomId(), TaskType.LearningStory.getValue(), exgrscType,
				exgrscInfo.getDay());
		List<ActivityVo> intListFollowup = programTaskExgrscInfoMapper.getFollowupActivityManagement(exgrscInfo.getRoomId(), TaskType.LearningStory.getValue(),
				exgrscType, exgrscInfo.getDay());
		intList.addAll(intListFollowup);
		// 获取关联的Observation
		List<ActivityVo> obsList = programTaskExgrscInfoMapper.getActivityManagement(exgrscInfo.getRoomId(), TaskType.LearningStory.getValue(), exgrscType,
				exgrscInfo.getDay());
		List<ActivityVo> obsListFollowup = programTaskExgrscInfoMapper.getFollowupActivityManagement(exgrscInfo.getRoomId(), TaskType.LearningStory.getValue(),
				exgrscType, exgrscInfo.getDay());
		obsList.addAll(obsListFollowup);
		// 获取关联的Learning Story
		List<ActivityVo> leaList = programTaskExgrscInfoMapper.getActivityManagement(exgrscInfo.getRoomId(), TaskType.LearningStory.getValue(), exgrscType,
				exgrscInfo.getDay());
		List<ActivityVo> leaListFollowup = programTaskExgrscInfoMapper.getFollowupActivityManagement(exgrscInfo.getRoomId(), TaskType.LearningStory.getValue(),
				exgrscType, exgrscInfo.getDay());
		leaList.addAll(leaListFollowup);
		// 获取相关活动
		List<ActivityVo> allList = new ArrayList<ActivityVo>();
		allList.addAll(intList);
		allList.addAll(obsList);
		allList.addAll(leaList);
		List<ActivitiesVo> actList = getActivities(exgrscInfo.getActivities(), allList);

		exgrscInfoVo.setIntList(intList);
		exgrscInfoVo.setObsList(obsList);
		exgrscInfoVo.setLeaList(leaList);
		exgrscInfoVo.setActList(actList);
		log.info("addOrUpdatePorgramTaskExgrsc>>>end");
		return successResult((Object) exgrscInfoVo);
	}

	/**
	 * @description 验证
	 * @author hxzhang
	 * @create 2016年9月25日下午2:40:25
	 * @version 1.0
	 * @param exgrscInfo
	 * @return
	 */
	private ServiceResult<Object> validatePorgramTaskExgrsc(ProgramTaskExgrscInfo exgrscInfo, short exgrscType) {
		// 1.每个room每天只能存在一条
		if (StringUtil.isEmpty(exgrscInfo.getId())) {
			ProgramTaskExgrscInfo p = programTaskExgrscInfoMapper.getOnliyOneExgrscByRoomIdType(exgrscInfo.getRoomId(), exgrscType, exgrscInfo.getDay());
			if (null != p) {
				return failResult(getTipMsg("task_save_exists"));
			}
		}

		return successResult();
	}

	private String initOrUpdateProgramTaskExgrscInfo(ProgramTaskExgrscInfoVo exgrscInfoVo, UserInfoVo currentUser, short exgrscType) {
		ProgramTaskExgrscInfo exgrscInfo = exgrscInfoVo.getProgramTaskExgrscInfo();
		log.info("initOrUpdateProgramTaskExgrscInfo>>>start");
		String id;
		// 新增
		if (StringUtil.isEmpty(exgrscInfo.getId())) {
			log.info("initProgramTaskExgrscInfo>>>init>>>>start");
			id = UUID.randomUUID().toString();
			exgrscInfo.setId(id);
			exgrscInfo.setState(ProgramState.Pending.getValue());
			exgrscInfo.setType(exgrscType);
			exgrscInfo.setCenterId(roomInfoMapper.selectCenterIdByRoom(exgrscInfo.getRoomId()));
			exgrscInfo.setDeleteFlag(DeleteFlag.Default.getValue());
			programTaskExgrscInfoMapper.insert(exgrscInfo);
			taskInstance.addTaskInstance(programRegisterTaskService.getTaskType(exgrscInfo.getType()), exgrscInfo.getCenterId(), exgrscInfo.getRoomId(), null,
					exgrscInfo.getId(), exgrscInfo.getCreateAccountId(), exgrscInfo.getDay());
			log.info("initProgramTaskExgrscInfo>>>init>>>>end");
			dealNqsList(id, exgrscInfoVo.getNqsVoList());
			dealTagsAccountIds(id, exgrscInfoVo.getTagAccountIds());
		} else {// 编辑
			log.info("updateProgramTaskExgrscInfo>>>update>>>>start");
			ProgramTaskExgrscInfo dbExgrscInfo = programTaskExgrscInfoMapper.selectByPrimaryKey(exgrscInfo.getId());
			id = dbExgrscInfo.getId();
			dbExgrscInfo.setActivities(exgrscInfo.getActivities());
			dbExgrscInfo.setReflection(exgrscInfo.getReflection());
			dbExgrscInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
			dbExgrscInfo.setUpdateTime(new Date());
			programTaskExgrscInfoMapper.updateByPrimaryKey(dbExgrscInfo);
			log.info("updateProgramTaskExgrscInfo>>>update>>>>end");
			dealNqsList(id, exgrscInfoVo.getNqsVoList());
			dealTagsAccountIds(id, exgrscInfoVo.getTagAccountIds());
		}
		log.info("initOrUpdateProgramTaskExgrscInfo>>>start");
		return id;
	}

	private void dealTagsAccountIds(String taskId, List<String> tagsAccountIds) {
		// 先删除已存在的数据
		programTagsInfoMapper.removeTags(taskId);
		// 重新添加数据
		for (String accountId : tagsAccountIds) {
			ProgramTagsInfo info = new ProgramTagsInfo();
			info.setId(UUID.randomUUID().toString());
			info.setTaskId(taskId);
			info.setAccountId(accountId);
			info.setDeleteFlag(false);
			programTagsInfoMapper.insert(info);
		}
	}

	private void dealNqsList(String taskId, List<NqsVo> nqsVoList) {
		// 先删除已存在数据
		programNqsInfoMapper.removeNqsList(taskId);
		// 重新添加数据
		for (NqsVo vo : nqsVoList) {
			ProgramNqsInfo info = new ProgramNqsInfo();
			info.setId(UUID.randomUUID().toString());
			info.setTaskId(taskId);
			info.setNqsVersion(vo.getVersion());
			info.setDeleteFlag(false);
			programNqsInfoMapper.insert(info);
		}
	}

	/**
	 * @description 处理手动创建的活动,和Interest,Observation带来的活动
	 * @author hxzhang
	 * @create 2016年9月21日上午10:42:41
	 * @version 1.0
	 * @param exgrscInfo
	 * @return
	 */
	private List<ActivitiesVo> getActivities(String activitiesStr, List<ActivityVo> allList) {
		List<ActivitiesVo> actLsit = new ArrayList<ActivitiesVo>();
		// 处理Interest,Observation,Learning Story带来的活动
		for (ActivityVo activityVo : allList) {
			if (StringUtil.isEmpty(activityVo.getEducatorExperience())) {
				continue;
			}
			String[] EducatorExperience = activityVo.getEducatorExperience().split(",");
			for (String s : EducatorExperience) {
				ActivitiesVo act = new ActivitiesVo();
				act.setName(s);
				act.setOperation(false);
				actLsit.add(act);
			}
		}
		// 处理手动填写活动
		if (StringUtil.isNotEmpty(activitiesStr)) {
			String[] activities = activitiesStr.split(",");
			for (int i = 0; i < activities.length; i++) {
				ActivitiesVo act = new ActivitiesVo();
				act.setName(activities[i]);
				act.setOperation(true);
				actLsit.add(act);
			}
		}
		return actLsit;
	}

	@Override
	public ServiceResult<Object> getProgramTaskExgrsc(String id, String roomId, short exgrscType) {
		ProgramTaskExgrscInfoVo exgrscInfoVo = new ProgramTaskExgrscInfoVo();
		ProgramTaskExgrscInfo exgrscInfo = new ProgramTaskExgrscInfo();
		if (StringUtil.isEmpty(id)) {
			List<FileVo> fileList = new ArrayList<FileVo>();
			exgrscInfoVo.setProgramTaskExgrscInfo(exgrscInfo);
			exgrscInfoVo.setImages(fileList);
		} else {
			exgrscInfo = programTaskExgrscInfoMapper.selectByPrimaryKey(id);
			List<FileVo> filelist = attachmentInfoMapper.getFileListVo(id);
			exgrscInfoVo.setProgramTaskExgrscInfo(exgrscInfo);
			exgrscInfoVo.setImages(filelist);
			roomId = exgrscInfo.getRoomId();
		}
		// 获取关联的Interest
		List<ActivityVo> intList = programTaskExgrscInfoMapper.getActivityManagement(roomId, TaskType.Interest.getValue(), exgrscType, exgrscInfo.getDay());
		List<ActivityVo> intListFollowup = programTaskExgrscInfoMapper.getFollowupActivityManagement(roomId, TaskType.Interest.getValue(), exgrscType,
				exgrscInfo.getDay());
		// 获取Interest和InterestFollowup关联的活动
		// List<ActivityVo> intActivityList =
		// programTaskExgrscInfoMapper.getTaskAndFollowupActivity(roomId,
		// TaskType.Interest.getValue(), exgrscType,
		// exgrscInfo.getDay());
		// 获取关联的Observation
		List<ActivityVo> obsList = programTaskExgrscInfoMapper.getActivityManagement(roomId, TaskType.Observation.getValue(), exgrscType, exgrscInfo.getDay());
		List<ActivityVo> obsListFollowup = programTaskExgrscInfoMapper.getFollowupActivityManagement(roomId, TaskType.Observation.getValue(), exgrscType,
				exgrscInfo.getDay());
		// 获取Observation和ObservationFollowup关联的活动
		// List<ActivityVo> obsActivityList =
		// programTaskExgrscInfoMapper.getTaskAndFollowupActivity(roomId,
		// TaskType.Observation.getValue(),
		// exgrscType, exgrscInfo.getDay());
		// 获取关联的Learning Story
		List<ActivityVo> leaList = programTaskExgrscInfoMapper.getActivityManagement(roomId, TaskType.LearningStory.getValue(), exgrscType, exgrscInfo.getDay());
		List<ActivityVo> leaListFollowup = programTaskExgrscInfoMapper.getFollowupActivityManagement(roomId, TaskType.LearningStory.getValue(), exgrscType,
				exgrscInfo.getDay());
		// 获取Learning Story和Learning StoryFollowup关联的活动
		// List<ActivityVo> leaActivityList =
		// programTaskExgrscInfoMapper.getTaskAndFollowupActivity(roomId,
		// TaskType.LearningStory.getValue(),
		// exgrscType, exgrscInfo.getDay());
		// 获取相关活动
		List<ActivityVo> allList = new ArrayList<ActivityVo>();
		allList.addAll(intListFollowup);
		allList.addAll(obsListFollowup);
		allList.addAll(leaListFollowup);
		List<ActivitiesVo> actList = getActivities(exgrscInfo.getActivities(), allList);

		intList.addAll(intListFollowup);
		obsList.addAll(obsListFollowup);
		leaList.addAll(leaListFollowup);

		exgrscInfoVo.setIntList(intList);
		exgrscInfoVo.setObsList(obsList);
		exgrscInfoVo.setLeaList(leaList);
		exgrscInfoVo.setActList(actList);

		List<String> tagAccountIds = programTagsInfoMapper.getTagsAccountIds(id);
		exgrscInfoVo.setTagAccountIds(tagAccountIds);
		if (ListUtil.isNotEmpty(tagAccountIds)) {
			exgrscInfoVo.setSelecterPos(userInfoMapper.getSelecterPoByAccountIds(tagAccountIds));
		}

		List<NqsVo> nqsVoList = programNqsInfoMapper.getNqsVo(id);
		exgrscInfoVo.setNqsVoList(nqsVoList);

		return successResult((Object) exgrscInfoVo);
	}

	@Override
	public void createProgramTaskExgrscTimedTask(Date now) {
		// 周六,周日不生成任务
		if (DateUtil.getDayOfWeek(now) == 1 || DateUtil.getDayOfWeek(now) == 7) {
			return;
		}
		log.info("createProgramTaskExgrscTimedTask>>>start");
		// 获取所有room
		List<RoomInfo> roomList = roomInfoMapper.getAllRoom();
		for (RoomInfo r : roomList) {
			// 关园日不生成任务
			int count = closurePeriodMapper.getCountByCentreIdDate(r.getCentersId(), now);
			if (count > 0) {
				continue;
			}
			List<Short> taskTypes = roomTaskCheckMapper.getTaskTypeByRoomId(r.getId());
			if (taskTypes.contains(TaskType.ExploreCurriculum.getValue())) {
				addOrUpdatePorgramTaskExgrsc(createExgrscInfoVo(now, r.getId()), null, TaskType.ExploreCurriculum.getValue());
			}
			if (taskTypes.contains(TaskType.GrowCurriculum.getValue())) {
				addOrUpdatePorgramTaskExgrsc(createExgrscInfoVo(now, r.getId()), null, TaskType.GrowCurriculum.getValue());
			}
			if (taskTypes.contains(TaskType.SchoolReadiness.getValue())) {
				addOrUpdatePorgramTaskExgrsc(createExgrscInfoVo(now, r.getId()), null, TaskType.SchoolReadiness.getValue());
			}
		}
		log.info("createProgramTaskExgrscTimedTask>>>end");
	}

	/**
	 * @description 创建Vo实体
	 * @author Hxzhang
	 * @create 2016年9月25日下午9:49:54
	 */
	private ProgramTaskExgrscInfoVo createExgrscInfoVo(Date now, String roomId) {
		ProgramTaskExgrscInfoVo exgrscInfoVo = new ProgramTaskExgrscInfoVo();
		ProgramTaskExgrscInfo exgrscInfo = new ProgramTaskExgrscInfo();
		exgrscInfo.setRoomId(roomId);
		exgrscInfo.setDay(now);
		exgrscInfoVo.setProgramTaskExgrscInfo(exgrscInfo);
		return exgrscInfoVo;
	}

	@Override
	public void updateOverDueProgramTaskExgrscTimedTask(Date now) {
		log.info("updateOverDueProgramTaskExgrscTimedTask>>>start");
		// 获取过期的ProgramTaskExgrsc
		List<String> overDueExgrscIdList = programTaskExgrscInfoMapper.getOverDueProgramTaskExgrsc(now);
		log.info("updateOverDueProgramTaskExgrscTimedTask>>>overDueExgrscIdList:" + overDueExgrscIdList.size());
		// 将过期的在记录表中更新为overdue
		int instanceCount = taskInstanceInfoMapper.updateStatuByValueIds(overDueExgrscIdList);
		log.info("updateOverDueProgramTaskExgrscTimedTask>>>instanceCount:" + instanceCount);
		// 将过期的记录更新为过期
		int exgrscCount = programTaskExgrscInfoMapper.updateOverDueProgramTaskExgrsc(now);
		log.info("updateOverDueProgramTaskExgrscTimedTask>>>exgrscCount:" + exgrscCount);
		log.info("updateOverDueProgramTaskExgrscTimedTask>>>end");
	}
}
