package com.aoyuntek.aoyun.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.context.ContextLoader;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.aoyuntek.aoyun.condtion.NewsCondition;
import com.aoyuntek.aoyun.condtion.NewsFeedAuthorityCondition;
import com.aoyuntek.aoyun.constants.S3Constants;
import com.aoyuntek.aoyun.dao.AttachmentInfoMapper;
import com.aoyuntek.aoyun.dao.AuthorityGroupInfoMapper;
import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.NewsAccountsInfoMapper;
import com.aoyuntek.aoyun.dao.NewsCommentInfoMapper;
import com.aoyuntek.aoyun.dao.NewsContentInfoMapper;
import com.aoyuntek.aoyun.dao.NewsGroupInfoMapper;
import com.aoyuntek.aoyun.dao.NewsInfoMapper;
import com.aoyuntek.aoyun.dao.NewsReceiveInfoMapper;
import com.aoyuntek.aoyun.dao.NqsInfoMapper;
import com.aoyuntek.aoyun.dao.NqsRelationInfoMapper;
import com.aoyuntek.aoyun.dao.RoleInfoMapper;
import com.aoyuntek.aoyun.dao.RoomInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.dao.UserStaffMapper;
import com.aoyuntek.aoyun.dao.YelfInfoMapper;
import com.aoyuntek.aoyun.dao.YelfNewsInfoMapper;
import com.aoyuntek.aoyun.entity.po.AttachmentInfo;
import com.aoyuntek.aoyun.entity.po.CentersInfo;
import com.aoyuntek.aoyun.entity.po.NewsAccountsInfo;
import com.aoyuntek.aoyun.entity.po.NewsCommentInfo;
import com.aoyuntek.aoyun.entity.po.NewsContentInfo;
import com.aoyuntek.aoyun.entity.po.NewsGroupInfo;
import com.aoyuntek.aoyun.entity.po.NewsInfo;
import com.aoyuntek.aoyun.entity.po.NewsReceiveInfo;
import com.aoyuntek.aoyun.entity.po.NqsInfo;
import com.aoyuntek.aoyun.entity.po.NqsRelationInfo;
import com.aoyuntek.aoyun.entity.po.RoleInfo;
import com.aoyuntek.aoyun.entity.po.RoomInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.po.YelfInfo;
import com.aoyuntek.aoyun.entity.po.YelfNewsInfo;
import com.aoyuntek.aoyun.entity.vo.CusDateVo;
import com.aoyuntek.aoyun.entity.vo.FileVo;
import com.aoyuntek.aoyun.entity.vo.NewsFeedInfoVo;
import com.aoyuntek.aoyun.entity.vo.NewsInfoListVo;
import com.aoyuntek.aoyun.entity.vo.NewsfeedOtherVo;
import com.aoyuntek.aoyun.entity.vo.NqsVo;
import com.aoyuntek.aoyun.entity.vo.RoleInfoVo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.aoyun.entity.vo.UserInGroupInfo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.GroupFlag;
import com.aoyuntek.aoyun.enums.GroupType;
import com.aoyuntek.aoyun.enums.NewsStatus;
import com.aoyuntek.aoyun.enums.NewsType;
import com.aoyuntek.aoyun.enums.ResultStatus;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.enums.TagType;
import com.aoyuntek.aoyun.service.IAuthGroupService;
import com.aoyuntek.aoyun.service.ICommonService;
import com.aoyuntek.aoyun.service.INewsInfoService;
import com.aoyuntek.aoyun.uitl.DateUtil;
import com.aoyuntek.aoyun.uitl.EmailUtil;
import com.aoyuntek.aoyun.uitl.FilePathUtil;
import com.aoyuntek.aoyun.uitl.FileUtil;
import com.aoyuntek.aoyun.uitl.MyListUtil;
import com.aoyuntek.aoyun.uitl.PDFUtil;
import com.aoyuntek.framework.constants.PropertieNameConts;
import com.aoyuntek.framework.model.Pager;
import com.theone.aws.util.FileFactory;
import com.theone.common.util.ServiceResult;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

/**
 * 
 * @description newFeed服务类
 * @author gfwang
 * @create 2016年6月27日下午2:18:45
 * @version 1.0
 */
@Service
public class NewsInfoServiceImpl extends BaseWebServiceImpl<NewsInfo, NewsInfoMapper> implements INewsInfoService {
	/**
	 * 日志
	 */
	public static Logger logger = Logger.getLogger(NewsInfoServiceImpl.class);
	@Autowired
	private NewsInfoMapper newsInfoMapper;
	@Autowired
	private NewsReceiveInfoMapper newsReceiveInfoMapper;
	@Autowired
	private NqsRelationInfoMapper nqsRelationInfoMapper;
	@Autowired
	private NewsCommentInfoMapper newsCommentInfoMapper;
	@Autowired
	private NqsInfoMapper nqsInfoMapper;
	@Autowired
	private NewsContentInfoMapper newsContentInfoMapper;
	@Autowired
	private NewsGroupInfoMapper newsGroupInfoMapper;
	@Autowired
	private AttachmentInfoMapper attachmentInfoMapper;
	@Autowired
	private NewsAccountsInfoMapper newsAccountsInfoMapper;
	@Autowired
	private ICommonService commonService;
	@Value("#{auth}")
	private Properties authProperties;
	@Autowired
	private CentersInfoMapper centersInfoMapper;
	@Autowired
	private UserStaffMapper userStaffMapper;
	@Autowired
	private IAuthGroupService authGroupService;
	@Autowired
	private YelfNewsInfoMapper yelfNewsInfoMapper;
	@Autowired
	private RoomInfoMapper roomInfoMapper;
	@Autowired
	private UserInfoMapper userInfoMapper;
	@Autowired
	private YelfInfoMapper yelfInfoMapper;
	@Autowired
	private AuthorityGroupInfoMapper authorityGroupInfoMapper;
	@Autowired
	private RoleInfoMapper roleInfoMapper;

	private void setRole(NewsCondition condition, UserInfoVo currentUser) {
		List<RoleInfoVo> list = currentUser.getRoleInfoVoList();

		condition.setCeoFlag(containRoles(list, Role.CEO));

		condition.setCenterManagerFlag(containRoles(list, Role.CentreManager, Role.EducatorSecondInCharge));

		condition.setEctFlag(containRoles(list, Role.EducatorECT));

		condition.setEducatorFlag(containRoles(list, Role.Educator));

		condition.setCookFlag(containRoles(list, Role.Cook));

		condition.setParentFlag(containRoles(list, Role.Parent));

		condition.setCasualFlag(containRoles(list, Role.Casual));
	}

	@Override
	public ServiceResult<Pager<NewsInfoListVo, NewsCondition>> saveGetNewsList(NewsCondition condition, UserInfoVo currentUser) {
		// 处理查询条件
		handleNewsCondition(condition);
		condition.setPageSize(15);
		condition.setStartSize(condition.getPageIndex());

		NewsFeedAuthorityCondition authorityCondition = new NewsFeedAuthorityCondition();
		ServiceResult<List<String>> result = getNewsAuths(currentUser, authorityCondition);
		// 找不到小孩 返回
		if (ResultStatus.Fail.getValue() == (int) result.getCode()) {
			return failResult(result.getMsg());
		}
		// condition.setNewsIds(result.getReturnObj());
		condition.setAuthorityCondition(authorityCondition);
		setRole(condition, currentUser);
		// 分页集合
		long start = System.currentTimeMillis();
		List<NewsInfoListVo> newsInfoListVoList = null;
		if (condition.isCeoFlag()) { // CEO
			newsInfoListVoList = newsInfoMapper.getPagerListCEO(condition);

			solidNewsfeedLook(newsInfoListVoList, currentUser);
			handleNewsCommentInfo(newsInfoListVoList, condition, currentUser);
			// 总数
			// totalSize = newsInfoMapper.getPagerListCountCEO(condition);
		} else if ((condition.isCenterManagerFlag() || condition.isEctFlag()) && StringUtil.isNotEmpty(currentUser.getUserInfo().getCentersId())) {

			newsInfoListVoList = newsInfoMapper.getPagerListCM(condition);
			solidNewsfeedLook(newsInfoListVoList, currentUser);
			// 根据当前登录用户处理看见回复并处理回复排序
			handleNewsCommentInfo(newsInfoListVoList, condition, currentUser);
			// 总数
			// totalSize = newsInfoMapper.getPagerListCountCM(condition);
		} else if (condition.isEducatorFlag() && !condition.isEctFlag() && !condition.isCenterManagerFlag()
				&& StringUtil.isNotEmpty(currentUser.getUserInfo().getCentersId())) {
			// 如果只是普通老师（不是ect,不是没有园区的兼职）
			newsInfoListVoList = newsInfoMapper.getPagerListEdu(condition);
			solidNewsfeedLook(newsInfoListVoList, currentUser);
			// 根据当前登录用户处理看见回复并处理回复排序
			handleNewsCommentInfo(newsInfoListVoList, condition, currentUser);
			// 总数
			// totalSize = newsInfoMapper.getPagerListCountEdu(condition);
		} else if (condition.isCookFlag() && StringUtil.isNotEmpty(currentUser.getUserInfo().getCentersId())) {
			// 如果只是普通厨师（不是ect,不是没有园区的兼职）
			newsInfoListVoList = newsInfoMapper.getPagerListCook(condition);
			solidNewsfeedLook(newsInfoListVoList, currentUser);
			// 根据当前登录用户处理看见回复并处理回复排序
			handleNewsCommentInfo(newsInfoListVoList, condition, currentUser);
			// 总数
			// totalSize = newsInfoMapper.getPagerListCountCook(condition);
		} else if (condition.isCasualFlag() && StringUtil.isEmpty(currentUser.getUserInfo().getCentersId())) {
			// 如果只是兼职，且没有园区
			newsInfoListVoList = newsInfoMapper.getPagerListCasual(condition);
			solidNewsfeedLook(newsInfoListVoList, currentUser);
			// 根据当前登录用户处理看见回复并处理回复排序
			handleNewsCommentInfo(newsInfoListVoList, condition, currentUser);
			// 总数
			// totalSize = newsInfoMapper.getPagerListCountCasual(condition);
		} else if (condition.isParentFlag()) {
			// 如果为家长
			condition.setStr(getSystemMsg("AbsentRegister").trim());
			newsInfoListVoList = newsInfoMapper.getPagerListParent(condition);
			solidNewsfeedLook(newsInfoListVoList, currentUser);
			// 根据当前登录用户处理看见回复并处理回复排序
			handleNewsCommentInfo(newsInfoListVoList, condition, currentUser);
			// 总数
			// totalSize = newsInfoMapper.getPagerListCountParent(condition);
		}

		System.err.println(System.currentTimeMillis() - start);
		// 分页实体
		Pager<NewsInfoListVo, NewsCondition> pager = new Pager<NewsInfoListVo, NewsCondition>(newsInfoListVoList, condition);
		// 获取时间源
		List<CusDateVo> timeSource = DateUtil.getDateSlot();
		condition.setTimeSource(timeSource);
		// 获取center,room
		condition.setSelectCenters(userStaffMapper.selectCenters(null));
		condition.setSelectRoom(userStaffMapper.selectRoom(null));

		return successResult(getTipMsg("news_get_succeed"), pager);
	}

	@Override
	public ServiceResult<Object> getNewFeedback(NewsCondition condition, UserInfoVo currUser) {
		handleNewsCondition(condition);
		setRole(condition, currUser);
		List<String> list = new ArrayList<String>();
		if (condition.isCeoFlag()) {
			list = newsInfoMapper.newFeedbackCeo(condition);
		} else if ((condition.isCenterManagerFlag() || condition.isEctFlag()) && StringUtil.isNotEmpty(currUser.getUserInfo().getCentersId())) {
			NewsFeedAuthorityCondition authorityCondition = new NewsFeedAuthorityCondition();
			ServiceResult<List<String>> result = getNewsAuths(currUser, authorityCondition);
			if (ResultStatus.Fail.getValue() == (int) result.getCode()) {
				return failResult((Object) list);
			}
			condition.setAuthorityCondition(authorityCondition);
			list = newsInfoMapper.newFeedbackCM(condition);
		} else if (condition.isEducatorFlag() && !condition.isEctFlag() && !condition.isCenterManagerFlag()
				&& StringUtil.isNotEmpty(currUser.getUserInfo().getCentersId())) {
			NewsFeedAuthorityCondition authorityCondition = new NewsFeedAuthorityCondition();
			ServiceResult<List<String>> result = getNewsAuths(currUser, authorityCondition);
			if (ResultStatus.Fail.getValue() == (int) result.getCode()) {
				return failResult((Object) list);
			}
			condition.setAuthorityCondition(authorityCondition);
			list = newsInfoMapper.newFeedbackEdu(condition);
		} else if (condition.isCookFlag() && StringUtil.isNotEmpty(currUser.getUserInfo().getCentersId())) {
			NewsFeedAuthorityCondition authorityCondition = new NewsFeedAuthorityCondition();
			ServiceResult<List<String>> result = getNewsAuths(currUser, authorityCondition);
			if (ResultStatus.Fail.getValue() == (int) result.getCode()) {
				return failResult((Object) list);
			}
			condition.setAuthorityCondition(authorityCondition);
			list = newsInfoMapper.newFeedbackCook(condition);
		}

		return successResult((Object) list);
	}

	// 处理家长可见孩子吃喝拉撒newsfeed
	private List<String> dealParnetVisible(UserInfoVo currentUser) {
		List<String> list = newsInfoMapper.getNewsIdByFamilyId(currentUser.getUserInfo().getFamilyId());
		return list;
	}

	private void solidNewsfeedLook(List<NewsInfoListVo> newsInfoListVoList, UserInfoVo currentUser) {
		// 添加后加入分组人员与news关系
		List<NewsReceiveInfo> newsReceiveInfoList = new ArrayList<NewsReceiveInfo>();
		for (NewsInfoListVo newsInfoListVo : newsInfoListVoList) {
			int count = newsReceiveInfoMapper.getUnNewsReceiveByNewsIdAccountId(newsInfoListVo.getId(), currentUser.getAccountInfo().getId());
			if (count == 1) {
				continue;
			}
			NewsReceiveInfo newsReceiveInfo = new NewsReceiveInfo();
			newsReceiveInfo.setId(UUID.randomUUID().toString());
			newsReceiveInfo.setNewsId(newsInfoListVo.getId());
			newsReceiveInfo.setReceiveAccountId(currentUser.getAccountInfo().getId());
			newsReceiveInfo.setReceiveUserName(
					currentUser.getUserInfo().getFirstName() + " " + (null == currentUser.getUserInfo().getMiddleName() ? currentUser.getUserInfo().getLastName()
							: currentUser.getUserInfo().getMiddleName() + " " + currentUser.getUserInfo().getLastName()));
			newsReceiveInfo.setGroupFlag(GroupFlag.In.getValue());
			newsReceiveInfo.setDeleteFlag(DeleteFlag.Default.getValue());
			newsReceiveInfoList.add(newsReceiveInfo);
		}
		if (null != newsReceiveInfoList && newsReceiveInfoList.size() != 0) {
			newsReceiveInfoMapper.insterBatchNewsReceiveInfo(newsReceiveInfoList);
		}
	}

	@Override
	public ServiceResult<List<String>> getNewsAuths(UserInfoVo currentUser, NewsFeedAuthorityCondition condition) {
		// 设置当前人所属分组
		long a = System.currentTimeMillis();
		List<String> list = authGroupService.isInGroup(currentUser, GroupType.newsFeedGroup);
		System.err.println("-----------***********************------>parent=" + (System.currentTimeMillis() - a));
		condition.setGroupList(list);
		// 设置当前人accountID
		condition.setCurrtenAccountId(currentUser.getAccountInfo().getId());
		// 当前所属园区
		condition.setCurrtenCenterId(currentUser.getUserInfo().getCentersId());
		// 设置当前人的roomID 可能为空
		String roomId = currentUser.getUserInfo().getRoomId();
		log.info("getNewsAuths|roomId" + roomId);
		condition.setCurrtenRoomId(roomId);
		List<String> parentCenterIds = currentUser.getParentCenterIds();
		condition.setParentCenterIds(parentCenterIds);
		condition.setParentRoomIds(currentUser.getParentRoomsIds());
		List<RoleInfoVo> roles = currentUser.getRoleInfoVoList();
		// 设置当前人的角色集合
		condition.setCurrtenRoleList(roles);

		// 如果只有家长角色,且家长没有小孩
		if (roles.size() == 1 && roles.get(0).getValue() == Role.Parent.getValue() && parentCenterIds.size() == 0) {
			return failResult(getTipMsg("no_kid_parent"));
		}
		short flag = 1;
		for (RoleInfoVo role : roles) {
			// ceo 零时工可查看所有
			if (Role.CEO.getValue() == role.getValue()) {
				flag = -1;
			}
			if (Role.Casual.getValue() == role.getValue()) {
				flag = -2;
			}
		}
		condition.setCeoFlag(flag);
		List<String> newIds = null;// newsInfoMapper.getNewsIdByCondition(condition);
		return successResult(newIds);
	}

	@Override
	public ServiceResult<NewsFeedInfoVo> getNews(String newsId, UserInfoVo currentUser) {
		NewsFeedInfoVo newsFeedInfoVo = new NewsFeedInfoVo();
		// 获取所有分组信息
		// TODO
		List<SelecterPo> groupNames = commonService.getGroupsNews("", currentUser);
		if (StringUtil.isEmpty(newsId)) {
			newsFeedInfoVo.setGroupNameSource(groupNames);
			return successResult(newsFeedInfoVo);
		} else {
			newsFeedInfoVo = newsInfoMapper.getNews(newsId);
			// 已删除的newsfeed不能打开
			if (newsFeedInfoVo.getNewsInfo().getDeleteFlag() == 1) {
				return failResult(getTipMsg("news_deleted_CannotOperate"));
			}
			List<SelecterPo> selectAccounts = newsFeedInfoVo.getSelectAccounts();
			for (int i = selectAccounts.size() - 1; i >= 0; i--) {
				if (StringUtil.isEmpty(selectAccounts.get(i).getId())) {
					selectAccounts.remove(i);
				}
			}
			newsFeedInfoVo.setGroupNameSource(groupNames);
		}
		return successResult(getTipMsg("news_get_succeed"), newsFeedInfoVo);
	}

	@Override
	public ServiceResult<Integer> removeNewsByNewsId(String newsId, UserInfoVo currentUser) {
		// 获取该微博信息
		NewsInfo newsInfo = newsInfoMapper.selectByPrimaryKey(newsId);
		// 已删除的newsfeed不能再次被删除
		if (newsInfo.getDeleteFlag() == 1) {
			return failResult(getTipMsg("news_deleted_CannotOperate"));
		}
		// 管理员,本园园长,该微博的创建人(该微博在草稿状态)才能删除
		if (isRole(currentUser, Role.CEO.getValue())
				|| ((isRole(currentUser, Role.CentreManager.getValue()) || isRole(currentUser, Role.EducatorSecondInCharge.getValue()))
						&& currentUser.getUserInfo().getCentersId().equals(newsInfo.getCentersId()))
				|| (currentUser.getAccountInfo().getId().equals(newsInfo.getCreateAccountId()) && newsInfo.getStatus() == NewsStatus.Draft.getValue())) {
			// 获得影响行数
			int count = newsInfoMapper.updateNewsAndConmments(newsId, currentUser.getAccountInfo().getId(), new Date());
			// 删除评论
			// newsInfoMapper.removeCommentByNewsId(newsId);
			if (count <= 0) {
				return failResult(getTipMsg("news_remove_failed"));
			}
		} else {
			return failResult(getTipMsg("no.permissions"));
		}
		return successResult(getTipMsg("news_remove_succeed"));
	}

	@Override
	public ServiceResult<Object> removeNewsCommentsBynewsCommentId(String newsCommentId, UserInfoVo currentUser) {
		NewsInfo newsInfo = newsInfoMapper.getNewsInfoByNewsCommentId(newsCommentId);
		// 已删除的newsfeed不能删除评论
		if (newsInfo.getDeleteFlag() == 1) {
			return failResult(getTipMsg("news_deleted_CannotOperate"));
		}
		NewsCommentInfo newsInfoComent = newsCommentInfoMapper.selectByPrimaryKey(newsCommentId);
		// 创建者、回复者、管理员(必须是本园区管理员)才能删除评论
		if (isRole(currentUser, Role.CEO.getValue())
				|| ((isRole(currentUser, Role.CentreManager.getValue()) || isRole(currentUser, Role.EducatorSecondInCharge.getValue()))
						&& currentUser.getUserInfo().getCentersId().equals(newsInfo.getCentersId()))
				|| (currentUser.getAccountInfo().getId().equals(newsInfo.getCreateAccountId()))
				|| (currentUser.getAccountInfo().getId().equals(newsInfoComent.getCommentAccountId()))) {
			// 获得影响行数
			newsInfoMapper.updateNewsComment(newsCommentId, currentUser.getAccountInfo().getId(), new Date());
		} else {
			return failResult(getTipMsg("no.permissions"));
		}
		return successResult(getTipMsg("news_comment_remove_succeed"), (Object) newsInfo);
	}

	@Override
	public ServiceResult<Object> addOrUpdateNews(NewsFeedInfoVo newsFeedInfoVo, UserInfoVo currentUser) {
		NewsInfo newsInfo = newsFeedInfoVo.getNewsInfo();
		List<String> groupName = newsFeedInfoVo.getGroupName();
		List<NqsVo> nqsVoList = newsFeedInfoVo.getNqsVoList();
		List<String> accounts = newsFeedInfoVo.getAccounts();
		List<FileVo> fileVoList = newsFeedInfoVo.getFileVoList();
		String id = null;
		// 相关信息验证
		ServiceResult<Object> result = validateAddOrUpdateNews(newsInfo, groupName, nqsVoList, accounts, fileVoList, newsFeedInfoVo.getContent(), currentUser);
		if (!result.isSuccess()) {
			return result;
		}
		String content = newsFeedInfoVo.getContent().replaceAll("[\\n]", "<br/>").replaceAll("<br>", "<br/>");
		// 获取分组信息
		Set<String> receiverVos = getAllAccountInfo(groupName);
		Short newsType = newsInfo.getNewsType();
		if (isHaveNewsType(newsInfo.getNewsType(), NewsType.ManualPost) && (null == receiverVos || receiverVos.size() == 0)) {
			return failResult(getTipMsg("group_error"));
		}
		if (StringUtil.isEmpty(newsInfo.getId())) {
			// add
			// 初始化 newsInfo
			id = UUID.randomUUID().toString();
			String imgId = UUID.randomUUID().toString();
			newsInfo.setId(id);
			initNewsInfo(imgId, newsInfo, currentUser, null);
			newsInfoMapper.insertSelective(newsInfo);
			// 初始化newsReceiveInfo
			initNewsReceiveInfo(id, receiverVos);
			// 初始化NqsNewsInfo
			initNqsNewsInfo(id, nqsVoList);
			// 初始化NewsContentInfo
			intiNewsContentInfo(id, content);
			// 初始化NewsAccountsInfo
			initNewsAccountsInfo(id, accounts, TagType.UnTagChild.getValue());
			// 初始化NewsGroupInfo
			initNewsGroupInfo(id, groupName);
			// 初始化AttachmentInfo
			try {
				initAttachmentInfo(imgId, fileVoList);
			} catch (Exception e) {
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				logger.error("msg_file_upload_failed||", e);
				return failResult(getTipMsg("file.upload.timeout"));
			}
		} else {
			// update
			NewsInfo dbNews = newsInfoMapper.selectByPrimaryKey(newsInfo.getId());
			String newsCentreId = StringUtil.isEmpty(newsInfo.getCentersId()) ? "" : newsInfo.getCentersId();
			String currtenCentreId = StringUtil.isEmpty(currentUser.getUserInfo().getCentersId()) ? "" : newsInfo.getCentersId();
			if (isRole(currentUser, Role.CEO.getValue())
					|| ((isRole(currentUser, Role.CentreManager.getValue()) || isRole(currentUser, Role.EducatorSecondInCharge.getValue())
							|| isRole(currentUser, Role.EducatorECT.getValue())) && currtenCentreId.equals(newsCentreId))
					|| (currentUser.getAccountInfo().getId().equals(newsInfo.getCreateAccountId()) && dbNews.getStatus() == NewsStatus.Draft.getValue())) {
				id = dbNews.getId();
				String imgId = dbNews.getImgId();
				initNewsInfo(imgId, newsInfo, currentUser, dbNews);
				newsInfoMapper.updateByPrimaryKey(dbNews);
				// 更新NewsContentInfo内容
				NewsContentInfo dbNewsContentInfo = newsContentInfoMapper.getNewsContentInfoByNewsId(dbNews.getId());
				dbNewsContentInfo.setContent(content);
				newsContentInfoMapper.updateByPrimaryKeySelective(dbNewsContentInfo);

				// 如果是菜单，这些都不需要做 add bu gfwang

				// Manual Post可编辑
				if (isHaveNewsType(newsType, NewsType.ManualPost)) {
					// 删除newsReceiveInfo之前记录
					newsReceiveInfoMapper.updateNewsReceiveByNewsId(dbNews.getId());
					// 重新插入newsReceiveInfo
					initNewsReceiveInfo(dbNews.getId(), receiverVos);

					// 删除NewsGroupInfo之前记录
					newsGroupInfoMapper.updateNewsGroupInfoByNewsId(dbNews.getId());
					// 重新插入NewsGroupInfo
					initNewsGroupInfo(dbNews.getId(), groupName);
				}

				// Manual Post/Explore Curriculum/Grow
				// Curriculum/Lessons/School Readiness/Weekly Evalution可编辑
				if (isHaveNewsType(newsType, NewsType.ManualPost, NewsType.ExploreCurriculum, NewsType.GrowCurriculum, NewsType.Lessons, NewsType.SchoolReadiness,
						NewsType.WeeklyEvalutionExplore, NewsType.WeeklyEvalutionGrow)) {
					// 删除NqsNewsInfo之前记录
					nqsRelationInfoMapper.removeNqsRelationByObjId(dbNews.getId());
					// 重新插入NqsNewsInfo
					initNqsNewsInfo(dbNews.getId(), nqsVoList);
				}

				// Manual Post/Explore Curriculum/Grow
				// Curriculum/Lessons/School Readiness/Weekly Evalution可编辑
				if (isHaveNewsType(newsType, NewsType.ManualPost, NewsType.ExploreCurriculum, NewsType.GrowCurriculum, NewsType.Lessons, NewsType.SchoolReadiness)) {
					// 删除NewsAccountsInfo之前记录
					newsAccountsInfoMapper.updateNewsAccountsInfoByNewsId(dbNews.getId());
					// 重新插入NewsAccountsInfo
					initNewsAccountsInfo(dbNews.getId(), accounts, TagType.UnTagChild.getValue());
				}

				// Meal/Sleep/Nappy/Toilet Registers/Weekly Evalution不可以编辑
				if (notHaveNewswType(newsType, NewsType.ChildMeal, NewsType.Sleep, NewsType.Nappy, NewsType.ToiletRegisters, NewsType.WeeklyEvalutionExplore,
						NewsType.WeeklyEvalutionGrow)) {
					// 重新插入AttachmentInfo
					// 删除文件
					delFile(fileVoList, imgId);
					try {
						initAttachmentInfo(imgId, fileVoList);
					} catch (Exception e) {
						logger.error("msg_file_upload_failed||", e);
						throw new RuntimeException();
					}
				}
			}

		}
		return successResult(getTipMsg("operation_success"), (Object) id);
	}

	/**
	 * @description 初始化newsInfo信息
	 * @author hxzhang
	 * @create 2016年7月20日下午2:38:39
	 * @version 1.0
	 * @param imgId
	 *            IMG ID
	 * @param newsInfo
	 *            newsInfo实体
	 * @param userInfoVo
	 *            userInfoVo实体
	 * @param dbNews
	 *            数据库中dbNews
	 */
	private void initNewsInfo(String imgId, NewsInfo newsInfo, UserInfoVo userInfoVo, NewsInfo dbNews) {
		if (null == dbNews) {
			newsInfo.setCreateAccountId(userInfoVo.getAccountInfo().getId());
			// newsInfo.setCreateUserName(getUserFullName(userInfoVo));
			if (StringUtil.isNotEmpty(userInfoVo.getUserInfo().getCentersId())) {
				newsInfo.setCentersId(userInfoVo.getUserInfo().getCentersId());
			}
			if (StringUtil.isNotEmpty(userInfoVo.getUserInfo().getRoomId())) {
				newsInfo.setRoomId(userInfoVo.getUserInfo().getRoomId());
			}
			newsInfo.setStatus(NewsStatus.Draft.getValue());
			Date time = new Date();
			newsInfo.setCreateTime(time);
			newsInfo.setUpdateTime(time);
			newsInfo.setUpdateAccountId(userInfoVo.getAccountInfo().getId());
			newsInfo.setDeleteFlag(DeleteFlag.Default.getValue());
			newsInfo.setImgId(imgId);
			newsInfo.setNewsType(NewsType.ManualPost.getValue());
		} else {
			dbNews.setUpdateTime(new Date());
			dbNews.setUpdateAccountId(userInfoVo.getAccountInfo().getId());
			dbNews.setImgId(imgId);
		}
	}

	/**
	 * @description 初始化NewsReceiveInfo
	 * @author hxzhang
	 * @create 2016年7月20日下午2:39:36
	 * @version 1.0
	 * @param newsId
	 *            newsId
	 * @param receiverVos
	 *            可见人集合
	 */
	private void initNewsReceiveInfo(String newsId, Set<String> receiverVos) {
		// add gfwang
		if (ListUtil.isEmpty(receiverVos)) {
			return;
		}
		List<NewsReceiveInfo> newsReceiveInfoList = new ArrayList<NewsReceiveInfo>();
		for (String receiverVo : receiverVos) {
			NewsReceiveInfo newsReceiveInfo = new NewsReceiveInfo();
			newsReceiveInfo.setId(UUID.randomUUID().toString());
			newsReceiveInfo.setNewsId(newsId);
			newsReceiveInfo.setReceiveAccountId(receiverVo);
			newsReceiveInfo.setGroupFlag((short) 1);
			// newsReceiveInfo.setReceiveUserName(receiverVo.getName());
			newsReceiveInfo.setDeleteFlag((short) 0);
			newsReceiveInfoList.add(newsReceiveInfo);
		}
		newsReceiveInfoMapper.insterBatchNewsReceiveInfo(newsReceiveInfoList);
	}

	/**
	 * @description 初始化NqsNewsInfo
	 * @author hxzhang
	 * @create 2016年7月20日下午2:40:27
	 * @version 1.0
	 * @param newId
	 *            newId
	 * @param nqsVoList
	 *            NQS集合
	 */
	private void initNqsNewsInfo(String newId, List<NqsVo> nqsVoList) {
		// add gfwang
		if (ListUtil.isEmpty(nqsVoList)) {
			return;
		}
		List<NqsRelationInfo> nqsNewsInfoList = new ArrayList<NqsRelationInfo>();
		for (NqsVo nqs : nqsVoList) {
			NqsRelationInfo nqsNewsInfo = new NqsRelationInfo();
			nqsNewsInfo.setId(UUID.randomUUID().toString());
			nqsNewsInfo.setObjId(newId);
			nqsNewsInfo.setNqsVersion(nqs.getVersion());
			nqsNewsInfo.setDeleteFlag((short) 0);
			nqsNewsInfo.setTag(nqs.getTag());
			nqsNewsInfoList.add(nqsNewsInfo);
		}
		nqsRelationInfoMapper.insterBatchNqsRelationInfo(nqsNewsInfoList);
	}

	@Override
	public ServiceResult<Object> addOrUpdateNewsComment(NewsCommentInfo newsCommentInfo, UserInfoVo currentUser) {

		ServiceResult<Object> result = validateAddOrUpdateNewsComment(newsCommentInfo, currentUser);
		if (!result.isSuccess()) {
			return result;
		}
		int line = 0;
		if (StringUtil.isEmpty(newsCommentInfo.getId())) {
			// add
			// 初始化newsCommentInfo
			newsCommentInfo.setId(UUID.randomUUID().toString());
			newsCommentInfo.setCommentAccountId(currentUser.getAccountInfo().getId());
			// newsCommentInfo.setCommentUserName(getUserFullName(currentUser));
			Date time = new Date();
			newsCommentInfo.setCreateTime(time);
			newsCommentInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
			newsCommentInfo.setUpdateTime(time);
			newsCommentInfo.setDeleteFlag((short) 0);
			line = newsCommentInfoMapper.insertSelective(newsCommentInfo);
		} else {
			// update
			// 获取数据库对象
			NewsCommentInfo dbComment = newsCommentInfoMapper.selectByPrimaryKey(newsCommentInfo.getId());
			dbComment.setContent(newsCommentInfo.getContent());
			dbComment.setUpdateAccountId(currentUser.getAccountInfo().getId());
			dbComment.setUpdateTime(new Date());
			line = newsCommentInfoMapper.updateByPrimaryKeySelective(dbComment);
		}
		// 插入数据库
		if (line != 0) {
			return successResult(getTipMsg("operation_success"), (Object) newsCommentInfo);
		}
		return failResult(getTipMsg("operation_failed"));

	}

	@Override
	public ServiceResult<Object> updateNews(String newsId, UserInfoVo currentUser) {
		NewsInfo dbNews = newsInfoMapper.selectByPrimaryKey(newsId);
		// 已删除的newfeed不能再次Appreoved
		if (dbNews.getDeleteFlag() == 1) {
			return failResult(getTipMsg("news_deleted_CannotOperate"));
		}

		dbNews.setApproveAccountId(currentUser.getAccountInfo().getId());
		dbNews.setStatus(NewsStatus.Appreoved.getValue());
		dbNews.setApproveTime(new Date());
		int line = newsInfoMapper.updateByPrimaryKeySelective(dbNews);
		if (line != 0) {
			return successResult(getTipMsg("operation_success"));
		}
		return failResult(getTipMsg("operation_failed"));
	}

	/**
	 * @description 获取分组以及分组以外的查看人信息
	 * @author hxzhang
	 * @create 2016年6月30日下午3:29:22
	 * @version 1.0
	 * @param groupName
	 *            分组信息
	 * @return 返回查询结果
	 */
	private Set<String> getAllAccountInfo(List<String> groupName) {
		// 获取所有分组
		List<String> groupNames = authGroupService.getAllGroups();
		// 获取分组及分组以外的接收对象
		Set<String> ids = new HashSet<String>();
		if (!groupNames.containsAll(groupName)) {
			return ids;
		}

		for (String item : groupName) {
			List<UserInGroupInfo> accounts = authGroupService.dealAccountListByGroup(item);
			for (UserInGroupInfo account : accounts) {
				if (account.getDisabled()) {
					continue;
				}
				ids.add(account.getAccountId());
			}
		}
		return ids;
	}

	private boolean isHaveNewsType(Short currtenType, NewsType... newsTypes) {
		boolean result = false;
		if (currtenType == null) {
			return true;
		}
		for (NewsType type : newsTypes) {
			if (currtenType == type.getValue()) {
				result = true;
			}
		}
		return result;
	}

	private boolean notHaveNewswType(Short currtenType, NewsType... newsTypes) {
		boolean result = true;
		for (NewsType type : newsTypes) {
			if (currtenType == type.getValue()) {
				result = false;
			}
		}
		return result;
	}

	/**
	 * @description 信息验证
	 * @author hxzhang
	 * @create 2016年7月22日下午1:48:30
	 * @version 1.0
	 * @param newsInfo
	 *            微博信息
	 * @param groupName
	 *            分组信息
	 * @param nqsVoList
	 *            NQS信息
	 * @param accounts
	 *            关联人信息
	 * @param fileVoList
	 *            上传文件集合
	 * @param content
	 *            微博内容
	 * @param currentUser
	 *            当前登录用户
	 * @return 返回结果
	 */
	private ServiceResult<Object> validateAddOrUpdateNews(NewsInfo newsInfo, List<String> groupName, List<NqsVo> nqsVoList, List<String> accounts,
			List<FileVo> fileVoList, String content, UserInfoVo currentUser) {
		// 已删除的微博不能进行操作
		if (newsInfo.getDeleteFlag() != null && newsInfo.getDeleteFlag() == 1) {
			return failResult(getTipMsg("news_deleted_CannotOperate"));
		}
		// 1.微博内容非空验证
		if (StringUtil.isEmpty(content)) {
			return failResult(getTipMsg("news_content_empty"));
		}
		// 2.查看人非空验证
		if (isHaveNewsType(newsInfo.getNewsType(), NewsType.ManualPost) && ListUtil.isEmpty(groupName)) {
			return failResult(getTipMsg("news_visibility"));
		}
		// 3.NQS非空验证,有效性验证
		if (isHaveNewsType(newsInfo.getNewsType(), NewsType.ManualPost, NewsType.ExploreCurriculum, NewsType.Lessons, NewsType.SchoolReadiness,
				NewsType.WeeklyEvalutionExplore, NewsType.WeeklyEvalutionGrow) && ListUtil.isEmpty(nqsVoList)) {
			return failResult(getTipMsg("news_nqs_empty"));
		}
		// edit by gfwang
		if (ListUtil.isNotEmpty(nqsVoList)) {
			List<String> dbNsqs = nqsInfoMapper.getDbNqs(nqsVoList);
			for (NqsVo nqs : nqsVoList) {
				if (!dbNsqs.contains(nqs.getVersion())) {
					return failResult(nqs + " " + getTipMsg("msg_nqs"));
				}
			}
		}

		// 4.附件只支持上传4个
		if (ListUtil.isNotEmpty(fileVoList) && fileVoList.size() > 4) {
			return failResult(getTipMsg("news_upload"));
		}
		// 5.控制用户news分组选择权限
		List<SelecterPo> groupNames = commonService.getGroupsNews("", currentUser);
		for (String gn : groupName) {
			boolean flag = false;
			// 编辑保留微博原有分组
			if (StringUtil.isNotEmpty(newsInfo.getId())) {
				// 获取该微博原有的分组
				List<String> dbGroup = newsGroupInfoMapper.getGroupIdByNewsId(newsInfo.getId());
				// 保留微博原有的分组
				if (dbGroup.contains(gn)) {
					continue;
				}
			}
			for (SelecterPo gns : groupNames) {
				if (gns.getId().equals(gn)) {
					flag = true;
				}
			}
			if (!flag) {
				return failResult(getTipMsg("news_groupName_error"));
			}
		}

		return successResult();
	}

	/**
	 * @description 初始化CentersInfo
	 * @author hxzhang
	 * @create 2016年7月20日下午2:42:28
	 * @version 1.0
	 * @param newsId
	 *            微博ID
	 * @param content
	 *            微博内容
	 */
	private void intiNewsContentInfo(String newsId, String content) {
		NewsContentInfo newsContentInfo = new NewsContentInfo();
		newsContentInfo.setId(UUID.randomUUID().toString());
		newsContentInfo.setNewsId(newsId);
		newsContentInfo.setContent(content);
		newsContentInfoMapper.insertSelective(newsContentInfo);
	}

	/**
	 * @description 初始化NewsAccountsInfo
	 * @author hxzhang
	 * @create 2016年7月20日下午2:42:51
	 * @version 1.0
	 * @param newsId
	 *            微博ID
	 * @param accounts
	 *            关联人信息
	 */
	private void initNewsAccountsInfo(String newsId, List<String> accounts, short type) {
		if (ListUtil.isEmpty(accounts)) {
			return;
		}
		List<NewsAccountsInfo> newsAccountsInfos = new ArrayList<NewsAccountsInfo>();
		for (String accountId : accounts) {
			NewsAccountsInfo newsAccountsInfo = new NewsAccountsInfo();
			newsAccountsInfo.setId(UUID.randomUUID().toString());
			newsAccountsInfo.setNewsId(newsId);
			newsAccountsInfo.setAccountId(accountId);
			newsAccountsInfo.setDeleteFlag(DeleteFlag.Default.getValue());
			newsAccountsInfo.setType(type);
			String name = newsAccountsInfoMapper.getUserNameByAccountId(accountId);
			if (StringUtil.isNotEmpty(name)) {
				newsAccountsInfo.setUsername(name.trim());
			}
			newsAccountsInfos.add(newsAccountsInfo);
		}
		newsAccountsInfoMapper.insterBatchNewsAccountsInfo(newsAccountsInfos);
	}

	/**
	 * @description 初始化NewsGroupInfo
	 * @author hxzhang
	 * @create 2016年7月20日下午2:43:25
	 * @version 1.0
	 * @param newsId
	 *            微博ID
	 * @param groupName
	 *            分组信息
	 */
	private void initNewsGroupInfo(String newsId, List<String> groupName) {
		// add gfwang
		if (ListUtil.isEmpty(groupName)) {
			return;
		}
		List<NewsGroupInfo> newsGroupInfos = new ArrayList<NewsGroupInfo>();
		for (String gn : groupName) {
			NewsGroupInfo newsGroupInfo = new NewsGroupInfo();
			newsGroupInfo.setId(UUID.randomUUID().toString());
			newsGroupInfo.setNewsId(newsId);
			String centerId = authorityGroupInfoMapper.getCenterId(gn);
			newsGroupInfo.setGroupCenterId(centerId);
			newsGroupInfo.setGroupName(gn);
			newsGroupInfo.setDeleteFlag((short) 0);
			newsGroupInfos.add(newsGroupInfo);
		}
		newsGroupInfoMapper.insterBatchNewsGroupInfo(newsGroupInfos);
	}

	/**
	 * @description 初始化AttachmentInfo
	 * @author hxzhang
	 * @create 2016年7月20日下午2:43:57
	 * @version 1.0
	 * @param sourceId
	 *            附件源ID
	 * @param fileVoList
	 *            上传文件集合
	 * @throws Exception
	 *             异常
	 */
	private void initAttachmentInfo(String sourceId, List<FileVo> fileVoList) throws Exception {
		if (ListUtil.isEmpty(fileVoList)) {
			return;
		}
		FileFactory fac = new FileFactory(Region.getRegion(Regions.AP_SOUTHEAST_2), PropertieNameConts.S3);
		List<AttachmentInfo> attachmentInfos = new ArrayList<AttachmentInfo>();
		for (FileVo fileVo : fileVoList) {
			AttachmentInfo attachmentInfo = new AttachmentInfo();
			attachmentInfo.setId(UUID.randomUUID().toString());
			attachmentInfo.setSourceId(sourceId);

			String tempDir = fileVo.getRelativePathName();
			String mainDir = FilePathUtil.getMainPath(FilePathUtil.NewsFeed_PATH, tempDir);
			String rePath = fac.copyFile(tempDir, mainDir);
			attachmentInfo.setAttachId(rePath);

			attachmentInfo.setAttachName(fileVo.getFileName());
			attachmentInfo.setVisitUrl(fileVo.getVisitedUrl());
			attachmentInfo.setDeleteFlag((short) 0);
			attachmentInfos.add(attachmentInfo);
		}
		attachmentInfoMapper.insterBatchAttachmentInfo(attachmentInfos);
	}

	/**
	 * @description 删除附件
	 * @author hxzhang
	 * @create 2016年7月20日下午2:44:56
	 * @version 1.0
	 * @param fileVoList
	 *            附件集合
	 * @param sourceId
	 *            附件源ID
	 */
	private void delFile(List<FileVo> fileVoList, String sourceId) {
		// 保留不删除的附件
		List<FileVo> unDelList = new ArrayList<FileVo>();
		if (null != fileVoList && fileVoList.size() != 0) {
			for (int i = fileVoList.size() - 1; i >= 0; i--) {
				if (fileVoList.get(i).getRelativePathName().indexOf(FilePathUtil.NewsFeed_PATH) != -1) {
					unDelList.add(fileVoList.get(i));
					fileVoList.remove(i);
				}
			}
		}
		attachmentInfoMapper.deleteBatchAttachmentInfo(unDelList, sourceId);
	}

	@Override
	public ServiceResult<Object> updateUnApproveNews(String newsId, UserInfoVo currentUser) {
		NewsInfo dbNews = newsInfoMapper.selectByPrimaryKey(newsId);
		// 已删除的newfeed不能再次Draft
		if (dbNews.getDeleteFlag() == 1) {
			return failResult(getTipMsg("news_deleted_CannotOperate"));
		}
		dbNews.setApproveAccountId(currentUser.getAccountInfo().getId());
		dbNews.setStatus(NewsStatus.Draft.getValue());
		int line = newsInfoMapper.updateByPrimaryKeySelective(dbNews);
		if (line != 0) {
			return successResult(getTipMsg("operation_success"));
		}
		return failResult(getTipMsg("operation_failed"));
	}

	@Override
	public ServiceResult<Object> putScore(String newsId, String score, UserInfoVo currentUser) {
		NewsInfo dbNews = newsInfoMapper.selectByPrimaryKey(newsId);
		// 已删除的newfeed不能再次评分
		if (dbNews.getDeleteFlag() == 1) {
			return failResult(getTipMsg("news_deleted_CannotOperate"));
		}

		dbNews.setScore(Integer.valueOf(score));
		dbNews.setUpdateAccountId(currentUser.getAccountInfo().getId());
		dbNews.setUpdateTime(new Date());
		int line = newsInfoMapper.updateByPrimaryKeySelective(dbNews);
		if (line != 0) {
			return successResult(getTipMsg("operation_success"));
		}
		return failResult(getTipMsg("operation_failed"));
	}

	/**
	 * @description 处理回复排序
	 * @author hxzhang
	 * @create 2016年7月20日下午2:45:32
	 * @version 1.0
	 * @param newsInfoListVoList
	 *            微博列表对象集合
	 */
	private void handleCommentList(List<NewsInfoListVo> newsInfoListVoList) {
		for (NewsInfoListVo newsInfoListVo : newsInfoListVoList) {
			List<NewsCommentInfo> commentList = newsInfoListVo.getCommentList();
			if (ListUtil.isNotEmpty(commentList)) {
				@SuppressWarnings("rawtypes")
				Iterator it = commentList.iterator();
				while (it.hasNext()) {
					NewsCommentInfo common = (NewsCommentInfo) it.next();
					if (null == common.getUpdateTime()) {
						it.remove();
						continue;
					}
				}
				Collections.sort(commentList, new Comparator<NewsCommentInfo>() {
					public int compare(NewsCommentInfo o1, NewsCommentInfo o2) {
						if (o1.getUpdateTime().getTime() > o2.getUpdateTime().getTime()) {
							return -1;
						}
						return 0;
					}
				});
			}
		}

	}

	/**
	 * @description 处理查询条件
	 * @author hxzhang
	 * @create 2016年7月19日下午4:54:24
	 * @version 1.0
	 * @param condition
	 *            查询条件
	 */
	private void handleNewsCondition(NewsCondition condition) {
		if (StringUtil.isNotEmpty(condition.getNqsVersion())) {
			if (condition.getNqsVersion().toUpperCase().equals("QA1")) {
				condition.setSelectNqs("1");
			} else if (condition.getNqsVersion().toUpperCase().equals("QA2")) {
				condition.setSelectNqs("2");
			} else if (condition.getNqsVersion().toUpperCase().equals("QA3")) {
				condition.setSelectNqs("3");
			} else if (condition.getNqsVersion().toUpperCase().equals("QA4")) {
				condition.setSelectNqs("4");
			} else if (condition.getNqsVersion().toUpperCase().equals("QA5")) {
				condition.setSelectNqs("5");
			} else if (condition.getNqsVersion().toUpperCase().equals("QA6")) {
				condition.setSelectNqs("6");
			} else if (condition.getNqsVersion().toUpperCase().equals("QA7")) {
				condition.setSelectNqs("7");
			} else {
				condition.setSelectNqs(condition.getNqsVersion());
			}
		} else {
			condition.setSelectNqs(null);
		}
		if (StringUtil.isNotEmpty(condition.getType())) {
			List<String> str = new ArrayList<String>(Arrays.asList(condition.getType().split(";")));
			condition.setSelectType(str);
		} else {
			condition.setSelectType(null);
		}
	}

	/**
	 * @description 评论数据验证
	 * @author hxzhang
	 * @create 2016年7月20日下午3:39:54
	 * @version 1.0
	 * @param newsCommentInfo
	 *            NewsCommentInfo
	 * @param currentUser
	 *            UserInfoVo
	 * @return 返回结果
	 */
	private ServiceResult<Object> validateAddOrUpdateNewsComment(NewsCommentInfo newsCommentInfo, UserInfoVo currentUser) {
		NewsInfo newsInfo = newsInfoMapper.selectByPrimaryKey(newsCommentInfo.getNewsId());
		// 已删除的newfeed不能再次评论,和编辑之前的评论
		if (newsInfo.getDeleteFlag() == 1) {
			return failResult(getTipMsg("news_deleted_CannotOperate"));
		}
		// 1.草稿状态下不能新增评论
		if (StringUtil.isEmpty(newsCommentInfo.getId()) && NewsStatus.Draft.getValue() == newsInfo.getStatus()) {
			return failResult(getTipMsg("comment_add"));
		}
		// 2.回复只能被回复创建者修改
		NewsCommentInfo dbComment = newsCommentInfoMapper.selectByPrimaryKey(newsCommentInfo.getId());
		if (StringUtil.isNotEmpty(newsCommentInfo.getId()) && !currentUser.getAccountInfo().getId().equals(dbComment.getCommentAccountId())) {
			return failResult(getTipMsg("del_comment_error"));
		}

		return successResult();
	}

	/**
	 * @description 判断当前登录人角色值
	 * @author hxzhang
	 * @create 2016年7月20日下午7:36:08
	 * @version 1.0
	 * @param currentUser
	 *            当前登录人
	 * @param role
	 *            角色值
	 * @return 返回结果
	 */
	private boolean isRole(UserInfoVo currentUser, Short role) {
		List<RoleInfoVo> roleList = currentUser.getRoleInfoVoList();
		for (RoleInfoVo roleInfoVo : roleList) {
			if (role.compareTo(roleInfoVo.getValue()) == 0) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * @description 根据角色获取组
	 * @author gfwang
	 * @create 2016年7月20日下午7:12:32
	 * @version 1.0
	 * @param user
	 *            当前用户
	 * @return 返回查询结果
	 */
	private List<String> getGroupByCurrtenUser(UserInfoVo user) {
		String head = "news.list.";
		Set<String> set = new LinkedHashSet<String>();
		// 园区id
		String centerId = user.getUserInfo().getCentersId();
		if (StringUtil.isEmpty(centerId)) {
			return new MyListUtil<String>().set2List(set);
		}
		CentersInfo center = centersInfoMapper.selectByPrimaryKey(centerId);
		String name = center.getName();
		List<RoleInfoVo> roles = user.getRoleInfoVoList();
		for (RoleInfoVo role : roles) {
			String roleName = Role.getNameByValue(role.getValue());
			roleName = head + roleName.replaceAll(" ", "_");
			String groups = authProperties.getProperty(roleName);
			if (StringUtil.isEmpty(groups)) {
				continue;
			}
			groups = MessageFormat.format(groups, name);
			set.addAll(Arrays.asList(groups.split("###")));
		}
		return new MyListUtil<String>().set2List(set);
	}

	/**
	 * @description 处理当前登录用户角色
	 * @author hxzhang
	 * @create 2016年7月22日下午1:50:18
	 * @version 1.0
	 * @param newsInfoListVoList
	 *            List<NewsInfoListVo>
	 * @param condition
	 *            查询条件
	 * @param currentUser
	 *            当前登录用户
	 */
	private void handleNewsCommentInfo(List<NewsInfoListVo> newsInfoListVoList, NewsCondition condition, UserInfoVo currentUser) {
		// 当前登录用户为家长只能查看自己及家庭人员的评论,不是则查看所有.
		if (containRoles(currentUser.getRoleInfoVoList(), Role.Parent)) {
			// 获取该家庭所有的家长Account信息
			List<String> accountIds = userInfoMapper.getAccountIdsByFamilyId(currentUser.getUserInfo().getFamilyId());
			for (NewsInfoListVo newsInfoListVo : newsInfoListVoList) {
				List<NewsCommentInfo> commentList = newsInfoListVo.getCommentList();
				for (int i = commentList.size() - 1; i >= 0; i--) {
					if (!accountIds.contains(commentList.get(i).getCommentAccountId())) {
						commentList.remove(i);
					}
				}
			}
		}
		handleCommentList(newsInfoListVoList);
	}

	@Override
	public ServiceResult<Object> createNewsByOther(NewsfeedOtherVo newsInfoVo) {
		String id = null;
		newsInfoVo.setContent(newsInfoVo.getContent().replaceAll("[\\n]", "<br/>"));
		if (StringUtil.isEmpty(newsInfoVo.getNewsId())) {
			NewsInfo news = new NewsInfo();
			id = UUID.randomUUID().toString();
			news.setId(id);
			initNewsInfo(newsInfoVo, news);
			newsInfoMapper.insert(news);
			// 初始化NewsContentInfo
			intiNewsContentInfo(id, newsInfoVo.getContent());
			if (ListUtil.isNotEmpty(newsInfoVo.getNqsVoList())) {
				// 初始化NqsNewsInfo
				initNqsNewsInfo(id, newsInfoVo.getNqsVoList());
			}
			if (ListUtil.isNotEmpty(newsInfoVo.getEylfList())) {
				// 初始化EylfNewsInfo
				initEylfNewsInfo(id, newsInfoVo.getEylfList());
			}
			// 初始化NewsAccountsInfo
			initNewsAccountsInfo(id, newsInfoVo.getAccounts(), TagType.UnTagChild.getValue());
			initNewsAccountsInfo(id, newsInfoVo.getTagAccounts(), TagType.TagChild.getValue());

		} else {
			id = newsInfoVo.getNewsId();
			NewsInfo news = newsInfoMapper.selectByPrimaryKey(newsInfoVo.getNewsId());
			initNewsInfo(newsInfoVo, news);
			// add by gfwang,每次分享都要修改创建时间
			news.setCreateTime(new Date());
			news.setUpdateTime(new Date());

			newsInfoMapper.updateByPrimaryKey(news);
			// 更新NewsContentInfo内容
			NewsContentInfo dbNewsContentInfo = newsContentInfoMapper.getNewsContentInfoByNewsId(news.getId());
			dbNewsContentInfo.setContent(newsInfoVo.getContent());
			newsContentInfoMapper.updateByPrimaryKeySelective(dbNewsContentInfo);

			if (ListUtil.isNotEmpty(newsInfoVo.getNqsVoList())) {
				// 删除NqsNewsInfo之前记录
				nqsRelationInfoMapper.removeNqsRelationByObjId(news.getId());
				// 初始化NqsNewsInfo
				initNqsNewsInfo(news.getId(), newsInfoVo.getNqsVoList());
			}
			if (ListUtil.isNotEmpty(newsInfoVo.getEylfList())) {
				// 删除YelfNewsInfo之前记录
				yelfNewsInfoMapper.updateYelfNewsInfoByNewsId(news.getId());
				// 初始化EylfNewsInfo
				initEylfNewsInfo(news.getId(), newsInfoVo.getEylfList());
			}
			// 删除NewsAccountsInfo之前记录
			newsAccountsInfoMapper.removeNewsAccountsInfoByNewsId(news.getId());
			// 初始化NewsAccountsInfo
			initNewsAccountsInfo(id, newsInfoVo.getAccounts(), TagType.UnTagChild.getValue());
			initNewsAccountsInfo(id, newsInfoVo.getTagAccounts(), TagType.TagChild.getValue());
		}
		return successResult((Object) id);
	}

	/**
	 * @description 初始化Eylf信息,并批量插入
	 * @author hxzhang
	 * @create 2016年9月21日下午7:34:31
	 * @version 1.0
	 * @param newsId
	 * @param eylfList
	 */
	private void initEylfNewsInfo(String newsId, List<YelfInfo> eylfList) {
		List<YelfNewsInfo> eylfNewsList = new ArrayList<YelfNewsInfo>();
		for (YelfInfo y : eylfList) {
			YelfNewsInfo yn = new YelfNewsInfo();
			yn.setId(UUID.randomUUID().toString());
			yn.setNewsId(newsId);
			yn.setYelfVersion(y.getVersion());
			yn.setDeleteFlag(DeleteFlag.Default.getValue());
			eylfNewsList.add(yn);
		}
		yelfNewsInfoMapper.insterBatchYelfNewsInfo(eylfNewsList);
	}

	private NewsInfo initNewsInfo(NewsfeedOtherVo newsInfoVo, NewsInfo news) {
		UserInfoVo currentUser = newsInfoVo.getCurrtenUser();
		String centerId = StringUtil.isEmpty(newsInfoVo.getCenterId()) ? roomInfoMapper.selectCenterIdByRoom(newsInfoVo.getRoomId()) : newsInfoVo.getCenterId();
		news.setCentersId(centerId);
		news.setRoomId(newsInfoVo.getRoomId());
		if (null != currentUser) {
			news.setCreateAccountId(currentUser.getAccountInfo().getId());
			news.setCreateUserName(currentUser.getUserInfo().getFullName());
		}
		Date now = new Date();
		Date createTime = null == newsInfoVo.getCreateTime() ? now : newsInfoVo.getCreateTime();
		Date updateTime = null == newsInfoVo.getUpdateTime() ? now : newsInfoVo.getUpdateTime();
		news.setCreateTime(createTime);
		news.setUpdateTime(updateTime);
		news.setNewsTag(newsInfoVo.getNewsTags());
		news.setStatus(newsInfoVo.getStatus());
		// 状态为Approve添加Approve时间
		if (newsInfoVo.getStatus() == NewsStatus.Appreoved.getValue()) {
			news.setApproveTime(createTime);
		}
		news.setNewsType(newsInfoVo.getNewsType().getValue());
		// 获取program带来的附件
		List<AttachmentInfo> attLsit = attachmentInfoMapper.getAttachmentInfoBySourceId(newsInfoVo.getSourceId());
		String sourceId = UUID.randomUUID().toString();

		// 将附件从program目录拷newsfeed目录
		FileFactory fac;
		try {
			fac = new FileFactory(Region.getRegion(Regions.AP_SOUTHEAST_2), PropertieNameConts.S3);
			for (AttachmentInfo att : attLsit) {
				att.setId(UUID.randomUUID().toString());
				att.setSourceId(sourceId);
				String programMainDir = att.getAttachId();
				String newsMainDir = FilePathUtil.getMainPath(FilePathUtil.NewsFeed_PATH, programMainDir);
				String rePath = fac.copyFile(programMainDir, newsMainDir);
				att.setAttachId(rePath);
			}
			// 插入附件信息
			if (ListUtil.isNotEmpty(attLsit)) {
				attachmentInfoMapper.insterBatchAttachmentInfo(attLsit);
			}
		} catch (Exception e) {
			logger.info("createNewsByOther=====>uploud file error");
			e.printStackTrace();
		}

		news.setImgId(sourceId);
		news.setDeleteFlag(DeleteFlag.Default.getValue());
		return news;
	}

	@Override
	public void getNewsfeedPdf(String id, OutputStream outputStream, UserInfoVo currentUser) throws Exception {
		NewsInfo newsInfo = newsInfoMapper.selectByPrimaryKey(id);
		String filePath = ContextLoader.getCurrentWebApplicationContext().getServletContext().getRealPath("/") + "templet" + File.separator + "pdf" + File.separator;
		String template = filePath + "newsfeedPdf.html";
		String html = FileUtil.readTxt(template);
		html = getHtmlCertificate(newsInfo, html, currentUser);
		List<File> files = new ArrayList<File>();
		System.err.println(html);
		files.add(FileUtil.getFile(html, filePath));
		PDFUtil.htmls2Pdf(files, outputStream);
	}

	public String getHtmlCertificate(NewsInfo newsInfo, String html, UserInfoVo currentUser) throws FileNotFoundException {
		Map<String, String> map = new HashMap<String, String>();
		switch (newsInfo.getNewsType()) {
		case 6:
		case 5:
		case 11:
		case 9:
			map = getTemValue(newsInfo, "EYLF:", getEylfStr(newsInfo.getId()), "", map);
			break;
		case 4:
		case 8:
		case 10:
		case 18:
		case 20:
			map = getTemValue(newsInfo, "", "", "", map);
			break;
		case 13:
		case 14:
		case 15:
		case 16:
			newsInfo.setCreateAccountId(null);
			map = getTemValue(newsInfo, "", "", "", map);
			break;
		case 0:
		case 1:
		case 2:
		case 7:
		case 3:
		case 17:
		case 19:
			map = getTemValue(newsInfo, "Attached NQS:", getNqsStr(newsInfo.getId()), "Participated Children or Staff:", map);
			break;
		case 12:
			map = getTemValue(newsInfo, "Attached NQS:", getNqsStr(newsInfo.getId()), "", map);
			break;
		default:
			break;
		}

		html = html.replaceAll(EmailUtil.getHtmlTag("avatar"), null == map.get("avatar") ? "" : map.get("avatar"));
		html = html.replaceAll(EmailUtil.getHtmlTag("dateAndType"), null == map.get("dateAndType") ? "" : map.get("dateAndType"));
		html = html.replaceAll(EmailUtil.getHtmlTag("author"), null == map.get("author") ? "" : map.get("author"));
		html = html.replaceAll(EmailUtil.getHtmlTag("centerName"), null == map.get("centerName") ? "" : map.get("centerName"));
		html = html.replaceAll(EmailUtil.getHtmlTag("roomName"), null == map.get("roomName") ? "" : map.get("roomName"));
		html = html.replaceAll(EmailUtil.getHtmlTag("type"), null == map.get("type") ? "" : map.get("type"));
		html = html.replaceAll(EmailUtil.getHtmlTag("img"), null == map.get("img") ? "" : map.get("img"));
		if (newsInfo.getNewsType() == NewsType.TodayMenus.getValue()) {
			html = html.replaceAll(EmailUtil.getHtmlTag("details"), null == map.get("details") ? "" : map.get("details"));
		} else {
			html = html.replaceAll(EmailUtil.getHtmlTag("details"), null == map.get("details") ? "" : convertSpecialCode(map.get("details")));
		}
		html = html.replaceAll(EmailUtil.getHtmlTag("nqsOrEylf"), null == map.get("nqsOrEylf") ? "" : map.get("nqsOrEylf"));
		html = html.replaceAll(EmailUtil.getHtmlTag("version"), null == map.get("version") ? "" : map.get("version"));
		html = html.replaceAll(EmailUtil.getHtmlTag("listTitle"), null == map.get("listTitle") ? "" : map.get("listTitle"));
		html = html.replaceAll(EmailUtil.getHtmlTag("nameList"), null == map.get("nameList") ? "" : map.get("nameList"));
		return html;
	}

	/**
	 * @description 获取模版数据
	 * @author Hxzhang
	 * @create 2016年9月28日下午8:06:14
	 */
	private Map<String, String> getTemValue(NewsInfo newsInfo, String nqsOrEylf, String version, String listTitle, Map<String, String> map) throws FileNotFoundException {
		UserInfo child = userInfoMapper.getTagChildByNewsId(newsInfo.getId());
		UserInfo createUser = userInfoMapper.getUserInfoByAccountId(newsInfo.getCreateAccountId());
		List<AttachmentInfo> attList = attachmentInfoMapper.getAttachmentInfoBySourceId(newsInfo.getImgId());
		String img = NewsType.Birthday.getValue() == newsInfo.getNewsType() ? MessageFormat.format(getSystemPdfMsg("one_img"), getSystemMsg("birthday"))
				: getImgStr(attList);
		String nameList = getNameList(newsInfo.getId());

		map.put("avatar", getAvatar(child));
		map.put("dateAndType", getDateAndType(newsInfo, child));
		map.put("author", null == createUser ? getSystemMsg("system") : createUser.getFullName());
		map.put("centerName", getCenterName(newsInfo.getCentersId()));
		map.put("roomName", getRoomName(newsInfo.getRoomId()));
		map.put("type", getNewsType(newsInfo.getNewsType()).getDesc());
		map.put("img", img);
		String content = newsInfoMapper.getContentByNewsId(newsInfo.getId());
		content = content.replaceAll("&nbsp;", " ");
		map.put("details", content);
		map.put("nqsOrEylf", nqsOrEylf);
		map.put("version", version);
		map.put("listTitle", StringUtil.isEmpty(nameList) ? "" : listTitle);
		map.put("nameList", nameList);

		return map;
	}

	/**
	 * @description 获取centerName
	 * @author Hxzhang
	 * @create 2016年9月29日上午10:13:24
	 */
	private String getCenterName(String centerId) {
		if (StringUtil.isEmpty(centerId)) {
			return null;
		}
		CentersInfo c = centersInfoMapper.selectByPrimaryKey(centerId);
		return MessageFormat.format(getSystemPdfMsg("center"), c.getName());
	}

	/**
	 * @description 获取room名称
	 * @author Hxzhang
	 * @create 2016年9月28日下午10:50:34
	 */
	private String getRoomName(String roomId) {
		if (StringUtil.isEmpty(roomId)) {
			return null;
		}
		RoomInfo r = roomInfoMapper.selectByPrimaryKey(roomId);
		return MessageFormat.format(getSystemPdfMsg("room"), r.getName());
	}

	/**
	 * @description 获取newsfeed关联的人员
	 * @author Hxzhang
	 * @create 2016年9月28日下午8:18:40
	 */
	private String getNameList(String newsId) {
		List<String> nameList = userInfoMapper.getNameList(newsId);
		String str = "";
		for (String n : nameList) {
			if (StringUtil.isEmpty(n)) {
				continue;
			}
			str += MessageFormat.format(getSystemPdfMsg("name"), n);
		}
		return str;
	}

	/**
	 * @description 处理头像
	 * @author Hxzhang
	 * @create 2016年9月28日下午8:07:27
	 */
	public String getAvatar(UserInfo child) throws FileNotFoundException {
		if (null == child) {
			return null;
		}
		if (StringUtil.isEmpty(child.getAvatar())) {
			String simpleName = getSimpleName(child.getFirstName(), child.getLastName());
			return MessageFormat.format(getSystemPdfMsg("avatar_name_simple"), "pdf" + child.getPersonColor(), simpleName);
		}
		String avatar = StringUtil.isEmpty(child.getAvatar()) ? getSystemMsg("emptyAvatar")
				: convertSpecialCode(new FileFactory(Region.getRegion(Regions.AP_SOUTHEAST_2), PropertieNameConts.S3).generateUrl(S3Constants.BUCKET_NAME,
						child.getAvatar(), S3Constants.URL_TIMEOUT_HOUR));

		return avatar = MessageFormat.format(getSystemPdfMsg("avatar"), avatar);
	}

	public String getSimpleName(String firstName, String lastName) {
		StringBuffer buffer = new StringBuffer();
		if (StringUtil.isNotEmpty(firstName) && firstName.length() >= 1) {
			buffer.append(firstName.substring(0, 1));
		}
		if (StringUtil.isNotEmpty(lastName) && lastName.length() >= 1) {
			buffer.append(lastName.substring(0, 1));
		}
		return buffer.toString();
	}

	/**
	 * @description 获取日期类型
	 * @author Hxzhang
	 * @create 2016年9月28日下午8:06:58
	 */
	private String getDateAndType(NewsInfo newsInfo, UserInfo child) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
		String str = (null == child ? "" : " on " + child.getFullName());
		return sdf.format(newsInfo.getCreateTime()) + " " + getNewsType(newsInfo.getNewsType()).getDesc() + str;
	}

	/**
	 * @description 获取NewsType描述
	 * @author Hxzhang
	 * @create 2016年9月27日上午9:26:07
	 */
	private NewsType getNewsType(short value) {
		for (NewsType nt : NewsType.values()) {
			if (value == nt.getValue()) {
				return nt;
			}
		}
		return null;
	}

	/**
	 * @description 获取Details
	 * @author Hxzhang
	 * @create 2016年9月27日上午10:19:25
	 */
	// private String getDetailsStr(short newsType, Object... date) {
	// String detailsStr = null;
	// if (date.length == 5) {
	// detailsStr =
	// MessageFormat.format(getSystemPdfMsg(getNewsType(newsType).toString()),
	// date[0].toString(), date[1].toString(),
	// date[2].toString(), date[3].toString(), date[4].toString());
	// } else if (date.length == 4) {
	// detailsStr =
	// MessageFormat.format(getSystemPdfMsg(getNewsType(newsType).toString()),
	// date[0].toString(), date[1].toString(),
	// date[2].toString(), date[3].toString());
	// }
	// return detailsStr;
	// }

	/**
	 * @description 获取选择的Eylf
	 * @author Hxzhang
	 * @create 2016年9月27日上午10:37:23
	 */
	private String getEylfStr(String id) {
		String eylfStr = "";
		List<YelfInfo> eylfList = yelfInfoMapper.getYelfInfoByNewsId(id);
		for (YelfInfo e : eylfList) {
			eylfStr += MessageFormat.format(getSystemPdfMsg("version"), e.getVersion().toString(), e.getContent().toString());
		}
		return eylfStr;
	}

	private String getNqsStr(String id) {
		String nqsStr = "";
		List<NqsInfo> nqsList = nqsInfoMapper.getNqsInfoListByNewsId(id);
		for (NqsInfo n : nqsList) {
			nqsStr += MessageFormat.format(getSystemPdfMsg("version"), n.getVersion().toString(), n.getContent().toString());
		}
		return nqsStr;
	}

	/**
	 * @description 转义S3url
	 * @author Hxzhang
	 * @throws FileNotFoundException
	 * @create 2016年9月27日下午3:13:53
	 */
	private String getImgStr(List<AttachmentInfo> attList) throws FileNotFoundException {
		String imgStr = "";
		if (attList.size() == 1) {
			imgStr = MessageFormat.format(getSystemPdfMsg("one_img"), getUrl(attList.get(0).getAttachId()));
		} else if (attList.size() == 2) {
			imgStr = MessageFormat.format(getSystemPdfMsg("two_img"), getUrl(attList.get(0).getAttachId()), getUrl(attList.get(1).getAttachId()));
		} else if (attList.size() == 3) {
			imgStr = MessageFormat.format(getSystemPdfMsg("three_img"), getUrl(attList.get(0).getAttachId()), getUrl(attList.get(1).getAttachId()),
					getUrl(attList.get(2).getAttachId()));
		} else if (attList.size() == 4) {
			imgStr = MessageFormat.format(getSystemPdfMsg("four_img"), getUrl(attList.get(0).getAttachId()), getUrl(attList.get(1).getAttachId()),
					getUrl(attList.get(2).getAttachId()), getUrl(attList.get(3).getAttachId()));
		}
		return imgStr;
	}

	/**
	 * @description 处理url,S3转换url及特殊字符转义
	 * @author Hxzhang
	 * @create 2016年9月29日下午1:56:20
	 */
	private String getUrl(String url) throws FileNotFoundException {

		return convertSpecialCode(
				new FileFactory(Region.getRegion(Regions.AP_SOUTHEAST_2), PropertieNameConts.S3).generateUrl(S3Constants.BUCKET_NAME, url, S3Constants.URL_TIMEOUT_HOUR));
	}

	@Override
	public void updateParentComment(String newsId) {
		// 获取该微博最后一次评论的是否是家长
		List<NewsCommentInfo> list = newsCommentInfoMapper.getListByNewsId(newsId);
		if (ListUtil.isEmpty(list)) {
			newsInfoMapper.setParentComment(newsId, false);
			return;
		}
		List<RoleInfo> roleList = roleInfoMapper.getRoleListByAccountId(list.get(0).getCommentAccountId());
		if (containRoles(Role.Parent, roleList) == 1) {
			newsInfoMapper.setParentComment(newsId, true);
		} else {
			newsInfoMapper.setParentComment(newsId, false);
		}
	}
}
