package com.aoyuntek.aoyun.service.task;

import java.util.Date;

import com.aoyuntek.aoyun.dao.task.TaskInfoMapper;
import com.aoyuntek.aoyun.entity.po.task.TaskInfo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.task.TaskInfoVo;
import com.aoyuntek.aoyun.service.IBaseWebService;
import com.theone.common.util.ServiceResult;

/**
 * 
 * taskCore
 * 
 * @author dlli5 at 2016年9月20日下午5:34:31
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public interface ITaskCoreService extends IBaseWebService<TaskInfo, TaskInfoMapper> {

    /**
     * 
     * @author dlli5 at 2016年10月9日下午4:08:14
     * @param day
     * @param taskInfoVo
     * @param currentId
     * @return
     * @throws Exception
     */
    public ServiceResult<Object> dealInitTaskInstance(Date day,TaskInfoVo taskInfoVo,  UserInfoVo currentId) throws Exception;
    
    /**
     * 
     * @author dlli5 at 2016年10月9日下午4:08:18
     * @param taskInstanceId
     * @param accountId
     * @return
     */
    public ServiceResult<Object> dealTaskResponsibleLog(String taskInstanceId,String accountId);
    
    /**
     * 
     * @author dlli5 at 2016年10月9日下午4:08:22
     * @param taskInfoVo
     * @param accountId
     * @param centreId
     * @param roomId
     * @param obj_id
     * @param day
     * @return
     * @throws Exception
     */
    public ServiceResult<Object> dealTaskInstance(TaskInfoVo taskInfoVo, String accountId,String centreId, String roomId, String obj_id,Date day)throws Exception ;
}
