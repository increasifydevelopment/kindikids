package com.aoyuntek.aoyun.service;

import java.util.List;

import com.aoyuntek.aoyun.condtion.MessageCondition;
import com.aoyuntek.aoyun.dao.MessageInfoMapper;
import com.aoyuntek.aoyun.entity.po.MessageInfo;
import com.aoyuntek.aoyun.entity.vo.MessageInfoVo;
import com.aoyuntek.aoyun.entity.vo.MessageListVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.framework.model.Pager;
import com.theone.common.util.ServiceResult;

public interface IMessageInfoService extends IBaseWebService<MessageInfo, MessageInfoMapper> {
    /**
     * 
     * @description 获取收件箱及发件箱所有MSG
     * @author bbq
     * @create 2016年6月24日下午1:50:10
     * @version 1.0
     * @param currentUser
     *            当前用户
     * @param condition
     *            站内信查询条件
     * @return 返回操作结果
     */
    ServiceResult<Pager<MessageListVo, MessageCondition>> getPagerMsgs(UserInfoVo currentUser, MessageCondition condition);

    /**
     * @description 发送Message
     * @author hxzhang
     * @create 2016年6月23日下午8:26:02
     * @version 1.0
     * @param messageCondition
     *            messageInfo实体
     * @param nsqVersion
     *            NQS信息
     * @param currentUser
     *            当前登录用户
     * @return 返回操作结果
     */
    ServiceResult<Object> addMessage(MessageCondition messageCondition, UserInfoVo currentUser);

    /**
     * @description 获取MessageInfo
     * @author hxzhang
     * @create 2016年6月24日上午9:12:53
     * @version 1.0
     * @param messageId
     *            Message ID
     * @param currentUser
     *            当前登录用户
     * @return 返回操作结果
     */
    ServiceResult<List<MessageInfoVo>> dealMessageInfo(String messageId, UserInfoVo currentUser);

    /**
     * 
     * @description 删除MSG
     * @author bbq
     * @create 2016年6月27日下午1:50:11
     * @version 1.0
     * @param msgId
     *            信息id
     * @param isInbox
     *            收件箱 or 发件箱
     * @return 删除结果
     */
    ServiceResult<Integer> removeByMsgId(String msgId, boolean isInbox);

    /**
     * 
     * @description 获取未读消息数量
     * @author bbq
     * @create 2016年6月27日下午2:35:47
     * @version 1.0
     * @param accountId
     *            当前用户Id
     * @return 未读消息数量
     */
    int getUnreadCount(String accountId);
    
    ServiceResult<Object> updateForward(List<String> accountIds,String msgId);

}
