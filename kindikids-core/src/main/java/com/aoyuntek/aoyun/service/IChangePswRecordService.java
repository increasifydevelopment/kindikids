package com.aoyuntek.aoyun.service;

import com.aoyuntek.aoyun.dao.ChangePswRecordInfoMapper;
import com.aoyuntek.aoyun.entity.po.ChangePswRecordInfo;

public interface IChangePswRecordService extends IBaseWebService<ChangePswRecordInfo, ChangePswRecordInfoMapper> {

}
