package com.aoyuntek.aoyun.service.dashboard.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.condtion.dashboard.DashboardCondition;
import com.aoyuntek.aoyun.condtion.dashboard.SelecterCondition;
import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.NewsInfoMapper;
import com.aoyuntek.aoyun.dao.NewsReceiveInfoMapper;
import com.aoyuntek.aoyun.dao.NqsInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.dao.dashboard.DashboardMapper;
import com.aoyuntek.aoyun.entity.po.NqsInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.vo.RoleInfoVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.dashboard.DashboardBaseDataVo;
import com.aoyuntek.aoyun.entity.vo.dashboard.DashboardDataVo;
import com.aoyuntek.aoyun.entity.vo.dashboard.DashboardGroup;
import com.aoyuntek.aoyun.entity.vo.dashboard.DashboardVo;
import com.aoyuntek.aoyun.entity.vo.dashboard.NqsDashboardVo;
import com.aoyuntek.aoyun.entity.vo.task.PersonSelectVo;
import com.aoyuntek.aoyun.enums.NqsTag;
import com.aoyuntek.aoyun.enums.Score;
import com.aoyuntek.aoyun.enums.TaskCategory;
import com.aoyuntek.aoyun.enums.dashboard.Activities;
import com.aoyuntek.aoyun.enums.dashboard.SelectType;
import com.aoyuntek.aoyun.enums.dashboard.TasksCategories;
import com.aoyuntek.aoyun.enums.dashboard.TasksProgress;
import com.aoyuntek.aoyun.service.dashboard.IDashboardService;
import com.aoyuntek.aoyun.service.impl.BaseWebServiceImpl;
import com.theone.common.util.ServiceResult;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

@Service
public class DashboardServiceImpl extends BaseWebServiceImpl<UserInfo, UserInfoMapper> implements IDashboardService {

    @Autowired
    private DashboardMapper dashboardMapper;
    @Autowired
    private NqsInfoMapper nqsInfoMapper;
    @Autowired
    private CentersInfoMapper centersInfoMapper;
    @Autowired
    private NewsReceiveInfoMapper newsReceiveInfoMapper;
    @Autowired
    private NewsInfoMapper newsInfoMapper;

    @Override
    public List<DashboardGroup> getSelectList(UserInfoVo currtenUser, String p) {
        // 0小孩，1员工，2园区，3room，4 organisation
        List<RoleInfoVo> roleList = currtenUser.getRoleInfoVoList();
        SelecterCondition condition = new SelecterCondition(p, roleList);
        List<DashboardGroup> result = new ArrayList<DashboardGroup>();
        // 1.获取兼职

        List<PersonSelectVo> staffListNoCenter = dashboardMapper.getSelectStaffList(condition);
        // 2.添加一个公共的Organisation
        /*
         * PersonSelectVo org = new PersonSelectVo(); org.setType((short) 4);
         * org.setText("Organisation"); org.setId("Organisation");
         * staffListNoCenter.add(0,org);
         */
        result.add(new DashboardGroup(null, staffListNoCenter));
        // 获取园区
        List<PersonSelectVo> centerList = dashboardMapper.getCenterList(condition);
        List<DashboardGroup> centerRoomList = dashboardMapper.getRoomList(condition);
        // 获取园区对应测room
        condition.setRoleType(1);
        for (PersonSelectVo center : centerList) {
            String centerId = center.getId();
            String centerName = center.getText();
            for (DashboardGroup item : centerRoomList) {
                // 表示为同一园区
                if (centerName.equals(item.getText())) {
                    item.getChildren().add(0, center);
                    condition.setCenterId(centerId);
                    // 存放园区下的小孩
                    item.getChildren().addAll(dashboardMapper.getSelectStaffList(condition));
                    item.getChildren().addAll(dashboardMapper.getChildList(condition));
                }
            }
        }
        result.addAll(centerRoomList);
        keyWordSearch(p, result);
        return result;
    }

    private void keyWordSearch(String p, List<DashboardGroup> result) {
        if (StringUtil.isEmpty(p)) {
            return;
        }
        Iterator<DashboardGroup> it = result.iterator();
        p = p.toLowerCase();
        while (it.hasNext()) {
            DashboardGroup select = it.next();
            String text = select.getText();
            List<PersonSelectVo> children = select.getChildren();
            // 筛选一遍子集
            removeItem(p, children);
            // 如果子集为空，根据父级筛选
            if (ListUtil.isEmpty(children) && StringUtil.isNotEmpty(text) && !text.toLowerCase().contains(p)) {
                it.remove();
            }
        }
    }

    private void removeItem(String p, List<PersonSelectVo> children) {
        Iterator<PersonSelectVo> it = children.iterator();
        while (it.hasNext()) {
            PersonSelectVo select = it.next();
            if (!select.getText().toLowerCase().contains(p.toLowerCase())) {
                it.remove();
            }
        }
    }

    @Override
    public ServiceResult<Object> getTaskCategories(DashboardCondition c) {
        // 获取已完成Task的总数量
        DashboardBaseDataVo vo = new DashboardBaseDataVo();
        int total = dashboardMapper.getCompleteTaskTotal(c.getStartDate(), c.getEndDate());
        List<PersonSelectVo> chooseOrgs = c.getChooseOrgs();
        List<DashboardVo> dvList = new ArrayList<DashboardVo>();
        // 循环TasksCategories枚举,获取每种Task并封装成对象
        for (TasksCategories tc : TasksCategories.values()) {
            DashboardVo dv = new DashboardVo();
            dv.setType(tc.getValue());
            List<DashboardDataVo> dataList = new ArrayList<DashboardDataVo>();
            // 循环前台用户选择的条件,拼接sql进行查询
            for (PersonSelectVo p : chooseOrgs) {
                DashboardDataVo ddv = new DashboardDataVo();
                // 获取每种Task数量
                double count = getCountCategories(c, p, tc);
                // center或room下平均task数量
                count = getStaffCount(p, c, count, tc.getValue());
                ddv.setTotalCount(new BigDecimal(count).setScale(2, BigDecimal.ROUND_HALF_UP));
                double percent = total == 0 ? 0 : count / total;
                ddv.setPercent(new BigDecimal(percent * 100).setScale(0, BigDecimal.ROUND_HALF_UP));
                dataList.add(ddv);
            }
            dv.setDataList(dataList);
            dvList.add(dv);
        }
        vo.setTaskCategories(dvList);
        vo.setTcMaxCount(getMaxCount(dvList));
        return successResult((Object) vo);
    }

    /**
     * @description 获取完成task平均值
     * @author hxzhang
     * @create 2016年11月14日下午4:04:47
     */
    private double getStaffCount(PersonSelectVo p, DashboardCondition c, double count, short type) {
        if (p.getType() != SelectType.room.getValue() && p.getType() != SelectType.center.getValue()) {
            return count;
        }
        int staffCount = dashboardMapper.getStaffCount(p.getId(), c.getStartDate(), c.getEndDate(), p.getType());
        count = staffCount == 0 ? 0 : count / staffCount;
        return count;
    }

    /**
     * @description 获取最大值
     * @author hxzhang
     * @create 2016年11月14日下午2:42:56
     */
    private BigDecimal getMaxCount(List<DashboardVo> dvList) {
        BigDecimal maxCount = new BigDecimal(0);
        for (DashboardVo dashboardVo : dvList) {
            for (DashboardDataVo dashboardDataVo : dashboardVo.getDataList()) {
                maxCount = maxCount.compareTo(dashboardDataVo.getTotalCount()) == -1 ? dashboardDataVo.getTotalCount() : maxCount;
            }
        }
        return maxCount;
    }

    /**
     * @description 获取每种Task数量
     * @author hxzhang
     * @create 2016年11月10日上午9:49:33
     */
    private int getCountCategories(DashboardCondition c, PersonSelectVo p, TasksCategories tc) {
        short taskType = -1;
        String selectId = p.getId();
        short selectType = p.getType();
        if (tc.getValue() == TasksCategories.CentreTasks.getValue()) {
            taskType = TaskCategory.CENTER.getValue();
        } else if (tc.getValue() == TasksCategories.RoomTasks.getValue()) {
            taskType = TaskCategory.ROOM.getValue();
        } else if (tc.getValue() == TasksCategories.ChildrenTasks.getValue()) {
            taskType = TaskCategory.CHILDRRENN.getValue();
        } else {
            taskType = TaskCategory.STAFF.getValue();
        }

        return dashboardMapper.getCount(c.getStartDate(), c.getEndDate(), selectId, tc.getValue(), taskType, selectType);
    }

    @Override
    public Object getNqs(DashboardCondition condition) {
        List<PersonSelectVo> chooseOrg = condition.getChooseOrgs();
        List<List<NqsDashboardVo>> nqsList = new ArrayList<List<NqsDashboardVo>>();
        // 遍历选择的组织信息
        List<Integer> countList = new ArrayList<Integer>();
        for (PersonSelectVo item : chooseOrg) {
            String itemId = item.getId();
            // 组织类型，0小孩，1员工，2园区，3room，4所有
            short itemType = item.getType();
            // SelectType typeEnum = SelectType.getType(itemType);
            condition.setChooseId(itemId);
            condition.setType(itemType);
            List<NqsDashboardVo> list = dashboardMapper.getNqsStatistics(condition);
            int totalCount=  getTotalCount(list);
            //int totalCount = dashboardMapper.getNqsStatisticsCount(condition);
            nqsList.add(handleNqs(list, totalCount, countList));
        }
        // 获取最大值
        int maxCount = Collections.max(countList);
        return new DashboardBaseDataVo(handleData(nqsList), maxCount, condition);
    }
    
    private int getTotalCount( List<NqsDashboardVo> list){
        int count=0;
        for (NqsDashboardVo nqsDashboardVo : list) {
            count+=nqsDashboardVo.getCount();
        }
        return count;
    }

    /**
     * 
     * @description
     * @author gfwang
     * @create 2016年11月14日下午8:10:32
     * @version 1.0
     * @param result
     * @return
     */
    private Object handleData(List<List<NqsDashboardVo>> result) {
        List<Object> obj = new ArrayList<Object>();
        // 获取所有根节点
        List<NqsInfo> roots = dashboardMapper.getRootNqs(NqsTag.V2.getValue());
        for (NqsInfo node : roots) {
            String version = node.getVersion();
            String content = node.getContent();
            obj.add(new NqsDashboardVo(version, content, getVoList(version, result)));
        }
        return obj;
    }

    private List<NqsDashboardVo> getVoList(String rootNode, List<List<NqsDashboardVo>> result) {
        List<NqsDashboardVo> tempList = new ArrayList<NqsDashboardVo>();
        for (List<NqsDashboardVo> itemList : result) {
            for (NqsDashboardVo vo : itemList) {
                String nodeName = vo.getRootNode();
                if (!rootNode.equals(nodeName)) {
                    continue;
                }
                tempList.add(vo);
            }
        }
        return tempList;
    }

    /**
     * 
     * @description 组装数据+计算
     * @author gfwang
     * @create 2016年11月14日下午8:09:15
     * @version 1.0
     * @param list
     * @param totalCount
     * @return
     */
    private List<NqsDashboardVo> handleNqs(List<NqsDashboardVo> list, int totalCount, List<Integer> countList) {
        List<NqsInfo> roots = dashboardMapper.getRootNqs(NqsTag.V2.getValue());
        List<NqsDashboardVo> result = new ArrayList<NqsDashboardVo>();
        for (NqsInfo node : roots) {
            String version = node.getVersion();
            String content = node.getContent();
            ServiceResult<NqsDashboardVo> itemResult = getItemByVersion(version, content, list);
            NqsDashboardVo item = itemResult.getReturnObj();
            int singleCount = item.getCount();
            // 计算最大值
            countList.add(singleCount);
            if (!itemResult.isSuccess()) {
                result.add(new NqsDashboardVo(version, content, singleCount, totalCount, new BigDecimal(0)));
            } else {
                result.add(new NqsDashboardVo(version, content, singleCount, totalCount, divide(singleCount, totalCount)));
            }
        }
        return result;
    }

    /**
     * 
     * @description 除法运算
     * @author gfwang
     * @create 2016年11月14日下午8:09:36
     * @version 1.0
     * @param singleCount
     * @param totalCount
     * @return
     */
    private BigDecimal divide(int singleCount, int totalCount) {
        BigDecimal result = new BigDecimal(singleCount).divide(new BigDecimal(totalCount), 2, RoundingMode.HALF_UP);
        return result.multiply(new BigDecimal(100));
    }

    /**
     * 
     * @description 根据版本号获取 元素
     * @author gfwang
     * @create 2016年11月14日下午8:10:05
     * @version 1.0
     * @param version
     * @param content
     * @param list
     * @return
     */
    private ServiceResult<NqsDashboardVo> getItemByVersion(String version, String content, List<NqsDashboardVo> list) {
        for (NqsDashboardVo item : list) {
            if (version.equals(item.getRootNode())) {
                return successResult(item);
            }
        }
        return failResult2(new NqsDashboardVo(version, content, 0, 0, new BigDecimal(0)));
    }

    /**
     * 获取Activities
     */
    @Override
    public DashboardBaseDataVo getActivitiesList(UserInfoVo currentUser, DashboardCondition condition) {
        DashboardBaseDataVo vo = new DashboardBaseDataVo();
        log.info("getActivitiesList|start");
        List<DashboardVo> dashboardVoList = new ArrayList<DashboardVo>();
        // 循环活动列表，分别获取每项活动的DashboardVo
        for (Activities activity : Activities.values()) {
            DashboardVo dashboardVo = getActivityDashboardVo(activity.getValue(), condition);
            dashboardVoList.add(dashboardVo);
        }
        vo.setActivityList(dashboardVoList);
        vo.setAcMaxCount(getMaxCount(dashboardVoList));
        log.info("getActivitiesList|end");
        return vo;
    }

    /**
     * 
     * @description 获取activities的DashboardVo
     * @author mingwang
     * @create 2016年11月11日下午5:19:03
     * @version 1.0
     * @param activityType
     * @param condition
     * @return
     */
    private DashboardVo getActivityDashboardVo(short activityType, DashboardCondition condition) {
        log.info("getActivitiesList|getDashboardVo|start|activityType=" + activityType);
        DashboardVo dashboardVo = new DashboardVo();
        // observation，interest..的type
        dashboardVo.setType(activityType);
        List<DashboardDataVo> dataList = new ArrayList<DashboardDataVo>();
        // 循环selected items（选择的center、room或者人）
        for (PersonSelectVo selectVo : condition.getChooseOrgs()) {
            int totalCount = dashboardMapper.getActivitiesTotalCount(selectVo.getType(), selectVo.getId(), condition.getStartDate(), condition.getEndDate());
            // 小孩，center，room的activity总数量
            // 小孩，center，room的单个activity数量
            int activitiesCount = dashboardMapper.getActivitiesCount(activityType, selectVo.getType(), selectVo.getId(), condition.getStartDate(), condition.getEndDate());
            log.info("getDataList|totalCount=" + totalCount + "---activitiesCount=" + activitiesCount);
            // 组织类型，0小孩，1员工，2园区，3room，4所有
            DashboardDataVo activityDataVo = getDashboardDataVo(activitiesCount, totalCount, selectVo, condition, (short) -1);// -1这里是为了ACTIVES不排除员工
            dataList.add(activityDataVo);
        }
        dashboardVo.setDataList(dataList);
        return dashboardVo;
    }

    /**
     * 获取QIP
     */
    @Override
    public ServiceResult<Object> getQip(DashboardCondition condition) {
        DashboardBaseDataVo vo = new DashboardBaseDataVo();
        List<DashboardVo> qipList = new ArrayList<DashboardVo>();
        // 循环评分列表（1-5）
        for (Score score : Score.values()) {
            DashboardVo qipData = getQipDashboardVo(condition, score.getValue());
            qipList.add(qipData);
        }
        vo.setQipList(qipList);
        vo.setQipMaxCount(getMaxCount(qipList));
        log.info("QIPMaxCount=" + getMaxCount(qipList));
        return successResult((Object) vo);
    }

    /**
     * 
     * @description 获取QIP的DashboardVo
     * @author mingwang
     * @create 2016年11月16日上午9:46:27
     * @version 1.0
     * @param condition
     * @param score
     * @return
     */
    private DashboardVo getQipDashboardVo(DashboardCondition condition, short score) {
        DashboardVo temp = new DashboardVo();
        // 评分（1,2,3,4,5）
        temp.setType(score);
        List<DashboardDataVo> qipDataList = new ArrayList<DashboardDataVo>();
        // 循环selected items（选择的center、room或者人）
        for (PersonSelectVo selectVo : condition.getChooseOrgs()) {
            int scoreCount = dashboardMapper.getScoreCount(condition, selectVo.getId(), selectVo.getType(), score);
            // 员工，center，room的score（1,2,3,4,5）总数量
            int totalCount = dashboardMapper.getTotalCount(condition, selectVo.getId(), selectVo.getType(), score);
            log.info("scoreCount=" + scoreCount + "---|" + score + "----|totalCount=" + totalCount);
            DashboardDataVo qipDataVo = getDashboardDataVo(scoreCount, totalCount, selectVo, condition, SelectType.child.getValue());
            qipDataList.add(qipDataVo);
        }
        temp.setDataList(qipDataList);
        return temp;

    }

    /**
     * 
     * @description 获取activities、QIP的DashboardDataVo
     * @author mingwang
     * @create 2016年11月16日上午9:46:53
     * @version 1.0
     * @param singleCount
     * @param totalCount
     * @param selectVo
     * @param condition
     * @param exclusionType
     * @return
     */
    private DashboardDataVo getDashboardDataVo(int singleCount, int totalCount, PersonSelectVo selectVo, DashboardCondition condition, short exclusionType) {
        DashboardDataVo returnDataVo = new DashboardDataVo(new BigDecimal(0), new BigDecimal(0));
        // 如果totalCount、singleCount为0，
        // exclusionType：排除的type（activity排除员工、QIP排除小孩），直接返回0
        if (totalCount != 0 && singleCount != 0 && selectVo.getType() != exclusionType) {
            // 单个的占比
            // BigDecimal percent_bd = new BigDecimal(singleCount).divide(new
            // BigDecimal(totalCount), 2, RoundingMode.HALF_UP);
            BigDecimal percent_bd = divide(singleCount, totalCount);
            // 单个数量转换成BigDecimal
            BigDecimal singleCount_bd = new BigDecimal(singleCount);
            // 如果是center，room，Organisation，计算平均值
            if (selectVo.getType() == SelectType.center.getValue() || selectVo.getType() == SelectType.room.getValue() || selectVo.getType() == SelectType.organisation.getValue()) {
                int personCount;
                // 如果排除的是小孩，计算员工数量；如果排除的是员工，计算小孩的数量
                if (exclusionType == SelectType.child.getValue()) {
                    // 获取数据库中的center/room/all员工数量
                    personCount = dashboardMapper.getStaffCount(selectVo.getId(), condition.getStartDate(), condition.getEndDate(), selectVo.getType());
                } else {
                    // 获取数据库中的center/room/all小孩数量
                    personCount = dashboardMapper.getChildCount(selectVo.getId(), selectVo.getType(), condition.getStartDate(), condition.getEndDate());
                }
                log.info("personCount=" + personCount);
                // singleCount平均值
                if (personCount != 0) {
                    singleCount_bd = new BigDecimal(singleCount).divide(new BigDecimal(personCount), 2, RoundingMode.HALF_UP);
                } else {
                    singleCount_bd = new BigDecimal(0);
                }
            }
            returnDataVo = new DashboardDataVo(percent_bd, singleCount_bd);
        }
        return returnDataVo;
    }

    // by sjwang
    public DashboardBaseDataVo _getQip(DashboardCondition c, DashboardBaseDataVo vo) {

        int k = 0;
        List<DashboardVo> temp = null;

        List<PersonSelectVo> chooseOrgs = c.getChooseOrgs();
        int size = chooseOrgs.size();
        BigDecimal maxCount = new BigDecimal(0);
        List<DashboardVo> vos = getOrig(size);

        for (PersonSelectVo SelectVo : chooseOrgs) {

            if (SelectVo.type == 1) {

                temp = dashboardMapper.getStaffList(c, SelectVo.getId());

            } else if (SelectVo.type == 2) {

                temp = dashboardMapper.getCentersList(c, SelectVo.getId());

            } else if (SelectVo.type == 3) {

                temp = dashboardMapper.getRoomVoList(c, SelectVo.getId());
            } else if (SelectVo.type == 4) {

                temp = dashboardMapper.getOrgnizeList(c, SelectVo.getId());
            }
            vos = match(vos, temp, k++);

        }
        List<BigDecimal> list = new ArrayList<BigDecimal>();
        for (DashboardVo dashboardVo : vos) {
            List<DashboardDataVo> dataList = dashboardVo.getDataList();
            for (DashboardDataVo dashboardDataVo : dataList) {
                list.add(dashboardDataVo.getTotalCount());
            }

        }

        maxCount = Collections.max(list);

        vo.setQipList(vos);

        vo.setQipMaxCount(maxCount);

        return vo;
    }

    /**
     * 
     * @description 获得原始的List<DashboardVo>
     * @author sjwang
     * @create 2016年11月11日上午11:20:19
     * @version 1.0
     * @param size
     * @return
     */
    private List<DashboardVo> getOrig(int size) {
        List<DashboardVo> vos = new ArrayList<DashboardVo>();
        for (int i = 0; i < 5; i++) {
            DashboardVo dv = new DashboardVo();
            List<DashboardDataVo> dataList = new ArrayList<DashboardDataVo>();
            for (int j = 0; j < size; j++) {
                DashboardDataVo date = new DashboardDataVo();
                date.setPercent(new BigDecimal(0));
                date.setTotalCount(new BigDecimal(0));
                dataList.add(date);
            }

            dv.setDataList(dataList);
            vos.add(dv);

        }
        return vos;
    }

    /**
     * 
     * @description 根据需要改变List<DashboardVo>
     * @author sjwang
     * @create 2016年11月11日上午11:22:09
     * @version 1.0
     * @param vos
     * @param temp
     * @param j
     */
    private List<DashboardVo> match(List<DashboardVo> vos, List<DashboardVo> temp, int j) {
        BigDecimal percent;
        BigDecimal totalCount;
        int n = 0;
        for (Score s : Score.values()) {
            for (int i = 0; i < temp.size(); i++) {
                if (ListUtil.isEmpty(temp.get(i).getDataList())) {
                    continue;
                }
                if (s.getValue() == temp.get(i).getType()) {
                    percent = temp.get(i).getDataList().get(0).getPercent();
                    totalCount = temp.get(i).getDataList().get(0).getTotalCount();
                    vos.get(n).setType(temp.get(i).getType());
                    vos.get(n).getDataList().get(j).setPercent(percent);
                    vos.get(n).getDataList().get(j).setTotalCount(totalCount);
                }
            }
            n++;
        }
        return vos;
    }

    @Override
    public ServiceResult<Object> getTaskProgress(DashboardCondition c) {
        DashboardBaseDataVo vo = new DashboardBaseDataVo();
        // 获取完成,逾期完成,过期的task总数
        int total = dashboardMapper.getCompleteOverDueTaskTotal(c.getStartDate(), c.getEndDate());
        List<PersonSelectVo> chooseOrgs = c.getChooseOrgs();
        List<DashboardVo> dvList = new ArrayList<DashboardVo>();
        for (TasksProgress tp : TasksProgress.values()) {
            DashboardVo dv = new DashboardVo();
            dv.setType(tp.getValue());
            List<DashboardDataVo> ddvList = new ArrayList<DashboardDataVo>();
            for (PersonSelectVo p : chooseOrgs) {
                DashboardDataVo ddv = new DashboardDataVo();
                // 获取OnTime,LateCompletion,Overdue各个的数量
                int count = dashboardMapper.getProgressCount(c.getStartDate(), c.getEndDate(), tp.getValue(), p.getType(), p.getId());
                // 如果查的是Center,Room则需要查询出其所包含的员工
                double totalCount = getStaffCount(p, c, count, p.getType());
                ddv.setTotalCount(new BigDecimal(totalCount).setScale(2, BigDecimal.ROUND_HALF_UP));
                double percent = total == 0 ? 0 : totalCount / total;
                ddv.setPercent(new BigDecimal(percent * 100).setScale(0, BigDecimal.ROUND_HALF_UP));
                ddvList.add(ddv);
                dv.setDataList(ddvList);
            }
            dvList.add(dv);
            vo.setTaskProgress(dvList);
            vo.setTpMaxCount(getMaxCount(dvList));
        }
        return successResult((Object) vo);
    }
}
