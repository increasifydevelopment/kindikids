package com.aoyuntek.aoyun.service.program.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.aoyuntek.aoyun.dao.AttachmentInfoMapper;
import com.aoyuntek.aoyun.dao.ChildAttendanceInfoMapper;
import com.aoyuntek.aoyun.dao.ClosurePeriodMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.dao.YelfInfoMapper;
import com.aoyuntek.aoyun.dao.program.ChildEylfInfoMapper;
import com.aoyuntek.aoyun.dao.program.EylfCheckInfoMapper;
import com.aoyuntek.aoyun.dao.program.ProgramIntobsleaInfoMapper;
import com.aoyuntek.aoyun.dao.program.TaskFollowupEylfInfoMapper;
import com.aoyuntek.aoyun.dao.program.TaskProgramFollowUpInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskInstanceInfoMapper;
import com.aoyuntek.aoyun.entity.po.ChildAttendanceInfo;
import com.aoyuntek.aoyun.entity.po.ClosurePeriod;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.po.program.ChildEylfInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramIntobsleaInfo;
import com.aoyuntek.aoyun.entity.po.program.TaskFollowupEylfInfo;
import com.aoyuntek.aoyun.entity.po.program.TaskProgramFollowUpInfo;
import com.aoyuntek.aoyun.entity.vo.ChildEylfVo;
import com.aoyuntek.aoyun.entity.vo.EylfInfoVo;
import com.aoyuntek.aoyun.entity.vo.FileVo;
import com.aoyuntek.aoyun.entity.vo.FollowUpVo;
import com.aoyuntek.aoyun.entity.vo.FollowupEylfVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.EylfCheckValue;
import com.aoyuntek.aoyun.enums.ProgramState;
import com.aoyuntek.aoyun.enums.TaskType;
import com.aoyuntek.aoyun.service.impl.BaseWebServiceImpl;
import com.aoyuntek.aoyun.service.program.IProgramLessonService;
import com.aoyuntek.aoyun.service.program.ITaskProgramFollowUpService;
import com.aoyuntek.aoyun.service.task.ITaskInstance;
import com.theone.common.util.ServiceResult;
import com.theone.date.util.DateUtil;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

@Service
public class TaskProgramFollowUpServiceImpl extends BaseWebServiceImpl<TaskProgramFollowUpInfo, TaskProgramFollowUpInfoMapper> implements ITaskProgramFollowUpService {
	private static Logger log = Logger.getLogger(ProgramTaskExgrscServiceImpl.class);
	@Autowired
	private ProgramIntobsleaInfoMapper programIntobsleaInfoMapper;
	@Autowired
	private YelfInfoMapper yelfInfoMapper;
	@Autowired
	private EylfCheckInfoMapper eylfCheckInfoMapper;
	@Autowired
	private UserInfoMapper userInfoMapper;
	@Autowired
	private TaskProgramFollowUpInfoMapper taskProgramFollowUpInfoMapper;
	@Autowired
	private TaskFollowupEylfInfoMapper taskFollowupEylfInfoMapper;
	@Autowired
	private ChildEylfInfoMapper childEylfInfoMapper;
	@Autowired
	private AttachmentInfoMapper attachmentInfoMapper;
	@Autowired
	private IProgramLessonService programLessonService;
	@Autowired
	private ITaskInstance taskInstance;
	@Autowired
	private TaskInstanceInfoMapper taskInstanceInfoMapper;
	@Autowired
	private ClosurePeriodMapper closurePeriodMapper;
	@Autowired
	private ChildAttendanceInfoMapper childAttendanceInfoMapper;

	@Override
	public ServiceResult<Object> getTaskProgramFollowUp(String id) {
		FollowUpVo followUpVo = new FollowUpVo();
		// 获取followup信息
		TaskProgramFollowUpInfo followUpInfo = taskProgramFollowUpInfoMapper.selectByPrimaryKey(id);
		// 获取Interest,Observation,Learning Story信息
		ProgramIntobsleaInfo intobsleaInfo = programIntobsleaInfoMapper.selectByPrimaryKey(followUpInfo.getTaskId());
		intobsleaInfo.setTemp1(taskInstanceInfoMapper.getByValueId(intobsleaInfo.getId()).getId());
		;
		// 获取小孩信息
		UserInfo child = userInfoMapper.getUserInfoByAccountId(intobsleaInfo.getChildId());
		// 获取Followup中已选择的eylf集合
		List<FollowupEylfVo> followupEylfVoList = yelfInfoMapper.getFollowupEylfVoByFollowUpId(followUpInfo.getId());
		// 获取Followup check信息,(未选择过的,通过小孩生日直接获取,更新过直接找生成的镜像)
		List<ChildEylfVo> childEylfVoList = eylfCheckInfoMapper.getFollowupEylfcheckLog(intobsleaInfo.getChildId(), getAge(child, intobsleaInfo), false);
		if (ListUtil.isEmpty(childEylfVoList)) {
			// 获取上个阶段未完成的eylf check信息
			childEylfVoList = eylfCheckInfoMapper.getFollowupEylfcheckLog(intobsleaInfo.getChildId(), getAge(child, intobsleaInfo) - 1, true);
			// 合并本阶段的
			childEylfVoList.addAll(eylfCheckInfoMapper.getFollowupEylfcheckByAge(getAge(child, intobsleaInfo)));
		}
		// 获取图片信息
		List<FileVo> fileList = attachmentInfoMapper.getFileListVo(followUpInfo.getId());

		// 处理eylf
		List<EylfInfoVo> eylfList = getAllEylf(followupEylfVoList, childEylfVoList);

		followUpVo.setIntobsleaInfo(intobsleaInfo);
		followUpVo.setFollowUpInfo(followUpInfo);
		followUpVo.setFileList(fileList);
		followUpVo.setEylfList(eylfList);
		followUpVo.setChild(child);

		return successResult((Object) followUpVo);
	}

	private List<EylfInfoVo> getAllEylf(List<FollowupEylfVo> eylfClickList, List<ChildEylfVo> eylfChcekList) {
		List<EylfInfoVo> rootNodes = yelfInfoMapper.getRootNode();
		for (int i = 0; i < rootNodes.size(); i++) {
			String rootVersion = rootNodes.get(i).getVersion();
			List<FollowupEylfVo> resultEylfCilckList = new ArrayList<FollowupEylfVo>();
			List<ChildEylfVo> resultEylfChcekList = new ArrayList<ChildEylfVo>();
			for (FollowupEylfVo ec : eylfClickList) {
				if (rootVersion.equals(ec.getParentNode())) {
					resultEylfCilckList.add(ec);
				}
			}
			for (ChildEylfVo ce : eylfChcekList) {
				if (rootVersion.equals(ce.getParentNode())) {
					resultEylfChcekList.add(ce);
				}
			}
			rootNodes.get(i).setEylfCilckList(resultEylfCilckList);
			rootNodes.get(i).setEylfCheckList(resultEylfChcekList);
		}
		return rootNodes;
	}

	/**
	 * @description 计算小孩的年龄
	 * @author hxzhang
	 * @create 2016年9月21日下午4:52:17
	 * @version 1.0
	 * @param birthday
	 * @return
	 */
	private int getAge(UserInfo child, ProgramIntobsleaInfo p) {
		Date now = p.getEvaluationDate();
		Date birthday = child.getBirthday();
		int age = DateUtil.getCalendar(now).get(Calendar.YEAR) - DateUtil.getCalendar(birthday).get(Calendar.YEAR) - 1;
		if (DateUtil.getCalendar(now).get(Calendar.MONTH) > DateUtil.getCalendar(birthday).get(Calendar.MONTH)
				|| (DateUtil.getCalendar(now).get(Calendar.MONTH) == DateUtil.getCalendar(birthday).get(Calendar.MONTH)
						&& DateUtil.getCalendar(now).get(Calendar.DAY_OF_MONTH) > DateUtil.getCalendar(birthday).get(Calendar.DAY_OF_MONTH))) {
			age = age + 1;
		}
		return age < 0 ? 0 : age;
	}

	@Override
	public ServiceResult<Object> addOrUpdateTaskProgramFollowUp(FollowUpVo followUpVo, UserInfoVo currentUser) {
		ProgramIntobsleaInfo intobsleaInfo = programIntobsleaInfoMapper.selectByPrimaryKey(followUpVo.getIntobsleaInfo().getId());
		TaskProgramFollowUpInfo followUpInfo = followUpVo.getFollowUpInfo();
		List<FollowupEylfVo> followupEylfVoList = new ArrayList<FollowupEylfVo>();
		List<ChildEylfVo> childEylfVoList = new ArrayList<ChildEylfVo>();
		List<EylfInfoVo> eylfList = followUpVo.getEylfList();
		// 验证
		ServiceResult<Object> result = validateFollowUp(intobsleaInfo, followUpInfo, eylfList, followupEylfVoList, childEylfVoList);
		if (!result.isSuccess()) {
			return result;
		}
		log.info("addOrUpdateTaskProgramFollowUp>>>start");
		List<FileVo> fileList = followUpVo.getFileList();
		UserInfo child = userInfoMapper.getUserInfoByAccountId(intobsleaInfo.getChildId());
		// 新增或更新
		boolean isAdd = initOrUpdateTaskProgramFollowUpInfo(intobsleaInfo, followUpInfo, followUpVo.getIntobsleaInfo().getClassDate(), currentUser);
		// 保存Eylf信息
		saveFollowupEylf(followupEylfVoList, followUpInfo.getId());
		if (!isAdd) {
			saveChildEylf(childEylfVoList, intobsleaInfo.getChildId(), getAge(child, intobsleaInfo));
		}
		// 保存上传的附件
		try {
			programLessonService.initImages(followUpInfo.getId(), fileList);
		} catch (Exception e) {
			e.printStackTrace();
			log.info("Upload attachments failed" + e);
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
			return failResult(getTipMsg("operation_failed"));
		}
		log.info("addOrUpdateTaskProgramFollowUp>>>start");
		return successResult((Object) followUpVo);
	}

	/**
	 * @description followup验证
	 * @author Hxzhang
	 * @create 2016年9月28日上午8:54:33
	 */
	private ServiceResult<Object> validateFollowUp(ProgramIntobsleaInfo i, TaskProgramFollowUpInfo f, List<EylfInfoVo> eylfList, List<FollowupEylfVo> followupEylfVoList,
			List<ChildEylfVo> childEylfVoList) {
		// 1.在生成followup的日期只能存在一条
		if (StringUtil.isEmpty(f.getId())) {
			f = taskProgramFollowUpInfoMapper.getTaskProgramFollowUpInfoByTaskId(i.getId());
			if (null != f) {
				return failResult(getTipMsg("task_save_exists"));
			}
		}
		// 2.未来时间不能编辑
		if (null != f && StringUtil.isNotEmpty(f.getId()) && i.getEvaluationDate().after(new Date())) {
			return failResult(getTipMsg("task.save.TaskFollowUpInfo.FutureDate"));
		}
		// 3.孩子的能力选择了Not Assessed,Reason必填验证
		for (EylfInfoVo ee : eylfList) {
			List<ChildEylfVo> vos = ee.getEylfCheckList();
			for (ChildEylfVo eylfVo : vos) {
				if (eylfVo.getValue() != null && eylfVo.getValue() == EylfCheckValue.NotAssessed.getValue() && StringUtil.isEmpty(eylfVo.getReason())) {
					return failResult(getTipMsg("EYLF_Outcomes_Reason"));
				}
			}
			followupEylfVoList.addAll(ee.getEylfCilckList());
			childEylfVoList.addAll(vos);
		}
		return successResult();
	}

	/**
	 * @description 初始化或编辑TaskProgramFollowUpInfo
	 * @author hxzhang
	 * @create 2016年9月22日下午1:51:38
	 * @version 1.0
	 * @param followUpVo
	 * @param followUpType
	 * @param currentUser
	 * @return
	 */
	private boolean initOrUpdateTaskProgramFollowUpInfo(ProgramIntobsleaInfo intobsleaInfo, TaskProgramFollowUpInfo followUpInfo, Date classDate,
			UserInfoVo currentUser) {
		log.info("initOrUpdateTaskProgramFollowUpInfo>>>start");
		boolean isAdd = false;
		// 新增
		if (StringUtil.isEmpty(followUpInfo.getId())) {
			log.info("initTaskProgramFollowUpInfo>>>start");
			followUpInfo.setId(UUID.randomUUID().toString());
			followUpInfo.setTaskId(intobsleaInfo.getId());
			followUpInfo.setState(ProgramState.Pending.getValue());
			followUpInfo.setUpdateTime(new Date());
			followUpInfo.setDeleteFlag(DeleteFlag.Default.getValue());
			taskProgramFollowUpInfoMapper.insertSelective(followUpInfo);

			UserInfo child = userInfoMapper.getUserInfoByAccountId(intobsleaInfo.getChildId());
			Date day = classDate == null ? intobsleaInfo.getEvaluationDate() : classDate;
			taskInstance.addTaskInstance(getFollowupType(intobsleaInfo.getType()), intobsleaInfo.getCenterId(), intobsleaInfo.getRoomId(), child.getGroupId(),
					followUpInfo.getId(), followUpInfo.getCreateAccountId(), day);
			isAdd = true;
			log.info("initTaskProgramFollowUpInfo>>>end");
		} else { // 编辑
			log.info("updateTaskProgramFollowUpInfo>>>start");
			TaskProgramFollowUpInfo dbFollowUpInfo = taskProgramFollowUpInfoMapper.selectByPrimaryKey(followUpInfo.getId());
			dbFollowUpInfo.setObservation(followUpInfo.getObservation());
			dbFollowUpInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
			dbFollowUpInfo.setUpdateTime(new Date());
			taskProgramFollowUpInfoMapper.updateByPrimaryKey(dbFollowUpInfo);
			log.info("updateTaskProgramFollowUpInfo>>>end");
		}
		log.info("initOrUpdateTaskProgramFollowUpInfo>>>end");
		return isAdd;
	}

	/**
	 * @description 批量保存Eylf信息
	 * @author hxzhang
	 * @create 2016年9月22日下午2:10:12
	 * @version 1.0
	 * @param followupEylfVoList
	 * @param followUpId
	 */
	private void saveFollowupEylf(List<FollowupEylfVo> followupEylfVoList, String followUpId) {
		// 删除之前的记录
		taskFollowupEylfInfoMapper.removeFollowupEylfInfoByFollowupId(followUpId);
		List<TaskFollowupEylfInfo> tfeList = new ArrayList<TaskFollowupEylfInfo>();
		for (FollowupEylfVo fv : followupEylfVoList) {
			TaskFollowupEylfInfo tfe = new TaskFollowupEylfInfo();
			tfe.setId(UUID.randomUUID().toString());
			tfe.setEylfVersion(fv.getVersion());
			tfe.setFollowId(followUpId);
			tfe.setCheckFlag(fv.getCheckFlag());
			tfeList.add(tfe);
		}
		if (ListUtil.isNotEmpty(tfeList)) {
			taskFollowupEylfInfoMapper.batchInsertTaskFollowupEylfInfo(tfeList);
		}
	}

	/**
	 * @description 批量保存EylfCheck信息
	 * @author hxzhang
	 * @create 2016年9月22日下午2:25:06
	 * @version 1.0
	 * @param childEylfVoList
	 * @param child
	 */
	private void saveChildEylf(List<ChildEylfVo> childEylfVoList, String accountId, int age) {
		// 删除之前的记录
		childEylfInfoMapper.removeChildEylfInfoByAccountIdAge(accountId, age);
		List<ChildEylfInfo> ceList = new ArrayList<ChildEylfInfo>();
		for (ChildEylfVo cv : childEylfVoList) {
			ChildEylfInfo ce = new ChildEylfInfo();
			ce.setId(UUID.randomUUID().toString());
			ce.setChildId(accountId);
			ce.setAgeScope(age);
			ce.setEylfCheckId(cv.getId());
			ce.setReason(cv.getReason());
			ce.setValue(cv.getValue());
			ceList.add(ce);
		}
		if (ListUtil.isNotEmpty(ceList)) {
			childEylfInfoMapper.batchInsertChildEylfInfo(ceList);
		}
	}

	/**
	 * @description 获取followupType
	 * @author hxzhang
	 * @create 2016年9月25日下午2:10:18
	 * @version 1.0
	 * @return
	 */
	private TaskType getFollowupType(short type) {
		switch (type) {
		case 4:
			return TaskType.InterestFollowup;
		case 6:
			return TaskType.ObservationFollowup;
		case 8:
			return TaskType.LearningStoryFollowup;
		default:
			break;
		}
		return null;
	}

	@Override
	public void createProgramFollowupTimedTask(Date now) {
		log.info("createProgramFollowupTimedTask>>>start");
		List<ProgramIntobsleaInfo> pList = programIntobsleaInfoMapper.getCreateFollowupProgramIntobsleaList(now);
		log.info("createProgramFollowupTimedTask>>>ProgramIntobsleaInfo" + pList.size());
		for (ProgramIntobsleaInfo p : pList) {
			FollowUpVo followUpVo = new FollowUpVo();
			// 关园日延期生成Followup
			dealClosurePeriodFollowup(p, now);
			initEylfCilkAndChcek(followUpVo, p);
			addOrUpdateTaskProgramFollowUp(followUpVo, null);
		}
		log.info("createProgramFollowupTimedTask>>>end");
	}

	/**
	 * 关园日延期生成Followup
	 * 
	 * @author hxzhang 2018年11月30日
	 * @param p
	 */
	private void dealClosurePeriodFollowup(ProgramIntobsleaInfo p, Date day) {
		List<ClosurePeriod> list = closurePeriodMapper.getListByCentreIdDate(p.getCenterId(), day);
		if (ListUtil.isEmpty(list)) {
			return;
		}
		// 获取孩子的上课周期
		UserInfo userInfo = userInfoMapper.getUserInfoByAccountId(p.getChildId());
		if (userInfo == null) {
			return;
		}
		ChildAttendanceInfo info = childAttendanceInfoMapper.getChildAttendanceByUserId(userInfo.getId());
		p.setClassDate(nextClassDate(p, day, info, list));
	}

	private Date nextClassDate(ProgramIntobsleaInfo p, Date day, ChildAttendanceInfo info, List<ClosurePeriod> list) {
		boolean flag = true;
		while (flag) {
			day = DateUtil.addDay(day, 1);
			list = closurePeriodMapper.getListByCentreIdDate(p.getCenterId(), day);
			if (ListUtil.isNotEmpty(list)) {
				continue;
			}
			if (info.getMonday() && getWeekOfDay(day) == Calendar.MONDAY) {
				flag = false;
			} else if (info.getTuesday() && getWeekOfDay(day) == Calendar.TUESDAY) {
				flag = false;
			} else if (info.getWednesday() && getWeekOfDay(day) == Calendar.WEDNESDAY) {
				flag = false;
			} else if (info.getThursday() && getWeekOfDay(day) == Calendar.THURSDAY) {
				flag = false;
			} else if (info.getFriday() && getWeekOfDay(day) == Calendar.FRIDAY) {
				flag = false;
			}
		}
		return day;
	}

	private int getWeekOfDay(Date day) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(day);
		return cal.get(Calendar.DAY_OF_WEEK);
	}

	@Override
	public void updateOverDueProgramFollowupTimedTask(Date now) {
		log.info("updateOverDueProgramFollowupTimedTask>>>start");
		// 获取过期的ProgramFollowup
		List<String> overDueFollowupList = taskProgramFollowUpInfoMapper.getOverDueProgramFollowup(now);
		log.info("updateOverDueProgramFollowupTimedTask>>>overDueFollowupList:" + overDueFollowupList.size());
		// 将记录表中的记录更新为overDue
		int instanceCount = taskInstanceInfoMapper.updateStatuByValueIds(overDueFollowupList);
		log.info("updateOverDueProgramFollowupTimedTask>>>instanceCount:" + instanceCount);
		// 将过期的记录状态更新为overDue
		int followupCount = taskProgramFollowUpInfoMapper.updateOverDueProgramFollowup(now);
		log.info("updateOverDueProgramFollowupTimedTask>>>followupCount:" + followupCount);
	}

	/**
	 * @description 初始化Eylf的cilk和check
	 * @author Hxzhang
	 * @create 2016年9月26日下午12:29:41
	 */
	private FollowUpVo initEylfCilkAndChcek(FollowUpVo followUpVo, ProgramIntobsleaInfo p) {
		TaskProgramFollowUpInfo followUpInfo = new TaskProgramFollowUpInfo();
		UserInfo child = userInfoMapper.getUserInfoByAccountId(p.getChildId());
		// 获取eylf信息
		List<FollowupEylfVo> followupEylfVoList = yelfInfoMapper.getAllEylf();
		// ================= 新增followup时不固化小孩EylfCheck的能力 hxzhang
		// =================
		// 获取在该小孩年龄段的eylf check信息
		List<ChildEylfVo> childEylfVoList = new ArrayList<ChildEylfVo>();
		// childEylfVoList =
		// eylfCheckInfoMapper.getFollowupEylfcheckByAge(getAge(child, p));
		// 获取上个阶段未完成的eylf check信息
		// childEylfVoList.addAll(unChildEylfVoList);
		// 处理eylf
		List<EylfInfoVo> eylfList = getAllEylf(followupEylfVoList, childEylfVoList);

		followUpVo.setIntobsleaInfo(p);
		followUpVo.setFollowUpInfo(followUpInfo);

		followUpVo.setEylfList(eylfList);
		followUpVo.setChild(child);
		return followUpVo;
	}

	@Override
	public void createProgramFollowup(ProgramIntobsleaInfo p) {
		if (null == p.getEvaluationDate()) {
			return;
		}
		FollowUpVo followUpVo = new FollowUpVo();
		// 关园日延期生成Followup
		dealClosurePeriodFollowup(p, p.getEvaluationDate());
		initEylfCilkAndChcek(followUpVo, p);
		addOrUpdateTaskProgramFollowUp(followUpVo, null);
	}
}
