package com.aoyuntek.aoyun.service;

import java.util.Date;
import java.util.List;

import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.theone.common.util.ServiceResult;

/**
 * @description User业务逻辑层接口
 * @author bbq
 * @create 2016年6月17日下午18:25:30
 * @version 1.0
 */
public interface IUserInfoService extends IBaseWebService<UserInfo, UserInfoMapper> {

	boolean isFutureEnrolled(String familyId);

	/**
	 * @description 展示未签到提示
	 * @author hxzhang
	 * @create 2016年12月23日上午10:18:45
	 */
	ServiceResult<Object> showNotSignOutTip(String accountId);

	/**
	 * @description 用户登录
	 * @author bbq
	 * @create 2016年6月17日下午18:25:30
	 * @version 1.0
	 * @param account
	 *            账号
	 * @param passWord
	 *            密码
	 * @return 用户实体
	 */
	ServiceResult<UserInfoVo> saveLogin(String account, String passWord, String googleVlidateId);

	/**
	 * @description 修改用户密码
	 * @author bqb
	 * @create 2016年6月17日下午18:25:30
	 * @version 1.0
	 * @param token
	 *            修改密码的token
	 * @param newPwd
	 *            新密码
	 * @return 操作结果
	 */
	ServiceResult<UserInfoVo> dealChangePwd(String token, String newPwd);

	/**
	 * @description 发送验证信息通过邮箱
	 * @author bbq
	 * @create 2016年6月17日下午18:25:30
	 * @version 1.0
	 * @param mail
	 *            邮箱地址
	 * @return 操作结果
	 */
	ServiceResult<Object> dealChangePwdByMail(String mail);

	/**
	 * 
	 * @description 邮件校验
	 * @author gfwang
	 * @create 2016年8月1日下午2:47:49
	 * @version 1.0
	 * @param email
	 *            email
	 * @return
	 */
	boolean validateEmail(String email, String userId);

	List<UserInfo> validateEmail(String email);

	/**
	 * 
	 * @author dlli5 at 2016年8月29日上午10:45:07
	 * @param childId
	 * @return
	 */
	UserInfo getUserInfoByAccountId(String childId);

	/**
	 * 
	 * @author abliu at 2016年8月29日上午10:45:07
	 * @param date
	 *            当天时间
	 * @return
	 */
	void sendUserNewsFeedByBirthday(Date date);

	/**
	 * @description 取消发送激活邮件
	 * @author hxzhang
	 * @create 2016年8月30日下午3:48:02
	 * @version 1.0
	 * @return
	 */
	ServiceResult<Object> updateCancelSendActiveEmail(String id);

	/**
	 * @description 同意条款
	 * @author hxzhang
	 * @create 2016年10月21日下午4:33:51
	 */
	UserInfoVo saveAgree(UserInfoVo currentUser);
}
