package com.aoyuntek.aoyun.service.meeting;

import java.util.List;

import com.aoyuntek.aoyun.condtion.MeetingAgendaCondition;
import com.aoyuntek.aoyun.dao.meeting.MeetingAgendaInfoMapper;
import com.aoyuntek.aoyun.entity.po.meeting.AgendaTypeInfo;
import com.aoyuntek.aoyun.entity.po.meeting.MeetingAgendaInfo;
import com.aoyuntek.aoyun.entity.vo.NqsVo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.meeting.MeetingAgendaInfoVo;
import com.aoyuntek.aoyun.service.IBaseWebService;
import com.theone.common.util.ServiceResult;

/**
 * 
 * @description MeetingAgenda业务逻辑层
 * @author mingwang
 * @create 2016年10月13日上午10:02:32
 * @version 1.0
 */
public interface IMeetingAgendaService extends IBaseWebService<MeetingAgendaInfo, MeetingAgendaInfoMapper> {

    /**
     * 
     * @description 更新Meeting agendaType
     * @author mingwang
     * @create 2016年10月13日上午10:16:25
     * @version 1.0
     * @param agendaTypeList
     * @param currentUser
     * @return
     */
    ServiceResult<Object> updateAgendaType(List<AgendaTypeInfo> agendaTypeList, UserInfoVo currentUser);
    void saveNqsRelation(String objId, List<NqsVo> nqsList);
    /**
     * 
     * @description 获取哦agendaType list
     * @author mingwang
     * @create 2016年10月13日上午10:33:28
     * @version 1.0
     * @return
     */
    ServiceResult<Object> getAgendaType();

    /**
     * 
     * @description 获取meetingAgenda
     * @author mingwang
     * @create 2016年10月13日下午3:17:08
     * @version 1.0
     * @return
     */
    ServiceResult<Object> getMeetingAgenda(String id);

    /**
     * 
     * @description 新增/更新MeetingAgenda
     * @author mingwang
     * @create 2016年10月13日下午3:46:17
     * @version 1.0
     * @param meetingAgendaInfoVo
     * @param currentUser
     * @return
     */
    ServiceResult<Object> addAndUpdateMeetingAgenda(MeetingAgendaInfoVo meetingAgendaInfoVo, UserInfoVo currentUser);

    /**
     * 
     * @description 获取meetingAgenda列表
     * @author mingwang
     * @create 2016年10月14日上午10:11:28
     * @version 1.0
     * @param condition
     * @return
     */
    ServiceResult<Object> getMeetingAgendaList(MeetingAgendaCondition condition);

    /**
     * 
     * @description 验证MeetingAgendaName是否重复
     * @author mingwang
     * @create 2016年10月14日下午2:33:20
     * @version 1.0
     * @param agendaName
     * @param id
     * @return
     */
    boolean validateMeetingAgendaName(String agendaName, String id);

    /**
     * 
     * @description 修改MeetingAgendaState状态
     * @author mingwang
     * @create 2016年10月14日下午4:23:00
     * @version 1.0
     * @param agendaState
     * @param id
     * @return
     */
    ServiceResult<Object> updateAgendaState(short agendaState, String id);

    /**
     * 
     * @description 获取agenda列表
     * @author mingwang
     * @create 2016年10月17日下午2:05:35
     * @version 1.0
     * @return
     */
    List<SelecterPo> getAgendaList();

}
