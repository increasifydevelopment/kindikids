package com.aoyuntek.aoyun.service.roster.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.roster.RosterInfoMapper;
import com.aoyuntek.aoyun.dao.roster.RosterShiftInfoMapper;
import com.aoyuntek.aoyun.dao.roster.RosterShiftTaskInfoMapper;
import com.aoyuntek.aoyun.dao.roster.RosterShiftTimeInfoMapper;
import com.aoyuntek.aoyun.dao.roster.RosterStaffInfoMapper;
import com.aoyuntek.aoyun.dao.roster.ShiftWeekInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskValueInfoMapper;
import com.aoyuntek.aoyun.entity.po.CentersInfo;
import com.aoyuntek.aoyun.entity.po.roster.RosterShiftInfo;
import com.aoyuntek.aoyun.entity.po.roster.RosterShiftTaskInfo;
import com.aoyuntek.aoyun.entity.po.roster.RosterShiftTimeInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskInfo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.roster.RosterShiftTimeVo;
import com.aoyuntek.aoyun.entity.vo.roster.RosterShiftVo;
import com.aoyuntek.aoyun.entity.vo.roster.ShiftWeekVo;
import com.aoyuntek.aoyun.enums.ArchivedStatus;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.factory.RosterFactory;
import com.aoyuntek.aoyun.service.impl.BaseWebServiceImpl;
import com.aoyuntek.aoyun.service.roster.IRosterShiftService;
import com.theone.common.util.ServiceResult;
import com.theone.date.util.DateUtil;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

@Service
public class RosterShiftServiceImpl extends BaseWebServiceImpl<RosterShiftInfo, RosterShiftInfoMapper> implements IRosterShiftService {

    @Autowired
    private RosterShiftInfoMapper rosterShiftInfoMapper;
    @Autowired
    private RosterShiftTimeInfoMapper rosterShiftTimeInfoMapper;
    @Autowired
    private ShiftWeekInfoMapper shiftWeekInfoMapper;
    @Autowired
    private RosterShiftTaskInfoMapper rosterShiftTaskInfoMapper;
    @Autowired
    private RosterInfoMapper rosterInfoMapper;
    @Autowired
    private TaskInfoMapper taskInfoMapper;
    @Autowired
    private RosterStaffInfoMapper rosterStaffInfoMapper;
    @Autowired
    private TaskValueInfoMapper taskValueInfoMapper;
    @Autowired
    private CentersInfoMapper centersInfoMapper;
    @Autowired
    private RosterFactory rosterFactory;

    @Override
    public ServiceResult<Object> getShiftTasks(String centreId, UserInfoVo currentUser) {
        List<SelecterPo> result = new ArrayList<SelecterPo>();
        List<TaskInfo> taskInfos = taskInfoMapper.getShiftTasks(centreId);
        for (TaskInfo taskInfo : taskInfos) {
            SelecterPo _SelecterPo = new SelecterPo(taskInfo.getId(), taskInfo.getTaskName());
            result.add(_SelecterPo);
        }
        return successResult((Object) result);
    }

    private ServiceResult<RosterShiftVo> saveShift(RosterShiftVo vo, UserInfoVo currentUser, boolean isImage) {
        Date now = new Date();
        vo.setCreateAccountId(currentUser.getAccountInfo().getId());
        vo.setCreateTime(now);
        vo.setDeleteFlag(DeleteFlag.Default.getValue());
        vo.setId(UUID.randomUUID().toString());
        vo.setCurrentFlag(true);
        rosterShiftInfoMapper.insert(vo);
        for (RosterShiftTimeVo rosterShiftTimeInfo : vo.getRosterShiftTimeVos()) {
            rosterShiftTimeInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
            if (!isImage || StringUtil.isEmpty(rosterShiftTimeInfo.getCode())) {
                rosterShiftTimeInfo.setCode(UUID.randomUUID().toString());
            }
            rosterShiftTimeInfo.setCreateTime(now);
            rosterShiftTimeInfo.setDeleteFlag(DeleteFlag.Default.getValue());
            rosterShiftTimeInfo.setId(UUID.randomUUID().toString());
            rosterShiftTimeInfo.setShiftId(vo.getId());

            for (ShiftWeekVo shiftWeekVo : rosterShiftTimeInfo.getShiftWeekVos()) {
                shiftWeekVo.setId(UUID.randomUUID().toString());
                shiftWeekVo.setShiftTimeId(rosterShiftTimeInfo.getId());
                shiftWeekInfoMapper.insert(shiftWeekVo);
            }

            for (TaskInfo taskInfo : rosterShiftTimeInfo.getRosterShiftTaskVos()) {
                RosterShiftTaskInfo rosterShiftTaskInfo = new RosterShiftTaskInfo();
                rosterShiftTaskInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
                rosterShiftTaskInfo.setCreateTime(now);
                rosterShiftTaskInfo.setDeleteFlag(DeleteFlag.Default.getValue());
                rosterShiftTaskInfo.setShiftTimeId(rosterShiftTimeInfo.getId());
                rosterShiftTaskInfo.setTaskId(taskInfo.getId());
                rosterShiftTaskInfoMapper.insert(rosterShiftTaskInfo);
            }
            rosterShiftTimeInfoMapper.insert(rosterShiftTimeInfo);
        }
        return successResult(vo);
    }

    private ServiceResult<RosterShiftVo> updateShift(RosterShiftVo vo, UserInfoVo currentUser) {
        Date now = new Date();
        RosterShiftInfo old = rosterShiftInfoMapper.selectByPrimaryKey(vo.getId());
        old.setDeleteFlag(DeleteFlag.Default.getValue());
        old.setUpdateAccountId(currentUser.getAccountInfo().getId());
        old.setUpdateTime(now);
        rosterShiftInfoMapper.updateByPrimaryKey(old);
        List<String> shiftTimeIds = new ArrayList<String>();
        for (RosterShiftTimeVo rosterShiftTimeInfo : vo.getRosterShiftTimeVos()) {
            boolean shiftAdd = StringUtil.isEmpty(rosterShiftTimeInfo.getId());
            RosterShiftTimeInfo oldRosterShiftTimeInfo = null;
            boolean changeTime = false;
            if (shiftAdd) {
                rosterShiftTimeInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
                rosterShiftTimeInfo.setCode(UUID.randomUUID().toString());
                rosterShiftTimeInfo.setCreateTime(now);
                rosterShiftTimeInfo.setDeleteFlag(DeleteFlag.Default.getValue());
                rosterShiftTimeInfo.setId(UUID.randomUUID().toString());
                rosterShiftTimeInfo.setShiftId(vo.getId());
            } else {
                oldRosterShiftTimeInfo = rosterShiftTimeInfoMapper.selectByPrimaryKey(rosterShiftTimeInfo.getId());
                oldRosterShiftTimeInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
                oldRosterShiftTimeInfo.setUpdateTime(now);
                oldRosterShiftTimeInfo.setDeleteFlag(DeleteFlag.Default.getValue());
                oldRosterShiftTimeInfo.setShiftId(vo.getId());
                if (oldRosterShiftTimeInfo.getShiftStartTime().getTime() != rosterShiftTimeInfo.getShiftStartTime().getTime()
                        || oldRosterShiftTimeInfo.getShiftEndTime().getTime() != rosterShiftTimeInfo.getShiftEndTime().getTime()) {
                    changeTime = true;
                }
                oldRosterShiftTimeInfo.setShiftStartTime(rosterShiftTimeInfo.getShiftStartTime());
                oldRosterShiftTimeInfo.setShiftEndTime(rosterShiftTimeInfo.getShiftEndTime());
            }

            for (ShiftWeekVo shiftWeekVo : rosterShiftTimeInfo.getShiftWeekVos()) {
                boolean shiftWeekAdd = StringUtil.isEmpty(shiftWeekVo.getId());
                if (shiftWeekAdd) {
                    shiftWeekVo.setId(UUID.randomUUID().toString());
                }
                shiftWeekVo.setShiftTimeId(rosterShiftTimeInfo.getId());
                if (shiftWeekAdd) {
                    shiftWeekInfoMapper.insert(shiftWeekVo);
                } else {
                    shiftWeekInfoMapper.updateByPrimaryKey(shiftWeekVo);
                }
            }

            List<String> shiftTaskIds = new ArrayList<String>();
            for (TaskInfo taskInfo : rosterShiftTimeInfo.getRosterShiftTaskVos()) {
                boolean isAble = taskInfoMapper.isAble(taskInfo.getId()) > 0;
                if (!isAble) {
                    TaskInfo nameTask = taskInfoMapper.selectByPrimaryKey(taskInfo.getId());
                    TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                    return failResult(MessageFormat.format(getTipMsg("task.RosterShift.task.disable"), nameTask.getTaskName()));
                }

                // 如果这个下不存在 则添加
                if (rosterShiftTaskInfoMapper.haveTask(taskInfo.getId(), rosterShiftTimeInfo.getId()) == 0) {
                    RosterShiftTaskInfo rosterShiftTaskInfo = new RosterShiftTaskInfo();
                    rosterShiftTaskInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
                    rosterShiftTaskInfo.setCreateTime(now);
                    rosterShiftTaskInfo.setDeleteFlag(DeleteFlag.Default.getValue());
                    rosterShiftTaskInfo.setShiftTimeId(rosterShiftTimeInfo.getId());
                    rosterShiftTaskInfo.setTaskId(taskInfo.getId());
                    rosterShiftTaskInfoMapper.insert(rosterShiftTaskInfo);
                }
                shiftTaskIds.add(taskInfo.getId());
            }
            // 删除以外的
            rosterShiftTaskInfoMapper.deleteOther(rosterShiftTimeInfo.getId(), shiftTaskIds);

            if (shiftAdd) {
                rosterShiftTimeInfoMapper.insert(rosterShiftTimeInfo);
            } else {
                rosterShiftTimeInfoMapper.updateByPrimaryKey(oldRosterShiftTimeInfo);
                // 看看时间是否改变 然后进行处理
                if (changeTime) {
                    rosterFactory.dealChangeTimeOfCasual(oldRosterShiftTimeInfo);
                }
            }
            // 记录
            shiftTimeIds.add(rosterShiftTimeInfo.getId());
        }

        // 删除以外的shiftTimeIds对应的下面所有
        rosterShiftTaskInfoMapper.deleteByOtherShiftTimeId(old.getId(), shiftTimeIds);
        shiftWeekInfoMapper.deleteByOtherShiftTimeId(old.getId(), shiftTimeIds);

        // 删除意外的shiftTimeIds
        rosterShiftTimeInfoMapper.deleteOther(vo.getId(), shiftTimeIds);
        return successResult(getTipMsg("task.RosterShift.time.save.success"), vo);
    }

    @Override
    public ServiceResult<RosterShiftVo> saveOrUpdateShift(RosterShiftVo vo, UserInfoVo currentUser) {
        boolean isAdd = StringUtil.isEmpty(vo.getId());
        if (isAdd) {
            // 看看centre是否已经存在了
            RosterShiftVo rosterShiftVo = rosterShiftInfoMapper.selectVoByCentreId(vo.getCenterId());
            if (rosterShiftVo != null) {
                return failResult(-1, getTipMsg("Roster.createBlankRoster.centre.have.shift"));
            }
            saveShift(vo, currentUser, false);
        } else {
            Date dayOfWeek = DateUtil.getWeekStart(new Date());
            // 判断此shift是否已经被历史用过了，如果已经用过了 那么镜像
            int usedCount = rosterInfoMapper.haveUsed(vo.getId(), dayOfWeek);
            if (usedCount > 0) {
                String oldId = vo.getId();

                List<String> newTimeCode = new ArrayList<String>();
                if (ListUtil.isNotEmpty(vo.getRosterShiftTimeVos())) {
                    for (RosterShiftTimeVo _RosterShiftTimeVo : vo.getRosterShiftTimeVos()) {
                        if (StringUtil.isNotEmpty(_RosterShiftTimeVo.getCode())) {
                            newTimeCode.add(_RosterShiftTimeVo.getCode());
                        }
                    }
                }
                saveShift(vo, currentUser, true);
                // 更新新的roster
                // rosterInfoMapper.updateShiftId(oldId, vo.getId());
                // 更新老的当前标示为false
                RosterShiftInfo old = rosterShiftInfoMapper.selectByPrimaryKey(oldId);
                old.setCurrentFlag(false);
                rosterShiftInfoMapper.updateByPrimaryKey(old);
                // 修改本周未Approved和以后的的roster shiftid 为最新
                rosterInfoMapper.updateFutureShiftId(dayOfWeek, oldId, vo.getId());

                // 删除减去掉的shifttime 对应未来的安排
                List<String> deleteShiftTimeCode = new ArrayList<String>();
                // 获取老的shifttime
                RosterShiftVo oldVo = rosterShiftInfoMapper.selectVoById(oldId);
                if (ListUtil.isNotEmpty(oldVo.getRosterShiftTimeVos())) {
                    for (RosterShiftTimeVo _RosterShiftTimeVo : oldVo.getRosterShiftTimeVos()) {
                        if (!newTimeCode.contains(_RosterShiftTimeVo.getCode())) {
                            deleteShiftTimeCode.add(_RosterShiftTimeVo.getCode());
                        }
                    }
                }

                if (ListUtil.isNotEmpty(deleteShiftTimeCode)) {
                    rosterStaffInfoMapper.deleteFutureRosterStaff(deleteShiftTimeCode, vo.getId());
                }

                // add by big 将非id的setCurrentFlag 都改为false
                rosterShiftInfoMapper.updateOtherCurrentFlag(old.getCenterId(), vo.getId());

                return successResult(getTipMsg("task.RosterShift.time.save.success"), vo);
            } else {
                return updateShift(vo, currentUser);
            }
        }
        return successResult(getTipMsg("task.RosterShift.time.save.success"), vo);
    }

    @Override
    public ServiceResult<RosterShiftVo> getShiftVo(String centreId) {
        RosterShiftVo rosterShtfVo = rosterShiftInfoMapper.selectVoByCentreId(centreId);
        if (rosterShtfVo != null) {
            CentersInfo centersInfo = centersInfoMapper.selectByPrimaryKey(centreId);
            if (centersInfo.getStatus() == ArchivedStatus.Archived.getValue()) {
                return failResult(1, "", rosterShtfVo);
            }
            List<SelecterPo> list = (List<SelecterPo>) getShiftTasks(centreId, new UserInfoVo()).getReturnObj();
            List<RosterShiftTimeVo> rosterShiftTimeVos = rosterShtfVo.getRosterShiftTimeVos();
            for (RosterShiftTimeVo vo : rosterShiftTimeVos) {
                List<TaskInfo> taskInfos = vo.getRosterShiftTaskVos();
                for (TaskInfo info : taskInfos) {
                    for (SelecterPo po : list) {
                        if (StringUtil.isNotEmpty(info.getTaskName()) && info.getTaskName().equals(po.getText())) {
                            info.setId(po.getId());
                        }
                    }
                }
            }
        }
        return successResult(rosterShtfVo);
    }

    @Override
    public ServiceResult<RosterShiftVo> deleteShiftTime(String id, UserInfoVo currentUser) {
        int count = rosterStaffInfoMapper.haveStaff(id, DateUtil.getWeekStart(new Date()));
        if (count != 0) {
            return failResult(-1, getTipMsg("task.RosterShift.time.delete.havestaff"));
        }
        // ++++++++++++++++++++++++
        RosterShiftTimeInfo rosterShiftTimeInfo = rosterShiftTimeInfoMapper.selectByPrimaryKey(id);

        // 看本周或者之前有没有人用 如果有人用 那么镜像
        Date dayOfWeek = DateUtil.getWeekStart(new Date());
        // 判断此shift是否已经被历史用过了，如果已经用过了 那么镜像
        int usedCount = rosterInfoMapper.haveUsedOfShiftTime(rosterShiftTimeInfo.getShiftId(), dayOfWeek);
        if (usedCount > 0) {
            String oldId = rosterShiftTimeInfo.getShiftId();

            RosterShiftVo vo = rosterShiftInfoMapper.selectVoById(oldId);
            for (RosterShiftTimeVo rosterShiftTimeVo : vo.getRosterShiftTimeVos()) {
                if (rosterShiftTimeVo.getId().equals(rosterShiftTimeInfo.getId())) {
                    vo.getRosterShiftTimeVos().remove(rosterShiftTimeVo);
                    break;
                }
            }
            saveShift(vo, currentUser, true);
            // 更新新的roster
            // rosterInfoMapper.updateShiftId(oldId, vo.getId());
            // 更新老的当前标示为false
            RosterShiftInfo old = rosterShiftInfoMapper.selectByPrimaryKey(oldId);
            old.setCurrentFlag(false);
            rosterShiftInfoMapper.updateByPrimaryKey(old);
            // 删除未来shift中已经排班的
            rosterStaffInfoMapper.deleteByShiftTimeCode(DateUtil.addDay(dayOfWeek, 7), rosterShiftTimeInfo.getCode());
            // 修改本周未Approved和以后的的roster shiftid 为最新
            rosterInfoMapper.updateFutureShiftIdOfShiftTime(dayOfWeek, oldId, vo.getId());
            // add by big 将非id的setCurrentFlag 都改为false
            rosterShiftInfoMapper.updateOtherCurrentFlag(old.getCenterId(), vo.getId());
            // ++++++++++++++++++++++++
        } else {
            rosterShiftTimeInfo.setDeleteFlag(DeleteFlag.Delete.getValue());
            rosterShiftTimeInfoMapper.updateByPrimaryKey(rosterShiftTimeInfo);
            // 删除有安排
            rosterStaffInfoMapper.deleteByShiftTimeCode(null, rosterShiftTimeInfo.getCode());
        }
        return successResult(getTipMsg("task.RosterShift.time.delete.success"));
    }
}
