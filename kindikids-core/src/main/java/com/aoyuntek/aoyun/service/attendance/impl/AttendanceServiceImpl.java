package com.aoyuntek.aoyun.service.attendance.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.condtion.ApproveAddCondition;
import com.aoyuntek.aoyun.condtion.AttendanceCondtion;
import com.aoyuntek.aoyun.condtion.AttendanceManagementCondition;
import com.aoyuntek.aoyun.condtion.ExternalListCondition;
import com.aoyuntek.aoyun.condtion.TemporaryAttendanceCondition;
import com.aoyuntek.aoyun.constants.DateFormatterConstants;
import com.aoyuntek.aoyun.constants.TipMsgConstants;
import com.aoyuntek.aoyun.dao.AbsenteeRequestInfoMapper;
import com.aoyuntek.aoyun.dao.AccountInfoMapper;
import com.aoyuntek.aoyun.dao.AttendanceHistoryInfoMapper;
import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.ChangeAttendanceRequestInfoMapper;
import com.aoyuntek.aoyun.dao.ChildAttendanceInfoMapper;
import com.aoyuntek.aoyun.dao.ClosurePeriodMapper;
import com.aoyuntek.aoyun.dao.EmailMsgMapper;
import com.aoyuntek.aoyun.dao.InterimAttendanceMapper;
import com.aoyuntek.aoyun.dao.LeaveInfoMapper;
import com.aoyuntek.aoyun.dao.RequestLogInfoMapper;
import com.aoyuntek.aoyun.dao.RoomInfoMapper;
import com.aoyuntek.aoyun.dao.SigninInfoMapper;
import com.aoyuntek.aoyun.dao.SubtractedAttendanceMapper;
import com.aoyuntek.aoyun.dao.TemporaryAttendanceInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.dao.child.RoomChangeInfoMapper;
import com.aoyuntek.aoyun.dao.event.EventInfoMapper;
import com.aoyuntek.aoyun.entity.po.AbsenteeRequestInfo;
import com.aoyuntek.aoyun.entity.po.AbsenteeRequestInfoWithBLOBs;
import com.aoyuntek.aoyun.entity.po.AccountInfo;
import com.aoyuntek.aoyun.entity.po.AttendanceHistoryInfo;
import com.aoyuntek.aoyun.entity.po.CentersInfo;
import com.aoyuntek.aoyun.entity.po.ChangeAttendanceRequestInfo;
import com.aoyuntek.aoyun.entity.po.ChildAttendanceInfo;
import com.aoyuntek.aoyun.entity.po.EmailMsg;
import com.aoyuntek.aoyun.entity.po.InterimAttendance;
import com.aoyuntek.aoyun.entity.po.ReplaceChildAttendanceVo;
import com.aoyuntek.aoyun.entity.po.ReplaceChildVo;
import com.aoyuntek.aoyun.entity.po.RequestLogInfo;
import com.aoyuntek.aoyun.entity.po.RoomInfo;
import com.aoyuntek.aoyun.entity.po.SubtractedAttendance;
import com.aoyuntek.aoyun.entity.po.TemporaryAttendanceInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.po.child.RoomChangeInfo;
import com.aoyuntek.aoyun.entity.po.event.EventInfo;
import com.aoyuntek.aoyun.entity.vo.AttendanceChildVo;
import com.aoyuntek.aoyun.entity.vo.AttendanceDayVo;
import com.aoyuntek.aoyun.entity.vo.AttendanceReturnVo;
import com.aoyuntek.aoyun.entity.vo.ChildInfoVo;
import com.aoyuntek.aoyun.entity.vo.ExternalListVo;
import com.aoyuntek.aoyun.entity.vo.LeaveListVo;
import com.aoyuntek.aoyun.entity.vo.RequestLogVo;
import com.aoyuntek.aoyun.entity.vo.RoleInfoVo;
import com.aoyuntek.aoyun.entity.vo.SigninVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.child.AttendancePostion;
import com.aoyuntek.aoyun.entity.vo.child.HistoryVo;
import com.aoyuntek.aoyun.enums.ArchivedStatus;
import com.aoyuntek.aoyun.enums.AttendanceManagementStatus;
import com.aoyuntek.aoyun.enums.AttendanceManagementType;
import com.aoyuntek.aoyun.enums.AttendanceRequestType;
import com.aoyuntek.aoyun.enums.AttendanceType;
import com.aoyuntek.aoyun.enums.ChangeAttendanceRequestDayState;
import com.aoyuntek.aoyun.enums.ChangeAttendanceRequestState;
import com.aoyuntek.aoyun.enums.ChangeAttendanceRequestType;
import com.aoyuntek.aoyun.enums.ChildType;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.EmailType;
import com.aoyuntek.aoyun.enums.EnrolledState;
import com.aoyuntek.aoyun.enums.RequestLogType;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.enums.SubtractedType;
import com.aoyuntek.aoyun.enums.TemporaryType;
import com.aoyuntek.aoyun.enums.UserType;
import com.aoyuntek.aoyun.enums.child.RoomChangeOperate;
import com.aoyuntek.aoyun.factory.AttendanceFactory;
import com.aoyuntek.aoyun.factory.AttendanceHistoryFactory;
import com.aoyuntek.aoyun.factory.UserFactory;
import com.aoyuntek.aoyun.service.IFamilyService;
import com.aoyuntek.aoyun.service.attendance.IAttendanceCoreService;
import com.aoyuntek.aoyun.service.attendance.IAttendanceHistoryService;
import com.aoyuntek.aoyun.service.attendance.IAttendanceService;
import com.aoyuntek.aoyun.service.attendance.IRequestLogService;
import com.aoyuntek.aoyun.service.impl.BaseWebServiceImpl;
import com.aoyuntek.aoyun.uitl.TipMessageUtil;
import com.aoyuntek.framework.model.Pager;
import com.theone.common.util.ServiceResult;
import com.theone.date.util.DateUtil;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

@Service
public class AttendanceServiceImpl extends BaseWebServiceImpl<ChangeAttendanceRequestInfo, ChangeAttendanceRequestInfoMapper> implements IAttendanceService {

	@Autowired
	private UserInfoMapper userInfoMapper;
	@Autowired
	private ChangeAttendanceRequestInfoMapper changeAttendanceRequestInfoMapper;
	@Autowired
	private ChildAttendanceInfoMapper childAttendanceInfoMapper;
	@Autowired
	private AttendanceHistoryInfoMapper attendanceHistoryInfoMapper;
	@Autowired
	private CentersInfoMapper centersInfoMapper;
	@Autowired
	private AccountInfoMapper accountInfoMapper;
	@Autowired
	private RequestLogInfoMapper requestLogInfoMapper;
	@Autowired
	private RoomInfoMapper roomInfoMapper;
	@Autowired
	private SigninInfoMapper signinInfoMapper;
	@Autowired
	private TemporaryAttendanceInfoMapper temporaryAttendanceInfoMapper;
	@Autowired
	private AbsenteeRequestInfoMapper absenteeRequestInfoMapper;
	@Autowired
	private TipMessageUtil tipMessageUtil;
	@Autowired
	private IAttendanceCoreService attendanceCoreService;
	@Autowired
	private SubtractedAttendanceMapper subtractedAttendanceMapper;
	@Autowired
	private AttendanceFactory attendanceFactory;
	@Autowired
	private IAttendanceHistoryService attendanceHistoryService;
	@Autowired
	private LeaveInfoMapper leaveInfoMapper;
	@Autowired
	private EventInfoMapper eventInfoMapper;
	@Autowired
	private IFamilyService familyService;
	@Autowired
	private UserFactory userFactory;
	@Autowired
	private AttendanceHistoryFactory attendanceHistoryFactory;
	@Autowired
	private RoomChangeInfoMapper roomChangeInfoMapper;
	@Autowired
	private IRequestLogService requestLogService;
	@Autowired
	private EmailMsgMapper emailMsgMapper;
	@Autowired
	private ClosurePeriodMapper closurePeriodMapper;
	@Autowired
	private InterimAttendanceMapper interimAttendanceMapper;

	private Integer getOld(String accountId, Date beginDate, Date endDate) {
		return signinInfoMapper.getUserSignSize(accountId, beginDate, endDate);
	}

	private Integer getFuture(String accountId, Date beginDate, Date endDate) {
		Date now = new Date();
		int days = 0;
		ChildAttendanceInfo attendanceInfo = childAttendanceInfoMapper.selectByAccountId(accountId);
		UserInfo userInfo = userInfoMapper.getUserInfoByAccountId(accountId);
		AttendancePostion postion = new AttendancePostion(userInfo.getCentersId(), userInfo.getRoomId(), userInfo.getGroupId());
		postion.setMonday(attendanceInfo.getMonday());
		postion.setTuesday(attendanceInfo.getTuesday());
		postion.setWednesday(attendanceInfo.getWednesday());
		postion.setThursday(attendanceInfo.getThursday());
		postion.setFriday(attendanceInfo.getFriday());
		int daySize = com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(now, endDate);
		List<ChangeAttendanceRequestInfo> attendanceRequestInfos = changeAttendanceRequestInfoMapper.getListByChildAndDateScope(accountId, now, endDate);
		List<RoomChangeInfo> roomChangeInfos = roomChangeInfoMapper.getListByChild(accountId, now, endDate);

		for (int i = 0; i <= daySize; i++) {
			Date temp = DateUtil.addDay(now, i);

			if (ListUtil.isNotEmpty(attendanceRequestInfos)) {
				for (ChangeAttendanceRequestInfo re : attendanceRequestInfos) {
					if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(re.getChangeDate(), temp) != 0) {
						break;
					}
					AttendanceRequestType type = AttendanceRequestType.getType(re.getType());
					if (type == AttendanceRequestType.K) {
						continue;
					}
					postion.setRoomId(re.getNewRoomId());
					postion.setGroupId(re.getNewGroupId());
					if (type == AttendanceRequestType.C) {
						postion.setCentreId(re.getNewCentreId());
					}
				}
			}

			if (ListUtil.isNotEmpty(roomChangeInfos)) {
				for (RoomChangeInfo re : roomChangeInfos) {
					if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(re.getDay(), temp) != 0) {
						continue;
					}
					RoomChangeOperate operate = RoomChangeOperate.getType(re.getOperate());
					switch (operate) {
					case REMOVEALL: {
						attendanceFactory.dealAddOrRemove(postion, re, false);
					}
						break;
					case REMOVEDAY: {
						attendanceFactory.dealAddOrRemove(postion, re, false);
					}
						break;
					case ADDALL: {
						attendanceFactory.dealAddOrRemove(postion, re, true);
					}
						break;
					case ADDDAY: {
						attendanceFactory.dealAddOrRemove(postion, re, true);
					}
						break;
					}
				}
			}

			if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(beginDate, temp) >= 0) {
				if (attendanceFactory.haveAttendanceOfDay(postion, temp)) {
					days++;
				}
			}
			if (ListUtil.isNotEmpty(roomChangeInfos)) {
				for (RoomChangeInfo re : roomChangeInfos) {
					if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(re.getDay(), temp) != 0) {
						continue;
					}
					RoomChangeOperate operate = RoomChangeOperate.getType(re.getOperate());
					switch (operate) {
					case REMOVEALL: {
					}
						break;
					case REMOVEDAY: {
						attendanceFactory.dealAddOrRemove(postion, re, true);
					}
						break;
					case ADDALL: {
					}
						break;
					case ADDDAY: {
						attendanceFactory.dealAddOrRemove(postion, re, false);
					}
						break;
					}
				}
			}
		}
		return days;
	}

	@Override
	public List<AttendanceHistoryInfo> getChangeCentreHistory(int intervalDay, Date day) {
		List<AttendanceHistoryInfo> historyInfos = attendanceHistoryInfoMapper.getChangeCentreHistory(intervalDay, day);
		if (ListUtil.isEmpty(historyInfos)) {
			return new ArrayList<AttendanceHistoryInfo>();
		}
		List<AttendanceHistoryInfo> list = new ArrayList<AttendanceHistoryInfo>();
		for (AttendanceHistoryInfo attendanceHistoryInfo : historyInfos) {
			// 看有没有转园
			// 看是不是第一次
			AttendanceHistoryInfo upInfo = attendanceHistoryInfoMapper.getUpHistory(attendanceHistoryInfo.getChildId(), attendanceHistoryInfo.getCreateTime());
			if (upInfo != null && !upInfo.getCenterId().equals(attendanceHistoryInfo.getCenterId())) {
				list.add(upInfo);
			}
		}
		return list;
	}

	@Override
	public ServiceResult<Object> dealSubtractedAttendance(UserInfoVo currentUser, String roomId, String childId, Date day, int type, boolean cover, boolean interim) {
		RoomInfo room = roomInfoMapper.selectByPrimaryKey(roomId);

		boolean isCentreManager = (containRole(Role.CentreManager, currentUser.getRoleInfoVoList()) == 1)
				|| (containRole(Role.EducatorSecondInCharge, currentUser.getRoleInfoVoList()) == 1);

		// 自己院长只能操作自己园的
		if (isCentreManager) {
			boolean iscentreAdmin = attendanceCoreService.isCenterManager(currentUser, room.getCentersId(), isCentreManager);
			if (!iscentreAdmin) {
				return failResult(-1, getTipMsg("no.permissions"));
			}
		}

		int dayOfWeek = DateUtil.getDayOfWeek(day) - 1;
		SubtractedAttendance subtractedAttendance = new SubtractedAttendance();
		subtractedAttendance.setBeginTime(day);

		// 自定义临时排课取消
		if (interim) {
			if ((short) type == SubtractedType.DAY.getValue()) {
				// 只删除当天的
				interimAttendanceMapper.removeForDay(childId, day);
			} else {
				List<InterimAttendance> list = interimAttendanceMapper.getNowAndFutureList(childId, day);
				for (InterimAttendance i : list) {
					if (DateUtil.getDayOfWeek(day) == DateUtil.getDayOfWeek(i.getDay())) {
						interimAttendanceMapper.removeForDay(childId, i.getDay());
					}
				}
			}
			return successResult();
		}

		if ((short) type == SubtractedType.DAY.getValue()) {
			// 如果这一天本身就是被零时加上去的 那么取消
			List<RoomChangeInfo> changeInfos = roomChangeInfoMapper.getListByRoomAndDay(roomId, childId, day, RoomChangeOperate.ADDDAY.getValue());
			if (ListUtil.isNotEmpty(changeInfos)) {
				return failResult(-1, getTipMsg("attendance.subtractedAttendance.day.child.have.addday"));
			}

			subtractedAttendance.setEndTime(day);
			subtractedAttendance.setCentreId(room.getCentersId());
			subtractedAttendance.setChildId(childId);
			subtractedAttendance.setCreateAccountId(currentUser.getAccountInfo().getId());
			subtractedAttendance.setCreateTime(new Date());
			subtractedAttendance.setDayWeek((short) dayOfWeek);
			subtractedAttendance.setDeleteFlag(DeleteFlag.Default.getValue());
			subtractedAttendance.setId(UUID.randomUUID().toString());
			subtractedAttendance.setRoomId(roomId);
			subtractedAttendance.setUpdateAccountId(currentUser.getAccountInfo().getId());
			subtractedAttendance.setUpdateTime(new Date());
			subtractedAttendanceMapper.insert(subtractedAttendance);

			RoomChangeInfo roomChangeInfo = new RoomChangeInfo();
			roomChangeInfo.setCentreId(room.getCentersId());
			roomChangeInfo.setChildId(childId);
			roomChangeInfo.setDay(day);
			roomChangeInfo.setDayWeek((short) dayOfWeek);
			roomChangeInfo.setOperate(RoomChangeOperate.REMOVEDAY.getValue());
			roomChangeInfo.setReqId(subtractedAttendance.getId());
			roomChangeInfo.setRoomId(roomId);
			roomChangeInfoMapper.insert(roomChangeInfo);

			requestLogService.dealLog(currentUser, subtractedAttendance);
		} else {
			subtractedAttendance.setCentreId(room.getCentersId());
			subtractedAttendance.setChildId(childId);
			subtractedAttendance.setCreateAccountId(currentUser.getAccountInfo().getId());
			subtractedAttendance.setCreateTime(new Date());
			subtractedAttendance.setDayWeek((short) dayOfWeek);
			subtractedAttendance.setDeleteFlag(DeleteFlag.Default.getValue());
			subtractedAttendance.setId(UUID.randomUUID().toString());
			subtractedAttendance.setRoomId(roomId);
			subtractedAttendance.setUpdateAccountId(currentUser.getAccountInfo().getId());
			subtractedAttendance.setUpdateTime(new Date());

			boolean removeFlag = false;
			// 如果减去的是今天 那么判断是否是原本的上课 如果是那么取消勾选
			if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(day, new Date()) == 0) {
				UserInfo userInfo = userInfoMapper.getUserInfoByAccountId(childId);
				ChildAttendanceInfo attendanceInfo = childAttendanceInfoMapper.selectByUserId(userInfo.getId());
				boolean have = attendanceFactory.dealSubtractedAttendance(attendanceInfo, day);
				if (have) {
					childAttendanceInfoMapper.updateByPrimaryKeySelective(attendanceInfo);
					// 伪造为了传递参数
					ChangeAttendanceRequestInfo attendanceRequestInfo = new ChangeAttendanceRequestInfo();
					attendanceRequestInfo.setChildId(childId);
					attendanceRequestInfo.setChangeDate(new Date());
					attendanceRequestInfo.setNewCentreId(userInfo.getCentersId());
					attendanceRequestInfo.setNewRoomId(userInfo.getRoomId());
					attendanceFactory.dealAttendanceRequest(attendanceRequestInfo, attendanceInfo);
					// 这里记录历史
					attendanceHistoryService.dealAttendanceHistory(attendanceRequestInfo, currentUser);
				} else {
					removeFlag = true;
				}
				subtractedAttendance.setDeleteFlag(DeleteFlag.Delete.getValue());
			} else {
				removeFlag = true;
			}
			if (removeFlag) {
				RoomChangeInfo roomChangeInfo = new RoomChangeInfo();
				roomChangeInfo.setCentreId(room.getCentersId());
				roomChangeInfo.setChildId(childId);
				roomChangeInfo.setDay(day);
				roomChangeInfo.setDayWeek((short) dayOfWeek);
				roomChangeInfo.setOperate(RoomChangeOperate.REMOVEALL.getValue());
				roomChangeInfo.setReqId(subtractedAttendance.getId());
				roomChangeInfo.setRoomId(roomId);
				roomChangeInfoMapper.insert(roomChangeInfo);
			}
			subtractedAttendanceMapper.insert(subtractedAttendance);
			requestLogService.dealLog(currentUser, subtractedAttendance);
			minusAttends(dayOfWeek, childId, currentUser);
		}
		return successResult();
	}

	private void minusAttends(int dayOfWeek, String childId, UserInfoVo currentUser) {
		List<ChangeAttendanceRequestInfo> changeAttendanceRequestInfos = changeAttendanceRequestInfoMapper.getListByChildAndState(childId,
				ChangeAttendanceRequestState.Default.getValue());
		if (ListUtil.isEmpty(changeAttendanceRequestInfos)) {
			changeAttendanceRequestInfos = changeAttendanceRequestInfoMapper.getListByChildAndState(childId, ChangeAttendanceRequestState.PartialAproved.getValue());
		}
		if (ListUtil.isEmpty(changeAttendanceRequestInfos)) {
			return;
		}
		ChangeAttendanceRequestInfo requestInfo = changeAttendanceRequestInfos.get(0);
		requestInfo.setDayOfWeek(dayOfWeek);
		if (attendanceFactory.isDealAll(requestInfo)) {
			requestInfo.setState(ChangeAttendanceRequestState.Over.getValue());
		} else {
			requestInfo.setState(ChangeAttendanceRequestState.PartialAproved.getValue());
		}
		requestInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
		requestInfo.setUpdateTime(new Date());

		switch (dayOfWeek) {
		case 1:
			requestInfo.setMonday(ChangeAttendanceRequestDayState.UnCheck.getValue());
			break;
		case 2:
			requestInfo.setTuesday(ChangeAttendanceRequestDayState.UnCheck.getValue());
			break;
		case 3:
			requestInfo.setWednesday(ChangeAttendanceRequestDayState.UnCheck.getValue());
			break;
		case 4:
			requestInfo.setThursday(ChangeAttendanceRequestDayState.UnCheck.getValue());
			break;
		case 5:
			requestInfo.setFriday(ChangeAttendanceRequestDayState.UnCheck.getValue());
			break;
		}
		changeAttendanceRequestInfoMapper.updateByPrimaryKey(requestInfo);
		// 发送管理员减去小孩邮件
		ChildAttendanceInfo attendanceInfo = childAttendanceInfoMapper.selectByAccountId(childId);
		attendanceInfo.setNewMonday(requestInfo.getMonday() == ChangeAttendanceRequestDayState.Check.getValue());
		attendanceInfo.setNewTuesday(requestInfo.getTuesday() == ChangeAttendanceRequestDayState.Check.getValue());
		attendanceInfo.setNewWednesday(requestInfo.getWednesday() == ChangeAttendanceRequestDayState.Check.getValue());
		attendanceInfo.setNewThursday(requestInfo.getThursday() == ChangeAttendanceRequestDayState.Check.getValue());
		attendanceInfo.setNewFriday(requestInfo.getFriday() == ChangeAttendanceRequestDayState.Check.getValue());
		attendanceInfo.setRequestId(requestInfo.getId());

		String title = getSystemEmailMsg("approve_attendance_title");
		String content = getSystemEmailMsg("approve_attendance_content");
		userFactory.sendParentEmailByChild(title, content, childId, true, EmailType.approve_attendance.getValue(), null, null, attendanceInfo, attendanceInfo.isPlan());
	}

	private ServiceResult<ChildAttendanceInfo> validateApproveAdd(UserInfoVo currentUser, CentersInfo centersInfo, RoomInfo roomInfo, ChildAttendanceInfo attendanceInfo,
			ApproveAddCondition condtion) {
		boolean isCentreManager = (containRole(Role.CentreManager, currentUser.getRoleInfoVoList()) == 1)
				|| (containRole(Role.EducatorSecondInCharge, currentUser.getRoleInfoVoList()) == 1);
		int dayOfWeek = DateUtil.getDayOfWeek(condtion.getDay()) - 1;

		// 自己院长只能操作自己园的
		if (isCentreManager) {
			boolean iscentreAdmin = attendanceCoreService.isCenterManager(currentUser, centersInfo.getId(), isCentreManager);
			if (!iscentreAdmin) {
				return failResult(-1, getTipMsg("no.permissions"));
			}
		}

		// 如果是外部 或者内部未入园的 那么判断是否有其他期望周期
		boolean isExternalOrEnrolledStateDefault = (attendanceInfo.getType() == ChildType.External.getValue())
				|| (attendanceInfo.getEnrolled() == EnrolledState.Default.getValue());

		if (!isExternalOrEnrolledStateDefault) {
			boolean isFull = attendanceFactory.futureRoomFull(centersInfo.getId(), roomInfo.getId(), condtion.getDay(), (short) dayOfWeek);

			if (isFull && (!condtion.isAddForMax())) {
				return failResult(1, MessageFormat.format(getTipMsg("child.attendance.approveAdd.over"), TipMsgConstants.WEEKITEMS[dayOfWeek - 1]));
			}
		}

		if (isExternalOrEnrolledStateDefault) {
			// 那么提示 是否安排其他
			if (attendanceInfo.checkDay()) {
				if (!condtion.isAlertOther()) {
					return failResult(2, tipMessageUtil.getAlertForAttendance(attendanceInfo, (short) dayOfWeek));
				}

				if (condtion.isDealAll() && !condtion.isAddForMaxAll()) {
					List<Short> dayOfWeeks = attendanceFactory.getChangeDayOfAttendance(attendanceInfo);
					StringBuilder sb = new StringBuilder();
					for (Short tempDayOfWeeks : dayOfWeeks) {
						boolean isFull = attendanceFactory.futureRoomFull(centersInfo.getId(), roomInfo.getId(), condtion.getDay(), tempDayOfWeeks);
						if (isFull && (!condtion.isAddForMax())) {
							sb.append(TipMsgConstants.WEEKNAMES[tempDayOfWeeks - 1] + ",");
						}
					}
					if (StringUtil.isNotEmpty(sb.toString())) {
						return failResult(1, MessageFormat.format(getTipMsg("child.attendance.approveAdd.over"), StringUtil.trimEnd(sb.toString(), ",")));
					}
				} else if (!condtion.isDealAll() && !condtion.isAddForMax()) {
					boolean isFull = attendanceFactory.futureRoomFull(centersInfo.getId(), roomInfo.getId(), condtion.getDay(), (short) dayOfWeek);
					if (isFull && (!condtion.isAddForMax())) {
						return failResult(1, MessageFormat.format(getTipMsg("child.attendance.approveAdd.over"), TipMsgConstants.WEEKITEMS[dayOfWeek - 1]));
					}
				}
			}
		} else {
			// // 如果是外部以及内部未入园的 那么需要判断是否存在未来的安排
			// ServiceResult<ChildAttendanceInfo> result =
			// attendanceCoreService.validateRequestOfJob(condtion.getChildId(),
			// condtion.isCoverOld(),
			// ChangeAttendanceRequestType.AddAttendance);
			// // 参与基本验证
			// if (!result.isSuccess()) {
			// return result;
			// }

			// 这里要判断是否有临时 和永久的
			TemporaryAttendanceCondition temporaryAttendanceCondition = new TemporaryAttendanceCondition();
			temporaryAttendanceCondition.setBeginTime(condtion.getDay());
			temporaryAttendanceCondition.setChildId(condtion.getChildId());
			temporaryAttendanceCondition.setDayOfWeek((short) dayOfWeek);
			temporaryAttendanceCondition.setRoomId(condtion.getRoomId());
			List<TemporaryAttendanceInfo> haveTemporarys = temporaryAttendanceInfoMapper.getList(temporaryAttendanceCondition);
			if (!condtion.isCoverOld() && haveTemporarys.size() > 0) {
				return failResult(3, getTipMsg("submitChangeAttendance.dealApproveReplace.contain"));
			}
			for (TemporaryAttendanceInfo temporaryAttendanceInfo : haveTemporarys) {
				temporaryAttendanceInfo.setDeleteFlag(DeleteFlag.Delete.getValue());
				temporaryAttendanceInfoMapper.updateByPrimaryKey(temporaryAttendanceInfo);
			}
		}

		// 临时排课前先查询该小孩是否有排课
		if (condtion.isInterim() && !condtion.isConfirm()) {
			if (havaAttendance(centersInfo.getId(), condtion.getDay(), condtion.getChildId())) {
				return failResult(6, getTipMsg("add.interim.attendance.error"));
			}
			return failResult(5, getTipMsg("add.interim.attendance.confirm"));
		}

		return successResult();
	}

	@Override
	public ServiceResult<ChildAttendanceInfo> dealApproveAdd(UserInfoVo currentUser, ApproveAddCondition condtion) {
		// 查询room信息
		RoomInfo roomInfo = roomInfoMapper.selectByPrimaryKey(condtion.getRoomId());
		// 查询CentersInfo
		CentersInfo centersInfo = centersInfoMapper.selectByPrimaryKey(roomInfo.getCentersId());
		// 直接加进来
		UserInfo childInfo = userInfoMapper.getUserInfoByAccountId(condtion.getChildId());
		// 查询ChildAttendanceInfo
		ChildAttendanceInfo attendanceInfo = childAttendanceInfoMapper.selectByUserId(childInfo.getId());
		// 这一天是那一天
		int dayOfWeek = DateUtil.getDayOfWeek(condtion.getDay()) - 1;

		// 校验
		ServiceResult<ChildAttendanceInfo> result = validateApproveAdd(currentUser, centersInfo, roomInfo, attendanceInfo, condtion);
		if (!result.isSuccess()) {
			return result;
		}
		String requestId = "";
		// 内部 已入院 则是第一个 家长申请的场景 处理
		boolean isInside = attendanceInfo.getType() == ChildType.Inside.getValue() && attendanceInfo.getEnrolled() == EnrolledState.Enrolled.getValue();
		if (isInside) {
			// 临时排课,只记录信息不做任何改变.
			if (condtion.isInterim()) {
				dealInterimAttendance(condtion.getChildId(), centersInfo.getId(), roomInfo.getId(), condtion.getDay(), currentUser);
				return successResult();
			}
			// 修改排课申请的某一天改为2已安排
			List<ChangeAttendanceRequestInfo> changeAttendanceRequestInfos = changeAttendanceRequestInfoMapper.getListByChildAndState(condtion.getChildId(),
					ChangeAttendanceRequestState.Default.getValue());
			if (ListUtil.isEmpty(changeAttendanceRequestInfos)) {
				changeAttendanceRequestInfos = changeAttendanceRequestInfoMapper.getListByChildAndState(condtion.getChildId(),
						ChangeAttendanceRequestState.PartialAproved.getValue());
			}

			// 处理周期 将处理的天设置状态为 ChangeAttendanceRequestDayState.Deal
			List<TemporaryAttendanceInfo> temporaryList = attendanceFactory.dealAttendance(changeAttendanceRequestInfos.get(0), dayOfWeek, false, condtion.getDay(),
					childInfo.getCentersId(), condtion.getRoomId(), condtion.getChildId(), currentUser.getAccountInfo().getId());

			// 判断是否请求已经被全部安排
			// TODO big 这里可能已经该安排变为2 然后又减去了????
			if (attendanceFactory.isDealAll(changeAttendanceRequestInfos.get(0))) {
				changeAttendanceRequestInfos.get(0).setState(ChangeAttendanceRequestState.Over.getValue());
			} else {
				changeAttendanceRequestInfos.get(0).setState(ChangeAttendanceRequestState.PartialAproved.getValue());
			}
			changeAttendanceRequestInfos.get(0).setUpdateAccountId(currentUser.getAccountInfo().getId());
			changeAttendanceRequestInfos.get(0).setUpdateTime(new Date());
			changeAttendanceRequestInfoMapper.updateByPrimaryKey(changeAttendanceRequestInfos.get(0));

			// 插入永久安排到数据库
			ChildAttendanceInfo childAttendanceInfo = childAttendanceInfoMapper.selectByUserId(childInfo.getId());
			for (TemporaryAttendanceInfo temporaryAttendanceInfo : temporaryList) {
				// 如果是今天安排 切安排的周期也是今天
				if ((com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(new Date(), temporaryAttendanceInfo.getBeginTime()) == 0)
						&& (DateUtil.getDayOfWeek(temporaryAttendanceInfo.getBeginTime()) - 1 == temporaryAttendanceInfo.getDayWeek())) {
					attendanceFactory.dealAttendance(childAttendanceInfo, temporaryAttendanceInfo.getDayWeek());
					childAttendanceInfoMapper.updateByPrimaryKey(childAttendanceInfo);
					// 插入临时安排
					temporaryAttendanceInfo.setDeleteFlag(DeleteFlag.Delete.getValue());
					temporaryAttendanceInfoMapper.insert(temporaryAttendanceInfo);
					requestLogService.dealLog(currentUser, temporaryAttendanceInfo);
				} else {
					// 插入临时安排
					temporaryAttendanceInfoMapper.insert(temporaryAttendanceInfo);
					// 加入空间影响
					RoomChangeInfo roomChangeInfo = new RoomChangeInfo();
					roomChangeInfo.setCentreId(centersInfo.getId());
					roomChangeInfo.setChildId(condtion.getChildId());
					roomChangeInfo.setDay(temporaryAttendanceInfo.getBeginTime());
					roomChangeInfo.setDayWeek(temporaryAttendanceInfo.getDayWeek());
					roomChangeInfo.setOperate(RoomChangeOperate.ADDALL.getValue());
					roomChangeInfo.setReqId(temporaryAttendanceInfo.getId());
					roomChangeInfo.setRoomId(roomInfo.getId());
					roomChangeInfoMapper.insert(roomChangeInfo);

					requestLogService.dealLog(currentUser, temporaryAttendanceInfo);
				}
			}
			requestId = changeAttendanceRequestInfos.get(0).getId();
		} else {
			// 更新centersId
			childInfo.setCentersId(roomInfo.getCentersId());
			// 1.修改 child信息
			childInfo.setRoomId(roomInfo.getId());
			// 2.修改attent信息
			attendanceInfo.setEnrolled(EnrolledState.Default.getValue());
			attendanceInfo.setChangeDate(condtion.getDay());
			if (attendanceInfo.getEnrolmentDate() == null || attendanceInfo.getEnrolmentDate().after(condtion.getDay())) {
				attendanceInfo.setEnrolmentDate(condtion.getDay());
			}
			// 如果是外部的
			if (attendanceInfo.getType() == ChildType.External.getValue()) {
				attendanceInfo.setType(ChildType.Inside.getValue());
				attendanceInfo.setRoomId(condtion.getRoomId());
				// 小孩从外部到内部给家长发送从外部到内部邮件
				userFactory.sendParentEmailByChild(getSystemEmailMsg("child_inside_title"), getSystemEmailMsg("child_inside_content"), condtion.getChildId(), true,
						EmailType.inside.getValue(), null, null, attendanceInfo, attendanceInfo.isPlan());
			}

			// 如果不安排全部 那么将attendanceInfo信息全部至为不勾选
			if (!condtion.isDealAll()) {
				attendanceInfo.setMonday(false);
				attendanceInfo.setTuesday(false);
				attendanceInfo.setWednesday(false);
				attendanceInfo.setThursday(false);
				attendanceInfo.setFriday(false);
			}
			// 勾选对应天
			attendanceFactory.dealAttendance(attendanceInfo, dayOfWeek);

			// 加入请求队列
			ChangeAttendanceRequestInfo attendanceRequestInfo = getRequest(currentUser, childInfo, attendanceInfo, condtion.getChildId(), condtion.getDay());
			// 如果改变的时间在当前时间以前或者今天 則不要經過定時任務立馬執行
			if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(new Date(), condtion.getDay()) <= 0) {
				// 這個請求不在job中執行了
				attendanceRequestInfo.setState(ChangeAttendanceRequestState.Over.getValue());
				attendanceInfo.setEnrolled(EnrolledState.Enrolled.getValue());
			} else {
				dealRoomChange(null, attendanceRequestInfo);
			}

			AccountInfo accountInfo = accountInfoMapper.selectByPrimaryKey(condtion.getChildId());
			accountInfo.setStatus(ArchivedStatus.UnArchived.getValue());
			accountInfoMapper.updateByPrimaryKey(accountInfo);
			childAttendanceInfoMapper.updateByPrimaryKey(attendanceInfo);
			changeAttendanceRequestInfoMapper.insert(attendanceRequestInfo);
			userInfoMapper.updateByPrimaryKey(childInfo);
			requestId = attendanceRequestInfo.getId();
		}
		// 将requestLog的updatetime更新
		RequestLogInfo logInfo = requestLogInfoMapper.getRequestLogInfoByObjId(requestId);
		if (logInfo != null) {
			logInfo.setUpdateTime(new Date());
			logInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
			requestLogInfoMapper.updateByPrimaryKeySelective(logInfo);
		}

		attendanceInfo.setRequestId(requestId);
		return successResult(attendanceInfo);
	}

	private void dealInterimAttendance(String childAccountId, String centerId, String roomId, Date day, UserInfoVo current) {
		InterimAttendance interim = new InterimAttendance();
		interim.setId(UUID.randomUUID().toString());
		interim.setAccountId(childAccountId);
		interim.setCenterId(centerId);
		interim.setRoomId(roomId);
		interim.setDay(day);
		interim.setCreateTime(new Date());
		interim.setCreateAccountId(current.getAccountInfo().getId());
		interim.setDeleteFlag(DeleteFlag.Default.getValue());
		interimAttendanceMapper.insert(interim);
	}

	private ChangeAttendanceRequestInfo getRequest(UserInfoVo currentUser, UserInfo child, ChildAttendanceInfo childAttendanceInfo, String accountId, Date changeDay) {
		ChangeAttendanceRequestInfo attendanceRequestInfo = new ChangeAttendanceRequestInfo();
		// 加入请求队列
		attendanceRequestInfo = new ChangeAttendanceRequestInfo();
		attendanceRequestInfo.setChangeDate(changeDay);
		attendanceRequestInfo.setChildId(accountId);
		attendanceRequestInfo.setFriday(childAttendanceInfo.getFridayShort());
		attendanceRequestInfo.setMonday(childAttendanceInfo.getMondayShort());
		attendanceRequestInfo.setNewCentreId(child.getCentersId());
		attendanceRequestInfo.setNewGroupId(child.getGroupId());
		attendanceRequestInfo.setNewRoomId(child.getRoomId());
		attendanceRequestInfo.setThursday(childAttendanceInfo.getThursdayShort());
		attendanceRequestInfo.setTuesday(childAttendanceInfo.getTuesdayShort());
		attendanceRequestInfo.setSourceType(ChangeAttendanceRequestType.AddAttendance.getValue());
		attendanceRequestInfo.setType(AttendanceRequestType.C.getValue());
		attendanceRequestInfo.setWednesday(childAttendanceInfo.getWednesdayShort());
		attendanceRequestInfo.setId(UUID.randomUUID().toString());
		attendanceRequestInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
		attendanceRequestInfo.setCreateTime(new Date());
		attendanceRequestInfo.setDeleteFlag(DeleteFlag.Default.getValue());
		attendanceRequestInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
		attendanceRequestInfo.setUpdateTime(new Date());
		attendanceRequestInfo.setState(ChangeAttendanceRequestState.OnJob.getValue());
		return attendanceRequestInfo;
	}

	@Override
	public ServiceResult<Object> updateTemporaryRequest(UserInfoVo currentUser, int type, String logId, short value) {
		boolean isCeo = containRole(Role.CEO, currentUser.getRoleInfoVoList()) == 1;
		boolean isCentreManager = (containRole(Role.CentreManager, currentUser.getRoleInfoVoList()) == 1)
				|| (containRole(Role.EducatorSecondInCharge, currentUser.getRoleInfoVoList()) == 1);
		boolean isAdmin = isCeo || isCentreManager;
		if (!isAdmin) {
			return failResult(-1, getTipMsg("no.permissions"));
		}

		RequestLogInfo requestLogInfo = requestLogInfoMapper.selectByPrimaryKey(logId);
		String objId = requestLogInfo.getObjId();
		UserInfo userInfo = userInfoMapper.getUserInfoByAccountId(requestLogInfo.getChildId());
		ChildAttendanceInfo attendanceInfo = childAttendanceInfoMapper.getChildAttendanceByUserId(userInfo.getId());
		if ((short) type == RequestLogType.TemporaryAttendance.getValue()) {
			TemporaryAttendanceInfo temporaryAttendanceInfo = temporaryAttendanceInfoMapper.selectByPrimaryKey(objId);

			if (temporaryAttendanceInfo == null) {
				return failResult(-1, getTipMsg("no.permissions"));
			}
			AttendancePostion attendancePostion = attendanceFactory.getChildPostion(requestLogInfo.getChildId(), temporaryAttendanceInfo.getBeginTime());
			if (ChangeAttendanceRequestState.Cancel.getValue() == value) {
				temporaryAttendanceInfo.setDeleteFlag(DeleteFlag.Delete.getValue());
				temporaryAttendanceInfoMapper.updateByPrimaryKey(temporaryAttendanceInfo);
				roomChangeInfoMapper.deleteByReqId(objId, null);
			}
			attendanceInfo.setNewMonday(attendancePostion.getMonday());
			attendanceInfo.setNewTuesday(attendancePostion.getTuesday());
			attendanceInfo.setNewWednesday(attendancePostion.getWednesday());
			attendanceInfo.setNewThursday(attendancePostion.getThursday());
			attendanceInfo.setNewFriday(attendancePostion.getFriday());
		} else {
			SubtractedAttendance subtractedAttendance = subtractedAttendanceMapper.selectByPrimaryKey(objId);

			if (subtractedAttendance == null) {
				return failResult(-1, getTipMsg("no.permissions"));
			}
			dealSubtractedAttendance(attendanceInfo, subtractedAttendance);
			if (ChangeAttendanceRequestState.Cancel.getValue() == value) {
				subtractedAttendance.setDeleteFlag(DeleteFlag.Delete.getValue());
				subtractedAttendanceMapper.updateByPrimaryKey(subtractedAttendance);
				roomChangeInfoMapper.deleteByReqId(objId, null);
			}
		}
		requestLogInfo.setState(ChangeAttendanceRequestState.Cancel.getValue());
		requestLogInfo.setUpdateTime(new Date());
		requestLogInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
		requestLogInfoMapper.updateByPrimaryKey(requestLogInfo);

		String title = getSystemEmailMsg("cancel_attendance_title");
		String content = getSystemEmailMsg("cancel_attendance_content");
		userFactory.sendParentEmailByChild(title, content, requestLogInfo.getChildId(), true, EmailType.cancel_attendance.getValue(), null, null, attendanceInfo,
				attendanceInfo.isPlan());
		userFactory.sendEmailCeoManager(title, content, requestLogInfo.getChildId(), EmailType.cancel_attendance.getValue(), null, null, attendanceInfo);

		return successResult();
	}

	/**
	 * @description 处理上课周期
	 * @author hxzhang
	 * @create 2017年6月15日下午6:24:59
	 */
	private void dealSubtractedAttendance(ChildAttendanceInfo attendanceInfo, SubtractedAttendance subtractedAttendance) {
		// 同步上课周期
		attendanceInfo.setNewMonday(attendanceInfo.getMonday());
		attendanceInfo.setNewTuesday(attendanceInfo.getTuesday());
		attendanceInfo.setNewWednesday(attendanceInfo.getWednesday());
		attendanceInfo.setNewThursday(attendanceInfo.getThursday());
		attendanceInfo.setNewFriday(attendanceInfo.getFriday());

		switch (subtractedAttendance.getDayWeek()) {
		case 1:
			attendanceInfo.setNewMonday(false);
			break;
		case 2:
			attendanceInfo.setNewTuesday(false);
			break;
		case 3:
			attendanceInfo.setNewWednesday(false);
			break;
		case 4:
			attendanceInfo.setNewThursday(false);
			break;
		case 5:
			attendanceInfo.setNewFriday(false);
			break;

		default:
			break;
		}
	}

	@Override
	public ServiceResult<Object> updateAttendanceRequest(UserInfoVo currentUser, String objid, short value) {
		ChangeAttendanceRequestInfo changeAttendanceRequestInfo = changeAttendanceRequestInfoMapper.selectByPrimaryKey(objid);
		boolean isCeo = containRole(Role.CEO, currentUser.getRoleInfoVoList()) == 1;
		boolean isCentreManager = (containRole(Role.CentreManager, currentUser.getRoleInfoVoList()) == 1)
				|| (containRole(Role.EducatorSecondInCharge, currentUser.getRoleInfoVoList()) == 1);
		boolean isAdmin = isCeo || isCentreManager;
		if (changeAttendanceRequestInfo == null) {
			return failResult(-1, getTipMsg("no.permissions"));
		}

		// 取消申请
		if (ChangeAttendanceRequestState.Cancel.getValue() == value) {
			short sourceType = changeAttendanceRequestInfo.getSourceType();
			if (sourceType == RequestLogType.ParentRequest.getValue()) {
				if (changeAttendanceRequestInfo.getState() != ChangeAttendanceRequestState.Default.getValue()) {
					return failResult(-1, getTipMsg("no.permissions"));
				}
				if (!isAdmin) {
					boolean isMyFamily = false;
					// 只能修改自己家族小孩的
					List<UserInfo> childInfos = userInfoMapper.getChildInfoByFamilyId(currentUser.getUserInfo().getFamilyId());
					if (ListUtil.isEmpty(childInfos)) {
						return failResult(-1, getTipMsg("no.permissions"));
					}
					for (UserInfo item : childInfos) {
						AccountInfo accountInfo = accountInfoMapper.getAccountInfoByUserId(item.getId());
						if (accountInfo.getId().equals(changeAttendanceRequestInfo.getChildId())) {
							isMyFamily = true;
							break;
						}
					}
					if (!isMyFamily) {
						return failResult(-1, getTipMsg("no.permissions"));
					}
				} else if (isCentreManager) {
					// 申請的小孩必須是自己園下的
					UserInfo child = userInfoMapper.getUserInfoByAccountId(changeAttendanceRequestInfo.getChildId());
					boolean isCentreAdmin = attendanceCoreService.isCenterManager(currentUser, child.getCentersId(), isCentreManager);
					if (isCentreManager && !isCentreAdmin) {
						return failResult(-1, getTipMsg("no.permissions"));
					}
				}

			} // 园长取消申请
			else if (sourceType == RequestLogType.CentreManagerRequest.getValue()) {
				// 取消已通过的转园申请
				if (changeAttendanceRequestInfo.getState() != ChangeAttendanceRequestState.Default.getValue()) {
					// 是否是新园的园长
					boolean isCentreAdmin = attendanceCoreService.isCenterManager(currentUser, changeAttendanceRequestInfo.getNewCentreId(), isCentreManager);
					if (!isCeo && !isCentreAdmin) {
						return failResult(-1, getTipMsg("no.permissions"));
					}
				} else {
					if (!changeAttendanceRequestInfo.getCreateAccountId().equals(currentUser.getAccountInfo().getId())) {
						return failResult(-1, getTipMsg("no.permissions"));
					}
				}
			} else if (sourceType == RequestLogType.CeoChangeCentre.getValue() || sourceType == RequestLogType.CentreManagerSure.getValue()
					|| sourceType == RequestLogType.ChangeRoom.getValue() || sourceType == RequestLogType.ChangeAttendance.getValue()) {
				// 不是园长或者不是CEO均没有权限操作
				if (!isAdmin) {
					return failResult(-1, getTipMsg("no.permissions"));
				}
				// 园长确认转园 只有角色为园长的 自己以及CEO 才能取消
				if (sourceType == RequestLogType.CentreManagerSure.getValue()) {
					if (!isCeo) {
						if (!changeAttendanceRequestInfo.getApprovalAccountId().equals(currentUser.getAccountInfo().getId())) {
							return failResult(-1, getTipMsg("no.permissions"));
						}
					}
				} else {
					if (isCentreManager) {
						UserInfo child = userInfoMapper.getUserInfoByAccountId(changeAttendanceRequestInfo.getChildId());
						boolean isCentreAdmin = attendanceCoreService.isCenterManager(currentUser, child.getCentersId(), isCentreManager);
						if (isCentreManager && !isCentreAdmin) {
							return failResult(-1, getTipMsg("no.permissions"));
						}
					}
				}
				// 看是否已经被安排over了 如果是 那么不可以取消
				if (changeAttendanceRequestInfo.getState() != ChangeAttendanceRequestState.OnJob.getValue()) {
					return failResult(-1, getTipMsg("no.permissions"));
				}
			}
			// 删除安排时候的tbl_room_change数据
			roomChangeInfoMapper.deleteByReqId(changeAttendanceRequestInfo.getId(), null);
		}
		// 拒绝转园申请
		else if (ChangeAttendanceRequestState.Discard.getValue() == value) {
			// 拒绝转园申请
			if (changeAttendanceRequestInfo.getSourceType() == RequestLogType.CentreManagerRequest.getValue()) {
				// 被申请园的园长 才能操作
				boolean isCentreAdmin = attendanceCoreService.isCenterManager(currentUser, changeAttendanceRequestInfo.getNewCentreId(), isCentreManager);
				if (isCentreManager && !isCentreAdmin) {
					return failResult(-1, getTipMsg("no.permissions"));
				}
			}
		}
		changeAttendanceRequestInfo.setState(value);
		changeAttendanceRequestInfo.setApprovalAccountId(currentUser.getAccountInfo().getId());
		changeAttendanceRequestInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
		changeAttendanceRequestInfo.setUpdateTime(new Date());
		changeAttendanceRequestInfoMapper.updateByPrimaryKey(changeAttendanceRequestInfo);
		// 取消 change attendance or room 给家长,CEO,园长发送邮件
		// (modifybyhxzhang2017-10-23)
		if (changeAttendanceRequestInfo.getType() == 0 || changeAttendanceRequestInfo.getType() == 1) {
			String accountId = changeAttendanceRequestInfo.getChildId();
			ChildAttendanceInfo att = childAttendanceInfoMapper.getChildAttendanceByUserId(userInfoMapper.getUserInfoByAccountId(accountId).getId());
			if (containRoles(currentUser.getRoleInfoVoList(), Role.Parent)) {
				if (changeAttendanceRequestInfo.getMonday() == ChangeAttendanceRequestDayState.UnCheck.getValue()
						|| changeAttendanceRequestInfo.getMonday() == ChangeAttendanceRequestDayState.Cancel.getValue()) {
					att.setNewMonday(false);
				} else {
					att.setNewMonday(true);
				}
				if (changeAttendanceRequestInfo.getTuesday() == ChangeAttendanceRequestDayState.UnCheck.getValue()
						|| changeAttendanceRequestInfo.getTuesday() == ChangeAttendanceRequestDayState.Cancel.getValue()) {
					att.setNewTuesday(false);
				} else {
					att.setNewTuesday(true);
				}
				if (changeAttendanceRequestInfo.getWednesday() == ChangeAttendanceRequestDayState.UnCheck.getValue()
						|| changeAttendanceRequestInfo.getWednesday() == ChangeAttendanceRequestDayState.Cancel.getValue()) {
					att.setNewWednesday(false);
				} else {
					att.setNewWednesday(true);
				}
				if (changeAttendanceRequestInfo.getThursday() == ChangeAttendanceRequestDayState.UnCheck.getValue()
						|| changeAttendanceRequestInfo.getThursday() == ChangeAttendanceRequestDayState.Cancel.getValue()) {
					att.setNewThursday(false);
				} else {
					att.setNewThursday(true);
				}
				if (changeAttendanceRequestInfo.getFriday() == ChangeAttendanceRequestDayState.UnCheck.getValue()
						|| changeAttendanceRequestInfo.getFriday() == ChangeAttendanceRequestDayState.Cancel.getValue()) {
					att.setNewFriday(false);
				} else {
					att.setNewFriday(true);
				}
			} else {
				att.setNewMonday(changeAttendanceRequestInfo.getMonday() == 0 ? false : true);
				att.setNewTuesday(changeAttendanceRequestInfo.getTuesday() == 0 ? false : true);
				att.setNewWednesday(changeAttendanceRequestInfo.getWednesday() == 0 ? false : true);
				att.setNewThursday(changeAttendanceRequestInfo.getThursday() == 0 ? false : true);
				att.setNewFriday(changeAttendanceRequestInfo.getFriday() == 0 ? false : true);
			}

			String title = getSystemEmailMsg("cancel_attendance_title");
			String content = getSystemEmailMsg("cancel_attendance_content");

			if (!(isAdmin && userFactory.havePlanEmail(objid))) {
				userFactory.sendParentEmailByChild(title, content, accountId, true, EmailType.cancel_attendance.getValue(), null, null, att, att.isPlan());
			}
			userFactory.sendEmailCeoManager(title, content, accountId, EmailType.cancel_attendance.getValue(), null, null, att);
		}
		// 取消chang centre 给家长,园长,ceo发送邮件
		if (changeAttendanceRequestInfo.getType() == 2 && ChangeAttendanceRequestState.Cancel.getValue() == value) {
			attendanceFactory.cancelChangCentreSendEmail(changeAttendanceRequestInfo, currentUser);
		}
		// 将requestLog的updatetime更新
		RequestLogInfo logInfo = requestLogInfoMapper.getRequestLogInfoByObjId(objid);
		if (logInfo != null) {
			logInfo.setUpdateTime(new Date());
			logInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
			requestLogInfoMapper.updateByPrimaryKeySelective(logInfo);
		}

		// 删除email
		userFactory.removePlanEmail(objid);

		return successResult();
	}

	@Override
	public ServiceResult<Object> getRequestLogObj(UserInfoVo currentUser, String objId, int type) {
		ChangeAttendanceRequestInfo changeAttendanceRequestInfo = changeAttendanceRequestInfoMapper.selectByPrimaryKey(objId);
		ChildInfoVo child = new ChildInfoVo();
		UserInfo userInfo = userInfoMapper.getUserInfoByAccountId(changeAttendanceRequestInfo.getChildId());
		AccountInfo accountInfo = accountInfoMapper.selectByPrimaryKey(changeAttendanceRequestInfo.getChildId());
		userInfo.setCentersId(changeAttendanceRequestInfo.getNewCentreId());
		userInfo.setRoomId(changeAttendanceRequestInfo.getNewRoomId());
		userInfo.setGroupId(changeAttendanceRequestInfo.getNewGroupId());
		child.setUserInfo(userInfo);
		ChildAttendanceInfo attendance = new ChildAttendanceInfo();
		attendance.setMonday(changeAttendanceRequestInfo.getMonday() != ChangeAttendanceRequestDayState.UnCheck.getValue()
				&& changeAttendanceRequestInfo.getMonday() != ChangeAttendanceRequestDayState.Cancel.getValue());
		attendance.setTuesday(changeAttendanceRequestInfo.getTuesday() != ChangeAttendanceRequestDayState.UnCheck.getValue()
				&& changeAttendanceRequestInfo.getTuesday() != ChangeAttendanceRequestDayState.Cancel.getValue());
		attendance.setWednesday(changeAttendanceRequestInfo.getWednesday() != ChangeAttendanceRequestDayState.UnCheck.getValue()
				&& changeAttendanceRequestInfo.getWednesday() != ChangeAttendanceRequestDayState.Cancel.getValue());
		attendance.setThursday(changeAttendanceRequestInfo.getThursday() != ChangeAttendanceRequestDayState.UnCheck.getValue()
				&& changeAttendanceRequestInfo.getThursday() != ChangeAttendanceRequestDayState.Cancel.getValue());
		attendance.setFriday(changeAttendanceRequestInfo.getFriday() != ChangeAttendanceRequestDayState.UnCheck.getValue()
				&& changeAttendanceRequestInfo.getFriday() != ChangeAttendanceRequestDayState.Cancel.getValue());
		attendance.setCreateAccountId("");
		attendance.setChangeDate(changeAttendanceRequestInfo.getChangeDate());
		child.setAttendance(attendance);
		child.setAccountInfo(accountInfo);
		if (changeAttendanceRequestInfo.getType() == AttendanceRequestType.C.getValue()
				&& changeAttendanceRequestInfo.getState() == ChangeAttendanceRequestState.Default.getValue()) {
			userInfo.setRoomId(null);
			userInfo.setGroupId(null);
		}
		return successResult((Object) child);
	}

	@Override
	public ServiceResult<Map<String, Object>> getAttendance(String userId) {
		Map<String, Object> returnMap = new HashMap<String, Object>();
		AccountInfo accountInfo = accountInfoMapper.selectAccountInfoByUserId(userId);
		ChildAttendanceInfo childAttendanceInfo = childAttendanceInfoMapper.selectByUserId(userId);
		// 拼接歷史消息
		List<RequestLogVo> requestLogVos = requestLogInfoMapper.selectLogVo(accountInfo.getId());
		// User
		UserInfo userInfo = userInfoMapper.selectByPrimaryKey(userId);
		// 上课历史
		HistoryVo historyVo = attendanceHistoryFactory.getHistoryVo(userInfo, accountInfo.getId(), childAttendanceInfo.getLeaveDate());
		// 获取孩子邮件信息
		List<EmailMsg> emailMsgList = emailMsgMapper.getEmailMsgListByUserId(userId);
		returnMap.put("attendanceInfo", childAttendanceInfo);
		returnMap.put("requestLogs", requestLogVos);
		returnMap.put("historys", historyVo);
		returnMap.put("userInfo", userInfo);
		returnMap.put("accountInfo", accountInfo);
		returnMap.put("emailMsgList", emailMsgList);
		return successResult(returnMap);
	}

	@Override
	public ServiceResult<Object> dealApproveReplace(UserInfoVo currentUser, String attendanceRequestId, String childId, Date day, int dayOrInstances, int type,
			boolean overOld) {
		UserInfo child = userInfoMapper.getUserInfoByAccountId(childId);
		boolean isCentreManager = (containRole(Role.CentreManager, currentUser.getRoleInfoVoList()) == 1)
				|| (containRole(Role.EducatorSecondInCharge, currentUser.getRoleInfoVoList()) == 1);
		// 自己园长只能操作自己园的
		if (isCentreManager) {
			boolean iscentreAdmin = attendanceCoreService.isCenterManager(currentUser, child.getCentersId(), isCentreManager);
			if (!iscentreAdmin) {
				return failResult(-1, getTipMsg("no.permissions"));
			}
		}

		ChangeAttendanceRequestInfo attendanceRequestInfo = changeAttendanceRequestInfoMapper.selectByPrimaryKey(attendanceRequestId);

		TemporaryAttendanceInfo temp = new TemporaryAttendanceInfo();
		temp.setBeginTime(day);
		temp.setCentreId(attendanceRequestInfo.getNewCentreId());
		temp.setChildId(attendanceRequestInfo.getChildId());
		temp.setCreateAccountId(currentUser.getAccountInfo().getId());
		temp.setCreateTime(new Date());
		int dayOfWeek = DateUtil.getDayOfWeek(day) - 1;
		temp.setDayWeek((short) dayOfWeek);
		temp.setDeleteFlag(DeleteFlag.Default.getValue());
		// today
		if (type == 0) {
			temp.setEndTime(day);
		} else {
			temp.setEndTime(DateUtil.addWeeks(day, dayOrInstances - 1));
		}
		temp.setId(UUID.randomUUID().toString());
		temp.setRaplaceChildId(childId);
		temp.setRoomId(attendanceRequestInfo.getNewRoomId());
		temp.setType(TemporaryType.Temporary.getValue());
		if (type == 0) {
			// 看今天是否有其它地方有安排 如果有提示先取消
			List<TemporaryAttendanceInfo> haveDays = temporaryAttendanceInfoMapper.getDayAttendances(day, attendanceRequestInfo.getChildId());
			if (!ListUtil.isEmpty(haveDays)) {
				return failResult(-1, getTipMsg("submitChangeAttendance.dealApproveReplace.day.allready"));
			}
			// 加入单个天的零时增加
			RoomChangeInfo changeInfo = new RoomChangeInfo();
			changeInfo.setCentreId(child.getCentersId());
			changeInfo.setChildId(attendanceRequestInfo.getChildId());
			changeInfo.setDay(day);
			changeInfo.setDayWeek((short) dayOfWeek);
			changeInfo.setOperate(RoomChangeOperate.ADDDAY.getValue());
			changeInfo.setReqId(temp.getId());
			changeInfo.setRoomId(child.getRoomId());
			roomChangeInfoMapper.insert(changeInfo);

		} else if (type == 1) {
			// 判断在此期间是否存在有零时的安排
			TemporaryAttendanceCondition temporaryAttendanceCondition = new TemporaryAttendanceCondition();
			temporaryAttendanceCondition.setBeginTime(temp.getBeginTime());
			temporaryAttendanceCondition.setEndTime(temp.getEndTime());
			temporaryAttendanceCondition.setChildId(attendanceRequestInfo.getChildId());
			temporaryAttendanceCondition.setDayOfWeek((short) dayOfWeek);
			// temporaryAttendanceCondition.setRoomId(attendanceRequestInfo.getNewRoomId());
			List<TemporaryAttendanceInfo> haveTemporarys = temporaryAttendanceInfoMapper.getList(temporaryAttendanceCondition);
			if (!overOld && haveTemporarys.size() > 0) {
				return failResult(-1, getTipMsg("submitChangeAttendance.dealApproveReplace.contain"));
			}
			// for (TemporaryAttendanceInfo temporaryAttendanceInfo :
			// haveTemporarys) {
			// temporaryAttendanceInfo.setDeleteFlag(DeleteFlag.Delete.getValue());
			// temporaryAttendanceInfoMapper.updateByPrimaryKey(temporaryAttendanceInfo);
			// }

			// 如果存在时间冲突的changeAttendanceRequest
			List<ChangeAttendanceRequestInfo> changeAttendanceRequestInfos = changeAttendanceRequestInfoMapper.haveAttendanceRequestScope(
					attendanceRequestInfo.getChildId(), attendanceRequestInfo.getNewRoomId(), dayOfWeek, temp.getBeginTime(), temp.getEndTime());
			if (!overOld && changeAttendanceRequestInfos.size() > 0) {
				return failResult(-1, getTipMsg("submitChangeAttendance.dealApproveReplace.request.contain"));
			}
			// for (ChangeAttendanceRequestInfo changeAttendanceRequestInfo :
			// changeAttendanceRequestInfos) {
			// changeAttendanceRequestInfo.setState(ChangeAttendanceRequestState.Cancel.getValue());
			// changeAttendanceRequestInfoMapper.updateByPrimaryKey(changeAttendanceRequestInfo);
			// }

			// 如果存在时间冲突的 转园申请
			List<ChangeAttendanceRequestInfo> changeRoomRequestList = changeAttendanceRequestInfoMapper.haveChangeCenterRequestScope(attendanceRequestInfo.getChildId(),
					attendanceRequestInfo.getNewRoomId(), temp.getEndTime());
			if (!overOld && changeRoomRequestList.size() > 0) {
				return failResult(-1, getTipMsg("submitChangeAttendance.dealApproveReplace.change.center"));
			}
			// for (ChangeAttendanceRequestInfo changeAttendanceRequestInfo :
			// changeRoomRequestList) {
			// changeAttendanceRequestInfo.setState(ChangeAttendanceRequestState.Cancel.getValue());
			// changeAttendanceRequestInfoMapper.updateByPrimaryKey(changeAttendanceRequestInfo);
			// }
			// 加入多个天的零时增加
			Date tempDay = day;
			for (int i = 0; i < dayOrInstances; i++) {
				Date tempDate = DateUtil.addDay(tempDay, 7 * i);
				RoomChangeInfo changeInfo = new RoomChangeInfo();
				changeInfo.setCentreId(child.getCentersId());
				changeInfo.setChildId(attendanceRequestInfo.getChildId());
				changeInfo.setDay(tempDate);
				changeInfo.setDayWeek((short) dayOfWeek);
				changeInfo.setOperate(RoomChangeOperate.ADDDAY.getValue());
				changeInfo.setReqId(temp.getId());
				changeInfo.setRoomId(child.getRoomId());
				roomChangeInfoMapper.insert(changeInfo);
			}
		}

		temporaryAttendanceInfoMapper.insert(temp);
		requestLogService.dealLog(currentUser, temp);
		return successResult();
	}

	@Override
	public ServiceResult<ReplaceChildAttendanceVo> getAddChilds(String roomId, Date day) {
		RoomInfo roomInfo = roomInfoMapper.selectByPrimaryKey(roomId);
		// 判断是否为空 切 未归档
		if (roomInfo == null || roomInfo.getStatus() == ArchivedStatus.Archived.getValue()) {
			return failResult(-1, getTipMsg("illegal.request"));
		}
		// 内部的
		int dayOfWeek = DateUtil.getDayOfWeek(day) - 1;
		List<ReplaceChildVo> internal = signinInfoMapper.getInternalReplaceChilds(roomInfo.getCentersId(), roomId, dayOfWeek, day);
		List<ReplaceChildVo> internalResult = new ArrayList<ReplaceChildVo>();
		// 查看这个人是否在这个园 这个时间点
		if (ListUtil.isNotEmpty(internal)) {
			for (ReplaceChildVo replaceChildVo : internal) {
				AttendancePostion attendancePostion = attendanceFactory.getChildPostion(replaceChildVo.getId(), day);
				if (attendanceFactory.haveAttendanceOfDay(attendancePostion, day)) {
					continue;
				}
				if (roomId.equals(attendancePostion.getRoomId())) {
					internalResult.add(0, replaceChildVo);
				}
			}
		}
		// 获取该园区所有孩子
		List<ReplaceChildVo> allInternal = signinInfoMapper.getChildListByCenterId(roomInfo.getCentersId());

		// 即将进去的
		List<ReplaceChildVo> siblings = signinInfoMapper.getSiblingsReplaceChilds(roomInfo.getCentersId(), roomId, dayOfWeek, day);

		// 外部的
		List<ReplaceChildVo> externals = signinInfoMapper.getExternalsReplaceChilds(roomInfo.getCentersId(), roomId, dayOfWeek, day);

		ReplaceChildAttendanceVo replaceChildAttendanceVo = new ReplaceChildAttendanceVo();
		replaceChildAttendanceVo.setInternal(internalResult);
		replaceChildAttendanceVo.setSiblings(siblings);
		replaceChildAttendanceVo.setExternal(externals);

		replaceChildAttendanceVo.setAllInternal(allInternal);

		return successResult(replaceChildAttendanceVo);
	}

	@Override
	public ServiceResult<ReplaceChildAttendanceVo> getReplaceChilds(String absenteeId, String chilId, Date day) {
		// TODO big 这里可能有问题 关于转园的问题
		UserInfo userInfo = userInfoMapper.getUserInfoByAccountId(chilId);
		// 获取小孩请假开始时间
		AbsenteeRequestInfo absenteeRequestInfo = absenteeRequestInfoMapper.selectByPrimaryKey(absenteeId);
		AttendancePostion postion = attendanceFactory.getChildPostion(absenteeRequestInfo.getChildId(), day);

		ReplaceChildVo absenteeChild = new ReplaceChildVo();
		absenteeChild.setFristName(userInfo.getFirstName());
		absenteeChild.setName(userInfo.getFullName());
		absenteeChild.setAvatar(userInfo.getAvatar());
		absenteeChild.setPersonColor(userInfo.getPersonColor());
		List<UserInfo> parents = userInfoMapper.getParentInfoByFamilyId(userInfo.getFamilyId());
		if (ListUtil.isNotEmpty(parents)) {
			absenteeChild.setParentName(parents.get(0).getFullName());
			absenteeChild.setParentPhone(parents.get(0).getPhoneNumber());
			absenteeChild.setParentMobile(parents.get(0).getMobileNumber());
		}
		absenteeChild.setDate(absenteeRequestInfo.getBeginDate());

		// 获取当天申请排课的小孩信息
		int dayOfWeek = DateUtil.getDayOfWeek(day) - 1;
		List<ReplaceChildVo> internal = signinInfoMapper.getInternalReplaceChilds(userInfo.getCentersId(), userInfo.getRoomId(), dayOfWeek, day);
		if (ListUtil.isNotEmpty(internal)) {
			// 排除自己
			for (ReplaceChildVo replaceChildVo : internal) {
				if (replaceChildVo.getId().equals(absenteeRequestInfo.getChildId())) {
					internal.remove(replaceChildVo);
					break;
				}
			}
		}
		List<ReplaceChildVo> internalResult = new ArrayList<ReplaceChildVo>();
		// 查看这个人是否在这个园 这个时间点
		if (ListUtil.isNotEmpty(internal)) {
			for (ReplaceChildVo replaceChildVo : internal) {
				AttendancePostion attendancePostion = attendanceFactory.getChildPostion(replaceChildVo.getId(), day);
				if (attendanceFactory.haveAttendanceOfDay(attendancePostion, day)) {
					continue;
				}
				if (postion.getRoomId().equals(attendancePostion.getRoomId())) {
					internalResult.add(0, replaceChildVo);
				}
			}
		}
		// 计算能替换多少个周期
		// int replaceWeekInstances =
		// attendanceCoreService.getReaplaceInstance(userInfo.getCentersId(),
		// userInfo.getRoomId(), day, absenteeRequestInfo, dayOfWeek);
		// vo.setWeekInstances(replaceWeekInstances);

		// 计算替换的人的可替换周期
		internalResult = attendanceCoreService.getReaplaceInstanceOfChilds(userInfo.getCentersId(), userInfo.getRoomId(), day, absenteeRequestInfo, dayOfWeek,
				internalResult);

		ReplaceChildAttendanceVo vo = new ReplaceChildAttendanceVo();
		vo.setAbsenteeChild(absenteeChild);
		vo.setAbsenteeRequestInfo(absenteeRequestInfo);
		vo.setInternal(internalResult);

		return successResult(vo);
	}

	private ServiceResult<AttendanceReturnVo> getChildAttendanceList(UserInfoVo currentUser, AttendanceCondtion attendanceCondtion) {
		boolean isCeo = containRole(Role.CEO, currentUser.getRoleInfoVoList()) == 1;
		boolean isCentreManager = (containRole(Role.CentreManager, currentUser.getRoleInfoVoList()) == 1)
				|| (containRole(Role.EducatorSecondInCharge, currentUser.getRoleInfoVoList()) == 1);
		boolean isShowAllCentreTree = isCeo || (isCentreManager && StringUtil.isEmpty(currentUser.getUserInfo().getCentersId()));
		// 当两者都为空 但是当前角色不是CEO 那么就有问题
		if (!isShowAllCentreTree && StringUtil.isEmpty(attendanceCondtion.getCentersId()) && StringUtil.isEmpty(attendanceCondtion.getRoomId())) {
			return failResult(-1, getTipMsg("no.permissions"));
		}

		// 如果是這樣必須是該院院長
		if (StringUtil.isNotEmpty(attendanceCondtion.getCentersId())) {
			boolean haveRole = attendanceCoreService.isCenterManager(currentUser, attendanceCondtion.getCentersId(), isCentreManager);
			if (!isShowAllCentreTree && isCentreManager && !haveRole) {
				return failResult(-1, getTipMsg("no.permissions"));
			}
		}

		if (StringUtil.isNotEmpty(attendanceCondtion.getRoomId())) {
			RoomInfo roomInfo = roomInfoMapper.selectByPrimaryKey(attendanceCondtion.getRoomId());
			boolean haveRole = attendanceCoreService.isCenterManager(currentUser, roomInfo.getCentersId(), isCentreManager);
			if (!isShowAllCentreTree && isCentreManager && !haveRole) {
				return failResult(-1, getTipMsg("no.permissions"));
			}
		}

		Date weekDate = null;
		// 如果时间传过来的是空 那么说明前台想回到当前周
		// 查月
		if (attendanceCondtion.getType() == 0) {
			weekDate = DateUtil.parse(attendanceCondtion.getTime() + "/01", "yyyy/MM/dd");
		} // 上周
		else if (attendanceCondtion.getType() == 1) {
			weekDate = DateUtil.addDay(attendanceCondtion.getWeekDate(), -7);
		} // 下周
		else if (attendanceCondtion.getType() == 3) {
			weekDate = DateUtil.addDay(attendanceCondtion.getWeekDate(), 7);
		} else if (attendanceCondtion.getType() == 4) {
			weekDate = attendanceCondtion.getWeekDate();
			if (weekDate == null) {
				weekDate = new Date();
			}
		} else {
			weekDate = new Date();
		}

		// 日期只能下一年
		if (DateUtil.getCalendar(weekDate).get(Calendar.YEAR) > (DateUtil.getCalendar(new Date()).get(Calendar.YEAR) + 1)) {
			return failResult(-1, getTipMsg("no.permissions"));
		}

		Date now = DateUtil.parse(DateUtil.format(new Date(), DateUtil.yyyyMMdd), DateUtil.yyyyMMdd);

		Date weekBegin = DateUtil.getWeekStart(DateUtil.parse(DateUtil.format(weekDate, "yyyy/MM/dd"), "yyyy/MM/dd"));
		if (attendanceCondtion.getType() == 0) {
			if (DateUtil.getMonth(weekBegin) != DateUtil.getMonth(weekDate)) {
				weekBegin = DateUtil.addWeeks(weekBegin, 1);
			}
		}
		// 获取roomId下的总人数
		int roomChildSize = 0;
		if (StringUtil.isNotEmpty(attendanceCondtion.getRoomId())) {
			RoomInfo roomInfo = roomInfoMapper.selectByPrimaryKey(attendanceCondtion.getRoomId());
			roomChildSize = roomInfo.getChildCapacity();
		}

		List<SigninVo> signinInfos = new ArrayList<SigninVo>();

		List<AttendanceDayVo> weekDays = new ArrayList<AttendanceDayVo>();
		// abliu 获取visibility
		Short visibility = getVisibility(currentUser);
		for (int i = 0; i <= 6; i++) {
			Date day = DateUtil.addDay(weekBegin, i);
			int cha = com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(day, now);
			List<AttendanceChildVo> attendanceChilds = new ArrayList<AttendanceChildVo>();
			AttendanceDayVo attendanceDayVo = new AttendanceDayVo();
			attendanceDayVo.setDay(DateUtil.getDayOfMonth(day));
			attendanceDayVo.setEvent(getEvent(day, visibility, currentUser));
			attendanceDayVo.setDate(day);
			attendanceDayVo.setToday(false);
			if (cha == 0) {
				attendanceDayVo.setToday(true);
			}
			// 如果是周末和周六暂时跳过
			if (i == 0 || i == 6) {
				attendanceDayVo.setAttendanceChilds(attendanceChilds);
				weekDays.add(attendanceDayVo);
				continue;
			}

			// 查询临时安排的
			int dayOfWeek = i;

			// 如果是过去时间 那么排课取的是签到表中的数据
			if (cha > 0) {
				signinInfos = signinInfoMapper.getAttendanceLogs(attendanceCondtion.getCentersId(), attendanceCondtion.getRoomId(), day);
			}
			// 如果是当前 那么当天的应该是动态计算出来的
			else if (cha == 0) {
				signinInfos = attendanceCoreService.getTodaySignChilds(attendanceCondtion.getCentersId(), attendanceCondtion.getRoomId(), (short) dayOfWeek, day);
				// 获取自定义临时排课
				List<SigninVo> interims = signinInfoMapper.getInterimList(attendanceCondtion.getCentersId(), attendanceCondtion.getRoomId(), day);
				signinInfos = this.mergeAttend(signinInfos, interims);
			} else {
				signinInfos = attendanceCoreService.getFutureRoomChilds(attendanceCondtion.getCentersId(), attendanceCondtion.getRoomId(), day, (short) dayOfWeek);
				// 获取自定义临时排课
				List<SigninVo> interims = signinInfoMapper.getInterimList(attendanceCondtion.getCentersId(), attendanceCondtion.getRoomId(), day);
				signinInfos = this.mergeAttend(signinInfos, interims);
			}

			// 排序
			attendanceFactory.sortChildArrendance(signinInfos);

			// 获取请假的小孩
			List<LeaveListVo> leaves = attendanceFactory.getLeaveChilds(signinInfos);
			attendanceDayVo.setLeaves(leaves);

			// 处理转换
			attendanceChilds = attendanceFactory.convertToSigninVo(signinInfos, day, dayOfWeek);

			if (cha <= 0) {
				attendanceFactory.fillEmpty(roomChildSize, attendanceChilds);
			}
			attendanceDayVo.setAttendanceChilds(attendanceChilds);

			weekDays.add(attendanceDayVo);
		}
		attendanceCondtion.setTime(DateUtil.format(weekBegin, "yyyy/MM"));
		attendanceCondtion.setWeekDate(weekBegin);
		return successResult(new AttendanceReturnVo(weekDays, attendanceCondtion));
	}

	/**
	 * 合并排课和临时排课
	 * 
	 * @author hxzhang 2019年4月3日
	 * @param signinInfos
	 * @param interims
	 */
	private List<SigninVo> mergeAttend(List<SigninVo> signinInfos, List<SigninVo> interims) {
		if (ListUtil.isEmpty(interims)) {
			return signinInfos;
		}
		List<SigninVo> list = new ArrayList<SigninVo>();
		for (SigninVo vo : signinInfos) {
			boolean repeat = false;
			for (SigninVo i : interims) {
				if (vo.getAccountId().equals(i.getAccountId())) {
					repeat = true;
				}
			}
			if (repeat) {
				continue;
			}
			list.add(vo);
		}
		list.addAll(interims);
		return list;
	}

	@Override
	public ServiceResult<List<AttendanceChildVo>> getTodayAttendanceList(String centersId, String findStr, int type) {
		CentersInfo centersInfo = centersInfoMapper.selectByPrimaryKey(centersId);
		Date day = DateUtil.parse(DateUtil.format(new Date(), "yyyy/MM/dd"), "yyyy/MM/dd");
		int dayOfWeek = DateUtil.getDayOfWeek(day) - 1;
		// 搜索 过滤
		List<AttendanceChildVo> attendanceChilds = new ArrayList<AttendanceChildVo>();

		// 如果是周末和周六暂时跳过
		if (dayOfWeek == 0 || dayOfWeek == 6) {
			return successResult(attendanceChilds);
		}

		List<SigninVo> signinVos = new ArrayList<SigninVo>();
		if (type == 0) {
			signinVos = attendanceCoreService.getTodaySignChilds(centersInfo.getId(), null, (short) dayOfWeek, day);
			attendanceFactory.sortChildArrendance(signinVos);
			attendanceChilds = attendanceFactory.convertToTodaySignInChilds(signinVos, findStr);
		} else {
			signinVos = attendanceCoreService.getTodaySignOutChilds(centersInfo.getId(), day);
			attendanceFactory.sortChildArrendance(signinVos);
			attendanceChilds = attendanceFactory.convertToTodaySignOutChilds(signinVos, findStr);
		}

		return successResult(attendanceChilds);
	}

	private ServiceResult<AttendanceReturnVo> getVistorAttendanceList(UserInfoVo currentUser, AttendanceCondtion attendanceCondtion) {
		Date weekDate = null;
		// 如果时间传过来的是空 那么说明前台想回到当前周
		// 查月
		if (attendanceCondtion.getType() == 0) {
			weekDate = DateUtil.parse(attendanceCondtion.getTime() + "/01", "yyyy/MM/dd");
		} // 上周
		else if (attendanceCondtion.getType() == 1) {
			weekDate = DateUtil.addDay(attendanceCondtion.getWeekDate(), -7);
		} // 下周
		else if (attendanceCondtion.getType() == 3) {
			weekDate = DateUtil.addDay(attendanceCondtion.getWeekDate(), 7);
		} else if (attendanceCondtion.getType() == 4) {
			weekDate = attendanceCondtion.getWeekDate();
		} else {
			weekDate = new Date();
		}

		// 日期只能下一年
		if (DateUtil.getCalendar(weekDate).get(Calendar.YEAR) > (DateUtil.getCalendar(new Date()).get(Calendar.YEAR) + 1)) {
			return failResult(-1, getTipMsg("no.permissions"));
		}

		Date now = DateUtil.parse(DateUtil.format(new Date(), DateUtil.yyyyMMdd), DateUtil.yyyyMMdd);

		Date weekBegin = DateUtil.getWeekStart(DateUtil.parse(DateUtil.format(weekDate, "yyyy/MM/dd"), "yyyy/MM/dd"));
		if (attendanceCondtion.getType() == 0) {
			if (DateUtil.getMonth(weekBegin) != DateUtil.getMonth(weekDate)) {
				weekBegin = DateUtil.addWeeks(weekBegin, 1);
			}
		}
		List<AttendanceDayVo> weekDays = new ArrayList<AttendanceDayVo>();
		// abliu 获取visibility
		Short visibility = getVisibility(currentUser);
		for (int i = 0; i <= 6; i++) {
			Date day = DateUtil.addDay(weekBegin, i);
			int cha = com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(day, now);
			AttendanceDayVo _AttendanceDayVo = new AttendanceDayVo();
			_AttendanceDayVo.setDay(DateUtil.getDayOfMonth(day));
			_AttendanceDayVo.setEvent(getEvent(day, visibility, currentUser));
			_AttendanceDayVo.setDate(day);
			_AttendanceDayVo.setToday(false);
			if (cha == 0) {
				_AttendanceDayVo.setToday(true);
			}

			List<SigninVo> signinInfos = new ArrayList<SigninVo>();
			// 如果是过去时间 那么排课取的是签到表中的数据
			if (cha > 0) {
				signinInfos = signinInfoMapper.getVisitorAttendanceLogs(attendanceCondtion.getCentersId(), day);
			}
			// 如果是当前 那么当天的应该是动态计算出来的
			else if (cha == 0) {
				signinInfos = signinInfoMapper.getVisitorAttendanceLogs(attendanceCondtion.getCentersId(), day);
			}
			// 如果是未来 那么应该是应该上课的加上临时替换上课 的
			else {

			}
			_AttendanceDayVo.setSigns(signinInfos);
			weekDays.add(_AttendanceDayVo);
		}
		attendanceCondtion.setTime(DateUtil.format(weekBegin, "yyyy/MM"));
		attendanceCondtion.setWeekDate(weekBegin);
		return successResult(new AttendanceReturnVo(weekDays, attendanceCondtion));
	}

	@Override
	public ServiceResult<AttendanceReturnVo> getAttendanceList(UserInfoVo currentUser, AttendanceCondtion attendanceCondtion) {
		if (attendanceCondtion.getListType() == AttendanceType.Child.getValue()) {
			return getChildAttendanceList(currentUser, attendanceCondtion);
		} else if (attendanceCondtion.getListType() == AttendanceType.Vistor.getValue()) {
			return getVistorAttendanceList(currentUser, attendanceCondtion);
		} else if (attendanceCondtion.getListType() == AttendanceType.Staff.getValue()) {
			return getStaffAttendanceList(currentUser, attendanceCondtion);
		}
		return successResult();
	}

	private ServiceResult<ChildAttendanceInfo> validateRequestAuthority(UserInfoVo currentUser, UserInfo childInfo, ChildAttendanceInfo attendanceInfo,
			ChangeAttendanceRequestInfo re, boolean coverPartialAproved) {
		boolean isCeo = containRole(Role.CEO, currentUser.getRoleInfoVoList()) == 1;
		boolean isCentreManager = (containRole(Role.CentreManager, currentUser.getRoleInfoVoList()) == 1)
				|| (containRole(Role.EducatorSecondInCharge, currentUser.getRoleInfoVoList()) == 1);
		boolean isParent = (containRole(Role.Parent, currentUser.getRoleInfoVoList()) == 1);
		boolean isAdmin = isCeo || isCentreManager;

		// 同一天请求不能存在两个
		if (StringUtil.isEmpty(re.getId())) {
			int oneDaySize = changeAttendanceRequestInfoMapper.getRequestOfDay(re);
			if (oneDaySize > 0) {
				return failResult(-1, getTipMsg("submitChangeAttendanceRequest.day.allready"));
			}
		}

		// 如果处理时间大于=离园时间 则给很出提示
		if (attendanceInfo.getLeaveDate() != null && com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(re.getChangeDate(), attendanceInfo.getLeaveDate()) <= 0) {
			return failResult(1001, getTipMsg("child.attendance.approveAdd.haveLeverequest"));
		}

		AttendanceRequestType attendanceRequestType = AttendanceRequestType.getType(re.getType());
		switch (attendanceRequestType) {
		case K: {
			// 如果是家长申请
			if (isParent) {
				// 只能修改自己家族小孩的
				List<UserInfo> childInfos = userInfoMapper.getChildInfoByFamilyId(currentUser.getUserInfo().getFamilyId());
				if (ListUtil.isEmpty(childInfos)) {
					return failResult(-1, getTipMsg("no.permissions"));
				}
				boolean isMyFamily = false;
				for (UserInfo item : childInfos) {
					AccountInfo accountInfo = accountInfoMapper.getAccountInfoByUserId(item.getId());
					if (accountInfo.getId().equals(re.getChildId())) {
						isMyFamily = true;
						break;
					}
				}
				if (!isMyFamily) {
					return failResult(-1, getTipMsg("no.permissions"));
				}

				try {
					// 如果存在相同请求 那么提示
					// 如果是家长申请 那么如果有和之前一样的 那么只生效不一样的
					ChangeAttendanceRequestInfo image = new ChangeAttendanceRequestInfo();
					PropertyUtils.copyProperties(image, re);
					attendanceFactory.dealParentAttendance(attendanceInfo, image);
					int sameSize = changeAttendanceRequestInfoMapper.haveSameRequest(image);
					if (sameSize > 0) {
						return failResult(102, getTipMsg("submitChangeAttendanceRequest.allready"));
					}
				} catch (Exception e) {
				}

				// 看是否已有未审批的申请 如果有则不给申请
				List<ChangeAttendanceRequestInfo> attendanceRequestInfos = changeAttendanceRequestInfoMapper.getListByChildAndStateAndType(re.getChildId(),
						ChangeAttendanceRequestType.ParentRequest.getValue(), ChangeAttendanceRequestState.Default.getValue());
				if (ListUtil.isNotEmpty(attendanceRequestInfos)) {
					return failResult(101, getTipMsg("submitChangeAttendanceRequest.allready"));
				}

				// 看看是否有部分审批的申请 如果则提示是否确认
				attendanceRequestInfos = changeAttendanceRequestInfoMapper.getListByChildAndState(re.getChildId(),
						ChangeAttendanceRequestState.PartialAproved.getValue());
				if (!coverPartialAproved && ListUtil.isNotEmpty(attendanceRequestInfos)) {
					return failResult(3, getTipMsg("submitChangeAttendanceRequest.allready.dealSame"));
				}
			} else {
				// 管理员申请
				if (!isAdmin) {
					return failResult(-1, getTipMsg("no.permissions"));
				}
				// 不是CEO的话需要判断是否是自己管辖的room的
				if (isCentreManager) {
					if (!childInfo.getCentersId().equals(currentUser.getUserInfo().getCentersId())) {
						return failResult(-1, getTipMsg("no.permissions"));
					}
				}
			}
		}
			break;
		case R: {
			// 管理员申请
			if (!isAdmin) {
				return failResult(-1, getTipMsg("no.permissions"));
			}
			// 不是CEO的话需要判断是否是自己管辖的room的
			if (isCentreManager) {
				String centreId = roomInfoMapper.selectByPrimaryKey(re.getNewRoomId()).getCentersId();
				if (!centreId.equals(currentUser.getUserInfo().getCentersId())) {
					return failResult(-1, getTipMsg("no.permissions"));
				}
			}
		}
			break;
		case C: {
			if (isCentreManager) {
				if (StringUtil.isEmpty(childInfo.getCentersId())) {
					break;
				}
				// 园长申请
				if (StringUtil.isEmpty(re.getId())) {
					if (!childInfo.getCentersId().equals(currentUser.getUserInfo().getCentersId())) {
						return failResult(-1, getTipMsg("no.permissions"));
					}
				} else {
					if (!re.getNewCentreId().equals(currentUser.getUserInfo().getCentersId())) {
						return failResult(-1, getTipMsg("no.permissions"));
					}
				}
			}
		}
			break;
		default:
			break;
		}
		return successResult();
	}

	@Override
	public ServiceResult<ChildAttendanceInfo> submitChangeAttendanceRequest(UserInfoVo currentUser, ChangeAttendanceRequestInfo attendanceRequestInfo, boolean isOverOld,
			boolean isFullOver) {
		UserInfo childInfo = userInfoMapper.getUserInfoByAccountId(attendanceRequestInfo.getChildId());
		if (childInfo == null) {
			return failResult(-1, getTipMsg("submitChangeAttendanceRequest.childInfo.null"));
		}
		// 所转园区和孩子园区一致,不能转园.
		if (attendanceRequestInfo.getType() == AttendanceRequestType.C.getValue() && StringUtil.isNotEmpty(childInfo.getCentersId())
				&& childInfo.getCentersId().equals(attendanceRequestInfo.getNewCentreId())) {
			return failResult(getTipMsg("change_attendance_select_centre_error"));
		}

		ChildAttendanceInfo attendanceInfo = childAttendanceInfoMapper.selectByUserId(childInfo.getId());

		// 验证请求权限和合法性
		ServiceResult<ChildAttendanceInfo> validateResult = validateRequestAuthority(currentUser, childInfo, attendanceInfo, attendanceRequestInfo, isOverOld);
		if (!validateResult.isSuccess()) {
			return validateResult;
		}

		// 验证数据影响
		validateResult = validateRequestInfluence(currentUser, childInfo, attendanceInfo, attendanceRequestInfo);
		if (!validateResult.isSuccess()) {
			return validateResult;
		}

		// 填充相关初始化值
		fillRequestData(currentUser, childInfo, attendanceInfo, attendanceRequestInfo);

		// 看看是否满园
		validateResult = validateFutureRoomFull(childInfo, attendanceRequestInfo, isFullOver);
		if (!validateResult.isSuccess()) {
			return validateResult;
		}

		// 处理请求
		dealRequest(currentUser, childInfo, attendanceInfo, attendanceRequestInfo);

		attendanceInfo.setRequestId(attendanceRequestInfo.getId());

		RequestLogInfo logInfo = requestLogInfoMapper.getRequestLogInfoByObjId(attendanceRequestInfo.getId());
		if (logInfo != null) {
			logInfo.setUpdateTime(new Date());
			logInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
			requestLogInfoMapper.updateByPrimaryKeySelective(logInfo);
		}

		return successResult(attendanceInfo);
	}

	private ServiceResult<ChildAttendanceInfo> validateFutureRoomFull(UserInfo childInfo, ChangeAttendanceRequestInfo re, boolean isFullOver) {
		ChangeAttendanceRequestType type = ChangeAttendanceRequestType.getType(re.getSourceType());
		boolean checkRoomSize = (type != ChangeAttendanceRequestType.ParentRequest)
				&& !((type == ChangeAttendanceRequestType.CentreManagerRequest) && (re == null || StringUtil.isEmpty(re.getId())));

		if (checkRoomSize && !BooleanUtils.isTrue(isFullOver)) {
			if (StringUtil.isNotEmpty(childInfo.getCentersId()) && StringUtil.isNotEmpty(childInfo.getRoomId())) {
				List<Short> dayOfWeeks = attendanceFactory.getDayOfWeeks(childInfo.getId(), re);
				String roomId = StringUtil.isNotEmpty(re.getNewRoomId()) ? re.getNewRoomId() : childInfo.getRoomId();

				StringBuilder sb = new StringBuilder();
				for (Short tempDayOfWeeks : dayOfWeeks) {
					boolean isFull = attendanceFactory.futureRoomFull(childInfo.getCentersId(), roomId, re.getChangeDate(), (short) tempDayOfWeeks);
					if (isFull) {
						sb.append(TipMsgConstants.WEEKNAMES[tempDayOfWeeks - 1] + ",");
					}
				}
				if (StringUtil.isNotEmpty(sb.toString())) {
					return failResult(11, MessageFormat.format(getTipMsg("child.attendance.approveAdd.over"), StringUtil.trimEnd(sb.toString(), ",")));
				}
			}
		}
		return successResult();
	}

	private ServiceResult<ChildAttendanceInfo> validateRequestInfluence(UserInfoVo currentUser, UserInfo childInfo, ChildAttendanceInfo attendanceInfo,
			ChangeAttendanceRequestInfo re) {
		boolean isCeo = containRole(Role.CEO, currentUser.getRoleInfoVoList()) == 1;
		boolean isCentreManager = (containRole(Role.CentreManager, currentUser.getRoleInfoVoList()) == 1)
				|| (containRole(Role.EducatorSecondInCharge, currentUser.getRoleInfoVoList()) == 1);
		boolean isAdmin = isCeo || isCentreManager;
		AttendanceRequestType attendanceRequestType = AttendanceRequestType.getType(re.getType());
		switch (attendanceRequestType) {
		case K: {
			if (isAdmin) {
				// 判断之前有没有RC
				int size = changeAttendanceRequestInfoMapper.getBeforeGreaterThanRequest(re);
				if (size > 0) {
					return failResult(-1, MessageFormat.format(getTipMsg("submitChangeAttendanceRequest.allready.beforegreaterthan.rc"),
							DateUtil.format(re.getChangeDate(), DateFormatterConstants.SYS_FORMAT2)));
				}
			}
		}
			break;
		case R: {
			// 判断之前有没有C
			int size = changeAttendanceRequestInfoMapper.getBeforeGreaterThanRequest(re);
			if (size > 0) {
				return failResult(-1, MessageFormat.format(getTipMsg("submitChangeAttendanceRequest.allready.beforegreaterthan.c"),
						DateUtil.format(re.getChangeDate(), DateFormatterConstants.SYS_FORMAT2)));
			}

			// 判断之后有没有K
			size = changeAttendanceRequestInfoMapper.getAfterLessGreaterThanRequest(re);
			if (size > 0) {
				return failResult(-1, MessageFormat.format(getTipMsg("submitChangeAttendanceRequest.allready.afterlessthan.k"),
						DateUtil.format(re.getChangeDate(), DateFormatterConstants.SYS_FORMAT2)));
			}
		}
			break;
		case C: {
			// 判断之后有没有K
			int size = changeAttendanceRequestInfoMapper.getAfterLessGreaterThanRequest(re);
			if (size > 0) {
				return failResult(-1, MessageFormat.format(getTipMsg("submitChangeAttendanceRequest.allready.afterlessthan.rk"),
						DateUtil.format(re.getChangeDate(), DateFormatterConstants.SYS_FORMAT2)));
			}
		}
			break;
		}

		// 判断请假
		switch (attendanceRequestType) {
		case C: {
			List<AbsenteeRequestInfoWithBLOBs> absenteeRequestInfoWithBLOBs = absenteeRequestInfoMapper.getAbsenteesByDate(re.getChildId(), re.getChangeDate());
			if (ListUtil.isNotEmpty(absenteeRequestInfoWithBLOBs)) {
				return failResult(-1, getTipMsg("submitChangeAttendanceRequest.allready.absenteerequest"));
			}
		}
			break;
		default:
			break;
		}
		return successResult();
	}

	private void fillRequestData(UserInfoVo currentUser, UserInfo childInfo, ChildAttendanceInfo attendanceInfo, ChangeAttendanceRequestInfo re) {
		boolean isCeo = containRole(Role.CEO, currentUser.getRoleInfoVoList()) == 1;
		boolean isCentreManager = (containRole(Role.CentreManager, currentUser.getRoleInfoVoList()) == 1)
				|| (containRole(Role.EducatorSecondInCharge, currentUser.getRoleInfoVoList()) == 1);
		boolean isParent = (containRole(Role.Parent, currentUser.getRoleInfoVoList()) == 1);
		AttendanceRequestType attendanceRequestType = AttendanceRequestType.getType(re.getType());

		if (StringUtil.isEmpty(re.getId())) {
			re.setCreateAccountId(currentUser.getAccountInfo().getId());
			re.setCreateTime(new Date());
			re.setDeleteFlag(DeleteFlag.Default.getValue());
			re.setUpdateAccountId(currentUser.getAccountInfo().getId());
			re.setUpdateTime(new Date());
		} else {
			ChangeAttendanceRequestInfo old = changeAttendanceRequestInfoMapper.selectByPrimaryKey(re.getId());
			old.setUpdateAccountId(currentUser.getAccountInfo().getId());
			old.setUpdateTime(new Date());
			old.setState(re.getState());
			old.setNewCentreId(re.getNewCentreId());
			old.setNewRoomId(re.getNewRoomId());
			old.setNewGroupId(re.getNewGroupId());
			old.setMonday(re.getMonday());
			old.setTuesday(re.getTuesday());
			old.setWednesday(re.getWednesday());
			old.setThursday(re.getThursday());
			old.setFriday(re.getFriday());
			old.setApprovalAccountId(currentUser.getAccountInfo().getId());
			try {
				PropertyUtils.copyProperties(re, old);
			} catch (Exception e) {
			}
		}

		switch (attendanceRequestType) {
		case K: {
			if (isParent) {
				re.setState(ChangeAttendanceRequestState.Default.getValue());
				// 如果是家长申请 那么如果有和之前一样的 那么只生效不一样的
				// attendanceFactory.dealParentAttendance(attendanceInfo, re);
			} else {
				re.setState(ChangeAttendanceRequestState.OnJob.getValue());

				AttendancePostion attendancePostion = attendanceFactory.getChildPostion(re.getChildId(), re.getChangeDate());
				re.setNewCentreId(attendancePostion.getCentreId());
				re.setNewRoomId(attendancePostion.getRoomId());
				re.setNewGroupId(attendancePostion.getGroupId());
			}
		}
			break;
		case R: {
			re.setState(ChangeAttendanceRequestState.OnJob.getValue());
			AttendancePostion attendancePostion = attendanceFactory.getChildPostion(re.getChildId(), re.getChangeDate());
			re.setNewCentreId(attendancePostion.getCentreId());
		}
			break;
		case C: {
			if (isCeo) {
				re.setState(ChangeAttendanceRequestState.OnJob.getValue());
			} else {
				if (StringUtil.isEmpty(re.getId())) {
					if (isCentreManager && re.getSourceType() == RequestLogType.CEOExternal2Inside.getValue()) {
						re.setState(ChangeAttendanceRequestState.OnJob.getValue());
					} else {
						re.setState(ChangeAttendanceRequestState.Default.getValue());
					}
				} else {
					re.setState(ChangeAttendanceRequestState.OnJob.getValue());
				}
			}
		}
			break;
		}
	}

	private void dealRequest(UserInfoVo currentUser, UserInfo childInfo, ChildAttendanceInfo attendanceInfo, ChangeAttendanceRequestInfo re) {
		boolean isCentreManager = (containRole(Role.CentreManager, currentUser.getRoleInfoVoList()) == 1)
				|| (containRole(Role.EducatorSecondInCharge, currentUser.getRoleInfoVoList()) == 1);
		boolean isParent = (containRole(Role.Parent, currentUser.getRoleInfoVoList()) == 1);
		AttendanceRequestType attendanceRequestType = AttendanceRequestType.getType(re.getType());

		// 判断是否是家长进行了取消上课周期的操作
		if (isParent) {
			attendanceFactory.dealParentCancel(re);
		}

		// 删除影响的数据
		deleteDataOfChange(re);

		// 是否要立即生效
		// 入园时间是今天 不是家长 不是园长新增或者是园长外到内 是centre发生变化
		boolean dealNow = (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(re.getChangeDate(), new Date()) == 0) && !isParent
				&& !(isCentreManager && StringUtil.isEmpty(re.getId()) && attendanceRequestType == AttendanceRequestType.C) && !(isCentreManager
						&& re.getSourceType() == ChangeAttendanceRequestType.CEOExternal2Inside.getValue() && attendanceRequestType == AttendanceRequestType.C);

		AttendancePostion attendancePostion = attendanceFactory.getChildPostion(re.getChildId(), re.getChangeDate());

		boolean isCentreManagerSure = false;
		if (StringUtil.isEmpty(re.getId())) {
			re.setId(UUID.randomUUID().toString());
			changeAttendanceRequestInfoMapper.insert(re);
		} else {
			re.setState(ChangeAttendanceRequestState.OnJob.getValue());
			changeAttendanceRequestInfoMapper.updateByPrimaryKey(re);
			isCentreManagerSure = true;
		}

		// 处理改变空间
		attendanceFactory.dealChangeRoomWithEmail(childInfo, re, currentUser);

		// 如果是外部的小孩 从外到内
		dealExternal2Inside(dealNow, attendanceInfo, childInfo, re);

		if (dealNow) {
			attendanceCoreService.dealAttendanceNow(currentUser, childInfo, attendanceInfo, re);
		} else {
			dealRoomChange(attendancePostion, re);
		}

		if (!isCentreManagerSure) {
			// 记录日志
			requestLogService.dealLog(currentUser, re);
		}

		// 如果是上课周期改变了 切不是家长的改变
		if (attendanceRequestType == AttendanceRequestType.K && re.getState() == ChangeAttendanceRequestState.OnJob.getValue()) {
			attendanceCoreService.dealParentRequest(re.getChildId(), re);
		}
	}

	private void dealExternal2Inside(Boolean dealNow, ChildAttendanceInfo attendanceInfo, UserInfo childInfo, ChangeAttendanceRequestInfo re) {
		if (re.getSourceType() != ChangeAttendanceRequestType.CEOExternal2Inside.getValue()) {
			return;
		}
		if (dealNow) {
			attendanceInfo.setEnrolled(EnrolledState.Enrolled.getValue());
		}
		attendanceInfo.setChangeDate(re.getChangeDate());
		attendanceInfo.setEnrolmentDate(re.getChangeDate());
		attendanceInfo.setType(ChildType.Inside.getValue());
		attendanceInfo.setCenterId(re.getNewCentreId());
		attendanceInfo.setOrientation(re.getOrientation());

		// 同步上课周期,解决第一次未来时间入园管理员无法改变上课周期及邮件上课周期
		attendanceFactory.dealAttendance(attendanceInfo, re);

		childInfo.setCentersId(re.getNewCentreId());
		childInfo.setRoomId(re.getNewRoomId());
		childInfo.setGroupId(re.getNewGroupId());
		userInfoMapper.updateByPrimaryKey(childInfo);

		AccountInfo accountInfo = accountInfoMapper.selectByPrimaryKey(re.getChildId());
		if (dealNow) {
			accountInfo.setStatus(ArchivedStatus.UnArchived.getValue());
		}
		accountInfoMapper.updateByPrimaryKey(accountInfo);

		childAttendanceInfoMapper.updateByPrimaryKey(attendanceInfo);
		childAttendanceInfoMapper.setEnrolledFlag(attendanceInfo.getUserId());

		// 小孩从外部到内部给家长发送从外部到内部邮件
		attendanceInfo.setPlan(re.isPlan());
		userFactory.sendParentEmailByChild(getSystemEmailMsg("child_inside_title"), getSystemEmailMsg("child_inside_content"), re.getChildId(), true,
				EmailType.inside.getValue(), null, null, attendanceInfo, attendanceInfo.isPlan());
		// 发送欢迎邮件
		familyService.sendEmailForParent(childInfo, true, re.isPlan());

	}

	private void dealRoomChange(AttendancePostion attendancePostion, ChangeAttendanceRequestInfo re) {
		// 插入影响的安排
		if (re.getState() == ChangeAttendanceRequestState.OnJob.getValue()) {
			if (attendancePostion != null) {
				List<RoomChangeInfo> removeChangeInfos = attendanceFactory.getRemoveRoomChangeInfoList(re.getChildId(), re.getId(), attendancePostion,
						re.getChangeDate());
				for (RoomChangeInfo removeChangeInfo : removeChangeInfos) {
					roomChangeInfoMapper.insert(removeChangeInfo);
				}
			}

			List<RoomChangeInfo> addChangeInfos = attendanceFactory.getRoomChangeInfoList(re);
			for (RoomChangeInfo roomChangeInfo : addChangeInfos) {
				roomChangeInfoMapper.insert(roomChangeInfo);
			}
		}
	}

	/**
	 * 删除可能会导致影响的数据
	 * 
	 * @author dlli5 at 2016年12月14日上午9:02:13
	 * @param re
	 */
	private void deleteDataOfChange(ChangeAttendanceRequestInfo re) {
		AttendanceRequestType attendanceRequestType = AttendanceRequestType.getType(re.getType());
		switch (attendanceRequestType) {
		case K: {

		}
			break;
		case R: {
			// 删除后面的K
			List<ChangeAttendanceRequestInfo> afterRequestInfos = changeAttendanceRequestInfoMapper.getListByChildAndDateScope(re.getChildId(), re.getChangeDate(), null);
			if (ListUtil.isNotEmpty(afterRequestInfos)) {
				for (ChangeAttendanceRequestInfo change : afterRequestInfos) {
					if (change.getType() != AttendanceRequestType.K.getValue()) {
						break;
					}
					change.setState(ChangeAttendanceRequestState.Cancel.getValue());
					changeAttendanceRequestInfoMapper.updateByPrimaryKey(change);
					roomChangeInfoMapper.deleteByReqId(change.getId(), null);
				}
			}
		}
			break;
		case C: {
			// 删除后面的R K
			List<ChangeAttendanceRequestInfo> afterRequestInfos = changeAttendanceRequestInfoMapper.getListByChildAndDateScope(re.getChildId(), re.getChangeDate(), null);
			if (ListUtil.isNotEmpty(afterRequestInfos)) {
				for (ChangeAttendanceRequestInfo change : afterRequestInfos) {
					if (change.getType() != AttendanceRequestType.R.getValue() && change.getType() != AttendanceRequestType.K.getValue()) {
						break;
					}
					change.setState(ChangeAttendanceRequestState.Cancel.getValue());
					changeAttendanceRequestInfoMapper.updateByPrimaryKey(change);
					roomChangeInfoMapper.deleteByReqId(change.getId(), null);
				}
			}
		}
			break;
		}
	}

	@Override
	public Integer getChildAttendanceDays(String accountId, Date beginDate, Date endDate) {
		if (beginDate == null || endDate == null) {
			return 0;
		}
		Date now = new Date();
		boolean isOld = com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(endDate, now) > 0;
		boolean isOldNow = com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(beginDate, now) >= 0
				&& com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(endDate, now) <= 0;
		boolean isFuture = com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(beginDate, now) < 0;
		// 如果是历史
		if (isOld) {
			return getOld(accountId, beginDate, endDate);
		}
		// 如果是未来
		if (isFuture) {
			return getFuture(accountId, beginDate, endDate);
		}
		if (isOldNow) {
			return getOld(accountId, beginDate, DateUtil.addDay(now, -1)) + getFuture(accountId, now, endDate);
		}
		return 0;
	}

	private ServiceResult<AttendanceReturnVo> getStaffAttendanceList(UserInfoVo currentUser, AttendanceCondtion attendanceCondtion) {
		Date weekDate = null;
		// 如果时间传过来的是空 那么说明前台想回到当前周
		// 查月
		if (attendanceCondtion.getType() == 0) {
			weekDate = DateUtil.parse(attendanceCondtion.getTime() + "/01", "yyyy/MM/dd");
		} // 上周
		else if (attendanceCondtion.getType() == 1) {
			weekDate = DateUtil.addDay(attendanceCondtion.getWeekDate(), -7);
		} // 下周
		else if (attendanceCondtion.getType() == 3) {
			weekDate = DateUtil.addDay(attendanceCondtion.getWeekDate(), 7);
		} else if (attendanceCondtion.getType() == 4) {
			weekDate = attendanceCondtion.getWeekDate();
		} else {
			weekDate = new Date();
		}

		// 日期只能下一年
		if (DateUtil.getCalendar(weekDate).get(Calendar.YEAR) > (DateUtil.getCalendar(new Date()).get(Calendar.YEAR) + 1)) {
			return failResult(-1, getTipMsg("no.permissions"));
		}

		Date now = DateUtil.parse(DateUtil.format(new Date(), DateUtil.yyyyMMdd), DateUtil.yyyyMMdd);

		Date weekBegin = DateUtil.getWeekStart(DateUtil.parse(DateUtil.format(weekDate, "yyyy/MM/dd"), "yyyy/MM/dd"));
		if (attendanceCondtion.getType() == 0) {
			if (DateUtil.getMonth(weekBegin) != DateUtil.getMonth(weekDate)) {
				weekBegin = DateUtil.addWeeks(weekBegin, 1);
			}
		}
		List<AttendanceDayVo> weekDays = new ArrayList<AttendanceDayVo>();
		// abliu 获取visibility
		Short visibility = getVisibility(currentUser);
		for (int i = 0; i <= 6; i++) {
			Date day = DateUtil.addDay(weekBegin, i);
			int cha = com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(day, now);
			AttendanceDayVo _AttendanceDayVo = new AttendanceDayVo();
			_AttendanceDayVo.setDay(DateUtil.getDayOfMonth(day));
			_AttendanceDayVo.setEvent(getEvent(day, visibility, currentUser));
			_AttendanceDayVo.setDate(day);
			_AttendanceDayVo.setToday(false);
			if (cha == 0) {
				_AttendanceDayVo.setToday(true);
			}

			List<SigninVo> signinInfos = new ArrayList<SigninVo>();
			List<LeaveListVo> leaves = new ArrayList<LeaveListVo>();
			signinInfos = signinInfoMapper.getStaffAttendanceLogs(attendanceCondtion.getCentersId(), attendanceCondtion.getRoomId(), day);
			if (isRole(currentUser, Role.CEO.getValue())) {
				attendanceCondtion.setDay(day);
				attendanceCondtion.setCeo(true);
			} else if ((isRole(currentUser, Role.CentreManager.getValue()) || isRole(currentUser, Role.EducatorSecondInCharge.getValue()))
					&& !isRole(currentUser, Role.Casual.getValue())) {
				// attendanceCondtion.setCentersId(currentUser.getUserInfo().getCentersId());
				attendanceCondtion.setDay(day);
				if (currentUser.getUserInfo().getCentersId().equals(attendanceCondtion.getCentersId())) {
					attendanceCondtion.setCurrentCentreManager(true);
				} else {
					attendanceCondtion.setCurrentCentreManager(false);
				}
			} else if ((isRole(currentUser, Role.CentreManager.getValue()) || isRole(currentUser, Role.EducatorSecondInCharge.getValue()))
					&& isRole(currentUser, Role.Casual.getValue())) {
				// 兼职园长,只能查看自己园区人员的相关信息
				if (currentUser.getUserInfo().getCentersId().equals(attendanceCondtion.getCentersId())) {
					attendanceCondtion.setDay(day);
					attendanceCondtion.setCurrentCentreManager(true);
				} else {
					attendanceCondtion.setCentersId(null);
					attendanceCondtion.setCurrentCentreManager(false);
				}
			} else if (isRole(currentUser, Role.Parent.getValue())) {
				attendanceCondtion.setCentreIds(currentUser.getParentCenterIds());
				attendanceCondtion.setDay(day);
			} else {
				attendanceCondtion.setAccountId(currentUser.getAccountInfo().getId());
				attendanceCondtion.setDay(day);
			}
			leaves = leaveInfoMapper.getStaffLeaves(attendanceCondtion);

			_AttendanceDayVo.setSigns(signinInfos);
			_AttendanceDayVo.setLeaves(leaves);

			weekDays.add(_AttendanceDayVo);
		}
		attendanceCondtion.setTime(DateUtil.format(weekBegin, "yyyy/MM"));
		attendanceCondtion.setWeekDate(weekBegin);
		return successResult(new AttendanceReturnVo(weekDays, attendanceCondtion));
	}

	/**
	 * @description 判断用户角色
	 * @author hxzhang
	 * @create 2016年10月20日下午3:01:39
	 */
	private boolean isRole(UserInfoVo currentUser, Short role) {
		List<RoleInfoVo> roleList = currentUser.getRoleInfoVoList();
		for (RoleInfoVo roleInfoVo : roleList) {
			if (role.compareTo(roleInfoVo.getValue()) == 0) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 获取event所需要的visibility
	 * 
	 * @param currentUser
	 * @return
	 */
	private Short getVisibility(UserInfoVo currentUser) {
		// 获取当前属于什么角色
		List<RoleInfoVo> roleInfoVoList = currentUser.getRoleInfoVoList();
		boolean isCEOORCentreManager = false;
		Short visibility = null;
		// 设置当前登录者角色集合
		for (int j = 0, m = roleInfoVoList.size(); j < m; j++) {
			// 判断是否是CEO或者园长 二级院长
			if (roleInfoVoList.get(j).getValue() == Role.CEO.getValue() || roleInfoVoList.get(j).getValue() == Role.CentreManager.getValue()
					|| roleInfoVoList.get(j).getValue() == Role.EducatorSecondInCharge.getValue()) {
				isCEOORCentreManager = true;
				break;
			}
		}
		if (!isCEOORCentreManager) {
			// 判断当前员工是家长:1 还是员工:0
			if (UserType.Parent.getValue().equals(currentUser.getUserInfo().getUserType())) {
				visibility = (short) 2;
			}
			if (UserType.Staff.getValue().equals(currentUser.getUserInfo().getUserType().shortValue())) {
				visibility = (short) 1;
			}
		}
		return visibility;
	}

	/**
	 * 获取event
	 * 
	 * @param day
	 * @param visibility
	 * @param currentUser
	 * @return
	 */
	private String getEvent(Date day, Short visibility, UserInfoVo currentUser) {
		// 获取event的信息
		List<EventInfo> eventList = new ArrayList<EventInfo>();
		eventList = eventInfoMapper.getListByWhere(day, visibility, currentUser.getAccountInfo().getId(), "");
		String eventTitle = "";
		if (eventList.size() == 1) {
			eventTitle = eventList.get(0).getTitle();
		} else {
			for (int j = 0; j < eventList.size(); j++) {
				eventTitle += eventList.get(j).getTitle() + "</br>";
			}
		}
		return eventTitle;
	}

	@Override
	public ServiceResult<Object> getList(UserInfoVo currentUser, AttendanceManagementCondition condition) {
		List<Short> selectType = new ArrayList<Short>();
		condition.setSelectType(selectType);
		List<Short> selectStatus = new ArrayList<Short>();
		condition.setSelectStatus(selectStatus);
		// 处理condition
		dealAttendanceManagementType(condition);
		dealAttendanceManagementStatus(condition);

		condition.setCenterId(currentUser.getUserInfo().getCentersId());
		// 将查询第一页设置为当前页.
		condition.setStartSize(condition.getPageIndex());

		List<RequestLogVo> list = requestLogInfoMapper.getPagarList(condition);
		for (RequestLogVo vo : list) {
			ChildInfoVo childInfoVo = new ChildInfoVo();
			UserInfo userInfo = userInfoMapper.selectByPrimaryKey(userInfoMapper.getUserInfoByAccountId(vo.getChildId()).getId());
			AccountInfo accountInfo = accountInfoMapper.selectByPrimaryKey(vo.getChildId());
			ChangeAttendanceRequestInfo requestInfo = changeAttendanceRequestInfoMapper.selectByPrimaryKey(vo.getObjId());
			childInfoVo.setUserInfo(userInfo);
			childInfoVo.setAccountInfo(accountInfo);
			vo.setChildVo(childInfoVo);
			vo.setRequestInfo(requestInfo);
		}
		int totalSize = requestLogInfoMapper.getPagarListCount(condition);
		condition.setTotalSize(totalSize);
		Pager<RequestLogVo, AttendanceManagementCondition> pager = new Pager<RequestLogVo, AttendanceManagementCondition>(list, condition);
		return successResult((Object) pager);
	}

	private void dealAttendanceManagementType(AttendanceManagementCondition condition) {
		if (StringUtil.isEmpty(condition.getTypeStr())) {
			return;
		}
		List<Short> selectType = new ArrayList<Short>();
		if (AttendanceManagementType.GivingNotice.getDesc().toLowerCase().contains(condition.getTypeStr().toLowerCase())) {
			selectType.add(AttendanceManagementType.GivingNotice.getValue());
		}
		if (AttendanceManagementType.AbsenceRequest.getDesc().toLowerCase().contains(condition.getTypeStr().toLowerCase())) {
			selectType.add(AttendanceManagementType.AbsenceRequest.getValue());
		}
		if (AttendanceManagementType.ChangeofAttendance.getDesc().toLowerCase().contains(condition.getTypeStr().toLowerCase())) {
			selectType.add(AttendanceManagementType.ChangeofAttendance.getValue());
		}
		if (AttendanceManagementType.ChangeofCentre.getDesc().toLowerCase().contains(condition.getTypeStr().toLowerCase())) {
			selectType.add(AttendanceManagementType.ChangeofCentre.getValue());
		}
		if (AttendanceManagementType.ChangeofRoom.getDesc().toLowerCase().contains(condition.getTypeStr().toLowerCase())) {
			selectType.add(AttendanceManagementType.ChangeofRoom.getValue());
		}
		if (ListUtil.isEmpty(selectType)) {
			selectType.add(AttendanceManagementType.Default.getValue());
		}
		condition.setSelectType(selectType);
	}

	private void dealAttendanceManagementStatus(AttendanceManagementCondition condition) {
		if (StringUtil.isEmpty(condition.getStatus())) {
			return;
		}
		List<Short> selectStatus = new ArrayList<Short>();
		if (AttendanceManagementStatus.Pending.getDesc().toLowerCase().contains(condition.getStatus().toLowerCase())) {
			selectStatus.add(AttendanceManagementStatus.Pending.getValue());
		}
		if (AttendanceManagementStatus.Approved.getDesc().toLowerCase().contains(condition.getStatus().toLowerCase())) {
			selectStatus.add(AttendanceManagementStatus.Approved.getValue());
			selectStatus.add(AttendanceManagementStatus.Approved2.getValue());
		}
		if (AttendanceManagementStatus.Cancelled.getDesc().toLowerCase().contains(condition.getStatus().toLowerCase())) {
			selectStatus.add(AttendanceManagementStatus.Cancelled.getValue());
		}
		if (AttendanceManagementStatus.Declined.getDesc().toLowerCase().contains(condition.getStatus().toLowerCase())) {
			selectStatus.add(AttendanceManagementStatus.Declined.getValue());
		}
		if (AttendanceManagementStatus.Lapsed.getDesc().toLowerCase().contains(condition.getStatus().toLowerCase())) {
			selectStatus.add(AttendanceManagementStatus.Lapsed.getValue());
		}
		if (AttendanceManagementStatus.PartialApproved.getDesc().toLowerCase().contains(condition.getStatus().toLowerCase())) {
			selectStatus.add(AttendanceManagementStatus.PartialApproved.getValue());
		}
		if (ListUtil.isEmpty(selectStatus)) {
			selectStatus.add(AttendanceManagementStatus.Default.getValue());
		}
		condition.setSelectStatus(selectStatus);
	}

	@Override
	public ServiceResult<Object> getExternalList(UserInfoVo currentUser, ExternalListCondition condition) {
		Date date = condition.getAge() == null ? null : condition.getAge();
		// 处理上课周期
		dealAttends(condition);
		condition.setStartSize(condition.getPageIndex());
		List<ExternalListVo> list = requestLogInfoMapper.getExternalList(condition);
		for (ExternalListVo vo : list) {
			ChildInfoVo childInfoVo = new ChildInfoVo();
			UserInfo userInfo = userInfoMapper.selectByPrimaryKey(vo.getId());
			AccountInfo accountInfo = accountInfoMapper.getAccountInfoByUserId(vo.getId());
			ChildAttendanceInfo attendanceInfo = childAttendanceInfoMapper.getChildAttendanceByUserId(vo.getId());
			childInfoVo.setUserInfo(userInfo);
			childInfoVo.setAccountInfo(accountInfo);
			childInfoVo.setAttendance(attendanceInfo);
			vo.setChildVo(childInfoVo);
			vo.setLiveAge(com.aoyuntek.aoyun.uitl.DateUtil.calculateAge(vo.getBirthday(), date));
			vo.setReqDate(StringUtil.isNotEmpty(vo.getReqDate()) ? vo.getReqDate().substring(0, vo.getReqDate().length() - 1) : vo.getReqDate());
		}
		int count = requestLogInfoMapper.getExternalListCount(condition);
		condition.setTotalSize(count);
		Pager<ExternalListVo, ExternalListCondition> pager = new Pager<ExternalListVo, ExternalListCondition>(list, condition);
		return successResult((Object) pager);
	}

	private void dealAttends(ExternalListCondition condition) {
		String[] reqDates = condition.getReqDates();
		if (reqDates == null || reqDates.length == 0) {
			return;
		}
		StringBuffer sb = new StringBuffer();
		for (String s : reqDates) {
			sb.append(s + ",");
		}
		condition.setReqDate(sb.toString().substring(0, sb.toString().length() - 1));
	}

	@Override
	public String exprotCsv(UserInfoVo currentUser, AttendanceManagementCondition condition) {
		List<Short> selectType = new ArrayList<Short>();
		condition.setSelectType(selectType);
		List<Short> selectStatus = new ArrayList<Short>();
		condition.setSelectStatus(selectStatus);
		// 处理condition
		dealAttendanceManagementType(condition);
		dealAttendanceManagementStatus(condition);

		condition.setCenterId(currentUser.getUserInfo().getCentersId());
		// 取消分页
		condition.setPageSize(0);

		List<RequestLogVo> list = requestLogInfoMapper.getPagarList(condition);
		StringBuffer buffer = new StringBuffer();
		buffer.append("Children" + "," + "Attendance description" + "," + "	Centre" + "," + "Type of attendance" + "," + "Date of application" + "," + "Date of operation"
				+ "," + "Status" + "\r\n");
		for (RequestLogVo vo : list) {
			buffer.append(vo.toString());
			buffer.append("\r\n");
		}
		return buffer.toString();
	}

	/**
	 * 
	 * @author hxzhang 2019年3月29日
	 * @param centerId
	 * @param roomId
	 * @param day
	 * @param accountId
	 * @return
	 */
	@SuppressWarnings("static-access")
	private boolean havaAttendance(String centerId, Date day, String accountId) {
		Date now = DateUtil.parse(DateUtil.format(new Date(), DateUtil.yyyyMMdd), DateUtil.yyyyMMdd);
		int cha = com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(day, now);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(now);
		int dayOfWeek = calendar.get(calendar.DAY_OF_WEEK) - 1;
		List<String> roomIds = roomInfoMapper.getRoomIdsByCenterId(centerId);
		for (String roomId : roomIds) {
			List<SigninVo> signinInfos = new ArrayList<SigninVo>();
			if (cha == 0) {
				signinInfos = attendanceCoreService.getTodaySignChilds(centerId, roomId, (short) dayOfWeek, day);
				// 获取自定义临时排课
				List<SigninVo> interims = signinInfoMapper.getInterimList(centerId, roomId, day);
				signinInfos = this.mergeAttend(signinInfos, interims);
			} else {
				signinInfos = attendanceCoreService.getFutureRoomChilds(centerId, roomId, day, (short) dayOfWeek);
				// 获取自定义临时排课
				List<SigninVo> interims = signinInfoMapper.getInterimList(centerId, roomId, day);
				signinInfos = this.mergeAttend(signinInfos, interims);
			}

			for (SigninVo vo : signinInfos) {
				if (accountId.equals(vo.getAccountId())) {
					return true;
				}
			}
		}
		return false;
	}
}
