package com.aoyuntek.aoyun.service.policy;

import java.util.List;

import com.aoyuntek.aoyun.condtion.PolicyCondition;
import com.aoyuntek.aoyun.dao.policy.PolicyInfoMapper;
import com.aoyuntek.aoyun.entity.po.policy.PolicyCategoriesInfo;
import com.aoyuntek.aoyun.entity.po.policy.PolicyInfo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.policy.PolicyInfoVo;
import com.aoyuntek.aoyun.service.IBaseWebService;
import com.aoyuntek.framework.model.Pager;
import com.theone.common.util.ServiceResult;

public interface IPolicyService extends IBaseWebService<PolicyInfo, PolicyInfoMapper> {
	
	/**
     * @description 更新policy
     * @author abliu
     * @create 2016年6月28日上午9:59:32
     * @version 1.0
     * @param PolicyInfoVo
     *            PolicyInfoVo实体
     * @param userInfoVo
     *            当前登录用户
     * @return 返回操作结果
     */
	ServiceResult<Object> addOrUpdatePolicy(PolicyInfoVo policyInfoVo, UserInfoVo currentUser);
	
	/**
	 * 更新分类
	 * @param PolicyCategoriesInfos
	 * @param currentUser
	 * @return
	 */
	ServiceResult<Object> addOrUpdatePolicyCategory(List<PolicyCategoriesInfo> PolicyCategoriesInfos, UserInfoVo currentUser);
	
	/**
	 * 通过id获取Policy信息
	 * @param id
	 * @param currentUser
	 * @return
	 */
	ServiceResult<PolicyInfoVo> getPolicyInfo(String id,UserInfoVo currentUser);
	
	/**
	 * 获取集合
	 * @param policyCondition
	 * @param currentUser
	 * @return
	 */
	ServiceResult<Pager<PolicyInfoVo,PolicyCondition>> getPolicyList(PolicyCondition policyCondition, UserInfoVo currentUser);
	
	 /**
	  * 获取完整分类信息
	  * @return
	  */
	ServiceResult<List<PolicyCategoriesInfo>> getCategoriesSrcList();
	
	/**
	 * 删除policy
	 * @param id
	 * @return
	 */
	ServiceResult<Object> deletePolicy(String id);
	
	/**
	 * 判断policy分类是否可以删除
	 * @param id
	 * @return
	 */
	ServiceResult<Object> existDeletePolicy(String id);
}
