package com.aoyuntek.aoyun.service.logs;

import com.aoyuntek.aoyun.condtion.LogsCondition;
import com.aoyuntek.aoyun.dao.LogsDetaileInfoMapper;
import com.aoyuntek.aoyun.entity.po.LogsDetaileInfo;
import com.aoyuntek.aoyun.entity.vo.LogsDetaileVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.service.IBaseWebService;
import com.aoyuntek.framework.model.Pager;
import com.theone.common.util.ServiceResult;

public interface ILogsService extends IBaseWebService<LogsDetaileInfo, LogsDetaileInfoMapper> {

	/**
	 * 通过condition获取logs记录
	 * @param condition
	 * @param currentUser
	 * @return
	 */
    ServiceResult<Pager<LogsDetaileVo, LogsCondition>> getLogsList(LogsCondition condition, UserInfoVo currentUser);
}
