package com.aoyuntek.aoyun.service.program;

import java.util.Date;

import com.aoyuntek.aoyun.dao.program.TaskProgramFollowUpInfoMapper;
import com.aoyuntek.aoyun.entity.po.program.ProgramIntobsleaInfo;
import com.aoyuntek.aoyun.entity.po.program.TaskProgramFollowUpInfo;
import com.aoyuntek.aoyun.entity.vo.FollowUpVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.service.IBaseWebService;
import com.theone.common.util.ServiceResult;

/**
 * @description TaskProgramFollowUp业务逻辑层接口
 * @author Hxzhang 2016年9月21日下午3:24:09
 * @version 1.0
 */
public interface ITaskProgramFollowUpService extends IBaseWebService<TaskProgramFollowUpInfo, TaskProgramFollowUpInfoMapper> {
    /**
     * @description 获取follow up信息
     * @author hxzhang
     * @create 2016年9月21日下午3:27:00
     * @version 1.0
     * @param id
     * @return
     */
    ServiceResult<Object> getTaskProgramFollowUp(String id);

    /**
     * @description 新增或编辑FollowUp
     * @author hxzhang
     * @create 2016年9月22日上午11:45:07
     * @version 1.0
     * @param followUpVo
     * @return
     */

    ServiceResult<Object> addOrUpdateTaskProgramFollowUp(FollowUpVo followUpVo, UserInfoVo currentUser);
    /**
     * @description 定时任务创建ProgramFollowup
     * @author hxzhang
     * @create 2016年9月25日下午4:59:15
     * @version 1.0
     * @param now
     */
    void createProgramFollowupTimedTask(Date now);

    /**
     * @description 将过期的followup状态更新为overDue
     * @author hxzhang
     * @create 2016年9月25日下午7:54:34
     * @version 1.0
     * @param now
     */
    void updateOverDueProgramFollowupTimedTask(Date now);

    /**
     * @description 创建Followup
     * @author hxzhang
     * @create 2016年10月11日上午11:12:10
     */
    void createProgramFollowup(ProgramIntobsleaInfo p);
}
