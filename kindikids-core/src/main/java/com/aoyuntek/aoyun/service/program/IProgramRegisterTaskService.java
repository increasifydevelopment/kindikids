package com.aoyuntek.aoyun.service.program;

import java.util.Date;

import com.aoyuntek.aoyun.dao.program.ProgramRegisterTaskInfoMapper;
import com.aoyuntek.aoyun.entity.po.program.ProgramRegisterItemInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramRegisterTaskInfo;
import com.aoyuntek.aoyun.entity.vo.ProgramRegisterTaskInfoVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.enums.TaskType;
import com.aoyuntek.framework.service.IBaseService;
import com.theone.common.util.ServiceResult;

public interface IProgramRegisterTaskService extends IBaseService<ProgramRegisterTaskInfo, ProgramRegisterTaskInfoMapper> {

    /**
     * 
     * @description 新增/更新ProgramRegisterTask
     * @author mingwang
     * @create 2016年9月22日下午7:37:11
     * @version 1.0
     * @param registerTaskInfoVo
     * @param currentUser
     * @return
     */
    ServiceResult<Object> addOrUpdateRegister(ProgramRegisterTaskInfoVo registerTaskInfoVo, UserInfoVo currentUser);

    /**
     * 
     * @description
     * @author mingwang
     * @create 2016年9月23日上午9:12:20
     * @version 1.0
     * @param id
     * @param registerId
     * @return
     */

    /**
     * 
     * @description 获取ProgramRegisterTask
     * @author mingwang
     * @create 2016年9月23日上午9:16:12
     * @version 1.0
     * @param id
     * @param roomId
     * @return
     */
    ServiceResult<Object> getRegisterTaskInfo(String id);

    /**
     * 
     * @description 自动生成定时任务Register
     * @author mingwang
     * @create 2016年9月25日下午1:58:01
     * @version 1.0
     * @param date
     * @param currentUser
     * @return
     */
    void createTimingTask(Date date, short taskType);

    /**
     * @description 通过value获取TaskType
     * @author mingwang
     * @create 2016年9月23日上午9:16:12
     * @version 1.0
     * @return
     */
    TaskType getTaskType(short type);

    /**
     * 
     * @description 获取当前room签到的所有小孩
     * @author mingwang
     * @create 2016年9月26日下午2:47:05
     * @version 1.0
     * @param roomId
     * @return
     */
    ServiceResult<Object> getSignChilds(String roomId, Date date, String params, Short type);

    /**
     * 
     * @description 将Register的过期状态更新为overDue
     * @author mingwang
     * @create 2016年9月28日下午2:16:53
     * @version 1.0
     * @param now
     */
    void updateOverDueRegisterTimedTask(Date now, short taskType);

    ServiceResult<Object> updateRegisterItemTime(ProgramRegisterItemInfo itemInfo);

    /**
     * 
     * @description 获取当前room所在centre签到的所有小孩
     * @author mingwang
     * @create 2017年2月24日上午10:25:18
     * @version 1.0
     * @param roomId
     * @param date
     * @param p
     * @param type
     * @return
     */
    ServiceResult<Object> getCentreSignChilds(String roomId, Date date, String p, Short type);

    /**
     * @description 获取指定room下所有的孩子(为归档,未删除)
     * @author hxzhang
     * @create 2017年3月14日上午9:23:22
     */
    ServiceResult<Object> getChildsByRoom(String roomId, String p);

}
