package com.aoyuntek.aoyun.service.waiting.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.condtion.waitingList.WaitingListCondition;
import com.aoyuntek.aoyun.dao.AccountInfoMapper;
import com.aoyuntek.aoyun.dao.ApplicationListMapper;
import com.aoyuntek.aoyun.dao.ApplicationParentMapper;
import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.ChildAttendanceInfoMapper;
import com.aoyuntek.aoyun.dao.ChildLogsInfoMapper;
import com.aoyuntek.aoyun.dao.EmailMsgMapper;
import com.aoyuntek.aoyun.dao.EmailPlanInfoMapper;
import com.aoyuntek.aoyun.dao.FamilyInfoMapper;
import com.aoyuntek.aoyun.dao.LogsDetaileInfoMapper;
import com.aoyuntek.aoyun.dao.OutChildCenterInfoMapper;
import com.aoyuntek.aoyun.dao.OutRelevantInfoMapper;
import com.aoyuntek.aoyun.dao.RequestLogInfoMapper;
import com.aoyuntek.aoyun.dao.SourceLogsInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.entity.po.AccountInfo;
import com.aoyuntek.aoyun.entity.po.ApplicationList;
import com.aoyuntek.aoyun.entity.po.ApplicationParent;
import com.aoyuntek.aoyun.entity.po.CentersInfo;
import com.aoyuntek.aoyun.entity.po.ChildAttendanceInfo;
import com.aoyuntek.aoyun.entity.po.ChildLogsInfo;
import com.aoyuntek.aoyun.entity.po.EmailMsg;
import com.aoyuntek.aoyun.entity.po.EmailPlanInfo;
import com.aoyuntek.aoyun.entity.po.FamilyInfo;
import com.aoyuntek.aoyun.entity.po.LogsDetaileInfo;
import com.aoyuntek.aoyun.entity.po.OutChildCenterInfo;
import com.aoyuntek.aoyun.entity.po.OutFamilyPo;
import com.aoyuntek.aoyun.entity.po.OutRelevantInfo;
import com.aoyuntek.aoyun.entity.po.SourceLogsInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.vo.ChildInfoVo;
import com.aoyuntek.aoyun.entity.vo.QueuevVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.waiting.CountResultVo;
import com.aoyuntek.aoyun.entity.vo.waiting.SubmitModel;
import com.aoyuntek.aoyun.entity.vo.waiting.WaitingDetailsVo;
import com.aoyuntek.aoyun.entity.vo.waiting.WaitingListVo;
import com.aoyuntek.aoyun.enums.AllocateType;
import com.aoyuntek.aoyun.enums.ApplicationStatus;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.enums.WaitingType;
import com.aoyuntek.aoyun.factory.UserFactory;
import com.aoyuntek.aoyun.service.IFamilyService;
import com.aoyuntek.aoyun.service.IUserInfoService;
import com.aoyuntek.aoyun.service.impl.BaseWebServiceImpl;
import com.aoyuntek.aoyun.service.waiting.IWaitingService;
import com.aoyuntek.framework.model.Pager;
import com.theone.aws.util.AwsEmailUtil;
import com.theone.common.util.ServiceResult;
import com.theone.date.util.DateUtil;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

@Service
public class WaitingServiceImpl extends BaseWebServiceImpl<ApplicationList, ApplicationListMapper> implements IWaitingService {
	@Autowired
	private ApplicationListMapper applicationListMapper;
	@Autowired
	private ApplicationParentMapper applicationParentMapper;
	@Autowired
	private OutChildCenterInfoMapper outChildCenterInfoMapper;
	@Autowired
	private CentersInfoMapper centersInfoMapper;
	@Autowired
	private RequestLogInfoMapper requestLogInfoMapper;
	@Autowired
	private UserInfoMapper userInfoMapper;
	@Autowired
	private AccountInfoMapper accountInfoMapper;
	@Autowired
	private ChildAttendanceInfoMapper childAttendanceInfoMapper;
	@Autowired
	private IUserInfoService userInfoService;
	@Autowired
	private IFamilyService familyService;
	@Autowired
	private OutRelevantInfoMapper outRelevantInfoMapper;
	@Autowired
	private EmailPlanInfoMapper emailPlanInfoMapper;
	@Autowired
	private UserFactory userFactory;
	@Autowired
	private FamilyInfoMapper familyInfoMapper;
	@Autowired
	private LogsDetaileInfoMapper logsDetaileInfoMapper;
	@Autowired
	private SourceLogsInfoMapper sourceLogsInfoMapper;
	@Autowired
	private ChildLogsInfoMapper childLogsInfoMapper;
	@Autowired
	private EmailMsgMapper emailMsgMapper;

	@Override
	public ServiceResult<Object> getList(WaitingListCondition condition, UserInfoVo currentUser) {
		dealCondition(condition, currentUser);
		condition.setStartSize(condition.getPageIndex());
		List<WaitingListVo> list = applicationListMapper.getWaitingList(condition);
		int totalSize = applicationListMapper.getWaitingListCount(condition);
		condition.setTotalSize(totalSize);
		dealResultList(list, condition);
		Pager<WaitingListVo, WaitingListCondition> pager = new Pager<WaitingListVo, WaitingListCondition>(list, condition);
		return successResult((Object) pager);
	}

	/**
	 * 处理查询条件
	 * 
	 * @author hxzhang at 2018年5月15日下午6:20:45
	 * @param condition
	 */
	private void dealCondition(WaitingListCondition condition, UserInfoVo currentUser) {
		if (StringUtil.isNotEmpty(condition.getParam())) {
			condition.setSelectPeople(applicationListMapper.searchPeople(condition.getParam()));
		} else {
			condition.setSelectPeople(null);
		}
		if (ListUtil.isNotEmpty(condition.getSelectCenterIds())) {
			condition.setCenterIds(applicationListMapper.getObjIdBySelectCenterId(condition.getSelectCenterIds()));
		} else {
			condition.setCenterIds(null);
		}

		List<CentersInfo> allCenters = centersInfoMapper.getAllCentersInfos();
		for (CentersInfo info : allCenters) {
			if (info.getId().equals(getSystemMsg("rydeId"))) {
				info.setRyde(true);
			}
		}
		condition.setAllCenters(allCenters);

		if (containRoles(currentUser.getRoleInfoVoList(), Role.Parent)) {
			condition.setFamilyId(currentUser.getUserInfo().getFamilyId());
		}
	}

	private String dealCentreId(WaitingListCondition condition) {
		List<String> list = centersInfoMapper.getCentreIds(condition.getSelectCenterIds());
		String centreIds = "";
		for (String str : list) {
			centreIds += str + ",";
		}
		return centreIds.substring(0, centreIds.length() - 1);
	}

	/**
	 * 处理返回结果
	 * 
	 * @author hxzhang at 2018年5月11日下午5:40:20
	 * @param list
	 */
	private void dealResultList(List<WaitingListVo> list, WaitingListCondition condition) {
		Date date = condition.getLiveAge() == null ? null : condition.getLiveAge();
		for (WaitingListVo vo : list) {
			dealCenter(vo);
			vo.setLiveAge(com.aoyuntek.aoyun.uitl.DateUtil.calculateAge(vo.getBirthday(), date));
			WaitingType type = WaitingType.getType(vo.getListType());
			switch (type) {
			case ApplicationList:
				vo.setQueue(applicationListMapper.getQueue(vo.getId(), type.getValue()));
				break;
			case ExternalWaitList:
				vo.setQueue(applicationListMapper.getQueue(vo.getId(), type.getValue()));
				dealChildDetails(vo);
				break;
			case SiblingWaitList:
				vo.setQueue(applicationListMapper.getQueue(vo.getId(), type.getValue()));
				dealChildDetails(vo);
				break;
			case ArchivedApplicationList:
				vo.setQueue(applicationListMapper.getQueue(vo.getId(), type.getValue()));
				break;
			default:
				break;
			}
		}
		WaitingListCondition selectCountsCondition = resetCondition(condition);
		condition
				.setCounts(new Integer[] { applicationListMapper.getApplictionCount(selectCountsCondition), applicationListMapper.getArchivedCount(selectCountsCondition),
						applicationListMapper.getExternalCount(selectCountsCondition), applicationListMapper.getSiblingCount(selectCountsCondition) });
	}

	private WaitingListCondition resetCondition(WaitingListCondition condition) {
		WaitingListCondition selectCountsCondition = condition;
		selectCountsCondition.setListType(null);
		return selectCountsCondition;
	}

	private void dealCenter(WaitingListVo vo) {
		List<CentersInfo> list = outChildCenterInfoMapper.getCenterNameByUserId(vo.getId());
		if (vo.getListType() == ApplicationStatus.MovedToArchivedApplication.getValue() && vo.getArchivedFrom() != ApplicationStatus.PendingForAction.getValue()) {
			list = outChildCenterInfoMapper.getCenterNameByUserId(applicationListMapper.selectByPrimaryKey(vo.getId()).getUserId());
		}
		for (CentersInfo info : list) {
			if (info.getId().equals(getSystemMsg("rydeId"))) {
				info.setRyde(true);
			}
		}
		vo.setCenters(list);
	}

	private void dealChildDetails(WaitingListVo vo) {
		ChildInfoVo childInfoVo = new ChildInfoVo();
		UserInfo userInfo = userInfoMapper.selectByPrimaryKey(vo.getId());
		AccountInfo accountInfo = accountInfoMapper.getAccountInfoByUserId(vo.getId());
		ChildAttendanceInfo attendanceInfo = childAttendanceInfoMapper.getChildAttendanceByUserId(vo.getId());
		childInfoVo.setUserInfo(userInfo);
		childInfoVo.setAccountInfo(accountInfo);
		childInfoVo.setAttendance(attendanceInfo);
		vo.setChildVo(childInfoVo);
	}

	@Override
	public ServiceResult<Object> getDetails(String id) {
		ApplicationList child = applicationListMapper.getChildDetails(id);
		List<String> centerIds = outChildCenterInfoMapper.getCentreIdListByuserId(child.getId());
		List<ApplicationParent> parents = applicationParentMapper.getParentDetails(id);
		if (parents.size() == 1) {
			parents.add(new ApplicationParent());
		}
		OutRelevantInfo info = outRelevantInfoMapper.getInfo(id);
		List<CentersInfo> centersInfos = centersInfoMapper.getAllCentersInfos();
		for (CentersInfo c : centersInfos) {
			if (!centerIds.contains(c.getId())) {
				continue;
			}
			c.setChoose(true);
		}
		return successResult((Object) new WaitingDetailsVo(child, parents, centersInfos, info));
	}

	@Override
	public ServiceResult<Object> getCount(UserInfoVo currentUser) {
		CountResultVo vo = new CountResultVo();
		vo.setParent(containRoles(currentUser.getRoleInfoVoList(), Role.Parent));
		vo.setCounts(
				new Integer[] { applicationListMapper.getApplictionCount(new WaitingListCondition()), applicationListMapper.getArchivedCount(new WaitingListCondition()),
						applicationListMapper.getExternalCount(new WaitingListCondition()), applicationListMapper.getSiblingCount(new WaitingListCondition()) });
		return successResult((Object) vo);
	}

	@Override
	public ServiceResult<Object> updatePositionDate(WaitingListVo vo, String ip, UserInfoVo currentUser) {
		WaitingType waitingType = WaitingType.getType(vo.getListType());
		switch (waitingType) {
		case ApplicationList:
		case ArchivedApplicationList:
			applicationListMapper.updatePositionDate(vo.getId(), vo.getApplicationDatePosition());
			break;
		case ExternalWaitList:
		case SiblingWaitList:
			requestLogInfoMapper.updatePositionDate(vo.getId(), vo.getApplicationDatePosition());
			break;
		}
		dealLogsPosition(vo, waitingType, ip, currentUser);
		return successResult();
	}

	/**
	 * @author hxzhang at 2018年7月27日下午3:24:54
	 * @param vo
	 */
	private void dealLogsPosition(WaitingListVo vo, WaitingType waitingType, String ip, UserInfoVo currentUser) {
		int queue = applicationListMapper.getQueue(vo.getId(), vo.getListType());
		if (vo.getQueue() == queue) {
			return;
		}
		SourceLogsInfo sourceLogsInfo = initSourceLogsInfo(ip, currentUser, vo.getId());
		String tag = waitingType.getDesc() + "'s position";
		LogsDetaileInfo logsDetaileInfo = initLogsDetaileInfo(sourceLogsInfo.getId(), tag, "position", String.valueOf(vo.getQueue()), String.valueOf(queue));
		logsDetaileInfoMapper.insert(logsDetaileInfo);
		sourceLogsInfoMapper.insert(sourceLogsInfo);
	}

	private LogsDetaileInfo initLogsDetaileInfo(String sourceLogsId, String tag, String changeFieldName, String oldVal, String newVal) {
		LogsDetaileInfo logsDetaileInfo = new LogsDetaileInfo();
		logsDetaileInfo.setId(UUID.randomUUID().toString());
		logsDetaileInfo.setChangeFieldName(changeFieldName);
		logsDetaileInfo.setOldValue(oldVal);
		logsDetaileInfo.setNewValue(newVal);
		logsDetaileInfo.setTags(tag);
		logsDetaileInfo.setSourceLogsId(sourceLogsId);
		return logsDetaileInfo;
	}

	private SourceLogsInfo initSourceLogsInfo(String ip, UserInfoVo currentUser, String sourceId) {
		SourceLogsInfo sourceLogsInfo = new SourceLogsInfo();
		sourceLogsInfo.setId(UUID.randomUUID().toString());
		sourceLogsInfo.setSourceId(sourceId);
		sourceLogsInfo.setUpdateTime(new Date());
		if (currentUser != null) {
			sourceLogsInfo.setAccountId(currentUser.getAccountInfo().getId());
		}
		sourceLogsInfo.setIpAddress(ip);
		sourceLogsInfo.setIsAdd(false);
		sourceLogsInfo.setIsSuccess(true);
		sourceLogsInfo.setIsDelete(false);

		return sourceLogsInfo;
	}

	@Override
	public ServiceResult<Object> submitMove(SubmitModel model, UserInfoVo currentUser) {
		AllocateType type = AllocateType.getType(model.getFromType(), model.getMoveToType());
		switch (type) {
		case ApplicationToExternal: {
			// 验证家长邮箱是否与系统有重复
			ServiceResult<Object> result = validateSubmitMove(model);
			if (!result.isSuccess()) {
				return result;
			}
			dealRequestToOutside(model, currentUser);
			break;
		}
		case ApplicationToSibling: {
			// 验证家长邮箱是否与系统有重复
			ServiceResult<Object> result = validateSubmitMove(model);
			if (!result.isSuccess()) {
				return result;
			}
			dealRequestToOutside(model, currentUser);
			break;
		}
		case ApplicationToArchived: {
			dealAllocate(model, currentUser);
			break;
		}
		case ExternalToApplication: {
			ApplicationList app = dealOutsideToRequest(model, currentUser);
			synchroLogs(model.getObjId(), app.getId());
			dealCenter(app.getId());
			break;
		}
		case ExternalToSibling: {
			dealSiblingFlag(model, currentUser, true);
			break;
		}
		case ExternalToArchived: {
			dealOutsideToRequest(model, currentUser);
			break;
		}
		case SiblingToApplication: {
			ApplicationList app = dealOutsideToRequest(model, currentUser);
			synchroLogs(model.getObjId(), app.getId());
			dealCenter(app.getId());
			break;
		}
		case SiblingToExternal: {
			dealSiblingFlag(model, currentUser, false);
			break;
		}
		case SiblingToArchived: {
			dealOutsideToRequest(model, currentUser);
			break;
		}
		case ArchivedToApplication: {
			dealAllocate(model, currentUser);
			dealCenter(model.getObjId());
			break;
		}
		case ArchivedToExternal: {
			// 验证家长邮箱是否与系统有重复
			ServiceResult<Object> result = validateSubmitMove(model);
			if (!result.isSuccess()) {
				return result;
			}
			dealRequestToOutside(model, currentUser);
			break;
		}
		case ArchivedToSibling: {
			// 验证家长邮箱是否与系统有重复
			ServiceResult<Object> result = validateSubmitMove(model);
			if (!result.isSuccess()) {
				return result;
			}
			dealRequestToOutside(model, currentUser);
			break;
		}
		default:
			break;
		}
		return successResult((Object) model);
	}

	private ApplicationList dealOutsideToRequest(SubmitModel model, UserInfoVo currentUser) {
		// 获取孩子的request信息
		ApplicationList app = applicationListMapper.getRequestByUserId(model.getObjId());
		if (app == null) {
			app = dealFillRequest(app, model, currentUser);
		} else {
			app.setUserId(model.getObjId());
			app.setStatus(model.getMoveToType());
			app.setArchivedFrom(model.getFromType());
			app.setUpdateTime(new Date());
			app.setUpdateAccountId(currentUser.getAccountInfo().getId());
			app.setDeleteFlag(false);
			applicationListMapper.updateByPrimaryKeySelective(app);
		}
		// 处理家庭
		dealFamilyInfo(model);
		// 处理计划邮件
		userFactory.removePlanEmail(model.getFamilyId());

		return app;
	}

	private void dealFamilyInfo(SubmitModel model) {
		// 获取孩子信息
		UserInfo child = userInfoMapper.selectByPrimaryKey(model.getObjId());
		// 判断家庭是否只有一个孩子
		List<UserInfo> childs = userInfoMapper.getChildInfoByFamilyId(child.getFamilyId());
		// 删除孩子相关信息
		familyService.removeChild(child.getId());
		// 如果该家庭只有一个孩子,则删除家长及家庭
		if (childs.size() == 1) {
			familyService.removeParent(child.getFamilyId());
			familyService.removeFamily(child.getFamilyId());
		}
	}

	private ApplicationList dealFillRequest(ApplicationList app, SubmitModel model, UserInfoVo currentUser) {
		// 填充孩子信息
		app = new ApplicationList();
		app.setId(UUID.randomUUID().toString());
		// 获取孩子信息
		UserInfo child = userInfoMapper.selectByPrimaryKey(model.getObjId());
		app.setFirstName(child.getFirstName());
		app.setLastName(child.getLastName());
		app.setGender(child.getGender());
		app.setBirthday(child.getBirthday());
		ChildAttendanceInfo atten = childAttendanceInfoMapper.getChildAttendanceByUserId(model.getObjId());
		app.setApplicationDate(atten.getCreateTime());
		app.setApplicationDatePosition(atten.getPositioningdate());
		app.setRequestedStartDate(atten.getRequestStartDate());
		app.setMonday(atten.getMonday());
		app.setTuesday(atten.getTuesday());
		app.setWednesday(atten.getWednesday());
		app.setThursday(atten.getThursday());
		app.setFriday(atten.getFriday());

		app.setStatus(model.getMoveToType());
		app.setArchivedFrom(model.getFromType());
		app.setUpdateTime(new Date());
		app.setUpdateAccountId(currentUser.getAccountInfo().getId());
		app.setDeleteFlag(false);
		app.setUserId(child.getId());
		applicationListMapper.insertSelective(app);

		// 填充家长信息
		List<UserInfo> parents = userInfoMapper.getParentInfoByFamilyId(child.getFamilyId());
		Date now = new Date();
		for (UserInfo u : parents) {
			ApplicationParent p = new ApplicationParent();
			p.setId(UUID.randomUUID().toString());
			p.setApplicationId(app.getId());
			p.setFirstName(u.getFirstName());
			p.setLastName(u.getLastName());
			p.setAddress(u.getAddress());
			p.setSuburb(u.getSuburb());
			p.setPostcode(u.getPostcode());
			p.setHomePhone(u.getPhoneNumber());
			p.setWorkPhone(u.getWorkPhoneNumber());
			p.setMobile(u.getMobileNumber());
			p.setEmail(u.getEmail());
			p.setCreateTime(now);
			p.setUpdateTime(now);
			p.setDeleteFlag(false);
			now.setTime(now.getTime() + 1000);
			applicationParentMapper.insertSelective(p);
		}
		return app;
	}

	/**
	 * 处理Allocate
	 * 
	 * @author hxzhang at 2018年5月18日上午9:53:26
	 * @param model
	 * @param currentUser
	 */
	private void dealAllocate(SubmitModel model, UserInfoVo currentUser) {
		ApplicationList app = applicationListMapper.selectByPrimaryKey(model.getObjId());
		app.setStatus(model.getMoveToType());
		app.setArchivedFrom(model.getFromType());
		app.setUpdateTime(new Date());
		app.setUpdateAccountId(currentUser.getAccountInfo().getId());
		applicationListMapper.updateByPrimaryKeySelective(app);
	}

	/**
	 * 处理外部转入申请是园区问题
	 * 
	 * @author hxzhang at 2018年6月22日下午7:33:08
	 * @param id
	 */
	private void dealCenter(String id) {
		ApplicationList app = applicationListMapper.selectByPrimaryKey(id);
		if (StringUtil.isNotEmpty(app.getUserId())) {
			// 处理园区信息
			List<OutChildCenterInfo> list = outChildCenterInfoMapper.getListByUserId(app.getUserId());
			for (OutChildCenterInfo o : list) {
				o.setChildUserId(app.getId());
				outChildCenterInfoMapper.updateByPrimaryKeySelective(o);
			}
		}
	}

	/**
	 * 
	 * @author hxzhang at 2018年5月21日上午8:40:09
	 * @param model
	 * @param currentUser
	 */
	private void dealSiblingFlag(SubmitModel model, UserInfoVo currentUser, boolean flag) {
		if (StringUtil.isNotEmpty(model.getFamilyId())) {
			// 获取孩子信息
			UserInfo child = userInfoMapper.selectByPrimaryKey(model.getObjId());
			// 获取该家庭小孩数量
			List<UserInfo> childs = userInfoMapper.getChildInfoByFamilyId(child.getFamilyId());
			// 如果只有一个小孩,将该家庭及家长删除
			if (childs.size() == 1) {
				familyService.removeParent(child.getFamilyId());
				familyService.removeFamily(child.getFamilyId());
			}
			child.setFamilyId(model.getFamilyId());
			child.setUpdateTime(new Date());
			child.setUpdateAccountId(currentUser.getAccountInfo().getId());
			userInfoMapper.updateByPrimaryKey(child);
		}
		// 更新孩子亲戚标识
		requestLogInfoMapper.updateSiblingFlag(model.getObjId(), new Date(), currentUser.getAccountInfo().getId(), flag);
	}

	private void dealRequestToOutside(SubmitModel model, UserInfoVo currentUser) {
		// 获取孩子申请信息
		ApplicationList app = applicationListMapper.selectByPrimaryKey(model.getObjId());
		String oldUserId = app.getUserId();
		// 获取孩子家长申请信息
		List<ApplicationParent> appParents = applicationParentMapper.getParentDetails(model.getObjId());
		// 获取孩子园区信息
		List<OutChildCenterInfo> outCenters = outChildCenterInfoMapper.getListByUserId(app.getId());
		if (ListUtil.isEmpty(outCenters)) {
			outCenters = outChildCenterInfoMapper.getListByUserId(app.getUserId());
		}

		String familyId = StringUtil.isNotEmpty(model.getFamilyId()) ? model.getFamilyId() : UUID.randomUUID().toString();
		UserInfo child = new UserInfo();
		child.setFirstName(app.getFirstName());
		child.setLastName(app.getLastName());
		child.setBirthday(app.getBirthday());
		child.setGender(app.getGender());
		// 封装两个家长对象
		List<UserInfo> parentList = dealParentList(appParents, model);

		OutFamilyPo outFamily = new OutFamilyPo();
		outFamily.setChild(child);
		outFamily.setParentList(parentList);
		outFamily.setChangeDate(app.getRequestedStartDate());
		outFamily.setMonday(app.getMonday());
		outFamily.setTuesday(app.getTuesday());
		outFamily.setWednesday(app.getWednesday());
		outFamily.setThursday(app.getThursday());
		outFamily.setFriday(app.getFriday());
		outFamily.setFamilyName(ListUtil.isEmpty(parentList) ? null : parentList.get(0).getLastName());
		outFamily.setFamilyId(familyId);
		outFamily.setApplicationDate(app.getApplicationDate());
		outFamily.setApplicationDatePosition(app.getApplicationDatePosition());
		outFamily.setSiblingFlag(model.getMoveToType() == ApplicationStatus.MovedToSibling.getValue() ? true : false);
		String childId = familyService.saveOutside(outFamily);

		// 先删除已经存在的孩子(这里解决来回从外部到归档的申请时会出现多个孩子的情况)
		userInfoMapper.deleteUserById(app.getUserId());

		app.setStatus(model.getMoveToType());
		app.setUserId(childId);
		app.setUpdateTime(new Date());
		app.setUpdateAccountId(currentUser.getAccountInfo().getId());

		for (OutChildCenterInfo o : outCenters) {
			o.setChildUserId(childId);
			outChildCenterInfoMapper.updateByPrimaryKeySelective(o);
		}

		// 保存familyId
		model.setFamilyId(familyId);

		// 同步孩子已存在的log信息
		synchroLogs(app.getId(), childId);
		// 同步已存在的notes信息
		synchroNotes(childId, oldUserId);
		// 同步Email Msg
		synchroEmailMsg(app.getId(), childId);
		// 同步Additional information
		synchroInformation(app, childId, oldUserId, currentUser);

		applicationListMapper.updateByPrimaryKeySelective(app);
	}

	/**
	 * @author hxzhang 2018年10月11日
	 * @param requestId
	 * @param childId
	 */
	private void synchroInformation(ApplicationList request, String childId, String oldChildId, UserInfoVo currentUser) {
		if (StringUtil.isNotEmpty(oldChildId)) {
			return;
		}
		OutRelevantInfo info = outRelevantInfoMapper.getInfo(request.getId());
		if (info == null) {
			return;
		}
		if (StringUtil.isEmpty(info.getProposal())) {
			return;
		}

		ChildLogsInfo childLogsInfo = new ChildLogsInfo();
		childLogsInfo.setId(UUID.randomUUID().toString());
		childLogsInfo.setReason(info.getProposal());
		childLogsInfo.setLogDate(request.getApplicationDate());
		childLogsInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
		childLogsInfo.setCreateTime(request.getApplicationDate());
		childLogsInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
		childLogsInfo.setUpdateTime(request.getApplicationDate());
		childLogsInfo.setUserId(childId);
		childLogsInfo.setDeleteFlag(DeleteFlag.Default.getValue());
		childLogsInfoMapper.insertSelective(childLogsInfo);
	}

	/**
	 * @author hxzhang 2018年8月28日
	 * @param appId
	 * @param userId
	 */
	private void synchroEmailMsg(String appId, String userId) {
		List<EmailMsg> list = emailMsgMapper.getEmailMsgListByAppId(appId);
		for (EmailMsg msg : list) {
			msg.setUserId(userId);
			emailMsgMapper.updateByPrimaryKey(msg);
		}
	}

	/**
	 * @author hxzhang 2018年8月8日 上午9:41:39
	 *
	 * @param oldUserId
	 * @param newUserId
	 */
	private void synchroNotes(String newUserId, String oldUserId) {
		if (StringUtil.isEmpty(oldUserId)) {
			return;
		}
		childLogsInfoMapper.updateNotesUserId(oldUserId, newUserId);
	}

	private void synchroLogs(String sourceId, String synchroId) {
		List<SourceLogsInfo> infos = sourceLogsInfoMapper.getInfoBySourceId(sourceId);
		if (ListUtil.isEmpty(infos)) {
			ApplicationList app = applicationListMapper.selectByPrimaryKey(sourceId);
			if (app != null) {
				infos = sourceLogsInfoMapper.getInfoBySourceId(app.getUserId());
			}
		}
		if (ListUtil.isEmpty(infos)) {
			return;
		}
		for (SourceLogsInfo info : infos) {
			info.setSourceId(synchroId);
			sourceLogsInfoMapper.updateByPrimaryKeySelective(info);
		}
	}

	private List<UserInfo> dealParentList(List<ApplicationParent> appParents, SubmitModel model) {
		List<UserInfo> parentList = new ArrayList<UserInfo>();
		if (StringUtil.isNotEmpty(model.getFamilyId())) {
			return parentList;
		}
		for (ApplicationParent p : appParents) {
			UserInfo parent = new UserInfo();
			parent.setFirstName(p.getFirstName());
			parent.setLastName(p.getLastName());
			parent.setAddress(p.getAddress());
			parent.setSuburb(p.getSuburb());
			parent.setPostcode(p.getPostcode());
			parent.setPhoneNumber(p.getHomePhone());
			parent.setMobileNumber(p.getMobile());
			parent.setWorkPhoneNumber(p.getWorkPhone());
			parent.setEmail(p.getEmail());
			parentList.add(parent);
		}
		return parentList;
	}

	private ServiceResult<Object> validateSubmitMove(SubmitModel model) {
		if (StringUtil.isNotEmpty(model.getFamilyId())) {
			return successResult();
		}

		// 如果选择了已有家庭，且没有选择family，则不给提交
		if (model.getFamilyToType() != 0 && StringUtil.isEmpty(model.getFamilyId())) {
			return failResult(getTipMsg("family_is_required"));
		}
		// 获取孩子家长申请信息
		List<ApplicationParent> appParents = applicationParentMapper.getParentDetails(model.getObjId());
		for (ApplicationParent user : appParents) {
			List<UserInfo> list = userInfoService.validateEmail(user.getEmail());
			if (list != null && list.size() != 0) {
				log.info("parent.email.repeat");
				FamilyInfo familyInfo = familyInfoMapper.selectByPrimaryKey(list.get(0).getFamilyId());
				return failResult((Object) 2, MessageFormat.format(getTipMsg("user.email.repeat"), familyInfo.getFamilyName()), (Object) familyInfo.getId());
			}
		}
		return successResult();
	}

	@Override
	public Object getFamilySelect(String p) {
		return applicationListMapper.getFamilySelect(p);
	}

	@Override
	public void sendPlanEmail() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				List<EmailPlanInfo> list = emailPlanInfoMapper.getList();
				String emailKey = getSystemMsg("email_key");
				AwsEmailUtil emailUtil = new AwsEmailUtil();
				for (EmailPlanInfo email : list) {
					String objId = email.getObjId();
					String sendAddress = getSystemMsg("mailHostAccount");
					String sendName = getSystemMsg("email_name");
					try {
						emailUtil.sendgrid(sendAddress, sendName, email.getReceiveraddress(), email.getReceivername(), email.getSub(), email.getMsg(), true, null, null,
								emailKey);
						System.err.println(email.getMsg().toString());
						emailPlanInfoMapper.remove(objId);
					} catch (Exception e) {
						log.error(e.getMessage(), e);
					}
				}
			}
		}).start();
	}

	@Override
	public ServiceResult<Object> save(ApplicationList app, String ip, UserInfoVo currentUser) {
		ApplicationList dbApp = applicationListMapper.selectByPrimaryKey(app.getId());
		requestedStartDateLog(app.getId(), dbApp.getRequestedStartDate(), app.getRequestedStartDate(), ip, currentUser);
		dbApp.setRequestedStartDate(app.getRequestedStartDate());
		applicationListMapper.updateByPrimaryKey(dbApp);
		return successResult(getTipMsg("common_save"));
	}

	private void requestedStartDateLog(String id, Date oldVal, Date newVal, String ip, UserInfoVo currentUser) {
		if (DateUtil.isSameDay(oldVal, newVal)) {
			return;
		}
		SourceLogsInfo sourceLogsInfo = initSourceLogsInfo(ip, currentUser, id);
		LogsDetaileInfo logsDetaileInfo = initLogsDetaileInfo(sourceLogsInfo.getId(), "Requested Start Date", "Requested Start Date",
				DateUtil.format(oldVal, "dd/MM/yyyy"), DateUtil.format(newVal, "dd/MM/yyyy"));
		logsDetaileInfoMapper.insert(logsDetaileInfo);
		sourceLogsInfoMapper.insert(sourceLogsInfo);
	}

	@Override
	public String exportCsv(WaitingListCondition condition, UserInfoVo currentUser) {
		dealCondition(condition, currentUser);
		// 取消分页
		condition.setPageSize(0);
		List<WaitingListVo> list = applicationListMapper.getWaitingList(condition);
		Date date = condition.getLiveAge() == null ? null : condition.getLiveAge();
		StringBuffer buffer = new StringBuffer();
		Map<String, Integer> map = getAllQueue();
		buffer.append("Position" + "," + "List" + "," + "Region" + "," + "Child" + "," + "Age" + "," + "Requested Days" + "," + "Application Date" + ","
				+ "Editable Application Date" + "," + "Requested Start Date" + "\r\n");
		for (WaitingListVo vo : list) {
			buffer.append(map.get(vo.getId()) + "," + getType(vo) + "," + getRegion(vo) + "," + vo.getChildName() + "," + getAge(vo, date) + "," + getRequestedDays(vo)
					+ "," + DateUtil.format(vo.getApplicationDate(), "dd/MM/yyyy") + "," + DateUtil.format(vo.getApplicationDatePosition(), "dd/MM/yyyy") + ","
					+ DateUtil.format(vo.getRequestedStartDate(), "dd/MM/yyyy") + "\r\n");
		}
		return buffer.toString();
	}

	private String getRequestedDays(WaitingListVo vo) {
		StringBuffer buffer = new StringBuffer();
		if (vo.isMonday()) {
			buffer.append("MON,");
		}
		if (vo.isTuesday()) {
			buffer.append("TUE,");
		}
		if (vo.isWednesday()) {
			buffer.append("WED,");
		}
		if (vo.isThursday()) {
			buffer.append("THU,");
		}
		if (vo.isFriday()) {
			buffer.append("FRI,");
		}
		if (buffer.length() == 0) {
			return "";
		}
		return "\"" + buffer.toString().substring(0, buffer.toString().length() - 1) + "\"";
	}

	private String getAge(WaitingListVo vo, Date date) {
		Integer[] integers = com.aoyuntek.aoyun.uitl.DateUtil.calculateAge(vo.getBirthday(), date);
		return integers[0] + "Y " + integers[1] + "M " + integers[2] + "D";
	}

	private String getRegion(WaitingListVo vo) {
		List<CentersInfo> list = outChildCenterInfoMapper.getCenterNameByUserId(vo.getId());
		if (vo.getListType() == ApplicationStatus.MovedToArchivedApplication.getValue() && vo.getArchivedFrom() != ApplicationStatus.PendingForAction.getValue()) {
			list = outChildCenterInfoMapper.getCenterNameByUserId(applicationListMapper.selectByPrimaryKey(vo.getId()).getUserId());
		}
		if (ListUtil.isEmpty(list)) {
			return "";
		}
		StringBuffer centerStr = new StringBuffer();
		for (CentersInfo info : list) {
			if (info.getId().equals(getSystemMsg("rydeId"))) {
				centerStr.append("Ryde");
			} else {
				centerStr.append(info.getName());
			}
			centerStr.append(",");
		}
		return "\"" + centerStr.toString().substring(0, centerStr.length() - 1) + "\"";
	}

	private String getType(WaitingListVo vo) {
		WaitingType type = WaitingType.getType(vo.getListType());
		switch (type) {
		case ApplicationList:
			return "APPLICATION";
		case SiblingWaitList:
			return "SIBLING";
		case ExternalWaitList:
			return "EXTERNAL";
		case ArchivedApplicationList:
			return "ARCHIVED";
		}
		return null;
	}

	private Map<String, Integer> getAllQueue() {
		Map<String, Integer> map = new HashMap<String, Integer>();
		List<QueuevVo> list = applicationListMapper.getQueueForType(WaitingType.ApplicationList.getValue());
		list.addAll(applicationListMapper.getQueueForType(WaitingType.SiblingWaitList.getValue()));
		list.addAll(applicationListMapper.getQueueForType(WaitingType.ExternalWaitList.getValue()));
		list.addAll(applicationListMapper.getQueueForType(WaitingType.ArchivedApplicationList.getValue()));
		for (QueuevVo vo : list) {
			map.put(vo.getId(), vo.getQueue());
		}
		return map;
	}
}
