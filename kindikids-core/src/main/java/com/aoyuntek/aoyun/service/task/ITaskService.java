package com.aoyuntek.aoyun.service.task;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.aoyuntek.aoyun.condtion.AttendanceCondtion;
import com.aoyuntek.aoyun.condtion.TaskCondtion;
import com.aoyuntek.aoyun.dao.task.TaskInfoMapper;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskInfo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.task.PersonSelectVo;
import com.aoyuntek.aoyun.entity.vo.task.TaskInfoVo;
import com.aoyuntek.aoyun.service.IBaseWebService;
import com.theone.common.util.ServiceResult;

/**
 * 
 * task
 * 
 * @author dlli5 at 2016年9月20日下午5:34:31
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public interface ITaskService extends IBaseWebService<TaskInfo, TaskInfoMapper> {

	List<SelecterPo> getTaskNames(String p, UserInfoVo currentUser);

	/**
	 * @description 获取program日历信息
	 * @author hxzhang
	 * @create 2016年9月25日上午11:39:27
	 * @version 1.0
	 * @param currentUser
	 * @param attendanceCondtion
	 * @return
	 */
	ServiceResult<Object> getCalendar(UserInfoVo currentUser, AttendanceCondtion attendanceCondtion);

	/**
	 * 
	 * @author dlli5 at 2016年9月20日下午5:36:03
	 * @param category
	 * @param currentUser
	 * @return
	 */
	public ServiceResult<List> getSelectList(int category, UserInfoVo currentUser, String taskId, String findText);

	/**
	 * 
	 * @author dlli5 at 2016年9月21日上午9:09:43
	 * @param list
	 * @param category
	 * @param currentUser
	 * @param taskId
	 * @return
	 */
	public ServiceResult<List> getResponsibleList(List<PersonSelectVo> list, int category, UserInfoVo currentUser, String taskId,
			String findText);

	/**
	 * 
	 * @author dlli5 at 2016年9月21日上午10:59:54
	 * @param obj
	 * @param currentUser
	 * @return
	 */
	public ServiceResult<Object> saveOrUpdateTask(TaskInfoVo obj, UserInfoVo currentUser) throws Exception;

	/**
	 * 
	 * @author dlli5 at 2016年9月21日上午11:30:20
	 * @param list
	 * @param category
	 * @param currentUser
	 * @param taskId
	 * @param findText
	 * @return
	 */
	public ServiceResult<List> getImplementersList(List<PersonSelectVo> list, int category, UserInfoVo currentUser, String taskId,
			String findText);

	/**
	 * 
	 * @author dlli5 at 2016年9月21日下午9:38:00
	 * @param taskId
	 * @param statu
	 * @param currentUser
	 * @return
	 */
	public ServiceResult<Object> updateStatu(String taskId, int statu, UserInfoVo currentUser);

	/**
	 * 
	 * @author dlli5 at 2016年9月21日下午9:45:16
	 * @param condtion
	 * @param currentUser
	 * @return
	 */
	public ServiceResult<Object> getTaskList(TaskCondtion condtion, UserInfoVo currentUser);

	/**
	 * 
	 * @author dlli5 at 2016年9月22日上午8:58:08
	 * @param taskId
	 * @param currentUser
	 * @return
	 */
	public ServiceResult<Object> getTask(String taskId, UserInfoVo currentUser) throws Exception;

	/**
	 * 
	 * @author dlli5 at 2016年9月22日上午11:17:32
	 * @param condtion
	 * @param currentUser
	 * @return
	 */
	public ServiceResult<Object> getUserTaskList(TaskCondtion condtion, UserInfoVo currentUser);

	public ServiceResult<Object> getTaskTableView(TaskCondtion condtion, UserInfoVo currentUser);

	public ServiceResult<Object> getExportData(TaskCondtion condtion, UserInfoVo currentUser);

	public void exportCsv(HttpServletResponse response, List<Map<String, String>> list);

	/**
	 * 
	 * @description 获取pedding,overdue的个数
	 * @author gfwang
	 * @create 2016年11月30日上午8:54:20
	 * @version 1.0
	 * @param condtion
	 * @param currentUser
	 * @return
	 */
	public ServiceResult<Object> getTaskCount(TaskCondtion condtion, UserInfoVo currentUser);

	/**
	 * 
	 * @description 校验task名称重复
	 * @author gfwang
	 * @create 2016年9月25日上午10:02:29
	 * @version 1.0
	 * @param taskName
	 * @param taskId
	 * @return
	 */
	public boolean validateTaskName(String taskName, String taskId);

	/**
	 * 
	 * @author dlli5 at 2016年9月28日上午9:15:41
	 * @param currentUser
	 * @param condtion
	 * @return
	 */
	ServiceResult<Object> getTodayNote(UserInfoVo currentUser, TaskCondtion condtion);

	/**
	 * 
	 * @author dlli5 at 2016年9月28日上午9:15:45
	 * @param currentUser
	 * @param taskInstanceId
	 * @return
	 */
	ServiceResult<Object> getCustmerTaskInstance(UserInfoVo currentUser, String taskInstanceId);

	/**
	 * 
	 * @author dlli5 at 2016年9月28日下午1:53:22
	 * @param currentUser
	 * @return
	 */
	ServiceResult<Object> getWhenRequiredTasks(UserInfoVo currentUser);

	ServiceResult<Object> getWhenRequiredTasks2(UserInfoVo currentUser);

	/**
	 * 
	 * @author dlli5 at 2016年9月28日下午2:33:14
	 * @param currentUser
	 * @param taskId
	 * @return
	 */
	ServiceResult<Object> getWhenRequiredSelects(UserInfoVo currentUser, String taskId);

	/**
	 * 
	 * @author dlli5 at 2016年9月28日下午11:31:42
	 * @param currentUser
	 * @param id
	 * @param type
	 * @param statu
	 * @return
	 */
	ServiceResult<Object> saveTaskFormValue(UserInfoVo currentUser, String id, int type, int statu, String json) throws Exception;

	/**
	 * 
	 * @author dlli5 at 2016年9月28日下午11:31:37
	 * @param currentUser
	 * @param taskId
	 * @param list
	 * @return
	 */
	ServiceResult<Object> saveWhenRequiredTask(UserInfoVo currentUser, String taskId, List<PersonSelectVo> list, Date day) throws Exception;

	/**
	 * 
	 * @author dlli5 at 2016年10月19日上午10:50:51
	 * @param currentUser
	 * @param taskInstanceId
	 * @return
	 */
	ServiceResult<Object> dealObsoleteTask(UserInfoVo currentUser, String taskInstanceId, Boolean followUpFlag, Date day);

	/**
	 * @description 还押金Task
	 * @author hxzhang
	 * @create 2016年10月26日上午10:23:51
	 */
	void createDepositTask(UserInfo userInfo, Date now);

	/**
	 * @description 还帽子Task
	 * @author hxzhang
	 * @create 2016年10月26日上午10:23:27
	 */
	// void createHatTask(UserInfo userInfo, UserInfoVo currentUser, Date now);
}
