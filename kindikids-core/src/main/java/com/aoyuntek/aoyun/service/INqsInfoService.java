package com.aoyuntek.aoyun.service;

import java.util.Date;
import java.util.List;

import com.aoyuntek.aoyun.dao.NqsInfoMapper;
import com.aoyuntek.aoyun.entity.po.NqsInfo;
import com.aoyuntek.aoyun.entity.vo.NqsVo;
import com.aoyuntek.aoyun.service.IBaseWebService;

public interface INqsInfoService extends IBaseWebService<NqsInfo, NqsInfoMapper> {
    List<NqsVo> getList(Date date);
}
