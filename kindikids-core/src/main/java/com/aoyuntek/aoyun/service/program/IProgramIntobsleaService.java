package com.aoyuntek.aoyun.service.program;

import java.util.Date;

import com.aoyuntek.aoyun.dao.program.ProgramIntobsleaInfoMapper;
import com.aoyuntek.aoyun.entity.po.program.ProgramIntobsleaInfo;
import com.aoyuntek.aoyun.entity.vo.ProgramIntobsleaInfoVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.service.IBaseWebService;
import com.theone.common.util.ServiceResult;

/**
 * 
 * @description program Intobslea(interest,observasion,learningStory) 业务逻辑接口
 * @author mingwang
 * @create 2016年9月20日下午5:33:31
 * @version 1.0
 */
public interface IProgramIntobsleaService extends IBaseWebService<ProgramIntobsleaInfo, ProgramIntobsleaInfoMapper> {

    /**
     * 
     * @description 新增/更新program中Intobslea
     * @author mingwang
     * @create 2016年9月20日下午5:55:31
     * @version 1.0
     * @param programIntobsleaInfo
     * @param currentUser
     * @return
     */
    ServiceResult<Object> addOrUpdateintobslea(ProgramIntobsleaInfoVo programIntobsleaInfoVo, UserInfoVo currentUser, Date date, boolean confirm);

    /**
     * 
     * @description 删除program中Intobslea
     * @author mingwang
     * @create 2016年9月21日下午1:59:39
     * @version 1.0
     * @param intobsleaId
     * @return
     */
    ServiceResult<Object> removeIntobslea(String intobsleaId);

    /**
     * 
     * @description 获取ProgramIntobsleaInfo信息
     * @author mingwang
     * @create 2016年9月22日上午9:58:37
     * @version 1.0
     * @param intobsleaId
     * @return
     */
    ServiceResult<Object> getIntobsleaInfo(String intobsleaId);

    /**
     * 
     * @description Insobslea过期测试
     * @author mingwang
     * @create 2016年9月28日上午11:25:52
     * @version 1.0
     * @param now
     */
    void updateOverDueIntobsleaTimedTask(Date now);

    /**
     * 
     * @description task是否有followUp
     * @author mingwang
     * @create 2017年1月16日上午10:31:57
     * @version 1.0
     * @param intobsleaId
     * @return
     */
    ServiceResult<Object> hasFollowUp(String intobsleaId);

}
