package com.aoyuntek.aoyun.service.impl;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.condtion.ProfileTempCondition;
import com.aoyuntek.aoyun.constants.DateFormatterConstants;
import com.aoyuntek.aoyun.dao.profile.ChildFormRelationInfoMapper;
import com.aoyuntek.aoyun.dao.profile.ProfileTemplateInfoMapper;
import com.aoyuntek.aoyun.entity.po.profile.ProfileTemplateInfo;
import com.aoyuntek.aoyun.entity.vo.FormInfoVO;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.ProfileTempState;
import com.aoyuntek.aoyun.enums.ProfileTempType;
import com.aoyuntek.aoyun.enums.meeting.CurrtenFlag;
import com.aoyuntek.aoyun.service.IFormInfoService;
import com.aoyuntek.aoyun.service.IProfileService;
import com.aoyuntek.aoyun.uitl.GsonUtil;
import com.aoyuntek.framework.model.Pager;
import com.theone.common.util.ServiceResult;
import com.theone.string.util.StringUtil;

/**
 * @description 业务逻辑实现层
 * @author Hxzhang 2016年10月21日上午9:50:44
 */
@Service
public class ProfileServiceImpl extends BaseWebServiceImpl<ProfileTemplateInfo, ProfileTemplateInfoMapper> implements IProfileService {
    @Autowired
    private ProfileTemplateInfoMapper profileTemplateInfoMapper;
    @Autowired
    private ChildFormRelationInfoMapper childFormRelationInfoMapper;
    @Autowired
    private IFormInfoService formService;

    @Override
    public ServiceResult<Object> saveProfileTemp(ProfileTemplateInfo profileTemplateInfo, UserInfoVo currentUser, String formJson) {
        ServiceResult<Object> result = validateProfile(profileTemplateInfo, formJson);
        if (!result.isSuccess()) {
            return result;
        }
        boolean isUse = isUse(profileTemplateInfo);
        if(!StringUtil.isEmpty(formJson)){
            FormInfoVO formInfoVO = (FormInfoVO) GsonUtil.jsonToBean(formJson, FormInfoVO.class, DateFormatterConstants.SYS_FORMAT,
                    DateFormatterConstants.SYS_FORMAT2, DateFormatterConstants.SYS_T_FORMAT, DateFormatterConstants.SYS_T_FORMAT2);
            String instanceId = formService.saveForm(formInfoVO).getReturnObj();
            profileTemplateInfo.setInstanceId(instanceId);
        }
        // 新增或编辑
        if (StringUtil.isEmpty(profileTemplateInfo.getId())) {
            profileTemplateInfo.setId(UUID.randomUUID().toString());
            profileTemplateInfo.setState(ProfileTempState.Enable.getValuse());
            profileTemplateInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
            Date now = new Date();
            profileTemplateInfo.setCreateTime(now);
            profileTemplateInfo.setUpdateTime(now);
            profileTemplateInfo.setDeleteFlag(DeleteFlag.Default.getValue());
            profileTemplateInfo.setCurrtenFlag(CurrtenFlag.NewVersion.getValue());
            profileTemplateInfoMapper.insert(profileTemplateInfo);
        } else {
            ProfileTemplateInfo dbData = profileTemplateInfoMapper.selectByPrimaryKey(profileTemplateInfo.getId());
            dbData.setUpdateAccountId(currentUser.getAccountInfo().getId());
            dbData.setUpdateTime(new Date());
            if (isUse) { // 模版被使用将之前更新为老版本,重新插入新版本
                dbData.setCurrtenFlag(CurrtenFlag.OldVersion.getValue());
                profileTemplateInfoMapper.updateByPrimaryKey(dbData);
                dbData.setId(UUID.randomUUID().toString());
                dbData.setInstanceId(profileTemplateInfo.getInstanceId());
                dbData.setTemplateName(profileTemplateInfo.getTemplateName());
                dbData.setCurrtenFlag(CurrtenFlag.NewVersion.getValue());
                profileTemplateInfoMapper.insert(dbData);
            } else { // 未被使用直接更新
                dbData.setInstanceId(profileTemplateInfo.getInstanceId());
                dbData.setTemplateName(profileTemplateInfo.getTemplateName());
                profileTemplateInfoMapper.updateByPrimaryKey(dbData);
            }
        }
        return successResult(getTipMsg("profile_save"));
    }

    /**
     * @description 判断模版是否被使用
     * @author hxzhang
     * @create 2016年10月21日下午5:03:42
     */
    private boolean isUse(ProfileTemplateInfo p) {
        if (StringUtil.isEmpty(p.getId())) {
            return false;
        }
        int count = childFormRelationInfoMapper.getCountByTempId(p.getId());
        if (count != 0) {
            return true;
        }
        return false;
    }

    /**
     * @description 数据验证
     * @author hxzhang
     * @create 2016年10月21日下午1:07:12
     */
    private ServiceResult<Object> validateProfile(ProfileTemplateInfo p, String json) {
        // 1.名称非空验证
        if (StringUtil.isEmpty(p.getTemplateName())) {
            if (ProfileTempType.child.getValue() == p.getProfileTempType()) {
                return failResult(getTipMsg("profile_Orientation_name"));
            } else {
                return failResult(getTipMsg("profile_Induction_name"));
            }
        }
        // 2.名称长度验证
        if (p.getTemplateName().length() > 255) {
            if (ProfileTempType.child.getValue() == p.getProfileTempType()) {
                return failResult(getTipMsg("profile_Orientation_name_long"));
            } else {
                return failResult(getTipMsg("profile_Induction_name_long"));
            }
        }
        // 3.名称唯一性验证
        int count = profileTemplateInfoMapper.validateTempName(p.getId(), p.getTemplateName(), p.getProfileTempType());
        if (count != 0) {
            if (ProfileTempType.child.getValue() == p.getProfileTempType()) {
                return failResult(getTipMsg("profile_Orientation_name_exists"));
            } else {
                return failResult(getTipMsg("profile_Induction_name_exists"));
            }
        }
        // 4.Instance模版非空验证
        if (StringUtil.isEmpty(p.getInstanceId()) && StringUtil.isEmpty(json)) {
            return failResult(getTipMsg("profile_template"));
        }

        return successResult();
    }

    @Override
    public ServiceResult<Object> getProfileTempInfo(String id) {
        return successResult((Object) profileTemplateInfoMapper.selectByPrimaryKey(id));
    }

    @Override
    public ServiceResult<Object> getProfileTempList(ProfileTempCondition condition) {
        // 设置起始页
        condition.setStartSize(condition.getPageIndex());
        List<ProfileTemplateInfo> list = profileTemplateInfoMapper.getPagerList(condition);
        int totalSize = profileTemplateInfoMapper.getPagerListCount(condition);
        condition.setTotalSize(totalSize);
        Pager<ProfileTemplateInfo, ProfileTempCondition> pager = new Pager<ProfileTemplateInfo, ProfileTempCondition>(list, condition);
        return successResult((Object) pager);
    }

    @Override
    public ServiceResult<Object> saveOpera(String id, short state, UserInfoVo currentUser) {
        ProfileTemplateInfo dbData = profileTemplateInfoMapper.selectByPrimaryKey(id);
        dbData.setState(state);
        dbData.setUpdateAccountId(currentUser.getAccountInfo().getId());
        dbData.setUpdateTime(new Date());
        profileTemplateInfoMapper.updateByPrimaryKey(dbData);
        return successResult(getTipMsg("profile_Opera"));
    }

    @Override
    public List<SelecterPo> getSelectList(short type) {
        return profileTemplateInfoMapper.getSelectList(type);
    }

    @Override
    public ServiceResult<Object> getProfileTempByUserId(String userId, String templateId) {
        return successResult((Object) profileTemplateInfoMapper.getProfileTempByUserId(userId, templateId));
    }
}
