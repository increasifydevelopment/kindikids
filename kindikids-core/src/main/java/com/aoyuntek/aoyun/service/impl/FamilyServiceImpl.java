package com.aoyuntek.aoyun.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.ContextLoader;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.aoyuntek.aoyun.condtion.FamilyCondition;
import com.aoyuntek.aoyun.condtion.UserCondition;
import com.aoyuntek.aoyun.condtion.waitingList.WaitingListCondition;
import com.aoyuntek.aoyun.constants.TipMsgConstants;
import com.aoyuntek.aoyun.dao.AbsenteeRequestInfoMapper;
import com.aoyuntek.aoyun.dao.AccountInfoMapper;
import com.aoyuntek.aoyun.dao.AccountRoleInfoMapper;
import com.aoyuntek.aoyun.dao.ApplicationListMapper;
import com.aoyuntek.aoyun.dao.ApplicationParentMapper;
import com.aoyuntek.aoyun.dao.AttachmentInfoMapper;
import com.aoyuntek.aoyun.dao.AttendanceHistoryInfoMapper;
import com.aoyuntek.aoyun.dao.BackgroundPersonInfoMapper;
import com.aoyuntek.aoyun.dao.BackgroundPetsInfoMapper;
import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.ChangeAttendanceRequestInfoMapper;
import com.aoyuntek.aoyun.dao.ChangePswRecordInfoMapper;
import com.aoyuntek.aoyun.dao.ChildAttendanceInfoMapper;
import com.aoyuntek.aoyun.dao.ChildBackgroundInfoMapper;
import com.aoyuntek.aoyun.dao.ChildCustodyArrangeInfoMapper;
import com.aoyuntek.aoyun.dao.ChildDietaryRequireInfoMapper;
import com.aoyuntek.aoyun.dao.ChildHealthMedicalDetailInfoMapper;
import com.aoyuntek.aoyun.dao.ChildHealthMedicalInfoMapper;
import com.aoyuntek.aoyun.dao.ChildLogsInfoMapper;
import com.aoyuntek.aoyun.dao.ChildMedicalInfoMapper;
import com.aoyuntek.aoyun.dao.ChildNotesMapper;
import com.aoyuntek.aoyun.dao.EmailMsgMapper;
import com.aoyuntek.aoyun.dao.EmergencyContactInfoMapper;
import com.aoyuntek.aoyun.dao.EmergencySignatureInfoMapper;
import com.aoyuntek.aoyun.dao.FamilyInfoMapper;
import com.aoyuntek.aoyun.dao.GivingNoticeInfoMapper;
import com.aoyuntek.aoyun.dao.LogsDetaileInfoMapper;
import com.aoyuntek.aoyun.dao.MsgEmailInfoMapper;
import com.aoyuntek.aoyun.dao.NewsAccountsInfoMapper;
import com.aoyuntek.aoyun.dao.OutChildCenterInfoMapper;
import com.aoyuntek.aoyun.dao.OutRelevantInfoMapper;
import com.aoyuntek.aoyun.dao.PreviousEnrolmentMapper;
import com.aoyuntek.aoyun.dao.PreviousEnrolmentRelationMapper;
import com.aoyuntek.aoyun.dao.RelationKidParentInfoMapper;
import com.aoyuntek.aoyun.dao.RequestLogInfoMapper;
import com.aoyuntek.aoyun.dao.RoleInfoMapper;
import com.aoyuntek.aoyun.dao.RoomGroupInfoMapper;
import com.aoyuntek.aoyun.dao.RoomInfoMapper;
import com.aoyuntek.aoyun.dao.SourceLogsInfoMapper;
import com.aoyuntek.aoyun.dao.SubtractedAttendanceMapper;
import com.aoyuntek.aoyun.dao.TemporaryAttendanceInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.dao.child.RoomChangeInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskInstanceInfoMapper;
import com.aoyuntek.aoyun.entity.po.AbsenteeRequestInfoWithBLOBs;
import com.aoyuntek.aoyun.entity.po.AccountInfo;
import com.aoyuntek.aoyun.entity.po.AccountRoleInfo;
import com.aoyuntek.aoyun.entity.po.ApplicationList;
import com.aoyuntek.aoyun.entity.po.ApplicationParent;
import com.aoyuntek.aoyun.entity.po.AttendanceHistoryInfo;
import com.aoyuntek.aoyun.entity.po.BackgroundPersonInfo;
import com.aoyuntek.aoyun.entity.po.BackgroundPetsInfo;
import com.aoyuntek.aoyun.entity.po.CentersInfo;
import com.aoyuntek.aoyun.entity.po.ChangeAttendanceRequestInfo;
import com.aoyuntek.aoyun.entity.po.ChangePswRecordInfo;
import com.aoyuntek.aoyun.entity.po.ChildAttendanceInfo;
import com.aoyuntek.aoyun.entity.po.ChildBackgroundInfo;
import com.aoyuntek.aoyun.entity.po.ChildCustodyArrangeInfo;
import com.aoyuntek.aoyun.entity.po.ChildDietaryRequireInfo;
import com.aoyuntek.aoyun.entity.po.ChildHealthMedicalDetailInfo;
import com.aoyuntek.aoyun.entity.po.ChildHealthMedicalInfo;
import com.aoyuntek.aoyun.entity.po.ChildLogsInfo;
import com.aoyuntek.aoyun.entity.po.ChildMedicalInfo;
import com.aoyuntek.aoyun.entity.po.ChildNotes;
import com.aoyuntek.aoyun.entity.po.EmailMsg;
import com.aoyuntek.aoyun.entity.po.EmergencyContactInfo;
import com.aoyuntek.aoyun.entity.po.EmergencySignatureInfo;
import com.aoyuntek.aoyun.entity.po.FamilyInfo;
import com.aoyuntek.aoyun.entity.po.LogsDetaileInfo;
import com.aoyuntek.aoyun.entity.po.MsgEmailInfo;
import com.aoyuntek.aoyun.entity.po.OutChildCenterInfo;
import com.aoyuntek.aoyun.entity.po.OutFamilyPo;
import com.aoyuntek.aoyun.entity.po.OutRelevantInfo;
import com.aoyuntek.aoyun.entity.po.PreviousEnrolment;
import com.aoyuntek.aoyun.entity.po.PreviousEnrolmentRelation;
import com.aoyuntek.aoyun.entity.po.RelationKidParentInfo;
import com.aoyuntek.aoyun.entity.po.RequestLogInfo;
import com.aoyuntek.aoyun.entity.po.RoomGroupInfo;
import com.aoyuntek.aoyun.entity.po.RoomInfo;
import com.aoyuntek.aoyun.entity.po.SourceLogsInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.vo.ChildBackgroundInfoVo;
import com.aoyuntek.aoyun.entity.vo.ChildInfoVo;
import com.aoyuntek.aoyun.entity.vo.ChildLogsVo;
import com.aoyuntek.aoyun.entity.vo.EmailVo;
import com.aoyuntek.aoyun.entity.vo.EylfInfoVo;
import com.aoyuntek.aoyun.entity.vo.FamilyInfoVo;
import com.aoyuntek.aoyun.entity.vo.FileListVo;
import com.aoyuntek.aoyun.entity.vo.FileVo;
import com.aoyuntek.aoyun.entity.vo.MoveFamilyVo;
import com.aoyuntek.aoyun.entity.vo.OutModleVo;
import com.aoyuntek.aoyun.entity.vo.ReportVo;
import com.aoyuntek.aoyun.entity.vo.RequestLogVo;
import com.aoyuntek.aoyun.entity.vo.RoleInfoVo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.aoyun.entity.vo.StaffRoleInfoVo;
import com.aoyuntek.aoyun.entity.vo.TaskVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.ValueInfoVO;
import com.aoyuntek.aoyun.entity.vo.child.AttendanceHistoryGroupVo;
import com.aoyuntek.aoyun.entity.vo.child.HistoryVo;
import com.aoyuntek.aoyun.entity.vo.waiting.WaitingListVo;
import com.aoyuntek.aoyun.enums.AccountActiveStatus;
import com.aoyuntek.aoyun.enums.ApplicationStatus;
import com.aoyuntek.aoyun.enums.ArchivedStatus;
import com.aoyuntek.aoyun.enums.ChangeAttendanceRequestState;
import com.aoyuntek.aoyun.enums.ChildType;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.EmailMsgType;
import com.aoyuntek.aoyun.enums.EmailPlanType;
import com.aoyuntek.aoyun.enums.EmailType;
import com.aoyuntek.aoyun.enums.EnrolledState;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.enums.SelectAnOption;
import com.aoyuntek.aoyun.enums.SendEmailType;
import com.aoyuntek.aoyun.enums.TokenStatus;
import com.aoyuntek.aoyun.enums.UserType;
import com.aoyuntek.aoyun.factory.AttendanceFactory;
import com.aoyuntek.aoyun.factory.AttendanceHistoryFactory;
import com.aoyuntek.aoyun.factory.UserFactory;
import com.aoyuntek.aoyun.service.ICommonService;
import com.aoyuntek.aoyun.service.IFamilyService;
import com.aoyuntek.aoyun.service.IFormInfoService;
import com.aoyuntek.aoyun.service.IMsgEmailInfoService;
import com.aoyuntek.aoyun.service.IUserInfoService;
import com.aoyuntek.aoyun.service.attendance.IAttendanceCoreService;
import com.aoyuntek.aoyun.service.attendance.IAttendanceHistoryService;
import com.aoyuntek.aoyun.uitl.EmailUtil;
import com.aoyuntek.aoyun.uitl.FilePathUtil;
import com.aoyuntek.aoyun.uitl.FreemarkerUtil;
import com.aoyuntek.aoyun.uitl.ObjectCompUtil;
import com.aoyuntek.aoyun.uitl.SqlUtil;
import com.aoyuntek.framework.constants.PropertieNameConts;
import com.aoyuntek.framework.model.Pager;
import com.theone.aws.util.FileFactory;
import com.theone.common.util.ServiceResult;
import com.theone.date.util.DateUtil;
import com.theone.json.util.JsonUtil;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

/**
 * @description Family业务逻辑层实现类
 * @author hxzhang
 * @create 2016年7月28日下午asda2:01:23
 * @version 1.0
 */
@Service
public class FamilyServiceImpl extends BaseWebServiceImpl<UserInfo, UserInfoMapper> implements IFamilyService {

	public static Logger logger = Logger.getLogger(MessageInfoServiceImpl.class);
	@Autowired
	private UserInfoMapper userInfoMapper;
	@Autowired
	private ChildCustodyArrangeInfoMapper custodyArrangeMapper;
	@Autowired
	private ChildDietaryRequireInfoMapper dietaryRequireMapper;
	@Autowired
	private ChildMedicalInfoMapper medicalMapper;
	@Autowired
	private EmergencyContactInfoMapper emergencyContactMapper;
	@Autowired
	private ChildBackgroundInfoMapper childBackgroundInfoMapper;
	@Autowired
	private BackgroundPetsInfoMapper backgroundPetsInfoMapper;
	@Autowired
	private BackgroundPersonInfoMapper backgroundPersonInfoMapper;
	@Autowired
	private ChildHealthMedicalDetailInfoMapper childHealthMedicalDetailInfoMapper;
	@Autowired
	private ChildHealthMedicalInfoMapper childHealthMedicalInfoMapper;
	@Autowired
	private AccountInfoMapper accountInfoMapper;
	@Autowired
	private FamilyInfoMapper familyInfoMapper;
	@Autowired
	private AccountRoleInfoMapper accountRoleInfoMapper;
	@Autowired
	private RoleInfoMapper roleInfoMapper;
	@Autowired
	private AttachmentInfoMapper attachmentInfoMapper;
	@Autowired
	private ChangePswRecordInfoMapper changePswRecordInfoMapper;
	@Autowired
	private RelationKidParentInfoMapper relationKidParentInfoMapper;
	@Autowired
	private CentersInfoMapper centersInfoMapper;
	@Autowired
	private RoomInfoMapper roomInfoMapper;
	@Autowired
	private RoomGroupInfoMapper roomGroupInfoMapper;
	@Autowired
	private ChildAttendanceInfoMapper childAttendanceInfoMapper;
	@Autowired
	private RequestLogInfoMapper requestLogInfoMapper;
	@Autowired
	private AttendanceHistoryInfoMapper attendanceHistoryInfoMapper;
	@Autowired
	private ICommonService commonService;
	@Autowired
	private RoomGroupInfoMapper groupMapper;
	@Autowired
	private RoomInfoMapper roomMapper;
	@Autowired
	private CentersInfoMapper centreMapper;
	@Autowired
	private IUserInfoService userInfoService;
	@Autowired
	private IAttendanceCoreService attendanceCoreService;
	@Autowired
	private GivingNoticeInfoMapper givingNoticeInfoMapper;
	@Autowired
	private AbsenteeRequestInfoMapper absenteeRequestInfoMapper;
	@Autowired
	private ChangeAttendanceRequestInfoMapper changeAttendanceRequestInfoMapper;
	@Autowired
	private TemporaryAttendanceInfoMapper temporaryAttendanceInfoMapper;
	@Autowired
	private SubtractedAttendanceMapper subtractedAttendanceMapper;
	@Autowired
	private ChildLogsInfoMapper childLogsInfoMapper;
	@Autowired
	private AttendanceFactory attendanceFactory;
	@Autowired
	private SourceLogsInfoMapper sourceLogsInfoMapper;
	@Autowired
	private LogsDetaileInfoMapper logsDetaileInfoMapper;
	@Autowired
	private MsgEmailInfoMapper msgEmailInfoMapper;
	@Autowired
	private IFormInfoService formService;
	@Autowired
	private IMsgEmailInfoService msgEmailInfoService;
	@Autowired
	private UserFactory userFactory;
	@Autowired
	private AttendanceHistoryFactory attendanceHistoryFactory;
	@Autowired
	private OutRelevantInfoMapper outRelevantInfoMapper;
	@Autowired
	private OutChildCenterInfoMapper outChildCenterInfoMapper;
	@Autowired
	private RoomChangeInfoMapper roomChangeInfoMapper;
	@Autowired
	private IAttendanceHistoryService attendanceHistoryService;
	@Autowired
	private TaskInstanceInfoMapper taskInstanceInfoMapper;
	@Autowired
	private EmailMsgMapper emailMsgMapper;
	@Autowired
	private EmergencySignatureInfoMapper emergencySignatureInfoMapper;
	@Autowired
	private ApplicationListMapper applicationListMapper;
	@Autowired
	private ApplicationParentMapper applicationParentMapper;
	@Autowired
	private ChildNotesMapper childNotesMapper;
	@Autowired
	private PreviousEnrolmentMapper previousEnrolmentMapper;
	@Autowired
	private PreviousEnrolmentRelationMapper previousEnrolmentRelationMapper;
	@Autowired
	private NewsAccountsInfoMapper newsAccountsInfoMapper;

	@Override
	public ServiceResult<Object> addOrUpdateChild(ChildInfoVo childInfoVo, String familyName, UserInfoVo currentUser, String ip) throws Exception {
		String valueId = "";
		if (StringUtil.isNotEmpty(childInfoVo.getJson()) && !isRole(currentUser, Role.Parent.getValue())) {
			ValueInfoVO valueInfoVO = (ValueInfoVO) JsonUtil.jsonToBean(childInfoVo.getJson(), ValueInfoVO.class);
			ServiceResult<String> formResult = formService.saveInstance(valueInfoVO);
			if (!formResult.isSuccess()) {
				return failResult(getTipMsg("operation_failed"));
			}
			valueId = formResult.getReturnObj();
		}

		// 验证
		ServiceResult<Object> result = validateAddOrUpdateChild(childInfoVo, currentUser, familyName);
		if (!result.isSuccess()) {
			return result;
		}
		String familyId = childInfoVo.getUserInfo().getFamilyId();
		Short childType = ChildType.External.getValue();
		String oldRoomId = null;
		if (StringUtil.isEmpty(childInfoVo.getUserInfo().getId())) {
			// 新增
			// 初始化用户信息
			UserInfo userInfo = initOrUpdateUserInfo(childInfoVo.getUserInfo(), currentUser, familyId, UserType.Child.getValue(), null);
			String userId = userInfo.getId();
			familyId = userInfo.getFamilyId();
			// 初始化AccountInfo信息
			initOrUpdateAccountInfo(userInfo, currentUser, familyId, Role.Children.getValue(), childType);
			// 初始化FamilyInfo信息
			initOrUpdateFamilyInfo(familyId, familyName);
			// 初始化监护安排信息
			initOrUpdateChildCustodyArrangeInfo(userId, childInfoVo.getCustodyArrange(), null);
			// 初始化紧急联系人信息
			initOrUpdateEmergencyContactInfo(userId, childInfoVo.getEmergencyContactList(), null);
			// 初始化健康和医疗信息
			initOrUpdateChildHealthMedicalInfo(userId, childInfoVo.getHealthMedical(), null);

			// 初始化健康和医疗详细信息
			initOrUpdateChildHealthMedicalDetailInfo(userId, childInfoVo.getHealthMedicalDetailList(), null);
			// 初始化背景信息
			initOrUpdateChildBackgroundInfoVo(userId, childInfoVo.getBackground(), null);
			// 初始化饮食要求信息
			initOrUpdateChildDietaryRequireInfo(userId, childInfoVo.getDietaryRequire(), null);
			// 初始化药物信息
			initOrUpdateChildMedicalInfo(userId, childInfoVo.getMedical(), null);
			// 初始化紧急联系人签名
			initOrUpdateEmergencySignature(userId, childInfoVo.getEmergencySignatureInfo());
			// 建立家庭关系
			buildRelationKidParentInfo(userInfo);
			// 執行Attendance
			result = initOrUpdateAttendanceInfo(currentUser, userId, childInfoVo, currentUser, oldRoomId, ip);
			if (!result.isSuccess()) {
				return result;
			}
		} else {
			SourceLogsInfo sourceLogsInfo = initSourceLogsInfo(ip, currentUser, childInfoVo.getUserInfo().getId());
			oldRoomId = userInfoMapper.selectByPrimaryKey(childInfoVo.getUserInfo().getId()).getRoomId();
			// 编辑
			// 如果发现group 改变了 那么处理一下
			UserInfo oldUser = userInfoMapper.selectByPrimaryKey(childInfoVo.getUserInfo().getId());
			// 更新用户信息
			UserInfo userInfo = initOrUpdateUserInfo(childInfoVo.getUserInfo(), currentUser, familyId, UserType.Child.getValue(), sourceLogsInfo);
			if (StringUtil.isNotEmpty(userInfo.getGroupId()) && !userInfo.getGroupId().equals(oldUser.getGroupId())) {
				AccountInfo dbAccountInfo = accountInfoMapper.selectAccountInfoByUserId(userInfo.getId());
				taskInstanceInfoMapper.updateGroupId(dbAccountInfo.getId(), userInfo.getGroupId());
			}

			String userId = userInfo.getId();
			familyId = userInfo.getFamilyId();
			// 更新AccountInfo信息
			initOrUpdateAccountInfo(userInfo, currentUser, familyId, Role.Children.getValue(), childType);
			// 更新监护安排信息
			initOrUpdateChildCustodyArrangeInfo(userId, childInfoVo.getCustodyArrange(), sourceLogsInfo);
			// 更新紧急联系人信息
			initOrUpdateEmergencyContactInfo(userId, childInfoVo.getEmergencyContactList(), sourceLogsInfo);
			// 更新健康和医疗信息
			initOrUpdateChildHealthMedicalInfo(userId, childInfoVo.getHealthMedical(), sourceLogsInfo);
			// 更新健康和医疗详细信息
			initOrUpdateChildHealthMedicalDetailInfo(userId, childInfoVo.getHealthMedicalDetailList(), sourceLogsInfo);
			// 更新背景信息
			initOrUpdateChildBackgroundInfoVo(userId, childInfoVo.getBackground(), sourceLogsInfo);
			// 更新饮食要求信息
			initOrUpdateChildDietaryRequireInfo(userId, childInfoVo.getDietaryRequire(), sourceLogsInfo);
			// 更新药物信息
			initOrUpdateChildMedicalInfo(userId, childInfoVo.getMedical(), sourceLogsInfo);
			// 更新FamilyInfo
			initOrUpdateFamilyInfo(familyId, familyName);
			// 更新紧急联系人签名
			initOrUpdateEmergencySignature(userId, childInfoVo.getEmergencySignatureInfo());
			// AttendanceInfo
			result = initOrUpdateAttendanceInfo(currentUser, userId, childInfoVo, currentUser, oldRoomId, ip);
			if (!result.isSuccess()) {
				return result;
			}
			childInfoVo.setAttendance(childAttendanceInfoMapper.selectByPrimaryKey(childInfoVo.getAttendance().getId()));

			commonService.getPersonColor(userInfo);

			newsAccountsInfoMapper.updateUserNameByAccountId(childInfoVo.getAccountInfo().getId(), getFullName(childInfoVo.getUserInfo()));
		}
		String userId = childInfoVo.getUserInfo().getId();
		if (!isRole(currentUser, Role.Parent.getValue())) {
			commonService.buildUserProfileRelation(userId, childInfoVo.getProfileId(), valueId);
		}
		AccountInfo accountInfo = accountInfoMapper.getAccountInfoByUserId(childInfoVo.getUserInfo().getId());
		childInfoVo.setAccountInfo(accountInfo);
		// 上课历史
		HistoryVo historyVo = attendanceHistoryFactory.getHistoryVo(childInfoVo.getUserInfo(), accountInfo.getId(), childInfoVo.getAttendance().getLeaveDate());
		childInfoVo.setAttendanceHistoryInfos(historyVo);
		// 拼接歷史消息
		List<RequestLogVo> requestLogVos = requestLogInfoMapper.selectLogVo(accountInfo.getId());
		childInfoVo.setRequestLogVos(requestLogVos);

		return successResult(getTipMsg("user.submit.success"), (Object) childInfoVo);
	}

	private String getFullName(UserInfo user) {
		String middleName = (user.getMiddleName() == null || "".equals(user.getMiddleName())) ? "" : " " + user.getMiddleName();
		String fullName = user.getFirstName() + middleName + " " + user.getLastName();
		return fullName;
	}

	/**
	 * 新增或更新紧急联系人签名
	 * 
	 * @author hxzhang at 2018年4月23日下午3:02:55
	 * @param es
	 */
	private void initOrUpdateEmergencySignature(String userId, EmergencySignatureInfo es) {
		EmergencySignatureInfo dbEs = emergencySignatureInfoMapper.getInfoByUserId(userId);
		if (null == dbEs) {
			es.setId(UUID.randomUUID().toString());
			es.setUserId(userId);
			emergencySignatureInfoMapper.insert(es);
		} else {
			dbEs.setEmergencySignature(es.getEmergencySignature());
			emergencySignatureInfoMapper.updateInfoByUserId(userId, dbEs.getEmergencySignature());
		}
	}

	/*
	 * 初始化日志主表
	 */
	private SourceLogsInfo initSourceLogsInfo(String ip, UserInfoVo currentUser, String sourceId) {
		SourceLogsInfo sourceLogsInfo = new SourceLogsInfo();
		sourceLogsInfo.setId(UUID.randomUUID().toString());
		sourceLogsInfo.setSourceId(sourceId);
		sourceLogsInfo.setUpdateTime(new Date());
		if (currentUser != null) {
			sourceLogsInfo.setAccountId(currentUser.getAccountInfo().getId());
		}
		sourceLogsInfo.setIpAddress(ip);
		sourceLogsInfo.setIsAdd(false);
		sourceLogsInfo.setIsSuccess(true);
		sourceLogsInfo.setIsDelete(false);

		return sourceLogsInfo;
	}

	private String dealParentList(OutFamilyPo outFamily, String familyId) {
		List<UserInfo> parentList = outFamily.getParentList();
		List<UserInfo> newParentList = new ArrayList<UserInfo>();
		for (UserInfo p : parentList) {
			UserInfo u = userInfoMapper.getUserInfoByEmail(p.getEmail());
			if (u == null) {
				newParentList.add(p);
			} else {
				familyId = u.getFamilyId();
				UserInfo user = new UserInfo();
				newParentList.add(user);
				outFamily.setFamilyName(familyInfoMapper.selectByPrimaryKey(familyId).getFamilyName());
			}
		}
		outFamily.setParentList(newParentList);
		return familyId;
	}

	@Override
	public ServiceResult<Object> addFamilyOut(OutFamilyPo outFamily, OutModleVo outModleVo) {
		log.info("addFamilyOut|start|");

		String familyId = UUID.randomUUID().toString();
		// 判断是否是同一个家庭
		// if (Boolean.parseBoolean(getSystemMsg("same_family_flag"))) {
		// familyId = dealParentList(outFamily, familyId);
		// }
		// 家庭名字非空验证，邮箱格式验证
		ServiceResult<Object> result = validateOut(outFamily);
		if (!result.isSuccess()) {
			return failResult(result.getMsg());
		}
		UserInfo child = outFamily.getChild();
		log.info("add child");
		// 判断小孩所在的center、room、group是否存在
		int res_center = -1;
		// 判断center是否存在
		List<String> centersIds = outModleVo.getCentreList();
		if (!ListUtil.isEmpty(centersIds)) {
			for (String id : centersIds) {
				res_center = centersInfoMapper.isCenterExist(id);
				log.info("addFamily|res_center=" + res_center);
				// 若有不存在，返回失败并提示信息
				if (res_center == 0) {
					return failResult(getTipMsg("center_exist"));
				}
			}
		} else {
			// 园区必选，否则返回失败并提示信息
			return failResult(getTipMsg("center_empty"));
		}
		// 保存Application Request
		String appId = saveApplication(outFamily);
		// 保存Application Request Parent
		saveApplicationParent(appId, outFamily);
		// 保存Centre信息
		insertOutChildCenterInfo(appId, centersIds);
		// 插入外部页面额外字段
		insertOutRelevantInfo(appId, outModleVo);
		// 发送邮件给admin@kindikids.com.au
		sendSpecialEmail(outModleVo);
		outFamily.getChild().setId(appId);
		return successResult((Object) outFamily);

		// // 新增用戶
		// String childId = addUserOut(child, familyId, UserType.Child, new
		// Date());
		// // 新增賬戶信息
		// log.info("start|buildRelationKidParentInfo");
		// AccountInfo accountInfo = addAccountOut(child, Role.Children);
		// // 建立关系
		// buildRelationKidParentInfo(child);
		// // 新增attends
		// addAttendsOut(child.getId(), outFamily);
		// // 新增健康详细信息
		// addHealthMedicalDetail(child.getId());
		// ChildInfoVo childInfoVo = new ChildInfoVo();
		// // 初始化饮食要求信息
		// initOrUpdateChildDietaryRequireInfo(child.getId(),
		// childInfoVo.getDietaryRequire(), null);
		// // 初始化药物信息
		// initOrUpdateChildMedicalInfo(child.getId(), childInfoVo.getMedical(),
		// null);
		// Date now = new Date();
		// for (UserInfo parent : outFamily.getParentList()) {
		// if (StringUtil.isEmpty(parent.getFirstName()) ||
		// StringUtil.isEmpty(parent.getLastName())
		// || StringUtil.isEmpty(parent.getEmail())) {
		// continue;
		// }
		// // 新增用戶
		// addUserOut(parent, familyId, UserType.Parent, now);
		// // 新增賬戶信息
		// addAccountOut(parent, Role.Parent);
		// log.info("start|buildRelationKidParentInfo");
		// // 建立关系
		// buildRelationKidParentInfo(parent);
		// now.setTime(now.getTime() + 1000);
		// }
		// // 更新FamilyInfo
		// String familyName = outFamily.getFamilyName();
		// initOrUpdateFamilyInfo(familyId, familyName);
		// log.info("addFamilyOut|end|familyName=" + familyName);
		// // 插入所选的Center
		// insertOutChildCenterInfo(childId, centersIds);
		// // 插入外部页面额外字段
		// insertOutRelevantInfo(childId, outModleVo);
		// // 发送邮件给admin@kindikids.com.au
		// sendSpecialEmail(outModleVo);
		// return successResult((Object) accountInfo);
	}

	/**
	 * 保存Application Rquest
	 * 
	 * @author hxzhang at 2018年5月9日下午8:16:08
	 * @param outFamily
	 * @return
	 */
	private String saveApplication(OutFamilyPo outFamily) {
		// 保存孩子
		UserInfo child = outFamily.getChild();
		ApplicationList applicationList = new ApplicationList();
		String id = UUID.randomUUID().toString();
		applicationList.setId(id);
		applicationList.setFirstName(child.getFirstName());
		applicationList.setLastName(child.getLastName());
		applicationList.setGender(child.getGender());
		applicationList.setBirthday(child.getBirthday());
		applicationList.setRequestedStartDate(outFamily.getChangeDate());
		applicationList.setMonday(outFamily.getMonday());
		applicationList.setTuesday(outFamily.getTuesday());
		applicationList.setWednesday(outFamily.getWednesday());
		applicationList.setThursday(outFamily.getThursday());
		applicationList.setFriday(outFamily.getFriday());
		Date now = new Date();
		applicationList.setApplicationDate(now);
		applicationList.setApplicationDatePosition(now);
		applicationList.setUpdateTime(now);
		applicationList.setStatus(ApplicationStatus.PendingForAction.getValue());
		applicationList.setDeleteFlag(false);
		applicationListMapper.insert(applicationList);
		return id;
	}

	/**
	 * 保存Application Parent
	 * 
	 * @author hxzhang at 2018年5月9日下午8:16:31
	 * @param aapId
	 * @param outFamily
	 */
	private void saveApplicationParent(String appId, OutFamilyPo outFamily) {
		List<UserInfo> list = outFamily.getParentList();
		Date now = new Date();
		for (UserInfo u : list) {
			if (StringUtil.isEmpty(u.getEmail())) {
				continue;
			}
			ApplicationParent parent = new ApplicationParent();
			String id = UUID.randomUUID().toString();
			parent.setId(id);
			parent.setApplicationId(appId);
			parent.setFirstName(u.getFirstName());
			parent.setLastName(u.getLastName());
			parent.setAddress(u.getAddress());
			parent.setSuburb(u.getSuburb());
			parent.setPostcode(u.getPostcode());
			parent.setHomePhone(u.getPhoneNumber());
			parent.setWorkPhone(u.getWorkPhoneNumber());
			parent.setMobile(u.getMobileNumber());
			parent.setEmail(u.getEmail());
			parent.setCreateTime(now);
			parent.setUpdateTime(now);
			parent.setDeleteFlag(false);
			applicationParentMapper.insert(parent);
			now.setTime(now.getTime() + 1000);
		}
	}

	/**
	 * @description 插入外部所选Center信息
	 * @author hxzhang
	 * @create 2016年12月9日下午2:45:12
	 */
	private void insertOutChildCenterInfo(String childId, List<String> ids) {
		for (String centerId : ids) {
			outChildCenterInfoMapper.insert(new OutChildCenterInfo(childId, centerId));
		}
	}

	/**
	 * @description 初始化OutRelevantInfo
	 * @author hxzhang
	 * @create 2016年12月9日上午8:34:53
	 */
	private void insertOutRelevantInfo(String childId, OutModleVo outModleVo) {
		OutRelevantInfo outRelevantInfo = new OutRelevantInfo();
		outRelevantInfo.setId(UUID.randomUUID().toString());
		outRelevantInfo.setChildUserId(childId);
		outRelevantInfo.setRelationship1(outModleVo.getRelationship1());
		outRelevantInfo.setRelationship2(outModleVo.getRelationship2());
		outRelevantInfo.setNationality(outModleVo.getNationality());
		outRelevantInfo.setLanguage(outModleVo.getLanguages());
		outRelevantInfo.setReligion(outModleVo.getReligion());
		if (StringUtil.isNotEmpty(outModleVo.getHasImmunised())) {
			outRelevantInfo.setHasImmunised(Short.valueOf(outModleVo.getHasImmunised()));
		}
		if (StringUtil.isNotEmpty(outModleVo.getHasAnaphylaxis())) {
			outRelevantInfo.setHasAnaphylaxis(Short.valueOf(outModleVo.getHasAnaphylaxis()));
		}
		outRelevantInfo.setLearningDifficulties(outModleVo.getLearningDifficulties());
		outRelevantInfo.setMedicalConditions(outModleVo.getMedicalConditions());
		outRelevantInfo.setDietaryRequipments(outModleVo.getDietaryRequipments());
		outRelevantInfo.setAllergies(outModleVo.getAllergies());
		outRelevantInfo.setProposal(outModleVo.getProposal());
		if (StringUtil.isNotEmpty(outModleVo.getChannel())) {
			String choose = outModleVo.getChannel().toLowerCase();
			if (SelectAnOption.Google.getDesc().toLowerCase().equals(choose)) {
				outRelevantInfo.setChannel(SelectAnOption.Google.getValue());

			} else if (SelectAnOption.Friend.getDesc().toLowerCase().equals(choose)) {
				outRelevantInfo.setChannel(SelectAnOption.Friend.getValue());

			} else if (SelectAnOption.Website.getDesc().toLowerCase().equals(choose)) {
				outRelevantInfo.setChannel(SelectAnOption.Website.getValue());

			} else if (SelectAnOption.Other.getDesc().toLowerCase().equals(choose)) {
				outRelevantInfo.setChannel(SelectAnOption.Other.getValue());
				outRelevantInfo.setChannelOther(outModleVo.getChannelOther());

			} else if (SelectAnOption.Facebook.getDesc().toLowerCase().equals(choose)) {
				outRelevantInfo.setChannel(SelectAnOption.Facebook.getValue());

			} else if (SelectAnOption.Instagram.getDesc().toLowerCase().equals(choose)) {
				outRelevantInfo.setChannel(SelectAnOption.Instagram.getValue());

			} else if (SelectAnOption.DrovePast.getDesc().toLowerCase().equals(choose)) {
				outRelevantInfo.setChannel(SelectAnOption.DrovePast.getValue());

			}
		}

		outRelevantInfoMapper.insertSelective(outRelevantInfo);
	}

	/**
	 * 
	 * @description addAttendsOut
	 * @author gfwang
	 * @create 2016年8月29日下午8:13:08
	 * @version 1.0
	 * @param userId
	 *            小孩id
	 */
	private void addAttendsOut(String userId, OutFamilyPo outFamily) {
		log.info("addAttendsOut|start|useId=" + userId);
		Date now = new Date();
		ChildAttendanceInfo childAttendanceInfo = new ChildAttendanceInfo();
		childAttendanceInfo.setId(UUID.randomUUID().toString());
		childAttendanceInfo.setUserId(userId);
		childAttendanceInfo.setDeleteFlag(DeleteFlag.Default.getValue());
		childAttendanceInfo.setCreateTime(outFamily.getApplicationDate());
		childAttendanceInfo.setPositioningdate(outFamily.getApplicationDatePosition());
		childAttendanceInfo.setUpdateTime(now);
		childAttendanceInfo.setMonday(outFamily.getMonday());
		childAttendanceInfo.setTuesday(outFamily.getTuesday());
		childAttendanceInfo.setWednesday(outFamily.getWednesday());
		childAttendanceInfo.setThursday(outFamily.getThursday());
		childAttendanceInfo.setFriday(outFamily.getFriday());
		childAttendanceInfo.setEnrolled(EnrolledState.Default.getValue());
		childAttendanceInfo.setType(ChildType.External.getValue());
		childAttendanceInfo.setEnrolmentDate(outFamily.getChangeDate());
		childAttendanceInfo.setRequestStartDate(outFamily.getChangeDate());
		childAttendanceInfo.setSiblingflag(outFamily.isSiblingFlag());
		childAttendanceInfoMapper.insertSelective(childAttendanceInfo);
		log.info("addAttendsOut|start|useId=" + userId);
	}

	/**
	 * 
	 * @description 新增用戶
	 * @author gfwang
	 * @create 2016年8月29日下午7:52:49
	 * @version 1.0
	 * @param userInfo
	 *            用戶
	 * @param familyId
	 *            familyId
	 */
	private String addUserOut(UserInfo userInfo, String familyId, UserType userType, Date now) {
		String userId = UUID.randomUUID().toString();
		log.info("addUserOut|satrt|familyId=" + familyId + "|userId=" + userId);
		userInfo.setId(userId);
		userInfo.setCreateTime(now);
		userInfo.setUpdateTime(now);
		userInfo.setFamilyId(familyId);
		userInfo.setUserType(userType.getValue());
		userInfo.setDeleteFlag(DeleteFlag.Default.getValue());
		// 获取personColor
		String personColor = commonService.getPersonColor(userInfo);
		log.info("addUserOut|personColor=" + personColor);
		userInfo.setPersonColor(personColor);
		userInfoMapper.insertSelective(userInfo);
		log.info("addUserOut|end|familyId=" + familyId + "|userId=" + userId);
		return userId;
	}

	/**
	 * 
	 * @description 新增外部Account
	 * @author gfwang
	 * @create 2016年8月29日下午7:58:17
	 * @version 1.0
	 * @param userInfo
	 *            userInfo
	 * @param role
	 *            角色
	 */
	private AccountInfo addAccountOut(UserInfo userInfo, Role role) {
		String accountId = UUID.randomUUID().toString();
		log.info("addAccountOut|satrt|role=" + role.getValue() + "|accountId=" + accountId);
		AccountInfo accountInfo = new AccountInfo();
		accountInfo.setId(accountId);
		accountInfo.setAccount(userInfo.getEmail());
		Date now = new Date();
		accountInfo.setCreateTime(now);
		accountInfo.setDeleteFlag(DeleteFlag.Default.getValue());
		accountInfo.setStatus(ArchivedStatus.Default.getValue());
		accountInfo.setUpdateTime(now);
		accountInfo.setUserId(userInfo.getId());
		accountInfoMapper.insertSelective(accountInfo);

		// 新增AccountRoleInfo
		AccountRoleInfo accountRoleInfo = new AccountRoleInfo();
		accountRoleInfo.setAccountId(accountId);
		accountRoleInfo.setCreateTime(now);
		accountRoleInfo.setDeleteFlag(DeleteFlag.Default.getValue());
		accountRoleInfo.setId(UUID.randomUUID().toString());
		accountRoleInfo.setRoleId(roleInfoMapper.selectRoleIdByValue(role.getValue()));
		accountRoleInfo.setUpdateTime(now);
		accountRoleInfoMapper.insertSelective(accountRoleInfo);
		log.info("addAccountOut|end|role=" + role.getValue() + "|accountId=" + accountId);
		return accountInfo;
	}

	/**
	 * @description 新增小孩健康详细信息
	 * @author hxzhang
	 * @create 2016年8月30日上午10:12:29
	 * @version 1.0
	 */
	private void addHealthMedicalDetail(String userId) {
		List<ChildHealthMedicalDetailInfo> healthMedicalDetailList = new ArrayList<ChildHealthMedicalDetailInfo>();
		for (int i = 0; i < 13; i++) {
			ChildHealthMedicalDetailInfo chmdi = new ChildHealthMedicalDetailInfo();
			Date now = new Date();
			chmdi.setId(UUID.randomUUID().toString());
			chmdi.setUserId(userId);
			chmdi.setDeleteFlag(DeleteFlag.Default.getValue());
			now.setTime(now.getTime() + i * 1000);
			chmdi.setCreateTime(now);
			chmdi.setNameIndex(i);
			healthMedicalDetailList.add(chmdi);
		}
		childHealthMedicalDetailInfoMapper.insterBatchChildHealthMedicalDetailInfo(healthMedicalDetailList);
	}

	private ServiceResult<Object> validateOut(OutFamilyPo outFamily) {
		if (outFamily == null) {
			return failResult(getTipMsg("out.data.empty"));
		}
		// 必须投familyName
		if (StringUtil.isEmpty(outFamily.getFamilyName())) {
			return failResult(getTipMsg("family_name"));
		}
		// 必须有小孩
		if (null == outFamily.getChild()) {
			return failResult(getTipMsg("out.child.empty"));
		}
		// 必须有家长
		if (ListUtil.isEmpty(outFamily.getParentList())) {
			return failResult(getTipMsg("out.child.parent.empty"));
		}
		UserInfo child = outFamily.getChild();
		ServiceResult<Object> result = validateOutInfo(child, true);
		if (!result.isSuccess()) {
			log.info("child validate fail");
			return failResult(result.getMsg());
		}

		// 判断是否有重复email
		Map<String, String> tempMap = new HashMap<String, String>();
		List<UserInfo> parents = outFamily.getParentList();
		// 第一个家长必填验证
		result = validateParent(parents.get(0), tempMap);
		if (!result.isSuccess() && !Boolean.parseBoolean(getSystemMsg("same_family_flag"))) {
			return result;
		}
		UserInfo p2 = parents.get(1);
		// 第二个家长选择性验证
		if (StringUtil.isNotEmpty(p2.getFirstName()) || StringUtil.isNotEmpty(p2.getLastName()) || StringUtil.isNotEmpty(p2.getEmail())) {
			result = validateParent(p2, tempMap);
			if (!result.isSuccess()) {
				return result;
			}
		}

		// Date of change非空验证
		if (null == outFamily.getChangeDate()) {
			return failResult(getTipMsg("out.changeDate.empty"));
		}
		// 上课时间不能少于两天
		List<Boolean> chooseList = isChoose(outFamily);
		if (chooseList.size() < 2) {
			return failResult(getTipMsg("out.chooseClassTime"));
		}

		return successResult();
	}

	/**
	 * @description 验证家长数据是否合法
	 * @author hxzhang
	 * @create 2016年12月12日下午4:44:05
	 */
	private ServiceResult<Object> validateParent(UserInfo parent, Map<String, String> tempMap) {
		ServiceResult<Object> result = validateOutInfo(parent, false);
		if (!result.isSuccess()) {
			log.info("parent validate fail");
			return failResult(result.getMsg());
		}
		// 判断是否正确的邮箱格式
		String email = parent.getEmail();
		// if (!RegexUtil.isMailAddress(email)) {
		// log.info("Incorrect email address format");
		// return failResult(getTipMsg("email_address"));
		// }
		if (tempMap.containsKey(email)) {
			log.info("list email repeat");
			return failResult(getTipMsg("parent.email.repeat"));
		}
		tempMap.put(email, parent.getEmail());

		return result;
	}

	/**
	 * 
	 * @description 验证外部信息是否正确
	 * @author gfwang
	 * @create 2016年8月29日下午7:48:07
	 * @version 1.0
	 * @param user
	 *            用户
	 * @param isChild
	 *            是否小孩
	 * @return true |false
	 */
	private ServiceResult<Object> validateOutInfo(UserInfo user, boolean isChild) {
		// firstName 判空
		if (StringUtil.isEmpty(user.getFirstName())) {
			if (isChild) {
				return failResult(getTipMsg("child_firstName"));
			} else {
				return failResult(getTipMsg("parent_givenName"));
			}
		}
		// firstName 长度判断
		if (user.getFirstName().length() > 50) {
			if (isChild) {
				return failResult(getTipMsg("child_firstName_overflow"));
			} else {
				return failResult(getTipMsg("parent_givenName_overflow"));
			}
		}
		// lastName 判空
		if (StringUtil.isEmpty(user.getLastName())) {
			if (isChild) {
				return failResult(getTipMsg("child_surname"));
			} else {
				return failResult(getTipMsg("parent_surname"));
			}
		}
		// lastName 长度判断
		if (user.getLastName().length() > 50) {
			if (isChild) {
				return failResult(getTipMsg("child_surname_overflow"));
			} else {
				return failResult(getTipMsg("parent_surname_overflow"));
			}
		}
		// 性别判空
		if (user.getGender() == null) {
			if (isChild) {
				return failResult(getTipMsg("child_gender"));
			}
		}
		// 邮箱判空
		if (!isChild && StringUtil.isEmpty(user.getEmail())) {
			log.info("parent_email");
			return failResult(getTipMsg("parent_email"));
		}
		// 判断邮箱重复
		// if (!isChild) {
		// boolean isrepeat = userInfoService.validateEmail(user.getEmail(),
		// null);
		// if (!isrepeat) {
		// log.info("parent.email.repeat");
		// return failResult(getTipMsg("parent.email.repeat"));
		// }
		// }

		return successResult();
	}

	/**
	 * @description 初始化UserInfo,并插入数据库
	 * @author hxzhang
	 * @create 2016年7月28日下午4:45:25
	 * @version 1.0
	 * @param userInfo
	 *            UserInfo对象
	 * @param currentUser
	 *            当前登录用户
	 * @return
	 */
	private UserInfo initOrUpdateUserInfo(UserInfo userInfo, UserInfoVo currentUser, String familyId, Short userType, SourceLogsInfo sourceLogsInfo) {
		// 父选项未选,子选项清空
		if (null == userInfo.getFamilyStatus() || userInfo.getFamilyStatus() != 3) {
			userInfo.setFamilyStatusOther("");
		}
		List<FileVo> fileVoList = new ArrayList<FileVo>();
		fileVoList.addAll(userInfo.getFileList());
		String userId = "";
		String fileId = "";
		if (StringUtil.isEmpty(familyId)) {
			familyId = UUID.randomUUID().toString();
		}
		if (StringUtil.isEmpty(userInfo.getId())) {
			userId = UUID.randomUUID().toString();
			userInfo.setId(userId);
			Date now = new Date();
			userInfo.setCreateTime(now);
			userInfo.setUpdateTime(now);
			userInfo.setCreateAccountId(currentUser.getUserInfo().getId());
			userInfo.setUpdateAccountId(currentUser.getUserInfo().getId());
			if (StringUtil.isNotEmpty(userInfo.getAvatar())) {
				try {
					String result = FilePathUtil.getMainPath(FilePathUtil.AVATAR_CHILD, userInfo.getAvatar());
					new FileFactory(Region.getRegion(Regions.AP_SOUTHEAST_2), PropertieNameConts.S3).copyFile(userInfo.getAvatar(), result);
					userInfo.setAvatar(result);
				} catch (FileNotFoundException e) {
					log.info("initOrUpdateUserInfo|save|FileNotFoundException s3 error", e);
					e.printStackTrace();
				} catch (Exception e) {
					log.info("initOrUpdateUserInfo|save|s3 error", e);
					e.printStackTrace();
				}
			}
			userInfo.setFamilyId(familyId);
			userInfo.setUserType(userType);
			fileId = UUID.randomUUID().toString();
			userInfo.setBirthCert(fileId);
			userInfo.setDeleteFlag(DeleteFlag.Default.getValue());
			String color = commonService.getPersonColor(userInfo);
			userInfo.setPersonColor(color);
			userInfoMapper.insertSelective(userInfo);
		} else {
			// 获取数据库用户信息
			userId = userInfo.getId();
			UserInfo dbUser = userInfoMapper.selectByPrimaryKey(userId);
			if (StringUtil.isNotEmpty(userInfo.getAvatar())) {
				if (userInfo.getAvatar().contains("http")) {
					userInfo.setAvatar(dbUser.getAvatar());
				} else {
					try {
						String result = FilePathUtil.getMainPath(FilePathUtil.AVATAR_CHILD, userInfo.getAvatar());
						new FileFactory(Region.getRegion(Regions.AP_SOUTHEAST_2), PropertieNameConts.S3).copyFile(userInfo.getAvatar(), result);
						userInfo.setAvatar(result);
					} catch (FileNotFoundException e) {
						log.info("initOrUpdateUserInfo|update|FileNotFoundException s3 error", e);
						e.printStackTrace();
					} catch (Exception e) {
						log.info("initOrUpdateUserInfo|update|s3 error", e);
						e.printStackTrace();
					}
				}
			}
			fileId = dbUser.getBirthCert();
			if (StringUtil.isEmpty(fileId)) {
				fileId = UUID.randomUUID().toString();
				userInfo.setBirthCert(fileId);
			}
			userInfo.setUpdateAccountId(currentUser.getUserInfo().getId());
			userInfo.setUpdateTime(new Date());
			userInfo.setCreateTime(dbUser.getCreateTime());
			userInfo.setCreateAccountId(dbUser.getCreateAccountId());
			userInfo.setFamilyId(dbUser.getFamilyId());
			userInfo.setUserType(dbUser.getUserType());
			userInfo.setDeleteFlag(dbUser.getDeleteFlag());
			userInfo.setOldId(dbUser.getOldId());

			if (StringUtil.isNotEmpty(userInfo.getCrn())) {
				userInfo.setHubworkId(dbUser.getHubworkId());
			} else {
				userInfo.setHubworkId(null);
			}

			userInfoMapper.updateByPrimaryKey(userInfo);

			if (userType == UserType.Child.getValue()) {
				updateLogsDetaile(dbUser, userInfo, "userInfo", sourceLogsInfo, "");
			} else {
				updateLogsDetaileparent(dbUser, userInfo, "userInfo", sourceLogsInfo);
				// 保存家长
			}
			// 记录历史记录

			// 删除之前附件
			delFile(fileVoList, fileId);
		}
		// 上传附件
		try {
			commonService.initAttachmentInfo(fileId, fileVoList, FilePathUtil.FAMILY_PATH);
		} catch (Exception e) {
			logger.error("msg_file_upload_failed||", e);
			throw new RuntimeException();

		}
		return userInfo;
	}

	private void updateLogsDetaileparent(Object oldObj, Object newObj, String detaileType, SourceLogsInfo sourceLogsInfo) {
		if (sourceLogsInfo == null)
			return;
		String sourceId = sourceLogsInfo.getId();
		boolean havaLogs = false;
		if (oldObj == null || newObj == null)
			return;

		List<LogsDetaileInfo> logsInfoList = new ArrayList<LogsDetaileInfo>();
		try {
			logsInfoList = ObjectCompUtil.getResult(oldObj, newObj, "dd/MM/yyyy", sourceId, null, null, "parent", getFeildsProperties());
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		for (int i = 0, m = logsInfoList.size(); i < m; i++) {
			LogsDetaileInfo logDetaileInfo = logsInfoList.get(i);
			if ("parent_gender".equals(logDetaileInfo.getChangeFieldName())) {
				if ("0".equals(logDetaileInfo.getOldValue()))
					logDetaileInfo.setOldValue("Female");
				if ("1".equals(logDetaileInfo.getOldValue()))
					logDetaileInfo.setOldValue("Male");
				if ("0".equals(logDetaileInfo.getNewValue()))
					logDetaileInfo.setNewValue("Female");
				if ("1".equals(logDetaileInfo.getNewValue()))
					logDetaileInfo.setNewValue("Male");
			}
			logsDetaileInfoMapper.insert(logDetaileInfo);
			havaLogs = true;
		}
		// 判断是否有logs记录
		if (havaLogs) {
			SourceLogsInfo dbsourceLogsInfo = sourceLogsInfoMapper.selectByPrimaryKey(sourceLogsInfo.getId());
			if (dbsourceLogsInfo == null) {
				sourceLogsInfoMapper.insert(sourceLogsInfo);
			}
		}

	}

	private void updateLogsDetaile(Object oldObj, Object newObj, String detaileType, SourceLogsInfo sourceLogsInfo, String index) {
		if (sourceLogsInfo == null)
			return;
		String sourceId = sourceLogsInfo.getId();
		boolean havaLogs = false;
		if (oldObj == null || newObj == null)
			return;
		String keyIndex = index;
		String tagsIndex = index;
		String compType = detaileType;
		List<LogsDetaileInfo> logsInfoList = new ArrayList<LogsDetaileInfo>();
		switch (detaileType) {
		case "userInfo":// USER对象更新
			keyIndex = "";
			tagsIndex = "";
			break;
		case "emergencyContact":// 紧急联系人
		case "sameHousehold":
			keyIndex = "";
			break;
		case "BackgroundPetsInfo":
			keyIndex = "";
			compType = "";
			break;
		case "HealthMedicalDetailInfo":
			tagsIndex = "";
			compType = "";
			break;
		case "HealthMedical":
		case "Background":
		case "custody":
		case "dietaryRequire":
		case "Medical":
			keyIndex = "";
			tagsIndex = "";
			compType = "";
			break;
		}
		try {
			logsInfoList = ObjectCompUtil.getResult(oldObj, newObj, "dd/MM/yyyy", sourceId, keyIndex, tagsIndex, compType, getFeildsProperties());
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

		for (int i = 0, m = logsInfoList.size(); i < m; i++) {
			LogsDetaileInfo logDetaileInfo = logsInfoList.get(i);
			switch (detaileType) {
			case "userInfo":// USER对象更新
				// 更改性别对于的英文gender
				if ("userInfo_gender".equals(logDetaileInfo.getChangeFieldName())) {
					if ("0".equals(logDetaileInfo.getOldValue()))
						logDetaileInfo.setOldValue("Female");
					if ("1".equals(logDetaileInfo.getOldValue()))
						logDetaileInfo.setOldValue("Male");
					if ("0".equals(logDetaileInfo.getNewValue()))
						logDetaileInfo.setNewValue("Female");
					if ("1".equals(logDetaileInfo.getNewValue()))
						logDetaileInfo.setNewValue("Male");
				}
				// familyStatus需要单独处理
				if ("userInfo_familyStatus".equals(logDetaileInfo.getChangeFieldName())) {
					logDetaileInfo.setOldValue(getFeildsProperties().getProperty("familyStatus" + logDetaileInfo.getOldValue()));
					logDetaileInfo.setNewValue(getFeildsProperties().getProperty("familyStatus" + logDetaileInfo.getNewValue()));
				}
				break;
			case "HealthMedical":
			case "custody":
				// custodyParentId 转换成家长名字
				if ("custodyParentId".equals(logDetaileInfo.getChangeFieldName())) {
					UserInfo userInfo = userInfoMapper.selectByPrimaryKey(logDetaileInfo.getOldValue());
					if (userInfo != null) {
						logDetaileInfo.setOldValue(getUserName(userInfo));
					}
					userInfo = userInfoMapper.selectByPrimaryKey(logDetaileInfo.getNewValue());
					if (userInfo != null) {
						logDetaileInfo.setNewValue(getUserName(userInfo));
					}
				}
				break;
			case "emergencyContact":// 紧急联系人
				// relationshipChild，转换成文本
				if ("emergencyContact_relationshipChild".equals(logDetaileInfo.getChangeFieldName())) {
					logDetaileInfo.setOldValue(getFeildsProperties().getProperty("relationshipChild" + logDetaileInfo.getOldValue()));
					logDetaileInfo.setNewValue(getFeildsProperties().getProperty("relationshipChild" + logDetaileInfo.getNewValue()));
				}
				// personAuthority1 更改为yes no
				if ("emergencyContact_personAuthority1".equals(logDetaileInfo.getChangeFieldName())
						|| "emergencyContact_personAuthority2".equals(logDetaileInfo.getChangeFieldName())
						|| "emergencyContact_personAuthority3".equals(logDetaileInfo.getChangeFieldName())
						|| "emergencyContact_personAuthority4".equals(logDetaileInfo.getChangeFieldName())
						|| "emergencyContact_personAuthority5".equals(logDetaileInfo.getChangeFieldName())
						|| "emergencyContact_personAuthority6".equals(logDetaileInfo.getChangeFieldName())) {

					boolToYesNo(logDetaileInfo);
				}
				break;
			case "HealthMedicalDetailInfo":
				boolToYesNo(logDetaileInfo);
				break;
			case "Background":
				// sameHousehold/bottleCup/childHavePets/underTwoYears/specialRequirementsSleep/clothingNeeds/assistanceDuringFeed/haveCulturalReligious
				// true false
				if ("sameHousehold".equals(logDetaileInfo.getChangeFieldName()) || "bottleCup".equals(logDetaileInfo.getChangeFieldName())
						|| "childHavePets".equals(logDetaileInfo.getChangeFieldName()) || "underTwoYears".equals(logDetaileInfo.getChangeFieldName())
						|| "specialRequirementsSleep".equals(logDetaileInfo.getChangeFieldName()) || "clothingNeeds".equals(logDetaileInfo.getChangeFieldName())
						|| "assistanceDuringFeed".equals(logDetaileInfo.getChangeFieldName()) || "haveCulturalReligious".equals(logDetaileInfo.getChangeFieldName())) {

					boolToYesNo(logDetaileInfo);
				}
				if ("wayToilet".equals(logDetaileInfo.getChangeFieldName())) {
					logDetaileInfo.setOldValue(getFeildsProperties().getProperty("wayToilet_" + logDetaileInfo.getOldValue()));
					logDetaileInfo.setNewValue(getFeildsProperties().getProperty("wayToilet_" + logDetaileInfo.getNewValue()));
				}
				break;
			case "dietaryRequire":
				if (";dairy1;dairy2;dairy3;dairyOther;nuts1;nuts2;nuts3;nuts4;nutsOther;eggs1;eggs2;eggs3;eggsOther;meat1;meat2;meat3;meat4;meat5;meatOther;"
						.contains(";" + logDetaileInfo.getChangeFieldName() + ";")) {
					boolToYesNo(logDetaileInfo);
				}

				if ("epiPen".equals(logDetaileInfo.getChangeFieldName())) {
					if ("0".equals(logDetaileInfo.getOldValue()))
						logDetaileInfo.setOldValue("NO");
					if ("1".equals(logDetaileInfo.getOldValue()))
						logDetaileInfo.setOldValue("YES");
					if ("0".equals(logDetaileInfo.getNewValue()))
						logDetaileInfo.setNewValue("NO");
					if ("1".equals(logDetaileInfo.getNewValue()))
						logDetaileInfo.setNewValue("YES");
				}
				break;
			case "Medical":
				if (";applicableEpilepsy;applicableAsthma;applicableEczema;applicableOther;".contains(";" + logDetaileInfo.getChangeFieldName() + ";")) {
					boolToYesNo(logDetaileInfo);
				}

				if ("requireLongMedic".equals(logDetaileInfo.getChangeFieldName())) {
					// update by gfwang 04/17/2017
					if ("-1".equals(logDetaileInfo.getOldValue())) {
						logDetaileInfo.setOldValue("");
					}
					if ("0".equals(logDetaileInfo.getOldValue()))
						logDetaileInfo.setOldValue("NO");
					if ("1".equals(logDetaileInfo.getOldValue()))
						logDetaileInfo.setOldValue("YES");
					if ("0".equals(logDetaileInfo.getNewValue()))
						logDetaileInfo.setNewValue("NO");
					if ("1".equals(logDetaileInfo.getNewValue()))
						logDetaileInfo.setNewValue("YES");
				}
				break;
			}
			logsDetaileInfoMapper.insert(logDetaileInfo);
			havaLogs = true;
		}

		// 判断是否有logs记录
		if (havaLogs) {
			SourceLogsInfo dbsourceLogsInfo = sourceLogsInfoMapper.selectByPrimaryKey(sourceLogsInfo.getId());
			if (dbsourceLogsInfo == null) {
				sourceLogsInfoMapper.insert(sourceLogsInfo);
			}
		}
	}

	private void boolToYesNo(LogsDetaileInfo logDetaileInfo) {
		if ("true".equals(logDetaileInfo.getOldValue())) {
			logDetaileInfo.setOldValue("YES");
		} else if ("false".equals(logDetaileInfo.getOldValue())) {
			logDetaileInfo.setOldValue("NO");
		}

		if ("true".equals(logDetaileInfo.getNewValue())) {
			logDetaileInfo.setNewValue("YES");
		} else if ("false".equals(logDetaileInfo.getNewValue())) {
			logDetaileInfo.setNewValue("NO");
		}
	}

	/*
	 * 获取username全称
	 */
	private String getUserName(UserInfo userInfo) {
		String name = "";
		if (StringUtil.isNotEmpty(userInfo.getFirstName())) {
			name = userInfo.getFirstName();
		}
		if (StringUtil.isNotEmpty(userInfo.getMiddleName())) {
			if (StringUtil.isEmpty(name)) {
				name = userInfo.getMiddleName();
			} else {
				name = name + " " + userInfo.getMiddleName();
			}
		}
		if (StringUtil.isNotEmpty(userInfo.getLastName())) {
			if (StringUtil.isEmpty(name)) {
				name = userInfo.getLastName();
			} else {
				name = name + " " + userInfo.getLastName();
			}
		}
		return name;
	}

	/**
	 * @description 初始化小孩的监护安排,并插入数据库
	 * @author hxzhang
	 * @create 2016年7月28日下午4:44:09
	 * @version 1.0
	 * @param userId
	 *            小孩ID
	 * @param custodyArrange
	 *            ChildCustodyArrangeInfo
	 */
	private void initOrUpdateChildCustodyArrangeInfo(String userId, ChildCustodyArrangeInfo custodyArrange, SourceLogsInfo sourceLogsInfo) {
		if (custodyArrange == null) {
			return;
		}
		List<FileVo> fileVoList = new ArrayList<FileVo>();
		fileVoList.addAll(custodyArrange.getFileList());
		String fileId = "";
		if (StringUtil.isEmpty(custodyArrange.getId())) {
			custodyArrange.setId(UUID.randomUUID().toString());
			custodyArrange.setUserId(userId);
			fileId = UUID.randomUUID().toString();
			custodyArrange.setLegalDocumentId(fileId);
			custodyArrange.setDeleteFlag(DeleteFlag.Default.getValue());
			custodyArrangeMapper.insertSelective(custodyArrange);
		} else {
			ChildCustodyArrangeInfo dbCustodyArrange = custodyArrangeMapper.selectByPrimaryKey(custodyArrange.getId());
			fileId = dbCustodyArrange.getLegalDocumentId();
			custodyArrange.setDeleteFlag(dbCustodyArrange.getDeleteFlag());
			custodyArrange.setUserId(dbCustodyArrange.getUserId());
			custodyArrangeMapper.updateByPrimaryKey(custodyArrange);

			// 记录历史记录
			updateLogsDetaile(dbCustodyArrange, custodyArrange, "custody", sourceLogsInfo, "");

			// 删除之前附件
			delFile(fileVoList, fileId);
		}
		// 上传附件
		try {
			commonService.initAttachmentInfo(fileId, fileVoList, FilePathUtil.FAMILY_PATH);
		} catch (Exception e) {
			logger.error("msg_file_upload_failed||", e);
			throw new RuntimeException();
		}
	}

	/**
	 * @description 初始化饮食要求,并插入数据库
	 * @author hxzhang
	 * @create 2016年7月28日下午5:18:18
	 * @version 1.0
	 * @param userId
	 *            小孩ID
	 * @param dietaryRequire
	 *            ChildDietaryRequireInfo
	 */
	private void initOrUpdateChildDietaryRequireInfo(String userId, ChildDietaryRequireInfo dietaryRequire, SourceLogsInfo sourceLogsInfo) {
		if (dietaryRequire == null) {
			return;
		}
		// 父选项未勾选,子选项清空
		if (unselected(dietaryRequire.getDairyOther())) {
			dietaryRequire.setDairyOherSpecify("");
		}
		if (unselected(dietaryRequire.getNutsOther())) {
			dietaryRequire.setNutsOtherSpecify("");
		}
		if (unselected(dietaryRequire.getEggsOther())) {
			dietaryRequire.setEggsOtherSpecify("");
		}
		if (unselected(dietaryRequire.getMeatOther())) {
			dietaryRequire.setMeatOtherSpecify("");
		}
		List<FileVo> fileVoList = new ArrayList<FileVo>();
		fileVoList.addAll(dietaryRequire.getFileList());
		String fileId = "";
		if (StringUtil.isEmpty(dietaryRequire.getId())) {
			fileId = UUID.randomUUID().toString();
			dietaryRequire.setId(UUID.randomUUID().toString());
			dietaryRequire.setUserId(userId);
			dietaryRequire.setEpiPenAttachId(fileId);
			dietaryRequire.setDeleteFlag(DeleteFlag.Default.getValue());
			dietaryRequireMapper.insertSelective(dietaryRequire);
		} else {
			ChildDietaryRequireInfo dbDietaryRequire = dietaryRequireMapper.selectByPrimaryKey(dietaryRequire.getId());
			fileId = dbDietaryRequire.getEpiPenAttachId();
			// 父选项未勾选,子选项清空
			if (!isChoose(dietaryRequire.getEpiPen())) {
				dietaryRequire.setEpiPenAttachId("");
				fileVoList = null;
			}
			fileId = StringUtil.isEmpty(fileId) ? UUID.randomUUID().toString() : fileId;
			dietaryRequire.setEpiPenAttachId(fileId);
			dietaryRequire.setUserId(dbDietaryRequire.getUserId());
			dietaryRequire.setDeleteFlag(dbDietaryRequire.getDeleteFlag());
			dietaryRequireMapper.updateByPrimaryKey(dietaryRequire);

			// 记录历史记录
			updateLogsDetaile(dbDietaryRequire, dietaryRequire, "dietaryRequire", sourceLogsInfo, "");

			// 删除之前的附件
			delFile(fileVoList, fileId);
		}
		// 上传附件
		try {
			commonService.initAttachmentInfo(fileId, fileVoList, FilePathUtil.FAMILY_PATH);
		} catch (Exception e) {
			logger.error("msg_file_upload_failed||", e);
			throw new RuntimeException();
		}
	}

	/**
	 * @description 初始化药物信息,并插入数据库
	 * @author hxzhang
	 * @create 2016年7月28日下午5:22:38
	 * @version 1.0
	 * @param userId
	 *            小孩ID
	 * @param medical
	 *            ChildMedicalInfo
	 */
	private void initOrUpdateChildMedicalInfo(String userId, ChildMedicalInfo medical, SourceLogsInfo sourceLogsInfo) {
		if (medical == null) {
			return;
		}
		// 父选项未选择,子选项清空
		if (unselected(medical.getApplicableOther())) {
			medical.setApplicableOtherSpecify("");
		}
		List<FileVo> fileVoList = new ArrayList<FileVo>();
		fileVoList.addAll(medical.getFileList());
		String fileId = "";
		if (StringUtil.isEmpty(medical.getId())) {
			medical.setId(UUID.randomUUID().toString());
			medical.setUserId(userId);
			fileId = UUID.randomUUID().toString();
			medical.setRequireLongMedicAttachId(fileId);
			medical.setDeleteFlag(DeleteFlag.Default.getValue());
			medicalMapper.insertSelective(medical);
		} else {
			ChildMedicalInfo dbMedical = medicalMapper.selectByPrimaryKey(medical.getId());
			fileId = dbMedical.getRequireLongMedicAttachId();
			// 父选项未选择,子选项清空
			if (!isChoose(medical.getRequireLongMedic())) {
				medical.setRequireLongMedicAttachId("");
				fileVoList = null;
			}
			fileId = StringUtil.isEmpty(fileId) ? UUID.randomUUID().toString() : fileId;
			medical.setRequireLongMedicAttachId(fileId);
			medical.setUserId(dbMedical.getUserId());
			medical.setDeleteFlag(dbMedical.getDeleteFlag());
			medicalMapper.updateByPrimaryKey(medical);

			// 记录历史记录
			updateLogsDetaile(dbMedical, medical, "Medical", sourceLogsInfo, "");

			// 删除之前的附件
			delFile(fileVoList, fileId);
		}
		// 上传附件
		try {
			commonService.initAttachmentInfo(fileId, fileVoList, FilePathUtil.FAMILY_PATH);
		} catch (Exception e) {
			logger.error("msg_file_upload_failed||", e);
			throw new RuntimeException();
		}
	}

	/**
	 * @description 初始化紧急联系人信息,批量插入数据
	 * @author hxzhang
	 * @create 2016年7月28日下午6:02:51
	 * @version 1.0
	 * @param userId
	 *            小孩ID
	 * @param emergencyContactList
	 *            List<EmergencyContactInfo>
	 */
	private void initOrUpdateEmergencyContactInfo(String userId, List<EmergencyContactInfo> emergencyContactList, SourceLogsInfo sourceLogsInfo) {
		// 重新插入数据
		int i = 1;
		for (EmergencyContactInfo ec : emergencyContactList) {
			String oldId = ec.getId();

			Date now = new Date();
			// 父选项未选,子选项清空
			if (null == ec.getRelationshipChild() || ec.getRelationshipChild() != 5) {
				ec.setRelationshipChildOther("");
			}
			ec.setId(UUID.randomUUID().toString());
			ec.setUserId(userId);
			now.setTime(now.getTime() + i * 1000);
			ec.setCreateTime(now);
			ec.setDeleteFlag(DeleteFlag.Default.getValue());

			// 记录历史记录
			// 获取数据库old数据
			EmergencyContactInfo dbInfo = emergencyContactMapper.selectByPrimaryKey(oldId);
			updateLogsDetaile(dbInfo, ec, "emergencyContact", sourceLogsInfo, String.valueOf(i));

			i += i;
		}
		// 删除之前记录,并插入新纪录
		emergencyContactMapper.removeEmergencyContactInfoByUserId(userId);
		if (null == emergencyContactList || emergencyContactList.size() == 0) {
			return;
		}
		emergencyContactMapper.insterBatchEmergencyContactInfo(emergencyContactList);
	}

	/**
	 * @description 初始化小孩背景信息
	 * @author hxzhang
	 * @create 2016年7月28日下午6:36:11
	 * @version 1.0
	 * @param userId
	 *            小孩ID
	 * @param background
	 *            ChildBackgroundInfoVo
	 */
	private void initOrUpdateChildBackgroundInfoVo(String userId, ChildBackgroundInfoVo background, SourceLogsInfo sourceLogsInfo) {
		if (background == null) {
			return;
		}
		ChildBackgroundInfo childBackgroundInfo = background.getChildBackgroundInfo();
		if (childBackgroundInfo == null) {
			return;
		}
		// 父选项未勾选,子选项清空
		if (unselected(childBackgroundInfo.getBottleCup())) {
			childBackgroundInfo.setBottleCupSpecify("");
		}
		if (unselected(childBackgroundInfo.getSpecialRequirementsSleep())) {
			childBackgroundInfo.setSleepSpecify("");
		}
		if (unselected(childBackgroundInfo.getClothingNeeds())) {
			childBackgroundInfo.setClothingSpecify("");
		}
		if (unselected(childBackgroundInfo.getAssistanceDuringFeed())) {
			childBackgroundInfo.setFeedSpecify("");
		}
		if (unselected(childBackgroundInfo.getHaveCulturalReligious())) {
			childBackgroundInfo.setReligiousSpecify("");
		}
		if (unselected(childBackgroundInfo.getUnderTwoYears())) {
			childBackgroundInfo.setSleepTimes("");
			childBackgroundInfo.setMealTimes("");
			childBackgroundInfo.setBottleTimes("");
		}

		String cbId = "";
		if (StringUtil.isEmpty(childBackgroundInfo.getId())) {
			cbId = UUID.randomUUID().toString();
			childBackgroundInfo.setId(cbId);
			childBackgroundInfo.setUserId(userId);
			childBackgroundInfo.setDeleteFlag(DeleteFlag.Default.getValue());
			childBackgroundInfoMapper.insertSelective(childBackgroundInfo);
		} else {
			ChildBackgroundInfo dbChildBackgroundInfo = childBackgroundInfoMapper.selectByPrimaryKey(childBackgroundInfo.getId());
			cbId = dbChildBackgroundInfo.getId();
			childBackgroundInfo.setUserId(dbChildBackgroundInfo.getUserId());
			childBackgroundInfo.setDeleteFlag(dbChildBackgroundInfo.getDeleteFlag());
			childBackgroundInfoMapper.updateByPrimaryKey(childBackgroundInfo);

			// 记录历史记录
			updateLogsDetaile(dbChildBackgroundInfo, childBackgroundInfo, "Background", sourceLogsInfo, "");

		}
		// 父选项未勾选,子选项清空
		List<BackgroundPetsInfo> petList = background.getPetList();
		if (unselected(childBackgroundInfo.getChildHavePets())) {
			backgroundPetsInfoMapper.removeBackgroundPetsInfoByBackgroundId(cbId);
			petList = null;
			background.setPetList(petList);
		}
		if (null != petList && petList.size() != 0) {
			// 重新插入新记录
			int i = 1;
			for (BackgroundPetsInfo pets : petList) {
				String oldId = pets.getId();
				Date now = new Date();
				pets.setId(UUID.randomUUID().toString());
				pets.setBackgroundId(cbId);
				pets.setDeleteFlag(DeleteFlag.Default.getValue());
				now.setTime(now.getTime() + i * 1000);
				pets.setCreateTime(now);
				// 记录历史记录
				BackgroundPetsInfo dbBackgroundPetsInfo = backgroundPetsInfoMapper.selectByPrimaryKey(oldId);
				updateLogsDetaile(dbBackgroundPetsInfo, pets, "BackgroundPetsInfo", sourceLogsInfo, String.valueOf(i));
				i += i;
			}
			// 删除之前BackgroundPetsInfo记录
			backgroundPetsInfoMapper.removeBackgroundPetsInfoByBackgroundId(cbId);

			backgroundPetsInfoMapper.insterBatchBackgroundPetsInfo(petList);
		}

		// 父选项未勾选,子选项清空
		List<BackgroundPersonInfo> personList = background.getPersonList();
		if (unselected(childBackgroundInfo.getSameHousehold())) {
			backgroundPersonInfoMapper.reomveBackgroundPersonInfoByBackgroundId(cbId);
			personList = null;
			background.setPersonList(personList);
		}
		if (null != personList && personList.size() != 0) {

			// 重新插入新记录
			int i = 1;
			for (BackgroundPersonInfo person : personList) {
				String oldId = person.getId();
				Date now = new Date();
				person.setId(UUID.randomUUID().toString());
				person.setBackgroundId(cbId);
				person.setDeleteFlag(DeleteFlag.Default.getValue());
				now.setTime(now.getTime() + i * 1000);
				person.setCreateTime(now);

				// 记录历史记录
				BackgroundPersonInfo dbBackgroundPersonInfo = backgroundPersonInfoMapper.selectByPrimaryKey(oldId);
				updateLogsDetaile(dbBackgroundPersonInfo, person, "sameHousehold", sourceLogsInfo, String.valueOf(i));
				i += i;
			}
			// 删除之前BackgroundPersonInfo记录
			backgroundPersonInfoMapper.reomveBackgroundPersonInfoByBackgroundId(cbId);

			backgroundPersonInfoMapper.insterBatchBackgroundPersonInfo(personList);
		}

	}

	/**
	 * @description 初始化医疗详细信息
	 * @author hxzhang
	 * @create 2016年7月28日下午7:30:21
	 * @version 1.0
	 * @param userId
	 *            小孩ID
	 * @param healthMedicalDetailList
	 *            List<ChildHealthMedicalDetailInfo>
	 */
	private void initOrUpdateChildHealthMedicalDetailInfo(String userId, List<ChildHealthMedicalDetailInfo> healthMedicalDetailList, SourceLogsInfo sourceLogsInfo) {
		if (null == healthMedicalDetailList || healthMedicalDetailList.size() == 0) {
			return;
		}

		// 重新插入新记录
		int i = 1;
		for (ChildHealthMedicalDetailInfo cm : healthMedicalDetailList) {
			String oldId = cm.getId();

			Date now = new Date();
			cm.setId(UUID.randomUUID().toString());
			cm.setUserId(userId);
			cm.setDeleteFlag(DeleteFlag.Default.getValue());
			now.setTime(now.getTime() + i * 1000);
			cm.setCreateTime(now);

			ChildHealthMedicalDetailInfo dbObj = childHealthMedicalDetailInfoMapper.selectByPrimaryKey(oldId);
			// 记录历史记录
			updateLogsDetaile(dbObj, cm, "HealthMedicalDetailInfo", sourceLogsInfo, String.valueOf(cm.getNameIndex()));

			i += i;
		}

		// 删除之前ChildHealthMedicalDetailInfo记录
		childHealthMedicalDetailInfoMapper.removeChildHealthMedicalDetailInfoByUserId(userId);

		childHealthMedicalDetailInfoMapper.insterBatchChildHealthMedicalDetailInfo(healthMedicalDetailList);
	}

	/**
	 * @description 初始化健康医疗信息
	 * @author hxzhang
	 * @create 2016年7月28日下午7:52:16
	 * @version 1.0
	 * @param userId
	 *            小孩ID
	 * @param healthMedical
	 *            ChildHealthMedicalInfo
	 */
	private void initOrUpdateChildHealthMedicalInfo(String userId, ChildHealthMedicalInfo healthMedical, SourceLogsInfo sourceLogsInfo) {
		if (healthMedical == null) {
			return;
		}
		List<FileVo> fileVoList = new ArrayList<FileVo>();
		fileVoList.addAll(healthMedical.getFileList());
		String fileId = "";
		if (StringUtil.isEmpty(healthMedical.getId())) {
			healthMedical.setId(UUID.randomUUID().toString());
			healthMedical.setUserId(userId);
			fileId = UUID.randomUUID().toString();
			healthMedical.setImmunisationFileId(fileId);
			healthMedical.setDeleteFlag(DeleteFlag.Default.getValue());
			childHealthMedicalInfoMapper.insertSelective(healthMedical);
		} else {
			ChildHealthMedicalInfo dbHealthMedical = childHealthMedicalInfoMapper.selectByPrimaryKey(healthMedical.getId());
			fileId = dbHealthMedical.getImmunisationFileId();
			healthMedical.setDeleteFlag(dbHealthMedical.getDeleteFlag());
			healthMedical.setUserId(dbHealthMedical.getUserId());
			childHealthMedicalInfoMapper.updateByPrimaryKey(healthMedical);

			// 记录历史记录
			updateLogsDetaile(dbHealthMedical, healthMedical, "HealthMedical", sourceLogsInfo, "");

			// 删除附件
			delFile(fileVoList, fileId);
		}
		// 上传附件
		try {
			commonService.initAttachmentInfo(fileId, fileVoList, FilePathUtil.FAMILY_PATH);
		} catch (Exception e) {
			logger.error("msg_file_upload_failed||", e);
			throw new RuntimeException();
		}
	}

	private ServiceResult<Object> initOrUpdateAttendanceInfo(UserInfoVo userInfoVo, String userId, ChildInfoVo childInfoVo, UserInfoVo currentUser, String oldRoomId,
			String ip) {
		boolean isCeo = containRole(Role.CEO, userInfoVo.getRoleInfoVoList()) == 1;
		boolean isCentreManager = (containRole(Role.CentreManager, userInfoVo.getRoleInfoVoList()) == 1)
				|| (containRole(Role.EducatorSecondInCharge, userInfoVo.getRoleInfoVoList()) == 1);
		boolean isAdmin = isCeo || isCentreManager;
		if (!isAdmin) {
			return successResult();
		}

		ChildAttendanceInfo childAttendanceInfo = childInfoVo.getAttendance();
		AccountInfo accountInfo = accountInfoMapper.getAccountInfoByUserId(userId);

		if (StringUtil.isEmpty(childAttendanceInfo.getId())) {
			childAttendanceInfo.setId(UUID.randomUUID().toString());
			childAttendanceInfo.setUserId(userId);
			childAttendanceInfo.setDeleteFlag(DeleteFlag.Default.getValue());
			childAttendanceInfo.setCreateAccountId(userInfoVo.getAccountInfo().getId());
			Date now = new Date();
			childAttendanceInfo.setCreateTime(now);
			childAttendanceInfo.setUpdateAccountId(userInfoVo.getAccountInfo().getId());
			childAttendanceInfo.setUpdateTime(now);
			childAttendanceInfo.setRequestStartDate(now);
			childAttendanceInfo.setEnrolled(EnrolledState.Default.getValue());
			childAttendanceInfo.setType(ChildType.Inside.getValue());
			childAttendanceInfo.setEnrolmentDate(childAttendanceInfo.getChangeDate());
			childAttendanceInfo.setPositioningdate(now);
			childAttendanceInfo.setEnrolledFlag(true);
			// 是否立即生效
			if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(childAttendanceInfo.getChangeDate(), new Date()) >= 0) {
				// 刪除之前存在的
				// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				// 如果是新增 那么作废之前的申请
				changeAttendanceRequestInfoMapper.updateState(accountInfo.getId(), ChangeAttendanceRequestState.OnJob.getValue());
				// 删除临时永久和删除的排课记录
				temporaryAttendanceInfoMapper.deleteOld(accountInfo.getId());
				// 刪除移除的
				subtractedAttendanceMapper.deleteOld(accountInfo.getId());
				// 删除所有
				roomChangeInfoMapper.deleteByChild(accountInfo.getId());
				// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				childAttendanceInfo.setEnrolled(EnrolledState.Enrolled.getValue());
				accountInfo.setStatus(ArchivedStatus.UnArchived.getValue());
				accountInfoMapper.updateByPrimaryKey(accountInfo);
				// 此小孩之前已入院 那么记录原本的信息到历史表
				// 伪造为了传递参数
				ChangeAttendanceRequestInfo attendanceRequestInfo = new ChangeAttendanceRequestInfo();
				attendanceRequestInfo.setChildId(accountInfo.getId());
				attendanceRequestInfo.setChangeDate(childAttendanceInfo.getChangeDate());
				attendanceRequestInfo.setNewCentreId(childInfoVo.getUserInfo().getCentersId());
				attendanceRequestInfo.setNewRoomId(childInfoVo.getUserInfo().getRoomId());
				attendanceFactory.dealAttendanceRequest(attendanceRequestInfo, childAttendanceInfo);
				attendanceHistoryService.dealAttendanceHistory(attendanceRequestInfo, currentUser);
			} else {
				UserInfo userInfo = userInfoMapper.selectByPrimaryKey(userId);
				// 生成请求ONJOB
				attendanceCoreService.dealAttendanceInit(currentUser, childAttendanceInfo, accountInfo, userInfo);
			}

			childAttendanceInfoMapper.insertSelective(childAttendanceInfo);
		} else {
			ChildAttendanceInfo oldChildAttendanceInfo = childAttendanceInfoMapper.selectByPrimaryKey(childAttendanceInfo.getId());
			requestedStartDateLog(childAttendanceInfo.getUserId(), oldChildAttendanceInfo.getRequestStartDate(), childAttendanceInfo.getRequestStartDate(), ip,
					currentUser);
			oldChildAttendanceInfo.setMonday(childAttendanceInfo.getMonday());
			oldChildAttendanceInfo.setTuesday(childAttendanceInfo.getTuesday());
			oldChildAttendanceInfo.setWednesday(childAttendanceInfo.getWednesday());
			oldChildAttendanceInfo.setThursday(childAttendanceInfo.getThursday());
			oldChildAttendanceInfo.setFriday(childAttendanceInfo.getFriday());
			oldChildAttendanceInfo.setRequestStartDate(childAttendanceInfo.getRequestStartDate());
			// 如果是外部的那麼不可编辑
			if (oldChildAttendanceInfo.getType() == ChildType.External.getValue()) {
				// 更新园区
				dealOutCenters(childInfoVo);
				childAttendanceInfoMapper.updateByPrimaryKey(oldChildAttendanceInfo);
				return successResult();
			}
			oldChildAttendanceInfo.setUpdateAccountId(userInfoVo.getAccountInfo().getId());
			oldChildAttendanceInfo.setUpdateTime(new Date());
			oldChildAttendanceInfo.setUserId(userId);
			oldChildAttendanceInfo.setDeleteFlag(DeleteFlag.Default.getValue());
			oldChildAttendanceInfo.setType(childAttendanceInfo.getType());
			oldChildAttendanceInfo.setEnrolmentDate(childAttendanceInfo.getEnrolmentDate());
			oldChildAttendanceInfo.setLeaveDate(childAttendanceInfo.getLeaveDate());
			oldChildAttendanceInfo.setMonday(childAttendanceInfo.getMonday());
			oldChildAttendanceInfo.setTuesday(childAttendanceInfo.getTuesday());
			oldChildAttendanceInfo.setWednesday(childAttendanceInfo.getWednesday());
			oldChildAttendanceInfo.setThursday(childAttendanceInfo.getThursday());
			oldChildAttendanceInfo.setFriday(childAttendanceInfo.getFriday());
			oldChildAttendanceInfo.setEnrolmentDate(childAttendanceInfo.getChangeDate());
			oldChildAttendanceInfo.setChangeDate(childAttendanceInfo.getChangeDate());
			oldChildAttendanceInfo.setOrientation(childAttendanceInfo.getOrientation());
			// 直接归档
			// (去除当天直接归档操作,实际操作为下一天执行) hxzhang
			// if (childAttendanceInfo.getLeaveDate() != null
			// &&
			// com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(childAttendanceInfo.getLeaveDate(),
			// new Date()) == 0) {
			// childAttendanceInfoMapper.updateByPrimaryKey(oldChildAttendanceInfo);
			// userFactory.archivedChild(childInfoVo.getUserInfo(), currentUser,
			// childAttendanceInfo.getLeaveDate(),
			// EmailType.leaveCenter.getValue(), true);
			// return successResult();
			// }
			childAttendanceInfoMapper.updateByPrimaryKey(oldChildAttendanceInfo);

			// 如果已经入园 那么就不再进行下面的入院操作
			if (oldChildAttendanceInfo.getEnrolled() == EnrolledState.Enrolled.getValue()) {
				return successResult();
			}

			// 是否立即生效
			if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(childAttendanceInfo.getChangeDate(), new Date()) >= 0) {
				// 刪除之前存在的
				// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				// 如果是新增 那么作废之前的申请
				changeAttendanceRequestInfoMapper.updateState(accountInfo.getId(), ChangeAttendanceRequestState.OnJob.getValue());
				// 删除临时永久和删除的排课记录
				temporaryAttendanceInfoMapper.deleteOld(accountInfo.getId());
				// 刪除移除的
				subtractedAttendanceMapper.deleteOld(accountInfo.getId());
				// 删除所有
				roomChangeInfoMapper.deleteByChild(accountInfo.getId());
				// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
				oldChildAttendanceInfo.setEnrolled(EnrolledState.Enrolled.getValue());
				accountInfo.setStatus(ArchivedStatus.UnArchived.getValue());
				accountInfoMapper.updateByPrimaryKey(accountInfo);
				childAttendanceInfoMapper.updateByPrimaryKey(oldChildAttendanceInfo);

				// 此小孩之前已入院 那么记录原本的信息到历史表
				// 伪造为了传递参数
				ChangeAttendanceRequestInfo attendanceRequestInfo = new ChangeAttendanceRequestInfo();
				attendanceRequestInfo.setChildId(accountInfo.getId());
				attendanceRequestInfo.setChangeDate(childAttendanceInfo.getChangeDate());
				attendanceRequestInfo.setNewCentreId(childInfoVo.getUserInfo().getCentersId());
				attendanceRequestInfo.setNewRoomId(childInfoVo.getUserInfo().getRoomId());
				attendanceFactory.dealAttendanceRequest(attendanceRequestInfo, childAttendanceInfo);
				attendanceHistoryService.dealAttendanceHistory(attendanceRequestInfo, currentUser);
			} else {
				UserInfo userInfo = userInfoMapper.selectByPrimaryKey(userId);
				// 生成请求ONJOB
				attendanceCoreService.dealAttendanceInit(currentUser, childAttendanceInfo, accountInfo, userInfo);
			}

		}
		return successResult();
	}

	private void dealOutCenters(ChildInfoVo vo) {
		List<String> centers = vo.getUserInfo().getOutCenterIds();
		outChildCenterInfoMapper.deleteCenterByUserId(vo.getUserInfo().getId());
		insertOutChildCenterInfo(vo.getUserInfo().getId(), centers);
	}

	private void requestedStartDateLog(String id, Date oldVal, Date newVal, String ip, UserInfoVo currentUser) {
		if (DateUtil.isSameDay(oldVal, newVal)) {
			return;
		}
		SourceLogsInfo sourceLogsInfo = initSourceLogsInfo(ip, currentUser, id);
		LogsDetaileInfo logsDetaileInfo = initLogsDetaileInfo(sourceLogsInfo.getId(), "Requested Start Date", "Requested Start Date",
				DateUtil.format(oldVal, "dd/MM/yyyy"), DateUtil.format(newVal, "dd/MM/yyyy"));
		logsDetaileInfoMapper.insert(logsDetaileInfo);
		sourceLogsInfoMapper.insert(sourceLogsInfo);
	}

	private LogsDetaileInfo initLogsDetaileInfo(String sourceLogsId, String tag, String changeFieldName, String oldVal, String newVal) {
		LogsDetaileInfo logsDetaileInfo = new LogsDetaileInfo();
		logsDetaileInfo.setId(UUID.randomUUID().toString());
		logsDetaileInfo.setChangeFieldName(changeFieldName);
		logsDetaileInfo.setOldValue(oldVal);
		logsDetaileInfo.setNewValue(newVal);
		logsDetaileInfo.setTags(tag);
		logsDetaileInfo.setSourceLogsId(sourceLogsId);

		return logsDetaileInfo;
	}

	/**
	 * @description 初始化或更新AccountInfo信息
	 * @author hxzhang
	 * @create 2016年7月29日下午3:09:46
	 * @version 1.0
	 * @param userInfo
	 *            用户信息
	 * @param currentUser
	 *            当前登录信息
	 */
	private void initOrUpdateAccountInfo(UserInfo userInfo, UserInfoVo currentUser, String familyId, short value, Short childType) {
		AccountInfo dbAccountInfo = accountInfoMapper.selectAccountInfoByUserId(userInfo.getId());
		if (null == dbAccountInfo) {
			String accountId = UUID.randomUUID().toString();
			AccountInfo accountInfo = new AccountInfo();
			accountInfo.setId(accountId);
			accountInfo.setAccount(userInfo.getEmail());
			accountInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
			Date now = new Date();
			accountInfo.setCreateTime(now);
			accountInfo.setDeleteFlag(DeleteFlag.Default.getValue());
			accountInfo.setStatus(ArchivedStatus.Default.getValue());
			if (childType == ChildType.Inside.getValue()) {
				accountInfo.setStatus(ArchivedStatus.UnArchived.getValue());
			}
			accountInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
			accountInfo.setUpdateTime(now);
			accountInfo.setUserId(userInfo.getId());
			accountInfoMapper.insertSelective(accountInfo);
			// 新增AccountRoleInfo
			AccountRoleInfo accountRoleInfo = new AccountRoleInfo();
			accountRoleInfo.setAccountId(accountId);
			accountRoleInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
			accountRoleInfo.setCreateTime(now);
			accountRoleInfo.setDeleteFlag(DeleteFlag.Default.getValue());
			accountRoleInfo.setId(UUID.randomUUID().toString());
			accountRoleInfo.setRoleId(roleInfoMapper.selectRoleIdByValue(value));
			accountRoleInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
			accountRoleInfo.setUpdateTime(now);
			accountRoleInfoMapper.insertSelective(accountRoleInfo);
		} else {
			dbAccountInfo.setAccount(userInfo.getEmail());
			dbAccountInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
			dbAccountInfo.setUpdateTime(new Date());
			accountInfoMapper.updateByPrimaryKey(dbAccountInfo);
		}
	}

	/**
	 * @description 初始化FamilyInfo
	 * @author hxzhang
	 * @create 2016年7月29日下午4:11:09
	 * @version 1.0
	 * @param familyId
	 *            familyId
	 */
	private void initOrUpdateFamilyInfo(String familyId, String familyName) {
		FamilyInfo familyInfo = familyInfoMapper.selectByPrimaryKey(familyId);
		if (null == familyInfo) {
			familyInfo = new FamilyInfo();
			familyInfo.setId(familyId);
			Date now = new Date();
			familyInfo.setCreateTime(now);
			familyInfo.setUpdateTime(now);
			familyInfo.setFamilyName(familyName);
			familyInfo.setDeleteFlag(DeleteFlag.Default.getValue());
			familyInfoMapper.insertSelective(familyInfo);
		} else {
			familyInfo.setUpdateTime(new Date());
			familyInfo.setFamilyName(familyName);
			familyInfoMapper.updateByPrimaryKey(familyInfo);
		}
	}

	private void initOrUpdateFamilyOut(String familyId, String familyName) {
		FamilyInfo familyInfo = familyInfoMapper.selectByPrimaryKey(familyId);
		if (null != familyInfo) {
			return;
		}
		familyInfo = new FamilyInfo();
		familyInfo.setId(familyId);
		Date now = new Date();
		familyInfo.setCreateTime(now);
		familyInfo.setUpdateTime(now);
		familyInfo.setFamilyName(familyName);
		familyInfo.setDeleteFlag(DeleteFlag.Default.getValue());
		familyInfoMapper.insertSelective(familyInfo);
	}

	@Override
	public ServiceResult<Object> addOrUpdateParent(UserInfo userInfo, String familyName, UserInfoVo currentUser, String ip) {
		// 验证
		ServiceResult<Object> result = validateAddOrUpdateParent(userInfo, familyName, currentUser);
		if (!result.isSuccess()) {
			return result;
		}
		String familyId = userInfo.getFamilyId();
		// boolean havaMale = judgeMale(familyId);
		if (StringUtil.isEmpty(userInfo.getId())) {
			// 新增
			// 初始化userInfo
			userInfo = initOrUpdateUserInfo(userInfo, currentUser, familyId, UserType.Parent.getValue(), null);
			familyId = userInfo.getFamilyId();
			// 初始化AccountInfo
			initOrUpdateAccountInfo(userInfo, currentUser, familyId, Role.Parent.getValue(), ChildType.Inside.getValue());
			// 初始化FamilyInfo信息
			initOrUpdateFamilyInfo(familyId, familyName);
			// 建立家庭关系
			buildRelationKidParentInfo(userInfo);
			// 获取该家庭已入园的小孩
			List<UserInfo> childList = userInfoMapper.getEnrolledChildByFamilyId(familyId);
			// 发送邮件
			if (null != childList && childList.size() != 0) {
				List<UserInfo> userList = new ArrayList<UserInfo>();
				userList.add(userInfo);
				saveBatchWelEmail(userList, SendEmailType.haveCancel.getValue(), false, null);
			}

		} else {
			SourceLogsInfo sourceLogsInfo = initSourceLogsInfo(ip, currentUser, userInfo.getId());
			userInfo = initOrUpdateUserInfo(userInfo, currentUser, familyId, (short) 1, sourceLogsInfo);
			familyId = userInfo.getFamilyId();
			// 初始化AccountInfo
			initOrUpdateAccountInfo(userInfo, currentUser, familyId, Role.Parent.getValue(), ChildType.Inside.getValue());
			// 初始化FamilyInfo信息
			initOrUpdateFamilyInfo(familyId, familyName);

			newsAccountsInfoMapper.updateUserNameByAccountId(accountInfoMapper.getAccountInfoByUserId(userInfo.getId()).getId(), getFullName(userInfo));
		}

		return successResult(getTipMsg("user.submit.success"), (Object) userInfo);
	}

	@Override
	public ServiceResult<Object> getFamilyInfo(String familyId) {
		FamilyInfoVo familyInfoVo = new FamilyInfoVo();
		List<ChildInfoVo> childList = new ArrayList<ChildInfoVo>();

		// 获取家长的信息
		List<UserInfo> parentList = userInfoMapper.getParentInfoByFamilyId(familyId);

		// 获取小孩子的信息
		List<UserInfo> childInfos = userInfoMapper.getChildInfoByFamilyId(familyId);
		if (ListUtil.isEmpty(childInfos)) {
			familyId = userInfoMapper.selectByPrimaryKey(applicationListMapper.selectByPrimaryKey(familyId).getUserId()).getFamilyId();
			childInfos = userInfoMapper.getChilds(familyId);
			parentList = userInfoMapper.getParents(familyId);
		}
		for (UserInfo user : childInfos) {
			ChildInfoVo childInfoVo = new ChildInfoVo();
			// 获取账户信息
			AccountInfo accountInfo = accountInfoMapper.getAccountInfoByUserId(user.getId());
			// 获取监护安排
			ChildCustodyArrangeInfo childCustodyArrangeInfo = custodyArrangeMapper.getChildCustodyArrangeInfoByUserId(user.getId());
			// 获取紧急联系人
			List<EmergencyContactInfo> emergencyContactList = emergencyContactMapper.selectByUserId(user.getId());
			// 获取健康和医疗信息
			ChildHealthMedicalInfo healthMedical = childHealthMedicalInfoMapper.getChildHealthMedicalInfoByUserId(user.getId());
			// 获取健康和医疗详细信息
			List<ChildHealthMedicalDetailInfo> healthMedicalDetailList = childHealthMedicalDetailInfoMapper.getChildHealthMedicalDetailInfoByUserId(user.getId());
			// childAttendanceInfo
			ChildAttendanceInfo childAttendanceInfo = childAttendanceInfoMapper.selectByUserId(user.getId());

			// 获取背景信息
			ChildBackgroundInfoVo background = new ChildBackgroundInfoVo();
			ChildBackgroundInfo backgroundInfo = childBackgroundInfoMapper.getChildBackgroundInfoByUserId(user.getId());
			List<BackgroundPersonInfo> bpnList = null;
			List<BackgroundPetsInfo> bpsList = null;
			if (backgroundInfo != null) {
				bpnList = backgroundPersonInfoMapper.getBackgroundPersonInfoByBackgroundId(backgroundInfo.getId());
				bpsList = backgroundPetsInfoMapper.getBackgroundPetsInfoByBackgroundId(backgroundInfo.getId());
			}
			background.setChildBackgroundInfo(backgroundInfo);
			background.setPersonList(bpnList);
			background.setPetList(bpsList);
			// 获取饮食要求
			ChildDietaryRequireInfo dietaryRequire = dietaryRequireMapper.getChildDietaryRequireByUserId(user.getId());
			// 获取药物
			ChildMedicalInfo medical = medicalMapper.getChildMedicalByUserId(user.getId());
			// 获取紧急联系人签名
			EmergencySignatureInfo es = emergencySignatureInfoMapper.getInfoByUserId(user.getId());

			// 如果是外部小孩,动态获取centerId
			if (accountInfo.getStatus() == -1 && StringUtil.isEmpty(user.getCentersId())) {
				String centers = outChildCenterInfoMapper.getCenterIdsByUserId(user.getId());
				user.setOutCenterIds(StringUtil.isEmpty(centers) ? new ArrayList<String>() : Arrays.asList(centers.split(",")));
			}

			childInfoVo.setUserInfo(user);
			childInfoVo.setAccountInfo(accountInfo);
			childInfoVo.setBackground(background);
			childInfoVo.setCustodyArrange(childCustodyArrangeInfo);
			childInfoVo.setDietaryRequire(dietaryRequire);
			childInfoVo.setEmergencyContactList(emergencyContactList);
			childInfoVo.setHealthMedical(healthMedical);
			childInfoVo.setHealthMedicalDetailList(healthMedicalDetailList);
			childInfoVo.setMedical(medical);
			childInfoVo.setAttendance(childAttendanceInfo);
			childInfoVo.setEmergencySignatureInfo(null == es ? new EmergencySignatureInfo() : es);

			// 拼接歷史消息
			List<RequestLogVo> requestLogVos = requestLogInfoMapper.selectLogVo(accountInfo.getId());
			childInfoVo.setRequestLogVos(requestLogVos);

			// 上课历史
			HistoryVo historyVo = attendanceHistoryFactory.getHistoryVo(childInfoVo.getUserInfo(), accountInfo.getId(), childAttendanceInfo.getLeaveDate());
			childInfoVo.setAttendanceHistoryInfos(historyVo);

			// 获取孩子邮件信息
			List<EmailMsg> emailMsgList = emailMsgMapper.getEmailMsgListByUserId(user.getId());
			childInfoVo.setEmailMsgList(emailMsgList);

			// 给家长在notes/logs展示相关的task

			// 获取Previous Enrolment
			List<PreviousEnrolment> enrolments = dealTimeline(user.getId());
			childInfoVo.setEnrolments(enrolments);

			childList.add(childInfoVo);
		}

		FamilyInfo family = familyInfoMapper.selectByPrimaryKey(familyId);

		familyInfoVo.setChildList(childList);
		familyInfoVo.setParentList(parentList);
		familyInfoVo.setFamilyName(family.getFamilyName());
		return successResult((Object) familyInfoVo);
	}

	private List<PreviousEnrolment> dealTimeline(String userId) {
		List<PreviousEnrolment> list = previousEnrolmentMapper.getListByChild(userId);
		for (PreviousEnrolment p : list) {
			List<String> objIds = previousEnrolmentMapper.getObjIds(p.getId());
			// 获取相关Request
			List<RequestLogVo> requestLogVos = requestLogInfoMapper.getListByIds(objIds);
			// 获取上课历史
			List<AttendanceHistoryGroupVo> historyGroupVos = attendanceHistoryInfoMapper.getCentreGroupByIds(objIds);
			// 获取孩子邮件信息
			List<EmailMsg> emailMsgList = emailMsgMapper.getListByIds(objIds);

			p.setRequestLogVos(requestLogVos);
			p.setHistoryVo(new HistoryVo(historyGroupVos, null));
			p.setEmailMsgList(emailMsgList);
		}
		return list;
	}

	@Override
	public ServiceResult<Pager<FileListVo, FamilyCondition>> getPageList(UserInfoVo currentUser, FamilyCondition condition) {
		// 在设置角色权限之前处理centre search
		dealSelectCentre(condition, currentUser);
		// 设置查看权限
		// ceo,园长,二级园长可以查看所有
		if (!(isRole(currentUser, Role.CEO.getValue()) || isRole(currentUser, Role.CentreManager.getValue())
				|| isRole(currentUser, Role.EducatorSecondInCharge.getValue()))) {
			// Educator,EducatorECT,EducatorCertifiedSupersor,EducatorFirstAidOfficer,EducatorNominatedSupervisor,Cook只能查看本园的
			if (isRole(currentUser, Role.Educator.getValue()) || isRole(currentUser, Role.EducatorECT.getValue())
					|| isRole(currentUser, Role.EducatorCertifiedSupersor.getValue()) || isRole(currentUser, Role.EducatorFirstAidOfficer.getValue())
					|| isRole(currentUser, Role.EducatorNominatedSupervisor.getValue()) || isRole(currentUser, Role.Cook.getValue())) {
				condition.setCenterId(currentUser.getUserInfo().getCentersId());
			}
			// 家长只能查看自己家的
			if (isRole(currentUser, Role.Parent.getValue())) {
				condition.setFamilyId(currentUser.getUserInfo().getFamilyId());
			}
			// 兼职没有查看权限
			if (isRole(currentUser, Role.Casual.getValue()) && StringUtil.isEmpty(currentUser.getUserInfo().getCentersId())) {
				return successResult(new Pager<FileListVo, FamilyCondition>(null, condition));
			}
			if (isRole(currentUser, Role.Casual.getValue()) && StringUtil.isNotEmpty(currentUser.getUserInfo().getCentersId())) {
				condition.setCenterId(currentUser.getUserInfo().getCentersId());
			}
		}
		condition.setStartSize(condition.getPageIndex());
		String keyWord = condition.getKeyWords();
		condition.setKeyWords(SqlUtil.likeEscape(keyWord));
		List<FileListVo> list = familyInfoMapper.getPagerList(condition);
		int totalSize = familyInfoMapper.getPagerListCount(condition);
		condition.setTotalSize(totalSize);
		Pager<FileListVo, FamilyCondition> pager = new Pager<FileListVo, FamilyCondition>(list, condition);
		condition.setKeyWords(keyWord);
		return successResult(pager);
	}

	private void dealSelectCentre(FamilyCondition condition, UserInfoVo currentUser) {
		if (getSystemMsg("edensorPark").equals(condition.getCenterId())) {
			condition.setCenterId(null);
			condition.setCentreIds(dealEdensorPark());
		} else {
			condition.setCentreIds(null);
		}
	}

	private List<String> dealEdensorPark() {
		List<String> edensorParkIds = new ArrayList<String>();
		List<CentersInfo> list = centersInfoMapper.getAllCentersInfos();
		for (CentersInfo c : list) {
			if (getSystemMsg("rydeId").equals(c.getId())) {
				continue;
			}
			edensorParkIds.add(c.getId());
		}
		return edensorParkIds;
	}

	private List<String> dealEdensorPark(UserInfoVo currentUser) {
		List<String> edensorParkIds = new ArrayList<String>();
		StaffRoleInfoVo vo = commonService.getMenuOrgs((short) 0, currentUser);
		for (CentersInfo c : vo.getCentersInfos()) {
			if (c.isRyde()) {
				continue;
			}
			edensorParkIds.add(c.getId());
		}
		return edensorParkIds;
	}

	@Override
	public List<SelecterPo> selectParentChilds(String p) {
		List<SelecterPo> list = new ArrayList<SelecterPo>();
		if (StringUtil.isEmpty(p)) {
			return list;
		}
		return familyInfoMapper.selectParentChilds(p);
	}

	/**
	 * @description 删除附件
	 * @author hxzhang
	 * @create 2016年7月20日下午2:44:56
	 * @version 1.0
	 * @param fileVoList
	 *            附件集合
	 * @param sourceId
	 *            附件源ID
	 */
	private void delFile(List<FileVo> fileVoList, String sourceId) {
		// 保留不删除的附件
		List<FileVo> unDelList = new ArrayList<FileVo>();
		if (null != fileVoList && fileVoList.size() != 0) {
			for (int i = fileVoList.size() - 1; i >= 0; i--) {
				if (StringUtil.isNotEmpty(fileVoList.get(i).getRelativePathName()) && fileVoList.get(i).getRelativePathName().indexOf(FilePathUtil.FAMILY_PATH) != -1) {
					unDelList.add(fileVoList.get(i));
					fileVoList.remove(i);
				}
			}
		}
		attachmentInfoMapper.deleteBatchAttachmentInfo(unDelList, sourceId);
	}

	@Override
	public ServiceResult<Object> removeChild(String userId, boolean sureDelete, UserInfoVo currentUser) {
		// 获取该小孩所在的family有几个小孩
		int childNum = userInfoMapper.getUserNumByUserId(userId, UserType.Child.getValue());
		// 获取该小孩虽在的family有几个家长
		int parentNum = userInfoMapper.getUserNumByUserId(userId, UserType.Parent.getValue());
		// 判断该小孩所在的family中存在家长,并且只剩下一个小孩时不能被删除
		if (childNum <= 1 && parentNum != 0) {
			return failResult(getTipMsg("child.delete.failed"));
		}
		// 判断是否有未来安排 begin
		ChildAttendanceInfo childAttendanceInfo = childAttendanceInfoMapper.selectByUserId(userId);
		if (!sureDelete && childAttendanceInfo.getEnrolled() == EnrolledState.Enrolled.getValue()) {
			return failResult(2, getTipMsg("user.user.delete.confirm"));
		}
		// 判断是否有未来安排 end

		// 删除该小孩与家长的关系
		AccountInfo accountInfo = accountInfoMapper.getAccountInfoByUserId(userId);
		// 删除该小孩[改为调用存储过程]
		userInfoMapper.deleteChild(accountInfo.getId());
		// relationKidParentInfoMapper.updateRelationBykidAccountId(accountInfo.getId());
		// 更新家庭信息
		updateFamilyInfo(userId);
		userInfoMapper.updateTimeByUserId(userId, new Date());
		return successResult(getTipMsg("operation_success"));

	}

	@Override
	public ServiceResult<Object> removeParent(String userId, UserInfoVo currentUser) {
		// 删除家长
		userInfoMapper.deleteParent(userId);
		// 删除账户信息
		accountInfoMapper.removeAccountInfoByUserId(userId);
		// 更新家庭信息
		updateFamilyInfo(userId);
		// 更新更新时间
		userInfoMapper.updateTimeByUserId(userId, new Date());
		return successResult(getTipMsg("operation_success"));

	}

	// is hand 是否手动
	@Override
	public ServiceResult<Object> saveBatchWelEmail(List<UserInfo> userList, short sendEmailType, boolean isPlan, UserInfo child) {
		String familyId = userList.get(0).getFamilyId();
		// 获取该用户的账户信息
		List<EmailVo> emailVoList = new ArrayList<EmailVo>();
		for (UserInfo user : userList) {
			AccountInfo accountInfo = accountInfoMapper.selectAccountInfoByUserId(user.getId());
			ChangePswRecordInfo record = new ChangePswRecordInfo();
			record.setId(UUID.randomUUID().toString());
			Date now = new Date();
			record.setCreateTime(now);
			record.setDeleteFlag(DeleteFlag.Default.getValue());
			record.setStatus(TokenStatus.Unused.getValue());
			String token = UUID.randomUUID().toString();
			record.setToken(token);
			record.setAccountId(accountInfo.getId());
			// 先删除过时邮件记录
			changePswRecordInfoMapper.deleteByAccount(accountInfo.getId());
			changePswRecordInfoMapper.insert(record);
			String sendAddress = getSystemMsg("mailHostAccount");
			String sendName = getSystemMsg("email_name");
			String sub = "";
			String msg = "";
			switch (sendEmailType) {
			case 0:
				sub = getSystemEmailMsg("welcome_to_kindiView_title");
				msg = MessageFormat.format(getSystemEmailMsg("welcome_to_kindiView_content"), userFactory.getUserName(user), getSystemMsg("rootUrl"), token);
				break;
			case 1:
			case 3:
				sub = getSystemEmailMsg("welcome_to_kindiView_title");
				msg = MessageFormat.format(getSystemEmailMsg("welcome_to_kindiView_content_haveCancel"), userFactory.getUserName(user), getSystemMsg("rootUrl"), token,
						user.getId());
				break;
			case 2:
				sub = getSystemEmailMsg("reminder_parent_title");
				msg = MessageFormat.format(getSystemEmailMsg("reminder_parent_content"), userFactory.getUserName(user), getSystemMsg("rootUrl"), token, user.getId());
				break;

			default:
				break;
			}
			String html = EmailUtil.replaceHtml(sub, msg);
			System.err.println(html);
			if (sendEmailType == SendEmailType.timedTask.getValue() || sendEmailType == SendEmailType.welTimedTask.getValue()) {
				MsgEmailInfo msgEmailInfo = MsgEmailInfo.getEmailMsgInstance(sendAddress, sendName, user.getEmail(), userFactory.getUserName(user), sub, null, null, null,
						html);
				msgEmailInfoMapper.insert(msgEmailInfo);
			} else {
				EmailVo emailVo = initEmailVo(sendName, user, sub, html);
				emailVoList.add(emailVo);
			}

		}
		if (sendEmailType != SendEmailType.timedTask.getValue() && sendEmailType != SendEmailType.welTimedTask.getValue() && !isPlan) {
			msgEmailInfoService.batchSendEamil(emailVoList);
		}
		if (isPlan) {
			userFactory.dealPlanEmail(familyId, emailVoList, EmailPlanType.A.getValue(), child.getId());
		}

		return successResult(getTipMsg("send_email_success"));
	}

	/**
	 * @description 初始化EmailVo
	 * @author hxzhang
	 * @create 2016年9月2日下午5:51:58
	 * @version 1.0
	 * @param user
	 * @param sub
	 * @param msg
	 * @return
	 */
	private EmailVo initEmailVo(String sendName, UserInfo user, String sub, String msg) {
		EmailVo emailVo = new EmailVo();
		emailVo.setSendAddress(getSystemMsg("mailHostAccount"));
		emailVo.setSendName(sendName);
		emailVo.setEmail_key(getSystemMsg("email_key"));
		emailVo.setReceiverAddress(user.getEmail());
		emailVo.setReceiverName(userFactory.getUserName(user));
		emailVo.setSub(sub);
		emailVo.setMsg(msg);
		emailVo.setHtml(true);
		emailVo.setReplyTos(null);
		emailVo.setAttachments(null);
		return emailVo;
	}

	@Override
	public ServiceResult<Object> updateArchiveChild(String childId, boolean falg, UserInfoVo currentUser, Date lastDate) {
		int line;
		if (falg) {
			line = accountInfoMapper.updateStatusByUserId(childId, AccountActiveStatus.Disabled.getValue());
			/*
			 * // 取消小孩相关的申请(request)
			 * attendanceCoreService.cancelRelatedRequest(childId);
			 */
			// 调用归档方法做相关操作
			UserInfo userInfo = userInfoMapper.selectByPrimaryKey(childId);

			// 更新last date
			childAttendanceInfoMapper.updateLeaveDateById(lastDate, childId);
			userFactory.archivedChild(userInfo, currentUser, new Date(), EmailType.archiveChild.getValue(), true);
		} else {
			line = accountInfoMapper.updateStatusByUserId(childId, AccountActiveStatus.Enabled.getValue());
			delCentreInfoByUnArchive(childId);
			// 清空离园时间
			childAttendanceInfoMapper.removeLeaveDateByUserId(childId);
			AccountInfo accountInfo = accountInfoMapper.selectAccountInfoByUserId(childId);
			// 还原请假信息
			requestLogInfoMapper.dealUnArchived(accountInfo.getId());
		}
		// 更新家庭信息
		updateFamilyInfo(childId);
		// 更新更新时间
		userInfoMapper.updateTimeByUserId(childId, new Date());
		if (line != 0) {
			if (falg) {
				return successResult(getTipMsg("archived_success"));
			} else {
				return successResult(getTipMsg("unArchived_success"));
			}

		}
		return failResult(getTipMsg("operation_failed"));
	}

	/**
	 * @description 建立孩子与父母的关系
	 * @author hxzhang
	 * @create 2016年8月3日下午5:34:43
	 * @version 1.0
	 * @param userInfo
	 *            用户对象
	 */
	private void buildRelationKidParentInfo(UserInfo userInfo) {
		AccountInfo accountInfo = accountInfoMapper.selectAccountInfoByUserId(userInfo.getId());
		RelationKidParentInfo rkp = new RelationKidParentInfo();
		if (userInfo.getUserType() == UserType.Child.getValue()) {
			// 获取该小孩所在家庭的家长
			List<AccountInfo> accounts = accountInfoMapper.getAccountByFamilyId(userInfo.getFamilyId(), UserType.Parent.getValue());
			if (null != accounts && accounts.size() != 0) {
				for (AccountInfo uv : accounts) {
					rkp.setParentAccountId(uv.getId());
					rkp.setId(UUID.randomUUID().toString());
					rkp.setKidAccountId(accountInfo.getId());
					rkp.setDeleteFlag(DeleteFlag.Default.getValue());
					relationKidParentInfoMapper.insertSelective(rkp);
				}
			}
		}
		if (userInfo.getUserType() == UserType.Parent.getValue()) {
			// 获取该家长所在家庭的小孩
			List<AccountInfo> accounts = accountInfoMapper.getAccountByFamilyId(userInfo.getFamilyId(), UserType.Child.getValue());
			if (null != accounts && accounts.size() != 0) {
				for (AccountInfo uv : accounts) {
					rkp.setKidAccountId(uv.getId());
					rkp.setId(UUID.randomUUID().toString());
					rkp.setParentAccountId(accountInfo.getId());
					rkp.setDeleteFlag(DeleteFlag.Default.getValue());
					relationKidParentInfoMapper.insertSelective(rkp);
				}
			}
		}
	}

	/**
	 * @description 判断当前登录人角色值
	 * @author hxzhang
	 * @create 2016年7月20日下午7:36:08
	 * @version 1.0
	 * @param currentUser
	 *            当前登录人
	 * @param role
	 *            角色值
	 * @return 返回结果
	 */
	private boolean isRole(UserInfoVo currentUser, Short role) {
		List<RoleInfoVo> roleList = currentUser.getRoleInfoVoList();
		for (RoleInfoVo roleInfoVo : roleList) {
			if (role.compareTo(roleInfoVo.getValue()) == 0) {
				return true;
			}
		}
		return false;
	}

	@Override
	public StaffRoleInfoVo getFamilyOrgs() {
		StaffRoleInfoVo staffRoleInfoVo = new StaffRoleInfoVo();
		List<CentersInfo> centersInfos = centersInfoMapper.getCentersInfo(null, null);
		List<RoomInfo> roomInfos = roomInfoMapper.getRoomInfo();
		List<RoomGroupInfo> roomGroupInfos = roomGroupInfoMapper.getRoomGroupInfo();
		staffRoleInfoVo.setCentersInfos(centersInfos);
		staffRoleInfoVo.setGroups(roomGroupInfos);
		staffRoleInfoVo.setRoomInfos(roomInfos);
		return staffRoleInfoVo;
	}

	/**
	 * @description 信息验证
	 * @author hxzhang
	 * @create 2016年8月11日上午10:42:43
	 * @version 1.0
	 * @param childInfoVo
	 *            ChildInfoVo
	 * @param currentUser
	 *            当前登录用户
	 * @return ServiceResult<Object>
	 */
	private ServiceResult<Object> validateAddOrUpdateChild(ChildInfoVo childInfoVo, UserInfoVo currentUser, String familyName) {
		UserInfo child = childInfoVo.getUserInfo();
		AccountInfo account = childInfoVo.getAccountInfo();
		// 1.小孩园区的非空验证
		if (StringUtil.isEmpty(child.getCentersId()) && account.getStatus() != null && account.getStatus() != -1) {
			return failResult(getTipMsg("select_center"));
		}

		if (ListUtil.isEmpty(childInfoVo.getUserInfo().getOutCenterIds()) && childInfoVo.getAttendance().getType() == ChildType.External.getValue()) {
			return failResult(getTipMsg("select_center"));
		}

		Date date = childInfoVo.getAttendance().getEnrolmentDate();
		if (childInfoVo.getAttendance().getChangeDate() != null) {
			date = childInfoVo.getAttendance().getChangeDate();
		}
		if (childInfoVo.getAttendance().getType() != ChildType.External.getValue() && date != null) {
			// edit by hxzhang 20180601
			// if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(new
			// Date(), date) <= 0) {
			// 2.小孩room非空验证
			if (StringUtil.isEmpty(child.getRoomId())) {
				return failResult(getTipMsg("select_room"));
			}

			// 3.小孩分组非空验证
			if (StringUtil.isEmpty(child.getGroupId())) {
				return failResult(getTipMsg("select_group"));
			}
			// }
		}

		// 4.性别不能为空
		if (null == child.getGender()) {
			return failResult(getTipMsg("child_gender"));
		}
		if (null == child.getBirthday()) {
			return failResult(getTipMsg("child_birthday_require"));
		}
		// 5.园长只能新增,编辑本园区的小孩
		// 判断小孩是否是外部小孩,外部小孩将centerId串切开
		if (childInfoVo.getAttendance().getType() == ChildType.External.getValue()) {
			List<String> centers = childInfoVo.getUserInfo().getOutCenterIds();
			if (isRole(currentUser, Role.CentreManager.getValue()) || isRole(currentUser, Role.EducatorSecondInCharge.getValue())) {

			}
		} else {
			if (isRole(currentUser, Role.CentreManager.getValue()) || isRole(currentUser, Role.EducatorSecondInCharge.getValue())) {
				if (!currentUser.getUserInfo().getCentersId().equals(childInfoVo.getUserInfo().getCentersId())) {
					return failResult(getTipMsg("no.permissions"));
				}
			}
		}

		// 6.家长不能新增小孩和编辑小孩的园区信息
		if (isRole(currentUser, Role.Parent.getValue())) {
			if (StringUtil.isEmpty(childInfoVo.getUserInfo().getId())) {
				return failResult(getTipMsg("no.permissions"));
			}
			String centreId = childInfoVo.getUserInfo().getCentersId();
			String roomId = childInfoVo.getUserInfo().getRoomId();
			String groupId = childInfoVo.getUserInfo().getGroupId();
			UserInfo dbChild = userInfoMapper.selectByPrimaryKey(childInfoVo.getUserInfo().getId());
			// centre
			if (!centreId.equals(dbChild.getCentersId())) {
				return failResult(getTipMsg("no.permissions"));
			}
			// room
			if (StringUtil.isNotEmpty(roomId) && StringUtil.isNotEmpty(dbChild.getRoomId()) && !roomId.equals(dbChild.getRoomId())) {
				return failResult(getTipMsg("no.permissions"));
			}
			// group
			if (StringUtil.isNotEmpty(groupId) && StringUtil.isNotEmpty(dbChild.getGroupId()) && !groupId.equals(dbChild.getGroupId())) {
				return failResult(getTipMsg("no.permissions"));
			}
		}
		// 7.家庭名字非空验证
		if (StringUtil.isEmpty(familyName)) {
			return failResult(getTipMsg("family_name"));
		}

		// 8.FirstName非空验证
		if (StringUtil.isEmpty(childInfoVo.getUserInfo().getFirstName())) {
			return failResult(getTipMsg("child_firstName"));
		}
		// 9.Surname非空验证
		if (StringUtil.isEmpty(childInfoVo.getUserInfo().getLastName())) {
			return failResult(getTipMsg("child_surname"));
		}
		// Enrolment ID
		if (!validateEnrolmentID(childInfoVo.getUserInfo().getId(), childInfoVo.getUserInfo().getCrn())) {
			return failResult(getTipMsg("child.enrolmentID.repeat"));
		}
		// 10.紧急联系人Name非空验证
		List<EmergencyContactInfo> emergencyContactList = childInfoVo.getEmergencyContactList();
		if (null != emergencyContactList && emergencyContactList.size() != 0) {
			for (EmergencyContactInfo eci : emergencyContactList) {
				if (StringUtil.isEmpty(eci.getName())) {
					return failResult(getTipMsg("emergency_contact_name"));
				}
			}
		}
		// 11.DietaryRequire.TermsConditions非空验证
		if (null == childInfoVo.getDietaryRequire().getTermsConditions()) {
			return failResult(getTipMsg("dietaryRequire_termsConditions"));
		}
		// 12.Medical.TermsConditions非空验证
		if (null == childInfoVo.getMedical().getTermsConditions()) {
			return failResult(getTipMsg("medical_termsConditions"));
		}

		// Attendance 约束
		if (childInfoVo.getAttendance().getType() != null && childInfoVo.getAttendance().getType() == ChildType.Inside.getValue()
				&& (childInfoVo.getAttendance().getChangeDate() == null)) {
			return failResult(getTipMsg("child.attendance.save.date.empty"));
		}

		// 如果小孩为入院 那么如果填写的date of change时间是以前的 那么 room 要求必填,而且不是外部小孩
		if ((childInfoVo.getAttendance().getType() != ChildType.External.getValue())
				&& (childInfoVo.getAttendance().getEnrolled() == null || childInfoVo.getAttendance().getEnrolled() == EnrolledState.Default.getValue())) {
			if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(childInfoVo.getAttendance().getChangeDate(), new Date()) >= 0) {
				if (StringUtil.isEmpty(childInfoVo.getUserInfo().getRoomId())) {
					return failResult(getTipMsg("child.attendance.save.room.empty"));
				}
			}
		}

		// 如果离园时间<=当前时间 则提示
		if (childInfoVo.getAttendance().getLeaveDate() != null
				&& com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(childInfoVo.getAttendance().getLeaveDate(), new Date()) > 0) {
			if (account.getStatus() != ArchivedStatus.Archived.getValue()) {
				return failResult(-1, getTipMsg("child.attendance.save.leaveDate.before"));
			}
		}

		// 是否满园
		boolean needCheckRoomSize = StringUtil.isEmpty(childInfoVo.getAttendance().getId());
		if (!needCheckRoomSize) {
			ChildAttendanceInfo oldChildAttendanceInfo = childAttendanceInfoMapper.selectByPrimaryKey(childInfoVo.getAttendance().getId());
			if (oldChildAttendanceInfo.getEnrolled() == EnrolledState.Default.getValue()) {
				needCheckRoomSize = true;
			}
		}
		ServiceResult<Object> result = validateWeekDay(childInfoVo.getAttendance(), currentUser);
		if (!result.isSuccess()) {
			return result;
		}

		if (needCheckRoomSize && !BooleanUtils.isTrue(childInfoVo.getOverFull())) {
			// 看看是否满园
			if (StringUtil.isNotEmpty(childInfoVo.getUserInfo().getCentersId()) && StringUtil.isNotEmpty(childInfoVo.getUserInfo().getRoomId())) {
				String userIdTemp = StringUtil.isEmpty(childInfoVo.getAttendance().getId()) ? null : child.getId();
				if (StringUtil.isNotEmpty(userIdTemp) && StringUtil.isEmpty(child.getRoomId())) {
					userIdTemp = null;
				}
				List<Short> dayOfWeeks = attendanceFactory.getChangeDayOfAttendance(userIdTemp, childInfoVo.getAttendance());
				StringBuilder sb = new StringBuilder();
				for (Short tempDayOfWeeks : dayOfWeeks) {
					boolean isFull = attendanceFactory.futureRoomFull(childInfoVo.getUserInfo().getCentersId(), childInfoVo.getUserInfo().getRoomId(),
							childInfoVo.getAttendance().getChangeDate(), (short) tempDayOfWeeks);
					if (isFull) {
						sb.append(TipMsgConstants.WEEKNAMES[tempDayOfWeeks - 1] + ",");
					}
				}
				if (StringUtil.isNotEmpty(sb.toString())) {
					return failResult(11, MessageFormat.format(getTipMsg("child.attendance.approveAdd.over"), StringUtil.trimEnd(sb.toString(), ",")));
				}
			}
		}
		return successResult();
	}

	private boolean validateEnrolmentID(String id, String enrolmentID) {
		if (StringUtil.isEmpty(enrolmentID)) {
			return true;
		}
		int count = userInfoMapper.getCountByIdCrn(id, enrolmentID);
		return count > 0 ? false : true;
	}

	private ServiceResult<Object> validateWeekDay(ChildAttendanceInfo childAttend, UserInfoVo currentUser) {
		int count = 0;
		if (isChoose(childAttend.getMonday())) {
			count++;
		}
		if (isChoose(childAttend.getTuesday())) {
			count++;
		}
		if (isChoose(childAttend.getWednesday())) {
			count++;
		}
		if (isChoose(childAttend.getThursday())) {
			count++;
		}
		if (isChoose(childAttend.getFriday())) {
			count++;
		}
		return count < 1 ? failResult(getTipMsg("family_week_choose")) : successResult();
	}

	private boolean isChoose(Boolean value) {
		if (value == null) {
			return false;
		}
		return value;
	}

	/**
	 * @description 未选项
	 * @author hxzhang
	 * @create 2016年8月9日下午2:56:44
	 * @version 1.0
	 * @param option
	 *            选择项
	 * @return true or false
	 */
	private boolean unselected(Boolean option) {
		if (null == option || option == false) {
			return true;
		}
		return false;
	}

	@Override
	public void saveFamilyInfo(String userId) {
		// 获取家庭信息
		FamilyInfo familyInfo = familyInfoMapper.getFamilyInfoByUserId(userId);
		// 获取family中所有人
		List<UserInfo> allUsers = userInfoMapper.getAllUsersByFamilyId(familyInfo.getId());
		// 如果该家庭中没有成员,则将该家庭删除
		if (null == allUsers || allUsers.size() == 0) {
			familyInfo.setDeleteFlag(DeleteFlag.Delete.getValue());
		}
		familyInfoMapper.updateByPrimaryKey(familyInfo);
	}

	/**
	 * @description 新增,编辑家长数据验证
	 * @author hxzhang
	 * @create 2016年8月11日上午8:32:02
	 * @version 1.0
	 * @param userInfo
	 *            家长
	 * @return 返回结果
	 */
	private ServiceResult<Object> validateAddOrUpdateParent(UserInfo userInfo, String familyName, UserInfoVo currentUser) {
		// 1.家长性别非空验证
		if (null == userInfo.getGender()) {
			return failResult(getTipMsg("parent_gender"));
		}
		// 2.家庭名称非空验证
		if (StringUtil.isEmpty(familyName)) {
			return failResult(getTipMsg("family_name"));
		}
		// 3.Given Name非空验证
		if (StringUtil.isEmpty(userInfo.getFirstName())) {
			return failResult(getTipMsg("parent_givenName"));
		}
		// 4.Surname非空验证
		if (StringUtil.isEmpty(userInfo.getLastName())) {
			return failResult(getTipMsg("parent_surname"));
		}
		// 5.Email非空验证
		if (StringUtil.isEmpty(userInfo.getEmail())) {
			return failResult(getTipMsg("parent_email"));
		}
		// 6.Email唯一性验证
		if (StringUtil.isEmpty(userInfo.getId()) && !userInfoService.validateEmail(userInfo.getEmail(), "")) {
			return failResult(getTipMsg("parent.email.repeat"));
		}
		// 7.兼职园长不能编辑家长信息
		isCasualCentreManager(currentUser);

		return successResult();
	}

	/**
	 * @description 更新家庭信息
	 * @author hxzhang
	 * @create 2016年8月11日下午4:17:03
	 * @version 1.0
	 * @param userId
	 *            用户ID
	 */
	private void updateFamilyInfo(String userId) {
		FamilyInfo familyInfo = familyInfoMapper.getFamilyInfoByUserId(userId);
		familyInfo.setUpdateTime(new Date());
		familyInfoMapper.updateByPrimaryKey(familyInfo);
	}

	@Override
	public int updateFamilyTimedTask(Date now) {
		logger.info("updateFamilyTimedTask==>start");
		// 获取小孩归档的家庭
		int count = familyInfoMapper.updateParentByTimedTask(now);
		System.out.println(count);
		logger.info("updateFamilyTimedTask==>end");
		return count;
	}

	@Override
	public void updateArchiveTime(String userId) {
		// 获取家庭信息
		FamilyInfo familyInfo = familyInfoMapper.getFamilyInfoByUserId(userId);
		// 判断该家庭是否有未归档小孩
		List<UserInfo> childList = userInfoMapper.getUnArchiveChildByFamilyId(familyInfo.getId());
		if (null == childList || childList.size() == 0) {
			familyInfo.setArchiveTime(new Date());
		} else {
			familyInfo.setArchiveTime(null);
		}
		familyInfoMapper.updateByPrimaryKey(familyInfo);

	}

	@Override
	public void sendEmailForParent(UserInfo child, boolean... flags) {
		// 获取该家庭已入园的小孩
		List<UserInfo> childList = userInfoMapper.getEnrolledChildByFamilyId(child.getFamilyId());
		// 获取该家庭的家长
		List<UserInfo> parentList = userInfoMapper.getParentInfoByFamilyId(child.getFamilyId());
		if (null == parentList || parentList.size() == 0) {
			return;
		}
		// 判断家长是否被归档
		boolean isArchived = true;
		for (UserInfo userInfo : parentList) {
			AccountInfo accountInfo = accountInfoMapper.getAccountInfoByUserId(userInfo.getId());
			if (accountInfo.getStatus() != ArchivedStatus.Archived.getValue()) {
				isArchived = false;
			}
		}
		// 判断是否有已经入园的孩子,除了当前孩子
		int enrolledCount = 0;
		for (UserInfo userInfo : childList) {
			ChildAttendanceInfo att = childAttendanceInfoMapper.getChildAttendanceByUserId(userInfo.getId());
			if (att.getEnrolled() == 1 && !userInfo.getId().equals(child.getId())) {
				enrolledCount++;
			}
		}

		if (isArchived) {
			saveBatchWelEmail(parentList, !flags[0] ? SendEmailType.welTimedTask.getValue() : SendEmailType.haveCancel.getValue(), flags[1], child);
		} else {
			// 发送邮件
			if (enrolledCount == 0) { // 没有已经入园的孩子则需要发送邮件
				saveBatchWelEmail(parentList, !flags[0] ? SendEmailType.welTimedTask.getValue() : SendEmailType.haveCancel.getValue(), flags[1], child);
				userFactory.createEmailMsg(child, parentList, EmailMsgType.msg_welcome.getValue());
			}
		}
	}

	@Override
	public void delCentreInfoByUnArchive(String userId) {
		UserInfo user = userInfoMapper.selectByPrimaryKey(userId);
		// 判断group是否归档
		RoomGroupInfo groupInfo = groupMapper.selectByPrimaryKey(user.getGroupId());
		if (null != groupInfo && 0 == groupInfo.getStatus()) {
			user.setGroupId(null);
		}
		// 判断room是否归档
		RoomInfo roomInfo = roomInfoMapper.selectByPrimaryKey(user.getRoomId());
		if (null != roomInfo && 0 == roomInfo.getStatus()) {
			user.setRoomId(null);
		}
		// 判断centre是否归档
		CentersInfo centreInfo = centreMapper.selectByPrimaryKey(user.getCentersId());
		if (null != centreInfo && 0 == centreInfo.getStatus()) {
			user.setCentersId(null);
		}
		userInfoMapper.updateByPrimaryKey(user);
		// 重新加入
		if (StringUtil.isNotEmpty(user.getCentersId()) && StringUtil.isNotEmpty(user.getRoomId())) {
			AccountInfo accountInfo = accountInfoMapper.selectAccountInfoByUserId(userId);
			ChildAttendanceInfo childAttendanceInfo = childAttendanceInfoMapper.selectByAccountId(accountInfo.getId());
			// 此小孩之前已入院 那么记录原本的信息到历史表
			ChangeAttendanceRequestInfo attendanceRequestInfo = new ChangeAttendanceRequestInfo();
			attendanceRequestInfo.setChildId(accountInfo.getId());
			attendanceRequestInfo.setChangeDate(new Date());
			attendanceRequestInfo.setNewCentreId(user.getCentersId());
			attendanceRequestInfo.setNewRoomId(user.getRoomId());
			attendanceFactory.dealAttendanceRequest(attendanceRequestInfo, childAttendanceInfo);
			attendanceHistoryService.dealAttendanceHistory(attendanceRequestInfo, null);
		}
	}

	@Override
	public void sendActiveEmailTimedTask(Date now) {
		logger.info("sendActiveEmailTimedTask==>start");
		// 获取有未激活家长且有已入园的家庭
		List<FamilyInfo> familyInfoList = familyInfoMapper.getHaveUnActiveParentEnrolledChild();
		for (FamilyInfo family : familyInfoList) {
			// 获取家庭中最早入园小孩的入园信息
			List<ChildAttendanceInfo> attendanceList = childAttendanceInfoMapper.getChildAttendanceInfoByFamilyId(family.getId());
			long date = DateUtil.getPreviousDay(attendanceList.get(0).getEnrolmentDate());
			int n = 1;
			logger.info("mei7tianfasongyoujian==>start");
			while (!((date + (n * 7)) > (DateUtil.getPreviousDay(now)))) {
				if (((date + (n * 7)) - (DateUtil.getPreviousDay(now))) == 0) {
					saveBatchWelEmail(userInfoMapper.getUnActiveParentByFamilyId(family.getId()), SendEmailType.timedTask.getValue(), false, null);
				}
				n += n;
			}
			logger.info("mei7tianfasongyoujian==>end");
		}
		logger.info("sendActiveEmailTimedTask==>end");
	}

	@Override
	public ServiceResult<Object> updateEnrolChild(String userId, String accountId) {
		log.info("updateEnrolChild|start|userId=" + userId);
		childAttendanceInfoMapper.enrolChild(userId);
		accountInfoMapper.updateStatusByUserId(userId, AccountActiveStatus.Enabled.getValue());
		log.info("updateEnrolChild|end|accountId=" + accountId);
		return successResult(getTipMsg("operation_success"));
	}

	/**
	 * @description 判断是否为true
	 * @author hxzhang
	 * @create 2016年9月2日上午10:18:35
	 * @version 1.0
	 * @param o
	 * @return
	 */
	private boolean isTrue(Boolean o) {
		if (null != o && o == true) {
			return true;
		}
		return false;
	}

	/**
	 * @description 判断是否选择
	 * @author hxzhang
	 * @create 2016年9月2日下午5:51:24
	 * @version 1.0
	 * @param outFamily
	 * @return
	 */
	private List<Boolean> isChoose(OutFamilyPo outFamily) {
		List<Boolean> chooseList = new ArrayList<Boolean>();
		if (isTrue(outFamily.getMonday())) {
			chooseList.add(outFamily.getMonday());
		}
		if (isTrue(outFamily.getTuesday())) {
			chooseList.add(outFamily.getTuesday());
		}
		if (isTrue(outFamily.getWednesday())) {
			chooseList.add(outFamily.getWednesday());
		}
		if (isTrue(outFamily.getThursday())) {
			chooseList.add(outFamily.getThursday());
		}
		if (isTrue(outFamily.getFriday())) {
			chooseList.add(outFamily.getFriday());
		}
		return chooseList;
	}

	@Override
	public ServiceResult<Object> saveWelEmail(String userId) {
		List<UserInfo> userList = new ArrayList<UserInfo>();
		userList.add(userInfoMapper.selectByPrimaryKey(userId));
		ServiceResult<Object> result = saveBatchWelEmail(userList, SendEmailType.isManual.getValue(), false, null);
		return result;
	}

	@Override
	public List<ChildLogsVo> getLogList(String userId) {
		List<ChildLogsVo> list = childLogsInfoMapper.getLogList(userId);
		for (ChildLogsVo childLogsVo : list) {
			childLogsVo.setReason(childLogsVo.getReason().replace("\n", "<br/>"));
		}
		return list;
	}

	@Override
	public ServiceResult<Object> addLogs(ChildLogsVo childLogsVo, UserInfoVo currtenUser) {
		logger.info("addLogs|start|");
		ServiceResult<Object> result = validateLog(childLogsVo);
		if (!result.isSuccess()) {
			logger.info("data fail");
			return failResult(result.getMsg());
		}
		String attachId = initFile(childLogsVo);
		// 当前登录人
		String currtenAccountId = currtenUser.getAccountInfo().getId();
		String logId = childLogsVo.getId();
		Date date = new Date();
		if (StringUtil.isEmpty(logId)) {
			String uuid = UUID.randomUUID().toString();
			ChildLogsInfo childLog = new ChildLogsInfo();
			childLog.setId(uuid);
			childLog.setReason(childLogsVo.getReason());
			childLog.setLogDate(childLogsVo.getLogDate());
			childLog.setUserId(childLogsVo.getUserId());
			childLog.setAttachId(attachId);
			childLog.setCreateTime(date);
			childLog.setUpdateTime(date);
			childLog.setCreateAccountId(currtenAccountId);
			childLog.setUpdateAccountId(currtenAccountId);
			childLog.setDeleteFlag(DeleteFlag.Default.getValue());
			int count = childLogsInfoMapper.insert(childLog);
			logger.info("addLogs|end|count=" + count);
			return successResult((Object) childLogsVo);
		} else {
			logger.info("logId=" + logId);
			ChildLogsInfo dbLog = childLogsInfoMapper.selectByPrimaryKey(logId);
			dbLog.setCreateAccountId(currtenAccountId);
			dbLog.setLogDate(childLogsVo.getLogDate());
			dbLog.setReason(childLogsVo.getReason());
			dbLog.setAttachId(attachId);
			dbLog.setUpdateTime(date);
			dbLog.setCreateAccountId(currtenAccountId);
			dbLog.setUpdateAccountId(currtenAccountId);
			int count = childLogsInfoMapper.updateByPrimaryKeySelective(dbLog);
			if (StringUtil.isEmpty(attachId)) {
				childLogsInfoMapper.updateAttachIdNull(logId);
			}
			logger.info("addLogs|end|count=" + count);
			return successResult((Object) dbLog);
		}
	}

	private String initFile(ChildLogsVo childLogsVo) {
		if (StringUtil.isNotEmpty(childLogsVo.getAttachId())) {
			return childLogsVo.getAttachId();
		}
		FileVo file = childLogsVo.getFile();
		String sourceId = null;
		if (file != null) {
			sourceId = UUID.randomUUID().toString();
			List<FileVo> fileVoList = new ArrayList<FileVo>();
			fileVoList.add(file);
			try {
				commonService.initAttachmentInfo(sourceId, fileVoList, FilePathUtil.FAMILY_PATH);
			} catch (Exception e) {
				logger.error("msg_file_upload_failed||", e);
				throw new RuntimeException();
			}
		}
		return sourceId;
	}

	/**
	 * 
	 * @description 数据校验
	 * @author gfwang
	 * @create 2016年9月5日下午7:36:45
	 * @version 1.0
	 * @param log
	 * @return
	 */
	private ServiceResult<Object> validateLog(ChildLogsVo childLog) {
		if (StringUtil.isEmpty(childLog.getReason())) {
			return failResult(getTipMsg("child.log.reason.empty"));
		}
		if (null == childLog.getLogDate()) {
			return failResult(getTipMsg("child.log.logdate.empty"));
		}
		return successResult();
	}

	/*
	 * private ServiceResult<Object> operationAuth() { }
	 */

	@Override
	public ServiceResult<Object> deleteLogs(String logId) {
		logger.info("deleteLogs|start|logId=" + logId);
		int count = childLogsInfoMapper.deleteLogById(logId);
		logger.info("deleteLogs|end|count=" + count);
		return successResult();
	}

	@Override
	public ServiceResult<Object> getEnrolledChildByRoomId(String roomId, String params) {
		List<SelecterPo> childList = userInfoMapper.getEnrolledChildByRoomId(roomId, params);
		return successResult((Object) childList);
	}

	@Override
	public ServiceResult<Object> getChildListForSelect2(String id, String p) {
		return successResult((Object) userInfoMapper.getChildListForSelect2(id, p));
	}

	@Override
	public List<Integer> getAgeStage() {
		List<Integer> ages = familyInfoMapper.getAgeStage();
		return ages;
	}

	@Override
	public List<EylfInfoVo> getEylf(Integer ageScope, String childId) {
		List<EylfInfoVo> info = familyInfoMapper.getChildScopeEylf(ageScope, childId);
		return info;
	}

	@Override
	public ServiceResult<Object> updateAttendChangeState(UserInfoVo user, String userId, boolean attendFlag) {
		ChildAttendanceInfo changeInfo = childAttendanceInfoMapper.selectByUserId(userId);
		if (changeInfo.getType() == ChildType.Inside.getValue()) {
			return failResult(getTipMsg("no.permissions"));
		}
		int count = childAttendanceInfoMapper.attendChangeByOut(attendFlag, userId);
		logger.info("attendChangeState result=" + count);
		return successResult();
	}

	@Override
	public ServiceResult<Object> taskList(String accountId) {
		String str = getSystemMsg("custom_task_name");
		String[] strs = str.split(";");
		List<TaskVo> list = familyInfoMapper.getTaskList(accountId, strs);
		return successResult((Object) list);
	}

	@Override
	public ServiceResult<Object> getReportsHtml(ReportVo reportVo) throws Exception {
		// 处理PDF相关数据
		UserInfo child = userInfoMapper.selectByPrimaryKey(reportVo.getUserId());
		reportVo.setName(child.getFullName());
		reportVo.setAvatar(getAvatar(child));

		String filePath = ContextLoader.getCurrentWebApplicationContext().getServletContext().getRealPath("/") + "resources" + File.separator + "library" + File.separator
				+ "template" + File.separator;
		String templateName = "report.ftl";
		FreemarkerUtil freemarkerUtil = new FreemarkerUtil(filePath);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(reportVo.getClass().getSimpleName(), reportVo);
		String html = freemarkerUtil.printString(templateName, params);
		return successResult((Object) html);
	}

	public void saveActivateParents(String userId) {
		String familyId = userInfoMapper.selectByPrimaryKey(userId).getFamilyId();
		// 查询该家庭是否有未归档的孩子
		UserCondition condition = new UserCondition();
		condition.setUserType(UserType.Child.getValue());
		condition.setStatus(ArchivedStatus.UnArchived.getValue());
		condition.setFamilyId(familyId);
		List<UserInfo> childList = userInfoMapper.getUserByCondition(condition);
		if (ListUtil.isEmpty(childList)) {
			return;
		}
		// 查询该家庭是否有归档的家长
		condition.setUserType(UserType.Parent.getValue());
		condition.setStatus(ArchivedStatus.Archived.getValue());
		List<UserInfo> parentList = userInfoMapper.getUserByCondition(condition);
		if (ListUtil.isEmpty(parentList)) {
			return;
		}
		// 将归档的家长重新激活
		for (UserInfo userInfo : parentList) {
			userInfoMapper.updateStatusByUserId(ArchivedStatus.UnArchived.getValue(), userInfo.getId());
		}
		// 清空归档时间
		familyInfoMapper.clearArchiveTimeByFamilyId(familyId);
	}

	/**
	 * @description 选择特殊的园区发送邮件
	 * @author hxzhang
	 * @create 2017年7月27日下午3:07:17
	 */
	private void sendSpecialEmail(OutModleVo outModleVo) {
		List<String> idList = outModleVo.getCentreList();
		if (ListUtil.isEmpty(idList)) {
			return;
		}
		List<CentersInfo> centerList = centersInfoMapper.getCentersByIds(idList);
		String centerStr = "";
		for (CentersInfo c : centerList) {
			centerStr += "<tr><td>Kindikids Early Learning " + c.getName() + "<br/>" + c.getCentreAddress() + "</td></tr>";
		}
		Map<String, String> map = dealOutModleVo(outModleVo, centerStr);
		String htmlStr = getSpecialEmailStr(map);
		System.err.println(htmlStr);
		// 获取秘密抄送人
		String[] bccs = getSystemMsg("specialAdminEmailBccs").split(";");
		userFactory.sendSpecialEmail(new EmailVo(getSystemMsg("specialAdminEmail"), getSystemMsg("specialAdminName"), "WAITLIST APPLICATION", htmlStr, true, bccs));
	}

	private Map<String, String> dealOutModleVo(OutModleVo outModleVo, String centerStr) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("centerStr", centerStr);
		map.put("firstName1", outModleVo.getFirstname1());
		map.put("lastName1", outModleVo.getLastname1());
		map.put("relationship1", outModleVo.getRelationship1());
		map.put("address1", outModleVo.getAddress1());
		map.put("suburb1", outModleVo.getSuburb1());
		map.put("postcode1", outModleVo.getPostcode1());
		map.put("homephone1", outModleVo.getHomephone1());
		map.put("workPhone1", outModleVo.getWorkphone1());
		map.put("mobile1", outModleVo.getMobile1());
		map.put("email1", outModleVo.getEmail1());

		map.put("firstName2", outModleVo.getFirstname2());
		map.put("lastName2", outModleVo.getLastname2());
		map.put("relationship2", outModleVo.getRelationship2());
		map.put("address2", outModleVo.getAddress2());
		map.put("suburb2", outModleVo.getSuburb2());
		map.put("postcode2", outModleVo.getPostcode2());
		map.put("homephone2", outModleVo.getHomephone2());
		map.put("workPhone2", outModleVo.getWorkphone2());
		map.put("mobile2", outModleVo.getMobile2());
		map.put("email2", outModleVo.getEmail2());

		map.put("firstName", outModleVo.getChildFirstname());
		map.put("lastName", outModleVo.getChildLastname());
		if (outModleVo.getChildSex() != null && outModleVo.getChildSex().equals("1")) {
			map.put("gender", "Male");
		} else if (outModleVo.getChildSex() != null && outModleVo.getChildSex().equals("0")) {
			map.put("gender", "Female");
		}

		map.put("birth", outModleVo.getDobChild());
		map.put("nationality", outModleVo.getNationality());
		map.put("language", outModleVo.getLanguages());
		map.put("religion", outModleVo.getReligion());
		if (outModleVo.getHasImmunised() != null && outModleVo.getHasImmunised().equals("1")) {
			map.put("immunised", "YES");
		} else {
			map.put("immunised", "NO");
		}
		if (outModleVo.getHasAnaphylaxis() != null && outModleVo.getHasAnaphylaxis().equals("1")) {
			map.put("anaphy", "YES");
		} else {
			map.put("anaphy", "NO");
		}
		map.put("diff", outModleVo.getLearningDifficulties());
		map.put("medical", outModleVo.getMedicalConditions());
		map.put("requir", outModleVo.getDietaryRequipments());
		map.put("aller", outModleVo.getAllergies());
		map.put("information", outModleVo.getProposal());
		map.put("aboutUs", outModleVo.getChannel());

		map.put("dateStart", outModleVo.getChangeDate());
		String str = "";
		if (outModleVo.getMonday() != null && outModleVo.getMonday()) {
			str += "Monday,";
		}
		if (outModleVo.getTuesday() != null && outModleVo.getTuesday()) {
			str += "Tuesday,";
		}
		if (outModleVo.getWednesday() != null && outModleVo.getWednesday()) {
			str += "Wednesday,";
		}
		if (outModleVo.getThursday() != null && outModleVo.getThursday()) {
			str += "Thursday,";
		}
		if (outModleVo.getFriday() != null && outModleVo.getFriday()) {
			str += "Friday,";
		}
		if (StringUtil.isNotEmpty(str)) {
			str = str.substring(0, str.length() - 1);
		}

		map.put("reques", str);
		return map;
	}

	private String getSpecialEmailStr(Map<String, String> map) {
		String html = getSpecialEmailStr();
		for (String key : map.keySet()) {
			html = html.replaceAll(getHtmlTag(key), map.get(key));
		}
		return html;
	}

	private String getHtmlTag(String tagName) {
		return "\\$\\{" + tagName + "\\}";
	}

	@Override
	public String saveOutside(OutFamilyPo outFamily) {
		UserInfo child = outFamily.getChild();
		String familyId = outFamily.getFamilyId();
		// 新增用戶
		String childId = addUserOut(child, familyId, UserType.Child, new Date());
		// 新增賬戶信息
		log.info("start|buildRelationKidParentInfo");
		AccountInfo accountInfo = addAccountOut(child, Role.Children);
		// 建立关系
		buildRelationKidParentInfo(child);
		// 新增attends
		addAttendsOut(child.getId(), outFamily);
		// 新增健康详细信息
		addHealthMedicalDetail(child.getId());
		ChildInfoVo childInfoVo = new ChildInfoVo();
		// 初始化饮食要求信息
		initOrUpdateChildDietaryRequireInfo(child.getId(), childInfoVo.getDietaryRequire(), null);
		// 初始化药物信息
		initOrUpdateChildMedicalInfo(child.getId(), childInfoVo.getMedical(), null);
		// 新增监护人
		initOrUpdateChildCustodyArrangeInfo(child.getId(), childInfoVo.getCustodyArrange(), null);
		// 新增健康医疗信息
		initOrUpdateChildHealthMedicalInfo(child.getId(), childInfoVo.getHealthMedical(), null);
		Date now = new Date();
		for (UserInfo parent : outFamily.getParentList()) {
			if (StringUtil.isEmpty(parent.getFirstName()) || StringUtil.isEmpty(parent.getLastName()) || StringUtil.isEmpty(parent.getEmail())) {
				continue;
			}
			// 新增用戶
			addUserOut(parent, familyId, UserType.Parent, now);
			// 新增賬戶信息
			addAccountOut(parent, Role.Parent);
			log.info("start|buildRelationKidParentInfo");
			// 建立关系
			buildRelationKidParentInfo(parent);
			now.setTime(now.getTime() + 1000);
		}
		// 更新FamilyInfo
		String familyName = outFamily.getFamilyName();
		initOrUpdateFamilyOut(familyId, familyName);
		return childId;
	}

	@Override
	public void removeChild(String userId) {
		// 删除孩子相关信息
		userInfoMapper.removeUserById(userId);
		accountInfoMapper.removeAccountByUserId(userId);
		// relationKidParentInfoMapper.removeRelation(accountInfoMapper.getAccountInfoByUserId(userId).getId());
		// childAttendanceInfoMapper.removeChildAtten(userId);
		// childHealthMedicalDetailInfoMapper.removeHealthMedicalDetail(userId);
		// dietaryRequireMapper.removeDietaryRequire(userId);
		// medicalMapper.removeMedical(userId);
	}

	@Override
	public void removeParent(String familyId) {
		// 获取家长
		List<UserInfo> parents = userInfoMapper.getParentInfoByFamilyId(familyId);
		for (UserInfo p : parents) {
			userInfoMapper.removeUserById(p.getId());
			accountInfoMapper.removeAccountByUserId(p.getId());
		}
	}

	@Override
	public void removeFamily(String familyId) {
		// 删除家庭
		familyInfoMapper.removeFamilyById(familyId);
	}

	@Override
	public ServiceResult<Object> saveMoveAnotherFamily(MoveFamilyVo vo, UserInfoVo currtenUser) {
		// 获取孩子信息
		UserInfo child = userInfoMapper.selectByPrimaryKey(vo.getUserId());
		// 获取该家庭小孩数量
		List<UserInfo> childs = userInfoMapper.getChildInfoByFamilyId(child.getFamilyId());
		// 如果只有一个小孩,将该家庭及家长删除
		if (childs.size() == 1) {
			removeParent(child.getFamilyId());
			removeFamily(child.getFamilyId());
		}
		// 更新孩子新的家庭
		child.setFamilyId(vo.getFamilyId());
		child.setUpdateTime(new Date());
		child.setUpdateAccountId(currtenUser.getAccountInfo().getId());
		userInfoMapper.updateByPrimaryKey(child);
		return successResult();
	}

	@Override
	public ServiceResult<Object> saveMoveToOutside(String userId, boolean flag, UserInfoVo currtenUser) {
		// 处理孩子attendance信息
		dealAttendance(userId, flag, currtenUser);
		// 处理孩子attendance rquest信息
		dealAttendanceRequest(userId, currtenUser);
		// 处理孩子AbsenteeRequest
		dealAbsenteeRequest(userId, currtenUser);
		// 处理孩子temporary attendance
		dealTemporary(userId);
		// 处理孩子Subtracted
		dealSubtracted(userId);
		// 处理孩子离园申请giving notice
		dealGivingNotice(userId);
		// 处理孩子园区
		dealCenter(userId);
		// 处理孩子account信息
		dealAccountInfo(userId, currtenUser);
		// 处理孩子user信息
		dealUserInfo(userId, currtenUser);

		return successResult();
	}

	private void dealCenter(String userId) {
		// 判断该小孩在外部园区是否存在记录
		if (ListUtil.isNotEmpty(outChildCenterInfoMapper.getListByUserId(userId))) {
			return;
		}
		outChildCenterInfoMapper.insert(new OutChildCenterInfo(userId, userInfoMapper.selectByPrimaryKey(userId).getCentersId()));
	}

	private void dealAttendanceHistory(String accountId, UserInfoVo currtenUser, String previousEnrolmentId) {
		List<AttendanceHistoryInfo> list = attendanceHistoryInfoMapper.getListByChild(accountId);
		for (AttendanceHistoryInfo a : list) {
			dealPreviousEnrolmentRelation(a.getId(), previousEnrolmentId);
		}
		attendanceHistoryInfoMapper.removeHistoryByChildId(accountId, new Date(), currtenUser.getAccountInfo().getId());
	}

	private void dealGivingNotice(String userId) {
		givingNoticeInfoMapper.cancelChildGivingNotice(userId);
	}

	private void dealEmailMsg(String userId, String previousEnrolmentId) {
		List<EmailMsg> list = emailMsgMapper.getEmailMsgListByUserId(userId);
		for (EmailMsg emailMsg : list) {
			dealPreviousEnrolmentRelation(emailMsg.getId(), previousEnrolmentId);
		}
		emailMsgMapper.removeEmailMsgByUserId(userId);
	}

	private void dealTemporary(String userId) {
		temporaryAttendanceInfoMapper.deleteOld(accountInfoMapper.getAccountInfoByUserId(userId).getId());
	}

	private void dealSubtracted(String userId) {
		subtractedAttendanceMapper.deleteOld(accountInfoMapper.getAccountInfoByUserId(userId).getId());
	}

	private void dealAbsenteeRequest(String userId, UserInfoVo currtenUser) {
		List<AbsenteeRequestInfoWithBLOBs> list = absenteeRequestInfoMapper.getAbsenteesByChild(accountInfoMapper.getAccountInfoByUserId(userId).getId());
		for (AbsenteeRequestInfoWithBLOBs bloBs : list) {
			bloBs.setState(ChangeAttendanceRequestState.Cancel.getValue());
			bloBs.setUpdateTime(new Date());
			bloBs.setUpdateAccountId(currtenUser.getAccountInfo().getId());
			bloBs.setDeleteFlag(DeleteFlag.Delete.getValue());
			absenteeRequestInfoMapper.updateByPrimaryKey(bloBs);
		}
	}

	private void dealAttendanceRequest(String userId, UserInfoVo currtenUser) {
		List<ChangeAttendanceRequestInfo> list = changeAttendanceRequestInfoMapper.getListByChild(accountInfoMapper.getAccountInfoByUserId(userId).getId());
		for (ChangeAttendanceRequestInfo info : list) {
			info.setState(ChangeAttendanceRequestState.Cancel.getValue());
			info.setUpdateTime(new Date());
			info.setUpdateAccountId(currtenUser.getAccountInfo().getId());
			changeAttendanceRequestInfoMapper.updateByPrimaryKey(info);
		}
	}

	private void dealPreviousEnrolmentRelation(String objId, String previousEnrolmentId) {
		PreviousEnrolmentRelation p = new PreviousEnrolmentRelation();
		p.setObjId(objId);
		p.setPreviousEnrolmentId(previousEnrolmentId);
		previousEnrolmentRelationMapper.insert(p);
	}

	private void dealAttendance(String userId, boolean flag, UserInfoVo currtenUser) {
		ChildAttendanceInfo attendanceInfo = childAttendanceInfoMapper.getChildAttendanceByUserId(userId);

		// 创建Previous Enrolment
		dealPreviousEnrolment(attendanceInfo, currtenUser);

		attendanceInfo.setLeaveDate(null);
		attendanceInfo.setRequestDate(null);
		attendanceInfo.setEnrolled(EnrolledState.Default.getValue());
		attendanceInfo.setAttendFlag(false);
		attendanceInfo.setType((short) 1);
		attendanceInfo.setChangeDate(null);
		attendanceInfo.setEnrolmentDate(null);
		attendanceInfo.setOrientation(null);
		attendanceInfo.setSiblingflag(flag);
		attendanceInfo.setAttendFlag(true);

		attendanceInfo.setUpdateTime(new Date());
		attendanceInfo.setUpdateAccountId(currtenUser.getAccountInfo().getId());
		childAttendanceInfoMapper.updateByPrimaryKey(attendanceInfo);
	}

	private void dealPreviousEnrolment(ChildAttendanceInfo attendanceInfo, UserInfoVo currtenUser) {
		String id = UUID.randomUUID().toString();
		PreviousEnrolment previousEnrolment = new PreviousEnrolment();
		previousEnrolment.setId(id);
		previousEnrolment.setApplicationDate(attendanceInfo.getCreateTime());
		previousEnrolment.setRequestedDate(attendanceInfo.getRequestStartDate());
		previousEnrolment.setOrientationDate(attendanceInfo.getOrientation());
		previousEnrolment.setFirstDay(attendanceInfo.getChangeDate());
		previousEnrolment.setRequestedLastDay(attendanceInfo.getRequestDate());
		previousEnrolment.setLastDay(attendanceInfo.getLeaveDate());
		previousEnrolment.setUserId(attendanceInfo.getUserId());
		previousEnrolment.setStatus(accountInfoMapper.getAccountInfoByUserId(attendanceInfo.getUserId()).getStatus());
		previousEnrolment.setType(attendanceInfo.getType());
		previousEnrolment.setEnrolled(attendanceInfo.getEnrolled());
		previousEnrolment.setCreateTime(new Date());
		previousEnrolment.setDeleteFlag(false);

		previousEnrolmentMapper.insert(previousEnrolment);

		// 处理孩子入园时的email msg
		dealEmailMsg(attendanceInfo.getUserId(), id);

		AccountInfo accountInfo = accountInfoMapper.getAccountInfoByUserId(attendanceInfo.getUserId());
		// 处理孩子attendance history
		dealAttendanceHistory(accountInfo.getId(), currtenUser, id);
		// 处理RequestLog
		dealRequestLog(accountInfo.getId(), id);
	}

	private void dealRequestLog(String accountId, String previousEnrolmentId) {
		List<RequestLogInfo> list = requestLogInfoMapper.getListByChild(accountId);
		for (RequestLogInfo r : list) {
			dealPreviousEnrolmentRelation(r.getId(), previousEnrolmentId);
		}
		requestLogInfoMapper.removeLogsByChild(accountId);
	}

	private void dealUserInfo(String userId, UserInfoVo currtenUser) {
		UserInfo userInfo = userInfoMapper.selectByPrimaryKey(userId);
		userInfo.setCentersId(null);
		userInfo.setRoomId(null);
		userInfo.setGroupId(null);
		userInfo.setUpdateTime(new Date());
		userInfo.setUpdateAccountId(currtenUser.getAccountInfo().getId());
		userInfoMapper.updateByPrimaryKey(userInfo);
	}

	private void dealAccountInfo(String userId, UserInfoVo currtenUser) {
		AccountInfo accountInfo = accountInfoMapper.getAccountInfoByUserId(userId);
		accountInfo.setStatus(ArchivedStatus.Default.getValue());
		accountInfo.setUpdateTime(new Date());
		accountInfo.setUpdateAccountId(currtenUser.getAccountInfo().getId());
		accountInfoMapper.updateByPrimaryKey(accountInfo);
	}

	@Override
	public void dealPlanEmail(ChildAttendanceInfo childAttendanceInfo, short plan) {
		if (plan == 0) {
			return;
		}
		AccountInfo accountInfo = accountInfoMapper.getAccountInfoByUserId(childAttendanceInfo.getUserId());
		UserInfo userInfo = userInfoMapper.selectByPrimaryKey(childAttendanceInfo.getUserId());
		childAttendanceInfo.setCenterId(userInfo.getCentersId());
		childAttendanceInfo.setPlan(plan == 1 ? true : false);
		userFactory.removePlanEmail(accountInfo.getId());
		userFactory.sendParentEmailByChild(getSystemEmailMsg("child_inside_title"), getSystemEmailMsg("child_inside_content"), accountInfo.getId(), true,
				EmailType.inside.getValue(), null, null, childAttendanceInfo, childAttendanceInfo.isPlan());
	}

	@Override
	public int dealNotes() {
		WaitingListCondition condition = new WaitingListCondition();
		List<Short> typeList = new ArrayList<Short>();
		typeList.add((short) 1);
		typeList.add((short) 2);
		condition.setListType(typeList);
		// 获取External和Sibling
		List<WaitingListVo> list = applicationListMapper.getOutChild(condition);
		for (WaitingListVo vo : list) {
			List<UserInfo> childs = userInfoMapper.getChildListForNotes(vo.getChildName(), vo.getBirthday());
			// 获取孩子最早的ID
			if (ListUtil.isEmpty(childs)) {
				continue;
			}
			UserInfo child = childs.get(0);
			if (child == null) {
				continue;
			}
			// 通过ID去查询对应的notes
			List<ChildNotes> notes = childNotesMapper.getNotesByUserId(child.getId());
			synchroData(notes, vo);
		}
		return list.size();
	}

	private void synchroData(List<ChildNotes> notes, WaitingListVo vo) {
		for (ChildNotes note : notes) {
			ChildLogsInfo childLogsInfo = new ChildLogsInfo();
			childLogsInfo.setId(UUID.randomUUID().toString());
			childLogsInfo.setReason(note.getReason());
			childLogsInfo.setAttachId(note.getAttachId());
			childLogsInfo.setCreateAccountId(note.getCreateAccountId());
			childLogsInfo.setLogDate(note.getLogDate());
			childLogsInfo.setCreateTime(note.getCreateTime());
			childLogsInfo.setUserId(vo.getId());
			childLogsInfo.setDeleteFlag(note.getDeleteFlag());
			childLogsInfo.setUpdateAccountId(note.getUpdateAccountId());
			childLogsInfo.setUpdateTime(note.getUpdateTime());
			childLogsInfoMapper.insert(childLogsInfo);
		}
	}

	@Override
	public ServiceResult<Object> saveComplete(ChildInfoVo childInfoVo) throws Exception {
		ValueInfoVO valueInfoVO = (ValueInfoVO) JsonUtil.jsonToBean(childInfoVo.getJson(), ValueInfoVO.class);
		ServiceResult<String> formResult = formService.saveInstance(valueInfoVO);
		if (!formResult.isSuccess()) {
			return failResult(getTipMsg("operation_failed"));
		}
		String valueId = formResult.getReturnObj();
		commonService.buildUserProfileRelation(childInfoVo.getUserInfo().getId(), childInfoVo.getProfileId(), valueId);

		accountInfoMapper.completeById(childInfoVo.getAccountInfo().getId());
		userFactory.sendParentEmailByChild(getSystemEmailMsg("into_center_title"), getSystemEmailMsg("into_center_content"), childInfoVo.getAccountInfo().getId(), true,
				EmailType.intoCenter.getValue(), null, null, null, false);
		return successResult();
	}

	@Override
	public ServiceResult<Object> sendConfirmationEnrolmentEmail(String id) {
		userFactory.sendParentEmailByChild(getSystemEmailMsg("into_center_title"), getSystemEmailMsg("into_center_content"), id, true, EmailType.intoCenter.getValue(),
				null, null, null, false);
		return successResult();
	}
}
