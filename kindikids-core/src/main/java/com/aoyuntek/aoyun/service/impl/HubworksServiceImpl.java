package com.aoyuntek.aoyun.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.jsoup.Connection;
import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.vo.HubworksUserVo;
import com.aoyuntek.aoyun.service.IHubworksService;
import com.aoyuntek.aoyun.service.attendance.ISignService;
import com.aoyuntek.aoyun.uitl.GsonUtil;
import com.theone.date.util.DateUtil;
import com.theone.string.util.StringUtil;

@Service
public class HubworksServiceImpl extends BaseWebServiceImpl<UserInfo, UserInfoMapper> implements IHubworksService {
	@Autowired
	private UserInfoMapper userInfoMapper;
	@Autowired
	private ISignService signService;

	private Document childListDoc;
	private Document attendListDoc;

	@Override
	public String dealChildSync(Logger logger) {
		try {
			String[] hubUsernames = getSystemMsg("hubUsername").split(";");
			String passwrod = getSystemMsg("hubPassword");
			for (String username : hubUsernames) {
				Connection connection = Jsoup.connect("https://hubhello.com/hubhello/api/session").userAgent("Mozilla/5.0").timeout(50 * 1000).method(Method.POST)
						.header("Content-Type", "application/json").requestBody("{\"login\":\"" + username + "\",\"password\":\"" + passwrod + "\"}")
						.followRedirects(true).ignoreContentType(true);
				String json = connection.post().body().ownText();
				HubworksUserVo vo = (HubworksUserVo) GsonUtil.jsonToBean(json, HubworksUserVo.class);
				String serviceId = vo.getService_id();
				Response res = connection.execute();

				this.getChildList(serviceId, res);

				List<String> list = match(childListDoc.toString(), "div", "boxid"); // boxid值
				for (int i = 0; i < list.size(); i++) {
					int count = 5;
					boolean flag = dealHubworksCrn(list.get(i), serviceId, res, logger);
					while (!flag && count > 0) {
						flag = dealHubworksCrn(list.get(i), serviceId, res, logger);
						count--;
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "OK";
	}

	private boolean getChildList(String serviceId, Response res) {
		try {
			this.childListDoc = Jsoup.connect("https://hubhello.com/services/" + serviceId + "/children#").cookies(res.cookies()).maxBodySize(0).timeout(50000).get();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	private boolean dealHubworksCrn(String hubworksId, String serviceId, Response res, Logger logger) {
		try {
			Document crnData = Jsoup.connect("https://hubhello.com/services/" + serviceId + "/children/" + hubworksId).cookies(res.cookies()).timeout(50000).get();
			String crn = crnData.select("#child_crn_" + hubworksId).prev().text().replaceAll("\\\\n", "").trim();

			logger.info(serviceId + " - " + hubworksId + " - " + crn);

			if (StringUtil.isNotEmpty(crn)) {
				userInfoMapper.syncHubworksIdByCrn(hubworksId, crn);
			}
		} catch (IOException e) {
			e.printStackTrace();
			logger.info(serviceId + " - " + "error:" + hubworksId);
			return false;
		}
		return true;
	}

	@Override
	public String dealChildAttend(Date today, Logger logger) {
		try {
			String[] hubUsernames = getSystemMsg("hubUsername").split(";");
			String passwrod = getSystemMsg("hubPassword");
			for (String username : hubUsernames) {
				Connection connection = Jsoup.connect("https://hubhello.com/hubhello/api/session").userAgent("Mozilla/5.0").timeout(50 * 1000).method(Method.POST)
						.header("Content-Type", "application/json").requestBody("{\"login\":\"" + username + "\",\"password\":\"" + passwrod + "\"}")
						.followRedirects(true).ignoreContentType(true);
				String json = connection.post().body().ownText();
				HubworksUserVo vo = (HubworksUserVo) GsonUtil.jsonToBean(json, HubworksUserVo.class);
				String serviceId = vo.getService_id();
				Response res = connection.execute();

				Calendar calendar = Calendar.getInstance();
				calendar.setTime(today);
				int y = calendar.get(Calendar.YEAR);
				int m = calendar.get(Calendar.MONTH) + 1;
				int d = calendar.get(Calendar.DAY_OF_MONTH);

				this.getAttendList(serviceId, res, y, m, d);

				List<String> list = match(attendListDoc.toString(), "div", "boxid"); // boxid值

				logger.info(serviceId + " - " + list.size());

				for (int i = 0; i < list.size(); i++) {
					String id = list.get(i);
					boolean flag = dealSyncChildAttendance(today, id, serviceId, res, y, m, d, logger);
					int count = 5;
					while (!flag && count > 0) {
						flag = dealSyncChildAttendance(today, id, serviceId, res, y, m, d, logger);
						count--;
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "OK";
	}

	private boolean getAttendList(String serviceId, Response res, int y, int m, int d) {
		try {
			this.attendListDoc = Jsoup.connect("https://hubhello.com/services/" + serviceId + "/attendances/year/" + y + "/month/" + m + "/day/" + d)
					.cookies(res.cookies()).maxBodySize(0).timeout(50000).get();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	private boolean dealSyncChildAttendance(Date today, String id, String serviceId, Response res, int y, int m, int d, Logger logger) {
		try {
			Document attendTime = Jsoup
					.connect("https://hubhello.com/services/" + serviceId + "/attendances/year/" + y + "/month/" + m + "/day/" + d + "/" + id + "?hidden_child=false")
					.cookies(res.cookies()).header("X-Requested-With", "XMLHttpRequest").timeout(50000).get();
			attendTime = Jsoup.parse(attendTime.toString().replace("\\&quot;", ""));
			String signinId = id + "_actual_start_time_0";
			String signoutId = id + "_actual_end_time_0";

			Element signinElement = attendTime.getElementById(signinId);
			String signinTime = "";
			if (signinElement != null) {
				signinTime = signinElement.attr("value");
			}
			Element siginoutElement = attendTime.getElementById(signoutId);
			String signoutTime = "";
			if (siginoutElement != null) {
				signoutTime = siginoutElement.attr("value");
			}

			signService.dealSyncChildAttendance(id, today, signinTime, signoutTime);
			logger.info(serviceId + " - " + id + " - SyncAttendance - " + DateUtil.format(new Date(), DateUtil.yyyyMMddHHmmssSpt));

		} catch (Exception e) {
			e.printStackTrace();
			logger.info(serviceId + " - " + "error:" + id);
			return false;
		}
		return true;
	}

	public static List<String> match(String source, String element, String attr) {
		List<String> result = new ArrayList<String>();
		String reg = "<" + element + "[^<>]*?\\s" + attr + "=['\"]?(.*?)['\"]?\\s.*?>";
		Matcher m = Pattern.compile(reg).matcher(source);
		while (m.find()) {
			String r = m.group(1);
			result.add(r);
		}
		return result;
	}
}
