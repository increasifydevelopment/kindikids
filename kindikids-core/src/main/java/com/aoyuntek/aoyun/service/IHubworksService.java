package com.aoyuntek.aoyun.service;

import java.util.Date;

import org.apache.log4j.Logger;

/**
 * hubworks接口服务
 * 
 * @author hxzhang 2018年12月28日
 */
public interface IHubworksService {
	/**
	 * 同步孩子
	 * 
	 * @author hxzhang 2019年1月1日
	 * @return
	 */
	public String dealChildSync(Logger logger);

	/**
	 * 同步当天上课孩子签到
	 * 
	 * @author hxzhang 2019年1月1日
	 * @param today
	 */
	String dealChildAttend(Date today, Logger logger);

}
