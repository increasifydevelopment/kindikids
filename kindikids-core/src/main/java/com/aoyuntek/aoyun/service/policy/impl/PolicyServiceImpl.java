package com.aoyuntek.aoyun.service.policy.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.aoyuntek.aoyun.condtion.EventCondition;
import com.aoyuntek.aoyun.condtion.PolicyCondition;
import com.aoyuntek.aoyun.dao.AttachmentInfoMapper;
import com.aoyuntek.aoyun.dao.NqsRelationInfoMapper;
import com.aoyuntek.aoyun.dao.policy.PolicyCategoriesInfoMapper;
import com.aoyuntek.aoyun.dao.policy.PolicyDocumentsInfoMapper;
import com.aoyuntek.aoyun.dao.policy.PolicyInfoMapper;
import com.aoyuntek.aoyun.dao.policy.PolicyRoleRelationInfoMapper;
import com.aoyuntek.aoyun.entity.po.AttachmentInfo;
import com.aoyuntek.aoyun.entity.po.NqsRelationInfo;
import com.aoyuntek.aoyun.entity.po.policy.PolicyCategoriesInfo;
import com.aoyuntek.aoyun.entity.po.policy.PolicyDocumentsInfo;
import com.aoyuntek.aoyun.entity.po.policy.PolicyInfo;
import com.aoyuntek.aoyun.entity.po.policy.PolicyRoleRelationInfo;
import com.aoyuntek.aoyun.entity.vo.NqsVo;
import com.aoyuntek.aoyun.entity.vo.RoleInfoVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.policy.PolicyDocumentVo;
import com.aoyuntek.aoyun.entity.vo.policy.PolicyInfoVo;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.service.impl.BaseWebServiceImpl;
import com.aoyuntek.aoyun.service.policy.IPolicyService;
import com.aoyuntek.aoyun.uitl.FilePathUtil;
import com.aoyuntek.framework.constants.PropertieNameConts;
import com.aoyuntek.framework.model.Pager;
import com.theone.aws.util.FileFactory;
import com.theone.common.util.ServiceResult;
import com.theone.date.util.DateUtil;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

@Service
public class PolicyServiceImpl extends BaseWebServiceImpl<PolicyInfo, PolicyInfoMapper> implements IPolicyService {

	/**
	 * 日志
	 */
	public static Logger logger = Logger.getLogger(PolicyServiceImpl.class);
	@Autowired
	private PolicyInfoMapper policyInfoMapper;

	@Autowired
	private NqsRelationInfoMapper nqsRelationInfoMapper;

	@Autowired
	private PolicyRoleRelationInfoMapper policyRoleRelationInfoMapper;

	@Autowired
	private AttachmentInfoMapper attachmentInfoMapper;

	@Autowired
	private PolicyDocumentsInfoMapper policyDocumentsInfoMapper;

	@Autowired
	private PolicyCategoriesInfoMapper policyCategoriesInfoMapper;

	@Override
	public ServiceResult<Object> addOrUpdatePolicy(PolicyInfoVo policyInfoVo, UserInfoVo currentUser) {
		String id = null;
		// 数据合法性
		ServiceResult<Object> result = validateAddOrUpdatePolicy(policyInfoVo);
		if (!result.isSuccess()) {
			return result;
		}
		// 处理PolicyInfo
		PolicyInfo policyInfo = operationPolicyInfo(policyInfoVo, currentUser);

		// 处理Nqs
		operationNqs(policyInfo.getId(), policyInfoVo.getNqsInfo());

		// 处理角色关系
		operationVisibility(policyInfo.getId(), policyInfoVo.getPolicyVisibility());

		try {
			// 处理附件
			operationDoc(policyInfo.getId(), policyInfoVo.getPolicyDocList(), currentUser);
		} catch (Exception e) {
			logger.error("Policy_file_upload_failed||", e);
			throw new RuntimeException();
		}

		return successResult(getTipMsg("operation_success"), (Object) id);
	}
	
	@Override
	public ServiceResult<Object> deletePolicy(String id) {
		Date time = new Date();
		// 编辑
		PolicyInfo policyInfo = policyInfoMapper.selectByPrimaryKey(id);
		if(policyInfo!=null){
			policyInfo.setDeleteFlag(DeleteFlag.Delete.getValue());
			policyInfo.setUpdateTime(time);
			// 更新
			policyInfoMapper.updateByPrimaryKey(policyInfo);
		}
		return successResult(getTipMsg("operation_success"), (Object) id);
	}
	
	public ServiceResult<Object> existDeletePolicy(String policyCategoryId){
		int count=policyInfoMapper.selectPolicyCountVoByCategoryId(policyCategoryId);
		if(count>0){
			return failResult(getTipMsg("policy_category_existingPolicy")); 
		}
		
		return successResult(getTipMsg("operation_success"));
	}
	

	public ServiceResult<Object> addOrUpdatePolicyCategory(List<PolicyCategoriesInfo> PolicyCategoriesInfos, UserInfoVo currentUser) {
		int j = 0;
		// 判断是否有重复
		// 数据合法性
		ServiceResult<Object> result = validateAddOrUpdatePolicyCategory(PolicyCategoriesInfos);
		if (!result.isSuccess()) {
			return result;
		}
		
		for (int i =0,m= PolicyCategoriesInfos.size(); i < m; i++) {
			Date time = new Date();
			j++;
			PolicyCategoriesInfo policyCategoriesInfo = PolicyCategoriesInfos.get(i);
			// 判断是否是删除
			if (policyCategoriesInfo.getDeleteFlag() == 1 && !StringUtil.isEmpty(policyCategoriesInfo.getId())) {
				policyCategoriesInfo = policyCategoriesInfoMapper.selectByPrimaryKey(policyCategoriesInfo.getId());
				if (null != policyCategoriesInfo) {
					//判断该分类是否存在policy
					int count=policyInfoMapper.selectPolicyCountVoByCategoryId(policyCategoriesInfo.getId());
					if(count>0){
						return failResult(policyCategoriesInfo.getName()+" "+getTipMsg("policy_category_existingPolicy")); 
					}
					policyCategoriesInfo.setDeleteFlag(DeleteFlag.Delete.getValue());
					policyCategoriesInfoMapper.updateByPrimaryKey(policyCategoriesInfo);
				}
				continue;
			}

			// 判断是否新增
			if (policyCategoriesInfo.getDeleteFlag() == 0 && StringUtil.isEmpty(policyCategoriesInfo.getId())) {
				PolicyCategoriesInfo policyCategoriesInfoAdd = new PolicyCategoriesInfo();
				// 新增
				policyCategoriesInfoAdd.setId(UUID.randomUUID().toString());
				policyCategoriesInfoAdd.setName(policyCategoriesInfo.getName());
				policyCategoriesInfoAdd.setCreateAccountId(currentUser.getAccountInfo().getId());
				policyCategoriesInfoAdd.setCreateTime(DateUtil.addMinius(time, j));
				policyCategoriesInfoAdd.setDeleteFlag(DeleteFlag.Default.getValue());
				policyCategoriesInfoAdd.setUpdateAccountId(currentUser.getAccountInfo().getId());
				policyCategoriesInfoAdd.setUpdateTime(DateUtil.addMinius(time, j));
				policyCategoriesInfoMapper.insert(policyCategoriesInfoAdd);
				continue;
			}

			// 判断是否编辑
			if (policyCategoriesInfo.getDeleteFlag() == 0 && !StringUtil.isEmpty(policyCategoriesInfo.getId())) {
				// 编辑
				PolicyCategoriesInfo policyCategoriesInfoEdit = new PolicyCategoriesInfo();
				policyCategoriesInfoEdit = policyCategoriesInfoMapper.selectByPrimaryKey(policyCategoriesInfo.getId());
				if (null != policyCategoriesInfoEdit) {
					policyCategoriesInfoEdit.setName(policyCategoriesInfo.getName());
					policyCategoriesInfoEdit.setUpdateAccountId(currentUser.getAccountInfo().getId());
					policyCategoriesInfoEdit.setUpdateTime(DateUtil.addMinius(time, j));
					policyCategoriesInfoMapper.updateByPrimaryKey(policyCategoriesInfoEdit);
				}
				continue;
			}
		}

		return successResult(getTipMsg("operation_success"));
	}

	/**
	 * 演示编辑分类数据合法性
	 * 
	 * @param PolicyCategoriesInfos
	 * @return
	 */
	private ServiceResult<Object> validateAddOrUpdatePolicyCategory(List<PolicyCategoriesInfo> policyCategoriesInfos) {
		//判断分类名称是否有重复
		for(int i=0,m=policyCategoriesInfos.size();i<m;i++){
			PolicyCategoriesInfo categoriesInfo1=policyCategoriesInfos.get(i);
			for(int j=0,n=policyCategoriesInfos.size();j<n;j++){
				PolicyCategoriesInfo categoriesInfo2=policyCategoriesInfos.get(j);
				if(categoriesInfo1.getId()!=categoriesInfo2.getId() && categoriesInfo1.getName().equals(categoriesInfo2.getName()) 
						&& categoriesInfo1.getDeleteFlag()==0 && categoriesInfo2.getDeleteFlag()==0){
					return failResult(getTipMsg("policy_category_repeat")); 
				}
			}
		}
		
		return successResult();
	}

	@Override
	public ServiceResult<PolicyInfoVo> getPolicyInfo(String id, UserInfoVo currentUser) {
		PolicyInfoVo policyInfoVo = new PolicyInfoVo();
		if (!StringUtil.isEmpty(id)) {
			// 判断是否有权限可以修改，只有CEO及园长可以修改
			policyInfoVo = policyInfoMapper.selectPolicyInfoVoById(id);
			if (null == policyInfoVo) {
				return failResult(getTipMsg("operation_failed"));
			}
		}
		// 获取所有分类
		List<PolicyCategoriesInfo> policyCategoriesSrcList = policyCategoriesInfoMapper
				.selectCategoriesByIdOrDeleteFlag(policyInfoVo.getCategoryId());
		if (null != policyCategoriesSrcList && policyCategoriesSrcList.size() != 0) {
			policyInfoVo.setPolicyCategoriesSrcList(policyCategoriesSrcList);
		}
		return successResult(getTipMsg("operation_success"), policyInfoVo);
	}

	public ServiceResult<Pager<PolicyInfoVo, PolicyCondition>> getPolicyList(PolicyCondition policyCondition, UserInfoVo currentUser) {
		dealCondition(policyCondition);
		List<PolicyInfoVo> policyInfoVoList = new ArrayList<PolicyInfoVo>();
		List<RoleInfoVo> roleInfoVoList = currentUser.getRoleInfoVoList();
		List<String> currentRoles = new ArrayList<String>();
		boolean isCEOORCentreManager = false;
		policyCondition.setStartSize(policyCondition.getPageIndex());
		// 设置当前登录者角色集合
		for (int i = 0, m = roleInfoVoList.size(); i < m; i++) {
			// 判断是否是CEO或者园长
			if (roleInfoVoList.get(i).getValue() == Role.CEO.getValue() || roleInfoVoList.get(i).getValue() == Role.CentreManager.getValue()
					|| roleInfoVoList.get(i).getValue() == Role.EducatorSecondInCharge.getValue()) {
				isCEOORCentreManager = true;
				break;
			}
			currentRoles.add(roleInfoVoList.get(i).getId());
		}
		if (!isCEOORCentreManager) {
			policyCondition.setCurrentRoles(currentRoles);
		}
		// 查询集合
        policyInfoVoList = policyInfoMapper.selectPolicyInfoVoList(policyCondition);

		// 总数
		int totalSize = policyInfoMapper.selectPolicyInfoVoListCount(policyCondition);
		policyCondition.setTotalSize(totalSize);

		Pager<PolicyInfoVo, PolicyCondition> pager = new Pager<PolicyInfoVo, PolicyCondition>(policyInfoVoList, policyCondition);
		return successResult(getTipMsg("operation_success"), pager);
	}
	
	private void dealCondition(PolicyCondition condition) {
		if (StringUtil.isNotEmpty(condition.getNqsVersion())) {
			if (condition.getNqsVersion().toUpperCase().equals("QA1")) {
				condition.setSelectNqs("1");
			} else if (condition.getNqsVersion().toUpperCase().equals("QA2")) {
				condition.setSelectNqs("2");
			} else if (condition.getNqsVersion().toUpperCase().equals("QA3")) {
				condition.setSelectNqs("3");
			} else if (condition.getNqsVersion().toUpperCase().equals("QA4")) {
				condition.setSelectNqs("4");
			} else if (condition.getNqsVersion().toUpperCase().equals("QA5")) {
				condition.setSelectNqs("5");
			} else if (condition.getNqsVersion().toUpperCase().equals("QA6")) {
				condition.setSelectNqs("6");
			} else if (condition.getNqsVersion().toUpperCase().equals("QA7")) {
				condition.setSelectNqs("7");
			} else {
				condition.setSelectNqs(condition.getNqsVersion());
			}
		} else {
			condition.setSelectNqs(null);
		}
	}

	/**
	 * 获取完整分类信息
	 * 
	 * @return
	 */
	public ServiceResult<List<PolicyCategoriesInfo>> getCategoriesSrcList() {
		// 处理分类信息
		List<PolicyCategoriesInfo> policyCategoriesSrcList = policyCategoriesInfoMapper.selectCategoriesByIdOrDeleteFlag("");

		return successResult(getTipMsg("operation_success"), policyCategoriesSrcList);
	}

	/**
	 * 处理上传附件
	 * 
	 * @param id
	 * @param policyDocList
	 */
	private void operationDoc(String policyId, List<PolicyDocumentVo> policyDocList, UserInfoVo currentUser) throws Exception {
		if (null == policyDocList || policyDocList.size() == 0) {
			return;
		}

		List<String> ids = new ArrayList<String>();

		// 处理被页面删除的附件
		for (int i = policyDocList.size() - 1; i >= 0; i--) {
			// 如果页面标记为delete则处理删除
			if (policyDocList.get(i).getDeleteFlag() == 1) {
				if (null == policyDocList.get(i).getDocumentsId() || StringUtil.isEmpty(policyDocList.get(i).getDocumentsId())) {
					policyDocList.remove(i);
					continue;
				}
				attachmentInfoMapper.deleteBatchAttachmentInfo(null, policyDocList.get(i).getDocumentsId());

				ids.add(policyDocList.get(i).getId());

				policyDocList.remove(i);
			}
		}

		if (ids.size() != 0) {
			// 批量虚拟删除附件
			policyDocumentsInfoMapper.deleteDocByIds(ids);
		}

		int j = 0;
		FileFactory fac = new FileFactory(Region.getRegion(Regions.AP_SOUTHEAST_2),PropertieNameConts.S3);
		List<AttachmentInfo> attachmentInfos = new ArrayList<AttachmentInfo>();
		List<PolicyDocumentsInfo> policyDocumentsInfos = new ArrayList<PolicyDocumentsInfo>();
		// 新增附件
		for (int i = policyDocList.size() - 1; i >= 0; i--) {
			Date time = new Date();
			j++;
			PolicyDocumentVo policyDocVo = policyDocList.get(i);
			if (StringUtil.isEmpty(policyDocVo.getId()) && policyDocVo.getAttachId().indexOf(FilePathUtil.POLICY_PATH) == -1) {
				// 创建附件主表
				PolicyDocumentsInfo policyDocInfo = new PolicyDocumentsInfo();
				policyDocInfo.setId(UUID.randomUUID().toString());
				policyDocInfo.setPolicyId(policyId);
				policyDocInfo.setDocumentsId(UUID.randomUUID().toString());
				policyDocInfo.setVersion(policyDocVo.getVersion());
				policyDocInfo.setCreateTime(DateUtil.addMinius(time, j));
				policyDocInfo.setUpdateTime(DateUtil.addMinius(time, j));
				policyDocInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
				policyDocInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
				policyDocInfo.setDeleteFlag(DeleteFlag.Default.getValue());
				policyDocumentsInfos.add(policyDocInfo);

				// 创建附件表
				AttachmentInfo attachmentInfo = new AttachmentInfo();
				attachmentInfo.setId(UUID.randomUUID().toString());
				attachmentInfo.setSourceId(policyDocInfo.getDocumentsId());
				// 拷贝文件
				String tempDir = policyDocVo.getAttachId();
				String mainDir = FilePathUtil.getMainPath(FilePathUtil.POLICY_PATH, tempDir);
				String rePath = fac.copyFile(tempDir, mainDir);

				attachmentInfo.setAttachId(rePath);
				attachmentInfo.setAttachName(policyDocVo.getFileName());
				// attachmentInfo.setVisitUrl(policyDocVo.getVisitUrl());
				attachmentInfo.setDeleteFlag(DeleteFlag.Default.getValue());
				attachmentInfos.add(attachmentInfo);
			}
		}
		if (attachmentInfos.size() != 0) {
			attachmentInfoMapper.insterBatchAttachmentInfo(attachmentInfos);
		}
		if (policyDocumentsInfos.size() != 0) {
			policyDocumentsInfoMapper.insterBatchPolicyDocInfo(policyDocumentsInfos);
		}

	}

	/**
	 * 处理visibility
	 * 
	 * @param id
	 * @param policyVisibility
	 */
	private void operationVisibility(String policyId, List<PolicyRoleRelationInfo> policyVisibilityList) {
		if (ListUtil.isEmpty(policyVisibilityList)) {
			return;
		}
		// 删除之前的关系
		policyRoleRelationInfoMapper.deleteByPolicyKey(policyId);

		// 更新角色对于PolicyId
		for (PolicyRoleRelationInfo policeRole : policyVisibilityList) {
			policeRole.setPolicyId(policyId);
		}
		policyRoleRelationInfoMapper.insterBatchVisiblityRelationInfo(policyVisibilityList);

	}

	/**
	 * 处理NQS记录
	 * 
	 * @param policyId
	 * @param nqsVoList
	 */
	private void operationNqs(String policyId, List<NqsVo> nqsVoList) {
		if (ListUtil.isEmpty(nqsVoList)) {
			return;
		}
		// 删除之前的关系
		nqsRelationInfoMapper.removeNqsRelationByObjId(policyId);

		List<NqsRelationInfo> nqsNewsInfoList = new ArrayList<NqsRelationInfo>();
		for (NqsVo nqs : nqsVoList) {
			NqsRelationInfo nqsNewsInfo = new NqsRelationInfo();
			nqsNewsInfo.setId(UUID.randomUUID().toString());
			nqsNewsInfo.setObjId(policyId);
			nqsNewsInfo.setNqsVersion(nqs.getVersion());
			nqsNewsInfo.setDeleteFlag(DeleteFlag.Default.getValue());
			nqsNewsInfo.setTag(nqs.getTag());
			nqsNewsInfoList.add(nqsNewsInfo);
		}
		nqsRelationInfoMapper.insterBatchNqsRelationInfo(nqsNewsInfoList);
	}

	/**
	 * 新增更新PolicyInfo
	 * 
	 * @param policyInfoVo
	 * @param currentUser
	 * @return
	 */
	private PolicyInfo operationPolicyInfo(PolicyInfoVo policyInfoVo, UserInfoVo currentUser) {
		PolicyInfo policyInfo = new PolicyInfo();
		Date time = new Date();
		if (StringUtil.isEmpty(policyInfoVo.getId())) {
			// 新增
			policyInfo.setId(UUID.randomUUID().toString());
			policyInfo.setPolicyName(policyInfoVo.getPolicyName());
			policyInfo.setCategoryId(policyInfoVo.getCategoryId());
			if (null != currentUser) {
				policyInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
			}
			policyInfo.setCreateTime(time);
			policyInfo.setDeleteFlag(DeleteFlag.Default.getValue());
			policyInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
			policyInfo.setUpdateTime(time);
			// 插入
			policyInfoMapper.insert(policyInfo);
		} else {
			// 编辑
			policyInfo = policyInfoMapper.selectByPrimaryKey(policyInfoVo.getId());
			policyInfo.setPolicyName(policyInfoVo.getPolicyName());
			policyInfo.setCategoryId(policyInfoVo.getCategoryId());
			policyInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
			policyInfo.setUpdateTime(time);
			// 更新
			policyInfoMapper.updateByPrimaryKey(policyInfo);
		}

		return policyInfo;
	}

	/**
	 * 保存和编辑policy进行数据合法性验证
	 * 
	 * @param policyInfoVo
	 * @return
	 */
	private ServiceResult<Object> validateAddOrUpdatePolicy(PolicyInfoVo policyInfoVo) {
		// Policy Name 不能为空 Category不能为空 Nqs不能为空 Visibility 不能为空
		if (StringUtil.isEmpty(policyInfoVo.getPolicyName())) {
			return failResult(getTipMsg("policy_name_empty"));
		}

		if (StringUtil.isEmpty(policyInfoVo.getCategoryId())) {
			return failResult(getTipMsg("policy_category_empty"));
		}

		if (null == policyInfoVo.getNqsInfo() || policyInfoVo.getNqsInfo().size() == 0) {
			return failResult(getTipMsg("policy_nqs_empty"));
		}

		if (null == policyInfoVo.getPolicyVisibility() || policyInfoVo.getPolicyVisibility().size() == 0) {
			return failResult(getTipMsg("policy_visibilisy_empty"));
		}

		// 附件不能为空
		if (null == policyInfoVo.getPolicyDocList() || policyInfoVo.getPolicyDocList().size() == 0) {
			return failResult(getTipMsg("policy_document_empty"));
		} else {
			int m = policyInfoVo.getPolicyDocList().size();
			int i = 0;

			for (i = 0; i < m; i++) {
				// 如果页面标记为delete则处理删除
				if (policyInfoVo.getPolicyDocList().get(i).getDeleteFlag() == 0) {
					break;
				}
			}
			if (i >= m) {
				return failResult(getTipMsg("policy_document_empty"));
			}

			for (i = 0; i < m; i++) {
				if (policyInfoVo.getPolicyDocList().get(i).getDeleteFlag() == 0) {
					for (int j = 0; j < m; j++) {
						if (policyInfoVo.getPolicyDocList().get(j).getDeleteFlag() == 0
								&& policyInfoVo.getPolicyDocList().get(j).getVersion().trim()
										.equals(policyInfoVo.getPolicyDocList().get(i).getVersion().trim())
								&& policyInfoVo.getPolicyDocList().get(j).getAttachId() != policyInfoVo.getPolicyDocList().get(i).getAttachId()) {
							return failResult(getTipMsg("policy_document_version_repeat"));
						}
					}
				}
			}
		}
		return successResult();
	}

}
