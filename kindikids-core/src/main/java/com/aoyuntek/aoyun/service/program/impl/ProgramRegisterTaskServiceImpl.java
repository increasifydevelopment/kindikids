package com.aoyuntek.aoyun.service.program.impl;

import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.condtion.WeekMenuCondition;
import com.aoyuntek.aoyun.dao.ClosurePeriodMapper;
import com.aoyuntek.aoyun.dao.RoomInfoMapper;
import com.aoyuntek.aoyun.dao.RoomTaskCheckMapper;
import com.aoyuntek.aoyun.dao.SigninInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.dao.WeekNutrionalMenuInfoMapper;
import com.aoyuntek.aoyun.dao.program.ProgramRegisterItemInfoMapper;
import com.aoyuntek.aoyun.dao.program.ProgramRegisterSleepInfoMapper;
import com.aoyuntek.aoyun.dao.program.ProgramRegisterTaskInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskInstanceInfoMapper;
import com.aoyuntek.aoyun.entity.po.RoomInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.po.WeekNutrionalMenuInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramRegisterItemInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramRegisterSleepInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramRegisterTaskInfo;
import com.aoyuntek.aoyun.entity.vo.ProgramNewsFeedVo;
import com.aoyuntek.aoyun.entity.vo.ProgramRegisterItemInfoVo;
import com.aoyuntek.aoyun.entity.vo.ProgramRegisterTaskInfoVo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.aoyun.entity.vo.SigninVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.ProgramState;
import com.aoyuntek.aoyun.enums.RegisterServings;
import com.aoyuntek.aoyun.enums.TaskType;
import com.aoyuntek.aoyun.enums.Week;
import com.aoyuntek.aoyun.service.attendance.IAttendanceCoreService;
import com.aoyuntek.aoyun.service.impl.BaseWebServiceImpl;
import com.aoyuntek.aoyun.service.program.IProgramLessonService;
import com.aoyuntek.aoyun.service.program.IProgramRegisterTaskService;
import com.aoyuntek.aoyun.service.task.ITaskInstance;
import com.aoyuntek.aoyun.uitl.DateUtil;
import com.aoyuntek.aoyun.uitl.MyListUtil;
import com.theone.common.util.ServiceResult;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

@Service
public class ProgramRegisterTaskServiceImpl extends BaseWebServiceImpl<ProgramRegisterTaskInfo, ProgramRegisterTaskInfoMapper> implements IProgramRegisterTaskService {

	public static Logger logger = Logger.getLogger(ProgramRegisterTaskServiceImpl.class);

	@Autowired
	private ProgramRegisterTaskInfoMapper registerTaskInfoMapper;
	@Autowired
	private ProgramRegisterItemInfoMapper registerItemInfoMapper;
	@Autowired
	private ProgramRegisterSleepInfoMapper registerSleepInfoMapper;
	@Autowired
	private RoomInfoMapper roomInfoMapper;
	@Autowired
	private ITaskInstance taskInstance;
	@Autowired
	private RoomInfoMapper roomMapper;
	@Autowired
	private IProgramLessonService lessonService;
	@Autowired
	private IAttendanceCoreService attendanceCoreService;
	@Autowired
	private SigninInfoMapper signinInfoMapper;
	@Autowired
	private TaskInstanceInfoMapper taskInstanceInfoMapper;
	@Autowired
	private UserInfoMapper userInfoMapper;
	@Autowired
	private WeekNutrionalMenuInfoMapper weekNutrionalMenuInfoMapper;
	@Autowired
	private RoomTaskCheckMapper roomTaskCheckMapper;
	@Autowired
	private ClosurePeriodMapper closurePeriodMapper;

	/**
	 * 新增/更新ProgramRegisterTask
	 */
	@Override
	public ServiceResult<Object> addOrUpdateRegister(ProgramRegisterTaskInfoVo registerTaskInfoVo, UserInfoVo currentUser) {
		log.info("addOrUpdateRegister|start");
		// 等待新增/更新的ProgramRegisterTaskInfo
		ProgramRegisterTaskInfo registerTaskInfo = registerTaskInfoVo.getProgramRegisterTaskInfo();
		// 等待新增/更新的List<ProgramRegisterItemInfoVo>
		List<ProgramRegisterItemInfoVo> registerItemList = registerTaskInfoVo.getRegisterItemList();
		// 定义返回值
		ProgramRegisterTaskInfoVo returnTaskInfoVo = new ProgramRegisterTaskInfoVo();
		String taskId = registerTaskInfo.getId();
		log.info("addOrUpdateRegister|taskId=" + taskId);
		// 新增
		if (StringUtil.isEmpty(taskId)) {
			taskId = UUID.randomUUID().toString();
			// 保存ProgramRegisterTaskInfo
			ProgramRegisterTaskInfo saveRegisterTaskInfo = saveRegisterTaskInfo(taskId, registerTaskInfo, currentUser);
			// 批量保存ProgramRegisterItemInfoVo
			List<ProgramRegisterItemInfoVo> saveRegisterItems = saveRegisterItems(taskId, registerItemList, currentUser, registerTaskInfo.getType());
			// 将新增的RegisterTask和RegisterItems放入返回值
			returnTaskInfoVo.setProgramRegisterTaskInfo(saveRegisterTaskInfo);
			returnTaskInfoVo.setRegisterItemList(saveRegisterItems);
		} else {
			// 更新ProgramRegisterTaskInfo
			ProgramRegisterTaskInfo updateRegisterTaskInfo = updateRegisterTaskInfo(registerTaskInfo, currentUser);
			// 批量更新ProgramRegisterItemInfoVo
			ServiceResult<Object> result = updateRegisterItems(taskId, registerItemList, currentUser, registerTaskInfo.getType());
			if (!result.isSuccess()) {
				return failResult(result.getMsg());
			}
			List<ProgramRegisterItemInfoVo> updateRegisterItems = (List<ProgramRegisterItemInfoVo>) result.getReturnObj();
			returnTaskInfoVo.setProgramRegisterTaskInfo(updateRegisterTaskInfo);
			returnTaskInfoVo.setRegisterItemList(updateRegisterItems);
		}
		if (registerTaskInfoVo.getIsSubmit() != null && registerTaskInfoVo.getIsSubmit()) {
			if (ListUtil.isEmpty(registerTaskInfoVo.getRegisterItemList())) {
				return failResult(getTipMsg("program.register.submit"));
			}
			// 验证
			for (ProgramRegisterItemInfoVo itemInfoVo : registerItemList) {
				ProgramRegisterItemInfo registerItemInfo = itemInfoVo.getProgramRegisterItemInfo();
				ServiceResult<Object> result = validateServing(registerItemInfo, itemInfoVo, registerTaskInfo.getType());
				if (!result.isSuccess()) {
					return failResult(result.getMsg());
				}
			}
			log.info("submit ");
			submitNewsFeed(registerTaskInfoVo, currentUser);
			registerTaskInfoMapper.updateStateByItem(ProgramState.Complete.getValue(), taskId);
			if (registerTaskInfo.getType() == TaskType.SunscreenRegister.getValue()) {
				// 将taskInstance记录更新为完成状态
				taskInstanceInfoMapper.updateStatus(taskId, ProgramState.Complete.getValue(), currentUser.getAccountInfo().getId(), new Date());
			}
		}
		log.info("addOrUpdateRegister|end");
		return successResult(getTipMsg("operation_success"), (Object) returnTaskInfoVo);
	}

	/**
	 * 
	 * @description send newsfeed
	 * @author gfwang
	 * @create 2016年9月25日下午8:50:07
	 * @version 1.0
	 * @param programNewsFeedVo
	 * @param registerTaskInfoVo
	 */
	private void submitNewsFeed(ProgramRegisterTaskInfoVo registerTaskInfoVo, UserInfoVo currentUser) {
		List<ProgramRegisterItemInfoVo> registerItemList = registerTaskInfoVo.getRegisterItemList();
		if (ListUtil.isEmpty(registerItemList)) {
			return;
		}
		ProgramRegisterTaskInfo programRegisterTaskInfo = registerTaskInfoVo.getProgramRegisterTaskInfo();
		Short taskType = programRegisterTaskInfo.getType();
		String roomId = programRegisterTaskInfo.getRoomId();
		String taskId = programRegisterTaskInfo.getId();

		log.info("taskId=" + taskId + ",roomId" + roomId + ",taskType" + taskType);
		List<ProgramRegisterItemInfoVo> list = new ArrayList<ProgramRegisterItemInfoVo>(registerItemList);
		// 一个小孩会有多个sleepRegister，分享newsfeed时要将多个合并成一个
		if (programRegisterTaskInfo.getType() == TaskType.SleepRegister.getValue()) {
			list = HandleRegisterItemList(registerItemList);
		}
		// 发送newsfeed，每个register中的每个小孩都发一次
		for (ProgramRegisterItemInfoVo item : list) {
			ProgramRegisterItemInfo programRegisterItemInfo = item.getProgramRegisterItemInfo();
			String newsFeedId = programRegisterItemInfo.getNewsfeedId();
			log.info("newsFeedId=" + newsFeedId);

			// 组装对象
			ProgramNewsFeedVo programNewsFeedVo = new ProgramNewsFeedVo();
			programNewsFeedVo.setTaskId(taskId);
			programNewsFeedVo.setNewsId(newsFeedId);
			programNewsFeedVo.setAccounts(Arrays.asList(new String[] { programRegisterItemInfo.getChildId() }));
			programNewsFeedVo.setTaskType(taskType);
			programNewsFeedVo.setRoomId(roomId);
			String content = getNewsContent(taskType, programRegisterTaskInfo.getDay(), item);
			// sleepRegister中的content已经处理好（同一小孩的多个合并成同一个）
			if (programRegisterTaskInfo.getType() == TaskType.SleepRegister.getValue()) {
				content = item.getContent();
			}
			programNewsFeedVo.setContent(content);
			programNewsFeedVo.setCurrtenUser(currentUser);

			// 开始发送newsfeed
			if (programRegisterTaskInfo.getType() == TaskType.SunscreenRegister.getValue()) {
				return;
			}
			ServiceResult<Object> result = lessonService.saveShareNewsfeed(programRegisterItemInfo.getId(), programNewsFeedVo, programRegisterItemInfo.getChildId());
			// 更新newsfeedId,
			if (programRegisterTaskInfo.getType() == TaskType.SleepRegister.getValue()) {
				for (ProgramRegisterItemInfoVo vo : registerItemList) {
					if (StringUtil.compare(vo.getProgramRegisterItemInfo().getChildId(), item.getProgramRegisterItemInfo().getChildId())) {
						vo.getProgramRegisterItemInfo().setNewsfeedId((String) result.getReturnObj());
					}
				}
			} else {
				programRegisterItemInfo.setNewsfeedId((String) result.getReturnObj());
			}
		}
	}

	/**
	 * 
	 * @description 同一个孩子的多个sleepRegister合并成一个
	 * @author mingwang
	 * @create 2017年1月5日下午3:28:07
	 * @param registerItemList
	 * @return
	 */
	private List<ProgramRegisterItemInfoVo> HandleRegisterItemList(List<ProgramRegisterItemInfoVo> registerItemList) {
		Set<String> set = new HashSet<String>();
		// 所有的小孩放入set中
		for (ProgramRegisterItemInfoVo item : registerItemList) {
			set.add(item.getProgramRegisterItemInfo().getChildId());
		}
		List<ProgramRegisterItemInfoVo> returnList = new ArrayList<ProgramRegisterItemInfoVo>();
		// 循环小孩set
		for (String childId : set) {
			ProgramRegisterItemInfoVo programRegisterItemInfoVo = null;
			String content = getSystemMsg("sleep_content");
			String newsfeedId = null;
			// 循环registerList，合并同一个小孩sleepRegister的content
			for (ProgramRegisterItemInfoVo item : registerItemList) {
				if (StringUtil.compare(childId, item.getProgramRegisterItemInfo().getChildId())) {
					programRegisterItemInfoVo = item;
					content += "<br/>" + getNewsContent(TaskType.SleepRegister.getValue(), null, item);
					programRegisterItemInfoVo.setContent(content);
					if (StringUtil.isNotEmpty(item.getProgramRegisterItemInfo().getNewsfeedId())) {
						newsfeedId = item.getProgramRegisterItemInfo().getNewsfeedId();
					}
				}
			}
			programRegisterItemInfoVo.getProgramRegisterItemInfo().setNewsfeedId(newsfeedId);
			returnList.add(programRegisterItemInfoVo);
		}
		return returnList;
	}

	private String dateStrFilter(Date day) {
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm a", Locale.ENGLISH);
		String dateStr = sdf.format(day);
		log.info("dateStrFilter|dateStr=" + dateStr);
		if (dateStr.contains("08:00")) {
			return getSystemMsg("time1_range");
		} else if (dateStr.contains("10:00")) {
			return getSystemMsg("time2_range");
		} else if (dateStr.contains("13:00")) {
			return getSystemMsg("time3_range");
		} else {
			return getSystemMsg("time4_range");
		}
	}

	private boolean isAbsent(short type, RegisterServings... serving) {
		for (RegisterServings item : serving) {
			if (type == item.getValue()) {
				return true;
			}
		}
		return false;
	}

	private String getNewsContent(Short taskType, Date day, ProgramRegisterItemInfoVo item) {
		TaskType type = TaskType.getType(taskType);
		Short serving = item.getProgramRegisterItemInfo().getServings();
		// 如果缺席，直接显示
		if (isAbsent(serving, RegisterServings.AbsentMeal, RegisterServings.AbsentNappy, RegisterServings.AbsentSleep, RegisterServings.AbsentSunscreen,
				RegisterServings.AbsentToilet)) {
			return getSystemMsg("AbsentRegister");
		}
		String childId = item.getProgramRegisterItemInfo().getChildId();
		String content = "";
		SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
		switch (type) {
		case MealRegister:
			String key = TaskType.MealRegister.getDesc().replace(" ", "") + serving;
			String tip1 = getSystemMsg(key);
			WeekNutrionalMenuInfo menuInfo = getMenu(childId, day);
			String tip2 = menuInfo == null ? "" : menuInfo.getLunch();
			String tip3 = menuInfo == null ? "" : menuInfo.getDessert();
			content = MessageFormat.format(getSystemMsg("meal_content"), tip1, tip2, tip3);
			break;
		case SleepRegister:
			if (serving.compareTo(RegisterServings.Rest.getValue()) == 0) {
				content = getSystemMsg("sleep_content_ret");
			} else {
				String startDate = sdf.format(item.getProgramRegisterSleepInfo().getSleepTime());
				String endDate = sdf.format(item.getProgramRegisterSleepInfo().getWokeupTime());
				content = MessageFormat.format(getSystemMsg("sleep_content_sleep"), startDate, endDate);
			}
			break;
		case NappyRegister:
			ProgramRegisterItemInfo nappyInfo = registerItemInfoMapper.selectByPrimaryKey(item.getProgramRegisterItemInfo().getId());
			String nappyChangeTime = coveDate(nappyInfo.getUpdateTime());
			String tag_nappy = TaskType.NappyRegister.getDesc().replace(" ", "") + serving;
			content = MessageFormat.format(getSystemMsg("nappy_content"), nappyChangeTime, getSystemMsg(tag_nappy));
			break;
		case ToiletRegister:
			ProgramRegisterItemInfo toiletInfo = registerItemInfoMapper.selectByPrimaryKey(item.getProgramRegisterItemInfo().getId());
			String toiletChangeTime = coveDate(toiletInfo.getUpdateTime());
			String tag_toilet = TaskType.ToiletRegister.getDesc().replace(" ", "") + serving;
			content = MessageFormat.format(getSystemMsg("toilet_content"), toiletChangeTime, getSystemMsg(tag_toilet));
			break;
		case SunscreenRegister:
			content = getSystemMsg("sunscreen.register");
			break;
		default:
			break;
		}
		return content;

	}

	private String coveDate(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
		String format = sdf.format(date);
		return format;
	}

	private WeekNutrionalMenuInfo getMenu(String childId, Date date) {
		// 菜单周期,4周轮换
		int MENU_CYCLE = 4;
		// 菜单开始日期
		Date startDate = DateUtil.parseString(getSystemMsg("menu_begin_date"));
		int menuDays = com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(startDate, date);
		menuDays++;
		// 总的周数，一周7天
		short weekTotal = (short) (menuDays % 7 == 0 ? menuDays / 7 : menuDays / 7 + 1);
		// 第几周
		short weekNum = (short) (weekTotal % MENU_CYCLE == 0 ? MENU_CYCLE : weekTotal % MENU_CYCLE);
		// 星期几
		short weekToday = (short) (menuDays % 7 == 0 ? 7 : menuDays % 7);
		UserInfo userInfo = userInfoMapper.getUserInfoByAccountId(childId);
		WeekMenuCondition condition = new WeekMenuCondition(userInfo.getCentersId(), weekNum, weekToday);
		condition.setRoomId(userInfo.getRoomId());
		WeekNutrionalMenuInfo menuInfo = weekNutrionalMenuInfoMapper.getTodayMenu(condition);
		return menuInfo;
	}

	public static void main(String[] args) {
		// 菜单周期,4周轮换
		int MENU_CYCLE = 4;
		Date start = DateUtil.parseString("2015-09-14");
		Date end = DateUtil.parseString("2018-03-16");
		int menuDays = com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(start, end);
		menuDays++;
		// 总的周数，一周7天
		short weekTotal = (short) (menuDays % 7 == 0 ? menuDays / 7 : menuDays / 7 + 1);
		// 第几周
		short weekNum = (short) (weekTotal % MENU_CYCLE == 0 ? MENU_CYCLE : weekTotal % MENU_CYCLE);
		// 星期几
		short weekToday = (short) (menuDays % 7 == 0 ? 7 : menuDays % 7);
		System.err.println(weekNum);
		System.err.println(weekToday);
	}

	/**
	 * 
	 * @description 更新registerItems
	 * @author mingwang
	 * @create 2017年2月4日下午3:20:41
	 * @version 1.0
	 * @param taskId
	 * @param registerItemList
	 * @param currentUser
	 * @param taskType
	 * @return
	 */
	private ServiceResult<Object> updateRegisterItems(String taskId, List<ProgramRegisterItemInfoVo> registerItemList, UserInfoVo currentUser, short taskType) {
		// registerItems的一些验证
		ServiceResult<Object> validateResult = validateRegisterItems(registerItemList, taskType);
		if (!validateResult.isSuccess()) {
			return failResult(validateResult.getMsg());
		}
		registerItemInfoMapper.updateRemoveByTaskId(taskId);
		if (ListUtil.isNotEmpty(registerItemList)) {
			saveRegisterItems(taskId, registerItemList, currentUser, taskType);
		}
		return successResult((Object) registerItemList);
	}

	/**
	 * 
	 * @description 批量更新ProgramRegisterItemInfoVo
	 * @author mingwang
	 * @create 2016年9月22日下午10:06:31
	 * @version 1.0
	 * @param taskId
	 * @param registerItemList
	 * @param currentUser
	 */
	private ServiceResult<Object> updateRegisterItem(String taskId, List<ProgramRegisterItemInfoVo> registerItemList, UserInfoVo currentUser, short taskType) {
		log.info("addOrUpdateRegister|updateRegisterItems|start|taskId=" + taskId);
		// registerItems的一些验证
		ServiceResult<Object> validateResult = validateRegisterItems(registerItemList, taskType);
		if (!validateResult.isSuccess()) {
			return failResult(validateResult.getMsg());
		}
		// 待更新的ProgramRegisterItemInfo(item)
		List<ProgramRegisterItemInfo> registerItems_update = new ArrayList<ProgramRegisterItemInfo>();
		// 待更新的ProgramRegisterSleepInfo(sleep)
		List<ProgramRegisterSleepInfo> registerSleeps_update = new ArrayList<ProgramRegisterSleepInfo>();
		// 更新的ProgramRegisterItemInfo中sleep的增加(sleep)
		List<ProgramRegisterSleepInfo> registerSleep_add = new ArrayList<ProgramRegisterSleepInfo>();
		// 更新的ProgramRegisterItemInfo中sleep的删除(sleep)
		List<String> registerSleep_remove = new ArrayList<String>();
		// 新增的ProgramRegisterItemInfoVo(item)
		List<ProgramRegisterItemInfoVo> registerItemList_newAdd = new ArrayList<ProgramRegisterItemInfoVo>();
		// 删除的ProgramRegisterItemInfo(item)
		List<String> registerItems_remove = new ArrayList<String>();
		// 从数据库获取ProgramRegisterItemInfo集合(db_item)
		List<ProgramRegisterItemInfo> dbRegisterItems = registerItemInfoMapper.getRegisterItemsByTaskId(taskId);
		// 待删除的item，复制从数据库获取的item
		for (ProgramRegisterItemInfo programRegisterItemInfo : dbRegisterItems) {
			registerItems_remove.add(programRegisterItemInfo.getId());
		}
		// 将新增的ProgramRegisterItemInfo和ProgramRegisterSleepInfo分离出来放入对应的增、删、改集合
		for (ProgramRegisterItemInfoVo registerItemInfoVo : registerItemList) {
			ProgramRegisterItemInfo registerItemInfo = registerItemInfoVo.getProgramRegisterItemInfo();
			// serving的一些验证（包括serving必填和sleep时间必填）
			// ServiceResult<Object> result = validateServing(registerItemInfo,
			// registerItemInfoVo, taskType);
			// if (!result.isSuccess()) {
			// return failResult(result.getMsg());
			// }
			// 如果ProgramRegisterItemInfo中id为空，放入到新增的集合中
			if (StringUtil.isEmpty(registerItemInfo.getId())) {
				// 新增registerItemInfoVo
				registerItemList_newAdd.add(registerItemInfoVo);
			} else {
				// 循环数据库中items
				for (ProgramRegisterItemInfo dbRegisterItemInfo : dbRegisterItems) {
					// 待更新的item和数据库中item的Id相同时执行更新操作
					if (StringUtil.compare(registerItemInfo.getId(), dbRegisterItemInfo.getId())) {
						short sleep = RegisterServings.Sleep.getValue();
						short serving = registerItemInfo.getServings();
						// 数据库中的serving不为空时
						if (dbRegisterItemInfo.getServings() != null) {
							short db_serving = dbRegisterItemInfo.getServings();
							// 非sleep更新为sleep
							if (sleep == serving && sleep != db_serving) {
								registerItemInfoVo.getProgramRegisterSleepInfo().setRegisterId(registerItemInfo.getId());
								registerSleep_add.add(registerItemInfoVo.getProgramRegisterSleepInfo());
								// sleep更新为非sleep
							} else if (sleep != serving && sleep == db_serving) {
								registerSleep_remove.add(dbRegisterItemInfo.getId());
								// sleep更新
							} else if (sleep == serving && sleep == db_serving) {
								registerSleeps_update.add(registerItemInfoVo.getProgramRegisterSleepInfo());
							}
							// 数据库中serving为空时
						} else {
							// 更新为sleep
							if (sleep == registerItemInfo.getServings()) {
								registerItemInfoVo.getProgramRegisterSleepInfo().setRegisterId(registerItemInfo.getId());
								registerSleep_add.add(registerItemInfoVo.getProgramRegisterSleepInfo());
							}
						}
						dbRegisterItemInfo.setChildId(registerItemInfo.getChildId());
						dbRegisterItemInfo.setServings(registerItemInfo.getServings());
						if (taskType != TaskType.NappyRegister.getValue() && taskType != TaskType.ToiletRegister.getValue()) {
							dbRegisterItemInfo.setUpdateTime(new Date());
						} else {
							if (registerItemInfoVo.getChangeTime() != null) {
								dbRegisterItemInfo.setUpdateTime(registerItemInfoVo.getChangeTime());
							}
						}
						dbRegisterItemInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
						// 将待更新的ProgramRegisterItemInfo放入待更新的集合中
						registerItems_update.add(dbRegisterItemInfo);
						// 将待更新的ProgramRegisterItemInfo从待删除集合中移除
						registerItems_remove.remove(dbRegisterItemInfo.getId());
					}
				}
			}
		}
		if (!ListUtil.isEmpty(registerItemList_newAdd)) {
			// 批量保存ProgramRegisterItemInfoVo
			saveRegisterItems(taskId, registerItemList_newAdd, currentUser, taskType);
		}
		if (!ListUtil.isEmpty(registerItems_remove)) {
			// 删除items
			removeRegisterItems(registerItems_remove);
		}
		if (!ListUtil.isEmpty(registerItems_update)) {
			// 批量更新ProgramRegisterItemInfo
			registerItemInfoMapper.batchUpdateRegisterItems(registerItems_update);
		}
		if (!ListUtil.isEmpty(registerSleep_add)) {
			// 批量保存sleep
			registerSleepInfoMapper.batchInsertRegisterSleeps(registerSleep_add);
		}
		if (!ListUtil.isEmpty(registerSleep_remove)) {
			// 删除sleep
			registerSleepInfoMapper.batchDeleteById(registerSleep_remove);
		}
		if (!ListUtil.isEmpty(registerSleeps_update)) {
			// 批量更新ProgramRegisterSleepInfo
			registerSleepInfoMapper.batchUpdateRegisterSleeps(registerSleeps_update);
		}
		log.info("addOrUpdateRegister|updateRegisterItems|end");
		return successResult((Object) registerItemList);
	}

	/**
	 * 
	 * @description registerItems的一些验证
	 * @author mingwang
	 * @create 2016年10月12日上午10:29:55
	 * @version 1.0
	 * @param registerItemList
	 * @return
	 */
	private ServiceResult<Object> validateRegisterItems(List<ProgramRegisterItemInfoVo> registerItemList, short taskType) {
		List<String> childIds = new ArrayList<String>();
		for (ProgramRegisterItemInfoVo programRegisterItemInfoVo : registerItemList) {
			ProgramRegisterItemInfo itemInfo = programRegisterItemInfoVo.getProgramRegisterItemInfo();
			// 小孩必选
			if (StringUtil.isEmpty(itemInfo.getChildId())) {
				return failResult(getTipMsg("program.child.empty"));
			}
			childIds.add(itemInfo.getChildId());
		}
		// 判断集合中元素是否重复，重复返回此元素，不重复返回null
		String hasSame = hasSame(childIds);
		if (StringUtil.isNotEmpty(hasSame) && taskType != TaskType.SleepRegister.getValue()) {
			// UserInfo userInfoByAccountId =
			// userInfoMapper.getUserInfoByAccountId(hasSame);
			// String firstName =
			// StringUtil.isEmpty(userInfoByAccountId.getFirstName()) ? "" :
			// userInfoByAccountId.getFirstName();
			// String middleName =
			// StringUtil.isEmpty(userInfoByAccountId.getMiddleName()) ? "" :
			// " " + userInfoByAccountId.getMiddleName();
			// String lastName =
			// StringUtil.isEmpty(userInfoByAccountId.getLastName()) ? "" : " "
			// + userInfoByAccountId.getLastName();
			// String name = firstName + middleName + lastName;
			// return failResult(name + " " +
			// getTipMsg("program.register.child.repeat"));
			return failResult(getTipMsg("program.register.child.repeat"));
		}
		return successResult();
	}

	/**
	 * 判断集合中元素是否重复，并返回这个重复的元素
	 */
	private String hasSame(List<String> list) {
		for (int i = 0; i < list.size() - 1; i++) {
			for (int j = i + 1; j < list.size(); j++) {
				if (StringUtil.compare(list.get(i), list.get(j))) {
					return list.get(i);
				}
			}
		}
		return null;
	}

	/**
	 * 
	 * @description serving验证
	 * @author mingwang
	 * @create 2016年10月9日下午2:13:16
	 * @version 1.0
	 * @param registerItemInfo
	 * @param registerItemInfoVo
	 * @param taskType
	 * @return
	 */
	private ServiceResult<Object> validateServing(ProgramRegisterItemInfo registerItemInfo, ProgramRegisterItemInfoVo registerItemInfoVo, short taskType) {
		// serving必填，不填返回错误信息
		if (registerItemInfo.getServings() == null) {
			if (taskType == TaskType.MealRegister.getValue()) {
				return failResult(getTipMsg("program.register.meal.empty"));
			}
			if (taskType == TaskType.SleepRegister.getValue() || taskType == TaskType.NappyRegister.getValue() || taskType == TaskType.ToiletRegister.getValue()) {
				return failResult(getTipMsg("program.register.nappyTolietSleep.empty"));
			}
			if (taskType == TaskType.SunscreenRegister.getValue()) {
				return failResult(getTipMsg("program.register.sunscreen.empty"));
			}
		}
		// 时间判空，返回错误提示
		if (registerItemInfo.getServings() == RegisterServings.Sleep.getValue()) {
			if (registerItemInfoVo.getProgramRegisterSleepInfo() == null
					|| (registerItemInfoVo.getProgramRegisterSleepInfo() != null && (registerItemInfoVo.getProgramRegisterSleepInfo().getSleepTime() == null
							|| registerItemInfoVo.getProgramRegisterSleepInfo().getWokeupTime() == null))) {
				return failResult(getTipMsg("program.register.SleepTime.empty"));
			}
		}
		return successResult();
	}

	/**
	 * 
	 * @description 删除
	 * @author mingwang
	 * @create 2016年9月23日下午5:23:14
	 * @version 1.0
	 * @param registerItems_remove
	 */
	private void removeRegisterItems(List<String> registerItems_remove) {
		List<String> remove_ids = new ArrayList<String>();
		for (String id : registerItems_remove) {
			ProgramRegisterItemInfo selectByPrimaryKey = registerItemInfoMapper.selectByPrimaryKey(id);
			if (selectByPrimaryKey.getServings() != null && RegisterServings.Sleep.getValue() == selectByPrimaryKey.getServings()) {
				remove_ids.add(id);
			}
		}
		// 批量删除RegisterItems(修改delete_flag状态)
		int deleteItemCount = registerItemInfoMapper.batchDeleteById(registerItems_remove);
		log.info("deleteItemCount=" + deleteItemCount);
		if (ListUtil.isNotEmpty(remove_ids)) {
			// 批量删除RegisterSleep（直接删除）
			int deleteSleepCount = registerSleepInfoMapper.batchDeleteById(remove_ids);
			log.info("deleteSleepCount=" + deleteSleepCount);
		}
	}

	/**
	 * 
	 * @description 更新ProgramRegisterTaskInfo
	 * @author mingwang
	 * @create 2016年9月22日下午10:06:07
	 * @version 1.0
	 * @param registerTaskInfo
	 * @param currentUser
	 */
	private ProgramRegisterTaskInfo updateRegisterTaskInfo(ProgramRegisterTaskInfo registerTaskInfo, UserInfoVo currentUser) {
		log.info("addOrUpdateRegister|updateRegisterTaskInfo|start");
		ProgramRegisterTaskInfo dbRegisterTaskInfo = registerTaskInfoMapper.selectByPrimaryKey(registerTaskInfo.getId());
		dbRegisterTaskInfo.setUpdateTime(new Date());
		dbRegisterTaskInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
		registerTaskInfoMapper.updateByPrimaryKeySelective(dbRegisterTaskInfo);
		log.info("addOrUpdateRegister|updateRegisterTaskInfo|end");
		return dbRegisterTaskInfo;
	}

	/**
	 * 
	 * @description 批量保存ProgramRegisterItemInfoVo
	 * @author mingwang
	 * @create 2016年9月22日下午8:23:17
	 * @version 1.0
	 * @param registerId
	 * @param registerItemList
	 */
	private List<ProgramRegisterItemInfoVo> saveRegisterItems(String taskId, List<ProgramRegisterItemInfoVo> registerItemList, UserInfoVo currentUser, short taskType) {
		log.info("addOrUpdateRegister|saveRegisterItems|start|taskId=" + taskId);
		// 待插入数据库的ProgramRegisterItemInfo
		List<ProgramRegisterItemInfo> registerItems = new ArrayList<ProgramRegisterItemInfo>();
		// 待插入数据库的ProgramRegisterSleepInfo
		List<ProgramRegisterSleepInfo> registerSleeps = new ArrayList<ProgramRegisterSleepInfo>();
		for (ProgramRegisterItemInfoVo registerItemInfoVo : registerItemList) {
			String registerId = UUID.randomUUID().toString();
			ProgramRegisterItemInfo registerItemInfo = registerItemInfoVo.getProgramRegisterItemInfo();
			registerItemInfo.setId(registerId);
			registerItemInfo.setTaskId(taskId);
			registerItemInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
			// nappyRegister和toiletRegister的更新时间设置为鼠标点击选项（前端radio）时的时间
			if (taskType == TaskType.NappyRegister.getValue() || taskType == TaskType.ToiletRegister.getValue()) {
				if (registerItemInfoVo.getChangeTime() != null) {
					registerItemInfo.setUpdateTime(registerItemInfoVo.getChangeTime());
				}
			} else {
				registerItemInfo.setUpdateTime(new Date());
			}
			registerItemInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
			registerItemInfo.setDeleteFlag(DeleteFlag.Default.getValue());
			// 将初始化后的ProgramRegisterItemInfo放入集合中
			registerItems.add(registerItemInfo);
			if (registerItemInfo.getServings() != null && RegisterServings.Sleep.getValue() == registerItemInfo.getServings()) {
				if (registerItemInfoVo.getProgramRegisterSleepInfo() == null) {
					continue;
				}
				ProgramRegisterSleepInfo registerSleepInfo = registerItemInfoVo.getProgramRegisterSleepInfo();
				registerSleepInfo.setRegisterId(registerId);
				// 将初始化后的ProgramRegisterSleepInfo放入集合中
				registerSleeps.add(registerSleepInfo);
			}
		}
		// 批量插入ProgramRegisterItemInfo
		registerItemInfoMapper.batchInsertRegisterItems(registerItems);
		if (!ListUtil.isEmpty(registerSleeps)) {
			// 批量插入ProgramRegisterSleepInfo
			registerSleepInfoMapper.batchInsertRegisterSleeps(registerSleeps);
		}
		log.info("addOrUpdateRegister|saveRegisterItems|end");
		return registerItemList;
	}

	/**
	 * 
	 * @description 保存ProgramRegisterTaskInfo
	 * @author mingwang
	 * @create 2016年9月22日下午8:10:10
	 * @version 1.0
	 * @param registerId
	 * @param registerTaskInfo
	 * @param currentUser
	 */
	private ProgramRegisterTaskInfo saveRegisterTaskInfo(String registerId, ProgramRegisterTaskInfo registerTaskInfo, UserInfoVo currentUser) {
		log.info("addOrUpdateRegister|saveRegisterTaskInfo|start|registerId=" + registerId);
		registerTaskInfo.setId(registerId);
		registerTaskInfo.setState(ProgramState.Pending.getValue());
		registerTaskInfo.setCenterId(roomInfoMapper.selectCenterIdByRoom(registerTaskInfo.getRoomId()));
		Date date = new Date();
		registerTaskInfo.setDay(date);
		registerTaskInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
		registerTaskInfo.setUpdateTime(date);
		registerTaskInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
		registerTaskInfo.setDeleteFlag(DeleteFlag.Default.getValue());
		int insertSelective = registerTaskInfoMapper.insertSelective(registerTaskInfo);
		// 保存列表记录
		taskInstance.addTaskInstance(getTaskType(registerTaskInfo.getType()), registerTaskInfo.getCenterId(), registerTaskInfo.getRoomId(), null,
				registerTaskInfo.getId(), registerTaskInfo.getCreateAccountId(), date);
		log.info("addOrUpdateRegister|saveRegisterTaskInfo|end|insertSelective=" + insertSelective);
		return registerTaskInfo;
	}

	/**
	 * 获取ProgramRegisterTaskInfo
	 */
	@Override
	public ServiceResult<Object> getRegisterTaskInfo(String id) {
		log.info("getRegisterTaskInfo|start|id=" + id);
		// 定义返回的对象
		ProgramRegisterTaskInfoVo registerTaskInfoVo = new ProgramRegisterTaskInfoVo();
		ProgramRegisterTaskInfo programRegisterTaskInfo = new ProgramRegisterTaskInfo();
		// 如果id为空，返回一个空对象
		if (StringUtil.isEmpty(id)) {
			List<ProgramRegisterItemInfoVo> registerItemList = new ArrayList<ProgramRegisterItemInfoVo>();
			registerTaskInfoVo.setProgramRegisterTaskInfo(programRegisterTaskInfo);
			registerTaskInfoVo.setRegisterItemList(registerItemList);
		} else {
			// 数据库中获取programRegisterTaskInfo
			programRegisterTaskInfo = registerTaskInfoMapper.selectByPrimaryKey(id);
			// 数据库中获取ProgramRegisterItemInfoVo
			List<ProgramRegisterItemInfoVo> registerItemList = registerItemInfoMapper.getRegisterItems(id);
			// ProgramRegisterItemInfoVo中小孩设置Id
			for (ProgramRegisterItemInfoVo registerItem : registerItemList) {
				registerItem.getProgramRegisterItemUsers().get(0).setId(registerItem.getProgramRegisterItemInfo().getChildId());
			}
			registerTaskInfoVo.setProgramRegisterTaskInfo(programRegisterTaskInfo);
			registerTaskInfoVo.setRegisterItemList(registerItemList);
		}
		log.info("getRegisterTaskInfo|end");
		return successResult((Object) registerTaskInfoVo);
	}

	/**
	 * @description 通过value获取枚举对象
	 * @author mingwang
	 * @create 2016年9月23日下午3:52:26
	 * @version 1.0
	 * @param type
	 * @return
	 */
	public TaskType getTaskType(short type) {
		for (TaskType t : TaskType.values()) {
			if (t.getValue() == type) {
				return t;
			}
		}
		return null;
	}

	/**
	 * 获取当前room签到的所有小孩
	 */
	@Override
	public ServiceResult<Object> getSignChilds(String roomId, Date date, String params, Short type) {
		String centerId = roomInfoMapper.selectCenterIdByRoom(roomId);
		short weekNum = getDayOfWeek(date);
		// 获取今天应到小孩
		List<SigninVo> todaySignChilds = new ArrayList<SigninVo>();
		// 获取今天实到小孩
		List<SelecterPo> alityToChilds = new ArrayList<SelecterPo>();
		// 如果为周末则不执行
		if (weekNum != Week.Saturday.getValue() && weekNum != Week.Sunday.getValue()) {
			todaySignChilds = attendanceCoreService.getTodaySignChilds(centerId, roomId, getDayOfWeek(date), date);
			alityToChilds = signinInfoMapper.getTodayInChild(date, roomId);
		}
		List<SelecterPo> toDayIn = new ArrayList<SelecterPo>();
		SimpleDateFormat sdf = new SimpleDateFormat(DateUtil.yyyyMMdd);
		try {
			Date parse = sdf.parse(sdf.format(new Date()));
			if (date.before(parse)) {
				toDayIn = alityToChilds;
			} else {
				toDayIn = convertObj(todaySignChilds, alityToChilds);
			}
		} catch (ParseException e) {
			log.info("getSignChilds|date parse" + e);
		}
		// 依据toilet和nappy筛选小孩(type是小孩背景信息中的wayToilet)
		if (type != null && (type == TaskType.ToiletRegister.getValue() || type == TaskType.NappyRegister.getValue())) {
			List<SelecterPo> filterToDayIn = filterChilds(toDayIn, type);
			return successResult((Object) filterToDayIn);
		}
		if (StringUtil.isEmpty(params)) {
			return successResult((Object) toDayIn);
		}
		Iterator<SelecterPo> it = toDayIn.iterator();
		while (it.hasNext()) {
			SelecterPo select = it.next();
			String name = select.getText().toLowerCase();
			name = StringUtil.isNotEmpty(name) ? name.toLowerCase() : name;
			params = StringUtil.isNotEmpty(params) ? params.toLowerCase() : params;
			if (!name.contains(params)) {
				it.remove();
			}
		}
		return successResult((Object) toDayIn);

	}

	/**
	 * 
	 * @description 依据toilet和nappy筛选小孩
	 * @author mingwang
	 * @create 2016年10月20日下午5:41:22
	 * @version 1.0
	 * @param toDayIn
	 * @param taskType
	 * @return
	 */
	private List<SelecterPo> filterChilds(List<SelecterPo> toDayIn, short type) {
		List<SelecterPo> getChildByWayTlilet = userInfoMapper.getChildByWayTlilet(getWayToiletType(type));
		// 两个集合的交集（intersection交集）
		return ListUtil.intersection(toDayIn, getChildByWayTlilet);
	}

	/**
	 * 
	 * @description 获取date是当前周的第几天
	 * @author mingwang
	 * @create 2016年9月28日上午9:04:36
	 * @version 1.0
	 * @param date
	 * @return
	 */
	private short getDayOfWeek(Date date) {
		if (DateUtil.getDayOfWeek(date) == 1) {
			return Week.Sunday.getValue();
		}
		return (short) (DateUtil.getDayOfWeek(date) - 1);
	}

	/**
	 * 
	 * @description
	 * @author gfwang
	 * @create 2016年9月26日下午10:04:25
	 * @version 1.0
	 * @param todaySignChilds
	 *            今天应到的小孩
	 * @param toDayIn
	 *            今天实到的小孩
	 * @return
	 */
	private List<SelecterPo> convertObj(List<SigninVo> todaySignChilds, List<SelecterPo> toDayIn) {
		Set<SelecterPo> set = new LinkedHashSet<SelecterPo>();
		set.addAll(toDayIn);
		for (SigninVo sign : todaySignChilds) {
			String accountId = sign.getAccountId();
			String name = sign.getName();
			String avater = sign.getImg();
			String personColor = sign.getPersonColor();
			set.add(new SelecterPo(accountId, name, avater, personColor));
		}
		return new MyListUtil<SelecterPo>().set2List(set);
	}

	/**
	 * 自动生成定时任务Register
	 */
	@Override
	public void createTimingTask(Date date, short taskType) {
		createRegisterTask(date, taskType);
		createRegisterItems(date, taskType);
	}

	/**
	 * 自动生成定时任务Register
	 */
	public void createRegisterTask(Date date, short taskType) {
		// 周末不发送定时任务
		if (DateUtil.getDayOfWeek(date) == 7 || DateUtil.getDayOfWeek(date) == 1) {
			return;
		}
		// 生成ToiletRegister、NappyRegister的四个时间点
		List<Date> dates = convertDate(date);
		List<ProgramRegisterTaskInfo> taskInfos = new ArrayList<ProgramRegisterTaskInfo>();
		List<String> roomIds = roomMapper.getRoomIds();
		ProgramRegisterTaskInfo registerTaskInfo = null;
		// 循环所有room，生成register
		for (String id : roomIds) {
			// 关园期间不生成
			RoomInfo roomInfo = roomInfoMapper.selectByPrimaryKey(id);
			int c = closurePeriodMapper.getCountByCentreIdDate(roomInfo.getCentersId(), date);
			if (c > 0) {
				continue;
			}
			// 查询每个Room是否可生成该task
			int i = roomTaskCheckMapper.getTaskCountByRoomIdTaskType(id, taskType);
			if (i == 0) {
				continue;
			}
			int count = registerTaskInfoMapper.getCountByRoomAndType(id, taskType, date);
			// 如果当天已经生成，则不需要再生成
			if (taskType == TaskType.ToiletRegister.getValue() || taskType == TaskType.NappyRegister.getValue()) {
				if (count >= 4) {
					continue;
				}
			} else {
				if (count >= 1) {
					continue;
				}
			}
			// 如果是ToiletRegister和NappyRegister，分别在四个时间点生成register，其他的生成一条register
			if (taskType == TaskType.ToiletRegister.getValue() || taskType == TaskType.NappyRegister.getValue()) {
				for (Date date_TN : dates) {
					ProgramRegisterTaskInfo initRegisterTaskInfo = initRegisterTaskInfo(registerTaskInfo, id, taskType, date_TN);
					taskInfos.add(initRegisterTaskInfo);
					// 保存列表记录
					taskInstance.addTaskInstance(getTaskType(taskType), initRegisterTaskInfo.getCenterId(), initRegisterTaskInfo.getRoomId(), null,
							initRegisterTaskInfo.getId(), initRegisterTaskInfo.getCreateAccountId(), date_TN);
				}
			} else {
				ProgramRegisterTaskInfo initRegisterTaskInfo = initRegisterTaskInfo(registerTaskInfo, id, taskType, date);
				taskInfos.add(initRegisterTaskInfo);
				// 保存列表记录
				taskInstance.addTaskInstance(getTaskType(taskType), initRegisterTaskInfo.getCenterId(), initRegisterTaskInfo.getRoomId(), null,
						initRegisterTaskInfo.getId(), initRegisterTaskInfo.getCreateAccountId(), date);
			}
		}
		// 插入数据库
		if (!ListUtil.isEmpty(taskInfos)) {
			int insertSelective = registerTaskInfoMapper.batchInsert(taskInfos);
			log.info("creatTimingTask|insertSelective=" + insertSelective);
		}
	}

	/**
	 * 
	 * @description 时间转换(根据传入的时间转换成ToiletRegister，NappyRegister需要的四个时间点) 8:00
	 *              10：00 13:00 15:30
	 * @author mingwang
	 * @create 2016年10月10日下午6:15:39
	 * @version 1.0
	 */
	private List<Date> convertDate(Date date) {
		List<Date> dates = new ArrayList<Date>();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat sdf_add = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		String format = sdf.format(date);
		try {
			dates.add(sdf_add.parse(format + " " + getSystemMsg("time1")));
			dates.add(sdf_add.parse(format + " " + getSystemMsg("time2")));
			dates.add(sdf_add.parse(format + " " + getSystemMsg("time3")));
			dates.add(sdf_add.parse(format + " " + getSystemMsg("time4")));
		} catch (ParseException e) {
			log.error("convertDate", e);
			e.printStackTrace();
		}
		return dates;
	}

	/**
	 * 
	 * @description 初始化ProgramRegisterTaskInfo
	 * @author mingwang
	 * @create 2016年10月10日下午6:04:53
	 * @version 1.0
	 * @param registerTaskInfo
	 * @param id
	 * @param taskType
	 * @param date
	 * @return
	 */
	private ProgramRegisterTaskInfo initRegisterTaskInfo(ProgramRegisterTaskInfo registerTaskInfo, String id, short taskType, Date date) {
		// 初始化ProgramRegisterTaskInfo信息
		registerTaskInfo = new ProgramRegisterTaskInfo();
		registerTaskInfo.setId(UUID.randomUUID().toString());
		registerTaskInfo.setState(ProgramState.Pending.getValue());
		String centerId = roomInfoMapper.selectCenterIdByRoom(id);
		registerTaskInfo.setCenterId(centerId);
		registerTaskInfo.setRoomId(id);
		registerTaskInfo.setType(taskType);
		registerTaskInfo.setDay(date);
		registerTaskInfo.setUpdateTime(date);
		registerTaskInfo.setDeleteFlag(DeleteFlag.Default.getValue());
		return registerTaskInfo;
	}

	/**
	 * 自动生成定时任务RegisterItems
	 */
	public void createRegisterItems(Date date, short taskType) {
		// 周末不发送定时任务
		if (DateUtil.getDayOfWeek(date) == 7 || DateUtil.getDayOfWeek(date) == 1) {
			return;
		}
		List<Date> dates = convertDate(date);
		List<ProgramRegisterItemInfo> registerItemInfos = new ArrayList<ProgramRegisterItemInfo>();
		List<String> roomIds = roomMapper.getRoomIds();
		List<ProgramRegisterItemInfo> RegisterItemInfoList = null;
		// 循环所有room，生成registerItems
		for (String roomId : roomIds) {
			int i = roomTaskCheckMapper.getTaskCountByRoomIdTaskType(roomId, taskType);
			if (i == 0) {
				continue;
			}
			if (taskType == TaskType.ToiletRegister.getValue() || taskType == TaskType.NappyRegister.getValue()) {
				for (Date date_TN : dates) {
					String taskId = registerTaskInfoMapper.getIdByRoomAndType(roomId, taskType, date_TN);
					RegisterItemInfoList = initRegisterItemInfo(registerItemInfos, roomId, taskType, date_TN, taskId);
				}
			} else {
				String taskId = registerTaskInfoMapper.getIdByRoomAndType(roomId, taskType, date);
				RegisterItemInfoList = initRegisterItemInfo(registerItemInfos, roomId, taskType, date, taskId);
			}
		}
		if (!ListUtil.isEmpty(RegisterItemInfoList)) {
			registerItemInfoMapper.batchInsertRegisterItems(RegisterItemInfoList);
		}
	}

	/**
	 * 初始化ItemInfo
	 */
	private List<ProgramRegisterItemInfo> initRegisterItemInfo(List<ProgramRegisterItemInfo> registerItemInfos, String roomId, short taskType, Date date, String taskId) {
		// 数据库中已经初始化过的小孩Id
		List<String> childIds = registerItemInfoMapper.getChildIdByTaskId(taskId);
		// 获取已签到的小孩
		ServiceResult<Object> result = getSignChilds(roomId, date, "", taskType);
		List<SelecterPo> todayInChilds = (List<SelecterPo>) result.getReturnObj();
		for (SelecterPo child : todayInChilds) {
			// 如果该小孩已经生成过记录，continue
			if (isContainChildId(childIds, child.getId())) {
				continue;
			}
			ProgramRegisterItemInfo registerItemInfo = new ProgramRegisterItemInfo();
			registerItemInfo.setId(UUID.randomUUID().toString());
			registerItemInfo.setTaskId(taskId);
			registerItemInfo.setChildId(child.getId());
			registerItemInfo.setUpdateTime(date);
			registerItemInfo.setDeleteFlag(DeleteFlag.Default.getValue());
			registerItemInfos.add(registerItemInfo);
		}
		return registerItemInfos;
	}

	/**
	 * 
	 * @description 根据taskType获取WayToilet
	 * @author mingwang
	 * @create 2016年10月20日下午7:08:50
	 * @version 1.0
	 * @param taskType
	 * @return
	 */
	private short getWayToiletType(short taskType) {
		if (taskType == TaskType.ToiletRegister.getValue()) {
			return 1;
		}
		if (taskType == TaskType.NappyRegister.getValue()) {
			return 2;
		}
		return 0;
	}

	/**
	 * 
	 * @description childId集合是否包含id
	 * @author mingwang
	 * @create 2016年9月29日下午3:37:05
	 * @version 1.0
	 * @param childIds
	 * @param id
	 * @return
	 */
	private boolean isContainChildId(List<String> childIds, String id) {
		for (String childId : childIds) {
			if (childId.equals(id)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 将Register的过期状态更新为overDue
	 */
	@Override
	public void updateOverDueRegisterTimedTask(Date now, short taskType) {
		log.info("updateOverDueRegisterTimedTask|start");
		// 获取过期的Register
		List<String> overDueIntobsleaIds = registerTaskInfoMapper.getOverDueRegister(now, taskType);
		// 将记录表中的记录更新为overDue
		int instanceCount = taskInstanceInfoMapper.updateStatuByValueIds(overDueIntobsleaIds);
		log.info("updateOverDueRegisterTimedTask|instanceCount:" + instanceCount);
		// 将过期Register状态更新为overDue
		int registerCount = registerTaskInfoMapper.updateOverDueRegister(now, taskType);
		log.info("updateOverDueRegisterTimedTask|end|weeklyCount:" + registerCount);

	}

	@Override
	public ServiceResult<Object> updateRegisterItemTime(ProgramRegisterItemInfo itemInfo) {
		itemInfo.setUpdateTime(new Date());
		registerItemInfoMapper.updateItemTime(itemInfo);
		return successResult();
	}

	/**
	 * 获取当前room所在centre的所有签到小孩
	 */
	@Override
	public ServiceResult<Object> getCentreSignChilds(String roomId, Date date, String params, Short type) {
		List<SelecterPo> toDayIn = new ArrayList<SelecterPo>();
		// 获取当前room所在的centreId
		String centerId = roomInfoMapper.selectCenterIdByRoom(roomId);
		// 根据centreId获取该园区里所有的room
		List<SelecterPo> selecterRooms = roomInfoMapper.selecterRooms(centerId, null, null);
		// 循环所有的room，获取每个room的签到小孩，加入集合中
		for (SelecterPo room : selecterRooms) {
			ServiceResult<Object> signChilds = getSignChilds(room.getId(), date, params, type);
			toDayIn.addAll((List<SelecterPo>) signChilds.getReturnObj());
		}
		return successResult((Object) toDayIn);
	}

	@Override
	public ServiceResult<Object> getChildsByRoom(String roomId, String p) {
		List<SelecterPo> childList = userInfoMapper.getChildsByRoom(roomId, p);
		return successResult((Object) childList);
	}

}
