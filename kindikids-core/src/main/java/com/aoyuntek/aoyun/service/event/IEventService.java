package com.aoyuntek.aoyun.service.event;

import com.aoyuntek.aoyun.condtion.EventCondition;
import com.aoyuntek.aoyun.condtion.GalleryCondition;
import com.aoyuntek.aoyun.dao.event.EventInfoMapper;
import com.aoyuntek.aoyun.entity.po.CentersInfo;
import com.aoyuntek.aoyun.entity.po.event.EventInfo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.event.EventInfoVo;
import com.aoyuntek.aoyun.entity.vo.event.GalleryInfoVo;
import com.aoyuntek.aoyun.service.IBaseWebService;
import com.aoyuntek.framework.model.Pager;
import com.theone.common.util.ServiceResult;

public interface IEventService extends IBaseWebService<EventInfo, EventInfoMapper> {
    /**
     * 获取event信息
     * 
     * @param id
     * @param currentUser
     * @return
     */
    ServiceResult<EventInfoVo> getEventInfo(String id, UserInfoVo currentUser);

    /**
     * 保存或者更新Event
     * 
     * @param eventInfoVo
     * @param currentUser
     * @return
     */
    ServiceResult<Object> addOrUpdateEvent(EventInfoVo eventInfoVo, UserInfoVo currentUser);

    /**
     * 通过familyid 获取该家长所有小孩
     * 
     * @param familyId
     * @return
     */
    ServiceResult<Object> getChildIdByFamilyId(String familyId);

    /**
     * 获取集合
     * 
     * @param eventCondition
     * @param currentUser
     * @return
     */
    ServiceResult<Pager<EventInfoVo, EventCondition>> getEventList(EventCondition eventCondition, UserInfoVo currentUser);

    /**
     * 获取相册集合
     * 
     * @param galleryCondition
     * @param currentUser
     * @return
     */
    ServiceResult<Pager<GalleryInfoVo, GalleryCondition>> getGalleryList(GalleryCondition galleryCondition, UserInfoVo currentUser);

    /**
     * @description 获取园区集合
     * @author hxzhang
     * @create 2017年2月28日上午9:28:36
     */
    ServiceResult<Object> getCenterList();

    ServiceResult<Object> deleteEvent(String id, UserInfoVo currentUser);

}
