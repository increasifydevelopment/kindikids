package com.aoyuntek.aoyun.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.aoyuntek.aoyun.dao.MsgEmailInfoMapper;
import com.aoyuntek.aoyun.entity.po.MsgEmailInfo;
import com.aoyuntek.aoyun.entity.vo.EmailVo;
import com.aoyuntek.aoyun.enums.jobs.MsgEmailCategory;
import com.aoyuntek.aoyun.service.IMsgEmailInfoService;
import com.aoyuntek.aoyun.uitl.EmailUtil;
import com.sendgrid.SendGridException;
import com.theone.aws.util.AwsEmailUtil;
import com.theone.aws.util.SmsUtil;

@Service
public class MsgEmailServiceImpl extends BaseWebServiceImpl<MsgEmailInfo, MsgEmailInfoMapper> implements IMsgEmailInfoService {
    /**
     * 日志对象
     */
    public static Logger log = Logger.getLogger(MsgEmailServiceImpl.class);
    /**
     * msgEmailInfoMapper
     */
    @Autowired
    private MsgEmailInfoMapper msgEmailInfoMapper;

    @Override
    public void sendMsg() {
        sendPhoneMsg();
        sendEmailMsg();
    }

    /**
     * 
     * @description 发送短信
     * @author gfwang
     * @create 2016年10月10日上午10:29:55
     * @version 1.0
     */
    private void sendPhoneMsg() {
        log.info("sendPhoneMsg|start|");
        List<MsgEmailInfo> phoneList = msgEmailInfoMapper.getList(MsgEmailCategory.PhoneMsg.getValue());
        for (MsgEmailInfo phoneMsg : phoneList) {
            String id = phoneMsg.getId();
            try {
                log.info("send phone id=" + id);
                new SmsUtil(Region.getRegion(Regions.AP_SOUTHEAST_2)).sendSms(phoneMsg.getMsg(), phoneMsg.getReceiveraddress());
                int count = msgEmailInfoMapper.deleteById(id);
                log.info("send phone success," + count);
            } catch (Exception e) {
                log.error("批量发送短信失败id=" + id);
                log.error(e.getMessage(), e);
            }
        }
        log.info("sendPhoneMsg|end|");
    }

    /**
     * 
     * @description 发送邮件
     * @author gfwang
     * @create 2016年10月10日上午10:30:01
     * @version 1.0
     */
    private void sendEmailMsg() {
        log.info("sendEmailMsg|start|");
        List<MsgEmailInfo> emailList = msgEmailInfoMapper.getList(MsgEmailCategory.EmailMsg.getValue());
        log.info("emailList size=" + emailList.size());
        String emailKey = getSystemMsg("email_key");
        AwsEmailUtil emailUtil = new AwsEmailUtil();
        for (MsgEmailInfo email : emailList) {
            String id = email.getId();
            log.info("send email id=" + id);
            try {
                emailUtil.sendgrid(email.getSenderaddress(), email.getSendername(), email.getReceiveraddress(), email.getReceivername(),
                        email.getSub(), email.getMsg(), true, email.getReplyto(), null, emailKey);
                int count = msgEmailInfoMapper.deleteById(id);
                log.info("send email success," + count);
            } catch (Exception e) {
                log.error("批量发送邮件失败,id=" + id);
                log.error(e.getMessage(), e);
            }
        }
        log.info("sendEmailMsg|end|");
    }

    @Override
    public void batchSendEamil(final List<EmailVo> emailVoList) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String emailKey = getSystemMsg("email_key");
                String sendAddress = getSystemMsg("mailHostAccount");
                String sendName = getSystemMsg("email_name");
                for (EmailVo emailVo : emailVoList) {
                    try {
                        AwsEmailUtil a = new AwsEmailUtil();
                        a.sendgrid(sendAddress, sendName, emailVo.getReceiverAddress(), emailVo.getReceiverName(), emailVo.getSub(),
                                emailVo.getMsg(), emailVo.isHtml(), emailVo.getReplyTos(), emailVo.getAttachments(), emailKey);
                    } catch (SendGridException e) {
                        log.error("发送邮件失败" + e.toString());
                    }
                }
            }
        }).start();
    }

    @Override
    public void batchSendEamil(final List<EmailVo> emailVoList, final String[] bccs) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String emailKey = getSystemMsg("email_key");
                String sendAddress = getSystemMsg("mailHostAccount");
                String sendName = getSystemMsg("email_name");
                for (EmailVo emailVo : emailVoList) {
                    try {
                        EmailUtil.sendgrid(sendAddress, sendName, emailVo.getReceiverAddress(), emailVo.getReceiverName(), emailVo.getSub(),
                                emailVo.getMsg(), emailVo.isHtml(), emailVo.getReplyTos(), emailVo.getAttachments(), emailKey, bccs);
                    } catch (SendGridException e) {
                        log.error("发送邮件失败" + e.toString());
                    }
                }
            }
        }).start();
    }
}
