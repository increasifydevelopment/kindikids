package com.aoyuntek.aoyun.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.context.ContextLoader;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.aoyuntek.aoyun.constants.S3Constants;
import com.aoyuntek.aoyun.entity.po.RoleInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.vo.RoleInfoVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.enums.TaskType;
import com.aoyuntek.aoyun.service.IBaseWebService;
import com.aoyuntek.aoyun.uitl.FileUtil;
import com.aoyuntek.framework.constants.PropertieNameConts;
import com.aoyuntek.framework.service.impl.BaseService;
import com.theone.aws.util.FileFactory;
import com.theone.common.util.ServiceResult;
import com.theone.string.util.StringUtil;

/**
 * 基类
 * 
 * @author dlli5 at 2016年4月26日下午2:51:35
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 * @param <T>
 * @param <M>
 */
public class BaseWebServiceImpl<T, M> extends BaseService<T, M> implements IBaseWebService<T, M> {

    @Value("#{tip}")
    private Properties tipProperties;
    @Value("#{sys}")
    private Properties sysProperties;
    @Value("#{email}")
    private Properties emailProperties;
    @Value("#{pdfProp}")
    private Properties pdfProperties;
    @Value("#{feilds}")
    private Properties feildsProperties;

    public Properties getFeildsProperties() {
        return feildsProperties;
    }

    public void setFeildsProperties(Properties feildsProperties) {
        this.feildsProperties = feildsProperties;
    }

    public String getTemplet() {
        return ContextLoader.getCurrentWebApplicationContext().getServletContext().getRealPath("/") + "templet" + File.separator;
    }

    /**
     * @description 获取提示语
     * @author xdwang
     * @create 2015年12月13日下午2:07:04
     * @version 1.0
     * @param tipKey
     *            提示语配置文件key
     * @return key对应的value值
     */
    public String getTipMsg(String tipKey) {
        return tipProperties.getProperty(tipKey);
    }

    /**
     * @description 获取系统配置
     * @author xdwang
     * @create 2015年12月13日下午2:07:28
     * @version 1.0
     * @param tipKey
     *            配置key值
     * @return value值
     */
    public String getSystemMsg(String tipKey) {
        return sysProperties.getProperty(tipKey);
    }

    /**
     * @description 获取系统邮件模版配置
     * @author xdwang
     * @create 2015年12月13日下午2:07:28
     * @version 1.0
     * @param tipKey
     *            配置key值
     * @return value值
     */
    public String getSystemEmailMsg(String tipKey) {
        return emailProperties.getProperty(tipKey);
    }

    /**
     * 
     * @description 当此人为兼职+二级园长的时候，或者园长且因为园区归档导致没有园区的时候，提示无权限操作
     * @author gfwang
     * @create 2016年11月18日下午4:07:11
     * @version 1.0
     * @param vo
     *            当前用户
     * @return
     */
    public ServiceResult<Object> isCasualCentreManager(UserInfoVo vo) {
        List<RoleInfoVo> roleInfoVoList = vo.getRoleInfoVoList();
        String centreId = vo.getUserInfo().getCentersId();
        boolean isCentreManger = containRoles(roleInfoVoList, Role.CentreManager, Role.EducatorSecondInCharge);
        if (isCentreManger && StringUtil.isEmpty(centreId)) {
            return failResult(getTipMsg("no.permissions"));
        }
        return successResult();
    }

    /**
     * @description 当前用户是兼职且未排班
     * @author hxzhang
     * @create 2017年1月18日下午2:48:33
     */
    public ServiceResult<Object> isCasualAndNotRoster(UserInfoVo vo) {
        List<RoleInfoVo> roleInfoVoList = vo.getRoleInfoVoList();
        String centreId = vo.getUserInfo().getCentersId();
        boolean isCasual = containRoles(roleInfoVoList, Role.Casual);
        if (isCasual && StringUtil.isEmpty(centreId)) {
            return failResult(getTipMsg("no.permissions"));
        }
        return successResult();
    }

    /**
     * @description 获取系统配置
     * @author xdwang
     * @create 2015年12月13日下午2:07:28
     * @version 1.0
     * @param tipKey
     *            配置key值
     * @return value值
     */
    public String getSystemfeildsMsg(String tipKey) {
        return feildsProperties.getProperty(tipKey);
    }

    /**
     * @description 获取系统邮件模版配置
     * @author xdwang
     * @create 2015年12月13日下午2:07:28
     * @version 1.0
     * @param tipKey
     *            配置key值
     * @return value值
     */
    public String getSystemPdfMsg(String tipKey) {
        return pdfProperties.getProperty(tipKey);
    }

    /**
     * 
     * @description 是否包含此角色
     * @author gfwang
     * @create 2016年7月21日上午9:17:26
     * @version 1.0
     * @param role
     *            要判断的角色
     * @param roleList
     *            角色集合
     * @return 1包含，-1不包含
     */
    public short containRole(Role role, List<RoleInfoVo> roleList) {
        short result = -1;
        for (RoleInfoVo allRole : roleList) {
            if (allRole.getValue() == role.getValue()) {
                result = 1;
            }
        }
        return result;
    }

    public short containRoles(Role role, List<RoleInfo> roleList) {
        short result = -1;
        for (RoleInfo allRole : roleList) {
            if (allRole.getValue() == role.getValue()) {
                result = 1;
            }
        }
        return result;
    }

    /**
     * 
     * @description have roles
     * @author gfwang
     * @create 2016年9月8日下午1:59:16
     * @version 1.0
     * @param roleList
     * @param roles
     * @return
     */
    public boolean containRoles(List<RoleInfoVo> roleList, Role... roles) {
        boolean result = false;
        for (RoleInfoVo allRole : roleList) {
            for (Role role : roles) {
                if (allRole.getValue() == role.getValue()) {
                    result = true;
                }
            }
        }
        return result;
    }

    public boolean containRolesAll(List<RoleInfo> roleList, Role... roles) {
        // 需要具备的角色
        for (Role role : roles) {
            boolean haveRole = false;
            // 已有的角色
            for (RoleInfo allRole : roleList) {
                if (allRole.getValue() == role.getValue()) {
                    haveRole = true;
                    break;
                }
            }
            if (!haveRole) {
                return false;
            }
        }
        return true;
    }

    public boolean containRolesOr(List<RoleInfo> roleList, Role... roles) {
        // 需要具备的角色
        for (Role role : roles) {
            // 已有的角色
            for (RoleInfo allRole : roleList) {
                if (allRole.getValue() == role.getValue()) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean containTaskType(Short currtenType, TaskType... types) {
        boolean result = false;
        for (TaskType type : types) {
            if (currtenType == type.getValue()) {
                result = true;
            }
        }
        return result;
    }

    /**
     * 
     * @description 是否选择
     * @author gfwang
     * @create 2016年8月15日下午2:59:22
     * @version 1.0
     * @param value
     * @return
     */
    public boolean isChoose(Short value) {
        if (value == null) {
            return false;
        }
        return value == 1 ? true : false;
    }

    /**
     * 
     * @description 替换特殊字符
     * @author gfwang
     * @create 2016年5月25日下午10:52:29
     * @version 1.0
     * @param value
     * @return
     */
    public String convertSpecialCode(String value) {
        if (StringUtil.isEmpty(value)) {
            return "";
        }
        // 读取配置文件特殊字符及其对应转义字符
        String specialCodeStr = getSystemMsg("special_code");
        String[] twainSpecialCodes = specialCodeStr.split(",");
        Map<String, String> singlespecialCodeMap = new LinkedHashMap<String, String>();
        for (String twainSpecialCode : twainSpecialCodes) {
            System.err.println("twainSpecialCode = " + twainSpecialCode);
            String[] singleSpecialCodeArray = twainSpecialCode.split(":");
            singlespecialCodeMap.put(singleSpecialCodeArray[0], singleSpecialCodeArray[1]);
        }
        // 遍历配置文件读取到的替特殊字符，替换为对应转义字符
        Iterator it = singlespecialCodeMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> entity = (Entry) it.next();
            System.err.println("entity:Key Value = " + entity.getKey() + " " + entity.getValue());
            value = value.replaceAll(entity.getKey(), entity.getValue());
        }
        return value;
    }

    /**
     * @description 获取用户头像,如果没有头像获取背景色及名字缩写
     * @author hxzhang
     * @create 2017年3月6日下午4:22:46
     */
    public String getAvatar(UserInfo user) throws FileNotFoundException {
        if (null == user) {
            return null;
        }
        if (StringUtil.isEmpty(user.getAvatar())) {
            String simpleName = getSimpleName(user.getFirstName(), user.getLastName());
            return MessageFormat.format(getSystemPdfMsg("avatar_name_simple"), "pdf" + user.getPersonColor(), simpleName);
        }
        String avatar = StringUtil.isEmpty(user.getAvatar()) ? getSystemMsg("emptyAvatar") : convertSpecialCode(new FileFactory(
                Region.getRegion(Regions.AP_SOUTHEAST_2), PropertieNameConts.S3).generateUrl(S3Constants.BUCKET_NAME, user.getAvatar(),
                S3Constants.URL_TIMEOUT_HOUR));

        return avatar = MessageFormat.format(getSystemPdfMsg("avatar"), avatar);
    }

    /**
     * @description 获取用户名字缩写
     * @author hxzhang
     * @create 2017年3月6日下午4:23:31
     */
    public String getSimpleName(String firstName, String lastName) {
        StringBuffer buffer = new StringBuffer();
        if (StringUtil.isNotEmpty(firstName) && firstName.length() >= 1) {
            buffer.append(firstName.substring(0, 1));
        }
        if (StringUtil.isNotEmpty(lastName) && lastName.length() >= 1) {
            buffer.append(lastName.substring(0, 1));
        }
        return buffer.toString();
    }

    public static String getSpecialEmailStr() {
        String html = "";
        String filePath = "";
        try {
            filePath = ContextLoader.getCurrentWebApplicationContext().getServletContext().getRealPath("/") + "templet" + File.separator + "email"
                    + File.separator + "waitlist-email.html";
        } catch (Exception e) {
            filePath = System.getProperty("user.dir") + File.separator + "templet" + File.separator + "waitlist-email.html";
        }
        try {
            html = FileUtil.readTxt(filePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return html;
    }

}
