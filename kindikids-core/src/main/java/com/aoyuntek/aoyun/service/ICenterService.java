package com.aoyuntek.aoyun.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.aoyuntek.aoyun.condtion.CenterCondition;
import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.entity.po.CentersInfo;
import com.aoyuntek.aoyun.entity.po.RoomGroupInfo;
import com.aoyuntek.aoyun.entity.po.RoomInfo;
import com.aoyuntek.aoyun.entity.vo.CenterInfoVo;
import com.aoyuntek.aoyun.entity.vo.GroupInfoVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.WeekNutrionalMenuVo;
import com.aoyuntek.framework.condtion.BaseCondition;
import com.aoyuntek.framework.model.Pager;
import com.theone.common.util.ServiceResult;

/**
 * 
 * @description Centers业务逻辑层接口
 * @author mingwang
 * @create 2016年8月18日下午2:02:56
 * @version 1.0
 */
public interface ICenterService extends IBaseWebService<CentersInfo, CentersInfoMapper> {

    /**
     * 
     * @description 新增园区
     * @author mingwang
     * @create 2016年8月18日下午2:57:01
     * @version 1.0
     * @param centersInfo
     *            centersInfo
     * @param currentUser
     *            当前登录用户
     * @return
     */
    ServiceResult<Object> addOrUpdateCenter(CenterInfoVo centersInfo, UserInfoVo currentUser);

    /**
     * 
     * @description 获取园区列表
     * @author mingwang
     * @create 2016年8月18日下午4:26:31
     * @version 1.0
     * @return
     */
    ServiceResult<Pager<CenterInfoVo, BaseCondition>> getCenterList(CenterCondition condition, UserInfoVo currentUser);

    /**
     * 
     * @description 新增Room
     * @author mingwang
     * @create 2016年8月19日下午3:25:21
     * @version 1.0
     * @param roomInfo
     * @param currentUser
     * @return
     */
    ServiceResult<Object> addOrUpdateRoom(RoomInfo roomInfo, UserInfoVo currentUser);

    /**
     * 
     * @description 新增Group
     * @author gfwang
     * @create 2016年8月19日下午3:25:21
     * @version 1.0
     * @param roomInfo
     * @param currentUser
     * @return
     */
    ServiceResult<Object> addOrUpdateGroup(RoomGroupInfo groupInfo, UserInfoVo currentUser);

    /**
     * 
     * @description 获取园区信息、room列表信息、菜单信息
     * @author gfwang
     * @create 2016年8月20日上午11:39:27
     * @version 1.0
     * @param centerId
     *            园区ID
     * @return 此园区信息
     */
    ServiceResult<CenterInfoVo> getCenterInfo(String centerId);

    /**
     * 
     * @description 获取1园区的菜单
     * @author gfwang
     * @create 2016年8月20日下午1:55:25
     * @version 1.0
     * @param centerId
     *            园区
     * @param weekNum
     *            第几周
     * @return
     */
    List<WeekNutrionalMenuVo> getWeekMenuByCenter(String centerId, Short weekNum);

    /**
     * 
     * @description 获取1个分组信息
     * @author gfwang
     * @create 2016年8月22日上午11:46:56
     * @version 1.0
     * @param groupId
     *            groupId
     * @return GroupInfoVo
     */
    GroupInfoVo getGroupInfo(String groupId);

    /**
     * 
     * @description 归档/解除归档group
     * @author mingwang
     * @create 2016年8月22日上午10:08:54
     * @version 1.0
     * @param groupId
     * 
     * @param flag
     *            归档/解除归档
     * @return
     */
    ServiceResult<Object> updateArchiveGroup(String groupId, boolean flag, UserInfoVo currentUser);

    /**
     * 
     * @description 归档/解除归档room
     * @author mingwang
     * @create 2016年8月22日下午1:52:40
     * @version 1.0
     * @param roomId
     * @param flag
     *            归档/解除归档
     * @return
     */
    ServiceResult<Object> updateArchiveRoom(String roomId, boolean flag, UserInfoVo currentUser);

    /**
     * 
     * @description 归档/解除归档center
     * @author mingwang
     * @create 2016年8月22日下午2:16:03
     * @version 1.0
     * @param centerId
     * @param flag
     * @param currentUser
     * @return
     */
    ServiceResult<Object> updateArchiveCenter(String centerId, boolean flag, UserInfoVo currentUser);

    /**
     * 
     * @description getCenterMenuList
     * @author gfwang
     * @create 2016年8月23日上午9:45:29
     * @version 1.0
     * @param user
     *            cuuternuser
     * @return
     */
    ServiceResult<Object> getCenterMenuList(UserInfoVo user);

    /**
     * 
     * @description 新增菜单
     * @author mingwang
     * @create 2016年8月23日上午9:37:41
     * @version 1.0
     * @param weekMenuList
     * @param currentUser
     * @return
     */
    ServiceResult<Object> updateWeekMenu(List<WeekNutrionalMenuVo> weekMenuList, UserInfoVo currentUser);

    /**
     * 
     * @description 定时发送菜单
     * @author mingwang
     * @create 2016年8月24日下午3:32:49
     * @version 1.0
     * @param newsFeedInfoVo
     * @param currentUser
     * @return
     */
    void sendTodaysMenu(Date date);

    /**
     * 
     * @description 删除园长/老师时解除与center/group的关系
     * @author mingwang
     * @create 2016年8月26日下午2:14:31
     * @version 1.0
     * @param userId
     */
    void releaseStaff(String userId);

    /**
     * 
     * @description 获取名称
     * @author gfwang
     * @create 2016年9月25日下午9:11:17
     * @version 1.0
     * @param roomId
     * @return
     */
    Map<String, String> getCenterRoom(String roomId);

    /**
     * @description
     * @author hxzhang
     * @create 2016年12月8日下午1:13:13
     */
    ServiceResult<Object> getAllCentersInfos();

    void saveWeekNutrionalMenuInfo(String centerId, UserInfoVo currentUser);

    void saveCheckRoomTask(String id, boolean check);
}
