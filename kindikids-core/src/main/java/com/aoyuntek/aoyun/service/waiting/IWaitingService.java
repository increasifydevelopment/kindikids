package com.aoyuntek.aoyun.service.waiting;

import com.aoyuntek.aoyun.condtion.waitingList.WaitingListCondition;
import com.aoyuntek.aoyun.entity.po.ApplicationList;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.waiting.SubmitModel;
import com.aoyuntek.aoyun.entity.vo.waiting.WaitingListVo;
import com.theone.common.util.ServiceResult;

public interface IWaitingService {
	ServiceResult<Object> getList(WaitingListCondition condition, UserInfoVo currentUser);

	ServiceResult<Object> getDetails(String id);

	ServiceResult<Object> getCount(UserInfoVo currentUser);

	ServiceResult<Object> updatePositionDate(WaitingListVo vo, String ip, UserInfoVo currentUser);

	ServiceResult<Object> submitMove(SubmitModel model, UserInfoVo currentUser);

	Object getFamilySelect(String p);

	void sendPlanEmail();

	ServiceResult<Object> save(ApplicationList app, String ip, UserInfoVo currentUser);

	String exportCsv(WaitingListCondition condition, UserInfoVo currentUser);
}
