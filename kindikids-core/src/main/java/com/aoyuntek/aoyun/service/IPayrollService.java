package com.aoyuntek.aoyun.service;

import java.util.Date;

import com.aoyuntek.aoyun.condtion.PayrollCondition;

public interface IPayrollService {

	void dealClosurePeriodData(Date now);

	StringBuffer payrollExport(PayrollCondition condition);

}
