package com.aoyuntek.aoyun.service;

import com.aoyuntek.aoyun.condtion.EmailListCondition;
import com.aoyuntek.aoyun.dao.EmailPlanInfoMapper;
import com.aoyuntek.aoyun.entity.po.EmailPlanInfo;
import com.theone.common.util.ServiceResult;

public interface IEmailListService extends IBaseWebService<EmailPlanInfo, EmailPlanInfoMapper> {
	/**
	 * @author hxzhang 2018年9月26日
	 * @param condition
	 * @return
	 */
	ServiceResult<Object> getList(EmailListCondition condition);

	/**
	 * @author hxzhang 2018年9月26日
	 * @param condition
	 */
	ServiceResult<Object> sendEmail(EmailListCondition condition);
}
