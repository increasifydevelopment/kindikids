package com.aoyuntek.aoyun.service.old;

import java.util.List;
import java.util.Map;

import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.entity.po.AccountInfo;
import com.aoyuntek.aoyun.entity.po.AccountRoleInfo;
import com.aoyuntek.aoyun.entity.po.AttachmentInfo;
import com.aoyuntek.aoyun.entity.po.CredentialsInfo;
import com.aoyuntek.aoyun.entity.po.EmergencyContactInfo;
import com.aoyuntek.aoyun.entity.po.MedicalImmunisationInfo;
import com.aoyuntek.aoyun.entity.po.MedicalInfo;
import com.aoyuntek.aoyun.entity.po.MessageContentInfo;
import com.aoyuntek.aoyun.entity.po.MessageInfo;
import com.aoyuntek.aoyun.entity.po.MessageLookInfo;
import com.aoyuntek.aoyun.entity.po.NewsCommentInfo;
import com.aoyuntek.aoyun.entity.po.NewsContentInfo;
import com.aoyuntek.aoyun.entity.po.NewsGroupInfo;
import com.aoyuntek.aoyun.entity.po.NewsInfo;
import com.aoyuntek.aoyun.entity.po.NewsReceiveInfo;
import com.aoyuntek.aoyun.entity.po.NqsRelationInfo;
import com.aoyuntek.aoyun.entity.po.SignChildInfoWithBLOBs;
import com.aoyuntek.aoyun.entity.po.SignSelationInfo;
import com.aoyuntek.aoyun.entity.po.SignVisitorInfoWithBLOBs;
import com.aoyuntek.aoyun.entity.po.SigninInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.po.YelfNewsInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramIntobsleaInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramLessonInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramRegisterItemInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramRegisterSleepInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramRegisterTaskInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramRegisterItemInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramRegisterTaskInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramTaskExgrscInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramWeeklyEvalutionInfo;
import com.aoyuntek.aoyun.entity.po.program.TaskProgramFollowUpInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskInstanceInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskResponsibleLogInfo;
import com.aoyuntek.aoyun.entity.vo.old.OldFamilyInfoVo;
import com.aoyuntek.aoyun.service.IBaseWebService;

public interface IOldDataService extends IBaseWebService<UserInfo, UserInfoMapper> {
    void importStaff(List<UserInfo> userList, List<AccountInfo> accountList, List<AccountRoleInfo> accountRoleList,
            List<EmergencyContactInfo> contactList, List<CredentialsInfo> credentialsList, List<MedicalInfo> medicalList,
            List<MedicalImmunisationInfo> immunisationList, List<AttachmentInfo> attachList, List<String> centerMangerMap,
            Map<String, String> groupMangerMap);

    void importVisitorSign(List<SigninInfo> signinInfos, List<SignVisitorInfoWithBLOBs> signVisitorInfosInfoWithBLOBs,
            List<SignSelationInfo> signSelationInfos);

    void importStaffSign(List<SigninInfo> signinInfos);

    void importChildSign(List<SigninInfo> signinInfos, List<SignChildInfoWithBLOBs> signChildInfoWithBLOBs, List<SignSelationInfo> signSelationInfos);

    void importProgramIntobslea(List<ProgramIntobsleaInfo> list, List<TaskInstanceInfo> taskInstanceInfos,
            List<TaskResponsibleLogInfo> responsibleList, List<AttachmentInfo> fileList);

    void importFollowup(List<TaskProgramFollowUpInfo> list, List<TaskInstanceInfo> taskInstanceInfos, List<TaskResponsibleLogInfo> responsibleList);

    /**
     * 
     * @description newsfeed数据导入
     * @author mingwang
     * @create 2016年12月14日下午3:29:17
     * @param newsList
     * @param nqsList
     * @param contentList
     * @param commentList
     * @param newsGroupList
     * @param newsReceiveInfoList
     * @param attachmentInfos
     */
    void importNewsFeed(List<NewsInfo> newsList, List<NqsRelationInfo> nqsList, List<NewsContentInfo> contentList, List<NewsCommentInfo> commentList,
            List<NewsGroupInfo> newsGroupList, List<NewsReceiveInfo> newsReceiveInfoList, List<AttachmentInfo> attachmentInfos,
            List<YelfNewsInfo> eylfList);

    void importFamily(OldFamilyInfoVo familyVo);

    void importMessage(List<NqsRelationInfo> nqs, List<AttachmentInfo> attachmentInfos, List<MessageLookInfo> messageLookInfos,
            List<MessageInfo> messageInfos, List<MessageContentInfo> messageContentInfos);

    void importProgramCurriculums(List<TaskInstanceInfo> taskInstanceList, List<ProgramTaskExgrscInfo> taskList, List<AttachmentInfo> fileList,
            List<TaskResponsibleLogInfo> responsibleList);

    void importWeekly(List<ProgramWeeklyEvalutionInfo> programWeeklyEvalutionInfos, List<TaskInstanceInfo> TaskInstanceList,List<TaskResponsibleLogInfo> taskResponsibleLogInfos);

    void importLesson(List<ProgramLessonInfo> lessonList, List<NqsRelationInfo> nqsList, List<AttachmentInfo> attachmentInfos, List<TaskResponsibleLogInfo> responsibleList);

    void importRegister(List<ProgramRegisterTaskInfo> taskInfoList, List<ProgramRegisterItemInfo> registerItemList,
            List<TaskInstanceInfo> taskInstanceList, List<ProgramRegisterSleepInfo> sleepItemList, List<TaskResponsibleLogInfo> responsibleList);

    void importNappyRegister(List<ProgramRegisterTaskInfo> taskInfoList, List<ProgramRegisterItemInfo> taskItemList, List<TaskResponsibleLogInfo> responsibleList);
}
