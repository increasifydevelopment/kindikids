package com.aoyuntek.aoyun.service.logs.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.condtion.LogsCondition;
import com.aoyuntek.aoyun.dao.LogsDetaileInfoMapper;
import com.aoyuntek.aoyun.entity.po.LogsDetaileInfo;
import com.aoyuntek.aoyun.entity.vo.LogsDetaileVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.service.impl.BaseWebServiceImpl;
import com.aoyuntek.aoyun.service.logs.ILogsService;
import com.aoyuntek.framework.model.Pager;
import com.theone.common.util.ServiceResult;

@Service
public class LogsServiceImpl extends BaseWebServiceImpl<LogsDetaileInfo, LogsDetaileInfoMapper> implements ILogsService {

	@Autowired
	private LogsDetaileInfoMapper logsInfoMapper;

	@Override
	public ServiceResult<Pager<LogsDetaileVo, LogsCondition>> getLogsList(LogsCondition condition, UserInfoVo currentUser) {	
		    condition.setStartSize(condition.getPageIndex());
	        List<LogsDetaileVo> list = logsInfoMapper.getLogsList(condition);
	        int totalSize = logsInfoMapper.getLogsCount(condition);
	        condition.setTotalSize(totalSize);
	        Pager<LogsDetaileVo, LogsCondition> pager = new Pager<LogsDetaileVo, LogsCondition>(list, condition);
	        
	        return successResult(pager);
	}

}
