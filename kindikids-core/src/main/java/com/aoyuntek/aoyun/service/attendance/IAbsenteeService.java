package com.aoyuntek.aoyun.service.attendance;

import java.util.Date;

import com.aoyuntek.aoyun.dao.AbsenteeRequestInfoMapper;
import com.aoyuntek.aoyun.entity.po.AbsenteeRequestInfo;
import com.aoyuntek.aoyun.entity.po.AbsenteeRequestInfoWithBLOBs;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.service.IBaseWebService;
import com.theone.common.util.ServiceResult;

public interface IAbsenteeService extends IBaseWebService<AbsenteeRequestInfo, AbsenteeRequestInfoMapper> {
    /**
     * @description 获取请假信息
     * @author hxzhang
     * @create 2016年8月26日下午4:53:01
     * @version 1.0
     * @param id
     * @return
     */
    ServiceResult<Object> getAbsentee(String id, String userId);

    /**
     * @description 新增或更新请假
     * @author hxzhang
     * @create 2016年8月26日下午5:07:26
     * @version 1.0
     * @param ariWithBLOBs
     * @param currentUser
     * @return
     */
    ServiceResult<Object> addOrUpdateAbsentee(AbsenteeRequestInfoWithBLOBs ariWithBLOBs, UserInfoVo currentUser, boolean isShow);

    /**
     * @description 更新请假操作
     * @author hxzhang
     * @create 2016年8月26日下午5:54:44
     * @version 1.0
     * @param id
     * @param opera
     * @return
     */
    ServiceResult<Object> updateOperaAbsenteeRequestInfo(String id, short opera, UserInfoVo currentUser);

    /**
     * @description 定时任务
     * @author hxzhang
     * @create 2016年8月28日下午5:39:21
     * @version 1.0
     * @param now
     * @return
     */
    int updateAbsenteeStatusTimedTask(Date now);

    /**
     * @description 获取小孩的请假天数及总天数
     * @author hxzhang
     * @create 2016年12月6日下午5:05:35
     */
    ServiceResult<Object> getAbsenteeDays(AbsenteeRequestInfoWithBLOBs ariWithBLOBs);
}
