package com.aoyuntek.aoyun.service;

import java.util.List;

import com.aoyuntek.aoyun.condtion.ProfileTempCondition;
import com.aoyuntek.aoyun.dao.profile.ProfileTemplateInfoMapper;
import com.aoyuntek.aoyun.entity.po.profile.ProfileTemplateInfo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.theone.common.util.ServiceResult;

/**
 * @description 业务逻辑层接口
 * @author Hxzhang 2016年10月21日上午9:42:10
 */
public interface IProfileService extends IBaseWebService<ProfileTemplateInfo, ProfileTemplateInfoMapper> {
    /**
     * @description 保存简介模版
     * @author hxzhang
     * @create 2016年10月21日上午9:44:13
     */
    public ServiceResult<Object> saveProfileTemp(ProfileTemplateInfo profileTemplateInfo, UserInfoVo currentUser, String formJson);

    /**
     * @description 获取简介模版
     * @author hxzhang
     * @create 2016年10月21日上午10:29:03
     */
    public ServiceResult<Object> getProfileTempInfo(String id);

    /**
     * @description 获取模版列表
     * @author hxzhang
     * @create 2016年10月21日上午10:29:57
     */
    public ServiceResult<Object> getProfileTempList(ProfileTempCondition condition);

    /**
     * @description 启用禁用模版
     * @author hxzhang
     * @create 2016年10月21日上午11:24:18
     */
    public ServiceResult<Object> saveOpera(String id, short state, UserInfoVo currentUser);
    
    List<SelecterPo> getSelectList(short type);

    /**
     * @description 通过用户ID获取模版信息
     * @author hxzhang
     * @create 2016年10月21日下午1:55:08
     */
    public ServiceResult<Object> getProfileTempByUserId(String userId,String templateId);
}
