package com.aoyuntek.aoyun.service.impl;

import java.io.FileNotFoundException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.aoyuntek.aoyun.condtion.MenuOrgsCondition;
import com.aoyuntek.aoyun.condtion.UserBySelectCondition;
import com.aoyuntek.aoyun.dao.AttachmentInfoMapper;
import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.MessageInfoMapper;
import com.aoyuntek.aoyun.dao.RoomGroupInfoMapper;
import com.aoyuntek.aoyun.dao.RoomInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.dao.UserStaffMapper;
import com.aoyuntek.aoyun.dao.profile.ChildFormRelationInfoMapper;
import com.aoyuntek.aoyun.entity.po.AttachmentInfo;
import com.aoyuntek.aoyun.entity.po.CentersInfo;
import com.aoyuntek.aoyun.entity.po.RoleInfo;
import com.aoyuntek.aoyun.entity.po.RoomInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.po.profile.ChildFormRelationInfo;
import com.aoyuntek.aoyun.entity.vo.BatchFileVo;
import com.aoyuntek.aoyun.entity.vo.FileVo;
import com.aoyuntek.aoyun.entity.vo.RoleInfoVo;
import com.aoyuntek.aoyun.entity.vo.RoomInfoVo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.aoyun.entity.vo.StaffRoleInfoVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.FunctionType;
import com.aoyuntek.aoyun.enums.GroupType;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.enums.UserType;
import com.aoyuntek.aoyun.service.IAuthGroupService;
import com.aoyuntek.aoyun.service.ICommonService;
import com.aoyuntek.aoyun.service.IFamilyService;
import com.aoyuntek.aoyun.uitl.FilePathUtil;
import com.aoyuntek.aoyun.uitl.MyListUtil;
import com.aoyuntek.framework.constants.PropertieNameConts;
import com.theone.aws.util.FileFactory;
import com.theone.common.util.ServiceResult;
import com.theone.date.util.DateUtil;
import com.theone.file.util.FileUtil;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

@Service
public class CommonServiceImpl extends BaseWebServiceImpl<UserInfo, UserInfoMapper> implements ICommonService {
	private static Logger log = Logger.getLogger(CommonServiceImpl.class);
	@Autowired
	private UserInfoMapper userMapper;

	@Autowired
	private MessageInfoMapper messageMapper;

	@Autowired
	private CentersInfoMapper centersInfoMapper;

	@Autowired
	private RoomInfoMapper roomMapper;
	@Value("#{auth}")
	private Properties authProperties;
	@Autowired
	private RoomGroupInfoMapper groupInfoMapper;
	@Autowired
	private UserStaffMapper staffMapper;
	@Autowired
	private AttachmentInfoMapper attachmentInfoMapper;

	@Autowired
	private IAuthGroupService authGroupService;

	@Autowired
	private ChildFormRelationInfoMapper childFormRelationInfoMapper;

	@Autowired
	private IFamilyService familyService;

	@Override
	public Set<SelecterPo> getReceiversMessage(String p, UserInfoVo currentUser, boolean isOnlyUsers, String msgId) {
		log.info("getReceiversMessage|start|defaultUser=" + msgId);
		List<String> list = null;
		String accountIdM = messageMapper.getAccountIdBymsgId(msgId);
		list = messageMapper.getAccountIdsByParentmsgId(msgId);
		list.add(accountIdM);
		// 初始化角色
		List<Short> roleList = initRole(currentUser);
		Set<SelecterPo> sets = new LinkedHashSet<SelecterPo>();
		String currtenAccountId = currentUser.getAccountInfo().getId();

		// 先获取分组
		if (!isOnlyUsers) {
			List<SelecterPo> groups = authGroupService.getGroupsByRole(currentUser, p, GroupType.MessageGroup);
			sets.addAll(groups);
		}
		for (Short role : roleList) {

			// 先获取分组
			if (!isOnlyUsers) {
				Set<String> groups = getMessageGroup(p, currentUser.getUserInfo().getCentersId(), GroupType.MessageGroup, role);
				for (String g : groups) {
					sets.add(new SelecterPo(g, g));
				}
			}
			// 再获取人
			if (StringUtil.isNotEmpty(p)) {
				if (role == Role.Parent.getValue()) {
					// 如果当前人为家长,获取家长的小孩所在园区的园长，二级园长
					sets.addAll(userMapper.getCenterMangersByParent(currtenAccountId, p));
				} else {
					// 按配置文件获取人
					sets.addAll(getMessageUsers(p, currentUser, role));
				}
			}
			if (isOnlyUsers) {
				Iterator<SelecterPo> it = sets.iterator();
				while (it.hasNext()) {
					SelecterPo select = it.next();
					String accountId = select.getId();
					if (list.contains(accountId)) {
						// 移除转发时下拉框的 （主邮件发件人，收件人）
						it.remove();
					}
				}
			}
		}
		return sets;
	}

	/**
	 * 
	 * @description 获取当前角色可选用户
	 * @author gfwang
	 * @create 2016年7月12日下午7:45:46
	 * @version 1.0
	 * @param p
	 *            参数
	 * @param currentUser
	 * @param role
	 *            角色
	 * @return list
	 * 
	 */
	private Set<SelecterPo> getMessageUsers(String p, UserInfoVo currentUser, short role) {
		log.info("getMessageUsers|start|role=" + role);
		Set<SelecterPo> sets = new LinkedHashSet<SelecterPo>();
		// message 临时工不可获取人员，小孩不可获取
		Role[] notAuthRole = new Role[] { Role.Children };
		if (!vilidateRole(role, notAuthRole)) {
			return sets;
		}
		UserBySelectCondition condition = initCondition(role, currentUser, p);
		sets = userMapper.getReceiveUser(condition);
		if (role == Role.CEO.getValue()) {
			// ceo只能选有小孩的家长
			sets.addAll(userMapper.getParentByKidCenter(null, p));
		}
		// 如果为园长，二级园长则,获取当前园区下小孩对应的家长
		if (role == Role.CentreManager.getValue() || role == Role.EducatorSecondInCharge.getValue()) {
			String centerId = currentUser.getUserInfo().getCentersId();
			log.info("getMessageUsers|CentreManager and EducatorSecondInCharge centerId=" + centerId);
			sets.addAll(userMapper.getParentByKidCenter(centerId, p));
		}
		return sets;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<SelecterPo> getUsersNews(String p, UserInfoVo currentUser, int type) {
		log.info("getUsersNews|start|type=" + type);
		// event 为4，不要删除的小孩
		// gallse 为 1，要删除的小孩
		int tempType = type;
		type = type == 4 ? 1 : type;
		Set<SelecterPo> sets = new LinkedHashSet<SelecterPo>();
		List<RoleInfoVo> roles = currentUser.getRoleInfoVoList();
		for (RoleInfo roleInfo : roles) {
			Short role = roleInfo.getValue();
			// message 临时工不可获取人员，家长，小孩不可获取
			Role[] notAuthRole = new Role[] { Role.Parent, Role.Children };
			if (!vilidateRole(role, notAuthRole) || StringUtil.isEmpty(p)) {
				// type ==1 为获取family小孩
				if (type == 1 && StringUtil.isNotEmpty(p)) {
					String familyId = currentUser.getUserInfo().getFamilyId();
					sets.addAll((Collection<? extends SelecterPo>) familyService.getChildListForSelect2(familyId, p).getReturnObj());
				}
				continue;
			}
			UserBySelectCondition condition = initNewsCondition(role, currentUser, p, type);
			condition.setType(tempType);
			sets.addAll(userMapper.getReceiveUser(condition));
		}
		return sets;
	}

	/**
	 * 
	 * @description 判断是否有权限
	 * @author gfwang
	 * @create 2016年7月14日下午5:13:02
	 * @version 1.0
	 * @param role
	 *            当前角色
	 * @param notAuthRole
	 *            无权限的角色
	 * @return true可以通行,false不可通行
	 */
	private boolean vilidateRole(short role, Role[] notAuthRole) {
		for (Role r : notAuthRole) {
			if (role == r.getValue()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 
	 * @description 初始化news下拉框查询条件
	 * @author gfwang
	 * @create 2016年7月22日下午2:27:42
	 * @version 1.0
	 * @param role
	 *            角色
	 * @param currentUser
	 *            当前用户
	 * @param p
	 *            关键字
	 * @param type
	 *            类型
	 * @return UserBySelectCondition
	 */
	private UserBySelectCondition initNewsCondition(short role, UserInfoVo currentUser, String p, int type) {
		log.info("initNewsCondition|start|type=" + type + "|role=" + role);
		List<RoleInfo> list = new ArrayList<RoleInfo>();
		UserBySelectCondition condition = new UserBySelectCondition();
		String centerId = currentUser.getUserInfo().getCentersId();
		condition.setName(p);
		// type=0所有 =1小孩 =2员工
		if (type == 1) {
			if (role == Role.CEO.getValue() || role == Role.Casual.getValue()) {
				list.add(new RoleInfo(Role.Children.getValue()));
			} else {
				list.add(new RoleInfo(centerId, Role.Children.getValue()));
			}
			condition.setRoles(list);
			return condition;
		}
		if (role == Role.CEO.getValue()) {
			if (type == 2) {
				list.add(new RoleInfo(Role.Children.getValue()));
				list.add(new RoleInfo(Role.Parent.getValue()));
				condition.setOpenNotIn(1);
			} else {
				list.add(new RoleInfo(Role.Parent.getValue()));
				condition.setOpenNotIn(1);
			}
		} else if (role == Role.CentreManager.getValue() || role == Role.EducatorSecondInCharge.getValue()) { // 如果为园长类的
			// 所有CEO
			list.add(new RoleInfo(Role.CEO.getValue()));
			// 所有园长
			list.add(new RoleInfo(Role.CentreManager.getValue()));
			// 所有二级园长
			list.add(new RoleInfo(Role.EducatorSecondInCharge.getValue()));
			// 园 所有老师
			list.add(new RoleInfo(centerId, Role.Educator.getValue()));
			list.add(new RoleInfo(centerId, Role.EducatorECT.getValue()));
			list.add(new RoleInfo(centerId, Role.EducatorCertifiedSupersor.getValue()));
			list.add(new RoleInfo(centerId, Role.EducatorFirstAidOfficer.getValue()));
			list.add(new RoleInfo(centerId, Role.EducatorNominatedSupervisor.getValue()));
			// 所有厨师
			list.add(new RoleInfo(Role.Cook.getValue()));
			// 所有临时工
			list.add(new RoleInfo(Role.Casual.getValue()));
			// 园所有小孩
			if (type == 0) {
				list.add(new RoleInfo(centerId, Role.Children.getValue()));
			}
		} else if (role == Role.Educator.getValue() || role == Role.EducatorECT.getValue() || role == Role.EducatorCertifiedSupersor.getValue()
				|| role == Role.EducatorFirstAidOfficer.getValue() || role == Role.EducatorNominatedSupervisor.getValue()) {
			// 所有CEO
			list.add(new RoleInfo(Role.CEO.getValue()));
			// 所有园长
			list.add(new RoleInfo(centerId, Role.CentreManager.getValue()));
			// 所有二级园长
			list.add(new RoleInfo(centerId, Role.EducatorSecondInCharge.getValue()));

			// 园 所有老师
			list.add(new RoleInfo(centerId, Role.Educator.getValue()));
			list.add(new RoleInfo(centerId, Role.EducatorECT.getValue()));
			list.add(new RoleInfo(centerId, Role.EducatorCertifiedSupersor.getValue()));
			list.add(new RoleInfo(centerId, Role.EducatorFirstAidOfficer.getValue()));
			list.add(new RoleInfo(centerId, Role.EducatorNominatedSupervisor.getValue()));
			// 园 所有厨师
			list.add(new RoleInfo(centerId, Role.Cook.getValue()));
			// 园所有小孩
			if (type == 0) {
				list.add(new RoleInfo(centerId, Role.Children.getValue()));
			}
		} else if (role == Role.Cook.getValue()) {
			// 所有CEO
			list.add(new RoleInfo(Role.CEO.getValue()));
			// 所有园长
			list.add(new RoleInfo(centerId, Role.CentreManager.getValue()));
			// 所有二级园长
			list.add(new RoleInfo(centerId, Role.EducatorSecondInCharge.getValue()));

			// 园 所有老师
			list.add(new RoleInfo(centerId, Role.Educator.getValue()));
			list.add(new RoleInfo(centerId, Role.EducatorECT.getValue()));
			list.add(new RoleInfo(centerId, Role.EducatorCertifiedSupersor.getValue()));
			list.add(new RoleInfo(centerId, Role.EducatorFirstAidOfficer.getValue()));
			list.add(new RoleInfo(centerId, Role.EducatorNominatedSupervisor.getValue()));
			// 园 所有厨师
			list.add(new RoleInfo(centerId, Role.Cook.getValue()));
			// 园所有小孩
			if (type == 0) {
				list.add(new RoleInfo(centerId, Role.Children.getValue()));
			}
		} else if (role == Role.Casual.getValue()) {
			list.add(new RoleInfo(Role.Parent.getValue()));
			condition.setOpenNotIn(1);
		}
		condition.setRoles(list);
		return condition;
	}

	/**
	 * 
	 * @description 初始化查询用户条件
	 * @author gfwang
	 * @create 2016年7月12日下午7:46:20
	 * @version 1.0
	 * @param role
	 *            角色
	 * @param currentUser
	 * @param p
	 *            参数
	 * @return condition
	 */
	private UserBySelectCondition initCondition(short role, UserInfoVo currentUser, String p) {
		log.info("initCondition|start|role=" + role);
		List<RoleInfo> list = new ArrayList<RoleInfo>();
		UserBySelectCondition condition = new UserBySelectCondition();
		String centerId = currentUser.getUserInfo().getCentersId();
		if (role == Role.CentreManager.getValue() || role == Role.EducatorSecondInCharge.getValue()) {// 如果为园长类的
			// 所有CEO
			list.add(new RoleInfo(Role.CEO.getValue()));
			// 所有园长
			list.add(new RoleInfo(Role.CentreManager.getValue()));
			// 所有二级园长
			list.add(new RoleInfo(Role.EducatorSecondInCharge.getValue()));

			// 园 所有老师
			list.add(new RoleInfo(centerId, Role.Educator.getValue()));
			list.add(new RoleInfo(centerId, Role.EducatorECT.getValue()));
			list.add(new RoleInfo(centerId, Role.EducatorCertifiedSupersor.getValue()));
			list.add(new RoleInfo(centerId, Role.EducatorFirstAidOfficer.getValue()));
			list.add(new RoleInfo(centerId, Role.EducatorNominatedSupervisor.getValue()));

			// 所有厨师
			list.add(new RoleInfo(Role.Cook.getValue()));

			// 所有临时工
			list.add(new RoleInfo(Role.Casual.getValue()));

			// 园所有小孩的家长
			// list.add(new RoleInfo(centerId, Role.Parent.getValue()));
		} else if (role == Role.Educator.getValue() || role == Role.EducatorECT.getValue() || role == Role.EducatorCertifiedSupersor.getValue()
				|| role == Role.EducatorFirstAidOfficer.getValue() || role == Role.EducatorNominatedSupervisor.getValue() || role == Role.Cook.getValue()) {
			list.add(new RoleInfo(Role.CEO.getValue()));
			list.add(new RoleInfo(centerId, Role.CentreManager.getValue()));
			list.add(new RoleInfo(centerId, Role.EducatorSecondInCharge.getValue()));

			// 园 所有老师
			list.add(new RoleInfo(centerId, Role.Educator.getValue()));
			list.add(new RoleInfo(centerId, Role.EducatorECT.getValue()));
			list.add(new RoleInfo(centerId, Role.EducatorCertifiedSupersor.getValue()));
			list.add(new RoleInfo(centerId, Role.EducatorFirstAidOfficer.getValue()));
			list.add(new RoleInfo(centerId, Role.EducatorNominatedSupervisor.getValue()));

			list.add(new RoleInfo(centerId, Role.Cook.getValue()));
		} else if (role == Role.Parent.getValue()) {
			list.add(new RoleInfo(centerId, Role.CentreManager.getValue()));
			list.add(new RoleInfo(centerId, Role.EducatorSecondInCharge.getValue()));
		} else if (role == Role.CEO.getValue()) {
			// ceo不给看小孩
			list.add(new RoleInfo(Role.Children.getValue()));
			list.add(new RoleInfo(Role.Parent.getValue()));
			condition.setOpenNotIn(1);
		} else if (role == Role.Casual.getValue()) {
			list.add(new RoleInfo(Role.Children.getValue()));
			list.add(new RoleInfo(Role.Parent.getValue()));
			condition.setOpenNotIn(1);
		}
		condition.setRoles(list);
		condition.setName(p);
		return condition;
	}

	/**
	 * 
	 * @description 根据角色获取组
	 * @author gfwang
	 * @create 2016年7月12日下午3:25:08
	 * @version 1.0
	 * @param p
	 *            参数
	 * @param currentUser
	 *            当前用户
	 * @return List<String>
	 */
	private Set<String> getMessageGroup(String p, String centerId, GroupType groupType, Short role) {
		String centerName = null;
		if (StringUtil.isNotEmpty(centerId)) {
			// 获取园区的等级
			centerName = centersInfoMapper.selectByPrimaryKey(centerId).getName();
		}
		// 获取角色
		String roleName = Role.getNameByValue(role).replaceAll(" ", "_");
		String groups = getAuthMsg(groupType.getName() + roleName);
		// 位Parent的时候分组为空
		if (StringUtil.isEmpty(groups)) {
			return new LinkedHashSet<String>();
		}
		groups = MessageFormat.format(groups, centerName, centerName);
		// 获取当前人的分组
		List<String> groupNames = new ArrayList<String>(Arrays.asList(groups.split("&&&")));
		// 获取××园长，××家长
		groupNames.addAll(addCentreGroup(centerName, groupType, role));
		if (StringUtil.isEmpty(p)) {
			return new MyListUtil<String>().list2Set((groupNames));
		}
		for (int i = groupNames.size() - 1; i >= 0; i--) {
			String group = groupNames.get(i);
			if (!group.toLowerCase().contains(p.toLowerCase())) {
				// 移除不匹配的项
				groupNames.remove(i);
			}
		}
		return new MyListUtil<String>().list2Set(groupNames);
	}

	/**
	 * 
	 * @description 动态获取家长分组，和老师分组
	 * @author gfwang
	 * @create 2016年8月19日下午2:01:08
	 * @version 1.0
	 * @param centerName
	 *            园区名称
	 * @return List<String>
	 */
	private List<String> addCentreGroup(String centerName, GroupType groupType, short role) {
		List<String> groups = new ArrayList<String>();
		if (groupType.getValue() == GroupType.MessageGroup.getValue()
				&& (role == Role.Casual.getValue() || role == Role.Parent.getValue() || role == Role.Educator.getValue() || role == Role.Cook.getValue())) {
			return groups;
		}
		// 如果有园区，则把配置的家长，老师转换为当前园区的
		if (StringUtil.isNotBlank(centerName)) {
			String parentGroup = MessageFormat.format(getAuthMsg("parent"), centerName);
			String educatorsGroup = MessageFormat.format(getAuthMsg("educators"), centerName);
			groups.add(parentGroup);
			groups.add(educatorsGroup);
			return groups;
		}
		// 没有园区，则获取所有园区家长，老师
		List<CentersInfo> centerList = centersInfoMapper.getCentersInfo(null, "true");
		for (CentersInfo center : centerList) {
			String name = center.getName();
			groups.add(MessageFormat.format(getAuthMsg("parent"), name));
			groups.add(MessageFormat.format(getAuthMsg("educators"), name));
		}
		return groups;
	}

	/**
	 * 
	 * @description 由于二级角色时需要用所属角色
	 * @author gfwang
	 * @create 2016年7月12日下午3:28:14
	 * @version 1.0
	 * @param currentUser
	 *            当前用户
	 * @return 最大角色
	 */
	private List<Short> initRole(UserInfoVo currentUser) {
		Set<Short> set = new LinkedHashSet<Short>();
		List<Short> list = new ArrayList<Short>();
		List<RoleInfoVo> roles = currentUser.getRoleInfoVoList();
		for (RoleInfoVo role : roles) {
			short currtenRole = role.getValue();
			if (currtenRole == Role.EducatorSecondInCharge.getValue()) {
				set.add(Role.CentreManager.getValue());
			} else if (currtenRole == Role.EducatorCertifiedSupersor.getValue() || currtenRole == Role.EducatorECT.getValue()
					|| currtenRole == Role.EducatorFirstAidOfficer.getValue() || currtenRole == Role.EducatorNominatedSupervisor.getValue()) {
				set.add(Role.Educator.getValue());
			} else {
				set.add(currtenRole);
			}
		}
		list.addAll(set);
		return list;
	}

	private String getAuthMsg(String tipKey) {
		return authProperties.getProperty(tipKey);
	}

	@Override
	public List<SelecterPo> getGroupsNews(String p, UserInfoVo currentUser) {
		log.info("getGroupsNews|p=" + p);
		// 初始化角色
		List<Short> list = initRole(currentUser);
		List<SelecterPo> groups = new ArrayList<SelecterPo>();
		Set<SelecterPo> temps = new LinkedHashSet<SelecterPo>();
		if (list.contains(Role.Casual.getValue())) {
			log.info("getGroupsNews|has casual");
			list.clear();
			list.add(Role.Casual.getValue());
		}
		for (Short role : list) {
			// temps.addAll(getMessageGroup(p,
			// currentUser.getUserInfo().getCentersId(),
			// GroupType.newsFeedGroup, role));
			temps.addAll(authGroupService.getGroupsByRole(currentUser, p, GroupType.newsFeedGroup));
		}
		groups.addAll(temps);
		return groups;
	}

	@Override
	public List<SelecterPo> getCenters(UserInfoVo currentUser, String p) {
		String centerId = "";
		if (currentUser != null) {
			centerId = currentUser.getUserInfo().getCentersId();
		}
		List<SelecterPo> list = centersInfoMapper.selecterCenters(centerId, p);
		return list;
	}

	@Override
	public List<SelecterPo> getRooms(UserInfoVo currentUser, String centerId, String p) {
		String roomId = currentUser.getUserInfo().getRoomId();
		List<SelecterPo> list = roomMapper.selecterRooms(centerId, roomId, p);
		return list;
	}

	@Override
	public List<SelecterPo> getGroups(UserInfoVo currentUser, String roomId, String p) {
		String groupId = currentUser.getUserInfo().getGroupId();
		return groupInfoMapper.selecterGroups(roomId, groupId, p);
	}

	@Override
	public List<SelecterPo> getStaff(UserInfoVo currentUser, String params, String centerId, int type) {
		// type ==1 不需要根据园长进行过滤
		UserBySelectCondition condition = new UserBySelectCondition();
		if (type != 1 && (isRole(currentUser, Role.CentreManager.getValue()) || isRole(currentUser, Role.EducatorSecondInCharge.getValue()))) {
			condition.setCenterId(currentUser.getUserInfo().getCentersId());
		}
		if (StringUtil.isNotEmpty(centerId)) {
			condition.setCenterId(centerId);
		}

		condition.setName(params);
		return userMapper.getStaff(condition);
	}

	@Override
	public List<SelecterPo> getStaff2(UserInfoVo currentUser, String params, String centerId, int type) {
		if (StringUtil.isEmpty(params)) {
			return new ArrayList<SelecterPo>();
		}
		// type ==1 不需要根据园长进行过滤
		UserBySelectCondition condition = new UserBySelectCondition();
		if (type != 1 && (isRole(currentUser, Role.CentreManager.getValue()) || isRole(currentUser, Role.EducatorSecondInCharge.getValue()))) {
			condition.setCenterId(currentUser.getUserInfo().getCentersId());
		}
		if (StringUtil.isNotEmpty(centerId)) {
			condition.setCenterId(centerId);
		}

		condition.setName(params);
		return userMapper.getStaff(condition);
	}

	@Override
	public List<SelecterPo> getStaffAndChild(UserInfoVo currentUser, String params, String centerId, int type) {
		if (StringUtil.isEmpty(params)) {
			return new ArrayList<SelecterPo>();
		}
		// type ==1 不需要根据园长进行过滤
		UserBySelectCondition condition = new UserBySelectCondition();
		if (type != 1 && (isRole(currentUser, Role.CentreManager.getValue()) || isRole(currentUser, Role.EducatorSecondInCharge.getValue()))) {
			condition.setCenterId(currentUser.getUserInfo().getCentersId());
		}
		if (StringUtil.isNotEmpty(centerId)) {
			condition.setCenterId(centerId);
		}

		condition.setName(params);
		List<SelecterPo> staffList = userMapper.getStaff(condition);
		List<SelecterPo> childList = userMapper.getChild(condition);
		childList.addAll(staffList);
		return childList;
	}

	@Override
	public List<SelecterPo> getStffByChoooseCenter(String centerId, String p) {
		// 根据选择园区，查询园区所有员工和兼职
		UserBySelectCondition condition = new UserBySelectCondition();
		condition.setCenterId(centerId);
		condition.setName(p);
		List<SelecterPo> list = userMapper.getStaffAndCasual(condition);
		return list;
	}

	/**
	 * @description 判断当前登录人角色值
	 * @author hxzhang
	 * @create 2016年7月20日下午7:36:08
	 * @version 1.0
	 * @param currentUser
	 *            当前登录人
	 * @param role
	 *            角色值
	 * @return 返回结果
	 */
	private boolean isRole(UserInfoVo currentUser, Short role) {
		List<RoleInfoVo> roleList = currentUser.getRoleInfoVoList();
		for (RoleInfoVo roleInfoVo : roleList) {
			if (role.compareTo(roleInfoVo.getValue()) == 0) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @description 初始化AttachmentInfo
	 * @author hxzhang
	 * @create 2016年7月20日下午2:43:57
	 * @version 1.0
	 * @param newsId
	 *            微博ID
	 * @param sourceId
	 *            附件源ID
	 * @param fileVoList
	 *            上传文件集合
	 * @throws Exception
	 *             异常
	 */
	@Override
	public void initAttachmentInfo(String sourceId, List<FileVo> fileVoList, String filePath) throws Exception {
		if (ListUtil.isEmpty(fileVoList)) {
			return;
		}
		FileFactory fac = new FileFactory(Region.getRegion(Regions.AP_SOUTHEAST_2), PropertieNameConts.S3);
		List<AttachmentInfo> attachmentInfos = new ArrayList<AttachmentInfo>();
		for (FileVo fileVo : fileVoList) {
			AttachmentInfo attachmentInfo = new AttachmentInfo();
			attachmentInfo.setId(UUID.randomUUID().toString());
			attachmentInfo.setSourceId(sourceId);

			String tempDir = fileVo.getRelativePathName();
			if (StringUtil.isEmpty(tempDir)) {
				continue;
			}
			String mainDir = FilePathUtil.getMainPath(filePath, tempDir);
			String rePath = fac.copyFile(tempDir, mainDir);
			attachmentInfo.setAttachId(rePath);

			attachmentInfo.setAttachName(fileVo.getFileName());
			attachmentInfo.setVisitUrl(fileVo.getVisitedUrl());
			attachmentInfo.setDeleteFlag((short) 0);
			attachmentInfos.add(attachmentInfo);
		}
		if (!ListUtil.isEmpty(attachmentInfos)) {
			attachmentInfoMapper.insterBatchAttachmentInfo(attachmentInfos);
		}
	}

	@Override
	public String getPersonColor(UserInfo userInfo) {
		log.info("getPersonColor|start");
		String familyId = userInfo.getFamilyId();
		Short userType = userInfo.getUserType();
		String systemColor = getSystemMsg("user.color");
		String[] colors = systemColor.split(",");
		// 第一个添加小孩时 取第一个颜色
		if (userType == UserType.Child.getValue() && StringUtil.isEmpty(familyId)) {
			return colors[0];
		}
		// 获取数据库中最后添加的员工/家长/小孩的personColor
		String personColor = userMapper.getPersonColorByUserInfo(userInfo);
		log.info("getPersonColor|personColor=" + personColor);
		if (StringUtil.isEmpty(personColor)) {
			return colors[0];
		}
		// 获取当前颜色，返回当前颜色的下一个颜色
		if (!StringUtil.isEmpty(personColor)) {
			for (int i = 0; i < colors.length; i++) {
				if (colors[i].equals(personColor)) {
					if (i == (colors.length - 1)) {
						return colors[0];
					}
					return colors[i + 1];
				}
			}
		}
		log.info("getPersonColor|end");
		return colors[0];
	}

	@Override
	public StaffRoleInfoVo getMenuOrgs(Short type, UserInfoVo currtenUser) {
		log.info("getMenuOrgs|satrt|type=" + type);
		StaffRoleInfoVo orgs = new StaffRoleInfoVo();
		MenuOrgsCondition condition = new MenuOrgsCondition();
		initMenuOrgsCondition(condition, type, currtenUser);
		List<RoomInfo> rooms = new ArrayList<RoomInfo>();
		List<CentersInfo> centers = null;
		if (type == FunctionType.RosteringList.getValue()) {
			condition.setWeekStart(DateUtil.getWeekStart(new Date()));
			centers = staffMapper.selectCentersWithRoster(condition);
		} else {
			centers = staffMapper.selectCenters(condition);
		}
		if (type == FunctionType.WatingList.getValue()) {
			rooms = staffMapper.selectRoomWithAttendance(condition);
			dealCentersWithAttendance(rooms, centers);
		} else {
			rooms = staffMapper.selectRoom(condition);
		}

		for (CentersInfo c : centers) {
			if (!getSystemMsg("rydeId").equals(c.getId())) {
				continue;
			}
			c.setRyde(true);
		}

		orgs.setCentersInfos(centers);
		orgs.setRoomInfos(rooms);
		log.info("getMenuOrgs|end|type=" + type);
		return orgs;
	}

	private void dealCentersWithAttendance(List<RoomInfo> rooms, List<CentersInfo> centers) {
		if (ListUtil.isEmpty(centers)) {
			return;
		}
		for (CentersInfo centersInfo : centers) {
			for (RoomInfo roomInfo : rooms) {
				if (roomInfo.getCentersId().equals(centersInfo.getId())) {
					centersInfo.setShouldCome(centersInfo.getShouldCome() + roomInfo.getShouldCome());
					centersInfo.setRealCome(centersInfo.getRealCome() + roomInfo.getRealCome());
				}
			}
		}
	}

	/**
	 * 
	 * @description 初始化组织信息查询条件
	 * @author gfwang
	 * @create 2016年9月8日下午7:21:12
	 * @version 1.0
	 * @param condition
	 * @param type
	 * @param currtenUser
	 */
	private void initMenuOrgsCondition(MenuOrgsCondition condition, Short type, UserInfoVo currtenUser) {
		if (null == type) {
			return;
		}
		List<RoleInfoVo> roleList = currtenUser.getRoleInfoVoList();
		if (type == FunctionType.Fmily.getValue()) {
			// 如果家长
			if (containRoles(currtenUser.getRoleInfoVoList(), Role.Parent)) {
				condition.setCenterIds(currtenUser.getParentCenterIds());
				condition.setRoomIds(currtenUser.getParentRoomsIds());
				// 开启条件查询
				condition.setOpenCondtion(1);
			} else if (containRoles(roleList, Role.Educator, Role.Cook, Role.EducatorCertifiedSupersor, Role.EducatorECT, Role.EducatorFirstAidOfficer)
					&& !containRoles(roleList, Role.EducatorSecondInCharge, Role.CentreManager)) {// 如果是老师角色，且不是二级园长
				String centerId = currtenUser.getUserInfo().getCentersId();
				String roomId = currtenUser.getUserInfo().getRoomId();
				List<String> centerIds = new ArrayList<String>();
				centerIds.add(centerId);
				condition.setCenterIds(centerIds);
				List<String> roomIds = new ArrayList<String>();
				roomIds.add(roomId);
				condition.setRoomIds(roomIds);
				// 开启条件查询
				condition.setOpenCondtion(1);
			}
		} else if (type == FunctionType.VisitorList.getValue()) {
			if (containRoles(roleList, Role.CentreManager, Role.EducatorSecondInCharge)) {
				String centerId = currtenUser.getUserInfo().getCentersId();
				if (StringUtil.isEmpty(centerId)) {
					// 处理 园长但是没有园的问题（查出所有）
					return;
				}
				String roomId = currtenUser.getUserInfo().getRoomId();
				List<String> centerIds = new ArrayList<String>();
				centerIds.add(centerId);
				condition.setCenterIds(centerIds);
				List<String> roomIds = new ArrayList<String>();
				roomIds.add(roomId);
				condition.setRoomIds(roomIds);
				// 开启条件查询
				condition.setOpenCondtion(1);
			}
		} else if (type == FunctionType.WatingList.getValue()) {
			if (containRoles(roleList, Role.CentreManager, Role.Educator)) {
				String centerId = currtenUser.getUserInfo().getCentersId();
				if (StringUtil.isEmpty(centerId)) {
					return;
				}
				List<String> centerIds = new ArrayList<String>();
				centerIds.add(centerId);
				condition.setCenterIds(centerIds);
				List<RoomInfoVo> roomVos = roomMapper.selectRoomsByCenterId(centerId);
				List<String> roomIds = new ArrayList<String>();
				for (RoomInfoVo room : roomVos) {
					roomIds.add(room.getId());
				}
				condition.setRoomIds(roomIds);
				// 开启条件查询
				condition.setOpenCondtion(1);
			}
		} else if (type == FunctionType.ProgramList.getValue()) {
			if (containRoles(roleList, Role.CEO)) {
				return;
			}
			String centerId = currtenUser.getUserInfo().getCentersId();
			condition.setCenterIds(Arrays.asList(new String[] { centerId }));

			String currtenRoomId = currtenUser.getUserInfo().getRoomId();
			List<RoomInfoVo> roomVos = roomMapper.selectRoomsByCenterId(centerId);
			List<String> roomIds = new ArrayList<String>();
			if (containRoles(roleList, Role.CentreManager, Role.EducatorSecondInCharge)) {
				for (RoomInfoVo room : roomVos) {
					roomIds.add(room.getId());
				}
			} else {
				for (RoomInfoVo room : roomVos) {
					if (room.getId().equals(currtenRoomId))
						roomIds.add(room.getId());
				}
			}
			condition.setRoomIds(roomIds);
			// 开启条件查询
			condition.setOpenCondtion(1);
		} else if (type == FunctionType.RosteringList.getValue()) {
			// ceo 兼职 可以看到所有园，园长，其它员工只能看到自己园，家长只能看到自己小孩所在的园
			if (containRoles(roleList, Role.CEO, Role.Casual, Role.CentreManager, Role.EducatorSecondInCharge)) {
				return;
			}
			condition.setOpenCondtion(1);
			if (containRoles(roleList, Role.Parent)) {
				condition.setCenterIds(currtenUser.getParentCenterIds());
				return;
			}
			condition.setCenterIds(Arrays.asList(new String[] { currtenUser.getUserInfo().getCentersId() }));
		} else if (type == FunctionType.NewsFeed.getValue()) {
			if (!containRoles(roleList, Role.Parent)) {
				return;
			}
			List<String> centerIds = new ArrayList<String>();
			String familyId = currtenUser.getUserInfo().getFamilyId();
			centerIds = staffMapper.getCentersByFamilyId(familyId);
			List<String> roomIds = new ArrayList<String>();
			roomIds = staffMapper.getRoomsByFamilyId(familyId);
			condition.setCenterIds(centerIds);
			condition.setRoomIds(roomIds);
			condition.setOpenCondtion(1);

			// TODO
		}
	}

	@Override
	public ServiceResult<BatchFileVo> uploadBatch(MultipartHttpServletRequest request, String sourceId, String dir) {
		if (StringUtil.isEmpty(sourceId)) {
			sourceId = UUID.randomUUID().toString();
		}
		BatchFileVo batchFile = new BatchFileVo();
		List<FileVo> fileVos = new ArrayList<FileVo>();
		FileFactory fac = null;
		try {
			fac = new FileFactory(Region.getRegion(Regions.AP_SOUTHEAST_2), PropertieNameConts.S3);
		} catch (FileNotFoundException e) {
			log.error("s3 fail", e);
			return failResult("upload time out");
		}
		Map<String, MultipartFile> map = request.getFileMap();
		// 遍历文件集合
		for (MultipartFile file : map.values()) {
			// 文件名
			String uploadFileName = file.getOriginalFilename();
			// 后缀
			String ext = FileUtil.getExtensionName(uploadFileName);
			String uploadFileId = UUID.randomUUID().toString() + "." + ext;
			// 上传s3
			String relaPath = "";
			try {
				// 上傳文件的相對路徑
				relaPath = fac.uploadFile(dir + uploadFileId, file.getInputStream());
			} catch (Exception e) {
				log.error("op", e);
				return failResult("upload time out");
			}
			fileVos.add(new FileVo(uploadFileId, uploadFileName, relaPath, ""));
		}
		batchFile.setSourceId(sourceId);
		insertFile(fileVos, sourceId);
		batchFile.setFiles(fileVos);
		return successResult(batchFile);
	}

	private void insertFile(List<FileVo> fileVos, String sourceId) {
		if (ListUtil.isEmpty(fileVos)) {
			return;
		}
		List<AttachmentInfo> list = new ArrayList<AttachmentInfo>();
		for (FileVo file : fileVos) {
			AttachmentInfo attach = new AttachmentInfo();
			String id = UUID.randomUUID().toString();
			attach.setId(id);
			attach.setAttachName(file.getFileName());
			attach.setSourceId(sourceId);
			attach.setDeleteFlag(DeleteFlag.Default.getValue());
			attach.setAttachId(file.getRelativePathName());
			attach.setAttachType((short) 1);
			list.add(attach);
			file.setId(id);
		}
		attachmentInfoMapper.insterBatchAttachmentInfo(list);
	}

	/*
	 * if (!getSystemMsg("attachFormat").contains(ext.toLowerCase())) {
	 * result.setMsg(getTipMsg("upload_attacherror"));
	 * result.setReturnObj(batchFile); return result; }
	 */

	@Override
	public int deleteFile(List<String> relativePathNames) {
		int count = attachmentInfoMapper.deleteFileByList(relativePathNames);
		log.info("deleteFile|count=" + count);
		return count;
	}

	@Override
	public void buildUserProfileRelation(String userId, String profileId, String valueId) {
		if (StringUtil.isEmpty(profileId)) {
			return;
		}
		ChildFormRelationInfo db = childFormRelationInfoMapper.getByUserId(userId);
		if (db == null) {
			ChildFormRelationInfo record = new ChildFormRelationInfo();
			record.setUserId(userId);
			record.setTemplateId(profileId);
			record.setValueId(valueId);
			childFormRelationInfoMapper.insert(record);
		} else {
			db.setTemplateId(profileId);
			db.setValueId(valueId);
			childFormRelationInfoMapper.updateByUserId(db);
		}
	}

}
