package com.aoyuntek.aoyun.service.event.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.aoyuntek.aoyun.condtion.EventCondition;
import com.aoyuntek.aoyun.condtion.GalleryCondition;
import com.aoyuntek.aoyun.dao.AttachmentInfoMapper;
import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.ClosurePeriodMapper;
import com.aoyuntek.aoyun.dao.FamilyInfoMapper;
import com.aoyuntek.aoyun.dao.NqsRelationInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.dao.event.EventChildInfoMapper;
import com.aoyuntek.aoyun.dao.event.EventDocumentsInfoMapper;
import com.aoyuntek.aoyun.dao.event.EventInfoMapper;
import com.aoyuntek.aoyun.entity.po.AttachmentInfo;
import com.aoyuntek.aoyun.entity.po.CentersInfo;
import com.aoyuntek.aoyun.entity.po.NqsRelationInfo;
import com.aoyuntek.aoyun.entity.po.event.EventChildInfo;
import com.aoyuntek.aoyun.entity.po.event.EventDocumentsInfo;
import com.aoyuntek.aoyun.entity.po.event.EventInfo;
import com.aoyuntek.aoyun.entity.vo.NqsVo;
import com.aoyuntek.aoyun.entity.vo.RoleInfoVo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.event.EventDocumentVo;
import com.aoyuntek.aoyun.entity.vo.event.EventInfoVo;
import com.aoyuntek.aoyun.entity.vo.event.GalleryInfoVo;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.enums.UserType;
import com.aoyuntek.aoyun.service.event.IEventService;
import com.aoyuntek.aoyun.service.impl.BaseWebServiceImpl;
import com.aoyuntek.aoyun.service.policy.impl.PolicyServiceImpl;
import com.aoyuntek.aoyun.uitl.FilePathUtil;
import com.aoyuntek.framework.constants.PropertieNameConts;
import com.aoyuntek.framework.model.Pager;
import com.theone.aws.util.FileFactory;
import com.theone.common.util.ServiceResult;
import com.theone.date.util.DateUtil;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

@Service
public class EventServiceImpl extends BaseWebServiceImpl<EventInfo, EventInfoMapper> implements IEventService {

	/**
	 * 日志
	 */
	public static Logger logger = Logger.getLogger(PolicyServiceImpl.class);
	@Autowired
	private EventInfoMapper eventInfoMapper;
	@Autowired
	private NqsRelationInfoMapper nqsRelationInfoMapper;
	@Autowired
	private AttachmentInfoMapper attachmentInfoMapper;
	@Autowired
	private EventDocumentsInfoMapper eventDocumentsInfoMapper;
	@Autowired
	private EventChildInfoMapper eventChildInfoMapper;
	@Autowired
	private UserInfoMapper userInfoMapper;
	@Autowired
	private FamilyInfoMapper familyInfoMapper;
	@Autowired
	private CentersInfoMapper centersInfoMapper;
	@Autowired
	private ClosurePeriodMapper closurePeriodMapper;

	@Override
	public ServiceResult<EventInfoVo> getEventInfo(String id, UserInfoVo currentUser) {
		EventInfoVo eventInfoVo = new EventInfoVo();
		if (!StringUtil.isEmpty(id)) {
			eventInfoVo = eventInfoMapper.selectEventInfoVoById(id);
			if (null == eventInfoVo) {
				return failResult(getTipMsg("operation_failed"));
			}
		}
		return successResult(getTipMsg("operation_success"), eventInfoVo);
	}

	@Override
	public ServiceResult<Pager<EventInfoVo, EventCondition>> getEventList(EventCondition eventCondition, UserInfoVo currentUser) {
		dealCondition(eventCondition);
		eventCondition.setStartSize(eventCondition.getPageIndex());
		List<EventInfoVo> eventInfoList = new ArrayList<EventInfoVo>();
		List<RoleInfoVo> roleInfoVoList = currentUser.getRoleInfoVoList();
		boolean isCEOORCentreManager = false;
		List<String> centerIds = new ArrayList<String>();
		// 设置当前登录者角色集合
		for (int i = 0, m = roleInfoVoList.size(); i < m; i++) {
			// 判断是否是CEO,ceo可以查看所有
			if (roleInfoVoList.get(i).getValue() == Role.CEO.getValue()) {
				eventCondition.setIsCeo(true);
				isCEOORCentreManager = true;
				break;
			}
			// 园长及二级园长只能查看本园区
			if (roleInfoVoList.get(i).getValue() == Role.CentreManager.getValue() || roleInfoVoList.get(i).getValue() == Role.EducatorSecondInCharge.getValue()) {
				centerIds.add(currentUser.getUserInfo().getCentersId());
				eventCondition.setCenterIds(centerIds);
				isCEOORCentreManager = true;
				break;
			}
		}
		if (!isCEOORCentreManager) {
			// 判断当前员工是家长:1 还是员工:0
			if (UserType.Parent.getValue().equals(currentUser.getUserInfo().getUserType().shortValue())) {
				// 获取该家庭所有小孩
				centerIds = familyInfoMapper.getChildsByFamilyId(currentUser.getUserInfo().getFamilyId());
				eventCondition.setCenterIds(centerIds);
				eventCondition.setVisibility((short) 2);
			}
			// 员工只能查看本园区
			if (UserType.Staff.getValue().equals(currentUser.getUserInfo().getUserType().shortValue())) {
				centerIds.add(currentUser.getUserInfo().getCentersId());
				eventCondition.setCenterIds(centerIds);
				eventCondition.setVisibility((short) 1);
			}
		}
		eventCondition.setCreateAccountId(currentUser.getAccountInfo().getId());
		eventInfoList = eventInfoMapper.selectEventInfoVoList(eventCondition);

		int totalSize = eventInfoMapper.selectEventInfoVoListCount(eventCondition);
		eventCondition.setTotalSize(totalSize);

		Pager<EventInfoVo, EventCondition> pager = new Pager<EventInfoVo, EventCondition>(eventInfoList, eventCondition);
		return successResult(getTipMsg("operation_success"), pager);
	}

	private void dealCondition(EventCondition condition) {
		if (StringUtil.isNotEmpty(condition.getNqsVersion())) {
			if (condition.getNqsVersion().toUpperCase().equals("QA1")) {
				condition.setSelectNqs("1");
			} else if (condition.getNqsVersion().toUpperCase().equals("QA2")) {
				condition.setSelectNqs("2");
			} else if (condition.getNqsVersion().toUpperCase().equals("QA3")) {
				condition.setSelectNqs("3");
			} else if (condition.getNqsVersion().toUpperCase().equals("QA4")) {
				condition.setSelectNqs("4");
			} else if (condition.getNqsVersion().toUpperCase().equals("QA5")) {
				condition.setSelectNqs("5");
			} else if (condition.getNqsVersion().toUpperCase().equals("QA6")) {
				condition.setSelectNqs("6");
			} else if (condition.getNqsVersion().toUpperCase().equals("QA7")) {
				condition.setSelectNqs("7");
			} else {
				condition.setSelectNqs(condition.getNqsVersion());
			}
		} else {
			condition.setSelectNqs(null);
		}
	}

	@Override
	public ServiceResult<Pager<GalleryInfoVo, GalleryCondition>> getGalleryList(GalleryCondition galleryCondition, UserInfoVo currentUser) {
		galleryCondition.setStartSize(galleryCondition.getPageIndex());
		List<GalleryInfoVo> galleryInfoList = new ArrayList<GalleryInfoVo>();

		galleryInfoList = eventInfoMapper.selectGalleryInfoVoList(galleryCondition);

		int totalSize = eventInfoMapper.selectGalleryInfoVoListCount(galleryCondition);
		galleryCondition.setTotalSize(totalSize);

		Pager<GalleryInfoVo, GalleryCondition> pager = new Pager<GalleryInfoVo, GalleryCondition>(galleryInfoList, galleryCondition);
		return successResult(getTipMsg("operation_success"), pager);
	}

	@Override
	public ServiceResult<Object> getChildIdByFamilyId(String familyId) {
		List<String> childIds = new ArrayList<String>();
		childIds = userInfoMapper.getChildIdByFamilyId(familyId);

		return successResult(getTipMsg("operation_success"), (Object) childIds);
	}

	@Override
	public ServiceResult<Object> addOrUpdateEvent(EventInfoVo eventInfoVo, UserInfoVo currentUser) {
		String id = null;
		// 验证数据合法性
		ServiceResult<Object> result = validateAddOrUpdate(eventInfoVo);
		if (!result.isSuccess()) {
			return result;
		}
		// 验证创建时间是否为公共假期
		ServiceResult<Object> validateResult = validateClosurePeriod(eventInfoVo);
		if (!validateResult.isSuccess() && !eventInfoVo.isConfirm()) {
			return validateResult;
		}
		// 处理event主体
		EventInfo eventInfo = operationEventInfo(eventInfoVo, currentUser);
		// 处理nqs
		operationNqs(eventInfo.getId(), eventInfoVo.getNqsInfo());
		// 处理附件
		try {
			operationDoc(eventInfo.getId(), eventInfoVo.getEventDocumentVoList(), currentUser);
		} catch (Exception e) {
			logger.error("Event_file_upload_failed||", e);
			throw new RuntimeException();
		}

		return successResult(getTipMsg("operation_success"), (Object) id);
	}

	private ServiceResult<Object> validateClosurePeriod(EventInfoVo vo) {
		if (StringUtil.isNotEmpty(vo.getId())) {
			return successResult();
		}
		int count = closurePeriodMapper.getRepeatBycentreIdDate(vo.getCenterId(), vo.getStartTime(), vo.getEndTime());
		if (count > 0) {
			return failResult(2);
		}
		return successResult();
	}

	/**
	 * 处理上传附件
	 * 
	 * @param id
	 * @param policyDocList
	 */
	private void operationDoc(String eventId, List<EventDocumentVo> eventDocList, UserInfoVo currentUser) throws Exception {
		if (null == eventDocList || eventDocList.size() == 0) {
			return;
		}
		List<String> ids = new ArrayList<String>();

		// 处理被页面删除的附件
		for (int i = eventDocList.size() - 1; i >= 0; i--) {
			// 如果页面标记为delete则处理删除
			if (eventDocList.get(i).getDeleteFlag() == 1) {
				if (null == eventDocList.get(i).getDocumentsId() || StringUtil.isEmpty(eventDocList.get(i).getDocumentsId())) {
					eventDocList.remove(i);
					continue;
				}
				attachmentInfoMapper.deleteBatchAttachmentInfo(null, eventDocList.get(i).getDocumentsId());

				ids.add(eventDocList.get(i).getId());

				eventDocList.remove(i);
			}
		}
		if (ids.size() != 0) {
			// 批量删除小孩
			eventChildInfoMapper.deleteDocChildsByIds(ids);
			// 批量虚拟删除附件
			eventDocumentsInfoMapper.deleteDocByIds(ids);
		}

		// 新增EVENT_PATH
		int j = 0;
		FileFactory fac = new FileFactory(Region.getRegion(Regions.AP_SOUTHEAST_2), PropertieNameConts.S3);
		List<AttachmentInfo> attachmentInfos = new ArrayList<AttachmentInfo>();
		List<EventDocumentsInfo> eventDocumentsInfos = new ArrayList<EventDocumentsInfo>();
		List<EventChildInfo> eventChildInfos = new ArrayList<EventChildInfo>();
		List<EventChildInfo> eventChildInfosDelete = new ArrayList<EventChildInfo>();
		// 新增附件
		for (int i = eventDocList.size() - 1; i >= 0; i--) {
			Date time = new Date();
			j++;
			EventDocumentVo eventDocumentVo = eventDocList.get(i);
			if (StringUtil.isEmpty(eventDocumentVo.getId()) && eventDocumentVo.getAttachId().indexOf(FilePathUtil.EVENT_PATH) == -1) {
				// 创建附件主表
				EventDocumentsInfo eventDocumentsInfo = new EventDocumentsInfo();
				eventDocumentsInfo.setId(UUID.randomUUID().toString());
				eventDocumentsInfo.setEventId(eventId);
				eventDocumentsInfo.setDocumentsId(UUID.randomUUID().toString());
				eventDocumentsInfo.setCreateTime(DateUtil.addMinius(time, j));
				eventDocumentsInfo.setUpdateTime(DateUtil.addMinius(time, j));
				eventDocumentsInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
				eventDocumentsInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
				eventDocumentsInfo.setDeleteFlag(DeleteFlag.Default.getValue());
				eventDocumentsInfos.add(eventDocumentsInfo);

				// 创建附件表
				AttachmentInfo attachmentInfo = new AttachmentInfo();
				attachmentInfo.setId(UUID.randomUUID().toString());
				attachmentInfo.setSourceId(eventDocumentsInfo.getDocumentsId());
				// 拷贝文件
				String tempDir = eventDocumentVo.getAttachId();
				String mainDir = FilePathUtil.getMainPath(FilePathUtil.EVENT_PATH, tempDir);
				String rePath = fac.copyFile(tempDir, mainDir);

				attachmentInfo.setAttachId(rePath);
				attachmentInfo.setAttachName(eventDocumentVo.getFileName());
				// attachmentInfo.setVisitUrl(policyDocVo.getVisitUrl());
				attachmentInfo.setDeleteFlag(DeleteFlag.Default.getValue());
				attachmentInfos.add(attachmentInfo);

				// 创建小孩关系
				for (int m = 0, n = eventDocumentVo.getChildSelectList().size(); m < n; m++) {
					SelecterPo selectChild = eventDocumentVo.getChildSelectList().get(m);

					EventChildInfo eventChildInfo = new EventChildInfo();
					eventChildInfo.setEventDocId(eventDocumentsInfo.getId());
					eventChildInfo.setChildId(selectChild.getId());
					eventChildInfo.setCreateTime(DateUtil.addMinius(time, j));
					eventChildInfo.setUpdateTime(DateUtil.addMinius(time, j));
					eventChildInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
					eventChildInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
					eventChildInfo.setDeleteFlag(DeleteFlag.Default.getValue());
					eventChildInfos.add(eventChildInfo);
					j++;
				}
			} else {
				// 编辑附件小孩选项
				String eventDocId = eventDocumentVo.getId();

				List<EventChildInfo> eventChildInfosDB = eventChildInfoMapper.getListByDocId(eventDocId);

				for (int m = eventDocumentVo.getChildSelectList().size() - 1; m >= 0; m--) {
					SelecterPo selectChild = eventDocumentVo.getChildSelectList().get(m);
					int k = 0;
					int l = eventChildInfosDB.size();
					for (k = 0; k < l; k++) {
						if (eventChildInfosDB.get(k).getChildId().equals(selectChild.getId())) {
							// 如果该childId相等，则认为编辑，则不需要修改
							// 删除数据库集合已经处理的
							eventChildInfosDB.remove(k);
							break;
						}
					}
					if (k >= l) {
						// 说明没有找到则需要新增
						EventChildInfo eventChildInfo = new EventChildInfo();

						eventChildInfo.setEventDocId(eventDocId);
						eventChildInfo.setChildId(selectChild.getId());
						eventChildInfo.setCreateTime(DateUtil.addMinius(time, j));
						eventChildInfo.setUpdateTime(DateUtil.addMinius(time, j));
						eventChildInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
						eventChildInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
						eventChildInfo.setDeleteFlag(DeleteFlag.Default.getValue());
						// 加入批量插入中
						eventChildInfos.add(eventChildInfo);
						j++;
					}
				}

				// eventChildInfosDB剩余的是要进行数据库删除
				eventChildInfosDelete.addAll(eventChildInfosDB);

			}
		}
		if (attachmentInfos.size() != 0) {
			attachmentInfoMapper.insterBatchAttachmentInfo(attachmentInfos);
		}
		if (eventDocumentsInfos.size() != 0) {
			eventDocumentsInfoMapper.insterBatchEventDocInfo(eventDocumentsInfos);
		}
		if (eventChildInfos.size() != 0) {
			eventChildInfoMapper.insterBatchEventDocChildInfo(eventChildInfos);
		}
		if (eventChildInfosDelete.size() != 0) {
			// 批量虚拟删除附件
			eventChildInfoMapper.deleteDocByEventChildInfos(eventChildInfosDelete);
		}
	}

	/**
	 * 处理NQS记录
	 * 
	 * @param policyId
	 * @param nqsVoList
	 */
	private void operationNqs(String eventId, List<NqsVo> nqsVoList) {
		if (ListUtil.isEmpty(nqsVoList)) {
			return;
		}
		// 删除之前的关系
		nqsRelationInfoMapper.removeNqsRelationByObjId(eventId);

		List<NqsRelationInfo> nqsNewsInfoList = new ArrayList<NqsRelationInfo>();
		for (NqsVo nqs : nqsVoList) {
			NqsRelationInfo nqsNewsInfo = new NqsRelationInfo();
			nqsNewsInfo.setId(UUID.randomUUID().toString());
			nqsNewsInfo.setObjId(eventId);
			nqsNewsInfo.setNqsVersion(nqs.getVersion());
			nqsNewsInfo.setDeleteFlag(DeleteFlag.Default.getValue());
			nqsNewsInfo.setTag(nqs.getTag());
			nqsNewsInfoList.add(nqsNewsInfo);
		}
		nqsRelationInfoMapper.insterBatchNqsRelationInfo(nqsNewsInfoList);
	}

	/**
	 * 处理eventInfo
	 * 
	 * @param eventInfoVo
	 * @param currentUser
	 * @return
	 */
	private EventInfo operationEventInfo(EventInfoVo eventInfoVo, UserInfoVo currentUser) {
		EventInfo eventInfo = new EventInfo();
		// 判断是否是新增
		Date time = new Date();
		if (StringUtil.isEmpty(eventInfoVo.getId())) {
			// 新增
			eventInfo.setId(UUID.randomUUID().toString());
			eventInfo.setTitle(eventInfoVo.getTitle());
			eventInfo.setDescription(eventInfoVo.getDescription());
			eventInfo.setVisibility(eventInfoVo.getVisibility());
			eventInfo.setStartTime(eventInfoVo.getStartTime());
			eventInfo.setEndTime(eventInfoVo.getEndTime());
			if (null != currentUser) {
				eventInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
			}
			eventInfo.setCreateTime(time);
			eventInfo.setDeleteFlag(DeleteFlag.Default.getValue());
			eventInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
			eventInfo.setUpdateTime(time);
			eventInfo.setCenterId(eventInfoVo.getCenterId());

			eventInfoMapper.insert(eventInfo);
		} else {
			// 编辑
			eventInfo = eventInfoMapper.selectByPrimaryKey(eventInfoVo.getId());
			eventInfo.setTitle(eventInfoVo.getTitle());
			eventInfo.setDescription(eventInfoVo.getDescription());
			eventInfo.setVisibility(eventInfoVo.getVisibility());
			eventInfo.setStartTime(eventInfoVo.getStartTime());
			eventInfo.setEndTime(eventInfoVo.getEndTime());
			eventInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
			eventInfo.setUpdateTime(time);
			eventInfo.setCenterId(eventInfoVo.getCenterId());

			eventInfoMapper.updateByPrimaryKey(eventInfo);
		}
		return eventInfo;
	}

	/**
	 * 验证数据合法性
	 * 
	 * @param eventInfoVo
	 * @return
	 */
	private ServiceResult<Object> validateAddOrUpdate(EventInfoVo eventInfoVo) {
		// Title 必填
		if (StringUtil.isEmpty(eventInfoVo.getTitle())) {
			return failResult(getTipMsg("event_title_empty"));
		}
		// Date Range 必填
		if (eventInfoVo.getStartTime() == null) {
			return failResult(getTipMsg("event_range_empty"));
		}
		if (eventInfoVo.getEndTime() == null) {
			return failResult(getTipMsg("event_range_empty"));
		}
		// 判断结束时间是否大于开始时间 todo 页面已经做了

		// Nqs 必填
		if (null == eventInfoVo.getNqsInfo() || eventInfoVo.getNqsInfo().size() == 0) {
			return failResult(getTipMsg("event_nqs_empty"));
		}

		// Visibility 必填
		if (eventInfoVo.getVisibility() == null || StringUtil.isEmpty(eventInfoVo.getVisibility().toString())) {
			return failResult(getTipMsg("event_visibility_empty"));
		}

		return successResult();
	}

	@Override
	public ServiceResult<Object> getCenterList() {
		List<CentersInfo> list = centersInfoMapper.getAllCenterForEvent();
		return successResult((Object) list);
	}

	@Override
	public ServiceResult<Object> deleteEvent(String id, UserInfoVo currentUser) {
		// 只有CEO才有权限才能删除Event
		if (!containRoles(currentUser.getRoleInfoVoList(), Role.CEO)) {
			return failResult(getTipMsg("no.permissions"));
		}
		eventInfoMapper.deleteByPrimaryKey(id);
		return successResult();
	}

}
