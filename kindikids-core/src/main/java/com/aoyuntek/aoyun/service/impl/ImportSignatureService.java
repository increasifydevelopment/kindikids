package com.aoyuntek.aoyun.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.dao.oldIdSignatureMapper;
import com.aoyuntek.aoyun.dao.profile.ChildFormRelationInfoMapper;
import com.aoyuntek.aoyun.entity.po.InstanceAttributeInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.po.ValueSelectInfo;
import com.aoyuntek.aoyun.entity.po.ValueStringInfo;
import com.aoyuntek.aoyun.entity.po.ValueTableInfo;
import com.aoyuntek.aoyun.entity.po.ValueTextInfo;
import com.aoyuntek.aoyun.entity.po.oldIdSignature;
import com.aoyuntek.aoyun.entity.vo.FormAttributeInfoDataVO;
import com.aoyuntek.aoyun.entity.vo.InstanceInfoDataVO;
import com.aoyuntek.aoyun.entity.vo.ValueInfoVO;
import com.aoyuntek.aoyun.service.ICommonService;
import com.aoyuntek.aoyun.service.IFormInfoService;
import com.aoyuntek.aoyun.service.IImportSignatureService;
import com.theone.common.util.ServiceResult;

@Service
public class ImportSignatureService implements IImportSignatureService {

    private String code = "9476caec-7faf-4dfa-b2a8-96eb84c79cbc";
    private String attrId = "47ed57c9-a1c6-48db-89b7-2f3bdaf9169c";
    private String templId = "e852e58c-28e0-4183-b6bf-70411d8668b9";
    @Autowired
    private IFormInfoService formService;
    @Autowired
    private UserInfoMapper userInfoMapper;
    @Autowired
    private ICommonService commonService;
    @Autowired
    private oldIdSignatureMapper oldIdSignatureMapper;
    @Autowired
    private ChildFormRelationInfoMapper childFormRelationInfoMapper;

    @Override
    public void importData2Stage3() throws Exception {

        List<oldIdSignature> list = oldIdSignatureMapper.getList();

        for (oldIdSignature signature : list) {
            UserInfo userInfo = userInfoMapper.getUserInfoByOldId(signature.getOldId());
            if (userInfo == null) {
                System.err.println("Error Data = " + signature.getOldId());
                continue;
            }
            // ChildFormRelationInfo relationInfo = childFormRelationInfoMapper.getByUserId(userInfo.getId());
            int count = userInfoMapper.haveRelation(userInfo.getId());
            if (count != 0) {
                System.err.println("Exist Data = " + userInfo.getId());
                continue;
            }
            // 处理Form表单,并创建实例
            String instanceId = dealValueInfoVO(signature.getSignature());
            // 将孩子和Orientation建立关系
            commonService.buildUserProfileRelation(userInfo.getId(), templId, instanceId);
        }
    }

    private String dealValueInfoVO(String signature) throws Exception {
        // 获取模版信息Children Orientation Stage 3 - 2017
        ValueInfoVO valueInfoVO = new ValueInfoVO();
        List<ValueStringInfo> valueStringInfos = new ArrayList<ValueStringInfo>();
        List<ValueTableInfo> valueTableInfos = new ArrayList<ValueTableInfo>();
        List<ValueTextInfo> valueTextInfos = new ArrayList<ValueTextInfo>();
        List<ValueSelectInfo> valueSelectInfos = new ArrayList<ValueSelectInfo>();
        List<InstanceAttributeInfo> instanceAttrInfos = new ArrayList<InstanceAttributeInfo>();
        List<String> addFiles = new ArrayList<String>();
        List<String> removeFiles = new ArrayList<String>();

        InstanceInfoDataVO instanceInfoDataVO = (InstanceInfoDataVO) formService.getInstanceTemplate(code, null).getReturnObj();
        List<FormAttributeInfoDataVO> attributes = instanceInfoDataVO.getAttributes();
        for (int i = 0; i < attributes.size(); i++) {
            int type = attributes.get(i).getType();
            String formAttrId = attributes.get(i).getAttrId();

            if (type >= 1 && type <= 6 && type != 5 && type != 2) {
                ValueStringInfo stringInfo = new ValueStringInfo();
                stringInfo.setFormAttrId(formAttrId);
                valueStringInfos.add(stringInfo);
            }
            if (type == 2 || type == 8) {
                ValueTextInfo textInfo = new ValueTextInfo();
                textInfo.setFormAttrId(formAttrId);
                if (attrId.equals(formAttrId)) {
                    textInfo.setValue(signature);
                }
                valueTextInfos.add(textInfo);
            }
        }
        valueInfoVO.setValueSelectInfo(valueSelectInfos);
        valueInfoVO.setValueStringInfo(valueStringInfos);
        valueInfoVO.setValueTableInfo(valueTableInfos);
        valueInfoVO.setValueTextInfo(valueTextInfos);
        valueInfoVO.setInstanceAttrInfo(instanceAttrInfos);
        valueInfoVO.setAddFiles(addFiles);
        valueInfoVO.setRemoveFiles(removeFiles);
        valueInfoVO.setFormId(instanceInfoDataVO.getFormId());

        // 创建实例
        ServiceResult<String> formResult = formService.saveInstance(valueInfoVO);

        return formResult.getReturnObj();
    }
}
