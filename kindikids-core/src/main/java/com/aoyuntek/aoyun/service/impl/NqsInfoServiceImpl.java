package com.aoyuntek.aoyun.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.dao.NqsInfoMapper;
import com.aoyuntek.aoyun.entity.po.NqsInfo;
import com.aoyuntek.aoyun.entity.vo.NqsVo;
import com.aoyuntek.aoyun.enums.NqsTag;
import com.aoyuntek.aoyun.service.INqsInfoService;
import com.theone.date.util.DateUtil;

@Service
public class NqsInfoServiceImpl extends BaseWebServiceImpl<NqsInfo, NqsInfoMapper> implements INqsInfoService {

    @Autowired
    NqsInfoMapper nqsMapper;

    @Override
    public List<NqsVo> getList(Date date) {
        Date baseDate = DateUtil.parse(getSystemMsg("nqs_date"), "yyyy-MM-dd");
        if (date.before(baseDate)) {
            return getNqs4Tag(NqsTag.V1.getValue());
        }
        return getNqs4Tag(NqsTag.V2.getValue());
    }

    private List<NqsVo> getNqs4Tag(Short tag) {

        String cacheKey = tag.shortValue() == NqsTag.V1.getValue() ? "cache_Nqs_V1" : "cache_Nqs_V2";
        Element el = null;
        CacheManager manager = CacheManager.create();
        // 通过manager可以生成指定名称的Cache对象
        Cache cache = manager.getCache("myCache");
        // 如果在缓存中直接返回
        if (cache.isKeyInCache(cacheKey) && cache.get(cacheKey) != null) {
            el = cache.get(cacheKey);
            return (List<NqsVo>) el.getObjectValue();
        }
        List<NqsVo> nqsAll = new ArrayList<NqsVo>();
        List<String> rootNqs = nqsMapper.getRootNqs(tag);
        for (String name : rootNqs) {
            NqsVo vo = recursiveTree(name,tag);
            nqsAll.add(vo);
        }
        if (nqsAll != null && nqsAll.size() > 0) {
            el = new Element(cacheKey, nqsAll);
            // 存入缓存
            cache.put(el);
        }
        return nqsAll;
    }

    /**
     * 
     * @description 递归节点
     * @author gfwang
     * @create 2016年7月8日上午8:47:36
     * @version 1.0
     * @param version
     *            版本
     * @return NqsVo
     */
    private NqsVo recursiveTree(String version,Short tag) {
        // 根据vesion获取节点对象
        NqsVo node = nqsMapper.geNqsVo(version,tag);
        // 查询vesion下的所有子节点
        List<NqsVo> childTreeNodes = nqsMapper.queryChildNqsVo(version,tag);
        // 遍历子节点
        for (NqsVo child : childTreeNodes) {
            // 递归
            NqsVo n = recursiveTree(child.getVersion(),tag);
            node.getNqsChild().add(n);
        }
        return node;
    }

}
