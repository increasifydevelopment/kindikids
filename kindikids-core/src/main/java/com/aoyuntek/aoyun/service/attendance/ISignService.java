package com.aoyuntek.aoyun.service.attendance;

import java.util.Date;
import java.util.List;

import com.aoyuntek.aoyun.condtion.SignCondition;
import com.aoyuntek.aoyun.dao.SigninInfoMapper;
import com.aoyuntek.aoyun.entity.po.SignVisitorInfoWithBLOBs;
import com.aoyuntek.aoyun.entity.po.SigninInfo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.aoyun.entity.vo.SigninInfoVo;
import com.aoyuntek.aoyun.entity.vo.StaffSignInfoVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.service.IBaseWebService;
import com.theone.common.util.ServiceResult;

/**
 * @description 签到业务逻辑层接口
 * @author Hxzhang 2016年9月1日上午11:14:20
 * @version 1.0
 */
public interface ISignService extends IBaseWebService<SigninInfo, SigninInfoMapper> {

	void dealSyncChildAttendance(String hubworksId, Date date, String signinTime, String signoutTime);

	/**
	 * @description 清除未签出标识
	 * @author hxzhang
	 * @create 2016年12月26日上午10:45:35
	 */
	ServiceResult<Object> removeNotHaveSignout(String accountId);

	/**
	 * @description 签入
	 * @author hxzhang
	 * @create 2016年9月1日上午11:07:26
	 * @version 1.0
	 * @param signInfo
	 * @return
	 */
	public ServiceResult<Object> saveVisitorSignIn(SignVisitorInfoWithBLOBs sviWithBLOBs, String centreId);

	/**
	 * @description 签出
	 * @author hxzhang
	 * @create 2016年9月1日下午3:32:00
	 * @version 1.0
	 * @param sviWithBLOBs
	 * @param centreId
	 * @param currentUser
	 * @return
	 */
	public ServiceResult<Object> saveVisitorSignOut(SignVisitorInfoWithBLOBs sviWithBLOBs);

	/**
	 * @description 获取签入列表
	 * @author hxzhang
	 * @create 2016年9月1日下午3:53:09
	 * @version 1.0
	 * @param centreId
	 * @return
	 */
	public ServiceResult<Object> getVisitorSignInList(String centreId, String params);

	/**
	 * @description 获取签到信息
	 * @author hxzhang
	 * @create 2016年9月5日下午5:37:08
	 * @version 1.0
	 * @param id
	 * @return
	 */
	public ServiceResult<Object> getSignInInfo(String id);

	/**
	 * @description 编辑签到信息
	 * @author hxzhang
	 * @create 2016年9月6日上午8:20:50
	 * @version 1.0
	 * @param sviWithBLOBs
	 * @return
	 */
	public ServiceResult<Object> updateSignIn(SigninInfoVo signinInfoVo, UserInfoVo currentUser);

	/**
	 * @description 获取导出数据
	 * @author hxzhang
	 * @create 2016年9月6日下午8:24:24
	 * @version 1.0
	 * @param condition
	 * @param currentUser
	 * @return
	 */
	public ServiceResult<Object> getExport(SignCondition condition, UserInfoVo currentUser);

	/**
	 * @description 根据园区名称获取园区ID
	 * @author hxzhang
	 * @create 2016年9月10日上午11:14:32
	 * @version 1.0
	 * @param name
	 * @return
	 */
	public ServiceResult<Object> getCentreId(String name, String currtenIp);

	/**
	 * @description 保存员工签入信息
	 * @author hxzhang
	 * @create 2016年9月12日上午11:30:15
	 * @version 1.0
	 * @param staffInfo
	 * @param pwd
	 * @return
	 */
	public ServiceResult<Object> saveStaffSigninOrSignout(String id, String centreId, String psw, short SigninType);

	/**
	 * @description 通过centreId获取签到员工
	 * @author hxzhang
	 * @create 2016年9月12日下午2:08:16
	 * @version 1.0
	 * @param centreId
	 * @return
	 */
	public ServiceResult<Object> getStaffList(String centreId, String name, int flag);

	/**
	 * 
	 * @author dlli5 at 2016年12月29日上午9:22:48
	 * @param centreId
	 * @param name
	 * @param flag
	 * @return
	 */
	public ServiceResult<Object> getSignStaffList(String centreId, String name, boolean isSign);

	/**
	 * @description 保存小孩签入信息
	 * @author hxzhang
	 * @create 2016年9月12日下午2:48:57
	 * @version 1.0
	 * @param sciWithBLOBs
	 * @return
	 */
	public ServiceResult<Object> saveChildSignin(String id, String inName, boolean sunscreenApp);

	/**
	 * @description 保存小孩签出信息
	 * @author hxzhang
	 * @create 2016年9月12日下午3:29:07
	 * @version 1.0
	 * @param sciWithBLOBs
	 * @return
	 */
	public ServiceResult<Object> saveChildSignout(String id, String outName);

	/**
	 * @description 通过签到ID获取小孩的签入签出信息
	 * @author hxzhang
	 * @create 2016年9月12日下午3:58:50
	 * @version 1.0
	 * @param id
	 * @return
	 */
	public ServiceResult<Object> getChildSignInfo(String id, Date date, UserInfoVo currentUser);

	/**
	 * @description 保存小孩签到信息
	 * @author hxzhang
	 * @create 2016年9月12日下午4:37:50
	 * @version 1.0
	 * @param signinInfoVo
	 * @return
	 */
	public ServiceResult<Object> updateChildSign(SigninInfoVo signinInfoVo, UserInfoVo currentUser, boolean flag);

	/**
	 * 
	 * @description 获取导出小孩考勤下拉列表
	 * @author gfwang
	 * @create 2016年9月18日上午9:09:41
	 * @version 1.0
	 * @return
	 */
	List<SelecterPo> getAllChilds(String p, String orgId, Integer type);

	/**
	 * @description 导出小孩签到信息
	 * @author hxzhang
	 * @create 2016年9月18日下午2:14:46
	 * @version 1.0
	 * @param condition
	 * @return
	 */
	ServiceResult<Object> getChildExport(SignCondition condition);

	/**
	 * @description 小孩在18:00时未签出发送短信给CEO,园长和二级园长
	 * @author hxzhang
	 * @create 2016年9月19日下午4:59:22
	 * @version 1.0
	 * @param now
	 */
	void sendMsgByChildUnSignoutTimedTask(Date now);

	/**
	 * @description 通过员工的accountId获取其签入签出信息
	 * @author hxzhang
	 * @create 2016年9月20日上午11:16:41
	 * @version 1.0
	 * @param accountId
	 * @return
	 */
	ServiceResult<Object> getStaffSignInfo(String accountId, Date date, String centerId);

	/**
	 * @description 新增或编辑员工签到信息
	 * @author hxzhang
	 * @create 2016年9月20日下午3:17:01
	 * @version 1.0
	 * @param staffSignInfoVo
	 * @return
	 */
	ServiceResult<Object> addOrUpdateStaffSign(StaffSignInfoVo staffSignInfoVo, UserInfoVo currentUser, Boolean comfirmSure);

	/**
	 * @description 获取员工签到导出数据
	 * @author hxzhang
	 * @create 2016年9月20日下午3:57:07
	 * @version 1.0
	 * @param condition
	 * @return
	 */
	ServiceResult<Object> getStaffExport(SignCondition condition);
}
