package com.aoyuntek.aoyun.service.attendance;

import java.util.Date;
import java.util.List;

import com.aoyuntek.aoyun.condtion.LeaveCondition;
import com.aoyuntek.aoyun.dao.LeaveInfoMapper;
import com.aoyuntek.aoyun.entity.po.LeaveInfo;
import com.aoyuntek.aoyun.entity.po.LeavePaidInfo;
import com.aoyuntek.aoyun.entity.vo.LeaveListVo;
import com.aoyuntek.aoyun.entity.vo.LeaveVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.service.IBaseWebService;
import com.aoyuntek.framework.model.Pager;
import com.theone.common.util.ServiceResult;

/**
 * @description 请假业务层接口
 * @author hxzhang
 * @create 2016年8月16日上午11:37:33
 * @version 1.0
 */
public interface ILeaveService extends IBaseWebService<LeaveInfo, LeaveInfoMapper> {
	/**
	 * @description 提交请假条
	 * @author hxzhang
	 * @create 2016年8月16日上午11:42:56
	 * @version 1.0
	 * @param id
	 *            请假人account ID
	 * @param statrDate
	 *            开始时间
	 * @param endDate
	 *            结束时间
	 * @param type
	 *            请假类型
	 * @param isAdmin
	 *            是否为管理员
	 * @return 返回操作结果
	 */
	ServiceResult<Object> saveLeaveInfo(LeaveVo leaveVo, UserInfoVo currentUser);

	/**
	 * @description 获取请假列表
	 * @author hxzhang
	 * @create 2016年8月16日下午2:54:07
	 * @version 1.0
	 * @param condition
	 *            查询条件
	 * @return 返回查询结果
	 */
	ServiceResult<Pager<LeaveListVo, LeaveCondition>> getLeaveList(LeaveCondition condition, UserInfoVo currentUser);

	/**
	 * @description 对请假条进行操作
	 * @author hxzhang
	 * @create 2016年8月16日下午5:25:33
	 * @version 1.0
	 * @param operaType
	 *            操作方式
	 * @param currentUser
	 *            当前登录用户
	 * @return 返回操作结果
	 */
	ServiceResult<Object> saveOperaLeave(List<LeavePaidInfo> lpiList, String leaveId, short operaType, String reason, boolean confirm, UserInfoVo currentUser);

	/**
	 * @description 获取请假时间
	 * @author hxzhang
	 * @create 2016年8月17日上午11:33:11
	 * @version 1.0
	 * @param leaveId
	 *            请假ID
	 * @return 返回查询结果
	 */
	ServiceResult<Object> getLeaveDate(String leaveId, UserInfoVo currentUser);

	/**
	 * @description Leave定时任务
	 * @author hxzhang
	 * @create 2016年8月18日下午5:42:39
	 * @version 1.0
	 * @param now
	 *            现在时间
	 */
	int updateLeaveTimedTask(Date now);

	/**
	 * @description 通过accountId取消该员工的待审批的请假条
	 * @author hxzhang
	 * @create 2016年8月24日上午10:50:17
	 * @version 1.0
	 * @param userId
	 *            userId
	 */
	void cancelRequestLeave(String userId);

	int updateRequestState(String leaveId);

}
