package com.aoyuntek.aoyun.service;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.VisitLogInfo;

/**
 * @description 访问记录业务逻辑层接口
 * @author xdwang
 * @create 2015年12月13日下午1:49:17
 * @version 1.0
 */
public interface IVisitLogService {

    /**
     * 批量添加访问日志
     * 
     * @param logInfos
     *            访问日子集合
     */
    public void saveBatch(List<VisitLogInfo> logInfos);
}
