package com.aoyuntek.aoyun.service.old.impl;

import java.io.UnsupportedEncodingException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.dao.AccountInfoMapper;
import com.aoyuntek.aoyun.dao.AccountRoleInfoMapper;
import com.aoyuntek.aoyun.dao.AttachmentInfoMapper;
import com.aoyuntek.aoyun.dao.AttendanceHistoryInfoMapper;
import com.aoyuntek.aoyun.dao.BackgroundPersonInfoMapper;
import com.aoyuntek.aoyun.dao.BackgroundPetsInfoMapper;
import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.ChildAttendanceInfoMapper;
import com.aoyuntek.aoyun.dao.ChildBackgroundInfoMapper;
import com.aoyuntek.aoyun.dao.ChildCustodyArrangeInfoMapper;
import com.aoyuntek.aoyun.dao.ChildDietaryRequireInfoMapper;
import com.aoyuntek.aoyun.dao.ChildHealthMedicalDetailInfoMapper;
import com.aoyuntek.aoyun.dao.ChildHealthMedicalInfoMapper;
import com.aoyuntek.aoyun.dao.ChildMedicalInfoMapper;
import com.aoyuntek.aoyun.dao.CredentialsInfoMapper;
import com.aoyuntek.aoyun.dao.EmergencyContactInfoMapper;
import com.aoyuntek.aoyun.dao.FamilyInfoMapper;
import com.aoyuntek.aoyun.dao.MedicalImmunisationInfoMapper;
import com.aoyuntek.aoyun.dao.MedicalInfoMapper;
import com.aoyuntek.aoyun.dao.MessageContentInfoMapper;
import com.aoyuntek.aoyun.dao.MessageInfoMapper;
import com.aoyuntek.aoyun.dao.MessageLookInfoMapper;
import com.aoyuntek.aoyun.dao.NewsCommentInfoMapper;
import com.aoyuntek.aoyun.dao.NewsContentInfoMapper;
import com.aoyuntek.aoyun.dao.NewsGroupInfoMapper;
import com.aoyuntek.aoyun.dao.NewsInfoMapper;
import com.aoyuntek.aoyun.dao.NewsReceiveInfoMapper;
import com.aoyuntek.aoyun.dao.NqsRelationInfoMapper;
import com.aoyuntek.aoyun.dao.RelationKidParentInfoMapper;
import com.aoyuntek.aoyun.dao.RoomGroupInfoMapper;
import com.aoyuntek.aoyun.dao.SignChildInfoMapper;
import com.aoyuntek.aoyun.dao.SignSelationInfoMapper;
import com.aoyuntek.aoyun.dao.SignVisitorInfoMapper;
import com.aoyuntek.aoyun.dao.SigninInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.dao.YelfNewsInfoMapper;
import com.aoyuntek.aoyun.dao.program.ChildEylfInfoMapper;
import com.aoyuntek.aoyun.dao.program.ProgramIntobsleaInfoMapper;
import com.aoyuntek.aoyun.dao.program.ProgramLessonInfoMapper;
import com.aoyuntek.aoyun.dao.program.ProgramRegisterItemInfoMapper;
import com.aoyuntek.aoyun.dao.program.ProgramRegisterSleepInfoMapper;
import com.aoyuntek.aoyun.dao.program.ProgramRegisterTaskInfoMapper;
import com.aoyuntek.aoyun.dao.program.ProgramTaskExgrscInfoMapper;
import com.aoyuntek.aoyun.dao.program.ProgramWeeklyEvalutionInfoMapper;
import com.aoyuntek.aoyun.dao.program.TaskProgramFollowUpInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskInstanceInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskResponsibleLogInfoMapper;
import com.aoyuntek.aoyun.entity.po.AccountInfo;
import com.aoyuntek.aoyun.entity.po.AccountRoleInfo;
import com.aoyuntek.aoyun.entity.po.AttachmentInfo;
import com.aoyuntek.aoyun.entity.po.CredentialsInfo;
import com.aoyuntek.aoyun.entity.po.EmergencyContactInfo;
import com.aoyuntek.aoyun.entity.po.MedicalImmunisationInfo;
import com.aoyuntek.aoyun.entity.po.MedicalInfo;
import com.aoyuntek.aoyun.entity.po.MessageContentInfo;
import com.aoyuntek.aoyun.entity.po.MessageInfo;
import com.aoyuntek.aoyun.entity.po.MessageLookInfo;
import com.aoyuntek.aoyun.entity.po.NewsCommentInfo;
import com.aoyuntek.aoyun.entity.po.NewsContentInfo;
import com.aoyuntek.aoyun.entity.po.NewsGroupInfo;
import com.aoyuntek.aoyun.entity.po.NewsInfo;
import com.aoyuntek.aoyun.entity.po.NewsReceiveInfo;
import com.aoyuntek.aoyun.entity.po.NqsRelationInfo;
import com.aoyuntek.aoyun.entity.po.SignChildInfoWithBLOBs;
import com.aoyuntek.aoyun.entity.po.SignSelationInfo;
import com.aoyuntek.aoyun.entity.po.SignVisitorInfoWithBLOBs;
import com.aoyuntek.aoyun.entity.po.SigninInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.po.YelfNewsInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramIntobsleaInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramLessonInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramRegisterItemInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramRegisterSleepInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramRegisterTaskInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramTaskExgrscInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramWeeklyEvalutionInfo;
import com.aoyuntek.aoyun.entity.po.program.TaskProgramFollowUpInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskInstanceInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskResponsibleLogInfo;
import com.aoyuntek.aoyun.entity.vo.old.OldFamilyInfoVo;
import com.aoyuntek.aoyun.service.impl.BaseWebServiceImpl;
import com.aoyuntek.aoyun.service.old.IOldDataService;
import com.theone.list.util.ListUtil;

@Service
public class OldDataServiceImpl extends BaseWebServiceImpl<UserInfo, UserInfoMapper> implements IOldDataService {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private UserInfoMapper userInfoMapper;
    @Autowired
    private AccountInfoMapper accountInfoMapper;
    @Autowired
    private AccountRoleInfoMapper accountRoleInfoMapper;
    @Autowired
    private EmergencyContactInfoMapper emergencyContactInfoMapper;
    @Autowired
    private MedicalInfoMapper medicalInfoMapper;
    @Autowired
    private MedicalImmunisationInfoMapper medicalImmunisationInfoMapper;
    @Autowired
    private CredentialsInfoMapper credentialsInfoMapper;
    @Autowired
    private AttachmentInfoMapper attachmentInfoMapper;
    @Autowired
    private CentersInfoMapper centersInfoMapper;
    @Autowired
    private RoomGroupInfoMapper groupInfoMapper;
    @Autowired
    private SigninInfoMapper signinInfoMapper;
    @Autowired
    private SignVisitorInfoMapper signVisitorInfoMapper;
    @Autowired
    private SignSelationInfoMapper signSelationInfoMapper;
    @Autowired
    private NewsInfoMapper newsInfoMapper;
    @Autowired
    private NqsRelationInfoMapper nqsRelationInfoMapper;
    @Autowired
    private NewsContentInfoMapper newsContentInfoMapper;
    @Autowired
    private NewsCommentInfoMapper newsCommentInfoMapper;
    @Autowired
    private NewsGroupInfoMapper newsGroupInfoMapper;
    @Autowired
    private NewsReceiveInfoMapper newsReceiveInfoMapper;
    @Autowired
    private TaskInstanceInfoMapper taskInstanceInfoMapper;
    @Autowired
    private ProgramIntobsleaInfoMapper programIntobsleaInfoMapper;
    @Autowired
    private TaskProgramFollowUpInfoMapper taskProgramFollowUpInfoMapper;
    @Autowired
    private AttendanceHistoryInfoMapper attendanceHistoryInfoMapper;
    @Autowired
    private YelfNewsInfoMapper yelfNewsInfoMapper;

    @Autowired
    private TaskResponsibleLogInfoMapper taskResponsibleLogInfoMapper;

    @Override
    public void importStaff(List<UserInfo> userList, List<AccountInfo> accountList, List<AccountRoleInfo> accountRoleList,
            List<EmergencyContactInfo> contactList, List<CredentialsInfo> credentialsList, List<MedicalInfo> medicalList,
            List<MedicalImmunisationInfo> immunisationList, List<AttachmentInfo> attachList, List<String> centerMangerList,
            Map<String, String> groupMangerMa) {
        // TODO Auto-generated method stub
        if (ListUtil.isNotEmpty(userList)) {
            userInfoMapper.insertBatch(userList);
        }
        if (ListUtil.isNotEmpty(accountList)) {
            accountInfoMapper.insertBatch(accountList);
        }
        if (ListUtil.isNotEmpty(accountRoleList)) {
            accountRoleInfoMapper.insertBatch(accountRoleList);
        }
        if (ListUtil.isNotEmpty(contactList)) {
            emergencyContactInfoMapper.insterBatchEmergencyContactInfo(contactList);
        }
        if (ListUtil.isNotEmpty(credentialsList)) {
            credentialsInfoMapper.insertBatch(credentialsList);
        }
        if (ListUtil.isNotEmpty(medicalList)) {
            medicalInfoMapper.insertBatch(medicalList);
        }
        if (ListUtil.isNotEmpty(immunisationList)) {
            medicalImmunisationInfoMapper.insertBatch(immunisationList);
        }
        if (ListUtil.isNotEmpty(attachList)) {
            attachmentInfoMapper.insertBatch(attachList);
        }

        for (String centerManagerUserId : centerMangerList) {
            centersInfoMapper.addCenterLead(centerManagerUserId);
        }
        for (Map.Entry<String, String> map : groupMangerMa.entrySet()) {
            String userId = map.getValue();
            groupInfoMapper.addGroupEduer(userId);
        }
    }

    @Autowired
    private RelationKidParentInfoMapper kidParentInfoMapper;
    @Autowired
    private ChildBackgroundInfoMapper childBackgroundInfoMapper;
    @Autowired
    private ChildHealthMedicalInfoMapper childHealthMedicalInfoMapper;
    @Autowired
    private ChildHealthMedicalDetailInfoMapper childHealthMedicalDetailInfoMapper;
    @Autowired
    private ChildMedicalInfoMapper childMedicalInfoMapper;
    @Autowired
    private ChildDietaryRequireInfoMapper childDietaryRequireInfoMapper;
    @Autowired
    private ChildAttendanceInfoMapper attendanceInfoMapper;
    @Autowired
    private BackgroundPetsInfoMapper backgroundPetsInfoMapper;
    @Autowired
    private BackgroundPersonInfoMapper backgroundPersonInfoMapper;
    @Autowired
    private ChildCustodyArrangeInfoMapper childCustodyArrangeInfoMapper;
    @Autowired
    private FamilyInfoMapper familyInfoMapper;
    @Autowired
    private SignChildInfoMapper signChildInfoMapper;
    @Autowired
    private ChildEylfInfoMapper childEylfInfoMapper;

    @Override
    public void importFamily(OldFamilyInfoVo familyVo) {
        // tbl_user
        if (ListUtil.isNotEmpty(familyVo.getUserList())) {
            userInfoMapper.insertBatch(familyVo.getUserList());
        }
        if (ListUtil.isNotEmpty(familyVo.getAccountList())) {
            accountInfoMapper.insertBatch(familyVo.getAccountList());
        }
        if (ListUtil.isNotEmpty(familyVo.getRoleRelationList())) {
            accountRoleInfoMapper.insertBatch(familyVo.getRoleRelationList());
        }
        if (ListUtil.isNotEmpty(familyVo.getParentChildRelationList())) {
            kidParentInfoMapper.insertBatch(familyVo.getParentChildRelationList());
        }
        if (ListUtil.isNotEmpty(familyVo.getFileList())) {
            attachmentInfoMapper.insertBatch(familyVo.getFileList());
        }
        if (ListUtil.isNotEmpty(familyVo.getBackgroundList())) {
            childBackgroundInfoMapper.insertBatch(familyVo.getBackgroundList());
        }
        if (ListUtil.isNotEmpty(familyVo.getMedicalList())) {
            childMedicalInfoMapper.insertBatch(familyVo.getMedicalList());
        }
        if (ListUtil.isNotEmpty(familyVo.getDietaryList())) {
            childDietaryRequireInfoMapper.insertBatch(familyVo.getDietaryList());
        }
        if (ListUtil.isNotEmpty(familyVo.getChildAttendList())) {
            attendanceInfoMapper.insertBatch(familyVo.getChildAttendList());
        }
        if (ListUtil.isNotEmpty(familyVo.getEmergencyContactList())) {
            emergencyContactInfoMapper.insterBatchEmergencyContactInfo(familyVo.getEmergencyContactList());
        }
        if (ListUtil.isNotEmpty(familyVo.getPersonList())) {
            backgroundPersonInfoMapper.insterBatchBackgroundPersonInfo(familyVo.getPersonList());
        }
        if (ListUtil.isNotEmpty(familyVo.getPetList())) {
            backgroundPetsInfoMapper.insterBatchBackgroundPetsInfo(familyVo.getPetList());
        }
        if (ListUtil.isNotEmpty(familyVo.getCustodyArrangeList())) {
            childCustodyArrangeInfoMapper.insertBatch(familyVo.getCustodyArrangeList());
        }

        if (ListUtil.isNotEmpty(familyVo.getFamilyList())) {
            familyInfoMapper.insertBatch(familyVo.getFamilyList());
        }
        if (ListUtil.isNotEmpty(familyVo.getHealthMedicalList())) {
            childHealthMedicalInfoMapper.insertBatch(familyVo.getHealthMedicalList());
        }
        if (ListUtil.isNotEmpty(familyVo.getHealthMedicalDetailList())) {
            childHealthMedicalDetailInfoMapper.insterBatchChildHealthMedicalDetailInfo(familyVo.getHealthMedicalDetailList());
        }
        if (ListUtil.isNotEmpty(familyVo.getChildAttendHistoryList())) {
            attendanceHistoryInfoMapper.insertBatch(familyVo.getChildAttendHistoryList());
        }

        if (ListUtil.isNotEmpty(familyVo.getEylfList())) {
            childEylfInfoMapper.batchInsertChildEylfInfo(familyVo.getEylfList());
        }
    }

    @Autowired
    private MessageLookInfoMapper messageLookInfoMapper;
    @Autowired
    private MessageInfoMapper messageInfoMapper;
    @Autowired
    private MessageContentInfoMapper messageContentInfoMapper;

    @Override
    public void importMessage(List<NqsRelationInfo> nqs, List<AttachmentInfo> attachmentInfos, List<MessageLookInfo> messageLookInfos,
            List<MessageInfo> messageInfos, List<MessageContentInfo> messageContentInfos) {
        if (ListUtil.isNotEmpty(nqs)) {
            nqsRelationInfoMapper.insterBatchNqsRelationInfo(nqs);
        }
        if (ListUtil.isNotEmpty(attachmentInfos)) {
            attachmentInfoMapper.insertBatch(attachmentInfos);
        }

        if (ListUtil.isNotEmpty(messageInfos)) {
            messageInfoMapper.insertBatch(messageInfos);
        }
        if (ListUtil.isNotEmpty(messageContentInfos)) {
            messageContentInfoMapper.insertBatch(messageContentInfos);
        }
        if (ListUtil.isNotEmpty(messageLookInfos)) {
            messageLookInfoMapper.insterBatchMessageReceiveInfo(messageLookInfos);
        }

    }

    @Override
    public void importVisitorSign(List<SigninInfo> signinInfos, List<SignVisitorInfoWithBLOBs> signVisitorInfosInfoWithBLOBs,
            List<SignSelationInfo> signSelationInfos) {
        if (ListUtil.isNotEmpty(signinInfos)) {
            signinInfoMapper.insertBatchSigninInfo(signinInfos);
        }
        if (ListUtil.isNotEmpty(signVisitorInfosInfoWithBLOBs)) {
            signVisitorInfoMapper.insertBatchSignVisitorInfo(signVisitorInfosInfoWithBLOBs);
        }
        if (ListUtil.isNotEmpty(signSelationInfos)) {
            signSelationInfoMapper.insertBatchSignSelationInfo(signSelationInfos);
        }

    }

    @Override
    public void importNewsFeed(List<NewsInfo> newsList, List<NqsRelationInfo> nqsList, List<NewsContentInfo> contentList,
            List<NewsCommentInfo> commentList, List<NewsGroupInfo> newsGroupList, List<NewsReceiveInfo> newsReceiveInfoList,
            List<AttachmentInfo> attachmentInfos, List<YelfNewsInfo> eylfList) {
        if (ListUtil.isNotEmpty(newsList)) {
            long start = System.currentTimeMillis();
            newsInfoMapper.insertBatch(newsList);
            long end = System.currentTimeMillis();
            System.out.println("insert newsInfo time--------->" + (end - start));
        }
        if (ListUtil.isNotEmpty(nqsList)) {
            nqsRelationInfoMapper.insterBatchNqsRelationInfo(nqsList);
        }
        if (ListUtil.isNotEmpty(contentList)) {
            newsContentInfoMapper.insertBatch(contentList);
        }
        if (ListUtil.isNotEmpty(commentList)) {
            newsCommentInfoMapper.insertBatch(commentList);
        }
        if (ListUtil.isNotEmpty(newsGroupList)) {
            newsGroupInfoMapper.insterBatchNewsGroupInfo(newsGroupList);
        }
        if (ListUtil.isNotEmpty(newsReceiveInfoList)) {
            newsReceiveInfoMapper.insterBatchNewsReceiveInfo(newsReceiveInfoList);
        }
        if (ListUtil.isNotEmpty(attachmentInfos)) {
            attachmentInfoMapper.insertBatch(attachmentInfos);
        }
        if (ListUtil.isNotEmpty(eylfList)) {
            yelfNewsInfoMapper.insterBatchYelfNewsInfo(eylfList);
        }

    }

    @Override
    public void importStaffSign(List<SigninInfo> signinInfos) {
        if (ListUtil.isNotEmpty(signinInfos)) {
            signinInfoMapper.insertBatchSigninInfo(signinInfos);
        }
    }

    public static void main(String[] args) throws UnsupportedEncodingException {
        String a = " The turtle who came to visit! 🐢</br>Today the Preschoolers witnessed a sp ";
        String b = new String(a.getBytes(), "utf8mb4");
        System.err.println(b);
    }

    @Override
    public void importChildSign(List<SigninInfo> signinInfos, List<SignChildInfoWithBLOBs> signChildInfoWithBLOBs,
            List<SignSelationInfo> signSelationInfos) {
        if (ListUtil.isNotEmpty(signinInfos)) {
            signinInfoMapper.insertBatchSigninInfo(signinInfos);
        }
        if (ListUtil.isNotEmpty(signChildInfoWithBLOBs)) {
            signChildInfoMapper.insertBatchSignChildInfo(signChildInfoWithBLOBs);
        }
        if (ListUtil.isNotEmpty(signSelationInfos)) {
            signSelationInfoMapper.insertBatchSignSelationInfo(signSelationInfos);
        }
    }

    private void aaa(final List<NewsInfo> newsList) {
        System.err.println("--------------------------------------->");
        //
        // final List<MyobData> myobList = myobDataList;
        // final int dvtIdN = dvtId;
        // String sql =
        // "insert into d_myob_data_loop(summary_type, summary_code, account_code,account_name, myob_takings, gst_code,is_default,statue, dvt_id,create_time,update_time) values (?,?,?,?,?,?,?,?,?,?,?)";
        String sql = "insert into tbl_news (id, centers_id, room_id,create_account_id, approve_account_id, create_user_name, update_account_id,"
                + " news_tag, approve_time, status, score, create_time,update_time, news_type,img_id, old_id, delete_flag) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";

        jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
            public int getBatchSize() {
                return newsList.size();
            }

            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ps.setString(1, newsList.get(i).getId());

                ps.setString(2, newsList.get(i).getCentersId());
                ps.setString(3, newsList.get(i).getRoomId());
                ps.setString(4, newsList.get(i).getCreateAccountId());

                ps.setString(5, newsList.get(i).getApproveAccountId());
                ps.setString(6, newsList.get(i).getCreateUserName());
                ps.setString(7, newsList.get(i).getUpdateAccountId());
                ps.setString(8, newsList.get(i).getNewsTag());
                // ps.setDate(9,new
                // Date(newsList.get(i).getApproveTime().getTime()) );
                ps.setDate(9, null);
                ps.setShort(10, newsList.get(i).getStatus());
                ps.setInt(11, newsList.get(i).getScore());
                // ps.setDate(12, new
                // Date(newsList.get(i).getCreateTime().getTime()));
                // ps.setDate(13, new
                // Date(newsList.get(i).getUpdateTime().getTime()));
                ps.setDate(12, null);
                ps.setDate(13, null);

                ps.setShort(14, newsList.get(i).getNewsType());
                ps.setString(15, newsList.get(i).getImgId());
                ps.setString(16, newsList.get(i).getOldId());

                ps.setShort(17, newsList.get(i).getDeleteFlag());

            }
        });

    }

    @Autowired
    private ProgramWeeklyEvalutionInfoMapper programWeeklyEvalutionInfoMapper;

    @Override
    public void importWeekly(List<ProgramWeeklyEvalutionInfo> programWeeklyEvalutionInfos, List<TaskInstanceInfo> TaskInstanceList,
            List<TaskResponsibleLogInfo> taskResponsibleLogInfos) {
        // TODO Auto-generated method stub
        if (ListUtil.isNotEmpty(programWeeklyEvalutionInfos)) {
            programWeeklyEvalutionInfoMapper.insertList(programWeeklyEvalutionInfos);

        }
        if (ListUtil.isNotEmpty(TaskInstanceList)) {
            taskInstanceInfoMapper.insertBatch(TaskInstanceList);

        }
        if (ListUtil.isNotEmpty(taskResponsibleLogInfos)) {
            taskResponsibleLogInfoMapper.insertBatch(taskResponsibleLogInfos);

        }

    }

    @Autowired
    private ProgramTaskExgrscInfoMapper programTaskExgrscInfoMapper;

    @Override
    public void importProgramCurriculums(List<TaskInstanceInfo> taskInstanceList, List<ProgramTaskExgrscInfo> taskList,
            List<AttachmentInfo> fileList, List<TaskResponsibleLogInfo> responsibleList) {
        if (ListUtil.isNotEmpty(taskList)) {
            programTaskExgrscInfoMapper.insertBatch(taskList);
        }
        if (ListUtil.isNotEmpty(taskInstanceList)) {
            taskInstanceInfoMapper.insertBatch(taskInstanceList);
        }
        if (ListUtil.isNotEmpty(fileList)) {
            attachmentInfoMapper.insertBatch(fileList);
        }
        if (ListUtil.isNotEmpty(responsibleList)) {
            taskResponsibleLogInfoMapper.insertBatch(responsibleList);
        }
    }

    @Override
    public void importProgramIntobslea(List<ProgramIntobsleaInfo> list, List<TaskInstanceInfo> taskInstanceInfos,
            List<TaskResponsibleLogInfo> responsibleList, List<AttachmentInfo> fileList) {
        if (ListUtil.isNotEmpty(list)) {
            programIntobsleaInfoMapper.batchInsertIntobsleaInfo(list);
        }
        if (ListUtil.isNotEmpty(taskInstanceInfos)) {
            taskInstanceInfoMapper.insertBatch(taskInstanceInfos);
        }
        if (ListUtil.isNotEmpty(fileList)) {
            attachmentInfoMapper.insertBatch(fileList);
        }
        if (ListUtil.isNotEmpty(responsibleList)) {
            taskResponsibleLogInfoMapper.insertBatch(responsibleList);
        }
    }

    @Override
    public void importFollowup(List<TaskProgramFollowUpInfo> list, List<TaskInstanceInfo> taskInstanceInfos,
            List<TaskResponsibleLogInfo> responsibleList) {
        if (ListUtil.isNotEmpty(list)) {
            taskProgramFollowUpInfoMapper.batchInsertFollowup(list);
        }
        if (ListUtil.isNotEmpty(taskInstanceInfos)) {
            taskInstanceInfoMapper.insertBatch(taskInstanceInfos);
        }
        if (ListUtil.isNotEmpty(responsibleList)) {
            taskResponsibleLogInfoMapper.insertBatch(responsibleList);
        }

    }

    @Autowired
    private ProgramLessonInfoMapper lessonInfoMapper;

    @Override
    public void importLesson(List<ProgramLessonInfo> lessonList, List<NqsRelationInfo> nqsList, List<AttachmentInfo> attachmentInfos,
            List<TaskResponsibleLogInfo> responsibleList) {
        if (ListUtil.isNotEmpty(lessonList)) {
            lessonInfoMapper.insertBatch(lessonList);
        }
        if (ListUtil.isNotEmpty(nqsList)) {
            nqsRelationInfoMapper.insterBatchNqsRelationInfo(nqsList);
        }
        if (ListUtil.isNotEmpty(attachmentInfos)) {
            attachmentInfoMapper.insertBatch(attachmentInfos);
        }
        if (ListUtil.isNotEmpty(responsibleList)) {
            taskResponsibleLogInfoMapper.insertBatch(responsibleList);
        }
        System.err.println("end");
    }

    @Autowired
    private ProgramRegisterTaskInfoMapper registerTaskInfoMapper;

    @Override
    public void importNappyRegister(List<ProgramRegisterTaskInfo> taskInfoList, List<ProgramRegisterItemInfo> taskItemList,
            List<TaskResponsibleLogInfo> responsibleList) {
        if (ListUtil.isNotEmpty(taskInfoList)) {
            registerTaskInfoMapper.batchInsert(taskInfoList);
        }
        if (ListUtil.isNotEmpty(taskItemList)) {
            registerItemInfoMapper.batchInsertRegisterItems(taskItemList);
        }
        if (ListUtil.isNotEmpty(responsibleList)) {
            taskResponsibleLogInfoMapper.insertBatch(responsibleList);
        }
    }

    @Autowired
    private ProgramRegisterItemInfoMapper registerItemInfoMapper;
    @Autowired
    private ProgramRegisterSleepInfoMapper programRegisterSleepInfoMapper;

    @Override
    public void importRegister(List<ProgramRegisterTaskInfo> taskInfoList, List<ProgramRegisterItemInfo> registerItemList,
            List<TaskInstanceInfo> taskInstanceList, List<ProgramRegisterSleepInfo> sleepItemList, List<TaskResponsibleLogInfo> responsibleList) {
        if (ListUtil.isNotEmpty(taskInfoList)) {
            registerTaskInfoMapper.batchInsert(taskInfoList);
        }
        if (ListUtil.isNotEmpty(registerItemList)) {
            registerItemInfoMapper.batchInsertRegisterItems(registerItemList);
        }
        if (ListUtil.isNotEmpty(sleepItemList)) {
            programRegisterSleepInfoMapper.batchInsertRegisterSleeps(sleepItemList);
        }
        if (ListUtil.isNotEmpty(taskInstanceList)) {
            taskInstanceInfoMapper.insertBatch(taskInstanceList);
        }
        if (ListUtil.isNotEmpty(responsibleList)) {
            taskResponsibleLogInfoMapper.insertBatch(responsibleList);
        }

    }

}
