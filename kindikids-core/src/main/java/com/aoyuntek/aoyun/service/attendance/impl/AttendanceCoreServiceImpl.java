/**
 * 
 */
package com.aoyuntek.aoyun.service.attendance.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.commons.beanutils.PropertyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.constants.SystemConstants;
import com.aoyuntek.aoyun.dao.AbsenteeRequestInfoMapper;
import com.aoyuntek.aoyun.dao.AccountInfoMapper;
import com.aoyuntek.aoyun.dao.AttendanceHistoryInfoMapper;
import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.ChangeAttendanceRequestInfoMapper;
import com.aoyuntek.aoyun.dao.ChildAttendanceInfoMapper;
import com.aoyuntek.aoyun.dao.GivingNoticeInfoMapper;
import com.aoyuntek.aoyun.dao.RequestLogInfoMapper;
import com.aoyuntek.aoyun.dao.RoomInfoMapper;
import com.aoyuntek.aoyun.dao.SigninInfoMapper;
import com.aoyuntek.aoyun.dao.SubtractedAttendanceMapper;
import com.aoyuntek.aoyun.dao.TemporaryAttendanceInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.dao.child.RoomChangeInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskInstanceInfoMapper;
import com.aoyuntek.aoyun.entity.po.AbsenteeRequestInfo;
import com.aoyuntek.aoyun.entity.po.AbsenteeRequestInfoWithBLOBs;
import com.aoyuntek.aoyun.entity.po.AccountInfo;
import com.aoyuntek.aoyun.entity.po.ChangeAttendanceRequestInfo;
import com.aoyuntek.aoyun.entity.po.ChildAttendanceInfo;
import com.aoyuntek.aoyun.entity.po.ReplaceChildVo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.po.child.RoomChangeInfo;
import com.aoyuntek.aoyun.entity.vo.SigninVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.child.AttendancePostion;
import com.aoyuntek.aoyun.enums.AttendanceRequestType;
import com.aoyuntek.aoyun.enums.ChangeAttendanceRequestState;
import com.aoyuntek.aoyun.enums.ChangeAttendanceRequestType;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.factory.AttendanceFactory;
import com.aoyuntek.aoyun.factory.UserFactory;
import com.aoyuntek.aoyun.service.IFamilyService;
import com.aoyuntek.aoyun.service.attendance.IAttendanceCoreService;
import com.aoyuntek.aoyun.service.attendance.IAttendanceHistoryService;
import com.aoyuntek.aoyun.service.impl.BaseWebServiceImpl;
import com.aoyuntek.aoyun.uitl.TipMessageUtil;
import com.theone.date.util.DateUtil;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

/**
 * 系统核心处理业务逻辑
 * 
 * @author dlli5 at 2016年8月31日上午8:53:29
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
@Service
public class AttendanceCoreServiceImpl extends BaseWebServiceImpl<UserInfo, UserInfoMapper> implements IAttendanceCoreService {

    @Autowired
    private UserInfoMapper userInfoMapper;
    @Autowired
    private AccountInfoMapper accountInfoMapper;
    @Autowired
    private ChangeAttendanceRequestInfoMapper changeAttendanceRequestInfoMapper;
    @Autowired
    private ChildAttendanceInfoMapper childAttendanceInfoMapper;
    @Autowired
    private AttendanceHistoryInfoMapper attendanceHistoryInfoMapper;
    @Autowired
    private TipMessageUtil tipMessageUtil;
    @Autowired
    private RequestLogInfoMapper requestLogInfoMapper;
    @Autowired
    private TemporaryAttendanceInfoMapper temporaryAttendanceInfoMapper;
    @Autowired
    private SubtractedAttendanceMapper subtractedAttendanceMapper;
    @Autowired
    private AttendanceFactory attendanceFactory;
    @Autowired
    private CentersInfoMapper centersInfoMapper;
    @Autowired
    private GivingNoticeInfoMapper givingNoticeInfoMapper;
    @Autowired
    private AbsenteeRequestInfoMapper absenteeRequestInfoMapper;
    @Autowired
    private SigninInfoMapper signinInfoMapper;
    @Autowired
    private RoomInfoMapper roomInfoMapper;
    @Autowired
    private IAttendanceHistoryService attendanceHistoryService;
    @Autowired
    private TaskInstanceInfoMapper taskInstanceInfoMapper;
    @Autowired
    private IFamilyService familyService;
    @Autowired
    private UserFactory userFactory;
    @Autowired
    private RoomChangeInfoMapper roomChangeInfoMapper;

    @Override
    public void dealAttendanceNow(UserInfoVo currentUser, UserInfo childInfo, ChildAttendanceInfo attendanceInfo, ChangeAttendanceRequestInfo re) {
        String currentUserId = currentUser == null ? SystemConstants.JobCreateAccountId : currentUser.getAccountInfo().getId();
        AttendanceRequestType attendanceRequestType = AttendanceRequestType.getType(re.getType());
        // 将排课结果直接修改到当前小孩的排课表中
        re.setState(ChangeAttendanceRequestState.Over.getValue());
        UserInfo oldUser=new UserInfo();
        try {
            PropertyUtils.copyProperties(oldUser,childInfo);
        } catch (Exception e) {
        }
        switch (attendanceRequestType) {
        case K: {
            attendanceFactory.dealAttendance(attendanceInfo, re);
            attendanceInfo.setUpdateAccountId(currentUserId);
            attendanceInfo.setUpdateTime(new Date());
            childAttendanceInfoMapper.updateByPrimaryKey(attendanceInfo);
        }
            break;
        case R: {
            attendanceFactory.dealAttendance(attendanceInfo, re);
            attendanceInfo.setUpdateAccountId(currentUserId);
            attendanceInfo.setUpdateTime(new Date());
            childAttendanceInfoMapper.updateByPrimaryKey(attendanceInfo);

            childInfo.setRoomId(re.getNewRoomId());
            childInfo.setGroupId(re.getNewGroupId());
            childInfo.setUpdateAccountId(currentUserId);
            childInfo.setUpdateTime(new Date());
            userInfoMapper.updateByPrimaryKey(childInfo);
        }
            break;
        case C: {
            attendanceFactory.dealAttendance(attendanceInfo, re);
            attendanceInfo.setUpdateAccountId(currentUserId);
            attendanceInfo.setUpdateTime(new Date());
            attendanceInfo.setEnrolmentDate(new Date());
            // attendanceInfo.setLeaveDate(null);
            childAttendanceInfoMapper.updateByPrimaryKey(attendanceInfo);

            childInfo.setCentersId(re.getNewCentreId());
            childInfo.setRoomId(re.getNewRoomId());
            childInfo.setGroupId(re.getNewGroupId());
            childInfo.setUpdateAccountId(currentUserId);
            childInfo.setUpdateTime(new Date());
            userInfoMapper.updateByPrimaryKey(childInfo);

            List<AbsenteeRequestInfoWithBLOBs> bloBs = absenteeRequestInfoMapper.getAbsenteesAfterDate(re.getChildId(), re.getChangeDate());
            if (ListUtil.isNotEmpty(bloBs)) {
                for (AbsenteeRequestInfoWithBLOBs absentee : bloBs) {
                    absentee.setDeleteFlag(DeleteFlag.Delete.getValue());
                    absenteeRequestInfoMapper.updateByPrimaryKey(absentee);
                }
            }
        }
            break;
        }
        changeAttendanceRequestInfoMapper.updateByPrimaryKey(re);
        // 记录历史
        attendanceHistoryService.dealAttendanceHistory(re, currentUser);

        switch (attendanceRequestType) {
        case R:
        case C: {
            // 如果今天有签到信息 那么将签到信息更新到最新的room下
            signinInfoMapper.updateChildSignOfToday(re.getChildId(), re.getNewCentreId(), re.getNewRoomId());

            // 如果有interest/observation/learning story及follow up 那么如果只改了group
            boolean justChangeGroup = attendanceFactory.justChangeGroup(oldUser, re);
            if (justChangeGroup) {
                taskInstanceInfoMapper.updateGroupId(re.getChildId(), re.getNewGroupId());
            }
        }
            break;
        default:
            break;
        }
    }

    @Override
    public void dealAttendanceInit(UserInfoVo currentUser, ChildAttendanceInfo attendanceInfo, AccountInfo accountInfo, UserInfo userInfo) {
        if (StringUtil.isEmpty(userInfo.getRoomId())) {
            //未入园 从已有roomId 到 null 删除之前的安排
            roomChangeInfoMapper.deleteByChild(accountInfo.getId());
            return;
        }
        // 刪除之前存在的
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // 如果是新增 那么作废之前的申请
        changeAttendanceRequestInfoMapper.updateState(accountInfo.getId(), ChangeAttendanceRequestState.OnJob.getValue());
        // 删除临时永久和删除的排课记录
        temporaryAttendanceInfoMapper.deleteOld(accountInfo.getId());
        // 刪除移除的
        subtractedAttendanceMapper.deleteOld(accountInfo.getId());
        // 删除所有
        roomChangeInfoMapper.deleteByChild(accountInfo.getId());
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // 新增最新的
        ChangeAttendanceRequestInfo attendanceRequestInfo = new ChangeAttendanceRequestInfo();
        attendanceRequestInfo.setChangeDate(attendanceInfo.getChangeDate());
        attendanceRequestInfo.setChildId(accountInfo.getId());
        attendanceRequestInfo.setFriday(attendanceInfo.getFridayShort());
        attendanceRequestInfo.setMonday(attendanceInfo.getMondayShort());
        attendanceRequestInfo.setThursday(attendanceInfo.getThursdayShort());
        attendanceRequestInfo.setTuesday(attendanceInfo.getTuesdayShort());
        attendanceRequestInfo.setWednesday(attendanceInfo.getWednesdayShort());
        attendanceRequestInfo.setNewCentreId(userInfo.getCentersId());
        attendanceRequestInfo.setNewGroupId(userInfo.getGroupId());
        attendanceRequestInfo.setNewRoomId(userInfo.getRoomId());
        attendanceRequestInfo.setSourceType(ChangeAttendanceRequestType.InitAttendance.getValue());
        attendanceRequestInfo.setType(AttendanceRequestType.C.getValue());
        attendanceRequestInfo.setId(UUID.randomUUID().toString());
        attendanceRequestInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
        attendanceRequestInfo.setCreateTime(new Date());
        attendanceRequestInfo.setDeleteFlag(DeleteFlag.Default.getValue());
        attendanceRequestInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
        attendanceRequestInfo.setUpdateTime(new Date());
        attendanceRequestInfo.setState(ChangeAttendanceRequestState.OnJob.getValue());
        changeAttendanceRequestInfoMapper.insert(attendanceRequestInfo);
        // 插入变更
        List<RoomChangeInfo> changeInfos = attendanceFactory.getRoomChangeInfoList(attendanceRequestInfo);
        for (RoomChangeInfo roomChangeInfo : changeInfos) {
            roomChangeInfoMapper.insert(roomChangeInfo);
        }
    }

    @Override
    public void dealParentRequest(String childId, ChangeAttendanceRequestInfo adminAttendanceRequestInfo) {
        // 修改排课申请的某一天改为2已安排
        List<ChangeAttendanceRequestInfo> changeAttendanceRequestInfos = changeAttendanceRequestInfoMapper.getListByChildAndStateAndType(childId,
                ChangeAttendanceRequestType.ParentRequest.getValue(), ChangeAttendanceRequestState.Default.getValue());
        if (ListUtil.isEmpty(changeAttendanceRequestInfos)) {
            changeAttendanceRequestInfos = changeAttendanceRequestInfoMapper.getListByChildAndStateAndType(childId,
                    ChangeAttendanceRequestType.ParentRequest.getValue(), ChangeAttendanceRequestState.PartialAproved.getValue());
        }
        if (ListUtil.isEmpty(changeAttendanceRequestInfos)) {
            return;
        }

        attendanceFactory.dealParentAttendanceWithAdminChange(adminAttendanceRequestInfo, changeAttendanceRequestInfos.get(0));
        changeAttendanceRequestInfoMapper.updateByPrimaryKey(changeAttendanceRequestInfos.get(0));
    }

    @Override
    public List<SigninVo> getTodaySignOutChilds(String centersId, Date day) {
        return signinInfoMapper.getTodaySignOutChilds(centersId, day);
    }

    @Override
    public List<SigninVo> getYesterdaySignChilds(String centersId, String roomId, short dayOfWeek, Date day) {
        // 本该上课的 signinInfos
        List<SigninVo> signinInfos = signinInfoMapper.getListByWhere(centersId, roomId, dayOfWeek, day);

        // 已然签到的 防止额外签到
        List<SigninVo> oldSigninfos = signinInfoMapper.getAttendanceLogs(centersId, roomId, day);

        // 搜索今天发生的变化
        List<RoomChangeInfo> roomChangeInfos = roomChangeInfoMapper.getListByDateScope(centersId, roomId, dayOfWeek, day, day);

        // 模拟计算应该产生的变化
        return attendanceFactory.getTodayAttendanceChild(signinInfos, roomChangeInfos, oldSigninfos, day);
    }

    @Override
    public List<SigninVo> getTodaySignChilds(String centersId, String roomId, short dayOfWeek, Date day) {
        // 本该上课的 signinInfos
        List<SigninVo> signinInfos = signinInfoMapper.getListByWhere(centersId, roomId, dayOfWeek, day);

        // 已然签到的 防止额外签到
        List<SigninVo> oldSigninfos = signinInfoMapper.getAttendanceLogs(centersId, roomId, day);

        // 搜索今天发生的变化
        List<RoomChangeInfo> roomChangeInfos = roomChangeInfoMapper.getListByDateScope(centersId, roomId, dayOfWeek, new Date(), day);

        // 模拟计算应该产生的变化
        signinInfos = attendanceFactory.getTodayAttendanceChild(signinInfos, roomChangeInfos, oldSigninfos, day);
        return signinInfos;
    }

    @Override
    public List<ReplaceChildVo> getReaplaceInstanceOfChilds(String centersId, String roomId, Date day, AbsenteeRequestInfo absenteeRequestInfo,
            int dayOfWeek, List<ReplaceChildVo> internalResult) {
        List<ReplaceChildVo> result = new ArrayList<ReplaceChildVo>();

        int lastDay = com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(day, absenteeRequestInfo.getEndDate());
        int size = 0;
        boolean gameOver = false;
        for (int i = 0; i < lastDay + 1; i++) {
            Calendar cdate = Calendar.getInstance();
            cdate.setTime(DateUtil.addDay(day, i));
            if (DateUtil.getDayOfWeek(cdate.getTime()) - 1 == dayOfWeek) {
                // 判断
                List<SigninVo> signinVos = getFutureRoomChilds(centersId, roomId, cdate.getTime(), (short) dayOfWeek);
                if (!contains(signinVos, absenteeRequestInfo.getChildId())) {
                    gameOver = true;
                }

                // 循环判断这些人还在不在此时间点上
                for (ReplaceChildVo replaceChildVo : internalResult) {
                    if (replaceChildVo.getWeekInstances() != null) {
                        continue;
                    }
                    AttendancePostion attendancePostion = attendanceFactory.getChildPostion(replaceChildVo.getId(), cdate.getTime());
                    if (!roomId.equals(attendancePostion.getRoomId())) {
                        replaceChildVo.setWeekInstances(size);
                    }
                }

                if (gameOver) {
                    break;
                }

                size++;
            }
        }

        // 循环判断这些人还在不在此时间点上
        for (ReplaceChildVo replaceChildVo : internalResult) {
            if (replaceChildVo.getWeekInstances() != null) {
                continue;
            }
            replaceChildVo.setWeekInstances(size);
        }

        // 除去不能替换的人
        for (ReplaceChildVo replaceChildVo : internalResult) {
            if (replaceChildVo.getWeekInstances() != 0)
                result.add(replaceChildVo);
        }

        return result;
    }

    @Override
    public int getReaplaceInstance(String centersId, String roomId, Date day, AbsenteeRequestInfo absenteeRequestInfo, int dayOfWeek) {
        int lastDay = com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(day, absenteeRequestInfo.getEndDate());
        int size = 0;
        for (int i = 0; i < lastDay + 1; i++) {
            Calendar cdate = Calendar.getInstance();
            cdate.setTime(DateUtil.addDay(day, i));
            if (DateUtil.getDayOfWeek(cdate.getTime()) - 1 == dayOfWeek) {
                // 判断
                List<SigninVo> signinVos = getFutureRoomChilds(centersId, roomId, cdate.getTime(), (short) dayOfWeek);
                if (!contains(signinVos, absenteeRequestInfo.getChildId())) {
                    return size;
                }
                size++;
            }
        }
        return size;
    }

    /**
     * 
     * @author dlli5 at 2016年12月29日下午9:20:00
     * @param signinVos
     * @param childId
     * @return
     */
    private boolean contains(List<SigninVo> signinVos, String childId) {
        if (ListUtil.isEmpty(signinVos)) {
            return false;
        }
        for (SigninVo signinVo : signinVos) {
            if (signinVo.getAccountId().equals(childId)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public List<SigninVo> getFutureRoomChilds(String centersId, String roomId, Date day, short dayOfWeek) {
        // 查询本周几应该上课的所有小孩数量
        List<String> roomChilds = userInfoMapper.getCurrentChildIdsOfRoom(centersId, roomId, (short) dayOfWeek);

        // 搜索今天到这个时间点变化的数据
        List<RoomChangeInfo> roomChangeInfos = roomChangeInfoMapper.getListByDateScope(centersId, roomId, dayOfWeek, new Date(), day);

        // 模拟计算应该产生的变化
        return attendanceFactory.getFutureAttendanceChild(roomChilds, roomChangeInfos, day);
    }

    @Override
    public boolean isCenterManager(UserInfoVo currentUser, String centersId, boolean isCentreAdmin) {
        // CentersInfo centersInfo =
        // centersInfoMapper.selectByPrimaryKey(centersId);
        if (isCentreAdmin) {
            if (StringUtil.isNotEmpty(currentUser.getUserInfo().getCentersId()) && currentUser.getUserInfo().getCentersId().equals(centersId)) {
                return true;
            }
            List<UserInfo> userInfos = userInfoMapper.getCenterSecondMangers(centersId);
            if (ListUtil.isNotEmpty(userInfos)) {
                for (UserInfo userInfo : userInfos) {
                    if (userInfo.getId().equals(currentUser.getUserInfo().getId())) {
                        return true;
                    }
                }
            }

        }
        return false;
    }
}
