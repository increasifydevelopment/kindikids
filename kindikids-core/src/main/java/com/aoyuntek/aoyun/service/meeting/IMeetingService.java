package com.aoyuntek.aoyun.service.meeting;

import com.aoyuntek.aoyun.condtion.MeetingCondition;
import com.aoyuntek.aoyun.dao.meeting.MeetingInfoMapper;
import com.aoyuntek.aoyun.entity.po.meeting.MeetingInfo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.meeting.MeetingInfoVo;
import com.aoyuntek.aoyun.service.IBaseWebService;
import com.theone.common.util.ServiceResult;

/**
 * @description 会议模版业务逻辑接口
 * @author Hxzhang 2016年10月13日上午10:35:38
 */
public interface IMeetingService extends IBaseWebService<MeetingInfo, MeetingInfoMapper> {
    boolean validateMeetingName(String id, String name);

    /**
     * 
     * @description 新增meetingInfo
     * @author mingwang
     * @create 2016年10月17日下午4:53:49
     * @version 1.0
     * @param meetingInfoVo
     * @param currentUser
     * @return
     */
    ServiceResult<Object> addMeetingInfo(MeetingInfoVo meetingInfoVo, UserInfoVo currentUser);

    /**
     * @description 获取会议列表
     * @author hxzhang
     * @create 2016年10月17日下午4:43:49
     */
    public ServiceResult<Object> getMeetingManaList(MeetingCondition condition, UserInfoVo currentUser);

    /**
     * @description 获取会议信息
     * @author hxzhang
     * @create 2016年10月18日上午10:11:14
     */
    public ServiceResult<Object> getMeetingManaInfo(String id, String tempId, UserInfoVo currentUser);

    /**
     * @description 保存会议信息
     * @author hxzhang
     * @create 2016年10月18日下午4:38:05
     */
    public ServiceResult<Object> saveMeetingMana(MeetingInfoVo meetingInfoVo, UserInfoVo currentUser, boolean confirm);

    /**
     * 
     * @description 删除meeting
     * @author mingwang
     * @create 2016年10月19日上午11:17:32
     * @version 1.0
     * @param meetingId
     * @return
     */
    ServiceResult<Object> updateDeleteByPrimaryKey(String meetingId);

    /**
     * @description 获取一行数据
     * @author hxzhang
     * @create 2016年10月23日下午2:28:01
     */
    ServiceResult<Object> getRowInfo(String id);

    /**
     * @description 获取会议生成的Other Task的责任人
     * @author hxzhang
     * @create 2016年10月23日下午3:11:49
     */
    String getLiabler(String id);

}
