package com.aoyuntek.aoyun.service.attendance;

import com.aoyuntek.aoyun.dao.AttendanceHistoryInfoMapper;
import com.aoyuntek.aoyun.entity.po.AttendanceHistoryInfo;
import com.aoyuntek.aoyun.entity.po.ChangeAttendanceRequestInfo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.service.IBaseWebService;
import com.theone.common.util.ServiceResult;

public interface IAttendanceHistoryService extends IBaseWebService<AttendanceHistoryInfo, AttendanceHistoryInfoMapper> {

    /**
     * 处理历史
     * 
     * @author dlli5 at 2016年9月18日上午10:46:47
     * @param attendanceRequestInfo
     * @param attendanceInfo
     * @param userInfoVo
     * @return
     */
    public ServiceResult<Object> dealAttendanceHistory(ChangeAttendanceRequestInfo attendanceRequestInfo, UserInfoVo userInfoVo);
}
