package com.aoyuntek.aoyun.service.task.impl;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.ContextLoader;

import com.aoyuntek.aoyun.condtion.AttendanceCondtion;
import com.aoyuntek.aoyun.condtion.CenterCondition;
import com.aoyuntek.aoyun.condtion.FollowUpCondtion;
import com.aoyuntek.aoyun.condtion.RoomCondition;
import com.aoyuntek.aoyun.condtion.TaskCondtion;
import com.aoyuntek.aoyun.condtion.UserCondition;
import com.aoyuntek.aoyun.condtion.UserDetailCondition;
import com.aoyuntek.aoyun.constants.SystemConstants;
import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.ChildCustodyArrangeInfoMapper;
import com.aoyuntek.aoyun.dao.ChildDietaryRequireInfoMapper;
import com.aoyuntek.aoyun.dao.ChildMedicalInfoMapper;
import com.aoyuntek.aoyun.dao.NqsInfoMapper;
import com.aoyuntek.aoyun.dao.RoleInfoMapper;
import com.aoyuntek.aoyun.dao.RoomInfoMapper;
import com.aoyuntek.aoyun.dao.SigninInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.dao.event.EventInfoMapper;
import com.aoyuntek.aoyun.dao.meeting.MeetingInfoMapper;
import com.aoyuntek.aoyun.dao.program.ProgramLessonInfoMapper;
import com.aoyuntek.aoyun.dao.program.ProgramMedicationInfoMapper;
import com.aoyuntek.aoyun.dao.program.ProgramTaskExgrscInfoMapper;
import com.aoyuntek.aoyun.dao.program.ProgramWeeklyEvalutionInfoMapper;
import com.aoyuntek.aoyun.dao.roster.RosterInfoMapper;
import com.aoyuntek.aoyun.dao.roster.RosterShiftInfoMapper;
import com.aoyuntek.aoyun.dao.roster.RosterShiftTaskInfoMapper;
import com.aoyuntek.aoyun.dao.roster.TempRosterCenterInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskFollowInstanceInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskFollowUpInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskFormRelationInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskFrequencyInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskInstanceInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskResponsibleLogInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskVisibleInfoMapper;
import com.aoyuntek.aoyun.entity.po.CentersInfo;
import com.aoyuntek.aoyun.entity.po.ChildDietaryRequireInfo;
import com.aoyuntek.aoyun.entity.po.ChildMedicalInfo;
import com.aoyuntek.aoyun.entity.po.RoomInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.po.event.EventInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramLessonInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramTaskExgrscInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramWeeklyEvalutionInfo;
import com.aoyuntek.aoyun.entity.po.roster.TempRosterCenterInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskFollowInstanceInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskFollowUpInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskFormRelationInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskFrequencyInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskInstanceInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskVisibleInfo;
import com.aoyuntek.aoyun.entity.vo.AttendanceDayVo;
import com.aoyuntek.aoyun.entity.vo.AttendanceReturnVo;
import com.aoyuntek.aoyun.entity.vo.FormInfoVO;
import com.aoyuntek.aoyun.entity.vo.NqsVo;
import com.aoyuntek.aoyun.entity.vo.ProgramMedicationInfoVo;
import com.aoyuntek.aoyun.entity.vo.RoleInfoVo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.aoyun.entity.vo.SigninVo;
import com.aoyuntek.aoyun.entity.vo.UserAvatarVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.ValueInfoVO;
import com.aoyuntek.aoyun.entity.vo.base.UserVo;
import com.aoyuntek.aoyun.entity.vo.dashboard.DashboardGroup;
import com.aoyuntek.aoyun.entity.vo.family.ChildCustodyArrangeVo;
import com.aoyuntek.aoyun.entity.vo.task.PersonSelectVo;
import com.aoyuntek.aoyun.entity.vo.task.TaskFollowUpInfoVo;
import com.aoyuntek.aoyun.entity.vo.task.TaskInfoVo;
import com.aoyuntek.aoyun.entity.vo.task.TaskInstanceVo;
import com.aoyuntek.aoyun.entity.vo.task.TaskListVo;
import com.aoyuntek.aoyun.entity.vo.todaynote.TodayNoteVo2;
import com.aoyuntek.aoyun.enums.ArchivedStatus;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.EnrolledState;
import com.aoyuntek.aoyun.enums.ProgramState;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.enums.SigninState;
import com.aoyuntek.aoyun.enums.TaskCategory;
import com.aoyuntek.aoyun.enums.TaskFollowUpStatu;
import com.aoyuntek.aoyun.enums.TaskFrequenceRepeatType;
import com.aoyuntek.aoyun.enums.TaskType;
import com.aoyuntek.aoyun.enums.TaskVisibleType;
import com.aoyuntek.aoyun.enums.UserType;
import com.aoyuntek.aoyun.factory.ShiftFactory;
import com.aoyuntek.aoyun.factory.TaskCentreFactory;
import com.aoyuntek.aoyun.factory.TaskChildFactory;
import com.aoyuntek.aoyun.factory.TaskCoreFactory;
import com.aoyuntek.aoyun.factory.TaskFactory;
import com.aoyuntek.aoyun.factory.TaskNoteFactory;
import com.aoyuntek.aoyun.factory.TaskRoomFactory;
import com.aoyuntek.aoyun.factory.TaskStaffFactory;
import com.aoyuntek.aoyun.factory.UserFactory;
import com.aoyuntek.aoyun.service.IFormInfoService;
import com.aoyuntek.aoyun.service.attendance.IAttendanceCoreService;
import com.aoyuntek.aoyun.service.impl.BaseWebServiceImpl;
import com.aoyuntek.aoyun.service.meeting.IMeetingAgendaService;
import com.aoyuntek.aoyun.service.task.ITaskCoreService;
import com.aoyuntek.aoyun.service.task.ITaskInstance;
import com.aoyuntek.aoyun.service.task.ITaskService;
import com.aoyuntek.aoyun.uitl.ExportConfigure;
import com.aoyuntek.aoyun.uitl.SqlUtil;
import com.aoyuntek.framework.model.Pager;
import com.csvreader.CsvWriter;
import com.theone.common.util.ServiceResult;
import com.theone.date.util.DateUtil;
import com.theone.json.util.JsonUtil;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

@Service
public class TaskServiceImpl extends BaseWebServiceImpl<TaskInfo, TaskInfoMapper> implements ITaskService {

	@Autowired
	private CentersInfoMapper centersInfoMapper;
	@Autowired
	private TaskFactory taskFactory;
	@Autowired
	private TaskCentreFactory taskCentreFactory;
	@Autowired
	private TaskRoomFactory taskRoomFactory;
	@Autowired
	private TaskChildFactory taskChildFactory;
	@Autowired
	private TaskStaffFactory taskStaffFactory;
	@Autowired
	private TaskCoreFactory taskCoreFactory;
	@Autowired
	private ITaskCoreService taskCoreService;
	@Autowired
	private RoomInfoMapper roomInfoMapper;
	@Autowired
	private UserInfoMapper userInfoMapper;
	@Autowired
	private TaskInfoMapper taskInfoMapper;
	@Autowired
	private TaskVisibleInfoMapper taskVisibleInfoMapper;
	@Autowired
	private TaskFollowUpInfoMapper taskFollowUpInfoMapper;
	@Autowired
	private TaskFrequencyInfoMapper taskFrequencyInfoMapper;
	@Autowired
	private TaskInstanceInfoMapper taskInstanceInfoMapper;
	@Autowired
	private ProgramTaskExgrscInfoMapper programTaskExgrscInfoMapper;
	@Autowired
	private ProgramLessonInfoMapper programLessonInfoMapper;
	@Autowired
	private IAttendanceCoreService attendanceCoreService;
	@Autowired
	private ChildDietaryRequireInfoMapper childDietaryRequireInfoMapper;
	@Autowired
	private EventInfoMapper eventInfoMapper;
	@Autowired
	private ProgramMedicationInfoMapper programMedicationInfoMapper;
	@Autowired
	private TaskNoteFactory taskNoteFactory;
	@Autowired
	private TaskFormRelationInfoMapper taskFormRelationInfoMapper;
	@Autowired
	private TaskFollowInstanceInfoMapper taskFollowInstanceInfoMapper;
	@Autowired
	private UserFactory userFactory;
	@Autowired
	private ITaskInstance taskInstance;
	@Autowired
	private IFormInfoService formInfoService;
	@Autowired
	private TaskFollowInstanceInfoMapper followInstanceInfoMapper;
	@Autowired
	private SigninInfoMapper signinInfoMapper;
	@Autowired
	private IFormInfoService formService;
	@Autowired
	private ChildCustodyArrangeInfoMapper childCustodyArrangeInfoMapper;
	@Autowired
	private RosterShiftTaskInfoMapper rosterShiftTaskInfoMapper;
	@Autowired
	private RosterInfoMapper rosterInfoMapper;
	@Autowired
	private ShiftFactory shiftFactory;
	@Autowired
	private IMeetingAgendaService agendaService;
	@Autowired
	private NqsInfoMapper nqsMapper;
	@Autowired
	private RosterShiftInfoMapper rosterShiftInfoMapper;
	@Autowired
	private ChildMedicalInfoMapper childMedicalInfoMapper;
	@Autowired
	private ProgramWeeklyEvalutionInfoMapper programWeeklyEvalutionInfoMapper;
	@Autowired
	private MeetingInfoMapper meetingInfoMapper;
	@Autowired
	private TempRosterCenterInfoMapper tempRosterCenterInfoMapper;
	@Autowired
	private RoleInfoMapper roleInfoMapper;
	@Autowired
	private TaskResponsibleLogInfoMapper taskResponsibleLogInfoMapper;

	@Override
	public ServiceResult<Object> dealObsoleteTask(UserInfoVo currentUser, String taskInstanceId, Boolean followUpFlag, Date day) {
		if (BooleanUtils.isTrue(followUpFlag)) {
			TaskFollowInstanceInfo followInstanceInfo = followInstanceInfoMapper.selectByPrimaryKey(taskInstanceId);
			if (followInstanceInfo != null) {
				followInstanceInfo.setStatu(ProgramState.Discard.getValue());
				followInstanceInfoMapper.updateByPrimaryKey(followInstanceInfo);
			}
		} else {
			// 更新实例的状态
			TaskInstanceInfo taskInstanceInfo = taskInstanceInfoMapper.selectByPrimaryKey(taskInstanceId);

			TaskFrequencyInfo frequencyInfo = taskFrequencyInfoMapper.selectByTaskId(taskInstanceInfo.getTaskModelId());
			if (frequencyInfo != null && frequencyInfo.getRepeatType() == TaskFrequenceRepeatType.WhenRequired.getValue()) {
				taskInstanceInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
				taskInstanceInfo.setUpdateTime(new Date());
				taskInstanceInfo.setEndDate(day);
			}

			taskInstanceInfo.setStatu(ProgramState.Discard.getValue());
			taskInstanceInfoMapper.updateByPrimaryKey(taskInstanceInfo);

			// 更新除了CustomTask
			if (taskInstanceInfo.getTaskType() != TaskType.CustomTask.getValue()) {
				taskInstanceInfoMapper.updateTaskStatus(taskInstanceInfo.getValueId(), taskInstanceInfo.getTaskType(), ProgramState.Discard.getValue());
			}
		}
		return successResult(getTipMsg("common_save"));
	}

	@Override
	public ServiceResult<Object> saveWhenRequiredTask(UserInfoVo currentUser, String taskId, List<PersonSelectVo> list, Date day) throws Exception {
		TaskInfo taskInfo = taskInfoMapper.selectByPrimaryKey(taskId);
		TaskInfoVo vo = new TaskInfoVo();
		PropertyUtils.copyProperties(vo, taskInfo);
		TaskCategory taskCategory = TaskCategory.getType(taskInfo.getTaskCategory());
		vo.setFormId(taskFormRelationInfoMapper.getByTaskId(taskInfo.getId()));
		// 获取follwup
		FollowUpCondtion followUpCondtion = new FollowUpCondtion();
		followUpCondtion.initAll();
		followUpCondtion.setTaskId(taskInfo.getId());
		followUpCondtion.setSortName("create_time");
		followUpCondtion.setSortOrder("desc");
		List<TaskFollowUpInfo> followUpInfos = taskFollowUpInfoMapper.getList(followUpCondtion);
		vo.setFollowUpInfos(followUpInfos);
		for (PersonSelectVo selectVo : list) {
			if (selectVo.getId().contains(",")) {
				selectVo.setId(selectVo.getId().split(",")[0]);
			}
		}
		switch (taskCategory) {
		case CENTER: {
			for (PersonSelectVo personSelectVo : list) {
				taskCoreService.dealTaskInstance(vo, currentUser.getAccountInfo().getId(), personSelectVo.getId(), null, null, day);
			}
		}
			break;
		case ROOM: {
			for (PersonSelectVo personSelectVo : list) {
				RoomInfo roomInfo = roomInfoMapper.selectByPrimaryKey(personSelectVo.getId());
				taskCoreService.dealTaskInstance(vo, currentUser.getAccountInfo().getId(), roomInfo.getCentersId(), personSelectVo.getId(), null, day);
			}
		}
			break;
		case CHILDRRENN: {
			for (PersonSelectVo personSelectVo : list) {
				UserInfo userInfo = userInfoMapper.getUserInfoByAccountId(personSelectVo.getId());
				taskCoreService.dealTaskInstance(vo, currentUser.getAccountInfo().getId(), userInfo.getCentersId(), userInfo.getRoomId(), personSelectVo.getId(), day);
			}
		}
			break;
		case STAFF: {
			for (PersonSelectVo personSelectVo : list) {
				UserInfo userInfo = userInfoMapper.getUserInfoByAccountId(personSelectVo.getId());
				taskCoreService.dealTaskInstance(vo, currentUser.getAccountInfo().getId(), userInfo.getCentersId(), userInfo.getRoomId(), personSelectVo.getId(), day);
			}
		}
			break;
		case Individual: {
			for (PersonSelectVo personSelectVo : list) {
				UserInfo userInfo = userInfoMapper.getUserInfoByAccountId(personSelectVo.getId());
				taskCoreService.dealTaskInstance(vo, currentUser.getAccountInfo().getId(), userInfo.getCentersId(), userInfo.getRoomId(), personSelectVo.getId(), day);
			}
		}
			break;
		default:
			break;
		}
		return successResult(getTipMsg("staff_save"));
	}

	@SuppressWarnings("unchecked")
	@Override
	public ServiceResult<Object> getWhenRequiredSelects(UserInfoVo currentUser, String taskId) {
		boolean isCeo = containRole(Role.CEO, currentUser.getRoleInfoVoList()) == 1;
		TaskInfo taskInfo = taskInfoMapper.selectByPrimaryKey(taskId);
		List<TaskVisibleInfo> selectObjs = taskVisibleInfoMapper.selectBySourceId(taskInfo.getSelectObjId());
		TaskCategory taskCategory = TaskCategory.getType(taskInfo.getTaskCategory());
		List<PersonSelectVo> selecterPos = new ArrayList<PersonSelectVo>();
		switch (taskCategory) {
		case CENTER: {
			boolean selectAllCentre = taskFactory.isSelectAllCenter(selectObjs);
			// 获取所有centre
			CenterCondition condition = new CenterCondition();
			condition.initAll();
			condition.setStatus(ArchivedStatus.UnArchived.getValue());
			if (selectAllCentre) {
				if (!isCeo) {
					List<String> centres = new ArrayList<String>();
					centres.add(currentUser.getUserInfo().getCentersId());
					condition.setCenterIds(centres);
				}
				List<CentersInfo> list = centersInfoMapper.getCenterList(condition);
				selecterPos = taskFactory.convertCentersInfo(list);
			} else {
				List<String> centres = new ArrayList<String>();
				for (TaskVisibleInfo taskVisibleInfo : selectObjs) {
					if (isCeo) {
						centres.add(taskVisibleInfo.getObjId());
					} else {
						if (StringUtil.isNotEmpty(currentUser.getUserInfo().getCentersId())
								&& currentUser.getUserInfo().getCentersId().equals(taskVisibleInfo.getObjId())) {
							centres.add(taskVisibleInfo.getObjId());
						}
					}
				}
				if (ListUtil.isEmpty(centres)) {
					centres.add(UUID.randomUUID().toString());
				}
				condition.setCenterIds(centres);
				List<CentersInfo> list = centersInfoMapper.getCenterList(condition);
				selecterPos = taskFactory.convertCentersInfo(list);
			}
		}
			break;
		case ROOM: {
			boolean selectAllRoomList = taskFactory.isSelectAllCentreRoom(selectObjs);
			RoomCondition roomCondition = new RoomCondition();
			roomCondition.initAll();
			roomCondition.setStatus(ArchivedStatus.UnArchived.getValue());
			if (selectAllRoomList) {
				if (!isCeo) {
					List<String> centreIds = new ArrayList<String>();
					centreIds.add(currentUser.getUserInfo().getCentersId());
					roomCondition.setCentreIds(centreIds);
				}
				List<RoomInfo> roomList = roomInfoMapper.getList(roomCondition);

				return successResult(TaskCategory.ROOM.getValue() + "", (Object) handleRoomsSelectPos(roomList));

				// selecterPos = taskFactory.convertRoomsInfo(roomList);
			} else {
				List<String> centreIds = new ArrayList<String>();
				List<String> roomIds = new ArrayList<String>();
				for (TaskVisibleInfo taskVisibleInfo : selectObjs) {
					if (isCeo) {
						if (taskVisibleInfo.getType() == TaskVisibleType.CENTER.getValue()) {
							centreIds.add(taskVisibleInfo.getObjId());
						} else {
							roomIds.add(taskVisibleInfo.getObjId());
						}
					} else {
						if (taskVisibleInfo.getType() == TaskVisibleType.CENTER.getValue()) {
							if (StringUtil.isNotEmpty(currentUser.getUserInfo().getCentersId())
									&& currentUser.getUserInfo().getCentersId().equals(taskVisibleInfo.getObjId())) {
								centreIds.add(taskVisibleInfo.getObjId());
							}
						} else {
							RoomInfo roomInfo = roomInfoMapper.selectByPrimaryKey(taskVisibleInfo.getObjId());
							if (StringUtil.isNotEmpty(currentUser.getUserInfo().getCentersId())
									&& currentUser.getUserInfo().getCentersId().equals(roomInfo.getCentersId())) {
								roomIds.add(taskVisibleInfo.getObjId());
							}
						}
					}
				}
				roomCondition.setCentreIds(centreIds);
				roomCondition.setRoomIds(roomIds);
				List<RoomInfo> roomList = new ArrayList<RoomInfo>();
				if (ListUtil.isNotEmpty(centreIds) || ListUtil.isNotEmpty(roomIds)) {
					roomList = roomInfoMapper.getList(roomCondition);
				}
				return successResult(TaskCategory.ROOM.getValue() + "", (Object) handleRoomsSelectPos(roomList));
				// selecterPos = taskFactory.convertRoomsInfo(roomList);
			}
		}
		case CHILDRRENN: {
			UserDetailCondition userCondtion = new UserDetailCondition();
			userCondtion.initAll();
			userCondtion.setUserType(UserType.Child.getValue());
			userCondtion.setStatus(ArchivedStatus.UnArchived.getValue());
			userCondtion.setEnrolled(EnrolledState.Enrolled.getValue());
			boolean selectAllChildList = taskFactory.isSelectAllChildrenRoom(selectObjs);
			if (selectAllChildList) {
				if (!isCeo) {
					List<String> centreIds = new ArrayList<String>();
					centreIds.add(currentUser.getUserInfo().getCentersId());
					userCondtion.setCentreIds(centreIds);
				}
				List<UserVo> userInfos = userInfoMapper.getListOfUserVo(userCondtion);
				selecterPos = taskFactory.convertUserInfo(userInfos);
			} else {
				List<String> centreIds = new ArrayList<String>();
				List<String> roomIds = new ArrayList<String>();
				List<String> accountId = new ArrayList<String>();
				for (TaskVisibleInfo taskVisibleInfo : selectObjs) {
					if (isCeo) {
						if (taskVisibleInfo.getType() == TaskVisibleType.CENTER.getValue()) {
							centreIds.add(taskVisibleInfo.getObjId());
						} else if (taskVisibleInfo.getType() == TaskVisibleType.ROOM.getValue()) {
							roomIds.add(taskVisibleInfo.getObjId());
						} else {
							accountId.add(taskVisibleInfo.getObjId());
						}
					} else {
						if (taskVisibleInfo.getType() == TaskVisibleType.CENTER.getValue()) {
							if (StringUtil.isNotEmpty(currentUser.getUserInfo().getCentersId())
									&& currentUser.getUserInfo().getCentersId().equals(taskVisibleInfo.getObjId())) {
								centreIds.add(taskVisibleInfo.getObjId());
							}
						} else if (taskVisibleInfo.getType() == TaskVisibleType.ROOM.getValue()) {
							RoomInfo roomInfo = roomInfoMapper.selectByPrimaryKey(taskVisibleInfo.getObjId());
							if (StringUtil.isNotEmpty(currentUser.getUserInfo().getCentersId())
									&& currentUser.getUserInfo().getCentersId().equals(roomInfo.getCentersId())) {
								roomIds.add(taskVisibleInfo.getObjId());
							}
						} else {
							UserInfo userInfo = userInfoMapper.getUserInfoByAccountId(taskVisibleInfo.getObjId());
							if (StringUtil.isNotEmpty(currentUser.getUserInfo().getCentersId())
									&& currentUser.getUserInfo().getCentersId().equals(userInfo.getCentersId())) {
								accountId.add(taskVisibleInfo.getObjId());
							}
						}
					}
				}
				userCondtion.setCentreIds(centreIds);
				List<UserVo> userInfos1 = new ArrayList<UserVo>();
				if (ListUtil.isNotEmpty(centreIds)) {
					userInfos1 = userInfoMapper.getListOfUserVo(userCondtion);
				}
				userCondtion.getCentreIds().clear();
				userCondtion.setRoomIds(roomIds);
				List<UserVo> userInfos2 = new ArrayList<UserVo>();
				if (ListUtil.isNotEmpty(roomIds)) {
					userInfos2 = userInfoMapper.getListOfUserVo(userCondtion);
				}
				userCondtion.getRoomIds().clear();
				userCondtion.setAccountIds(accountId);
				List<UserVo> userInfos3 = new ArrayList<UserVo>();
				if (ListUtil.isNotEmpty(accountId)) {
					userInfos3 = userInfoMapper.getListOfUserVo(userCondtion);
				}
				selecterPos = taskFactory.convertUserInfo(userFactory.duplicate(userInfos1, userInfos2, userInfos3));
			}
		}
			break;
		case STAFF: {
			UserDetailCondition userCondtion = new UserDetailCondition();
			userCondtion.initAll();
			userCondtion.setUserType(UserType.Staff.getValue());
			userCondtion.setStatus(ArchivedStatus.UnArchived.getValue());
			boolean selectAllChildList = taskFactory.isSelectAllStaff(selectObjs);
			if (selectAllChildList) {
				if (!isCeo) {
					List<String> centreIds = new ArrayList<String>();
					centreIds.add(currentUser.getUserInfo().getCentersId());
					userCondtion.setCentreIds(centreIds);
				}
				List<UserVo> userInfos = userInfoMapper.getListOfUserVo(userCondtion);
				selecterPos = taskFactory.convertUserInfo(userInfos);
			} else {
				List<String> centreIds = new ArrayList<String>();
				for (TaskVisibleInfo taskVisibleInfo : selectObjs) {
					if (isCeo) {
						centreIds.add(taskVisibleInfo.getObjId());
					} else {
						if (StringUtil.isNotEmpty(currentUser.getUserInfo().getCentersId())
								&& currentUser.getUserInfo().getCentersId().equals(taskVisibleInfo.getObjId())) {
							centreIds.add(taskVisibleInfo.getObjId());
						}
					}
				}
				userCondtion.setCentreIds(centreIds);
				List<UserVo> userInfos1 = new ArrayList<UserVo>();
				if (ListUtil.isNotEmpty(centreIds)) {
					userInfos1 = userInfoMapper.getListOfUserVo(userCondtion);
				}
				selecterPos = taskFactory.convertUserInfo(userInfos1);
			}
		}
			break;
		case Individual: {
			UserDetailCondition userCondtion = new UserDetailCondition();
			userCondtion.initAll();
			userCondtion.setUserType(UserType.Staff.getValue());
			userCondtion.setStatus(ArchivedStatus.UnArchived.getValue());
			boolean selectAllChildList = taskFactory.isSelectAllStaff(selectObjs);
			if (selectAllChildList) {
				if (!isCeo) {
					List<String> centreIds = new ArrayList<String>();
					centreIds.add(currentUser.getUserInfo().getCentersId());
					userCondtion.setCentreIds(centreIds);
				}
				List<UserVo> userInfos = userInfoMapper.getListOfUserVo(userCondtion);
				selecterPos = taskFactory.convertUserInfo(userInfos);
			} else {
				List<String> centreIds = new ArrayList<String>();
				for (TaskVisibleInfo taskVisibleInfo : selectObjs) {
					if (isCeo) {
						centreIds.add(taskVisibleInfo.getObjId());
					} else {
						if (StringUtil.isNotEmpty(currentUser.getUserInfo().getCentersId())
								&& currentUser.getUserInfo().getCentersId().equals(taskVisibleInfo.getObjId())) {
							centreIds.add(taskVisibleInfo.getObjId());
						}
					}
				}
				userCondtion.setCentreIds(centreIds);
				List<UserVo> userInfos1 = new ArrayList<UserVo>();
				if (ListUtil.isNotEmpty(centreIds)) {
					userInfos1 = userInfoMapper.getListOfUserVo(userCondtion);
				}
				selecterPos = taskFactory.convertUserInfo(userInfos1);
			}
		}
			break;
		}
		return successResult((Object) selecterPos);
	}

	private List<DashboardGroup> handleRoomsSelectPos(List<RoomInfo> roomList) {
		List<DashboardGroup> result = new ArrayList<DashboardGroup>();
		List<CentersInfo> centres = centersInfoMapper.getAllCentersInfos();
		for (CentersInfo c : centres) {
			String centreName = c.getName();
			List<PersonSelectVo> children = new ArrayList<PersonSelectVo>();
			boolean flag = false;
			for (RoomInfo room : roomList) {
				if (room.getCentersId().equals(c.getId())) {
					flag = true;
					children.add(new PersonSelectVo(TaskVisibleType.ROOM.getValue(), room.getId(), room.getName()));
				}
			}
			if (flag) {
				result.add(new DashboardGroup(centreName, children));
			}
		}
		return result;
	}

	@Override
	public ServiceResult<Object> getWhenRequiredTasks(UserInfoVo currentUser) {
		TaskCondtion condtion = new TaskCondtion();
		condtion.initAll();
		condtion.setCurrentFlag(true);
		condtion.setStatu(DeleteFlag.Default.getValue());
		condtion.setSortName("create_time");
		condtion.setSortOrder("DESC");
		condtion.setFrequenceRepeatType(TaskFrequenceRepeatType.WhenRequired.getValue());
		List<TaskInfo> taskInfos = taskInfoMapper.getList(condtion);
		List<SelecterPo> selecterPos = new ArrayList<SelecterPo>();
		for (TaskInfo taskInfo : taskInfos) {
			SelecterPo selecterPo = new SelecterPo(taskInfo.getId(), taskInfo.getTaskName());
			selecterPo.setSelected(false);
			selecterPos.add(selecterPo);
		}
		return successResult((Object) selecterPos);
	}

	@Override
	public ServiceResult<Object> getWhenRequiredTasks2(UserInfoVo currentUser) {
		TaskCondtion condtion = new TaskCondtion();
		condtion.initAll();
		condtion.setCurrentFlag(true);
		condtion.setStatu(DeleteFlag.Default.getValue());
		condtion.setSortName("create_time");
		condtion.setSortOrder("DESC");
		condtion.setFrequenceRepeatType(TaskFrequenceRepeatType.WhenRequired.getValue());
		List<TaskInfo> taskInfos = taskInfoMapper.getList(condtion);
		List<SelecterPo> selecterPos = new ArrayList<SelecterPo>();
		for (TaskInfo taskInfo : taskInfos) {
			if (!getSystemMsg("taskName").equals(taskInfo.getTaskName())) {
				continue;
			}
			SelecterPo selecterPo = new SelecterPo(taskInfo.getId(), taskInfo.getTaskName());
			selecterPo.setSelected(true);
			selecterPo.setDisabled(true);
			selecterPos.add(selecterPo);
		}
		return successResult((Object) selecterPos);
	}

	@Override
	public ServiceResult<Object> saveTaskFormValue(UserInfoVo currentUser, String id, int type, int statu, String json) throws Exception {
		Date now = new Date();

		// 如果需要保存表单的值
		if (StringUtil.isNotEmpty(json)) {
			ValueInfoVO valueInfoVO = (ValueInfoVO) JsonUtil.jsonToBean(json, ValueInfoVO.class);
			ServiceResult<String> result = formService.saveInstance(valueInfoVO);
			if (!result.isSuccess()) {
				return failResult(getTipMsg("operation_failed"));
			}
		}
		// 暂存
		if (statu == ProgramState.Pending.getValue()) {
			return successResult(getTipMsg("staff_save"));
		}

		String taskInstanceId = id;
		TaskInstanceInfo taskInstanceInfo = null;
		// task的状态
		if (type == 0) {
			taskInstanceInfo = taskInstanceInfoMapper.selectByPrimaryKey(id);
			// 如果状态不等於完成 则不更新状态
			if ((short) statu != ProgramState.Pending.getValue()) {
				taskInstanceInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
				taskInstanceInfo.setUpdateTime(new Date());
				taskInstanceInfo.setStatu((short) statu);
			}
			// 如果是when requie 那么如果完成了 更新它的结束时间为完成时间
			if ((short) statu == ProgramState.Complete.getValue()) {
				TaskFrequencyInfo frequencyInfo = taskFrequencyInfoMapper.selectByTaskId(taskInstanceInfo.getTaskModelId());
				if (frequencyInfo != null && frequencyInfo.getRepeatType() == TaskFrequenceRepeatType.WhenRequired.getValue()) {
					taskInstanceInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
					taskInstanceInfo.setUpdateTime(new Date());
					taskInstanceInfo.setEndDate(new Date());
				}
			}
			taskInstanceInfoMapper.updateByPrimaryKey(taskInstanceInfo);
		} else {
			TaskFollowInstanceInfo followInstanceInfo = taskFollowInstanceInfoMapper.selectByPrimaryKey(id);
			if ((short) statu != ProgramState.Pending.getValue()) {
				followInstanceInfo.setStatu((short) statu);
			}
			taskInstanceId = followInstanceInfo.getTaskInstanceId();
			taskInstanceInfo = taskInstanceInfoMapper.selectByPrimaryKey(taskInstanceId);
			// 如果是when requie 那么如果完成了 更新它的结束时间为完成时间
			if ((short) statu == ProgramState.Complete.getValue()) {
				TaskFrequencyInfo frequencyInfo = taskFrequencyInfoMapper.selectByTaskId(taskInstanceInfo.getTaskModelId());
				if (frequencyInfo.getRepeatType() == TaskFrequenceRepeatType.WhenRequired.getValue()) {
					followInstanceInfo.setEndDate(new Date());
				}
			}
			followInstanceInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
			followInstanceInfo.setUpdateTime(new Date());
			taskFollowInstanceInfoMapper.updateByPrimaryKey(followInstanceInfo);
		}

		if (taskInstanceInfo.getTaskType() != TaskType.CustomTask.getValue()) {
			return successResult(getTipMsg("staff_save"));
		}

		// 如果主task已经废弃 那么 则不在开启followup了
		if (taskInstanceInfo.getStatu() == ProgramState.Discard.getValue()) {
			return successResult(getTipMsg("staff_save"));
		}

		// 如果不是通过状态 不进行开放下一个的操作
		if (taskInstanceInfo.getStatu() != ProgramState.Complete.getValue()) {
			return successResult(getTipMsg("staff_save"));
		}

		// 获取follwup
		FollowUpCondtion followUpCondtion = new FollowUpCondtion();
		followUpCondtion.initAll();
		followUpCondtion.setTaskId(taskInstanceInfo.getTaskModelId());
		followUpCondtion.setSortName("create_time");
		followUpCondtion.setSortOrder("asc");
		List<TaskFollowUpInfo> followUpInfos = taskFollowUpInfoMapper.getList(followUpCondtion);
		if (ListUtil.isNotEmpty(followUpInfos)) {
			for (TaskFollowUpInfo taskFollowUpInfo : followUpInfos) {
				// 获取表单值
				TaskFollowInstanceInfo followInstanceInfo = taskFollowInstanceInfoMapper.getByFollowUpId(taskFollowUpInfo.getId(), taskInstanceId);
				// 如果是更新主task的状态 那么只能生成第一个followup的状态
				if (type == 0) {
					if (followInstanceInfo.getStatu() == ProgramState.Default.getValue()) {
						followInstanceInfo.setStatu(ProgramState.Pending.getValue());
						// add by big 20161117
						if (followInstanceInfo.getBeginDate() == null
								|| com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(followInstanceInfo.getBeginDate(), now) < 0) {
							followInstanceInfo.setBeginDate(new Date());
						}
						taskFollowInstanceInfoMapper.updateByPrimaryKey(followInstanceInfo);
						break;
					}
					break;
				} else {
					if (id.equals(followInstanceInfo.getId())) {
						type = 0;
					}
				}
			}
		}
		return successResult(getTipMsg("staff_save"));
	}

	@Override
	public ServiceResult<Object> getCustmerTaskInstance(UserInfoVo currentUser, String taskInstanceId) {
		boolean isCeo = containRole(Role.CEO, currentUser.getRoleInfoVoList()) == 1;
		boolean isCenterManager = containRoles(currentUser.getRoleInfoVoList(), Role.CentreManager, Role.EducatorSecondInCharge);
		TaskInstanceInfo taskInstanceInfo = taskInstanceInfoMapper.selectByPrimaryKey(taskInstanceId);
		String taskFormId = taskFormRelationInfoMapper.getByTaskId(taskInstanceInfo.getTaskModelId());

		TaskInfoVo taskInfoVo = taskInfoMapper.getVo(taskInstanceInfo.getTaskModelId());
		// 存储taskInstanceId为了查询当前用户是否能够操作权限
		taskInfoVo.setTemp1(taskInstanceId);

		TaskInstanceVo taskInstanceVo = new TaskInstanceVo();
		taskInstanceVo.setId(taskInstanceId);
		taskInstanceVo.setFormId(taskFormId);
		taskInstanceVo.setValueId(taskInstanceInfo.getValueId());
		taskInstanceVo.setStatu(taskInstanceInfo.getStatu());
		taskInstanceVo.setTaskName(taskInfoVo.getTaskName());
		taskInstanceVo.setOperationFlag(isCeo || isCenterManager ? true : taskCoreFactory.needShow(taskInfoVo, currentUser));

		List<TaskFollowInstanceInfo> followInstanceInfos = new ArrayList<TaskFollowInstanceInfo>();
		// 获取follwup
		FollowUpCondtion followUpCondtion = new FollowUpCondtion();
		followUpCondtion.initAll();
		followUpCondtion.setTaskId(taskInstanceInfo.getTaskModelId());
		followUpCondtion.setSortName("create_time");
		followUpCondtion.setSortOrder("asc");
		List<TaskFollowUpInfo> followUpInfos = taskFollowUpInfoMapper.getList(followUpCondtion);
		if (ListUtil.isNotEmpty(followUpInfos)) {
			for (TaskFollowUpInfo taskFollowUpInfo : followUpInfos) {
				String taskFollowFormId = taskFormRelationInfoMapper.getByTaskId(taskFollowUpInfo.getId());
				// 获取表单值
				TaskFollowInstanceInfo followInstanceInfo = taskFollowInstanceInfoMapper.getByFollowUpId(taskFollowUpInfo.getId(), taskInstanceId);
				if (followInstanceInfo == null) {
					followInstanceInfo = new TaskFollowInstanceInfo();
					followInstanceInfo.setFollowUpId(taskFollowUpInfo.getId());
					followInstanceInfo.setStatu(ProgramState.Pending.getValue());
				}
				followInstanceInfo.setTaskInstanceId(taskInstanceId);
				followInstanceInfo.setFormId(taskFollowFormId);
				// 是否有操作权限 begin
				TaskFollowUpInfoVo taskFollowUpInfoVo = taskFollowUpInfoMapper.getVo(taskFollowUpInfo.getId());
				if (followInstanceInfo.getStatu() != ProgramState.Default.getValue()) {
					taskFollowUpInfoVo.setTemp1(taskInstanceId);
					Boolean operationFlag = isCeo || isCenterManager ? true : taskCoreFactory.needShow(taskFollowUpInfoVo, currentUser);
					followInstanceInfo.setOperationFlag(operationFlag);
				}
				// 是否有操作权限 end
				followInstanceInfos.add(followInstanceInfo);
			}
		}
		taskInstanceVo.setFollowInstanceInfos(followInstanceInfos);
		// 获取自定义Task NQS版本
		taskInstanceVo.setNqsVos(nqsMapper.getNqsVoListByObjId(taskInstanceInfo.getTaskModelId()));

		return successResult((Object) taskInstanceVo);
	}

	@Override
	public ServiceResult<Object> getTodayNote(UserInfoVo currentUser, TaskCondtion condtion) {
		// 获取当前属于什么角色
		List<RoleInfoVo> roleInfoVoList = currentUser.getRoleInfoVoList();
		boolean isCEOORCentreManager = false;
		Short visibility = null;
		// 设置当前登录者角色集合
		for (int j = 0, m = roleInfoVoList.size(); j < m; j++) {
			// 判断是否是CEO或者园长 二级院长
			if (roleInfoVoList.get(j).getValue() == Role.CEO.getValue() || roleInfoVoList.get(j).getValue() == Role.CentreManager.getValue()
					|| roleInfoVoList.get(j).getValue() == Role.EducatorSecondInCharge.getValue()) {
				isCEOORCentreManager = true;
				break;
			}
		}
		if (!isCEOORCentreManager) {
			// 判断当前员工是家长:1 还是员工:0
			if (UserType.Parent.getValue().equals(currentUser.getUserInfo().getUserType())) {
				visibility = (short) 2;
			}
			if (UserType.Staff.getValue().equals(currentUser.getUserInfo().getUserType().shortValue())) {
				visibility = (short) 1;
			}
		}

		if ((StringUtil.isNotEmpty(condtion.getCentreId()) || StringUtil.isNotEmpty(condtion.getRoomId())) && DateUtil.isCurrentDate(condtion.getDay())) {
			int dayOfWeek = DateUtil.getDayOfWeek(condtion.getDay()) - 1;
			// 园room的小孩有食物过敏/医疗的（取基本信息中）“Jason Jay has seafood allergy.”
			// 获取今天应该到的小孩
			List<SigninVo> signinVos = attendanceCoreService.getTodaySignChilds(condtion.getCentreId(), condtion.getRoomId(), (short) dayOfWeek, condtion.getDay());
			List<SigninVo> oldSigninfos = signinInfoMapper.getAttendanceLogs(condtion.getCentreId(), condtion.getRoomId(), condtion.getDay());
			List<String> childAccountIds = new ArrayList<String>();
			for (SigninVo signinVo : signinVos) {
				childAccountIds.add(signinVo.getAccountId());
			}
			for (SigninVo signinVo : oldSigninfos) {
				if (signinVo.getState() != null && signinVo.getState() == SigninState.Signin.getValue() && !childAccountIds.contains(signinVo.getAbsenteeId())) {
					childAccountIds.add(signinVo.getAccountId());
				}
			}

			List<ChildDietaryRequireInfo> childDietaryRequireInfos = childDietaryRequireInfoMapper.getHaveDietaryRequire(childAccountIds);

			List<ChildMedicalInfo> childMedicalInfos = childMedicalInfoMapper.getHaveMedical(childAccountIds);

			// 某小孩has court orders
			List<ChildCustodyArrangeVo> custodyArrangeInfos = childCustodyArrangeInfoMapper.getListOfTodayNote(childAccountIds);

			// Event（取Event数据）“Today is Mother's Day!”
			String centerId = "";
			if (StringUtil.isNotEmpty(condtion.getCentreId())) {
				centerId = condtion.getCentreId();
			} else if (StringUtil.isEmpty(condtion.getCentreId()) && StringUtil.isNotEmpty(condtion.getRoomId())) {
				centerId = centersInfoMapper.getCenterIdByRoomId(condtion.getRoomId());
			}
			List<EventInfo> eventList = eventInfoMapper.getListByWhere(condtion.getDay(), visibility, currentUser.getAccountInfo().getId(), centerId);
			// 小孩进入我room的前一周“Sarah Smith will come on 01/01/2016.”
			List<UserInfo> willCome = userInfoMapper.getWillComeChild(condtion.getCentreId(), condtion.getRoomId(), condtion.getDay());
			willCome = userFactory.distinctUserInfo(willCome);

			// 小孩离开我room的前一周“Jenifer Black will leave on 01/01/2016.”
			List<UserInfo> willLeave = userInfoMapper.getWillLeaveChild(condtion.getCentreId(), condtion.getRoomId(), condtion.getDay());
			// 小孩吃什么药？form（待定）“Nancy need to take medicine three times perday.”
			List<ProgramMedicationInfoVo> medicationInfos = programMedicationInfoMapper.getWillTakeMedicine(condtion.getCentreId(), condtion.getRoomId(),
					condtion.getDay());
			// 几个overdue的task“You have 3 tasks overdue today.”
			int size = taskInstanceInfoMapper.getMyOverDueTaskInstanceSize(currentUser.getAccountInfo().getId());

			List<TodayNoteVo2> todayNoteVo = taskNoteFactory.dealTodayNote(childDietaryRequireInfos, eventList, willCome, willLeave, medicationInfos, size,
					custodyArrangeInfos, childMedicalInfos);
			return successResult((Object) todayNoteVo);
		} else {
			// Event（取Event数据）“Today is Mother's Day!”
			String centerId = "";
			if (StringUtil.isNotEmpty(condtion.getCentreId())) {
				centerId = condtion.getCentreId();
			} else if (StringUtil.isEmpty(condtion.getCentreId()) && StringUtil.isNotEmpty(condtion.getRoomId())) {
				centerId = centersInfoMapper.getCenterIdByRoomId(condtion.getRoomId());
			}
			List<EventInfo> eventList = eventInfoMapper.getListByWhere(condtion.getDay(), visibility, currentUser.getAccountInfo().getId(), centerId);
			List<TodayNoteVo2> todayNoteVo = taskNoteFactory.dealTodayNote(new ArrayList<ChildDietaryRequireInfo>(), eventList, new ArrayList<UserInfo>(),
					new ArrayList<UserInfo>(), new ArrayList<ProgramMedicationInfoVo>(), 0, new ArrayList<ChildCustodyArrangeVo>(), new ArrayList<ChildMedicalInfo>());
			return successResult((Object) todayNoteVo);
		}
	}

	@Override
	public ServiceResult<Object> getCalendar(UserInfoVo currentUser, AttendanceCondtion attendanceCondtion) {
		Date weekDate = null;
		// 如果时间传过来的是空 那么说明前台想回到当前周
		// 查月
		if (attendanceCondtion.getType() == 0) {
			weekDate = DateUtil.parse(attendanceCondtion.getTime() + "/01", "yyyy/MM/dd");
		} // 上周
		else if (attendanceCondtion.getType() == 1) {
			weekDate = DateUtil.addDay(attendanceCondtion.getWeekDate(), -7);
		} // 下周
		else if (attendanceCondtion.getType() == 3) {
			weekDate = DateUtil.addDay(attendanceCondtion.getWeekDate(), 7);
		} else if (attendanceCondtion.getType() == 4) {
			weekDate = attendanceCondtion.getWeekDate();
		} else {
			weekDate = new Date();
		}

		// 日期只能下一年
		if (DateUtil.getCalendar(weekDate).get(Calendar.YEAR) > (DateUtil.getCalendar(new Date()).get(Calendar.YEAR) + 1)) {
			return failResult(-1, getTipMsg("no.permissions"));
		}

		Date now = DateUtil.parse(DateUtil.format(new Date(), DateUtil.yyyyMMdd), DateUtil.yyyyMMdd);

		Date weekBegin = DateUtil.getWeekStart(DateUtil.parse(DateUtil.format(weekDate, "yyyy/MM/dd"), "yyyy/MM/dd"));
		if (attendanceCondtion.getType() == 0) {
			if (DateUtil.getMonth(weekBegin) != DateUtil.getMonth(weekDate)) {
				weekBegin = DateUtil.addWeeks(weekBegin, 1);
			}
		}
		List<AttendanceDayVo> weekDays = new ArrayList<AttendanceDayVo>();
		boolean falg = false;
		for (int i = 0; i <= 6; i++) {
			Date day = DateUtil.addDay(weekBegin, i);
			int cha = com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(day, now);
			AttendanceDayVo _AttendanceDayVo = new AttendanceDayVo();
			_AttendanceDayVo.setDay(DateUtil.getDayOfMonth(day));
			_AttendanceDayVo.setEvent("");
			_AttendanceDayVo.setDate(day);
			_AttendanceDayVo.setToday(false);
			if (cha == 0) {
				_AttendanceDayVo.setToday(true);
				falg = true;
			}
			weekDays.add(_AttendanceDayVo);
		}
		if (!falg) {
			AttendanceDayVo _AttendanceDayVo = weekDays.get(1);
			_AttendanceDayVo.setToday(true);
		}
		attendanceCondtion.setTime(DateUtil.format(weekDate, "yyyy/MM"));
		attendanceCondtion.setWeekDate(weekBegin);
		return successResult((Object) new AttendanceReturnVo(weekDays, attendanceCondtion));
	}

	/**
	 * 
	 * @author dlli5 at 2016年10月12日下午5:20:31
	 * @param condtion
	 * @param currentUser
	 * @return
	 */
	private List<String> getFollowUpTask(TaskCondtion condtion, UserInfoVo currentUser) {
		boolean isCeo = containRole(Role.CEO, currentUser.getRoleInfoVoList()) == 1;
		List<String> resultTaskFollowUpIds = new ArrayList<String>();
		// 获取今天应该显示
		List<TaskFollowUpInfoVo> taskFollowUpInfoVos = taskFollowUpInfoMapper.getTaskFollowUpsOfDate(condtion.getDay());
		if (!isCeo) {
			for (TaskFollowUpInfoVo taskFollowUpInfoVo : taskFollowUpInfoVos) {
				taskFollowUpInfoVo.setTemp1(taskFollowUpInfoVo.getFollowUpId());
				// 看TaskFollowUp的责任和可填写人是否包含我自己
				if (taskCoreFactory.needShow(taskFollowUpInfoVo, currentUser)) {
					resultTaskFollowUpIds.add(taskFollowUpInfoVo.getFollowUpId());
				}
			}
			List<String> followupIdList = taskInstanceInfoMapper.getIndividualTaskFollowupIdByAccountId(condtion.getDay(), currentUser.getAccountInfo().getId());
			// 园长可以查看所有园区的
			if (containRoles(currentUser.getRoleInfoVoList(), Role.CentreManager, Role.EducatorSecondInCharge)) {
				List<String> list = getFollowupIdsByCenterManager(condtion, currentUser);
				resultTaskFollowUpIds.addAll(list);
			}
			resultTaskFollowUpIds.addAll(followupIdList);
		} else {
			for (TaskFollowUpInfoVo taskFollowUpInfoVo : taskFollowUpInfoVos) {
				resultTaskFollowUpIds.add(taskFollowUpInfoVo.getFollowUpId());
			}
		}
		List<String> responsibleTaskIds = condtion.getResponsibleTaskIds();
		responsibleTaskIds.addAll(getSearchTaskFollowUp(taskFollowUpInfoVos, condtion.getResponsibleIds()));
		condtion.setResponsibleTaskIds(responsibleTaskIds);
		List<String> implementersTaskIds = condtion.getImplementersTaskIds();
		implementersTaskIds.addAll(getSearchTaskFollowUp(taskFollowUpInfoVos, condtion.getImplementersIds()));
		condtion.setImplementersTaskIds(implementersTaskIds);
		return resultTaskFollowUpIds;
	}

	private List<String> getFollowupIdsByCenterManager(TaskCondtion condtion, UserInfoVo currentUser) {
		List<String> list = new ArrayList<String>();
		// 获取该园区所有员工
		List<String> accounts = centersInfoMapper.getStaffAccountIdByCenterId(currentUser.getUserInfo().getCentersId());
		for (String account : accounts) {
			List<String> followupIdList = taskInstanceInfoMapper.getIndividualTaskFollowupIdByAccountId(condtion.getDay(), account);
			list.addAll(followupIdList);
		}
		return list;
	}

	/**
	 * 
	 * @author dlli5 at 2016年10月12日下午5:20:35
	 * @param condtion
	 * @param currentUser
	 * @return
	 */
	private List<String> getTask(TaskCondtion condtion, UserInfoVo currentUser) {
		boolean isCeo = containRole(Role.CEO, currentUser.getRoleInfoVoList()) == 1;
		List<String> resultTask = new ArrayList<String>();
		// 获取今天应该显示
		List<TaskInfoVo> taskInfoVos = taskInfoMapper.getUserTask(condtion.getDay());
		dealCasualRoster(currentUser, condtion);
		// 如果不是CEO 那么需要根据
		if (!isCeo) {
			for (TaskInfoVo taskInfoVo : taskInfoVos) {
				// 检测自己是否在责任人和可填写人其中
				if (taskCoreFactory.needShow(taskInfoVo, currentUser)) {
					resultTask.add(taskInfoVo.getId());
				}
			}
			// 获取Individual task责任人和可填写人是当前用户
			List<String> taskIdList = taskInstanceInfoMapper.getIndividualTaskIdByAccountId(condtion.getDay(), currentUser.getAccountInfo().getId());
			resultTask.addAll(taskIdList);
			// 园长可以查看所有园区的
			if (containRoles(currentUser.getRoleInfoVoList(), Role.CentreManager, Role.EducatorSecondInCharge)) {
				List<String> list = getTaskIdsByCenterManager(condtion, currentUser);
				resultTask.addAll(list);
			}
		} else {
			for (TaskInfoVo taskInfoVo : taskInfoVos) {
				resultTask.add(taskInfoVo.getId());
			}
		}
		condtion.setResponsibleTaskIds(getSearchTask(taskInfoVos, condtion.getResponsibleIds()));
		condtion.setImplementersTaskIds(getSearchTask(taskInfoVos, condtion.getImplementersIds()));
		return resultTask;
	}

	/**
	 * 通过责任人或可填写人获取对应的task
	 * 
	 * @author hxzhang at 2018年4月26日下午5:03:10
	 * @param condtion
	 * @return
	 */
	private List<String> getSearchTaskFollowUp(List<TaskFollowUpInfoVo> taskFollowUpInfoVos, List<String> accountIds) {
		List<String> resultTask = new ArrayList<String>();
		if (ListUtil.isEmpty(accountIds)) {
			return resultTask;
		}
		for (TaskFollowUpInfoVo vo : taskFollowUpInfoVos) {
			for (String accountId : accountIds) {
				List<UserInfoVo> userInfoVos = userInfoMapper.getUserList(new UserCondition(accountId));
				if (taskCoreFactory.needShow(vo, userInfoVos.get(0))) {
					resultTask.add(vo.getFollowUpId());
				}
			}
		}
		return resultTask;
	}

	private List<String> getSearchTask(List<TaskInfoVo> taskInfoVos, List<String> accountIds) {
		List<String> resultTask = new ArrayList<String>();
		if (ListUtil.isEmpty(accountIds)) {
			return resultTask;
		}
		for (TaskInfoVo vo : taskInfoVos) {
			for (String accountId : accountIds) {
				List<UserInfoVo> userInfoVos = userInfoMapper.getUserList(new UserCondition(accountId));
				if (taskCoreFactory.needShow(vo, userInfoVos.get(0))) {
					resultTask.add(vo.getId());
				}
			}
		}
		return resultTask;
	}

	/**
	 * 这里处理兼职排班可查看task, 获取兼职排班历史园区及角色记录
	 * 
	 * @author hxzhang at 2018年3月22日下午5:27:11
	 * @param currentUser
	 * @param condtion
	 */
	private void dealCasualRoster(UserInfoVo currentUser, TaskCondtion condtion) {
		List<RoleInfoVo> roleList = currentUser.getRoleInfoVoList();
		if (!containRoles(roleList, Role.Casual)) {
			return;
		}
		List<TempRosterCenterInfo> rosterCenterInfos = tempRosterCenterInfoMapper.getTempRoster(currentUser.getAccountInfo().getId(), condtion.getDay());
		List<RoleInfoVo> historyRoles = new ArrayList<RoleInfoVo>();
		List<String> rosterCnetreIds = new ArrayList<String>();
		for (TempRosterCenterInfo info : rosterCenterInfos) {
			RoleInfoVo vo = new RoleInfoVo();
			vo.setValue(roleInfoMapper.selectByPrimaryKey(info.getRoleId()).getValue());
			historyRoles.add(vo);
			rosterCnetreIds.add(info.getCentreId());
		}
		currentUser.setHistoryRoles(historyRoles);
		currentUser.setRosterCentreIds(rosterCnetreIds);
	}

	private List<String> getTaskIdsByCenterManager(TaskCondtion condtion, UserInfoVo currentUser) {
		List<String> list = new ArrayList<String>();
		// 获取该园区所有员工
		List<String> accounts = centersInfoMapper.getStaffAccountIdByCenterId(currentUser.getUserInfo().getCentersId());
		for (String account : accounts) {
			List<String> followupIdList = taskInstanceInfoMapper.getIndividualTaskIdByAccountId(condtion.getDay(), account);
			list.addAll(followupIdList);
		}
		return list;
	}

	@Override
	public ServiceResult<Object> getUserTaskList(TaskCondtion condtion, UserInfoVo currentUser) {
		dealCondition(condtion);
		handleSerchCondition(condtion, currentUser);
		List<TaskListVo> taskListVos = taskInstanceInfoMapper.getListVo(condtion);
		int totalSize = taskInstanceInfoMapper.getListVoCount(condtion);
		// 加载责任人头像
		taskFactory.dealAvatar(taskListVos);
		condtion.setTotalSize(totalSize);
		Pager<TaskListVo, TaskCondtion> pager = new Pager<TaskListVo, TaskCondtion>(taskListVos, condtion);
		return successResult((Object) pager);
	}

	@Override
	public ServiceResult<Object> getTaskTableView(TaskCondtion condtion, UserInfoVo currentUser) {
		dealConditionForTaskTable(condtion);
		dealTaskStatus(condtion);
		handleSerchConditionForTable(condtion, currentUser);
		dealSerchForPeople(condtion);
		String date1 = DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss SSS");
		List<TaskListVo> taskListVos = taskInstanceInfoMapper.getListVoForTable(condtion);
		String date2 = DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss SSS");
		int totalSize = taskInstanceInfoMapper.getListVoCountForTable(condtion);
		String date3 = DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss SSS");
		// 处理查询结果
		dealResultData(taskListVos);
		String date4 = DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss SSS");
		System.err.println(date1 + "---" + date2 + "---" + date3 + "---" + date4);
		condtion.setTotalSize(totalSize);
		Pager<TaskListVo, TaskCondtion> pager = new Pager<TaskListVo, TaskCondtion>(taskListVos, condtion);
		return successResult((Object) pager);
	}

	/**
	 * 处理责任人和可填写人搜索
	 * 
	 * @author hxzhang at 2018年5月6日下午2:39:39
	 * @param condtion
	 */
	private void dealSerchForPeople(TaskCondtion condtion) {
		// 获取责任人可见task
		List<String> responsibleIds = condtion.getResponsibleIds();
		if (responsibleIds != null && ListUtil.isNotEmpty(responsibleIds)) {
			condtion.setTaskInstanceIds(taskResponsibleLogInfoMapper.getTaskIdByByInstanceId(responsibleIds));
		} else {
			condtion.setTaskInstanceIds(new ArrayList<String>());
		}
	}

	// 处理查询条件，gfwang 抽出
	private void handleSerchConditionForTable(TaskCondtion condtion, UserInfoVo currentUser) {
		boolean isCeo = containRole(Role.CEO, currentUser.getRoleInfoVoList()) == 1;
		boolean isCentreManager = (containRole(Role.CentreManager, currentUser.getRoleInfoVoList()) == 1)
				|| (containRole(Role.EducatorSecondInCharge, currentUser.getRoleInfoVoList()) == 1);

		if (containRoles(currentUser.getRoleInfoVoList(), Role.Educator)) {
			// condtion.setRoomId(currentUser.getUserInfo().getRoomId());
		}
		// 兼职排班可以查看其所排的所有园区task,在这里去除兼职的园区信息,通过下面处理其可见task
		if (!containRoles(currentUser.getRoleInfoVoList(), Role.Casual)) {
			condtion.setUserCentreId(currentUser.getUserInfo().getCentersId());
		}

		// 找到今天可见状态的 task 模板id
		List<String> taskModelList = getTask(condtion, currentUser);
		condtion.setTaskIds(taskModelList);
		// 找到今天可见的taskFollow id
		List<String> taskFollowUpIds = getFollowUpTask(condtion, currentUser);
		condtion.setTaskFollowUpIds(taskFollowUpIds);
		// String sortName = condtion.getSortName() + " " +
		// condtion.getSortOrder() + ","
		// + "v.task_type asc,v.create_time
		// asc,v.`task_model_id`,v.`value_id`,v.`centre_id`,v.`room_id`,v.`group_id`,v.`obj_id`";
		// condtion.setSortName(sortName);
		condtion.setStartSize(condtion.getPageIndex());
		if (!isCeo) {
			condtion.setAccountId(currentUser.getAccountInfo().getId());
			// 如果是园长那么对21 22 设置可见centreId范围
			condtion.setCentreIdOfManager(UUID.randomUUID().toString());
			if (isCentreManager) {
				condtion.setCentreIdOfManager(currentUser.getUserInfo().getCentersId());
			}
		}
		if (isCeo || isCentreManager) {
			condtion.setAdmin(true);
		}
	}

	@Override
	public ServiceResult<Object> getExportData(TaskCondtion condtion, UserInfoVo currentUser) {
		dealConditionForTaskTable(condtion);
		dealTaskStatus(condtion);
		handleSerchConditionForTable(condtion, currentUser);
		condtion.setPageSize(0);
		List<TaskListVo> taskListVos = taskInstanceInfoMapper.getListVoForTable(condtion);
		// 处理查询结果
		dealResultData(taskListVos);
		// 处理导出excel数据模型
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		for (TaskListVo vo : taskListVos) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("Dates", vo.getEndDate() == null ? "" : DateUtil.format(vo.getEndDate(), "dd/MM/yyyy"));
			map.put("Centre", vo.getcName());
			map.put("Room", vo.getrName());
			map.put("Task Name", vo.getTaskName());
			String responsibleNames = "";
			for (UserAvatarVo user : vo.getAvatar()) {
				responsibleNames += user.getFullName().trim() + ",";
			}
			map.put("Responsible Person", "".equals(responsibleNames) ? "" : responsibleNames.substring(0, responsibleNames.length() - 1));
			String implementersNames = "";
			for (UserAvatarVo user : vo.getImplementersAvatar()) {
				implementersNames += user.getFullName().trim() + ",";
			}
			map.put("Task Implementers", "".equals(implementersNames) ? "" : implementersNames.substring(0, implementersNames.length() - 1));
			map.put("Subject Person", vo.getSubPerson());
			map.put("NQS", vo.getNqsStr());
			switch (vo.getStatu()) {
			case 0:
				map.put("Task Status", "Pending");
				break;
			case 1:
				map.put("Task Status", "Complete");
				break;
			case 2:
				map.put("Task Status", "Overdue");
				break;
			case -1:
				map.put("Task Status", "Obsolete");
				break;
			}
			resultList.add(map);
		}
		return successResult((Object) resultList);
	}

	private void dealTaskStatus(TaskCondtion condition) {
		if (ListUtil.isEmpty(condition.getTaskStatus())) {
			condition.setStatusList(null);
			return;
		}
		List<Short> list = new ArrayList<Short>();
		for (String s : condition.getTaskStatus()) {
			if (s.equals("Pending")) {
				list.add(ProgramState.Pending.getValue());
			}
			if (s.equals("Complete")) {
				list.add(ProgramState.Complete.getValue());
			}
			if (s.equals("Overdue")) {
				list.add(ProgramState.OverDue.getValue());
			}
			if (s.equals("Obsolete")) {
				list.add(ProgramState.Discard.getValue());
			}
		}
		condition.setStatusList(list);
	}

	private void dealConditionForTaskTable(TaskCondtion condition) {
		if (ListUtil.isEmpty(condition.getNqs())) {
			condition.setNqsList(null);
			return;
		}
		List<String> list = new ArrayList<String>();
		for (String n : condition.getNqs()) {
			if (n.toUpperCase().equals("QA1")) {
				list.add("1");
			} else if (n.toUpperCase().equals("QA2")) {
				list.add("2");
			} else if (n.toUpperCase().equals("QA3")) {
				list.add("3");
			} else if (n.toUpperCase().equals("QA4")) {
				list.add("4");
			} else if (n.toUpperCase().equals("QA5")) {
				list.add("5");
			} else if (n.toUpperCase().equals("QA6")) {
				list.add("6");
			} else if (n.toUpperCase().equals("QA7")) {
				list.add("7");
			}
		}
		condition.setNqsList(list);
	}

	private void dealResultData(List<TaskListVo> taskListVos) {
		Map<String, String> map = getCentreRoomForMap();
		for (TaskListVo vo : taskListVos) {
			// 获取NQS版本信息
			dealNqsVersion(vo);
			// 处理centre,room
			// dealCentreRoom(vo, map);
			// 加载责任人头像
			String date11 = DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss SSS");
			taskFactory.dealAvatar(vo);
			String date22 = DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss SSS");
			// 加载可填写人头像
			taskFactory.dealImplementersAvatar(vo);
			String date33 = DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss SSS");
			System.err.println(date11 + "---" + date22 + "---" + date33);
			// 处理Subject Person
			// dealSubjectPerson(vo);
		}
	}

	/**
	 * 处理Subject Person
	 * 
	 * @author hxzhang at 2018年4月25日上午11:03:23
	 * @param vo
	 */
	private void dealSubjectPerson(TaskListVo vo) {
		TaskType taskType = TaskType.getType(vo.getTaskType());
		switch (taskType) {
		case CustomTask:
			vo.setSubPerson(userInfoMapper.getFullNameByAccountId(vo.getObjId()));
			break;
		case Interest:
		case InterestFollowup:
		case Observation:
		case ObservationFollowup:
		case LearningStory:
		case LearningStoryFollowup:
			vo.setSubPerson(vo.getTaskName().split("'")[0].trim());
			break;
		}
	}

	/**
	 * 获取Centre,Room信息
	 * 
	 * @author hxzhang at 2018年4月25日上午10:55:54
	 * @return
	 */
	private Map<String, String> getCentreRoomForMap() {
		// 获取所有CENTRE
		List<CentersInfo> allCentres = centersInfoMapper.getAllCentersInfos();
		// 获取所有ROOM
		List<RoomInfo> allRooms = roomInfoMapper.getAllRoom();

		Map<String, String> map = new HashMap<String, String>();
		for (CentersInfo c : allCentres) {
			map.put(c.getId(), c.getName());
		}
		for (RoomInfo r : allRooms) {
			map.put(r.getId(), r.getName());
		}

		return map;
	}

	/**
	 * 处理Centre和Room名称
	 * 
	 * @author hxzhang at 2018年4月24日下午4:57:54
	 * @param taskListVos
	 */
	private void dealCentreRoom(TaskListVo vo, Map<String, String> map) {
		vo.setcName(map.get(vo.getCentreId()));
		vo.setrName(map.get(vo.getRoomId()));
	}

	// 获取NQS版本信息
	private void dealNqsVersion(TaskListVo vo) {
		TaskType taskType = TaskType.getType(vo.getTaskType());
		switch (taskType) {
		case CustomTask:
			if (vo.getDataType() == 0) {
				vo.setNqsVos(nqsMapper.getNqsVoListByObjId(vo.getTaskModelId()));
			} else {
				TaskInstanceInfo taskInstanceInfo = taskInstanceInfoMapper.selectByPrimaryKey(vo.getId());
				vo.setNqsVos(nqsMapper.getNqsVoListByObjId(taskInstanceInfo.getTaskModelId()));
			}
			break;

		case ExploreCurriculum:
		case GrowCurriculum:
		case SchoolReadiness:
			ProgramTaskExgrscInfo exgrscInfo = programTaskExgrscInfoMapper.selectByPrimaryKey(vo.getValueId());
			vo.setNqsVos(nqsMapper.getNqsVoListByObjId(exgrscInfo.getNewsfeedId()));
			break;

		case Lesson:
			ProgramLessonInfo lessonInfo = programLessonInfoMapper.selectByPrimaryKey(vo.getValueId());
			vo.setNqsVos(nqsMapper.getNqsVoListByObjId(lessonInfo.getNewsfeedId()));
			break;

		case WeeklyEvalution:
			ProgramWeeklyEvalutionInfo weeklyEvalutionInfo = programWeeklyEvalutionInfoMapper.selectByPrimaryKey(vo.getValueId());
			vo.setNqsVos(nqsMapper.getNqsVoListByObjId(weeklyEvalutionInfo.getNewsfeedId()));
			break;

		case MeetingTask:
			vo.setNqsVos(nqsMapper.getNqsVoListByObjId(meetingInfoMapper.getTempIdByRowId(vo.getValueId())));
			break;

		default:
			break;
		}
		vo.setNqsStr(dealNqsStr(vo.getNqsVos()));
	}

	/**
	 * 处理nqs字符串
	 * 
	 * @author hxzhang at 2018年4月24日下午7:52:05
	 * @param nqsVos
	 * @return
	 */
	private String dealNqsStr(List<NqsVo> nqsVos) {
		String str = "";
		if (ListUtil.isEmpty(nqsVos)) {
			return str;
		}
		for (NqsVo vo : nqsVos) {
			str += vo.getVersion() + ", ";
		}
		return str.substring(0, str.trim().length() - 1);
	}

	private void dealCondition(TaskCondtion condition) {
		if (StringUtil.isNotEmpty(condition.getNqsVersion())) {
			if (condition.getNqsVersion().toUpperCase().equals("QA1")) {
				condition.setSelectNqs("1");
			} else if (condition.getNqsVersion().toUpperCase().equals("QA2")) {
				condition.setSelectNqs("2");
			} else if (condition.getNqsVersion().toUpperCase().equals("QA3")) {
				condition.setSelectNqs("3");
			} else if (condition.getNqsVersion().toUpperCase().equals("QA4")) {
				condition.setSelectNqs("4");
			} else if (condition.getNqsVersion().toUpperCase().equals("QA5")) {
				condition.setSelectNqs("5");
			} else if (condition.getNqsVersion().toUpperCase().equals("QA6")) {
				condition.setSelectNqs("6");
			} else if (condition.getNqsVersion().toUpperCase().equals("QA7")) {
				condition.setSelectNqs("7");
			} else {
				condition.setSelectNqs(condition.getNqsVersion());
			}
		} else {
			condition.setSelectNqs(null);
		}
		// if(StringUtil.isNotEmpty(condition.getSelectNqs())){
		// condition.setSelectNqsIds(taskInstanceInfoMapper.nqsSelectVer(condition.getSelectNqs()));
		// }
	}

	// 处理查询条件，gfwang 抽出
	private void handleSerchCondition(TaskCondtion condtion, UserInfoVo currentUser) {
		boolean isCeo = containRole(Role.CEO, currentUser.getRoleInfoVoList()) == 1;
		boolean isCentreManager = (containRole(Role.CentreManager, currentUser.getRoleInfoVoList()) == 1)
				|| (containRole(Role.EducatorSecondInCharge, currentUser.getRoleInfoVoList()) == 1);

		if (containRoles(currentUser.getRoleInfoVoList(), Role.Educator)) {
			// condtion.setRoomId(currentUser.getUserInfo().getRoomId());
		}
		// 兼职排班可以查看其所排的所有园区task,在这里去除兼职的园区信息,通过下面处理其可见task
		if (!containRoles(currentUser.getRoleInfoVoList(), Role.Casual)) {
			condtion.setUserCentreId(currentUser.getUserInfo().getCentersId());
		}

		// if(!isCeo){
		// if(StringUtil.isEmpty(condtion.getCentreId())){
		// condtion.setCentreId(currentUser.getUserInfo().getCentersId());
		// }
		// }
		// 找到今天可见状态的 task 模板id
		List<String> taskModelList = getTask(condtion, currentUser);
		condtion.setTaskIds(taskModelList);
		// 找到今天可见的taskFollow id
		List<String> taskFollowUpIds = getFollowUpTask(condtion, currentUser);
		condtion.setTaskFollowUpIds(taskFollowUpIds);
		condtion.setSortName("v.end_date ASC,v.task_type asc,v.create_time asc,v.`task_model_id`,v.`value_id`,v.`centre_id`,v.`room_id`,v.`group_id`,v.`obj_id`");
		condtion.setSortOrder("ASC");
		condtion.setStartSize(condtion.getPageIndex());
		if (!isCeo) {
			condtion.setAccountId(currentUser.getAccountInfo().getId());
			// 如果是园长那么对21 22 设置可见centreId范围
			condtion.setCentreIdOfManager(UUID.randomUUID().toString());
			if (isCentreManager) {
				condtion.setCentreIdOfManager(currentUser.getUserInfo().getCentersId());
			}
		}
		if (isCeo || isCentreManager) {
			condtion.setAdmin(true);
		}
	}

	@Override
	public ServiceResult<Object> getTaskCount(TaskCondtion condtion, UserInfoVo currentUser) {
		// add by gfwang
		dealCondition(condtion);
		handleSerchCondition(condtion, currentUser);
		condtion.setStatu(ProgramState.Pending.getValue());
		int pedingCount = taskInstanceInfoMapper.getListVoCount(condtion);
		condtion.setStatu(ProgramState.OverDue.getValue());
		int overDueCount = taskInstanceInfoMapper.getListVoCount(condtion);
		// 加载责任人头像
		return successResult((Object) Arrays.asList(new Integer[] { pedingCount, overDueCount }));
	}

	@Override
	public ServiceResult<Object> getTask(String taskId, UserInfoVo currentUser) throws Exception {
		TaskInfoVo taskInfoVo = new TaskInfoVo();
		TaskInfo taskInfo = taskInfoMapper.selectByPrimaryKey(taskId);
		PropertyUtils.copyProperties(taskInfoVo, taskInfo);
		taskInfoVo.setFormId(taskFormRelationInfoMapper.getByTaskId(taskId));

		// 获取各个下拉列表
		List<TaskVisibleInfo> taskVisibleInfos = taskVisibleInfoMapper.selectBySourceId(taskInfo.getSelectObjId());
		// 转换为模拟用户选择当作参数处理
		List<PersonSelectVo> selectList = taskFactory.convertTaskVisible(taskVisibleInfos);

		taskInfoVo.setSelectTaskVisiblesSelect(
				taskFactory.filterSelected(getSelectList(taskInfo.getTaskCategory(), currentUser, taskInfo.getSelectObjId(), null).getReturnObj()));
		taskInfoVo.setResponsibleTaskVisiblesSelect(taskFactory
				.filterSelected(getResponsibleList(selectList, taskInfo.getTaskCategory(), currentUser, taskInfo.getResponsiblePeopleId(), null).getReturnObj()));
		taskInfoVo.setTaskImplementersVisiblesSelect(taskFactory
				.filterSelected(getImplementersList(selectList, taskInfo.getTaskCategory(), currentUser, taskInfo.getTaskImplementersId(), null).getReturnObj()));

		// 获取follwup
		FollowUpCondtion followUpCondtion = new FollowUpCondtion();
		followUpCondtion.initAll();
		followUpCondtion.setTaskId(taskInfo.getId());
		followUpCondtion.setSortName("create_time");
		followUpCondtion.setSortOrder("asc");
		List<TaskFollowUpInfo> followUpInfos = taskFollowUpInfoMapper.getList(followUpCondtion);
		if (ListUtil.isNotEmpty(followUpInfos)) {
			for (TaskFollowUpInfo taskFollowUpInfo : followUpInfos) {
				taskFollowUpInfo.setFormId(taskFormRelationInfoMapper.getByTaskId(taskFollowUpInfo.getId()));
				taskFollowUpInfo.setResponsibleTaskVisiblesSelect(taskFactory.filterSelected(
						getResponsibleList(selectList, taskInfo.getTaskCategory(), currentUser, taskFollowUpInfo.getResponsiblePersonId(), null).getReturnObj()));
				taskFollowUpInfo.setTaskImplementersVisiblesSelect(taskFactory.filterSelected(
						getImplementersList(selectList, taskInfo.getTaskCategory(), currentUser, taskFollowUpInfo.getCompleteStepId(), null).getReturnObj()));
			}
		}
		taskInfoVo.setFollowUpInfos(followUpInfos);

		// 获取频率
		TaskFrequencyInfo taskFrequencyInfo = taskFrequencyInfoMapper.selectByTaskId(taskId);
		taskInfoVo.setTaskFrequencyInfo(taskFrequencyInfo);
		taskInfoVo.setNqsList(nqsMapper.getNqsVoListByObjId(taskId));
		return successResult((Object) taskInfoVo);
	}

	@Override
	public ServiceResult<Object> getTaskList(TaskCondtion condtion, UserInfoVo currentUser) {
		// condtion.setStatu(DeleteFlag.Default.getValue());
		condtion.setCurrentFlag(true);
		condtion.setSortName("create_time");
		condtion.setSortOrder("DESC");
		condtion.setStartSize(condtion.getPageIndex());
		String keyWord = condtion.getKeyWord();
		condtion.setKeyWord(SqlUtil.likeEscape(condtion.getKeyWord()));
		List<TaskInfo> taskInfos = taskInfoMapper.getList(condtion);
		int total = taskInfoMapper.getListCount(condtion);
		condtion.setTotalSize(total);
		condtion.setKeyWord(keyWord);
		Pager<TaskInfo, TaskCondtion> pager = new Pager<TaskInfo, TaskCondtion>(taskInfos, condtion);
		return successResult((Object) pager);
	}

	@Override
	public ServiceResult<Object> updateStatu(String taskId, int statu, UserInfoVo currentUser) {
		TaskInfo taskInfo = taskInfoMapper.selectByPrimaryKey(taskId);
		// 如果禁用 那么判断是否有shif任务
		if ((short) statu == DeleteFlag.Delete.getValue() && taskInfo.getShiftFlag()) {
			int count = rosterShiftTaskInfoMapper.haveUseShiftTask(taskId);
			if (count > 0) {
				return failResult(getTipMsg("task.updateStatu.shfit.used"));
			}
		}

		taskInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
		taskInfo.setUpdateTime(new Date());
		taskInfo.setStatu((short) statu);
		taskInfoMapper.updateByPrimaryKey(taskInfo);
		return successResult(getTipMsg("task.updateStatu.success"));
	}

	@Override
	public ServiceResult<List> getImplementersList(List<PersonSelectVo> selectList, int category, UserInfoVo currentUser, String taskId, String findText) {
		List<TaskVisibleInfo> taskVisibleInfos = new ArrayList<TaskVisibleInfo>();
		if (StringUtil.isNotEmpty(taskId)) {
			taskVisibleInfos = taskVisibleInfoMapper.selectBySourceId(taskId);
		}
		List result = new ArrayList();
		TaskCategory taskCategory = TaskCategory.getType((short) category);
		switch (taskCategory) {
		case CENTER: {
			List<UserVo> userInfos = new ArrayList<UserVo>();
			boolean onlySelectOneCentre = taskCentreFactory.haveOnlySelectOneCenter(selectList);
			if (onlySelectOneCentre) {
				UserDetailCondition userCondtion = new UserDetailCondition();
				userCondtion.initAll();
				userCondtion.setUserType(UserType.Staff.getValue());
				if (ListUtil.isEmpty(taskVisibleInfos)) {
					userCondtion.setStatus(ArchivedStatus.UnArchived.getValue());
				}
				List<String> centresList = taskCentreFactory.getSelectCentreIds(selectList);
				userCondtion.setCentreIds(centresList);
				userInfos = userInfoMapper.getListOfUserVo(userCondtion);
			}
			result = taskCentreFactory.dealCenterOfImplementers(userInfos, taskVisibleInfos, findText);
		}
			break;
		case ROOM: {
			List<UserVo> userInfos = new ArrayList<UserVo>();
			boolean selectAllCenter = taskRoomFactory.haveOnlySelectOneRoom(selectList);
			if (selectAllCenter) {
				UserDetailCondition userCondtion = new UserDetailCondition();
				userCondtion.initAll();
				userCondtion.setUserType(UserType.Staff.getValue());
				if (ListUtil.isEmpty(taskVisibleInfos)) {
					userCondtion.setStatus(ArchivedStatus.UnArchived.getValue());
				}
				List<String> roomIds = taskRoomFactory.getSelectRoomIds(selectList);
				userCondtion.setRoomIds(roomIds);
				userInfos = userInfoMapper.getListOfUserVo(userCondtion);
			}
			result = taskRoomFactory.dealRoomOfImplementers(userInfos, taskVisibleInfos, findText);
		}
			break;
		case CHILDRRENN: {
			List<UserVo> userInfos = new ArrayList<UserVo>();
			boolean selectAllCenter = taskChildFactory.haveOnlySelectChildrenInSameRoom(selectList);
			if (selectAllCenter) {
				UserDetailCondition userCondtion = new UserDetailCondition();
				userCondtion.initAll();
				userCondtion.setUserType(UserType.Staff.getValue());
				if (ListUtil.isEmpty(taskVisibleInfos)) {
					userCondtion.setStatus(ArchivedStatus.UnArchived.getValue());
				}
				List<String> roomIds = taskChildFactory.getSelectRoomIds(selectList);
				userCondtion.setRoomIds(roomIds);
				userInfos = userInfoMapper.getListOfUserVo(userCondtion);
			}
			result = taskChildFactory.dealChildOfImplementers(userInfos, taskVisibleInfos, findText);
		}
			break;
		case STAFF: {
			List<UserVo> userInfos = new ArrayList<UserVo>();
			boolean selectAllCenter = taskStaffFactory.haveOnlySelectOneCenter(selectList);
			if (selectAllCenter) {
				UserDetailCondition userCondtion = new UserDetailCondition();
				userCondtion.initAll();
				userCondtion.setUserType(UserType.Staff.getValue());
				if (ListUtil.isEmpty(taskVisibleInfos)) {
					userCondtion.setStatus(ArchivedStatus.UnArchived.getValue());
				}
				List<String> centresList = taskStaffFactory.getSelectCentreIds(selectList);
				userCondtion.setCentreIds(centresList);
				userInfos = userInfoMapper.getListOfUserVo(userCondtion);
			}
			result = taskStaffFactory.dealStaffOfImplementers(userInfos, taskVisibleInfos, findText);
		}
			break;
		default:
			break;
		}
		if (ListUtil.isNotEmpty(taskVisibleInfos)) {
			for (TaskVisibleInfo taskVisibleInfo : taskVisibleInfos) {
				boolean contain = false;
				for (Object object : result) {
					PersonSelectVo resultItem = (PersonSelectVo) object;
					if (resultItem.getId().split(",")[0].equals(taskVisibleInfo.getObjId())) {
						contain = true;
					}
				}
				if (!contain) {
					if (taskVisibleInfo.getType() == TaskVisibleType.USER.getValue()) {
						UserInfo userInfo = userInfoMapper.getUserInfoByAccountId(taskVisibleInfo.getObjId());
						PersonSelectVo select = new PersonSelectVo(TaskVisibleType.USER.getValue(), taskVisibleInfo.getObjId(), userInfo.getFullName());
						result.add(select);
					} else if (taskVisibleInfo.getType() == TaskVisibleType.ROOM.getValue()) {

					} else if (taskVisibleInfo.getType() == TaskVisibleType.CENTER.getValue()) {

					}
				}
			}
		}
		return successResult(result);
	}

	private void dealTaskVisibleInfo(TaskInfoVo obj, UserInfoVo currentUser, TaskInfo taskInfo) {
		List<TaskVisibleInfo> selectVisibleInfos = obj.getSelectTaskVisibles();
		if (ListUtil.isNotEmpty(selectVisibleInfos)) {
			for (TaskVisibleInfo taskVisibleInfo : selectVisibleInfos) {
				taskVisibleInfo.setSourceId(taskInfo.getSelectObjId());
				taskVisibleInfoMapper.insert(taskVisibleInfo);
			}
		}

		List<TaskVisibleInfo> responsibleList = obj.getResponsibleTaskVisibles();
		if (ListUtil.isNotEmpty(responsibleList)) {
			for (TaskVisibleInfo taskVisibleInfo : responsibleList) {
				taskVisibleInfo.setSourceId(taskInfo.getResponsiblePeopleId());
				taskVisibleInfoMapper.insert(taskVisibleInfo);
			}
		}

		List<TaskVisibleInfo> taskImplementersInfos = obj.getTaskImplementersVisibles();
		if (ListUtil.isNotEmpty(taskImplementersInfos)) {
			for (TaskVisibleInfo taskVisibleInfo : taskImplementersInfos) {
				taskVisibleInfo.setSourceId(taskInfo.getTaskImplementersId());
				taskVisibleInfoMapper.insert(taskVisibleInfo);
			}
		}
	}

	private void dealTaskFollowUpInfo(TaskInfoVo obj, UserInfoVo currentUser, TaskInfo taskInfo) {
		List<TaskFollowUpInfo> newFollowUpInfos = obj.getFollowUpInfos();
		if (ListUtil.isNotEmpty(newFollowUpInfos)) {
			Date createTime = new Date();
			int scond = 0;
			for (TaskFollowUpInfo taskFollowUpInfo : newFollowUpInfos) {
				taskFollowUpInfo.setId(UUID.randomUUID().toString());
				taskFollowUpInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
				taskFollowUpInfo.setCreateTime(DateUtil.addSeconds(createTime, scond++));
				taskFollowUpInfo.setDeleteFlag(DeleteFlag.Default.getValue());
				taskFollowUpInfo.setResponsiblePersonId(UUID.randomUUID().toString());
				taskFollowUpInfo.setCompleteStepId(UUID.randomUUID().toString());
				taskFollowUpInfo.setFollowFlag(BooleanUtils.isTrue(taskFollowUpInfo.getFollowFlag()));
				taskFollowUpInfo.setStatu(TaskFollowUpStatu.Default.getValue());

				// 插入模板
				taskFormRelationInfoMapper.insert(new TaskFormRelationInfo(taskFollowUpInfo.getId(), taskFollowUpInfo.getFormId()));

				List<TaskVisibleInfo> responsibleListItem = taskFollowUpInfo.getResponsibleTaskVisibles();
				if (ListUtil.isNotEmpty(responsibleListItem)) {
					for (TaskVisibleInfo taskVisibleInfo : responsibleListItem) {
						taskVisibleInfo.setSourceId(taskFollowUpInfo.getResponsiblePersonId());
						taskVisibleInfoMapper.insert(taskVisibleInfo);
					}
				}

				List<TaskVisibleInfo> taskImplementersInfosItem = taskFollowUpInfo.getTaskImplementersVisibles();
				if (ListUtil.isNotEmpty(taskImplementersInfosItem)) {
					for (TaskVisibleInfo taskVisibleInfo : taskImplementersInfosItem) {
						taskVisibleInfo.setSourceId(taskFollowUpInfo.getCompleteStepId());
						taskVisibleInfoMapper.insert(taskVisibleInfo);
					}
				}

				taskFollowUpInfo.setTaskId(taskInfo.getId());
				taskFollowUpInfoMapper.insert(taskFollowUpInfo);
			}
		}
	}

	private void dealTaskFrequency(TaskInfoVo obj, UserInfoVo currentUser, TaskInfo taskInfo) {
		if (!(obj.getTaskCategory() == TaskCategory.CENTER.getValue() && BooleanUtils.isTrue(obj.getShiftFlag()))) {
			obj.getTaskFrequencyInfo().setTaskId(taskInfo.getId());
			obj.getTaskFrequencyInfo().setId(UUID.randomUUID().toString());
			obj.getTaskFrequencyInfo().setCreateAccountId(currentUser.getAccountInfo().getId());
			obj.getTaskFrequencyInfo().setCreateTime(new Date());
			obj.getTaskFrequencyInfo().setDeleteFlag(DeleteFlag.Default.getValue());
			taskFrequencyInfoMapper.insert(obj.getTaskFrequencyInfo());
		}
	}

	private ServiceResult<Object> saveTask(TaskInfoVo obj, UserInfoVo currentUser, String code) {
		TaskInfo taskInfo = obj;
		taskInfo.setId(UUID.randomUUID().toString());
		taskInfo.setTaskCode(code);
		taskInfo.setDeleteFlag(DeleteFlag.Default.getValue());
		taskInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
		taskInfo.setCreateTime(new Date());
		taskInfo.setSelectObjId(UUID.randomUUID().toString());
		taskInfo.setResponsiblePeopleId(UUID.randomUUID().toString());
		taskInfo.setTaskImplementersId(UUID.randomUUID().toString());
		taskInfo.setShiftFlag(BooleanUtils.isTrue(obj.getShiftFlag()));
		taskInfo.setFollowFlag(BooleanUtils.isTrue(obj.getFollowFlag()));
		taskInfo.setStatu(DeleteFlag.Default.getValue());
		taskInfo.setCurrentFlag(true);
		taskInfo.setImmediateEffect(obj.getImmediateEffect());

		taskFormRelationInfoMapper.insert(new TaskFormRelationInfo(taskInfo.getId(), obj.getFormId()));

		// 处理TaskVisibleInfo
		dealTaskVisibleInfo(obj, currentUser, taskInfo);

		// TaskFollowUpInfo
		dealTaskFollowUpInfo(obj, currentUser, taskInfo);

		// dealTaskFrequency
		dealTaskFrequency(obj, currentUser, taskInfo);

		// add by gfwang
		agendaService.saveNqsRelation(obj.getId(), obj.getNqsList());

		taskInfoMapper.insert(taskInfo);

		obj.setStatu(taskInfo.getStatu());
		obj.setId(taskInfo.getId());
		return successResult((Object) obj);
	}

	private ServiceResult<Object> updateTask(TaskInfoVo obj, UserInfoVo currentUser) {
		TaskInfo taskInfo = taskInfoMapper.selectByPrimaryKey(obj.getId());
		taskInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
		taskInfo.setUpdateTime(new Date());
		taskInfo.setTaskName(obj.getTaskName());
		taskInfo.setTaskCategory(obj.getTaskCategory());
		taskInfo.setShiftFlag(BooleanUtils.isTrue(obj.getShiftFlag()));
		taskInfo.setFollowFlag(BooleanUtils.isTrue(obj.getFollowFlag()));
		taskInfo.setImmediateEffect(obj.getImmediateEffect());

		taskFormRelationInfoMapper.deleteByTaskId(taskInfo.getId());
		taskFormRelationInfoMapper.insert(new TaskFormRelationInfo(taskInfo.getId(), obj.getFormId()));

		// 删除原来的记录
		taskVisibleInfoMapper.deleteBySourceId(taskInfo.getSelectObjId());
		taskVisibleInfoMapper.deleteBySourceId(taskInfo.getResponsiblePeopleId());
		taskVisibleInfoMapper.deleteBySourceId(taskInfo.getTaskImplementersId());
		// 获取原来的FollowUp
		FollowUpCondtion followUpCondtion = new FollowUpCondtion();
		followUpCondtion.initAll();
		followUpCondtion.setTaskId(taskInfo.getId());
		List<TaskFollowUpInfo> followUpInfos = taskFollowUpInfoMapper.getList(followUpCondtion);
		if (ListUtil.isNotEmpty(followUpInfos)) {
			for (TaskFollowUpInfo taskFollowUpInfo : followUpInfos) {
				taskVisibleInfoMapper.deleteBySourceId(taskFollowUpInfo.getResponsiblePersonId());
				taskVisibleInfoMapper.deleteBySourceId(taskFollowUpInfo.getCompleteStepId());
				taskFormRelationInfoMapper.deleteByTaskId(taskFollowUpInfo.getId());
			}
			// 删除FollowUp
			taskFollowUpInfoMapper.deleteByTaskId(taskInfo.getId());
		}
		// 删除TaskCategory
		taskFrequencyInfoMapper.deleteByTaskId(taskInfo.getId());

		// 处理TaskVisibleInfo
		dealTaskVisibleInfo(obj, currentUser, taskInfo);

		// TaskFollowUpInfo
		dealTaskFollowUpInfo(obj, currentUser, taskInfo);

		// dealTaskFrequency
		dealTaskFrequency(obj, currentUser, taskInfo);

		// add by gfwang
		agendaService.saveNqsRelation(obj.getId(), obj.getNqsList());

		taskInfoMapper.updateByPrimaryKey(taskInfo);
		return successResult((Object) obj);
	}

	private void dealSaveForm(TaskInfoVo obj) {
		// task中的form（workflow）
		FormInfoVO taskFormObj = obj.getTaskFormObj();
		// 如果form为空（没有修改），不需要操作
		if (taskFormObj != null) {
			String taskFormObjId = formService.saveForm(taskFormObj).getReturnObj();
			obj.setFormId(taskFormObjId);
			System.out.println(taskFormObjId);
		}

		// followUp中的form（workflow）
		List<TaskFollowUpInfo> followUpInfos = obj.getFollowUpInfos();
		for (TaskFollowUpInfo followUpInfo : followUpInfos) {
			FormInfoVO followUpObj = followUpInfo.getTaskFormObj();
			// 如果form为空（没有修改），不需要操作
			if (followUpObj != null) {
				String followUpObjId = formService.saveForm(followUpObj).getReturnObj();
				followUpInfo.setFormId(followUpObjId);
				System.out.println(followUpObjId);
			}
		}
	}

	@Override
	public ServiceResult<Object> saveOrUpdateTask(TaskInfoVo obj, UserInfoVo currentUser) throws Exception {
		ServiceResult<Object> reuslt = null;
		Date day = new Date();

		if (StringUtil.isEmpty(obj.getId())) {
			dealSaveForm(obj);
			reuslt = saveTask(obj, currentUser, UUID.randomUUID().toString());
			taskCoreService.dealInitTaskInstance(day, obj, currentUser);
			reuslt.setMsg(getTipMsg("staff_save"));
			return reuslt;
		}

		TaskInfo oldTask = taskInfoMapper.selectByPrimaryKey(obj.getId());
		if (!oldTask.getShiftFlag()) {
			TaskFrequencyInfo taskFrequencyInfo = taskFrequencyInfoMapper.selectByTaskId(obj.getId());
			TaskFrequenceRepeatType frequenceRepeatType = TaskFrequenceRepeatType.getType(taskFrequencyInfo.getRepeatType());

			// 是否有完成的 或者 overdue的 如果有 那么就不删除
			int haveOld = taskInstanceInfoMapper.getTaskInstanceSizeOfStatu(obj.getId(), ProgramState.Pending.getValue(), false);
			// 提示是否有未过期的东西 如果有那么提示
			int count = taskInstanceInfoMapper.getTaskInstanceSizeOfStatu(obj.getId(), ProgramState.Pending.getValue(), true);

			boolean deleteTask = (count > 0) && (frequenceRepeatType != TaskFrequenceRepeatType.WhenRequired);

			boolean imageFlag = (haveOld > 0 && frequenceRepeatType != TaskFrequenceRepeatType.WhenRequired)
					|| (haveOld + count > 0 && frequenceRepeatType == TaskFrequenceRepeatType.WhenRequired);

			if (deleteTask && !obj.getOverWrite() && obj.getImmediateEffect()) {
				return failResult(101, getTipMsg("task.save.havetask"));
			}
			// 通过页面是否勾选Immediate effect,进行删除padding的task任务
			if (obj.getImmediateEffect()) {
				deletePendingTask(oldTask.getTaskCode());
			}

			dealSaveForm(obj);

			// 如果有老的镜像
			if (imageFlag) {
				String oldTaskId = obj.getId();
				obj.setId(null);
				reuslt = saveTask(obj, currentUser, oldTask.getTaskCode());
				taskCoreService.dealInitTaskInstance(day, obj, currentUser);
				// 改下来的状态
				TaskInfo old = taskInfoMapper.selectByPrimaryKey(oldTaskId);
				old.setCurrentFlag(false);
				taskInfoMapper.updateByPrimaryKey(old);
			} else {
				reuslt = updateTask(obj, currentUser);
				if (obj.getTaskCategory() == TaskCategory.Individual.getValue()) {
					// 删除follow up instance
					taskFollowInstanceInfoMapper.deleteDoingByModelId(obj.getId());
					// 删除instance
					taskInstanceInfoMapper.deleteDoingByTaskModelId(obj.getId());
				}
				taskCoreService.dealInitTaskInstance(day, obj, currentUser);
			}
		} else {
			int haveOld = taskInstanceInfoMapper.getTaskInstanceSizeOfStatu(obj.getId(), null, false);
			// 如果shift 已经被用了 那么如果在改为非shift 则不给
			if (haveOld > 0 && BooleanUtils.isFalse(obj.getShiftFlag())) {
				return failResult(getTipMsg("task.save.roster.use.shift.change"));
			}

			// 如果换园了
			List<String> centreIds = rosterShiftInfoMapper.getUseCentreIds(obj.getId(), true);
			boolean alsoContainCentre = taskCentreFactory.alsoContainCentre(obj.getSelectTaskVisibles(), centreIds);
			if (!alsoContainCentre) {
				return failResult(getTipMsg("task.change.select.task.shift"));
			}

			if (haveOld > 0 && !obj.getOverWrite()) {
				return failResult(101, getTipMsg("task.save.roster.use.shift.task"));
			}

			List<String> oldCentreIds = rosterShiftInfoMapper.getUseCentreIds(obj.getId(), false);
			boolean needImageOfOldShift = taskCentreFactory.alsoContainCentre(obj.getSelectTaskVisibles(), oldCentreIds);
			// TODO big 这里如果有老的shift 那么可能就没有必要镜像处理了 是否需要镜像回归于haveOld的判断

			dealSaveForm(obj);

			boolean image = haveOld > 0;
			// 如果是shfit任务 那么 需要生成新的排班task
			if (image) {
				String oldTaskId = obj.getId();
				// dealCopyTask(obj.getId());
				obj.setId(null);
				reuslt = saveTask(obj, currentUser, oldTask.getTaskCode());
				taskCoreService.dealInitTaskInstance(day, obj, currentUser);
				// 改下来的状态
				TaskInfo old = taskInfoMapper.selectByPrimaryKey(oldTaskId);
				old.setCurrentFlag(false);
				taskInfoMapper.updateByPrimaryKey(old);
				// 镜像shift 保存新的shift 切 新的shift的taskId 变更为新的 更新未来roster
				// shfitId为最新的
				shiftFactory.imageWithTask(oldTaskId, obj.getId(), currentUser);
			} else {
				reuslt = updateTask(obj, currentUser);
			}
		}
		reuslt.setMsg(getTipMsg("staff_save"));
		return reuslt;
	}

	private void deletePendingTask(String code) {
		List<String> taskIds = taskInfoMapper.getTaskIdByCode(code);
		for (String id : taskIds) {
			// 删除follow up instance
			taskFollowInstanceInfoMapper.deleteDoingByModelId(id);
			// 删除instance
			taskInstanceInfoMapper.deleteDoingByTaskModelId(id);
		}
	};

	private String dealCopyTask(String taskId) throws Exception {
		String newId = UUID.randomUUID().toString();
		TaskInfo taskInfo = taskInfoMapper.selectByPrimaryKey(taskId);
		TaskInfo newTaskInfo = new TaskInfoVo();
		PropertyUtils.copyProperties(newTaskInfo, taskInfo);
		newTaskInfo.setId(newId);
		newTaskInfo.setCurrentFlag(false);
		// 选择项
		String selectObjIdNew = UUID.randomUUID().toString();
		List<TaskVisibleInfo> taskVisibleInfos = taskVisibleInfoMapper.selectBySourceId(newTaskInfo.getSelectObjId());
		for (TaskVisibleInfo taskVisibleInfo : taskVisibleInfos) {
			TaskVisibleInfo taskVisibleInfoNew = new TaskVisibleInfo();
			PropertyUtils.copyProperties(taskVisibleInfoNew, taskVisibleInfo);
			taskVisibleInfoNew.setSourceId(selectObjIdNew);
			taskVisibleInfoMapper.insert(taskVisibleInfoNew);
		}
		newTaskInfo.setSelectObjId(selectObjIdNew);

		// 责任人
		String responsiblePeopleIdNew = UUID.randomUUID().toString();
		List<TaskVisibleInfo> responsiblePeopleList = taskVisibleInfoMapper.selectBySourceId(newTaskInfo.getResponsiblePeopleId());
		for (TaskVisibleInfo taskVisibleInfo : responsiblePeopleList) {
			TaskVisibleInfo taskVisibleInfoNew = new TaskVisibleInfo();
			PropertyUtils.copyProperties(taskVisibleInfoNew, taskVisibleInfo);
			taskVisibleInfoNew.setSourceId(responsiblePeopleIdNew);
			taskVisibleInfoMapper.insert(taskVisibleInfoNew);
		}
		newTaskInfo.setResponsiblePeopleId(responsiblePeopleIdNew);

		// 填写人
		String taskImplementerIdNew = UUID.randomUUID().toString();
		List<TaskVisibleInfo> taskImplementerList = taskVisibleInfoMapper.selectBySourceId(newTaskInfo.getTaskImplementersId());
		for (TaskVisibleInfo taskVisibleInfo : taskImplementerList) {
			TaskVisibleInfo taskVisibleInfoNew = new TaskVisibleInfo();
			PropertyUtils.copyProperties(taskVisibleInfoNew, taskVisibleInfo);
			taskVisibleInfoNew.setSourceId(taskImplementerIdNew);
			taskVisibleInfoMapper.insert(taskVisibleInfoNew);
		}
		newTaskInfo.setTaskImplementersId(taskImplementerIdNew);

		TaskFrequencyInfo frequencyInfo = taskFrequencyInfoMapper.selectByTaskId(taskId);
		if (frequencyInfo != null) {
			TaskFrequencyInfo frequencyInfoNew = new TaskFrequencyInfo();
			PropertyUtils.copyProperties(frequencyInfoNew, frequencyInfo);
			frequencyInfoNew.setId(UUID.randomUUID().toString());
			frequencyInfoNew.setTaskId(newId);
			taskFrequencyInfoMapper.insert(frequencyInfoNew);
		}

		FollowUpCondtion condition = new FollowUpCondtion();
		condition.initAll();
		condition.setTaskId(taskId);
		List<TaskFollowUpInfo> followUpInfos = taskFollowUpInfoMapper.getList(condition);
		if (ListUtil.isNotEmpty(followUpInfos)) {
			for (TaskFollowUpInfo taskFollowUpInfo : followUpInfos) {
				TaskFollowUpInfo taskFollowUpInfoNew = new TaskFollowUpInfo();
				PropertyUtils.copyProperties(taskFollowUpInfoNew, taskFollowUpInfo);
				taskFollowUpInfoNew.setTaskId(newId);
				taskFollowUpInfoNew.setId(UUID.randomUUID().toString());

				// 责任人
				String responsiblePeopleIdNewOfFollowUp = UUID.randomUUID().toString();
				List<TaskVisibleInfo> responsiblePeopleListOfFollowUp = taskVisibleInfoMapper.selectBySourceId(taskFollowUpInfo.getCompleteStepId());
				for (TaskVisibleInfo taskVisibleInfo : responsiblePeopleListOfFollowUp) {
					TaskVisibleInfo taskVisibleInfoNew = new TaskVisibleInfo();
					PropertyUtils.copyProperties(taskVisibleInfoNew, taskVisibleInfo);
					taskVisibleInfoNew.setSourceId(responsiblePeopleIdNewOfFollowUp);
					taskVisibleInfoMapper.insert(taskVisibleInfoNew);
				}
				taskFollowUpInfoNew.setCompleteStepId(responsiblePeopleIdNewOfFollowUp);

				// 填写人
				String taskImplementerIdNewOfFollowUp = UUID.randomUUID().toString();
				List<TaskVisibleInfo> taskImplementerListOfFollowUp = taskVisibleInfoMapper.selectBySourceId(taskFollowUpInfo.getResponsiblePersonId());
				for (TaskVisibleInfo taskVisibleInfo : taskImplementerListOfFollowUp) {
					TaskVisibleInfo taskVisibleInfoNew = new TaskVisibleInfo();
					PropertyUtils.copyProperties(taskVisibleInfoNew, taskVisibleInfo);
					taskVisibleInfoNew.setSourceId(taskImplementerIdNewOfFollowUp);
					taskVisibleInfoMapper.insert(taskVisibleInfoNew);
				}
				taskFollowUpInfoNew.setResponsiblePersonId(taskImplementerIdNewOfFollowUp);

				taskFollowUpInfoMapper.insert(taskFollowUpInfoNew);

				// 将值都更新为老的
				taskFollowInstanceInfoMapper.updateFollowUpId(taskFollowUpInfo.getId(), taskFollowUpInfoNew.getId());
			}
		}
		taskInfoMapper.insert(newTaskInfo);

		// 删除follow up instance
		taskInstanceInfoMapper.updateModelId(taskId, newId);

		return newId;
	}

	@Override
	public ServiceResult<List> getResponsibleList(List<PersonSelectVo> selectList, int category, UserInfoVo currentUser, String taskId, String findText) {
		List<TaskVisibleInfo> taskVisibleInfos = new ArrayList<TaskVisibleInfo>();
		if (StringUtil.isNotBlank(taskId)) {
			taskVisibleInfos = taskVisibleInfoMapper.selectBySourceId(taskId);
		}
		List result = new ArrayList();
		TaskCategory taskCategory = TaskCategory.getType((short) category);
		switch (taskCategory) {
		case CENTER: {
			List<UserVo> userInfos = new ArrayList<UserVo>();
			boolean selectAllCenter = taskCentreFactory.haveOnlySelectOneCenter(selectList);
			if (selectAllCenter) {
				UserDetailCondition userCondtion = new UserDetailCondition();
				userCondtion.initAll();
				userCondtion.setUserType(UserType.Staff.getValue());
				if (ListUtil.isEmpty(taskVisibleInfos)) {
					userCondtion.setStatus(ArchivedStatus.UnArchived.getValue());
				}
				List<String> centresList = taskCentreFactory.getSelectCentreIds(selectList);
				userCondtion.setCentreIds(centresList);
				userInfos = userInfoMapper.getListOfUserVo(userCondtion);
			}
			result = taskCentreFactory.dealCenterOfResponsible(userInfos, taskVisibleInfos, findText);
		}
			break;
		case ROOM: {
			List<UserVo> userInfos = new ArrayList<UserVo>();
			boolean selectAllCenter = taskRoomFactory.haveOnlySelectOneRoom(selectList);
			if (selectAllCenter) {
				UserDetailCondition userCondtion = new UserDetailCondition();
				userCondtion.initAll();
				userCondtion.setUserType(UserType.Staff.getValue());
				if (ListUtil.isEmpty(taskVisibleInfos)) {
					userCondtion.setStatus(ArchivedStatus.UnArchived.getValue());
				}
				List<String> roomIds = taskRoomFactory.getSelectRoomIds(selectList);
				userCondtion.setRoomIds(roomIds);
				userInfos = userInfoMapper.getListOfUserVo(userCondtion);
			}
			result = taskRoomFactory.dealRoomOfResponsible(userInfos, taskVisibleInfos, findText);
		}
			break;
		case CHILDRRENN: {
			List<UserVo> userInfos = new ArrayList<UserVo>();
			boolean selectAllCenter = taskChildFactory.haveOnlySelectChildrenInSameRoom(selectList);
			if (selectAllCenter) {
				UserDetailCondition userCondtion = new UserDetailCondition();
				userCondtion.initAll();
				userCondtion.setUserType(UserType.Staff.getValue());
				if (ListUtil.isEmpty(taskVisibleInfos)) {
					userCondtion.setStatus(ArchivedStatus.UnArchived.getValue());
				}
				List<String> roomIds = taskChildFactory.getSelectRoomIds(selectList);
				userCondtion.setRoomIds(roomIds);
				userInfos = userInfoMapper.getListOfUserVo(userCondtion);
			}
			result = taskChildFactory.dealChildrenOfResponsible(userInfos, taskVisibleInfos, findText);
		}
			break;
		case STAFF: {
			List<UserVo> userInfos = new ArrayList<UserVo>();
			boolean selectAllCenter = taskStaffFactory.haveOnlySelectOneCenter(selectList);
			if (selectAllCenter) {
				UserDetailCondition userCondtion = new UserDetailCondition();
				userCondtion.initAll();
				userCondtion.setUserType(UserType.Staff.getValue());
				if (ListUtil.isEmpty(taskVisibleInfos)) {
					userCondtion.setStatus(ArchivedStatus.UnArchived.getValue());
				}
				List<String> centresList = taskStaffFactory.getSelectCentreIds(selectList);
				userCondtion.setCentreIds(centresList);
				userInfos = userInfoMapper.getListOfUserVo(userCondtion);
			}
			result = taskStaffFactory.deaStaffOfResponsible(userInfos, taskVisibleInfos, findText);
		}
			break;
		default:
			break;
		}

		if (ListUtil.isNotEmpty(taskVisibleInfos)) {
			for (TaskVisibleInfo taskVisibleInfo : taskVisibleInfos) {
				boolean contain = false;
				for (Object object : result) {
					PersonSelectVo resultItem = (PersonSelectVo) object;
					if (resultItem.getId().split(",")[0].equals(taskVisibleInfo.getObjId())) {
						contain = true;
					}
				}
				if (!contain) {
					if (taskVisibleInfo.getType() == TaskVisibleType.USER.getValue()) {
						UserInfo userInfo = userInfoMapper.getUserInfoByAccountId(taskVisibleInfo.getObjId());
						PersonSelectVo select = new PersonSelectVo(TaskVisibleType.USER.getValue(), taskVisibleInfo.getObjId(), userInfo.getFullName());
						result.add(select);
					} else if (taskVisibleInfo.getType() == TaskVisibleType.ROOM.getValue()) {

					} else if (taskVisibleInfo.getType() == TaskVisibleType.CENTER.getValue()) {

					}
				}
			}
		}
		return successResult(result);
	}

	@Override
	public ServiceResult<List> getSelectList(int category, UserInfoVo currentUser, String taskId, String findText) {
		List<TaskVisibleInfo> taskVisibleInfos = new ArrayList<TaskVisibleInfo>();
		if (StringUtil.isNotEmpty(taskId)) {
			taskVisibleInfos = taskVisibleInfoMapper.selectBySourceId(taskId);
		}
		TaskCategory taskCategory = TaskCategory.getType((short) category);

		List result = new ArrayList();
		switch (taskCategory) {
		case CENTER: {
			CenterCondition condition = new CenterCondition();
			condition.initAll();
			if (ListUtil.isEmpty(taskVisibleInfos)) {
				condition.setStatus(ArchivedStatus.UnArchived.getValue());
			}
			List<CentersInfo> list = centersInfoMapper.getCenterList(condition);
			result = taskCentreFactory.dealCenterOfSelect(list, taskVisibleInfos, findText);
		}
			break;
		case ROOM: {
			CenterCondition condition = new CenterCondition();
			condition.initAll();
			if (ListUtil.isEmpty(taskVisibleInfos)) {
				condition.setStatus(ArchivedStatus.UnArchived.getValue());
			}
			List<CentersInfo> list = centersInfoMapper.getCenterList(condition);

			RoomCondition roomCondition = new RoomCondition();
			roomCondition.initAll();
			if (ListUtil.isEmpty(taskVisibleInfos)) {
				roomCondition.setStatus(ArchivedStatus.UnArchived.getValue());
			}
			List<RoomInfo> roomList = roomInfoMapper.getList(roomCondition);
			result = taskRoomFactory.dealRoomOfSelect(list, roomList, taskVisibleInfos, findText);
		}
			break;
		case CHILDRRENN: {
			CenterCondition condition = new CenterCondition();
			condition.initAll();
			if (ListUtil.isEmpty(taskVisibleInfos)) {
				condition.setStatus(ArchivedStatus.UnArchived.getValue());
			}
			List<CentersInfo> list = centersInfoMapper.getCenterList(condition);

			RoomCondition roomCondition = new RoomCondition();
			roomCondition.initAll();
			if (ListUtil.isEmpty(taskVisibleInfos)) {
				roomCondition.setStatus(ArchivedStatus.UnArchived.getValue());
			}
			List<RoomInfo> roomList = roomInfoMapper.getList(roomCondition);

			UserDetailCondition userCondtion = new UserDetailCondition();
			userCondtion.initAll();
			userCondtion.setUserType(UserType.Child.getValue());
			if (ListUtil.isEmpty(taskVisibleInfos)) {
				userCondtion.setStatus(ArchivedStatus.UnArchived.getValue());
			}
			userCondtion.setEnrolled(EnrolledState.Enrolled.getValue());
			List<UserVo> userInfos = userInfoMapper.getListOfUserVo(userCondtion);
			result = taskChildFactory.dealChildOfSelect(list, roomList, userInfos, taskVisibleInfos, findText);
		}
			break;
		case STAFF: {
			CenterCondition condition = new CenterCondition();
			condition.initAll();
			if (ListUtil.isEmpty(taskVisibleInfos)) {
				condition.setStatus(ArchivedStatus.UnArchived.getValue());
			}
			List<CentersInfo> list = centersInfoMapper.getCenterList(condition);
			result = taskStaffFactory.dealStaffOfSelect(list, taskVisibleInfos, findText);
		}
			break;
		case Individual: {
			CenterCondition condition = new CenterCondition();
			condition.initAll();
			if (ListUtil.isEmpty(taskVisibleInfos)) {
				condition.setStatus(ArchivedStatus.UnArchived.getValue());
			}
			List<CentersInfo> list = centersInfoMapper.getCenterList(condition);
			result = taskStaffFactory.dealStaffOfSelect(list, taskVisibleInfos, findText);
		}
			break;
		default:
			break;
		}
		return successResult(result);
	}

	@Override
	public boolean validateTaskName(String taskName, String taskId) {
		int count = taskInfoMapper.validateTaskName(taskId, taskName);
		return count > 0 ? false : true;
	}

	@Override
	public void createDepositTask(UserInfo u, Date now) {
		// 判断是否已存在换押金task
		int count = taskInstance.getTaskInstance(TaskType.DepositTask, u.getAccountId(), now);
		if (count != 0) {
			return;
		}
		taskInstance.addTaskInstance(TaskType.DepositTask, u.getCentersId(), u.getRoomId(), u.getGroupId(), u.getAccountId(), SystemConstants.JobCreateAccountId, now);
	}

	@Override
	public void exportCsv(HttpServletResponse response, List<Map<String, String>> list) {
		try {
			// 创建CSV写对象
			File tempFile = File.createTempFile("TaskTable", ".csv");
			CsvWriter csvWriter = new CsvWriter(tempFile.getCanonicalPath(), ',', Charset.forName("UTF-8"));

			csvWriter.writeRecord(ExportConfigure.TaskTable);
			for (Map<String, String> map : list) {
				csvWriter.write(map.get("Dates"));
				csvWriter.write(map.get("Centre"));
				csvWriter.write(map.get("Room"));
				csvWriter.write(map.get("Task Name"));
				csvWriter.write(map.get("Responsible Person"));
				csvWriter.write(map.get("Task Implementers"));
				csvWriter.write(map.get("Subject Person"));
				csvWriter.write(map.get("NQS"));
				csvWriter.write(map.get("Task Status"));
				csvWriter.endRecord();
			}
			csvWriter.close();
			java.io.OutputStream out = response.getOutputStream();
			byte[] b = new byte[10240];
			java.io.File fileLoad = new java.io.File(tempFile.getCanonicalPath());
			response.reset();
			response.setContentType("application/csv");
			response.setHeader("content-disposition", "attachment; filename=TaskTable.csv");
			long fileLength = fileLoad.length();
			String length1 = String.valueOf(fileLength);
			response.setHeader("Content_Length", length1);
			java.io.FileInputStream in = new java.io.FileInputStream(fileLoad);
			int n;
			while ((n = in.read(b)) != -1) {
				out.write(b, 0, n); // 每次写入out1024字节
			}
			in.close();
			out.close();
		} catch (IOException e) {

		}
	}

	@Override
	public List<SelecterPo> getTaskNames(String p, UserInfoVo currentUser) {
		if (StringUtil.isEmpty(p)) {
			return new ArrayList<SelecterPo>();
		}
		TaskCondtion condtion = new TaskCondtion();
		condtion.setSelectName(true);
		condtion.setTaskName(p);
		// handleSerchConditionForTable(condtion, currentUser);
		return taskInstanceInfoMapper.getTaskNames(condtion);
	}

	// @Override
	// public void createHatTask(UserInfo u, UserInfoVo currentUser, Date now) {
	// String accountId = currentUser == null ?
	// SystemConstants.JobCreateAccountId :
	// currentUser.getAccountInfo().getId();
	// taskInstance.addTaskInstance(TaskType.HatTask, u.getCentersId(),
	// u.getRoomId(), u.getGroupId(), u.getAccountId(), accountId, now);
	// }

}
