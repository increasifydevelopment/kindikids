package com.aoyuntek.aoyun.service;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.OutFamilyPo;

public interface IWaitingListService {
    void importData(List<OutFamilyPo> data, String centerIds);
}
