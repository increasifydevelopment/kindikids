package com.aoyuntek.aoyun.service.impl;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.dao.FunctionInfoMapper;
import com.aoyuntek.aoyun.dao.MenuInfoMapper;
import com.aoyuntek.aoyun.entity.po.FunctionInfo;
import com.aoyuntek.aoyun.entity.po.MenuInfo;
import com.aoyuntek.aoyun.entity.vo.RoleInfoVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.service.IFunctionService;
import com.aoyuntek.aoyun.uitl.MyListUtil;
import com.theone.common.util.ServiceResult;

@Service
public class FunctionServiceImpl extends BaseWebServiceImpl<FunctionInfo, FunctionInfoMapper> implements IFunctionService {
    /**
     * 日志
     */
    public static Logger logger = Logger.getLogger(FunctionServiceImpl.class);

    /**
     * functionMapper
     */
    @Autowired
    private FunctionInfoMapper functionMapper;

    /**
     * menuInfoMapper
     */
    @Autowired
    private MenuInfoMapper menuInfoMapper;

    @Override
    public ServiceResult<List<FunctionInfo>> getFunctionList(UserInfoVo userInfoVO, String url) {
        logger.info("getFunctionList|start|url=" + url);
        // 判空
        List<RoleInfoVo> roleInfoList = userInfoVO.getRoleInfoVoList();
        if (null == roleInfoList || roleInfoList.size() == 0) {
            logger.info("getFunctionList|session err" + url);
            return failResult("session err");
        }
        // 获取function集合
        Set<FunctionInfo> tempSets = new LinkedHashSet<FunctionInfo>();
        for (RoleInfoVo r : roleInfoList) {
            tempSets.addAll(functionMapper.getFunctionList(r.getId(), url));
        }
        logger.info("getFunctionList|end|url=" + url);
        return successResult(new MyListUtil<FunctionInfo>().set2List(tempSets));
    }

    @Override
    public ServiceResult<List<MenuInfo>> getMenuList(UserInfoVo userInfo) {
        logger.info("getMenuList|start|");
        if(userInfo==null){
            return successResult();
        }
        List<MenuInfo> list = menuInfoMapper.getMenuList(userInfo.getRoleInfoVoList());
        logger.info("getMenuList|end|");
        return successResult(list);
    }
}
