package com.aoyuntek.aoyun.service.impl;

import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.aoyuntek.aoyun.condtion.CenterCondition;
import com.aoyuntek.aoyun.condtion.WeekMenuCondition;
import com.aoyuntek.aoyun.constants.S3Constants;
import com.aoyuntek.aoyun.dao.AttachmentInfoMapper;
import com.aoyuntek.aoyun.dao.AuthorityGroupInfoMapper;
import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.CentreManagerRelationInfoMapper;
import com.aoyuntek.aoyun.dao.GroupManagerRelationInfoMapper;
import com.aoyuntek.aoyun.dao.NewsContentInfoMapper;
import com.aoyuntek.aoyun.dao.NewsInfoMapper;
import com.aoyuntek.aoyun.dao.NqsRelationInfoMapper;
import com.aoyuntek.aoyun.dao.RoomGroupInfoMapper;
import com.aoyuntek.aoyun.dao.RoomInfoMapper;
import com.aoyuntek.aoyun.dao.RoomTaskCheckMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.dao.WeekNutrionalMenuInfoMapper;
import com.aoyuntek.aoyun.entity.po.AttachmentInfo;
import com.aoyuntek.aoyun.entity.po.CentersInfo;
import com.aoyuntek.aoyun.entity.po.GroupManagerRelationInfo;
import com.aoyuntek.aoyun.entity.po.NewsContentInfo;
import com.aoyuntek.aoyun.entity.po.NewsInfo;
import com.aoyuntek.aoyun.entity.po.NqsRelationInfo;
import com.aoyuntek.aoyun.entity.po.RoomGroupInfo;
import com.aoyuntek.aoyun.entity.po.RoomInfo;
import com.aoyuntek.aoyun.entity.po.RoomTaskCheck;
import com.aoyuntek.aoyun.entity.po.WeekNutrionalMenuInfo;
import com.aoyuntek.aoyun.entity.vo.CenterInfoVo;
import com.aoyuntek.aoyun.entity.vo.FileVo;
import com.aoyuntek.aoyun.entity.vo.GroupInfoVo;
import com.aoyuntek.aoyun.entity.vo.RoleInfoVo;
import com.aoyuntek.aoyun.entity.vo.RoomInfoVo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.aoyun.entity.vo.UserAvatarVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.WeekNutrionalMenuVo;
import com.aoyuntek.aoyun.enums.AccountActiveStatus;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.NewsStatus;
import com.aoyuntek.aoyun.enums.NewsType;
import com.aoyuntek.aoyun.enums.NqsTag;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.enums.Week;
import com.aoyuntek.aoyun.service.IAuthGroupService;
import com.aoyuntek.aoyun.service.ICenterService;
import com.aoyuntek.aoyun.service.INewsInfoService;
import com.aoyuntek.aoyun.uitl.FilePathUtil;
import com.aoyuntek.framework.condtion.BaseCondition;
import com.aoyuntek.framework.constants.PropertieNameConts;
import com.aoyuntek.framework.model.Pager;
import com.theone.aws.util.FileFactory;
import com.theone.common.util.ServiceResult;
import com.theone.date.util.DateUtil;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

/**
 * 
 * @description Centers业务逻辑层实现类
 * @author mingwang
 * @create 2016年8月18日下午2:24:27
 * @version 1.0
 */
@Service
public class CenterServiceImpl extends BaseWebServiceImpl<CentersInfo, CentersInfoMapper> implements ICenterService {

	/**
	 * log
	 */
	private static Logger log = Logger.getLogger(CenterServiceImpl.class);

	/**
	 * centersInfoMapper
	 */
	@Autowired
	private CentersInfoMapper centersInfoMapper;

	/**
	 * roomInfoMapper
	 */
	@Autowired
	private RoomInfoMapper roomInfoMapper;

	/**
	 * weekMapper
	 */
	@Autowired
	private WeekNutrionalMenuInfoMapper weekMapper;

	/**
	 * groupMapper
	 */
	@Autowired
	private RoomGroupInfoMapper groupMapper;

	/**
	 * userInfoMapper
	 */
	@Autowired
	private UserInfoMapper userInfoMapper;

	/**
	 * attachmentInfoMapper
	 */
	@Autowired
	private AttachmentInfoMapper attachmentInfoMapper;

	/**
	 * newsInfoService
	 */
	@Autowired
	private INewsInfoService newsInfoService;

	/**
	 * newsInfoMapper
	 */
	@Autowired
	private NewsInfoMapper newsInfoMapper;

	/**
	 * contentInfoMapper
	 */
	@Autowired
	private NewsContentInfoMapper contentInfoMapper;

	/**
	 * nqsNewsInfoMapper
	 */
	@Autowired
	private NqsRelationInfoMapper nqsRelationInfoMapper;
	@Autowired
	private IAuthGroupService authGroupService;
	@Autowired
	private AuthorityGroupInfoMapper authorityGroupInfoMapper;
	@Autowired
	private RoomTaskCheckMapper roomTaskCheckMapper;
	@Autowired
	private CentreManagerRelationInfoMapper centreManagerRelationInfoMapper;
	@Autowired
	private GroupManagerRelationInfoMapper groupManagerRelationInfoMapper;

	/**
	 * 新增/更新园区信息
	 */
	@Override
	public ServiceResult<Object> addOrUpdateCenter(CenterInfoVo centersInfoVo, UserInfoVo currentUser) {
		log.info("addOrUpdateCenter|start");
		CentersInfo centersInfo = centersInfoVo.getCenter();
		FileVo image = centersInfoVo.getImage();
		String centerName = centersInfo.getName();
		String centerId = centersInfo.getId();
		String sourceId = "";
		String oldCenterName = "";
		// 验证园长和二级园长没有园区或者不是自己的园区时，提示没有权限
		ServiceResult<Object> validatePermission = validatePermission(centersInfoVo.getCenter().getId(), currentUser);
		if (!validatePermission.isSuccess()) {
			return validatePermission;
		}
		// 判断园区名称是否为空
		if (StringUtil.isEmpty(centerName)) {
			return failResult(getTipMsg("add_center"));
		}
		// 校验名称是否重复
		int res = centersInfoMapper.validateName(centerName, centerId);
		log.info("addOrUpdateCenter|res=" + res);
		log.info("addOrUpdateCenter|centerName=" + centerName + "|centerId" + centerId);
		if (res >= 1) {
			return failResult(getTipMsg("add_center"));
		}
		// 新增园区信息
		if (StringUtil.isEmpty(centerId)) {
			int tag = centersInfoMapper.validateName(centerName, centerId);
			log.info("addOrUpdateCenter|validateName-center|tag=" + tag);
			if (tag >= 1) {
				return failResult(getTipMsg("add_center"));
			}
			centerId = UUID.randomUUID().toString();
			// 保存园区信息
			saveCenterInfo(centerId, sourceId, centersInfo, currentUser);
			// 保存菜单信息
			saveWeekNutrionalMenuInfo(centerId, currentUser);
			// 新增分组关系 add by gfwang
			authGroupService.addOrUpdateGroup(centerId, centerName, oldCenterName, true);
		} else {
			// 如果center已归档，不允许更新园区信息
			int index = centersInfoMapper.isCentersAchivedById(centerId);
			if (index >= 1) {
				return failResult(getTipMsg("staffCenter_archive"));
			}
			// 更新园区信息
			sourceId = centersInfo.getCentreImageId();
			oldCenterName = updateCenterInfo(centerId, sourceId, centersInfo, currentUser);
			// 新增分组关系 add by gfwang
			authGroupService.addOrUpdateGroup(centerId, centerName, oldCenterName, false);
		}
		// 设置图片
		initImage(image, sourceId);
		log.info("addOrUpdateCenter|end");
		return successResult(getTipMsg("staff_save"), (Object) centersInfo);
	}

	/**
	 * 
	 * @description 保存园区信息
	 * @author mingwang
	 * @create 2016年9月1日上午11:21:32
	 * @version 1.0
	 * @param centerId
	 *            园区Id
	 * @param sourceId
	 *            centerImageId
	 * @param centersInfo
	 *            园区信息
	 * @param currentUser
	 *            当前登录用户
	 */
	private void saveCenterInfo(String centerId, String sourceId, CentersInfo centersInfo, UserInfoVo currentUser) {
		// 初始化center信息
		centersInfo.setId(centerId);
		centersInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
		Date date = new Date();
		centersInfo.setCreateTime(date);
		centersInfo.setUpdateTaccountId(currentUser.getAccountInfo().getId());
		centersInfo.setStatus(AccountActiveStatus.Enabled.getValue());
		centersInfo.setUpdateTime(date);
		centersInfo.setDeleteFlag(DeleteFlag.Default.getValue());
		sourceId = UUID.randomUUID().toString();
		centersInfo.setCentreImageId(sourceId);
		// 插入center
		centersInfoMapper.insertSelective(centersInfo);
	}

	/**
	 * 
	 * @description 保存菜单信息
	 * @author mingwang
	 * @create 2016年9月1日上午11:29:50
	 * @version 1.0
	 * @param centerId
	 *            园区Id
	 * @param currentUser
	 *            当前登录用户
	 */
	@Override
	public void saveWeekNutrionalMenuInfo(String centerId, UserInfoVo currentUser) {
		// 四周一个循环，一周五个工作日
		int MENU_CYCLE = 4;
		int WORK_DAYS = 5;
		// 初始化weekNutrionalMenu信息
		ArrayList<WeekNutrionalMenuInfo> list = new ArrayList<WeekNutrionalMenuInfo>();
		for (short i = 1; i <= MENU_CYCLE; i++) {
			for (short j = 1; j <= WORK_DAYS; j++) {
				WeekNutrionalMenuInfo menuInfo = new WeekNutrionalMenuInfo();
				menuInfo.setId(UUID.randomUUID().toString());
				menuInfo.setCenterId(centerId);
				menuInfo.setWeekNum(i);
				menuInfo.setWeekToday(j);
				menuInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
				Date date = new Date();
				menuInfo.setCreateTime(date);
				menuInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
				menuInfo.setUpdateTime(date);
				menuInfo.setDeleteFlag(DeleteFlag.Default.getValue());
				list.add(menuInfo);
			}
		}
		// 插入weekNutrionalMenu
		weekMapper.initMenuInfo(list);
	}

	/**
	 * 
	 * @description 更新园区信息
	 * @author mingwang
	 * @create 2016年9月1日上午11:35:07
	 * @version 1.0
	 * @param centerId
	 *            园区Id
	 * @param sourceId
	 *            centerImageId
	 * @param centersInfo
	 *            园区信息
	 * @param currentUser
	 *            当前登录用户
	 */
	private String updateCenterInfo(String centerId, String sourceId, CentersInfo centersInfo, UserInfoVo currentUser) {
		log.info("updateCenterInfo|start");
		CentersInfo dbCenter = centersInfoMapper.selectByPrimaryKey(centerId);
		sourceId = dbCenter.getCentreImageId();
		log.info("updateCenterInfo|sourceId=" + sourceId);
		if (StringUtil.isEmpty(sourceId)) {
			sourceId = UUID.randomUUID().toString();
			dbCenter.setCentreImageId(sourceId);
		}
		String oldCenterName = dbCenter.getName();
		dbCenter.setName(centersInfo.getName());
		dbCenter.setCentreAddress(centersInfo.getCentreAddress());
		dbCenter.setCentreContact(centersInfo.getCentreContact());
		dbCenter.setEmailAddress(centersInfo.getEmailAddress());
		dbCenter.setUpdateTime(new Date());
		dbCenter.setUpdateTaccountId(currentUser.getAccountInfo().getId());
		// 更新
		centersInfoMapper.updateByPrimaryKey(dbCenter);
		log.info("updateCenterInfo|end");
		return oldCenterName;
	}

	/**
	 * 
	 * @description 设置图片
	 * @author gfwang
	 * @create 2016年8月25日上午9:19:54
	 * @version 1.0
	 * @param file
	 * @param sourceId
	 */
	private void initImage(FileVo file, String sourceId) {
		try {
			// 文件为空
			if (file == null) {
				attachmentInfoMapper.updateDeleteBySourceId(sourceId);
				return;
			}
			// 相对路径不为空并且相对路径不包括临时目录
			if (StringUtil.isNotEmpty(file.getRelativePathName()) && !file.getRelativePathName().contains(FilePathUtil.TEMP_PATH)) {
				return;
			}
			int count = attachmentInfoMapper.updateDeleteBySourceId(sourceId);
			log.info("initImage|count=" + count + "|sourceId=" + sourceId);
			// 相对路径名为空
			if (StringUtil.isEmpty(file.getRelativePathName())) {
				return;
			}
			FileFactory fac = new FileFactory(Region.getRegion(Regions.AP_SOUTHEAST_2), PropertieNameConts.S3);
			AttachmentInfo attachmentInfo = setAttInfo(file, fac, sourceId);
			attachmentInfoMapper.insert(attachmentInfo);
		} catch (Exception e) {
			log.error("addOrUpdateCenter|initImage|s3 error", e);
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @description 初始化对象
	 * @author gfwang
	 * @create 2016年8月24日下午1:35:30
	 * @version 1.0
	 * @param file
	 * @param attachmentInfo
	 * @param fac
	 * @return
	 * @throws Exception
	 */
	private AttachmentInfo setAttInfo(FileVo file, FileFactory fac, String sourceId) throws Exception {
		AttachmentInfo attachmentInfo = new AttachmentInfo();
		attachmentInfo.setId(UUID.randomUUID().toString());
		attachmentInfo.setSourceId(sourceId);
		attachmentInfo.setAttachId(file.getRelativePathName());
		String tempDir = file.getRelativePathName();
		String mainDir = FilePathUtil.getMainPath(FilePathUtil.CENTER_MANAGER, tempDir);
		String rePath = fac.copyFile(tempDir, mainDir);
		attachmentInfo.setAttachName(file.getFileName());
		attachmentInfo.setAttachId(rePath);
		String visitUrl = fac.getS3RouteUrl(rePath, S3Constants.URL_TIMEOUT_HOUR);
		attachmentInfo.setVisitUrl(visitUrl);
		attachmentInfo.setDeleteFlag((short) 0);
		return attachmentInfo;
	}

	/**
	 * 获取园区列表
	 */
	@Override
	public ServiceResult<Pager<CenterInfoVo, BaseCondition>> getCenterList(CenterCondition condition, UserInfoVo currentUser) {
		log.info("getCenterList|start");
		// 获取当前用户的角色信息
		List<RoleInfoVo> roleList = currentUser.getRoleInfoVoList();
		String userCenterId = "";
		// ceo、园长、二级园长才有权限获取园区列表
		if (containRole(Role.CEO, roleList) == 1 || containRole(Role.CentreManager, roleList) == 1 || containRole(Role.EducatorSecondInCharge, roleList) == 1) {
			userCenterId = currentUser.getUserInfo().getCentersId();
			condition.setCenterId(userCenterId);
		} else {
			log.info("getCenterList|failResult|no.permissions|end");
			return failResult(getTipMsg("no.permissions"));
		}
		condition.setStartSize(condition.getPageIndex());
		// 园区内所有center集合
		List<CentersInfo> centerInfoList = centersInfoMapper.getCenterList(condition);
		// 园区内所有center(包含room信息)集合
		List<CenterInfoVo> centerVoList = new ArrayList<CenterInfoVo>();
		for (CentersInfo centerInfo : centerInfoList) {
			String centerId = centerInfo.getId();
			// 获取center下的所有room信息
			List<RoomInfoVo> selecterRooms = roomInfoMapper.selectRoomsByCenterId(centerId);
			centerVoList.add(new CenterInfoVo(centerInfo, selecterRooms));
		}
		// 总的center数量
		int totalSize = centersInfoMapper.getCentersCount(condition);
		condition.setTotalSize(totalSize);
		Pager<CenterInfoVo, BaseCondition> pager = new Pager<CenterInfoVo, BaseCondition>(centerVoList, condition);
		log.info("getCenterList|successResult|end");
		return successResult(getTipMsg("news_get_succeed"), pager);
	}

	/**
	 * 新增/更新room信息
	 */
	@Override
	public ServiceResult<Object> addOrUpdateRoom(RoomInfo roomInfo, UserInfoVo currentUser) {
		String roomName = roomInfo.getName();
		String roomId = roomInfo.getId();
		log.info("addOrUpdateRoom|start|roomName=" + roomName + "|roomId=" + roomId);
		// 验证园长和二级园长没有园区或者不是自己的园区时，提示没有权限
		ServiceResult<Object> validatePermission = validatePermission(roomInfo.getCentersId(), currentUser);
		if (!validatePermission.isSuccess()) {
			return validatePermission;
		}
		// 判断room名称是否为空
		if (StringUtil.isEmpty(roomInfo.getName())) {
			return failResult(getTipMsg("add_room"));
		}
		String centerId = roomInfo.getCentersId();
		// 校验room名称是否重复
		int res = roomInfoMapper.validateName(centerId, roomId, roomName);
		if (res >= 1) {
			return failResult(getTipMsg("add_room"));
		}
		// 判断是否新增
		if (StringUtil.isEmpty(roomId)) {
			// 如果center已归档，不允许新增room
			int index = centersInfoMapper.isCentersAchivedById(centerId);
			if (index >= 1) {
				return failResult(getTipMsg("staffCenter_archive"));
			}
			int tag = roomInfoMapper.validateName(centerId, roomId, roomName);
			if (tag >= 1) {
				return failResult(getTipMsg("add_room"));
			}
			roomId = UUID.randomUUID().toString();
			Date date = new Date();
			roomInfo.setId(roomId);
			roomInfo.setCreateTime(date);
			roomInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
			roomInfo.setUpdateTime(date);
			roomInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
			roomInfo.setStatus(AccountActiveStatus.Enabled.getValue());
			roomInfo.setDeleteFlag(DeleteFlag.Default.getValue());
			// 插入room信息
			roomInfoMapper.insertSelective(roomInfo);
			log.info("addOrUpdateRoom|end");
			List<RoomTaskCheck> list = dealRoomTaskCheck(roomId);
			roomInfo.setCheckList(list);
			return successResult(getTipMsg("staff_save"), (Object) roomInfo);
		} else {
			// 如果room归档，不可以更改roomInfo
			int index = roomInfoMapper.isRoomAchivedById(roomId);
			if (index >= 1) {
				return failResult(getTipMsg("staffRoom_archive"));
			}
			RoomInfo room = roomInfoMapper.selectByPrimaryKey(roomId);
			room.setName(roomInfo.getName());
			room.setChildCapacity(roomInfo.getChildCapacity());
			room.setAgeGroup(roomInfo.getAgeGroup());
			room.setUpdateTime(new Date());
			room.setUpdateAccountId(currentUser.getAccountInfo().getId());
			// 更新room信息
			roomInfoMapper.updateByPrimaryKeySelective(room);
			log.info("addOrUpdateRoom|end");
			return successResult(getTipMsg("staff_save"), (Object) room);
		}
	}

	/**
	 * @description 新增room将该room可自动创建的task全部勾选
	 * @author hxzhang
	 * @create 2017年6月1日下午2:07:52
	 */
	private List<RoomTaskCheck> dealRoomTaskCheck(String roomId) {
		List<RoomTaskCheck> list = new ArrayList<RoomTaskCheck>();
		String str = getSystemMsg("room_task_check");
		String[] strs = str.split(";");
		for (int i = 0; i < strs.length; i++) {
			RoomTaskCheck roomTaskCheck = new RoomTaskCheck();
			short type = Short.parseShort(strs[i]);
			roomTaskCheck.setId(UUID.randomUUID().toString());
			roomTaskCheck.setRoomId(roomId);
			roomTaskCheck.setTaskType(type);
			roomTaskCheck.setCheckFlag(true);
			Date now = new Date();
			now.setTime(now.getTime() + i * 1000);
			roomTaskCheck.setCreateTime(now);
			roomTaskCheck.setDeleteFlag(DeleteFlag.Default.getValue());

			list.add(roomTaskCheck);
		}
		if (ListUtil.isNotEmpty(list)) {
			roomTaskCheckMapper.insertBatch(list);
		}
		return list;
	}

	/**
	 * 新增/更新group信息
	 */
	@Override
	public ServiceResult<Object> addOrUpdateGroup(RoomGroupInfo groupInfo, UserInfoVo currentUser) {
		log.info("addOrUpdateGroup|start");
		String currtenAccoutId = currentUser.getAccountInfo().getId();
		log.info("addOrUpdateGroup|currentAccoutId=" + currtenAccoutId);
		// 验证园长和二级园长没有园区或者不是自己的园区时，提示没有权限
		ServiceResult<Object> validatePermission = validatePermission(groupInfo.getCenterId(), currentUser);
		if (!validatePermission.isSuccess()) {
			return validatePermission;
		}
		// 判断group名称是否为空
		if (StringUtil.isEmpty(groupInfo.getName())) {
			return failResult(getTipMsg("add_group"));
		}
		String centerId = groupInfo.getCenterId();
		String roomId = groupInfo.getRoomId();
		// 如果room或者center归档，不可以新增group
		int index1 = roomInfoMapper.isRoomAchivedById(roomId);
		int index2 = centersInfoMapper.isCentersAchivedById(centerId);
		if (index1 >= 1 || index2 >= 1) {
			return failResult(getTipMsg("staffCenter_archive"));
		}
		// 校验group名称是否重复
		int res = roomInfoMapper.validateName(roomId, groupInfo.getId(), groupInfo.getName());
		if (res >= 1) {
			return failResult(getTipMsg("add_group"));
		}
		// 判断是否新增
		if (StringUtil.isEmpty(groupInfo.getId())) {
			int tag = roomInfoMapper.validateName(roomId, groupInfo.getId(), groupInfo.getName());
			if (tag >= 1) {
				return failResult(getTipMsg("add_group"));
			}
			groupInfo.setId(UUID.randomUUID().toString());
			Date date = new Date();
			groupInfo.setCreateAccountId(currtenAccoutId);
			groupInfo.setCreateTime(date);
			groupInfo.setUpdateAccountId(currtenAccoutId);
			groupInfo.setUpdateTime(date);
			groupInfo.setStatus(AccountActiveStatus.Enabled.getValue());
			groupInfo.setDeleteFlag(DeleteFlag.Default.getValue());
			// 插入groupInfo
			groupMapper.insertSelective(groupInfo);
			log.info("addOrUpdateGroup|end");
			return successResult(getTipMsg("staff_save"), (Object) groupInfo);
		} else {
			RoomGroupInfo group = groupMapper.selectByPrimaryKey(groupInfo.getId());
			group.setUpdateAccountId(currtenAccoutId);
			group.setUpdateTime(new Date());
			group.setName(groupInfo.getName());
			// 更新groupInfo
			groupMapper.updateByPrimaryKey(group);
			log.info("addOrUpdateGroup|end");
			return successResult(getTipMsg("staff_save"), (Object) group);
		}
	}

	/**
	 * 获取园区信息、room列表信息、菜单信息
	 */
	@Override
	public ServiceResult<CenterInfoVo> getCenterInfo(String centerId) {
		log.info("getCenterInfo|start|centerId=" + centerId);
		CenterInfoVo center = centersInfoMapper.getCenterInfoVo(centerId);
		if (center == null) {
			return failResult(getTipMsg("no.permissions"));
		}
		String sourceId = center.getCenter().getCentreImageId();
		// 获取附件
		List<AttachmentInfo> files = attachmentInfoMapper.getAttachmentInfoBySourceId(sourceId);
		center.setImage(transformationFileVo(files));
		// 获取room信息
		List<RoomInfoVo> roomList = roomInfoMapper.selectRoomsByCenterId(centerId);
		// 获取菜单信息
		List<WeekNutrionalMenuInfo> weekMenuList = new ArrayList<WeekNutrionalMenuInfo>();
		weekMenuList.add(new WeekNutrionalMenuInfo());
		weekMenuList.add(new WeekNutrionalMenuInfo());
		weekMenuList.add(new WeekNutrionalMenuInfo());
		weekMenuList.add(new WeekNutrionalMenuInfo());
		center.setRoomList(roomList);
		center.setWeekMenuList(weekMenuList);
		// 获取园长集合
		List<UserAvatarVo> list = centreManagerRelationInfoMapper.getCentreManagers(centerId);
		center.setList(list);
		log.info("getCenterInfo|end");
		return successResult(center);

	}

	/**
	 * 
	 * @description 换FILEVo
	 * @author gfwang
	 * @create 2016年8月24日下午1:41:45
	 * @version 1.0
	 * @param files
	 * @return
	 */
	private FileVo transformationFileVo(List<AttachmentInfo> files) {
		if (ListUtil.isEmpty(files)) {
			return null;
		}
		AttachmentInfo attachment = files.get(0);
		FileVo file = new FileVo(attachment.getAttachId(), attachment.getAttachName(), attachment.getAttachId(), attachment.getVisitUrl());
		return file;
	}

	/**
	 * 根据centerId和weekNum获取菜单信息
	 */
	@Override
	public List<WeekNutrionalMenuVo> getWeekMenuByCenter(String centerId, Short weekNum) {
		log.info("getWeekMenuByCenter|start|centerId=" + centerId + "|weekNum=" + weekNum);
		WeekMenuCondition condition = new WeekMenuCondition();
		condition.setCenterId(centerId);
		condition.setWeekNum(weekNum);
		List<WeekNutrionalMenuVo> oneList = weekMapper.getMenuList(condition);
		log.info("getWeekMenuByCenter|end");
		return oneList;
	}

	/**
	 * 根据groupId获取group信息
	 */
	@Override
	public GroupInfoVo getGroupInfo(String groupId) {
		log.info("getGroupInfo|start|groupId=" + groupId);
		GroupInfoVo group = groupMapper.getGroupInfo(groupId);
		if (group != null) {
			group.setChildList(groupMapper.getChildsByGroup(groupId));
		}
		// 获取分组负责人集合
		List<UserAvatarVo> list = groupManagerRelationInfoMapper.getGroupManagers(groupId);
		group.setList(list);
		log.info("getGroupInfo|end");
		return group;
	}

	/**
	 * 归档/解除归档group
	 */
	@Override
	public ServiceResult<Object> updateArchiveGroup(String groupId, boolean flag, UserInfoVo currentUser) {
		log.info("updateArchiveGroup|start|group=" + groupId + "|flag=" + flag);
		String centerId = centersInfoMapper.getCenterIdByGroupId(groupId);
		// 验证园长和二级园长没有园区或者不是自己的园区时，提示没有权限
		ServiceResult<Object> validatePermission = validatePermission(centerId, currentUser);
		if (!validatePermission.isSuccess()) {
			return validatePermission;
		}
		int line;
		// 归档
		if (!flag) {
			// 根据groupId获取group中所有成员的userId
			List<String> userIdList = groupMapper.getMembersByGroupId(groupId);
			// group里没有成员，直接归档
			if (userIdList.size() == 0) {
				line = groupMapper.updateStatusByGroupId(groupId, AccountActiveStatus.Disabled.getValue());
			} else {
				// 判断group中所有成员是否归档
				int index = groupMapper.isAllArchived(userIdList);
				// 有成员未归档
				if (index >= 1) {
					return failResult(getTipMsg("archive_group"));
				} else {
					line = groupMapper.updateStatusByGroupId(groupId, AccountActiveStatus.Disabled.getValue());
				}
			}
			// 解除归档
		} else {
			line = groupMapper.updateStatusByGroupId(groupId, AccountActiveStatus.Enabled.getValue());
		}
		if (line == 0) {
			return failResult(getTipMsg("operation_failed"));
		}
		log.info("updateArchiveGroup|end");
		return successResult(getTipMsg("operation_success"));
	}

	/**
	 * 归档/解除归档room
	 */
	@Override
	public ServiceResult<Object> updateArchiveRoom(String roomId, boolean flag, UserInfoVo currentUser) {
		log.info("updateArchiveRoom|start|roomId=" + roomId + "|flag=" + flag);
		String centerId = centersInfoMapper.getCenterIdByRoomId(roomId);
		// 验证园长和二级园长没有园区或者不是自己的园区时，提示没有权限
		ServiceResult<Object> validatePermission = validatePermission(centerId, currentUser);
		if (!validatePermission.isSuccess()) {
			return validatePermission;
		}
		int line;
		// 归档
		if (!flag) {
			// 判断room中group是否全部归档
			int index = roomInfoMapper.isAllArchived(roomId);
			// 获取room中所有人的userId
			// 有group未归档
			if (index >= 1) {
				return failResult(getTipMsg("archive_room_group"));
			}
			List<String> userIdList = roomInfoMapper.getUserListByRoomId(roomId);
			int tag = 0;
			if (userIdList.size() != 0) {
				tag = centersInfoMapper.isAllUserArchived(userIdList);
			}
			// 有user未归档
			if (tag >= 1) {
				return failResult(getTipMsg("archive_room"));
			}
			line = roomInfoMapper.updateStatusByRoomId(roomId, AccountActiveStatus.Disabled.getValue());
			// 解除归档
		} else {
			int res = centersInfoMapper.isCentersAchivedByRoomId(roomId);
			log.info("================res = " + res);
			if (res >= 1) {
				return failResult(getTipMsg("operation_failed"));
			}
			line = roomInfoMapper.updateStatusByRoomId(roomId, AccountActiveStatus.Enabled.getValue());
		}
		if (line == 0) {
			return failResult(getTipMsg("operation_failed"));
		}
		log.info("updateArchiveRoom|end");
		return successResult(getTipMsg("operation_success"));
	}

	/**
	 * 归档/解除归档center
	 */
	@Override
	public ServiceResult<Object> updateArchiveCenter(String centerId, boolean flag, UserInfoVo currentUser) {
		log.info("updateArchiveCenter|start|centerId=" + centerId + "|falg=" + flag);
		List<RoleInfoVo> roleInfoVoList = currentUser.getRoleInfoVoList();
		if (containRoles(roleInfoVoList, Role.CentreManager, Role.EducatorSecondInCharge)) {
			return failResult(getTipMsg("no.permissions"));
		}
		int line = 0;
		// 归档
		if (!flag) {
			// 判断该园区中的所有room是否归档
			int index = centersInfoMapper.isAllArchived(centerId);
			// 有room未归档
			if (index >= 1) {
				return failResult(getTipMsg("archive_center_room"));
			}
			// 获取园区中所有人的userId
			List<String> userIdList = centersInfoMapper.getUserListByCenterId(centerId);
			int tag = 0;
			if (userIdList.size() != 0) {
				tag = centersInfoMapper.isAllUserArchived(userIdList);
				log.info("updateArchiveCenter|tag=" + tag);
			}
			// 有user未归档
			if (tag >= 1) {
				return failResult(getTipMsg("archive_center"));
			}
			// 归档
			line = centersInfoMapper.updateStatusByCenterId(centerId, AccountActiveStatus.Disabled.getValue());
			// 更改authorityGroup表中的status
			authorityGroupInfoMapper.updateAuthorityGroupStatus(centerId, AccountActiveStatus.Disabled.getValue());
		} else {
			// 解除归档
			line = centersInfoMapper.updateStatusByCenterId(centerId, AccountActiveStatus.Enabled.getValue());
			// 更改authorityGroup表中的status
			authorityGroupInfoMapper.updateAuthorityGroupStatus(centerId, AccountActiveStatus.Enabled.getValue());
		}
		if (line == 0) {
			return failResult(getTipMsg("operation_failed"));
		}
		log.info("updateArchiveCenter|end");
		return successResult(getTipMsg("operation_success"));
	}

	/**
	 * 获取菜单
	 */
	@Override
	public ServiceResult<Object> getCenterMenuList(UserInfoVo user) {
		log.info("getCenterMenuList|start");
		List<RoleInfoVo> roleList = user.getRoleInfoVoList();
		if (ListUtil.isEmpty(roleList)) {
			return failResult(1);
		}
		// 如果不是Ceo，c园长，
		if (!(containRole(Role.CEO, roleList) == 1 || containRole(Role.CentreManager, roleList) == 1 || containRole(Role.EducatorSecondInCharge, roleList) == 1)) {
			return failResult(1);
		}
		String centerId = user.getUserInfo().getCentersId();
		List<CentersInfo> list = centersInfoMapper.getCentersInfo(centerId, null);
		log.info("getCenterMenuList|end|centerId=" + centerId);
		return successResult((Object) list);
	}

	/**
	 * 新增菜单
	 */
	@Override
	public ServiceResult<Object> updateWeekMenu(List<WeekNutrionalMenuVo> weekMenuList, UserInfoVo currentUser) {
		String centerId = weekMenuList.get(0).getCenterId();
		// 验证园长和二级园长没有园区或者不是自己的园区时，提示没有权限
		ServiceResult<Object> validatePermission = validatePermission(centerId, currentUser);
		if (!validatePermission.isSuccess()) {
			return validatePermission;
		}
		// 如果center已归档，不允许更新菜单
		int index = centersInfoMapper.isCentersAchivedById(centerId);
		if (index >= 1) {
			return failResult(getTipMsg("staffCenter_archive"));
		}
		log.info("updateWeekMenu|start");
		for (WeekNutrionalMenuVo weekNutrionalMenuVo : weekMenuList) {
			String sourceId = weekNutrionalMenuVo.getLunchImageId();
			FileVo image = weekNutrionalMenuVo.getImage();
			log.info("updateWeekMenu|sourceId=" + sourceId);
			if (StringUtil.isEmpty(sourceId) && image != null) {
				sourceId = UUID.randomUUID().toString();
			}
			// 保存图片
			initImage(image, sourceId);
			weekNutrionalMenuVo.setLunchImageId(sourceId);
		}
		// 更新菜单
		weekMapper.updateMenuInfo(weekMenuList);
		log.info("updateWeekMenu|end");
		return successResult(getTipMsg("staff_save"), (Object) weekMenuList);
	}

	/**
	 * 定时发送菜单
	 */
	@Override
	public void sendTodaysMenu(Date date) {
		log.info("sendTodaysMenu|start");
		// 菜单周期,4周轮换
		int MENU_CYCLE = 4;
		// 判断当天的newsfeed是否已发送
		int index = newsInfoMapper.getTodaysMenu(NewsType.TodayMenus.getValue(), date);
		if (index > 0) {
			log.info("sendTodaysMenu|Has been sent");
			return;
		}
		// 菜单开始日期
		Date startDate = DateUtil.parseString(getSystemMsg("menu_begin_date"));
		// 如果date小于菜单开始日期，直接返回
		if (startDate.after(date)) {
			return;
		}
		// date距离菜单开始日期的总天数
		int menuDays = com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(startDate, date);
		menuDays++;
		// 总的周数，一周7天
		short weekTotal = (short) (menuDays % 7 == 0 ? menuDays / 7 : menuDays / 7 + 1);
		// 第几周
		short weekNum = (short) (weekTotal % MENU_CYCLE == 0 ? MENU_CYCLE : weekTotal % MENU_CYCLE);
		// 星期几
		short weekToday = (short) (menuDays % 7 == 0 ? 7 : menuDays % 7);
		// 如果是周末，直接返回，不发送菜单
		if (weekToday == Week.Saturday.getValue() || weekToday == Week.Sunday.getValue()) {
			log.info("sendTodaysMenu|return|weekToday=" + weekToday);
			return;
		}
		List<SelecterPo> centerList = centersInfoMapper.selecterCenters(null, null);
		for (SelecterPo center : centerList) {
			// 获取当天菜单
			WeekNutrionalMenuInfo weekTodayMenuInfo = weekMapper.getTodayMenu(new WeekMenuCondition(center.getId(), weekNum, weekToday));
			// 如果菜单里breakfast，lunch等全都为空，则返回不发送此菜单
			if (StringUtil.isEmpty(weekTodayMenuInfo.getBreakfast()) && StringUtil.isEmpty(weekTodayMenuInfo.getMorningTea())
					&& StringUtil.isEmpty(weekTodayMenuInfo.getLunch()) && StringUtil.isEmpty(weekTodayMenuInfo.getDessert())
					&& StringUtil.isEmpty(weekTodayMenuInfo.getAfternoonTea()) && StringUtil.isEmpty(weekTodayMenuInfo.getLateAfternoonTea())) {
				log.info("sendTodaysMenu|return|end");
				continue;
			}
			String breakfast = StringUtil.isEmpty(weekTodayMenuInfo.getBreakfast()) ? "" : weekTodayMenuInfo.getBreakfast();
			String morningTea = StringUtil.isEmpty(weekTodayMenuInfo.getMorningTea()) ? "" : weekTodayMenuInfo.getMorningTea();
			String lunch = StringUtil.isEmpty(weekTodayMenuInfo.getLunch()) ? "" : weekTodayMenuInfo.getLunch();
			String dessert = StringUtil.isEmpty(weekTodayMenuInfo.getDessert()) ? "" : weekTodayMenuInfo.getDessert();
			String afternoonTea = StringUtil.isEmpty(weekTodayMenuInfo.getAfternoonTea()) ? "" : weekTodayMenuInfo.getAfternoonTea();
			String lateAfternoonTea = StringUtil.isEmpty(weekTodayMenuInfo.getLateAfternoonTea()) ? "" : weekTodayMenuInfo.getLateAfternoonTea();
			// 菜单的标题和内容
			String content = MessageFormat.format(getSystemEmailMsg("newsfeed.menu.content"), breakfast, morningTea, lunch, dessert, afternoonTea, lateAfternoonTea);
			log.info("sendTodaysMenu|content:" + content);
			String newsId = UUID.randomUUID().toString();
			String sourceId = UUID.randomUUID().toString();
			int count = saveNewsInfo(center.getId(), newsId, sourceId, date);
			log.info("sendTodaysMenu|count=" + count + "newsId=" + newsId);
			if (count >= 1) {
				// 保存附件，NQS，内容
				saveAttNqsCon(weekTodayMenuInfo, sourceId, newsId, content);
			}
		}
		log.info("sendTodaysMenu|end");
	}

	/**
	 * 保存newsfeed附件，NQS，内容
	 */
	private void saveAttNqsCon(WeekNutrionalMenuInfo weekTodayMenuInfo, String sourceId, String newsId, String content) {
		// 附件
		List<AttachmentInfo> attList = attachmentInfoMapper.getAttachmentInfoBySourceId(weekTodayMenuInfo.getLunchImageId());
		for (AttachmentInfo att : attList) {
			att.setId(UUID.randomUUID().toString());
			att.setSourceId(sourceId);
			attachmentInfoMapper.insert(att);
		}
		// NQS
		String[] nqsArr = getSystemMsg("nqs").split(";");
		for (String nqsVersion : nqsArr) {
			NqsRelationInfo nqsNews = new NqsRelationInfo();
			nqsNews.setId(UUID.randomUUID().toString());
			nqsNews.setObjId(newsId);
			nqsNews.setDeleteFlag(DeleteFlag.Default.getValue());
			nqsNews.setNqsVersion(nqsVersion);
			nqsNews.setTag(NqsTag.V2.getValue());
			nqsRelationInfoMapper.insert(nqsNews);
		}
		// 内容
		NewsContentInfo newsContent = new NewsContentInfo();
		String commontId = UUID.randomUUID().toString();
		newsContent.setId(commontId);
		newsContent.setNewsId(newsId);
		newsContent.setContent(content);
		int count2 = contentInfoMapper.insert(newsContent);
		log.info("sendTodaysMenu|count2=" + count2 + "|commontId=" + commontId);

	}

	/**
	 * 保存newsInfo
	 */
	private int saveNewsInfo(String id, String newsId, String sourceId, Date date) {
		NewsInfo newsInfo = new NewsInfo();

		newsInfo.setId(newsId);
		newsInfo.setNewsType(NewsType.TodayMenus.getValue());
		newsInfo.setCentersId(id);
		newsInfo.setStatus(NewsStatus.Appreoved.getValue());
		newsInfo.setDeleteFlag(DeleteFlag.Default.getValue());
		newsInfo.setApproveTime(date);
		newsInfo.setCreateTime(date);
		newsInfo.setUpdateTime(date);
		newsInfo.setImgId(sourceId);

		int count = newsInfoMapper.insert(newsInfo);
		return count;
	}

	/**
	 * 
	 * @description 获取菜单开始日期menu_begin_date距离date的总天数
	 * @author mingwang
	 * @create 2016年8月26日上午11:31:23
	 * @version 1.0
	 * @return
	 */
	public int getMenuDays(Date date) {
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateStart = getSystemMsg("menu_begin_date");
		String dateNowStr = sdf1.format(date);
		Date start = null;
		Date now = null;
		try {
			start = sdf2.parse(dateStart + " 00:00:00");
			now = sdf2.parse(dateNowStr + " 00:00:00");
		} catch (ParseException e) {
			log.info("sendTodaysMenu|getMenuDays|ParseException");
			e.printStackTrace();
		}
		if (start.after(now)) {
			log.info("sendTodaysMenu|getMenuDays|start after now");
			return -1;
		}
		long s1 = start.getTime();
		long s2 = now.getTime();
		long diff = s2 - s1;
		double diffDays = (double) diff / (double) (24 * 60 * 60 * 1000);
		return (int) diffDays;
	}

	/**
	 * 删除园长/老师时解除与center/group的关系
	 */
	@Override
	public void releaseStaff(String userId) {
		log.info("releaseStaff|start");
		int clearCenterLead = centersInfoMapper.clearCenterLead(userId);
		int clearGroupEduer = groupMapper.clearGroupEduer(userId);
		log.info("releaseStaff|end|clearCenterLead=" + clearCenterLead + "|clearGroupEduer=" + clearGroupEduer);
	}

	@Override
	public Map<String, String> getCenterRoom(String roomId) {
		Map<String, String> map = new HashMap<String, String>();
		RoomInfo room = roomInfoMapper.selectByPrimaryKey(roomId);
		CentersInfo center = centersInfoMapper.selectByPrimaryKey(room.getCentersId());
		map.put("roomName", room.getName());
		map.put("centerName", center.getName());
		return map;
	}

	/**
	 * 
	 * @description 当此人为兼职+二级园长的时候,或者园长且因为园区归档导致没有园区的时候,或者操作的不是自己的园区,提示没有权限操作
	 * @author mingwang
	 * @create 2016年11月22日下午2:37:33
	 * @version 1.0
	 * @param centerId
	 * @param currentUser
	 * @return
	 */
	private ServiceResult<Object> validatePermission(String centerId, UserInfoVo currentUser) {
		boolean containRoles = containRoles(currentUser.getRoleInfoVoList(), Role.CentreManager, Role.EducatorSecondInCharge);
		if (containRoles) {
			if (StringUtil.isEmpty(currentUser.getUserInfo().getCentersId())) {
				return failResult(getTipMsg("no.permissions"));
			} else {
				if (!centerId.equals(currentUser.getUserInfo().getCentersId())) {
					return failResult(getTipMsg("no.permissions"));
				}
			}
		}
		return successResult();
	}

	@Override
	public ServiceResult<Object> getAllCentersInfos() {
		// 获取Center详细信息
		return successResult((Object) centersInfoMapper.getAllCentersInfos());
	}

	@Override
	public void saveCheckRoomTask(String id, boolean check) {
		roomTaskCheckMapper.updateCheckById(id, check);
	}
}
