package com.aoyuntek.aoyun.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.condtion.UserDetailCondition;
import com.aoyuntek.aoyun.dao.AccountInfoMapper;
import com.aoyuntek.aoyun.dao.AccountRoleInfoMapper;
import com.aoyuntek.aoyun.dao.ChildAttendanceInfoMapper;
import com.aoyuntek.aoyun.dao.ChildDietaryRequireInfoMapper;
import com.aoyuntek.aoyun.dao.ChildHealthMedicalDetailInfoMapper;
import com.aoyuntek.aoyun.dao.ChildMedicalInfoMapper;
import com.aoyuntek.aoyun.dao.FamilyInfoMapper;
import com.aoyuntek.aoyun.dao.OutChildCenterInfoMapper;
import com.aoyuntek.aoyun.dao.RelationKidParentInfoMapper;
import com.aoyuntek.aoyun.dao.RoleInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.entity.po.AccountInfo;
import com.aoyuntek.aoyun.entity.po.AccountRoleInfo;
import com.aoyuntek.aoyun.entity.po.ChildAttendanceInfo;
import com.aoyuntek.aoyun.entity.po.ChildDietaryRequireInfo;
import com.aoyuntek.aoyun.entity.po.ChildHealthMedicalDetailInfo;
import com.aoyuntek.aoyun.entity.po.ChildMedicalInfo;
import com.aoyuntek.aoyun.entity.po.FamilyInfo;
import com.aoyuntek.aoyun.entity.po.OutChildCenterInfo;
import com.aoyuntek.aoyun.entity.po.OutFamilyPo;
import com.aoyuntek.aoyun.entity.po.RelationKidParentInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.po.WaitingListPo;
import com.aoyuntek.aoyun.enums.ArchivedStatus;
import com.aoyuntek.aoyun.enums.ChildType;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.EnrolledState;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.enums.UserType;
import com.aoyuntek.aoyun.service.ICommonService;
import com.aoyuntek.aoyun.service.IUserInfoService;
import com.aoyuntek.aoyun.service.IWaitingListService;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

@Service
public class WaitingListServiceImpl implements IWaitingListService {
    @Autowired
    private ChildAttendanceInfoMapper childAttendanceInfoMapper;
    @Autowired
    private ICommonService commonService;
    @Autowired
    private UserInfoMapper userInfoMapper;
    @Autowired
    private AccountInfoMapper accountInfoMapper;
    @Autowired
    private RoleInfoMapper roleInfoMapper;
    @Autowired
    private AccountRoleInfoMapper accountRoleInfoMapper;
    @Autowired
    private ChildHealthMedicalDetailInfoMapper childHealthMedicalDetailInfoMapper;
    @Autowired
    private FamilyInfoMapper familyInfoMapper;
    @Autowired
    private OutChildCenterInfoMapper outChildCenterInfoMapper;
    @Autowired
    private RelationKidParentInfoMapper relationKidParentInfoMapper;
    @Autowired
    private IUserInfoService userInfoService;
    @Autowired
    private ChildDietaryRequireInfoMapper childDietaryRequireInfoMapper;
    @Autowired
    private ChildMedicalInfoMapper childMedicalInfoMapper;

    private static Logger log = Logger.getLogger(WaitingListServiceImpl.class);

    private int childCount = 0;
    private int parentCount = 0;
    private int existChildCount = 0;

    @Override
    public void importData(List<OutFamilyPo> data, String centerIds) {
        childCount = 0;
        parentCount = 0;
        existChildCount = 0;
        List<WaitingListPo> waitingListPos = new ArrayList<WaitingListPo>();
        List<OutFamilyPo> newData = new ArrayList<OutFamilyPo>();
        for (OutFamilyPo outFamilyPo : data) {
            // 家长email为空
            String email = StringUtil.isNotEmpty(outFamilyPo.getParentList().get(0).getEmail()) ? outFamilyPo.getParentList().get(0).getEmail()
                    : outFamilyPo.getParentList().get(1).getEmail();
            if (StringUtil.isEmpty(email)) {
                log.error("error emailisnull " + outFamilyPo.getOldId());
                continue;
            }
            // 将email重复
            boolean falg = true;
            for (UserInfo u : outFamilyPo.getParentList()) {
                if ("lara.butres@hotmail.com".equals(u.getEmail())) {
                    System.err.println("outFamilyPooutFamilyPooutFamilyPooutFamilyPo");
                }
                UserDetailCondition condition = new UserDetailCondition();
                condition.setEmail(u.getEmail());
                List<UserInfo> list = userInfoMapper.getByCondition(condition);
                if (ListUtil.isEmpty(list)) {
                    falg = false;
                }
                for (UserInfo uu : list) {
                    if (samePerson(u, uu)) {
                        falg = false;
                    }
                }
            }
            if (falg) {
                log.error("error isexist " + outFamilyPo.getOldId());
                continue;
            }

            // 将上课周期为空的变成全选
            if (!choose(outFamilyPo)) {
                outFamilyPo.setMonday(true);
                outFamilyPo.setTuesday(true);
                outFamilyPo.setWednesday(true);
                outFamilyPo.setThursday(true);
                outFamilyPo.setFriday(true);
            }
            // 将家长1和家长2相同的email通过加*来区分
            List<UserInfo> parents = outFamilyPo.getParentList();
            if (parents.size() == 2 && parents.get(0).getEmail().equals(parents.get(1).getEmail())) {
                String e = "*" + parents.get(1).getEmail();
                parents.get(1).setEmail(e);
            }
            newData.add(outFamilyPo);
        }

        // 处理同一家庭的孩子
        for (int i = 0; i < newData.size(); i++) {
            WaitingListPo waitingListPo = new WaitingListPo();
            List<OutFamilyPo> outFamilyPos = new ArrayList<OutFamilyPo>();
            OutFamilyPo po = newData.get(i);
            String email = StringUtil.isNotEmpty(po.getParentList().get(0).getEmail()) ? po.getParentList().get(0).getEmail() : po.getParentList()
                    .get(1).getEmail();
            outFamilyPos.add(po);
            for (int j = newData.size() - 1; j > i; j--) {
                OutFamilyPo po2 = newData.get(j);
                String email2 = StringUtil.isNotEmpty(po2.getParentList().get(0).getEmail()) ? po2.getParentList().get(0).getEmail() : po2
                        .getParentList().get(1).getEmail();
                if (email.equals(email2)) {
                    outFamilyPos.add(po2);
                    newData.remove(j);
                }
            }
            waitingListPo.setOutFamilyPos(outFamilyPos);
            waitingListPos.add(waitingListPo);
        }

        System.err.println(waitingListPos.size());
        // 导入数据
        for (WaitingListPo po : waitingListPos) {
            String familyId = UUID.randomUUID().toString();
            List<OutFamilyPo> outFamilyPos = po.getOutFamilyPos();
            List<UserInfo> parents = outFamilyPos.get(0).getParentList();
            // 处理家庭关系
            boolean goon = dealFamilyRelation(outFamilyPos, centerIds);
            if (!goon) {
                continue;
            }
            for (OutFamilyPo outFamilyPo : outFamilyPos) {
                String childId = addUserOut(outFamilyPo.getChild(), familyId, UserType.Child, outFamilyPo.getTime(), outFamilyPo.getOldId());
                addAccountOut(outFamilyPo.getChild(), Role.Children, outFamilyPo.getTime());
                buildRelationKidParentInfo(outFamilyPo.getChild());
                addAttendsOut(childId, outFamilyPo);
                addHealthMedicalDetail(childId);
                // 初始化饮食要求
                initDietaryRequire(childId);
                //
                initMedical(childId);
                // 插入所选的Center
                String[] cs = centerIds.split(",");
                insertOutChildCenterInfo(childId, Arrays.asList(cs));
            }
            Date now = new Date();
            for (UserInfo parent : parents) {
                if (StringUtil.isEmpty(parent.getFirstName()) || StringUtil.isEmpty(parent.getLastName()) || StringUtil.isEmpty(parent.getEmail())) {
                    continue;
                }
                // 新增用戶
                addUserOut(parent, familyId, UserType.Parent, now, null);
                // 新增賬戶信息
                addAccountOut(parent, Role.Parent, new Date());
                // 建立关系
                buildRelationKidParentInfo(parent);
                now.setTime(now.getTime() + 1000);

            }

            String familyName = outFamilyPos.get(0).getFamilyName();
            initOrUpdateFamilyInfo(familyId, familyName);
        }
        log.error("existChildCount=" + existChildCount);
        log.error("childCount=" + childCount);
        log.error("parentCount=" + parentCount);
    }

    /**
     * @description 处理家庭关系
     * @author hxzhang
     * @create 2017年5月9日上午10:50:50
     */
    private boolean dealFamilyRelation(List<OutFamilyPo> outFamilyPos, String centerIds) {
        boolean goon = true;
        // 处理在2017-04-07 00:00至2017-04-07 01:00导入的数据,只需更新creat time和update time
        for (OutFamilyPo outFamilyPo : outFamilyPos) {
            UserInfo dbUserInfo = userInfoMapper.getOutChildByName(outFamilyPo.getChild().getFirstName(), outFamilyPo.getChild().getLastName());
            if (dbUserInfo != null) {
                // 已存在小孩则将孩子的child attendance中的creat time和update time更新
                childAttendanceInfoMapper.updateOutChildTime(dbUserInfo.getId(), outFamilyPo.getTime());
                // 更新user中的creat time和update time
                userInfoMapper.updateOutUserTime(dbUserInfo.getId(), outFamilyPo.getTime(), outFamilyPo.getOldId());
                existChildCount = existChildCount + 1;
                List<UserInfo> parens = outFamilyPo.getParentList();
                List<UserInfo> dbParents = userInfoMapper.getParentInfoByFamilyId(dbUserInfo.getFamilyId());

                Date now = new Date();
                for (UserInfo uu : parens) {
                    boolean t = false;
                    for (UserInfo u : dbParents) {
                        if (samePerson2(u, uu)) {
                            t = true;
                        }
                    }
                    if (!t && StringUtil.isNotEmpty(uu.getEmail())) {
                        // 新增用戶
                        addUserOut(uu, dbParents.get(0).getFamilyId(), UserType.Parent, now, null);
                        // 新增賬戶信息
                        addAccountOut(uu, Role.Parent, new Date());
                        // 建立关系
                        buildRelationKidParentInfo(uu);
                        now.setTime(now.getTime() + 1000);
                    }
                }

                goon = false;
            } else {
                goon = dealSameFamily(outFamilyPo, centerIds);
            }

        }
        return goon;
    }

    /**
     * @description 处理同一家庭的孩子进入家庭
     * @author hxzhang
     * @create 2017年5月9日下午1:01:20
     */
    private boolean dealSameFamily(OutFamilyPo outFamilyPo, String centerIds) {
        List<UserInfo> parents = outFamilyPo.getParentList();
        for (UserInfo parent : parents) {
            if ("lara.butres@hotmail.com".equals(parent.getEmail())) {
                System.err.println("outFamilyPooutFamilyPooutFamilyPooutFamilyPo");
            }
            String familyId = "";
            boolean falg = true;
            UserDetailCondition condition = new UserDetailCondition();
            condition.setEmail(parent.getEmail());
            List<UserInfo> infos = userInfoMapper.getByCondition(condition);
            if (ListUtil.isEmpty(infos)) {
                continue;
            }
            List<UserInfo> list = userInfoMapper.getParentInfoByFamilyId(infos.get(0).getFamilyId());
            Date now = new Date();
            for (UserInfo dbParent : list) {
                if (samePerson2(parent, dbParent)) {
                    falg = false;
                    familyId = dbParent.getFamilyId();
                }
            }

            if (StringUtil.isNotEmpty(parent.getEmail()) && falg) {
                // 新增用戶
                addUserOut(parent, infos.get(0).getFamilyId(), UserType.Parent, now, null);
                // 新增賬戶信息
                addAccountOut(parent, Role.Parent, new Date());
                // 建立关系
                buildRelationKidParentInfo(parent);
                now.setTime(now.getTime() + 1000);
            }
            if (!falg) {
                if (StringUtil.isEmpty(familyId)) {
                    System.err.println("121211313131");
                }
                List<UserInfo> childs = userInfoMapper.getChildByNameFamilyId(outFamilyPo.getChild().getFirstName(), outFamilyPo.getChild()
                        .getLastName(), familyId);
                if (ListUtil.isNotEmpty(childs)) {
                    log.error("error sameChild " + outFamilyPo.getOldId());
                    return false;
                } else {
                    String childId = addUserOut(outFamilyPo.getChild(), familyId, UserType.Child, outFamilyPo.getTime(), outFamilyPo.getOldId());
                    addAccountOut(outFamilyPo.getChild(), Role.Children, outFamilyPo.getTime());
                    buildRelationKidParentInfo(outFamilyPo.getChild());
                    addAttendsOut(childId, outFamilyPo);
                    addHealthMedicalDetail(childId);
                    // 初始化饮食要求
                    initDietaryRequire(childId);
                    //
                    initMedical(childId);
                    // 插入所选的Center
                    String[] cs = centerIds.split(",");
                    insertOutChildCenterInfo(childId, Arrays.asList(cs));
                    return false;
                }
            }
        }
        return true;
    }

    private boolean samePerson(UserInfo u1, UserInfo u2) {
        String fname1 = StringUtil.isNotEmpty(u1.getFirstName()) ? u1.getFirstName().toLowerCase() : u1.getFirstName();
        String lname1 = StringUtil.isNotEmpty(u1.getLastName()) ? u1.getLastName().toLowerCase() : u1.getLastName();
        String email1 = StringUtil.isNotEmpty(u1.getEmail()) ? u1.getEmail().toLowerCase() : u1.getEmail();
        String fname2 = StringUtil.isNotEmpty(u2.getFirstName()) ? u2.getFirstName().toLowerCase() : u2.getFirstName();
        String lname2 = StringUtil.isNotEmpty(u2.getLastName()) ? u2.getLastName().toLowerCase() : u2.getLastName();
        String email2 = StringUtil.isNotEmpty(u2.getEmail()) ? u2.getEmail().toLowerCase() : u2.getEmail();
        if (StringUtil.isNotEmpty(fname1) && StringUtil.isNotEmpty(fname2) && StringUtil.isNotEmpty(lname1) && StringUtil.isNotEmpty(lname2)
                && StringUtil.isNotEmpty(email1) && StringUtil.isNotEmpty(email2) && fname1.equals(fname2) && lname1.equals(lname2)
                && email1.equals(email2)) {
            return true;
        }
        return false;
    }

    private boolean samePerson2(UserInfo u1, UserInfo u2) {
        String fname1 = StringUtil.isNotEmpty(u1.getFirstName()) ? u1.getFirstName().toLowerCase() : u1.getFirstName();
        String lname1 = StringUtil.isNotEmpty(u1.getLastName()) ? u1.getLastName().toLowerCase() : u1.getLastName();
        String email1 = StringUtil.isNotEmpty(u1.getEmail()) ? u1.getEmail().toLowerCase() : u1.getEmail();
        String fname2 = StringUtil.isNotEmpty(u2.getFirstName()) ? u2.getFirstName().toLowerCase() : u2.getFirstName();
        String lname2 = StringUtil.isNotEmpty(u2.getLastName()) ? u2.getLastName().toLowerCase() : u2.getLastName();
        String email2 = StringUtil.isNotEmpty(u2.getEmail()) ? u2.getEmail().toLowerCase() : u2.getEmail();
        if (StringUtil.isNotEmpty(fname1) && StringUtil.isNotEmpty(fname2) && StringUtil.isNotEmpty(lname1) && StringUtil.isNotEmpty(lname2)
                && StringUtil.isNotEmpty(email1) && StringUtil.isNotEmpty(email2) && fname1.equals(fname2) && lname1.equals(lname2)
                && email1.equals(email2)) {
            return true;
        }
        if (StringUtil.isNotEmpty(fname1) && StringUtil.isNotEmpty(fname2) && StringUtil.isNotEmpty(lname1) && StringUtil.isNotEmpty(lname2)
                && fname1.equals(fname2) && lname1.equals(lname2)) {
            return true;
        }
        if (StringUtil.isNotEmpty(email1) && StringUtil.isNotEmpty(email2) && email1.equals(email2)) {
            return true;
        }
        return false;
    }

    private void addAttendsOut(String userId, OutFamilyPo outFamily) {
        ChildAttendanceInfo childAttendanceInfo = new ChildAttendanceInfo();
        childAttendanceInfo.setId(UUID.randomUUID().toString());
        childAttendanceInfo.setUserId(userId);
        childAttendanceInfo.setDeleteFlag(DeleteFlag.Default.getValue());
        childAttendanceInfo.setCreateTime(outFamily.getTime());
        childAttendanceInfo.setUpdateTime(outFamily.getTime());
        childAttendanceInfo.setMonday(outFamily.getMonday());
        childAttendanceInfo.setTuesday(outFamily.getTuesday());
        childAttendanceInfo.setWednesday(outFamily.getWednesday());
        childAttendanceInfo.setThursday(outFamily.getThursday());
        childAttendanceInfo.setFriday(outFamily.getFriday());
        childAttendanceInfo.setEnrolled(EnrolledState.Default.getValue());
        childAttendanceInfo.setType(ChildType.External.getValue());
        childAttendanceInfo.setEnrolmentDate(outFamily.getChangeDate());
        childAttendanceInfo.setChangeDate(outFamily.getChangeDate());
        childAttendanceInfoMapper.insertSelective(childAttendanceInfo);
    }

    private String addUserOut(UserInfo userInfo, String familyId, UserType userType, Date now, String oldId) {
        String userId = UUID.randomUUID().toString();
        userInfo.setId(userId);
        userInfo.setCreateTime(now);
        userInfo.setUpdateTime(now);
        userInfo.setFamilyId(familyId);
        userInfo.setUserType(userType.getValue());
        userInfo.setDeleteFlag(DeleteFlag.Default.getValue());
        userInfo.setOldId(oldId);
        // 获取personColor
        String personColor = commonService.getPersonColor(userInfo);
        userInfo.setPersonColor(personColor);
        userInfoMapper.insertSelective(userInfo);
        if (userType == UserType.Child) {
            childCount = childCount + 1;
        }
        if (userType == UserType.Parent) {
            parentCount = parentCount + 1;
        }
        return userId;
    }

    private void addAccountOut(UserInfo userInfo, Role role, Date now) {
        String accountId = UUID.randomUUID().toString();
        AccountInfo accountInfo = new AccountInfo();
        accountInfo.setId(accountId);
        accountInfo.setAccount(userInfo.getEmail());
        accountInfo.setCreateTime(now);
        accountInfo.setDeleteFlag(DeleteFlag.Default.getValue());
        accountInfo.setStatus(ArchivedStatus.Default.getValue());
        accountInfo.setUpdateTime(now);
        accountInfo.setUserId(userInfo.getId());
        accountInfoMapper.insertSelective(accountInfo);

        // 新增AccountRoleInfo
        AccountRoleInfo accountRoleInfo = new AccountRoleInfo();
        accountRoleInfo.setAccountId(accountId);
        accountRoleInfo.setCreateTime(now);
        accountRoleInfo.setDeleteFlag(DeleteFlag.Default.getValue());
        accountRoleInfo.setId(UUID.randomUUID().toString());
        accountRoleInfo.setRoleId(roleInfoMapper.selectRoleIdByValue(role.getValue()));
        accountRoleInfo.setUpdateTime(now);
        accountRoleInfoMapper.insertSelective(accountRoleInfo);

    }

    private void addHealthMedicalDetail(String userId) {
        List<ChildHealthMedicalDetailInfo> healthMedicalDetailList = new ArrayList<ChildHealthMedicalDetailInfo>();
        for (int i = 0; i < 13; i++) {
            ChildHealthMedicalDetailInfo chmdi = new ChildHealthMedicalDetailInfo();
            Date now = new Date();
            chmdi.setId(UUID.randomUUID().toString());
            chmdi.setUserId(userId);
            chmdi.setDeleteFlag(DeleteFlag.Default.getValue());
            now.setTime(now.getTime() + i * 1000);
            chmdi.setCreateTime(now);
            chmdi.setNameIndex(i);
            healthMedicalDetailList.add(chmdi);
        }
        childHealthMedicalDetailInfoMapper.insterBatchChildHealthMedicalDetailInfo(healthMedicalDetailList);
    }

    private void initOrUpdateFamilyInfo(String familyId, String familyName) {
        FamilyInfo familyInfo = familyInfoMapper.selectByPrimaryKey(familyId);
        if (null == familyInfo) {
            familyInfo = new FamilyInfo();
            familyInfo.setId(familyId);
            Date now = new Date();
            familyInfo.setCreateTime(now);
            familyInfo.setUpdateTime(now);
            familyInfo.setFamilyName(familyName);
            familyInfo.setDeleteFlag(DeleteFlag.Default.getValue());
            familyInfoMapper.insertSelective(familyInfo);
        } else {
            familyInfo.setUpdateTime(new Date());
            familyInfo.setFamilyName(familyName);
            familyInfoMapper.updateByPrimaryKey(familyInfo);
        }
    }

    private void insertOutChildCenterInfo(String childId, List<String> ids) {
        for (String centerId : ids) {
            outChildCenterInfoMapper.insert(new OutChildCenterInfo(childId, centerId));
        }
    }

    private void buildRelationKidParentInfo(UserInfo userInfo) {
        AccountInfo accountInfo = accountInfoMapper.selectAccountInfoByUserId(userInfo.getId());
        RelationKidParentInfo rkp = new RelationKidParentInfo();
        if (userInfo.getUserType() == UserType.Child.getValue()) {
            // 获取该小孩所在家庭的家长
            List<AccountInfo> accounts = accountInfoMapper.getAccountByFamilyId(userInfo.getFamilyId(), UserType.Parent.getValue());
            if (null != accounts && accounts.size() != 0) {
                for (AccountInfo uv : accounts) {
                    rkp.setParentAccountId(uv.getId());
                    rkp.setId(UUID.randomUUID().toString());
                    rkp.setKidAccountId(accountInfo.getId());
                    rkp.setDeleteFlag(DeleteFlag.Default.getValue());
                    relationKidParentInfoMapper.insertSelective(rkp);
                }
            }
        }
        if (userInfo.getUserType() == UserType.Parent.getValue()) {
            // 获取该家长所在家庭的小孩
            List<AccountInfo> accounts = accountInfoMapper.getAccountByFamilyId(userInfo.getFamilyId(), UserType.Child.getValue());
            if (null != accounts && accounts.size() != 0) {
                for (AccountInfo uv : accounts) {
                    rkp.setKidAccountId(uv.getId());
                    rkp.setId(UUID.randomUUID().toString());
                    rkp.setParentAccountId(accountInfo.getId());
                    rkp.setDeleteFlag(DeleteFlag.Default.getValue());
                    relationKidParentInfoMapper.insertSelective(rkp);
                }
            }
        }
    }

    private boolean choose(OutFamilyPo po) {
        if ("Catalina".equals(po.getChild().getFirstName())) {
            System.err.println("111111111111111111111111");
        }
        if (null != po.getMonday() && po.getMonday()) {
            return true;
        }
        if (null != po.getThursday() && po.getThursday()) {
            return true;
        }
        if (null != po.getTuesday() && po.getTuesday()) {
            return true;
        }
        if (null != po.getWednesday() && po.getWednesday()) {
            return true;
        }
        if (null != po.getFriday() && po.getFriday()) {
            return true;
        }
        return false;
    }

    private void initDietaryRequire(String userId) {
        ChildDietaryRequireInfo dietaryRequire = new ChildDietaryRequireInfo();
        dietaryRequire.setId(UUID.randomUUID().toString());
        dietaryRequire.setUserId(userId);
        dietaryRequire.setEpiPenAttachId(UUID.randomUUID().toString());
        dietaryRequire.setDeleteFlag(DeleteFlag.Default.getValue());
        childDietaryRequireInfoMapper.insertSelective(dietaryRequire);
    }

    private void initMedical(String userId) {
        ChildMedicalInfo medical = new ChildMedicalInfo();
        medical.setId(UUID.randomUUID().toString());
        medical.setUserId(userId);
        medical.setRequireLongMedicAttachId(UUID.randomUUID().toString());
        medical.setDeleteFlag(DeleteFlag.Default.getValue());
        childMedicalInfoMapper.insertSelective(medical);
    }

}
