package com.aoyuntek.aoyun.service.attendance.impl;

import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.constants.SystemConstants;
import com.aoyuntek.aoyun.dao.AttendanceHistoryInfoMapper;
import com.aoyuntek.aoyun.entity.po.AttendanceHistoryInfo;
import com.aoyuntek.aoyun.entity.po.ChangeAttendanceRequestInfo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.factory.AttendanceFactory;
import com.aoyuntek.aoyun.service.attendance.IAttendanceHistoryService;
import com.aoyuntek.aoyun.service.impl.BaseWebServiceImpl;
import com.theone.common.util.ServiceResult;

@Service
public class AttendanceHistoryServiceImpl extends BaseWebServiceImpl<AttendanceHistoryInfo, AttendanceHistoryInfoMapper> implements
        IAttendanceHistoryService {

    @Autowired
    private AttendanceHistoryInfoMapper attendanceHistoryInfoMapper;
    @Autowired
    private AttendanceFactory attendanceFactory;

    @Override
    public ServiceResult<Object> dealAttendanceHistory(ChangeAttendanceRequestInfo attendanceRequestInfo, UserInfoVo userInfoVo) {
        String createUserId = SystemConstants.JobCreateAccountId;
        if (userInfoVo != null) {
            createUserId = userInfoVo.getAccountInfo().getId();
        }
        AttendanceHistoryInfo max = attendanceHistoryInfoMapper.getCurrentGroupNo(attendanceRequestInfo.getChildId());

        // AttendanceHistoryInfo 記錄表更歷史
        AttendanceHistoryInfo attendanceHistoryInfo = new AttendanceHistoryInfo();
        if (max == null) {
            attendanceHistoryInfo.setGroup(1);
        } else {
            if (!max.getCenterId().equals(attendanceRequestInfo.getNewCentreId())) {
                max.setGroup(max.getGroup() + 1);
                // 如果是之前有记录 并且 园发生了变更 那么就清空endDate 更新 beginDate
                // TODO big sssssssssssss
            }
            attendanceHistoryInfo.setGroup(max.getGroup());
        }
        attendanceHistoryInfo.setId(UUID.randomUUID().toString());
        attendanceHistoryInfo.setChildId(attendanceRequestInfo.getChildId());
        attendanceHistoryInfo.setBeginTime(attendanceRequestInfo.getChangeDate());
        attendanceHistoryInfo.setCenterId(attendanceRequestInfo.getNewCentreId());
        attendanceHistoryInfo.setRoomId(attendanceRequestInfo.getNewRoomId());
        attendanceFactory.dealAttendanceHistory(attendanceHistoryInfo, attendanceRequestInfo);
        attendanceHistoryInfo.setCreateAccountId(createUserId);
        attendanceHistoryInfo.setCreateTime(new Date());
        attendanceHistoryInfo.setUpdateAccountId(createUserId);
        attendanceHistoryInfo.setUpdateTime(new Date());
        attendanceHistoryInfo.setDeleteFlag(DeleteFlag.Default.getValue());
        // 更新上一次的的endtime

        attendanceHistoryInfoMapper.updateEndTime(attendanceRequestInfo.getChildId(), attendanceRequestInfo.getChangeDate());

        attendanceHistoryInfoMapper.insert(attendanceHistoryInfo);
        return successResult();
    }
}
