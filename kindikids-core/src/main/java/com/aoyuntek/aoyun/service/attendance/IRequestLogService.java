package com.aoyuntek.aoyun.service.attendance;

import com.aoyuntek.aoyun.dao.RequestLogInfoMapper;
import com.aoyuntek.aoyun.entity.po.ChangeAttendanceRequestInfo;
import com.aoyuntek.aoyun.entity.po.RequestLogInfo;
import com.aoyuntek.aoyun.entity.po.SubtractedAttendance;
import com.aoyuntek.aoyun.entity.po.TemporaryAttendanceInfo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.service.IBaseWebService;

public interface IRequestLogService extends IBaseWebService<RequestLogInfo, RequestLogInfoMapper> {

    void dealLog(UserInfoVo currentUser, ChangeAttendanceRequestInfo re);

    void dealLog(UserInfoVo currentUser, TemporaryAttendanceInfo temp);

    void dealLog(UserInfoVo currentUser, SubtractedAttendance subtractedAttendance);
}
