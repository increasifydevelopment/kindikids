package com.aoyuntek.aoyun.service.attendance;

import java.util.Date;

import com.aoyuntek.aoyun.dao.GivingNoticeInfoMapper;
import com.aoyuntek.aoyun.entity.po.GivingNoticeInfo;
import com.aoyuntek.aoyun.entity.po.GivingNoticeInfoWithBLOBs;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.service.IBaseWebService;
import com.theone.common.util.ServiceResult;

public interface IGivingNoticeInfoService extends IBaseWebService<GivingNoticeInfo, GivingNoticeInfoMapper> {
    /**
     * @description 获取离园申请记录
     * @author hxzhang
     * @create 2016年8月26日上午9:34:58
     * @version 1.0
     * @param id
     * @return
     */
    ServiceResult<Object> getGivingNoticeInfo(String id);

    /**
     * @description 保存离园信息
     * @author hxzhang
     * @create 2016年8月26日上午10:11:34
     * @version 1.0
     * @param givingNoticeInfo
     * @return
     */
    ServiceResult<Object> addOrUpdateGivingNoticeInfo(GivingNoticeInfoWithBLOBs givingNoticeInfoWithBLOBs, UserInfoVo currentUser);

    /**
     * @description 更新对离园申请操作
     * @author hxzhang
     * @create 2016年8月26日下午2:33:05
     * @version 1.0
     * @param opera
     * @return
     */
    ServiceResult<Object> updateOperaGivingNotice(String id, short opera, UserInfoVo currentUser);

    /**
     * @description 定时任务
     * @author hxzhang
     * @create 2016年8月28日下午5:30:55
     * @version 1.0
     * @param now
     * @return
     */
    int updateGivingNoticeTimedTask(Date now);

    /**
     * @description 将离园申请到期且未被审批更新为失效
     * @author hxzhang
     * @create 2016年9月7日下午8:06:03
     * @version 1.0
     * @param now
     * @return
     */
    int updateGivingNoticeStatusTimedTask(Date now);
}
