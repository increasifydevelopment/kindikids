package com.aoyuntek.aoyun.service.attendance.impl;

import java.text.MessageFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.condtion.LeaveCondition;
import com.aoyuntek.aoyun.condtion.UserDetailCondition;
import com.aoyuntek.aoyun.constants.DateFormatterConstants;
import com.aoyuntek.aoyun.dao.AccountInfoMapper;
import com.aoyuntek.aoyun.dao.LeaveInfoMapper;
import com.aoyuntek.aoyun.dao.LeavePaidInfoMapper;
import com.aoyuntek.aoyun.dao.MsgEmailInfoMapper;
import com.aoyuntek.aoyun.dao.StaffEmploymentInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.dao.roster.RosterStaffInfoMapper;
import com.aoyuntek.aoyun.entity.po.AccountInfo;
import com.aoyuntek.aoyun.entity.po.LeaveInfo;
import com.aoyuntek.aoyun.entity.po.LeavePaidInfo;
import com.aoyuntek.aoyun.entity.po.MsgEmailInfo;
import com.aoyuntek.aoyun.entity.po.StaffEmploymentInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.vo.LeaveListVo;
import com.aoyuntek.aoyun.entity.vo.LeaveVo;
import com.aoyuntek.aoyun.entity.vo.RoleInfoVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.EmailType;
import com.aoyuntek.aoyun.enums.LeaveStatus;
import com.aoyuntek.aoyun.enums.LeaveType;
import com.aoyuntek.aoyun.enums.OperaType;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.enums.jobs.MsgEmailType;
import com.aoyuntek.aoyun.factory.EmailFactory;
import com.aoyuntek.aoyun.factory.UserFactory;
import com.aoyuntek.aoyun.service.attendance.ILeaveService;
import com.aoyuntek.aoyun.service.impl.BaseWebServiceImpl;
import com.aoyuntek.aoyun.uitl.EmailUtil;
import com.aoyuntek.framework.model.Pager;
import com.theone.aws.util.AwsEmailUtil;
import com.theone.common.util.ServiceResult;
import com.theone.date.util.DateUtil;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

@Service
public class LeaveServiceImpl extends BaseWebServiceImpl<LeaveInfo, LeaveInfoMapper> implements ILeaveService {
	/**
	 * 日志
	 */
	public static Logger logger = Logger.getLogger(LeaveServiceImpl.class);
	@Autowired
	private LeaveInfoMapper leaveInfoMapper;
	@Autowired
	private LeavePaidInfoMapper leavePaidInfoMapper;
	@Autowired
	private UserInfoMapper userInfoMapper;
	@Autowired
	private AccountInfoMapper accountInfoMapper;
	@Autowired
	private RosterStaffInfoMapper rosterStaffInfoMapper;
	@Autowired
	private EmailFactory emailFactory;
	@Autowired
	private MsgEmailInfoMapper msgEmailInfoMapper;
	@Autowired
	private StaffEmploymentInfoMapper staffEmploymentInfoMapper;

	@Override
	public ServiceResult<Object> saveLeaveInfo(LeaveVo vo, UserInfoVo currentUser) {
		LeaveInfo leaveInfo;
		String accountId = vo.isAdmin() ? vo.getAccountId() : currentUser.getAccountInfo().getId();
		vo.setAccountId(accountId);
		// 验证
		ServiceResult<Object> result = validateSaveLeave(vo, currentUser);
		if (!result.isSuccess()) {
			return result;
		}
		// 如果补请假,不需要判断是否有排班信息
		// if (DateUtil.getPreviousDay(endDate) >= DateUtil.getPreviousDay(new
		// Date())) {
		// 如果请假人被排班了,则给出提示
		if (!vo.isConfirm()) {
			int count = rosterStaffInfoMapper.getCountByAccountIdAndDate(accountId, vo.getStartDate(), vo.getEndDate());
			if (count != 0) {
				return successResult2(2, getTipMsg("leave_request_prompt"));
			}
		}
		// 请假人存在排班信息,还执意要请假,先删除排班信息,再进行请假动作
		// rosterStaffInfoMapper.removRosterStaffInfo(accountId,
		// judgeDate(startDate), judgeDate(endDate));
		// 如果是临时排班,删除临时角色权限.
		// accountRoleInfoMapper.removeTempRoleByAccountId(accountId);
		// }

		UserInfo staff = null;
		String staffName = null;
		if (vo.isAdmin()) {
			staff = userInfoMapper.getUserInfoByAccountId(vo.getAccountId());
			staffName = userFactory.getUserName(staff);
		} else {
			staffName = userFactory.getUserName(currentUser.getUserInfo());
		}

		// 初始化LeaveInfo
		leaveInfo = initLeaveInfo(staff, vo, currentUser);
		// 给Admin发送邮件
		String[] emailAddress = getSystemMsg("emailAddress").split(";");
		for (int i = 0; i < emailAddress.length; i++) {
			String email = emailAddress[i];
			UserDetailCondition condition = new UserDetailCondition();
			condition.setEmail(email);
			List<UserInfo> users = userInfoMapper.getByCondition(condition);
			String adminUserName = email;
			if (!ListUtil.isEmpty(users)) {
				UserInfo user = users.get(0);
				adminUserName = userFactory.getUserName(user);
			}
			String html = "";
			// 不是管理員申請的
			if (!vo.isAdmin()) {
				String title = getSystemEmailMsg("staff_leave_to_admin_title");
				String content = java.text.MessageFormat.format(getSystemEmailMsg("staff_leave_to_admin_content"), adminUserName, staffName);
				html = EmailUtil.replaceHtml(title, content);
				sendMail(emailAddress[i], adminUserName, title, html, true);
				continue;
			}
			// 管理員申請的
			String title = getSystemEmailMsg("admin_apply_staff_send_admin_title");
			String content = getSystemEmailMsg("admin_apply_staff_send_admin_content");
			html = emailFactory.initEmail(leaveInfo, adminUserName, staffName, title, content);
			sendMail(emailAddress[i], adminUserName, title, html, true);
		}
		// 如果是管理员给员工请假,给员工发送请假邮件
		if (vo.isAdmin()) {
			String title = getSystemEmailMsg("adminforstaff_leave_email_title");
			String content = getSystemEmailMsg("adminforstaff_leave_email_content");
			String html = emailFactory.initEmailHtml(leaveInfo, staffName, title, content);
			sendMail(staff.getEmail(), staffName, title, html, true);
		}
		return successResult(getTipMsg("operation_success"), (Object) leaveInfo);
	}

	/**
	 * @description 判断日期是否大于当天
	 * @author hxzhang
	 * @create 2016年10月31日下午3:45:05
	 */
	private Date judgeDate(Date date) {
		Date now = new Date();
		if (DateUtil.getPreviousDay(date) >= DateUtil.getPreviousDay(now)) {
			return date;
		} else {
			return now;
		}
	}

	@Override
	public ServiceResult<Pager<LeaveListVo, LeaveCondition>> getLeaveList(LeaveCondition condition, UserInfoVo currentUser) {
		// 处理condition
		handleCondition(condition);
		// 园长只能查看本园区员工的请假
		if (isRole(currentUser, Role.CentreManager.getValue()) || isRole(currentUser, Role.EducatorSecondInCharge.getValue())) {
			condition.setSelectCenterId(currentUser.getUserInfo().getCentersId());
		}
		condition.setStartSize(condition.getPageIndex());
		condition.setSelectAccountId(currentUser.getAccountInfo().getId());
		List<LeaveListVo> leaveList = leaveInfoMapper.getLeaveList(condition);
		int totalSize = leaveInfoMapper.getLeaveListCount(condition);
		condition.setTotalSize(totalSize);
		Pager<LeaveListVo, LeaveCondition> pager = new Pager<LeaveListVo, LeaveCondition>(leaveList, condition);
		return successResult(pager);
	}

	@Override
	public ServiceResult<Object> saveOperaLeave(List<LeavePaidInfo> lpiList, String leaveId, short operaType, String reason, boolean confirm, UserInfoVo currentUser) {
		LeaveInfo dbLeave = leaveInfoMapper.selectByPrimaryKey(leaveId);
		UserInfo userInfo = userInfoMapper.getUserInfoByAccountId(dbLeave.getAccountId());
		// 验证员工请假时间段是否排班,有则提示
		if (operaType == OperaType.approve.getValue() && !confirm) {
			int count = rosterStaffInfoMapper.getCountByAccountIdAndDate(dbLeave.getAccountId(), dbLeave.getStartDate(), dbLeave.getEndDate());
			if (count != 0) {
				return successResult2(2, getTipMsg("leave_approve_prompt"));
			}
		}
		String staffName = userFactory.getUserName(userInfo);
		dbLeave.setOperaAccountId(currentUser.getAccountInfo().getId());
		dbLeave.setUpdateTime(new Date());
		if (operaType == OperaType.withdrawn.getValue() && dbLeave.getStatus() == LeaveStatus.requested.getValue()) { // withdrawn操作只能对requested状态下的请假进行
			dbLeave.setStatus(LeaveStatus.withdrawn.getValue());
		} else if (operaType == OperaType.approve.getValue()) {
			if (dbLeave.getStatus() == LeaveStatus.requested.getValue()) {
				// approve操作只能对requested状态下的请假进行
				dbLeave.setStatus(LeaveStatus.approved.getValue());
				// 更新请假时间
				saveLeaveDate(lpiList, leaveId);
				// TODO gfwang
				String title = getSystemEmailMsg("staff_leave_ceo_approve_title");
				String content = getSystemEmailMsg("staff_leave_ceo_approve_content");
				String html = emailFactory.initStaffLeaveChangeState(dbLeave, staffName, title, content, EmailType.staff_leave_ceo_approve);
				sendMail(userInfo.getEmail(), staffName, title, html, true);
			} else if (dbLeave.getStatus() == LeaveStatus.approved.getValue()) {
				// 更新请假时间
				saveLeaveDate(lpiList, leaveId);
			}
		} else if (operaType == OperaType.reject.getValue() && dbLeave.getStatus() == LeaveStatus.requested.getValue()) { // reject操作只能对requested状态下的请假进行
			dbLeave.setStatus(LeaveStatus.rejected.getValue());
			dbLeave.setReason(reason);
			// TODO gfwang
			String title = getSystemEmailMsg("staff_leave_ceo_rejected_title");
			String content = getSystemEmailMsg("staff_leave_ceo_rejected_content");
			String html = emailFactory.initStaffLeaveChangeState(dbLeave, staffName, title, content, EmailType.staff_leave_ceo_rejected);
			sendMail(userInfo.getEmail(), staffName, title, html, true);
		} else if (operaType == OperaType.cancel.getValue()
				&& (dbLeave.getStatus() == LeaveStatus.requested.getValue() || dbLeave.getStatus() == LeaveStatus.approved.getValue())) { // cancel操作对requested,approved状态下的请假进行
			dbLeave.setStatus(LeaveStatus.cancelled.getValue());
			dbLeave.setReason(reason);
			// TODO gfwang
			String title = getSystemEmailMsg("staff_leave_ceo_cancle_title");
			String content = getSystemEmailMsg("staff_leave_ceo_cancle_content");
			String html = emailFactory.initStaffLeaveChangeState(dbLeave, staffName, title, content, EmailType.staff_leave_ceo_cancle);
			sendMail(userInfo.getEmail(), userFactory.getUserName(userInfo), title, html, true);
		}
		leaveInfoMapper.updateByPrimaryKeySelective(dbLeave);
		return successResult(getTipMsg("operation_success"));
	}

	/**
	 * @description 初始化LeaveInfo
	 * @author hxzhang
	 * @create 2016年8月17日上午10:05:37
	 * @version 1.0
	 * @param accountId
	 *            请假人accontsId
	 * @param statrDate
	 *            开始时间
	 * @param endDate
	 *            结束时间
	 * @param type
	 *            请假类别
	 * @param isAdmin
	 *            是否为管理员
	 * @param currentUser
	 *            当前登录用户
	 * @return 返回LeaveId
	 * @throws ParseException
	 */
	private LeaveInfo initLeaveInfo(UserInfo staff, LeaveVo vo, UserInfoVo currentUser) {
		if (StringUtil.isEmpty(vo.getId())) {
			LeaveInfo leaveInfo = new LeaveInfo();
			if (vo.isAdmin()) { // 管理员提员工请假,请假条状态为approved
				leaveInfo.setAccountId(vo.getAccountId());
				String centerId = StringUtil.isEmpty(vo.getCenterId()) ? staff.getCentersId() : vo.getCenterId();
				leaveInfo.setCentreId(centerId);
				leaveInfo.setRoomId(staff.getRoomId());
				leaveInfo.setRoleType(accountInfoMapper.getRoleNameByAccountId(vo.getAccountId()));
				leaveInfo.setStatus(LeaveStatus.approved.getValue());
			} else { // 员工自己请假,请假条状态为requested
				leaveInfo.setAccountId(currentUser.getAccountInfo().getId());
				leaveInfo.setCentreId(StringUtil.isEmpty(currentUser.getUserInfo().getCentersId()) ? null : currentUser.getUserInfo().getCentersId());
				leaveInfo.setRoomId(currentUser.getUserInfo().getRoomId());
				leaveInfo.setRoleType(accountInfoMapper.getRoleNameByAccountId(currentUser.getAccountInfo().getId()));
				leaveInfo.setStatus(LeaveStatus.requested.getValue());
			}
			String leaveId = UUID.randomUUID().toString();
			leaveInfo.setId(leaveId);
			leaveInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
			leaveInfo.setOperaAccountId(currentUser.getAccountInfo().getId());
			leaveInfo.setLeaveType(vo.getType());
			leaveInfo.setStartDate(vo.getStartDate());
			leaveInfo.setEndDate(vo.getEndDate());
			Date now = new Date();
			leaveInfo.setCreateTime(now);
			leaveInfo.setUpdateTime(now);
			leaveInfo.setDeleteFlag(DeleteFlag.Default.getValue());
			leaveInfo.setLeaveReason(vo.getReason());
			leaveInfo.setLeaveColor(String.valueOf(vo.getType()));
			leaveInfoMapper.insertSelective(leaveInfo);
			return leaveInfo;
		} else {
			LeaveInfo leaveInfo = leaveInfoMapper.selectByPrimaryKey(vo.getId());
			leaveInfo.setLeaveType(vo.getType());
			leaveInfo.setStartDate(vo.getStartDate());
			leaveInfo.setEndDate(vo.getEndDate());
			leaveInfo.setStatus(LeaveStatus.approved.getValue());
			leaveInfo.setOperaAccountId(currentUser.getAccountInfo().getId());
			leaveInfo.setUpdateTime(new Date());
			leaveInfo.setLeaveReason(vo.getReason());
			leaveInfo.setLeaveColor(String.valueOf(vo.getType()));
			leaveInfoMapper.updateByPrimaryKeySelective(leaveInfo);
			return leaveInfo;
		}
	}

	/**
	 * @description 初始化LeavePaidInfo
	 * @author hxzhang
	 * @create 2016年8月17日上午11:05:40
	 * @version 1.0
	 * @param leaveId
	 *            Leave ID
	 * @param type
	 *            请假类型
	 * @param startDate
	 *            开始时间
	 * @param endDate
	 *            结束时间
	 * @param sdf
	 *            SimpleDateFormat
	 * @throws ParseException
	 *             异常
	 */
	private void initLeavePaidInfo(String leaveId, short type, Date fromDate, Date toDate) {
		List<LeavePaidInfo> lpiList = new ArrayList<LeavePaidInfo>();
		if (type == LeaveType.LongServiceLeave.getValue()) {
			Date customDate = fromDate;
			while (customDate.getTime() < toDate.getTime()) {
				LeavePaidInfo lpi = new LeavePaidInfo();
				lpi.setLeaveStartDate(customDate);
				customDate = DateUtil.addDay(customDate, 4);
				lpi.setLeaveEndDate(customDate);
				customDate = DateUtil.addDay(customDate, 3);
				lpi.setId(UUID.randomUUID().toString());
				lpi.setLeaveId(leaveId);
				lpi.setDeleteFlag(DeleteFlag.Default.getValue());
				lpiList.add(lpi);
			}
		} else {
			while (fromDate.getTime() <= toDate.getTime()) {
				LeavePaidInfo lpi = new LeavePaidInfo();
				// 判断日期是否是周六,周日
				if (!isWeekend(fromDate)) {
					lpi.setLeaveStartDate(fromDate);
					lpi.setId(UUID.randomUUID().toString());
					lpi.setLeaveId(leaveId);
					lpi.setDeleteFlag(DeleteFlag.Default.getValue());
					lpiList.add(lpi);
				}
				fromDate = DateUtil.addDay(fromDate, 1);

			}
		}
		if (null != lpiList && lpiList.size() != 0) {
			leavePaidInfoMapper.insertBatch(lpiList);
		}
	}

	@Override
	public ServiceResult<Object> getLeaveDate(String leaveId, UserInfoVo currentUser) {
		// TODO 验证
		List<LeavePaidInfo> lpiList = leavePaidInfoMapper.getLeavePaidInfoList(leaveId);
		return successResult((Object) lpiList);
	}

	/**
	 * @description 启用线程发送邮件
	 * @author hxzhang
	 * @create 2016年8月2日上午10:30:52
	 * @version 1.0
	 * @param sendAddress
	 *            发送者地址
	 * @param sendName
	 *            发送者姓名
	 * @param receiverAddress
	 *            接受者地址
	 * @param receiverName
	 *            接受者姓名
	 * @param sub
	 *            主题
	 * @param msg
	 *            内容
	 * @param isHtml
	 *            是否html
	 * @param emial_key
	 *            aws邮件key
	 */
	private void sendMail(final String receiverAddress, final String receiverName, final String sub, final String msg, final boolean isHtml) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					AwsEmailUtil a = new AwsEmailUtil();
					a.sendgrid(getSystemMsg("mailHostAccount"), getSystemMsg("email_name"), receiverAddress, receiverName, sub, msg, isHtml, null, null,
							getSystemMsg("email_key"));
				} catch (Exception e) {
					logger.error("发送邮件失败" + e.toString());
				}
			}
		}).start();
	}

	/**
	 * @description 判断当前登录人角色值
	 * @author hxzhang
	 * @create 2016年7月20日下午7:36:08
	 * @version 1.0
	 * @param currentUser
	 *            当前登录人
	 * @param role
	 *            角色值
	 * @return 返回结果
	 */
	private boolean isRole(UserInfoVo currentUser, Short role) {
		List<RoleInfoVo> roleList = currentUser.getRoleInfoVoList();
		for (RoleInfoVo roleInfoVo : roleList) {
			if (role.compareTo(roleInfoVo.getValue()) == 0) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @description 保存请假时间
	 * @author hxzhang
	 * @create 2016年8月18日上午8:36:18
	 * @version 1.0
	 * @param lpiList
	 *            List<LeavePaidInfo>
	 * @param leaveId
	 *            leaveId
	 */
	private void saveLeaveDate(List<LeavePaidInfo> lpiList, String leaveId) {
		if (null == lpiList || lpiList.size() == 0) {
			return;
		}
		// 删除之前记录
		leavePaidInfoMapper.removeLeavePaidInfoByLeaveId(leaveId);
		// 重新插入数据
		leavePaidInfoMapper.insertBatch(lpiList);
	}

	@Autowired
	private UserFactory userFactory;

	/**
	 * @description 新增数据验证
	 * @author hxzhang
	 * @create 2016年8月18日下午2:00:36
	 * @version 1.0
	 * @param startDate
	 *            开始时间
	 * @param endDate
	 *            结束时间
	 * @param type
	 *            请假类型
	 * @param isAdmin
	 *            isAdmin
	 * @return 返回结果
	 * @throws ParseException
	 */
	ServiceResult<Object> validateSaveLeave(LeaveVo vo, UserInfoVo currentUser) {
		// 请假类型不能为空
		if (vo.getType() == null) {
			return failResult(getTipMsg("Leave_type_empty"));
		}
		// 请假时间不能为空
		if (vo.getStartDate() == null) {
			if (isTimeLeaveType(vo.getType())) {
				return failResult(getTipMsg("leave_for_these_dates_empty"));
			}
			return failResult(getTipMsg("Leave_time_start_empty"));
		}
		if (vo.getEndDate() == null) {
			if (isTimeLeaveType(vo.getType())) {
				return failResult(getTipMsg("leave_for_these_dates_empty"));
			}
			return failResult(getTipMsg("Leave_time_end_empty"));
		}
		// 1.请假结束时间必须大于开始时间
		if (vo.getStartDate().getTime() > vo.getEndDate().getTime()) {
			if (isTimeLeaveType(vo.getType())) {
				return failResult(getTipMsg("leave_for_these_dates_error"));
			}
			return failResult(getTipMsg("Leave_time"));
		}
		// 2.员工自己请假请假开始日期要不小于当天
		if (!vo.isAdmin() && DateUtil.getPreviousDay(vo.getStartDate()) < DateUtil.getPreviousDay(new Date())) {
			if (isTimeLeaveType(vo.getType())) {
				return failResult(getTipMsg("leave_for_these_dates_staff"));
			}
			return failResult(getTipMsg("leave_staff"));
		}
		// 3.LongServiceLeave开始日期必须是周一,结束日期必须是周五
		if (vo.getType() == LeaveType.LongServiceLeave.getValue()) {
			if (!isMonday(vo.getStartDate())) {
				if (isTimeLeaveType(vo.getType())) {
					return failResult(getTipMsg("leave_choose_start_date"));
				}
				return failResult(getTipMsg("leave_choose_from_date"));
			}
			if (!isFriday(vo.getEndDate())) {
				if (isTimeLeaveType(vo.getType())) {
					return failResult(getTipMsg("leave_choose_end_date"));
				}
				return failResult(getTipMsg("leave_choose_to_date"));
			}
		}
		// 4.非LongServiceLeave,请假日期必须是工作日
		if (vo.getType() != LeaveType.LongServiceLeave.getValue()) {
			long i = DateUtil.getDaysBetween(vo.getStartDate(), vo.getEndDate());
			if (i <= 1 && isWeekend(vo.getStartDate()) && isWeekend(vo.getEndDate())) {
				return failResult(getTipMsg("leave_choose"));
			}
		}
		// 5.请假区间不能重复
		if (leaveDateRepeat(vo.getId(), vo.getAccountId(), vo.getStartDate(), vo.getEndDate(), vo.getType())) {
			return failResult(getTipMsg("leave_request_dateRange"));
		}
		// 请假原因必填
		if (StringUtil.isEmpty(vo.getReason())) {
			return failResult(getTipMsg("Leave_reason"));
		}
		// 请假在Employment End Date之后
		if (checkEmploymentEndDate(vo)) {
			return failResult(22, getTipMsg("leave_employment_end_date"));
		}

		return successResult();
	}

	private boolean checkEmploymentEndDate(LeaveVo vo) {
		StaffEmploymentInfo info = staffEmploymentInfoMapper.getCurrentEmploy(vo.getAccountId());
		if (info == null) {
			return false;
		}
		if (info.getLastDay() == null) {
			return false;
		}
		if (!vo.getEndDate().before(info.getLastDay())) {
			return true;
		}
		return false;
	}

	private boolean isTimeLeaveType(short type) {
		if (type == LeaveType.ChangeOfRDO.getValue() || type == LeaveType.ChangeOfShift.getValue() || type == LeaveType.RDO.getValue()
				|| type == LeaveType.ParentLeave.getValue() || type == LeaveType.LongServiceLeave.getValue()) {
			return false;
		}
		return true;
	}

	/**
	 * 判断时间是否交集
	 * 
	 * @description
	 * @author gfwang
	 * @create 2016年10月8日下午5:30:42
	 * @version 1.0
	 * @param accountId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	private boolean leaveDateRepeat(String id, String accountId, Date startDate, Date endDate, Short type) {
		if (type == LeaveType.ChangeOfRDO.getValue() || type == LeaveType.ChangeOfShift.getValue()) {
			return false;
		}
		int count = leaveInfoMapper.getDateRepeatCount(id, accountId, startDate, endDate);
		if (validateLeaveType(type)) {
			count = leaveInfoMapper.getTimeRepeatCount(id, accountId, startDate, endDate);
		}
		return count > 0 ? true : false;
	}

	private boolean validateLeaveType(Short type) {
		boolean flag = true;
		switch (type) {
		case 3:
		case 2:
		case 7:
		case 8:
		case 9:
			flag = false;
			break;
		}
		return flag;
	}

	/**
	 * @description 判断是否是星期一
	 * @author hxzhang
	 * @create 2016年8月18日下午3:17:34
	 * @version 1.0
	 * @param date
	 *            Date
	 * @return 返回结果
	 */
	private boolean isMonday(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		if (cal.get(Calendar.DAY_OF_WEEK) == 2) {
			return true;
		}
		return false;
	}

	/**
	 * @description 判断是否是星期五
	 * @author hxzhang
	 * @create 2016年8月18日下午3:18:13
	 * @version 1.0
	 * @param date
	 *            Date
	 * @return 返回结果
	 */
	private boolean isFriday(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		if (cal.get(Calendar.DAY_OF_WEEK) == 6) {
			return true;
		}
		return false;
	}

	/**
	 * @description 判断是否是周末
	 * @author hxzhang
	 * @create 2016年8月22日下午4:21:38
	 * @version 1.0
	 * @param date
	 *            Date
	 * @return 返回结果
	 */
	private boolean isWeekend(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		if (cal.get(Calendar.DAY_OF_WEEK) == 7 || cal.get(Calendar.DAY_OF_WEEK) == 1) {
			return true;
		}
		return false;
	}

	@Override
	public int updateLeaveTimedTask(Date now) {
		logger.info("updateLeaveTimedTask==>start");
		int count = 0;
		List<LeaveListVo> userInfoList = userInfoMapper.getOverdueLeaveInfo(now);
		for (LeaveListVo leaveInfo : userInfoList) {
			count += leaveInfoMapper.updateOverdueLeaveInfo(leaveInfo.getId());
			logger.info("updateLeaveTimedTask==>sendEmail==>start,leaveId=" + leaveInfo.getId());
			String title = getSystemEmailMsg("staff_leave_lapsed_title");
			String content = getSystemEmailMsg("staff_leave_lapsed_content");
			String name = leaveInfo.getUserName();
			String beginDate = DateUtil.format(leaveInfo.getStartDate(), DateFormatterConstants.SYS_FORMAT2);
			String endDate = DateUtil.format(leaveInfo.getEndDate(), DateFormatterConstants.SYS_FORMAT2);
			String html = EmailUtil.replaceHtml(title, MessageFormat.format(content, name, beginDate + "-" + endDate));
			MsgEmailInfo msgInfo = MsgEmailInfo.getEmailMsgInstance(getSystemMsg("mailHostAccount"), getSystemMsg("email_name"), leaveInfo.getEmail(), name, title, null,
					now, MsgEmailType.staff_leave_lapsed.getValue(), html);
			msgEmailInfoMapper.insert(msgInfo);
			logger.info("updateLeaveTimedTask==>sendEmail==>end");
		}
		logger.info("updateLeaveTimedTask==>end==>count:" + count);
		return count;
	}

	/**
	 * @description 处理LeaveCondition
	 * @author hxzhang
	 * @create 2016年8月19日上午8:55:29
	 * @version 1.0
	 * @param condition
	 *            LeaveCondition
	 */
	private void handleCondition(LeaveCondition condition) {
		if (StringUtil.isNotEmpty(condition.getLeaveType())) {
			List<Short> leaveTypeList = new ArrayList<Short>();
			for (LeaveType leaveType : LeaveType.values()) {
				String keyWord = StringUtil.isEmpty(condition.getLeaveType()) ? "" : condition.getLeaveType();
				if (leaveType.getDesc().toLowerCase().equals(keyWord.toLowerCase().trim())) {
					leaveTypeList.add(leaveType.getValue());
				}
			}
			if (ListUtil.isEmpty(leaveTypeList)) {
				leaveTypeList.add((short) -1);
			}
			condition.setSelectLeaveType(leaveTypeList);
		} else {
			condition.setSelectLeaveType(null);
		}
		if (StringUtil.isNotEmpty(condition.getStatus())) {
			List<Short> leaveStatusList = new ArrayList<Short>();
			for (LeaveStatus leaveStatus : LeaveStatus.values()) {
				if (leaveStatus.getDesc().toLowerCase().contains(condition.getStatus().toLowerCase())) {
					leaveStatusList.add(leaveStatus.getValue());
				}
			}
			if (null == leaveStatusList || leaveStatusList.size() == 0) {
				leaveStatusList.add((short) -1);
			}
			condition.setSelectStatus(leaveStatusList);
		} else {
			condition.setSelectStatus(null);
		}
	}

	@Override
	public void cancelRequestLeave(String userId) {
		UserInfo userInfo = userInfoMapper.selectByPrimaryKey(userId);
		AccountInfo accountInfo = accountInfoMapper.getAccountInfoByUserId(userId);
		// 取消待审批的请假记录
		int count = leaveInfoMapper.cancelLeaveByAccountId(accountInfo.getId(), getSystemMsg("cancel_reason"));
		// 发送邮件
		if (count != 0) {
			sendMail(userInfo.getEmail(), userFactory.getUserName(userInfo), getSystemEmailMsg("cancel_leave_email_content_title"),
					getSystemEmailMsg("cancel_leave_email_content"), true);
		}
	}

	public static void main(String[] args) {
		for (LeaveType leaveType : LeaveType.values()) {
			System.err.println(leaveType.getValue() + "=====" + leaveType.getDesc() + "====" + LeaveType.PersonalLeave.getDesc());

		}
	}

	@Override
	public int updateRequestState(String leaveId) {
		LeaveInfo leave = leaveInfoMapper.selectByPrimaryKey(leaveId);
		leave.setStatus(LeaveStatus.requested.getValue());
		return updateByPrimaryKey(leave);
	}
}
