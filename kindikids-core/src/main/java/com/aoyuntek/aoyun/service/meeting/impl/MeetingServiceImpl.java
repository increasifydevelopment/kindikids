package com.aoyuntek.aoyun.service.meeting.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.condtion.MeetingCondition;
import com.aoyuntek.aoyun.dao.meeting.MeetingInfoMapper;
import com.aoyuntek.aoyun.dao.meeting.MeetingRowInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskInstanceInfoMapper;
import com.aoyuntek.aoyun.entity.po.meeting.MeetingColumnsInfo;
import com.aoyuntek.aoyun.entity.po.meeting.MeetingInfo;
import com.aoyuntek.aoyun.entity.po.meeting.MeetingRowInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskInstanceInfo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.meeting.MeetingInfoVo;
import com.aoyuntek.aoyun.entity.vo.meeting.MeetingTempVo;
import com.aoyuntek.aoyun.entity.vo.meeting.TableRowVo;
import com.aoyuntek.aoyun.entity.vo.meeting.TableVo;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.ProgramState;
import com.aoyuntek.aoyun.enums.TaskFrequenceRepeatType;
import com.aoyuntek.aoyun.enums.TaskType;
import com.aoyuntek.aoyun.enums.meeting.MeetingMinutes;
import com.aoyuntek.aoyun.enums.meeting.MeetingTempState;
import com.aoyuntek.aoyun.service.impl.BaseWebServiceImpl;
import com.aoyuntek.aoyun.service.meeting.IMeetingService;
import com.aoyuntek.aoyun.service.meeting.IMeetingTempService;
import com.aoyuntek.aoyun.service.task.ITaskInstance;
import com.aoyuntek.framework.model.Pager;
import com.theone.common.util.ServiceResult;
import com.theone.date.util.DateUtil;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

/**
 * @description 会议模版业务逻辑层实现
 * @author gfwang 2016年10月13日上午10:40:36
 */
@Service
public class MeetingServiceImpl extends BaseWebServiceImpl<MeetingInfo, MeetingInfoMapper> implements IMeetingService {

    @Autowired
    private MeetingInfoMapper meetingInfoMapper;
    @Autowired
    private MeetingRowInfoMapper meetingRowInfoMapper;
    @Autowired
    private ITaskInstance taskInstance;
    @Autowired
    private IMeetingTempService meetingTempService;
    @Autowired
    private TaskInstanceInfoMapper taskInstanceInfoMapper;

    @Override
    public boolean validateMeetingName(String id, String name) {
        int count = meetingInfoMapper.getCountByName(id, name);
        return count > 0 ? false : true;
    }

    /**
     * 新增meetingInfo
     */
    @Override
    public ServiceResult<Object> addMeetingInfo(MeetingInfoVo meetingInfoVo, UserInfoVo currentUser) {
        log.info("addMeetingInfo|start");
        // meetingName不能为空
        if (StringUtil.isEmpty(meetingInfoVo.getMeetingInfo().getMeetingName())) {
            return failResult(getTipMsg("meeting_name_empty"));
        }
        // meetingName不能重复
        int count = meetingInfoMapper.getCountByName(meetingInfoVo.getMeetingInfo().getId(), meetingInfoVo.getMeetingInfo().getMeetingName());
        if (count > 0) {
            return failResult(getTipMsg("meeting_name_repeat"));
        }
        // meetingDate不能为空
        if (meetingInfoVo.getMeetingInfo().getDay() == null) {
            return failResult(getTipMsg("meeting_date_empty"));
        }
        // meetingTemplate不能为空
        if (StringUtil.isEmpty(meetingInfoVo.getMeetingInfo().getTemplateId())) {
            return failResult(getTipMsg("meeting_template_empty"));
        }
        // 初始化meetingInfo信息
        MeetingInfo meetingInfo = new MeetingInfo();
        meetingInfo.setId(UUID.randomUUID().toString());
        meetingInfo.setMeetingName(meetingInfoVo.getMeetingInfo().getMeetingName());
        meetingInfo.setTemplateId(meetingInfoVo.getMeetingInfo().getTemplateId());
        meetingInfo.setMeetingAgendaId(meetingInfoVo.getMeetingInfo().getMeetingAgendaId());
        Date day = meetingInfoVo.getMeetingInfo().getDay();
        meetingInfo.setDay(day);
        meetingInfo.setState(MeetingTempState.Enable.getValuse());
        meetingInfo.setCreateTime(setTime(day));
        meetingInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
        meetingInfo.setUpdateTime(setTime(day));
        meetingInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
        meetingInfo.setDeleteFlag(DeleteFlag.Default.getValue());
        // 插入meetingInfo
        int insert = meetingInfoMapper.insert(meetingInfo);
        log.info("addMeetingInfo|end|insert=" + insert);
        return successResult(getTipMsg("operation_success"), (Object) meetingInfoVo);
    }

    /**
     * 
     * @description 判重(Action Item)
     * @author sjwang
     * @create 2016年10月31日上午10:51:00
     * @version 1.0
     * @param meetingInfoVo
     * @return
     */
    private boolean itemRepeat(MeetingInfoVo meetingInfoVo) {
        List<TableVo> table = meetingInfoVo.getTable();
        HashSet<String> all = new HashSet<String>();
        int i = 0;
        for (TableVo tableVo : table) {
            List<TableRowVo> rowList = tableVo.getRowList();
            for (TableRowVo tableRowVo : rowList) {
                if (null != tableRowVo.getColumnType() && tableRowVo.getColumnType() == 0) {
                    all.add(tableRowVo.getValue());
                    i++;
                }
            }
        }
        if (all.size() == i) {
            return false;
        }
        return true;

    }

    /**
     * @description 拼接日期时间
     * @author hxzhang
     * @create 2016年10月25日下午6:33:12
     */
    private Date setTime(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String str = new SimpleDateFormat("yyyy-MM-dd").format(date) + " " + new SimpleDateFormat("HH:mm:ss").format(new Date());
        try {
            date = sdf.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    @Override
    public ServiceResult<Object> getMeetingManaList(MeetingCondition condition, UserInfoVo currentUser) {
        condition.setStartSize(condition.getPageIndex());
        List<MeetingInfo> meetingList = meetingInfoMapper.getMeetingManaList(condition);
        int totalSize = meetingInfoMapper.getMeetingManaCount(condition);
        condition.setTotalSize(totalSize);
        Pager<MeetingInfo, MeetingCondition> pager = new Pager<MeetingInfo, MeetingCondition>(meetingList, condition);
        return successResult((Object) pager);
    }

    @Override
    public ServiceResult<Object> getMeetingManaInfo(String id, String tempId, UserInfoVo currentUser) {
        MeetingInfoVo meetingInfoVo = new MeetingInfoVo();
        // 获取会议信息
        MeetingInfo meetingInfo = meetingInfoMapper.selectByPrimaryKey(id);
        // 获取会议模版信息
        MeetingInfoVo nqsAndCol = meetingInfoMapper.getNqsAndCol(tempId);
        // 获取会议表格信息
        MeetingInfoVo table = meetingInfoMapper.getMeetingTable(id);
        meetingInfoVo.setMeetingInfo(meetingInfo);
        meetingInfoVo.setColList(null == nqsAndCol ? null : nqsAndCol.getColList());
        meetingInfoVo.setNqsList(null == nqsAndCol ? null : nqsAndCol.getNqsList());
        meetingInfoVo.setTable(null == table ? null : table.getTable());

        // 判断是否为whenRequire gfwang
        if (meetingInfo != null) {
            String templateId = meetingInfo.getTemplateId();
            MeetingTempVo mtVo = (MeetingTempVo) meetingTempService.getMeetingTempInfo(templateId).getReturnObj();
            Short repeats = mtVo.getFrequency().getRepeats();
            if (repeats.equals(TaskFrequenceRepeatType.WhenRequired.getValue())) {
                meetingInfoVo.setWhenRequireFlag(true);
            } else {
                meetingInfoVo.setWhenRequireFlag(false);
            }
        }
        return successResult((Object) meetingInfoVo);
    }

    @Override
    public ServiceResult<Object> saveMeetingMana(MeetingInfoVo meetingInfoVo, UserInfoVo currentUser, boolean confirm) {
        ServiceResult<Object> result = validateMeetingMana(meetingInfoVo);
        if (!result.isSuccess()) {
            return result;
        }
        // 获取数据库中Table信息
        MeetingInfoVo dbTable = meetingInfoMapper.getMeetingTable(meetingInfoVo.getMeetingInfo().getId());
        List<TableVo> dbTableList = dbTable.getTable();
        // 无需提示该信息 hxzhang
        // 更新时时Task存在Pending的MeetingTask提示是否删除.
        ServiceResult<Object> resultData = handleExistMeetingTask(dbTableList, confirm);
        // if ((int) resultData.getCode() == 2) {
        // return resultData;
        // }
        // 更新会议信息
        MeetingInfo dbData = meetingInfoMapper.selectByPrimaryKey(meetingInfoVo.getMeetingInfo().getId());
        dbData.setUpdateTime(new Date());
        dbData.setCreateAccountId(currentUser.getAccountInfo().getId());
        dbData.setMeetingAgendaId(meetingInfoVo.getMeetingInfo().getMeetingAgendaId());
        meetingInfoMapper.updateByPrimaryKey(dbData);

        // 保存表格信息
        List<TableVo> tableList = meetingInfoVo.getTable();
        List<MeetingRowInfo> rowList = new ArrayList<MeetingRowInfo>();
        Date createTime = new Date();
        for (TableVo tv : tableList) {
            List<TableRowVo> trvList = tv.getRowList();
            String rowId = UUID.randomUUID().toString();
            tv.setRowId(rowId);
            initTableData(rowList, trvList, rowId, dbData, createTime, currentUser);
            createTime = new Date(createTime.getTime() + 1000);
            // 先判断,再创建OtherTask
            createOtherTask(tv, currentUser);
        }
        // 插入之前先删除数据
        meetingRowInfoMapper.deleteMeetingRowInfoByMeetingId(dbData.getId());
        // 批量插入数据
        if (ListUtil.isNotEmpty(rowList)) {
            meetingRowInfoMapper.insertBatchMeetingRowInfo(rowList);
        }
        return successResult(getTipMsg("meeting_save"));
    }

    /**
     * @description 处理已存在的会议Task
     * @author hxzhang
     * @create 2016年10月27日上午11:38:53
     */
    private ServiceResult<Object> handleExistMeetingTask(List<TableVo> tableList, boolean confirm) {
        confirm = true;
        if (!confirm) {
            String returnStr = "";
            boolean returnfalg = false;
            for (TableVo tv : tableList) {
                TaskInstanceInfo ti = taskInstanceInfoMapper.getTaskInstanceInfoByValueId(tv.getRowId());
                if (null == ti) {
                    continue;
                }
                if (ti.getStatu() == ProgramState.Pending.getValue()) {
                    returnStr += ti.getValueName() + ",";
                    returnfalg = true;
                }
            }
            if (returnfalg) {
                return successResult2(2, returnStr.substring(0, returnStr.length() - 1) + " " + getTipMsg("meeting_save_delete_meeting_task"));
            }
        }
        // 确认删除
        if (confirm) {
            for (TableVo tv : tableList) {
                TaskInstanceInfo ti = taskInstanceInfoMapper.getTaskInstanceInfoByValueId(tv.getRowId());
                if (null == ti) {
                    continue;
                }
                if (ti.getStatu() == ProgramState.Pending.getValue()) {
                    taskInstanceInfoMapper.deleteTaskInstanceByValueId(ti.getValueId());
                }
            }
        }
        return successResult();
    }

    /**
     * @description 初始化Table数据
     * @author hxzhang
     * @create 2016年10月24日下午3:39:00
     */
    private void initTableData(List<MeetingRowInfo> rowList, List<TableRowVo> trvList, String rowId, MeetingInfo dbData, Date createTime,
            UserInfoVo currentUser) {
        List<MeetingRowInfo> list = new ArrayList<MeetingRowInfo>();
        boolean allNull = true;
        for (TableRowVo trv : trvList) {
            MeetingRowInfo mri = new MeetingRowInfo();
            mri.setId(UUID.randomUUID().toString());
            mri.setRowId(rowId);
            mri.setMeetingId(dbData.getId());
            mri.setColumnId(trv.getColumnId());
            mri.setInputValue(trv.getValue());
            mri.setCreateTime(createTime);
            mri.setCreateAccountId(currentUser.getAccountInfo().getId());
            mri.setUpdateTime(createTime);
            mri.setDeleteFlag(DeleteFlag.Default.getValue());
            list.add(mri);
            if (StringUtil.isNotEmpty(trv.getValue())) {
                allNull = false;
            }
        }
        if (!allNull) {
            rowList.addAll(list);
        }
    }

    /**
     * @description 数据验证
     * @author hxzhang
     * @create 2016年10月24日上午10:11:51
     */
    private ServiceResult<Object> validateMeetingMana(MeetingInfoVo meetingInfoVo) {
        // item name 判空
        for (TableVo table : meetingInfoVo.getTable()) {
            for (TableRowVo tableRow : table.getRowList()) {
                if (null != tableRow.getColumnType() && tableRow.getColumnType() == 0 && StringUtil.isEmpty(tableRow.getValue())) {
                    return failResult(getTipMsg("meeting_save_item_required"));
                }
            }
        }
        // item name 重复判断
        boolean itemRepeat = itemRepeat(meetingInfoVo);
        if (itemRepeat) {
            return failResult(getTipMsg("meeting_save_item_repeat"));
        }
        // 1.table输入框字符长度验证
        List<MeetingColumnsInfo> colList = meetingInfoVo.getColList();
        List<TableVo> tvList = meetingInfoVo.getTable();
        for (TableVo tv : tvList) {
            List<TableRowVo> trvList = tv.getRowList();
            for (int i = 0; i < trvList.size(); i++) {
                TableRowVo trv = trvList.get(i);
                if (null != trv.getColumnType()
                        && (trv.getColumnType() == MeetingMinutes.Responsibility.getValues() || trv.getColumnType() == MeetingMinutes.Timeframe
                                .getValues())) {
                    continue;
                }
                if (StringUtil.isEmpty(trv.getValue())) {
                    continue;
                }
                if (trv.getValue().length() > 255) {
                    return failResult(colList.get(i).getColumnName() + " " + getTipMsg("meeting_save_row_value"));
                }
            }
        }
        // 2.Timeframe只能输入当天及未来时间
        for (TableVo tv : tvList) {
            List<TableRowVo> trvList = tv.getRowList();
            for (int i = 0; i < colList.size(); i++) {
                MeetingColumnsInfo c = colList.get(i);
                if (null == c.getColumnType() || c.getColumnType() != MeetingMinutes.Timeframe.getValues()) {
                    continue;
                }
                TableRowVo r = trvList.get(i);
                if (StringUtil.isEmpty(r.getValue())) {
                    continue;
                }
                Date date = DateUtil.parse(r.getValue(), "dd/MM/yyyy");
                if (DateUtil.getPreviousDay(date) < DateUtil.getPreviousDay(new Date())) {
                    return failResult(getTipMsg("meeting_save_row_value_date"));
                }
            }
        }
        return successResult();
    }

    /**
     * 删除meeting
     */
    @Override
    public ServiceResult<Object> updateDeleteByPrimaryKey(String meetingId) {
        meetingInfoMapper.updateDeleteByPrimaryKey(meetingId);
        return successResult(getTipMsg("operation_success"));
    }

    /**
     * @description 创建OtherTask
     * @author hxzhang
     * @create 2016年10月19日下午1:52:33
     */
    private void createOtherTask(TableVo tv, UserInfoVo currentUser) {
        List<TableRowVo> trvList = tv.getRowList();
        Date date = null;
        for (TableRowVo trv : trvList) {
            if (containType(trv.getColumnType())) {
                if (StringUtil.isEmpty(trv.getValue())) {
                    return;
                }
            }
            if (trv.getColumnType() != null && trv.getColumnType() == MeetingMinutes.Timeframe.getValues()) {
                date = DateUtil.parse(trv.getValue(), "dd/MM/yyyy");
            }
        }
        taskInstance.addTaskInstance(TaskType.MeetingTask, null, null, null, tv.getRowId(), currentUser.getAccountInfo().getId(), new Date(), date);
    }

    /**
     * @description 判断是否是ActionItem,Responsibility,Timeframe
     * @author hxzhang
     * @create 2016年10月19日下午2:34:48
     */
    private boolean containType(Short type) {
        if (type == null) {
            return false;
        }
        if (type == MeetingMinutes.ActionItem.getValues()) {
            return true;
        }
        if (type == MeetingMinutes.Responsibility.getValues()) {
            return true;
        }
        if (type == MeetingMinutes.Timeframe.getValues()) {
            return true;
        }
        return false;
    }

    @Override
    public ServiceResult<Object> getRowInfo(String id) {
        return successResult((Object) meetingRowInfoMapper.getRowInfoByRowId(id));
    }

    @Override
    public String getLiabler(String id) {
        return meetingRowInfoMapper.getLiablerByRowId(id);
    }
}
