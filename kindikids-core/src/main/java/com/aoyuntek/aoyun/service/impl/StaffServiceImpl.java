package com.aoyuntek.aoyun.service.impl;

import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.aoyuntek.aoyun.condtion.StaffCondition;
import com.aoyuntek.aoyun.dao.AccountInfoMapper;
import com.aoyuntek.aoyun.dao.AccountRoleInfoMapper;
import com.aoyuntek.aoyun.dao.AttachmentInfoMapper;
import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.CentreManagerRelationInfoMapper;
import com.aoyuntek.aoyun.dao.ChangePswRecordInfoMapper;
import com.aoyuntek.aoyun.dao.CredentialsInfoMapper;
import com.aoyuntek.aoyun.dao.EmergencyContactInfoMapper;
import com.aoyuntek.aoyun.dao.GroupManagerRelationInfoMapper;
import com.aoyuntek.aoyun.dao.LeaveInfoMapper;
import com.aoyuntek.aoyun.dao.LogsDetaileInfoMapper;
import com.aoyuntek.aoyun.dao.MedicalImmunisationInfoMapper;
import com.aoyuntek.aoyun.dao.MedicalInfoMapper;
import com.aoyuntek.aoyun.dao.MsgEmailInfoMapper;
import com.aoyuntek.aoyun.dao.NewsAccountsInfoMapper;
import com.aoyuntek.aoyun.dao.RoleInfoMapper;
import com.aoyuntek.aoyun.dao.RoomGroupInfoMapper;
import com.aoyuntek.aoyun.dao.RoomInfoMapper;
import com.aoyuntek.aoyun.dao.SigninInfoMapper;
import com.aoyuntek.aoyun.dao.SourceLogsInfoMapper;
import com.aoyuntek.aoyun.dao.StaffEmploymentInfoMapper;
import com.aoyuntek.aoyun.dao.TaggingsInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.dao.UserStaffMapper;
import com.aoyuntek.aoyun.dao.roster.RosterStaffInfoMapper;
import com.aoyuntek.aoyun.entity.po.AccountInfo;
import com.aoyuntek.aoyun.entity.po.AccountRoleInfo;
import com.aoyuntek.aoyun.entity.po.AttachmentInfo;
import com.aoyuntek.aoyun.entity.po.CentersInfo;
import com.aoyuntek.aoyun.entity.po.CentreManagerRelationInfo;
import com.aoyuntek.aoyun.entity.po.ChangePswRecordInfo;
import com.aoyuntek.aoyun.entity.po.CredentialsInfo;
import com.aoyuntek.aoyun.entity.po.EmergencyContactInfo;
import com.aoyuntek.aoyun.entity.po.GroupManagerRelationInfo;
import com.aoyuntek.aoyun.entity.po.LogsDetaileInfo;
import com.aoyuntek.aoyun.entity.po.MedicalImmunisationInfo;
import com.aoyuntek.aoyun.entity.po.MedicalInfo;
import com.aoyuntek.aoyun.entity.po.MsgEmailInfo;
import com.aoyuntek.aoyun.entity.po.RoleInfo;
import com.aoyuntek.aoyun.entity.po.RoomInfo;
import com.aoyuntek.aoyun.entity.po.SourceLogsInfo;
import com.aoyuntek.aoyun.entity.po.StaffEmploymentInfo;
import com.aoyuntek.aoyun.entity.po.TaggingsInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.po.roster.RosterStaffInfo;
import com.aoyuntek.aoyun.entity.vo.EmailVo;
import com.aoyuntek.aoyun.entity.vo.FileVo;
import com.aoyuntek.aoyun.entity.vo.RoleInfoVo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.aoyun.entity.vo.StaffInfoVo;
import com.aoyuntek.aoyun.entity.vo.StaffListVo;
import com.aoyuntek.aoyun.entity.vo.StaffRoleInfoVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.ValueInfoVO;
import com.aoyuntek.aoyun.enums.AccountActiveStatus;
import com.aoyuntek.aoyun.enums.ArchivedStatus;
import com.aoyuntek.aoyun.enums.Credential;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.EmployStatus;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.enums.SendEmailType;
import com.aoyuntek.aoyun.enums.StaffType;
import com.aoyuntek.aoyun.enums.TokenStatus;
import com.aoyuntek.aoyun.enums.UserType;
import com.aoyuntek.aoyun.factory.UserFactory;
import com.aoyuntek.aoyun.service.ICenterService;
import com.aoyuntek.aoyun.service.ICommonService;
import com.aoyuntek.aoyun.service.IFormInfoService;
import com.aoyuntek.aoyun.service.IStaffService;
import com.aoyuntek.aoyun.service.attendance.ILeaveService;
import com.aoyuntek.aoyun.service.roster.IRosterService;
import com.aoyuntek.aoyun.uitl.EmailUtil;
import com.aoyuntek.aoyun.uitl.FilePathUtil;
import com.aoyuntek.aoyun.uitl.ObjectCompUtil;
import com.aoyuntek.aoyun.uitl.SqlUtil;
import com.aoyuntek.framework.constants.PropertieNameConts;
import com.aoyuntek.framework.model.Pager;
import com.theone.aws.util.AwsEmailUtil;
import com.theone.aws.util.FileFactory;
import com.theone.common.util.ServiceResult;
import com.theone.date.util.DateUtil;
import com.theone.json.util.JsonUtil;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

/**
 * 员工服务类
 * 
 * @author rick
 * 
 */
@Service
public class StaffServiceImpl extends BaseWebServiceImpl<StaffListVo, UserInfoMapper> implements IStaffService {

	public static Logger logger = Logger.getLogger(StaffServiceImpl.class);

	@Autowired
	private UserInfoMapper userInfoMapper;
	@Autowired
	private AccountInfoMapper accountInfoMapper;
	@Autowired
	private AccountRoleInfoMapper accountRoleInfoMapper;
	@Autowired
	private RoleInfoMapper roleInfoMapper;
	@Autowired
	private UserStaffMapper staffMapper;
	@Autowired
	private EmergencyContactInfoMapper contactInfoMapper;
	@Autowired
	private MedicalInfoMapper medicalInfoMapper;
	@Autowired
	private MedicalImmunisationInfoMapper immunisationInfoMapper;
	@Autowired
	private CredentialsInfoMapper credentialsInfoMapper;
	@Autowired
	private AttachmentInfoMapper attachmentInfoMapper;
	@Autowired
	private ChangePswRecordInfoMapper changePswRecordInfoMapper;
	@Autowired
	private ILeaveService leaveService;
	@Autowired
	private CentersInfoMapper centersInfoMapper;
	@Autowired
	private RoomGroupInfoMapper groupInfoMapper;
	@Autowired
	private ICenterService centerService;
	@Autowired
	private RoomInfoMapper roomInfoMapper;
	@Autowired
	private ICommonService commonService;
	@Autowired
	private SigninInfoMapper signinInfoMapper;
	@Autowired
	private IFormInfoService formService;
	@Autowired
	private RosterStaffInfoMapper rosterStaffInfoMapper;
	@Autowired
	private LogsDetaileInfoMapper logsDetaileInfoMapper;
	@Autowired
	private SourceLogsInfoMapper sourceLogsInfoMapper;
	@Autowired
	private MsgEmailInfoMapper msgEmailInfoMapper;
	@Autowired
	private UserFactory userFactory;
	@Autowired
	private NewsAccountsInfoMapper newsAccountsInfoMapper;
	@Autowired
	private StaffEmploymentInfoMapper staffEmploymentInfoMapper;
	@Autowired
	private TaggingsInfoMapper taggingsInfoMapper;
	@Autowired
	private IRosterService rosterService;
	@Autowired
	private CentreManagerRelationInfoMapper centreManagerRelationInfoMapper;
	@Autowired
	private GroupManagerRelationInfoMapper groupManagerRelationInfoMapper;
	@Autowired
	private LeaveInfoMapper leaveInfoMapper;

	/**
	 * 分页显示员工
	 * 
	 * @author rick
	 * @date 2016年7月28日 上午11:16:57
	 * @param condition
	 * @return
	 */
	@Override
	public ServiceResult<Pager<StaffListVo, StaffCondition>> getPagerStaffs(StaffCondition condition) {
		logger.info("===================================getPagerStaffs begin====================================");
		List<StaffListVo> list = null;
		// 设置条件
		condition.setStartSize(condition.getPageIndex());
		String keyWord = SqlUtil.likeEscape(condition.getParams());
		condition.setParams(keyWord);
		// 查询列表

		if (getSystemMsg("edensorPark").equals(condition.getCenterId())) {
			condition.setCenterId(null);
			condition.setCentreIds(dealEdensorPark());
		} else {
			condition.setCentreIds(null);
		}

		list = staffMapper.getStaffPagerList(condition);

		int totalSize = staffMapper.getStaffPagerListCount(condition);
		condition.setTotalSize(totalSize);
		// 分页类包装
		Pager<StaffListVo, StaffCondition> pager = new Pager<StaffListVo, StaffCondition>(list, condition);
		condition.setParams(keyWord);

		logger.info("===================================getPagerStaffs begin====================================");
		return successResult(pager);
	}

	private List<String> dealEdensorPark() {
		List<String> edensorParkIds = new ArrayList<String>();
		List<CentersInfo> list = centersInfoMapper.getAllCentersInfos();
		for (CentersInfo c : list) {
			if (getSystemMsg("rydeId").equals(c.getId())) {
				continue;
			}
			edensorParkIds.add(c.getId());
		}
		return edensorParkIds;
	}

	/**
	 * 显示员工详细信息
	 * 
	 * @author rick
	 * @date 2016年7月28日 上午11:16:51
	 * @param userId
	 * @return
	 */
	@Override
	public ServiceResult<StaffInfoVo> saveGetStaffInfo(String userId) {
		logger.info("===================================getStaffInfo begin====================================");
		// 员工信息
		UserInfo userInfo = staffMapper.selectUserById(userId);
		// 账户信息
		AccountInfo accountInfo = staffMapper.selectAccountByUserId(userId);
		accountInfo.setPsw(null);
		// 角色信息
		List<AccountRoleInfo> roleInfoList = staffMapper.selectAccountRoleByAccountId(accountInfo.getId());
		// 紧急联系人
		List<EmergencyContactInfo> emergencyList = contactInfoMapper.selectByUserId(userId);
		// 医疗信息
		MedicalInfo medicalInfo = medicalInfoMapper.selectByUserId(userId);
		// 疫苗接种信息
		List<MedicalImmunisationInfo> immunisationInfoList = immunisationInfoMapper.selectByUserId(userId);
		// 资格证书信息
		List<CredentialsInfo> credentialsInfoList = credentialsInfoMapper.selectByUserId(userId);
		// 当前Employment信息
		StaffEmploymentInfo staffEmploymentInfo = staffEmploymentInfoMapper.getCurrentEmploy(accountInfo.getId());
		// 获取Employment申请记录
		List<StaffEmploymentInfo> employInfoList = staffEmploymentInfoMapper.getList(accountInfo.getId());

		StaffInfoVo staffInfoVo = new StaffInfoVo();
		staffInfoVo.setUserInfo(userInfo);
		staffInfoVo.setAccountInfo(accountInfo);
		staffInfoVo.setRoleInfo(roleInfoList);
		staffInfoVo.setContactInfo(emergencyList);
		staffInfoVo.setMedicalInfo(medicalInfo);
		staffInfoVo.setMedicalImmunisationInfo(immunisationInfoList);
		staffInfoVo.setCredentialsInfo(credentialsInfoList);
		staffInfoVo.setEmployInfo(staffEmploymentInfo);
		staffInfoVo.setEmployInfoList(employInfoList);

		logger.info("===================================getStaffInfo begin====================================");
		return successResult(staffInfoVo);
	}

	/**
	 * 验证Staff Employ信息是否存在,不存在立刻创建
	 * 
	 * @author hxzhang 2018年11月20日
	 */
	private StaffEmploymentInfo dealStaffEmploy(StaffEmploymentInfo info, UserInfo userInfo, AccountInfo accountInfo) {
		if (info != null) {
			return info;
		}

		String startTime = getSystemMsg("staff_employ_startTime");
		String endTime = getSystemMsg("staff_employ_endTime");
		BigDecimal hours = getStaffHours(userInfo);

		info = new StaffEmploymentInfo();
		info.setId(UUID.randomUUID().toString());
		info.setAccountId(accountInfo.getId());

		info.setMonday(false);
		info.setMonStartTime(startTime);
		info.setMonEndTime(endTime);
		info.setMonHours(hours);

		info.setTuesday(false);
		info.setTueStartTime(startTime);
		info.setTueEndTime(endTime);
		info.setTueHours(hours);

		info.setWednesday(false);
		info.setWedStartTime(startTime);
		info.setWedEndTime(endTime);
		info.setWedHours(hours);

		info.setThursday(false);
		info.setThuStartTime(startTime);
		info.setThuEndTime(endTime);
		info.setThuHours(hours);

		info.setFriday(false);
		info.setFriStartTime(startTime);
		info.setFriEndTime(endTime);
		info.setFriHours(hours);

		info.setCurrent(true);
		info.setDeleteFlag(false);

		staffEmploymentInfoMapper.insertSelective(info);

		return info;
	}

	private BigDecimal getStaffHours(UserInfo userInfo) {
		if (userInfo.getStaffType() == null) {
			return BigDecimal.valueOf(Double.valueOf(getSystemMsg("staff_employ_hours")));
		}
		if (StaffType.FullTime.getValue() == userInfo.getStaffType()) {
			return BigDecimal.valueOf(Double.valueOf(getSystemMsg("staff_employ_hours")));
		}
		return BigDecimal.valueOf(0);
	}

	/**
	 * 检查当前登录用户是否有添加某角色员工的权限
	 * 
	 * @author rick
	 * @date 2016年8月11日 上午11:12:12
	 * @param currentUser
	 *            当前用户
	 * @param accountRoleInfos
	 *            新增员工的roleList
	 * @param roleInfos
	 *            所有RoleList
	 * @return
	 */
	private boolean staffEdit(UserInfoVo currentUser, List<AccountRoleInfo> accountRoleInfos, List<RoleInfo> roleInfos, String staffId) {
		List<RoleInfoVo> roleList = currentUser.getRoleInfoVoList();
		// 当前角色
		Short currRole = null;
		for (RoleInfoVo roleInfoVo : roleList) {
			// 如果是二级园长
			if (roleInfoVo.getValue() == Role.EducatorSecondInCharge.getValue()) {
				currRole = Role.CentreManager.getValue();
			}
			if (roleInfoVo.getRoleType() == 1) {
				if (currRole == null || (currRole < roleInfoVo.getValue())) {
					currRole = roleInfoVo.getValue();
				}
			}
		}

		if (currRole == 0) { // CEO
			return true;
		}

		if (currRole == 0) { // CEO
			return true;
		}

		// 员工角色
		Short staffRole = null;
		for (RoleInfo roleInfo : roleInfos) {
			for (AccountRoleInfo accountRoleInfo : accountRoleInfos) {
				if (roleInfo.getId().equals(accountRoleInfo.getRoleId()) && roleInfo.getRoleType() == 1) {
					staffRole = roleInfo.getValue();
				}
			}
		}
		// 新增
		if (StringUtil.isBlank(staffId)) {
			// 园长
			if (currRole == 1 || currRole == 5) {
				return true;
			}
		} else {
			if (currentUser.getUserInfo().getId().equals(staffId)) {
				return true;
			}
		}
		// 如果登录用户roleValue小于新增员工的,则表示可以编辑
		if (currRole < staffRole) {
			return true;
		}
		return false;
	}

	/**
	 * 更新/新增员工信息
	 * 
	 * @author rick
	 * @date 2016年7月28日 上午11:16:47
	 * @param staffInfoVo
	 * @param currentUser
	 *            当前登录用户
	 * @return
	 * @throws Exception
	 */
	@Override
	public ServiceResult<Object> updateStaffInfo(StaffInfoVo staffInfoVo, UserInfoVo currentUser, String ip) throws Exception {
		logger.info("===================================updateStaffInfo begin====================================");
		// 当此人为兼职+二级园长的时候，或者园长且因为园区归档导致没有园区的时候，提示无权限操作
		ServiceResult<Object> result_permissions = isCasualCentreManager(currentUser);
		if (!result_permissions.isSuccess()) {
			// 无权限新增员工
			if (StringUtil.isEmpty(staffInfoVo.getUserInfo().getId())) {
				return result_permissions;
			}
			// 无权限更新员工信息(可以更新自己)
			if (StringUtil.isNotEmpty(staffInfoVo.getUserInfo().getId()) && !staffInfoVo.getUserInfo().getId().equals(currentUser.getUserInfo().getId())) {
				return result_permissions;
			}
		}
		String valueId = "";
		// 如果stafftype是兼职，则更新Staff角色
		if (staffInfoVo.getUserInfo().getStaffType() != null && staffInfoVo.getUserInfo().getStaffType() == StaffType.Casual.getValue()) {
			// 是否需要判断角色中是否已经存在兼职
			String id = roleInfoMapper.selectRoleIdByValue(Role.Casual.getValue());
			// 如果存在兼职角色则进行删除
			for (int i = staffInfoVo.getRoleInfo().size() - 1; i >= 0; i--) {
				if (staffInfoVo.getRoleInfo().get(i).getRoleId().equals(id)) {
					staffInfoVo.getRoleInfo().remove(i);
				}
			}
			AccountRoleInfo accountRoleInfo = new AccountRoleInfo();
			accountRoleInfo.setRoleId(id);
			staffInfoVo.getRoleInfo().add(accountRoleInfo);
		} else {
			String id = roleInfoMapper.selectRoleIdByValue(Role.Casual.getValue());
			// 如果存在兼职角色则进行删除
			for (int i = staffInfoVo.getRoleInfo().size() - 1; i >= 0; i--) {
				if (staffInfoVo.getRoleInfo().get(i).getRoleId().equals(id)) {
					staffInfoVo.getRoleInfo().remove(i);
				}
			}
		}

		if (StringUtil.isNotEmpty(staffInfoVo.getJson())) {
			ValueInfoVO valueInfoVO = (ValueInfoVO) JsonUtil.jsonToBean(staffInfoVo.getJson(), ValueInfoVO.class);
			ServiceResult<String> formResult = formService.saveInstance(valueInfoVO);
			if (!formResult.isSuccess()) {
				return failResult(getTipMsg("operation_failed"));
			}
			valueId = formResult.getReturnObj();
		}
		UserInfo userInfo = staffInfoVo.getUserInfo();
		// 用户Id
		String userId = userInfo.getId();
		// accountId
		String accountId = staffInfoVo.getAccountInfo().getId();

		if (StringUtil.isEmpty(userId) && !containRoles(currentUser.getRoleInfoVoList(), Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge)) {
			return failResult(getTipMsg("no.permissions"));
		}

		// 权限控制
		/*
		 * List<AccountRoleInfo> accountRoleInfos = staffInfoVo.getRoleInfo();
		 * List<RoleInfo> roleInfos = staffMapper.selectRole(); if
		 * (!staffEdit(currentUser, accountRoleInfos, roleInfos, userId)) {
		 * return failResult(getTipMsg("no.permissions")); }
		 */

		// 附件列表
		List<FileVo> fileList = new ArrayList<FileVo>();

		// 出生证明附件
		List<FileVo> certFile = userInfo.getFileList();
		if (certFile.isEmpty()) {
			// 空附件设置随机的附件Id
			userInfo.setBirthCert(UUID.randomUUID().toString());
		} else {
			FileVo fileVo = certFile.get(0);
			if (StringUtil.isBlank(fileVo.getId())) {
				String certFileId = UUID.randomUUID().toString();
				userInfo.setBirthCert(certFileId);
				fileVo.setId(certFileId);
				// 添加到附件集合
				fileList.add(fileVo);
			}
		}

		SourceLogsInfo sourceLogsInfo = initSourceLogsInfo(ip, currentUser, userInfo.getId());

		// 是否为新增
		Boolean isAdd = false;
		if (StringUtil.isBlank(userId)) {
			isAdd = true;
			// 判断邮箱唯一
			int res = staffMapper.selectAccount(userInfo.getEmail(), null);
			if (res >= 1) {
				return failResult(getTipMsg("staff_email_registered"));
			}
			userId = UUID.randomUUID().toString();
			// 更新园长，和group负责人 gfwang
			ServiceResult<Object> result = updateCenterOrGroupManager(staffInfoVo.getRoleInfo(), userId, staffInfoVo);
			if (!result.isSuccess()) {
				return failResult(result.getMsg());
			}
			// 验证Employment Start Date,Employment Last Day
			if (staffInfoVo.getEmployInfo().getLastDay() != null && userInfo.getStaffInDate() != null
					&& !staffInfoVo.getEmployInfo().getLastDay().after(userInfo.getStaffInDate())) {
				return failResult(getTipMsg("staff_employ_endDay"));
			}
			// 初始化用户信息
			userInfo.setId(userId);
			userInfo.setUserType(UserType.Staff.getValue());
			userInfo.setDeleteFlag(DeleteFlag.Default.getValue());
			if (StringUtil.isNotEmpty(userInfo.getAvatar())) {
				try {
					String pathAvatar = FilePathUtil.getMainPath(FilePathUtil.AVATAR_STAFF, userInfo.getAvatar());
					new FileFactory(Region.getRegion(Regions.AP_SOUTHEAST_2), PropertieNameConts.S3).copyFile(userInfo.getAvatar(), pathAvatar);
					userInfo.setAvatar(pathAvatar);
				} catch (FileNotFoundException e) {
					log.info("updateStaffInfo|save|FileNotFoundException s3 error", e);
					e.printStackTrace();
				} catch (Exception e) {
					log.info("updateStaffInfo|save|s3 error", e);
					e.printStackTrace();
				}
			}
			// 获取personColor
			String personColor = commonService.getPersonColor(userInfo);
			log.info("updateStaffInfo|personColor=" + personColor);
			userInfo.setPersonColor(personColor);
			Date date = new Date();
			userInfo.setCreateTime(date);
			userInfo.setCreateAccountId(currentUser.getUserInfo().getId());
			userInfo.setUpdateTime(date);
			userInfo.setUpdateAccountId(currentUser.getUserInfo().getId());
			userInfo.setStaffInDate(userInfo.getStaffInDate());
			if (containRole(staffInfoVo.getRoleInfo(), Role.Casual.getValue())) {
				userInfo.setCentersId("");
			}
			userInfoMapper.insert(userInfo);

			if (StringUtil.isBlank(accountId)) {
				// 初始化账户信息
				AccountInfo accountInfo = staffInfoVo.getAccountInfo();
				accountId = UUID.randomUUID().toString();
				accountInfo.setId(accountId);
				accountInfo.setUserId(userId);
				accountInfo.setStatus(AccountActiveStatus.Enabled.getValue());
				accountInfo.setDeleteFlag(DeleteFlag.Default.getValue());
				Date date2 = new Date();
				accountInfo.setCreateTime(date2);
				accountInfo.setCreateAccountId(currentUser.getUserInfo().getId());
				accountInfo.setUpdateTime(date2);
				accountInfo.setUpdateAccountId(currentUser.getUserInfo().getId());
				accountInfo.setCardId(buildCardId());
				accountInfoMapper.insertSelective(accountInfo);
				staffInfoVo.setAccountInfo(accountInfo);

				StaffEmploymentInfo staffEmploymentInfo = staffInfoVo.getEmployInfo();
				staffEmploymentInfo.setAccountId(accountId);
				staffEmploymentInfo.setEffectiveDate(new Date());
				saveEmploymentRequest(staffEmploymentInfo, currentUser, null);
			}
		} else {

			if (StringUtil.isNotEmpty(userInfo.getAvatar())) {
				if (userInfo.getAvatar().contains("http")) {
					UserInfo oldUserinfo = userInfoMapper.selectByPrimaryKey(userId);
					userInfo.setAvatar(oldUserinfo.getAvatar());
				} else {
					try {
						String pathAvatar = FilePathUtil.getMainPath(FilePathUtil.AVATAR_STAFF, userInfo.getAvatar());
						new FileFactory(Region.getRegion(Regions.AP_SOUTHEAST_2), PropertieNameConts.S3).copyFile(userInfo.getAvatar(), pathAvatar);
						userInfo.setAvatar(pathAvatar);
					} catch (FileNotFoundException e) {
						log.info("updateStaffInfo|update|FileNotFoundException s3 error", e);
						e.printStackTrace();
					} catch (Exception e) {
						log.info("updateStaffInfo|update|s3 error", e);
						e.printStackTrace();
					}
				}
			}

			// 判断邮箱唯一（排除自己）
			int res = staffMapper.selectAccount(userInfo.getEmail(), userId);
			if (res >= 1) {
				return failResult(getTipMsg("staff_email_registered"));
			} // 抽出方法

			// 数据库角色信息
			List<AccountRoleInfo> dbRoleInfo = accountRoleInfoMapper.getRoleInfoByAccountId(staffInfoVo.getAccountInfo().getId());
			List<AccountRoleInfo> roleInfo = staffInfoVo.getRoleInfo();
			// 是否换角色(casual)
			if ((containRole(roleInfo, Role.Casual.getValue()) && !containRole(dbRoleInfo, Role.Casual.getValue()))
					|| (!containRole(roleInfo, Role.Casual.getValue()) && containRole(dbRoleInfo, Role.Casual.getValue()))) {
				// 是否有排班
				int count = rosterStaffInfoMapper.getCountByAccountIdFutureDate(staffInfoVo.getAccountInfo().getId());
				if (count > 0) {
					return failResult(getTipMsg("staff_not_save_role"));
				}
			}
			UserInfo dbUser = userInfoMapper.selectByPrimaryKey(userId);
			// 是否转园
			if (isChangeCentre(dbUser, userInfo) && !containRole(roleInfo, Role.Casual.getValue())) {
				// 是否有排班
				int count = rosterStaffInfoMapper.getCountByAccountIdFutureDate(staffInfoVo.getAccountInfo().getId());
				if (count > 0) {
					return failResult(getTipMsg("staff_not_save_center"));
				}
			}

			// 更新园长，和group负责人 gfwang
			ServiceResult<Object> result = updateCenterOrGroupManager(staffInfoVo.getRoleInfo(), userId, staffInfoVo);
			if (!result.isSuccess()) {
				return failResult(result.getMsg());
			}
			// 更新用户账户信息
			Date date3 = new Date();
			userInfo.setOldId(dbUser.getOldId());
			userInfo.setCreateTime(dbUser.getCreateTime());
			// userInfo.setStaffInDate(date3);
			if (containRole(roleInfo, Role.Casual.getValue())) {
				userInfo.setCentersId("");
			}
			userInfo.setUpdateTime(date3);
			userInfo.setUpdateAccountId(currentUser.getUserInfo().getId());

			// 如果是变为CEO那么删除园区
			if (staffInfoVo.getRoleInfo().size() == 1) {
				if (roleInfoMapper.selectByPrimaryKey(staffInfoVo.getRoleInfo().get(0).getRoleId()).getValue() == Role.CEO.getValue()) {
					userInfo.setCentersId("");
				}
			}

			if (isChangeCentre(dbUser, userInfo)) {
				leaveService.cancelRequestLeave(userId);

			}
			userInfoMapper.updateByPrimaryKey(userInfo);

			updateLogsDetaile(dbUser, userInfo, "staffInfo", sourceLogsInfo, "", "");

			AccountInfo dbAccount = accountInfoMapper.selectByPrimaryKey(accountId);
			dbAccount.setAccount(staffInfoVo.getAccountInfo().getAccount());
			dbAccount.setUpdateTime(date3);
			dbAccount.setUpdateAccountId(currentUser.getUserInfo().getId());
			/*
			 * staffInfoVo.getAccountInfo().setPsw(dbAccount.getPsw());
			 * staffInfoVo.getAccountInfo().setUpdateTime(date3);
			 * staffInfoVo.getAccountInfo
			 * ().setUpdateAccountId(currentUser.getUserInfo().getId());
			 */
			accountInfoMapper.updateByPrimaryKey(dbAccount);

			// 员工转园则将员工当天的签到信息带入新的园区
			if (StringUtil.isNotEmpty(dbUser.getCentersId()) && StringUtil.isNotEmpty(staffInfoVo.getUserInfo().getCentersId())) {
				if (!dbUser.getCentersId().equals(staffInfoVo.getUserInfo().getCentersId())) {
					signinInfoMapper.updateCentreIdByAccountId(staffInfoVo.getUserInfo().getCentersId(), staffInfoVo.getAccountInfo().getId());
				}
			}
		}

		// 插入新数据前 删除医疗、疫苗、紧急联系人、资格证书、角色信息
		// staffMapper.deleteMedical(userId);
		// staffMapper.deleteMedicalImmu(userId);
		// staffMapper.deleteEmergency(userId);
		// staffMapper.deleteCredentials(userId);
		// staffMapper.deleteAccountRole(accountId);

		// 保存角色信息
		saveAccountRoleInfo(staffInfoVo.getRoleInfo(), accountId, sourceLogsInfo);
		// 保存医疗信息
		saveMedicalInfo(staffInfoVo.getMedicalInfo(), userId, sourceLogsInfo);
		// 保存紧急联系人
		saveEmergencyContactInfo(staffInfoVo.getContactInfo(), userId, sourceLogsInfo);
		// 保存疫苗接种信息
		saveMedicalImmunisationInfo(staffInfoVo.getMedicalImmunisationInfo(), userId, sourceLogsInfo);
		// 保存资格证书信息
		List<FileVo> credentFiles = saveCredentialsInfo(staffInfoVo.getCredentialsInfo(), userId, sourceLogsInfo);
		if (credentFiles != null && !credentFiles.isEmpty()) {
			// 添加到附件集合
			fileList.addAll(credentFiles);
		}

		if (fileList != null && !fileList.isEmpty()) {
			// 保存附件
			try {
				saveAttachmentInfo(fileList);
			} catch (Exception e) {
				log.error("updateStaffInfo", e);
				e.printStackTrace();
			}
		}

		if (isAdd) {
			// 新增用户发送欢迎邮件
			List<UserInfo> userInfos = new ArrayList<UserInfo>();
			userInfos.add(userInfo);
			insertWelEmail(userInfos, SendEmailType.haveCancel.getValue());
		}

		commonService.buildUserProfileRelation(userId, staffInfoVo.getProfileId(), valueId);

		// 更新用户姓名
		newsAccountsInfoMapper.updateUserNameByAccountId(accountId, getFullName(userInfo));
		logger.info("===================================updateStaffInfo begin====================================");
		return successResult(getTipMsg("staff_save"));
	}

	private void dealResponsiblePerson(StaffInfoVo vo) {
		if (containRole(vo.getRoleInfo(), Role.CentreManager.getValue())) {

			CentreManagerRelationInfo info = new CentreManagerRelationInfo(vo.getUserInfo().getCentersId(), vo.getUserInfo().getId(), false);
			centreManagerRelationInfoMapper.insert(info);
		} else if (containRole(vo.getRoleInfo(), Role.Educator.getValue())) {

			GroupManagerRelationInfo info = new GroupManagerRelationInfo(vo.getUserInfo().getGroupId(), vo.getUserInfo().getId(), false);
			groupManagerRelationInfoMapper.insert(info);
		}
	}

	/**
	 * 
	 * @description 初始化日志主表
	 * @author mingwang
	 * @create 2016年11月29日上午10:55:33
	 * @version 1.0
	 * @return
	 */
	private SourceLogsInfo initSourceLogsInfo(String ip, UserInfoVo currentUser, String sourceId) {
		SourceLogsInfo sourceLogsInfo = new SourceLogsInfo();
		sourceLogsInfo.setId(UUID.randomUUID().toString());
		sourceLogsInfo.setSourceId(sourceId);
		sourceLogsInfo.setUpdateTime(new Date());
		if (currentUser != null) {
			sourceLogsInfo.setAccountId(currentUser.getAccountInfo().getId());
		}
		sourceLogsInfo.setIpAddress(ip);
		sourceLogsInfo.setIsAdd(false);
		sourceLogsInfo.setIsSuccess(true);
		sourceLogsInfo.setIsDelete(false);

		return sourceLogsInfo;
	}

	private void updateLogsDetaile(Object oldObj, Object newObj, String detaileType, SourceLogsInfo sourceLogsInfo, String index, String tagIndex) {
		if (sourceLogsInfo == null) {
			return;
		}
		String sourceId = sourceLogsInfo.getId();
		boolean havaLogs = false;
		if (oldObj == null || newObj == null) {
			return;
		}
		String keyIndex = index;
		String tagsIndex = tagIndex;
		String compType = detaileType;
		List<LogsDetaileInfo> logsInfoList = new ArrayList<LogsDetaileInfo>();
		switch (detaileType) {
		case "staffInfo":// USER对象更新
			keyIndex = "";
			tagsIndex = "";
			break;
		case "staffEmergency":
			keyIndex = "";
			break;
		case "staffMedical":
			break;
		case "staffImmunisation":
			compType = "";
			break;
		case "staffCredentials":
			break;
		case "staffRoleInfo":
			tagsIndex = "";
			break;
		}
		try {
			logsInfoList = ObjectCompUtil.getResult(oldObj, newObj, "dd/MM/yyyy", sourceId, keyIndex, tagsIndex, compType, getFeildsProperties());
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		for (int i = 0, m = logsInfoList.size(); i < m; i++) {
			LogsDetaileInfo logDetaileInfo = logsInfoList.get(i);
			switch (detaileType) {
			case "staffInfo":// USER对象更新
				// 更改性别对于的英文gender
				if ("staffInfo_gender".equals(logDetaileInfo.getChangeFieldName())) {
					if ("0".equals(logDetaileInfo.getOldValue()))
						logDetaileInfo.setOldValue("Female");
					if ("1".equals(logDetaileInfo.getOldValue()))
						logDetaileInfo.setOldValue("Male");
					if ("0".equals(logDetaileInfo.getNewValue()))
						logDetaileInfo.setNewValue("Female");
					if ("1".equals(logDetaileInfo.getNewValue()))
						logDetaileInfo.setNewValue("Male");
				}
				// maritalStatus需要单独处理
				if ("staffInfo_maritalStatus".equals(logDetaileInfo.getChangeFieldName())) {
					logDetaileInfo.setOldValue(getFeildsProperties().getProperty("maritalStatus" + logDetaileInfo.getOldValue()));
					logDetaileInfo.setNewValue(getFeildsProperties().getProperty("maritalStatus" + logDetaileInfo.getNewValue()));
				}
				if ("staffInfo_contactEmail".equals(logDetaileInfo.getChangeFieldName()) || "staffInfo_contactLetter".equals(logDetaileInfo.getChangeFieldName())
						|| "staffInfo_contactPerson".equals(logDetaileInfo.getChangeFieldName())
						|| "staffInfo_contactPhone".equals(logDetaileInfo.getChangeFieldName())) {
					boolToYesNo(logDetaileInfo);
				}
				if ("staffInfo_staffType".equals(logDetaileInfo.getChangeFieldName())) {
					if ("0".equals(logDetaileInfo.getOldValue())) {
						logDetaileInfo.setOldValue("FullTime");
					}
					if ("1".equals(logDetaileInfo.getOldValue())) {
						logDetaileInfo.setOldValue("PartTime");
					}
					if ("2".equals(logDetaileInfo.getOldValue())) {
						logDetaileInfo.setOldValue("Casual");
					}
					if ("0".equals(logDetaileInfo.getNewValue())) {
						logDetaileInfo.setNewValue("FullTime");
					}
					if ("1".equals(logDetaileInfo.getNewValue())) {
						logDetaileInfo.setNewValue("PartTime");
					}
					if ("2".equals(logDetaileInfo.getNewValue())) {
						logDetaileInfo.setNewValue("Casual");
					}
				}
				if ("staffInfo_centersId".equals(logDetaileInfo.getChangeFieldName())) {
					logDetaileInfo.setOldValue(centersInfoMapper.getCenterNameById(logDetaileInfo.getOldValue()));
					logDetaileInfo.setNewValue(centersInfoMapper.getCenterNameById(logDetaileInfo.getNewValue()));
				}
				if ("staffInfo_roomId".equals(logDetaileInfo.getChangeFieldName())) {
					logDetaileInfo.setOldValue(roomInfoMapper.getRoomNameById(logDetaileInfo.getOldValue()));
					logDetaileInfo.setNewValue(roomInfoMapper.getRoomNameById(logDetaileInfo.getNewValue()));
				}
				if ("staffInfo_groupId".equals(logDetaileInfo.getChangeFieldName())) {
					logDetaileInfo.setOldValue(groupInfoMapper.getGroupNameById(logDetaileInfo.getOldValue()));
					logDetaileInfo.setNewValue(groupInfoMapper.getGroupNameById(logDetaileInfo.getNewValue()));
				}
				break;
			case "staffEmergency":
			case "staffMedical":
			case "staffImmunisation":
				if (";hadVaccine1;hadVaccine2;hadVaccine3;hadVaccine4;hadVaccine5;hadVaccine6;vaccineDate1;vaccineDate2;vaccineDate3;vaccineDate4;vaccineDate5;vaccineDate6;confirmed1;confirmed2;confirmed3;confirmed4;confirmed5;confirmed6;confirmedDate1;confirmedDate2;confirmedDate3;confirmedDate4;confirmedDate5;confirmedDate6;"
						.contains(";" + logDetaileInfo.getChangeFieldName() + ";")) {
					boolToYesNo(logDetaileInfo);
				}
				break;
			case "staffCredentials":
				break;
			case "staffRoleInfo":
				logDetaileInfo.setOldValue(roleInfoMapper.getNameById(logDetaileInfo.getOldValue()));
				logDetaileInfo.setNewValue(roleInfoMapper.getNameById(logDetaileInfo.getNewValue()));
				break;
			}
			logsDetaileInfoMapper.insert(logDetaileInfo);
			havaLogs = true;
		}
		// 判断是否有logs记录
		if (havaLogs) {
			SourceLogsInfo dbsourceLogsInfo = sourceLogsInfoMapper.selectByPrimaryKey(sourceLogsInfo.getId());
			if (dbsourceLogsInfo == null) {
				sourceLogsInfoMapper.insert(sourceLogsInfo);
			}
		}
	}

	private void boolToYesNo(LogsDetaileInfo logDetaileInfo) {
		if ("true".equals(logDetaileInfo.getOldValue())) {
			logDetaileInfo.setOldValue("YES");
		} else if ("false".equals(logDetaileInfo.getOldValue())) {
			logDetaileInfo.setOldValue("NO");
		}

		if ("true".equals(logDetaileInfo.getNewValue())) {
			logDetaileInfo.setNewValue("YES");
		} else if ("false".equals(logDetaileInfo.getNewValue())) {
			logDetaileInfo.setNewValue("NO");
		}
	}

	/**
	 * 
	 * @description updateCenterOrGroupManager
	 * @author gfwang
	 * @create 2016年8月24日上午8:59:02
	 * @version 1.0
	 * @param user
	 *            当前用户
	 * @param centerId
	 *            园区Id
	 * @param groupId
	 *            groupId
	 */
	private ServiceResult<Object> updateCenterOrGroupManager(List<AccountRoleInfo> roleInfoList, String userId, StaffInfoVo staffInfoVo) {
		String centerId = staffInfoVo.getUserInfo().getCentersId();
		String groupId = staffInfoVo.getUserInfo().getGroupId();
		userId = StringUtil.isEmpty(userId) ? "" : userId;
		log.info("updateCenterOrGroupManager|centerId=" + centerId + "|groupId=" + groupId);

		// 如果从其园区负责人，或者分组负责人 变更到CEO或兼职等没有园区，没有分组的时候，清除之前设置的园区负责人
		if (StringUtil.isEmpty(centerId) && StringUtil.isEmpty(groupId)) {
			// 清除园区负责人
			int cl1 = centersInfoMapper.clearCenterLead(userId);
			// 清除分组负责任人
			int cl2 = groupInfoMapper.clearGroupEduer(userId);
			log.info("清除所有負責人：cl1=" + cl1 + "|cl2=" + cl2);
			return successResult();
		}
		if (containRole(roleInfoList, Role.CentreManager.getValue())) {
			log.info("update Center");
			// 如果没有园区
			if (StringUtil.isEmpty(centerId)) {
				return successResult();
			}

			CentreManagerRelationInfo centreManagerRelationInfo = new CentreManagerRelationInfo(centerId, userId, false);
			centreManagerRelationInfoMapper.insert(centreManagerRelationInfo);

		} else if (containRole(roleInfoList, Role.Educator.getValue())) {
			log.info("update group");
			if (StringUtil.isEmpty(groupId)) {
				return successResult();
			}
			GroupManagerRelationInfo groupManagerRelationInfo = new GroupManagerRelationInfo(groupId, userId, false);
			groupManagerRelationInfoMapper.insert(groupManagerRelationInfo);
		} else {
			// 清除园区负责人
			int cl1 = centersInfoMapper.clearCenterLead(userId);
			// 清除分组负责任人
			int cl2 = groupInfoMapper.clearGroupEduer(userId);
			log.info("清除所有負責人：cl1=" + cl1 + "|cl2=" + cl2);
		}
		return successResult();
	}

	private ServiceResult<Object> updateCenterOrGroupManager(List<AccountRoleInfo> roleInfoList, UserInfo user) {
		String centerId = user.getCentersId();
		String groupId = user.getGroupId();
		String userId = user.getId();
		log.info("updateCenterOrGroupManager|centerId=" + centerId + "|groupId=" + groupId);

		// 如果从其园区负责人，或者分组负责人 变更到CEO或兼职等没有园区，没有分组的时候，清除之前设置的园区负责人
		if (StringUtil.isEmpty(centerId) && StringUtil.isEmpty(groupId)) {
			// 清除园区负责人
			int cl1 = centersInfoMapper.clearCenterLead(userId);
			// 清除分组负责任人
			int cl2 = groupInfoMapper.clearGroupEduer(userId);
			log.info("清除所有負責人：cl1=" + cl1 + "|cl2=" + cl2);
			return successResult();
		}
		if (containRole(roleInfoList, Role.CentreManager.getValue())) {
			log.info("update Center");
			// 如果没有园区
			if (StringUtil.isEmpty(centerId)) {
				return successResult();
			}

			CentreManagerRelationInfo centreManagerRelationInfo = new CentreManagerRelationInfo(centerId, userId, false);
			centreManagerRelationInfoMapper.insert(centreManagerRelationInfo);

		} else if (containRole(roleInfoList, Role.Educator.getValue())) {
			log.info("update group");
			if (StringUtil.isEmpty(groupId)) {
				return successResult();
			}

			GroupManagerRelationInfo groupManagerRelationInfo = new GroupManagerRelationInfo(groupId, userId, false);
			groupManagerRelationInfoMapper.insert(groupManagerRelationInfo);
		} else {
			// 清除园区负责人
			int cl1 = centersInfoMapper.clearCenterLead(userId);
			// 清除分组负责任人
			int cl2 = groupInfoMapper.clearGroupEduer(userId);
			log.info("清除所有負責人：cl1=" + cl1 + "|cl2=" + cl2);
		}
		return successResult();
	}

	/**
	 * 
	 * @description
	 * @author mingwang
	 * @create 2017年1月19日下午4:03:18
	 * @param currtenUserId
	 *            当前操作的员工userId
	 * @param leadId
	 *            数据库当前操作的center（group）的园长（老师）的userId
	 * @param name
	 *            当前操作的centerName（groupName）
	 * @param flag
	 *            标识当前操作的是center还是group
	 * @return
	 */
	private ServiceResult<Object> isContentReplace(String currtenUserId, String leadId, String name, boolean flag) {
		if (StringUtil.isEmpty(leadId)) {
			return successResult();
		}
		// 如果管理员和当前人是一样
		if (leadId.equals(currtenUserId)) {
			return successResult();
		}
		// 判断之前的管理人员是否为 归档状态
		AccountInfo account = accountInfoMapper.getAccountInfoByUserId(leadId);
		if (account.getStatus() == ArchivedStatus.Archived.getValue()) {
			return successResult();
		}
		UserInfo leader = userInfoMapper.selectByPrimaryKey(leadId);
		String fullName = getFullName(leader);
		// if (flag) {
		// return
		// failResult(MessageFormat.format(getTipMsg("satff_centerLead_exist"),
		// fullName, name));
		// }
		// return
		// failResult(MessageFormat.format(getTipMsg("satff_groupLead_exist"),
		// fullName, name));
		return successResult();
	}

	private String getFullName(UserInfo user) {
		String middleName = (user.getMiddleName() == null || "".equals(user.getMiddleName())) ? "" : " " + user.getMiddleName();
		String fullName = user.getFirstName() + middleName + " " + user.getLastName();
		return fullName;
	}

	/**
	 * 
	 * @description 是否包含這個角色
	 * @author gfwang
	 * @create 2016年8月25日上午11:20:18
	 * @version 1.0
	 * @param roleInfoList
	 *            list
	 * @param roleValue
	 *            角色
	 * @return
	 */
	private boolean containRole(List<AccountRoleInfo> roleInfoList, short roleValue) {
		boolean result = false;
		for (AccountRoleInfo role : roleInfoList) {
			String id = role.getRoleId();
			RoleInfo currtenRole = roleInfoMapper.selectByPrimaryKey(id);
			if (roleValue == currtenRole.getValue()) {
				result = true;
			}
		}
		return result;
	}

	/**
	 * 保存角色信息
	 * 
	 * @author rick
	 * @date 2016年8月4日 上午10:06:48
	 * @param roleInfos
	 * @param accountId
	 * @param sourceLogsInfo
	 */
	public void saveAccountRoleInfo(List<AccountRoleInfo> roleInfos, String accountId, SourceLogsInfo sourceLogsInfo) {
		// 如果是cook要删除Casual以外的标签
		List<AccountRoleInfo> roleInfos2 = new ArrayList<AccountRoleInfo>();
		boolean isCook = false;
		for (AccountRoleInfo info : roleInfos) {
			RoleInfo roleInfo = roleInfoMapper.selectByPrimaryKey(info.getRoleId());
			if (null == roleInfo) {
				continue;
			}
			if (roleInfo.getValue() == Role.Cook.getValue()) {
				roleInfos2.add(info);
				isCook = true;
			}
			if (isCook && roleInfo.getValue() == Role.Casual.getValue()) {
				roleInfos2.add(info);
			}
		}
		List<AccountRoleInfo> dbRoleInfos = accountRoleInfoMapper.getRoleInfoByAccountId(accountId);
		updateRoleLogs(roleInfos, dbRoleInfos, sourceLogsInfo);
		if (!roleInfos.isEmpty()) {

			for (AccountRoleInfo accountRoleInfo : roleInfos) {
				AccountRoleInfo dbAccountRoleInfo = accountRoleInfoMapper.selectByPrimaryKey(accountRoleInfo.getId());
				accountRoleInfo.setId(UUID.randomUUID().toString());
				accountRoleInfo.setAccountId(accountId);
				accountRoleInfo.setDeleteFlag(DeleteFlag.Default.getValue());
				updateLogsDetaile(dbAccountRoleInfo, accountRoleInfo, "staffRoleInfo", sourceLogsInfo, "", "");
			}
		}
		staffMapper.deleteAccountRole(accountId);
		// 批量插入
		if (ListUtil.isNotEmpty(roleInfos2) && isCook) {
			accountRoleInfoMapper.insertBatch(roleInfos2);
		} else if (ListUtil.isNotEmpty(roleInfos) && !isCook) {
			accountRoleInfoMapper.insertBatch(roleInfos);
		}
	}

	/**
	 * 
	 * @description 处理Specify role日志
	 * @author mingwang
	 * @create 2016年11月30日下午6:28:10
	 * @version 1.0
	 * @param roleInfos
	 * @param dbRoleInfos
	 * @param sourceLogsInfo
	 */
	private void updateRoleLogs(List<AccountRoleInfo> roleInfos, List<AccountRoleInfo> dbRoleInfos, SourceLogsInfo sourceLogsInfo) {
		List<AccountRoleInfo> specifyRoles = getSpecifyRoles(roleInfos);
		List<AccountRoleInfo> dbSpecifyRoles = getSpecifyRoles(dbRoleInfos);
		Set<String> roleNames = new HashSet<String>();
		Set<String> dbRoleNames = new HashSet<String>();
		String oldValue = "";
		String newValue = "";
		for (AccountRoleInfo roleInfo : specifyRoles) {
			roleNames.add(roleInfo.getRoleId());
			newValue += "," + roleInfoMapper.getNameById(roleInfo.getRoleId());
		}
		if (StringUtil.isNotEmpty(newValue)) {
			newValue = newValue.substring(1);
		}
		for (AccountRoleInfo dbRoleInfo : dbSpecifyRoles) {
			dbRoleNames.add(dbRoleInfo.getRoleId());
			oldValue += "," + roleInfoMapper.getNameById(dbRoleInfo.getRoleId());
		}
		if (StringUtil.isNotEmpty(oldValue)) {
			oldValue = oldValue.substring(1);
		}
		if (!ListUtil.isEqualList(roleNames, dbRoleNames)) {
			LogsDetaileInfo logsDetaileInfo = new LogsDetaileInfo(UUID.randomUUID().toString(), "", oldValue, newValue,
					getFeildsProperties().getProperty("staffRoleInfo_specify"), sourceLogsInfo.getId());
			logsDetaileInfoMapper.insert(logsDetaileInfo);
			SourceLogsInfo dbsourceLogsInfo = sourceLogsInfoMapper.selectByPrimaryKey(sourceLogsInfo.getId());
			if (dbsourceLogsInfo == null) {
				sourceLogsInfoMapper.insert(sourceLogsInfo);
			}
		}

	}

	/**
	 * 
	 * @description 过滤SpecifyRoles
	 * @author mingwang
	 * @create 2016年11月30日下午6:49:35
	 * @version 1.0
	 * @param roleInfos
	 * @return
	 */
	private List<AccountRoleInfo> getSpecifyRoles(List<AccountRoleInfo> roleInfos) {
		List<AccountRoleInfo> returnRoles = new ArrayList<AccountRoleInfo>();
		for (AccountRoleInfo RoleInfo : roleInfos) {
			String roleName = roleInfoMapper.getNameById(RoleInfo.getRoleId());
			if (roleName.equals(Role.EducatorNominatedSupervisor.getName()) || roleName.equals(Role.EducatorECT.getName())
					|| roleName.equals(Role.EducatorSecondInCharge.getName()) || roleName.equals(Role.EducatorCertifiedSupersor.getName())
					|| roleName.equals(Role.EducatorFirstAidOfficer.getName()) || roleName.equals(Role.Diploma.getName())
					|| roleName.equals(Role.Educational_Leader.getName())) {
				returnRoles.add(RoleInfo);
			}
		}
		return returnRoles;
	}

	/**
	 * 保存医疗信息
	 * 
	 * @author rick
	 * @date 2016年8月4日 上午10:06:58
	 * @param medicalInfo
	 * @param userId
	 * @param sourceLogsInfo
	 */
	public void saveMedicalInfo(MedicalInfo medicalInfo, String userId, SourceLogsInfo sourceLogsInfo) {
		if (medicalInfo != null) {
			MedicalInfo dbMedicalInfo = medicalInfoMapper.selectByPrimaryKey(medicalInfo.getId());
			medicalInfo.setUserId(userId);
			medicalInfo.setId(UUID.randomUUID().toString());
			medicalInfo.setDeleteFlag(DeleteFlag.Default.getValue());
			staffMapper.deleteMedical(userId);

			updateLogsDetaile(dbMedicalInfo, medicalInfo, "staffMedical", sourceLogsInfo, "", "");
			medicalInfoMapper.insert(medicalInfo);
		}
	}

	/**
	 * 保存紧急联系人信息
	 * 
	 * @author rick
	 * @date 2016年8月4日 上午10:07:04
	 * @param emergencyList
	 * @param userId
	 */
	public void saveEmergencyContactInfo(List<EmergencyContactInfo> emergencyList, String userId, SourceLogsInfo sourceLogsInfo) {
		if (!emergencyList.isEmpty()) {
			int i = 1;
			for (EmergencyContactInfo emergencyContactInfo : emergencyList) {
				EmergencyContactInfo dbEmergencyContactInfo = contactInfoMapper.selectByPrimaryKey(emergencyContactInfo.getId());
				emergencyContactInfo.setUserId(userId);
				emergencyContactInfo.setId(UUID.randomUUID().toString());
				emergencyContactInfo.setDeleteFlag(DeleteFlag.Default.getValue());
				updateLogsDetaile(dbEmergencyContactInfo, emergencyContactInfo, "staffEmergency", sourceLogsInfo, String.valueOf(i), String.valueOf(i));
				i += 1;
			}
		}
		staffMapper.deleteEmergency(userId);
		// 批量插入
		if (ListUtil.isNotEmpty(emergencyList)) {
			contactInfoMapper.insterBatchEmergencyContactInfo(emergencyList);
		}
	}

	/**
	 * 保存疫苗接种信息
	 * 
	 * @author rick
	 * @date 2016年8月4日 上午10:07:41
	 * @param immunisationList
	 * @param userId
	 * @param sourceLogsInfo
	 */
	public void saveMedicalImmunisationInfo(List<MedicalImmunisationInfo> immunisationList, String userId, SourceLogsInfo sourceLogsInfo) {
		if (!immunisationList.isEmpty()) {
			for (MedicalImmunisationInfo medicalImmunisationInfo : immunisationList) {
				MedicalImmunisationInfo dbMedicalImmunisationInfo = immunisationInfoMapper.selectByPrimaryKey(medicalImmunisationInfo.getId());
				medicalImmunisationInfo.setUserId(userId);
				medicalImmunisationInfo.setId(UUID.randomUUID().toString());
				medicalImmunisationInfo.setDeleteFlag(DeleteFlag.Default.getValue());
				updateLogsDetaile(dbMedicalImmunisationInfo, medicalImmunisationInfo, "staffImmunisation", sourceLogsInfo, medicalImmunisationInfo.getName(),
						medicalImmunisationInfo.getName());
			}
			staffMapper.deleteMedicalImmu(userId);
			// 批量插入
			immunisationInfoMapper.insertBatch(immunisationList);
		}
	}

	/**
	 * 保存资格证书信息
	 * 
	 * @author rick
	 * @date 2016年8月4日 上午10:07:51
	 * @param credentialsList
	 * @param userId
	 * @param sourceLogsInfo
	 * @return
	 */
	public List<FileVo> saveCredentialsInfo(List<CredentialsInfo> credentialsList, String userId, SourceLogsInfo sourceLogsInfo) {
		List<FileVo> fileVos = new ArrayList<FileVo>();
		if (credentialsList.isEmpty()) {
			return null;
		}
		handleCredentialsLogs(credentialsList, sourceLogsInfo);
		Date createTime = new Date();
		for (CredentialsInfo credentialsInfo : credentialsList) {

			credentialsInfo.setUserId(userId);
			credentialsInfo.setId(UUID.randomUUID().toString());
			credentialsInfo.setDeleteFlag(DeleteFlag.Default.getValue());
			credentialsInfo.setCreateTime(createTime);
			createTime = DateUtil.addSeconds(createTime, 1);
			// 附件1
			List<FileVo> fileVoList = credentialsInfo.getFileList();
			if (fileVoList.isEmpty()) {
				credentialsInfo.setCertificateImgId(null);
				credentialsInfo.setLetterImgId(null);
				continue;
			}
			FileVo credFileVo = fileVoList.get(0);
			if (credFileVo != null && StringUtil.isBlank(credFileVo.getId())) {
				String fileId = UUID.randomUUID().toString();
				if (credentialsInfo.getType() == Credential.ChildrenCheck.getValue()) {
					// type=3
					credentialsInfo.setLetterImgId(fileId);
				} else {
					credentialsInfo.setCertificateImgId(fileId);
				}
				credFileVo.setId(fileId);
				fileVos.add(credFileVo);
			}

			// 附件2 type=3
			if (credentialsInfo.getType() == Credential.ChildrenCheck.getValue()) {
				List<FileVo> fileVoList2 = credentialsInfo.getFileList2();
				if (fileVoList2.isEmpty()) {
					credentialsInfo.setEmployVerifyAttachId(null);
					continue;
				}
				FileVo credFileVo2 = fileVoList2.get(0);
				if (credFileVo2 != null && StringUtil.isBlank(credFileVo2.getId())) {
					String fileId = UUID.randomUUID().toString();
					credentialsInfo.setEmployVerifyAttachId(fileId);
					credFileVo2.setId(fileId);
					fileVos.add(credFileVo2);
				}
			}
		}
		staffMapper.deleteCredentials(userId);
		// 批量插入
		credentialsInfoMapper.insertBatch(credentialsList);
		// 返回组装的附件列表
		return fileVos;
	}

	/**
	 * 
	 * @description 处理Credentials日志
	 * @author mingwang
	 * @create 2016年11月30日下午5:33:14
	 * @version 1.0
	 * @param credentialsList
	 * @param sourceLogsInfo
	 */
	private void handleCredentialsLogs(List<CredentialsInfo> credentialsList, SourceLogsInfo sourceLogsInfo) {
		List<CredentialsInfo> qualificationList = new ArrayList<CredentialsInfo>();
		List<CredentialsInfo> firstAidsList = new ArrayList<CredentialsInfo>();
		List<CredentialsInfo> childrenCheckList = new ArrayList<CredentialsInfo>();
		List<CredentialsInfo> trainingsList = new ArrayList<CredentialsInfo>();
		for (CredentialsInfo credentialsInfo : credentialsList) {
			if (credentialsInfo.getType() == Credential.Qualification.getValue()) {
				qualificationList.add(credentialsInfo);
			}
			if (credentialsInfo.getType() == Credential.FirstAids.getValue()) {
				firstAidsList.add(credentialsInfo);
			}
			if (credentialsInfo.getType() == Credential.ChildrenCheck.getValue()) {
				childrenCheckList.add(credentialsInfo);
			}
			if (credentialsInfo.getType() == Credential.Trainings.getValue()) {
				trainingsList.add(credentialsInfo);
			}
		}
		updateLogs(qualificationList, sourceLogsInfo);
		updateLogs(firstAidsList, sourceLogsInfo);
		updateLogs(childrenCheckList, sourceLogsInfo);
		updateLogs(trainingsList, sourceLogsInfo);

	}

	/**
	 * 
	 * @description 日志处理
	 * @author mingwang
	 * @create 2016年11月30日下午5:43:03
	 * @version 1.0
	 * @param qualificationList
	 * @param sourceLogsInfo
	 */
	private void updateLogs(List<CredentialsInfo> credentialsList, SourceLogsInfo sourceLogsInfo) {
		int i = 1;
		for (CredentialsInfo credentialsInfo : credentialsList) {
			CredentialsInfo dbCredentialsInfo = credentialsInfoMapper.selectByPrimaryKey(credentialsInfo.getId());
			updateLogsDetaile(dbCredentialsInfo, credentialsInfo, "staffCredentials", sourceLogsInfo, String.valueOf(credentialsInfo.getType()), String.valueOf(i));
			i += 1;
		}

	}

	/**
	 * 发送欢迎邮件方法
	 * 
	 * @author rick
	 * @date 2016年8月9日 下午2:09:45
	 * @param userInfo
	 *            用户信息
	 * @param accountId
	 *            账户id
	 */
	public void insertWelEmail(List<UserInfo> userInfos, short sendEmailType) {
		logger.info("===================================sendWelEmail begin====================================");
		List<EmailVo> emailVoList = new ArrayList<EmailVo>();
		for (UserInfo userInfo : userInfos) {
			AccountInfo accountInfo = accountInfoMapper.selectAccountInfoByUserId(userInfo.getId());
			// token
			String token = UUID.randomUUID().toString();
			String email = userInfo.getEmail();
			String middleName = StringUtil.isEmpty(userInfo.getMiddleName()) ? "" : userInfo.getMiddleName() + " ";
			String name = userInfo.getFirstName() + " " + middleName + userInfo.getLastName();
			String sendAddress = getSystemMsg("mailHostAccount");
			String sendName = getSystemMsg("email_name");
			String receiverAddress = email;
			String receiverName = name;
			String sub = "";
			String msg = "";
			switch (sendEmailType) {
			case 0:
				sub = getSystemEmailMsg("welcome_to_kindikids_title");
				msg = MessageFormat.format(getSystemEmailMsg("welcome_to_kindikids_content"), userFactory.getUserName(userInfo), getSystemMsg("rootUrl"), token);
				break;
			case 1:
				sub = getSystemEmailMsg("welcome_to_kindikids_title");
				msg = MessageFormat.format(getSystemEmailMsg("welcome_to_kindikids_content_haveCancel"), userFactory.getUserName(userInfo), getSystemMsg("rootUrl"),
						token, userInfo.getId());
				break;
			case 2:
				sub = getSystemEmailMsg("reminder_staff_title");
				msg = MessageFormat.format(getSystemEmailMsg("reminder_staff_content"), userFactory.getUserName(userInfo), getSystemMsg("rootUrl"), token,
						userInfo.getId());
				break;
			default:
				break;
			}
			String html = EmailUtil.replaceHtml(sub, msg);
			System.err.println(html);
			boolean isHtml = true;
			String email_key = getSystemMsg("email_key");
			// 手动过来的直接发送
			if (sendEmailType != SendEmailType.timedTask.getValue()) {
				sendMail(sendAddress, sendName, receiverAddress, receiverName, sub, html, isHtml, email_key);
			} else { // 定时任务过来的存入数据库
				MsgEmailInfo msgEmailInfo = MsgEmailInfo.getEmailMsgInstance(sendAddress, sendName, userInfo.getEmail(), userFactory.getUserName(userInfo), sub, null,
						null, null, html);
				msgEmailInfoMapper.insert(msgEmailInfo);
			}
			// 将token存入数据库
			ChangePswRecordInfo record = new ChangePswRecordInfo();
			record.setToken(token);
			record.setCreateTime(new Date());
			record.setAccountId(accountInfo.getId());
			record.setStatus(TokenStatus.Unused.getValue());
			record.setId(UUID.randomUUID().toString());
			record.setDeleteFlag(DeleteFlag.Default.getValue());
			// 先删除过时邮件记录
			changePswRecordInfoMapper.deleteByAccount(accountInfo.getId());
			changePswRecordInfoMapper.insert(record);
		}
		logger.info("===================================sendWelEmail end====================================");
	}

	/**
	 * 发送欢迎邮件
	 */
	@Override
	public ServiceResult<Object> insertWelEmail(String userId, String accountId, boolean haveCancel) {
		UserInfo userInfo = userInfoMapper.selectByPrimaryKey(userId);
		List<UserInfo> userInfos = new ArrayList<UserInfo>();
		userInfos.add(userInfo);
		insertWelEmail(userInfos, SendEmailType.isManual.getValue());
		return successResult(getTipMsg("staff_send_welcome_email"));
	}

	/**
	 * 批量保存附件
	 * 
	 * @author rick
	 * @date 2016年8月2日 上午10:54:32
	 * @param fileVos
	 * @throws Exception
	 */
	private void saveAttachmentInfo(List<FileVo> fileVos) throws Exception {
		logger.info("===================================saveAttachmentInfo begin====================================");
		if (ListUtil.isEmpty(fileVos)) {
			return;
		}
		List<AttachmentInfo> list = new ArrayList<AttachmentInfo>();
		FileFactory fac = new FileFactory(Region.getRegion(Regions.AP_SOUTHEAST_2), PropertieNameConts.S3);
		for (FileVo fileVo : fileVos) {
			AttachmentInfo attachmentInfo = new AttachmentInfo();
			attachmentInfo.setId(UUID.randomUUID().toString());
			attachmentInfo.setSourceId(fileVo.getId());
			attachmentInfo.setAttachName(fileVo.getFileName());
			attachmentInfo.setVisitUrl(fileVo.getVisitedUrl());
			attachmentInfo.setDeleteFlag(DeleteFlag.Default.getValue());
			String attachId = fileVo.getRelativePathName();
			if (StringUtil.isBlank(attachId)) {
				continue;
			}
			String mainDir = FilePathUtil.getMainPath(FilePathUtil.STAFF_PATH, attachId);
			attachmentInfo.setAttachId(mainDir);
			list.add(attachmentInfo);
			fac.copyFile(attachId, mainDir);
		}
		if (!ListUtil.isEmpty(list)) {
			attachmentInfoMapper.insterBatchAttachmentInfo(list);
		}
		logger.info("===================================saveAttachmentInfo end====================================");
	}

	/**
	 * 获取组、园区
	 * 
	 * @author rick
	 * @date 2016年7月28日 上午11:16:35
	 * @return
	 */
	@Override
	public ServiceResult<StaffRoleInfoVo> getRoleList() {
		logger.info("===================================getRoleList begin====================================");
		StaffRoleInfoVo staffRoleInfoVo = new StaffRoleInfoVo();
		staffRoleInfoVo.setRoleInfos(staffMapper.selectRole());
		List<CentersInfo> list = staffMapper.selectCenters(null);
		for (CentersInfo c : list) {
			if (!getSystemMsg("rydeId").equals(c.getId())) {
				continue;
			}
			c.setRyde(true);
		}
		staffRoleInfoVo.setCentersInfos(list);
		staffRoleInfoVo.setRoomInfos(staffMapper.selectRoom(null));
		staffRoleInfoVo.setGroups(staffMapper.selectGroup());
		logger.info("===================================getRoleList begin====================================");
		return successResult(staffRoleInfoVo);
	}

	/**
	 * 删除员工
	 */
	@Override
	public ServiceResult<Object> deleteStaff(String userId, UserInfoVo currentUser) {
		logger.info("===================================deleteStaff begin====================================");
		String currentUserId = currentUser.getUserInfo().getId();
		if (StringUtil.compare(userId, currentUserId)) {
			return failResult(getTipMsg("can_not_change_self"));
		}
		// 获取员工的角色信息
		AccountInfo account = staffMapper.selectAccountByUserId(userId);
		// 判断员工是否有排班
		int count = rosterStaffInfoMapper.getCountByAccountIdFutureDate(account.getId());
		// 有排班的员工不能删除
		if (count > 0) {
			return failResult(getTipMsg("staff_not_delete"));
		}
		staffMapper.deleteStaffUser(userId);
		staffMapper.deleteStaffAccount(userId);
		userInfoMapper.updateTimeByUserId(userId, new Date());
		// 解除园长/老师与center/group的关系
		centerService.releaseStaff(userId);
		// updateAchive(userId, AccountActiveStatus.Disabled.getValue());
		// 取消该员工的request的leave
		leaveService.cancelRequestLeave(userId);
		logger.info("===================================deleteStaff begin====================================");
		return successResult(getTipMsg("staff_delete"));
	}

	/**
	 * 员工归档/解除归档
	 */
	@Override
	public ServiceResult<Object> updateAchive(String userId, int type, UserInfoVo currentUser) {
		logger.info("===================================updateAchive begin====================================");
		String currentUserId = currentUser.getUserInfo().getId();
		// 判断是否自己（员工不能归档自己）
		if (StringUtil.compare(userId, currentUserId)) {
			return failResult(getTipMsg("can_not_change_self"));
		} else {
			ServiceResult<Object> result = isCasualCentreManager(currentUser);
			if (!result.isSuccess()) {
				result.setCode(1);
				return result;
			}
		}
		// 获取员工的角色信息
		AccountInfo account = staffMapper.selectAccountByUserId(userId);
		if (type == 1) {
			// 获取要归档员工的userInfo
			UserInfo userInfo = staffMapper.selectUserById(userId);
			// 判断center是否归档
			int index_centers = centersInfoMapper.isCentersAchived(userId);
			if (index_centers >= 1) {
				// 清除该员工的centerId,roomId,groupId
				int tag_center = staffMapper.clearStaffCenterInfo(userInfo.getCentersId(), userInfo.getRoomId(), userInfo.getGroupId(), userId);
				// 解除归档
				staffMapper.updateAchive(userId, String.valueOf(type));
				log.info("updateAchive|tag_center=" + tag_center);
				// 解除园长/老师与center/group的关系
				centerService.releaseStaff(userId);
				return successResult2(2, getTipMsg("staffCenter_archive"));
			}
			// 判断room是否归档
			int index_room = roomInfoMapper.isRoomAchived(userId);
			if (index_room >= 1) {
				// 清除该员工的roomId、groupId
				int tag_room = staffMapper.clearStaffCenterInfo(null, userInfo.getRoomId(), userInfo.getGroupId(), userId);
				// 解除归档
				staffMapper.updateAchive(userId, String.valueOf(type));
				log.info("updateAchive|tag_room=" + tag_room);
				// 解除园长/老师与center/group的关系
				centerService.releaseStaff(userId);
				return successResult2(3, getTipMsg("staffRoom_archive"));
			}
			// 判断group是否归档
			int index_group = groupInfoMapper.isGroupAchived(userId);
			if (index_group >= 1) {
				// 清除该员工的groupId
				int tag_group = staffMapper.clearStaffCenterInfo(null, null, userInfo.getGroupId(), userId);
				// 解除归档
				staffMapper.updateAchive(userId, String.valueOf(type));
				log.info("updateAchive|tag_group=" + tag_group);
				// 解除园长/老师与center/group的关系
				centerService.releaseStaff(userId);
				return successResult2(4, getTipMsg("staffGroup_archive"));
			}

			List<AccountRoleInfo> accountRoleInfoList = staffMapper.selectAccountRoleByAccountId(account.getId());
			// 如果是园长
			if (containRole(accountRoleInfoList, Role.CentreManager.getValue())) {
				// 获取归档前所在园区的centersLeaderId
				String centersLeaderId = centersInfoMapper.isCentersLeaderInUse(userId);
				log.info("updateAchive|centersLeaderId=" + centersLeaderId);
				// 判断centersLeaderId是否为空
				if (!StringUtil.isEmpty(centersLeaderId)) {
					// 判断是否是自己
					if (!userId.equals(centersLeaderId)) {
						log.info("updateAchive|" + !userId.equals(centersLeaderId));
						// 获取centersLeaderId的accountInfo
						AccountInfo accountInfo = staffMapper.selectAccountByUserId(centersLeaderId);
						// 判断该centersLeader是否归档
						if (AccountActiveStatus.Enabled.getValue() == accountInfo.getStatus()) {
							// 清除要归档园长的centerId,roomId,groupId
							staffMapper.clearStaffCenterInfo(userInfo.getCentersId(), userInfo.getRoomId(), userInfo.getGroupId(), userId);
							// 解除归档
							staffMapper.updateAchive(userId, String.valueOf(type));
							return successResult2(2, getTipMsg("position_in_use"));
						} else {
							// 重新绑定要归档园长与center的关系
							int addCenterLead = centersInfoMapper.addCenterLead(userId);
							log.info("updateAchive|addCenterLead=" + addCenterLead);
						}
					}
				}
				// 解除归档
				// staffMapper.updateAchive(userId, String.valueOf(type));
				// return successResult(getTipMsg("staff_unachive"));
			}
			// 如果是老师
			if (containRole(accountRoleInfoList, Role.Educator.getValue())) {
				// 获取归档前所在group的educatorId
				String educatorId = groupInfoMapper.isEducatorInUse(userId);
				log.info("updateAchive|educatorId=" + educatorId);
				// 判断educatorId是否为空
				if (!StringUtil.isEmpty(educatorId)) {
					// 判断是否是自己
					if (!userId.equals(educatorId)) {
						log.info("updateAchive|" + !userId.equals(educatorId));
						// 获取educator的accountInfo
						AccountInfo accountInfo = staffMapper.selectAccountByUserId(educatorId);
						// 判断该educator是否归档
						if (AccountActiveStatus.Enabled.getValue() == accountInfo.getStatus()) {
							// 清除要归档老师的groupId
							staffMapper.clearStaffCenterInfo(null, null, userInfo.getGroupId(), userId);
							// 解除归档
							staffMapper.updateAchive(userId, String.valueOf(type));
							return successResult2(4, getTipMsg("position_in_use"));
						} else {
							// 重新绑定要归档老师与group的关系
							int addGroupEduer = groupInfoMapper.addGroupEduer(userId);
							log.info("updateAchive|addGroupEduer=" + addGroupEduer);
						}
					}
				}
			}
			// 解除归档
			staffMapper.updateAchive(userId, String.valueOf(type));
			// 清空Employment End Day
			staffMapper.clearEmploymentEndDay(account.getId());
		}
		if (type == 0) {
			// 判断员工是否有排班
			int count = rosterStaffInfoMapper.getCountByAccountIdFutureDate(account.getId());
			// 有排班的员工不能归档
			if (count > 0) {
				return failResult(getTipMsg("staff_not_archive"));
			}
			// 归档
			staffMapper.updateAchive(userId, String.valueOf(type));
			// 取消该员工的request的leave
			leaveService.cancelRequestLeave(userId);
			// 解除园长/老师与center/group的关系
			// centerService.releaseStaff(userId);
		}
		userInfoMapper.updateTimeByUserId(userId, new Date());
		logger.info("===================================updateAchive end====================================");
		if (type == AccountActiveStatus.Disabled.getValue()) {
			return successResult(getTipMsg("staff_achive"));
		}
		return successResult(getTipMsg("staff_unachive"));
	}

	/**
	 * sendEmail Thread
	 * 
	 * @author rick
	 * @date 2016年8月11日 上午9:18:08
	 * @param sendAddress
	 * @param sendName
	 * @param receiverAddress
	 * @param receiverName
	 * @param sub
	 * @param msg
	 * @param isHtml
	 * @param emial_key
	 */
	private void sendMail(final String sendAddress, final String sendName, final String receiverAddress, final String receiverName, final String sub, final String msg,
			final boolean isHtml, final String emialKey) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					AwsEmailUtil a = new AwsEmailUtil();
					a.sendgrid(sendAddress, sendName, receiverAddress, receiverName, sub, msg, isHtml, null, null, emialKey);
				} catch (Exception e) {
					logger.error("发送邮件失败" + e.toString());
				}
			}
		}).start();
	}

	/**
	 * @description 判断员工是否转园
	 * @author hxzhang
	 * @create 2016年8月24日上午11:45:01
	 * @version 1.0
	 * @param dbUser
	 *            数据库中数据
	 * @param userInfo
	 *            表单提交数据
	 * @return 返回结果
	 */
	private boolean isChangeCentre(UserInfo dbUser, UserInfo userInfo) {
		if (StringUtil.isEmpty(dbUser.getCentersId()) && StringUtil.isEmpty(userInfo.getCentersId())) {
			return false;
		}
		if (StringUtil.isNotEmpty(dbUser.getCentersId()) && StringUtil.isEmpty(userInfo.getCentersId())) {
			return true;
		}
		if (StringUtil.isEmpty(dbUser.getCentersId()) && StringUtil.isNotEmpty(userInfo.getCentersId())) {
			return true;
		}
		if (!dbUser.getCentersId().equals(userInfo.getCentersId())) {
			return true;
		}
		return false;
	}

	/**
	 * 更新staff的center、room、group信息
	 */
	@Override
	public ServiceResult<Object> updateStaff(String userId, String centerId, String roomId, String groupId) {
		staffMapper.updateStaffInfo(userId, centerId, roomId, groupId);
		return null;
	}

	@Override
	public int sendActiveEmailTimedTask(Date now) {
		logger.info("sendActiveEmailTimedTask==>start");
		List<UserInfo> sendEmailList = new ArrayList<UserInfo>();
		List<UserInfo> staffList = staffMapper.getUnActiveStaff();
		for (int i = 0; i < staffList.size(); i++) {
			long date = DateUtil.getPreviousDay(staffList.get(i).getCreateTime());
			int n = 1;
			logger.info("mei7tianfasongyoujian==>start");
			while (!((date + (n * 7)) > (DateUtil.getPreviousDay(now)))) {
				if (((date + (n * 7)) - (DateUtil.getPreviousDay(now))) == 0) {
					sendEmailList.add(staffList.get(i));// 添加满足条件的
				}
				n += n;
			}
			logger.info("mei7tianfasongyoujian==>end");
		}
		// 判断是否有满足条件的,有则发送.
		if (ListUtil.isNotEmpty(sendEmailList)) {
			insertWelEmail(sendEmailList, SendEmailType.timedTask.getValue());
		}
		logger.info("sendActiveEmailTimedTask==>end");
		return sendEmailList.size();
	}

	@Override
	public List<SelecterPo> getSelecterStaff(StaffCondition condition) {
		String roomId = condition.getRoomId();
		RoomInfo room = roomInfoMapper.selectByPrimaryKey(roomId);
		condition.setCenterId(room.getCentersId());
		condition.setRoomId(null);
		return userInfoMapper.getSelectStaff(condition);
	}

	@Override
	public List<SelecterPo> getCasualStaff(String p) {
		return userInfoMapper.getCasualStaff(p);
	}

	private Date getCurrentDate() {
		return DateUtil.parse(DateUtil.format(new Date(), "dd/MM/yyyy"), "dd/MM/yyyy");
	}

	@Override
	public ServiceResult<Object> saveEmploymentRequest(StaffEmploymentInfo staffEmploymentInfo, UserInfoVo currentUser, HttpServletRequest request) {
		// 基础信息验证
		ServiceResult<Object> validateEmploymentInfoResult = validateEmploymentInfo(staffEmploymentInfo, currentUser);
		if (!validateEmploymentInfoResult.isSuccess()) {
			return validateEmploymentInfoResult;
		}

		// 验证未来是否有排班
		ServiceResult<Object> futureRosterValidateResult = futureRosterValidate(staffEmploymentInfo);
		if (!futureRosterValidateResult.isSuccess()) {
			return futureRosterValidateResult;
		}

		ServiceResult<Object> validateRegionCentreRosterResult = validateRegionCentreRoster(staffEmploymentInfo);
		if (!validateRegionCentreRosterResult.isSuccess()) {
			return validateRegionCentreRosterResult;
		}

		// 如果已存在待执行的申请,现在取消
		staffEmploymentInfoMapper.cancelRequestByAccountId(staffEmploymentInfo.getAccountId());

		String id = UUID.randomUUID().toString();
		staffEmploymentInfo.setId(id);
		staffEmploymentInfo.setDeleteFlag(false);
		staffEmploymentInfo.setCreateTime(new Date());
		staffEmploymentInfo.setCreateAccountId(currentUser.getAccountInfo().getId());

		// 判断是否是当天
		if (DateUtil.isSameDay(staffEmploymentInfo.getEffectiveDate(), new Date())) {
			if (staffEmploymentInfo.getLastDay() != null && !staffEmploymentInfo.getLastDay().after(new Date())) {
				// 如果last day不是未来时间,立即归档
				AccountInfo accountInfo = accountInfoMapper.selectByPrimaryKey(staffEmploymentInfo.getAccountId());
				ServiceResult<Object> result = updateAchive(accountInfo.getUserId(), 0, currentUser);
				if (!result.isSuccess()) {
					return result;
				}
			}

			staffEmploymentInfo.setCurrent(true);
			staffEmploymentInfo.setStatus(EmployStatus.Executed.getValue());
			// 同步其他信息
			syncInfo(staffEmploymentInfo, currentUser);
			// 移除当前Employ
			staffEmploymentInfoMapper.removeCurrentEmploy(staffEmploymentInfo.getAccountId());
		} else {
			staffEmploymentInfo.setStatus(EmployStatus.Request.getValue());
			staffEmploymentInfo.setCurrent(false);
		}

		staffEmploymentInfoMapper.insertSelective(staffEmploymentInfo);
		dealTags(id, staffEmploymentInfo.getTags());

		// 处理未来排班
		removeFutureRoster(staffEmploymentInfo, currentUser, request);
		removeRegionCentreRoster(staffEmploymentInfo, currentUser, request);

		return successResult();
	}

	private ServiceResult<Object> validateEmploymentInfo(StaffEmploymentInfo staffEmploymentInfo, UserInfoVo currentUser) {
		// 只有Director,Centre Manager,Second In Charge才有权限
		if (!containRoles(currentUser.getRoleInfoVoList(), Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge)) {
			return failResult(getTipMsg("no.permissions"));
		}
		// 验证work time
		if (!validateWorkTime(staffEmploymentInfo)) {
			return failResult(getTipMsg("staff_employ_time_error"));
		}
		// Effective from Date不能是过去时间
		if (staffEmploymentInfo.getEffectiveDate().before(getCurrentDate())) {
			return failResult(getTipMsg("staff_effective_date"));
		}
		// 验证Employment Start Date,Employment Last Day
		UserInfo userInfo = userInfoMapper.getUserInfoByAccountId(staffEmploymentInfo.getAccountId());
		if (staffEmploymentInfo.getLastDay() != null && userInfo.getStaffInDate() != null && staffEmploymentInfo.getLastDay().before(userInfo.getStaffInDate())) {
			return failResult(getTipMsg("staff_employ_endDay"));
		}
		// 验证解雇日期后有无排班
		if (endDateRosterValidate(staffEmploymentInfo)) {
			return failResult(getTipMsg("staff_end_date_roster"));
		}
		// 验证解雇日期后有无请假
		if (endDateLeaveValidate(staffEmploymentInfo)) {
			return failResult(getTipMsg("staff_end_date_leave"));
		}
		return successResult();
	}

	private boolean validateWorkTime(StaffEmploymentInfo info) {
		if (info.getMonday() != null && info.getMonday() && !validateTime(info.getMonStartTime(), info.getMonEndTime())) {
			return false;
		}
		if (info.getTuesday() != null && info.getTuesday() && !validateTime(info.getTueStartTime(), info.getTueEndTime())) {
			return false;
		}
		if (info.getWednesday() != null && info.getWednesday() && !validateTime(info.getWedStartTime(), info.getWedEndTime())) {
			return false;
		}
		if (info.getThursday() != null && info.getThursday() && !validateTime(info.getThuStartTime(), info.getThuEndTime())) {
			return false;
		}
		if (info.getFriday() != null && info.getFriday() && !validateTime(info.getFriStartTime(), info.getFriEndTime())) {
			return false;
		}
		return true;
	}

	private boolean validateTime(String startTime, String endTime) {
		SimpleDateFormat format = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
		try {
			Date sDate = format.parse(startTime);
			Date eDate = format.parse(endTime);
			if (!(eDate.getTime() > sDate.getTime())) {
				return false;
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return true;
	}

	private ServiceResult<Object> validateRegionCentreRoster(StaffEmploymentInfo info) {
		if (info.getConfirm2() != 0) {
			return successResult();
		}
		// 获取当前EmploymentInfo
		StaffEmploymentInfo dbInfo = staffEmploymentInfoMapper.getCurrentEmploy(info.getAccountId());
		// 获取Staff的排班
		List<RosterStaffInfo> list = rosterStaffInfoMapper.getListByCondition(info.getAccountId(), info.getCentreId(), info.getRegion());
		if (ListUtil.isNotEmpty(list)) {
			if (dbInfo == null) {
				return failResult(3);
			}
			if (info.getRegion() != dbInfo.getRegion()) {
				if (info.getRegion() == null) {
					return failResult(3);
				}
				if (info.getRegion() != 0) {
					return failResult(3);
				}
			}
		}
		// 获取当前Staff
		UserInfo userInfo = userInfoMapper.getUserInfoByAccountId(info.getAccountId());
		if (info.getCentreId() == null && userInfo.getCentersId() == null) {
			return successResult();
		}
		if ((info.getCentreId() != null && !info.getCentreId().equals(userInfo.getCentersId())) && ListUtil.isNotEmpty(list)) {
			return failResult(4);
		}
		if (userInfo.getCentersId() != null && !userInfo.getCentersId().equals(info.getCentreId()) && ListUtil.isNotEmpty(list)) {
			return failResult(4);
		}
		return successResult();
	}

	private boolean endDateLeaveValidate(StaffEmploymentInfo info) {
		int count = leaveInfoMapper.haveLeaveAfterDay(info.getAccountId(), info.getLastDay());
		return count > 0 ? true : false;
	}

	private boolean endDateRosterValidate(StaffEmploymentInfo info) {
		int count = rosterStaffInfoMapper.havaRoster(info.getAccountId(), info.getLastDay());
		return count > 0 ? true : false;
	}

	private void removeFutureRoster(StaffEmploymentInfo info, UserInfoVo currentUser, HttpServletRequest request) {
		// 如果不为2则不许要处理
		if (info.getConfirm() != 2) {
			return;
		}
		for (RosterStaffInfo r : info.getRosterStaffInfos()) {
			rosterService.deleteRosterStaff(r.getId(), currentUser, false, request);
		}
	}

	private void removeRegionCentreRoster(StaffEmploymentInfo info, UserInfoVo currentUser, HttpServletRequest request) {
		if (info.getConfirm2() != 2) {
			return;
		}
		List<RosterStaffInfo> list = rosterStaffInfoMapper.getListByCondition(info.getAccountId(), info.getCentreId(), info.getRegion());
		for (RosterStaffInfo r : list) {
			rosterService.deleteRosterStaff(r.getId(), currentUser, false, request);
		}
	}

	private ServiceResult<Object> futureRosterValidate(StaffEmploymentInfo info) {
		if (info.getConfirm() != 0) {
			return successResult();
		}
		ServiceResult<Object> result = rosterService.validateFutureRoster(info);
		if (!result.isSuccess()) {
			return result;
		}
		return result;
	}

	private void dealTags(String employId, List<String> tags) {
		for (String tag : tags) {
			TaggingsInfo info = new TaggingsInfo();
			info.setEmploymentTagsId(employId);
			info.setTagId(tag);
			taggingsInfoMapper.insert(info);
		}
	}

	private void syncInfo(StaffEmploymentInfo staffEmploymentInfo, UserInfoVo currentUser) {
		// 同步用户信息
		UserInfo userInfo = userInfoMapper.getUserInfoByAccountId(staffEmploymentInfo.getAccountId());
		userInfo.setCentersId(staffEmploymentInfo.getCentreId());
		userInfo.setRoomId(staffEmploymentInfo.getRoomId());
		userInfo.setGroupId(staffEmploymentInfo.getGroupId());
		userInfo.setStaffType(staffEmploymentInfo.getStaffType());
		userInfoMapper.updateByPrimaryKey(userInfo);
		// 同步用户角色信息
		roleInfoMapper.removeByAccountId(staffEmploymentInfo.getAccountId());
		List<String> roles = new ArrayList<String>();
		roles.add(staffEmploymentInfo.getPrimaryRole());
		roles.addAll(staffEmploymentInfo.getTags());
		// Casual
		if (staffEmploymentInfo.getStaffType() != null && staffEmploymentInfo.getStaffType() == StaffType.Casual.getValue()) {
			String id = roleInfoMapper.selectRoleIdByValue(Role.Casual.getValue());
			roles.add(id);
		}

		for (String role : roles) {
			AccountRoleInfo info = new AccountRoleInfo();
			info.setId(UUID.randomUUID().toString());
			info.setAccountId(staffEmploymentInfo.getAccountId());
			info.setRoleId(role);
			info.setDeleteFlag(DeleteFlag.Default.getValue());
			info.setCreateTime(new Date());
			info.setCreateAccountId(currentUser == null ? "JOB" : currentUser.getAccountInfo().getId());
			accountRoleInfoMapper.insert(info);
		}
		// 同步园长和Group负责人
		List<AccountRoleInfo> roleInfoList = accountRoleInfoMapper.getRoleInfoByAccountId(staffEmploymentInfo.getAccountId());
		updateCenterOrGroupManager(roleInfoList, userInfo);
	}

	@Override
	public ServiceResult<Object> updateEmploymentRequest(String id, UserInfoVo currentUser) {
		// 只有Director,Centre Manager,Second In Charge才有权限
		if (!containRoles(currentUser.getRoleInfoVoList(), Role.CEO, Role.CentreManager, Role.EducatorSecondInCharge)) {
			return failResult(getSystemMsg("no.permissions"));
		}
		staffEmploymentInfoMapper.cancelRequest(id);
		return successResult();
	}

	@Override
	public void dealEmployment(Date now) {
		logger.info("dealEmployment==>start");
		// 执行Employment
		List<StaffEmploymentInfo> list = staffEmploymentInfoMapper.getEmployByEffectiveDay(now);
		for (StaffEmploymentInfo s : list) {
			// 同步数据
			syncInfo(s, null);
			// 更改状态
			s.setCurrent(true);
			s.setStatus(EmployStatus.Executed.getValue());
			// 去除当前Employment
			staffEmploymentInfoMapper.cancelCurrentEmploy(s.getAccountId());

			staffEmploymentInfoMapper.updateByPrimaryKeySelective(s);
		}
		logger.info("dealEmployment==>end");
	}

	@Override
	public void dealAchiveStaff(Date now) {
		logger.info("dealAchiveStaff==>start");
		// 归档员工
		List<StaffEmploymentInfo> list = staffEmploymentInfoMapper.getEmployByLastDay(now);
		for (StaffEmploymentInfo s : list) {
			UserInfo userInfo = userInfoMapper.getUserInfoByAccountId(s.getAccountId());
			// 归档
			staffMapper.updateAchive(userInfo.getId(), String.valueOf(0));
			// 取消该员工的request的leave
			leaveService.cancelRequestLeave(userInfo.getId());
		}
		logger.info("dealAchiveStaff==>end");
	}

	/**
	 * 生成Card ID
	 * 
	 * @author hxzhang 2019年3月14日
	 * @return
	 */
	private String buildCardId() {
		StringBuffer sb;
		int count = 0;
		do {
			sb = new StringBuffer();
			String str = "abcdefghijklm1234567890nopqrstuvwxyz";
			Random random = new Random();
			// 长度为几就循环几次
			for (int i = 0; i < 10; i++) {
				int number = random.nextInt(36);
				sb.append(str.charAt(number));
			}
			count = accountInfoMapper.getCountByCardId(sb.toString());
		} while (count != 0);
		return sb.toString();
	}

	@Override
	public void dealStaffCardId() {
		List<String> list = staffMapper.getAllStaffAccountId();
		for (String accountId : list) {
			staffMapper.savaCardIdById(accountId, buildCardId());
		}
	}

	@Override
	public void dealStaffTags() {
		List<String> list = staffEmploymentInfoMapper.getLostTags();
		for (String accountId : list) {
			StaffEmploymentInfo info = staffEmploymentInfoMapper.getCurrentEmploy(accountId);
			List<TaggingsInfo> taggingsInfos = taggingsInfoMapper.getListByEmployId(info.getId());
			List<AccountRoleInfo> accountRoleInfos = accountRoleInfoMapper.getRoleInfoByAccountId(accountId);
			for (TaggingsInfo t : taggingsInfos) {
				boolean have = false;
				for (AccountRoleInfo accountRoleInfo : accountRoleInfos) {
					if (t.getTagId().equals(accountRoleInfo.getRoleId())) {
						have = true;
						System.err.println(accountId + " -- " + t.getTagId());
					}
				}
				if (have) {
					continue;
				}
				AccountRoleInfo accountRoleInfo = new AccountRoleInfo();
				accountRoleInfo.setId(UUID.randomUUID().toString());
				accountRoleInfo.setAccountId(accountId);
				accountRoleInfo.setRoleId(t.getTagId());
				accountRoleInfo.setTempFlag(false);
				accountRoleInfo.setDeleteFlag((short) 0);
				accountRoleInfoMapper.insert(accountRoleInfo);
			}
		}
	}
}
