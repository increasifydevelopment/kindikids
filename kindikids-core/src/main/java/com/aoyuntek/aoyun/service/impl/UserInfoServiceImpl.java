package com.aoyuntek.aoyun.service.impl;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.condtion.UserCondition;
import com.aoyuntek.aoyun.condtion.UserDetailCondition;
import com.aoyuntek.aoyun.dao.AccountInfoMapper;
import com.aoyuntek.aoyun.dao.ChangePswRecordInfoMapper;
import com.aoyuntek.aoyun.dao.FamilyInfoMapper;
import com.aoyuntek.aoyun.dao.NewsInfoMapper;
import com.aoyuntek.aoyun.dao.RoomInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.entity.po.AccountInfo;
import com.aoyuntek.aoyun.entity.po.ChangePswRecordInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.vo.NewsfeedOtherVo;
import com.aoyuntek.aoyun.entity.vo.RecaptchaVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.base.UserVo;
import com.aoyuntek.aoyun.enums.AccountActiveStatus;
import com.aoyuntek.aoyun.enums.ArchivedStatus;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.EnrolledState;
import com.aoyuntek.aoyun.enums.NewsType;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.enums.TokenStatus;
import com.aoyuntek.aoyun.factory.UserFactory;
import com.aoyuntek.aoyun.service.INewsInfoService;
import com.aoyuntek.aoyun.service.IUserInfoService;
import com.aoyuntek.aoyun.uitl.EmailUtil;
import com.aoyuntek.aoyun.uitl.GsonUtil;
import com.aoyuntek.aoyun.uitl.SaltMD5Util;
import com.theone.aws.util.AwsEmailUtil;
import com.theone.common.util.ServiceResult;
import com.theone.date.util.DateUtil;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

@Service
public class UserInfoServiceImpl extends BaseWebServiceImpl<UserInfo, UserInfoMapper> implements IUserInfoService {

	/**
	 * 日志
	 */
	public static Log logger = LogFactory.getLog(UserInfoServiceImpl.class);
	@Autowired
	private UserInfoMapper userInfoMapper;
	@Autowired
	private AccountInfoMapper accountInfoMapper;
	@Autowired
	private ChangePswRecordInfoMapper changePswRecordInfoMapper;
	@Autowired
	private RoomInfoMapper roomInfoMapper;
	@Autowired
	private FamilyInfoMapper familyInfoMapper;
	@Autowired
	private NewsInfoMapper newsInfoMapper;
	@Autowired
	private UserFactory userFactory;

	/**
	 * newsInfoService
	 */
	@Autowired
	private INewsInfoService newsInfoService;

	@Value("#{auth}")
	private Properties authProperties;

	@Override
	public UserInfo getUserInfoByAccountId(String childId) {
		return userInfoMapper.getUserInfoByAccountId(childId);
	}

	@Override
	public ServiceResult<UserInfoVo> saveLogin(String account, String password, String googleVlidateId) {
		// 设置查询条件
		UserCondition condition = new UserCondition();
		condition.setAccount(account);
		condition.setPsw(SaltMD5Util.saltMd5(getSystemMsg("salt"), password));
		// MD5Util.string2MD5()
		// 获取用户信息
		List<UserInfoVo> userList = userInfoMapper.getUserList(condition);
		// 判空
		Integer errorNum = accountInfoMapper.getErrorNumByAccount(account);
		if (userList == null || userList.size() == 0) {
			if (errorNum == null) {
				return failResult(getTipMsg("login.emailAddressError"));
			}
			// 记录密码错误次数,次数达到三次进行Google验证
			if (!validate(errorNum, googleVlidateId)) {
				return failResult(3, getTipMsg("login.emailPasswordError"));
			}
			errorNum = errorNum + 1;
			accountInfoMapper.addErrorNum(errorNum + 1, account);

			return failResult(getTipMsg("login.emailPasswordError"));
		}

		UserInfoVo user = userList.get(0);

		user.setFutureEnrolled(isFutureEnrolled(user.getUserInfo().getFamilyId()));

		user.setRoleInfoVoList(userInfoMapper.getRoleByAccount(user.getAccountInfo().getId()));
		// 判断是否为小孩登录
		if (containRole(Role.Children, user.getRoleInfoVoList()) > -1) {
			return failResult(getTipMsg("login.fail.kid"));
		}

		// 如果为家长，家长需要看到所有小孩所在园，tag小孩的newsfeed
		short flag = containRole(Role.Parent, user.getRoleInfoVoList());
		if (flag > -1) {
			String parentAccountId = user.getAccountInfo().getId();
			// child in center list
			List<String> centers = userInfoMapper.getCenterByMyKid(parentAccountId);
			user.setParentCenterIds(centers);

			String parentUserId = user.getUserInfo().getId();
			List<String> roomIds = roomInfoMapper.getRoomsByChild(parentUserId);
			user.setParentRoomsIds(roomIds);

			// 判断该家庭有正常入园的小孩,则将该家长改为可以登录
			List<UserInfo> childList = userInfoMapper.getEnrolledChildByFamilyId2(user.getUserInfo().getFamilyId());
			AccountInfo accountInfo = user.getAccountInfo();
			if (accountInfo.getStatus() == AccountActiveStatus.Disabled.getValue() && (null != childList && childList.size() != 0)) {
				accountInfo.setStatus(AccountActiveStatus.Enabled.getValue());
				accountInfoMapper.updateByPrimaryKeySelective(accountInfo);
				// 清空归档时间
				familyInfoMapper.clearArchiveTimeByFamilyId(user.getUserInfo().getFamilyId());
				return successResult(user);
			}
		}

		// 用户是否被禁用
		if (user.getAccountInfo().getStatus() == AccountActiveStatus.Disabled.getValue()) {
			return failResult(getTipMsg("login.emailstatus"));
		}
		// 登录成功,清空错误次数
		accountInfoMapper.addErrorNum(0, account);
		return successResult(user);
	}

	public boolean validate(int errorNum, String googleVlidateId) {
		if (errorNum > 2) {
			return false;
		}
		if (StringUtil.isNotEmpty(googleVlidateId) && !reCAPTCHA(googleVlidateId)) {
			return false;
		}
		return true;
	}

	public boolean reCAPTCHA(String googleVlidateId) {
		if (StringUtil.isEmpty(googleVlidateId)) {
			return false;
		}
		try {
			CloseableHttpClient httpClient = HttpClients.createDefault();
			String url = getSystemMsg("reCAPTCHA_Url");
			url = url + "?secret=" + getSystemMsg("reCAPTCHA_Secretkey") + "&response=" + googleVlidateId;
			HttpPost httpPost = new HttpPost(url);
			httpPost.setHeader("Content-type", "application/json");
			CloseableHttpResponse httpResponse = httpClient.execute(httpPost);
			HttpEntity entityResponse = httpResponse.getEntity();
			RecaptchaVo recaptchaVo = (RecaptchaVo) GsonUtil.jsonToBean(EntityUtils.toString(entityResponse, "utf-8"), RecaptchaVo.class);
			return recaptchaVo.isSuccess();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 判断是否事未来入园的孩子家长
	 * 
	 * @author hxzhang 2018年8月23日 下午1:57:55
	 *
	 * @param familyId
	 * @return
	 */
	public boolean isFutureEnrolled(String familyId) {
		if (StringUtil.isEmpty(familyId)) {
			return false;
		}
		// 获取该家庭所有的孩子
		List<UserInfo> childs = userInfoMapper.getChildListByFamilyId(familyId);
		if (ListUtil.isEmpty(childs)) {
			return false;
		}
		for (UserInfo child : childs) {
			if (child.getEnrolled() == EnrolledState.Enrolled.getValue()) {
				return false;
			}
		}
		return true;
	}

	@Override
	public ServiceResult<Object> dealChangePwdByMail(String account) {
		// 设置查询条件
		UserCondition condition = new UserCondition();
		condition.setAccount(account);
		// 获取用户信息
		List<UserInfoVo> userList = userInfoMapper.getUserList(condition);
		// 判空
		if (userList == null || userList.size() <= 0) {
			return failResult(getTipMsg("resetPwd.sendChangePwdByMail.noUser"));
		}
		// 账号是否被禁用
		if (userList.get(0).getAccountInfo().getStatus() == AccountActiveStatus.Disabled.getValue()) {
			return failResult(getTipMsg("resetPwd.sendChangePwdByMail.disabled"));
		}
		// 发送邮件
		UserInfo user = userList.get(0).getUserInfo();
		String email = user.getEmail();
		String name = userFactory.getUserName(user);
		String sendAddress = getSystemMsg("mailHostAccount");
		String sendName = getSystemMsg("email_name");
		String receiverAddress = email;
		String receiverName = name;
		String sub = getSystemEmailMsg("forgot_password_title");
		String token = UUID.randomUUID().toString();
		String msg = MessageFormat.format(getSystemEmailMsg("forgot_password_content"), name, getSystemMsg("rootUrl"), token);
		String html = EmailUtil.replaceHtml(sub, msg);
		String email_key = getSystemMsg("email_key");
		sendMail(sendAddress, sendName, receiverAddress, receiverName, sub, html, true, email_key);
		// 将token存入数据库
		String accountId = userList.get(0).getAccountInfo().getId();
		ChangePswRecordInfo record = new ChangePswRecordInfo();
		record.setToken(token);
		record.setCreateTime(new Date());
		record.setAccountId(accountId);
		record.setStatus(TokenStatus.Unused.getValue());
		record.setId(UUID.randomUUID().toString());
		record.setDeleteFlag(DeleteFlag.Default.getValue());
		// 先删除过时邮件记录
		changePswRecordInfoMapper.deleteByAccount(accountId);
		changePswRecordInfoMapper.insert(record);
		// 返回结果
		return failResult(getTipMsg("resetPwd.sendChangePwdByMail.success"));
	}

	/**
	 * @description 并发发送email
	 * @author bqbao
	 * @create 2016年6月16日下午2:32:57
	 * @version 1.0
	 * @param sendAddress
	 *            发送人
	 * @param mail
	 *            接收人地址
	 * @param mailName
	 *            接收人名
	 * @param sub
	 *            主题
	 * @param msg
	 *            内容
	 * @param isHtml
	 *            是否html
	 * @param emial_key
	 *            aws邮件key
	 */
	private void sendMail(final String sendAddress, final String sendName, final String receiverAddress, final String receiverName, final String sub, final String msg,
			final boolean isHtml, final String emial_key) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					AwsEmailUtil a = new AwsEmailUtil();
					a.sendgrid(sendAddress, sendName, receiverAddress, receiverName, sub, msg, isHtml, null, null, emial_key);
				} catch (Exception e) {
					logger.error("发送邮件失败" + e.toString());
				}
			}
		}).start();
	}

	@Override
	public ServiceResult<UserInfoVo> dealChangePwd(String token, String newPwd) {
		// 获取密码更改记录
		ChangePswRecordInfo changePswRecordInfo = changePswRecordInfoMapper.getRecordByToken(token);
		// 验证Code合法性，并找到关联用户
		ServiceResult<UserInfo> verificationResult = verificationLink(changePswRecordInfo);
		if (!verificationResult.isSuccess()) {
			return failResult(verificationResult.getMsg());
		}
		// 修改用户密码
		AccountInfo accountInfo = accountInfoMapper.selectByPrimaryKey(changePswRecordInfo.getAccountId());
		accountInfo.setPsw(SaltMD5Util.saltMd5(getSystemMsg("salt"), newPwd));
		accountInfo.setStatus(ArchivedStatus.UnArchived.getValue());
		accountInfoMapper.updateByPrimaryKey(accountInfo);
		// 删除token(更改链接为无效链接)
		changePswRecordInfo.setStatus(TokenStatus.Used.getValue());
		changePswRecordInfo.setDeleteFlag(DeleteFlag.Default.getValue());
		changePswRecordInfoMapper.updateByPrimaryKey(changePswRecordInfo);

		UserCondition condition = new UserCondition();
		condition.setAccount(accountInfo.getAccount());
		condition.setPsw(SaltMD5Util.saltMd5(getSystemMsg("salt"), newPwd));
		List<UserInfoVo> userList = userInfoMapper.getUserList(condition);
		UserInfoVo user = userList.get(0);
		user.setRoleInfoVoList(userInfoMapper.getRoleByAccount(user.getAccountInfo().getId()));
		short flag = containRole(Role.Parent, user.getRoleInfoVoList());
		if (flag > -1) {
			String parentAccountId = user.getAccountInfo().getId();
			List<String> centers = userInfoMapper.getCenterByMyKid(parentAccountId);
			user.setParentCenterIds(centers);
		}
		// 返回结果 result.setSuccess(true);
		return successResult(getTipMsg("resetPwd.reset.success"), user);
	}

	/**
	 * @description 验证Code合法性，并找到关联用户
	 * @author bbq
	 * @param changePswRecordInfo
	 *            密码修改记录
	 * @return ServiceResult<UserInfo> 结果集
	 */
	private ServiceResult<UserInfo> verificationLink(ChangePswRecordInfo changePswRecordInfo) {
		// 判空
		if (null == changePswRecordInfo) {
			return failResult(getTipMsg("resetPwd.invalid"));
		}
		// t判断oken是否已使用
		if (changePswRecordInfo.getStatus() == TokenStatus.Used.getValue()) {
			return failResult(getTipMsg("resetPwd.reset.linkinvalid.used"));
		}
		// 判断时间是否大于24小时
		int seconds = DateUtil.compareDateInSeconds(new Date(), changePswRecordInfo.getCreateTime());
		if (seconds / 3600 > Integer.parseInt(getSystemMsg("linkOutTime"))) {
			return failResult(getTipMsg("resetPwd.reset.linkinvalid.overdue"));
		}
		// 返回结果
		return successResult();
	}

	@Override
	public boolean validateEmail(String email, String userId) {
		UserDetailCondition condition = new UserDetailCondition();
		condition.setEmail(email);
		condition.setUserId(userId);
		List<UserInfo> list = userInfoMapper.getByCondition(condition);
		if (list != null && list.size() >= 1) {
			return false;
		}
		return true;
	}

	@Override
	public List<UserInfo> validateEmail(String email) {
		UserDetailCondition condition = new UserDetailCondition();
		condition.setEmail(email);
		return userInfoMapper.getByCondition(condition);
	}

	@Override
	public ServiceResult<Object> updateCancelSendActiveEmail(String id) {
		int count = accountInfoMapper.updatePswByUserId(id);
		if (count == 0) {
			return failResult(getTipMsg("operation_failed"));
		}
		return successResult(getTipMsg("operation_success"));
	}

	@Override
	public void sendUserNewsFeedByBirthday(Date date) {
		log.info("sendTodaysMenu|start|date:" + date);

		List<UserVo> userList = userInfoMapper.getListByBirthday(date);

		for (int i = 0, m = userList.size(); i < m; i++) {
			UserVo user = userList.get(i);
			// 小孩发送newsfeed
			if (user.getUserType() == 2 || user.getUserType() == 0) {
				NewsfeedOtherVo newsInfoVo = new NewsfeedOtherVo();
				// 当前小孩今天是否创建过newsfeed Todo
				int count = newsInfoMapper.getBirthdayListCount(user.getId(), user.getCentersId(), user.getRoomId(), date);
				if (count > 0)
					continue;
				List<String> accounts = new ArrayList<String>();
				accounts.add(user.getAccountId());
				newsInfoVo.setCenterId(user.getCentersId());
				newsInfoVo.setRoomId(user.getRoomId());
				newsInfoVo.setTagAccounts(accounts);
				newsInfoVo.setStatus((short) 1);
				newsInfoVo.setNewsType(NewsType.Birthday);
				newsInfoVo.setContent(getSystemMsg("newsfeed.birthday"));
				newsInfoVo.setCreateTime(date);
				newsInfoVo.setUpdateTime(date);
				newsInfoService.createNewsByOther(newsInfoVo);
			}
		}

	}

	@Override
	public UserInfoVo saveAgree(UserInfoVo currentUser) {
		accountInfoMapper.saveAgree(currentUser.getAccountInfo().getId());
		currentUser.getAccountInfo().setAgreeFlag(true);
		return currentUser;
	}

	@Override
	public ServiceResult<Object> showNotSignOutTip(String accountId) {
		int count = accountInfoMapper.notHaveSignOutByAccountId(accountId);
		if (count != 0) {
			accountInfoMapper.removeNotHaveSignOut(accountId);
			return failResult(2, getTipMsg("nothavesignout"));
		}
		return successResult();
	}
}
