package com.aoyuntek.aoyun.service.attendance;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.aoyuntek.aoyun.condtion.ApproveAddCondition;
import com.aoyuntek.aoyun.condtion.AttendanceCondtion;
import com.aoyuntek.aoyun.condtion.AttendanceManagementCondition;
import com.aoyuntek.aoyun.condtion.ExternalListCondition;
import com.aoyuntek.aoyun.dao.ChangeAttendanceRequestInfoMapper;
import com.aoyuntek.aoyun.entity.po.AttendanceHistoryInfo;
import com.aoyuntek.aoyun.entity.po.ChangeAttendanceRequestInfo;
import com.aoyuntek.aoyun.entity.po.ChildAttendanceInfo;
import com.aoyuntek.aoyun.entity.po.ReplaceChildAttendanceVo;
import com.aoyuntek.aoyun.entity.vo.AttendanceChildVo;
import com.aoyuntek.aoyun.entity.vo.AttendanceReturnVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.service.IBaseWebService;
import com.theone.common.util.ServiceResult;

public interface IAttendanceService extends IBaseWebService<ChangeAttendanceRequestInfo, ChangeAttendanceRequestInfoMapper> {

	/**
	 * 获取一个小孩在一定时间范围内 上课的天数
	 * 
	 * @author dlli5 at 2016年12月6日下午1:17:34
	 * @param accountId
	 * @param beginDate
	 * @param endDate
	 * @return
	 */
	public Integer getChildAttendanceDays(String accountId, Date beginDate, Date endDate);

	/**
	 * 
	 * @author dlli5 at 2016年8月22日下午3:11:22
	 * @param currentId
	 * @param attendanceRequestInfo
	 * @return
	 */
	public ServiceResult<ChildAttendanceInfo> submitChangeAttendanceRequest(UserInfoVo currentUser, ChangeAttendanceRequestInfo attendanceRequestInfo, boolean isOver,
			boolean isFullOver);

	/**
	 * 
	 * @author dlli5 at 2016年8月24日上午8:07:15
	 * @param currentUser
	 * @param centreId
	 * @param roomId
	 * @param weekDate
	 * @return
	 */
	public ServiceResult<AttendanceReturnVo> getAttendanceList(UserInfoVo currentUser, AttendanceCondtion attendanceCondtion);

	/**
	 * 
	 * @author dlli5 at 2016年8月25日下午5:18:37
	 * @param chilId
	 * @param day
	 * @return
	 */
	public ServiceResult<ReplaceChildAttendanceVo> getReplaceChilds(String absenteeId, String chilId, Date day);

	/**
	 * 获取上课安排添加的数据
	 * 
	 * @author dlli5 at 2016年8月30日上午10:40:53
	 * @param chilId
	 * @param day
	 * @return
	 */
	public ServiceResult<ReplaceChildAttendanceVo> getAddChilds(String roomId, Date day);

	/**
	 * 
	 * @author dlli5 at 2016年8月25日下午6:01:21
	 * @param userId
	 * @return
	 */
	public ServiceResult<Map<String, Object>> getAttendance(String userId);

	/**
	 * 
	 * @author dlli5 at 2016年8月28日下午4:51:51
	 * @param currentUser
	 * @param objId
	 * @param type
	 * @return
	 */
	public ServiceResult<Object> getRequestLogObj(UserInfoVo currentUser, String objId, int type);

	/**
	 * \
	 * 
	 * @author dlli5 at 2016年8月28日下午4:51:49
	 * @param currentUser
	 * @param objId
	 * @param value
	 * @return
	 */
	public ServiceResult<Object> updateAttendanceRequest(UserInfoVo currentUser, String objid, short value);

	/**
	 * 
	 * @author dlli5 at 2016年8月29日下午9:20:48
	 * @param absenteeId
	 * @param replaceChildId
	 * @param dayOrInstances
	 * @param type
	 * @return
	 */
	public ServiceResult<Object> dealApproveReplace(UserInfoVo currentUser, String absenteeId, String replaceChildId, Date day, int dayOrInstances, int type,
			boolean overOld);

	/**
	 * 
	 * @author dlli5 at 2016年8月30日下午4:17:57
	 * @param currentUser
	 * @param roomId
	 * @param childId
	 * @param day
	 * @return
	 */
	public ServiceResult<ChildAttendanceInfo> dealApproveAdd(UserInfoVo currentUser, ApproveAddCondition condtion);

	/**
	 * 
	 * @author dlli5 at 2016年8月31日下午2:05:02
	 * @param currentUser
	 * @param childId
	 * @param day
	 * @return
	 */
	public ServiceResult<Object> dealSubtractedAttendance(UserInfoVo currentUser, String centreId, String childId, Date day, int type, boolean cover, boolean interim);

	/**
	 * 
	 * @author dlli5 at 2016年9月12日下午6:04:52
	 * @param currentUser
	 * @param roomId
	 * @return
	 */
	public ServiceResult<List<AttendanceChildVo>> getTodayAttendanceList(String centreId, String findStr, int type);

	/**
	 * 
	 * @author dlli5 at 2016年11月1日下午2:20:56
	 * @param intervalDay
	 * @return
	 */
	public List<AttendanceHistoryInfo> getChangeCentreHistory(int intervalDay, Date day);

	public ServiceResult<Object> updateTemporaryRequest(UserInfoVo currentUser, int type, String objId, short value);

	ServiceResult<Object> getList(UserInfoVo currentUser, AttendanceManagementCondition condition);

	ServiceResult<Object> getExternalList(UserInfoVo currentUser, ExternalListCondition condition);

	String exprotCsv(UserInfoVo currentUser, AttendanceManagementCondition condition);
}
