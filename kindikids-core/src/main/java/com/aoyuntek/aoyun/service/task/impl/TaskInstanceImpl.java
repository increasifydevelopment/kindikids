package com.aoyuntek.aoyun.service.task.impl;

import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.condtion.TaskCondtion;
import com.aoyuntek.aoyun.constants.SystemConstants;
import com.aoyuntek.aoyun.dao.task.TaskFrequencyInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskInstanceInfoMapper;
import com.aoyuntek.aoyun.entity.po.task.TaskInstanceInfo;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.ProgramState;
import com.aoyuntek.aoyun.enums.TaskCategory;
import com.aoyuntek.aoyun.enums.TaskType;
import com.aoyuntek.aoyun.factory.TaskFactory;
import com.aoyuntek.aoyun.service.impl.BaseWebServiceImpl;
import com.aoyuntek.aoyun.service.task.ITaskInstance;
import com.theone.common.util.ServiceResult;
import com.theone.string.util.StringUtil;

@Service
public class TaskInstanceImpl extends BaseWebServiceImpl<TaskInstanceInfo, TaskInstanceInfoMapper> implements ITaskInstance {

    @Autowired
    private TaskFrequencyInfoMapper taskFrequencyInfoMapper;
    @Autowired
    private TaskInstanceInfoMapper taskInstanceInfoMapper;
    @Autowired
    private TaskFactory taskFactory;

    @Override
    public ServiceResult<Object> deleteTaskInstanceByValueId(String valueId) {
        taskInstanceInfoMapper.deleteTaskInstanceByValueId(valueId);
        return successResult();
    }

    @Override
    public ServiceResult<Object> deleteTaskInstanceById(String id) {
        TaskInstanceInfo instanceInfo = taskInstanceInfoMapper.selectByPrimaryKey(id);
        instanceInfo.setDeleteFlag(DeleteFlag.Delete.getValue());
        taskInstanceInfoMapper.updateByPrimaryKey(instanceInfo);
        return successResult();
    }

    @Override
    public ServiceResult<TaskInstanceInfo> addTaskInstance(TaskType taskType, String centreId, String roomId, String groupId, String valueId,
            String createAccountId, Date day) {
        return addTaskInstance(taskType, centreId, roomId, groupId, null, TaskCategory.ROOM, centreId, valueId, createAccountId, day);
    }

    @Override
    public ServiceResult<TaskInstanceInfo> addTaskInstance(TaskType taskType, String centreId, String roomId, String groupId, String valueId,
            String createAccountId, Date startDate, Date endDate) {
        return addTaskInstance(taskType, centreId, roomId, groupId, null, TaskCategory.ROOM, centreId, valueId, createAccountId, startDate, endDate);
    }

    @Override
    public ServiceResult<TaskInstanceInfo> updateTaskInstance(String valueId, Date day) {
        taskInstanceInfoMapper.updateTaskInstanceForDay(valueId, day);
        return successResult();
    }

    @Override
    public ServiceResult<TaskInstanceInfo> addTaskInstance(TaskType taskType, String centreId, String roomId, String obj_id,
            TaskCategory taskCategory, String taskModelId, String valueId, String createAccountId, Date day) {
        return addTaskInstance(taskType, centreId, roomId, null, obj_id, taskCategory, taskModelId, valueId, createAccountId, day);
    }

    @Override
    public ServiceResult<TaskInstanceInfo> addTaskInstance(TaskType taskType, String centreId, String roomId, String groupId, String obj_id,
            TaskCategory taskCategory, String taskModelId, String valueId, String createAccountId, Date day) {
        Date[] dates = taskFactory.dealDate(taskType, taskModelId, day);
        return addTaskInstance(taskType, centreId, roomId, groupId, obj_id, taskCategory, taskModelId, valueId, createAccountId, dates[0], dates[1]);
    }

    @Override
    public ServiceResult<TaskInstanceInfo> addTaskInstance(TaskType taskType, String centreId, String roomId, String groupId, String obj_id,
            TaskCategory taskCategory, String taskModelId, String valueId, String createAccountId, Date startDate, Date endDate) {
        createAccountId = StringUtil.isEmpty(createAccountId) ? SystemConstants.JobCreateAccountId : createAccountId;
        TaskInstanceInfo model = new TaskInstanceInfo();
        model.setId(UUID.randomUUID().toString());
        model.setCentreId(centreId);
        model.setRoomId(roomId);
        model.setGroupId(groupId);
        model.setObjId(obj_id);
        model.setObjType(taskCategory.getValue());
        model.setCreateAccountId(createAccountId);
        model.setCreateTime(new Date());
        model.setDeleteFlag(DeleteFlag.Default.getValue());
        model.setStatu(ProgramState.Pending.getValue());
        model.setTaskModelId(taskModelId);
        model.setTaskType(taskType.getValue());
        model.setValueId(valueId);
        model.setBeginDate(startDate);
        model.setEndDate(endDate);
        insert(model);
        return successResult(model);
    }

    @Override
    public int getTaskInstance(TaskType taskType, String valueId, Date date) {
        return taskInstanceInfoMapper.getTaskInstanceInfoList(valueId, taskType.getValue(), date);
    }
}
