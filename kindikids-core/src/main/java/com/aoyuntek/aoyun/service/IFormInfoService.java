package com.aoyuntek.aoyun.service;

import com.aoyuntek.aoyun.dao.FormInfoMapper;
import com.aoyuntek.aoyun.entity.po.FormInfo;
import com.aoyuntek.aoyun.entity.vo.FormInfoDataVO;
import com.aoyuntek.aoyun.entity.vo.FormInfoVO;
import com.aoyuntek.aoyun.entity.vo.InstanceInfoDataVO;
import com.aoyuntek.aoyun.entity.vo.ValueInfoVO;
import com.theone.common.util.ServiceResult;

public interface IFormInfoService  extends IBaseWebService<FormInfo, FormInfoMapper>{
	ServiceResult<String> saveForm(FormInfoVO formInfoVO);
	
	ServiceResult<FormInfoDataVO> getForm(String formId);
	
	ServiceResult<Object> getInstanceTemplate(String code,String instanceId);
	
	ServiceResult<ValueInfoVO> getInstance(String instanceId);
	
	ServiceResult<String> saveInstance(ValueInfoVO valueInfoVO) throws Exception;
	
	ServiceResult<String> createInstance(String code) throws Exception;
}
