package com.aoyuntek.aoyun.service.meeting;

import java.util.Date;
import java.util.List;

import com.aoyuntek.aoyun.condtion.MeetingCondition;
import com.aoyuntek.aoyun.dao.meeting.MeetingTemplateInfoMapper;
import com.aoyuntek.aoyun.entity.po.meeting.MeetingTemplateInfo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.meeting.MeetingTempVo;
import com.aoyuntek.aoyun.service.IBaseWebService;
import com.theone.common.util.ServiceResult;

/**
 * @description 会议模版业务逻辑接口
 * @author Hxzhang 2016年10月13日上午10:35:38
 */
public interface IMeetingTempService extends IBaseWebService<MeetingTemplateInfo, MeetingTemplateInfoMapper> {
    /**
     * @description 获取会议模版列表
     * @author hxzhang
     * @create 2016年10月13日上午10:38:35
     */
    public ServiceResult<Object> getMeetingTempList(MeetingCondition condition);

    /**
     * @description 新增或编辑会议模版
     * @author hxzhang
     * @create 2016年10月13日上午11:52:15
     */
    public ServiceResult<Object> addOrUpdateMeetingTemp(MeetingTempVo meetingTempVo, UserInfoVo currentUser);

    /**
     * @description 获取会议模版信息
     * @author hxzhang
     * @create 2016年10月13日下午3:33:24
     */
    public ServiceResult<Object> getMeetingTempInfo(String id);

    /**
     * @description 验证会议模版是否重复
     * @author hxzhang
     * @create 2016年10月14日上午9:47:43
     */
    public boolean validateMeetingTempName(String name, String id);

    /**
     * @description 禁用启用邮件模版
     * @author hxzhang
     * @create 2016年10月14日上午11:33:50
     */
    public ServiceResult<Object> saveOperaMeetingTemp(String id, short state, UserInfoVo currentUser);

    /**
     * @description 保存会议记录
     * @author hxzhang
     * @create 2016年10月17日下午2:55:28
     */
    public void createMeetingTempForMeetingTimeTask(Date now);

    /**
     * @description 以SelecterPo查出所有最新的会议模版
     * @author hxzhang
     * @create 2016年10月18日上午9:13:47
     */
    public List<SelecterPo> getMeetingTempForSelecterPo();
}
