package com.aoyuntek.aoyun.service.impl;

import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.dao.ChangePswRecordInfoMapper;
import com.aoyuntek.aoyun.entity.po.ChangePswRecordInfo;
import com.aoyuntek.aoyun.service.IChangePswRecordService;

@Service
public class ChangePswRecordServiceImpl extends BaseWebServiceImpl<ChangePswRecordInfo, ChangePswRecordInfoMapper> implements IChangePswRecordService {

}
