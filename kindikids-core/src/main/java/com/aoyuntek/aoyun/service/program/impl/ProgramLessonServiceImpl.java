package com.aoyuntek.aoyun.service.program.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.aoyuntek.aoyun.dao.AccountInfoMapper;
import com.aoyuntek.aoyun.dao.AttachmentInfoMapper;
import com.aoyuntek.aoyun.dao.RoomInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.dao.program.ProgramIntobsleaInfoMapper;
import com.aoyuntek.aoyun.dao.program.ProgramLessonInfoMapper;
import com.aoyuntek.aoyun.dao.program.ProgramNqsInfoMapper;
import com.aoyuntek.aoyun.dao.program.ProgramRegisterItemInfoMapper;
import com.aoyuntek.aoyun.dao.program.ProgramRegisterTaskInfoMapper;
import com.aoyuntek.aoyun.dao.program.ProgramTagsInfoMapper;
import com.aoyuntek.aoyun.dao.program.ProgramTaskExgrscInfoMapper;
import com.aoyuntek.aoyun.dao.program.ProgramWeeklyEvalutionInfoMapper;
import com.aoyuntek.aoyun.dao.program.TaskProgramFollowUpInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskInstanceInfoMapper;
import com.aoyuntek.aoyun.entity.po.AttachmentInfo;
import com.aoyuntek.aoyun.entity.po.RoomGroupInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramLessonInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramNqsInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramTagsInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramWeeklyEvalutionInfo;
import com.aoyuntek.aoyun.entity.po.program.TaskProgramFollowUpInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskInstanceInfo;
import com.aoyuntek.aoyun.entity.vo.ActivitiesVo;
import com.aoyuntek.aoyun.entity.vo.FileVo;
import com.aoyuntek.aoyun.entity.vo.NewsfeedOtherVo;
import com.aoyuntek.aoyun.entity.vo.NqsVo;
import com.aoyuntek.aoyun.entity.vo.ProgramLessonInfoVo;
import com.aoyuntek.aoyun.entity.vo.ProgramNewsFeedVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.NewsStatus;
import com.aoyuntek.aoyun.enums.NewsType;
import com.aoyuntek.aoyun.enums.ProgramState;
import com.aoyuntek.aoyun.enums.ProgramWeeklyType;
import com.aoyuntek.aoyun.enums.TaskType;
import com.aoyuntek.aoyun.service.INewsInfoService;
import com.aoyuntek.aoyun.service.impl.BaseWebServiceImpl;
import com.aoyuntek.aoyun.service.program.IProgramLessonService;
import com.aoyuntek.aoyun.service.task.ITaskInstance;
import com.aoyuntek.aoyun.uitl.FilePathUtil;
import com.aoyuntek.framework.constants.PropertieNameConts;
import com.theone.aws.util.FileFactory;
import com.theone.common.util.ServiceResult;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

/**
 * 
 * @description program lesson 业务逻辑实现类
 * @author mingwang
 * @create 2016年9月20日上午10:55:24
 * @version 1.0
 */
@Service
public class ProgramLessonServiceImpl extends BaseWebServiceImpl<ProgramLessonInfo, ProgramLessonInfoMapper> implements IProgramLessonService {

	/**
	 * log
	 */
	private static Logger log = Logger.getLogger(ProgramLessonServiceImpl.class);

	/**
	 * ProgramLessonInfoMapper
	 */
	@Autowired
	private ProgramLessonInfoMapper lessonInfoMapper;
	@Autowired
	private AttachmentInfoMapper attachmentInfoMapper;
	@Autowired
	private INewsInfoService newsInfoService;
	@Autowired
	private ProgramTaskExgrscInfoMapper programTaskExgrscInfoMapper;
	@Autowired
	private ProgramIntobsleaInfoMapper programIntobsleaInfoMapper;
	@Autowired
	private TaskProgramFollowUpInfoMapper taskProgramFollowUpInfoMapper;
	@Autowired
	private ITaskInstance taskInstance;
	@Autowired
	private RoomInfoMapper roomInfoMapper;
	@Autowired
	private ProgramRegisterItemInfoMapper registerItemInfoMapper;
	@Autowired
	private TaskInstanceInfoMapper taskInstanceInfoMapper;
	@Autowired
	private ProgramWeeklyEvalutionInfoMapper weeklyEvalutionInfoMapper;
	@Autowired
	private AccountInfoMapper accountInfoMapper;
	@Autowired
	private UserInfoMapper userInfoMapper;
	@Autowired
	private ProgramNqsInfoMapper programNqsInfoMapper;
	@Autowired
	private ProgramTagsInfoMapper programTagsInfoMapper;

	/**
	 * 新增/修改program中Lesson
	 */
	@Override
	public ServiceResult<Object> addOrUpdateLesson(ProgramLessonInfoVo programLessonInfoVo, UserInfoVo currentUser, Date date) {
		log.info("addOrUpdateLesson|start");
		ProgramLessonInfo programLessonInfo = programLessonInfoVo.getProgramLessonInfo();
		ProgramLessonInfo returnLessonInfo = null;
		String lessonId = programLessonInfo.getId();
		log.info("addOrUpdateLesson|lessonId=" + lessonId);
		// 如果lessonId为空，新增programLessonInfo
		if (StringUtil.isEmpty(lessonId)) {
			lessonId = UUID.randomUUID().toString();
			// 保存programLessonInfo
			ProgramLessonInfo saveLessonInfo = saveProgramLessonInfo(lessonId, programLessonInfo, currentUser, date);
			ServiceResult<TaskInstanceInfo> result = taskInstance.addTaskInstance(TaskType.Lesson, saveLessonInfo.getCenterId(), saveLessonInfo.getRoomId(), null,
					saveLessonInfo.getId(), saveLessonInfo.getCreateAccountId(), saveLessonInfo.getDay());
			saveLessonInfo.setTemp1(result.getReturnObj().getId());
			returnLessonInfo = saveLessonInfo;
		} else {
			// 更新programLessonInfo
			ProgramLessonInfo updateLessonInfo = updateProgramLessonInfo(programLessonInfo, currentUser);
			returnLessonInfo = updateLessonInfo;
		}
		// 处理图片
		initImages(lessonId, programLessonInfoVo.getImages());

		dealTagsAccountIds(lessonId, programLessonInfoVo.getTagAccountIds());
		dealNqsList(lessonId, programLessonInfoVo.getNqsVoList());

		log.info("addOrUpdateLesson|end");
		return successResult(getTipMsg("operation_success"), (Object) returnLessonInfo);
	}

	private void dealTagsAccountIds(String taskId, List<String> tagsAccountIds) {
		// 先删除已存在的数据
		programTagsInfoMapper.removeTags(taskId);
		// 重新添加数据
		for (String accountId : tagsAccountIds) {
			ProgramTagsInfo info = new ProgramTagsInfo();
			info.setId(UUID.randomUUID().toString());
			info.setTaskId(taskId);
			info.setAccountId(accountId);
			info.setDeleteFlag(false);
			programTagsInfoMapper.insert(info);
		}
	}

	private void dealNqsList(String taskId, List<NqsVo> nqsVoList) {
		// 先删除已存在数据
		programNqsInfoMapper.removeNqsList(taskId);
		// 重新添加数据
		for (NqsVo vo : nqsVoList) {
			ProgramNqsInfo info = new ProgramNqsInfo();
			info.setId(UUID.randomUUID().toString());
			info.setTaskId(taskId);
			info.setNqsVersion(vo.getVersion());
			info.setDeleteFlag(false);
			programNqsInfoMapper.insert(info);
		}
	}

	/**
	 * 
	 * @description 保存lesson信息
	 * @author mingwang
	 * @create 2016年9月20日上午11:32:10
	 * @version 1.0
	 * @param lessonId
	 * @param programLessonInfo
	 * @param currentUser
	 */
	private ProgramLessonInfo saveProgramLessonInfo(String lessonId, ProgramLessonInfo programLessonInfo, UserInfoVo currentUser, Date date) {
		// 初始化programLessonInfo
		programLessonInfo.setId(lessonId);
		programLessonInfo.setState(ProgramState.Pending.getValue());
		programLessonInfo.setDay(date);
		programLessonInfo.setCenterId(roomInfoMapper.selectCenterIdByRoom(programLessonInfo.getRoomId()));
		programLessonInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
		programLessonInfo.setUpdateTime(new Date());
		programLessonInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
		programLessonInfo.setDeleteFlag(DeleteFlag.Default.getValue());
		// 插入programLessonInfo
		lessonInfoMapper.insertSelective(programLessonInfo);
		return programLessonInfo;
	}

	/**
	 * 
	 * @description 更新lesson信息
	 * @author mingwang
	 * @create 2016年9月20日下午1:57:59
	 * @version 1.0
	 * @param lessonId
	 * @param LessonInfo
	 * @param currentUser
	 * @return
	 */
	private ProgramLessonInfo updateProgramLessonInfo(ProgramLessonInfo lessonInfo, UserInfoVo currentUser) {
		// 通过lessonId查询ProgramLessonInfo
		ProgramLessonInfo selectLessonInfo = lessonInfoMapper.selectByPrimaryKey(lessonInfo.getId());
		selectLessonInfo.setLessonName(lessonInfo.getLessonName());
		selectLessonInfo.setCurriculum(lessonInfo.getCurriculum());
		selectLessonInfo.setUnitName(lessonInfo.getUnitName());
		selectLessonInfo.setInstrctedBy(lessonInfo.getInstrctedBy());
		selectLessonInfo.setResources(lessonInfo.getResources());
		selectLessonInfo.setLearning(lessonInfo.getLearning());
		selectLessonInfo.setExtension(lessonInfo.getExtension());
		selectLessonInfo.setReflection(lessonInfo.getReflection());
		selectLessonInfo.setUpdateTime(new Date());
		selectLessonInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
		// 更新programLessonInfo
		lessonInfoMapper.updateByPrimaryKeySelective(selectLessonInfo);
		return selectLessonInfo;
	}

	/**
	 * 
	 * @description 保存图片
	 * @author mingwang
	 * @create 2016年9月21日上午9:36:19
	 * @version 1.0
	 * @param sourceId
	 * @param images
	 */
	@Override
	public void initImages(String sourceId, List<FileVo> images) {
		// 待保存的List为空，清空数据库里的附件图片并返回
		if (ListUtil.isEmpty(images)) {
			attachmentInfoMapper.updateDeleteBySourceId(sourceId);
			return;
		}
		// 数据库中获取图片List
		List<AttachmentInfo> attachmentInfoFromDb = attachmentInfoMapper.getAttachmentInfoBySourceId(sourceId);
		// 待保存的图片List
		List<FileVo> unDelImages = new ArrayList<FileVo>();
		for (FileVo image : images) {
			unDelImages.add(image);
		}
		for (AttachmentInfo attachmentInfo : attachmentInfoFromDb) {
			// 数据库中的图片和待保存图片逐个比较，标记不同的个数
			int flag = 0;
			for (FileVo image : images) {
				// 数据库图片和待保存图片不同，标记flag+1
				if (!StringUtil.compare(attachmentInfo.getAttachId(), image.getRelativePathName())) {
					flag++;
					// 数据库中的图片和待保存图片有相同，则从待保存的图片List中删除此图片
				} else {
					unDelImages.remove(image);
					break;
				}
				// 数据库中图片和List中的待保存图片都不同，则删除数据库中对应图片
				if (flag == images.size()) {
					// 删除数据库中对应图片
					attachmentInfoMapper.updateDeleteById(attachmentInfo.getId());
				}
			}
		}
		// 待保存图片在数据库中都已存在，返回
		if (ListUtil.isEmpty(unDelImages)) {
			return;
		}
		// 批量插入图片
		try {
			batchUploadAttachments(sourceId, unDelImages, FilePathUtil.PROGRAM_PATH);
		} catch (Exception e) {
			log.info("addOrUpdateLesson|initImages error", e);
			e.printStackTrace();
		}
	}

	/**
	 * 批量上传附件
	 */
	@Override
	public void batchUploadAttachments(String sourceId, List<FileVo> fileVoList, String afterPath) throws Exception {
		if (ListUtil.isEmpty(fileVoList)) {
			return;
		}
		// 删除之前的附件,再更新
		// TODO
		// 循环上传附件
		FileFactory fac = new FileFactory(Region.getRegion(Regions.AP_SOUTHEAST_2), PropertieNameConts.S3);
		List<AttachmentInfo> attachmentInfos = new ArrayList<AttachmentInfo>();
		for (FileVo fileVo : fileVoList) {
			AttachmentInfo attachmentInfo = new AttachmentInfo();
			attachmentInfo.setId(UUID.randomUUID().toString());
			attachmentInfo.setSourceId(sourceId);

			String tempDir = fileVo.getRelativePathName();
			String mainDir = FilePathUtil.getMainPath(afterPath, tempDir);
			String rePath = fac.copyFile(tempDir, mainDir);
			attachmentInfo.setAttachId(rePath);
			attachmentInfo.setAttachName(fileVo.getFileName());
			attachmentInfo.setVisitUrl(fileVo.getVisitedUrl());
			attachmentInfo.setDeleteFlag((short) 0);
			attachmentInfos.add(attachmentInfo);
		}
		attachmentInfoMapper.insterBatchAttachmentInfo(attachmentInfos);
	}

	/**
	 * 删除program中lesson
	 */
	@Override
	public ServiceResult<Object> removeLesson(String lessonId) {
		log.info("removeLesson|start|lessonId=" + lessonId);
		// 删除lesson
		int deleteLesson = lessonInfoMapper.deleteLesson(lessonId);
		// 删除附件图片
		int updateDeleteBySourceId = attachmentInfoMapper.updateDeleteBySourceId(lessonId);
		log.info("removeLesson|end|deleteLesson=" + deleteLesson + "|updateDeleteBySourceId=" + updateDeleteBySourceId);
		// 删除记录表中的记录
		taskInstance.deleteTaskInstanceByValueId(lessonId);
		return successResult(getTipMsg("operation_success"));
	}

	/**
	 * 获取programLessonInfo信息
	 */
	@Override
	public ServiceResult<Object> getLessonInfo(String lessonId) {
		ProgramLessonInfoVo programLessonInfoVo = lessonInfoMapper.getProgramLessonInfo(lessonId);
		List<String> tagAccountIds = programTagsInfoMapper.getTagsAccountIds(lessonId);
		programLessonInfoVo.setTagAccountIds(tagAccountIds);
		if (ListUtil.isNotEmpty(tagAccountIds)) {
			programLessonInfoVo.setSelecterPos(userInfoMapper.getSelecterPoByAccountIds(tagAccountIds));
		}

		List<NqsVo> nqsVoList = programNqsInfoMapper.getNqsVo(lessonId);
		programLessonInfoVo.setNqsVoList(nqsVoList);

		return successResult(getTipMsg("operation_success"), (Object) programLessonInfoVo);
	}

	/**
	 * share to newsfeed (lesson)
	 */
	@Override
	public ServiceResult<Object> saveShareNewsfeed(String objId, ProgramNewsFeedVo programNewsFeedVo, String childId) {
		programNewsFeedVo.setTaskId(objId);
		String newsfeedId = "";
		ServiceResult<Object> result = null;
		NewsfeedOtherVo newsOtherVo = handleNewsfeedOtherVo(programNewsFeedVo);
		Short taskType = programNewsFeedVo.getTaskType();
		UserInfoVo currentUser = programNewsFeedVo.getCurrtenUser();
		// 获取task的roomId,centerId
		RoomGroupInfo orgInfo = null;
		if (containTaskType(taskType, TaskType.MealRegister, TaskType.NappyRegister, TaskType.ToiletRegister, TaskType.SleepRegister)) {
			orgInfo = programIntobsleaInfoMapper.getCenterIdAndRoomIdRegister(objId);
		} else {
			orgInfo = programIntobsleaInfoMapper.getCenterIdAndRoomId(objId);
		}
		newsOtherVo.setCenterId(orgInfo.getCenterId());
		newsOtherVo.setRoomId(orgInfo.getRoomId());
		// Lesson
		if (containTaskType(taskType, TaskType.Lesson)) {
			// 校验
			result = validateNewsfeed(programNewsFeedVo);
			if (!result.isSuccess()) {
				return failResult(result.getMsg());
			}
			String newsTags = getActivies(programNewsFeedVo.getActList(), taskType);
			newsOtherVo.setNewsTags(newsTags);
			ServiceResult<Object> createNewsByOther = newsInfoService.createNewsByOther(newsOtherVo);
			newsfeedId = (String) createNewsByOther.getReturnObj();
			int updateNewsfeedId = lessonInfoMapper.updateNewsfeedId(objId, newsfeedId);
			log.info("shareNewsfeed|updateNewsfeedId=" + updateNewsfeedId);

		} else if (containTaskType(taskType, TaskType.WeeklyEvalution)) {
			// 周五
			result = validateNewsfeed(programNewsFeedVo);
			if (!result.isSuccess()) {
				return failResult(result.getMsg());
			}

			ServiceResult<Object> createNewsByOther = newsInfoService.createNewsByOther(newsOtherVo);
			newsfeedId = (String) createNewsByOther.getReturnObj();
			int updateNewsfeedId = weeklyEvalutionInfoMapper.updateNewsFeed(newsfeedId, objId);
			log.info("shareNewsfeed|WeeklyEvalution=" + updateNewsfeedId);
		} else if (containTaskType(taskType, TaskType.ExploreCurriculum, TaskType.GrowCurriculum, TaskType.SchoolReadiness)) {
			// 校验
			result = validateNewsfeed(programNewsFeedVo);
			if (!result.isSuccess()) {
				return failResult(result.getMsg());
			}
			newsOtherVo.setNewsTags(getActivies(programNewsFeedVo.getActList(), taskType));

			ServiceResult<Object> createNewsByOther = newsInfoService.createNewsByOther(newsOtherVo);
			newsfeedId = (String) createNewsByOther.getReturnObj();
			int count = programTaskExgrscInfoMapper.updateNewsfeedId(objId, newsfeedId);
			log.info("shareNewsfeed|updateNewsfeedId=" + count);

		} else if (containTaskType(taskType, TaskType.InterestFollowup, TaskType.ObservationFollowup, TaskType.LearningStoryFollowup)) {
			// 获取tag小孩的userId
			String userId = programNewsFeedVo.getAccounts().get(0);
			// userId转accountId
			String accountId = accountInfoMapper.selectAccountInfoByUserId(userId).getId();
			// 设置内容
			String content = initDealContent(programNewsFeedVo);
			newsOtherVo.setContent(content);
			// 查询 roomId
			TaskProgramFollowUpInfo followUp = taskProgramFollowUpInfoMapper.selectByPrimaryKey(objId);
			String roomId = programIntobsleaInfoMapper.selectByPrimaryKey(followUp.getTaskId()).getRoomId();
			newsOtherVo.setRoomId(roomId);
			// 设置accountId
			newsOtherVo.setTagAccounts(Arrays.asList(new String[] { accountId }));

			NewsType newsType = getType(taskType, programNewsFeedVo.getSchoolFlag());
			if (newsType != null) {
				newsOtherVo.setNewsType(newsType);
			}
			// 创建newsfed
			ServiceResult<Object> createNewsByOther = newsInfoService.createNewsByOther(newsOtherVo);
			newsfeedId = (String) createNewsByOther.getReturnObj();
			int count = taskProgramFollowUpInfoMapper.updateNewsfeedId(objId, newsfeedId);
			log.info("shareNewsfeed|programIntobsleaInfoMapper=" + count);

		} else if (containTaskType(taskType, TaskType.MealRegister, TaskType.NappyRegister, TaskType.ToiletRegister, TaskType.SleepRegister)) {

			String accountId = programNewsFeedVo.getAccounts().get(0);

			// 设置为tag的小孩
			newsOtherVo.setTagAccounts(Arrays.asList(new String[] { accountId }));
			newsOtherVo.setAccounts(null);
			newsOtherVo.setStatus(NewsStatus.Appreoved.getValue());
			ServiceResult<Object> createNewsByOther = newsInfoService.createNewsByOther(newsOtherVo);
			newsfeedId = (String) createNewsByOther.getReturnObj();
			int count = 0;
			// 更新registerItem中的newsfeedId（如果是sleepRegister，数据库中同一个小孩的所有sleepRegisterItem都需要更新）
			if (taskType == TaskType.SleepRegister.getValue()) {
				count = registerItemInfoMapper.updateSleepNewsFeedId(childId, newsfeedId);
			} else {
				count = registerItemInfoMapper.updateNewsFeedId(objId, newsfeedId);
			}
			objId = registerItemInfoMapper.getTaskIdByItemId(objId);
			log.info("shareNewsfeed|registerItemInfoMapper=" + count);

		} else if (containTaskType(taskType, TaskType.Interest, TaskType.Observation, TaskType.LearningStory)) {
			String content = initDealContent(programNewsFeedVo);
			newsOtherVo.setContent(content);

			String accountId = programNewsFeedVo.getAccounts().get(0);
			newsOtherVo.setAccounts(null);
			// 设置tag小孩
			newsOtherVo.setTagAccounts(Arrays.asList(new String[] { accountId }));
			NewsType newsType = getType(taskType, programNewsFeedVo.getSchoolFlag());
			if (newsType != null) {
				newsOtherVo.setNewsType(newsType);
			}
			ServiceResult<Object> createNewsByOther = newsInfoService.createNewsByOther(newsOtherVo);
			newsfeedId = (String) createNewsByOther.getReturnObj();
			int count = programIntobsleaInfoMapper.updateNewsfeedId(objId, newsfeedId);
			log.info("shareNewsfeed|updateNewsfeedId=" + count);
		}
		// 将taskInstance记录更新为完成状态
		taskInstanceInfoMapper.updateStatus(objId, ProgramState.Complete.getValue(), currentUser.getAccountInfo().getId(), new Date());
		return successResult(getTipMsg("operation_success"), (Object) newsfeedId);
	}

	/**
	 * 
	 * @description 获取活动
	 * @author gfwang
	 * @create 2016年9月27日下午8:51:41
	 * @version 1.0
	 * @param actList
	 * @return
	 */
	public String getActivies(List<ActivitiesVo> actList, Short taskType) {
		String result = "";
		if (ListUtil.isEmpty(actList) || containTaskType(taskType, TaskType.SchoolReadiness)) {
			return null;
		}
		for (ActivitiesVo activi : actList) {
			result += activi.getName() + ";";
		}
		result = result.substring(0, result.length() - 1);
		return StringUtil.isEmpty(result) ? null : result;
	}

	/**
	 * 
	 * @description 獲取newfeed類型
	 * @author gfwang
	 * @create 2016年9月27日下午8:49:26
	 * @version 1.0
	 * @param taskType
	 * @param schoolFlag
	 * @return
	 */
	private NewsType getType(Short taskType, Short schoolFlag) {
		if (schoolFlag == null) {
			return null;
		}
		if (containTaskType(taskType, TaskType.Observation) && schoolFlag == 3) {
			return NewsType.SchoolReadinessObservation;
		}
		if (containTaskType(taskType, TaskType.ObservationFollowup) && schoolFlag == 3) {
			return NewsType.SchoolReadinessObservationFollowUp;
		}
		return null;
	}

	/**
	 * 
	 * @description Learning Story |obs 内容组装
	 * @author gfwang
	 * @create 2016年9月26日下午7:43:05
	 * @version 1.0
	 * @param programNewsFeedVo
	 * @return
	 */
	private String initDealContent(ProgramNewsFeedVo programNewsFeedVo) {
		log.info("initContentObsLear|start");
		Short type = programNewsFeedVo.getTaskType();
		log.info("initContentObsLear|type=" + type);
		String templent = "";
		if (containTaskType(type, TaskType.LearningStory, TaskType.Observation, TaskType.Interest)) {
			// update by gfwang 2016-12-06 需求变更,加一个字段
			templent = getSystemEmailMsg("newsfeed.obs.lern.content");
			if (containTaskType(type, TaskType.Interest)) {
				templent = getSystemEmailMsg("newsfeed.interest.content");
				String content = MessageFormat.format(templent, programNewsFeedVo.getContentTip0(), programNewsFeedVo.getContentTip1(),
						programNewsFeedVo.getContentTip3());
				return content;
			}
			String content = MessageFormat.format(templent, programNewsFeedVo.getContentTip0(), programNewsFeedVo.getContentTip1(), programNewsFeedVo.getContentTip2(),
					programNewsFeedVo.getContentTip3());
			return content;
		} else if (containTaskType(type, TaskType.InterestFollowup)) {
			// update by gfwang 2016-12-06 需求变更,加一个字段
			templent = getSystemEmailMsg("newsfeed.inster.followup.content");
			String content = MessageFormat.format(templent, programNewsFeedVo.getContentTip0(), programNewsFeedVo.getContentTip1(), programNewsFeedVo.getContentTip2(),
					programNewsFeedVo.getContentTip3());
			return content;
		} else if (containTaskType(type, TaskType.ObservationFollowup, TaskType.LearningStoryFollowup)) {
			templent = getSystemEmailMsg("newsfeed.obslean.followup.content");
		}
		String content = MessageFormat.format(templent, programNewsFeedVo.getContentTip1(), programNewsFeedVo.getContentTip2(), programNewsFeedVo.getContentTip3());
		log.info("initContentObsLear|end|content=" + content);
		return content;
	}

	/**
	 * @description 处理NewsfeedOtherVo对象
	 * @author Hxzhang
	 * @create 2016年9月26日下午3:41:43
	 */
	private NewsfeedOtherVo handleNewsfeedOtherVo(ProgramNewsFeedVo programNewsFeedVo) {
		NewsfeedOtherVo newsOtherVo = new NewsfeedOtherVo();
		newsOtherVo.setCurrtenUser(programNewsFeedVo.getCurrtenUser());
		newsOtherVo.setNewsId(programNewsFeedVo.getNewsId());
		newsOtherVo.setContent(programNewsFeedVo.getContent());
		newsOtherVo.setEylfList(programNewsFeedVo.getEylfList());
		newsOtherVo.setNqsVoList(programNewsFeedVo.getNqsVoList());
		newsOtherVo.setRoomId(programNewsFeedVo.getRoomId());
		newsOtherVo.setSourceId(programNewsFeedVo.getTaskId());
		newsOtherVo.setStatus(NewsStatus.Draft.getValue());
		newsOtherVo.setNewsType(getNewsType(programNewsFeedVo.getTaskType(), programNewsFeedVo.getTaskId(), newsOtherVo));
		if (programNewsFeedVo.getTagType() == 0) {
			newsOtherVo.setAccounts(programNewsFeedVo.getAccounts());
		} else {
			newsOtherVo.setTagAccounts(programNewsFeedVo.getAccounts());
		}
		return newsOtherVo;
	}

	private NewsType getNewsType(short taskType, String taskId, NewsfeedOtherVo newsOtherVo) {
		TaskType taskTyp = TaskType.getType(taskType);
		switch (taskTyp) {
		case ExploreCurriculum:
			return NewsType.ExploreCurriculum;
		case GrowCurriculum:
			return NewsType.GrowCurriculum;
		case InterestFollowup:
			return NewsType.InterestFollowUp;
		case SchoolReadiness:
			return NewsType.SchoolReadiness;
		case LearningStory:
			return NewsType.LearningStory;
		case LearningStoryFollowup:
			return NewsType.LearningStoryFollowUp;
		case Lesson:
			return NewsType.Lessons;
		case Interest:
			return NewsType.Interest;
		case Observation:
			return NewsType.Observation;
		case ObservationFollowup:
			return NewsType.ObservationFollowUp;
		case NappyRegister:
			return NewsType.Nappy;
		case MealRegister:
			return NewsType.ChildMeal;
		case SleepRegister:
			return NewsType.Sleep;
		case ToiletRegister:
			return NewsType.ToiletRegisters;
		case WeeklyEvalution:
			ProgramWeeklyEvalutionInfo weekInfo = weeklyEvalutionInfoMapper.selectByPrimaryKey(taskId);
			if (weekInfo.getType() == ProgramWeeklyType.ExploreCurriculum.getValue()) {
				return NewsType.WeeklyEvalutionExplore;
			}
			return NewsType.WeeklyEvalutionGrow;
		default:
			break;
		}
		return null;
	}

	/**
	 * 
	 * @description
	 * @author gfwang
	 * @create 2016年9月26日下午8:19:22
	 * @version 1.0
	 * @param programNewsFeedVo
	 * @return
	 */
	private ServiceResult<Object> validateNewsfeed(ProgramNewsFeedVo programNewsFeedVo) {
		if (containTaskType(programNewsFeedVo.getTaskType(), TaskType.Lesson, TaskType.WeeklyEvalution, TaskType.ExploreCurriculum, TaskType.GrowCurriculum,
				TaskType.SchoolReadiness)) {
			// NQS必填
			if (IsMustCollections(programNewsFeedVo.getNqsVoList())) {
				return failResult(getTipMsg("program.share.nqs.empty"));
			}
			// 周评价去除tag小孩，新需求
			if (IsMustCollections(programNewsFeedVo.getAccounts()) && !containTaskType(programNewsFeedVo.getTaskType(), TaskType.WeeklyEvalution)) {
				return failResult(getTipMsg("program.share.child.empty"));
			}
			if (StringUtil.isEmpty(programNewsFeedVo.getContent())) {
				return failResult(getTipMsg("program.share.content.empty"));
			}
		}
		return successResult();
	}

	/**
	 * 
	 * @description 是否为空集合
	 * @author gfwang
	 * @create 2016年9月25日下午10:12:51
	 * @version 1.0
	 * @param list
	 * @return
	 */
	private boolean IsMustCollections(List list) {
		return ListUtil.isEmpty(list) ? true : false;
	}

	/**
	 * 将lesson的过期状态更新为overDue
	 */
	@Override
	public void updateOverDueLessonTimedTask(Date now) {
		log.info("updateOverDueLessonTimedTask|start");
		// 获取过期的lesson
		List<String> overDueLessonIds = lessonInfoMapper.getOverDueLesson(now);
		// 将记录表中的记录更新为overDue
		int instanceCount = taskInstanceInfoMapper.updateStatuByValueIds(overDueLessonIds);
		log.info("updateOverDueLessonTimedTask||instanceCount:" + instanceCount);
		// 将过期lesson状态更新为overDue
		int lessonCount = lessonInfoMapper.updateOverDueLesson(now);
		log.info("updateOverDueLessonTimedTask|end|weeklyCount:" + lessonCount);
	}
}
