package com.aoyuntek.aoyun.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.aoyuntek.aoyun.dao.AttachmentInfoMapper;
import com.aoyuntek.aoyun.dao.FormAttributeInfoMapper;
import com.aoyuntek.aoyun.dao.FormInfoMapper;
import com.aoyuntek.aoyun.dao.InstanceAttributeInfoMapper;
import com.aoyuntek.aoyun.dao.InstanceInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.dao.ValueSelectInfoMapper;
import com.aoyuntek.aoyun.dao.ValueStringInfoMapper;
import com.aoyuntek.aoyun.dao.ValueTableInfoMapper;
import com.aoyuntek.aoyun.dao.ValueTextInfoMapper;
import com.aoyuntek.aoyun.entity.po.AttachmentInfo;
import com.aoyuntek.aoyun.entity.po.FormAttributeInfo;
import com.aoyuntek.aoyun.entity.po.FormInfo;
import com.aoyuntek.aoyun.entity.po.InstanceAttributeInfo;
import com.aoyuntek.aoyun.entity.po.ValueSelectInfo;
import com.aoyuntek.aoyun.entity.po.ValueStringInfo;
import com.aoyuntek.aoyun.entity.po.ValueTableInfo;
import com.aoyuntek.aoyun.entity.po.ValueTextInfo;
import com.aoyuntek.aoyun.entity.vo.FormAttributeInfoVO;
import com.aoyuntek.aoyun.entity.vo.FormInfoDataVO;
import com.aoyuntek.aoyun.entity.vo.FormInfoVO;
import com.aoyuntek.aoyun.entity.vo.InstanceInfoDataVO;
import com.aoyuntek.aoyun.entity.vo.ValueInfoVO;
import com.aoyuntek.aoyun.service.IFormInfoService;
import com.aoyuntek.aoyun.uitl.DiffComparator;
import com.aoyuntek.aoyun.uitl.DiffListUtil;
import com.aoyuntek.aoyun.uitl.FilePathUtil;
import com.aoyuntek.framework.constants.PropertieNameConts;
import com.theone.aws.util.FileFactory;
import com.theone.common.util.ServiceResult;
import com.theone.json.util.JsonUtil;

@Service
public class FormInfoServiceImpl extends BaseWebServiceImpl<FormInfo, FormInfoMapper> implements IFormInfoService {

    /**
     * 日志
     */
    public static Logger logger = Logger.getLogger(FormInfoServiceImpl.class);

    @Autowired
    private FormInfoMapper formInfoMapper;
    @Autowired
    private FormAttributeInfoMapper formAttributeInfoMapper;
    @Autowired
    private InstanceInfoMapper instanceInfoMapper;
    @Autowired
    private ValueSelectInfoMapper valueSelectInfoMapper;
    @Autowired
    private ValueStringInfoMapper valueStringInfoMapper;
    @Autowired
    private ValueTableInfoMapper valueTableInfoMapper;
    @Autowired
    private ValueTextInfoMapper valueTextInfoMapper;
    @Autowired
    private InstanceAttributeInfoMapper instanceAttributeInfoMapper;
    @Autowired
    private AttachmentInfoMapper attachmentInfoMapper;
    @Autowired
    private UserInfoMapper userInfoMapper;

    @Override
    public ServiceResult<String> saveForm(FormInfoVO formInfoVO) {
        ServiceResult<String> result = new ServiceResult<String>();
        if (formInfoVO.getFormId() == null) {
            formInfoVO.setCode(UUID.randomUUID().toString());
            addForm(formInfoVO);
            result.setReturnObj(formInfoVO.getCode());
            result.setCode(0);
            result.setSuccess(true);
        } else {
            if (instanceInfoMapper.getCountByInstanceId(formInfoVO.getFormId()) == 0) {
                updateForm(formInfoVO);
                result.setReturnObj(formInfoVO.getCode());
                result.setCode(0);
                result.setSuccess(true);
            } else {
                addForm(formInfoVO);
                result.setReturnObj(formInfoVO.getCode());
                result.setCode(0);
                result.setSuccess(true);
            }
        }
        return result;
    }

    private void addForm(FormInfoVO formInfoVO) {
        formInfoVO.setFormId(UUID.randomUUID().toString());
        formInfoVO.setCreateTime(new Date());
        formInfoMapper.insert(formInfoVO);
        for (FormAttributeInfoVO vo : formInfoVO.getAttributes()) {
            FormAttributeInfo formAttributeInfo = new FormAttributeInfo(vo);
            formAttributeInfo.setFormId(formInfoVO.getFormId());
            formAttributeInfo.setAttrId(UUID.randomUUID().toString());
            formAttributeInfoMapper.insert(formAttributeInfo);
        }
    }

    private void updateForm(FormInfoVO formInfoVO) {
        formInfoMapper.updateByPrimaryKeySelective(formInfoVO);
        List<FormAttributeInfo> attributes = formAttributeInfoMapper.selectByFormId(formInfoVO.getFormId());
        List<FormAttributeInfoVO> addList = new ArrayList<FormAttributeInfoVO>();
        List<FormAttributeInfo> removeList = new ArrayList<FormAttributeInfo>();
        List<FormAttributeInfoVO> updateList = new ArrayList<FormAttributeInfoVO>();
        DiffListUtil<FormAttributeInfoVO, FormAttributeInfo> myListUtil = new DiffListUtil<FormAttributeInfoVO, FormAttributeInfo>();
        myListUtil.decompose(formInfoVO.getAttributes(), attributes, new FormAttrtComparator(), addList, removeList, updateList);
        for (FormAttributeInfoVO vo : addList) {
            FormAttributeInfo formAttributeInfo = new FormAttributeInfo(vo);
            formAttributeInfo.setFormId(formInfoVO.getFormId());
            formAttributeInfo.setAttrId(UUID.randomUUID().toString());
            formAttributeInfoMapper.insert(formAttributeInfo);
        }
        for (FormAttributeInfo formAttributeInfo : removeList) {
            formAttributeInfoMapper.deleteByPrimaryKey(formAttributeInfo.getAttrId());
        }
        for (FormAttributeInfoVO vo : updateList) {
            FormAttributeInfo formAttributeInfo = new FormAttributeInfo(vo);
            formAttributeInfoMapper.updateByPrimaryKeySelective(formAttributeInfo);
        }
    }

    class FormAttrtComparator implements DiffComparator<FormAttributeInfoVO, FormAttributeInfo> {
        @Override
        public int compare(FormAttributeInfoVO t, FormAttributeInfo v) {
            return t.getAttrId().compareTo(v.getAttrId());
        }

        @Override
        public boolean equals(FormAttributeInfoVO t, FormAttributeInfo v) {
            if (t.getAttrId() == null) {
                return false;
            }
            if (v.getAttrId() == null) {
                return false;
            }
            return t.getAttrId().equals(v.getAttrId());
        }
    }

    @Override
    public ServiceResult<FormInfoDataVO> getForm(String code) {
        ServiceResult<FormInfoDataVO> result = new ServiceResult<FormInfoDataVO>();
        FormInfoDataVO formInfoVO = formInfoMapper.selectVoByCode(code);
        formInfoVO.setAttributes(formAttributeInfoMapper.selectByFormId(formInfoVO.getFormId()));
        result.setReturnObj(formInfoVO);
        result.setCode(0);
        result.setSuccess(true);
        return result;
    }

    @Override
    public ServiceResult<Object> getInstanceTemplate(String code, String instanceId) {
        ServiceResult<Object> result = new ServiceResult<Object>();
        InstanceInfoDataVO instanceInfoDataVO;
        if (instanceId != null) {
            instanceInfoDataVO = instanceInfoMapper.selectVOByPrimaryKey(instanceId);
            if (instanceInfoDataVO == null) {
                return failResult(getTipMsg("form_instance_not_fount"));
            }
        } else {
            instanceInfoDataVO = new InstanceInfoDataVO();
            instanceInfoDataVO.setFormId(formInfoMapper.selectVoByCode(code).getFormId());
        }
        instanceInfoDataVO.setAttributes(formAttributeInfoMapper.selectFormAttrVoByFormId(instanceInfoDataVO.getFormId(),
                instanceInfoDataVO.getInstanceId()));
        result.setReturnObj(instanceInfoDataVO);
        return result;
    }

    @Override
    public ServiceResult<ValueInfoVO> getInstance(String instanceId) {
        ServiceResult<ValueInfoVO> result = new ServiceResult<ValueInfoVO>();
        ValueInfoVO valueInfoVO = new ValueInfoVO();
        valueInfoVO.setValueStringInfo(valueStringInfoMapper.selectByInstanceId(instanceId));
        valueInfoVO.setValueTableInfo(valueTableInfoMapper.selectByInstanceId(instanceId));
        valueInfoVO.setValueTextInfo(valueTextInfoMapper.selectByInstanceId(instanceId));
        valueInfoVO.setValueSelectInfo(valueSelectInfoMapper.selectByInstanceId(instanceId));
        result.setReturnObj(valueInfoVO);
        result.setCode(0);
        result.setSuccess(true);
        return result;
    }

    @Override
    public ServiceResult<String> saveInstance(ValueInfoVO valueInfoVO) throws Exception {
        ServiceResult<String> result = new ServiceResult<String>();
        result.setCode(1);
        if (valueInfoVO.getAddFiles() != null) {
            saveAttachmentInfo(valueInfoVO.getAddFiles());
        }
        if (valueInfoVO.getRemoveFiles() != null) {
            removeAttachmentInfo(valueInfoVO.getRemoveFiles());
        }
        if (valueInfoVO.getInstanceId() == null) {
            valueInfoVO.setInstanceId(UUID.randomUUID().toString());
            instanceInfoMapper.insert(valueInfoVO);
            if (valueInfoVO.getValueSelectInfo() != null) {
                for (ValueSelectInfo t : valueInfoVO.getValueSelectInfo()) {
                    t.setInstanceId(valueInfoVO.getInstanceId());
                    t.setName(userInfoMapper.getFullNameByAccountId(t.getValue()));
                    valueSelectInfoMapper.insert(t);
                }
            }
            if (valueInfoVO.getValueStringInfo() != null) {
                for (ValueStringInfo t : valueInfoVO.getValueStringInfo()) {
                    t.setInstanceId(valueInfoVO.getInstanceId());
                    valueStringInfoMapper.insert(t);
                }
            }
            if (valueInfoVO.getValueTextInfo() != null) {
                for (ValueTextInfo t : valueInfoVO.getValueTextInfo()) {
                    t.setInstanceId(valueInfoVO.getInstanceId());
                    valueTextInfoMapper.insert(t);
                }
            }
            if (valueInfoVO.getValueTableInfo() != null) {
                for (ValueTableInfo t : valueInfoVO.getValueTableInfo()) {
                    t.setInstanceId(valueInfoVO.getInstanceId());
                    valueTableInfoMapper.insert(t);
                }
            }
            if (valueInfoVO.getInstanceAttrInfo() != null) {
                for (InstanceAttributeInfo t : valueInfoVO.getInstanceAttrInfo()) {
                    t.setInstanceId(valueInfoVO.getInstanceId());
                    instanceAttributeInfoMapper.insert(t);
                }
            }
            result.setReturnObj(valueInfoVO.getInstanceId());
            result.setCode(0);
            result.setSuccess(true);
        } else {
            valueSelectInfoMapper.deleteByInstanceId(valueInfoVO.getInstanceId());
            valueStringInfoMapper.deleteByInstanceId(valueInfoVO.getInstanceId());
            valueTextInfoMapper.deleteByInstanceId(valueInfoVO.getInstanceId());
            valueTableInfoMapper.deleteByInstanceId(valueInfoVO.getInstanceId());
            instanceAttributeInfoMapper.deleteByInstanceId(valueInfoVO.getInstanceId());
            if (valueInfoVO.getValueSelectInfo() != null) {
                for (ValueSelectInfo t : valueInfoVO.getValueSelectInfo()) {
                    t.setInstanceId(valueInfoVO.getInstanceId());
                    t.setName(userInfoMapper.getFullNameByAccountId(t.getValue()));
                    valueSelectInfoMapper.insert(t);
                }
            }
            if (valueInfoVO.getValueStringInfo() != null) {
                for (ValueStringInfo t : valueInfoVO.getValueStringInfo()) {
                    t.setInstanceId(valueInfoVO.getInstanceId());
                    valueStringInfoMapper.insert(t);
                }
            }
            if (valueInfoVO.getValueTextInfo() != null) {
                for (ValueTextInfo t : valueInfoVO.getValueTextInfo()) {
                    t.setInstanceId(valueInfoVO.getInstanceId());
                    valueTextInfoMapper.insert(t);
                }
            }
            if (valueInfoVO.getValueTableInfo() != null) {
                for (ValueTableInfo t : valueInfoVO.getValueTableInfo()) {
                    t.setInstanceId(valueInfoVO.getInstanceId());
                    valueTableInfoMapper.insert(t);
                }
            }
            if (valueInfoVO.getInstanceAttrInfo() != null) {
                for (InstanceAttributeInfo t : valueInfoVO.getInstanceAttrInfo()) {
                    t.setInstanceId(valueInfoVO.getInstanceId());
                    instanceAttributeInfoMapper.insert(t);
                }
            }
            result.setReturnObj(valueInfoVO.getInstanceId());
            result.setCode(0);
            result.setSuccess(true);
        }
        return result;
    }

    /**
     * @author jfzhao
     * @param ids
     * @throws Exception
     */
    private void saveAttachmentInfo(List<String> ids) throws Exception {
        FileFactory fac = null;
        fac = new FileFactory(Region.getRegion(Regions.AP_SOUTHEAST_2), PropertieNameConts.S3);
        for (String attachId : ids) {
            AttachmentInfo attachment = attachmentInfoMapper.selectByPrimaryKey(attachId);
            String mainDir = FilePathUtil.getMainPath(FilePathUtil.FORM_PATH, attachment.getAttachId());
            fac.copyFile(attachment.getAttachId(), mainDir);
            attachment.setAttachId(mainDir);
            attachmentInfoMapper.updateByPrimaryKey(attachment);
        }
    }

    private void removeAttachmentInfo(List<String> ids) {
        if (ids.size() > 0) {
            attachmentInfoMapper.deleteFileByIds(ids);
        }
    }

    private int parseInt(Object o) {
        if (o instanceof String) {
            try {
                return Integer.parseInt((String) o);
            } catch (NumberFormatException e) {
                return 0;
            }
        }
        if (o instanceof Double) {
            return ((Double) o).intValue();
        }
        return 0;
    }

    @Override
    public ServiceResult<String> createInstance(String code) throws Exception {
        FormInfoDataVO vo = getForm(code).getReturnObj();
        List<FormAttributeInfo> formAttributeList = vo.getAttributes();
        ValueInfoVO valueInfoVO = new ValueInfoVO();
        valueInfoVO.setFormId(vo.getFormId());
        for (FormAttributeInfo formAttributeInfo : formAttributeList) {
            if (formAttributeInfo.getType() == 7) {
                Map<String, Object> map = (Map<String, Object>) JsonUtil.jsonToBean(formAttributeInfo.getJson(), Map.class);
                if (valueInfoVO.getInstanceAttrInfo() == null) {
                    valueInfoVO.setInstanceAttrInfo(new ArrayList<InstanceAttributeInfo>());
                }

                InstanceAttributeInfo instanceAttr = new InstanceAttributeInfo();
                instanceAttr.setFormAttrId(formAttributeInfo.getAttrId());
                instanceAttr.setType((short) 1);
                instanceAttr.setValue(parseInt(map.get("rows")));
                valueInfoVO.getInstanceAttrInfo().add(instanceAttr);

                instanceAttr = new InstanceAttributeInfo();
                instanceAttr.setFormAttrId(formAttributeInfo.getAttrId());
                instanceAttr.setType((short) 2);
                instanceAttr.setValue(parseInt(map.get("cols")));
                valueInfoVO.getInstanceAttrInfo().add(instanceAttr);

                List<Map<String, Object>> list = (List<Map<String, Object>>) map.get("value");
                ValueTableInfo vt;
                if (valueInfoVO.getValueTableInfo() == null) {
                    valueInfoVO.setValueTableInfo(new ArrayList<ValueTableInfo>());
                }
                for (Map<String, Object> m : list) {
                    vt = new ValueTableInfo();
                    vt.setFormAttrId(formAttributeInfo.getAttrId());
                    vt.setCol(parseInt(m.get("col")));
                    vt.setRow(parseInt(m.get("row")));
                    vt.setValue((String) m.get("value"));
                    vt.setInputFlag(false);
                    valueInfoVO.getValueTableInfo().add(vt);
                }
            }
        }
        return saveInstance(valueInfoVO);
    }
}
