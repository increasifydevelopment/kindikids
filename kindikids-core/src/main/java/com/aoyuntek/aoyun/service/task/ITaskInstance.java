package com.aoyuntek.aoyun.service.task;

import java.util.Date;

import com.aoyuntek.aoyun.dao.task.TaskInstanceInfoMapper;
import com.aoyuntek.aoyun.entity.po.task.TaskInstanceInfo;
import com.aoyuntek.aoyun.enums.TaskCategory;
import com.aoyuntek.aoyun.enums.TaskType;
import com.aoyuntek.aoyun.service.IBaseWebService;
import com.theone.common.util.ServiceResult;

public interface ITaskInstance extends IBaseWebService<TaskInstanceInfo, TaskInstanceInfoMapper> {

    /**
     * 
     * @author dlli5 at 2016年10月11日上午10:14:07
     * @param valueId
     * @return
     */
    ServiceResult<Object> deleteTaskInstanceByValueId(String valueId);

    /**
     * 
     * @author dlli5 at 2016年10月11日上午10:14:00
     * @param id
     * @return
     */
    ServiceResult<Object> deleteTaskInstanceById(String id);

    /**
     * 
     * @author dlli5 at 2016年9月22日下午9:21:43
     * @param taskType
     * @param obj_id
     * @param taskCategory
     * @param taskId
     * @param valueId
     * @param createAccountId
     * @return
     */
    ServiceResult<TaskInstanceInfo> addTaskInstance(TaskType taskType, String cnetreId, String roomId, String groupId, String valueId,
            String createAccountId, Date day);

    ServiceResult<TaskInstanceInfo> addTaskInstance(TaskType taskType, String cnetreId, String roomId, String groupId, String valueId,
            String createAccountId, Date startDate, Date endDate);

    /**
     * 
     * @author dlli5 at 2016年10月11日上午10:14:38
     * @param valueId
     * @param day
     * @return
     */
    ServiceResult<TaskInstanceInfo> updateTaskInstance(String valueId, Date day);

    /**
     * 
     * @author dlli5 at 2016年9月22日下午9:21:46
     * @param taskType
     *            task类型
     * @param obj_id
     *            roomId
     * @param taskCategory
     *            task类型 枚举
     * @param valueId
     *            你的实体id
     * @param createAccountId
     *            创建人ID
     * @return
     */
    ServiceResult<TaskInstanceInfo> addTaskInstance(TaskType taskType, String centreId, String roomId, String obj_id, TaskCategory taskCategory,
            String taskModelId, String valueId, String createAccountId, Date day);

    /**
     * 
     * @author dlli5 at 2016年10月11日上午10:13:56
     * @param taskType
     * @param centreId
     * @param roomId
     * @param groupId
     * @param obj_id
     * @param taskCategory
     * @param taskModelId
     * @param valueId
     * @param createAccountId
     * @param day
     * @return
     */
    ServiceResult<TaskInstanceInfo> addTaskInstance(TaskType taskType, String centreId, String roomId, String groupId, String obj_id,
            TaskCategory taskCategory, String taskModelId, String valueId, String createAccountId, Date day);

    ServiceResult<TaskInstanceInfo> addTaskInstance(TaskType taskType, String centreId, String roomId, String groupId, String obj_id,
            TaskCategory taskCategory, String taskModelId, String valueId, String createAccountId, Date startDate, Date endDate);

    int getTaskInstance(TaskType taskType, String valueId, Date date);
}
