package com.aoyuntek.aoyun.service.program;

import java.util.Date;

import com.aoyuntek.aoyun.condtion.AttendanceCondtion;
import com.aoyuntek.aoyun.dao.program.ProgramWeeklyEvalutionInfoMapper;
import com.aoyuntek.aoyun.entity.po.program.ProgramWeeklyEvalutionInfo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.service.IBaseWebService;
import com.theone.common.util.ServiceResult;

/**
 * @description Weekly Evaluation业务逻辑层接口
 * @author Hxzhang 2016年9月23日下午2:07:53
 * @version 1.0
 */
public interface IWeeklyEvaluationService extends IBaseWebService<ProgramWeeklyEvalutionInfo, ProgramWeeklyEvalutionInfoMapper> {
    /**
     * @description 获取WeeklyEvaluation信息
     * @author hxzhang
     * @create 2016年9月23日下午2:18:38
     * @version 1.0
     * @return
     */
    ServiceResult<Object> getWeeklyEvaluationInfo(String id);

    /**
     * @description 新增或编辑WeeklyEvaluation信息
     * @author hxzhang
     * @create 2016年9月23日下午2:20:18
     * @version 1.0
     * @return
     */
    ServiceResult<Object> addOrUpdateWeeklyEvaluation(ProgramWeeklyEvalutionInfo programWeeklyEvalutionInfo, UserInfoVo currentUser);

    /**
     * @description 删除WeeklyEvaluation信息
     * @author hxzhang
     * @create 2016年9月23日下午2:21:33
     * @version 1.0
     * @return
     */
    ServiceResult<Object> removeWeeklyEvaluation(String id);

    /**
     * @description 每星期五生成WeeklyEvaluation
     * @author hxzhang
     * @create 2016年9月25日下午5:53:05
     * @version 1.0
     */
    void createWeeklyEvaluationOnFridayTimedTask(Date now);

    /**
     * @description 将过期的WeeklyEvaluation状态更新为overDue
     * @author hxzhang
     * @create 2016年9月25日下午8:41:48
     * @version 1.0
     * @param now
     */
    void updateOverDueWeeklyEvaluationTimedTask(Date now);
}
