package com.aoyuntek.aoyun.service.calendar.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.condtion.AttendanceCondtion;
import com.aoyuntek.aoyun.condtion.LeaveCondition;
import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.FamilyInfoMapper;
import com.aoyuntek.aoyun.dao.LeaveInfoMapper;
import com.aoyuntek.aoyun.dao.event.EventInfoMapper;
import com.aoyuntek.aoyun.entity.po.CentersInfo;
import com.aoyuntek.aoyun.entity.po.LeaveInfo;
import com.aoyuntek.aoyun.entity.po.event.EventInfo;
import com.aoyuntek.aoyun.entity.vo.CalendarDayVo;
import com.aoyuntek.aoyun.entity.vo.CalendarListVo;
import com.aoyuntek.aoyun.entity.vo.LeaveListVo;
import com.aoyuntek.aoyun.entity.vo.RoleInfoVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.enums.UserType;
import com.aoyuntek.aoyun.service.calendar.ICalendarService;
import com.aoyuntek.aoyun.service.impl.BaseWebServiceImpl;
import com.theone.common.util.ServiceResult;
import com.theone.date.util.DateUtil;
import com.theone.string.util.StringUtil;

@Service
public class CalendarService extends BaseWebServiceImpl<LeaveInfo, LeaveInfoMapper> implements ICalendarService {

	@Autowired
	private EventInfoMapper eventInfoMapper;
	@Autowired
	private LeaveInfoMapper leaveInfoMapper;
	@Autowired
	private FamilyInfoMapper familyInfoMapper;
	@Autowired
	private CentersInfoMapper centersInfoMapper;

	@Override
	public ServiceResult<Object> updateLeaveColor(String leaveId, String color) {
		Date time = new Date();
		// 编辑
		LeaveInfo leaveInfo = leaveInfoMapper.selectByPrimaryKey(leaveId);
		if (leaveInfo != null) {
			leaveInfo.setLeaveColor(color);
			// 更新
			leaveInfoMapper.updateByPrimaryKey(leaveInfo);
		}
		return successResult(getTipMsg("operation_success"), (Object) leaveId);
	}

	@Override
	public ServiceResult<Object> updateEventColor(String eventId, String color) {
		Date time = new Date();
		// 编辑
		EventInfo eventInfo = eventInfoMapper.selectByPrimaryKey(eventId);
		if (eventInfo != null) {
			eventInfo.setEventColor(color);
			// 更新
			eventInfoMapper.updateByPrimaryKey(eventInfo);
		}
		return successResult(getTipMsg("operation_success"), (Object) eventId);
	}

	@Override
	public ServiceResult<CalendarListVo> getCalendarList(UserInfoVo currentUser, AttendanceCondtion condition) {
		Date weekDate = null;
		// 如果时间传过来的是空 那么说明前台想回到当前周
		// 查月
		if (condition.getType() == 0) {
			weekDate = DateUtil.parse(condition.getTime() + "/01", "yyyy/MM/dd");
		} // 上周
		else if (condition.getType() == 1) {
			weekDate = DateUtil.addDay(condition.getWeekDate(), -7);
		} // 下周
		else if (condition.getType() == 3) {
			weekDate = DateUtil.addDay(condition.getWeekDate(), 7);
		} else if (condition.getType() == 4) {
			weekDate = condition.getWeekDate();
		} else {
			weekDate = new Date();
		}

		// 日期只能下一年
		if (DateUtil.getCalendar(weekDate).get(Calendar.YEAR) > (DateUtil.getCalendar(new Date()).get(Calendar.YEAR) + 1)) {
			return failResult(-1, getTipMsg("no.permissions"));
		}

		Date now = DateUtil.parse(DateUtil.format(new Date(), DateUtil.yyyyMMdd), DateUtil.yyyyMMdd);

		Date weekBegin = DateUtil.getWeekStart(DateUtil.parse(DateUtil.format(weekDate, "yyyy/MM/dd"), "yyyy/MM/dd"));
		if (condition.getType() == 0) {
			if (DateUtil.getMonth(weekBegin) != DateUtil.getMonth(weekDate)) {
				weekBegin = DateUtil.addWeeks(weekBegin, 1);
			}
		}

		List<LeaveListVo> leave = new ArrayList<LeaveListVo>();
		// TODO
		LeaveCondition leaveCondition = new LeaveCondition();
		leaveCondition.setSelectCenterId(condition.getCentersId());
		leaveCondition.setStartDate(weekBegin);
		leaveCondition.setEndDate(DateUtil.addDay(weekBegin, 6));
		if (getSystemMsg("edensorPark").equals(condition.getCentersId())) {
			leaveCondition.setSelectCenterId(null);
			leaveCondition.setCentreIds(dealEdensorPark());
		}
		leave = leaveInfoMapper.getLeaveListByCalendar(leaveCondition);
		// 获取当前属于什么角色
		List<RoleInfoVo> roleInfoVoList = currentUser.getRoleInfoVoList();
		boolean isCEOORCentreManager = false;
		Short visibility = null;
		List<String> centerIds = new ArrayList<String>();
		boolean isCeoAndAllCenters = false;
		// 设置当前登录者角色集合
		for (int j = 0, m = roleInfoVoList.size(); j < m; j++) {
			// 判断是否是CEO,ceo可以查看所有
			if (roleInfoVoList.get(j).getValue() == Role.CEO.getValue()) {
				isCEOORCentreManager = true;
				if (StringUtil.isEmpty(condition.getCentersId())) {
					isCeoAndAllCenters = true;
				}
				if (getSystemMsg("edensorPark").equals(condition.getCentersId())) {
					centerIds.addAll(dealEdensorPark());
				} else {
					centerIds.add(condition.getCentersId());
				}
				break;
			}
			// 园长及二级园长只能查看本园区
			if (roleInfoVoList.get(j).getValue() == Role.CentreManager.getValue() || roleInfoVoList.get(j).getValue() == Role.EducatorSecondInCharge.getValue()) {
				isCEOORCentreManager = true;
				if (getSystemMsg("edensorPark").equals(condition.getCentersId())) {
					centerIds.addAll(dealEdensorPark());
				} else {
					centerIds.add(condition.getCentersId());
				}
				break;
			}
		}
		if (!isCEOORCentreManager) {
			// 判断当前员工是家长:1 还是员工:0
			if (UserType.Parent.getValue().equals(currentUser.getUserInfo().getUserType())) {
				visibility = (short) 2;
				centerIds = familyInfoMapper.getChildsByFamilyId(currentUser.getUserInfo().getFamilyId());
			}
			if (UserType.Staff.getValue().equals(currentUser.getUserInfo().getUserType().shortValue())) {
				visibility = (short) 1;
				// 解决已排班的兼职All Center不能展示全部event的问题
				if (containRole(Role.Casual, currentUser.getRoleInfoVoList()) == 1 && StringUtil.isNotEmpty(currentUser.getUserInfo().getCentersId())) {
					centerIds.add(currentUser.getUserInfo().getCentersId());
				} else {
					if (getSystemMsg("edensorPark").equals(condition.getCentersId())) {
						centerIds.addAll(dealEdensorPark());
					} else {
						centerIds.add(condition.getCentersId());
					}
				}

			}
		}

		List<CalendarDayVo> weekDays = new ArrayList<CalendarDayVo>();
		for (int i = 0; i <= 6; i++) {
			Date day = DateUtil.addDay(weekBegin, i);
			int cha = com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(day, now);
			CalendarDayVo _CalendarDayVo = new CalendarDayVo();
			_CalendarDayVo.setDay(DateUtil.getDayOfMonth(day));
			_CalendarDayVo.setEvent("");
			_CalendarDayVo.setDate(day);
			_CalendarDayVo.setToday(false);
			if (cha == 0) {
				_CalendarDayVo.setToday(true);
			}

			// 获取event的信息
			List<EventInfo> eventList = new ArrayList<EventInfo>();
			eventList = eventInfoMapper.getListByWhere2(day, visibility, currentUser.getAccountInfo().getId(), centerIds, isCeoAndAllCenters);
			String eventTitle = "";
			if (eventList.size() == 1) {
				eventTitle = eventList.get(0).getTitle();
			} else {
				for (int j = 0; j < eventList.size(); j++) {
					eventTitle += eventList.get(j).getTitle() + "</br>";
				}
			}
			_CalendarDayVo.setEvent(eventTitle);

			_CalendarDayVo.setEventList(eventList);
			weekDays.add(_CalendarDayVo);
		}
		condition.setTime(DateUtil.format(weekBegin, "yyyy/MM"));
		condition.setWeekDate(weekBegin);
		return successResult(new CalendarListVo(weekDays, condition, leave));
	}

	private List<String> dealEdensorPark() {
		List<String> edensorParkIds = new ArrayList<String>();
		List<CentersInfo> list = centersInfoMapper.getAllCentersInfos();
		for (CentersInfo c : list) {
			if (getSystemMsg("rydeId").equals(c.getId())) {
				continue;
			}
			edensorParkIds.add(c.getId());
		}
		return edensorParkIds;
	}
}
