package com.aoyuntek.aoyun.service;

import java.util.List;

import com.aoyuntek.aoyun.condtion.AuthGroupCondition;
import com.aoyuntek.aoyun.dao.AuthorityGroupInfoMapper;
import com.aoyuntek.aoyun.entity.po.AuthorityGroupInfo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.aoyun.entity.vo.UserInGroupInfo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.enums.GroupType;

/**
 * 
 * @description 权限组服务
 * @author gfwang
 * @create 2016年8月29日下午2:55:51
 * @version 1.0
 */
public interface IAuthGroupService extends IBaseWebService<AuthorityGroupInfo, AuthorityGroupInfoMapper> {

    /**
     * 
     * @description 新增或者修改 权限组名称
     * @author gfwang
     * @create 2016年8月29日下午2:57:16
     * @version 1.0
     * @param centerId
     *            园区ID
     * @param centerName
     *            园区名称
     * @param currtenUser
     *            当前用户
     */
    void addOrUpdateGroup(String centerId, String centerName, String oldCenterName, boolean isAdd);

    /**
     * 
     * @description 根据角色获取分组
     * @author gfwang
     * @create 2016年9月12日下午3:34:27
     * @version 1.0
     * @param user
     *            当前用户
     * @return
     */
    List<SelecterPo> getGroupsByRole(UserInfoVo user, String keyWord, GroupType type);

    /**
     * 
     * @description 根据分组获取人
     * @author gfwang
     * @create 2016年9月12日下午4:04:23
     * @version 1.0
     * @param groupName
     *            分组名称
     * @return
     */
    List<UserInGroupInfo> dealAccountListByGroup(String groupName);

    /**
     * 
     * @description 获取所有分组
     * @author gfwang
     * @create 2016年9月13日上午9:41:42
     * @version 1.0
     * @return
     */
    List<String> getAllGroups();

    /**
     * 
     * @description 获取form权限组的 下拉框对象
     * @author gfwang
     * @create 2016年9月24日下午4:07:25
     * @version 1.0
     * @param condition
     *            condition
     * @return
     */
    List<SelecterPo> getSelecterGroup(AuthGroupCondition condition);

    /**
     * 
     * @description 根據選擇的分組集合查出對應的所有人
     * @author gfwang
     * @create 2016年9月24日下午6:31:44
     * @version 1.0
     * @param condition
     * @return
     */
    List<SelecterPo> dealSelectUsersByGroups(AuthGroupCondition condition);

    /**
     * 
     * @description 判断当前人属于哪些分组
     * @author gfwang
     * @create 2016年10月26日下午4:20:11
     * @version 1.0
     * @param user
     * @param type
     * @return
     */
    List<String> isInGroup(UserInfoVo user, GroupType type);
}
