package com.aoyuntek.aoyun.service.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.aoyuntek.aoyun.condtion.MenuCondition;
import com.aoyuntek.aoyun.constants.S3Constants;
import com.aoyuntek.aoyun.dao.AttachmentInfoMapper;
import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.ClosurePeriodMapper;
import com.aoyuntek.aoyun.dao.NewsContentInfoMapper;
import com.aoyuntek.aoyun.dao.NewsInfoMapper;
import com.aoyuntek.aoyun.dao.NqsRelationInfoMapper;
import com.aoyuntek.aoyun.dao.RoomInfoMapper;
import com.aoyuntek.aoyun.dao.WeekNutrionalMenuInfoMapper;
import com.aoyuntek.aoyun.entity.po.AttachmentInfo;
import com.aoyuntek.aoyun.entity.po.CentersInfo;
import com.aoyuntek.aoyun.entity.po.NewsContentInfo;
import com.aoyuntek.aoyun.entity.po.NewsInfo;
import com.aoyuntek.aoyun.entity.po.NqsRelationInfo;
import com.aoyuntek.aoyun.entity.po.RoomInfo;
import com.aoyuntek.aoyun.entity.po.WeekNutrionalMenuInfo;
import com.aoyuntek.aoyun.entity.vo.FileVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.WeekMenuVo;
import com.aoyuntek.aoyun.entity.vo.WeekNutrionalMenuVo;
import com.aoyuntek.aoyun.enums.ArchivedStatus;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.NewsStatus;
import com.aoyuntek.aoyun.enums.NewsType;
import com.aoyuntek.aoyun.enums.NqsTag;
import com.aoyuntek.aoyun.enums.Week;
import com.aoyuntek.aoyun.service.IWeekNutrionalMenuService;
import com.aoyuntek.aoyun.uitl.FilePathUtil;
import com.aoyuntek.aoyun.uitl.SqlUtil;
import com.aoyuntek.framework.constants.PropertieNameConts;
import com.aoyuntek.framework.model.Pager;
import com.theone.aws.util.FileFactory;
import com.theone.common.util.ServiceResult;
import com.theone.date.util.DateUtil;
import com.theone.string.util.StringUtil;

@Service
public class WeekNutrionalMenuServiceImpl extends BaseWebServiceImpl<WeekNutrionalMenuInfo, WeekNutrionalMenuInfoMapper> implements IWeekNutrionalMenuService {

	/**
	 * 日志
	 */
	public static Logger logger = Logger.getLogger(WeekNutrionalMenuServiceImpl.class);
	@Autowired
	private WeekNutrionalMenuInfoMapper weekNutrionalMenuInfoMapper;
	@Autowired
	private AttachmentInfoMapper attachmentInfoMapper;
	@Autowired
	private NewsInfoMapper newsInfoMapper;
	@Autowired
	private NqsRelationInfoMapper nqsRelationInfoMapper;
	@Autowired
	private NewsContentInfoMapper contentInfoMapper;
	@Autowired
	private RoomInfoMapper roomInfoMapper;
	@Autowired
	private CentersInfoMapper centersInfoMapper;
	@Autowired
	private ClosurePeriodMapper closurePeriodMapper;

	@Override
	public ServiceResult<Object> addOrUpdateMenu(WeekNutrionalMenuInfo menuInfo, UserInfoVo currentUser) {
		Integer WEEK_NUM = 5;
		List<WeekNutrionalMenuInfo> menuList = new ArrayList<WeekNutrionalMenuInfo>();
		Date date = new Date();
		String menuId = UUID.randomUUID().toString();
		for (int i = 1; i <= WEEK_NUM; i++) {
			WeekNutrionalMenuInfo menu = new WeekNutrionalMenuInfo();
			menu.setId(UUID.randomUUID().toString());
			menu.setMenuId(menuId);
			menu.setName(menuInfo.getName());
			menu.setWeekToday((short) i);
			menu.setCreateAccountId(currentUser.getAccountInfo().getId());
			menu.setCreateTime(new Date(date.getTime() + i * 1000));
			menu.setUpdateAccountId(currentUser.getAccountInfo().getId());
			menu.setUpdateTime(new Date(date.getTime() + i * 1000));
			menu.setDeleteFlag(DeleteFlag.Default.getValue());
			menuList.add(menu);
		}
		weekNutrionalMenuInfoMapper.initWeekMenuInfo(menuList);
		return successResult(getTipMsg("staff_save"), (Object) menuList);
	}

	@Override
	public ServiceResult<Pager<WeekNutrionalMenuVo, MenuCondition>> getMenuList(MenuCondition condition, UserInfoVo currentUser) {
		// condition.setSortName("create_time");
		// condition.setSortOrder("desc");
		condition.setStartSize(condition.getPageIndex());
		String keyWord = SqlUtil.likeEscape(condition.getKeyWords());
		condition.setKeyWords(keyWord);
		List<WeekNutrionalMenuVo> list = weekNutrionalMenuInfoMapper.getweekNutrionalMenuList(condition);
		int count = weekNutrionalMenuInfoMapper.getMenuListCount(condition);
		condition.setTotalSize(count);
		Pager<WeekNutrionalMenuVo, MenuCondition> pager = new Pager<WeekNutrionalMenuVo, MenuCondition>(list, condition);
		return successResult(pager);
	}

	@Override
	public ServiceResult<Object> getMenuInfo(String menuId) {
		List<WeekNutrionalMenuVo> list = weekNutrionalMenuInfoMapper.getMenuInfo(menuId);
		return successResult((Object) list);
	}

	@Override
	public ServiceResult<Object> updateWeekMenu(WeekMenuVo weekMenuVo, UserInfoVo currentUser) {
		List<WeekNutrionalMenuVo> list = weekMenuVo.getList();
		weekNutrionalMenuInfoMapper.updateMenuName(list.get(0).getMenuId(), weekMenuVo.getMenuName());
		for (WeekNutrionalMenuVo weekNutrionalMenuVo : list) {
			String sourceId = weekNutrionalMenuVo.getLunchImageId();
			FileVo image = weekNutrionalMenuVo.getImage();
			log.info("updateWeekMenu|sourceId=" + sourceId);
			if (StringUtil.isEmpty(sourceId) && image != null) {
				sourceId = UUID.randomUUID().toString();
			}
			// 保存图片
			initImage(image, sourceId);
			weekNutrionalMenuVo.setLunchImageId(sourceId);
		}
		weekNutrionalMenuInfoMapper.updateMenuInfo(list);
		return successResult((Object) weekMenuVo);
	}

	/**
	 * 
	 * @description 设置图片
	 * @author gfwang
	 * @create 2016年8月25日上午9:19:54
	 * @version 1.0
	 * @param file
	 * @param sourceId
	 */
	private void initImage(FileVo file, String sourceId) {
		try {
			// 文件为空
			if (file == null) {
				attachmentInfoMapper.updateDeleteBySourceId(sourceId);
				return;
			}
			// 相对路径不为空并且相对路径不包括临时目录
			if (StringUtil.isNotEmpty(file.getRelativePathName()) && !file.getRelativePathName().contains(FilePathUtil.TEMP_PATH)) {
				return;
			}
			int count = attachmentInfoMapper.updateDeleteBySourceId(sourceId);
			log.info("initImage|count=" + count + "|sourceId=" + sourceId);
			// 相对路径名为空
			if (StringUtil.isEmpty(file.getRelativePathName())) {
				return;
			}
			FileFactory fac = new FileFactory(Region.getRegion(Regions.AP_SOUTHEAST_2), PropertieNameConts.S3);
			AttachmentInfo attachmentInfo = setAttInfo(file, fac, sourceId);
			attachmentInfoMapper.insert(attachmentInfo);
		} catch (Exception e) {
			log.error("addOrUpdateCenter|initImage|s3 error", e);
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @description 初始化对象
	 * @author gfwang
	 * @create 2016年8月24日下午1:35:30
	 * @version 1.0
	 * @param file
	 * @param attachmentInfo
	 * @param fac
	 * @return
	 * @throws Exception
	 */
	private AttachmentInfo setAttInfo(FileVo file, FileFactory fac, String sourceId) throws Exception {
		AttachmentInfo attachmentInfo = new AttachmentInfo();
		attachmentInfo.setId(UUID.randomUUID().toString());
		attachmentInfo.setSourceId(sourceId);
		attachmentInfo.setAttachId(file.getRelativePathName());
		String tempDir = file.getRelativePathName();
		String mainDir = FilePathUtil.getMainPath(FilePathUtil.CENTER_MANAGER, tempDir);
		String rePath = fac.copyFile(tempDir, mainDir);
		attachmentInfo.setAttachName(file.getFileName());
		attachmentInfo.setAttachId(rePath);
		String visitUrl = fac.getS3RouteUrl(rePath, S3Constants.URL_TIMEOUT_HOUR);
		attachmentInfo.setVisitUrl(visitUrl);
		attachmentInfo.setDeleteFlag((short) 0);
		return attachmentInfo;
	}

	@Override
	public ServiceResult<Object> deleteMenu(String menuId) {
		int count = weekNutrionalMenuInfoMapper.getRelationCount(menuId);
		if (count > 0) {
			return failResult(getTipMsg("menu.delete.room.used"));
		}
		weekNutrionalMenuInfoMapper.deleteMenuByName(menuId);
		return successResult();
	}

	@Override
	public ServiceResult<List<WeekMenuVo>> getSelectMenuList() {
		List<WeekMenuVo> list = weekNutrionalMenuInfoMapper.getSelectMenus();
		return successResult(list);
	}

	@Override
	public ServiceResult<List<WeekNutrionalMenuVo>> saveMenu(String roomId, String menuId, String index) {
		weekNutrionalMenuInfoMapper.deleteRoomMenu(roomId, index);
		if (StringUtil.isNotEmpty(menuId)) {
			weekNutrionalMenuInfoMapper.insertRoomMenu(roomId, menuId, index);
		}
		return successResult();
	}

	@Override
	public ServiceResult<Object> getCurrentMenu(String roomId, String index) {
		String menuId = weekNutrionalMenuInfoMapper.getMenuId(roomId, index);
		return successResult((Object) menuId);
	}

	@Override
	public void sendMenu(Date date) {
		log.info("sendTodaysMenu|start");
		// 菜单周期,4周轮换
		int MENU_CYCLE = 4;
		// 判断当天的newsfeed是否已发送
		int index = newsInfoMapper.getTodaysMenu(NewsType.TodayMenus.getValue(), date);
		if (index > 0) {
			log.info("sendTodaysMenu|Has been sent");
			return;
		}
		// 菜单开始日期
		Date startDate = DateUtil.parseString(getSystemMsg("menu_begin_date"));
		// 如果date小于菜单开始日期，直接返回
		if (startDate.after(date)) {
			return;
		}
		// date距离菜单开始日期的总天数
		int menuDays = com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(startDate, date);
		menuDays++;
		// 总的周数，一周7天
		short weekTotal = (short) (menuDays % 7 == 0 ? menuDays / 7 : menuDays / 7 + 1);
		// 第几周
		short weekNum = (short) (weekTotal % MENU_CYCLE == 0 ? MENU_CYCLE : weekTotal % MENU_CYCLE);
		// 星期几
		short weekToday = (short) (menuDays % 7 == 0 ? 7 : menuDays % 7);
		// 如果是周末，直接返回，不发送菜单
		if (weekToday == Week.Saturday.getValue() || weekToday == Week.Sunday.getValue()) {
			log.info("sendTodaysMenu|return|weekToday=" + weekToday);
			return;
		}
		List<String> roomList = weekNutrionalMenuInfoMapper.getRoomList(weekNum);
		for (String roomId : roomList) {
			RoomInfo room = roomInfoMapper.selectByPrimaryKey(roomId);
			// 关园期间不发送
			int c = closurePeriodMapper.getCountByCentreIdDate(room.getCentersId(), date);
			if (c > 0) {
				continue;
			}
			// room归档不发送菜单newsfeed
			if (room.getStatus() == ArchivedStatus.Archived.getValue()) {
				continue;
			}
			// 获取当天菜单
			WeekNutrionalMenuInfo weekTodayMenuInfo = weekNutrionalMenuInfoMapper.getSendMenu(roomId, weekNum, weekToday);
			// 如果菜单里breakfast，lunch等全都为空，则返回不发送此菜单
			if (StringUtil.isEmpty(weekTodayMenuInfo.getBreakfast()) && StringUtil.isEmpty(weekTodayMenuInfo.getMorningTea())
					&& StringUtil.isEmpty(weekTodayMenuInfo.getLunch()) && StringUtil.isEmpty(weekTodayMenuInfo.getDessert())
					&& StringUtil.isEmpty(weekTodayMenuInfo.getAfternoonTea()) && StringUtil.isEmpty(weekTodayMenuInfo.getLateAfternoonTea())) {
				log.info("sendTodaysMenu|return|end");
				continue;
			}
			String breakfast = StringUtil.isEmpty(weekTodayMenuInfo.getBreakfast()) ? "" : weekTodayMenuInfo.getBreakfast();
			String morningTea = StringUtil.isEmpty(weekTodayMenuInfo.getMorningTea()) ? "" : weekTodayMenuInfo.getMorningTea();
			String lunch = StringUtil.isEmpty(weekTodayMenuInfo.getLunch()) ? "" : weekTodayMenuInfo.getLunch();
			String dessert = StringUtil.isEmpty(weekTodayMenuInfo.getDessert()) ? "" : weekTodayMenuInfo.getDessert();
			String afternoonTea = StringUtil.isEmpty(weekTodayMenuInfo.getAfternoonTea()) ? "" : weekTodayMenuInfo.getAfternoonTea();
			String lateAfternoonTea = StringUtil.isEmpty(weekTodayMenuInfo.getLateAfternoonTea()) ? "" : weekTodayMenuInfo.getLateAfternoonTea();
			// 菜单的标题和内容
			String content = MessageFormat.format(getSystemEmailMsg("newsfeed.menu.content"), breakfast, morningTea, lunch, dessert, afternoonTea, lateAfternoonTea);
			log.info("sendTodaysMenu|content:" + content);
			String newsId = UUID.randomUUID().toString();
			String sourceId = UUID.randomUUID().toString();
			int count = saveNewsInfo(room, newsId, sourceId, date);
			log.info("sendTodaysMenu|count=" + count + "newsId=" + newsId);
			if (count >= 1) {
				// 保存附件，NQS，内容
				saveAttNqsCon(weekTodayMenuInfo, sourceId, newsId, content);
			}
		}
		log.info("sendTodaysMenu|end");
	}

	private int saveNewsInfo(RoomInfo room, String newsId, String sourceId, Date date) {
		NewsInfo newsInfo = new NewsInfo();
		CentersInfo center = centersInfoMapper.selectByPrimaryKey(room.getCentersId());

		newsInfo.setId(newsId);
		newsInfo.setNewsType(NewsType.TodayMenus.getValue());
		newsInfo.setCentersId(center.getId());
		newsInfo.setRoomId(room.getId());
		newsInfo.setStatus(NewsStatus.Appreoved.getValue());
		newsInfo.setDeleteFlag(DeleteFlag.Default.getValue());
		newsInfo.setApproveTime(date);
		newsInfo.setCreateTime(date);
		newsInfo.setUpdateTime(date);
		newsInfo.setImgId(sourceId);

		int count = newsInfoMapper.insert(newsInfo);
		return count;
	}

	private void saveAttNqsCon(WeekNutrionalMenuInfo weekTodayMenuInfo, String sourceId, String newsId, String content) {
		// 附件
		List<AttachmentInfo> attList = attachmentInfoMapper.getAttachmentInfoBySourceId(weekTodayMenuInfo.getLunchImageId());
		for (AttachmentInfo att : attList) {
			att.setId(UUID.randomUUID().toString());
			att.setSourceId(sourceId);
			attachmentInfoMapper.insert(att);
		}
		// NQS
		String[] nqsArr = getSystemMsg("nqs").split(";");
		for (String nqsVersion : nqsArr) {
			NqsRelationInfo nqsNews = new NqsRelationInfo();
			nqsNews.setId(UUID.randomUUID().toString());
			nqsNews.setObjId(newsId);
			nqsNews.setDeleteFlag(DeleteFlag.Default.getValue());
			nqsNews.setNqsVersion(nqsVersion);
			nqsNews.setTag(NqsTag.V2.getValue());
			nqsRelationInfoMapper.insert(nqsNews);
		}
		// 内容
		NewsContentInfo newsContent = new NewsContentInfo();
		String commontId = UUID.randomUUID().toString();
		newsContent.setId(commontId);
		newsContent.setNewsId(newsId);
		newsContent.setContent(content);
		int count2 = contentInfoMapper.insert(newsContent);
		log.info("sendTodaysMenu|count2=" + count2 + "|commontId=" + commontId);

	}

}
