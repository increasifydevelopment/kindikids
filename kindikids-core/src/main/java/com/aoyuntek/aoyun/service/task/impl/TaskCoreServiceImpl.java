package com.aoyuntek.aoyun.service.task.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.constants.DateFormatterConstants;
import com.aoyuntek.aoyun.constants.SystemConstants;
import com.aoyuntek.aoyun.dao.ClosurePeriodMapper;
import com.aoyuntek.aoyun.dao.task.TaskFollowInstanceInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskInstanceInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskResponsibleLogInfoMapper;
import com.aoyuntek.aoyun.entity.po.RoomInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskFollowInstanceInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskFollowUpInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskFrequencyInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskInstanceInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskResponsibleLogInfo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.base.UserVo;
import com.aoyuntek.aoyun.entity.vo.task.TaskInfoVo;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.ProgramState;
import com.aoyuntek.aoyun.enums.TaskCategory;
import com.aoyuntek.aoyun.enums.TaskFrequenceRepeatType;
import com.aoyuntek.aoyun.enums.TaskType;
import com.aoyuntek.aoyun.factory.TaskCentreFactory;
import com.aoyuntek.aoyun.factory.TaskChildFactory;
import com.aoyuntek.aoyun.factory.TaskFactory;
import com.aoyuntek.aoyun.factory.TaskRoomFactory;
import com.aoyuntek.aoyun.factory.TaskStaffFactory;
import com.aoyuntek.aoyun.service.IFormInfoService;
import com.aoyuntek.aoyun.service.impl.BaseWebServiceImpl;
import com.aoyuntek.aoyun.service.task.ITaskCoreService;
import com.aoyuntek.aoyun.service.task.ITaskInstance;
import com.theone.common.util.ServiceResult;
import com.theone.date.util.DateUtil;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

@Service
public class TaskCoreServiceImpl extends BaseWebServiceImpl<TaskInfo, TaskInfoMapper> implements ITaskCoreService {

	@Autowired
	private ITaskInstance taskInstance;
	@Autowired
	private TaskFactory taskFactory;
	@Autowired
	private TaskCentreFactory taskCentreFactory;
	@Autowired
	private TaskRoomFactory taskRoomFactory;
	@Autowired
	private TaskChildFactory taskChildFactory;
	@Autowired
	private TaskStaffFactory taskStaffFactory;
	@Autowired
	private TaskInstanceInfoMapper taskInstanceInfoMapper;
	@Autowired
	private TaskResponsibleLogInfoMapper taskResponsibleLogInfoMapper;
	@Autowired
	private IFormInfoService formInfoService;
	@Autowired
	private TaskFollowInstanceInfoMapper followInstanceInfoMapper;
	@Autowired
	private ClosurePeriodMapper closurePeriodMapper;

	@Override
	public ServiceResult<Object> dealTaskInstance(TaskInfoVo taskInfoVo, String accountId, String centreId, String roomId, String objId, Date day) throws Exception {
		int count = closurePeriodMapper.getCountByCentreIdDate(centreId, day);
		if (count > 0) {
			return successResult();
		}
		TaskCategory taskCategory = TaskCategory.getType(taskInfoVo.getTaskCategory());
		ServiceResult<String> result = formInfoService.createInstance(taskInfoVo.getFormId());
		ServiceResult<TaskInstanceInfo> resultInstance = taskInstance.addTaskInstance(TaskType.CustomTask, centreId, roomId, objId, taskCategory, taskInfoVo.getId(),
				result.getReturnObj(), accountId, day);
		solidifyIndividualTaskResponsible(taskInfoVo.getTaskCategory(), resultInstance.getReturnObj().getId(), objId);
		accountId = StringUtil.isEmpty(accountId) ? SystemConstants.JobCreateAccountId : accountId;
		Date date = resultInstance.getReturnObj().getEndDate();
		int index = 1;
		if (ListUtil.isNotEmpty(taskInfoVo.getFollowUpInfos())) {
			for (TaskFollowUpInfo taskFollowUpInfo : taskInfoVo.getFollowUpInfos()) {
				TaskFollowInstanceInfo followInstanceInfo = new TaskFollowInstanceInfo();
				followInstanceInfo.setCreateTime(new Date());
				followInstanceInfo.setCreateAccountId(accountId);
				followInstanceInfo.setBeginDate(resultInstance.getReturnObj().getBeginDate());
				if (date != null) {
					followInstanceInfo.setBeginDate(DateUtil.addDay(date, index));
					followInstanceInfo.setEndDate(DateUtil.addDay(followInstanceInfo.getBeginDate(), taskFollowUpInfo.getDurationStep() - 1));
				}
				followInstanceInfo.setFollowUpId(taskFollowUpInfo.getId());
				followInstanceInfo.setFormId(taskFollowUpInfo.getFormId());
				String followId = UUID.randomUUID().toString();
				followInstanceInfo.setId(followId);
				followInstanceInfo.setStatu(ProgramState.Default.getValue());
				followInstanceInfo.setDeleteFlag(DeleteFlag.Default.getValue());
				followInstanceInfo.setTaskInstanceId(resultInstance.getReturnObj().getId());
				ServiceResult<String> valueId = formInfoService.createInstance(taskFollowUpInfo.getFormId());
				followInstanceInfo.setValueId(valueId.getReturnObj());
				followInstanceInfoMapper.insert(followInstanceInfo);
				index = index + taskFollowUpInfo.getDurationStep();
				solidifyIndividualTaskResponsible(taskInfoVo.getTaskCategory(), followId, objId);
			}
		}
		return successResult();
	}

	// 固化IndividualTask责任人
	private void solidifyIndividualTaskResponsible(Short taskCategory, String taskId, String accountId) {
		// 所有task都需要固化责任人
		// if (taskCategory != TaskCategory.Individual.getValue()) {
		// return;
		// }
		taskResponsibleLogInfoMapper.insert(new TaskResponsibleLogInfo(taskId, accountId));
	}

	@Override
	public ServiceResult<Object> dealTaskResponsibleLog(String taskInstanceId, String accountId) {
		TaskResponsibleLogInfo logInfo = new TaskResponsibleLogInfo();
		logInfo.setAcountId(accountId);
		logInfo.setTaskInstanceId(taskInstanceId);
		taskResponsibleLogInfoMapper.insert(logInfo);
		return successResult();
	}

	public static void main(String[] args) {
		Date fristOfDueDate = DateUtil.getWeekStart(new Date());
		System.out.println(DateUtil.getCalendar(fristOfDueDate).get(Calendar.YEAR));
	}

	@Override
	public ServiceResult<Object> dealInitTaskInstance(Date day, TaskInfoVo taskInfoVo, UserInfoVo currentUser) throws Exception {
		TaskFrequencyInfo frequencyInfo = taskInfoVo.getTaskFrequencyInfo();

		if (taskInfoVo.getStatu() == DeleteFlag.Delete.getValue()) {
			return successResult();
		}

		if (frequencyInfo == null) {
			return successResult();
		}

		if (BooleanUtils.isTrue(taskInfoVo.getShiftFlag())) {
			return successResult();
		}
		String createAccountId = currentUser == null ? SystemConstants.JobCreateAccountId : currentUser.getAccountInfo().getId();
		boolean needInit = false;
		Short shortVal = frequencyInfo.getRepeatType();
		TaskFrequenceRepeatType frequenceRepeatType = TaskFrequenceRepeatType.getType(shortVal);

		boolean inTimeScope = false;
		if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(frequencyInfo.getStartsDate(), day) >= 0) {
			if (frequencyInfo.getEndsDate() == null || com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(frequencyInfo.getEndsDate(), day) <= 0) {
				inTimeScope = true;
			}
		}

		switch (frequenceRepeatType) {
		case Daily: {
			if (inTimeScope && DateUtil.getDayOfWeek(day) != 7 && DateUtil.getDayOfWeek(day) != 1) {
				needInit = true;
			}
		}
			break;
		case Weekly: {
			if (inTimeScope) {
				// 如果开始weekday to stat 是今天 那么要生成
				if (Integer.valueOf(frequencyInfo.getDayToStart()) == DateUtil.getDayOfWeek(day) - 1) {
					if (Integer.valueOf(frequencyInfo.getDayToEnd()) >= DateUtil.getDayOfWeek(day) - 1) {
						needInit = true;
					}
				}
			}
		}
			break;
		case Monthly: {
			if (inTimeScope) {
				// 如果开始weekday to stat 是今天 那么要生成
				if ((Integer.valueOf(frequencyInfo.getDayToStart()) == DateUtil.getDayOfMonth(day))
						&& (Integer.valueOf(frequencyInfo.getDayToEnd()) >= DateUtil.getDayOfMonth(day))) {
					needInit = true;
				}
			}
		}
			break;
		case Quarterly: {
			if (inTimeScope) {
				if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(com.aoyuntek.aoyun.uitl.DateUtil.getFristDayOfQuarter(day), day) == 0) {
					needInit = true;
				}
			}
		}
			break;
		case Yearly: {
			if (inTimeScope) {
				int year = DateUtil.getCalendar(day).get(Calendar.YEAR);
				if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(DateUtil.parse(frequencyInfo.getDayToStart() + "/" + year, DateFormatterConstants.SYS_FORMAT2),
						day) == 0) {
					Date dueDateOfYear = DateUtil.parse(frequencyInfo.getDayToEnd() + "/" + year, DateFormatterConstants.SYS_FORMAT2);
					if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(dueDateOfYear, day) <= 0) {
						needInit = true;
					}
				}
			}
		}
			break;
		case OnceOff: {
			if (inTimeScope) {
				if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(DateUtil.parse(frequencyInfo.getDayToStart(), DateFormatterConstants.SYS_FORMAT2), day) == 0) {
					Date dueDateOfOnceOff = DateUtil.parse(frequencyInfo.getDayToEnd(), DateFormatterConstants.SYS_FORMAT2);
					if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(dueDateOfOnceOff, day) <= 0) {
						needInit = true;
					}
				}
			}
		}
			break;
		case WhenRequired:
			needInit = false;
			break;
		default:
			break;
		}

		if (needInit && StringUtil.isNotEmpty(taskInfoVo.getFormId())) {
			TaskCategory taskCategory = TaskCategory.getType(taskInfoVo.getTaskCategory());
			Date[] dates = taskFactory.dealDate(TaskType.CustomTask, taskInfoVo.getId(), day);
			switch (taskCategory) {
			case CENTER: {
				List<String> ccentreIds = taskCentreFactory.getCentreOfSelectCentre(taskInfoVo.getSelectTaskVisibles());
				for (String centreId : ccentreIds) {
					// 判断数据库中是否已经生成
					if (taskInstanceInfoMapper.haveTaskInstance(taskInfoVo.getId(), dates[0], dates[1], taskCategory.getValue(), centreId) > 0) {
						continue;
					}
					dealTaskInstance(taskInfoVo, createAccountId, centreId, null, null, day);
				}
			}
				break;
			case ROOM: {
				List<RoomInfo> rooms = taskRoomFactory.getRoomOfSelectRoom(taskInfoVo.getSelectTaskVisibles());
				for (RoomInfo room : rooms) {
					// 判断数据库中是否已经生成
					if (taskInstanceInfoMapper.haveTaskInstance(taskInfoVo.getId(), dates[0], dates[1], taskCategory.getValue(), room.getId()) > 0) {
						continue;
					}
					dealTaskInstance(taskInfoVo, createAccountId, room.getCentersId(), room.getId(), null, day);
				}
			}
				break;
			case CHILDRRENN: {
				List<UserVo> childrens = taskChildFactory.getChildOfSelectChild(taskInfoVo.getSelectTaskVisibles());
				for (UserVo child : childrens) {
					// 判断数据库中是否已经生成
					if (taskInstanceInfoMapper.haveTaskInstance(taskInfoVo.getId(), dates[0], dates[1], taskCategory.getValue(), child.getAccountId()) > 0) {
						continue;
					}
					dealTaskInstance(taskInfoVo, createAccountId, child.getCentersId(), child.getRoomId(), child.getAccountId(), day);
				}
			}
				break;
			case STAFF: {
				List<UserVo> staffs = taskStaffFactory.getStaffOfSelectStaff(taskInfoVo.getSelectTaskVisibles());
				for (UserVo staff : staffs) {
					// 判断数据库中是否已经生成
					if (taskInstanceInfoMapper.haveTaskInstance(taskInfoVo.getId(), dates[0], dates[1], taskCategory.getValue(), staff.getAccountId()) > 0) {
						continue;
					}
					dealTaskInstance(taskInfoVo, createAccountId, staff.getCentersId(), staff.getRoomId(), staff.getAccountId(), day);
				}
			}
				break;
			case Individual: {
				List<UserVo> staffs = taskStaffFactory.getStaffOfSelectStaff(taskInfoVo.getSelectTaskVisibles());
				for (UserVo staff : staffs) {
					// 判断数据库中是否已经生成
					if (taskInstanceInfoMapper.haveTaskInstance(taskInfoVo.getId(), dates[0], dates[1], taskCategory.getValue(), staff.getAccountId()) > 0) {
						continue;
					}
					dealTaskInstance(taskInfoVo, createAccountId, staff.getCentersId(), staff.getRoomId(), staff.getAccountId(), day);
				}
			}
				break;
			}
		}
		return successResult();
	}
}
