package com.aoyuntek.aoyun.service.attendance;

import java.util.Date;
import java.util.List;

import com.aoyuntek.aoyun.entity.po.AbsenteeRequestInfo;
import com.aoyuntek.aoyun.entity.po.AccountInfo;
import com.aoyuntek.aoyun.entity.po.ChangeAttendanceRequestInfo;
import com.aoyuntek.aoyun.entity.po.ChildAttendanceInfo;
import com.aoyuntek.aoyun.entity.po.ReplaceChildVo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.vo.SigninVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;

public interface IAttendanceCoreService {

    /**
     * admin在操作change的时候 看是否有家长申请 如果有 那么更新家长的申请
     * 
     * @author dlli5 at 2016年9月13日下午8:52:23
     * @param childId
     * @param adminAttendanceRequestInfo
     */
    public void dealParentRequest(String childId, ChangeAttendanceRequestInfo adminAttendanceRequestInfo);

    /**
     * 
     * @author dlli5 at 2016年9月10日下午12:32:21
     * @param userId
     * @param centersId
     * @param isCentreAdmin
     * @return
     */
    public boolean isCenterManager(UserInfoVo currentUser, String centersId, boolean isCentreAdmin);

    /**
     * 
     * @author dlli5 at 2016年9月10日下午1:44:11
     * @param centersId
     * @param roomId
     * @param day
     * @param dayOfWeek
     * @return
     */
    List<SigninVo> getFutureRoomChilds(String centersId, String roomId, Date day, short dayOfWeek);

    /**
     * 
     * @author dlli5 at 2016年9月12日下午6:03:17
     * @param centersId
     * @param roomId
     * @param dayOfWeek
     * @param day
     * @return
     */
    List<SigninVo> getTodaySignChilds(String centersId, String roomId, short dayOfWeek, Date day);

    /**
     * 
     * @author dlli5 at 2016年12月22日上午9:13:37
     * @param centersId
     * @param roomId
     * @param dayOfWeek
     * @param day
     * @return
     */
    List<SigninVo> getYesterdaySignChilds(String centersId, String roomId, short dayOfWeek, Date day);

    /**
     * 
     * @author dlli5 at 2016年9月13日下午3:19:09
     * @param day
     * @param absenteeRequestInfo
     * @param dayOfWeek
     * @return
     */
    public int getReaplaceInstance(String centerId, String roomId, Date day, AbsenteeRequestInfo absenteeRequestInfo, int dayOfWeek);

    /**
     * 
     * @author dlli5 at 2017年1月3日上午9:55:17
     * @param centersId
     * @param roomId
     * @param day
     * @param absenteeRequestInfo
     * @param dayOfWeek
     * @param internalResult
     */
    public List<ReplaceChildVo> getReaplaceInstanceOfChilds(String centersId, String roomId, Date day, AbsenteeRequestInfo absenteeRequestInfo, int dayOfWeek,
            List<ReplaceChildVo> internalResult);

    /**
     * 
     * @author dlli5 at 2016年9月13日下午6:37:42
     * @param id
     * @param day
     * @return
     */
    public List<SigninVo> getTodaySignOutChilds(String id, Date day);

    /**
     * 
     * @author dlli5 at 2016年12月13日下午12:22:38
     * @param currentUser
     * @param accountInfo
     * @param childAttendanceInfo
     */
    public void dealAttendanceInit(UserInfoVo currentUser, ChildAttendanceInfo attendanceInfo, AccountInfo accountInfo, UserInfo userInfo);

    /**
     * 
     * @author dlli5 at 2017年1月3日上午9:55:22
     * @param currentUser
     * @param childInfo
     * @param attendanceInfo
     * @param attendanceRequestInfo
     */
    public void dealAttendanceNow(UserInfoVo currentUser, UserInfo childInfo, ChildAttendanceInfo attendanceInfo,
            ChangeAttendanceRequestInfo attendanceRequestInfo);

}
