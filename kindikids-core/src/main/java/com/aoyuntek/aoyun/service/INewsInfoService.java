package com.aoyuntek.aoyun.service;

import java.io.OutputStream;
import java.util.List;

import com.aoyuntek.aoyun.condtion.NewsCondition;
import com.aoyuntek.aoyun.condtion.NewsFeedAuthorityCondition;
import com.aoyuntek.aoyun.dao.NewsInfoMapper;
import com.aoyuntek.aoyun.entity.po.NewsCommentInfo;
import com.aoyuntek.aoyun.entity.po.NewsInfo;
import com.aoyuntek.aoyun.entity.vo.NewsFeedInfoVo;
import com.aoyuntek.aoyun.entity.vo.NewsInfoListVo;
import com.aoyuntek.aoyun.entity.vo.NewsfeedOtherVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.framework.model.Pager;
import com.theone.common.util.ServiceResult;

public interface INewsInfoService extends IBaseWebService<NewsInfo, NewsInfoMapper> {

	void updateParentComment(String newsId);

	/**
	 * 
	 * @description 根据accountId 获取当前园区内，当前登录人可查看的所有微博
	 * @author bbq
	 * @create 2016年6月27日下午3:51:05
	 * @version 1.0
	 * @param currentUser
	 *            当前用户
	 * @param condition
	 *            查询条件
	 * @return 微博集合
	 */
	ServiceResult<Pager<NewsInfoListVo, NewsCondition>> saveGetNewsList(NewsCondition condition, UserInfoVo currentUser);

	/**
	 * 
	 * @description 获取一条微博及其评论
	 * @author bbq
	 * @create 2016年6月27日下午4:26:04
	 * @version 1.0
	 * @param newsId
	 *            微博Id
	 * @param centersId
	 *            当前园区
	 * @return 微博及其评论
	 */
	ServiceResult<NewsFeedInfoVo> getNews(String newsId, UserInfoVo currentUser);

	/**
	 * 
	 * @description 根据newsId删除微博
	 * @author bbq
	 * @create 2016年6月27日下午5:20:36
	 * @version 1.0
	 * @param newsId
	 *            微博Id
	 * @return 影响行数
	 */
	ServiceResult<Integer> removeNewsByNewsId(String newsId, UserInfoVo currentUser);

	/**
	 * @description 根据newsCommentId删除评论
	 * @author hxzhang
	 * @create 2016年7月14日下午3:51:11
	 * @version 1.0
	 * @param newsCommentId
	 *            评论Id
	 * @param currentUser
	 *            当前登录用户
	 * @return 影响行数
	 */
	ServiceResult<Object> removeNewsCommentsBynewsCommentId(String newsCommentId, UserInfoVo currentUser);

	/**
	 * @description 手动发送新微博
	 * @author hxzhang
	 * @create 2016年6月28日上午9:59:32
	 * @version 1.0
	 * @param newsInfo
	 *            newsInfo实体
	 * @param userInfoVo
	 *            当前登录用户
	 * @param groupName
	 *            分组信息
	 * @param nsqVersion
	 *            nsq信息
	 * @param centersInfo
	 *            centersInfo实体
	 * @return 返回操作结果
	 */
	ServiceResult<Object> addOrUpdateNews(NewsFeedInfoVo newsFeedInfoVo, UserInfoVo currentUser);

	/**
	 * @description 保存评论或回复
	 * @author hxzhang
	 * @create 2016年6月28日下午1:39:33
	 * @version 1.0
	 * @param newsCommentInfo
	 *            NewsCommentInfo实体
	 * @param currentUser
	 *            UserInfoVO
	 * @return 返回操作结果
	 */
	ServiceResult<Object> addOrUpdateNewsComment(NewsCommentInfo newsCommentInfo, UserInfoVo currentUser);

	/**
	 * @description 批准微博
	 * @author hxzhang
	 * @create 2016年6月28日下午2:11:21
	 * @version 1.0
	 * @param newsId
	 *            微博Id
	 * @return 返回操作结果
	 */
	ServiceResult<Object> updateNews(String newsId, UserInfoVo currentUser);

	/**
	 * @description 取消批准微博
	 * @author hxzhang
	 * @create 2016年7月14日下午7:25:34
	 * @version 1.0
	 * @param newsId
	 *            微博ID
	 * @param currentUser
	 *            当前登录用户
	 * @return 返回操作结果
	 */
	ServiceResult<Object> updateUnApproveNews(String newsId, UserInfoVo currentUser);

	/**
	 * @description 给微博评分
	 * @author hxzhang
	 * @create 2016年7月14日下午7:56:12
	 * @version 1.0
	 * @param newsId
	 *            微博ID
	 * @param putScore
	 *            评分
	 * @param currentUser
	 *            当前登录用户
	 * @return 返回操作结果
	 */
	ServiceResult<Object> putScore(String newsId, String putScore, UserInfoVo currentUser);

	/**
	 * 
	 * @description 获取当前可查看的newsfeed
	 * @author gfwang
	 * @create 2016年9月20日下午4:55:01
	 * @version 1.0
	 * @param currentUser
	 * @return
	 */
	ServiceResult<List<String>> getNewsAuths(UserInfoVo currentUser, NewsFeedAuthorityCondition condition);

	/**
	 * 
	 * @description 从其它模块创建newsfeed
	 * @author gfwang
	 * @create 2016年9月20日下午4:54:40
	 * @version 1.0
	 * @return
	 */
	ServiceResult<Object> createNewsByOther(NewsfeedOtherVo newsInfoVo);

	/**
	 * @description 下载PDF
	 * @author Hxzhang
	 * @create 2016年9月26日下午2:08:37
	 */
	void getNewsfeedPdf(String id, OutputStream outputStream, UserInfoVo currentUser) throws Exception;

	ServiceResult<Object> getNewFeedback(NewsCondition condition, UserInfoVo currUser);
}
