package com.aoyuntek.aoyun.service.roster.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.lf5.util.StreamUtils;
import org.aspectj.weaver.AjAttribute.PrivilegedAttribute;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.aoyuntek.aoyun.condtion.PayrollCondition;
import com.aoyuntek.aoyun.condtion.RosterCondition;
import com.aoyuntek.aoyun.condtion.RosterShiftCondition;
import com.aoyuntek.aoyun.condtion.RosterStaffCondition;
import com.aoyuntek.aoyun.condtion.UserCondition;
import com.aoyuntek.aoyun.constants.DateFormatterConstants;
import com.aoyuntek.aoyun.dao.AccountInfoMapper;
import com.aoyuntek.aoyun.dao.AccountRoleInfoMapper;
import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.ClosurePeriodCentresMapper;
import com.aoyuntek.aoyun.dao.ClosurePeriodMapper;
import com.aoyuntek.aoyun.dao.HolidaypayInfoMapper;
import com.aoyuntek.aoyun.dao.LeaveInfoMapper;
import com.aoyuntek.aoyun.dao.RoleInfoMapper;
import com.aoyuntek.aoyun.dao.SigninInfoMapper;
import com.aoyuntek.aoyun.dao.StaffEmploymentInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.dao.event.EventInfoMapper;
import com.aoyuntek.aoyun.dao.roster.RosterInfoMapper;
import com.aoyuntek.aoyun.dao.roster.RosterLeaveAbsenceInfoMapper;
import com.aoyuntek.aoyun.dao.roster.RosterLeaveItemInfoMapper;
import com.aoyuntek.aoyun.dao.roster.RosterShiftInfoMapper;
import com.aoyuntek.aoyun.dao.roster.RosterShiftTimeInfoMapper;
import com.aoyuntek.aoyun.dao.roster.RosterStaffInfoMapper;
import com.aoyuntek.aoyun.dao.roster.TempRosterCenterInfoMapper;
import com.aoyuntek.aoyun.entity.po.AccountInfo;
import com.aoyuntek.aoyun.entity.po.CentersInfo;
import com.aoyuntek.aoyun.entity.po.ClosurePeriod;
import com.aoyuntek.aoyun.entity.po.ClosurePeriodCentres;
import com.aoyuntek.aoyun.entity.po.HolidaypayInfo;
import com.aoyuntek.aoyun.entity.po.LeaveInfo;
import com.aoyuntek.aoyun.entity.po.RoleInfo;
import com.aoyuntek.aoyun.entity.po.SigninInfo;
import com.aoyuntek.aoyun.entity.po.StaffEmploymentInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.po.event.EventInfo;
import com.aoyuntek.aoyun.entity.po.roster.RosterInfo;
import com.aoyuntek.aoyun.entity.po.roster.RosterLeaveAbsenceInfo;
import com.aoyuntek.aoyun.entity.po.roster.RosterLeaveItemInfo;
import com.aoyuntek.aoyun.entity.po.roster.RosterShiftInfo;
import com.aoyuntek.aoyun.entity.po.roster.RosterShiftTimeInfo;
import com.aoyuntek.aoyun.entity.po.roster.RosterStaffInfo;
import com.aoyuntek.aoyun.entity.vo.ClosurePeriodsVo;
import com.aoyuntek.aoyun.entity.vo.PayrollVo;
import com.aoyuntek.aoyun.entity.vo.RoleInfoVo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.aoyun.entity.vo.StaffSignInfoVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.base.UserVo;
import com.aoyuntek.aoyun.entity.vo.roster.RosterLeaveVo;
import com.aoyuntek.aoyun.entity.vo.roster.RosterRoleDayVo;
import com.aoyuntek.aoyun.entity.vo.roster.RosterShiftTimeVo;
import com.aoyuntek.aoyun.entity.vo.roster.RosterShiftVo;
import com.aoyuntek.aoyun.entity.vo.roster.RosterStaffShiftTimeVo;
import com.aoyuntek.aoyun.entity.vo.roster.RosterStaffShiftVo;
import com.aoyuntek.aoyun.entity.vo.roster.RosterStaffVo;
import com.aoyuntek.aoyun.entity.vo.roster.RosterWeekVo;
import com.aoyuntek.aoyun.enums.ArchivedStatus;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.Holiday;
import com.aoyuntek.aoyun.enums.LeaveType;
import com.aoyuntek.aoyun.enums.PayrollCategory;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.enums.SigninState;
import com.aoyuntek.aoyun.enums.SigninType;
import com.aoyuntek.aoyun.enums.StaffType;
import com.aoyuntek.aoyun.enums.UserType;
import com.aoyuntek.aoyun.enums.roster.RosterStaffType;
import com.aoyuntek.aoyun.enums.roster.RosterStatus;
import com.aoyuntek.aoyun.enums.roster.RosterTemplateFlag;
import com.aoyuntek.aoyun.factory.RosterFactory;
import com.aoyuntek.aoyun.factory.UserFactory;
import com.aoyuntek.aoyun.service.attendance.IAttendanceCoreService;
import com.aoyuntek.aoyun.service.impl.BaseWebServiceImpl;
import com.aoyuntek.aoyun.service.roster.IRosterService;
import com.theone.common.util.ServiceResult;
import com.theone.date.util.DateUtil;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

@Service
public class RosterServiceImpl extends BaseWebServiceImpl<RosterStaffInfo, RosterStaffInfoMapper> implements IRosterService {

	@Autowired
	private RosterShiftInfoMapper rosterShiftInfoMapper;
	@Autowired
	private RosterShiftTimeInfoMapper rosterShiftTimeInfoMapper;
	@Autowired
	private RosterInfoMapper rosterInfoMapper;
	@Autowired
	private RosterStaffInfoMapper rosterStaffInfoMapper;
	@Autowired
	private RosterFactory rosterFactory;
	@Autowired
	private UserInfoMapper userInfoMapper;
	@Autowired
	private SigninInfoMapper signinInfoMapper;
	@Autowired
	private AccountRoleInfoMapper accountRoleInfoMapper;
	@Autowired
	private RoleInfoMapper roleInfoMapper;
	@Autowired
	private UserFactory userFactory;
	@Autowired
	private AccountInfoMapper accountInfoMapper;
	@Autowired
	private IAttendanceCoreService attendanceCoreService;
	@Autowired
	private TempRosterCenterInfoMapper tempRosterCenterInfoMapper;
	@Autowired
	private RosterLeaveItemInfoMapper rosterLeaveItemInfoMapper;
	@Autowired
	private RosterLeaveAbsenceInfoMapper rosterLeaveAbsenceInfoMapper;
	@Autowired
	private LeaveInfoMapper leaveInfoMapper;
	@Autowired
	private ClosurePeriodMapper closurePeriodMapper;
	@Autowired
	private ClosurePeriodCentresMapper closurePeriodCentresMapper;
	@Autowired
	private EventInfoMapper eventInfoMapper;
	@Autowired
	private StaffEmploymentInfoMapper staffEmploymentInfoMapper;
	@Autowired
	private CentersInfoMapper centersInfoMapper;
	@Autowired
	private HolidaypayInfoMapper holidaypayInfoMapper;

	@Override
	public ServiceResult<RosterStaffVo> dealRosterAsTemplate(String rosterId, boolean templateFlag, String name, UserInfoVo currentUser) {
		// 只有CEO 本园院长才能操作
		RosterInfo rosterInfoOld = rosterInfoMapper.selectByPrimaryKey(rosterId);
		boolean isCeo = containRole(Role.CEO, currentUser.getRoleInfoVoList()) == 1;
		boolean isCentreManager = (containRole(Role.CentreManager, currentUser.getRoleInfoVoList()) == 1)
				|| (containRole(Role.EducatorSecondInCharge, currentUser.getRoleInfoVoList()) == 1);
		if (isCentreManager && !rosterInfoOld.getCenterId().equals(currentUser.getUserInfo().getCentersId())) {
			isCentreManager = false;
		}
		if (!isCeo && !isCentreManager) {
			return failResult(getTipMsg("no.permissions"));
		}

		// 判断名称是否重复
		if (templateFlag) {
			RosterInfo rosterInfo = rosterInfoMapper.getByName(name, RosterTemplateFlag.Yes.getValue(), rosterInfoOld.getCenterId());
			if (rosterInfo != null && !rosterInfo.getId().equals(rosterId)) {
				return failResult(getTipMsg("Roster.staff.save.Templates.name.repate"));
			}
		}

		RosterInfo model = rosterInfoMapper.selectByPrimaryKey(rosterId);
		model.setTemplateFlag(templateFlag ? RosterTemplateFlag.Yes.getValue() : RosterTemplateFlag.NO.getValue());
		model.setUpdateAccountId(currentUser.getAccountInfo().getId());
		model.setUpdateTime(new Date());
		model.setTemplateName(templateFlag ? name : "");
		rosterInfoMapper.updateByPrimaryKey(model);
		return successResult(getTipMsg("common_udpate"));
	}

	/**
	 * 教研数据
	 * 
	 * @author dlli5 at 2016年12月23日下午1:58:20
	 * @param rosterId
	 * @return
	 */
	private ServiceResult<RosterStaffVo> validateData(String rosterId) {
		Date now = new Date();
		RosterInfo rosterInfo = rosterInfoMapper.selectByPrimaryKey(rosterId);
		RosterShiftVo rosterShiftVo = rosterShiftInfoMapper.selectVoById(rosterInfo.getShiftId());
		// 每天循环判断
		Date weekStart = rosterInfo.getStartDate();
		StringBuilder sbAlert = new StringBuilder();
		for (int i = 0; i < 7; i++) {
			weekStart = DateUtil.addDay(rosterInfo.getStartDate(), i);
			if (i == 0 || i == 6) {
				continue;
			}
			StringBuffer sbDay = new StringBuffer();
			if (ListUtil.isNotEmpty(rosterShiftVo.getRosterShiftTimeVos())) {
				// 1、第一个shift安排的人员是CS和FAO
				RosterShiftTimeVo frist = rosterShiftVo.getRosterShiftTimeVos().get(0);
				RosterStaffInfo fristStaff = rosterStaffInfoMapper.getFristOfRosterTime(rosterId, frist.getCode(), weekStart);
				if (fristStaff != null) {
					List<RoleInfo> roles = roleInfoMapper.getRoleInfoByAccountId(fristStaff.getStaffAccountId(), false);
					boolean haveRoles = containRolesOr(roles, Role.EducatorCertifiedSupersor, Role.EducatorFirstAidOfficer);
					if (!haveRoles) {
						sbDay.append(getTipMsg("Roster.staff.validate.1"));
					}
				}
				// 2、最后一个shift安排人员是CS和FAO
				if (rosterShiftVo.getRosterShiftTimeVos().size() > 1) {
					RosterShiftTimeVo last = rosterShiftVo.getRosterShiftTimeVos().get(rosterShiftVo.getRosterShiftTimeVos().size() - 1);
					RosterStaffInfo lastStaff = rosterStaffInfoMapper.getFristOfRosterTime(rosterId, last.getCode(), weekStart);
					if (lastStaff != null) {
						List<RoleInfo> roles = roleInfoMapper.getRoleInfoByAccountId(lastStaff.getStaffAccountId(), false);
						boolean haveRoles = containRolesOr(roles, Role.EducatorCertifiedSupersor, Role.EducatorFirstAidOfficer);
						if (!haveRoles) {
							sbDay.append(getTipMsg("Roster.staff.validate.2"));
						}
					}
				}
			}

			// 3、园区有多少小孩对应几个ECT,1-39:1ECT;40-59:2;60-89:3;90+:4
			int tempDay = com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(weekStart, now);
			int childCount = 0;
			// 过去
			if (tempDay > 0) {
				childCount = signinInfoMapper.getCentreChildLogCount(rosterInfo.getCenterId(), weekStart);
			}
			// 今天
			else if (tempDay == 0) {
				childCount = attendanceCoreService.getTodaySignChilds(rosterInfo.getCenterId(), null, (short) i, weekStart).size();
			}
			// 未来
			else {
				childCount = attendanceCoreService.getFutureRoomChilds(rosterInfo.getCenterId(), null, weekStart, (short) i).size();
			}
			// 计算需要的ECT
			int needEctStaff = rosterFactory.getNeedEctStaff(childCount);
			int getEctOfRosterStaff = rosterStaffInfoMapper.getRosterStaffCountWithRole(rosterId, weekStart, Role.EducatorECT.getValue());
			if (getEctOfRosterStaff < needEctStaff) {
				sbDay.append(getTipMsg("Roster.staff.validate.3"));
			}
			// 获取今天安排的员工
			List<RosterStaffVo> rosterStaffVos = rosterStaffInfoMapper.getShiftListByRosterIdAndDay(rosterId, weekStart);
			// 4、当天半数人员以上必须是ECT或者Diploma
			// update by big at 2017-02-07 去掉cook 总人数
			// int total = rosterStaffVos.size();
			int total = 0;
			int roleSize = 0;
			int el = 0;
			for (RosterStaffVo rosterStaffVo : rosterStaffVos) {
				if (rosterStaffVo.getStaffType() != RosterStaffType.Cook.getValue()) {
					total++;
				}
				// if(!containRoles(rosterStaffVo.getRoleInfos(), Role.Cook)){
				// total++;
				// }
				if ((rosterStaffVo.getStaffType() == RosterStaffType.AdditionalStaff.getValue()
						|| rosterStaffVo.getStaffType() == RosterStaffType.CenterManager.getValue()
						|| containRoles(rosterStaffVo.getRoleInfos(), Role.EducatorECT, Role.Diploma))
						&& rosterStaffVo.getStaffType() != RosterStaffType.Cook.getValue()) {
					roleSize++;
				}
				if (containRoles(rosterStaffVo.getRoleInfos(), Role.Educational_Leader)) {
					el++;
				}
			}
			if (roleSize < (total % 2 == 0 ? (total / 2) : (total / 2 + 1))) {
				sbDay.append(getTipMsg("Roster.staff.validate.4"));
			}
			// 7、每一个园每天必须有一个EL
			if (el < 1) {
				sbDay.append(getTipMsg("Roster.staff.validate.5"));
			}
			if (StringUtil.isNotEmpty(sbDay.toString())) {
				sbAlert.append(DateUtil.format(weekStart, DateFormatterConstants.SYS_FORMAT2) + ":<br/>" + sbDay + "<br/>");
			}
		}
		if (StringUtil.isNotEmpty(sbAlert.toString())) {
			return failResult(11, MessageFormat.format("<div style=\"text-align:left;\">{0}</div>", sbAlert.toString()));
		}
		return successResult();
	}

	@Override
	public ServiceResult<RosterStaffVo> updateRosterStatu(String rosterId, short statu, UserInfoVo currentUser, HttpServletRequest request, Boolean alert)
			throws Exception {
		RosterInfo model = rosterInfoMapper.selectByPrimaryKey(rosterId);
		// 只有CEO 本园院长才能操作
		boolean isCeo = containRole(Role.CEO, currentUser.getRoleInfoVoList()) == 1;
		boolean isCentreManager = (containRole(Role.CentreManager, currentUser.getRoleInfoVoList()) == 1)
				|| (containRole(Role.EducatorSecondInCharge, currentUser.getRoleInfoVoList()) == 1);
		if (isCentreManager && !currentUser.getUserInfo().getCentersId().equals(model.getCenterId())) {
			isCentreManager = false;
		}
		if (!isCeo && !isCentreManager) {
			return failResult(getTipMsg("no.permissions"));
		}

		ServiceResult<RosterStaffVo> validateResult = null;
		if (alert) {
			validateResult = validateData(rosterId);
			if (!validateResult.isSuccess()) {
				return validateResult;
			}
		}

		// 如果已经是Approved状态 那么就无法撼动了
		if (model.getState() == RosterStatus.Approved.getValue()) {
			// 重新发送邮件
			rosterFactory.sendEmailOfRoster(model, currentUser, new Date());
			return successResult(getTipMsg("common_udpate"));
		}

		// 如果想从大往低走 就算成功
		if (statu < model.getState()) {
			return successResult(getTipMsg("common_udpate"));
		}

		// 如果提交为Approved状态 那么生成各个角色的临时信息 切生成task
		if (statu == RosterStatus.Approved.getValue()) {
			// 判断是否是当周
			Date nowWeekBegin = DateUtil.getWeekStart(new Date());
			Date weekBegin = DateUtil.getWeekStart(model.getStartDate());
			// 如果是当周 切 当天不等于周六周末 那么生成role以及task
			int dayOfWeek = DateUtil.getDayOfWeek(new Date()) - 1;
			if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(nowWeekBegin, weekBegin) == 0 && (dayOfWeek != 0 && dayOfWeek != 6)) {
				// 更新后 更新session
				if (rosterFactory.approvedRoster(model, currentUser, new Date())) {
					userFactory.updateSession(request);
				}
			}
			model.setApprovedAccountId(currentUser.getAccountInfo().getId());
			model.setApprovedDate(new Date());
			rosterFactory.sendEmailOfRoster(model, currentUser, new Date());
		} else if (statu == RosterStatus.Submitted.getValue()) {
			model.setSubmitAccountId(currentUser.getAccountInfo().getId());
			model.setSubmitDate(new Date());
		}
		model.setState(statu);
		model.setUpdateAccountId(currentUser.getAccountInfo().getId());
		model.setUpdateTime(new Date());
		rosterInfoMapper.updateByPrimaryKey(model);
		return successResult(getTipMsg("common_udpate"));
	}

	@Override
	public ServiceResult<RosterStaffVo> updateRosterStaffItem(RosterStaffVo rosterStaffInfo, UserInfoVo currentUser, Boolean comfirmSure) {
		RosterStaffInfo old = rosterStaffInfoMapper.selectByPrimaryKey(rosterStaffInfo.getId());
		RosterInfo rosterInfo = rosterInfoMapper.selectByPrimaryKey(rosterStaffInfo.getRosterId());
		String centreId = rosterInfo.getCenterId();

		boolean isCeo = containRole(Role.CEO, currentUser.getRoleInfoVoList()) == 1;
		boolean isCentreManager = (containRole(Role.CentreManager, currentUser.getRoleInfoVoList()) == 1)
				|| (containRole(Role.EducatorSecondInCharge, currentUser.getRoleInfoVoList()) == 1);
		if (isCentreManager && !centreId.equals(currentUser.getUserInfo().getCentersId())) {
			isCentreManager = false;
		}
		if (!isCeo && !isCentreManager) {
			return failResult(getTipMsg("no.permissions"));
		}

		ServiceResult<RosterStaffVo> result = validateCasualTime(rosterStaffInfo, rosterInfo, null);
		if (!result.isSuccess()) {
			return result;
		}

		// 获取签到人角色
		List<RoleInfoVo> roleInfoVos = userInfoMapper.getRoleByAccount(rosterStaffInfo.getStaffAccountId());
		// 判断是否是兼职,兼职可以多园区签到.
		boolean isCasual = containRoles(roleInfoVos, Role.Casual);

		old.setUpdateAccountId(currentUser.getAccountInfo().getId());
		old.setUpdateTime(new Date());

		String dayStr = DateUtil.format(old.getDay(), DateUtil.yyyyMMdd);
		Date newStartTime = DateUtil.parse(dayStr + DateUtil.format(rosterStaffInfo.getStartTime(), DateUtil.HHmm), DateUtil.yyyyMMddHHmm);
		Date newEndTime = DateUtil.parse(dayStr + DateUtil.format(rosterStaffInfo.getEndTime(), DateUtil.HHmm), DateUtil.yyyyMMddHHmm);

		// 如果是兼职 那么进行处理
		if (isCasual) {
			rosterFactory.dealChangeTimeOfCasual(old.getId(), newStartTime, newEndTime, rosterInfo);
		}

		old.setEndTime(newEndTime);
		old.setStartTime(newStartTime);
		rosterStaffInfoMapper.updateByPrimaryKey(old);

		// 更新签到信息
		if (rosterStaffInfo.getSignInDate() != null) {
			String newSignDayStr = DateUtil.format(rosterStaffInfo.getDay(), DateUtil.yyyyMMdd);
			rosterStaffInfo.setSignInDate(DateUtil.parse(newSignDayStr + DateUtil.format(rosterStaffInfo.getSignInDate(), DateUtil.HHmm), DateUtil.yyyyMMddHHmm));
			// 是否需要补签到
			boolean needFullDate = false;
			// 如果签到时间不是今天 那么检查签到的那个时间是不是有签到信息
			if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(rosterStaffInfo.getDay(), old.getDay()) != 0) {
				// 获取这天是否有签到信息
				StaffSignInfoVo tempOtherDay = signinInfoMapper.getStaffSignByAccountId(rosterStaffInfo.getStaffAccountId(), rosterStaffInfo.getDay(), centreId);
				if (tempOtherDay != null && StringUtil.isNotEmpty(tempOtherDay.getSigninId()) && tempOtherDay.getSignInState() != SigninState.NoSignin.getValue()) {
					TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
					return failResult(getTipMsg("Roster.staff.update.signin.other.day.allready.have"));
				}

				// 如果是新增 那么如果没有排班 那么提示？//add by big
				if (!comfirmSure && tempOtherDay == null) {
					// 查看是否有排班
					int count = rosterStaffInfoMapper.getStaffAttendance(null, rosterStaffInfo.getStaffAccountId(), rosterStaffInfo.getDay());
					if (count == 0) {
						TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
						return failResult(2, getTipMsg("singn.staff.signinstaff.no.attance"));
					}
				}

				// 如果原来的天是过去 那么说明需要补签到
				if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(old.getDay(), new Date()) > 0) {
					needFullDate = true;
				}
			}

			// 获取这天是否有签到信息
			StaffSignInfoVo staffSignInfoVo = signinInfoMapper.getStaffSignByAccountId(rosterStaffInfo.getStaffAccountId(), old.getDay(), centreId);
			if (staffSignInfoVo != null && staffSignInfoVo.getSignInState() == SigninState.NoSignin.getValue()) {
				needFullDate = false;
			}

			// 新增
			boolean needAdd = staffSignInfoVo == null || StringUtil.isEmpty(staffSignInfoVo.getSigninId());
			// 改变了天且老的已经补签到
			boolean changeSignDay = com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(old.getDay(), rosterStaffInfo.getSignInDate()) != 0
					&& (!needAdd && staffSignInfoVo.getSignInState() == SigninState.NoSignin.getValue());
			if (needAdd || changeSignDay) {
				if (!isCasual) {
					// 删除这天有的补签到信息
					signinInfoMapper.deleteNoSign(rosterStaffInfo.getStaffAccountId(), rosterStaffInfo.getSignInDate());
				} else {
					signinInfoMapper.deleteNoSignOfCentre(rosterStaffInfo.getStaffAccountId(), rosterStaffInfo.getSignInDate(), rosterInfo.getCenterId());
				}
				SigninInfo signinInfo = new SigninInfo();
				signinInfo.setAccountId(rosterStaffInfo.getStaffAccountId());
				signinInfo.setCentreId(centreId);
				signinInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
				signinInfo.setCreateTime(new Date());
				signinInfo.setDeleteFlag(DeleteFlag.Default.getValue());
				signinInfo.setId(UUID.randomUUID().toString());
				signinInfo.setSignDate(rosterStaffInfo.getSignInDate());
				signinInfo.setState(SigninState.Signin.getValue());
				signinInfo.setType(SigninType.IN.getValue());
				signinInfoMapper.insert(signinInfo);
			} else {
				// 删除这天有的补签到信息
				// 如果时间发生改变 那么删除修改后的时间的未签到信息
				if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(old.getDay(), rosterStaffInfo.getSignInDate()) != 0) {
					if (!isCasual) {
						signinInfoMapper.deleteNoSign(rosterStaffInfo.getStaffAccountId(), rosterStaffInfo.getSignInDate());
					} else {
						signinInfoMapper.deleteNoSignOfCentre(rosterStaffInfo.getStaffAccountId(), rosterStaffInfo.getSignInDate(), rosterInfo.getCenterId());
					}
				}

				SigninInfo signinInfo = signinInfoMapper.selectByPrimaryKey(staffSignInfoVo.getSigninId());

				if (!isCasual) {
					// 是否要改变签到园区 如果这一天又排班信息
					RosterStaffInfo userRosterInfo = rosterStaffInfoMapper.getStaffAttendanceInfo(rosterStaffInfo.getStaffAccountId(), rosterStaffInfo.getSignInDate());
					if (userRosterInfo != null) {
						RosterInfo otherRoster = rosterInfoMapper.selectByPrimaryKey(userRosterInfo.getRosterId());
						signinInfo.setCentreId(otherRoster.getCenterId());
					}
				}
				signinInfo.setSignDate(rosterStaffInfo.getSignInDate());
				signinInfo.setState(SigninState.Signin.getValue());
				signinInfoMapper.updateByPrimaryKey(signinInfo);
			}

			// 补坑
			if (needFullDate) {
				SigninInfo signinInfo = new SigninInfo();
				signinInfo.setAccountId(rosterStaffInfo.getStaffAccountId());
				signinInfo.setCentreId(rosterInfo.getCenterId());
				signinInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
				signinInfo.setCreateTime(new Date());
				signinInfo.setDeleteFlag(DeleteFlag.Default.getValue());
				signinInfo.setId(UUID.randomUUID().toString());
				signinInfo.setSignDate(old.getDay());
				signinInfo.setState(SigninState.NoSignin.getValue());
				signinInfo.setType(SigninType.IN.getValue());
				signinInfoMapper.insert(signinInfo);
			}

			if (rosterStaffInfo.getSignOutDate() != null) {
				rosterStaffInfo.setSignOutDate(DateUtil.parse(newSignDayStr + DateUtil.format(rosterStaffInfo.getSignOutDate(), DateUtil.HHmm), DateUtil.yyyyMMddHHmm));
				if (staffSignInfoVo != null && StringUtil.isNotEmpty(staffSignInfoVo.getSignoutId())) {
					SigninInfo signinOutfo = signinInfoMapper.selectByPrimaryKey(staffSignInfoVo.getSignoutId());
					signinOutfo.setSignDate(rosterStaffInfo.getSignOutDate());
					signinOutfo.setState(SigninState.Signin.getValue());
					signinInfoMapper.updateByPrimaryKey(signinOutfo);
				} else {
					SigninInfo signinInfo = new SigninInfo();
					signinInfo.setAccountId(rosterStaffInfo.getStaffAccountId());
					signinInfo.setCentreId(centreId);
					signinInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
					signinInfo.setCreateTime(new Date());
					signinInfo.setDeleteFlag(DeleteFlag.Default.getValue());
					signinInfo.setId(UUID.randomUUID().toString());
					signinInfo.setSignDate(rosterStaffInfo.getSignOutDate());
					signinInfo.setState(SigninState.Signin.getValue());
					signinInfo.setType(SigninType.OUT.getValue());
					signinInfoMapper.insert(signinInfo);
				}
			} else {
				// 删除今天的迁出信息
				if (staffSignInfoVo != null && StringUtil.isNotEmpty(staffSignInfoVo.getSignoutId())) {
					signinInfoMapper.removeSignById(staffSignInfoVo.getSignoutId());
				}
			}

		} else {
			if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(old.getDay(), new Date()) == 0) {
				if (!isCasual) {
					signinInfoMapper.deleteNoSign(rosterStaffInfo.getStaffAccountId(), old.getDay());
				} else {
					signinInfoMapper.deleteNoSignOfCentre(rosterStaffInfo.getStaffAccountId(), old.getDay(), rosterInfo.getCenterId());
				}
			}

			boolean needFullDate = false;
			StaffSignInfoVo staffSignInfoVo = signinInfoMapper.getStaffSignByAccountId(rosterStaffInfo.getStaffAccountId(), old.getDay(), centreId);
			// 删除今天的签到信息
			if (staffSignInfoVo != null && StringUtil.isNotEmpty(staffSignInfoVo.getSigninId())) {
				SigninInfo signIn = signinInfoMapper.selectByPrimaryKey(staffSignInfoVo.getSigninId());
				if (signIn.getState() != SigninState.NoSignin.getValue()) {
					signinInfoMapper.removeSignById(staffSignInfoVo.getSigninId());
					needFullDate = true;
				}
			} else {
				needFullDate = true;
			}
			if (needFullDate) {
				if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(old.getDay(), new Date()) > 0) {
					// 补坑
					SigninInfo signinInfo = new SigninInfo();
					signinInfo.setAccountId(rosterStaffInfo.getStaffAccountId());
					signinInfo.setCentreId(rosterInfo.getCenterId());
					signinInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
					signinInfo.setCreateTime(new Date());
					signinInfo.setDeleteFlag(DeleteFlag.Default.getValue());
					signinInfo.setId(UUID.randomUUID().toString());
					signinInfo.setSignDate(old.getDay());
					signinInfo.setState(SigninState.NoSignin.getValue());
					signinInfo.setType(SigninType.IN.getValue());
					signinInfoMapper.insert(signinInfo);
				}
			}
			if (staffSignInfoVo != null && StringUtil.isNotEmpty(staffSignInfoVo.getSignoutId())) {
				signinInfoMapper.removeSignById(staffSignInfoVo.getSignoutId());
			}
		}
		return successResult(getTipMsg("common_save"));
	}

	@Override
	public ServiceResult<RosterStaffVo> getRosterStaff(String objId, UserInfoVo currentUser) {
		RosterStaffVo rosterStaffVo = rosterStaffInfoMapper.getVo(objId);
		// 遇到补签到
		if (rosterStaffVo.getSinState() != null && rosterStaffVo.getSinState() == SigninState.NoSignin.getValue()) {
			rosterStaffVo.setSignInDate(null);
		}

		List<LeaveInfo> leaves = leaveInfoMapper.getLeaves(rosterStaffVo.getStaffAccountId(), rosterStaffVo.getDay());
		rosterStaffVo.setLeaves(leaves);

		return successResult(rosterStaffVo);
	}

	@Override
	public ServiceResult<RosterStaffVo> deleteRosterStaff(String objId, UserInfoVo currentUser, Boolean alert, HttpServletRequest request) {
		Date now = new Date();
		RosterStaffInfo model = rosterStaffInfoMapper.selectByPrimaryKey(objId);
		if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(new Date(), model.getDay()) < 0) {
			return failResult(getTipMsg("Roster.staff.delete.date.fail"));
		}

		RosterInfo rosterInfo = rosterInfoMapper.selectByPrimaryKey(model.getRosterId());
		// 只有CEO 本园院长才能操作
		boolean isCeo = containRole(Role.CEO, currentUser.getRoleInfoVoList()) == 1;
		boolean isCentreManager = (containRole(Role.CentreManager, currentUser.getRoleInfoVoList()) == 1)
				|| (containRole(Role.EducatorSecondInCharge, currentUser.getRoleInfoVoList()) == 1);
		String currtenCentre = StringUtil.isEmpty(currentUser.getUserInfo().getCentersId()) ? "" : currentUser.getUserInfo().getCentersId();
		if (isCentreManager && !currtenCentre.equals(rosterInfo.getCenterId())) {
			isCentreManager = false;
		}
		if (!isCeo && !isCentreManager) {
			return failResult(getTipMsg("no.permissions"));
		}

		model.setDeleteFlag(DeleteFlag.Delete.getValue());
		rosterStaffInfoMapper.updateByPrimaryKey(model);

		// 此事务在需要提示的时候强制回滚
		if (rosterInfo.getState() == RosterStatus.Approved.getValue() && alert) {
			ServiceResult<RosterStaffVo> result = validateData(rosterInfo.getId());
			if (!result.isSuccess()) {
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				return result;
			}
		}

		// 删除角色园区任务安排
		tempRosterCenterInfoMapper.deleteByRosterStaffId(model.getId());

		if (rosterInfo.getState() != RosterStatus.Approved.getValue()) {
			return successResult(getTipMsg("Roster.staff.delete.success"));
		}

		// 删除员工角色关系
		if (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(new Date(), model.getDay()) == 0) {
			// 看是否是兼职
			List<RoleInfoVo> roleList = userInfoMapper.getRoleByAccount(model.getStaffAccountId());
			boolean isCasual = containRoles(roleList, Role.Casual);

			// 如果是兼职将园区至为null
			if (isCasual) {
				// 处理排班时间
				dealTimeScope(model, rosterInfo);
				// 看此是否在职 意思就是说 当前时间点是否在删除时间点的中间 如果是这样那么 重置园区 以及删除零食角色
				if (now.getTime() >= model.getStartTime().getTime() && now.getTime() <= model.getEndTime().getTime()) {
					// 给她的信息去掉园的信息
					UserInfo userInfo = userInfoMapper.getUserInfoByAccountId(model.getStaffAccountId());
					userInfo.setCentersId(null);
					userInfoMapper.updateByPrimaryKey(userInfo);

					// 删除所有临时角色
					accountRoleInfoMapper.deleteTempFlag(model.getStaffAccountId());
				}
			} else {
				// 删除所有临时角色
				accountRoleInfoMapper.deleteTempFlag(model.getStaffAccountId());
			}

			// 如果是自己那么
			if (model.getStaffAccountId().equals(currentUser.getAccountInfo().getId())) {
				userFactory.updateSession(request);
			}
		}

		return successResult(getTipMsg("Roster.staff.delete.success"));
	}

	private void dealTimeScope(RosterStaffInfo rosterStaffInfo, RosterInfo rosterInfo) {
		Date beginTime = rosterStaffInfo.getStartTime();
		Date endTime = rosterStaffInfo.getEndTime();
		if (StringUtil.isNotEmpty(rosterStaffInfo.getShiftTimeCode())) {
			RosterShiftTimeInfo rosterShiftTimeInfo = rosterShiftTimeInfoMapper.getByCode(rosterInfo.getShiftId(), rosterStaffInfo.getShiftTimeCode());
			beginTime = com.aoyuntek.aoyun.uitl.DateUtil.merge2DateTime(rosterStaffInfo.getDay(), rosterShiftTimeInfo.getShiftStartTime());
			endTime = com.aoyuntek.aoyun.uitl.DateUtil.merge2DateTime(rosterStaffInfo.getDay(), rosterShiftTimeInfo.getShiftEndTime());
		}
		rosterStaffInfo.setStartTime(beginTime);
		rosterStaffInfo.setEndTime(endTime);
	}

	private ServiceResult<RosterStaffVo> validateCasualTime(RosterStaffInfo rosterStaffInfo, RosterInfo rosterInfo, String oldRosterInfoId) {
		// 看看时间是否有冲突 针对兼职
		List<RoleInfoVo> roleList = userInfoMapper.getRoleByAccount(rosterStaffInfo.getStaffAccountId());
		boolean isCasual = containRoles(roleList, Role.Casual);
		if (isCasual) {
			// 找到今天此人所有的安排
			List<RosterStaffVo> rosterStaffVos = rosterStaffInfoMapper.getByStaffIdAndDay(rosterStaffInfo.getStaffAccountId(), rosterStaffInfo.getDay());
			if (ListUtil.isEmpty(rosterStaffVos)) {
				return successResult();
			}
			Date beginTime = rosterStaffInfo.getStartTime();
			Date endTime = rosterStaffInfo.getEndTime();
			if (StringUtil.isNotEmpty(rosterStaffInfo.getShiftTimeCode())) {
				RosterShiftTimeInfo rosterShiftTimeInfo = rosterShiftTimeInfoMapper.getByCode(rosterInfo.getShiftId(), rosterStaffInfo.getShiftTimeCode());
				beginTime = com.aoyuntek.aoyun.uitl.DateUtil.merge2DateTime(rosterStaffInfo.getDay(), rosterShiftTimeInfo.getShiftStartTime());
				endTime = com.aoyuntek.aoyun.uitl.DateUtil.merge2DateTime(rosterStaffInfo.getDay(), rosterShiftTimeInfo.getShiftEndTime());
			}
			// 查看安排之间是否有时间冲突
			for (RosterStaffVo rosterStaffVo : rosterStaffVos) {
				if (rosterStaffVo.getRosterId().equals(oldRosterInfoId)) {
					continue;
				}
				if (StringUtil.isNotEmpty(rosterStaffVo.getShiftTimeCode())) {
					RosterInfo rosterInfoTemp = rosterInfoMapper.selectByPrimaryKey(rosterStaffVo.getRosterId());
					RosterShiftTimeInfo rosterShiftTimeInfo = rosterShiftTimeInfoMapper.getByCode(rosterInfoTemp.getShiftId(), rosterStaffVo.getShiftTimeCode());
					Date tempStart = com.aoyuntek.aoyun.uitl.DateUtil.merge2DateTime(rosterStaffInfo.getDay(), rosterShiftTimeInfo.getShiftStartTime());
					Date tempEndTime = com.aoyuntek.aoyun.uitl.DateUtil.merge2DateTime(rosterStaffInfo.getDay(), rosterShiftTimeInfo.getShiftEndTime());
					rosterStaffVo.setStartTime(tempStart);
					rosterStaffVo.setEndTime(tempEndTime);
				}
				if (StringUtil.isNotEmpty(rosterStaffInfo.getId()) && rosterStaffInfo.getId().equals(rosterStaffVo.getId())) {
					continue;
				}
				if (com.aoyuntek.aoyun.uitl.DateUtil.isOverlap(beginTime, endTime, rosterStaffVo.getStartTime(), rosterStaffVo.getEndTime())) {
					return failResult(getTipMsg("Roster.staff.add.day.casual.time.repate"));
				}
			}

		}
		return successResult();
	}

	@Override
	public ServiceResult<RosterStaffVo> addRosterStaff(RosterStaffInfo rosterStaffInfo, UserInfoVo currentUser, Boolean alert, HttpServletRequest request) {
		RosterInfo rosterInfo = rosterInfoMapper.selectByPrimaryKey(rosterStaffInfo.getRosterId());

		// 只有CEO 本园院长才能操作
		boolean isCeo = containRole(Role.CEO, currentUser.getRoleInfoVoList()) == 1;
		boolean isCentreManager = (containRole(Role.CentreManager, currentUser.getRoleInfoVoList()) == 1)
				|| (containRole(Role.EducatorSecondInCharge, currentUser.getRoleInfoVoList()) == 1);
		String centreId = StringUtil.isEmpty(currentUser.getUserInfo().getCentersId()) ? "" : currentUser.getUserInfo().getCentersId();
		if (isCentreManager && !centreId.equals(rosterInfo.getCenterId())) {
			isCentreManager = false;
		}
		if (!isCeo && !isCentreManager) {
			return failResult(getTipMsg("no.permissions"));
		}
		// 验证Employment End Day
		if (checkEmploymentEndDay(rosterStaffInfo)) {
			return failResult(22, getTipMsg("Roster.add.employment.end.date"));
		}
		// 验证base hours
		ServiceResult<RosterStaffVo> validateBaseHoursResult = validateBaseHours(rosterStaffInfo);
		if (!validateBaseHoursResult.isSuccess()) {
			return validateBaseHoursResult;
		}
		// 判断当天是否已经有安排 shift排班部分 只允许一个人排
		// if (rosterStaffInfo.getStaffType() ==
		// RosterStaffType.Sign.getValue()) {
		// int count =
		// rosterStaffInfoMapper.haveStaffWithDay(rosterInfo.getId(),
		// rosterStaffInfo.getShiftTimeCode(), rosterStaffInfo.getDay());
		// if (count > 0) {
		// return failResult(getTipMsg("Roster.staff.add.day.have"));
		// }
		// }

		// 当前除了非兼职以外一天只能有一个安排
		// List<RoleInfoVo> roleList =
		// userInfoMapper.getRoleByAccount(rosterStaffInfo.getStaffAccountId());
		// boolean isCasual = containRoles(roleList, Role.Casual);
		// if (!isCasual) {
		// int count = rosterStaffInfoMapper.getStaffAttendance(null,
		// rosterStaffInfo.getStaffAccountId(), rosterStaffInfo.getDay());
		// if (count > 0) {
		// return failResult(getTipMsg("Roster.staff.add.day.have"));
		// }
		// }

		ServiceResult<RosterStaffVo> result = validateCasualTime(rosterStaffInfo, rosterInfo, null);
		if (!result.isSuccess()) {
			return result;
		}

		rosterStaffInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
		rosterStaffInfo.setCreateTime(new Date());
		rosterStaffInfo.setDeleteFlag(DeleteFlag.Default.getValue());
		rosterStaffInfo.setId(UUID.randomUUID().toString());
		// 员工排班默认带入Shift time
		if (rosterStaffInfo.getStaffType() == RosterStaffType.Sign.getValue()) {
			RosterShiftTimeInfo info = rosterShiftTimeInfoMapper.getByCode(rosterInfo.getShiftId(), rosterStaffInfo.getShiftTimeCode());
			rosterStaffInfo.setStartTime(info.getShiftStartTime());
			rosterStaffInfo.setEndTime(info.getShiftEndTime());
		}
		rosterStaffInfoMapper.insert(rosterStaffInfo);

		if (rosterInfo.getState() == RosterStatus.Approved.getValue() && alert) {
			result = validateData(rosterInfo.getId());
			if (!result.isSuccess()) {
				TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
				return result;
			}
		}

		if (rosterInfo.getState() == RosterStatus.Approved.getValue()) {
			boolean needFlshSession = rosterFactory.approveTempRole(rosterInfo, rosterStaffInfo, currentUser);
			if (needFlshSession) {
				userFactory.updateSession(request);
			}
		}
		// 看看有没有签到，如果签到的园跟这个园不同 那么更新签到信息
		StaffSignInfoVo staffSignInfoVo = signinInfoMapper.getStaffSignByAccountId(rosterStaffInfo.getStaffAccountId(), rosterStaffInfo.getDay(),
				rosterInfo.getCenterId());
		if (staffSignInfoVo != null) {
			if (StringUtil.isNotEmpty(staffSignInfoVo.getSigninId())) {
				SigninInfo signIn = signinInfoMapper.selectByPrimaryKey(staffSignInfoVo.getSigninId());
				if (!signIn.getCentreId().equals(rosterInfo.getCenterId())) {
					signIn.setCentreId(rosterInfo.getCenterId());
					signinInfoMapper.updateByPrimaryKey(signIn);
				}
			}
			if (StringUtil.isNotEmpty(staffSignInfoVo.getSignoutId())) {
				SigninInfo signIn = signinInfoMapper.selectByPrimaryKey(staffSignInfoVo.getSignoutId());
				if (!signIn.getCentreId().equals(rosterInfo.getCenterId())) {
					signIn.setCentreId(rosterInfo.getCenterId());
					signinInfoMapper.updateByPrimaryKey(signIn);
				}
			}
		}

		RosterStaffVo staffVo = rosterStaffInfoMapper.getVo(rosterStaffInfo.getId());
		return successResult(getTipMsg("Roster.staff.add.success"), staffVo);
	}

	private boolean checkEmploymentEndDay(RosterStaffInfo rosterStaffInfo) {
		StaffEmploymentInfo info = staffEmploymentInfoMapper.getCurrentEmploy(rosterStaffInfo.getStaffAccountId());
		if (info == null) {
			return false;
		}
		if (info.getLastDay() == null) {
			return false;
		}
		if (!rosterStaffInfo.getDay().before(info.getLastDay())) {
			return true;
		}
		return false;
	}

	private ServiceResult<RosterStaffVo> validateBaseHours(RosterStaffInfo rosterStaffInfo) {
		if (rosterStaffInfo.isConfirm()) {
			return successResult();
		}

		RosterShiftTimeInfo rosterShiftTimeInfo = rosterShiftTimeInfoMapper.selectByPrimaryKey(rosterStaffInfo.getShiftId());
		if (rosterShiftTimeInfo == null) {
			return successResult();
		}
		Date startTime = format(rosterShiftTimeInfo.getShiftStartTime());
		Date endTime = format(rosterShiftTimeInfo.getShiftEndTime());

		String startWorkTime = "";
		String endWorkTime = "";
		StaffEmploymentInfo info = staffEmploymentInfoMapper.getCurrentEmploy(rosterStaffInfo.getStaffAccountId());
		if (info == null) {
			return successResult();
		}
		switch (getWeekDay(rosterStaffInfo.getDay())) {
		case Calendar.MONDAY:
			startWorkTime = info.getMonStartTime();
			endWorkTime = info.getMonEndTime();
			break;
		case Calendar.TUESDAY:
			startWorkTime = info.getTueStartTime();
			endWorkTime = info.getTueEndTime();
			break;
		case Calendar.WEDNESDAY:
			startWorkTime = info.getWedStartTime();
			endWorkTime = info.getWedEndTime();
			break;
		case Calendar.THURSDAY:
			startWorkTime = info.getThuStartTime();
			endWorkTime = info.getThuEndTime();
			break;
		case Calendar.FRIDAY:
			startWorkTime = info.getFriStartTime();
			endWorkTime = info.getFriEndTime();
			break;
		}

		if (StringUtil.isNotEmpty(startWorkTime) && startTime.getTime() < parse(startWorkTime).getTime()) {
			return failResult(2, getTipMsg("Roster.staff.start.workTime"));
		}
		if (StringUtil.isNotEmpty(endWorkTime) && endTime.getTime() > parse(endWorkTime).getTime()) {
			return failResult(2, getTipMsg("Roster.staff.end.workTime"));
		}

		return successResult();
	}

	private int getWeekDay(Date day) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(day);
		return calendar.get(Calendar.DAY_OF_WEEK);
	}

	private Date format(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
		String dateStr = format.format(date);
		Date time = null;
		try {
			time = format.parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return time;
	}

	private Date parse(String dateStr) {
		SimpleDateFormat format = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
		Date time = null;
		try {
			time = format.parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return time;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ServiceResult<List<SelecterPo>> getRosterStaffOfRole(String p, String rosterId, String centreId, int rosterStaffTypeValue, Date day, String shiftId,
			UserInfoVo currentUser) {
		List<SelecterPo> result = new ArrayList<SelecterPo>();
		List<String> resultIds = new ArrayList<String>();
		List<SelecterPo> available = new ArrayList<SelecterPo>();
		List<SelecterPo> unavailable = new ArrayList<SelecterPo>();

		UserCondition userCondition = new UserCondition();
		userCondition.initAll();
		userCondition.setCentreId(centreId);
		userCondition.setUserType(UserType.Staff.getValue());
		userCondition.setStatus(ArchivedStatus.UnArchived.getValue());
		userCondition.setSortName("v.staff_type,v.last_name");
		userCondition.setSortOrder("ASC");
		userCondition.setNotLeaveFlag(true);
		userCondition.setDay(day);
		userCondition.setKeyWord(p);
		Short region = centersInfoMapper.getRegionBycentreId(centreId);
		userCondition.setRegion(region);
		List<UserVo> list = userInfoMapper.getListByCondtion(userCondition);
		list = userFactory.duplicate(list);
		for (UserVo userVo : list) {
			StaffEmploymentInfo info = staffEmploymentInfoMapper.getCurrentEmploy(userVo.getAccountId());
			String time = getWorkTime(info, day);
			boolean flag = isWorkDay(info, day);
			SelecterPo SelecterPo = new SelecterPo(userVo.getAccountId(), userVo.getName(), userVo.getAvatar(), userVo.getPersonColor(), time, userVo.getStaffType(),
					flag);
			List<RoleInfoVo> roleInfoVos = userInfoMapper.getRoleByAccount(userVo.getAccountId());
			SelecterPo.setRoleInfoVoList(roleInfoVos);
			result.add(SelecterPo);
			resultIds.add(userVo.getAccountId());
		}

		// 获取所有兼职人员【】
		userCondition.setRoleType(Role.Casual.getValue());
		userCondition.setCentreId(null);
		List<UserVo> listOfCasual = userInfoMapper.getListByCondtion(userCondition);
		listOfCasual = userFactory.duplicate(listOfCasual);
		for (UserVo userVo : listOfCasual) {
			if (resultIds.contains(userVo.getAccountId())) {
				continue;
			}
			StaffEmploymentInfo info = staffEmploymentInfoMapper.getCurrentEmploy(userVo.getAccountId());
			String time = getWorkTime(info, day);
			boolean flag = isWorkDay(info, day);
			SelecterPo SelecterPo = new SelecterPo(userVo.getAccountId(), userVo.getName(), userVo.getAvatar(), userVo.getPersonColor(), time, userVo.getStaffType(),
					flag);
			List<RoleInfoVo> roleInfoVos = userInfoMapper.getRoleByAccount(userVo.getAccountId());
			SelecterPo.setRoleInfoVoList(roleInfoVos);
			result.add(SelecterPo);
			resultIds.add(userVo.getAccountId());
		}
		RosterShiftTimeInfo timeInfo = rosterShiftTimeInfoMapper.selectByPrimaryKey(shiftId);
		for (SelecterPo po : result) {
			if (isRepeat(timeInfo, po.getId(), centreId, day)) {
				continue;
			}
			if (po.isWorkDay()) {
				available.add(po);
			} else {
				unavailable.add(po);
			}
		}
		available.addAll(unavailable);
		return successResult(available);
	}

	private boolean isRepeat(RosterShiftTimeInfo timeInfo, String accountId, String centreId, Date day) {
		if (timeInfo == null) {
			return false;
		}
		int c = rosterShiftTimeInfoMapper.getRepeatShiftCount(timeInfo.getShiftStartTime(), timeInfo.getShiftEndTime(), accountId, centreId, day);
		return c > 0 ? true : false;
	}

	private boolean isWorkDay(StaffEmploymentInfo info, Date day) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(day);
		int i = cal.get(Calendar.DAY_OF_WEEK);
		boolean flag = false;
		if (info == null) {
			return flag;
		}
		if (info.getLastDay() != null && !day.before(info.getLastDay())) {
			return flag;
		}
		switch (i) {
		case Calendar.MONDAY:
			if (info.getMonday()) {
				flag = true;
			}
			break;
		case Calendar.TUESDAY:
			if (info.getTuesday()) {
				flag = true;
			}
			break;
		case Calendar.WEDNESDAY:
			if (info.getWednesday()) {
				flag = true;
			}
			break;
		case Calendar.THURSDAY:
			if (info.getThursday()) {
				flag = true;
			}
			break;
		case Calendar.FRIDAY:
			if (info.getFriday()) {
				flag = true;
			}
			break;
		}
		return flag;
	}

	private String getWorkTime(RosterStaffVo vo) {
		if (vo.getStartTime() == null || vo.getEndTime() == null) {
			return null;
		}
		SimpleDateFormat format = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
		return format.format(vo.getStartTime()) + "-" + format.format(vo.getEndTime());
	}

	private String getWorkTime(StaffEmploymentInfo info, Date day) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(day);
		int i = cal.get(Calendar.DAY_OF_WEEK);
		String time = "";
		if (info == null) {
			return time;
		}
		switch (i) {
		case Calendar.MONDAY:
			if (info.getMonday()) {
				time = info.getMonStartTime() + "-" + info.getMonEndTime();
			}
			break;
		case Calendar.TUESDAY:
			if (info.getTuesday()) {
				time = info.getTueStartTime() + "-" + info.getTueEndTime();
			}
			break;
		case Calendar.WEDNESDAY:
			if (info.getWednesday()) {
				time = info.getWedStartTime() + "-" + info.getWedEndTime();
			}
			break;
		case Calendar.THURSDAY:
			if (info.getThursday()) {
				time = info.getThuStartTime() + "-" + info.getThuEndTime();
			}
			break;
		case Calendar.FRIDAY:
			if (info.getFriday()) {
				time = info.getFriStartTime() + "-" + info.getFriEndTime();
			}
			break;
		}

		return time;
	}

	@Override
	public ServiceResult<List<SelecterPo>> getRosterStaffForToday(String accountId, Date today, String p) {
		List<SelecterPo> result = new ArrayList<SelecterPo>();
		// RosterCondition rosterCondition = new RosterCondition();
		// rosterCondition.setCenterId(userInfoMapper.getUserInfoByAccountId(accountId).getCentersId());
		// Date weekStart = DateUtil.getWeekStart(today);
		// rosterCondition.setStartDate(weekStart);
		// List<RosterInfo> rosterInfos =
		// rosterInfoMapper.getList(rosterCondition);
		// List<RosterStaffVo> staffs =
		// rosterStaffInfoMapper.getByRosterIdAndDay(rosterInfos.get(0).getId(),
		// today);
		// 获取该园区所有员工
		UserCondition userCondition = new UserCondition();
		userCondition.initAll();
		userCondition.setUserType(UserType.Staff.getValue());
		userCondition.setStatus(ArchivedStatus.UnArchived.getValue());
		userCondition.setCentreId(userInfoMapper.getUserInfoByAccountId(accountId).getCentersId());
		userCondition.setKeyWord(p);
		List<UserVo> list = userInfoMapper.getListByCondtion(userCondition);
		List<UserVo> vos = userFactory.duplicate(list);
		for (UserVo userVo : vos) {
			SelecterPo SelecterPo = new SelecterPo(userVo.getAccountId(), userVo.getName(), userVo.getAvatar(), userVo.getPersonColor());
			result.add(SelecterPo);
		}
		return successResult(result);
	}

	@Override
	public ServiceResult<RosterWeekVo> createBlankRoster(String centreId, Date day, UserInfoVo currentUser) {
		boolean isCeo = containRole(Role.CEO, currentUser.getRoleInfoVoList()) == 1;
		// 如果不是CEO 必须是该园的园长 才可以操作
		if (!isCeo && (StringUtil.isEmpty(currentUser.getUserInfo().getCentersId()) || !currentUser.getUserInfo().getCentersId().equals(centreId))) {
			return failResult(getTipMsg("no.permissions"));
		}

		// 获取当前centre有没有最新shift
		RosterShiftCondition rosterShiftCondition = new RosterShiftCondition();
		rosterShiftCondition.setCenterId(centreId);
		rosterShiftCondition.setCurrentFlag(true);
		List<RosterShiftInfo> rosterShiftInfos = rosterShiftInfoMapper.getList(rosterShiftCondition);
		if (ListUtil.isEmpty(rosterShiftInfos)) {
			return failResult(getTipMsg("Roster.createBlankRoster.centre.haveno.shift"));
		}

		RosterShiftVo rosterShiftVo = rosterShiftInfoMapper.selectVoById(rosterShiftInfos.get(0).getId());
		if (rosterShiftVo == null || ListUtil.isEmpty(rosterShiftVo.getRosterShiftTimeVos())) {
			return failResult(getTipMsg("Roster.createBlankRoster.shift.no.shifttime"));
		}

		RosterCondition rosterCondition = new RosterCondition();
		rosterCondition.setCenterId(centreId);
		Date weekStart = DateUtil.getWeekStart(day);
		rosterCondition.setStartDate(weekStart);
		List<RosterInfo> rosterInfos = rosterInfoMapper.getList(rosterCondition);
		if (ListUtil.isNotEmpty(rosterInfos)) {
			return failResult(getTipMsg("Roster.createBlankRoster.already.have"));
		}

		// 设置empty的roster信息
		// 保存Roster
		RosterInfo newModel = new RosterInfo();
		newModel.setCenterId(centreId);
		newModel.setCreateAccountId(currentUser.getAccountInfo().getId());
		newModel.setCreateTime(new Date());
		newModel.setDeleteFlag(DeleteFlag.Default.getValue());
		Date startDate = DateUtil.getWeekStart(day);
		newModel.setStartDate(startDate);
		newModel.setEndDate(DateUtil.addDay(startDate, 6));
		newModel.setId(UUID.randomUUID().toString());
		newModel.setShiftId(rosterShiftInfos.get(0).getId());
		newModel.setState(RosterStatus.Draft.getValue());
		newModel.setTemplateFlag(RosterTemplateFlag.NO.getValue());
		rosterInfoMapper.insert(newModel);

		boolean isCurrentWeek = com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(startDate, DateUtil.getWeekStart(new Date())) == 0;
		int dayOfWeek = DateUtil.getDayOfWeek(new Date());

		// 当安排排班时，默认显示当前园的园长在centre manager的第一行中
		// 获取本院院长
		List<AccountInfo> accountInfos = accountInfoMapper.getListOfCentreRole(centreId, Role.CentreManager.getValue());
		for (int i = 0; i < 7; i++) {
			if (i == 0 || i == 6) {
				continue;
			}
			if (isCurrentWeek && (i + 1 < dayOfWeek)) {
				continue;
			}
			for (AccountInfo accountInfo : accountInfos) {
				RosterStaffInfo rosterStaffInfo = new RosterStaffInfo();
				rosterStaffInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
				rosterStaffInfo.setCreateTime(new Date());
				rosterStaffInfo.setDay(DateUtil.addDay(startDate, i));
				rosterStaffInfo.setDeleteFlag(DeleteFlag.Default.getValue());
				rosterStaffInfo.setId(UUID.randomUUID().toString());
				rosterStaffInfo.setRosterId(newModel.getId());
				rosterStaffInfo.setStaffAccountId(accountInfo.getId());
				rosterStaffInfo.setStaffType(RosterStaffType.CenterManager.getValue());
				rosterStaffInfoMapper.insert(rosterStaffInfo);
			}
		}

		RosterStaffCondition condition = new RosterStaffCondition();
		condition.setCenterId(centreId);
		condition.setDay(day);
		return getWeekRoster(condition, currentUser);
	}

	@Override
	public ServiceResult<RosterWeekVo> dealChooseTemplates(String id, Date day, UserInfoVo currentUser, HttpServletRequest request) throws Exception {
		RosterInfo templates = rosterInfoMapper.selectByPrimaryKey(id);

		boolean isCeo = containRole(Role.CEO, currentUser.getRoleInfoVoList()) == 1;
		// 如果不是CEO 必须是该园的园长 才可以操作
		if (!isCeo && (StringUtil.isEmpty(currentUser.getUserInfo().getCentersId()) || !currentUser.getUserInfo().getCentersId().equals(templates.getCenterId()))) {
			return failResult(getTipMsg("no.permissions"));
		}

		// 看看本身有沒有模板
		RosterInfo oldRosterInfo = null;
		RosterCondition rosterCondition = new RosterCondition();
		rosterCondition.setCenterId(templates.getCenterId());
		Date weekStart = DateUtil.getWeekStart(day);
		Date weekStartNow = DateUtil.getWeekStart(new Date());
		rosterCondition.setStartDate(weekStart);
		List<RosterInfo> rosterInfos = rosterInfoMapper.getList(rosterCondition);
		if (ListUtil.isNotEmpty(rosterInfos)) {
			oldRosterInfo = rosterInfos.get(0);
			if (oldRosterInfo.getState() == RosterStatus.Approved.getValue() && (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(weekStart, weekStartNow) == 0)) {
				return failResult(getTipMsg("Roster.staff.ChooseTemplates.current.have.approve.roster"));
			}
			// 园长不能操作未来approve的 RosterInfo
			boolean isCentreManager = (containRole(Role.CentreManager, currentUser.getRoleInfoVoList()) == 1)
					|| (containRole(Role.EducatorSecondInCharge, currentUser.getRoleInfoVoList()) == 1);
			if (isCentreManager && (com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(weekStart, weekStartNow) < 0)
					&& (oldRosterInfo.getState() == RosterStatus.Approved.getValue())) {
				return failResult(getTipMsg("Roster.staff.ChooseTemplates.future.have.approve.roster.centremanager"));
			}
		}

		// 看这个RosterInfo 对应shift 是不是当前
		RosterShiftVo rosterShiftInfo = rosterShiftInfoMapper.selectVoById(templates.getShiftId());
		if (!rosterShiftInfo.getCurrentFlag() && ListUtil.isNotEmpty(rosterShiftInfo.getRosterShiftTimeVos())) {
			return failResult(getTipMsg("Roster.staff.ChooseTemplates"));
		}

		if (oldRosterInfo != null) {
			// 如果选择的是自己
			if (oldRosterInfo.getId().equals(id)) {
				return failResult(getTipMsg("Roster.staff.ChooseTemplates.self"));
			}
			// 删除老的roster
			rosterInfoMapper.deleteRoster(oldRosterInfo.getId());
		}

		// 保存Roster
		RosterInfo newModel = new RosterInfo();
		newModel.setCenterId(templates.getCenterId());
		newModel.setCreateAccountId(currentUser.getAccountInfo().getId());
		newModel.setCreateTime(new Date());
		newModel.setDeleteFlag(DeleteFlag.Default.getValue());
		Date startDate = DateUtil.getWeekStart(day);
		newModel.setStartDate(startDate);
		newModel.setEndDate(DateUtil.addDay(startDate, 6));
		newModel.setId(UUID.randomUUID().toString());
		newModel.setShiftId(templates.getShiftId());
		newModel.setState(RosterStatus.Draft.getValue());
		if (oldRosterInfo != null) {
			newModel.setState(oldRosterInfo.getState());
		}
		newModel.setTemplateFlag(RosterTemplateFlag.NO.getValue());
		rosterInfoMapper.insert(newModel);

		// copy下面的设置 员工设置
		List<RosterStaffVo> rosterStaffVos = rosterStaffInfoMapper.getByRosterId(templates.getId());
		List<String> ids = dealCopyStaff(rosterStaffVos);
		for (RosterStaffVo rosterStaffVo : rosterStaffVos) {
			if (ids.contains(rosterStaffVo.getId())) {
				continue;
			}
			List<RoleInfo> roles = roleInfoMapper.getRoleInfoByAccountId(rosterStaffVo.getStaffAccountId(), false);
			boolean isCasual = containRoles(Role.Casual, roles) == 1;

			// 如果是AdditionalStaff 那么不加入 跳过
			if (rosterStaffVo.getStaffType() == RosterStaffType.AdditionalStaff.getValue()) {
				continue;
			}

			// 看今天有没有排班
			Date rosterDay = DateUtil.addDay(startDate, DateUtil.getDayOfWeek(rosterStaffVo.getDay()) - 1);
			// 如果有排班 直接跳过
			if (!isCasual) {
				// int count = rosterStaffInfoMapper.getStaffAttendance(null,
				// rosterStaffVo.getStaffAccountId(), rosterDay);
				// if (count != 0) {
				// continue;
				// }
			} else {
				RosterStaffInfo rosterStaffInfo = rosterStaffInfoMapper.selectByPrimaryKey(rosterStaffVo.getId());
				rosterStaffInfo.setDay(rosterDay);
				ServiceResult<RosterStaffVo> serviceResult = validateCasualTime(rosterStaffInfo, templates, oldRosterInfo != null ? oldRosterInfo.getId() : null);
				if (!serviceResult.isSuccess()) {
					continue;
				}
			}

			// 看这个人这一天是否请假了
			if (leaveInfoMapper.haveLeave(rosterStaffVo.getStaffAccountId(), rosterDay) > 0) {
				continue;
			}

			// 获取用户信息
			UserCondition condition = new UserCondition();
			List<String> accountIds = new ArrayList<String>();
			accountIds.add(rosterStaffVo.getStaffAccountId());
			condition.setAccountIds(accountIds);
			condition.setStatus(ArchivedStatus.UnArchived.getValue());
			List<UserVo> userList = userInfoMapper.getListByCondtion(condition);
			if (ListUtil.isEmpty(userList)) {
				continue;
			}

			// 如果是非兼职 如果已经转园了 那么不给
			if (!isCasual) {
				if (StringUtil.isNotEmpty(userList.get(0).getCentersId()) && !userList.get(0).getCentersId().equals(templates.getCenterId())) {
					continue;
				}
			}

			RosterStaffInfo rosterStaffInfo = new RosterStaffInfo();
			rosterStaffInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
			// rosterStaffInfo.setCreateTime(com.aoyuntek.aoyun.uitl.DateUtil.merge2DateTime2(rosterDay,
			// rosterStaffVo.getCreateTime()));
			rosterStaffInfo.setCreateTime(weekStartNow);
			weekStartNow = DateUtil.addSeconds(weekStartNow, -1);
			rosterStaffInfo.setDay(rosterDay);
			rosterStaffInfo.setDeleteFlag(DeleteFlag.Default.getValue());
			if (rosterStaffVo.getStartTime() != null) {
				Date startTime = DateUtil.parse(
						DateUtil.format(rosterStaffInfo.getDay(), DateUtil.yyyyMMdd) + DateUtil.format(rosterStaffVo.getStartTime(), DateUtil.HHmm),
						DateUtil.yyyyMMddHHmm);
				Date endTime = DateUtil.parse(DateUtil.format(rosterStaffInfo.getDay(), DateUtil.yyyyMMdd) + DateUtil.format(rosterStaffVo.getEndTime(), DateUtil.HHmm),
						DateUtil.yyyyMMddHHmm);
				rosterStaffInfo.setEndTime(endTime);
				rosterStaffInfo.setStartTime(startTime);
			}

			rosterStaffInfo.setId(UUID.randomUUID().toString());
			rosterStaffInfo.setRosterId(newModel.getId());
			rosterStaffInfo.setShiftTimeCode(rosterStaffVo.getShiftTimeCode());
			rosterStaffInfo.setStaffAccountId(rosterStaffVo.getStaffAccountId());
			rosterStaffInfo.setStaffType(rosterStaffVo.getStaffType());
			rosterStaffInfoMapper.insert(rosterStaffInfo);
		}

		if (oldRosterInfo != null && oldRosterInfo.getState() == RosterStatus.Approved.getValue()) {
			// 让它approve
			updateRosterStatu(newModel.getId(), RosterStatus.Approved.getValue(), currentUser, request, false);
		}
		RosterStaffCondition condition = new RosterStaffCondition();
		condition.setCenterId(templates.getCenterId());
		condition.setDay(day);
		return getWeekRoster(condition, currentUser);
	}

	private List<String> dealCopyStaff(List<RosterStaffVo> rosterStaffVos) {
		List<String> ids = new ArrayList<String>();
		for (int i = 0; i < rosterStaffVos.size(); i++) {
			RosterStaffVo vo = rosterStaffVos.get(i);
			for (int j = i + 1; j < rosterStaffVos.size(); j++) {
				RosterStaffVo r = rosterStaffVos.get(j);
				if (DateUtil.isSameDay(vo.getDay(), r.getDay()) && StringUtil.isNotEmpty(vo.getShiftTimeCode()) && vo.getShiftTimeCode().equals(r.getShiftTimeCode())
						&& !(vo.getId().equals(r.getId()))) {
					ids.add(r.getId());
				}
			}
		}
		return ids;
	}

	@Override
	public ServiceResult<List<SelecterPo>> getRosterStaffTemplates(String centreId) {
		List<SelecterPo> selecterPos = new ArrayList<SelecterPo>();
		List<RosterInfo> rosterInfos = rosterInfoMapper.getRosterStaffTemplates(centreId);
		for (RosterInfo rosterInfo : rosterInfos) {
			selecterPos.add(new SelecterPo(rosterInfo.getId(), rosterInfo.getTemplateName()));
		}
		return successResult(selecterPos);
	}

	@Override
	public ServiceResult<RosterWeekVo> getWeekRoster(RosterStaffCondition condition, UserInfoVo currentUser) {
		// 根据centreId 以及 day 确认Roster
		RosterCondition rosterCondition = new RosterCondition();
		rosterCondition.setCenterId(condition.getCenterId());
		Date weekStart = DateUtil.getWeekStart(condition.getDay());
		rosterCondition.setStartDate(weekStart);
		List<RosterInfo> rosterInfos = rosterInfoMapper.getList(rosterCondition);
		// 获取本周签到的员工
		List<RosterStaffVo> rosterStaffSigns = signinInfoMapper.getStaffSignByCentre(condition.getCenterId(), weekStart);
		if (ListUtil.isEmpty(rosterStaffSigns) && ListUtil.isEmpty(rosterInfos)) {
			return successResult();
		}
		if (ListUtil.isNotEmpty(rosterInfos)) {
			if (rosterInfos.get(0).getState() != RosterStatus.Approved.getValue()) {
				boolean isCeo = containRole(Role.CEO, currentUser.getRoleInfoVoList()) == 1;
				boolean isCentreManager = (containRole(Role.CentreManager, currentUser.getRoleInfoVoList()) == 1)
						|| (containRole(Role.EducatorSecondInCharge, currentUser.getRoleInfoVoList()) == 1);
				if (isCentreManager && !condition.getCenterId().equals(currentUser.getUserInfo().getCentersId())) {
					isCentreManager = false;
				}
				if (!isCeo && !isCentreManager) {
					return successResult();
				}
			} else {
				boolean isParent = containRole(Role.Parent, currentUser.getRoleInfoVoList()) == 1;
				// 如果是家长 那么不可以看到未来的东西
				if (isParent && com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(DateUtil.getWeekStart(new Date()), weekStart) > 0) {
					return successResult();
				}
			}
		}

		RosterWeekVo rosterWeekVo = new RosterWeekVo();
		if (ListUtil.isNotEmpty(rosterInfos)) {
			rosterWeekVo.setRosterInfo(rosterInfos.get(0));
		}

		List<RosterStaffVo> allWeekRosterStaff = new ArrayList<RosterStaffVo>();
		if (rosterWeekVo.getRosterInfo() != null) {
			allWeekRosterStaff = rosterStaffInfoMapper.getByRosterId(rosterWeekVo.getRosterInfo().getId());
		}

		// 組裝每天的角色 对应员工
		RosterStaffType[] roles = RosterStaffType.values();
		List<RosterRoleDayVo> roleDayVos = new ArrayList<RosterRoleDayVo>();
		for (int i = 0; i <= 6; i++) {
			if (i == 0 || i == 6) {
				continue;
			}
			Date day = DateUtil.addDay(weekStart, i);
			Map<Short, List<RosterStaffVo>> roleRosterStaffMap = new HashMap<Short, List<RosterStaffVo>>();

			for (RosterStaffType rosterStaffType : roles) {
				if (rosterStaffType == RosterStaffType.Sign) {
					roleRosterStaffMap.put(rosterStaffType.getValue(), rosterFactory.getRosterStaffListOfDay(rosterStaffSigns, i, allWeekRosterStaff));
					continue;
				}
				roleRosterStaffMap.put(rosterStaffType.getValue(), new ArrayList<RosterStaffVo>());
				for (RosterStaffVo rosterStaffInfo : allWeekRosterStaff) {
					if (StringUtil.isNotEmpty(rosterStaffInfo.getShiftTimeCode())) {
						continue;
					}

					int dayOfWeek = DateUtil.getDayOfWeek(rosterStaffInfo.getDay());
					if (dayOfWeek != DateUtil.getDayOfWeek(day)) {
						continue;
					}

					if (rosterStaffInfo.getStaffType() != rosterStaffType.getValue()) {
						continue;
					}
					rosterStaffInfo.setWorkTime(getWorkTime(rosterStaffInfo));
					rosterStaffInfo.setLeaveTimes(this.getLeaveTimes(rosterStaffInfo));
					roleRosterStaffMap.get(rosterStaffInfo.getStaffType()).add(0, rosterStaffInfo);
				}
			}

			roleDayVos.add(new RosterRoleDayVo(roleRosterStaffMap, day));
		}
		rosterWeekVo.setRoleDayVos(roleDayVos);

		if (rosterWeekVo.getRosterInfo() != null) {
			RosterShiftVo rosterShtfVo = rosterShiftInfoMapper.selectVoById(rosterWeekVo.getRosterInfo().getShiftId());

			List<RosterStaffShiftTimeVo> rosterStaffShiftTimeVos = new ArrayList<RosterStaffShiftTimeVo>();
			// 装载时间对应员工信息
			List<RosterShiftTimeVo> rosterShiftTimeVos = rosterShtfVo.getRosterShiftTimeVos();
			for (int index = 0; index < rosterShiftTimeVos.size(); index++) {
				RosterShiftTimeVo rosterShiftTimeVo = rosterShiftTimeVos.get(index);
				String title = rosterFactory.getTitle(rosterShiftTimeVo);
				List<RosterStaffShiftVo> rosterStaffShiftVos = new ArrayList<RosterStaffShiftVo>();

				for (int i = 0; i <= 6; i++) {
					if (i == 0 || i == 6) {
						continue;
					}
					List<RosterStaffVo> rosterStaffVos = new ArrayList<RosterStaffVo>();
					Date day = DateUtil.addDay(weekStart, i);
					for (RosterStaffVo rosterStaffInfo : allWeekRosterStaff) {
						if (DateUtil.isSameDay(rosterStaffInfo.getDay(), day)) {
							if (StringUtil.isEmpty(rosterStaffInfo.getShiftTimeCode())) {
								continue;
							}
							if (!rosterStaffInfo.getShiftTimeCode().equals(rosterShiftTimeVo.getCode())) {
								continue;
							}
							if (DateUtil.getDayOfWeek(rosterStaffInfo.getDay()) - 1 != i) {
								continue;
							}
							rosterStaffInfo.setWorkTime(getWorkTime(rosterStaffInfo));
							rosterStaffInfo.setLeaveTimes(this.getLeaveTimes(rosterStaffInfo));
							rosterStaffVos.add(rosterStaffInfo);
						}
					}
					rosterStaffShiftVos.add(new RosterStaffShiftVo(day, rosterStaffVos));
				}
				RosterStaffShiftTimeVo vo = new RosterStaffShiftTimeVo(title, rosterStaffShiftVos, index);
				vo.setShiftWeekVos(rosterShiftTimeVo.getShiftWeekVos());
				vo.setCode(rosterShiftTimeVo.getCode());
				vo.setId(rosterShiftTimeVo.getId());
				rosterStaffShiftTimeVos.add(vo);
			}
			rosterWeekVo.setRosterStaffShiftTimeVos(rosterStaffShiftTimeVos);
		}

		List<ClosurePeriodsVo> closurePeriodsVos = new ArrayList<ClosurePeriodsVo>();
		for (int i = 0; i <= 6; i++) {
			Date day = DateUtil.addDay(weekStart, i);
			dealClosurePeriod(closurePeriodsVos, day, condition.getCenterId());
		}
		rosterWeekVo.setClosurePeriodsVos(closurePeriodsVos);

		return successResult(rosterWeekVo);
	}

	private List<String> getLeaveTimes(RosterStaffVo rosterStaffInfo) {
		List<String> leaveTimes = new ArrayList<String>();
		if (rosterStaffInfo.getStartTime() == null || rosterStaffInfo.getEndTime() == null) {
			return leaveTimes;
		}
		List<LeaveInfo> list = leaveInfoMapper.getLeaves(rosterStaffInfo.getStaffAccountId(), rosterStaffInfo.getDay());
		for (LeaveInfo leaveInfo : list) {
			if (leaveInfo.getLeaveType() == LeaveType.ChangeOfRDO.getValue() || leaveInfo.getLeaveType() == LeaveType.ChangeOfShift.getValue()) {
				continue;
			}
			if (LeaveType.getLeaveForDay().contains(leaveInfo.getLeaveType())) {
				String time = DateUtil.formatDate(rosterStaffInfo.getStartTime(), "hh:mm a", Locale.ENGLISH) + "-"
						+ DateUtil.formatDate(rosterStaffInfo.getEndTime(), "hh:mm a", Locale.ENGLISH);
				if (StringUtil.isNotEmpty(time)) {
					leaveTimes.add(time);
				}
			} else {
				Date aStartTime = this.getHHmmss(rosterStaffInfo.getStartTime());
				Date aEndTime = this.getHHmmss(rosterStaffInfo.getEndTime());
				Date bStartTime = this.getHHmmss(leaveInfo.getStartDate());
				Date bEndTime = this.getHHmmss(leaveInfo.getEndDate());

				if (aStartTime.before(bEndTime) && bStartTime.before(aEndTime)) {
					Date startTime = DateUtil.compDate(aStartTime, bStartTime) ? bStartTime : aStartTime;
					Date endTime = DateUtil.compDate(aEndTime, bEndTime) ? aEndTime : bEndTime;
					leaveTimes.add(DateUtil.formatDate(startTime, "hh:mm a", Locale.ENGLISH) + "-" + DateUtil.formatDate(endTime, "hh:mm a", Locale.ENGLISH));
				}
			}
		}
		return leaveTimes;
	}

	private Date getHHmmss(Date date) {
		String dateStr = DateUtil.format(date, DateUtil.HHmmss);
		return DateUtil.parse(dateStr, DateUtil.HHmmss);
	}

	private String dayOfWeekTime(StaffEmploymentInfo info, Date day) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(day);
		int index = calendar.get(Calendar.DAY_OF_WEEK);
		if (index == Calendar.MONDAY && info.getMonday()) {
			return this.isNotEmpty(info.getMonStartTime(), info.getMonEndTime());
		}
		if (index == Calendar.TUESDAY && info.getTuesday()) {
			return this.isNotEmpty(info.getTueStartTime(), info.getTueEndTime());
		}
		if (index == Calendar.WEDNESDAY && info.getWednesday()) {
			return this.isNotEmpty(info.getWedStartTime(), info.getWedEndTime());
		}
		if (index == Calendar.THURSDAY && info.getThursday()) {
			return this.isNotEmpty(info.getThuStartTime(), info.getThuEndTime());
		}
		if (index == Calendar.FRIDAY && info.getFriday()) {
			return this.isNotEmpty(info.getFriStartTime(), info.getFriEndTime());
		}
		return null;
	}

	private String isNotEmpty(String str1, String str2) {
		if (StringUtil.isNotEmpty(str1) && StringUtil.isNotEmpty(str2)) {
			return str1 + "-" + str2;
		}
		return null;
	}

	private void dealClosurePeriod(List<ClosurePeriodsVo> closurePeriodsVos, Date day, String centreId) {
		ClosurePeriodsVo vo = new ClosurePeriodsVo();
		// 获取关园信息
		List<ClosurePeriod> list = closurePeriodMapper.getClosurePeriod(day, centreId);
		if (ListUtil.isNotEmpty(list)) {
			vo.setClosureFlag(true);
			vo.setClosurePeriods(list);
		}
		closurePeriodsVos.add(vo);
	}

	@Override
	public ServiceResult<List<RosterLeaveVo>> getRosterLeaveVoList() {
		List<RosterLeaveVo> list = rosterLeaveItemInfoMapper.selectRosterLeaveVo();
		return successResult(list);
	}

	@Override
	public ServiceResult<Object> saveRosterLeave(RosterLeaveVo rosterLeaveVo, UserInfoVo user) {
		RosterLeaveItemInfo rosterLeaveItemInfo = rosterLeaveVo.getItemList().get(0);
		String accountId = rosterLeaveVo.getAccountId();
		String currentUserId = user.getAccountInfo().getId();
		Date date = new Date();
		RosterLeaveAbsenceInfo rosterAbsenceInfo = rosterLeaveAbsenceInfoMapper.getLeaveInfoByAccountId(accountId);
		String rosterLeaveId = null;

		if (rosterAbsenceInfo == null) {
			RosterLeaveAbsenceInfo rosterLeaveAbsenceInfo = new RosterLeaveAbsenceInfo();
			rosterLeaveId = UUID.randomUUID().toString();
			rosterLeaveAbsenceInfo.setId(rosterLeaveId);
			rosterLeaveAbsenceInfo.setAccountId(accountId);
			rosterLeaveAbsenceInfo.setCreateTime(date);
			rosterLeaveAbsenceInfo.setUpdateTime(date);
			rosterLeaveAbsenceInfo.setCreateAccountId(currentUserId);
			rosterLeaveAbsenceInfo.setUpdateAccountId(currentUserId);
			rosterLeaveAbsenceInfo.setDeleteFlag(DeleteFlag.Default.getValue());
			rosterLeaveAbsenceInfoMapper.insertSelective(rosterLeaveAbsenceInfo);
		} else {

			rosterLeaveId = rosterAbsenceInfo.getId();
		}
		if (StringUtil.isNotEmpty(rosterLeaveItemInfo.getId())) {
			RosterLeaveItemInfo rosterLeaveItemInfo2 = rosterLeaveItemInfoMapper.selectByPrimaryKey(rosterLeaveItemInfo.getId());
			rosterLeaveItemInfo2.setAbsenceReason(rosterLeaveItemInfo.getAbsenceReason());
			rosterLeaveItemInfo2.setBeginAbsenceDate(rosterLeaveItemInfo.getBeginAbsenceDate());
			rosterLeaveItemInfo2.setEndAbsenceDate(rosterLeaveItemInfo.getEndAbsenceDate());
			rosterLeaveItemInfo2.setUpdateAccountId(currentUserId);
			rosterLeaveItemInfo2.setUpdateTime(date);
			rosterLeaveItemInfoMapper.updateByPrimaryKeySelective(rosterLeaveItemInfo2);
		} else {
			rosterLeaveItemInfo.setId(UUID.randomUUID().toString());
			rosterLeaveItemInfo.setRosterLeaveId(rosterLeaveId);
			rosterLeaveItemInfo.setEndAbsenceDate(rosterLeaveItemInfo.getBeginAbsenceDate());
			rosterLeaveItemInfo.setCreateTime(date);
			rosterLeaveItemInfo.setUpdateTime(date);
			rosterLeaveItemInfo.setCreateAccountId(currentUserId);
			rosterLeaveItemInfo.setUpdateAccountId(currentUserId);
			rosterLeaveItemInfo.setDeleteFlag(DeleteFlag.Default.getValue());
			rosterLeaveItemInfoMapper.insertSelective(rosterLeaveItemInfo);
		}

		return successResult((Object) rosterLeaveVo);
	}

	@Override
	public ServiceResult<Object> deleteRosterLeave(String id) {
		String rosterLeaveAbsenceId = rosterLeaveItemInfoMapper.getRosterLeaveIdById(id);
		rosterLeaveItemInfoMapper.deleteRosterLeave(id);

		int account = rosterLeaveItemInfoMapper.selectCountById(rosterLeaveAbsenceId);
		if (account == 0) {
			rosterLeaveAbsenceInfoMapper.deleteRosterLeave(rosterLeaveAbsenceId);
		}

		return successResult();
	}

	@Override
	public ServiceResult<Object> validateFutureRoster(StaffEmploymentInfo info) {
		// 获取未来排班
		List<RosterStaffInfo> list = rosterStaffInfoMapper.getListByAccountIdFutureDate(info.getAccountId());
		if (ListUtil.isEmpty(list)) {
			return successResult();
		}
		boolean returnFlag = true;
		List<RosterStaffInfo> returnList = new ArrayList<RosterStaffInfo>();
		for (RosterStaffInfo r : list) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(r.getDay());
			int w = cal.get(Calendar.DAY_OF_WEEK);

			if (w == Calendar.MONDAY && !info.getMonday()) {
				returnFlag = false;
				returnList.add(r);
			}
			if (w == Calendar.TUESDAY && !info.getTuesday()) {
				returnFlag = false;
				returnList.add(r);
			}
			if (w == Calendar.WEDNESDAY && !info.getWednesday()) {
				returnFlag = false;
				returnList.add(r);
			}
			if (w == Calendar.THURSDAY && !info.getThursday()) {
				returnFlag = false;
				returnList.add(r);
			}
			if (w == Calendar.FRIDAY && !info.getFriday()) {
				returnFlag = false;
				returnList.add(r);
			}
		}
		if (!returnFlag) {
			return failResult((Integer) 2, (Object) returnList);
		}
		return successResult();
	}

	@Override
	public ServiceResult<Object> saveClosurePeriod(ClosurePeriod closurePeriod, UserInfoVo currentUser, HttpServletRequest request) {
		ServiceResult<Object> result = validateClosurePeriod(closurePeriod);
		if (!result.isSuccess()) {
			return result;
		}
		ServiceResult<Object> validateRosterResult = validateRoster(closurePeriod);
		if (!validateRosterResult.isSuccess() && !closurePeriod.isConfirmRoster()) {
			return validateRosterResult;
		}
		ServiceResult<Object> validateEventResult = validateEvent(closurePeriod);
		if (!validateEventResult.isSuccess() && !closurePeriod.isConfirmEvent()) {
			return validateEventResult;
		}
		if (StringUtil.isEmpty(closurePeriod.getId())) {
			closurePeriod.setId(UUID.randomUUID().toString());
			String id = UUID.randomUUID().toString();
			closurePeriod.setChooseCentre(id);
			closurePeriod.setDeleteFlag(false);
			closurePeriodMapper.insert(closurePeriod);
			dealCentres(closurePeriod.getCentres(), id);
		} else {
			ClosurePeriod dbInfo = closurePeriodMapper.selectByPrimaryKey(closurePeriod.getId());
			dbInfo.setDateFrom(closurePeriod.getDateFrom());
			dbInfo.setDateTo(closurePeriod.getDateTo());
			dbInfo.setName(closurePeriod.getName());
			dbInfo.setHoliday(closurePeriod.getHoliday());
			closurePeriodMapper.updateByPrimaryKey(dbInfo);
			dealCentres(closurePeriod.getCentres(), dbInfo.getChooseCentre());
		}

		// 移除排班
		if (closurePeriod.isConfirmRoster()) {
			removeRoster(closurePeriod, currentUser, request);
		}
		// 移除Event
		if (closurePeriod.isConfirmEvent()) {
			removeEvent(closurePeriod);
		}

		return successResult();
	}

	private void removeEvent(ClosurePeriod c) {
		List<EventInfo> list = eventInfoMapper.getListByCentreIdDate(c.getCentres(), c.getDateFrom(), c.getDateTo());
		for (EventInfo e : list) {
			eventInfoMapper.removeEventById(e.getId());
		}
	}

	private ServiceResult<Object> validateEvent(ClosurePeriod closurePeriod) {
		List<EventInfo> list = eventInfoMapper.getListByCentreIdDate(closurePeriod.getCentres(), closurePeriod.getDateFrom(), closurePeriod.getDateTo());
		if (ListUtil.isNotEmpty(list)) {
			return failResult(3);
		}
		return successResult();
	}

	private void removeRoster(ClosurePeriod c, UserInfoVo currentUser, HttpServletRequest request) {
		List<RosterStaffInfo> list = rosterStaffInfoMapper.getListByCentreIdDate(c.getCentres(), c.getDateFrom(), c.getDateTo());
		for (RosterStaffInfo r : list) {
			deleteRosterStaff(r.getId(), currentUser, false, request);
		}
	}

	private ServiceResult<Object> validateClosurePeriod(ClosurePeriod closurePeriod) {
		// 验证
		if (ListUtil.isEmpty(closurePeriod.getCentres())) {
			return failResult(getTipMsg("closure_period_centres"));
		}
		if (closurePeriod.getDateFrom() == null) {
			return failResult(getTipMsg("closure_period_dateform"));
		}
		if (closurePeriod.getDateTo() == null) {
			return failResult(getTipMsg("closure_period_dateto"));
		}
		if (StringUtil.isEmpty(closurePeriod.getId()) && !closurePeriod.getDateFrom().after(currentDate())) {
			return failResult(getTipMsg("closure_period_dateform_now"));
		}
		if (closurePeriod.getDateTo().before(closurePeriod.getDateFrom())) {
			return failResult(getTipMsg("closure_period_date"));
		}
		if (StringUtil.isNotEmpty(closurePeriod.getId()) && !closurePeriod.getDateTo().after(currentDate())) {
			return failResult(getTipMsg("closure_period_date"));
		}
		if (StringUtil.isEmpty(closurePeriod.getName())) {
			return failResult(getTipMsg("closure_period_name"));
		}

		return successResult();
	}

	private ServiceResult<Object> validateRoster(ClosurePeriod c) {
		List<RosterStaffInfo> list = rosterStaffInfoMapper.getListByCentreIdDate(c.getCentres(), c.getDateFrom(), c.getDateTo());
		if (ListUtil.isEmpty(list)) {
			return successResult();
		}
		return failResult(2);
	}

	private Date currentDate() {
		return DateUtil.parse(DateUtil.format(new Date(), "dd/MM/yyyy"), "dd/MM/yyyy");
	}

	private boolean isRepeat(ClosurePeriod closurePeriod) {
		int count = closurePeriodMapper.getRepeatCount(closurePeriod.getDateFrom(), closurePeriod.getDateTo(), closurePeriod.getId(), closurePeriod.getCentres());
		return count > 0 ? true : false;
	}

	private void dealCentres(List<String> centres, String id) {
		closurePeriodCentresMapper.removeById(id);
		for (String centre : centres) {
			ClosurePeriodCentres c = new ClosurePeriodCentres();
			c.setClosurePeriodId(id);
			c.setCentreId(centre);
			c.setDeleteFlag(false);
			closurePeriodCentresMapper.insert(c);
		}
	}

	@Override
	public ServiceResult<Object> deleteClosurePeriod(String id) {
		closurePeriodMapper.removeClosurePeriod(id);
		closurePeriodCentresMapper.removeClosurePeriodCentres(id);
		return successResult();
	}
}
