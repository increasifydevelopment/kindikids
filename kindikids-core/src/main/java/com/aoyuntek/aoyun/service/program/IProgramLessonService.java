package com.aoyuntek.aoyun.service.program;

import java.io.FileNotFoundException;
import java.util.Date;
import java.util.List;

import com.aoyuntek.aoyun.dao.program.ProgramLessonInfoMapper;
import com.aoyuntek.aoyun.entity.po.program.ProgramLessonInfo;
import com.aoyuntek.aoyun.entity.vo.FileVo;
import com.aoyuntek.aoyun.entity.vo.ProgramNewsFeedVo;
import com.aoyuntek.aoyun.entity.vo.ProgramLessonInfoVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.service.IBaseWebService;
import com.theone.common.util.ServiceResult;

/**
 * 
 * @description program lesson 业务逻辑接口
 * @author mingwang
 * @create 2016年9月20日上午10:52:32
 * @version 1.0
 */
public interface IProgramLessonService extends IBaseWebService<ProgramLessonInfo, ProgramLessonInfoMapper> {

    /**
     * 
     * @description 新增/修改program中Lesson
     * @author mingwang
     * @create 2016年9月20日上午11:03:04
     * @version 1.0
     * @param programLessonInfo
     * @param currentUser
     * @return
     */
    ServiceResult<Object> addOrUpdateLesson(ProgramLessonInfoVo programLessonInfoVo, UserInfoVo currentUser, Date date);

    /**
     * 
     * @description 删除program中lesson
     * @author mingwang
     * @create 2016年9月20日下午2:20:09
     * @version 1.0
     * @param lessonId
     * @return
     */
    ServiceResult<Object> removeLesson(String lessonId);

    /**
     * @description 批量上传附件
     * @author hxzhang
     * @create 2016年9月20日下午5:02:53
     * @version 1.0
     * @param fileVoList
     * @throws FileNotFoundException
     * @throws Exception
     */
    void batchUploadAttachments(String sourceId, List<FileVo> fileVoList, String afterPath) throws Exception;

    /**
     * 
     * @description 保存图片
     * @author mingwang
     * @create 2016年9月21日上午9:36:19
     * @version 1.0
     * @param sourceId
     * @param images
     */
    void initImages(String sourceId, List<FileVo> images);

    /**
     * 
     * @description 获取programLessonInfo信息
     * @author mingwang
     * @create 2016年9月21日下午5:15:26
     * @version 1.0
     * @param lessonId
     * @return
     */
    ServiceResult<Object> getLessonInfo(String lessonId);

    /**
     * 
     * @description share to newsfeed (lesson)
     * @author mingwang
     * @create 2016年9月22日上午9:20:00
     * @version 1.0
     * @param programNewsFeedVo
     * @return
     */
    ServiceResult<Object> saveShareNewsfeed(String objId, ProgramNewsFeedVo programNewsFeedVo, String childId);

    /**
     * 
     * @description 将lesson的过期状态更新为overDue
     * @author mingwang
     * @create 2016年9月28日上午10:06:56
     * @version 1.0
     * @param now
     */
    void updateOverDueLessonTimedTask(Date now);

}
