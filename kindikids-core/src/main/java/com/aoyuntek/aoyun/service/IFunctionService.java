package com.aoyuntek.aoyun.service;

import java.util.List;

import com.aoyuntek.aoyun.dao.FunctionInfoMapper;
import com.aoyuntek.aoyun.entity.po.FunctionInfo;
import com.aoyuntek.aoyun.entity.po.MenuInfo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.theone.common.util.ServiceResult;

/**
 * @description Function业务逻辑层接口
 * @author admin
 * @create 2016年6月22日下午12:44:07
 * @version 1.0
 * 
 */
public interface IFunctionService extends IBaseWebService<FunctionInfo, FunctionInfoMapper> {
    /**
     * 
     * @description 获取function集合
     * @author bbq
     * @create 2016年6月22日下午12:44:07
     * @version 1.0
     * @param userInfoVO
     *            当前用户VO实体
     * @param url
     * @return ServiceResult结果集
     */
    ServiceResult<List<FunctionInfo>> getFunctionList(UserInfoVo userInfoVO, String url);

    /**
     * 
     * @description 获取能访问菜单集合
     * @author gfwang
     * @create 2016年9月14日下午1:37:20
     * @version 1.0
     * @param userInfo
     *            当前用户
     * @return ServiceResult<List<MenuInfo>>
     */
    ServiceResult<List<MenuInfo>> getMenuList(UserInfoVo userInfo);
}
