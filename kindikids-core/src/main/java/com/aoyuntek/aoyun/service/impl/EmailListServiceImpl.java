package com.aoyuntek.aoyun.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.condtion.EmailListCondition;
import com.aoyuntek.aoyun.dao.EmailPlanInfoMapper;
import com.aoyuntek.aoyun.entity.po.EmailPlanInfo;
import com.aoyuntek.aoyun.entity.vo.EmailListVo;
import com.aoyuntek.aoyun.service.IEmailListService;
import com.aoyuntek.aoyun.uitl.DateUtil;
import com.aoyuntek.framework.model.Pager;
import com.theone.aws.util.AwsEmailUtil;
import com.theone.common.util.ServiceResult;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

@Service
public class EmailListServiceImpl extends BaseWebServiceImpl<EmailPlanInfo, EmailPlanInfoMapper> implements IEmailListService {

	@Autowired
	private EmailPlanInfoMapper emailPlanInfoMapper;

	@Override
	public ServiceResult<Object> getList(EmailListCondition condition) {
		if (StringUtil.isNotEmpty(condition.getCreateTime())) {
			condition.setTime(DateUtil.parse(condition.getCreateTime(), "dd/MM/yyyy"));
		}
		condition.setStartSize(condition.getPageIndex());
		List<EmailListVo> list = emailPlanInfoMapper.getPagerList(condition);
		int totalSize = emailPlanInfoMapper.getPagerListCount(condition);
		condition.setTotalSize(totalSize);
		condition.setAllIds(emailPlanInfoMapper.getAllIds(condition));
		Pager<EmailListVo, EmailListCondition> pager = new Pager<EmailListVo, EmailListCondition>(list, condition);
		return successResult((Object) pager);
	}

	@Override
	public ServiceResult<Object> sendEmail(EmailListCondition condition) {
		final List<String> ids = condition.getCheckIds();
		if (ListUtil.isEmpty(ids)) {
			return failResult(getTipMsg("email_plan_send_error"));
		}
		final List<EmailPlanInfo> list = emailPlanInfoMapper.getListByIds(ids);
		new Thread(new Runnable() {
			@Override
			public void run() {
				String emailKey = getSystemMsg("email_key");
				AwsEmailUtil emailUtil = new AwsEmailUtil();
				for (EmailPlanInfo email : list) {
					String sendAddress = getSystemMsg("mailHostAccount");
					String sendName = getSystemMsg("email_name");
					try {
						emailUtil.sendgrid(sendAddress, sendName, email.getReceiveraddress(), email.getReceivername(), email.getSub(), email.getMsg(), true, null, null,
								emailKey);
						System.err.println(email.getMsg().toString());
					} catch (Exception e) {
						log.error(e.getMessage(), e);
					}
				}
			}
		}).start();
		emailPlanInfoMapper.updateStatus(ids);
		return successResult(getTipMsg("email_plan_send_success"));
	}
}
