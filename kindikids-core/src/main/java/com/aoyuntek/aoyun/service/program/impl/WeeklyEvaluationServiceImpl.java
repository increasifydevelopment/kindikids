package com.aoyuntek.aoyun.service.program.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.dao.RoomInfoMapper;
import com.aoyuntek.aoyun.dao.RoomTaskCheckMapper;
import com.aoyuntek.aoyun.dao.program.ProgramWeeklyEvalutionInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskInstanceInfoMapper;
import com.aoyuntek.aoyun.entity.po.RoomInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramWeeklyEvalutionInfo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.ProgramState;
import com.aoyuntek.aoyun.enums.ProgramWeeklyType;
import com.aoyuntek.aoyun.enums.TaskType;
import com.aoyuntek.aoyun.service.impl.BaseWebServiceImpl;
import com.aoyuntek.aoyun.service.program.IWeeklyEvaluationService;
import com.aoyuntek.aoyun.service.task.ITaskInstance;
import com.theone.common.util.ServiceResult;
import com.theone.string.util.StringUtil;

/**
 * @description Weekly Evaluation 业务逻辑实现层
 * @author Hxzhang 2016年9月23日下午2:13:33
 * @version 1.0
 */
@Service
public class WeeklyEvaluationServiceImpl extends BaseWebServiceImpl<ProgramWeeklyEvalutionInfo, ProgramWeeklyEvalutionInfoMapper> implements
        IWeeklyEvaluationService {

    @Autowired
    private RoomInfoMapper roomInfoMapper;
    @Autowired
    private ITaskInstance taskInstance;
    @Autowired
    private TaskInstanceInfoMapper taskInstanceInfoMapper;
    @Autowired
    private ProgramWeeklyEvalutionInfoMapper programWeeklyEvalutionInfoMapper;
    @Autowired
    private RoomTaskCheckMapper roomTaskCheckMapper;

    @Override
    public ServiceResult<Object> getWeeklyEvaluationInfo(String id) {
        ProgramWeeklyEvalutionInfo programWeeklyEvalutionInfo = programWeeklyEvalutionInfoMapper.selectByPrimaryKey(id);
        return successResult((Object) programWeeklyEvalutionInfo);
    }

    @Override
    public ServiceResult<Object> addOrUpdateWeeklyEvaluation(ProgramWeeklyEvalutionInfo programWeeklyEvalutionInfo, UserInfoVo currentUser) {
        // 验证
        ServiceResult<Object> result = validateWeeklyEvaluation(programWeeklyEvalutionInfo);
        if (!result.isSuccess()) {
            return result;
        }
        // 新增
        if (StringUtil.isEmpty(programWeeklyEvalutionInfo.getId())) {
            programWeeklyEvalutionInfo.setId(UUID.randomUUID().toString());
            programWeeklyEvalutionInfo.setCenterId(roomInfoMapper.selectCenterIdByRoom(programWeeklyEvalutionInfo.getRoomId()));
            programWeeklyEvalutionInfo.setDay(programWeeklyEvalutionInfo.getDay());
            programWeeklyEvalutionInfo.setState(ProgramState.Pending.getValue());
            programWeeklyEvalutionInfo.setUpdateTime(programWeeklyEvalutionInfo.getDay());
            programWeeklyEvalutionInfo.setDeleteFlag(DeleteFlag.Default.getValue());
            programWeeklyEvalutionInfoMapper.insert(programWeeklyEvalutionInfo);
            // 保存列表记录
            taskInstance.addTaskInstance(TaskType.WeeklyEvalution, programWeeklyEvalutionInfo.getCenterId(), programWeeklyEvalutionInfo.getRoomId(),
                    null, programWeeklyEvalutionInfo.getId(), programWeeklyEvalutionInfo.getCreateAccountId(), programWeeklyEvalutionInfo.getDay());
        } else { // 编辑
            ProgramWeeklyEvalutionInfo db = programWeeklyEvalutionInfoMapper.selectByPrimaryKey(programWeeklyEvalutionInfo.getId());
            db.setUpdateAccountId(currentUser.getAccountInfo().getId());
            db.setUpdateTime(new Date());
            db.setWeeklyEvalution(programWeeklyEvalutionInfo.getWeeklyEvalution());
            programWeeklyEvalutionInfoMapper.updateByPrimaryKey(db);
        }
        return successResult(getTipMsg("operation_success"), (Object) programWeeklyEvalutionInfo);
    }

    /**
     * @description 验证WeeklyEvaluation
     * @author hxzhang
     * @create 2016年9月25日下午3:39:52
     * @version 1.0
     * @param programWeeklyEvalutionInfo
     * @return
     */
    private ServiceResult<Object> validateWeeklyEvaluation(ProgramWeeklyEvalutionInfo programWeeklyEvalutionInfo) {
        // 1.每周五才能生成
        if (StringUtil.isEmpty(programWeeklyEvalutionInfo.getId()) && !isFriday(programWeeklyEvalutionInfo.getDay())) {
            return failResult(getTipMsg("task_save_isNotFriday"));
        }
        // 2.每个room每周五每个type只能生成一条
        if (StringUtil.isEmpty(programWeeklyEvalutionInfo.getId())) {
            ProgramWeeklyEvalutionInfo p = programWeeklyEvalutionInfoMapper.getFridayWeeklyEvalutionByRoomIdAndType(
                    programWeeklyEvalutionInfo.getRoomId(), programWeeklyEvalutionInfo.getDay(), programWeeklyEvalutionInfo.getType());
            if (null != p) {
                return failResult(getTipMsg("task_save_exists"));
            }
        }
        return successResult();
    }

    /**
     * @description 判断是否是星期五
     * @author hxzhang
     * @create 2016年8月18日下午3:18:13
     * @version 1.0
     * @param date
     *            Date
     * @return 返回结果
     */
    private boolean isFriday(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        if (cal.get(Calendar.DAY_OF_WEEK) == 6) {
            return true;
        }
        return false;
    }

    @Override
    public ServiceResult<Object> removeWeeklyEvaluation(String id) {
        programWeeklyEvalutionInfoMapper.removeProgramWeeklyEvalutionInfo(id);
        return successResult(getTipMsg("operation_success"));
    }

    @Override
    public void createWeeklyEvaluationOnFridayTimedTask(Date now) {
        List<RoomInfo> roomList = roomInfoMapper.getAllRoom();
        for (RoomInfo r : roomList) {
            List<Short> taskTypes = roomTaskCheckMapper.getTaskTypeByRoomId(r.getId());
            if (!taskTypes.contains(TaskType.WeeklyEvalution.getValue())) {
                continue;
            }
            for (ProgramWeeklyType type : ProgramWeeklyType.values()) {
                ProgramWeeklyEvalutionInfo p = new ProgramWeeklyEvalutionInfo();
                p.setType(type.getValue());
                p.setRoomId(r.getId());
                p.setDay(now);
                addOrUpdateWeeklyEvaluation(p, null);
            }
        }
    }

    @Override
    public void updateOverDueWeeklyEvaluationTimedTask(Date now) {
        log.info("updateOverDueWeeklyEvaluationTimedTask>>>start");
        // 获取过期的WeeklyEvaluation
        List<String> overDueWeeklyList = programWeeklyEvalutionInfoMapper.getOverDueWeeklyEvalution(now);
        log.info("updateOverDueWeeklyEvaluationTimedTask>>>overDueWeeklyList:" + overDueWeeklyList.size());
        // 将记录表中的记录更新为overDue
        int instanceCount = taskInstanceInfoMapper.updateStatuByValueIds(overDueWeeklyList);
        log.info("updateOverDueWeeklyEvaluationTimedTask>>>instanceCount:" + instanceCount);
        // 将过期记录状态更新为overDue
        int weeklyCount = programWeeklyEvalutionInfoMapper.updateOverDueWeeklyEvalution(now);
        log.info("updateOverDueWeeklyEvaluationTimedTask>>>weeklyCount:" + weeklyCount);
        log.info("updateOverDueWeeklyEvaluationTimedTask>>>end");
    }
}
