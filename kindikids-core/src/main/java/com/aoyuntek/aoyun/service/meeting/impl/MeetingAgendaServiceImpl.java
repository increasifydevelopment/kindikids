package com.aoyuntek.aoyun.service.meeting.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.condtion.MeetingAgendaCondition;
import com.aoyuntek.aoyun.dao.NqsRelationInfoMapper;
import com.aoyuntek.aoyun.dao.meeting.AgendaItemInfoMapper;
import com.aoyuntek.aoyun.dao.meeting.AgendaTypeInfoMapper;
import com.aoyuntek.aoyun.dao.meeting.MeetingAgendaInfoMapper;
import com.aoyuntek.aoyun.dao.meeting.MeetingInfoMapper;
import com.aoyuntek.aoyun.entity.po.NqsRelationInfo;
import com.aoyuntek.aoyun.entity.po.meeting.AgendaItemInfo;
import com.aoyuntek.aoyun.entity.po.meeting.AgendaTypeInfo;
import com.aoyuntek.aoyun.entity.po.meeting.MeetingAgendaInfo;
import com.aoyuntek.aoyun.entity.vo.NqsVo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.meeting.AgendaItemInfoVo;
import com.aoyuntek.aoyun.entity.vo.meeting.MeetingAgendaInfoVo;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.meeting.CurrtenFlag;
import com.aoyuntek.aoyun.enums.meeting.MeetingTempState;
import com.aoyuntek.aoyun.service.impl.BaseWebServiceImpl;
import com.aoyuntek.aoyun.service.meeting.IMeetingAgendaService;
import com.aoyuntek.framework.model.Pager;
import com.theone.common.util.ServiceResult;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

/**
 * 
 * @description MeetingAgenda业务逻辑实现类
 * @author mingwang
 * @create 2016年10月13日上午10:02:53
 * @version 1.0
 */
@Service
public class MeetingAgendaServiceImpl extends BaseWebServiceImpl<MeetingAgendaInfo, MeetingAgendaInfoMapper> implements IMeetingAgendaService {

    @Autowired
    private AgendaTypeInfoMapper agendaTypeInfoMapper;
    @Autowired
    private MeetingAgendaInfoMapper meetingAgendaInfoMapper;
    @Autowired
    private AgendaItemInfoMapper agendaItemInfoMapper;
    @Autowired
    private NqsRelationInfoMapper nqsRelationInfoMapper;
    @Autowired
    private MeetingInfoMapper meetingInfoMapper;

    /**
     * 更新agendaTypeList
     */
    @Override
    public ServiceResult<Object> updateAgendaType(List<AgendaTypeInfo> agendaTypeList, UserInfoVo currentUser) {
        if (ListUtil.isEmpty(agendaTypeList)) {
            // 删除数据库中记录
            int count = agendaTypeInfoMapper.updateDelete();
            log.info("updateAgendaType|updateDelete count=" + count);
            return successResult();
        }
        // 得到所有typeName并判断是否有重复
        List<String> agendaTypeNameList = new ArrayList<String>();
        for (AgendaTypeInfo agendaTypeInfo : agendaTypeList) {
            if (StringUtil.isEmpty(agendaTypeInfo.getTypeName())) {
                return failResult(getTipMsg("agendaTypeName_empty"));
            }
            agendaTypeNameList.add(agendaTypeInfo.getTypeName());
        }
        // 判断是否有重复元素
        if (!isUnique(agendaTypeNameList)) {
            return failResult(getTipMsg("agendaType_repeat"));
        }
        // 获取数据库中所有的agendaType记录
        List<AgendaTypeInfo> dbAgendaTypeList = agendaTypeInfoMapper.getAll();
        // 待删除的agendaType记录
        List<String> agendaTypeList_remove = new ArrayList<String>();
        for (AgendaTypeInfo dbAgendaTypeInfo : dbAgendaTypeList) {
            agendaTypeList_remove.add(dbAgendaTypeInfo.getId());
        }
        // 新增的agendaType记录
        List<AgendaTypeInfo> agendaTypeList_add = new ArrayList<AgendaTypeInfo>();
        // 更新的agendaType记录
        List<AgendaTypeInfo> agendaTypeList_update = new ArrayList<AgendaTypeInfo>();
        // 如果数据库为空，则所有的记录都为新增
        if (ListUtil.isEmpty(dbAgendaTypeList)) {
            // 保存agendaTypeList
            saveAgendaTypeList(agendaTypeList, currentUser);
        } else {
            // 循环list，将agendaType分别放进对应的增删改集合中
            for (AgendaTypeInfo agendaType : agendaTypeList) {
                if (StringUtil.isEmpty(agendaType.getId())) {
                    agendaTypeList_add.add(agendaType);
                    continue;
                }
                for (AgendaTypeInfo dbAgendaType : dbAgendaTypeList) {
                    if (StringUtil.compare(agendaType.getId(), dbAgendaType.getId())) {
                        agendaTypeList_update.add(agendaType);
                        agendaTypeList_remove.remove(agendaType.getId());
                    }
                }
            }
        }
        if (ListUtil.isNotEmpty(agendaTypeList_add)) {
            // 新增
            saveAgendaTypeList(agendaTypeList_add, currentUser);
        }
        if (ListUtil.isNotEmpty(agendaTypeList_update)) {
            // 更新
            updateAgendaTypeList(agendaTypeList_update, currentUser);
        }
        if (ListUtil.isNotEmpty(agendaTypeList_remove)) {
            // 删除
            int count = agendaTypeInfoMapper.batchDeleteAgendaType(agendaTypeList_remove);
            log.info("updateAgendaType|batchDeleteAgendaType count=" + count);
        }
        return successResult(getTipMsg("operation_success"), (Object) agendaTypeList);
    }

    /**
     * 
     * @description 判断集合中是否有重复元素
     * @author mingwang
     * @create 2016年10月17日下午1:34:08
     * @version 1.0
     * @param collection
     * @return
     */
    public static boolean isUnique(List<String> list) {
        return list.size() == new HashSet(list).size();
    }

    /**
     * 
     * @description 更新agendaTypeList
     * @author mingwang
     * @create 2016年10月13日下午1:25:30
     * @version 1.0
     * @param agendaTypeList_update
     * @param currentUser
     */
    private void updateAgendaTypeList(List<AgendaTypeInfo> agendaTypeList, UserInfoVo currentUser) {
        log.info("updateAgendaTypeList|saveAgendaTypeList|start");
        Date now = new Date();
        for (AgendaTypeInfo agendaTypeInfo : agendaTypeList) {
            AgendaTypeInfo dbAgendaTypeInfo = agendaTypeInfoMapper.selectByPrimaryKey(agendaTypeInfo.getId());
            dbAgendaTypeInfo.setTypeName(agendaTypeInfo.getTypeName());
            dbAgendaTypeInfo.setUpdateTime(now);
            dbAgendaTypeInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
            now = new Date(now.getTime() + 1000);
        }
        int count = agendaTypeInfoMapper.batchUpdateAgendaType(agendaTypeList);
        log.info("updateAgendaTypeList|batchUpdateAgendaType count=" + count);
        log.info("updateAgendaTypeList|saveAgendaTypeList|end");
    }

    /**
     * 
     * @description 保存agendaTypeList
     * @author mingwang
     * @create 2016年10月13日下午1:10:49
     * @version 1.0
     * @param agendaTypeList
     */
    private void saveAgendaTypeList(List<AgendaTypeInfo> agendaTypeList, UserInfoVo currentUser) {
        log.info("saveAgendaTypeList|saveAgendaTypeList|start");
        Date now = new Date();
        // 初始化agendaType信息
        for (AgendaTypeInfo agendaTypeInfo : agendaTypeList) {
            agendaTypeInfo.setId(UUID.randomUUID().toString());
            agendaTypeInfo.setCreateTime(now);
            agendaTypeInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
            agendaTypeInfo.setUpdateTime(now);
            agendaTypeInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
            agendaTypeInfo.setDeleteFlag(DeleteFlag.Default.getValue());
            now = new Date(now.getTime() + 1000);
        }
        // 批量插入agendatype
        int count = agendaTypeInfoMapper.batchInsertAgendaType(agendaTypeList);
        log.info("saveAgendaTypeList|batchInsertAgendaType count=" + count);
        log.info("saveAgendaTypeList|saveAgendaTypeList|end");
    }

    /**
     * 获取agendaTypeList
     */
    @Override
    public ServiceResult<Object> getAgendaType() {
        log.info("getAgendaType|start");
        List<AgendaTypeInfo> agendaTypeList = agendaTypeInfoMapper.getAll();
        // 如果数据库查询结果为空，则返回一个空对象
        if (ListUtil.isEmpty(agendaTypeList)) {
            return successResult((Object) new ArrayList<AgendaTypeInfo>());
        }
        log.info("getAgendaType|end");
        return successResult((Object) agendaTypeList);
    }

    /**
     * 获取meetingAgenda
     */
    @Override
    public ServiceResult<Object> getMeetingAgenda(String id) {
        log.info("getMeetingAgenda|start|id=" + id);
        MeetingAgendaInfoVo meetingAgendaInfoVo = null;
        // 如果id为空，返回一个空对象；id不为空，从数据库查找数据返回
        if (StringUtil.isEmpty(id)) {
            meetingAgendaInfoVo = new MeetingAgendaInfoVo();
            AgendaItemInfoVo agendaItemInfo = new AgendaItemInfoVo();
            List<AgendaItemInfoVo> agendaItems = new ArrayList<AgendaItemInfoVo>();
            agendaItems.add(agendaItemInfo);
            meetingAgendaInfoVo.setAgendaItems(agendaItems);
        } else {
            meetingAgendaInfoVo = meetingAgendaInfoMapper.getMeetinAgendaById(id);
            AgendaTypeInfo typeInfo = agendaTypeInfoMapper.selectByPrimaryKey(meetingAgendaInfoVo.getAgendaTypeId());
            meetingAgendaInfoVo.setTypeInfo(typeInfo);
            List<AgendaItemInfoVo> agendaItems = agendaItemInfoMapper.getItemsByAgendaId(id);
            meetingAgendaInfoVo.setAgendaItems(agendaItems);
        }
        log.info("getMeetingAgenda|end");
        return successResult((Object) meetingAgendaInfoVo);
    }

    /**
     * 新增/更新MeetingAgenda
     */
    @Override
    public ServiceResult<Object> addAndUpdateMeetingAgenda(MeetingAgendaInfoVo meetingAgendaInfoVo, UserInfoVo currentUser) {
        log.info("addAndUpdateMeetingAgenda|start");
        // 验证
        ServiceResult<Object> result = validateMeetingAgenda(meetingAgendaInfoVo);
        if (!result.isSuccess()) {
            return result;
        }
        // 新增/更新MeetingAgendaInfo
        String agendaId = saveOrUpdateMeetingAgendaInfo(meetingAgendaInfoVo, currentUser);
        log.info("agendaId=" + agendaId);
        // 新增/更新nqs
        saveNqsRelation(agendaId, meetingAgendaInfoVo.getNqsList());
        // 新增/更新agendaItems
        saveOrUpdateAgendaItemInfo(meetingAgendaInfoVo, currentUser, agendaId);
        log.info("addAndUpdateMeetingAgenda|end");
        return successResult(getTipMsg("operation_success"), (Object) meetingAgendaInfoVo);
    }

    /**
     * 
     * @description meetingAgenda的验证
     * @author mingwang
     * @create 2016年11月1日下午5:13:52
     * @version 1.0
     * @param meetingAgendaInfoVo
     * @return
     */
    private ServiceResult<Object> validateMeetingAgenda(MeetingAgendaInfoVo meetingAgendaInfoVo) {
        // name不能为空
        if (StringUtil.isEmpty(meetingAgendaInfoVo.getAgendaName())) {
            return failResult(getTipMsg("agendaName_empty"));
        }
        // type不能为空
        if (StringUtil.isEmpty(meetingAgendaInfoVo.getAgendaTypeId())) {
            return failResult(getTipMsg("agendaType_empty"));
        }
        // Item的一些验证
        for (AgendaItemInfo agendaItemInfo : meetingAgendaInfoVo.getAgendaItems()) {
            // ItemName不能为空
            if (StringUtil.isEmpty(agendaItemInfo.getItemName())) {
                return failResult(getTipMsg("agendaItem_empty"));
            }
            // itemName 长度限制
            if (agendaItemInfo.getItemName().length() > 255) {
                return failResult(getTipMsg("agendaItem_overflow"));
            }
            String minutes = agendaItemInfo.getItemMinutes();
            Integer time = agendaItemInfo.getItemTime();
            // time和minutes必须为数字格式
            /*
             * if ((time!=null && !RegexUtil.isInt(time)) ||
             * (StringUtil.isNotEmpty(minutes) && !RegexUtil.isInt(minutes))) {
             * return failResult(getTipMsg("agenda_time_minutes_format")); }
             */
            // time的取值范围
            if (time != null && (agendaItemInfo.getItemTime() < 0 || agendaItemInfo.getItemTime() > 999999)) {
                return failResult(getTipMsg("agenda_time_overflow"));
            }
            // minutes的取值范围（0-59）
            if (StringUtil.isNotEmpty(minutes) && (Integer.parseInt(minutes) < 0 || Integer.parseInt(minutes) > 59)) {
                return failResult(getTipMsg("agenda_mimutes_error"));
            }
        }
        return successResult();
    }

    /**
     * 
     * @description 新增/更新AgendaItemInfo
     * @author mingwang
     * @create 2016年10月14日上午8:59:42
     * @version 1.0
     * @param meetingAgendaInfoVo
     * @param currentUser
     */
    private void saveOrUpdateAgendaItemInfo(MeetingAgendaInfoVo meetingAgendaInfoVo, UserInfoVo currentUser, String agendaId) {
        log.info("addAndUpdateMeetingAgenda|saveOrUpdateAgendaItemInfo|start|agendaId=" + agendaId);
        // 如果meeting中有记录，不删除agendaItem的历史记录，并直接添加新纪录
        int count = meetingInfoMapper.selectByAgendaId(meetingAgendaInfoVo.getId());
        if (count <= 0) {
            // 历史记录删除
            agendaItemInfoMapper.updateDeleteByAgendaId(meetingAgendaInfoVo.getId());
        }
        // 如果需要更新的agendaItem集合为空，删除后历史记录后直接返回
        if (ListUtil.isEmpty(meetingAgendaInfoVo.getAgendaItems())) {
            return;
        }
        // 重新增加新纪录
        List<AgendaItemInfoVo> agendaItems = meetingAgendaInfoVo.getAgendaItems();
        List<AgendaItemInfo> agendaItems_new = new ArrayList<AgendaItemInfo>();
        Date now = new Date();
        for (AgendaItemInfo agendaItemInfo : agendaItems) {
            agendaItemInfo.setId(UUID.randomUUID().toString());
            agendaItemInfo.setAgendaId(agendaId);
            agendaItemInfo.setCreateTime(now);
            agendaItemInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
            agendaItemInfo.setUpdateTime(now);
            agendaItemInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
            agendaItemInfo.setDeleteFlag(DeleteFlag.Default.getValue());
            now = new Date(now.getTime() + 1000);
            agendaItems_new.add(agendaItemInfo);
        }
        // 批量插入
        int count_insert = agendaItemInfoMapper.batchInsertAgendaItems(agendaItems_new);
        log.info("batchInsertAgendaItems|count=" + count_insert);
        log.info("addAndUpdateMeetingAgenda|saveOrUpdateAgendaItemInfo|end");
    }

    /**
     * 
     * @description 保存meetingAgendaInfo
     * @author mingwang
     * @create 2016年10月13日下午4:18:27
     * @version 1.0
     * @param meetingAgendaInfoVo
     * @param currentUser
     */
    private String saveOrUpdateMeetingAgendaInfo(MeetingAgendaInfoVo meetingAgendaInfoVo, UserInfoVo currentUser) {
        log.info("addAndUpdateMeetingAgenda|saveOrUpdateMeetingAgendaInfo|start");
        // 新增
        if (StringUtil.isEmpty(meetingAgendaInfoVo.getId())) {
            MeetingAgendaInfo meetingAgendaInfo = new MeetingAgendaInfo();
            meetingAgendaInfo.setId(UUID.randomUUID().toString());
            meetingAgendaInfo.setAgendaName(meetingAgendaInfoVo.getAgendaName());
            meetingAgendaInfo.setAgendaTypeId(meetingAgendaInfoVo.getAgendaTypeId());
            meetingAgendaInfo.setParagraph(meetingAgendaInfoVo.getParagraph());
            meetingAgendaInfo.setState(MeetingTempState.Enable.getValuse());
            Date date = new Date();
            meetingAgendaInfo.setCreateTime(date);
            meetingAgendaInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
            meetingAgendaInfo.setUpdateTime(date);
            meetingAgendaInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
            meetingAgendaInfo.setDeleteFlag(DeleteFlag.Default.getValue());
            meetingAgendaInfo.setCurrtenFlag(CurrtenFlag.NewVersion.getValue());
            meetingAgendaInfo.setAgendaCode(UUID.randomUUID().toString());
            meetingAgendaInfoMapper.insertSelective(meetingAgendaInfo);
            return meetingAgendaInfo.getId();
        } else {
            // 更新
            String agendaId = meetingAgendaInfoVo.getId();
            int count = meetingInfoMapper.selectByAgendaId(agendaId);
            log.info("agendaId=" + agendaId + "|count=" + count);
            MeetingAgendaInfo dbMeetingAgendaInfo = meetingAgendaInfoMapper.selectByPrimaryKey(agendaId);
            dbMeetingAgendaInfo.setAgendaName(meetingAgendaInfoVo.getAgendaName());
            dbMeetingAgendaInfo.setAgendaTypeId(meetingAgendaInfoVo.getAgendaTypeId());
            dbMeetingAgendaInfo.setParagraph(meetingAgendaInfoVo.getParagraph());
            dbMeetingAgendaInfo.setUpdateTime(new Date());
            dbMeetingAgendaInfo.setUpdateAccountId(currentUser.getAccountInfo().getId());
            // 编辑前备份
            if (count > 0) {
                meetingAgendaInfoMapper.updateCurrentFlag(CurrtenFlag.OldVersion.getValue(), agendaId);
                agendaId = UUID.randomUUID().toString();
                log.info("agendaId=" + agendaId);
                dbMeetingAgendaInfo.setId(agendaId);
                meetingAgendaInfoMapper.insertSelective(dbMeetingAgendaInfo);
                return agendaId;
            }
            meetingAgendaInfoMapper.updateByPrimaryKey(dbMeetingAgendaInfo);
            return agendaId;
        }
    }

    /**
     * 新增/更新nqs
     */
    @Override
    public void saveNqsRelation(String objId, List<NqsVo> nqsList) {
        // 更新前先删除之前NQS信息
        nqsRelationInfoMapper.removeNqsRelationByObjId(objId);
        // nqsList为空，直接返回
        if (ListUtil.isEmpty(nqsList)) {
            return;
        }
        // 保存新NQS信息
        List<NqsRelationInfo> nrList = new ArrayList<NqsRelationInfo>();
        for (NqsVo n : nqsList) {
            NqsRelationInfo nr = new NqsRelationInfo();
            nr.setId(UUID.randomUUID().toString());
            nr.setObjId(objId);
            nr.setNqsVersion(n.getVersion());
            nr.setDeleteFlag(DeleteFlag.Default.getValue());
            nr.setTag(n.getTag());
            nrList.add(nr);
        }
        // 批量插入
        nqsRelationInfoMapper.insterBatchNqsRelationInfo(nrList);
    }

    /**
     * 获取meetingAgenda列表
     */
    @Override
    public ServiceResult<Object> getMeetingAgendaList(MeetingAgendaCondition condition) {
        log.info("getMeetingAgendaList|start");
        // 设置起始页为当前页
        condition.setStartSize(condition.getPageIndex());
        List<MeetingAgendaInfo> maList = meetingAgendaInfoMapper.getMeetingAgendaList(condition);
        // 获取记录总个数
        int totalSize = meetingAgendaInfoMapper.getMeetingAgendaListCount(condition);
        condition.setTotalSize(totalSize);
        Pager<MeetingAgendaInfo, MeetingAgendaCondition> pager = new Pager<MeetingAgendaInfo, MeetingAgendaCondition>(maList, condition);
        log.info("getMeetingAgendaList|end");
        return successResult((Object) pager);
    }

    /**
     * 验证MeetingAgendaName是否重复
     */
    @Override
    public boolean validateMeetingAgendaName(String agendaName, String id) {
        int count = meetingAgendaInfoMapper.validateMeetingAgendaName(agendaName, id);
        log.info("validateMeetingAgendaName| count=" + count);
        return count > 0 ? false : true;
    }

    /**
     * 修改MeetingAgendaState状态
     */
    @Override
    public ServiceResult<Object> updateAgendaState(short agendaState, String id) {
        log.info("updateAgendaState|start");
        int count = meetingAgendaInfoMapper.updateMeetingAgendaState(agendaState, id);
        log.info("updateAgendaState|end|updateMeetingAgendaState count=" + count);
        return successResult(getTipMsg("operation_success"));
    }

    /**
     * 获取agenda列表(selecterPo)
     */
    @Override
    public List<SelecterPo> getAgendaList() {
        log.info("getAgendaList(selecterPo)|start");
        List<SelecterPo> list = meetingAgendaInfoMapper.getAgendaList();
        log.info("getAgendaList(selecterPo)|end");
        return list;
    }

}
