package com.aoyuntek.aoyun.service.impl;

import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aoyuntek.aoyun.condtion.CenterCondition;
import com.aoyuntek.aoyun.condtion.FollowUpCondtion;
import com.aoyuntek.aoyun.condtion.RosterCondition;
import com.aoyuntek.aoyun.condtion.TaskCondtion;
import com.aoyuntek.aoyun.constants.SystemConstants;
import com.aoyuntek.aoyun.dao.AccountInfoMapper;
import com.aoyuntek.aoyun.dao.AccountRoleInfoMapper;
import com.aoyuntek.aoyun.dao.AttendanceHistoryInfoMapper;
import com.aoyuntek.aoyun.dao.CentersInfoMapper;
import com.aoyuntek.aoyun.dao.ChangeAttendanceRequestInfoMapper;
import com.aoyuntek.aoyun.dao.ChildAttendanceInfoMapper;
import com.aoyuntek.aoyun.dao.GivingNoticeInfoMapper;
import com.aoyuntek.aoyun.dao.MsgEmailInfoMapper;
import com.aoyuntek.aoyun.dao.RequestLogInfoMapper;
import com.aoyuntek.aoyun.dao.RoomInfoMapper;
import com.aoyuntek.aoyun.dao.SigninInfoMapper;
import com.aoyuntek.aoyun.dao.SubtractedAttendanceMapper;
import com.aoyuntek.aoyun.dao.TemporaryAttendanceInfoMapper;
import com.aoyuntek.aoyun.dao.UserInfoMapper;
import com.aoyuntek.aoyun.dao.roster.RosterInfoMapper;
import com.aoyuntek.aoyun.dao.roster.RosterStaffInfoMapper;
import com.aoyuntek.aoyun.dao.roster.TempRosterCenterInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskFollowInstanceInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskFollowUpInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskFormRelationInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskFrequencyInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskInstanceInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskResponsibleLogInfoMapper;
import com.aoyuntek.aoyun.dao.task.TaskVisibleInfoMapper;
import com.aoyuntek.aoyun.entity.po.AccountInfo;
import com.aoyuntek.aoyun.entity.po.AccountRoleInfo;
import com.aoyuntek.aoyun.entity.po.AttendanceHistoryInfo;
import com.aoyuntek.aoyun.entity.po.CentersInfo;
import com.aoyuntek.aoyun.entity.po.ChangeAttendanceRequestInfo;
import com.aoyuntek.aoyun.entity.po.ChildAttendanceInfo;
import com.aoyuntek.aoyun.entity.po.MsgEmailInfo;
import com.aoyuntek.aoyun.entity.po.SigninInfo;
import com.aoyuntek.aoyun.entity.po.SubtractedAttendance;
import com.aoyuntek.aoyun.entity.po.TemporaryAttendanceInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.po.roster.RosterInfo;
import com.aoyuntek.aoyun.entity.po.roster.TempRosterCenterInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskFollowUpInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskFrequencyInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskVisibleInfo;
import com.aoyuntek.aoyun.entity.vo.RoleInfoVo;
import com.aoyuntek.aoyun.entity.vo.RoomInfoVo;
import com.aoyuntek.aoyun.entity.vo.SigninVo;
import com.aoyuntek.aoyun.entity.vo.UserInfoVo;
import com.aoyuntek.aoyun.entity.vo.base.UserVo;
import com.aoyuntek.aoyun.entity.vo.roster.RosterStaffVo;
import com.aoyuntek.aoyun.entity.vo.task.TaskInfoVo;
import com.aoyuntek.aoyun.entity.vo.task.TaskListVo;
import com.aoyuntek.aoyun.enums.ArchivedStatus;
import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.EmailType;
import com.aoyuntek.aoyun.enums.EnrolledState;
import com.aoyuntek.aoyun.enums.Role;
import com.aoyuntek.aoyun.enums.SigninState;
import com.aoyuntek.aoyun.enums.SigninType;
import com.aoyuntek.aoyun.enums.TemporaryType;
import com.aoyuntek.aoyun.enums.roster.RosterStatus;
import com.aoyuntek.aoyun.factory.AttendanceFactory;
import com.aoyuntek.aoyun.factory.RosterFactory;
import com.aoyuntek.aoyun.factory.TaskFactory;
import com.aoyuntek.aoyun.factory.UserFactory;
import com.aoyuntek.aoyun.service.IFamilyService;
import com.aoyuntek.aoyun.service.IJobService;
import com.aoyuntek.aoyun.service.attendance.IAttendanceCoreService;
import com.aoyuntek.aoyun.service.attendance.IAttendanceHistoryService;
import com.aoyuntek.aoyun.service.attendance.IAttendanceService;
import com.aoyuntek.aoyun.service.task.ITaskCoreService;
import com.aoyuntek.aoyun.service.task.ITaskService;
import com.aoyuntek.aoyun.uitl.EmailUtil;
import com.theone.date.util.DateUtil;
import com.theone.list.util.ListUtil;
import com.theone.string.util.StringUtil;

@Service
public class JobServiceImpl extends BaseWebServiceImpl<SigninInfo, SigninInfoMapper> implements IJobService {

	public static Logger logger = Logger.getLogger("JobServiceImpl");

	@Autowired
	private UserInfoMapper userInfoMapper;
	@Autowired
	private ChangeAttendanceRequestInfoMapper changeAttendanceRequestInfoMapper;
	@Autowired
	private ChildAttendanceInfoMapper childAttendanceInfoMapper;
	@Autowired
	private AttendanceHistoryInfoMapper attendanceHistoryInfoMapper;
	@Autowired
	private CentersInfoMapper centersInfoMapper;
	@Autowired
	private AccountInfoMapper accountInfoMapper;
	@Autowired
	private RequestLogInfoMapper requestLogInfoMapper;
	@Autowired
	private RoomInfoMapper roomInfoMapper;
	@Autowired
	private SigninInfoMapper signinInfoMapper;
	@Autowired
	private TemporaryAttendanceInfoMapper temporaryAttendanceInfoMapper;
	@Autowired
	private IFamilyService familyService;
	@Autowired
	private SubtractedAttendanceMapper subtractedAttendanceMapper;
	@Autowired
	private AttendanceFactory attendanceFactory;
	@Autowired
	private GivingNoticeInfoMapper givingNoticeInfoMapper;
	@Autowired
	private IAttendanceCoreService attendanceCoreService;
	@Autowired
	private IAttendanceHistoryService attendanceHistoryService;
	@Autowired
	private TaskInfoMapper taskInfoMapper;
	@Autowired
	private ITaskCoreService iTaskCoreService;
	@Autowired
	private TaskFrequencyInfoMapper taskFrequencyInfoMapper;
	@Autowired
	private TaskVisibleInfoMapper taskVisibleInfoMapper;
	@Autowired
	private TaskInstanceInfoMapper taskInstanceInfoMapper;
	@Autowired
	private TaskFactory taskFactory;
	@Autowired
	private TaskResponsibleLogInfoMapper taskResponsibleLogInfoMapper;
	@Autowired
	private TaskFollowUpInfoMapper taskFollowUpInfoMapper;
	@Autowired
	private TaskFormRelationInfoMapper taskFormRelationInfoMapper;
	@Autowired
	private TaskFollowInstanceInfoMapper taskFollowInstanceInfoMapper;
	@Autowired
	private AccountRoleInfoMapper accountRoleInfoMapper;
	@Autowired
	private RosterFactory rosterFactory;
	@Autowired
	private RosterInfoMapper rosterInfoMapper;
	@Autowired
	private RosterStaffInfoMapper rosterStaffInfoMapper;
	@Autowired
	private ITaskService taskService;
	@Autowired
	private IAttendanceService attendanceService;
	@Autowired
	private UserFactory userFactory;
	@Autowired
	private MsgEmailInfoMapper msgEmailInfoMapper;

	@Override
	public void dealRosterJob(Logger logger, Date day, UserInfoVo currentUser) {
		// 获取历史所有加角色人员id
		List<String> tempAccountIds = accountRoleInfoMapper.getTempRoleAccountIds();
		logger.info(">>>>>>>> accountRoleInfoMapper.getTempRoleAccountIds()=" + (ListUtil.isEmpty(tempAccountIds) ? 0 : tempAccountIds.size()));
		// 删除零时角色
		accountRoleInfoMapper.deleteTempRole();
		logger.info(">>>>>>>> accountRoleInfoMapper.deleteTempRole()");

		int dayOfWeek = DateUtil.getDayOfWeek(day);

		// 删除兼职园的信息
		// if (ListUtil.isNotEmpty(tempAccountIds)) {
		// for (String accountId : tempAccountIds) {
		// logger.info(">>>>>>>> tempAccountIds " + accountId);
		// // gfwang update 2016-11-03
		// List<RoleInfoVo> roleList =
		// userInfoMapper.getRoleByAccount(accountId);
		// boolean isCasual = containRoles(roleList, Role.Casual);
		// if (isCasual) {
		// // 给她的信息去掉园的信息
		// UserInfo userInfo = userInfoMapper.getUserInfoByAccountId(accountId);
		// userInfo.setCentersId(null);
		// userInfoMapper.updateByPrimaryKey(userInfo);
		// logger.info(">>>>>>>> setCentersId " + userInfo.getFullName());
		// }
		// }
		// }
		// 扫描今天排班信息
		// 获取本周approve的排课信息
		RosterCondition rosterCondition = new RosterCondition();
		rosterCondition.initAll();
		Date weekStart = DateUtil.getWeekStart(day);
		rosterCondition.setStartDate(weekStart);
		rosterCondition.setState(RosterStatus.Approved.getValue());
		List<RosterInfo> rosterInfos = rosterInfoMapper.getList(rosterCondition);

		if (ListUtil.isNotEmpty(rosterInfos)) {
			for (RosterInfo rosterInfo : rosterInfos) {
				List<RosterStaffVo> rosterStaffInfos = rosterStaffInfoMapper.getByRosterIdAndDay(rosterInfo.getId(), DateUtil.addDay(day, -1));
				if (ListUtil.isEmpty(rosterStaffInfos)) {
					continue;
				}
				for (RosterStaffVo rosterStaffVo : rosterStaffInfos) {
					List<RoleInfoVo> roleList = userInfoMapper.getRoleByAccount(rosterStaffVo.getStaffAccountId());
					boolean isCasual = containRoles(roleList, Role.Casual);
					if (isCasual) {
						// 给她的信息去掉园的信息
						UserInfo userInfo = userInfoMapper.getUserInfoByAccountId(rosterStaffVo.getStaffAccountId());
						userInfo.setCentersId(null);
						userInfoMapper.updateByPrimaryKey(userInfo);
						logger.info(">>>>>>>> setCentersId " + userInfo.getFullName());
					}
				}
			}
			// 如果是礼拜六拜拜天 那么pass
			if (dayOfWeek == 1 || dayOfWeek == 7) {
				logger.info("day is 1 or 7");
				return;
			}
			for (RosterInfo rosterInfo : rosterInfos) {
				try {
					rosterFactory.approvedRoster(rosterInfo, currentUser, day);
				} catch (Exception e) {
					logger.error(e);
				}
			}
		}
	}

	@Override
	public void dealOverDueTaskJobOfCustmer(Logger logger, Date now) {
		TaskCondtion condtion = new TaskCondtion();
		condtion.initAll();
		condtion.setEndDate(now);
		List<TaskListVo> listVos = taskInstanceInfoMapper.getAllListVo(condtion);
		for (TaskListVo taskListVo : listVos) {
			String instanceId = taskListVo.getId();
			// 这里等于2 标示 这个task是followup类型
			if (taskListVo.getDataType().intValue() == 2) {
				instanceId = taskListVo.getValueId();
			}
			taskResponsibleLogInfoMapper.deleteByInstanceId(instanceId);
			// 获取责任人
			List<UserVo> users = taskFactory.getResponsiblePeople(taskListVo, now);
			for (UserVo accountId : users) {
				iTaskCoreService.dealTaskResponsibleLog(instanceId, accountId.getAccountId());
			}
		}
		taskFollowInstanceInfoMapper.updateStatu(now);
		taskInstanceInfoMapper.updateStatu(now);
	}

	@Override
	public void dealTaskJobOfCustmer(Logger logger, Date now) throws Exception {
		// 查询所有任务模板
		TaskCondtion condition = new TaskCondtion();
		condition.initAll();
		condition.setSortName("create_time");
		condition.setSortOrder("DESC");
		condition.setCurrentFlag(true);
		List<TaskInfo> taskInfos = taskInfoMapper.getList(condition);
		for (TaskInfo taskInfo : taskInfos) {
			if (taskInfo.getShiftFlag()) {
				logger.info(taskInfo.getTaskName() + " is shift...");
				continue;
			}
			TaskInfoVo taskInfoVo = new TaskInfoVo();
			PropertyUtils.copyProperties(taskInfoVo, taskInfo);
			taskInfoVo.setFormId(taskFormRelationInfoMapper.getByTaskId(taskInfo.getId()));
			TaskFrequencyInfo taskFrequencyInfo = taskFrequencyInfoMapper.selectByTaskId(taskInfo.getId());
			taskInfoVo.setTaskFrequencyInfo(taskFrequencyInfo);
			List<TaskVisibleInfo> taskVisibleInfos = taskVisibleInfoMapper.selectBySourceId(taskInfoVo.getSelectObjId());
			taskInfoVo.setSelectTaskVisibles(taskVisibleInfos);
			FollowUpCondtion followUpCondtion = new FollowUpCondtion();
			followUpCondtion.initAll();
			followUpCondtion.setTaskId(taskInfo.getId());
			followUpCondtion.setSortName("create_time");
			followUpCondtion.setSortOrder("desc");
			List<TaskFollowUpInfo> followUpInfos = taskFollowUpInfoMapper.getList(followUpCondtion);
			taskInfoVo.setFollowUpInfos(followUpInfos);
			iTaskCoreService.dealInitTaskInstance(now, taskInfoVo, null);
			logger.info("generate TaskInstance of " + taskInfo.getTaskName());
		}
	}

	@Override
	public void dealSubtractedAttendanceJob(Logger logger, Date now) {
		// 获取当前永久减去的信息
		List<SubtractedAttendance> subtractedAttendances = subtractedAttendanceMapper.getSubtractedAttendanceOfDate(now);
		if (ListUtil.isEmpty(subtractedAttendances)) {
			logger.info("+++++++++++++++++++++++++++++++subtractedAttendances is empty +++++++++++++++++++++++++++++++");
			return;
		}

		// 循环进行判断
		for (SubtractedAttendance subtractedAttendance : subtractedAttendances) {
			if (subtractedAttendance.getEndTime() != null
					&& com.aoyuntek.aoyun.uitl.DateUtil.getDaysIntervalNew(subtractedAttendance.getBeginTime(), subtractedAttendance.getEndTime()) == 0) {
				continue;
			}

			UserInfo userInfo = userInfoMapper.getUserInfoByAccountId(subtractedAttendance.getChildId());
			// 找到对应小孩的childAttendance
			ChildAttendanceInfo childAttendanceInfo = childAttendanceInfoMapper.selectByUserId(userInfo.getId());
			// 处理是否要去掉勾选
			boolean isChange = attendanceFactory.dealSubtractedAttendance(childAttendanceInfo, now);
			if (isChange) {
				// 伪造为了传递参数
				ChangeAttendanceRequestInfo attendanceRequestInfo = new ChangeAttendanceRequestInfo();
				attendanceRequestInfo.setChildId(subtractedAttendance.getChildId());
				attendanceRequestInfo.setChangeDate(now);
				attendanceRequestInfo.setNewCentreId(userInfo.getCentersId());
				attendanceRequestInfo.setNewRoomId(userInfo.getRoomId());
				attendanceFactory.dealAttendanceRequest(attendanceRequestInfo, childAttendanceInfo);
				// 这里记录历史
				attendanceHistoryService.dealAttendanceHistory(attendanceRequestInfo, null);
			}
			// 更新
			childAttendanceInfoMapper.updateByPrimaryKey(childAttendanceInfo);
		}
	}

	@Override
	public void dealAttendanceRequestJob(Logger logger, Date now, UserInfoVo currentUser) {
		List<ChangeAttendanceRequestInfo> requestInfos = changeAttendanceRequestInfoMapper.getAllJobRequest(now);

		if (ListUtil.isEmpty(requestInfos)) {
			logger.info("+++++++++++++++++++++++++++++++requestInfos is empty +++++++++++++++++++++++++++++++");
			return;
		}

		for (ChangeAttendanceRequestInfo attendanceRequestInfo : requestInfos) {
			UserInfo childInfo = userInfoMapper.getUserInfoByAccountId(attendanceRequestInfo.getChildId());
			logger.info(childInfo.getFirstName());
			ChildAttendanceInfo attendanceInfo = childAttendanceInfoMapper.selectByUserId(childInfo.getId());
			// 处理相关操作
			attendanceCoreService.dealAttendanceNow(currentUser, childInfo, attendanceInfo, attendanceRequestInfo);
		}
	}

	@Override
	public void dealEnrolledJob(Logger logger, Date now) {
		List<ChildAttendanceInfo> childAttendanceInfos = childAttendanceInfoMapper.getEnrolledChild(now);
		if (ListUtil.isEmpty(childAttendanceInfos)) {
			logger.info("+++++++++++++++++++++++++++++++childAttendanceInfos is empty +++++++++++++++++++++++++++++++");
		}
		// 入园
		for (ChildAttendanceInfo childAttendanceInfo : childAttendanceInfos) {
			childAttendanceInfo.setEnrolled(EnrolledState.Enrolled.getValue());
			childAttendanceInfoMapper.updateByPrimaryKey(childAttendanceInfo);
			UserInfo userInfo = userInfoMapper.selectByPrimaryKey(childAttendanceInfo.getUserId());
			AccountInfo accountInfo = accountInfoMapper.selectAccountInfoByUserId(childAttendanceInfo.getUserId());
			accountInfo.setStatus(ArchivedStatus.UnArchived.getValue());
			accountInfoMapper.updateByPrimaryKey(accountInfo);
			logger.info("+++++++++++++++++++++++++++++++sendEmailForParent is START +++++++++++++++++++++++++++++++");
			// 发送邮件给入院家长
			// familyService.sendEmailForParent(userInfo.getFamilyId(), false,
			// false);
			logger.info("+++++++++++++++++++++++++++++++sendEmailForParent is END +++++++++++++++++++++++++++++++");
			logger.info("+++++++++++++++++++++++++++++++into_center is START +++++++++++++++++++++++++++++++");
			// 发送入园邮件
			// userFactory.sendParentEmailByChild(getSystemEmailMsg("into_center_title"),
			// getSystemEmailMsg("into_center_content"), accountInfo.getId(),
			// false,
			// EmailType.intoCenter.getValue(), null, null, null, false);
			logger.info("+++++++++++++++++++++++++++++++into_center is END +++++++++++++++++++++++++++++++");

			logger.info(userInfo.getFirstName());
		}

		// 离园
		// 离园日期的下一天才会执行离园动作
		now = DateUtil.addDay(now, -1);
		List<ChildAttendanceInfo> childLeveAttendanceInfos = childAttendanceInfoMapper.getLeaveChild(now);
		if (ListUtil.isEmpty(childLeveAttendanceInfos)) {
			logger.info("+++++++++++++++++++++++++++++++childLeveAttendanceInfos is empty +++++++++++++++++++++++++++++++");
		}
		for (ChildAttendanceInfo childAttendanceInfo : childLeveAttendanceInfos) {
			AccountInfo accountInfo = accountInfoMapper.selectAccountInfoByUserId(childAttendanceInfo.getUserId());
			UserInfo userInfo = userInfoMapper.getUserInfoByAccountId(accountInfo.getId());
			userFactory.archivedChild(userInfo, null, childAttendanceInfo.getLeaveDate(), EmailType.leaveCenter.getValue(), false);
		}

		// 永久上课安排
		List<TemporaryAttendanceInfo> temporaryAttendanceInfos = temporaryAttendanceInfoMapper.getWillAttendance(TemporaryType.All.getValue(), now);
		if (ListUtil.isEmpty(temporaryAttendanceInfos)) {
			logger.info("+++++++++++++++++++++++++++++++temporaryAttendanceInfos is empty +++++++++++++++++++++++++++++++");
		}
		for (TemporaryAttendanceInfo temporaryAttendanceInfo : temporaryAttendanceInfos) {
			UserInfo userInfo = userInfoMapper.getUserInfoByAccountId(temporaryAttendanceInfo.getChildId());
			logger.info(userInfo.getFirstName());
			ChildAttendanceInfo childAttendanceInfo = childAttendanceInfoMapper.selectByUserId(userInfo.getId());
			attendanceFactory.dealAttendance(childAttendanceInfo, temporaryAttendanceInfo.getDayWeek());
			childAttendanceInfoMapper.updateByPrimaryKey(childAttendanceInfo);

			temporaryAttendanceInfo.setDeleteFlag(DeleteFlag.Delete.getValue());
			temporaryAttendanceInfoMapper.updateByPrimaryKey(temporaryAttendanceInfo);
		}
	}

	@Override
	public void dealStaffSigninJob(Logger logger, Date now, UserInfoVo currentUser) {
		int dayOfWeek = DateUtil.getDayOfWeek(now);
		if (dayOfWeek == 1 || dayOfWeek == 7) {
			logger.info("+++++++++++++++++++++++++++++++StaffSigninJob end +++++++++++++++++++++++++++++++");
			return;
		}
		// 获取本周approve的排课信息
		RosterCondition rosterCondition = new RosterCondition();
		rosterCondition.initAll();
		Date weekStart = DateUtil.getWeekStart(now);
		rosterCondition.setStartDate(weekStart);
		rosterCondition.setState(RosterStatus.Approved.getValue());
		List<RosterInfo> rosterInfos = rosterInfoMapper.getList(rosterCondition);
		if (ListUtil.isEmpty(rosterInfos)) {
			return;
		}
		List<SigninInfo> signinInfos = new ArrayList<SigninInfo>();
		List<String> notSignOut = new ArrayList<String>();
		List<MsgEmailInfo> msgEmailInfos = new ArrayList<MsgEmailInfo>();
		// 获取今天所有的员工排班
		List<String> rosterStaffList = new ArrayList<String>();
		for (RosterInfo rosterInfo : rosterInfos) {
			List<RosterStaffVo> rosterStaffSigns = signinInfoMapper.getStaffSignByCentreAndDay(rosterInfo.getCenterId(), now);
			List<RosterStaffVo> list = rosterStaffInfoMapper.getByRosterIdAndDay(rosterInfo.getId(), now);
			if (ListUtil.isEmpty(list) && ListUtil.isEmpty(rosterStaffSigns)) {
				continue;
			}

			for (RosterStaffVo rosterStaffVo : list) {
				rosterStaffList.add(rosterStaffVo.getStaffAccountId());
				if (rosterStaffVo.getSinState() == null) {
					SigninInfo signinInfo = new SigninInfo();
					signinInfo.setAccountId(rosterStaffVo.getStaffAccountId());
					signinInfo.setCentreId(rosterInfo.getCenterId());
					signinInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
					signinInfo.setCreateTime(new Date());
					signinInfo.setDeleteFlag(DeleteFlag.Default.getValue());
					signinInfo.setId(UUID.randomUUID().toString());
					signinInfo.setSignDate(now);
					signinInfo.setState(SigninState.NoSignin.getValue());
					signinInfo.setType(SigninType.IN.getValue());
					signinInfos.add(signinInfo);
				} else if (rosterStaffVo.getSignInDate() != null && rosterStaffVo.getSinState() != SigninState.NoSignin.getValue()
						&& rosterStaffVo.getSignOutDate() == null) {
					SigninInfo signinInfo = new SigninInfo();
					signinInfo.setAccountId(rosterStaffVo.getStaffAccountId());
					signinInfo.setCentreId(rosterInfo.getCenterId());
					signinInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
					signinInfo.setCreateTime(new Date());
					signinInfo.setDeleteFlag(DeleteFlag.Default.getValue());
					signinInfo.setId(UUID.randomUUID().toString());
					signinInfo.setSignDate(now);
					signinInfo.setState(SigninState.Signin.getValue());
					signinInfo.setType(SigninType.OUT.getValue());
					signinInfos.add(signinInfo);
					notSignOut.add(rosterStaffVo.getStaffAccountId());
					handleNotHaveSignoutEmail(rosterInfo.getCenterId(), rosterStaffVo, msgEmailInfos, now);
				}

			}

			// 格外签到
			for (RosterStaffVo rosterStaffVo : rosterStaffSigns) {
				if (!rosterStaffList.contains(rosterStaffVo.getStaffAccountId())) {
					if (rosterStaffVo.getSinState() != null && rosterStaffVo.getSinState() == SigninState.Signin.getValue() && rosterStaffVo.getSoutState() == null) {
						SigninInfo signinInfo = new SigninInfo();
						signinInfo.setAccountId(rosterStaffVo.getStaffAccountId());
						signinInfo.setCentreId(rosterInfo.getCenterId());
						signinInfo.setCreateAccountId(currentUser.getAccountInfo().getId());
						signinInfo.setCreateTime(new Date());
						signinInfo.setDeleteFlag(DeleteFlag.Default.getValue());
						signinInfo.setId(UUID.randomUUID().toString());
						signinInfo.setSignDate(now);
						signinInfo.setState(SigninState.Signin.getValue());
						signinInfo.setType(SigninType.OUT.getValue());
						signinInfos.add(signinInfo);
						notSignOut.add(rosterStaffVo.getStaffAccountId());
						handleNotHaveSignoutEmail(rosterInfo.getCenterId(), rosterStaffVo, msgEmailInfos, now);
					}
				}
			}
		}
		if (ListUtil.isNotEmpty(signinInfos)) {
			signinInfoMapper.insertBatch(signinInfos);
		}
		if (ListUtil.isNotEmpty(notSignOut)) {
			accountInfoMapper.updateNotSignOutByAccountId(now, notSignOut);
		}
		if (ListUtil.isNotEmpty(msgEmailInfos)) {
			msgEmailInfoMapper.batchInsert(msgEmailInfos);
		}
	}

	/**
	 * @description 处理未签出Email
	 * @author hxzhang
	 * @create 2016年12月27日下午4:56:20
	 */
	private void handleNotHaveSignoutEmail(String centreId, RosterStaffVo rosterStaffVo, List<MsgEmailInfo> msgEmailInfos, Date now) {
		Set<UserInfo> set = new HashSet<UserInfo>();
		// 获取员工信息
		UserInfo user = userInfoMapper.selectByPrimaryKey(rosterStaffVo.getUserId());
		set.add(user);
		// 获取CEO及当前园长
		List<UserInfo> users = userInfoMapper.getCEOAndMangersByCenterId(centreId);
		set.addAll(users);
		for (UserInfo u : set) {
			String title = getSystemEmailMsg("staff_not_signout_title");
			String content = MessageFormat.format(getSystemEmailMsg("staff_not_signout_content"), userFactory.getUserName(u), userFactory.getUserName(user),
					DateUtil.format(now, "dd/MM/yyyy"));
			String html = EmailUtil.replaceHtml(title, content);
			MsgEmailInfo msgEmailInfo = new MsgEmailInfo(UUID.randomUUID().toString(), getSystemMsg("mailHostAccount"), getSystemMsg("email_name"), u.getEmail(),
					userFactory.getUserName(u), title, null, new Date(), (short) 0, (short) 0, DeleteFlag.Default.getValue(), html);
			msgEmailInfos.add(msgEmailInfo);
		}
	}

	@Override
	public void dealChildSigninJob(Logger logger, Date now) {
		int dayOfWeek = DateUtil.getDayOfWeek(now);
		if (dayOfWeek == 1 || dayOfWeek == 7) {
			logger.info("+++++++++++++++++++++++++++++++SigninJob end +++++++++++++++++++++++++++++++");
			return;
		}
		dayOfWeek = dayOfWeek - 1;
		// 获取所有园区
		CenterCondition condition = new CenterCondition();
		condition.setPageIndex(0);
		condition.setPageSize(Integer.MAX_VALUE);
		condition.setStatus(ArchivedStatus.UnArchived.getValue());
		List<CentersInfo> centersInfos = centersInfoMapper.getCenterList(condition);
		if (ListUtil.isEmpty(centersInfos)) {
			logger.info("centersInfos is empty");
			return;
		}
		for (CentersInfo centersInfo : centersInfos) {
			List<RoomInfoVo> roomInfoVos = roomInfoMapper.selectRoomsByCenterId(centersInfo.getId());
			if (ListUtil.isEmpty(roomInfoVos)) {
				logger.info(MessageFormat.format("centersInfos {0}  roomInfoVos is empty", centersInfo.getName()));
				continue;
			}
			for (RoomInfoVo roomInfoVo : roomInfoVos) {
				List<SigninVo> signinInfos = attendanceCoreService.getYesterdaySignChilds(centersInfo.getId(), roomInfoVo.getId(), (short) dayOfWeek, now);
				List<SigninVo> interims = signinInfoMapper.getInterimList(centersInfo.getId(), roomInfoVo.getId(), now);
				signinInfos.addAll(interims);
				for (SigninVo signinVo : signinInfos) {
					if (signinVo.getState() == null) {
						SigninInfo temp = new SigninInfo();
						temp.setAccountId(signinVo.getAccountId());
						temp.setCentreId(centersInfo.getId());
						temp.setCreateAccountId(SystemConstants.JobCreateAccountId);
						temp.setCreateTime(new Date());
						temp.setDeleteFlag(DeleteFlag.Default.getValue());
						temp.setId(UUID.randomUUID().toString());
						temp.setRoomId(roomInfoVo.getId());
						temp.setSignDate(now);
						temp.setState(SigninState.NoSignin.getValue());
						temp.setType(SigninType.IN.getValue());
						temp.setUpdateAccountId(SystemConstants.JobCreateAccountId);
						temp.setUpdateTime(new Date());
						signinInfoMapper.insert(temp);
					}
				}
			}
		}
	}

	@Override
	public void dealEnrolledJobOneWeekAgo(Date now) {
		log.info("=================================dealEnrolledJobOneWeekAgo>>START====================================");
		List<ChildAttendanceInfo> childLeveAttendanceInfos = childAttendanceInfoMapper.getOneWeekAgoLeaveChild(oneWeekAgo(now));
		for (ChildAttendanceInfo c : childLeveAttendanceInfos) {
			AccountInfo accountInfo = accountInfoMapper.selectAccountInfoByUserId(c.getUserId());
			UserInfo userInfo = userInfoMapper.getUserInfoByAccountId(accountInfo.getId());
			taskService.createDepositTask(userInfo, now);
		}
		log.info("=================================dealEnrolledJobOneWeekAgo>>END====================================");
	}

	/**
	 * @description 获取一星期之前
	 * @author hxzhang
	 * @create 2016年10月26日下午12:09:21
	 */
	private Date oneWeekAgo(Date now) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			// 归档后延一天,即换押金task后延一天.
			now = DateUtil.subDays(sdf.parse(sdf.format(now)), 8);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return now;
	}

	public void dealChangeCenterJobOneWeekAgo(Date now) {
		List<AttendanceHistoryInfo> ahiList = attendanceService.getChangeCentreHistory(7, now);
		for (AttendanceHistoryInfo ahi : ahiList) {
			UserInfo userInfo = new UserInfo();
			userInfo.setCentersId(ahi.getCenterId());
			userInfo.setRoomId(ahi.getRoomId());
			userInfo.setGroupId(null);
			userInfo.setAccountId(ahi.getChildId());
			taskService.createDepositTask(userInfo, now);
		}
	}

	@Autowired
	private TempRosterCenterInfoMapper tempRosterCenterInfoMapper;

	@Override
	public void updateCaualsRole() {
		Date date = new Date();
		date = DateUtil.addDay(date, 3);
		removeCentreRole(date);
		addCentreRole(date);
	}

	private void addCentreRole(Date date) {
		List<TempRosterCenterInfo> list = tempRosterCenterInfoMapper.getBetweenChangeList(date);
		logger.info("addCentreRole count=" + list.size());
		for (TempRosterCenterInfo item : list) {
			String accuntId = item.getRosterStaffId();
			logger.info("addCentreRole accountId=" + accuntId);
			if (StringUtil.isEmpty(accuntId)) {
				logger.info("addCentreRole account id is empty");
				continue;
			}
			String centreId = item.getCentreId();
			String roleId = item.getRoleId();
			UserInfo userInfo = userInfoMapper.getUserInfoByAccountId(accuntId);
			userInfo.setCentersId(centreId);

			AccountRoleInfo accountRoleInfo = new AccountRoleInfo();
			accountRoleInfo.setId(UUID.randomUUID().toString());
			accountRoleInfo.setAccountId(accuntId);
			accountRoleInfo.setCreateTime(date);
			accountRoleInfo.setDeleteFlag(DeleteFlag.Default.getValue());
			accountRoleInfo.setRoleId(roleId);
			accountRoleInfo.setTempFlag(true);

			userInfoMapper.updateByPrimaryKey(userInfo);
			accountRoleInfoMapper.insert(accountRoleInfo);
		}
		int count = tempRosterCenterInfoMapper.deleteBetweenItem(date);
		logger.info("addCentreRole delete count=" + count);
	}

	private void removeCentreRole(Date date) {
		List<TempRosterCenterInfo> list = tempRosterCenterInfoMapper.getAfterChangelist(date);
		logger.info("removeCentreRole count=" + list.size());
		for (TempRosterCenterInfo item : list) {
			String accuntId = item.getRosterStaffId();
			logger.info("removeCentreRole accuntId=" + accuntId);
			if (StringUtil.isEmpty(accuntId)) {
				logger.info("removeCentreRole account id is empty");
				continue;
			}
			UserInfo userInfo = userInfoMapper.getUserInfoByAccountId(accuntId);
			userInfo.setCentersId(null);
			userInfoMapper.updateByPrimaryKey(userInfo);
			accountRoleInfoMapper.removeTempRoleByAccountId(accuntId);
		}
		int count = tempRosterCenterInfoMapper.deleteAfterChange(date);
		logger.info("removeCentreRole delete count=" + count);
	}
}
