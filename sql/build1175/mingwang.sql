drop table if exists tbl_room_menu;
create table tbl_room_menu
(
   menu_id       				varchar(50),
   week_num						smallint(6),
   room_id    					varchar(50)
);

INSERT INTO tbl_menu (
	id,
	NAME,
	url,
	active_flag,
	delete_flag
)
VALUES
	(
		'fe6d4195-4a55-11e7-81c3-00e0703507b4',
		'menu_list',
		'/menu_list',
		1,
		0
	);
	
INSERT INTO tbl_role_menu (id, role_id, menu_id)
VALUES
	(
		'0e74116d-4a58-11e7-81c3-00e0703507b4',
		'2e083440-49a3-11e6-b19e-00e0703507b4',
		'fe6d4195-4a55-11e7-81c3-00e0703507b4'
	),
	(
		'118fda3d-4a58-11e7-81c3-00e0703507b4',
		'4586aa2b-49a3-11e6-b19e-00e0703507b4',
		'fe6d4195-4a55-11e7-81c3-00e0703507b4'
	);