INSERT INTO tbl_function (
	id,
	NAME,
	target,
	menu_id,
	function_type
)
VALUES
	(
		'3948a31b-4798-11e8-8228-00e0703507b4',
		'task_taskTableView.do',
		'/task/taskTableView.do',
		'628d7718-8e8c-11e6-85fd-00e0703507b4',
		2
	);

INSERT INTO tbl_role_function (id, role_id, function_id)
VALUES
	(
		'4a031c76-4798-11e8-8228-00e0703507b4',
		'2e083440-49a3-11e6-b19e-00e0703507b4',
		'3948a31b-4798-11e8-8228-00e0703507b4'
	),(
		'50c5df65-4798-11e8-8228-00e0703507b4',
		'4586aa2b-49a3-11e6-b19e-00e0703507b4',
		'3948a31b-4798-11e8-8228-00e0703507b4'
	),
(
		'5a1f260f-4798-11e8-8228-00e0703507b4',
		'51c32196-49a3-11e6-b19e-00e0703507b4',
		'3948a31b-4798-11e8-8228-00e0703507b4'
	),
(
		'604901eb-4798-11e8-8228-00e0703507b4',
		'74ba21a6-49a3-11e6-b19e-00e0703507b4',
		'3948a31b-4798-11e8-8228-00e0703507b4'
	);
INSERT INTO tbl_function (
	id,
	NAME,
	target,
	menu_id,
	function_type
)
VALUES
	(
		'676150c6-491c-11e8-8228-00e0703507b4',
		'',
		'/common/getStaff2.do',
		'',
		1
	);
	
INSERT INTO tbl_function (
	id,
	NAME,
	target,
	menu_id,
	function_type
)
VALUES
	(
		'010c4bee-4920-11e8-8228-00e0703507b4',
		'',
		'/common/getStaffChild.do',
		'',
		1
	);
	
	INSERT INTO tbl_function (
	id,
	NAME,
	target,
	menu_id,
	function_type
)
VALUES
	(
		'832b7533-49e1-11e8-8348-00e0703507b4',
		'',
		'/task/export.do',
		'',
		1
	);
	
		INSERT INTO tbl_function (
	id,
	NAME,
	target,
	menu_id,
	function_type
)
VALUES
	(
		'e88cd595-49fa-11e8-8348-00e0703507b4',
		'',
		'/task/csv.do',
		'',
		1
	);


DELIMITER $$



DROP VIEW IF EXISTS `vv_1`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `vv_1` AS (
SELECT
  `i`.`id`                AS `id`,
  `i`.`centre_id`         AS `centre_id`,
  `i`.`room_id`           AS `room_id`,
  `i`.`group_id`          AS `group_id`,
  `i`.`obj_id`            AS `obj_id`,
  `i`.`value_id`          AS `value_id`,
  `i`.`obj_type`          AS `obj_type`,
  `i`.`statu`             AS `statu`,
  `i`.`task_type`         AS `task_type`,
  `i`.`begin_date`        AS `begin_date`,
  `i`.`end_date`          AS `end_date`,
  `t`.`task_name`         AS `task_name`,
  `i`.`task_model_id`     AS `task_model_id`,
  `i`.`create_time`       AS `create_time`,
  `i`.`update_account_id` AS `update_account_id`,
  0                       AS `t`,
  CONCAT(IF((IFNULL(`u`.`first_name`,'') = ''),'',CONCAT(`u`.`first_name`,' ')),IF((IFNULL(`u`.`middle_name`,'') = ''),'',CONCAT(`u`.`middle_name`,' ')),IF((IFNULL(`u`.`last_name`,'') = ''),'',CONCAT(`u`.`last_name`,' '))) AS `NAME`,
  `centre`.`name`         AS `cName`,
  `room`.`name`           AS `rName`
FROM (((((`tbl_task_instance` `i`
       JOIN `tbl_task` `t`
         ON (((`i`.`task_type` = 17)
              AND (`i`.`delete_flag` = 0)
              AND (`t`.`delete_flag` = 0)
              AND (`t`.`id` = `i`.`task_model_id`))))
      LEFT JOIN `tbl_account` `account`
        ON ((`i`.`obj_id` = `account`.`id`)))
     LEFT JOIN `tbl_user` `u`
       ON ((`account`.`user_id` = `u`.`id`)))
    LEFT JOIN `tbl_centers` `centre`
      ON ((`centre`.`id` = `i`.`centre_id`)))
   LEFT JOIN `tbl_room` `room`
     ON ((`room`.`id` = `i`.`room_id`))))$$

DELIMITER ;
DELIMITER $$



DROP VIEW IF EXISTS `vv_1`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `vv_1` AS (
SELECT
  `i`.`id`                AS `id`,
  `i`.`centre_id`         AS `centre_id`,
  `i`.`room_id`           AS `room_id`,
  `i`.`group_id`          AS `group_id`,
  `i`.`obj_id`            AS `obj_id`,
  `i`.`value_id`          AS `value_id`,
  `i`.`obj_type`          AS `obj_type`,
  `i`.`statu`             AS `statu`,
  `i`.`task_type`         AS `task_type`,
  `i`.`begin_date`        AS `begin_date`,
  `i`.`end_date`          AS `end_date`,
  `t`.`task_name`         AS `task_name`,
  `i`.`task_model_id`     AS `task_model_id`,
  `i`.`create_time`       AS `create_time`,
  `i`.`update_account_id` AS `update_account_id`,
  0                       AS `t`,
  CONCAT(IF((IFNULL(`u`.`first_name`,'') = ''),'',CONCAT(`u`.`first_name`,' ')),IF((IFNULL(`u`.`middle_name`,'') = ''),'',CONCAT(`u`.`middle_name`,' ')),IF((IFNULL(`u`.`last_name`,'') = ''),'',CONCAT(`u`.`last_name`,' '))) AS `NAME`,
  `centre`.`name`         AS `cName`,
  `room`.`name`           AS `rName`
FROM (((((`tbl_task_instance` `i`
       JOIN `tbl_task` `t`
         ON (((`i`.`task_type` = 17)
              AND (`i`.`delete_flag` = 0)
              AND (`t`.`delete_flag` = 0)
              AND (`t`.`id` = `i`.`task_model_id`))))
      LEFT JOIN `tbl_account` `account`
        ON ((`i`.`obj_id` = `account`.`id`)))
     LEFT JOIN `tbl_user` `u`
       ON ((`account`.`user_id` = `u`.`id`)))
    LEFT JOIN `tbl_centers` `centre`
      ON ((`centre`.`id` = `i`.`centre_id`)))
   LEFT JOIN `tbl_room` `room`
     ON ((`room`.`id` = `i`.`room_id`))))$$

DELIMITER ;
DELIMITER $$



DROP VIEW IF EXISTS `vv_3`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `vv_3` AS (
SELECT
  `i`.`id`                AS `id`,
  `i`.`centre_id`         AS `centre_id`,
  `i`.`room_id`           AS `room_id`,
  `i`.`group_id`          AS `group_id`,
  `i`.`obj_id`            AS `obj_id`,
  `i`.`value_id`          AS `value_id`,
  `i`.`obj_type`          AS `obj_type`,
  `lesson`.`state`        AS `statu`,
  `i`.`task_type`         AS `task_type`,
  `i`.`begin_date`        AS `begin_date`,
  `i`.`end_date`          AS `end_date`,
  CONCAT(`cc`.`name`,' ',`room`.`name`) AS `task_name`,
  '0'                     AS `task_model_id`,
  `i`.`create_time`       AS `create_time`,
  `i`.`update_account_id` AS `update_account_id`,
  1                       AS `t`,
  NULL                    AS `NAME`,
  `cc`.`name`             AS `cName`,
  `room`.`name`           AS `rName`
FROM (((`tbl_task_instance` `i`
     JOIN `tbl_program_lesson` `lesson`
       ON (((`i`.`task_type` = 2)
            AND (`i`.`delete_flag` = 0)
            AND (`lesson`.`delete_flag` = 0)
            AND (`lesson`.`id` = `i`.`value_id`))))
    LEFT JOIN `tbl_centers` `cc`
      ON ((`cc`.`id` = `i`.`centre_id`)))
   LEFT JOIN `tbl_room` `room`
     ON ((`room`.`id` = `i`.`room_id`))))$$

DELIMITER ;
DELIMITER $$



DROP VIEW IF EXISTS `vv_4`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `vv_4` AS (
SELECT `i`.`id` AS `id`,`i`.`centre_id` AS `centre_id`,`i`.`room_id` AS `room_id`,`i`.`group_id` AS `group_id`,`account`.`id` AS `obj_id`,`i`.`value_id` AS `value_id`,`i`.`obj_type` AS `obj_type`,`intobslea`.`state` AS `statu`,`i`.`task_type` AS `task_type`,`i`.`begin_date` AS `begin_date`,`i`.`end_date` AS `end_date`,CONCAT(`GetFullName`(`u`.`first_name`,`u`.`middle_name`,`u`.`last_name`),CONVERT((CASE WHEN (`i`.`task_type` = 4) THEN '\'s Interest' WHEN (`i`.`task_type` = 6) THEN '\'s Observation' WHEN (`i`.`task_type` = 8) THEN '\'s Learning Story' END) USING utf8)) AS `task_name`,'0' AS `task_model_id`,`i`.`create_time` AS `create_time`,`i`.`update_account_id` AS `update_account_id`,1 AS `t`,CONCAT(IF((IFNULL(`u`.`first_name`,'') = ''),'',CONCAT(`u`.`first_name`,' ')),IF((IFNULL(`u`.`middle_name`,'') = ''),'',CONCAT(`u`.`middle_name`,' ')),IF((IFNULL(`u`.`last_name`,'') = ''),'',CONCAT(`u`.`last_name`,' '))) AS `NAME`,`cc`.`name` AS `cName`,`room`.`name` AS `rName` FROM (((((`tbl_task_instance` `i` JOIN `tbl_program_intobslea` `intobslea` ON(((`i`.`task_type` IN (4,6,8)) AND (`i`.`delete_flag` = 0) AND (`intobslea`.`delete_flag` = 0) AND (`intobslea`.`id` = `i`.`value_id`)))) LEFT JOIN `tbl_centers` `cc` ON((`cc`.`id` = `i`.`centre_id`))) LEFT JOIN `tbl_room` `room` ON((`room`.`id` = `i`.`room_id`))) LEFT JOIN `tbl_account` `account` ON((`account`.`id` = `intobslea`.`child_id`))) LEFT JOIN `tbl_user` `u` ON((`u`.`id` = `account`.`user_id`))))$$

DELIMITER ;
DELIMITER $$



DROP VIEW IF EXISTS `vv_5`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `vv_5` AS (
SELECT `i`.`id` AS `id`,`i`.`centre_id` AS `centre_id`,`i`.`room_id` AS `room_id`,`i`.`group_id` AS `group_id`,`account`.`id` AS `obj_id`,`i`.`value_id` AS `value_id`,`i`.`obj_type` AS `obj_type`,`follow`.`state` AS `statu`,`i`.`task_type` AS `task_type`,`i`.`begin_date` AS `begin_date`,`i`.`end_date` AS `end_date`,CONCAT(CONCAT(`GetFullName`(`u`.`first_name`,`u`.`middle_name`,`u`.`last_name`),CONVERT((CASE WHEN (`i`.`task_type` = 5) THEN '\'s Interest Follow Up' WHEN (`i`.`task_type` = 7) THEN '\'s Observation Follow Up' WHEN (`i`.`task_type` = 9) THEN '\'s Learning Story Follow Up' END) USING utf8))) AS `task_name`,'0' AS `task_model_id`,`i`.`create_time` AS `create_time`,`i`.`update_account_id` AS `update_account_id`,1 AS `t`,CONCAT(IF((IFNULL(`u`.`first_name`,'') = ''),'',CONCAT(`u`.`first_name`,' ')),IF((IFNULL(`u`.`middle_name`,'') = ''),'',CONCAT(`u`.`middle_name`,' ')),IF((IFNULL(`u`.`last_name`,'') = ''),'',CONCAT(`u`.`last_name`,' '))) AS `NAME`,`cc`.`name` AS `cName`,`room`.`name` AS `rName` FROM ((((((`tbl_task_instance` `i` JOIN `tbl_task_program_follow_up` `follow` ON(((`i`.`task_type` IN (5,7,9)) AND (`i`.`delete_flag` = 0) AND (`follow`.`delete_flag` = 0) AND (`follow`.`id` = `i`.`value_id`)))) LEFT JOIN `tbl_centers` `cc` ON((`cc`.`id` = `i`.`centre_id`))) LEFT JOIN `tbl_room` `room` ON((`room`.`id` = `i`.`room_id`))) LEFT JOIN `tbl_program_intobslea` `intobslea` ON((`follow`.`task_id` = `intobslea`.`id`))) LEFT JOIN `tbl_account` `account` ON((`account`.`id` = `intobslea`.`child_id`))) LEFT JOIN `tbl_user` `u` ON((`u`.`id` = `account`.`user_id`))))$$

DELIMITER ;
DELIMITER $$



DROP VIEW IF EXISTS `vv_6`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `vv_6` AS (
SELECT `i`.`id` AS `id`,`i`.`centre_id` AS `centre_id`,`i`.`room_id` AS `room_id`,`i`.`group_id` AS `group_id`,`i`.`obj_id` AS `obj_id`,`i`.`value_id` AS `value_id`,`i`.`obj_type` AS `obj_type`,`register`.`state` AS `statu`,`i`.`task_type` AS `task_type`,`i`.`begin_date` AS `begin_date`,`i`.`end_date` AS `end_date`,(CASE WHEN ((`register`.`type` = 13) OR (`register`.`type` = 14)) THEN CONCAT(`cc`.`name`,' ',`room`.`name`,' ',CONVERT(DATE_FORMAT(`register`.`day`,'%h:%i %p') USING utf8)) ELSE CONCAT(`cc`.`name`,' ',`room`.`name`) END) AS `task_name`,'0' AS `task_model_id`,`i`.`create_time` AS `create_time`,`i`.`update_account_id` AS `update_account_id`,1 AS `t`,NULL AS `NAME`,`cc`.`name` AS `cName`,`room`.`name` AS `rName` FROM (((`tbl_task_instance` `i` JOIN `tbl_program_register_task` `register` ON(((`i`.`task_type` IN (11,12,13,14,15)) AND (`i`.`delete_flag` = 0) AND (`register`.`delete_flag` = 0) AND (`register`.`id` = `i`.`value_id`)))) LEFT JOIN `tbl_centers` `cc` ON((`cc`.`id` = `i`.`centre_id`))) LEFT JOIN `tbl_room` `room` ON((`room`.`id` = `i`.`room_id`))))$$

DELIMITER ;
DELIMITER $$



DROP VIEW IF EXISTS `vv_7`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `vv_7` AS (
SELECT
  `i`.`id`                AS `id`,
  `i`.`centre_id`         AS `centre_id`,
  `i`.`room_id`           AS `room_id`,
  `i`.`group_id`          AS `group_id`,
  `i`.`obj_id`            AS `obj_id`,
  `i`.`value_id`          AS `value_id`,
  `i`.`obj_type`          AS `obj_type`,
  `weekly`.`state`        AS `statu`,
  `i`.`task_type`         AS `task_type`,
  `i`.`begin_date`        AS `begin_date`,
  `i`.`end_date`          AS `end_date`,
  CONCAT(`cc`.`name`,' ',`room`.`name`,' ',CONVERT(IF((`weekly`.`type` = 0),'Weekly Evaluation Explore Curriculum','Weekly Evaluation Grow Curriculum') USING utf8)) AS `task_name`,
  '0'                     AS `task_model_id`,
  `i`.`create_time`       AS `create_time`,
  `i`.`update_account_id` AS `update_account_id`,
  1                       AS `t`,
  NULL                    AS `NAME`,
  `cc`.`name`             AS `cName`,
  `room`.`name`           AS `rName`
FROM (((`tbl_task_instance` `i`
     JOIN `tbl_program_weekly_evalution` `weekly`
       ON ((`weekly`.`id` = `i`.`value_id`)))
    LEFT JOIN `tbl_centers` `cc`
      ON ((`cc`.`id` = `i`.`centre_id`)))
   LEFT JOIN `tbl_room` `room`
     ON ((`room`.`id` = `i`.`room_id`)))
WHERE ((`i`.`task_type` = 10)
       AND (`i`.`delete_flag` = 0)
       AND (`weekly`.`delete_flag` = 0)))$$

DELIMITER ;
DELIMITER $$



DROP VIEW IF EXISTS `vv_8`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `vv_8` AS (
SELECT
  `i`.`id`                AS `id`,
  `i`.`centre_id`         AS `centre_id`,
  `i`.`room_id`           AS `room_id`,
  `i`.`group_id`          AS `group_id`,
  `i`.`obj_id`            AS `obj_id`,
  `i`.`value_id`          AS `value_id`,
  `i`.`obj_type`          AS `obj_type`,
  `medication`.`statu`    AS `statu`,
  `i`.`task_type`         AS `task_type`,
  `i`.`begin_date`        AS `begin_date`,
  `i`.`end_date`          AS `end_date`,
  CONCAT(`cc`.`name`,' ',`room`.`name`,' ',`GetFullName`(`u`.`first_name`,`u`.`middle_name`,`u`.`last_name`)) AS `task_name`,
  '0'                     AS `task_model_id`,
  `i`.`create_time`       AS `create_time`,
  `i`.`update_account_id` AS `update_account_id`,
  1                       AS `t`,
  NULL                    AS `NAME`,
  `cc`.`name`             AS `cName`,
  `room`.`name`           AS `rName`
FROM (((((`tbl_task_instance` `i`
       JOIN `tbl_program_medication` `medication`
         ON ((`medication`.`id` = `i`.`value_id`)))
      LEFT JOIN `tbl_centers` `cc`
        ON ((`cc`.`id` = `i`.`centre_id`)))
     LEFT JOIN `tbl_room` `room`
       ON ((`room`.`id` = `i`.`room_id`)))
    LEFT JOIN `tbl_account` `a`
      ON ((`a`.`id` = `medication`.`child_account_id`)))
   LEFT JOIN `tbl_user` `u`
     ON ((`u`.`id` = `a`.`user_id`)))
WHERE ((`i`.`task_type` = 18)
       AND (`i`.`delete_flag` = 0)
       AND (`medication`.`delete_flag` = 0)))$$

DELIMITER ;
DELIMITER $$



DROP VIEW IF EXISTS `vv_9`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `vv_9` AS (
SELECT
  `i`.`id`                AS `id`,
  `i`.`centre_id`         AS `centre_id`,
  `i`.`room_id`           AS `room_id`,
  `i`.`group_id`          AS `group_id`,
  `i`.`obj_id`            AS `obj_id`,
  `tfi`.`id`              AS `value_id`,
  `i`.`obj_type`          AS `obj_type`,
  `tfi`.`statu`           AS `statu`,
  `i`.`task_type`         AS `task_type`,
  `tfi`.`begin_date`      AS `begin_date`,
  `tfi`.`end_date`        AS `end_date`,
  CONCAT(`t`.`task_name`,'-follow up') AS `task_name`,
  `tfi`.`follow_up_id`    AS `task_model_id`,
  `i`.`create_time`       AS `create_time`,
  `i`.`update_account_id` AS `update_account_id`,
  2                       AS `t`,
  CONCAT(IF((IFNULL(`u`.`first_name`,'') = ''),'',CONCAT(`u`.`first_name`,' ')),IF((IFNULL(`u`.`middle_name`,'') = ''),'',CONCAT(`u`.`middle_name`,' ')),IF((IFNULL(`u`.`last_name`,'') = ''),'',CONCAT(`u`.`last_name`,' '))) AS `NAME`,
  `centre`.`name`         AS `cName`,
  `room`.`name`           AS `rName`
FROM ((((((`tbl_task_instance` `i`
        JOIN `tbl_task_follow_instance` `tfi`
          ON ((`tfi`.`task_instance_id` = `i`.`id`)))
       JOIN `tbl_task` `t`
         ON ((`t`.`id` = `i`.`task_model_id`)))
      LEFT JOIN `tbl_account` `account`
        ON ((`i`.`obj_id` = `account`.`id`)))
     LEFT JOIN `tbl_user` `u`
       ON ((`account`.`user_id` = `u`.`id`)))
    LEFT JOIN `tbl_centers` `centre`
      ON ((`centre`.`id` = `i`.`centre_id`)))
   LEFT JOIN `tbl_room` `room`
     ON ((`room`.`id` = `i`.`room_id`)))
WHERE ((`i`.`task_type` = 17)
       AND (`i`.`delete_flag` = 0)
       AND (`tfi`.`delete_flag` = 0)
       AND (`t`.`delete_flag` = 0)))$$

DELIMITER ;
DELIMITER $$



DROP VIEW IF EXISTS `vv_10`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `vv_10` AS (
SELECT DISTINCT
  `i`.`id`                AS `id`,
  `i`.`centre_id`         AS `centre_id`,
  `i`.`room_id`           AS `room_id`,
  `i`.`group_id`          AS `group_id`,
  `v`.`accountId`         AS `obj_id`,
  `i`.`value_id`          AS `value_id`,
  `i`.`obj_type`          AS `obj_type`,
  `i`.`statu`             AS `statu`,
  `i`.`task_type`         AS `task_type`,
  `i`.`begin_date`        AS `begin_date`,
  `i`.`end_date`          AS `end_date`,
  `v`.`name`              AS `task_name`,
  '0'                     AS `task_model_id`,
  `i`.`create_time`       AS `create_time`,
  `i`.`update_account_id` AS `update_account_id`,
  3                       AS `t`,
  NULL                    AS `NAME`,
  `centre`.`name`         AS `cName`,
  `room`.`name`           AS `rName`
FROM ((((`tbl_task_instance` `i`
      JOIN `v_meet_task_view` `v`
        ON ((`v`.`row_id` = `i`.`value_id`)))
     LEFT JOIN `tbl_centers` `centre`
       ON ((`centre`.`id` = `i`.`centre_id`)))
    LEFT JOIN `tbl_room` `room`
      ON ((`room`.`id` = `i`.`room_id`)))
   LEFT JOIN `v_meet_row_temp_view` `mrt`
     ON ((`i`.`value_id` = `mrt`.`row_id`)))
WHERE ((`i`.`task_type` = 20)
       AND (`i`.`delete_flag` = 0)))$$

DELIMITER ;.
DELIMITER $$



DROP VIEW IF EXISTS `vv_11`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `vv_11` AS (
SELECT DISTINCT
  `i`.`id`                AS `id`,
  `i`.`centre_id`         AS `centre_id`,
  `i`.`room_id`           AS `room_id`,
  `i`.`group_id`          AS `group_id`,
  `i`.`obj_id`            AS `obj_id`,
  `i`.`value_id`          AS `value_id`,
  `i`.`obj_type`          AS `obj_type`,
  `i`.`statu`             AS `statu`,
  `i`.`task_type`         AS `task_type`,
  `i`.`begin_date`        AS `begin_date`,
  `i`.`end_date`          AS `end_date`,
  `GetFullName`(
`u`.`first_name`,`u`.`middle_name`,`u`.`last_name`)  AS `task_name`,
  '0'                     AS `task_model_id`,
  `i`.`create_time`       AS `create_time`,
  `i`.`update_account_id` AS `update_account_id`,
  4                       AS `t`,
  NULL                    AS `NAME`,
  `centre`.`name`         AS `cName`,
  `room`.`name`           AS `rName`
FROM ((((`tbl_task_instance` `i`
      JOIN `tbl_account` `a`
        ON ((`a`.`id` = `i`.`value_id`)))
     JOIN `tbl_user` `u`
       ON ((`u`.`id` = `a`.`user_id`)))
    JOIN `tbl_centers` `centre`
      ON ((`centre`.`id` = `i`.`centre_id`)))
   JOIN `tbl_room` `room`
     ON ((`room`.`id` = `i`.`room_id`)))
WHERE ((`i`.`task_type` IN(21,22))
       AND (`i`.`delete_flag` = 0)))$$

DELIMITER ;




ALTER TABLE tbl_task_responsible_log ADD INDEX tbl_task_responsible_log_task_instance_id (task_instance_id);


ALTER TABLE tbl_task_responsible_log ADD INDEX tbl_task_responsible_log_acount_id (acount_id);