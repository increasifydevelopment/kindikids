INSERT INTO tbl_function (
	id,
	NAME,
	target,
	menu_id,
	function_type
)
VALUES
	(
		'fe75da83-c58d-11e6-985d-00e0703507b4',
		'attendance_updateTemporaryRequest',
		'/attendance/updateTemporaryRequest.do',
		'c27d4ffd-7994-11e6-85fd-00e0703507b4',
		2
	);


INSERT INTO tbl_role_function (id, role_id, function_id)
VALUES
	(
		'5eb882e3-c58e-11e6-985d-00e0703507b4',
		'2e083440-49a3-11e6-b19e-00e0703507b4',
		'fe75da83-c58d-11e6-985d-00e0703507b4'
	),
(
		'6444fdf2-c58e-11e6-985d-00e0703507b4',
		'4586aa2b-49a3-11e6-b19e-00e0703507b4',
		'fe75da83-c58d-11e6-985d-00e0703507b4'
	),
(
		'68139915-c58e-11e6-985d-00e0703507b4',
		'51c32196-49a3-11e6-b19e-00e0703507b4',
		'fe75da83-c58d-11e6-985d-00e0703507b4'
	),
(
		'6c609110-c58e-11e6-985d-00e0703507b4',
		'74ba21a6-49a3-11e6-b19e-00e0703507b4',
		'fe75da83-c58d-11e6-985d-00e0703507b4'
	),
(
		'703c345d-c58e-11e6-985d-00e0703507b4',
		'8090ce81-49a3-11e6-b19e-00e0703507b4',
		'fe75da83-c58d-11e6-985d-00e0703507b4'
	);
