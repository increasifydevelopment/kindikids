CREATE TABLE `tbl_room_change` (
  `centre_id` varchar(50) DEFAULT NULL,
  `room_id` varchar(50) DEFAULT NULL,
  `child_id` varchar(50) DEFAULT NULL,
  `req_id` varchar(50) DEFAULT NULL,
  `day_week` smallint(1) DEFAULT NULL,
  `operate` smallint(1) DEFAULT '1' COMMENT '0:add-all 1:remove-all 2:add-temp 3:remove-temp',
  `day` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `tbl_change_attendance_request`  ADD `source_type` smallint(1) DEFAULT 0;

ALTER TABLE `tbl_signin` ADD leave_flag BIT DEFAULT b'0';