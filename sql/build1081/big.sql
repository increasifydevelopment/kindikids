CREATE INDEX tbl_task_instance_centre_id ON`tbl_task_instance`(`centre_id`);

CREATE INDEX tbl_task_instance_room_id ON`tbl_task_instance`(`room_id`);

CREATE INDEX tbl_task_instance_group_id ON`tbl_task_instance`(`group_id`);

CREATE INDEX tbl_task_instance_obj_id ON`tbl_task_instance`(`obj_id`);

CREATE INDEX tbl_task_instance_obj_type ON`tbl_task_instance`(`obj_type`);

CREATE INDEX tbl_task_instance_value_id ON`tbl_task_instance`(`value_id`);


CREATE INDEX tbl_task_instance_task_type ON`tbl_task_instance`(`task_type`);

CREATE INDEX tbl_task_instance_task_model_id ON`tbl_task_instance`(`task_model_id`);


CREATE INDEX tbl_task_instance_statu ON`tbl_task_instance`(`statu`);


CREATE INDEX tbl_task_instance_date ON`tbl_task_instance`(`begin_date`,`end_date`);


CREATE INDEX tbl_task_task_code ON `tbl_task`(`task_code`);
CREATE INDEX tbl_task_shift_flag ON `tbl_task`(`shift_flag`);
CREATE INDEX tbl_task_delete_flag ON `tbl_task`(`delete_flag`);

CREATE INDEX tbl_program_task_exgrsc_delete_flag ON `tbl_program_task_exgrsc`(delete_flag);