INSERT INTO tbl_function (
id,
NAME,
target,
menu_id,
function_type
)
VALUES
(
'633c916f-edf1-11e8-96e0-408d5c9798a2',
'staff_saveEmploy',
'/staff/saveEmploy.do',
'3ea7375f-7890-11e6-85fd-00e0703507b4',
2
);



INSERT INTO tbl_role_function (id, role_id, function_id)
VALUES
(
'742a299a-edf1-11e8-96e0-408d5c9798a2',
'2e083440-49a3-11e6-b19e-00e0703507b4',
'633c916f-edf1-11e8-96e0-408d5c9798a2'
),
(
'78966619-edf1-11e8-96e0-408d5c9798a2',
'4586aa2b-49a3-11e6-b19e-00e0703507b4',
'633c916f-edf1-11e8-96e0-408d5c9798a2'
),
(
'7c72b2f7-edf1-11e8-96e0-408d5c9798a2',
'60a193d5-49a3-11e6-b19e-00e0703507b4',
'633c916f-edf1-11e8-96e0-408d5c9798a2'
);
	
INSERT INTO tbl_function (
id,
NAME,
target,
menu_id,
function_type
)
VALUES
(
'83d7679e-ed7a-11e8-96e0-408d5c9798a2',
'staff_cancelEmploy',
'/staff/cancelEmploy.do',
'3ea7375f-7890-11e6-85fd-00e0703507b4',
2
);



INSERT INTO tbl_role_function (id, role_id, function_id)
VALUES
	(
		'941f484a-ed7a-11e8-96e0-408d5c9798a2',
		'2e083440-49a3-11e6-b19e-00e0703507b4',
		'83d7679e-ed7a-11e8-96e0-408d5c9798a2'
	),
	(
		'99db88f4-ed7a-11e8-96e0-408d5c9798a2',
		'4586aa2b-49a3-11e6-b19e-00e0703507b4',
		'83d7679e-ed7a-11e8-96e0-408d5c9798a2'
	),
	(
		'9e208940-ed7a-11e8-96e0-408d5c9798a2',
		'60a193d5-49a3-11e6-b19e-00e0703507b4',
		'83d7679e-ed7a-11e8-96e0-408d5c9798a2'
	);
	
	
	
	
	
	
	/*
Navicat MySQL Data Transfer

Source Server         : 192.168.1.18
Source Server Version : 50636
Source Host           : 192.168.1.18:3306
Source Database       : kindikids_dev

Target Server Type    : MYSQL
Target Server Version : 50636
File Encoding         : 65001

Date: 2018-11-22 09:20:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_staff_employment
-- ----------------------------
DROP TABLE IF EXISTS `tbl_staff_employment`;
CREATE TABLE `tbl_staff_employment` (
  `id` varchar(50) NOT NULL,
  `account_id` varchar(50) DEFAULT NULL,
  `monday` bit(1) DEFAULT NULL,
  `mon_start_time` varchar(10) DEFAULT NULL,
  `mon_end_time` varchar(10) DEFAULT NULL,
  `mon_hours` decimal(10,3) DEFAULT NULL,
  `tuesday` bit(1) DEFAULT NULL,
  `tue_start_time` varchar(10) DEFAULT NULL,
  `tue_end_time` varchar(10) DEFAULT NULL,
  `tue_hours` decimal(10,3) DEFAULT NULL,
  `wednesday` bit(1) DEFAULT NULL,
  `wed_start_time` varchar(10) DEFAULT NULL,
  `wed_end_time` varchar(10) DEFAULT NULL,
  `wed_hours` decimal(10,3) DEFAULT NULL,
  `thursday` bit(1) DEFAULT NULL,
  `thu_start_time` varchar(10) DEFAULT NULL,
  `thu_end_time` varchar(10) DEFAULT NULL,
  `thu_hours` decimal(10,3) DEFAULT NULL,
  `friday` bit(1) DEFAULT NULL,
  `fri_start_time` varchar(10) DEFAULT NULL,
  `fri_end_time` varchar(10) DEFAULT NULL,
  `fri_hours` decimal(10,3) DEFAULT NULL,
  `last_day` datetime DEFAULT NULL,
  `primary_role` varchar(50) DEFAULT NULL,
  `employment_type` smallint(6) DEFAULT NULL,
  `region` smallint(6) DEFAULT NULL COMMENT '0 : All 1 : Edensor Park 2 : Ryde',
  `centre_id` varchar(50) DEFAULT NULL,
  `room_id` varchar(50) DEFAULT NULL,
  `group_id` varchar(50) DEFAULT NULL,
  `effective_date` datetime DEFAULT NULL,
  `staff_type` smallint(6) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `create_account_id` varchar(50) DEFAULT NULL,
  `current` bit(1) NOT NULL DEFAULT b'0',
  `delete_flag` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_taggings
-- ----------------------------
DROP TABLE IF EXISTS `tbl_taggings`;
CREATE TABLE `tbl_taggings` (
  `employment_tags_id` varchar(50) DEFAULT NULL,
  `tag_id` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

