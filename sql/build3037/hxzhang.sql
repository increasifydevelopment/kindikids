INSERT INTO tbl_function (
	id,
	NAME,
	target,
	menu_id,
	function_type
)
VALUES
(
	'40cf7557-b660-11e8-b729-408d5c9798a2',
	'family_enrolmentEmail.do',
	'/family/enrolmentEmail.do',
	'fe85639f-54ed-11e8-8348-00e0703507b4',
	1
);


UPDATE tbl_email_msg SET msg = REPLACE(msg,'Your first day at Kindikids email','Confirmation of Enrolment email') WHERE type = 3;