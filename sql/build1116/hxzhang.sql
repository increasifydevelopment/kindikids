ALTER TABLE tbl_event ADD COLUMN center_id VARCHAR (50);


INSERT INTO tbl_function (
	id,
	NAME,
	target,
	function_type
)
VALUES
	(
		UUID(),
		'event_centerList',
		'/event/centerList.do',
		1
	);