INSERT INTO tbl_function (
	id,
	NAME,
	target,
	menu_id,
	function_type
)
VALUE
	(
		'241a0745-b6a3-11e6-b88b-00e0703507b4',
		'task_getCount',
		'/task/getTaskCount.do',
		'628d7718-8e8c-11e6-85fd-00e0703507b4',
    1
	);
	
	
alter table tbl_child_attendance  add attend_flag bit(1) DEFAULT true;

alter table tbl_message_content modify column content LONGTEXT;


INSERT INTO tbl_function (
	id,
	NAME,
  target,
	menu_id,
	function_type
)
VALUE
	(
		'36927022-b6c7-11e6-b88b-00e0703507b4',
		'updateAttendChangeState',
		'/family/attendChange.do',
		'a610cba9-5a06-11e6-9202-00e0703507b4',
		2
	);



INSERT into tbl_role_function (id,role_id,function_id) 
VALUES ('a63a0d66-b6c7-11e6-b88b-00e0703507b4','2e083440-49a3-11e6-b19e-00e0703507b4','36927022-b6c7-11e6-b88b-00e0703507b4'),
('ffd0616a-b6c7-11e6-b88b-00e0703507b4','4586aa2b-49a3-11e6-b19e-00e0703507b4','36927022-b6c7-11e6-b88b-00e0703507b4'),
('0643f25b-b6c8-11e6-b88b-00e0703507b4','60a193d5-49a3-11e6-b19e-00e0703507b4','36927022-b6c7-11e6-b88b-00e0703507b4');

CREATE INDEX Idx_nqs_obj_id ON tbl_nqs_relation (obj_id);