DELIMITER $$

ALTER ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_family` AS 
SELECT
  `f`.`id`           AS `family_id`,
  `f`.`family_name`  AS `family_name`,
  CONCAT(IF((IFNULL(`u`.`first_name`,'') = ''),'',CONCAT(`u`.`first_name`,' ')),IF((IFNULL(`u`.`middle_name`,'') = ''),'',CONCAT(`u`.`middle_name`,' ')),IF((IFNULL(`u`.`last_name`,'') = ''),'',CONCAT(`u`.`last_name`,' '))) AS `user_name`,
  `u`.`id`           AS `user_id`,
  `u`.`user_type`    AS `user_type`,
  `a`.`status`       AS `status`,
  `a`.`id`           AS `account_id`,
  `u`.`avatar`       AS `avatar`,
  `u`.`email`        AS `email`,
  `u`.`create_time`  AS `create_time`,
  `u`.`person_color` AS `person_color`,
  `u`.`centers_id`   AS `centers_id`,
  `u`.`room_id`      AS `room_id`,
  `u`.`group_id`     AS `group_id`,
  `ca`.`enrolled`    AS `enrolled`,
  `ca`.`type`        AS `type`,
  `ca`.`siblingFlag` AS `siblingFlag`
FROM (((`tbl_family` `f`
     LEFT JOIN `tbl_user` `u`
       ON (((`f`.`id` = `u`.`family_id`)
            AND (`u`.`delete_flag` = 0))))
    LEFT JOIN `tbl_account` `a`
      ON (((`u`.`id` = `a`.`user_id`)
           AND (`a`.`delete_flag` = 0))))
   LEFT JOIN `tbl_child_attendance` `ca`
     ON (((`u`.`id` = `ca`.`user_id`)
          AND (`ca`.`delete_flag` = 0))))
WHERE (`f`.`delete_flag` = 0)$$

DELIMITER ;