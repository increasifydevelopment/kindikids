SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_centre_manager_relation
-- ----------------------------
DROP TABLE IF EXISTS `tbl_centre_manager_relation`;
CREATE TABLE `tbl_centre_manager_relation` (
  `centre_id` varchar(50) DEFAULT NULL,
  `user_id` varchar(50) DEFAULT NULL,
  `delete_flag` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_group_manager_relation
-- ----------------------------
DROP TABLE IF EXISTS `tbl_group_manager_relation`;
CREATE TABLE `tbl_group_manager_relation` (
  `group_id` varchar(50) DEFAULT NULL,
  `user_id` varchar(50) DEFAULT NULL,
  `delete_flag` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



INSERT INTO tbl_centre_manager_relation (centre_id,user_id,delete_flag) SELECT id,centers_leader_user_id,FALSE FROM tbl_centers;
INSERT INTO tbl_group_manager_relation (group_id,user_id,delete_flag) SELECT id,educator_user_id,FALSE FROM tbl_room_group WHERE educator_user_id IS NOT NULL;