UPDATE tbl_change_attendance_request
SET state = 5
WHERE
	id IN (
		SELECT
			t.id
		FROM
			(
				SELECT
					id
				FROM
					tbl_change_attendance_request
				WHERE
					child_id IN (
						SELECT
							child_id
						FROM
							tbl_change_attendance_request
						WHERE
							source_type = 6
						AND state = 0
					)
				GROUP BY
					child_id
				HAVING
					COUNT(child_id) = 1
			) t
	);

	
	
	
UPDATE tbl_change_attendance_request
SET state = 1
WHERE
	id IN (
		SELECT
			t.id
		FROM
			(
				SELECT
					id
				FROM
					tbl_change_attendance_request
				WHERE
					source_type = 6
				AND state = 0
			) t
	);