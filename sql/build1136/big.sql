/*Marymana Iskandar */
INSERT INTO tbl_attendance_history (
  id,
  child_id,
  center_id,
  room_id,
  monday,
  tuesday,
  wednesday,
  thursday,
  friday,
  delete_flag,
  begin_time,
  end_time,
  `group`,
  create_time
) 
VALUES
  (
    '30578e37-2034-11e7-8553-00e0703507b4',
    '562e5d78-a739-4284-b176-970f52ee6596',
    'ce183809-5319-4ac4-a1bf-ee35528f2f81',
    '0caf74fc-7827-4cfc-adb7-b6ce301b9b33',
    FALSE,
    TRUE,
    FALSE,
    FALSE,
    TRUE,
    0,
    '2017-03-09',
    '2017-03-23',
    1,
    '2017-03-09 08:56:30'
  );
  UPDATE tbl_attendance_history SET end_time='2017-03-09' WHERE id='66d1e2c2-ceb7-49b9-b463-e4ec05630e3d';
   UPDATE tbl_change_attendance_request SET state=3 WHERE id='1826d1f0-05cd-4384-8063-82d7b114dbec';
  /*Sophia Maniscalco  */
  INSERT INTO tbl_attendance_history (
  id,
  child_id,
  center_id,
  room_id,
  monday,
  tuesday,
  wednesday,
  thursday,
  friday,
  delete_flag,
  begin_time,
  end_time,
  `group`,
  create_time
) 
VALUES
  (
    '305930e3-2034-11e7-8553-00e0703507b4',
    '4a959b7f-aee9-4af4-aa79-6ac84d5fa4b7',
    'a15e1e6e-099e-4bf3-a668-cf11ff99abf6',
    '1bf08a64-7adc-4637-86af-0ca6b95851cd',
    FALSE,
    TRUE,
    TRUE,
    FALSE,
    TRUE,
    0,
    '2017-03-08',
    NULL,
    1,
    '2017-03-08'
  ) ;

  UPDATE tbl_attendance_history SET end_time='2017-03-08' WHERE id='363016ba-ea66-4b0f-899e-0e82f6f57cd3';
  UPDATE tbl_child_attendance SET wednesday=TRUE WHERE id='e7b9e8f3-65b0-4b97-b227-406b0a05464c';
  UPDATE tbl_change_attendance_request SET state=3 WHERE id='1c4845d5-7e5e-42e0-b18f-f285e98c4acb';
  UPDATE tbl_request_log SET state=NULL WHERE id='55886679-a352-4eb9-bf5d-61cd33a94df9';
  
  /*Viola Brindaro   */
  INSERT INTO tbl_attendance_history (
  id,
  child_id,
  center_id,
  room_id,
  monday,
  tuesday,
  wednesday,
  thursday,
  friday,
  delete_flag,
  begin_time,
  end_time,
  `group`,
  create_time
) 
VALUES
  (
    '305b4b16-2034-11e7-8553-00e0703507b4',
    '2925cbb5-ae26-4c72-8497-cbe03fee000e',
    'ce183809-5319-4ac4-a1bf-ee35528f2f81',
    '0caf74fc-7827-4cfc-adb7-b6ce301b9b33',
    TRUE,
    FALSE,
    TRUE,
    TRUE,
    TRUE,
    0,
    '2017-03-09',
    NULL,
    1,
    '2017-03-09'
  ) ;

  UPDATE tbl_attendance_history SET end_time='2017-03-09' WHERE id='408685f7-5d89-4c0c-a7e1-f4dca9c43964';
  UPDATE tbl_child_attendance SET thursday=TRUE WHERE id='e05679ea-6125-48c7-b3fb-c12f6e41b180';
  UPDATE tbl_change_attendance_request SET state=3 WHERE id='3a6261a5-842c-4c96-aa66-89e8a1c29769';
  
  /*Mila Elise Truong */
    
INSERT INTO tbl_attendance_history (
  id,
  child_id,
  center_id,
  room_id,
  monday,
  tuesday,
  wednesday,
  thursday,
  friday,
  delete_flag,
  begin_time,
  end_time,
  `group`,
  create_time
) 
VALUES
  (
    '4a9f2e4c-2034-11e7-8553-00e0703507b4',
    '33885f28-9074-452d-a86d-646692ad56df',
    '400f6580-82a5-411d-840a-41c18dbebf0c',
    'c2eb28e8-d5a3-4251-89b6-e365683a4bc4',
    FALSE,
    FALSE,
    FALSE,
    TRUE,
    TRUE,
    0,
    '2017-03-13',
    NULL,
    1,
    '2017-03-13'
  ) ;

  UPDATE tbl_attendance_history SET end_time='2017-03-13' WHERE id='4ff9cd01-81ee-411e-8c4c-bc1936bc5808';
  UPDATE tbl_change_attendance_request SET state=3 WHERE id='4a356880-5259-47ab-ba09-1248778471cd';
  /*Mark Rassam */
  UPDATE tbl_change_attendance_request SET state=1 WHERE id='77932370-dae2-494b-bcdb-5a27a8d50cbe';
  /*Vincent David Maniscalco */
  INSERT INTO tbl_attendance_history (
  id,
  child_id,
  center_id,
  room_id,
  monday,
  tuesday,
  wednesday,
  thursday,
  friday,
  delete_flag,
  begin_time,
  end_time,
  `group`,
  create_time
) 
VALUES
  (
    '4a9d1226-2034-11e7-8553-00e0703507b4',
    '185868d3-f270-4100-9df6-2275914dd53c',
    'a15e1e6e-099e-4bf3-a668-cf11ff99abf6',
    'cb87d300-83ab-4fb9-828f-fe1634d6d0f9',
    FALSE,
    TRUE,
    TRUE,
    TRUE,
    TRUE,
    0,
    '2017-03-08',
    NULL,
    1,
    '2017-03-08'
  ) ;

  UPDATE tbl_attendance_history SET end_time='2017-03-08' WHERE id='b77cab96-2a7f-4b44-b3d7-a05f93bdf7ff';
  UPDATE tbl_change_attendance_request SET state=3 WHERE id='95d04dd8-f1bb-4e3f-8c96-f5436340e51d';
  UPDATE tbl_child_attendance SET wednesday=TRUE WHERE id='dbdbe98b-8cf2-4250-bc5b-b66cc9230402';
  UPDATE  tbl_subtracted_attendance SET delete_flag=1 WHERE id IN('63dc6f7c-bfe6-4003-91fc-6f87adabb261','1957dc52-d2ec-4c6c-9914-d58e14e87d69');
  