drop table if exists tbl_relation_kid_parent;

/*==============================================================*/
/* Table: tbl_relation_kid_parent                               */
/*==============================================================*/
create table tbl_relation_kid_parent
(
   id                   varchar(50) not null,
   kid_account_id       varchar(50),
   parent_account_id    varchar(50),
   delete_flag          smallint(6),
   primary key (id)
);

alter table tbl_relation_kid_parent comment '小孩，父母关系表';