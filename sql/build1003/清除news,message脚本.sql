DELETE FROM tbl_news;

DELETE FROM tbl_news_accounts;

DELETE FROM tbl_news_comment;

DELETE FROM tbl_news_content;

DELETE FROM tbl_news_group;

DELETE FROM tbl_news_kids;

DELETE FROM tbl_news_receive;

DELETE FROM tbl_nqs_news;

DELETE FROM tbl_message;

DELETE FROM tbl_message_look;

DELETE FROM tbl_message_content;

DELETE FROM tbl_nqs_message;

DELETE FROM tbl_attachment;