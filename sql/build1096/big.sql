DELIMITER $$

DROP FUNCTION IF EXISTS `GetFullName`$$

CREATE DEFINER=`root`@`%` FUNCTION `GetFullName`(
  first_name VARCHAR (50),
  middle_name VARCHAR (50),
  last_name VARCHAR (50)
) RETURNS VARCHAR(255) CHARSET utf8
BEGIN
  DECLARE l_new_string VARCHAR (255) ;
  RETURN CONCAT(
    IF (
      IFNULL(first_name, '') = '',
      '',
      CONCAT(first_name, " ")
    ),
    IF (
      IFNULL(middle_name, '') = '',
      '',
      CONCAT(middle_name, " ")
    ),
    IF (
      IFNULL(last_name, '') = '',
      '',
      CONCAT(last_name, " ")
    )
  ) ;
END$$

DELIMITER ;