ALTER TABLE tbl_program_intobslea MODIFY COLUMN observation VARCHAR(2000);

ALTER TABLE tbl_task_program_follow_up MODIFY COLUMN observation VARCHAR(2000);