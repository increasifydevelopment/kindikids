DELETE FROM tbl_account where account!='Dev@increasify.com.au';
DELETE FROM tbl_account where isnull(account);
DELETE from tbl_account_role where account_id != '9549abe4-530a-11e6-9746-00e0703507b4';
DELETE from tbl_user where email!='Dev@increasify.com.au';
DELETE from tbl_user where isnull(email);
DELETE from tbl_medical_immunisation where user_id!='45f675b6-530a-11e6-9746-00e0703507b4';

DELETE from tbl_absentee_request;
DELETE from tbl_attachment;
DELETE from tbl_attendance_history;
DELETE from tbl_background_person;
DELETE from tbl_background_pets;
DELETE from tbl_centers;
DELETE from tbl_change_attendance_request;
DELETE from tbl_change_psw_record;
DELETE from tbl_child_attendance;
DELETE from tbl_child_background;
DELETE from tbl_child_custody_arrange;
DELETE from tbl_child_dietary_require;
DELETE from tbl_child_health_medical;
DELETE from tbl_child_health_medical_detail;
DELETE from tbl_child_logs;
DELETE from tbl_child_medical;
DELETE from tbl_credentials;
DELETE from tbl_emergency_contact;
DELETE from tbl_family;
DELETE from tbl_giving_notice;
DELETE from tbl_leave;
DELETE from tbl_leave_paid;
DELETE from tbl_medical;
DELETE from tbl_message;
DELETE from tbl_message_content;
DELETE from tbl_message_look;
DELETE from tbl_news;
DELETE from tbl_news_accounts;
DELETE from tbl_news_comment;
DELETE from tbl_news_content;
DELETE from tbl_news_group;

DELETE from tbl_news_receive;

DELETE from tbl_relation_kid_parent;
DELETE from tbl_request_log;
DELETE from tbl_room;
DELETE from tbl_room_group;
DELETE from tbl_sign_child;
DELETE from tbl_sign_relation;
DELETE from tbl_sign_visitor;
DELETE from tbl_signin;
DELETE from tbl_subtracted_attendance;
DELETE from tbl_temporary_attendance;
DELETE from tbl_transfe_request;
DELETE from tbl_week_nutrional_menu;
DELETE from tbl_yelf_news;

DELETE from tbl_child_eylf_log;
DELETE from tbl_child_eylf;
DELETE from tbl_task;
DELETE from tbl_task_followup_eylf;
DELETE FROM `tbl_task_instance`;
DELETE from tbl_task_followup_eylfcheck;
DELETE from tbl_task_follow_up;
DELETE from tbl_task_frequency;
DELETE from tbl_task_person_liable;
DELETE from tbl_task_program_follow_up;
DELETE from tbl_task_value;
DELETE from tbl_task_visible;
DELETE from tbl_program_intobslea;
DELETE from tbl_program_lesson;
delete from tbl_program_medication;
DELETE from tbl_program_register_item;
DELETE from tbl_program_register_sleep;
DELETE from tbl_program_register_task;
DELETE from tbl_program_task_exgrsc;
DELETE from tbl_program_weekly_evalution;
delete from tbl_task_form_relation;
delete from tbl_task_follow_instance;

delete from tbl_instance_attribute;
delete from tbl_value_string;
delete from tbl_value_table;
delete from tbl_value_text;

DELETE from tbl_value_select;
delete from tbl_form_attribute;
delete from tbl_source_logs;
delete from tbl_logs_detaile;
delete from tbl_task_responsible_log;
delete from tbl_time_medication;

delete from tbl_instance;
delete from tbl_form;


DELETE from tbl_roster_shift;
DELETE from tbl_roster_shift_time;
DELETE from tbl_roster_shift_task;
DELETE from tbl_shift_week;
DELETE from tbl_roster;
DELETE from tbl_roster_staff;
DELETE from tbl_policy_categories;
DELETE from tbl_policy;
DELETE from tbl_policy_role_relation;
DELETE from tbl_policy_documents;
DELETE from tbl_agenda_item;
DELETE from tbl_meeting_agenda;
DELETE from tbl_agenda_type;
DELETE from tbl_meeting;
DELETE from tbl_meeting_template;
DELETE from tbl_template_frequency;
DELETE from tbl_meeting_columns;
DELETE from tbl_meeting_row;

delete from tbl_child_form_relation;
delete from tbl_profile_template;
delete from tbl_nqs_relation;

delete from tbl_event;
DELETE from tbl_temp_roster_center;
DELETE from tbl_roster_leave_absence;
delete from tbl_roster_leave_item;
DELETE from tbl_out_child_center;

DELETE from tbl_event_child;
DELETE from tbl_event_documents;
DELETE from tbl_msg_email;
DELETE from tbl_out_relevant;
DELETE from tbl_relation_meeting;
DELETE from tbl_room_change;
/*
Navicat MySQL Data Transfer

Source Server         : gfwang
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : kindkids_dev

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2016-11-25 15:48:49
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_authority_group
-- ----------------------------
DROP TABLE IF EXISTS `tbl_authority_group`;
CREATE TABLE `tbl_authority_group` (
  `id` varchar(50) NOT NULL,
  `group_name` varchar(300) DEFAULT NULL COMMENT '閸掑棛绮嶉崥宥囆',
  `sql_tag` text COMMENT 'sql閻楀洦?閿?where 閿',
  `status` smallint(1) DEFAULT NULL COMMENT '1閸?鏁ら敍?缁備胶鏁',
  `create_time` datetime DEFAULT NULL,
  `delete_flag` smallint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_authority_group
-- ----------------------------
INSERT INTO `tbl_authority_group` VALUES ('2bbac36e-816e-11e6-85fd-00e0703507b4', 'All Staff', 'SELECT t.name AS NAME,t.email,t.id AS account_id,t.centers_id AS center_id,t.role_value AS role_value,0=1 as disabled FROM(SELECT CONCAT(u.first_name,\' \',IFNULL(u.middle_name,\'\'),\' \', u.last_name) AS NAME,u.email,u.user_type,a.id AS id,u.centers_id,r.`value` AS role_value,r.role_type AS role_type,c.name as center_name FROM tbl_user u LEFT JOIN tbl_centers c ON u.centers_id=c.id AND c.delete_flag=0 LEFT JOIN tbl_account a ON u.id = a.user_id AND a.delete_flag = 0 LEFT JOIN tbl_account_role ar ON a.id = ar.account_id AND ar.delete_flag = 0 LEFT JOIN tbl_role r ON ar.role_id = r.id AND r.delete_flag = 0 WHERE u.delete_flag = 0) t WHERE 1 = 1 and t.user_type=0', '1', '2016-09-23 17:14:57', '0');
INSERT INTO `tbl_authority_group` VALUES ('5b1c32bc-816f-11e6-85fd-00e0703507b4', 'All Active Children', 'SELECT t.name AS NAME,t.email,t.id AS account_id,t.centers_id AS center_id,t.role_value AS role_value,0=1 as disabled FROM(SELECT CONCAT(u.first_name,\' \',IFNULL(u.middle_name,\'\'),\' \', u.last_name) AS NAME,u.email,u.user_type,a.id AS id,a.`status`,u.centers_id,r.`value` AS role_value,r.role_type AS role_type,c.name as center_name FROM tbl_user u LEFT JOIN tbl_centers c ON u.centers_id=c.id AND c.delete_flag=0 LEFT JOIN tbl_account a ON u.id = a.user_id AND a.delete_flag = 0 LEFT JOIN tbl_account_role ar ON a.id = ar.account_id AND ar.delete_flag = 0 LEFT JOIN tbl_role r ON ar.role_id = r.id AND r.delete_flag = 0 WHERE u.delete_flag = 0) t WHERE 1 = 1 and t.user_type = 2 and STATUS=1', '1', '2016-09-23 17:23:19', '0');
INSERT INTO `tbl_authority_group` VALUES ('72a9e8d2-816c-11e6-85fd-00e0703507b4', 'All Children', 'SELECT t.name AS NAME,t.email,t.id AS account_id,t.centers_id AS center_id,t.role_value AS role_value,0=1 as disabled FROM(SELECT CONCAT(u.first_name,\' \',IFNULL(u.middle_name,\'\'),\' \', u.last_name) AS NAME,u.email,a.id AS id,u.centers_id,r.`value` AS role_value,r.role_type AS role_type,c.name as center_name FROM tbl_user u LEFT JOIN tbl_centers c ON u.centers_id=c.id AND c.delete_flag=0 LEFT JOIN tbl_account a ON u.id = a.user_id AND a.delete_flag = 0 LEFT JOIN tbl_account_role ar ON a.id = ar.account_id AND ar.delete_flag = 0 LEFT JOIN tbl_role r ON ar.role_id = r.id AND r.delete_flag = 0 WHERE u.delete_flag = 0) t WHERE 1 = 1 and t.role_value  in (11)', '1', '2016-09-23 17:02:46', '0');
INSERT INTO `tbl_authority_group` VALUES ('76f6e5a6-816f-11e6-85fd-00e0703507b4', 'All Archived Children', 'SELECT t.name AS NAME,t.email,t.id AS account_id,t.centers_id AS center_id,t.role_value AS role_value,0=1 as disabled FROM(SELECT CONCAT(u.first_name,\' \',IFNULL(u.middle_name,\'\'),\' \', u.last_name) AS NAME,u.email,u.user_type,a.id AS id,a.`status`,u.centers_id,r.`value` AS role_value,r.role_type AS role_type,c.name as center_name FROM tbl_user u LEFT JOIN tbl_centers c ON u.centers_id=c.id AND c.delete_flag=0 LEFT JOIN tbl_account a ON u.id = a.user_id AND a.delete_flag = 0 LEFT JOIN tbl_account_role ar ON a.id = ar.account_id AND ar.delete_flag = 0 LEFT JOIN tbl_role r ON ar.role_id = r.id AND r.delete_flag = 0 WHERE u.delete_flag = 0) t WHERE 1 = 1 and t.user_type = 2 and STATUS=0', '1', '2016-09-23 17:24:10', '0');
INSERT INTO `tbl_authority_group` VALUES ('83e51bab-816e-11e6-85fd-00e0703507b4', 'All Educators', 'SELECT t.name AS NAME,t.email,t.id AS account_id,t.centers_id AS center_id,t.role_value AS role_value,0=1 as  disabled FROM(SELECT CONCAT(u.first_name,\' \',IFNULL(u.middle_name,\'\'),\' \', u.last_name) AS NAME,u.email,a.id AS id,u.centers_id,r.`value` AS role_value,r.role_type AS role_type,c.name as center_name FROM tbl_user u LEFT JOIN tbl_centers c ON u.centers_id=c.id AND c.delete_flag=0 LEFT JOIN tbl_account a ON u.id = a.user_id AND a.delete_flag = 0 LEFT JOIN tbl_account_role ar ON a.id = ar.account_id AND ar.delete_flag = 0 LEFT JOIN tbl_role r ON ar.role_id = r.id AND r.delete_flag = 0 WHERE u.delete_flag = 0) t WHERE 1 = 1 and t.role_value in (2)', '1', '2016-09-23 17:17:04', '0');
INSERT INTO `tbl_authority_group` VALUES ('b372a7b9-8170-11e6-85fd-00e0703507b4', 'All Parents', 'SELECT t.name AS NAME,t.email,t.id AS account_id,t.centers_id AS center_id,t.role_value AS role_value,0=1 as disabled FROM(SELECT CONCAT(u.first_name,\' \',IFNULL(u.middle_name,\'\'),\' \', u.last_name) AS NAME,u.email,u.user_type,a.id AS id,a.`status`,u.centers_id,r.`value` AS role_value,r.role_type AS role_type,c.name as center_name FROM tbl_user u LEFT JOIN tbl_centers c ON u.centers_id=c.id AND c.delete_flag=0 LEFT JOIN tbl_account a ON u.id = a.user_id AND a.delete_flag = 0 LEFT JOIN tbl_account_role ar ON a.id = ar.account_id AND ar.delete_flag = 0 LEFT JOIN tbl_role r ON ar.role_id = r.id AND r.delete_flag = 0 WHERE u.delete_flag = 0) t WHERE 1 = 1 and t.user_type =1', '1', '2016-09-23 17:33:02', '0');
INSERT INTO `tbl_authority_group` VALUES ('bac9fb7e-7898-11e6-85fd-00e0703507b4', 'Directors', 'SELECT t.name AS NAME,t.email,t.id AS account_id,t.centers_id AS center_id,t.role_value AS role_value,0=1 as disabled FROM(SELECT CONCAT(u.first_name,\' \',IFNULL(u.middle_name,\'\'),\' \', u.last_name) AS NAME,u.email,a.id AS id,u.centers_id,r.`value` AS role_value,r.role_type AS role_type,c.name as center_name FROM tbl_user u LEFT JOIN tbl_centers c ON u.centers_id=c.id AND c.delete_flag=0 LEFT JOIN tbl_account a ON u.id = a.user_id AND a.delete_flag = 0 LEFT JOIN tbl_account_role ar ON a.id = ar.account_id AND ar.delete_flag = 0 LEFT JOIN tbl_role r ON ar.role_id = r.id AND r.delete_flag = 0 WHERE u.delete_flag = 0) t WHERE 1 = 1 and t.role_value  in (0)\r\n', '1', '2016-09-12 11:28:20', '0');
INSERT INTO `tbl_authority_group` VALUES ('bacba293-7898-11e6-85fd-00e0703507b4', 'Public', 'SELECT t.name AS NAME,t.email,t.id AS account_id,t.centers_id AS center_id,t.role_value AS role_value,0=1 as disabled FROM(SELECT CONCAT(u.first_name,\' \',IFNULL(u.middle_name,\'\'),\' \', u.last_name) AS NAME,u.email,a.id AS id,u.centers_id,r.`value` AS role_value,r.role_type AS role_type,c.name as center_name FROM tbl_user u LEFT JOIN tbl_centers c ON u.centers_id=c.id AND c.delete_flag=0 LEFT JOIN tbl_account a ON u.id = a.user_id AND a.delete_flag = 0 LEFT JOIN tbl_account_role ar ON a.id = ar.account_id AND ar.delete_flag = 0 LEFT JOIN tbl_role r ON ar.role_id = r.id AND r.delete_flag = 0 WHERE u.delete_flag = 0) t WHERE 1 = 1 \r\n', '1', '2016-09-12 11:28:15', '0');
INSERT INTO `tbl_authority_group` VALUES ('bacdb137-7898-11e6-85fd-00e0703507b4', 'All Centre Managers & 2IC', 'SELECT t.name AS NAME,t.email,t.id AS account_id,t.centers_id AS center_id,t.role_value AS role_value,0=1 as disabled FROM(SELECT CONCAT(u.first_name,\' \',IFNULL(u.middle_name,\'\'),\' \', u.last_name) AS NAME,u.email,a.id AS id,u.centers_id,r.value AS role_value,r.role_type AS role_type,c.name as center_name FROM tbl_user u LEFT JOIN tbl_centers c ON u.centers_id=c.id AND c.delete_flag=0 LEFT JOIN tbl_account a ON u.id = a.user_id AND a.delete_flag = 0 LEFT JOIN tbl_account_role ar ON a.id = ar.account_id AND ar.delete_flag = 0 LEFT JOIN tbl_role r ON ar.role_id = r.id AND r.delete_flag = 0 WHERE u.delete_flag = 0) t WHERE 1 = 1 and t.role_value  in (1,5)\r\n', '1', '2016-09-12 11:28:22', '0');
INSERT INTO `tbl_authority_group` VALUES ('bad448f2-7898-11e6-85fd-00e0703507b4', 'All Casuals', 'SELECT t.name AS NAME,t.email,t.id AS account_id,t.centers_id AS center_id,t.role_value AS role_value,0=1 as disabled FROM(SELECT CONCAT(u.first_name,\' \',IFNULL(u.middle_name,\'\'),\' \', u.last_name) AS NAME,u.email,a.id AS id,u.centers_id,r.`value` AS role_value,r.role_type AS role_type,c.name as center_name FROM tbl_user u LEFT JOIN tbl_centers c ON u.centers_id=c.id AND c.delete_flag=0 LEFT JOIN tbl_account a ON u.id = a.user_id AND a.delete_flag = 0 LEFT JOIN tbl_account_role ar ON a.id = ar.account_id AND ar.delete_flag = 0 LEFT JOIN tbl_role r ON ar.role_id = r.id AND r.delete_flag = 0 WHERE u.delete_flag = 0) t WHERE 1 = 1 and t.role_value  in (4)\r\n', '1', '2016-09-12 11:28:25', '0');
INSERT INTO `tbl_authority_group` VALUES ('f66a1800-7898-11e6-85fd-00e0703507b4', 'All Cooks', 'SELECT t.name AS NAME,t.email,t.id AS account_id,t.centers_id AS center_id,t.role_value AS role_value,0=1 as disabled FROM(SELECT CONCAT(u.first_name,\' \',IFNULL(u.middle_name,\'\'),\' \', u.last_name) AS NAME,u.email,a.id AS id,u.centers_id,r.`value` AS role_value,r.role_type AS role_type,c.name as center_name FROM tbl_user u LEFT JOIN tbl_centers c ON u.centers_id=c.id AND c.delete_flag=0 LEFT JOIN tbl_account a ON u.id = a.user_id AND a.delete_flag = 0 LEFT JOIN tbl_account_role ar ON a.id = ar.account_id AND ar.delete_flag = 0 LEFT JOIN tbl_role r ON ar.role_id = r.id AND r.delete_flag = 0 WHERE u.delete_flag = 0) t WHERE 1 = 1 and  t.role_value  in (9)\r\n', '1', '2016-09-12 11:28:29', '0');

/*
Navicat MySQL Data Transfer

Source Server         : gfwang
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : kindkids_dev

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2016-11-25 15:49:04
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_role_group_relation
-- ----------------------------
DROP TABLE IF EXISTS `tbl_role_group_relation`;
CREATE TABLE `tbl_role_group_relation` (
  `id` varchar(50) NOT NULL,
  `role_id` varchar(50) DEFAULT NULL,
  `auth_group_id` varchar(50) DEFAULT NULL,
  `type` smallint(1) DEFAULT NULL COMMENT '0message,1newsfeed',
  `delete_flag` smallint(1) DEFAULT NULL,
  `center_id` varchar(50) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_role_group_relation
-- ----------------------------
INSERT INTO `tbl_role_group_relation` VALUES ('0440747b-9b42-11e6-a748-00e0701320d1', '4586aa2b-49a3-11e6-b19e-00e0703507b4', 'bad448f2-7898-11e6-85fd-00e0703507b4', '1', '0', null, '2016-09-24 00:00:00');
INSERT INTO `tbl_role_group_relation` VALUES ('0caf1905-460a-4380-96ed-8ca616b520ab', null, '83e51bab-816e-11e6-85fd-00e0703507b4', '2', '0', null, '2016-09-24 18:04:08');
INSERT INTO `tbl_role_group_relation` VALUES ('0ff61400-499e-4ebf-9947-774106f3e02f', null, 'bad448f2-7898-11e6-85fd-00e0703507b4', '2', '0', null, '2016-09-24 18:04:08');
INSERT INTO `tbl_role_group_relation` VALUES ('10abf31a-78ab-11e6-85fd-00e0703507b4', '5bce6bab-49a3-11e6-b19e-00e0703507b4', 'bad448f2-7898-11e6-85fd-00e0703507b4', '1', '0', null, '2016-09-12 13:42:27');
INSERT INTO `tbl_role_group_relation` VALUES ('10af191f-78ab-11e6-85fd-00e0703507b4', '5bce6bab-49a3-11e6-b19e-00e0703507b4', 'f66a1800-7898-11e6-85fd-00e0703507b4', '1', '0', null, '2016-09-12 13:42:30');
INSERT INTO `tbl_role_group_relation` VALUES ('1fed3ac2-e862-4480-9e51-f035803e222a', null, '2bbac36e-816e-11e6-85fd-00e0703507b4', '2', '0', null, '2016-09-24 18:04:08');
INSERT INTO `tbl_role_group_relation` VALUES ('364c07c8-9b42-11e6-a748-00e0701320d1', '60a193d5-49a3-11e6-b19e-00e0703507b4', 'bad448f2-7898-11e6-85fd-00e0703507b4', '1', '0', null, '2016-09-24 00:00:00');
INSERT INTO `tbl_role_group_relation` VALUES ('469ca6ea-7899-11e6-85fd-00e0703507b4', '2e083440-49a3-11e6-b19e-00e0703507b4', 'bac9fb7e-7898-11e6-85fd-00e0703507b4', '0', '0', null, '2016-09-12 13:40:45');
INSERT INTO `tbl_role_group_relation` VALUES ('469eb46c-7899-11e6-85fd-00e0703507b4', '2e083440-49a3-11e6-b19e-00e0703507b4', 'bad448f2-7898-11e6-85fd-00e0703507b4', '0', '0', null, '2016-09-12 13:40:50');
INSERT INTO `tbl_role_group_relation` VALUES ('46a0c946-7899-11e6-85fd-00e0703507b4', '2e083440-49a3-11e6-b19e-00e0703507b4', 'bacdb137-7898-11e6-85fd-00e0703507b4', '0', '0', null, '2016-09-12 13:40:48');
INSERT INTO `tbl_role_group_relation` VALUES ('46a2dd5b-7899-11e6-85fd-00e0703507b4', '2e083440-49a3-11e6-b19e-00e0703507b4', 'f66a1800-7898-11e6-85fd-00e0703507b4', '0', '0', null, '2016-09-12 13:40:52');
INSERT INTO `tbl_role_group_relation` VALUES ('46a4dece-7899-11e6-85fd-00e0703507b4', '4586aa2b-49a3-11e6-b19e-00e0703507b4', 'bac9fb7e-7898-11e6-85fd-00e0703507b4', '0', '0', null, '2016-09-12 13:40:54');
INSERT INTO `tbl_role_group_relation` VALUES ('46a7c0ca-7899-11e6-85fd-00e0703507b4', '4586aa2b-49a3-11e6-b19e-00e0703507b4', 'bad448f2-7898-11e6-85fd-00e0703507b4', '0', '0', null, '2016-09-12 13:40:57');
INSERT INTO `tbl_role_group_relation` VALUES ('6034bb72-9b4b-11e6-85fd-00e0703507b4', '60a193d5-49a3-11e6-b19e-00e0703507b4', 'f66a1800-7898-11e6-85fd-00e0703507b4', '0', '0', null, '2016-10-26 15:11:45');
INSERT INTO `tbl_role_group_relation` VALUES ('6cbd6e19-5e28-489f-853c-da7427830361', null, '72a9e8d2-816c-11e6-85fd-00e0703507b4', '2', '0', null, '2016-09-24 18:04:08');
INSERT INTO `tbl_role_group_relation` VALUES ('713921e6-2ad8-45bc-a5d8-1994cc99782c', null, 'f66a1800-7898-11e6-85fd-00e0703507b4', '2', '0', null, '2016-09-24 18:04:08');
INSERT INTO `tbl_role_group_relation` VALUES ('7f6ce4c1-9b4a-11e6-85fd-00e0703507b4', '60a193d5-49a3-11e6-b19e-00e0703507b4', 'f66a1800-7898-11e6-85fd-00e0703507b4', '1', '0', null, '2016-10-26 15:05:14');
INSERT INTO `tbl_role_group_relation` VALUES ('80464c39-3c78-4915-a769-505fc621b554', null, 'b372a7b9-8170-11e6-85fd-00e0703507b4', '2', '0', null, '2016-09-24 18:04:08');
INSERT INTO `tbl_role_group_relation` VALUES ('812f6ffa-789b-11e6-85fd-00e0703507b4', '4586aa2b-49a3-11e6-b19e-00e0703507b4', 'bad448f2-7898-11e6-85fd-00e0703507b4', '0', '0', null, '2016-09-12 13:41:36');
INSERT INTO `tbl_role_group_relation` VALUES ('8131293d-789b-11e6-85fd-00e0703507b4', '4586aa2b-49a3-11e6-b19e-00e0703507b4', 'f66a1800-7898-11e6-85fd-00e0703507b4', '1', '0', null, '2016-09-12 13:42:17');
INSERT INTO `tbl_role_group_relation` VALUES ('8132cd5d-789b-11e6-85fd-00e0703507b4', '51c32196-49a3-11e6-b19e-00e0703507b4', 'bac9fb7e-7898-11e6-85fd-00e0703507b4', '1', '0', null, '2016-09-12 13:42:22');
INSERT INTO `tbl_role_group_relation` VALUES ('8134a468-789b-11e6-85fd-00e0703507b4', '5bce6bab-49a3-11e6-b19e-00e0703507b4', 'bacdb137-7898-11e6-85fd-00e0703507b4', '1', '0', null, '2016-09-12 13:42:25');
INSERT INTO `tbl_role_group_relation` VALUES ('8136a7e5-789b-11e6-85fd-00e0703507b4', '5bce6bab-49a3-11e6-b19e-00e0703507b4', 'bac9fb7e-7898-11e6-85fd-00e0703507b4', '1', '0', null, '2016-09-12 13:42:24');
INSERT INTO `tbl_role_group_relation` VALUES ('813975e2-789b-11e6-85fd-00e0703507b4', '74ba21a6-49a3-11e6-b19e-00e0703507b4', 'bac9fb7e-7898-11e6-85fd-00e0703507b4', '1', '0', null, '2016-09-12 13:42:33');
INSERT INTO `tbl_role_group_relation` VALUES ('9f7ace49-525d-48ad-9345-422ebc03a2a5', null, '76f6e5a6-816f-11e6-85fd-00e0703507b4', '2', '0', null, '2016-09-24 18:04:08');
INSERT INTO `tbl_role_group_relation` VALUES ('b0597f0d-9b4a-11e6-85fd-00e0703507b4', '60a193d5-49a3-11e6-b19e-00e0703507b4', 'bacdb137-7898-11e6-85fd-00e0703507b4', '1', '0', null, '2016-10-26 15:06:48');
INSERT INTO `tbl_role_group_relation` VALUES ('c2870752-5874-4767-ba85-ae70caa52c61', null, 'bacdb137-7898-11e6-85fd-00e0703507b4', '2', '0', null, '2016-09-24 18:04:08');
INSERT INTO `tbl_role_group_relation` VALUES ('c43cfa79-f221-4fcd-8cb7-b1c8e28bde86', null, '5b1c32bc-816f-11e6-85fd-00e0703507b4', '2', '0', null, '2016-09-24 18:04:08');
INSERT INTO `tbl_role_group_relation` VALUES ('c4a47c2d-789a-11e6-85fd-00e0703507b4', '4586aa2b-49a3-11e6-b19e-00e0703507b4', 'f66a1800-7898-11e6-85fd-00e0703507b4', '0', '0', null, '2016-09-12 13:41:38');
INSERT INTO `tbl_role_group_relation` VALUES ('c4a647dd-789a-11e6-85fd-00e0703507b4', '51c32196-49a3-11e6-b19e-00e0703507b4', 'bac9fb7e-7898-11e6-85fd-00e0703507b4', '0', '0', null, '2016-09-12 13:41:41');
INSERT INTO `tbl_role_group_relation` VALUES ('c4a826cb-789a-11e6-85fd-00e0703507b4', '74ba21a6-49a3-11e6-b19e-00e0703507b4', 'bac9fb7e-7898-11e6-85fd-00e0703507b4', '0', '0', null, '2016-09-12 13:41:59');
INSERT INTO `tbl_role_group_relation` VALUES ('c4aa1d43-789a-11e6-85fd-00e0703507b4', '5bce6bab-49a3-11e6-b19e-00e0703507b4', 'bac9fb7e-7898-11e6-85fd-00e0703507b4', '0', '0', null, '2016-09-12 13:41:43');
INSERT INTO `tbl_role_group_relation` VALUES ('c4ac1bb9-789a-11e6-85fd-00e0703507b4', '5bce6bab-49a3-11e6-b19e-00e0703507b4', 'bacdb137-7898-11e6-85fd-00e0703507b4', '0', '0', null, '2016-09-12 13:41:55');
INSERT INTO `tbl_role_group_relation` VALUES ('c4aefd66-789a-11e6-85fd-00e0703507b4', '2e083440-49a3-11e6-b19e-00e0703507b4', 'bacba293-7898-11e6-85fd-00e0703507b4', '1', '0', null, '2016-09-12 13:42:04');
INSERT INTO `tbl_role_group_relation` VALUES ('dae70232-789a-11e6-85fd-00e0703507b4', '2e083440-49a3-11e6-b19e-00e0703507b4', 'bac9fb7e-7898-11e6-85fd-00e0703507b4', '1', '0', null, '2016-09-12 13:42:01');
INSERT INTO `tbl_role_group_relation` VALUES ('dae8a9e6-789a-11e6-85fd-00e0703507b4', '2e083440-49a3-11e6-b19e-00e0703507b4', 'bacdb137-7898-11e6-85fd-00e0703507b4', '1', '0', null, '2016-09-12 13:42:07');
INSERT INTO `tbl_role_group_relation` VALUES ('daea90de-789a-11e6-85fd-00e0703507b4', '2e083440-49a3-11e6-b19e-00e0703507b4', 'bad448f2-7898-11e6-85fd-00e0703507b4', '1', '0', null, '2016-09-12 13:42:10');
INSERT INTO `tbl_role_group_relation` VALUES ('daece701-789a-11e6-85fd-00e0703507b4', '2e083440-49a3-11e6-b19e-00e0703507b4', 'f66a1800-7898-11e6-85fd-00e0703507b4', '1', '0', null, '2016-09-12 13:42:12');
INSERT INTO `tbl_role_group_relation` VALUES ('daf012c1-789a-11e6-85fd-00e0703507b4', '4586aa2b-49a3-11e6-b19e-00e0703507b4', 'bac9fb7e-7898-11e6-85fd-00e0703507b4', '1', '0', null, '2016-09-12 13:42:15');
INSERT INTO `tbl_role_group_relation` VALUES ('daf367ae-789a-11e6-85fd-00e0703507b4', '4586aa2b-49a3-11e6-b19e-00e0703507b4', 'bacdb137-7898-11e6-85fd-00e0703507b4', '1', '0', null, '2016-09-12 13:42:20');
INSERT INTO `tbl_role_group_relation` VALUES ('eab56875-9b4a-11e6-85fd-00e0703507b4', '60a193d5-49a3-11e6-b19e-00e0703507b4', 'bad448f2-7898-11e6-85fd-00e0703507b4', '0', '0', null, '2016-10-26 15:08:57');
