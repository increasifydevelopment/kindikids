INSERT INTO tbl_function (
	id,
	NAME,
	target,
	menu_id,
	function_type
)
VALUES
	(
		'6af4f639-15b3-11e7-ab8a-00e070220090',
		'waitingList_importWaitingList',
		'/waitingList/importWaitingList.do',
		'2321e129-8ea8-11e6-85fd-00e0703507b4',
		2
	);

INSERT INTO tbl_role_function (id, role_id, function_id)
VALUES
	(
		'e0518bf1-15b4-11e7-ab8a-00e070220090',
		'2e083440-49a3-11e6-b19e-00e0703507b4',
		'6af4f639-15b3-11e7-ab8a-00e070220090'
	);

DELETE FROM tbl_function WHERE id = '6af4f639-15b3-11e7-ab8a-00e070220090';
DELETE FROM tbl_role_function WHERE id = 'e0518bf1-15b4-11e7-ab8a-00e070220090';