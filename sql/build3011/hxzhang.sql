INSERT INTO tbl_function (
	id,
	NAME,
	target,
	menu_id,
	function_type
)
VALUES
(
	'4316bb11-6932-11e8-a955-00e0703507b4',
	'family_moveFamily.do',
	'/family/moveFamily.do',
	'fe85639f-54ed-11e8-8348-00e0703507b4',
	2
);

INSERT INTO tbl_role_function (id, role_id, function_id)
VALUES
(
	'a67a6015-6932-11e8-a955-00e0703507b4',
	'2e083440-49a3-11e6-b19e-00e0703507b4',
	'4316bb11-6932-11e8-a955-00e0703507b4'
),
(
	'aa698984-6932-11e8-a955-00e0703507b4',
	'4586aa2b-49a3-11e6-b19e-00e0703507b4',
	'4316bb11-6932-11e8-a955-00e0703507b4'
),
(
	'afd85914-6932-11e8-a955-00e0703507b4',
	'60a193d5-49a3-11e6-b19e-00e0703507b4',
	'4316bb11-6932-11e8-a955-00e0703507b4'
);

INSERT INTO tbl_function (
	id,
	NAME,
	target,
	menu_id,
	function_type
)
VALUES
(
	'1a687b04-694e-11e8-a955-00e0703507b4',
	'family_moveToOutside.do',
	'/family/moveToOutside.do',
	'fe85639f-54ed-11e8-8348-00e0703507b4',
	2
);

INSERT INTO tbl_role_function (id, role_id, function_id)
VALUES
(
	'2fca6b0f-694e-11e8-a955-00e0703507b4',
	'2e083440-49a3-11e6-b19e-00e0703507b4',
	'1a687b04-694e-11e8-a955-00e0703507b4'
),
(
	'37c8ee93-694e-11e8-a955-00e0703507b4',
	'4586aa2b-49a3-11e6-b19e-00e0703507b4',
	'1a687b04-694e-11e8-a955-00e0703507b4'
),
(
	'3bc45ba2-694e-11e8-a955-00e0703507b4',
	'60a193d5-49a3-11e6-b19e-00e0703507b4',
	'1a687b04-694e-11e8-a955-00e0703507b4'
);


DELIMITER $$

DROP VIEW IF EXISTS `view_archived_list`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `view_archived_list` AS 
SELECT
  `app`.`id`                        AS `id`,
  CONCAT(`app`.`first_name`,' ',`app`.`last_name`) AS `childName`,
  `app`.`birthday`                  AS `birthday`,
  `u`.`family_id`                   AS `familyId`,
  `app`.`monday`                    AS `monday`,
  `app`.`tuesday`                   AS `tuesday`,
  `app`.`wednesday`                 AS `wednesday`,
  `app`.`thursday`                  AS `thursday`,
  `app`.`friday`                    AS `friday`,
  `app`.`application_date`          AS `applicationDate`,
  `app`.`application_date_position` AS `applicationDatePosition`,
  `app`.`requested_start_date`      AS `requestedStartDate`,
  `app`.`archived_from`             AS `archivedFrom`,
  3                                 AS `listType`,
  3                                 AS `sort`
FROM (`tbl_application_list` `app`
   LEFT JOIN `tbl_user` `u`
     ON ((`app`.`user_id` = `u`.`id`)))
WHERE ((`app`.`delete_flag` = 0)
       AND (`app`.`status` = 3))
ORDER BY `app`.`application_date_position`$$

DELIMITER ;