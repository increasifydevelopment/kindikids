-- MySQL dump 10.13  Distrib 5.7.12, for Linux (x86_64)
--
-- Host: localhost    Database: kindkids_dev
-- ------------------------------------------------------
-- Server version	5.6.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_account`
--

DROP TABLE IF EXISTS `tbl_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_account` (
  `id` varchar(50) NOT NULL,
  `account` varchar(255) DEFAULT NULL,
  `psw` varchar(50) DEFAULT NULL,
  `status` smallint(6) DEFAULT '0' COMMENT '0禁用,1启用',
  `delete_flag` smallint(6) DEFAULT '0' COMMENT '0未删除,1已删除,',
  `create_time` datetime DEFAULT NULL,
  `create_account_id` varchar(50) DEFAULT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT NULL,
  `update_account_id` varchar(50) DEFAULT NULL COMMENT '修改人',
  `user_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_account`
--

LOCK TABLES `tbl_account` WRITE;
/*!40000 ALTER TABLE `tbl_account` DISABLE KEYS */;
INSERT INTO `tbl_account` VALUES ('111','bqbao@aoyuntek.cn','c4ca4238a0b923820dcc509a6f75849b',0,0,NULL,NULL,NULL,NULL,'2'),('11111','hxzhang@aoyuntek.cn','b59c67bf196a4758191e42f76670ceba',0,0,NULL,NULL,NULL,NULL,'3'),('12','dev@increasify.com.au','c4ca4238a0b923820dcc509a6f75849b',0,0,NULL,NULL,NULL,NULL,NULL),('222','longchen@aoyuntek.cn','c4ca4238a0b923820dcc509a6f75849b',0,0,NULL,NULL,NULL,NULL,'1');
/*!40000 ALTER TABLE `tbl_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_account_role`
--

DROP TABLE IF EXISTS `tbl_account_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_account_role` (
  `id` varchar(50) NOT NULL,
  `account_id` varchar(50) DEFAULT NULL,
  `role_id` varchar(50) DEFAULT NULL,
  `delete_flag` smallint(6) DEFAULT '0' COMMENT '1已删除,0未删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_account_id` varchar(50) DEFAULT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `update_account_id` varchar(50) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_account_role`
--

LOCK TABLES `tbl_account_role` WRITE;
/*!40000 ALTER TABLE `tbl_account_role` DISABLE KEYS */;
INSERT INTO `tbl_account_role` VALUES ('1','111','1',0,NULL,NULL,NULL,NULL),('2','111','2',0,NULL,NULL,NULL,NULL),('3','222','2',0,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `tbl_account_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_change_psw_record`
--

DROP TABLE IF EXISTS `tbl_change_psw_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_change_psw_record` (
  `id` varchar(50) NOT NULL,
  `token` varchar(200) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `status` smallint(1) DEFAULT NULL COMMENT '0未使用，1已使用',
  `account_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_change_psw_record`
--

LOCK TABLES `tbl_change_psw_record` WRITE;
/*!40000 ALTER TABLE `tbl_change_psw_record` DISABLE KEYS */;
INSERT INTO `tbl_change_psw_record` VALUES ('061319e3-e1a8-430f-834b-01316243bade','290ca19f-9050-4d41-b76e-2e3664ed2387','2016-06-17 12:14:41',0,'222'),('18ebdfab-0523-4f6d-9583-ff93706b0e8e','ded01833-79a4-4aa0-bfb6-c014caf4b921','2016-06-17 12:14:41',0,'222'),('2d413b9f-d2d3-48f9-8591-dd8b607dc8ca','a3e7f1e8-8a4b-4f5e-a40f-10a74e3e8b83','2016-06-17 12:14:44',0,'222'),('3cb8dba1-e32b-4963-8cf8-092831468c49','68802ff7-dd90-4697-8d9b-d294a609a9db','2016-06-17 09:51:16',1,'11111'),('3cca9f1f-e649-4885-b9ee-dfe858efa08e','8e5a935e-7139-4a71-9f5c-cec93d8f7476','2016-06-17 12:14:44',0,'222'),('4942e60c-f2fe-40bb-aebf-8f3d60fde960','c75877cd-c557-4104-b2f9-e0191b4ca3ef','2016-06-17 10:51:43',1,'123'),('4aea601f-0f02-4af1-8880-79a536cac2ee','c7e9d681-592e-4627-a30b-29998eeabaed','2016-06-17 11:20:27',1,'222'),('4d0d686b-446a-4a01-a5d0-9b82c4504ff6','4b3f9ea0-c417-4053-91d1-d766f556c2e3','2016-06-17 09:48:50',0,'11111'),('75aad834-8ae5-46b1-b6ae-25485bb9ab9e','b7fb1001-eb95-413e-8e36-50d8a28f9c6b','2016-06-17 09:43:24',0,'11111'),('7631b7a0-79c0-4ce3-9693-d749e571128f','1efcc76a-6117-4b8c-afda-46bbcca277cb','2016-06-17 09:44:53',1,'11111'),('776cbbb8-cd1e-4bb5-85bf-a6e9b3cc402c','1af05d4d-394f-43f6-9e80-e18185844cdc','2016-06-17 10:01:03',0,'222'),('9704e6da-7f87-4aa2-ae1e-79fe0fc5e8dc','e3a0f70b-2cc0-45a7-8920-43023a8a02c7','2016-06-17 12:15:42',0,'222'),('9c35b44b-2dc4-4bb0-b752-6c768a5fe0ad','51a5d0cd-20bd-4ecf-b849-519259206430','2016-06-17 11:02:58',1,'222'),('a2d68a00-9ae2-4019-9815-86aa61caaca0','698659c8-933c-4acf-8c0b-ac18e3aab8cb','2016-06-17 11:31:17',1,'222'),('aac43eac-83f3-41fd-a0bf-ebd578ed54bb','fb58dabb-ce9a-402b-9914-7d098442382c','2016-06-17 12:13:52',0,'222'),('be093bca-f77a-4bd0-b062-dcf5637b7a5e','d0d87e90-96cc-4179-8253-e829472be14b','2016-06-17 11:23:07',1,'222'),('bfcd3fd1-401c-4fa3-978c-167977e15460','a3758716-d526-4014-84f0-076f781f2a53','2016-06-17 12:13:52',0,'222'),('c5831f53-33c9-4105-8e70-daa823670363','6d4be828-85ee-47a9-ac69-7cc9f5d85808','2016-06-17 10:23:15',1,'222'),('d293ecb7-cf8b-4fee-8c14-280238a2b704','a3fe33e2-c438-499c-b3a9-f328b755c4b8','2016-06-17 11:25:40',1,'222'),('d4c973b2-ed38-4f61-b318-d4dcf6f674d9','4fa51c1a-413d-4966-a2fe-3fc84b245f4e','2016-06-17 10:21:52',1,'222'),('f0726fdd-4d12-4786-83af-bdd15bc5a40b','49553124-d85b-40ea-bc6e-c13796407c00','2016-06-17 10:50:44',1,'123'),('f9e191b5-a1cd-4d91-828b-1352c5640922','2d390cdb-11c4-4fdd-9314-9a64c5bc3ddc','2016-06-17 12:24:13',0,'222');
/*!40000 ALTER TABLE `tbl_change_psw_record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_function`
--

DROP TABLE IF EXISTS `tbl_function`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_function` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL COMMENT '功能点名称',
  `target` varchar(200) DEFAULT NULL COMMENT '功能点所指代的字符串',
  `menu_id` int(11) DEFAULT NULL COMMENT '指定属于某个模块下的功能点',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_function`
--

LOCK TABLES `tbl_function` WRITE;
/*!40000 ALTER TABLE `tbl_function` DISABLE KEYS */;
INSERT INTO `tbl_function` VALUES (1,'更新User信息','update',3),(2,'添加用户按钮','toAdd',4),(3,'保存用户按钮','add',5),(4,'create new project','toAdd',6),(5,'保存项目信息','add',7),(6,'更新项目信息','update',8),(7,'添加项目成员弹出框','toAdd',9),(8,'移除项目人员','remove',9),(9,'创建build','toCreateBuilding',10),(10,'创建unit','toCreateUnit',10),(11,'删除','remove',10),(12,'编辑','toEdit',10),(13,'edit','taskEdit',11),(14,'edit','date',11),(15,'保存','save',12);
/*!40000 ALTER TABLE `tbl_function` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_menu`
--

DROP TABLE IF EXISTS `tbl_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(200) DEFAULT NULL COMMENT '菜单所访问的URI',
  `parent_id` varchar(50) DEFAULT NULL COMMENT '该菜单的父级菜单ID',
  `sort` smallint(6) DEFAULT NULL COMMENT '表明menu的模块',
  `level` smallint(6) DEFAULT NULL,
  `active_flag` smallint(6) DEFAULT NULL,
  `delete_flag` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_menu`
--

LOCK TABLES `tbl_menu` WRITE;
/*!40000 ALTER TABLE `tbl_menu` DISABLE KEYS */;
INSERT INTO `tbl_menu` VALUES (1,'Site Diary','','',0,0,0,0),(2,'Finishes Matrix','','',1,0,0,0),(3,'Edit User Profile','/user/profile/?','',0,0,0,0),(4,'Alliance Group Users','/user/team','',0,0,0,0),(5,'Add User Profile','/user/profile/0','',0,0,0,0),(6,'Projects','/project/team','',0,0,0,0),(7,'Add Project details','/project/profile/0','',0,0,0,0),(8,'Edit Project details','/project/profile/?','',0,0,0,0),(9,'Team Members','/project/user/?','',0,0,0,0),(10,'project build','/project/buildings','',0,0,0,0),(11,'finishMatrix','/finishMatrix/building/?','',0,0,0,0),(12,'project task','/project/task/?','',0,0,0,0);
/*!40000 ALTER TABLE `tbl_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_role`
--

DROP TABLE IF EXISTS `tbl_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_role` (
  `id` varchar(50) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `value` smallint(6) DEFAULT NULL,
  `delete_flag` smallint(6) DEFAULT '0' COMMENT '1已删除,0未删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_account_id` varchar(50) DEFAULT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `update_account_id` varchar(50) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_role`
--

LOCK TABLES `tbl_role` WRITE;
/*!40000 ALTER TABLE `tbl_role` DISABLE KEYS */;
INSERT INTO `tbl_role` VALUES ('1','CEO',0,0,NULL,NULL,NULL,NULL),('2','KinderGartenLeader',1,0,NULL,NULL,NULL,NULL),('3','Teacher',2,0,NULL,NULL,NULL,NULL),('4','PartTimeTeacher',3,0,NULL,NULL,NULL,NULL),('5','Parent',4,0,NULL,NULL,NULL,NULL),('6','Kitchener',5,0,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `tbl_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_role_function`
--

DROP TABLE IF EXISTS `tbl_role_function`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_role_function` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `role_id` int(11) DEFAULT NULL COMMENT '功能点',
  `function_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_role_function`
--

LOCK TABLES `tbl_role_function` WRITE;
/*!40000 ALTER TABLE `tbl_role_function` DISABLE KEYS */;
INSERT INTO `tbl_role_function` VALUES (1,0,1),(2,1,1),(3,2,1),(4,0,2),(5,0,3),(6,0,4),(7,0,5),(8,0,6),(10,0,7),(11,1,7),(12,0,8),(13,1,8),(14,0,9),(15,1,9),(16,0,10),(17,1,10),(18,0,11),(19,1,11),(20,0,12),(21,1,12),(22,1,13),(23,0,13),(24,1,14),(25,0,14),(26,0,15),(27,1,15);
/*!40000 ALTER TABLE `tbl_role_function` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_role_menu`
--

DROP TABLE IF EXISTS `tbl_role_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_role_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_role_menu`
--

LOCK TABLES `tbl_role_menu` WRITE;
/*!40000 ALTER TABLE `tbl_role_menu` DISABLE KEYS */;
INSERT INTO `tbl_role_menu` VALUES (1,0,1),(2,0,2),(3,0,3),(4,1,3),(5,2,3);
/*!40000 ALTER TABLE `tbl_role_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_user`
--

DROP TABLE IF EXISTS `tbl_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_user` (
  `id` varchar(50) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `delete_flag` smallint(6) DEFAULT '0' COMMENT '1已删除,0未删除',
  `create_time` datetime DEFAULT NULL,
  `create_account_id` varchar(50) DEFAULT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT NULL,
  `update_account_id` varchar(50) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_user`
--

LOCK TABLES `tbl_user` WRITE;
/*!40000 ALTER TABLE `tbl_user` DISABLE KEYS */;
INSERT INTO `tbl_user` VALUES ('1','longchen@aoyuntek.cn',0,NULL,NULL,NULL,NULL),('2','bqbao@aoyuntek.cn',0,NULL,NULL,NULL,NULL),('3','hxzhang@aoyuntek.cn',0,NULL,NULL,NULL,NULL),('4','dev@increasify.com.au',0,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `tbl_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'kindkids_dev'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-06-22 14:12:34
