/*
Navicat MySQL Data Transfer

Source Server         : 192.168.1.3
Source Server Version : 50627
Source Host           : 192.168.1.3:3306
Source Database       : kindkids_dev

Target Server Type    : MYSQL
Target Server Version : 50627
File Encoding         : 65001

Date: 2018-04-23 16:10:43
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_emergency_signature
-- ----------------------------
DROP TABLE IF EXISTS `tbl_emergency_signature`;
CREATE TABLE `tbl_emergency_signature` (
  `id` varchar(50) DEFAULT NULL,
  `user_id` varchar(50) DEFAULT NULL,
  `emergency_signature` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
