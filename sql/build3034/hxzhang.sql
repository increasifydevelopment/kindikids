DELIMITER $$

DROP VIEW IF EXISTS `view_waiting_child_center`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `view_waiting_child_center` AS 
SELECT
  `app`.`id`      AS `obj_id`,
  `o`.`center_id` AS `center_id`,
  `c`.`name`      AS `name`
FROM ((`tbl_application_list` `app`
    JOIN `tbl_out_child_center` `o`
      ON ((`app`.`id` = `o`.`child_user_id`)))
   LEFT JOIN `tbl_centers` `c`
     ON ((`o`.`center_id` = `c`.`id`)))
WHERE (`app`.`status` IN(0,3))UNION ALL SELECT
                                          `app`.`id`       AS `obj_id`,
                                          `o`.`center_id`  AS `center_id`,
                                          `c`.`name`       AS `name`
                                        FROM ((`tbl_application_list` `app`
                                            JOIN `tbl_out_child_center` `o`
                                              ON ((`app`.`user_id` = `o`.`child_user_id`)))
                                           LEFT JOIN `tbl_centers` `c`
                                             ON ((`o`.`center_id` = `c`.`id`)))
                                        WHERE (`app`.`status` = 3)UNION ALL SELECT
                                                                              `u`.`id`          AS `obj_id`,
                                                                              `o`.`center_id`   AS `center_id`,
                                                                              `c`.`name`        AS `name`
                                                                            FROM ((((`tbl_user` `u`
                                                                                  LEFT JOIN `tbl_child_attendance` `ca`
                                                                                    ON ((`u`.`id` = `ca`.`user_id`)))
                                                                                 LEFT JOIN `tbl_account` `a`
                                                                                   ON ((`u`.`id` = `a`.`user_id`)))
                                                                                LEFT JOIN `tbl_out_child_center` `o`
                                                                                  ON ((`u`.`id` = `o`.`child_user_id`)))
                                                                               LEFT JOIN `tbl_centers` `c`
                                                                                 ON ((`o`.`center_id` = `c`.`id`)))
                                                                            WHERE ((`u`.`delete_flag` = 0)
                                                                                   AND (`ca`.`delete_flag` = 0)
                                                                                   AND (`ca`.`enrolled` = 0)
                                                                                   AND (`ca`.`type` = 1)
                                                                                   AND (`a`.`status` <> 1))$$

DELIMITER ;




SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_previous_enrolment
-- ----------------------------
DROP TABLE IF EXISTS `tbl_previous_enrolment`;
CREATE TABLE `tbl_previous_enrolment` (
  `id` varchar(50) NOT NULL,
  `application_date` datetime DEFAULT NULL,
  `requested_date` datetime DEFAULT NULL,
  `orientation_date` datetime DEFAULT NULL,
  `first_day` datetime DEFAULT NULL,
  `requested_last_day` datetime DEFAULT NULL,
  `last_day` datetime DEFAULT NULL,
  `user_id` varchar(50) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `enrolled` smallint(6) DEFAULT NULL,
  `type` smallint(6) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `delete_flag` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_previous_enrolment_relation
-- ----------------------------
DROP TABLE IF EXISTS `tbl_previous_enrolment_relation`;
CREATE TABLE `tbl_previous_enrolment_relation` (
  `obj_id` varchar(50) DEFAULT NULL,
  `previous_enrolment_id` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

