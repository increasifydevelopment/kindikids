UPDATE
	tbl_account
SET `status` = 1
WHERE
	user_id IN (
		select a.id FROM (SELECT
			id
		FROM
			tbl_user
		WHERE
			family_id IN (
				SELECT
					family_id
				FROM
					tbl_user
				WHERE
					id IN (
						SELECT
							user_id
						FROM
							tbl_account
						WHERE
							`status` = 1
					)
				AND user_type = 2
			)
		AND user_type = 1) a
	)
AND `status` = - 1;