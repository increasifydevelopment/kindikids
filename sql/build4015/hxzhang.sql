DELIMITER $$

ALTER ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_user_query` AS (
SELECT
  CONCAT(IF((IFNULL(`u`.`first_name`,'') = ''),'',CONCAT(`u`.`first_name`,' ')),IF((IFNULL(`u`.`middle_name`,'') = ''),'',CONCAT(`u`.`middle_name`,' ')),IF((IFNULL(`u`.`last_name`,'') = ''),'',CONCAT(`u`.`last_name`,' '))) AS `name`,
  `u`.`email`         AS `email`,
  `u`.`centers_id`    AS `centers_id`,
  `u`.`family_id`     AS `family_id`,
  `u`.`user_type`     AS `user_type`,
  `u`.`id`            AS `user_id`,
  `u`.`room_id`       AS `room_id`,
  `u`.`group_id`      AS `group_id`,
  `u`.`avatar`        AS `avatar`,
  `u`.`create_time`   AS `create_time`,
  `u`.`mobile_number` AS `mobile_number`,
  `a`.`id`            AS `account_id`,
  `a`.`account`       AS `account`,
  `a`.`status`        AS `status`,
  `r`.`value`         AS `value`,
  `r`.`name`          AS `role_name`,
  `ar`.`temp_flag`    AS `temp_flag`,
  `u`.`person_color`  AS `person_color`,
  `u`.`birthday`      AS `birthday`,
  `u`.`staff_type`    AS `staff_type`,
  `se`.`region`       AS `region`
FROM ((((`tbl_user` `u`
      LEFT JOIN `tbl_account` `a`
        ON (((`u`.`id` = `a`.`user_id`)
             AND (`a`.`delete_flag` = 0))))
     LEFT JOIN `tbl_account_role` `ar`
       ON (((`a`.`id` = `ar`.`account_id`)
            AND (`ar`.`delete_flag` = 0))))
    LEFT JOIN `tbl_role` `r`
      ON (((`ar`.`role_id` = `r`.`id`)
           AND (`r`.`delete_flag` = 0))))
   LEFT JOIN `tbl_staff_employment` `se`
     ON (((`se`.`account_id` = `a`.`id`)
          AND (`se`.`current` = 1))))
WHERE (`u`.`delete_flag` = 0))$$

DELIMITER ;