/*
Navicat MySQL Data Transfer

Source Server         : 192.168.1.18
Source Server Version : 50636
Source Host           : 192.168.1.18:3306
Source Database       : kindikids_dev

Target Server Type    : MYSQL
Target Server Version : 50636
File Encoding         : 65001

Date: 2019-04-03 16:38:29
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_child_interim_attendance
-- ----------------------------
DROP TABLE IF EXISTS `tbl_child_interim_attendance`;
CREATE TABLE `tbl_child_interim_attendance` (
  `id` varchar(50) NOT NULL,
  `account_id` varchar(50) DEFAULT NULL,
  `day` datetime DEFAULT NULL,
  `center_id` varchar(50) DEFAULT NULL,
  `room_id` varchar(50) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `create_account_id` varchar(50) DEFAULT NULL,
  `delete_flag` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
