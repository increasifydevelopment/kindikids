drop table if exists tbl_leave_paid;

/*==============================================================*/
/* Table: tbl_leave_paid                                        */
/*==============================================================*/
create table tbl_leave_paid
(
   id                   varchar(50) not null,
   leave_id             varchar(50),
   leave_start_date     datetime comment '开始时间',
   leave_end_date       datetime comment '结束时间',
   paid_flag            smallint(6) comment '是否支付1:paid,0:unpaid',
   delete_flag          smallint(6),
   primary key (id)
);