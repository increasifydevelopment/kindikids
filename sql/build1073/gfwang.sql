ALTER TABLE tbl_program_lesson MODIFY resources LONGTEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE tbl_program_lesson  CHARSET=utf8mb4;


INSERT INTO tbl_function (
	id,
	NAME,
	target,
	menu_id,
	function_type
)
VALUES
	(
		'e0ac9460-ce29-11e6-ae66-00e0703507b4',
		'account_not_sign_out',
		'/account/showNotSignOutTip.do',
		'9549abe4-530a-11e6-9746-12edsa864123',
		1
	),('a43d4f67-ce2b-11e6-ae66-00e0703507b4','child_sign_help','/rosterstaff/getRosterStaffForToday.do','c27d4ffd-7994-11e6-85fd-00e0703507b4',1);

INSERT into tbl_role_function (id,role_id,function_id) values
('76e22387-ce2a-11e6-ae66-00e0703507b4','2e083440-49a3-11e6-b19e-00e0703507b4','e0ac9460-ce29-11e6-ae66-00e0703507b4'),
('cb0cb883-ce2a-11e6-ae66-00e0703507b4','4586aa2b-49a3-11e6-b19e-00e0703507b4','e0ac9460-ce29-11e6-ae66-00e0703507b4'),
('9babbd93-ce2a-11e6-ae66-00e0703507b4','51c32196-49a3-11e6-b19e-00e0703507b4','e0ac9460-ce29-11e6-ae66-00e0703507b4'),
('73cb5246-ce2b-11e6-ae66-00e0703507b4','74ba21a6-49a3-11e6-b19e-00e0703507b4','e0ac9460-ce29-11e6-ae66-00e0703507b4'),

('09fbece6-ce2c-11e6-ae66-00e0703507b4','2e083440-49a3-11e6-b19e-00e0703507b4','a43d4f67-ce2b-11e6-ae66-00e0703507b4'),
('0e709c07-ce2c-11e6-ae66-00e0703507b4','4586aa2b-49a3-11e6-b19e-00e0703507b4','a43d4f67-ce2b-11e6-ae66-00e0703507b4'),
('14216454-ce2c-11e6-ae66-00e0703507b4','60a193d5-49a3-11e6-b19e-00e0703507b4','a43d4f67-ce2b-11e6-ae66-00e0703507b4');




