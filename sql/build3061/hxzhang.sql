DELIMITER $$

ALTER ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_email_list_view` AS 
SELECT
  `p`.`id`              AS `id`,
  `u`.`family_id`       AS `familyId`,
  CONCAT(IF((IFNULL(`u`.`first_name`,'') = ''),'',CONCAT(`u`.`first_name`,' ')),IF((IFNULL(`u`.`middle_name`,'') = ''),'',CONCAT(`u`.`middle_name`,' ')),IF((IFNULL(`u`.`last_name`,'') = ''),'',CONCAT(`u`.`last_name`,' '))) AS `childName`,
  `p`.`sub`             AS `email`,
  `c`.`name`            AS `centre`,
  `r`.`name`            AS `room`,
  `f`.`family_name`     AS `familyName`,
  `p`.`receiverAddress` AS `receiverEmail`,
  `p`.`receiverName`    AS `receiverName`,
  `p`.`create_time`     AS `createTime`,
  `p`.`status`          AS `status`,
  `p`.`delete_flag`     AS `deleteFlag`
FROM ((((`tbl_email_plan` `p`
      LEFT JOIN `tbl_user` `u`
        ON ((`p`.`user_id` = `u`.`id`)))
     LEFT JOIN `tbl_centers` `c`
       ON ((`u`.`centers_id` = `c`.`id`)))
    LEFT JOIN `tbl_room` `r`
      ON ((`u`.`room_id` = `r`.`id`)))
   LEFT JOIN `tbl_family` `f`
     ON ((`u`.`family_id` = `f`.`id`)))$$

DELIMITER ;

ALTER TABLE tbl_user ADD crn varchar(50);
ALTER TABLE tbl_user ADD hubwork_id varchar(50);

INSERT INTO tbl_function (
id,
NAME,
target,
menu_id,
function_type
)
VALUES
(
'711ef281-0b36-11e9-b05f-408d5c9798a2',
'job_syncHubworks',
'/job/syncHubworks.do',
'3ea7375f-7890-11e6-85fd-00e0703507b4',
2
);



INSERT INTO tbl_role_function (id, role_id, function_id)
VALUES
(
'800088ce-0b36-11e9-b05f-408d5c9798a2',
'2e083440-49a3-11e6-b19e-00e0703507b4',
'711ef281-0b36-11e9-b05f-408d5c9798a2'
),
(
'84963d09-0b36-11e9-b05f-408d5c9798a2',
'4586aa2b-49a3-11e6-b19e-00e0703507b4',
'711ef281-0b36-11e9-b05f-408d5c9798a2'
),
(
'8b596efd-0b36-11e9-b05f-408d5c9798a2',
'60a193d5-49a3-11e6-b19e-00e0703507b4',
'711ef281-0b36-11e9-b05f-408d5c9798a2'
);

INSERT INTO tbl_function (
id,
NAME,
target,
menu_id,
function_type
)
VALUES
(
'5faeee52-0f00-11e9-b05f-408d5c9798a2',
'job_syncChildAttend',
'/job/syncChildAttend.do',
'3ea7375f-7890-11e6-85fd-00e0703507b4',
2
);



INSERT INTO tbl_role_function (id, role_id, function_id)
VALUES
(
'6a8548ea-0f00-11e9-b05f-408d5c9798a2',
'2e083440-49a3-11e6-b19e-00e0703507b4',
'5faeee52-0f00-11e9-b05f-408d5c9798a2'
),
(
'71affc8d-0f00-11e9-b05f-408d5c9798a2',
'4586aa2b-49a3-11e6-b19e-00e0703507b4',
'5faeee52-0f00-11e9-b05f-408d5c9798a2'
),
(
'759a189d-0f00-11e9-b05f-408d5c9798a2',
'60a193d5-49a3-11e6-b19e-00e0703507b4',
'5faeee52-0f00-11e9-b05f-408d5c9798a2'
);
