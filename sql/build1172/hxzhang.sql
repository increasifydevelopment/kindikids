UPDATE tbl_request_log SET log_type = 0 WHERE obj_id IN (SELECT id FROM tbl_giving_notice);

UPDATE tbl_request_log SET log_type = 1 WHERE obj_id IN (SELECT id FROM tbl_absentee_request);

UPDATE tbl_request_log SET log_type = 2 WHERE obj_id IN (SELECT id FROM tbl_change_attendance_request where delete_flag = FALSE AND type = 1);

UPDATE tbl_request_log SET log_type = 3 WHERE obj_id IN (SELECT id FROM tbl_change_attendance_request where delete_flag = FALSE AND type = 2);

UPDATE tbl_request_log SET log_type = 4 WHERE obj_id IN (SELECT id FROM tbl_change_attendance_request where delete_flag = FALSE AND type = 0);