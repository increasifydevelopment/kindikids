UPDATE tbl_child_attendance SET enrolled_flag = TRUE WHERE enrolled = 0 and type = 0;

INSERT INTO tbl_function (
	id,
	NAME,
	target,
	menu_id,
	function_type
)
VALUES
(
	'b0c7c462-6fe2-11e8-a955-00e0703507b4',
	'waiting_sendPlanEmail.do',
	'/waiting/sendPlanEmail.do',
	'fe85639f-54ed-11e8-8348-00e0703507b4',
	2
);

INSERT INTO tbl_role_function (id, role_id, function_id)
VALUES
(
	'c305ce04-6fe2-11e8-a955-00e0703507b4',
	'2e083440-49a3-11e6-b19e-00e0703507b4',
	'b0c7c462-6fe2-11e8-a955-00e0703507b4'
),
(
	'c862b4e9-6fe2-11e8-a955-00e0703507b4',
	'4586aa2b-49a3-11e6-b19e-00e0703507b4',
	'b0c7c462-6fe2-11e8-a955-00e0703507b4'
),
(
	'cc92e7a4-6fe2-11e8-a955-00e0703507b4',
	'60a193d5-49a3-11e6-b19e-00e0703507b4',
	'b0c7c462-6fe2-11e8-a955-00e0703507b4'
);



SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_email_plan
-- ----------------------------
DROP TABLE IF EXISTS `tbl_email_plan`;
CREATE TABLE `tbl_email_plan` (
  `id` varchar(50) DEFAULT NULL,
  `senderAddress` varchar(500) DEFAULT NULL,
  `senderName` varchar(500) DEFAULT NULL,
  `receiverAddress` varchar(500) DEFAULT NULL,
  `receiverName` varchar(100) DEFAULT NULL,
  `sub` varchar(2000) DEFAULT NULL,
  `msg` text,
  `create_time` datetime DEFAULT NULL,
  `obj_id` varchar(50) DEFAULT NULL,
  `type` smallint(6) DEFAULT NULL,
  `delete_flag` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
