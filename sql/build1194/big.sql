DELIMITER $$

DROP VIEW IF EXISTS `v_room_change`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_room_change` AS (
SELECT
  IF((`cc`.`create_time` IS NOT NULL),`cc`.`create_time`,IF((`sub`.`create_time` IS NOT NULL),`sub`.`create_time`,`tem`.`create_time`)) AS `change_date`,
  `c`.`centre_id`   AS `centre_id`,
  `c`.`room_id`     AS `room_id`,
  `c`.`child_id`    AS `child_id`,
  `c`.`req_id`      AS `req_id`,
  `c`.`day_week`    AS `day_week`,
  `c`.`operate`     AS `operate`,
  `c`.`day`         AS `day`,
  `c`.`delete_flag` AS `delete_flag`
FROM (((`tbl_room_change` `c`
     LEFT JOIN `tbl_change_attendance_request` `cc`
       ON ((`cc`.`id` = `c`.`req_id`)))
    LEFT JOIN `tbl_subtracted_attendance` `sub`
      ON ((`sub`.`id` = `c`.`req_id`)))
   LEFT JOIN `tbl_temporary_attendance` `tem`
     ON ((`tem`.`id` = `c`.`req_id`))))$$

DELIMITER ;