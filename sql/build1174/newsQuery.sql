DELIMITER $$

ALTER ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_news_visibility` AS 
SELECT
  `n`.`id`                 AS `id`,
  `n`.`create_account_id`  AS `create_account_id`,
  `n`.`centers_id`         AS `centers_id`,
  `n`.`status`             AS `status`,
  `n`.`delete_flag`        AS `delete_flag`,
  `n`.`approve_time`       AS `approve_time`,
  `n`.`update_time`        AS `update_time`,
  `n`.`news_type`          AS `news_type`,
  `n`.`score`              AS `score`,
  `n`.`create_time`        AS `create_time`,
  `n`.`room_id`            AS `room_id`,
  `r`.`receive_account_id` AS `receive_account_id`
FROM (`tbl_news` `n`
   LEFT JOIN `tbl_news_receive` `r`
     ON (((`n`.`id` = `r`.`news_id`)
          AND (`r`.`delete_flag` = 0))))$$

DELIMITER ;






DELIMITER $$

ALTER ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_news_tag` AS 
SELECT
  `n`.`id`                AS `id`,
  `n`.`create_account_id` AS `create_account_id`,
  `n`.`centers_id`        AS `centers_id`,
  `n`.`status`            AS `status`,
  `n`.`delete_flag`       AS `delete_flag`,
  `n`.`approve_time`      AS `approve_time`,
  `n`.`update_time`       AS `update_time`,
  `n`.`news_type`         AS `news_type`,
  `n`.`score`             AS `score`,
  `n`.`create_time`       AS `create_time`,
  `n`.`room_id`           AS `room_id`,
  `a`.`account_id`        AS `account_id`
FROM (`tbl_news` `n`
   LEFT JOIN `tbl_news_accounts` `a`
     ON (((`n`.`id` = `a`.`news_id`)
          AND (`a`.`delete_flag` = 0))))$$

DELIMITER ;


DELIMITER $$

ALTER ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_news_group` AS 
SELECT
  `n`.`id`                AS `id`,
  `n`.`create_account_id` AS `create_account_id`,
  `n`.`centers_id`        AS `centers_id`,
  `n`.`status`            AS `status`,
  `n`.`delete_flag`       AS `delete_flag`,
  `n`.`approve_time`      AS `approve_time`,
  `n`.`update_time`       AS `update_time`,
  `n`.`news_type`         AS `news_type`,
  `n`.`score`             AS `score`,
  `n`.`create_time`       AS `create_time`,
  `n`.`room_id`           AS `room_id`,
  `g`.`group_name`        AS `group_id`,
  `g`.`group_center_id`   AS `group_center_id`
FROM (`tbl_news` `n`
   LEFT JOIN `tbl_news_group` `g`
     ON (((`n`.`id` = `g`.`news_id`)
          AND (`g`.`delete_flag` = 0))))$$

DELIMITER ;