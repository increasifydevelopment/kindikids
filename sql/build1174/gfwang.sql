
alter TABLE tbl_news_group add COLUMN group_center_id VARCHAR(50);
CREATE INDEX Idx_center_id ON tbl_news_group (group_center_id);
CREATE INDEX Idx_news_type ON tbl_news (news_type);


UPDATE tbl_news_group n
SET group_center_id = (
	SELECT
		o.center_id
	FROM
		(SELECT DISTINCT auth_group_id,center_id from tbl_role_group_relation where type=1 and  center_id is NOT NULL) o
	WHERE
		o.auth_group_id = n.group_name 
)
WHERE
	EXISTS (
		SELECT
1
		FROM
			(SELECT DISTINCT auth_group_id,center_id from tbl_role_group_relation  where type=1 and  center_id is NOT NULL) o
		WHERE
			n.group_name= o.auth_group_id 
	);

UPDATE tbl_authority_group set sql_tag=REPLACE(sql_tag,') t WHERE 1 = 1 and t.role_value  in (4)',' and u.user_type=0 ) t WHERE 1 = 1 and t.role_value  in (4)') where id='bad448f2-7898-11e6-85fd-00e0703507b4';

UPDATE tbl_authority_group set sql_tag=REPLACE(sql_tag,') t WHERE 1 = 1 and  t.role_value  in (9)',' and u.user_type=0 ) t WHERE 1 = 1 and  t.role_value  in (9)') where id='f66a1800-7898-11e6-85fd-00e0703507b4';

UPDATE tbl_authority_group set sql_tag=REPLACE(sql_tag,') t WHERE 1 = 1 and t.role_value =2 and t.centers_id',' AND user_type = 0 AND r.`value` = 2 ) t WHERE 1 = 1 and t.role_value =2 and t.centers_id') where sql_tag like '%) t WHERE 1 = 1 and t.role_value =2 and t.centers_id%';
  
UPDATE tbl_authority_group set sql_tag=REPLACE(sql_tag,') t WHERE 1 = 1 and t.role_value  in (0)',' AND u.user_type = 0 ) t WHERE 1 = 1 and t.role_value  in (0)') where id = 'bac9fb7e-7898-11e6-85fd-00e0703507b4';
  
UPDATE tbl_authority_group set sql_tag=REPLACE(sql_tag,') t WHERE 1 = 1 and t.role_value  in (1,5)',' AND u.user_type = 0 ) t WHERE 1 = 1 and t.role_value  in (1,5)') where id = 'bacdb137-7898-11e6-85fd-00e0703507b4';
