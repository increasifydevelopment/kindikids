INSERT INTO tbl_function (
	id,
	NAME,
	target,
	menu_id,
	function_type
)
VALUES
	(
		'ddafe6ff-4764-11e7-81c3-00e0703507b4',
		'center_check',
		'/center/check.do',
		'3ea7375f-7890-11e6-85fd-00e0703507b4',
		2
	);



INSERT INTO tbl_role_function (id, role_id, function_id)
VALUES
	(
		'eb6feeb5-4764-11e7-81c3-00e0703507b4',
		'2e083440-49a3-11e6-b19e-00e0703507b4',
		'ddafe6ff-4764-11e7-81c3-00e0703507b4'
	),
	(
		'f1367e7f-4764-11e7-81c3-00e0703507b4',
		'4586aa2b-49a3-11e6-b19e-00e0703507b4',
		'ddafe6ff-4764-11e7-81c3-00e0703507b4'
	),
	(
		'f8217155-4764-11e7-81c3-00e0703507b4',
		'60a193d5-49a3-11e6-b19e-00e0703507b4',
		'ddafe6ff-4764-11e7-81c3-00e0703507b4'
	);
	
	
	
	
	
	
	
	/*
Navicat MySQL Data Transfer

Source Server         : 192.168.1.3
Source Server Version : 50627
Source Host           : 192.168.1.3:3306
Source Database       : kindkids_dev

Target Server Type    : MYSQL
Target Server Version : 50627
File Encoding         : 65001

Date: 2017-06-02 17:07:13
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_room_task_check
-- ----------------------------
DROP TABLE IF EXISTS `tbl_room_task_check`;
CREATE TABLE `tbl_room_task_check` (
  `id` varchar(50) NOT NULL,
  `room_id` varchar(50) DEFAULT NULL,
  `task_type` smallint(2) DEFAULT NULL,
  `check_flag` bit(1) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `delete_flag` smallint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET FOREIGN_KEY_CHECKS=1;
