DELIMITER $$

DROP VIEW IF EXISTS `v_temp_roster_center`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_temp_roster_center` AS (
SELECT
  `trc`.`roster_staff_id` AS `roster_staff_id`,
  `trc`.`centre_id`       AS `centre_id`,
  `trc`.`role_id`         AS `role_id`,
  `trc`.`delete_falg`     AS `delete_falg`,
  `trc`.`state`           AS `state`,
  IF((`rs`.`shift_time_code` IS NOT NULL),STR_TO_DATE(CONCAT(DATE_FORMAT(`rs`.`day`,'%Y-%m-%d'),DATE_FORMAT(`rst`.`shift_start_time`,'%H:%i:%s')),'%Y-%m-%d%H:%i:%s'),`rs`.`start_time`) AS `start_time`,
  IF((`rs`.`shift_time_code` IS NOT NULL),STR_TO_DATE(CONCAT(DATE_FORMAT(`rs`.`day`,'%Y-%m-%d'),DATE_FORMAT(`rst`.`shift_end_time`,'%H:%i:%s')),'%Y-%m-%d%H:%i:%s'),`rs`.`end_time`) AS `end_time`
FROM ((`tbl_temp_roster_center` `trc`
    JOIN `tbl_roster_staff` `rs`
      ON ((`rs`.`id` = `trc`.`roster_staff_id`)))
   LEFT JOIN `tbl_roster_shift_time` `rst`
     ON (((`rst`.`code` = `rs`.`shift_time_code`)
          AND (`rst`.`delete_flag` = 0)))))$$

DELIMITER ;