alter table tbl_email_plan add status smallint;

UPDATE tbl_email_plan SET STATUS = 0 WHERE delete_flag = FALSE;


DELIMITER $$
DROP VIEW IF EXISTS `v_email_list_view`$$
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_email_list_view` AS 
SELECT
  `p`.`id`              AS `id`,
  CONCAT(IF((IFNULL(`u`.`first_name`,'') = ''),'',CONCAT(`u`.`first_name`,' ')),IF((IFNULL(`u`.`middle_name`,'') = ''),'',CONCAT(`u`.`middle_name`,' ')),IF((IFNULL(`u`.`last_name`,'') = ''),'',CONCAT(`u`.`last_name`,' '))) AS `childName`,
  `p`.`sub`             AS `email`,
  `c`.`name`            AS `centre`,
  `r`.`name`            AS `room`,
  `f`.`family_name`     AS `familyName`,
  `p`.`receiverAddress` AS `receiverEmail`,
  `p`.`receiverName`    AS `receiverName`,
  `p`.`create_time`     AS `createTime`,
  `p`.`status`          AS `status`,
  `p`.`delete_flag`     AS `deleteFlag`
FROM ((((`tbl_email_plan` `p`
      LEFT JOIN `tbl_user` `u`
        ON ((`p`.`user_id` = `u`.`id`)))
     LEFT JOIN `tbl_centers` `c`
       ON ((`u`.`centers_id` = `c`.`id`)))
    LEFT JOIN `tbl_room` `r`
      ON ((`u`.`room_id` = `r`.`id`)))
   LEFT JOIN `tbl_family` `f`
     ON ((`u`.`family_id` = `f`.`id`)))$$

DELIMITER ;