alter table tbl_application_list add user_id VARCHAR(50);

DELIMITER $$

DROP VIEW IF EXISTS `view_application_list`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `view_application_list` AS 
SELECT
  `app`.`id`                        AS `id`,
  CONCAT(`app`.`first_name`,' ',`app`.`last_name`) AS `childName`,
  `app`.`birthday`                  AS `birthday`,
  NULL                              AS `familyId`,
  `app`.`monday`                    AS `monday`,
  `app`.`tuesday`                   AS `tuesday`,
  `app`.`wednesday`                 AS `wednesday`,
  `app`.`thursday`                  AS `thursday`,
  `app`.`friday`                    AS `friday`,
  `app`.`application_date`          AS `applicationDate`,
  `app`.`application_date_position` AS `applicationDatePosition`,
  `app`.`requested_start_date`      AS `requestedStartDate`,
  `app`.`archived_from`             AS `archivedFrom`,
  0                                 AS `listType`,
  0                                 AS `sort`
FROM `tbl_application_list` `app`
WHERE ((`app`.`delete_flag` = 0)
       AND (`app`.`status` = 0))
ORDER BY `app`.`application_date_position`$$

DELIMITER ;

DELIMITER $$

DROP VIEW IF EXISTS `view_archived_list`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `view_archived_list` AS 
SELECT
  `app`.`id`                        AS `id`,
  CONCAT(`app`.`first_name`,' ',`app`.`last_name`) AS `childName`,
  `app`.`birthday`                  AS `birthday`,
  NULL                              AS `familyId`,
  `app`.`monday`                    AS `monday`,
  `app`.`tuesday`                   AS `tuesday`,
  `app`.`wednesday`                 AS `wednesday`,
  `app`.`thursday`                  AS `thursday`,
  `app`.`friday`                    AS `friday`,
  `app`.`application_date`          AS `applicationDate`,
  `app`.`application_date_position` AS `applicationDatePosition`,
  `app`.`requested_start_date`      AS `requestedStartDate`,
  `app`.`archived_from`             AS `archivedFrom`,
  3                                 AS `listType`,
  3                                 AS `sort`
FROM `tbl_application_list` `app`
WHERE ((`app`.`delete_flag` = 0)
       AND (`app`.`status` = 3))
ORDER BY `app`.`application_date_position`$$

DELIMITER ;

DELIMITER $$

DROP VIEW IF EXISTS `view_external_list`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `view_external_list` AS 
SELECT
  `u`.`id`                  AS `id`,
  CONCAT(`u`.`first_name`,' ',`u`.`last_name`) AS `childName`,
  `u`.`birthday`            AS `birthday`,
  `u`.`family_id`           AS `familyId`,
  `ca`.`monday`             AS `monday`,
  `ca`.`tuesday`            AS `tuesday`,
  `ca`.`wednesday`          AS `wednesday`,
  `ca`.`thursday`           AS `thursday`,
  `ca`.`friday`             AS `friday`,
  `ca`.`create_time`        AS `applicationDate`,
  `ca`.`positioningDate`    AS `applicationDatePosition`,
  `ca`.`request_start_date` AS `requestedStartDate`,
  NULL                      AS `archivedFrom`,
  1                         AS `listType`,
  2                         AS `sort`
FROM ((`tbl_user` `u`
    LEFT JOIN `tbl_child_attendance` `ca`
      ON ((`u`.`id` = `ca`.`user_id`)))
   LEFT JOIN `tbl_account` `a`
     ON ((`u`.`id` = `a`.`user_id`)))
WHERE ((`u`.`user_type` = 2)
       AND (`u`.`delete_flag` = 0)
       AND (`ca`.`enrolled` = 0)
       AND (`ca`.`type` = 1)
       AND (`a`.`status` <> 1)
       AND (`ca`.`siblingFlag` = 0))
ORDER BY `ca`.`positioningDate`$$

DELIMITER ;

DELIMITER $$

DROP VIEW IF EXISTS `view_sibling_list`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `view_sibling_list` AS 
SELECT
  `u`.`id`                  AS `id`,
  CONCAT(`u`.`first_name`,' ',`u`.`last_name`) AS `childName`,
  `u`.`birthday`            AS `birthday`,
  `u`.`family_id`           AS `familyId`,
  `ca`.`monday`             AS `monday`,
  `ca`.`tuesday`            AS `tuesday`,
  `ca`.`wednesday`          AS `wednesday`,
  `ca`.`thursday`           AS `thursday`,
  `ca`.`friday`             AS `friday`,
  `ca`.`create_time`        AS `applicationDate`,
  `ca`.`positioningDate`    AS `applicationDatePosition`,
  `ca`.`request_start_date` AS `requestedStartDate`,
  NULL                      AS `archivedFrom`,
  2                         AS `listType`,
  1                         AS `sort`
FROM ((`tbl_user` `u`
    LEFT JOIN `tbl_child_attendance` `ca`
      ON ((`u`.`id` = `ca`.`user_id`)))
   LEFT JOIN `tbl_account` `a`
     ON ((`u`.`id` = `a`.`user_id`)))
WHERE ((`u`.`user_type` = 2)
       AND (`u`.`delete_flag` = 0)
       AND (`ca`.`enrolled` = 0)
       AND (`ca`.`type` = 1)
       AND (`a`.`status` <> 1)
       AND (`ca`.`siblingFlag` = 1))
ORDER BY `ca`.`positioningDate`$$

DELIMITER ;

DELIMITER $$

DROP VIEW IF EXISTS `view_waiting_child_parent`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `view_waiting_child_parent` AS 
SELECT
  `app`.`id` AS `obj_id`,
  CONCAT(IF((IFNULL(`app`.`first_name`,'') = ''),'',CONCAT(`app`.`first_name`,' ')),IF((IFNULL(`app`.`last_name`,'') = ''),'',CONCAT(`app`.`last_name`,' '))) AS `childName`,
  CONCAT(IF((IFNULL(`p`.`first_name`,'') = ''),'',CONCAT(`p`.`first_name`,' ')),IF((IFNULL(`p`.`last_name`,'') = ''),'',CONCAT(`p`.`last_name`,' '))) AS `parentName`
FROM (`tbl_application_list` `app`
   LEFT JOIN `tbl_application_parent` `p`
     ON ((`app`.`id` = `p`.`application_id`)))
WHERE (`app`.`status` IN(0,3))UNION ALL SELECT
                                          `u`.`id`    AS `obj_id`,
                                          CONCAT(IF((IFNULL(`u`.`first_name`,'') = ''),'',CONCAT(`u`.`first_name`,' ')),IF((IFNULL(`u`.`middle_name`,'') = ''),'',CONCAT(`u`.`middle_name`,' ')),IF((IFNULL(`u`.`last_name`,'') = ''),'',CONCAT(`u`.`last_name`,' '))) AS `childName`,
                                          CONCAT(IF((IFNULL(`p`.`first_name`,'') = ''),'',CONCAT(`p`.`first_name`,' ')),IF((IFNULL(`p`.`middle_name`,'') = ''),'',CONCAT(`p`.`middle_name`,' ')),IF((IFNULL(`p`.`last_name`,'') = ''),'',CONCAT(`p`.`last_name`,' '))) AS `parentName`
                                        FROM (((`tbl_user` `u`
                                             LEFT JOIN `tbl_child_attendance` `ca`
                                               ON ((`u`.`id` = `ca`.`user_id`)))
                                            LEFT JOIN `tbl_account` `a`
                                              ON ((`u`.`id` = `a`.`user_id`)))
                                           LEFT JOIN `tbl_user` `p`
                                             ON ((`u`.`family_id` = `p`.`family_id`)))
                                        WHERE ((`u`.`delete_flag` = 0)
                                               AND (`ca`.`delete_flag` = 0)
                                               AND (`ca`.`enrolled` = 0)
                                               AND (`ca`.`type` = 1)
                                               AND (`a`.`status` <> 1)
                                               AND (`p`.`user_type` = 1))$$

DELIMITER ;