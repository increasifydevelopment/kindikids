
DROP TABLE IF EXISTS `tbl_value_select`;
CREATE TABLE `tbl_value_select` (
  `instance_id` varchar(50) DEFAULT NULL COMMENT '实例Id',
  `form_attr_id` varchar(50) DEFAULT NULL COMMENT '表单属性Id',
  `value` varchar(255) DEFAULT NULL COMMENT '值',
  `name` varchar(255) DEFAULT NULL COMMENT '名',
  KEY `FK_attr_vselect` (`form_attr_id`) USING BTREE,
  KEY `FK_instance_vselect` (`instance_id`) USING BTREE,
  CONSTRAINT `tbl_value_select_ibfk_1` FOREIGN KEY (`form_attr_id`) REFERENCES `tbl_form_attribute` (`attr_id`),
  CONSTRAINT `tbl_value_select_ibfk_2` FOREIGN KEY (`instance_id`) REFERENCES `tbl_instance` (`instance_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='动态表单值单选和多选形式';