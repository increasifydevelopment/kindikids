alter table tbl_sign_child modify column sign_in_name LONGTEXT;
alter table tbl_sign_child modify column sign_out_name LONGTEXT;

alter table tbl_sign_visitor modify column sign_in_name LONGTEXT;
alter table tbl_sign_visitor modify column sign_out_name LONGTEXT;


alter table tbl_giving_notice modify column parent_signature_id LONGTEXT;
alter table tbl_giving_notice modify column staff_signature_id LONGTEXT;
alter table tbl_giving_notice modify column bank_parent_signature_id LONGTEXT;

alter table tbl_absentee_request modify column parent_signature_id LONGTEXT;
alter table tbl_absentee_request modify column charge_signature_id LONGTEXT;

alter table tbl_value_text modify column value LONGTEXT;

alter table tbl_program_medication modify column parent_signature_text LONGTEXT;
alter table tbl_program_medication modify column staff_administering_signature LONGTEXT;
alter table tbl_program_medication modify column staff_checking LONGTEXT;

