DELIMITER $$

DROP PROCEDURE IF EXISTS `Pro_Delete_Child`$$

CREATE DEFINER=`root`@`%` PROCEDURE `Pro_Delete_Child`(IN accountId VARCHAR (50))
BEGIN
	UPDATE `tbl_account` SET `delete_flag`=1 WHERE id=accountId;
	UPDATE tbl_user SET delete_flag = 1 WHERE id =(SELECT a.`user_id` FROM tbl_account a WHERE a.id=accountId);
	UPDATE tbl_relation_kid_parent SET delete_flag = 1 WHERE kid_account_id = accountId;
	UPDATE `tbl_child_attendance` SET delete_flag=1 WHERE `user_id`=(SELECT a.`user_id` FROM tbl_account a WHERE a.id=accountId);
	UPDATE `tbl_change_attendance_request` SET `delete_flag`=1 WHERE `child_id`=accountId;
	UPDATE `tbl_absentee_request` SET `delete_flag`=1 WHERE `child_id`=accountId;
	UPDATE `tbl_temporary_attendance` SET `delete_flag`=1 WHERE `child_id`=accountId;
	UPDATE `tbl_subtracted_attendance` SET `delete_flag`=1 WHERE `child_id`=accountId;
	DELETE FROM `tbl_room_change` WHERE `child_id`=accountId;
    END$$

DELIMITER ;