ALTER TABLE tbl_program_lesson MODIFY COLUMN reflection VARCHAR(5000);

ALTER TABLE tbl_program_lesson MODIFY COLUMN extension VARCHAR(1000);

ALTER TABLE tbl_program_lesson ADD old_id VARCHAR (50);