DELIMITER $$

DROP PROCEDURE IF EXISTS `Pro_UnArchived`$$

CREATE DEFINER=`root`@`%` PROCEDURE `Pro_UnArchived`(IN childId VARCHAR (50))
BEGIN
  UPDATE 
    tbl_absentee_request a 
    JOIN tbl_request_log r 
      ON r.`obj_id` = a.`id` SET a.`state` = r.`state` 
  WHERE a.`child_id` =childId ;
  
  UPDATE tbl_request_log SET state=NULL WHERE child_id=childId AND `type`=13;
END$$

DELIMITER ;