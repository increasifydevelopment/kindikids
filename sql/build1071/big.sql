DELIMITER $$

DROP PROCEDURE IF EXISTS `Pro_SynLogState`$$

CREATE DEFINER=`root`@`%` PROCEDURE `Pro_SynLogState`(IN childId VARCHAR(50))
BEGIN
	UPDATE `tbl_request_log` l LEFT JOIN `tbl_absentee_request` ar ON ar.`id`=l.`obj_id` 
	SET l.`state`=
		    (CASE
		      WHEN ar.`state` = 0 
		      THEN  1 
		      ELSE  ar.`state` 
		    END)
	WHERE l.`child_id`=childId AND ar.`id` IS NOT NULL AND l.`type`=13 AND ar.state!=7;
	
	UPDATE `tbl_request_log` l LEFT JOIN `tbl_giving_notice` gn ON gn.`id`=l.`obj_id` 
	SET l.`state`=
		    (CASE
		      WHEN gn.`state` = 0 
		      THEN  1 
		      ELSE  gn.`state` 
		    END)
	WHERE l.`child_id`=childId AND gn.`id` IS NOT NULL AND l.`type`=12 AND gn.state!=7;
	
	UPDATE `tbl_request_log` l LEFT JOIN `tbl_change_attendance_request` ca ON ca.`id`=l.`obj_id` 
	SET l.`state`=
		    (CASE
		      WHEN (ca.`state` = 0 OR ca.state=2)
		      THEN  1 
		      WHEN (ca.`state` = 5 OR ca.state=3 )
		      THEN  1
		      ELSE  ca.`state` 
		    END)
	WHERE l.`child_id`=childId AND ca.`id` IS NOT NULL AND l.`type`IN(0,1,2,4,5) AND ca.state!=7;
	
	UPDATE tbl_giving_notice SET state = 7 WHERE child_id = childId;
	
	UPDATE tbl_absentee_request SET state = 7 WHERE child_id =childId;
	
	UPDATE tbl_change_attendance_request SET  state=7 WHERE child_id =childId;
	
	UPDATE tbl_subtracted_attendance SET delete_flag=7  WHERE child_id =childId;
	
	UPDATE tbl_temporary_attendance SET delete_flag=7 WHERE child_id =childId;
    END$$

DELIMITER ;