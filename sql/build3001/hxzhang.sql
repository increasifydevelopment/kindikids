SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_application_list
-- ----------------------------
DROP TABLE IF EXISTS `tbl_application_list`;
CREATE TABLE `tbl_application_list` (
  `id` varchar(50) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `gender` smallint(6) DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `requested_start_date` datetime DEFAULT NULL,
  `monday` bit(1) DEFAULT NULL,
  `tuesday` bit(1) DEFAULT NULL,
  `wednesday` bit(1) DEFAULT NULL,
  `thursday` bit(1) DEFAULT NULL,
  `friday` bit(1) DEFAULT NULL,
  `application_date` datetime DEFAULT NULL,
  `application_date_position` datetime DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `archived_from` smallint(6) DEFAULT NULL,
  `notes` varchar(500) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_account_id` varchar(50) DEFAULT NULL,
  `delete_flag` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_application_parent
-- ----------------------------
DROP TABLE IF EXISTS `tbl_application_parent`;
CREATE TABLE `tbl_application_parent` (
  `id` varchar(50) NOT NULL,
  `application_id` varchar(50) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `suburb` varchar(255) DEFAULT NULL,
  `postcode` varchar(50) DEFAULT NULL,
  `home_phone` varchar(50) DEFAULT NULL,
  `work_phone` varchar(50) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_account_id` varchar(50) DEFAULT NULL,
  `delete_flag` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_TBL_APPL_REFERENCE_TBL_APPL` (`application_id`),
  CONSTRAINT `FK_TBL_APPL_REFERENCE_TBL_APPL` FOREIGN KEY (`application_id`) REFERENCES `tbl_application_list` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




INSERT INTO tbl_menu (
	id,
	`name`,
	url,
	active_flag,
	delete_flag
)
VALUES
(
	'fe85639f-54ed-11e8-8348-00e0703507b4',
	'waiting',
	'application_list',
	1,
	0
);
	
INSERT INTO tbl_role_menu (id, role_id, menu_id)
VALUES
(
	'99946707-6217-11e8-a955-00e0703507b4',
	'2e083440-49a3-11e6-b19e-00e0703507b4',
	'fe85639f-54ed-11e8-8348-00e0703507b4'
),
(
	'd9fd7c05-6217-11e8-a955-00e0703507b4',
	'4586aa2b-49a3-11e6-b19e-00e0703507b4',
	'fe85639f-54ed-11e8-8348-00e0703507b4'
),
(
	'f1683819-6217-11e8-a955-00e0703507b4',
	'60a193d5-49a3-11e6-b19e-00e0703507b4',
	'fe85639f-54ed-11e8-8348-00e0703507b4'
);	
	
INSERT INTO tbl_function (
	id,
	NAME,
	target,
	menu_id,
	function_type
)
VALUES
(
	'880ea868-54ed-11e8-8348-00e0703507b4',
	'waiting_list.do',
	'/waiting/list.do',
	'fe85639f-54ed-11e8-8348-00e0703507b4',
	2
);

INSERT INTO tbl_role_function (id, role_id, function_id)
VALUES
(
	'825dc502-54ee-11e8-8348-00e0703507b4',
	'2e083440-49a3-11e6-b19e-00e0703507b4',  
	'880ea868-54ed-11e8-8348-00e0703507b4'
),(
	'88ccd3f4-54ee-11e8-8348-00e0703507b4',
	'4586aa2b-49a3-11e6-b19e-00e0703507b4',  
	'880ea868-54ed-11e8-8348-00e0703507b4'
),(
	'42d4f60c-621b-11e8-a955-00e0703507b4',
	'60a193d5-49a3-11e6-b19e-00e0703507b4',  
	'880ea868-54ed-11e8-8348-00e0703507b4'
);	
	
INSERT INTO tbl_function (
	id,
	NAME,
	target,
	menu_id,
	function_type
)
VALUES
(
	'4e7f5e0e-5755-11e8-a955-00e0703507b4',
	'waiting_details.do',
	'/waiting/details.do',
	'fe85639f-54ed-11e8-8348-00e0703507b4',
	2
);

INSERT INTO tbl_role_function (id, role_id, function_id)
VALUES
(
	'88d5eee3-5755-11e8-a955-00e0703507b4',
	'2e083440-49a3-11e6-b19e-00e0703507b4',  
	'4e7f5e0e-5755-11e8-a955-00e0703507b4'
),(
	'9b3b0903-5755-11e8-a955-00e0703507b4',
	'4586aa2b-49a3-11e6-b19e-00e0703507b4',  
	'4e7f5e0e-5755-11e8-a955-00e0703507b4'
),(
	'8ea4f5ee-621b-11e8-a955-00e0703507b4',
	'60a193d5-49a3-11e6-b19e-00e0703507b4', 
	'4e7f5e0e-5755-11e8-a955-00e0703507b4'
);
	
INSERT INTO tbl_function (
	id,
	NAME,
	target,
	menu_id,
	function_type
)
VALUES
(
	'2749a7fd-5852-11e8-a955-00e0703507b4',
	'waiting_count.do',
	'/waiting/count.do',
	'fe85639f-54ed-11e8-8348-00e0703507b4',
	1
);
