DELIMITER $$

DROP VIEW IF EXISTS `view_archived_list`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `view_archived_list` AS 
SELECT
  `app`.`id`                        AS `id`,
  IF((`app`.`archived_from` = 0),CONCAT(`app`.`first_name`,' ',`app`.`last_name`),CONCAT(`u`.`first_name`,IF((IFNULL(`u`.`middle_name`,'') = ''),' ',CONCAT(' ',`u`.`middle_name`,' ')),`u`.`last_name`)) AS `childName`,
  IF((`app`.`archived_from` = 0),`app`.`birthday`,`u`.`birthday`) AS `birthday`,
  `u`.`family_id`                   AS `familyId`,
  `app`.`monday`                    AS `monday`,
  `app`.`tuesday`                   AS `tuesday`,
  `app`.`wednesday`                 AS `wednesday`,
  `app`.`thursday`                  AS `thursday`,
  `app`.`friday`                    AS `friday`,
  `app`.`application_date`          AS `applicationDate`,
  `app`.`application_date_position` AS `applicationDatePosition`,
  `app`.`requested_start_date`      AS `requestedStartDate`,
  `app`.`archived_from`             AS `archivedFrom`,
  3                                 AS `listType`,
  3                                 AS `sort`,
  NULL                              AS `enrolledFlag`
FROM (`tbl_application_list` `app`
   LEFT JOIN `tbl_user` `u`
     ON ((`app`.`user_id` = `u`.`id`)))
WHERE ((`app`.`delete_flag` = 0)
       AND (`app`.`status` = 3))
ORDER BY `app`.`application_date_position`$$

DELIMITER ;


UPDATE tbl_account SET `status` = -1 WHERE user_id in (select user_id from tbl_child_attendance WHERE enrolled = 0 AND type = 0);
