alter table tbl_email_plan add user_id varchar(50);


INSERT INTO tbl_function (
	id,
	NAME,
	target,
	menu_id,
	function_type
)
VALUES
	(
		'54d7a022-c158-11e8-b729-408d5c9798a2',
		'attendance_emailList',
		'/emailList/list.do',
		'3ea7375f-7890-11e6-85fd-00e0703507b4',
		2
	);



INSERT INTO tbl_role_function (id, role_id, function_id)
VALUES
	(
		'6350f69c-c158-11e8-b729-408d5c9798a2',
		'2e083440-49a3-11e6-b19e-00e0703507b4',
		'54d7a022-c158-11e8-b729-408d5c9798a2'
	),
	(
		'69685aed-c158-11e8-b729-408d5c9798a2',
		'4586aa2b-49a3-11e6-b19e-00e0703507b4',
		'54d7a022-c158-11e8-b729-408d5c9798a2'
	),
	(
		'6ece1e98-c158-11e8-b729-408d5c9798a2',
		'60a193d5-49a3-11e6-b19e-00e0703507b4',
		'54d7a022-c158-11e8-b729-408d5c9798a2'
	);
	
	

	
INSERT INTO tbl_function (
	id,
	NAME,
	target,
	menu_id,
	function_type
)
VALUES
	(
		'119fe650-c186-11e8-b729-408d5c9798a2',
		'attendance_sendPlanEmail',
		'/emailList/sendPlanEmail.do',
		'3ea7375f-7890-11e6-85fd-00e0703507b4',
		2
	);



INSERT INTO tbl_role_function (id, role_id, function_id)
VALUES
	(
		'1a79f9e5-c186-11e8-b729-408d5c9798a2',
		'2e083440-49a3-11e6-b19e-00e0703507b4',
		'119fe650-c186-11e8-b729-408d5c9798a2'
	),
	(
		'1fd4bce9-c186-11e8-b729-408d5c9798a2',
		'4586aa2b-49a3-11e6-b19e-00e0703507b4',
		'119fe650-c186-11e8-b729-408d5c9798a2'
	),
	(
		'257170bf-c186-11e8-b729-408d5c9798a2',
		'60a193d5-49a3-11e6-b19e-00e0703507b4',
		'119fe650-c186-11e8-b729-408d5c9798a2'
	);
	
	
DELIMITER $$

DROP VIEW IF EXISTS `v_email_list_view`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_email_list_view` AS 
SELECT
  `p`.`id`              AS `id`,
  CONCAT(IF((IFNULL(`u`.`first_name`,'') = ''),'',CONCAT(`u`.`first_name`,' ')),IF((IFNULL(`u`.`middle_name`,'') = ''),'',CONCAT(`u`.`middle_name`,' ')),IF((IFNULL(`u`.`last_name`,'') = ''),'',CONCAT(`u`.`last_name`,' '))) AS `childName`,
  `p`.`sub`             AS `email`,
  `c`.`name`            AS `centre`,
  `r`.`name`            AS `room`,
  `f`.`family_name`     AS `familyName`,
  `p`.`receiverAddress` AS `receiverEmail`,
  `p`.`receiverName`    AS `receiverName`,
  `p`.`create_time`     AS `createTime`,
  `p`.`delete_flag`     AS `status`
FROM ((((`tbl_email_plan` `p`
      LEFT JOIN `tbl_user` `u`
        ON ((`p`.`user_id` = `u`.`id`)))
     LEFT JOIN `tbl_centers` `c`
       ON ((`u`.`centers_id` = `c`.`id`)))
    LEFT JOIN `tbl_room` `r`
      ON ((`u`.`room_id` = `r`.`id`)))
   LEFT JOIN `tbl_family` `f`
     ON ((`u`.`family_id` = `f`.`id`)))$$

DELIMITER ;