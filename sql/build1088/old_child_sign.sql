/*
Navicat MySQL Data Transfer

Source Server         : gfwang
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : kk_old

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2017-01-21 11:27:23
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for old_child_sign
-- ----------------------------
DROP TABLE IF EXISTS `old_child_sign`;
CREATE TABLE `old_child_sign` (
  `sign_date` varchar(50) DEFAULT NULL,
  `type` smallint(1) DEFAULT NULL,
  `state` smallint(1) DEFAULT NULL,
  `sign_in_name` longtext,
  `sunscreen_app` bit(1) DEFAULT NULL,
  `room_oldid` varchar(50) DEFAULT NULL,
  `child_oldId` varchar(50) DEFAULT NULL,
  `centre_oldid` varchar(50) DEFAULT NULL,
  `sign_out_name` longtext,
  `create_time` varchar(50) DEFAULT NULL,
  `create_account_id` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
