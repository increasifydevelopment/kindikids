INSERT INTO tbl_function (
	id,
	NAME,
	target,
	menu_id,
	function_type
)
VALUES
	(
		'34e38eac-d5ae-11e6-8c81-00e0701322e8',
		'leave_go_request',
		'/leave/goRequestState.do',
		'b2d6c3ba-7a24-11e6-85fd-00e0703507b4',
		2
	);



INSERT INTO tbl_role_function (id, role_id, function_id)
VALUES
	(
		'7bb0eed4-d5ae-11e6-8c81-00e0701322e8',
		'2e083440-49a3-11e6-b19e-00e0703507b4',
		'34e38eac-d5ae-11e6-8c81-00e0701322e8'
	),
	(
		'7bb22e16-d5ae-11e6-8c81-00e0701322e8',
		'4586aa2b-49a3-11e6-b19e-00e0703507b4',
		'34e38eac-d5ae-11e6-8c81-00e0701322e8'
	),
	(
		'ad635315-d5ae-11e6-8c81-00e0701322e8',
		'60a193d5-49a3-11e6-b19e-00e0703507b4',
		'34e38eac-d5ae-11e6-8c81-00e0701322e8'
	);