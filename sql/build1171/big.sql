DELIMITER $$

DROP VIEW IF EXISTS `v_1`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_1` AS (
SELECT
  `i`.`id`            AS `id`,
  `i`.`centre_id`     AS `centre_id`,
  `i`.`room_id`       AS `room_id`,
  `i`.`group_id`      AS `group_id`,
  `i`.`obj_id`        AS `obj_id`,
  `i`.`value_id`      AS `value_id`,
  `i`.`obj_type`      AS `obj_type`,
  `i`.`statu`         AS `statu`,
  `i`.`task_type`     AS `task_type`,
  `i`.`begin_date`    AS `begin_date`,
  `i`.`end_date`      AS `end_date`,
  `t`.`task_name`     AS `task_name`,
  `i`.`task_model_id` AS `task_model_id`,
  `i`.`create_time`   AS `create_time`,
  0                   AS `t`
FROM (`tbl_task_instance` `i`
   JOIN `tbl_task` `t`
     ON (((`i`.`task_type` = 17)
          AND (`i`.`delete_flag` = 0)
          AND (`t`.`delete_flag` = 0)
          AND (`t`.`id` = `i`.`task_model_id`)))))$$

DELIMITER ;
DELIMITER $$

DROP VIEW IF EXISTS `v_2`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_2` AS (
SELECT `i`.`id` AS `id`,`i`.`centre_id` AS `centre_id`,`i`.`room_id` AS `room_id`,`i`.`group_id` AS `group_id`,`i`.`obj_id` AS `obj_id`,`i`.`value_id` AS `value_id`,`i`.`obj_type` AS `obj_type`,`exgrsc`.`state` AS `statu`,`i`.`task_type` AS `task_type`,`i`.`begin_date` AS `begin_date`,`i`.`end_date` AS `end_date`,CONCAT(`cc`.`name`,' ',`room`.`name`) AS `task_name`,'0' AS `task_model_id`,`i`.`create_time` AS `create_time`,1 AS `t` FROM (((`tbl_task_instance` `i` JOIN `tbl_program_task_exgrsc` `exgrsc` ON(((`i`.`delete_flag` = 0) AND (`i`.`task_type` IN (0,1,3)) AND (`exgrsc`.`id` = `i`.`value_id`) AND (`exgrsc`.`delete_flag` = 0)))) LEFT JOIN `tbl_centers` `cc` ON((`cc`.`id` = `i`.`centre_id`))) LEFT JOIN `tbl_room` `room` ON((`room`.`id` = `i`.`room_id`))))$$

DELIMITER ;

DELIMITER $$

DROP VIEW IF EXISTS `v_3`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_3` AS (
SELECT
  `i`.`id`          AS `id`,
  `i`.`centre_id`   AS `centre_id`,
  `i`.`room_id`     AS `room_id`,
  `i`.`group_id`    AS `group_id`,
  `i`.`obj_id`      AS `obj_id`,
  `i`.`value_id`    AS `value_id`,
  `i`.`obj_type`    AS `obj_type`,
  `lesson`.`state`  AS `statu`,
  `i`.`task_type`   AS `task_type`,
  `i`.`begin_date`  AS `begin_date`,
  `i`.`end_date`    AS `end_date`,
  CONCAT(`cc`.`name`,' ',`room`.`name`) AS `task_name`,
  '0'               AS `task_model_id`,
  `i`.`create_time` AS `create_time`,
  1                 AS `t`
FROM (((`tbl_task_instance` `i`
     JOIN `tbl_program_lesson` `lesson`
       ON (((`i`.`task_type` = 2)
            AND (`i`.`delete_flag` = 0)
            AND (`lesson`.`delete_flag` = 0)
            AND (`lesson`.`id` = `i`.`value_id`))))
    LEFT JOIN `tbl_centers` `cc`
      ON ((`cc`.`id` = `i`.`centre_id`)))
   LEFT JOIN `tbl_room` `room`
     ON ((`room`.`id` = `i`.`room_id`))))$$

DELIMITER ;

DELIMITER $$

DROP VIEW IF EXISTS `v_4`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_4` AS (
SELECT `i`.`id` AS `id`,`i`.`centre_id` AS `centre_id`,`i`.`room_id` AS `room_id`,`i`.`group_id` AS `group_id`,`i`.`obj_id` AS `obj_id`,`i`.`value_id` AS `value_id`,`i`.`obj_type` AS `obj_type`,`intobslea`.`state` AS `statu`,`i`.`task_type` AS `task_type`,`i`.`begin_date` AS `begin_date`,`i`.`end_date` AS `end_date`,CONCAT(`GetFullName`(`u`.`first_name`,`u`.`middle_name`,`u`.`last_name`),CONVERT((CASE WHEN (`i`.`task_type` = 4) THEN '\'s Interest' WHEN (`i`.`task_type` = 6) THEN '\'s Observation' WHEN (`i`.`task_type` = 8) THEN '\'s Learning Story' END) USING utf8)) AS `task_name`,'0' AS `task_model_id`,`i`.`create_time` AS `create_time`,1 AS `t` FROM (((((`tbl_task_instance` `i` JOIN `tbl_program_intobslea` `intobslea` ON(((`i`.`task_type` IN (4,6,8)) AND (`i`.`delete_flag` = 0) AND (`intobslea`.`delete_flag` = 0) AND (`intobslea`.`id` = `i`.`value_id`)))) LEFT JOIN `tbl_centers` `cc` ON((`cc`.`id` = `i`.`centre_id`))) LEFT JOIN `tbl_room` `room` ON((`room`.`id` = `i`.`room_id`))) LEFT JOIN `tbl_account` `account` ON((`account`.`id` = `intobslea`.`child_id`))) LEFT JOIN `tbl_user` `u` ON((`u`.`id` = `account`.`user_id`))))$$

DELIMITER ;


DELIMITER $$

DROP VIEW IF EXISTS `v_5`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_5` AS (
SELECT `i`.`id` AS `id`,`i`.`centre_id` AS `centre_id`,`i`.`room_id` AS `room_id`,`i`.`group_id` AS `group_id`,`i`.`obj_id` AS `obj_id`,`i`.`value_id` AS `value_id`,`i`.`obj_type` AS `obj_type`,`follow`.`state` AS `statu`,`i`.`task_type` AS `task_type`,`i`.`begin_date` AS `begin_date`,`i`.`end_date` AS `end_date`,CONCAT(CONCAT(`GetFullName`(`u`.`first_name`,`u`.`middle_name`,`u`.`last_name`),CONVERT((CASE WHEN (`i`.`task_type` = 5) THEN '\'s Interest Follow Up' WHEN (`i`.`task_type` = 7) THEN '\'s Observation Follow Up' WHEN (`i`.`task_type` = 9) THEN '\'s Learning Story Follow Up' END) USING utf8))) AS `task_name`,'0' AS `task_model_id`,`i`.`create_time` AS `create_time`,1 AS `t` FROM ((((((`tbl_task_instance` `i` JOIN `tbl_task_program_follow_up` `follow` ON(((`i`.`task_type` IN (5,7,9)) AND (`i`.`delete_flag` = 0) AND (`follow`.`delete_flag` = 0) AND (`follow`.`id` = `i`.`value_id`)))) LEFT JOIN `tbl_centers` `cc` ON((`cc`.`id` = `i`.`centre_id`))) LEFT JOIN `tbl_room` `room` ON((`room`.`id` = `i`.`room_id`))) LEFT JOIN `tbl_program_intobslea` `intobslea` ON((`follow`.`task_id` = `intobslea`.`id`))) LEFT JOIN `tbl_account` `account` ON((`account`.`id` = `intobslea`.`child_id`))) LEFT JOIN `tbl_user` `u` ON((`u`.`id` = `account`.`user_id`))))$$

DELIMITER ;

DELIMITER $$

DROP VIEW IF EXISTS `v_6`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_6` AS (
SELECT `i`.`id` AS `id`,`i`.`centre_id` AS `centre_id`,`i`.`room_id` AS `room_id`,`i`.`group_id` AS `group_id`,`i`.`obj_id` AS `obj_id`,`i`.`value_id` AS `value_id`,`i`.`obj_type` AS `obj_type`,`register`.`state` AS `statu`,`i`.`task_type` AS `task_type`,`i`.`begin_date` AS `begin_date`,`i`.`end_date` AS `end_date`,(CASE WHEN ((`register`.`type` = 13) OR (`register`.`type` = 14)) THEN CONCAT(`cc`.`name`,' ',`room`.`name`,' ',CONVERT(DATE_FORMAT(`register`.`day`,'%h:%i %p') USING utf8)) ELSE CONCAT(`cc`.`name`,' ',`room`.`name`) END) AS `task_name`,'0' AS `task_model_id`,`i`.`create_time` AS `create_time`,1 AS `t` FROM (((`tbl_task_instance` `i` JOIN `tbl_program_register_task` `register` ON(((`i`.`task_type` IN (11,12,13,14,15)) AND (`i`.`delete_flag` = 0) AND (`register`.`delete_flag` = 0) AND (`register`.`id` = `i`.`value_id`)))) LEFT JOIN `tbl_centers` `cc` ON((`cc`.`id` = `i`.`centre_id`))) LEFT JOIN `tbl_room` `room` ON((`room`.`id` = `i`.`room_id`))))$$

DELIMITER ;
DELIMITER $$

DROP VIEW IF EXISTS `v_7`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_7` AS (
SELECT
  `i`.`id`          AS `id`,
  `i`.`centre_id`   AS `centre_id`,
  `i`.`room_id`     AS `room_id`,
  `i`.`group_id`    AS `group_id`,
  `i`.`obj_id`      AS `obj_id`,
  `i`.`value_id`    AS `value_id`,
  `i`.`obj_type`    AS `obj_type`,
  `weekly`.`state`  AS `statu`,
  `i`.`task_type`   AS `task_type`,
  `i`.`begin_date`  AS `begin_date`,
  `i`.`end_date`    AS `end_date`,
  CONCAT(`cc`.`name`,' ',`room`.`name`,' ',CONVERT(IF((`weekly`.`type` = 0),'Weekly Evaluation Explore Curriculum','Weekly Evaluation Grow Curriculum') USING utf8)) AS `task_name`,
  '0'               AS `task_model_id`,
  `i`.`create_time` AS `create_time`,
  1                 AS `t`
FROM (((`tbl_task_instance` `i`
     JOIN `tbl_program_weekly_evalution` `weekly`
       ON ((`weekly`.`id` = `i`.`value_id`)))
    LEFT JOIN `tbl_centers` `cc`
      ON ((`cc`.`id` = `i`.`centre_id`)))
   LEFT JOIN `tbl_room` `room`
     ON ((`room`.`id` = `i`.`room_id`)))
WHERE ((`i`.`task_type` = 10)
       AND (`i`.`delete_flag` = 0)
       AND (`weekly`.`delete_flag` = 0)))$$

DELIMITER ;
DELIMITER $$

DROP VIEW IF EXISTS `v_8`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_8` AS (
SELECT
  `i`.`id`             AS `id`,
  `i`.`centre_id`      AS `centre_id`,
  `i`.`room_id`        AS `room_id`,
  `i`.`group_id`       AS `group_id`,
  `i`.`obj_id`         AS `obj_id`,
  `i`.`value_id`       AS `value_id`,
  `i`.`obj_type`       AS `obj_type`,
  `medication`.`statu` AS `statu`,
  `i`.`task_type`      AS `task_type`,
  `i`.`begin_date`     AS `begin_date`,
  `i`.`end_date`       AS `end_date`,
  CONCAT(`cc`.`name`,' ',`room`.`name`,' ',`GetFullName`(`u`.`first_name`,`u`.`middle_name`,`u`.`last_name`)) AS `task_name`,
  '0'                  AS `task_model_id`,
  `i`.`create_time`    AS `create_time`,
  1                    AS `t`
FROM (((((`tbl_task_instance` `i`
       JOIN `tbl_program_medication` `medication`
         ON ((`medication`.`id` = `i`.`value_id`)))
      LEFT JOIN `tbl_centers` `cc`
        ON ((`cc`.`id` = `i`.`centre_id`)))
     LEFT JOIN `tbl_room` `room`
       ON ((`room`.`id` = `i`.`room_id`)))
    LEFT JOIN `tbl_account` `a`
      ON ((`a`.`id` = `medication`.`child_account_id`)))
   LEFT JOIN `tbl_user` `u`
     ON ((`u`.`id` = `a`.`user_id`)))
WHERE ((`i`.`task_type` = 18)
       AND (`i`.`delete_flag` = 0)
       AND (`medication`.`delete_flag` = 0)))$$

DELIMITER ;


DELIMITER $$

DROP VIEW IF EXISTS `v_9`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_9` AS (
SELECT
  `i`.`id`             AS `id`,
  `i`.`centre_id`      AS `centre_id`,
  `i`.`room_id`        AS `room_id`,
  `i`.`group_id`       AS `group_id`,
  `i`.`obj_id`         AS `obj_id`,
  `tfi`.`id`           AS `value_id`,
  `i`.`obj_type`       AS `obj_type`,
  `tfi`.`statu`        AS `statu`,
  `i`.`task_type`      AS `task_type`,
  `tfi`.`begin_date`   AS `begin_date`,
  `tfi`.`end_date`     AS `end_date`,
  CONCAT(`t`.`task_name`,'-follow up') AS `task_name`,
  `tfi`.`follow_up_id` AS `task_model_id`,
  `i`.`create_time`    AS `create_time`,
  2                    AS `t`
FROM ((`tbl_task_instance` `i`
    JOIN `tbl_task_follow_instance` `tfi`
      ON ((`tfi`.`task_instance_id` = `i`.`id`)))
   JOIN `tbl_task` `t`
     ON ((`t`.`id` = `i`.`task_model_id`)))
WHERE ((`i`.`task_type` = 17)
       AND (`i`.`delete_flag` = 0)
       AND (`tfi`.`delete_flag` = 0)
       AND (`t`.`delete_flag` = 0)))$$

DELIMITER ;

DELIMITER $$

DROP VIEW IF EXISTS `v_10`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_10` AS (
SELECT DISTINCT
  `i`.`id`          AS `id`,
  `i`.`centre_id`   AS `centre_id`,
  `i`.`room_id`     AS `room_id`,
  `i`.`group_id`    AS `group_id`,
  `v`.`accountId`   AS `obj_id`,
  `i`.`value_id`    AS `value_id`,
  `i`.`obj_type`    AS `obj_type`,
  `i`.`statu`       AS `statu`,
  `i`.`task_type`   AS `task_type`,
  `i`.`begin_date`  AS `begin_date`,
  `i`.`end_date`    AS `end_date`,
  `v`.`name`        AS `task_name`,
  '0'               AS `task_model_id`,
  `i`.`create_time` AS `create_time`,
  3                 AS `t`
FROM (`tbl_task_instance` `i`
   JOIN `v_meet_task_view` `v`
     ON ((`v`.`row_id` = `i`.`value_id`)))
WHERE ((`i`.`task_type` = 20)
       AND (`i`.`delete_flag` = 0)))$$

DELIMITER ;

DELIMITER $$

DROP VIEW IF EXISTS `v_11`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_11` AS (
SELECT DISTINCT
  `i`.`id`          AS `id`,
  `i`.`centre_id`   AS `centre_id`,
  `i`.`room_id`     AS `room_id`,
  `i`.`group_id`    AS `group_id`,
  `i`.`obj_id`      AS `obj_id`,
  `i`.`value_id`    AS `value_id`,
  `i`.`obj_type`    AS `obj_type`,
  `i`.`statu`       AS `statu`,
  `i`.`task_type`   AS `task_type`,
  `i`.`begin_date`  AS `begin_date`,
  `i`.`end_date`    AS `end_date`,
  `GetFullName`(
`u`.`first_name`,`u`.`middle_name`,`u`.`last_name`)  AS `task_name`,
  '0'               AS `task_model_id`,
  `i`.`create_time` AS `create_time`,
  4                 AS `t`
FROM ((`tbl_task_instance` `i`
    JOIN `tbl_account` `a`
      ON ((`a`.`id` = `i`.`value_id`)))
   JOIN `tbl_user` `u`
     ON ((`u`.`id` = `a`.`user_id`)))
WHERE ((`i`.`task_type` IN(21,22))
       AND (`i`.`delete_flag` = 0)))$$

DELIMITER ;

