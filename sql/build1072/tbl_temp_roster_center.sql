/*
Navicat MySQL Data Transfer

Source Server         : aoyun
Source Server Version : 50627
Source Host           : 192.168.1.3:3306
Source Database       : kindkids_dev

Target Server Type    : MYSQL
Target Server Version : 50627
File Encoding         : 65001

Date: 2016-12-29 10:19:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_temp_roster_center
-- ----------------------------
DROP TABLE IF EXISTS `tbl_temp_roster_center`;
CREATE TABLE `tbl_temp_roster_center` (
  `roster_staff_id` varchar(50) DEFAULT NULL COMMENT '外键',
  `centre_id` varchar(50) DEFAULT NULL,
  `role_id` varchar(50) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `delete_falg` bit(1) DEFAULT NULL COMMENT '是否删除'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
