alter table tbl_program_medication drop column staff_administering_signature ; 
alter table tbl_program_medication drop column staff_checking ;

ALTER TABLE tbl_time_medication ADD COLUMN  staff_administering_signature LONGTEXT;
ALTER TABLE tbl_time_medication ADD COLUMN  staff_checking LONGTEXT;


INSERT INTO tbl_function (
	id,
	NAME,
	target,
	menu_id,
	function_type
)
VALUES
	(
		'2f5880cc-ccac-11e6-ae66-00e0703507b4',
		'program_updateNappyToiletTime',
		'/program/updateNappyToiletTime.do',
		'2321e129-8ea8-11e6-85fd-00e0703507b4',
		2
	);
	
INSERT INTO tbl_role_function (id, role_id, function_id)
VALUES
	(
		'72aa1aec-ccac-11e6-ae66-00e0703507b4',
		'2e083440-49a3-11e6-b19e-00e0703507b4',
		'2f5880cc-ccac-11e6-ae66-00e0703507b4'
	),(
		'780e1fc8-ccac-11e6-ae66-00e0703507b4',
		'4586aa2b-49a3-11e6-b19e-00e0703507b4',
		'2f5880cc-ccac-11e6-ae66-00e0703507b4'
	),
(
		'7c711f76-ccac-11e6-ae66-00e0703507b4',
		'51c32196-49a3-11e6-b19e-00e0703507b4',
		'2f5880cc-ccac-11e6-ae66-00e0703507b4'
	),
(
		'803aab27-ccac-11e6-ae66-00e0703507b4',
		'74ba21a6-49a3-11e6-b19e-00e0703507b4',
		'2f5880cc-ccac-11e6-ae66-00e0703507b4'
	);
