UPDATE tbl_account SET not_sign_out = NULL;

alter table tbl_account MODIFY  not_sign_out DATE;


ALTER TABLE tbl_sign_child ADD COLUMN generation_sign bit;

ALTER TABLE tbl_sign_child ADD COLUMN generation_sign_account_id VARCHAR(50);
