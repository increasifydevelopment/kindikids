DELIMITER $$



DROP VIEW IF EXISTS `v_roster_staff_view`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_roster_staff_view` AS (
SELECT
  `rs`.`id`                AS `id`,
  `rs`.`shift_time_code`   AS `shift_time_code`,
  `rs`.`roster_id`         AS `roster_id`,
  `rs`.`day`               AS `day`,
  `rs`.`staff_account_id`  AS `staff_account_id`,
  `rs`.`staff_type`        AS `staff_type`,
  `rs`.`create_time`       AS `create_time`,
  `rs`.`create_account_id` AS `create_account_id`,
  `rs`.`update_time`       AS `update_time`,
  `rs`.`update_account_id` AS `update_account_id`,
  `rs`.`delete_flag`       AS `delete_flag`,
  `rs`.`start_time`        AS `start_time`,
  `rs`.`end_time`          AS `end_time`,
  CONCAT(IF((IFNULL(`u`.`first_name`,'') = ''),'',CONCAT(`u`.`first_name`,' ')),IF((IFNULL(`u`.`middle_name`,'') = ''),'',CONCAT(`u`.`middle_name`,' ')),IF((IFNULL(`u`.`last_name`,'') = ''),'',CONCAT(`u`.`last_name`,' '))) AS `name`,
  `u`.`avatar`             AS `avatar`,
  `u`.`person_color`       AS `person_color`,
  `u`.`id`                 AS `userId`,
  `s_in`.`state`           AS `sin_state`,
  `s_in`.`sign_date`       AS `signInDate`,
  `s_out`.`state`          AS `sout_state`,
  `s_out`.`sign_date`      AS `sinOutDate`,
  `r`.`id`                 AS `r_id`,
  `r`.`name`               AS `r_name`,
  `r`.`value`              AS `r_value`,
  `r`.`role_type`          AS `r_type`,
  `r`.`delete_flag`        AS `r_delete_flag`,
  `r`.`create_time`        AS `r_create_time`,
  `r`.`create_account_id`  AS `r_create_account_id`,
  `r`.`update_time`        AS `r_update_time`,
  `r`.`update_account_id`  AS `r_update_account_id`
FROM (((((((`tbl_roster_staff` `rs`
         JOIN `tbl_roster` `rr`
           ON ((`rr`.`id` = `rs`.`roster_id`)))
        LEFT JOIN `tbl_account` `a`
          ON ((`a`.`id` = `rs`.`staff_account_id`)))
       LEFT JOIN `tbl_user` `u`
         ON ((`u`.`id` = `a`.`user_id`)))
      LEFT JOIN `tbl_signin` `s_in`
        ON (((`s_in`.`account_id` = `rs`.`staff_account_id`)
             AND (`s_in`.`type` = 0)
             AND (`s_in`.`delete_flag` = 0)
             AND (`s_in`.`day` = `rs`.`day`)
             AND (`s_in`.`centre_id` = `rr`.`center_id`))))
     LEFT JOIN `tbl_signin` `s_out`
       ON (((`s_out`.`account_id` = `rs`.`staff_account_id`)
            AND (`s_out`.`type` = 1)
            AND (`s_out`.`delete_flag` = 0)
            AND (`s_out`.`day` = `rs`.`day`)
            AND (`s_out`.`centre_id` = `rr`.`center_id`))))
    LEFT JOIN `tbl_account_role` `ar`
      ON (((`ar`.`account_id` = `rs`.`staff_account_id`)
           AND (`ar`.`delete_flag` = 0)
           AND (ISNULL(`ar`.`temp_flag`)
                 OR (`ar`.`temp_flag` = 0)))))
   LEFT JOIN `tbl_role` `r`
     ON (((`r`.`id` = `ar`.`role_id`)
          AND (`r`.`delete_flag` = 0))))
WHERE (`rs`.`delete_flag` = 0)
ORDER BY `rs`.`day`,rs.staff_type,`rs`.`create_time` DESC,`rs`.`staff_account_id`)$$

DELIMITER ;