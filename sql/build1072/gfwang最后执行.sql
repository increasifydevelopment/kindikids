create or replace view v_leave_absence
as SELECT
	v.id,
	v.account_id,
	v.start_date AS begin_absence_date,
	v.end_date AS end_absence_date,
	v.reason AS absence_reason,
	v.create_time,
  0 as type
FROM
	tbl_leave v  
WHERE
	v.`status` = 1
AND v.leave_type = 0 and v.delete_flag=0
UNION all
SELECT
	i.id,
	r.account_id,
	i.begin_absence_date,
	i.end_absence_date,
	i.absence_reason,
	i.create_time,
  1 as type
FROM
	tbl_roster_leave_absence r
LEFT JOIN tbl_roster_leave_item i ON r.id = i.roster_leave_id
AND i.delete_flag = 0
WHERE
	r.delete_flag = 0

