/*
Navicat MySQL Data Transfer

Source Server         : aoyun
Source Server Version : 50627
Source Host           : 192.168.1.3:3306
Source Database       : kindkids_dev

Target Server Type    : MYSQL
Target Server Version : 50627
File Encoding         : 65001

Date: 2016-12-28 11:42:47
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_roster_leave_item
-- ----------------------------
DROP TABLE IF EXISTS `tbl_roster_leave_item`;
CREATE TABLE `tbl_roster_leave_item` (
  `id` varchar(50) NOT NULL,
  `roster_leave_id` varchar(50) DEFAULT NULL,
  `begin_absence_date` date DEFAULT NULL,
  `end_absence_date` date DEFAULT NULL,
  `absence_reason` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_account_id` varchar(50) DEFAULT NULL,
  `update_account_id` varchar(50) DEFAULT NULL,
  `delete_flag` smallint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
