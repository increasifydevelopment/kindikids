ALTER TABLE tbl_program_task_exgrsc ADD COLUMN old_id VARCHAR(50);

ALTER TABLE tbl_program_task_exgrsc MODIFY COLUMN reflection VARCHAR(6000);

ALTER TABLE tbl_program_lesson MODIFY lesson_name LONGTEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE tbl_program_lesson MODIFY learning LONGTEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE tbl_program_lesson MODIFY reflection LONGTEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

ALTER TABLE tbl_program_lesson  CHARSET=utf8mb4;