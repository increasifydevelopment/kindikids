ALTER TABLE `tbl_signin` ADD COLUMN `day` DATE;
UPDATE `tbl_signin` SET `day`=sign_date;

ALTER TABLE `tbl_absentee_request` ADD INDEX tbl_absentee_request_child_id (child_id);
ALTER TABLE `tbl_absentee_request` ADD INDEX tbl_absentee_request_state (state);

ALTER TABLE `tbl_signin` ADD INDEX tbl_signin_account_id(account_id);
ALTER TABLE `tbl_signin` ADD INDEX tbl_signin_type(`type`);
ALTER TABLE `tbl_signin` ADD INDEX tbl_signin_centre_id(`centre_id`);
ALTER TABLE `tbl_signin` ADD INDEX tbl_signin_room_id(`room_id`);
ALTER TABLE `tbl_signin` ADD INDEX tbl_signin_state(`state`);
ALTER TABLE `tbl_signin` ADD INDEX tbl_signin_delete_flag(`delete_flag`);
ALTER TABLE `tbl_signin` ADD INDEX tbl_signin_leave_flag(leave_flag);
ALTER TABLE `tbl_signin` ADD INDEX tbl_signin_sign_date(sign_date);
ALTER TABLE `tbl_signin` ADD INDEX tbl_signin_day(`day`);

ALTER TABLE `tbl_user` ADD INDEX tbl_user_user_type(user_type);
ALTER TABLE `tbl_user` ADD INDEX tbl_user_delete_flag(delete_flag);

ALTER TABLE `tbl_child_attendance` ADD INDEX tbl_child_attendance_user_id(`user_id`);
ALTER TABLE `tbl_child_attendance` ADD INDEX tbl_child_attendance_a(`monday`,`tuesday`,`thursday`,`friday`,`wednesday`);
ALTER TABLE `tbl_child_attendance` ADD INDEX tbl_child_attendance_enrolled(`enrolled`);
ALTER TABLE `tbl_child_attendance` ADD INDEX tbl_child_attendance_type(`type`);

ALTER TABLE tbl_sign_relation ADD INDEX tbl_sign_relation_obj_id (obj_id) ;
ALTER TABLE tbl_sign_relation ADD INDEX tbl_sign_relation_sign_id (`sign_id`) ;