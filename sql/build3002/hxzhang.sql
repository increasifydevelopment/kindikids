INSERT INTO tbl_function (
	id,
	NAME,
	target,
	menu_id,
	function_type
)
VALUES
(
	'a3f42b0b-58b3-11e8-a955-00e0703507b4',
	'waiting_position.do',
	'/waiting/position.do',
	'fe85639f-54ed-11e8-8348-00e0703507b4',
	2
);

INSERT INTO tbl_role_function (id, role_id, function_id)
VALUES
(
	'b3a3b54e-58b3-11e8-a955-00e0703507b4',
	'2e083440-49a3-11e6-b19e-00e0703507b4',
	'a3f42b0b-58b3-11e8-a955-00e0703507b4'
),
(
	'be4cf5d1-58b3-11e8-a955-00e0703507b4',
	'4586aa2b-49a3-11e6-b19e-00e0703507b4',
	'a3f42b0b-58b3-11e8-a955-00e0703507b4'
),
(
	'd3c5d7a1-621b-11e8-a955-00e0703507b4',
	'60a193d5-49a3-11e6-b19e-00e0703507b4',
	'a3f42b0b-58b3-11e8-a955-00e0703507b4'
);

INSERT INTO tbl_function (
	id,
	NAME,
	target,
	menu_id,
	function_type
)
VALUES
(
	'28456a61-59a2-11e8-a955-00e0703507b4',
	'waiting_submitMove.do',
	'/waiting/submitMove.do',
	'fe85639f-54ed-11e8-8348-00e0703507b4',
	2
);

INSERT INTO tbl_role_function (id, role_id, function_id)
VALUES
(
	'2f3c0790-59a2-11e8-a955-00e0703507b4',
	'2e083440-49a3-11e6-b19e-00e0703507b4',		
	'28456a61-59a2-11e8-a955-00e0703507b4'
),
(
	'341ee07d-59a2-11e8-a955-00e0703507b4',
	'4586aa2b-49a3-11e6-b19e-00e0703507b4',
	'28456a61-59a2-11e8-a955-00e0703507b4'
),(
	'f8bea769-621b-11e8-a955-00e0703507b4',
	'60a193d5-49a3-11e6-b19e-00e0703507b4', -- Second In Charge
	'28456a61-59a2-11e8-a955-00e0703507b4'
);
	
	
alter table tbl_child_attendance add siblingFlag bit default FALSE;

alter table tbl_child_attendance add positioningDate DATETIME;

UPDATE tbl_child_attendance SET positioningDate = create_time;

INSERT INTO tbl_function (
	id,
	NAME,
	target,
	menu_id,
	function_type
)
VALUES
(
	'ffd1642e-5c1f-11e8-a955-00e0703507b4',
	'waiting_familySelect.do',
	'/waiting/familySelect.do',
	'fe85639f-54ed-11e8-8348-00e0703507b4',
	2
);

INSERT INTO tbl_role_function (id, role_id, function_id)
VALUES
(
	'1497b48d-5c20-11e8-a955-00e0703507b4',
	'2e083440-49a3-11e6-b19e-00e0703507b4',		
	'ffd1642e-5c1f-11e8-a955-00e0703507b4'
),
(
	'190df7ae-5c20-11e8-a955-00e0703507b4',
	'4586aa2b-49a3-11e6-b19e-00e0703507b4',
	'ffd1642e-5c1f-11e8-a955-00e0703507b4'
),
(
	'1935062a-621c-11e8-a955-00e0703507b4',
	'60a193d5-49a3-11e6-b19e-00e0703507b4',
	'ffd1642e-5c1f-11e8-a955-00e0703507b4'
);

DELIMITER $$

DROP VIEW IF EXISTS `v_external_center_view`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_external_center_view` AS 
SELECT
  `u`.`id`           AS `user_id`,
  `o`.`center_id`    AS `center_id`,
  `ca`.`siblingFlag` AS `siblingFlag`
FROM (((`tbl_user` `u`
     LEFT JOIN `tbl_child_attendance` `ca`
       ON ((`u`.`id` = `ca`.`user_id`)))
    LEFT JOIN `tbl_account` `a`
      ON ((`u`.`id` = `a`.`user_id`)))
   LEFT JOIN `tbl_out_child_center` `o`
     ON ((`u`.`id` = `o`.`child_user_id`)))
WHERE ((`u`.`delete_flag` = 0)
       AND (`ca`.`delete_flag` = 0)
       AND (`ca`.`enrolled` = 0)
       AND (`ca`.`type` = 1)
       AND (`a`.`status` <> 1))$$

DELIMITER ;