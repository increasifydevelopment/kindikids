ALTER TABLE tbl_news_comment ADD is_parent bit;

UPDATE tbl_news_comment SET is_parent = TRUE WHERE comment_account_id IN (SELECT id FROM tbl_account WHERE user_id IN (SELECT id FROM tbl_user WHERE user_type = 1));

ALTER TABLE tbl_news ADD parent_comment bit;


UPDATE tbl_news
SET parent_comment = TRUE
WHERE
id IN (
SELECT
news_id
FROM
tbl_news_comment tnc
WHERE
EXISTS (
SELECT
1
FROM
tbl_news_comment tncc
GROUP BY
tncc.news_id
HAVING
MAX(tncc.create_time) = tnc.create_time
)
AND is_parent = TRUE
);

ALTER TABLE tbl_news ADD INDEX fulltext_parent_comment (parent_comment);

INSERT INTO tbl_function (
	id,
	NAME,
	target,
	menu_id,
	function_type
)
VALUES
(
	'58619468-e23a-11e8-96e0-408d5c9798a2',
	'news_feedback.do',
	'/news/feedback.do',
	'139e5b6c-0049-4279-beae-1dddfff7aa4e',
	1
);


SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_program_nqs
-- ----------------------------
DROP TABLE IF EXISTS `tbl_program_nqs`;
CREATE TABLE `tbl_program_nqs` (
  `id` varchar(50) DEFAULT NULL,
  `task_id` varchar(50) DEFAULT NULL,
  `nqs_version` varchar(50) DEFAULT NULL,
  `delete_flag` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_program_tags
-- ----------------------------
DROP TABLE IF EXISTS `tbl_program_tags`;
CREATE TABLE `tbl_program_tags` (
  `id` varchar(50) DEFAULT NULL,
  `task_id` varchar(50) DEFAULT NULL,
  `account_id` varchar(50) DEFAULT NULL,
  `delete_flag` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DELIMITER $$

ALTER ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_news_visibility` AS 
SELECT
  `n`.`id`                 AS `id`,
  `n`.`create_account_id`  AS `create_account_id`,
  `n`.`centers_id`         AS `centers_id`,
  `n`.`status`             AS `status`,
  `n`.`delete_flag`        AS `delete_flag`,
  `n`.`approve_time`       AS `approve_time`,
  `n`.`update_time`        AS `update_time`,
  `n`.`news_type`          AS `news_type`,
  `n`.`score`              AS `score`,
  `n`.`create_time`        AS `create_time`,
  `n`.`room_id`            AS `room_id`,
  `r`.`receive_account_id` AS `receive_account_id`,
  `n`.`parent_comment`     AS `parent_comment`
FROM (`tbl_news` `n`
   LEFT JOIN `tbl_news_receive` `r`
     ON (((`n`.`id` = `r`.`news_id`)
          AND (`r`.`delete_flag` = 0))))$$

DELIMITER ;


DELIMITER $$

ALTER ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_news_tag2` AS 
SELECT
  `n`.`id`                AS `id`,
  `n`.`create_account_id` AS `create_account_id`,
  `n`.`centers_id`        AS `centers_id`,
  `n`.`status`            AS `status`,
  `n`.`delete_flag`       AS `delete_flag`,
  `n`.`approve_time`      AS `approve_time`,
  `n`.`update_time`       AS `update_time`,
  `n`.`news_type`         AS `news_type`,
  `n`.`score`             AS `score`,
  `n`.`create_time`       AS `create_time`,
  `n`.`room_id`           AS `room_id`,
  `a`.`account_id`        AS `account_id`,
  `n`.`parent_comment`    AS `parent_comment`
FROM (`tbl_news` `n`
   LEFT JOIN `tbl_news_accounts` `a`
     ON (((`n`.`id` = `a`.`news_id`)
          AND (`a`.`delete_flag` = 0))))$$

DELIMITER ;


DELIMITER $$

ALTER ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_news_group` AS 
SELECT
  `n`.`id`                AS `id`,
  `n`.`create_account_id` AS `create_account_id`,
  `n`.`centers_id`        AS `centers_id`,
  `n`.`status`            AS `status`,
  `n`.`delete_flag`       AS `delete_flag`,
  `n`.`approve_time`      AS `approve_time`,
  `n`.`update_time`       AS `update_time`,
  `n`.`news_type`         AS `news_type`,
  `n`.`score`             AS `score`,
  `n`.`create_time`       AS `create_time`,
  `n`.`room_id`           AS `room_id`,
  `g`.`group_name`        AS `group_id`,
  `g`.`group_center_id`   AS `group_center_id`,
  `n`.`parent_comment`    AS `parent_comment`
FROM (`tbl_news` `n`
   LEFT JOIN `tbl_news_group` `g`
     ON (((`n`.`id` = `g`.`news_id`)
          AND (`g`.`delete_flag` = 0))))$$

DELIMITER ;
