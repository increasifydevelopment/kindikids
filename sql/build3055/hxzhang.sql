ALTER TABLE tbl_news_accounts ADD username varchar(255);

ALTER TABLE tbl_news_content ADD FULLTEXT INDEX fulltext_content (content);

ALTER TABLE tbl_news_accounts ADD FULLTEXT INDEX fulltext_username (username);


update tbl_news_accounts t1 LEFT JOIN v_user_account_query t2 on t1.`account_id` = t2.`account_id` set t1.username=t2.user_name;


ALTER TABLE `tbl_user` ADD INDEX tbl_user_family_id (`family_id`);