DELIMITER $$

ALTER ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_user_account_query` AS (
select
  concat(if((ifnull(`u`.`first_name`,'') = ''),'',concat(`u`.`first_name`,' ')),if((ifnull(`u`.`middle_name`,'') = ''),'',concat(`u`.`middle_name`,' ')),if((ifnull(`u`.`last_name`,'') = ''),'',concat(`u`.`last_name`,' '))) AS `user_name`,
  `u`.`email`        AS `user_email`,
  `u`.`centers_id`   AS `centers_id`,
  `u`.`room_id`      AS `room_id`,
  `a`.`id`           AS `account_id`,
  `u`.`id`           AS `user_id`,
  `a`.`status`       AS `STATUS`,
  `u`.`avatar`       AS `avatar`,
  `u`.`user_type`    AS `user_type`,
  `u`.`group_id`     AS `group_id`,
  `u`.`delete_flag`  AS `delete_flag`,
  `u`.`person_color` AS `person_color`,
  `u`.`family_id`    AS `family_id`,
  `u`.`create_time`  AS `create_time`,
  `u`.`update_time`  AS `update_time`,
  `u`.`staff_type`   AS `staff_type`
from (`tbl_account` `a`
   left join `tbl_user` `u`
     on ((`a`.`user_id` = `u`.`id`))))$$

DELIMITER ;





DELIMITER $$

ALTER ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_leave_absence` AS 
SELECT
  `v`.`id`           AS `id`,
  `v`.`account_id`   AS `account_id`,
  `v`.`start_date`   AS `begin_absence_date`,
  `v`.`end_date`     AS `end_absence_date`,
  `v`.`leave_reason` AS `absence_reason`,
  `v`.`create_time`  AS `create_time`,
  0                  AS `type`
FROM (`tbl_leave` `v`
   LEFT JOIN `v_user_account_query` `u`
     ON ((`v`.`account_id` = `u`.`account_id`)))
WHERE ((`v`.`status` = 1)
       AND (`v`.`leave_type` = 0)
       AND (`v`.`delete_flag` = 0)
       AND (`u`.`staff_type` IN(0,1))
       AND (`u`.`staff_type` IS NOT NULL))UNION ALL SELECT
                                                      `i`.`id`                 AS `id`,
                                                      `r`.`account_id`         AS `account_id`,
                                                      `i`.`begin_absence_date` AS `begin_absence_date`,
                                                      `i`.`end_absence_date`   AS `end_absence_date`,
                                                      `i`.`absence_reason`     AS `absence_reason`,
                                                      `i`.`create_time`        AS `create_time`,
                                                      1                        AS `type`
                                                    FROM (`tbl_roster_leave_absence` `r`
                                                       LEFT JOIN `tbl_roster_leave_item` `i`
                                                         ON (((`r`.`id` = `i`.`roster_leave_id`)
                                                              AND (`i`.`delete_flag` = 0))))
                                                    WHERE (`r`.`delete_flag` = 0)$$

DELIMITER ;