	INSERT INTO tbl_function (
	id,
	NAME,
	target,
	menu_id,
	function_type
)
VALUES
	(
		'77c417ee-f2e0-11e8-b0fe-408d5c9798a2',
		'rosterstaff_saveClosurePeriod',
		'/rosterstaff/saveClosurePeriod.do',
		'3ea7375f-7890-11e6-85fd-00e0703507b4',
		2
	);



INSERT INTO tbl_role_function (id, role_id, function_id)
VALUES
	(
		'84a28b0e-f2e0-11e8-b0fe-408d5c9798a2',
		'2e083440-49a3-11e6-b19e-00e0703507b4',
		'77c417ee-f2e0-11e8-b0fe-408d5c9798a2'
	),
	(
		'8a0a8457-f2e0-11e8-b0fe-408d5c9798a2',
		'4586aa2b-49a3-11e6-b19e-00e0703507b4',
		'77c417ee-f2e0-11e8-b0fe-408d5c9798a2'
	),
	(
		'8de94775-f2e0-11e8-b0fe-408d5c9798a2',
		'60a193d5-49a3-11e6-b19e-00e0703507b4',
		'77c417ee-f2e0-11e8-b0fe-408d5c9798a2'
	);
	
	
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_closure_period
-- ----------------------------
DROP TABLE IF EXISTS `tbl_closure_period`;
CREATE TABLE `tbl_closure_period` (
  `id` varchar(50) DEFAULT NULL,
  `choose_centre` varchar(50) DEFAULT NULL,
  `date_from` datetime DEFAULT NULL,
  `date_to` datetime DEFAULT NULL,
  `name` varchar(500) DEFAULT NULL,
  `holiday` bit(1) DEFAULT NULL,
  `delete_flag` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;	


SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_closure_period_centres
-- ----------------------------
DROP TABLE IF EXISTS `tbl_closure_period_centres`;
CREATE TABLE `tbl_closure_period_centres` (
  `closure_period_id` varchar(50) DEFAULT NULL,
  `centre_id` varchar(50) DEFAULT NULL,
  `delete_flag` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
