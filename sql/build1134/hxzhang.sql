INSERT INTO tbl_function (
	id,
	NAME,
	target,
	menu_id,
	function_type
)
VALUES
	(
		'e31b6994-1e4f-11e7-8553-00e0703507b4',
		'task_getWhenRequiredTasks2',
		'/task/getWhenRequiredTasks2.do',
		'2321e129-8ea8-11e6-85fd-00e0703507b4',
		2
	);

INSERT INTO tbl_role_function (id, role_id, function_id)
VALUES
	(
		'0ef522f2-1e50-11e7-8553-00e0703507b4',
		'2e083440-49a3-11e6-b19e-00e0703507b4',
		'e31b6994-1e4f-11e7-8553-00e0703507b4'
	),(
		'157e0af6-1e50-11e7-8553-00e0703507b4',
		'4586aa2b-49a3-11e6-b19e-00e0703507b4',
		'e31b6994-1e4f-11e7-8553-00e0703507b4'
	),
(
		'1bf50d76-1e50-11e7-8553-00e0703507b4',
		'51c32196-49a3-11e6-b19e-00e0703507b4',
		'e31b6994-1e4f-11e7-8553-00e0703507b4'
	),
(
		'25d5b7da-1e50-11e7-8553-00e0703507b4',
		'74ba21a6-49a3-11e6-b19e-00e0703507b4',
		'e31b6994-1e4f-11e7-8553-00e0703507b4'
	);
