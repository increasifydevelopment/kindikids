/*
Navicat MySQL Data Transfer

Source Server         : 192.168.1.18
Source Server Version : 50636
Source Host           : 192.168.1.18:3306
Source Database       : kindikids_test

Target Server Type    : MYSQL
Target Server Version : 50636
File Encoding         : 65001

Date: 2019-01-28 17:16:29
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_holiday_pay
-- ----------------------------
DROP TABLE IF EXISTS `tbl_holiday_pay`;
CREATE TABLE `tbl_holiday_pay` (
  `id` varchar(50) NOT NULL,
  `account_id` varchar(50) DEFAULT NULL,
  `employ_id` varchar(50) DEFAULT NULL,
  `day` datetime DEFAULT NULL,
  `hours` decimal(10,3) DEFAULT NULL,
  `role` varchar(50) DEFAULT NULL,
  `staff_type` smallint(6) DEFAULT NULL,
  `centre_id` varchar(50) DEFAULT NULL,
  `delete_flag` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
