INSERT INTO tbl_function (
id,
NAME,
target,
menu_id,
function_type
)
VALUES
(
'2f1224b6-14ba-11e9-b05f-408d5c9798a2',
'rosterstaff_payrollExport',
'/rosterstaff/payrollExport.do',
NULL,
2
);

INSERT INTO tbl_role_function (id, role_id, function_id)
VALUES
(
'a4a485dd-14ba-11e9-b05f-408d5c9798a2',
'2e083440-49a3-11e6-b19e-00e0703507b4',
'2f1224b6-14ba-11e9-b05f-408d5c9798a2'
);


INSERT INTO tbl_function (
id,
NAME,
target,
menu_id,
function_type
)
VALUES
(
'5fcd4ef4-188a-11e9-b05f-408d5c9798a2',
'job_dealCal.do',
'/job/dealCal.do',
NULL,
2
);

INSERT INTO tbl_role_function (id, role_id, function_id)
VALUES
(
'6d1ce4b4-188a-11e9-b05f-408d5c9798a2',
'2e083440-49a3-11e6-b19e-00e0703507b4',
'5fcd4ef4-188a-11e9-b05f-408d5c9798a2'
);


alter table tbl_leave add staff_type  SMALLINT(1);

UPDATE tbl_leave l SET l.staff_type = (SELECT u.staff_type FROM tbl_account a LEFT JOIN tbl_user u ON a.user_id = u.id WHERE l.account_id = a.id);


INSERT INTO tbl_function (
id,
NAME,
target,
menu_id,
function_type
)
VALUES
(
'dcbc683a-1ad0-11e9-85dd-408d5c9798a2',
'job_dealClosure.do',
'/job/dealClosure.do',
NULL,
2
);

INSERT INTO tbl_role_function (id, role_id, function_id)
VALUES
(
'eb94ab7b-1ad0-11e9-85dd-408d5c9798a2',
'2e083440-49a3-11e6-b19e-00e0703507b4',
'dcbc683a-1ad0-11e9-85dd-408d5c9798a2'
);