DELIMITER $$

DROP VIEW IF EXISTS `v_room_change_report`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_room_change_report` AS (
SELECT
  `rc`.`child_id` AS `child_id`,
  `rc`.`room_id`  AS `room_id`,
  SUM((CASE WHEN (`rc`.`operate` = 0) THEN 1 WHEN (`rc`.`operate` = 1) THEN -(1) WHEN (`rc`.`operate` = 2) THEN 1 WHEN (`rc`.`operate` = 3) THEN -(1) END)) AS `size`
FROM `tbl_room_change` `rc`
WHERE ((`rc`.`delete_flag` = 0)
       AND (`rc`.`day_week` = (DAYOFWEEK(NOW()) - 1))
       AND (`rc`.`day` = NOW()))
GROUP BY `rc`.`child_id`)$$

DELIMITER ;