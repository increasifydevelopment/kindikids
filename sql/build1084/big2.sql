DELIMITER $$

DROP VIEW IF EXISTS `v_room_change_report_deal`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `v_room_change_report_deal` AS (
SELECT
  `tbl`.`child_id` AS `child_id`,
  `tbl`.`room_id`  AS `room_id`,
  (CASE WHEN (`tbl`.`size` > 0) THEN 1 WHEN (`tbl`.`size` < 0) THEN -(1) ELSE 0 END) AS `size`
FROM `v_room_change_report` `tbl`)$$

DELIMITER ;