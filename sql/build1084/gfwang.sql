DROP INDEX idx_relation_kid_parent_kid_account_id ON tbl_relation_kid_parent;
 
 DROP INDEX idx_relation_kid_parent_account_id ON tbl_relation_kid_parent;
 
 CREATE INDEX idx_attach_source_id ON tbl_attachment (source_id);
 
 
 
ALTER TABLE tbl_message MODIFY subject LONGTEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

ALTER TABLE tbl_message  CHARSET=utf8mb4;


ALTER TABLE tbl_message_content MODIFY content LONGTEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

ALTER TABLE tbl_message_content  CHARSET=utf8mb4;

