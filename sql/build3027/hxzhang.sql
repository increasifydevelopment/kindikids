DELIMITER $$

DROP VIEW IF EXISTS `view_waiting_child_center`$$

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `view_waiting_child_center` AS 
SELECT
  `app`.`id`      AS `obj_id`,
  `o`.`center_id` AS `center_id`,
  `c`.`name`      AS `name`
FROM ((`tbl_application_list` `app`
    LEFT JOIN `tbl_out_child_center` `o`
      ON ((`app`.`id` = `o`.`child_user_id`)))
   LEFT JOIN `tbl_centers` `c`
     ON ((`o`.`center_id` = `c`.`id`)))
WHERE (`app`.`status` IN(0,3))UNION ALL SELECT
                                          `u`.`id`         AS `obj_id`,
                                          `o`.`center_id`  AS `center_id`,
                                          `c`.`name`       AS `name`
                                        FROM ((((`tbl_user` `u`
                                              LEFT JOIN `tbl_child_attendance` `ca`
                                                ON ((`u`.`id` = `ca`.`user_id`)))
                                             LEFT JOIN `tbl_account` `a`
                                               ON ((`u`.`id` = `a`.`user_id`)))
                                            LEFT JOIN `tbl_out_child_center` `o`
                                              ON ((`u`.`id` = `o`.`child_user_id`)))
                                           LEFT JOIN `tbl_centers` `c`
                                             ON ((`o`.`center_id` = `c`.`id`)))
                                        WHERE ((`u`.`delete_flag` = 0)
                                               AND (`ca`.`delete_flag` = 0)
                                               AND (`ca`.`enrolled` = 0)
                                               AND (`ca`.`type` = 1)
                                               AND (`a`.`status` <> 1))$$

DELIMITER ;