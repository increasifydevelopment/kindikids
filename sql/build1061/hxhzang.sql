SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_out_child_center
-- ----------------------------
DROP TABLE IF EXISTS `tbl_out_child_center`;
CREATE TABLE `tbl_out_child_center` (
  `id` varchar(50) NOT NULL,
  `child_user_id` varchar(50) DEFAULT NULL,
  `center_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_out_relevant
-- ----------------------------
DROP TABLE IF EXISTS `tbl_out_relevant`;
CREATE TABLE `tbl_out_relevant` (
  `id` varchar(50) NOT NULL,
  `child_user_id` varchar(50) DEFAULT NULL,
  `relationship1` varchar(255) DEFAULT NULL,
  `relationship2` varchar(255) DEFAULT NULL,
  `nationality` varchar(255) DEFAULT NULL,
  `language` varchar(255) DEFAULT NULL,
  `religion` varchar(255) DEFAULT NULL,
  `has_immunised` smallint(1) DEFAULT NULL,
  `has_anaphylaxis` smallint(1) DEFAULT NULL,
  `learning_difficulties` varchar(255) DEFAULT NULL,
  `medical_conditions` varchar(255) DEFAULT NULL,
  `dietary_requipments` varchar(255) DEFAULT NULL,
  `allergies` varchar(255) DEFAULT NULL,
  `proposal` varchar(255) DEFAULT NULL,
  `channel` smallint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;