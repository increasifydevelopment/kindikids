DELIMITER $$

USE `kindkids_test`$$

DROP PROCEDURE IF EXISTS `Pro_SynLogState`$$

CREATE DEFINER=`root`@`%` PROCEDURE `Pro_SynLogState`(IN childId VARCHAR(50))
BEGIN
	UPDATE `tbl_request_log` l LEFT JOIN `tbl_absentee_request` ar ON ar.`id`=l.`obj_id` 
	SET l.`state`=
		    (CASE
		      WHEN ar.`state` = 0 
		      THEN  1 
		      ELSE  ar.`state` 
		    END)
	WHERE l.`child_id`=childId AND ar.`id` IS NOT NULL AND l.`type`=5;
	
	UPDATE `tbl_request_log` l LEFT JOIN `tbl_giving_notice` gn ON gn.`id`=l.`obj_id` 
	SET l.`state`=
		    (CASE
		      WHEN gn.`state` = 0 
		      THEN  1 
		      ELSE  gn.`state` 
		    END)
	WHERE l.`child_id`=childId AND gn.`id` IS NOT NULL AND l.`type`=4;
	
	UPDATE `tbl_request_log` l LEFT JOIN `tbl_change_attendance_request` ca ON ca.`id`=l.`obj_id` 
	SET l.`state`=
		    (CASE
		      WHEN (ca.`state` = 0 OR ca.state=2)
		      THEN  1 
		      ELSE  ca.`state` 
		    END)
	WHERE l.`child_id`=childId AND ca.`id` IS NOT NULL AND l.`type`IN(0,1,2);
	
	UPDATE tbl_giving_notice SET state = 7 WHERE child_id = childId;
	
	UPDATE tbl_absentee_request SET state = 7 WHERE child_id =childId;
	
	UPDATE tbl_change_attendance_request SET  state=7 WHERE child_id =childId;
	
	UPDATE tbl_subtracted_attendance SET delete_flag=7  WHERE child_id =childId;
	
	UPDATE tbl_temporary_attendance SET delete_flag=7 WHERE child_id =childId;
    END$$

DELIMITER ;


ALTER TABLE `tbl_request_log` DROP COLUMN opera_flag;

ALTER TABLE `tbl_request_log` ADD COLUMN state SMALLINT DEFAULT NULL COMMENT '历史状态记录显示 用于存日志历史';