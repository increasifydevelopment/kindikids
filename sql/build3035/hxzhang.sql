DROP VIEW IF EXISTS vv_1_task CASCADE;

CREATE
DEFINER = 'root'@'%'
VIEW vv_1_task
AS
SELECT
  `i`.`id` AS `id`,
  `t`.`task_name` AS `task_name`
FROM (`tbl_task_instance` `i`
  JOIN `tbl_task` `t`
    ON (((`i`.`task_type` = 17)
    AND (`i`.`delete_flag` = 0)
    AND (`t`.`delete_flag` = 0)
    AND (`t`.`id` = `i`.`task_model_id`))));
    

    
    
DROP VIEW IF EXISTS vv_2_task CASCADE;

CREATE
DEFINER = 'root'@'%'
VIEW vv_2_task
AS
(SELECT
    `i`.`id` AS `id`,
    CONCAT(`cc`.`name`, ' ', `room`.`name`) AS `task_name`
  FROM (((`tbl_task_instance` `i`
    JOIN `tbl_program_task_exgrsc` `exgrsc`
      ON (((`i`.`delete_flag` = 0)
      AND (`i`.`task_type` IN (0, 1, 3))
      AND (`exgrsc`.`id` = `i`.`value_id`)
      AND (`exgrsc`.`delete_flag` = 0))))
    LEFT JOIN `tbl_centers` `cc`
      ON ((`cc`.`id` = `i`.`centre_id`)))
    LEFT JOIN `tbl_room` `room`
      ON ((`room`.`id` = `i`.`room_id`))));
      
      
      
      
DROP VIEW IF EXISTS vv_3_task CASCADE;

CREATE
DEFINER = 'root'@'%'
VIEW vv_3_task
AS
(SELECT
    `i`.`id` AS `id`,
    CONCAT(`cc`.`name`, ' ', `room`.`name`) AS `task_name`
  FROM (((`tbl_task_instance` `i`
    JOIN `tbl_program_lesson` `lesson`
      ON (((`i`.`task_type` = 2)
      AND (`i`.`delete_flag` = 0)
      AND (`lesson`.`delete_flag` = 0)
      AND (`lesson`.`id` = `i`.`value_id`))))
    LEFT JOIN `tbl_centers` `cc`
      ON ((`cc`.`id` = `i`.`centre_id`)))
    LEFT JOIN `tbl_room` `room`
      ON ((`room`.`id` = `i`.`room_id`))));
      
    
      
DROP VIEW IF EXISTS vv_4_task CASCADE;

CREATE
DEFINER = 'root'@'%'
VIEW vv_4_task
AS
(SELECT
    `i`.`id` AS `id`,
    CONCAT(`GetFullName`(`u`.`first_name`, `u`.`middle_name`, `u`.`last_name`), CONVERT((CASE WHEN (`i`.`task_type` = 4) THEN '\'s Interest' WHEN (`i`.`task_type` = 6) THEN '\'s Observation' WHEN (`i`.`task_type` = 8) THEN '\'s Learning Story' END) USING utf8)) AS `task_name`
  FROM (((`tbl_task_instance` `i`
    JOIN `tbl_program_intobslea` `intobslea`
      ON (((`i`.`task_type` IN (4, 6, 8))
      AND (`i`.`delete_flag` = 0)
      AND (`intobslea`.`delete_flag` = 0)
      AND (`intobslea`.`id` = `i`.`value_id`))))
    LEFT JOIN `tbl_account` `account`
      ON ((`account`.`id` = `intobslea`.`child_id`)))
    LEFT JOIN `tbl_user` `u`
      ON ((`u`.`id` = `account`.`user_id`))));


DROP VIEW IF EXISTS vv_5_task CASCADE;

CREATE
DEFINER = 'root'@'%'
VIEW vv_5_task
AS
(SELECT
    `i`.`id` AS `id`,
    CONCAT(CONCAT(`GetFullName`(`u`.`first_name`, `u`.`middle_name`, `u`.`last_name`), CONVERT((CASE WHEN (`i`.`task_type` = 5) THEN '\'s Interest Follow Up' WHEN (`i`.`task_type` = 7) THEN '\'s Observation Follow Up' WHEN (`i`.`task_type` = 9) THEN '\'s Learning Story Follow Up' END) USING utf8))) AS `task_name`
  FROM ((((`tbl_task_instance` `i`
    JOIN `tbl_task_program_follow_up` `follow`
      ON (((`i`.`task_type` IN (5, 7, 9))
      AND (`i`.`delete_flag` = 0)
      AND (`follow`.`delete_flag` = 0)
      AND (`follow`.`id` = `i`.`value_id`))))
    LEFT JOIN `tbl_program_intobslea` `intobslea`
      ON ((`follow`.`task_id` = `intobslea`.`id`)))
    LEFT JOIN `tbl_account` `account`
      ON ((`account`.`id` = `intobslea`.`child_id`)))
    LEFT JOIN `tbl_user` `u`
      ON ((`u`.`id` = `account`.`user_id`)))
  WHERE (`u`.`delete_flag` = 0));  



DROP VIEW IF EXISTS vv_6_task CASCADE;

CREATE
DEFINER = 'root'@'%'
VIEW vv_6_task
AS
(SELECT
    `i`.`id` AS `id`,
    (CASE WHEN ((`register`.`type` = 13) OR
        (`register`.`type` = 14)) THEN CONCAT(`cc`.`name`, ' ', `room`.`name`, ' ', CONVERT(DATE_FORMAT(`register`.`day`, '%h:%i %p') USING utf8)) ELSE CONCAT(`cc`.`name`, ' ', `room`.`name`) END) AS `task_name`
  FROM (((`tbl_task_instance` `i`
    JOIN `tbl_program_register_task` `register`
      ON (((`i`.`task_type` IN (11, 12, 13, 14, 15))
      AND (`i`.`delete_flag` = 0)
      AND (`register`.`delete_flag` = 0)
      AND (`register`.`id` = `i`.`value_id`))))
    LEFT JOIN `tbl_centers` `cc`
      ON ((`cc`.`id` = `i`.`centre_id`)))
    LEFT JOIN `tbl_room` `room`
      ON ((`room`.`id` = `i`.`room_id`))));
      
      
      
DROP VIEW IF EXISTS vv_7_task CASCADE;

CREATE
DEFINER = 'root'@'%'
VIEW vv_7_task
AS
(SELECT
    `i`.`id` AS `id`,
    CONCAT(`cc`.`name`, ' ', `room`.`name`, ' ', CONVERT(IF((`weekly`.`type` = 0), 'Weekly Evaluation Explore Curriculum', 'Weekly Evaluation Grow Curriculum') USING utf8)) AS `task_name`
  FROM (((`tbl_task_instance` `i`
    JOIN `tbl_program_weekly_evalution` `weekly`
      ON ((`weekly`.`id` = `i`.`value_id`)))
    LEFT JOIN `tbl_centers` `cc`
      ON ((`cc`.`id` = `i`.`centre_id`)))
    LEFT JOIN `tbl_room` `room`
      ON ((`room`.`id` = `i`.`room_id`)))
  WHERE ((`i`.`task_type` = 10)
  AND (`i`.`delete_flag` = 0)
  AND (`weekly`.`delete_flag` = 0)));
  
  
  
  
DROP VIEW IF EXISTS vv_8_task CASCADE;

CREATE
DEFINER = 'root'@'%'
VIEW vv_8_task
AS
(SELECT
    `i`.`id` AS `id`,
    CONCAT(`cc`.`name`, ' ', `room`.`name`, ' ', `GetFullName`(`u`.`first_name`, `u`.`middle_name`, `u`.`last_name`)) AS `task_name`
  FROM (((((`tbl_task_instance` `i`
    JOIN `tbl_program_medication` `medication`
      ON ((`medication`.`id` = `i`.`value_id`)))
    LEFT JOIN `tbl_centers` `cc`
      ON ((`cc`.`id` = `i`.`centre_id`)))
    LEFT JOIN `tbl_room` `room`
      ON ((`room`.`id` = `i`.`room_id`)))
    LEFT JOIN `tbl_account` `a`
      ON ((`a`.`id` = `medication`.`child_account_id`)))
    LEFT JOIN `tbl_user` `u`
      ON ((`u`.`id` = `a`.`user_id`)))
  WHERE ((`i`.`task_type` = 18)
  AND (`i`.`delete_flag` = 0)
  AND (`medication`.`delete_flag` = 0)));  
  
  
  
DROP VIEW IF EXISTS vv_9_task CASCADE;

CREATE
DEFINER = 'root'@'%'
VIEW vv_9_task
AS
(SELECT
    `i`.`id` AS `id`,
    CONCAT(`t`.`task_name`, '-follow up') AS `task_name`
  FROM ((`tbl_task_instance` `i`
    JOIN `tbl_task_follow_instance` `tfi`
      ON ((`tfi`.`task_instance_id` = `i`.`id`)))
    JOIN `tbl_task` `t`
      ON ((`t`.`id` = `i`.`task_model_id`)))
  WHERE ((`i`.`task_type` = 17)
  AND (`i`.`delete_flag` = 0)
  AND (`tfi`.`delete_flag` = 0)
  AND (`t`.`delete_flag` = 0)));
  
  
  
  
DROP VIEW IF EXISTS vv_10_task CASCADE;

CREATE
DEFINER = 'root'@'%'
VIEW vv_10_task
AS
(SELECT DISTINCT
    `i`.`id` AS `id`,
    `v`.`name` AS `task_name`
  FROM (`tbl_task_instance` `i`
    JOIN `v_meet_task_view` `v`
      ON ((`v`.`row_id` = `i`.`value_id`)))
  WHERE ((`i`.`task_type` = 20)
  AND (`i`.`delete_flag` = 0)));  
  
  
  
DROP VIEW IF EXISTS vv_11_task CASCADE;

CREATE
DEFINER = 'root'@'%'
VIEW vv_11_task
AS
(SELECT DISTINCT
    `i`.`id` AS `id`,
    `GetFullName`(`u`.`first_name`, `u`.`middle_name`, `u`.`last_name`) AS `task_name`
  FROM ((`tbl_task_instance` `i`
    JOIN `tbl_account` `a`
      ON ((`a`.`id` = `i`.`value_id`)))
    JOIN `tbl_user` `u`
      ON ((`u`.`id` = `a`.`user_id`)))
  WHERE ((`i`.`task_type` IN (21, 22))
  AND (`i`.`delete_flag` = 0)));  