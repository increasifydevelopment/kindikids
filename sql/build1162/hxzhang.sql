ALTER TABLE tbl_child_attendance add COLUMN orientation datetime;


ALTER TABLE tbl_child_attendance add COLUMN request_date datetime;


ALTER TABLE tbl_request_log add COLUMN log_type smallint;




/*
Navicat MySQL Data Transfer

Source Server         : 192.168.1.3
Source Server Version : 50627
Source Host           : 192.168.1.3:3306
Source Database       : kindkids_dev

Target Server Type    : MYSQL
Target Server Version : 50627
File Encoding         : 65001

Date: 2017-05-24 15:56:10
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_email_msg
-- ----------------------------
DROP TABLE IF EXISTS `tbl_email_msg`;
CREATE TABLE `tbl_email_msg` (
  `id` varchar(50) NOT NULL,
  `user_id` varchar(50) DEFAULT NULL,
  `msg` varchar(500) DEFAULT NULL,
  `type` smallint(1) DEFAULT NULL,
  `delete_flag` smallint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET FOREIGN_KEY_CHECKS=1;
