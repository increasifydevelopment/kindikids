package com.aoyuntek.framework.deserializer;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;

/**
 * @description 自定义返回值json日期格式 dd/MM/yyyy
 * @author xdwang
 * @create 2016年1月16日下午10:24:31
 * @version 1.0
 */
public class DateJsonSerializer2 extends JsonSerializer<Date> {

    @Override
    public void serialize(Date value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        String formattedDate = formatter.format(value);
        jgen.writeString(formattedDate);
    }
}