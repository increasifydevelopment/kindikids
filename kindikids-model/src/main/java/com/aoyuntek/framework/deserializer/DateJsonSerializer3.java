package com.aoyuntek.framework.deserializer;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;

/**
 * @description 将时间转换成"dd/MM/yyyy'T'HH:mm:ss.SSS"
 * @author Hxzhang 2016年9月8日上午11:10:28
 * @version 1.0
 */
public class DateJsonSerializer3 extends JsonSerializer<Date> {
    public static final SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy'T'HH:mm:ss.SSS");

    @Override
    public void serialize(Date arg0, JsonGenerator arg1, SerializerProvider arg2) throws IOException, JsonProcessingException {
        arg1.writeString(format.format(arg0));
    }
}
