package com.aoyuntek.framework.deserializer;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.aoyuntek.aoyun.constants.S3Constants;
import com.aoyuntek.framework.constants.PropertieNameConts;
import com.theone.aws.util.FileFactory;
import com.theone.string.util.StringUtil;

public class S3JsonSerializer extends JsonSerializer<String> {

    @Override
    public void serialize(String s3ObjjectKey, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException,
            JsonProcessingException {
        if (StringUtil.isNotEmpty(s3ObjjectKey)) {
            jsonGenerator.writeString(new FileFactory(Region.getRegion(Regions.AP_SOUTHEAST_2),PropertieNameConts.S3).generateUrl(S3Constants.BUCKET_NAME, s3ObjjectKey, S3Constants.URL_TIMEOUT_HOUR));
        } else {
            jsonGenerator.writeString(s3ObjjectKey);
        }
    }
}