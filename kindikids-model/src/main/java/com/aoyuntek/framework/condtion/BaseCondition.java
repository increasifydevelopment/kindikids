package com.aoyuntek.framework.condtion;

import java.util.Date;

import com.aoyuntek.framework.constants.PropertieNameConts;
import com.theone.date.util.DateUtil;
import com.theone.resource.util.PropertiesCacheUtil;

/**
 * @description 前后台交互的基本条件，查询基类
 * @author xdwang
 * @create 2015年11月28日下午2:27:19
 * @version 1.0
 */
public class BaseCondition {

	private Date now = DateUtil.parse(DateUtil.format(new Date(), DateUtil.yyyyMMdd), DateUtil.yyyyMMdd);

	public Date getNow() {
		return now;
	}

	public void setNow(Date now) {
		this.now = now;
	}

	public BaseCondition() {
		super();
	}

	public void initAll() {
		this.pageSize = Integer.MAX_VALUE;
		this.pageIndex = 0;
	}

	/**
	 * 分页从哪一条记录开始
	 */
	private int startSize = 0;
	/**
	 * 每页条数
	 */
	// CSOFF: MagicNumber
	private int pageSize = PropertiesCacheUtil.getIntValue("default_page_size", PropertieNameConts.SYSTEM);

	/**
	 * 当前第几页
	 */
	private int pageIndex;

	/**
	 * 总记录数
	 */
	private int totalSize;

	/**
	 * 总页数
	 */
	private int totalPageCount;

	/**
	 * 排序字段
	 */
	private String sortName;
	/**
	 * 排序方式（升序、降序）
	 */
	private String sortOrder;

	/**
	 * @description 获取开始记录行
	 * @author xdwang
	 * @create 2016年1月6日下午8:04:32
	 * @version 1.0
	 * @return 开始记录行
	 */
	public int getStartSize() {
		if (startSize != -1) {
			return startSize;
		}
		startSize = this.getPageSize() * (this.getPageIndex() - 1);
		return startSize;
	}

	/**
	 * @description 设置起始页
	 * @author xdwang
	 * @create 2016年1月24日上午11:04:27
	 * @version 1.0
	 * @param pageIndex
	 *            pageIndex
	 */
	public void setStartSize(int pageIndex) {
		if (pageIndex == 0) {
			pageIndex = 1;
		}
		this.startSize = this.getPageSize() * (pageIndex - 1);
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getTotalSize() {
		return totalSize;
	}

	/**
	 * @description 设置总记录数和总页数
	 * @author xdwang
	 * @create 2016年1月6日下午8:05:03
	 * @version 1.0
	 * @param totalSize
	 *            总记录数
	 */
	public void setTotalSize(int totalSize) {
		this.totalSize = totalSize;
		this.totalPageCount = totalSize / pageSize;
		if (totalSize % pageSize != 0) {
			this.totalPageCount += 1;
		}
	}

	public int getTotalPageCount() {
		return totalPageCount;
	}

	public void setTotalPageCount(int totalPageCount) {
		this.totalPageCount = totalPageCount;
	}

	public String getSortName() {
		return sortName;
	}

	public void setSortName(String sortName) {
		this.sortName = sortName;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	public int getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}

	protected boolean search = false;

	public boolean isSearch() {
		return search;
	}

	public void setSearch(boolean search) {
		this.search = search;
	}
}
