package com.aoyuntek.framework.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * @description 基础实体类
 * @author xdwang
 * @create 2015年12月13日下午2:01:34
 * @version 1.0
 */
@SuppressWarnings("serial")
public class BaseModel implements Serializable {

    private Boolean newMonday;

    private Boolean newTuesday;

    private Boolean newWednesday;

    private Boolean newThursday;

    private Boolean newFriday;
    
    private Date requestChangeDate;
    
    private String requestAccountId;
    
    private String requestId;
    
    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getRequestAccountId() {
        return requestAccountId;
    }

    public void setRequestAccountId(String requestAccountId) {
        this.requestAccountId = requestAccountId;
    }

    public Date getRequestChangeDate() {
        return requestChangeDate;
    }

    public void setRequestChangeDate(Date requestChangeDate) {
        this.requestChangeDate = requestChangeDate;
    }

    public Boolean getNewMonday() {
        return newMonday;
    }

    public void setNewMonday(Boolean newMonday) {
        this.newMonday = newMonday;
    }

    public Boolean getNewTuesday() {
        return newTuesday;
    }

    public void setNewTuesday(Boolean newTuesday) {
        this.newTuesday = newTuesday;
    }

    public Boolean getNewWednesday() {
        return newWednesday;
    }

    public void setNewWednesday(Boolean newWednesday) {
        this.newWednesday = newWednesday;
    }

    public Boolean getNewThursday() {
        return newThursday;
    }

    public void setNewThursday(Boolean newThursday) {
        this.newThursday = newThursday;
    }

    public Boolean getNewFriday() {
        return newFriday;
    }

    public void setNewFriday(Boolean newFriday) {
        this.newFriday = newFriday;
    }

    private String temp1;

    private Integer shouldCome = 0;

    private Integer realCome = 0;

    public Integer getShouldCome() {
        return shouldCome;
    }

    public void setShouldCome(Integer shouldCome) {
        this.shouldCome = shouldCome;
    }

    public Integer getRealCome() {
        return realCome;
    }

    public void setRealCome(Integer realCome) {
        this.realCome = realCome;
    }

    public String getTemp1() {
        return temp1;
    }

    public void setTemp1(String temp1) {
        this.temp1 = temp1;
    }

    /**
     * @description 格式化日期
     * @author xdwang
     * @create 2015年12月13日下午2:01:45
     * @version 1.0
     * @param date
     *            日期
     * @return 格式化后的日期
     */
    public String toDateStr(Date date) {
        if (date == null) {
            return "";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy", Locale.ENGLISH);
        return sdf.format(date);
    }

    public int toDoubleTwo(double f) {
        BigDecimal bg = new BigDecimal(f);
        int f1 = bg.setScale(0, BigDecimal.ROUND_HALF_UP).intValue();
        return f1;
    }

    public Short StringToInt(String str) {
        char[] chars = str.toCharArray();
        boolean startFlag = false;
        String result = "";
        for (char c : chars) {
            if (c != '0') {
                startFlag = true;
            }
            if (startFlag) {
                result += c;
            }
        }
        if (result == "") {
            return 0;
        }
        return Short.parseShort(result);
    }

    /**
     * @description 处理消息发送时间与当前时间差值
     * @author bbq
     * @create 2016年7月14日下午3:49:22
     * @version 1.0
     * @param time
     *            消息发送时间
     * @return 多久之前
     */
    public String getTimeAgoStr(Date time) {
        long sec = ((new Date()).getTime() - time.getTime()) / 1000;
        long min = sec / 60;
        long hour = min / 60;
        long day = hour / 24;
        if (day == 0) {
            if (hour == 0) {
                if (min == 0) {
                    if (sec == 0) {
                        return 1 + " sec ago";
                    }
                    return sec + " sec ago";
                } else {
                    return min + " min ago";
                }
            } else {
                return hour + " hours ago";
            }
        } else if (day > 10) {
            return "";

        } else {
            if (day == 1) {
                return day + " day ago";
            }
            return day + " days ago";
        }
    }
}
