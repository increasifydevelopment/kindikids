package com.aoyuntek.framework.model;

import java.util.List;

/**
 * @description 分页实体
 * @author xdwang
 * @create 2015年12月13日下午2:02:18
 * @version 1.0
 * @param <Model>
 *            模块实体
 * @param <ConditionModel>
 *            查询条件
 */
public class Pager<Model, ConditionModel> {

    /**
     * ConditionModel
     */
    private ConditionModel condition;

    /**
     * list
     */
    private List<Model> list;

    public Pager(List<Model> list, ConditionModel condition) {
        super();
        this.condition = condition;
        this.list = list;
    }

    public List<Model> getList() {
        return list;
    }

    public void setList(List<Model> list) {
        this.list = list;
    }

    public ConditionModel getCondition() {
        return condition;
    }

    public void setCondition(ConditionModel condition) {
        this.condition = condition;
    }

}