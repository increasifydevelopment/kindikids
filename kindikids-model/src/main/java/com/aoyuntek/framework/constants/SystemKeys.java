package com.aoyuntek.framework.constants;

/**
 * @description 系统key
 * @author xdwang
 * @create 2015年12月22日下午9:33:34
 * @version 1.0
 */
public class SystemKeys {

    /**
     * visit_log_pattern
     */
    public static final String VISITLOGPATTERN = "visit_log_pattern";

}
