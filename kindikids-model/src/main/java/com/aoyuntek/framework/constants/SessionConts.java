package com.aoyuntek.framework.constants;

/**
 * @description 用户session常量
 * @author xdwang
 * @create 2015年12月10日下午7:47:06
 * @version 1.0
 */
public class SessionConts {

    /**
     * 用户session常量
     */
    public static final String USERSESSION = "UserInfoSession";

}
