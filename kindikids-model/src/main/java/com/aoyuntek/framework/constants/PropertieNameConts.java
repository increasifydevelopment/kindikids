package com.aoyuntek.framework.constants;

/**
 * @description 配置文件常量
 * @author xdwang
 * @create 2015年12月13日下午1:54:46
 * @version 1.0
 */
public class PropertieNameConts {

    /**
     * 系统配置文件
     */
    public static final String SYSTEM = "/conf/system/system.properties";

    /**
     * 系统提示语配置文件
     */
    public static final String TIP = "/conf/message/tipmessage.properties";

    /**
     * 日志配置文件
     */
    public static final String LOG4J = "/conf/log4j/log4j.properties";
    
    /**
     * aws s3 配置文件
     */
    public static final String S3 = "/conf/system/s3.properties";
}
