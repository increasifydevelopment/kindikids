package com.aoyuntek.aoyun.entity.po.program;

public class ProgramNqsInfo {
    private String id;

    private String taskId;

    private String nqsVersion;

    private Boolean deleteFlag;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId == null ? null : taskId.trim();
    }

    public String getNqsVersion() {
        return nqsVersion;
    }

    public void setNqsVersion(String nqsVersion) {
        this.nqsVersion = nqsVersion == null ? null : nqsVersion.trim();
    }

    public Boolean getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Boolean deleteFlag) {
        this.deleteFlag = deleteFlag;
    }
}