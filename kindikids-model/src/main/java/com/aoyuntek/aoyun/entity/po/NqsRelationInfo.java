package com.aoyuntek.aoyun.entity.po;

import com.aoyuntek.framework.model.BaseModel;

public class NqsRelationInfo extends BaseModel {
    private String id;

    private String objId;

    private String nqsVersion;

    private Short deleteFlag;
    private Short tag;

    public Short getTag() {
        return tag;
    }

    public void setTag(Short tag) {
        this.tag = tag;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getObjId() {
        return objId;
    }

    public void setObjId(String objId) {
        this.objId = objId == null ? null : objId.trim();
    }

    public String getNqsVersion() {
        return nqsVersion;
    }

    public void setNqsVersion(String nqsVersion) {
        this.nqsVersion = nqsVersion == null ? null : nqsVersion.trim();
    }

    public Short getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Short deleteFlag) {
        this.deleteFlag = deleteFlag;
    }
}