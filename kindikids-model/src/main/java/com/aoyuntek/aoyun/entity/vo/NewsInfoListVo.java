package com.aoyuntek.aoyun.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aoyuntek.aoyun.entity.po.NewsCommentInfo;
import com.aoyuntek.aoyun.entity.po.NewsInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.po.YelfInfo;
import com.aoyuntek.framework.model.BaseModel;

/**
 * @description 微博列表
 * @author hxzhang
 * @create 2016年7月12日下午5:48:59
 * @version 1.0
 */
public class NewsInfoListVo extends BaseModel {
    /**
     * 序列化
     */
    private static final long serialVersionUID = 1L;
    /**
     * NewsInfo
     */
    private NewsInfo newsInfo = new NewsInfo();
    /**
     * List<FileVo>
     */
    private List<FileVo> fileVoList = new ArrayList<FileVo>();
    /**
     * List<NqsVo>
     */
    private List<NqsVo> nqsVoList = new ArrayList<NqsVo>();
    /**
     * content
     */
    private String content;
    /**
     * 当前登录用户AccountId
     */
    private String currentUserAccountId;

    private String id;

    private String centersName;

    private String roomName;

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getCentersName() {
        return centersName;
    }

    public void setCentersName(String centersName) {
        this.centersName = centersName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    /**
     * groupName
     */
    private List<SelecterPo> groupName = new ArrayList<SelecterPo>();
    
    private List<String> groupCenterId=new ArrayList<String>(); 
    /**
     * 回复集合
     */
    private List<NewsCommentInfo> commentList;
    /**
     * 微博关联人员
     */
    private List<UserInfo> accounts = new ArrayList<UserInfo>();
    /**
     * 微博关联小孩
     */
    private List<UserInfo> tagAccounts = new ArrayList<UserInfo>();
    /**
     */
    private List<YelfInfo> yelfList = new ArrayList<YelfInfo>();

    public List<String> getGroupCenterId() {
        return groupCenterId;
    }

    public void setGroupCenterId(List<String> groupCenterId) {
        this.groupCenterId = groupCenterId;
    }

    public List<YelfInfo> getYelfList() {
        return yelfList;
    }

    public void setYelfList(List<YelfInfo> yelfList) {
        this.yelfList = yelfList;
    }

    public List<UserInfo> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<UserInfo> accounts) {
        this.accounts = accounts;
    }

    public List<UserInfo> getTagAccounts() {
        return tagAccounts;
    }

    public void setTagAccounts(List<UserInfo> tagAccounts) {
        this.tagAccounts = tagAccounts;
    }

    public NewsInfo getNewsInfo() {
        return newsInfo;
    }

    public void setNewsInfo(NewsInfo newsInfo) {
        this.newsInfo = newsInfo;
    }

    public List<FileVo> getFileVoList() {
        return fileVoList;
    }

    public void setFileVoList(List<FileVo> fileVoList) {
        this.fileVoList = fileVoList;
    }

    public List<NqsVo> getNqsVoList() {
        return nqsVoList;
    }

    public void setNqsVoList(List<NqsVo> nqsVoList) {
        this.nqsVoList = nqsVoList;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<SelecterPo> getGroupName() {
        return groupName;
    }

    public void setGroupName(List<SelecterPo> groupName) {
        this.groupName = groupName;
    }

    public List<NewsCommentInfo> getCommentList() {
        return commentList;
    }

    public void setCommentList(List<NewsCommentInfo> commentList) {
        this.commentList = commentList;
    }

    public String getCurrentUserAccountId() {
        return currentUserAccountId;
    }

    public void setCurrentUserAccountId(String currentUserAccountId) {
        this.currentUserAccountId = currentUserAccountId;
    }

}
