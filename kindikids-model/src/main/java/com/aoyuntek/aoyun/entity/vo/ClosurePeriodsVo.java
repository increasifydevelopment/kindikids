package com.aoyuntek.aoyun.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aoyuntek.aoyun.entity.po.ClosurePeriod;

public class ClosurePeriodsVo {

	private boolean closureFlag = false;

	private List<ClosurePeriod> closurePeriods = new ArrayList<ClosurePeriod>();

	public List<ClosurePeriod> getClosurePeriods() {
		return closurePeriods;
	}

	public void setClosurePeriods(List<ClosurePeriod> closurePeriods) {
		this.closurePeriods = closurePeriods;
	}

	public boolean isClosureFlag() {
		return closureFlag;
	}

	public void setClosureFlag(boolean closureFlag) {
		this.closureFlag = closureFlag;
	}

}
