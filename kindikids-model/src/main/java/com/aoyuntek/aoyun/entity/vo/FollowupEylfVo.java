package com.aoyuntek.aoyun.entity.vo;

import com.aoyuntek.aoyun.entity.po.YelfInfo;

/**
 * @description 选择EylfVo
 * @author Hxzhang 2016年9月22日上午10:34:49
 * @version 1.0
 */
public class FollowupEylfVo extends YelfInfo {

    private static final long serialVersionUID = 1L;

    private Boolean checkFlag;

    public Boolean getCheckFlag() {
        return checkFlag;
    }

    public void setCheckFlag(Boolean checkFlag) {
        this.checkFlag = checkFlag;
    }

}
