package com.aoyuntek.aoyun.entity.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aoyuntek.aoyun.entity.po.ClosurePeriod;
import com.aoyuntek.aoyun.entity.vo.task.TodayNoteVo;

/**
 * 
 * @author dlli5 at 2016年8月24日上午11:16:57
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public class AttendanceDayVo implements Serializable {

	private static final long serialVersionUID = 1L;
	private String event;
	private TodayNoteVo todayNoteVo;
	private int day;
	private Date date;
	private boolean today;
	// 小孩专属封装
	private List<AttendanceChildVo> attendanceChilds = new ArrayList<AttendanceChildVo>();
	// 基本信息封装 访客 员工 都可
	private List<SigninVo> signs = new ArrayList<SigninVo>();
	// 员工请假信息
	private List<LeaveListVo> leaves = new ArrayList<LeaveListVo>();

	public TodayNoteVo getTodayNoteVo() {
		return todayNoteVo;
	}

	public void setTodayNoteVo(TodayNoteVo todayNoteVo) {
		this.todayNoteVo = todayNoteVo;
	}

	public List<SigninVo> getSigns() {
		return signs;
	}

	public void setSigns(List<SigninVo> signs) {
		this.signs = signs;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public boolean isToday() {
		return today;
	}

	public void setToday(boolean today) {
		this.today = today;
	}

	public List<AttendanceChildVo> getAttendanceChilds() {
		return attendanceChilds;
	}

	public void setAttendanceChilds(List<AttendanceChildVo> attendanceChilds) {
		this.attendanceChilds = attendanceChilds;
	}

	public List<LeaveListVo> getLeaves() {
		return leaves;
	}

	public void setLeaves(List<LeaveListVo> leaves) {
		this.leaves = leaves;
	}

}
