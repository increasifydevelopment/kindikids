package com.aoyuntek.aoyun.entity.vo;

import java.io.Serializable;
import java.util.List;

import com.aoyuntek.aoyun.condtion.AttendanceCondtion;
import com.aoyuntek.framework.model.BaseModel;

/**
 * 
 * @author dlli5 at 2016年8月24日上午11:17:18
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public class AttendanceReturnVo  implements Serializable{
	private static final long serialVersionUID = 1L;

	private List<AttendanceDayVo> weekDays;
	
	private AttendanceCondtion condition;
	

	public AttendanceReturnVo() {
		super();
	}

	public AttendanceReturnVo(List<AttendanceDayVo> weekDays, AttendanceCondtion condition) {
		super();
		this.weekDays = weekDays;
		this.condition = condition;
	}

	public List<AttendanceDayVo> getWeekDays() {
		return weekDays;
	}

	public void setWeekDays(List<AttendanceDayVo> weekDays) {
		this.weekDays = weekDays;
	}

	public AttendanceCondtion getCondition() {
		return condition;
	}

	public void setCondition(AttendanceCondtion condition) {
		this.condition = condition;
	}
}
