package com.aoyuntek.aoyun.entity.vo;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.InstanceInfo;

@SuppressWarnings("serial")
public class InstanceInfoDataVO extends InstanceInfo{

	private List<FormAttributeInfoDataVO> attributes;

	public List<FormAttributeInfoDataVO> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<FormAttributeInfoDataVO> list) {
		this.attributes = list;
	}
	
}
