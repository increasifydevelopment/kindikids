package com.aoyuntek.aoyun.entity.vo.event;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aoyuntek.aoyun.entity.vo.FileVo;
import com.aoyuntek.framework.model.BaseModel;

public class GalleryInfoVo extends BaseModel {
	private static final long serialVersionUID = 1L;
	
	private String describe;
	private Date createTime;
	private List<FileVo> images=new ArrayList<FileVo>();
	
	public String getDescribe() {
		return describe;
	}
	public void setDescribe(String describe) {
		this.describe = describe;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	public List<FileVo> getImages() {
		return images;
	}
	public void setImages(List<FileVo> images) {
		this.images = images;
	}
	
}
