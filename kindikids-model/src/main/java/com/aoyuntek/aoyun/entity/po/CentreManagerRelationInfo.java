package com.aoyuntek.aoyun.entity.po;

public class CentreManagerRelationInfo {
	private String centreId;

	private String userId;

	private Boolean deleteFlag;

	public String getCentreId() {
		return centreId;
	}

	public void setCentreId(String centreId) {
		this.centreId = centreId == null ? null : centreId.trim();
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId == null ? null : userId.trim();
	}

	public Boolean getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(Boolean deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public CentreManagerRelationInfo(String centreId, String userId, Boolean deleteFlag) {
		super();
		this.centreId = centreId;
		this.userId = userId;
		this.deleteFlag = deleteFlag;
	}
}