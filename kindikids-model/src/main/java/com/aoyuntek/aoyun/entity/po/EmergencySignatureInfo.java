package com.aoyuntek.aoyun.entity.po;

public class EmergencySignatureInfo {
	private String id;

	private String userId;

	private String emergencySignature;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id == null ? null : id.trim();
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getEmergencySignature() {
		return emergencySignature;
	}

	public void setEmergencySignature(String emergencySignature) {
		this.emergencySignature = emergencySignature == null ? null : emergencySignature.trim();
	}
}