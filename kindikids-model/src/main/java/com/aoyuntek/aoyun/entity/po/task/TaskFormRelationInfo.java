package com.aoyuntek.aoyun.entity.po.task;

import com.aoyuntek.framework.model.BaseModel;

public class TaskFormRelationInfo extends BaseModel {
    private static final long serialVersionUID = 1L;

    private String taskId;

    private String formId;

    public TaskFormRelationInfo() {
        super();
    }

    public TaskFormRelationInfo(String taskId, String formId) {
        super();
        this.taskId = taskId;
        this.formId = formId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId == null ? null : taskId.trim();
    }

    public String getFormId() {
        return formId;
    }

    public void setFormId(String formId) {
        this.formId = formId == null ? null : formId.trim();
    }
}