package com.aoyuntek.aoyun.entity.vo;

import java.util.Date;
import java.util.List;

import com.aoyuntek.aoyun.entity.po.YelfInfo;
import com.aoyuntek.aoyun.enums.NewsType;
import com.aoyuntek.framework.model.BaseModel;

/**
 * @description 其他创建newsfeedVo实体
 * @author Hxzhang 2016年9月26日下午3:23:17
 */
public class NewsfeedOtherVo extends BaseModel {
    /**
     * 序列化
     */
    private static final long serialVersionUID = 1L;
    /**
     * 附件源ID
     */
    private String sourceId;
    /**
     * NEWS ID
     */
    private String newsId;
    /**
     * room ID
     */
    private String roomId;
    /**
     * center ID
     */
    private String centerId;
    /**
     * 内容
     */
    private String content;

    /**
     * news 类型
     */
    private NewsType newsType;
    /**
     * nqs信息
     */
    private List<NqsVo> nqsVoList;
    /**
     * eylf信息
     */
    private List<YelfInfo> eylfList;
    /**
     * 当前登录用户
     */
    private UserInfoVo currtenUser;
    /**
     * tag的小孩
     */
    private List<String> tagAccounts;
    /**
     * 非tag小孩(多个小孩)
     */
    private List<String> accounts;
    /**
     * newsfeed状态
     */
    private Short status;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * program中的经验
     */
    private String newsTags;

    /**
     * 判断wekkly类型
     */
    private Short weeklyType;

    public Short getWeeklyType() {
        return weeklyType;
    }

    public void setWeeklyType(Short weeklyType) {
        this.weeklyType = weeklyType;
    }

    public String getNewsTags() {
        return newsTags;
    }

    public void setNewsTags(String newsTags) {
        this.newsTags = newsTags;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public String getNewsId() {
        return newsId;
    }

    public void setNewsId(String newsId) {
        this.newsId = newsId;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getCenterId() {
        return centerId;
    }

    public void setCenterId(String centerId) {
        this.centerId = centerId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public NewsType getNewsType() {
        return newsType;
    }

    public void setNewsType(NewsType newsType) {
        this.newsType = newsType;
    }

    public List<NqsVo> getNqsVoList() {
        return nqsVoList;
    }

    public void setNqsVoList(List<NqsVo> nqsVoList) {
        this.nqsVoList = nqsVoList;
    }

    public List<YelfInfo> getEylfList() {
        return eylfList;
    }

    public void setEylfList(List<YelfInfo> eylfList) {
        this.eylfList = eylfList;
    }

    public UserInfoVo getCurrtenUser() {
        return currtenUser;
    }

    public void setCurrtenUser(UserInfoVo currtenUser) {
        this.currtenUser = currtenUser;
    }

    public List<String> getTagAccounts() {
        return tagAccounts;
    }

    public void setTagAccounts(List<String> tagAccounts) {
        this.tagAccounts = tagAccounts;
    }

    public List<String> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<String> accounts) {
        this.accounts = accounts;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

}
