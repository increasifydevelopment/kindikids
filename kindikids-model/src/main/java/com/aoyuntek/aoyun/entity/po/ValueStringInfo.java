package com.aoyuntek.aoyun.entity.po;

@SuppressWarnings("serial")
public class ValueStringInfo extends ValueInfo {

    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value == null ? null : value.trim();
    }
}