package com.aoyuntek.aoyun.entity.vo.meeting;

import java.util.Date;
import java.util.List;

import com.aoyuntek.aoyun.entity.po.meeting.MeetingInfo;
import com.aoyuntek.aoyun.entity.vo.task.TodayNoteVo;
import com.aoyuntek.framework.model.BaseModel;

public class MeetingListVo extends BaseModel {
    /**
     * 序列化
     */
    private static final long serialVersionUID = 1L;

    private String event;
    private TodayNoteVo todayNoteVo;
    private int day;
    private Date date;
    private boolean today;

    private List<MeetingInfo> meetingList;

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public TodayNoteVo getTodayNoteVo() {
        return todayNoteVo;
    }

    public void setTodayNoteVo(TodayNoteVo todayNoteVo) {
        this.todayNoteVo = todayNoteVo;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isToday() {
        return today;
    }

    public void setToday(boolean today) {
        this.today = today;
    }

    public List<MeetingInfo> getMeetingList() {
        return meetingList;
    }

    public void setMeetingList(List<MeetingInfo> meetingList) {
        this.meetingList = meetingList;
    }

}
