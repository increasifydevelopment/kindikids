package com.aoyuntek.aoyun.entity.po;

import java.util.Date;

import com.aoyuntek.framework.model.BaseModel;

public class RoleGroupRelationInfo extends BaseModel {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String id;

    private String roleId;

    private String authGroupId;

    private Short type;

    private Short deleteFlag;

    private String centerId;

    private Date createTime;

    public RoleGroupRelationInfo(String roleId, String authGroupId, Short type, Short deleteFlag, String centerId, Date createTime) {
        super();
        this.roleId = roleId;
        this.authGroupId = authGroupId;
        this.type = type;
        this.deleteFlag = deleteFlag;
        this.centerId = centerId;
        this.createTime = createTime;
    }

    public RoleGroupRelationInfo(String roleId, String authGroupId, Short type, Short deleteFlag, Date createTime) {
        super();
        this.roleId = roleId;
        this.authGroupId = authGroupId;
        this.type = type;
        this.deleteFlag = deleteFlag;
        this.createTime = createTime;
    }

    public RoleGroupRelationInfo() {
    }

    public String getId() {
        return id;
    }

    public String getCenterId() {
        return centerId;
    }

    public void setCenterId(String centerId) {
        this.centerId = centerId;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId == null ? null : roleId.trim();
    }

    public String getAuthGroupId() {
        return authGroupId;
    }

    public void setAuthGroupId(String authGroupId) {
        this.authGroupId = authGroupId == null ? null : authGroupId.trim();
    }

    public Short getType() {
        return type;
    }

    public void setType(Short type) {
        this.type = type;
    }

    public Short getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Short deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}