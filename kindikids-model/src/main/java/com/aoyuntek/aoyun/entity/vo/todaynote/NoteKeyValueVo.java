package com.aoyuntek.aoyun.entity.vo.todaynote;

import com.aoyuntek.aoyun.enums.TodayNoteType;
import com.aoyuntek.framework.model.BaseModel;

public class NoteKeyValueVo extends BaseModel {
    private String key;

    private String value;

    public NoteKeyValueVo(String key, String value) {
        super();
        this.key = key;
        this.value = value;
    }

    public NoteKeyValueVo(String value) {
        super();
        this.value = value;
    }

    public NoteKeyValueVo() {
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
