package com.aoyuntek.aoyun.entity.vo;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.MenuInfo;
import com.aoyuntek.aoyun.entity.po.RoleInfo;

public class RoleInfoVo extends RoleInfo {
    public RoleInfoVo() {

    }

    /**
     * 序列化
     */
    private static final long serialVersionUID = 1L;
    /**
     * url权限信息
     */
    private List<MenuInfo> menuInfoList;
    
    private short tempFlag;

    public short getTempFlag() {
        return tempFlag;
    }

    public void setTempFlag(short tempFlag) {
        this.tempFlag = tempFlag;
    }

    public List<MenuInfo> getMenuInfoList() {
        return menuInfoList;
    }

    public void setMenuInfoList(List<MenuInfo> menuInfoList) {
        this.menuInfoList = menuInfoList;
    }
}
