package com.aoyuntek.aoyun.entity.po;

import java.util.List;

public class WaitingListPo {

    private List<OutFamilyPo> outFamilyPos;

    public List<OutFamilyPo> getOutFamilyPos() {
        return outFamilyPos;
    }

    public void setOutFamilyPos(List<OutFamilyPo> outFamilyPos) {
        this.outFamilyPos = outFamilyPos;
    }

}
