package com.aoyuntek.aoyun.entity.vo.roster;

import java.util.ArrayList;
import java.util.List;

import com.aoyuntek.aoyun.entity.po.roster.RosterShiftInfo;

public class RosterShiftVo extends RosterShiftInfo {

    private static final long serialVersionUID = 1L;

    private List<RosterShiftTimeVo> rosterShiftTimeVos = new ArrayList<RosterShiftTimeVo>();

    private Boolean operateFlag = true;

    public Boolean getOperateFlag() {
        return operateFlag;
    }

    public void setOperateFlag(Boolean operateFlag) {
        this.operateFlag = operateFlag;
    }

    public List<RosterShiftTimeVo> getRosterShiftTimeVos() {
        return rosterShiftTimeVos;
    }

    public void setRosterShiftTimeVos(List<RosterShiftTimeVo> rosterShiftTimeVos) {
        this.rosterShiftTimeVos = rosterShiftTimeVos;
    }

}
