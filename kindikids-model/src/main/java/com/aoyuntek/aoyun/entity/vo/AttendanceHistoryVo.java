package com.aoyuntek.aoyun.entity.vo;

import com.aoyuntek.aoyun.entity.po.AttendanceHistoryInfo;

public class AttendanceHistoryVo extends AttendanceHistoryInfo {

    private static final long serialVersionUID = 1L;

    private String centerName;

    private String roomName;

    public String getCenterName() {
        return centerName;
    }

    public void setCenterName(String centerName) {
        this.centerName = centerName;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }
}