package com.aoyuntek.aoyun.entity.vo;

import java.io.Serializable;
import java.util.List;

import com.aoyuntek.aoyun.entity.po.CentersInfo;
import com.aoyuntek.aoyun.entity.po.RoleInfo;
import com.aoyuntek.aoyun.entity.po.RoomGroupInfo;
import com.aoyuntek.aoyun.entity.po.RoomInfo;

public class StaffRoleInfoVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private List<RoleInfo> roleInfos;

    private List<CentersInfo> centersInfos;

    private List<RoomInfo> roomInfos;

    private List<RoomGroupInfo> groups;

    // private List<Group>

    public List<RoomGroupInfo> getGroups() {
        return groups;
    }

    public void setGroups(List<RoomGroupInfo> groups) {
        this.groups = groups;
    }

    public List<RoleInfo> getRoleInfos() {
        return roleInfos;
    }

    public void setRoleInfos(List<RoleInfo> roleInfos) {
        this.roleInfos = roleInfos;
    }

    public List<CentersInfo> getCentersInfos() {
        return centersInfos;
    }

    public void setCentersInfos(List<CentersInfo> centersInfos) {
        this.centersInfos = centersInfos;
    }

    public List<RoomInfo> getRoomInfos() {
        return roomInfos;
    }

    public void setRoomInfos(List<RoomInfo> roomInfos) {
        this.roomInfos = roomInfos;
    }

}
