package com.aoyuntek.aoyun.entity.vo;

public class SignatureVo {
    private String parent_signature;
    private String _id;

    public String getParent_signature() {
        return parent_signature;
    }

    public void setParent_signature(String parent_signature) {
        this.parent_signature = parent_signature;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

}
