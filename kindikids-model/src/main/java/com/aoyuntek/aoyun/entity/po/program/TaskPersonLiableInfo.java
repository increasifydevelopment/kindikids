package com.aoyuntek.aoyun.entity.po.program;

import com.aoyuntek.framework.model.BaseModel;

public class TaskPersonLiableInfo extends BaseModel{
    private String taskId;

    private String personId;

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId == null ? null : taskId.trim();
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId == null ? null : personId.trim();
    }
}