package com.aoyuntek.aoyun.entity.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aoyuntek.aoyun.entity.po.event.EventInfo;
import com.aoyuntek.framework.model.BaseModel;

public class CalendarDayVo  implements Serializable {
	private static final long serialVersionUID = 1L;
	
    private String event;
	private int day;
	private Date date;
	private boolean today;
	private List<EventInfo> eventList=new ArrayList<EventInfo>();
	
	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public boolean isToday() {
		return today;
	}

	public void setToday(boolean today) {
		this.today = today;
	}

	public List<EventInfo> getEventList() {
		return eventList;
	}

	public void setEventList(List<EventInfo> eventList) {
		this.eventList = eventList;
	}

	

}
