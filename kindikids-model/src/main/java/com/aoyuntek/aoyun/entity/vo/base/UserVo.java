package com.aoyuntek.aoyun.entity.vo.base;

import com.aoyuntek.aoyun.entity.po.UserInfo;

/**
 * 
 * @author dlli5 at 2016年9月21日下午3:21:53
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public class UserVo extends UserInfo {

    private static final long serialVersionUID = 1L;

    private String accountId;
    
    private String name;    
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }
}
