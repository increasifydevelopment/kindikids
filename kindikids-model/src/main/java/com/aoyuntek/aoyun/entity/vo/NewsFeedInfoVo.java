package com.aoyuntek.aoyun.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aoyuntek.aoyun.entity.po.NewsInfo;
import com.aoyuntek.framework.model.BaseModel;

/**
 * @description 保存NewsFeedVo实体
 * @author hxzhang
 * @create 2016年7月12日上午11:19:32
 * @version 1.0
 */
public class NewsFeedInfoVo extends BaseModel{
    /**
     * 序列化
     */
    private static final long serialVersionUID = 1L;
    /**
     * NewsInfo
     */
    private NewsInfo newsInfo = new NewsInfo();
    /**
     * List<FileVo>
     */
    private List<FileVo> fileVoList = new ArrayList<FileVo>();
    /**
     * List<NqsVo>
     */
    private List<NqsVo> nqsVoList = new ArrayList<NqsVo>();
    /**
     * content
     */
    private String content;
    /**
     * groupName
     */
    private List<String> groupName = new ArrayList<String>();
    /**
     * groupNamePo
     */
    private List<SelecterPo> groupNamePo = new ArrayList<SelecterPo>();
    /**
     * accounts(多个小孩)
     */
    private List<String> accounts = new ArrayList<String>();
    /**
     * tagAccounts
     */
    private List<String> tagAccounts = new ArrayList<String>();
    /**
     * 分组源
     */
    private List<SelecterPo> groupNameSource = new ArrayList<SelecterPo>();
    /**
     * 搜索联系人
     */
    private List<SelecterPo> selectAccounts = new ArrayList<SelecterPo>();

    public List<String> getTagAccounts() {
        return tagAccounts;
    }

    public void setTagAccounts(List<String> tagAccounts) {
        this.tagAccounts = tagAccounts;
    }

    public List<SelecterPo> getSelectAccounts() {
        return selectAccounts;
    }

    public void setSelectAccounts(List<SelecterPo> selectAccounts) {
        this.selectAccounts = selectAccounts;
    }

    public NewsInfo getNewsInfo() {
        return newsInfo;
    }

    public void setNewsInfo(NewsInfo newsInfo) {
        this.newsInfo = newsInfo;
    }

    public List<FileVo> getFileVoList() {
        return fileVoList;
    }

    public void setFileVoList(List<FileVo> fileVoList) {
        this.fileVoList = fileVoList;
    }

    public List<NqsVo> getNqsVoList() {
        return nqsVoList;
    }

    public void setNqsVoList(List<NqsVo> nqsVoList) {
        this.nqsVoList = nqsVoList;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<String> getGroupName() {
        return groupName;
    }

    public void setGroupName(List<String> groupName) {
        this.groupName = groupName;
    }

    public List<String> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<String> accounts) {
        this.accounts = accounts;
    }

    public List<SelecterPo> getGroupNameSource() {
        return groupNameSource;
    }

    public void setGroupNameSource(List<SelecterPo> groupNameSource) {
        this.groupNameSource = groupNameSource;
    }

    public List<SelecterPo> getGroupNamePo() {
        return groupNamePo;
    }

    public void setGroupNamePo(List<SelecterPo> groupNamePo) {
        this.groupNamePo = groupNamePo;
    }

}
