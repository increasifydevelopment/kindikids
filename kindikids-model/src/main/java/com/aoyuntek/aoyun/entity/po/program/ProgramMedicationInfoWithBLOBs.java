package com.aoyuntek.aoyun.entity.po.program;

public class ProgramMedicationInfoWithBLOBs extends ProgramMedicationInfo {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String parentSignatureText;

    public String getParentSignatureText() {
        return parentSignatureText;
    }

    public void setParentSignatureText(String parentSignatureText) {
        this.parentSignatureText = parentSignatureText == null ? null : parentSignatureText.trim();
    }

}