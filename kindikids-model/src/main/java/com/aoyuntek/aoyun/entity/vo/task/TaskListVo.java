package com.aoyuntek.aoyun.entity.vo.task;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.task.TaskInstanceInfo;
import com.aoyuntek.aoyun.entity.vo.NqsVo;
import com.aoyuntek.aoyun.entity.vo.UserAvatarVo;

/**
 * @description
 * @author Hxzhang 2016年9月22日下午5:13:26
 * @version 1.0
 */
public class TaskListVo extends TaskInstanceInfo {

	private static final long serialVersionUID = 1L;

	private String taskName;

	private Integer dataType;

	private List<UserAvatarVo> avatar;

	private List<UserAvatarVo> implementersAvatar;

	private List<NqsVo> nqsVos;

	private String cName;

	private String rName;

	private String nqsStr;

	private String subPerson;

	public List<UserAvatarVo> getImplementersAvatar() {
		return implementersAvatar;
	}

	public void setImplementersAvatar(List<UserAvatarVo> implementersAvatar) {
		this.implementersAvatar = implementersAvatar;
	}

	public String getSubPerson() {
		return subPerson;
	}

	public void setSubPerson(String subPerson) {
		this.subPerson = subPerson;
	}

	public String getNqsStr() {
		return nqsStr;
	}

	public void setNqsStr(String nqsStr) {
		this.nqsStr = nqsStr;
	}

	public String getcName() {
		return cName;
	}

	public void setcName(String cName) {
		this.cName = cName;
	}

	public String getrName() {
		return rName;
	}

	public void setrName(String rName) {
		this.rName = rName;
	}

	public List<NqsVo> getNqsVos() {
		return nqsVos;
	}

	public void setNqsVos(List<NqsVo> nqsVos) {
		this.nqsVos = nqsVos;
	}

	public Integer getDataType() {
		return dataType;
	}

	public void setDataType(Integer dataType) {
		this.dataType = dataType;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public List<UserAvatarVo> getAvatar() {
		return avatar;
	}

	public void setAvatar(List<UserAvatarVo> avatar) {
		this.avatar = avatar;
	}
}
