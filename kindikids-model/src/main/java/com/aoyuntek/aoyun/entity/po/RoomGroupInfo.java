package com.aoyuntek.aoyun.entity.po;

import java.util.Date;

import com.aoyuntek.framework.model.BaseModel;

public class RoomGroupInfo extends BaseModel {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
      * 老id
      */
     private String oldId;
     
     public String getOldId() {
         return oldId;
     }

     public void setOldId(String oldId) {
         this.oldId = oldId;
     }
    /**
     * This field was generated by MyBatis Generator. This field corresponds to
     * the database column tbl_room_group.id
     * 
     * @mbggenerated Thu Aug 18 08:42:14 CST 2016
     */
    private String id;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to
     * the database column tbl_room_group.name
     * 
     * @mbggenerated Thu Aug 18 08:42:14 CST 2016
     */
    private String name;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to
     * the database column tbl_room_group.educator_user_id
     * 
     * @mbggenerated Thu Aug 18 08:42:14 CST 2016
     */
    private String educatorUserId;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to
     * the database column tbl_room_group.status
     * 
     * @mbggenerated Thu Aug 18 08:42:14 CST 2016
     */
    private Short status;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to
     * the database column tbl_room_group.room_id
     * 
     * @mbggenerated Thu Aug 18 08:42:14 CST 2016
     */
    private String roomId;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to
     * the database column tbl_room_group.center_id
     * 
     * @mbggenerated Thu Aug 18 08:42:14 CST 2016
     */
    private String centerId;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to
     * the database column tbl_room_group.delete_flag
     * 
     * @mbggenerated Thu Aug 18 08:42:14 CST 2016
     */
    private Short deleteFlag;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to
     * the database column tbl_room_group.create_time
     * 
     * @mbggenerated Thu Aug 18 08:42:14 CST 2016
     */
    private Date createTime;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to
     * the database column tbl_room_group.update_time
     * 
     * @mbggenerated Thu Aug 18 08:42:14 CST 2016
     */
    private Date updateTime;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to
     * the database column tbl_room_group.create_account_id
     * 
     * @mbggenerated Thu Aug 18 08:42:14 CST 2016
     */
    private String createAccountId;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to
     * the database column tbl_room_group.update_account_id
     * 
     * @mbggenerated Thu Aug 18 08:42:14 CST 2016
     */
    private String updateAccountId;

    /**
     * This method was generated by MyBatis Generator. This method returns the
     * value of the database column tbl_room_group.id
     * 
     * @return the value of tbl_room_group.id
     * 
     * @mbggenerated Thu Aug 18 08:42:14 CST 2016
     */
    public String getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the
     * value of the database column tbl_room_group.id
     * 
     * @param id
     *            the value for tbl_room_group.id
     * 
     * @mbggenerated Thu Aug 18 08:42:14 CST 2016
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the
     * value of the database column tbl_room_group.name
     * 
     * @return the value of tbl_room_group.name
     * 
     * @mbggenerated Thu Aug 18 08:42:14 CST 2016
     */
    public String getName() {
        return name;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the
     * value of the database column tbl_room_group.name
     * 
     * @param name
     *            the value for tbl_room_group.name
     * 
     * @mbggenerated Thu Aug 18 08:42:14 CST 2016
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the
     * value of the database column tbl_room_group.educator_user_id
     * 
     * @return the value of tbl_room_group.educator_user_id
     * 
     * @mbggenerated Thu Aug 18 08:42:14 CST 2016
     */
    public String getEducatorUserId() {
        return educatorUserId;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the
     * value of the database column tbl_room_group.educator_user_id
     * 
     * @param educatorUserId
     *            the value for tbl_room_group.educator_user_id
     * 
     * @mbggenerated Thu Aug 18 08:42:14 CST 2016
     */
    public void setEducatorUserId(String educatorUserId) {
        this.educatorUserId = educatorUserId == null ? null : educatorUserId.trim();
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the
     * value of the database column tbl_room_group.status
     * 
     * @return the value of tbl_room_group.status
     * 
     * @mbggenerated Thu Aug 18 08:42:14 CST 2016
     */
    public Short getStatus() {
        return status;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the
     * value of the database column tbl_room_group.status
     * 
     * @param status
     *            the value for tbl_room_group.status
     * 
     * @mbggenerated Thu Aug 18 08:42:14 CST 2016
     */
    public void setStatus(Short status) {
        this.status = status;
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the
     * value of the database column tbl_room_group.room_id
     * 
     * @return the value of tbl_room_group.room_id
     * 
     * @mbggenerated Thu Aug 18 08:42:14 CST 2016
     */
    public String getRoomId() {
        return roomId;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the
     * value of the database column tbl_room_group.room_id
     * 
     * @param roomId
     *            the value for tbl_room_group.room_id
     * 
     * @mbggenerated Thu Aug 18 08:42:14 CST 2016
     */
    public void setRoomId(String roomId) {
        this.roomId = roomId == null ? null : roomId.trim();
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the
     * value of the database column tbl_room_group.center_id
     * 
     * @return the value of tbl_room_group.center_id
     * 
     * @mbggenerated Thu Aug 18 08:42:14 CST 2016
     */
    public String getCenterId() {
        return centerId;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the
     * value of the database column tbl_room_group.center_id
     * 
     * @param centerId
     *            the value for tbl_room_group.center_id
     * 
     * @mbggenerated Thu Aug 18 08:42:14 CST 2016
     */
    public void setCenterId(String centerId) {
        this.centerId = centerId == null ? null : centerId.trim();
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the
     * value of the database column tbl_room_group.delete_flag
     * 
     * @return the value of tbl_room_group.delete_flag
     * 
     * @mbggenerated Thu Aug 18 08:42:14 CST 2016
     */
    public Short getDeleteFlag() {
        return deleteFlag;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the
     * value of the database column tbl_room_group.delete_flag
     * 
     * @param deleteFlag
     *            the value for tbl_room_group.delete_flag
     * 
     * @mbggenerated Thu Aug 18 08:42:14 CST 2016
     */
    public void setDeleteFlag(Short deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the
     * value of the database column tbl_room_group.create_time
     * 
     * @return the value of tbl_room_group.create_time
     * 
     * @mbggenerated Thu Aug 18 08:42:14 CST 2016
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the
     * value of the database column tbl_room_group.create_time
     * 
     * @param createTime
     *            the value for tbl_room_group.create_time
     * 
     * @mbggenerated Thu Aug 18 08:42:14 CST 2016
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the
     * value of the database column tbl_room_group.update_time
     * 
     * @return the value of tbl_room_group.update_time
     * 
     * @mbggenerated Thu Aug 18 08:42:14 CST 2016
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the
     * value of the database column tbl_room_group.update_time
     * 
     * @param updateTime
     *            the value for tbl_room_group.update_time
     * 
     * @mbggenerated Thu Aug 18 08:42:14 CST 2016
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the
     * value of the database column tbl_room_group.create_account_id
     * 
     * @return the value of tbl_room_group.create_account_id
     * 
     * @mbggenerated Thu Aug 18 08:42:14 CST 2016
     */
    public String getCreateAccountId() {
        return createAccountId;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the
     * value of the database column tbl_room_group.create_account_id
     * 
     * @param createAccountId
     *            the value for tbl_room_group.create_account_id
     * 
     * @mbggenerated Thu Aug 18 08:42:14 CST 2016
     */
    public void setCreateAccountId(String createAccountId) {
        this.createAccountId = createAccountId == null ? null : createAccountId.trim();
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the
     * value of the database column tbl_room_group.update_account_id
     * 
     * @return the value of tbl_room_group.update_account_id
     * 
     * @mbggenerated Thu Aug 18 08:42:14 CST 2016
     */
    public String getUpdateAccountId() {
        return updateAccountId;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the
     * value of the database column tbl_room_group.update_account_id
     * 
     * @param updateAccountId
     *            the value for tbl_room_group.update_account_id
     * 
     * @mbggenerated Thu Aug 18 08:42:14 CST 2016
     */
    public void setUpdateAccountId(String updateAccountId) {
        this.updateAccountId = updateAccountId == null ? null : updateAccountId.trim();
    }
}