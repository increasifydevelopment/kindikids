package com.aoyuntek.aoyun.entity.vo;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.program.ProgramTaskExgrscInfo;
import com.aoyuntek.framework.model.BaseModel;

/**
 * @description ProgramTaskExgrscInfo返回VO实体
 * @author Hxzhang 2016年9月20日下午9:30:09
 * @version 1.0
 */
public class ResultExgrscInfo extends BaseModel {
    private static final long serialVersionUID = 1L;

    private ProgramTaskExgrscInfo exgrscInfo;

    private List<ActivityVo> intList;

    private List<ActivityVo> obsList;

    private List<ActivityVo> leaList;

    public ProgramTaskExgrscInfo getExgrscInfo() {
        return exgrscInfo;
    }

    public void setExgrscInfo(ProgramTaskExgrscInfo exgrscInfo) {
        this.exgrscInfo = exgrscInfo;
    }

    public List<ActivityVo> getIntList() {
        return intList;
    }

    public void setIntList(List<ActivityVo> intList) {
        this.intList = intList;
    }

    public List<ActivityVo> getObsList() {
        return obsList;
    }

    public void setObsList(List<ActivityVo> obsList) {
        this.obsList = obsList;
    }

    public List<ActivityVo> getLeaList() {
        return leaList;
    }

    public void setLeaList(List<ActivityVo> leaList) {
        this.leaList = leaList;
    }

}
