package com.aoyuntek.aoyun.entity.vo;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.WeekNutrionalMenuInfo;

public class WeekNutrionalMenuVo extends WeekNutrionalMenuInfo {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private FileVo image;
    
    private List<NameVo> names;
    
    public List<NameVo> getNames() {
        return names;
    }

    public void setNames(List<NameVo> names) {
        this.names = names;
    }

    public FileVo getImage() {
        return image;
    }

    public void setImage(FileVo image) {
        this.image = image;
    }
}
