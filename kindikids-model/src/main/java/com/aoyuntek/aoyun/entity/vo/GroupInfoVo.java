package com.aoyuntek.aoyun.entity.vo;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.aoyuntek.aoyun.entity.po.RoomGroupInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.framework.deserializer.S3JsonSerializer;

/**
 * 
 * @description 分组详细信息
 * @author gfwang
 * @create 2016年8月22日上午11:44:03
 * @version 1.0
 */
public class GroupInfoVo extends RoomGroupInfo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 分组老师
	 */
	private String educatorName;

	/**
	 * 分组老师头像
	 */
	private String educatorAvatar;

	private String personColor;

	private List<UserAvatarVo> list = new ArrayList<UserAvatarVo>();

	public List<UserAvatarVo> getList() {
		return list;
	}

	public void setList(List<UserAvatarVo> list) {
		this.list = list;
	}

	public String getPersonColor() {
		return personColor;
	}

	public void setPersonColor(String personColor) {
		this.personColor = personColor;
	}

	/**
	 * 分组小孩列表
	 */
	private List<UserInfo> childList;

	@JsonSerialize(using = S3JsonSerializer.class)
	public String getEducatorAvatar() {
		return educatorAvatar;
	}

	public void setEducatorAvatar(String educatorAvatar) {
		this.educatorAvatar = educatorAvatar;
	}

	public String getEducatorName() {
		return educatorName;
	}

	public void setEducatorName(String educatorName) {
		this.educatorName = educatorName;
	}

	public List<UserInfo> getChildList() {
		return childList;
	}

	public void setChildList(List<UserInfo> childList) {
		this.childList = childList;
	}

}
