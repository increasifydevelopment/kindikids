package com.aoyuntek.aoyun.entity.po;

import java.util.Date;

import com.aoyuntek.framework.model.BaseModel;

public class AttendanceHistoryInfo extends BaseModel {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to
     * the database column tbl_attendance_history.id
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    private String id;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to
     * the database column tbl_attendance_history.child_id
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    private String childId;

    private Integer group;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to
     * the database column tbl_attendance_history.center_id
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    private String centerId;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to
     * the database column tbl_attendance_history.room_id
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    private String roomId;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to
     * the database column tbl_attendance_history.monday
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    private Boolean monday;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to
     * the database column tbl_attendance_history.tuesday
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    private Boolean tuesday;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to
     * the database column tbl_attendance_history.wednesday
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    private Boolean wednesday;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to
     * the database column tbl_attendance_history.thursday
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    private Boolean thursday;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to
     * the database column tbl_attendance_history.friday
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    private Boolean friday;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to
     * the database column tbl_attendance_history.create_time
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    private Date createTime;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to
     * the database column tbl_attendance_history.create_account_id
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    private String createAccountId;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to
     * the database column tbl_attendance_history.update_time
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    private Date updateTime;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to
     * the database column tbl_attendance_history.update_account_id
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    private String updateAccountId;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to
     * the database column tbl_attendance_history.delete_flag
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    private Short deleteFlag;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to
     * the database column tbl_attendance_history.begin_time
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    private Date beginTime;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to
     * the database column tbl_attendance_history.end_time
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    private Date endTime;

    /**
     * This method was generated by MyBatis Generator. This method returns the
     * value of the database column tbl_attendance_history.id
     * 
     * @return the value of tbl_attendance_history.id
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    public String getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the
     * value of the database column tbl_attendance_history.id
     * 
     * @param id
     *            the value for tbl_attendance_history.id
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the
     * value of the database column tbl_attendance_history.child_id
     * 
     * @return the value of tbl_attendance_history.child_id
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    public String getChildId() {
        return childId;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the
     * value of the database column tbl_attendance_history.child_id
     * 
     * @param childId
     *            the value for tbl_attendance_history.child_id
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    public void setChildId(String childId) {
        this.childId = childId == null ? null : childId.trim();
    }

    public Integer getGroup() {
        return group;
    }

    public void setGroup(Integer group) {
        this.group = group;
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the
     * value of the database column tbl_attendance_history.center_id
     * 
     * @return the value of tbl_attendance_history.center_id
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    public String getCenterId() {
        return centerId;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the
     * value of the database column tbl_attendance_history.center_id
     * 
     * @param centerId
     *            the value for tbl_attendance_history.center_id
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    public void setCenterId(String centerId) {
        this.centerId = centerId == null ? null : centerId.trim();
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the
     * value of the database column tbl_attendance_history.room_id
     * 
     * @return the value of tbl_attendance_history.room_id
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    public String getRoomId() {
        return roomId;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the
     * value of the database column tbl_attendance_history.room_id
     * 
     * @param roomId
     *            the value for tbl_attendance_history.room_id
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    public void setRoomId(String roomId) {
        this.roomId = roomId == null ? null : roomId.trim();
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the
     * value of the database column tbl_attendance_history.monday
     * 
     * @return the value of tbl_attendance_history.monday
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    public Boolean getMonday() {
        return monday;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the
     * value of the database column tbl_attendance_history.monday
     * 
     * @param monday
     *            the value for tbl_attendance_history.monday
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    public void setMonday(Boolean monday) {
        this.monday = monday;
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the
     * value of the database column tbl_attendance_history.tuesday
     * 
     * @return the value of tbl_attendance_history.tuesday
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    public Boolean getTuesday() {
        return tuesday;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the
     * value of the database column tbl_attendance_history.tuesday
     * 
     * @param tuesday
     *            the value for tbl_attendance_history.tuesday
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    public void setTuesday(Boolean tuesday) {
        this.tuesday = tuesday;
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the
     * value of the database column tbl_attendance_history.wednesday
     * 
     * @return the value of tbl_attendance_history.wednesday
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    public Boolean getWednesday() {
        return wednesday;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the
     * value of the database column tbl_attendance_history.wednesday
     * 
     * @param wednesday
     *            the value for tbl_attendance_history.wednesday
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    public void setWednesday(Boolean wednesday) {
        this.wednesday = wednesday;
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the
     * value of the database column tbl_attendance_history.thursday
     * 
     * @return the value of tbl_attendance_history.thursday
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    public Boolean getThursday() {
        return thursday;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the
     * value of the database column tbl_attendance_history.thursday
     * 
     * @param thursday
     *            the value for tbl_attendance_history.thursday
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    public void setThursday(Boolean thursday) {
        this.thursday = thursday;
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the
     * value of the database column tbl_attendance_history.friday
     * 
     * @return the value of tbl_attendance_history.friday
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    public Boolean getFriday() {
        return friday;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the
     * value of the database column tbl_attendance_history.friday
     * 
     * @param friday
     *            the value for tbl_attendance_history.friday
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    public void setFriday(Boolean friday) {
        this.friday = friday;
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the
     * value of the database column tbl_attendance_history.create_time
     * 
     * @return the value of tbl_attendance_history.create_time
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the
     * value of the database column tbl_attendance_history.create_time
     * 
     * @param createTime
     *            the value for tbl_attendance_history.create_time
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the
     * value of the database column tbl_attendance_history.create_account_id
     * 
     * @return the value of tbl_attendance_history.create_account_id
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    public String getCreateAccountId() {
        return createAccountId;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the
     * value of the database column tbl_attendance_history.create_account_id
     * 
     * @param createAccountId
     *            the value for tbl_attendance_history.create_account_id
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    public void setCreateAccountId(String createAccountId) {
        this.createAccountId = createAccountId == null ? null : createAccountId.trim();
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the
     * value of the database column tbl_attendance_history.update_time
     * 
     * @return the value of tbl_attendance_history.update_time
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    public Date getUpdateTime() {
        return updateTime;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the
     * value of the database column tbl_attendance_history.update_time
     * 
     * @param updateTime
     *            the value for tbl_attendance_history.update_time
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the
     * value of the database column tbl_attendance_history.update_account_id
     * 
     * @return the value of tbl_attendance_history.update_account_id
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    public String getUpdateAccountId() {
        return updateAccountId;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the
     * value of the database column tbl_attendance_history.update_account_id
     * 
     * @param updateAccountId
     *            the value for tbl_attendance_history.update_account_id
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    public void setUpdateAccountId(String updateAccountId) {
        this.updateAccountId = updateAccountId == null ? null : updateAccountId.trim();
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the
     * value of the database column tbl_attendance_history.delete_flag
     * 
     * @return the value of tbl_attendance_history.delete_flag
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    public Short getDeleteFlag() {
        return deleteFlag;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the
     * value of the database column tbl_attendance_history.delete_flag
     * 
     * @param deleteFlag
     *            the value for tbl_attendance_history.delete_flag
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    public void setDeleteFlag(Short deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the
     * value of the database column tbl_attendance_history.begin_time
     * 
     * @return the value of tbl_attendance_history.begin_time
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    public Date getBeginTime() {
        return beginTime;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the
     * value of the database column tbl_attendance_history.begin_time
     * 
     * @param beginTime
     *            the value for tbl_attendance_history.begin_time
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the
     * value of the database column tbl_attendance_history.end_time
     * 
     * @return the value of tbl_attendance_history.end_time
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the
     * value of the database column tbl_attendance_history.end_time
     * 
     * @param endTime
     *            the value for tbl_attendance_history.end_time
     * 
     * @mbggenerated Thu Aug 18 16:19:17 CST 2016
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
}