package com.aoyuntek.aoyun.entity.po.task;

import java.util.Date;
import java.util.List;

import com.aoyuntek.aoyun.entity.vo.FormInfoVO;
import com.aoyuntek.framework.model.BaseModel;

public class TaskFollowUpInfo extends BaseModel {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String id;

    private String taskId;

    private String responsiblePersonId;

    private Short durationStep;

    private String completeStepId;

    private Boolean followFlag;

    private String createAccountId;

    private Date createTime;

    private String updateAccountId;

    private Date updateTime;

    private Short deleteFlag;

    private Short statu;

    private List<TaskVisibleInfo> responsibleTaskVisibles;

    private List<TaskVisibleInfo> taskImplementersVisibles;

    private Object responsibleTaskVisiblesSelect;

    private Object taskImplementersVisiblesSelect;
    
    private String formId;
    
    private FormInfoVO taskFormObj;
    
    public FormInfoVO getTaskFormObj() {
        return taskFormObj;
    }

    public void setTaskFormObj(FormInfoVO taskFormObj) {
        this.taskFormObj = taskFormObj;
    }

    public String getFormId() {
        return formId;
    }

    public void setFormId(String formId) {
        this.formId = formId;
    }

    public Object getResponsibleTaskVisiblesSelect() {
        return responsibleTaskVisiblesSelect;
    }

    public void setResponsibleTaskVisiblesSelect(Object responsibleTaskVisiblesSelect) {
        this.responsibleTaskVisiblesSelect = responsibleTaskVisiblesSelect;
    }

    public Object getTaskImplementersVisiblesSelect() {
        return taskImplementersVisiblesSelect;
    }

    public void setTaskImplementersVisiblesSelect(Object taskImplementersVisiblesSelect) {
        this.taskImplementersVisiblesSelect = taskImplementersVisiblesSelect;
    }

    public List<TaskVisibleInfo> getResponsibleTaskVisibles() {
        return responsibleTaskVisibles;
    }

    public void setResponsibleTaskVisibles(List<TaskVisibleInfo> responsibleTaskVisibles) {
        this.responsibleTaskVisibles = responsibleTaskVisibles;
    }

    public List<TaskVisibleInfo> getTaskImplementersVisibles() {
        return taskImplementersVisibles;
    }

    public void setTaskImplementersVisibles(List<TaskVisibleInfo> taskImplementersVisibles) {
        this.taskImplementersVisibles = taskImplementersVisibles;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId == null ? null : taskId.trim();
    }

    public String getResponsiblePersonId() {
        return responsiblePersonId;
    }

    public void setResponsiblePersonId(String responsiblePersonId) {
        this.responsiblePersonId = responsiblePersonId == null ? null : responsiblePersonId.trim();
    }

    public Short getDurationStep() {
        return durationStep;
    }

    public void setDurationStep(Short durationStep) {
        this.durationStep = durationStep;
    }

    public String getCompleteStepId() {
        return completeStepId;
    }

    public void setCompleteStepId(String completeStepId) {
        this.completeStepId = completeStepId == null ? null : completeStepId.trim();
    }

    public Boolean getFollowFlag() {
        return followFlag;
    }

    public void setFollowFlag(Boolean followFlag) {
        this.followFlag = followFlag;
    }

    public String getCreateAccountId() {
        return createAccountId;
    }

    public void setCreateAccountId(String createAccountId) {
        this.createAccountId = createAccountId == null ? null : createAccountId.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUpdateAccountId() {
        return updateAccountId;
    }

    public void setUpdateAccountId(String updateAccountId) {
        this.updateAccountId = updateAccountId == null ? null : updateAccountId.trim();
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Short getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Short deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public Short getStatu() {
        return statu;
    }

    public void setStatu(Short statu) {
        this.statu = statu;
    }
}