
package com.aoyuntek.aoyun.entity.vo;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.FormInfo;

@SuppressWarnings("serial")
public class FormInfoVO extends FormInfo {

    private List<FormAttributeInfoVO> attributes;

	public List<FormAttributeInfoVO> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<FormAttributeInfoVO> attributes) {
		this.attributes = attributes;
	}
}
