package com.aoyuntek.aoyun.entity.vo.child;

import java.util.List;

import com.aoyuntek.aoyun.entity.vo.AttendanceHistoryVo;
import com.aoyuntek.framework.model.BaseModel;

/**
 * 
 * @author dlli5 at 2016年11月30日上午9:01:46
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public class AttendanceHistoryGroupVo extends BaseModel {

    private static final long serialVersionUID = 1L;

    private String groupId;

    private String groupName;
    
    private Integer groupNo;

    private List<AttendanceHistoryVo> attendanceHistoryVos;

    public Integer getGroupNo() {
        return groupNo;
    }

    public void setGroupNo(Integer groupNo) {
        this.groupNo = groupNo;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<AttendanceHistoryVo> getAttendanceHistoryVos() {
        return attendanceHistoryVos;
    }

    public void setAttendanceHistoryVos(List<AttendanceHistoryVo> attendanceHistoryVos) {
        this.attendanceHistoryVos = attendanceHistoryVos;
    }
}
