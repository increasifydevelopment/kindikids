package com.aoyuntek.aoyun.entity.vo;

import java.io.Serializable;
import java.util.Date;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.aoyuntek.framework.deserializer.S3JsonSerializer;
import com.aoyuntek.framework.model.BaseModel;

/**
 * 
 * @author dlli5 at 2016年8月24日上午11:15:06
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public class AttendanceChildVo implements Serializable {

	private static final long serialVersionUID = 1L;
	private String name;
	private String img;
	private Boolean isSgin = null;
	private boolean isLeave;
	private String childId;
	private String centersId;
	private String roomId;
	private String personColor;
	private String absenteeId;
	private String lastName;
	private Boolean signOut = null;
	private Date birthday;
	private Integer[] age;
	private boolean interim = false;

	public boolean isInterim() {
		return interim;
	}

	public void setInterim(boolean interim) {
		this.interim = interim;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public Integer[] getAge() {
		return age;
	}

	public void setAge(Integer[] age) {
		this.age = age;
	}

	public Boolean getSignOut() {
		return signOut;
	}

	public void setSignOut(Boolean signOut) {
		this.signOut = signOut;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAbsenteeId() {
		return absenteeId;
	}

	public void setAbsenteeId(String absenteeId) {
		this.absenteeId = absenteeId;
	}

	public String getPersonColor() {
		return personColor;
	}

	public void setPersonColor(String personColor) {
		this.personColor = personColor;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonSerialize(using = S3JsonSerializer.class)
	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public Boolean isSgin() {
		return isSgin;
	}

	public void setSgin(Boolean isSgin) {
		this.isSgin = isSgin;
	}

	public boolean isLeave() {
		return isLeave;
	}

	public void setLeave(boolean isLeave) {
		this.isLeave = isLeave;
	}

	public String getChildId() {
		return childId;
	}

	public void setChildId(String childId) {
		this.childId = childId;
	}

	public String getCentersId() {
		return centersId;
	}

	public void setCentersId(String centersId) {
		this.centersId = centersId;
	}

	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}
}
