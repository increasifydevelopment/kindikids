package com.aoyuntek.aoyun.entity.po;

import java.util.Date;

import com.aoyuntek.framework.model.BaseModel;

@SuppressWarnings("serial")
public class FormInfo extends BaseModel{
    private String formId;

    private String code;

    private String name;

    private Date createTime;

    public String getFormId() {
        return formId;
    }

    public void setFormId(String formId) {
        this.formId = formId == null ? null : formId.trim();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}