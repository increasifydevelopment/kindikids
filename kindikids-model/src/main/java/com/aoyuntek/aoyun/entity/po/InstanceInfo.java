package com.aoyuntek.aoyun.entity.po;

import com.aoyuntek.framework.model.BaseModel;

@SuppressWarnings("serial")
public class InstanceInfo extends BaseModel {
    private String instanceId;

    private String formId;

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId == null ? null : instanceId.trim();
    }

    public String getFormId() {
        return formId;
    }

    public void setFormId(String formId) {
        this.formId = formId == null ? null : formId.trim();
    }
}