package com.aoyuntek.aoyun.entity.vo;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.FormAttributeInfo;
import com.aoyuntek.aoyun.entity.po.FormInfo;

@SuppressWarnings("serial")
public class FormInfoDataVO extends FormInfo {

    private List<FormAttributeInfo> attributes;

	public List<FormAttributeInfo> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<FormAttributeInfo> attributes) {
		this.attributes = attributes;
	}
}
