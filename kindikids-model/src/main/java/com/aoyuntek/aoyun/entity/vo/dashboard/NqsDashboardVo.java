package com.aoyuntek.aoyun.entity.vo.dashboard;

import java.math.BigDecimal;
import java.util.List;

import com.aoyuntek.framework.model.BaseModel;

public class NqsDashboardVo extends BaseModel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String rootNode;

    private String content;

    private int count;

    private int totalCount;

    private BigDecimal percent;

    private List<NqsDashboardVo> result;

    public NqsDashboardVo(String rootNode, String content, List<NqsDashboardVo> result) {
        this.rootNode = rootNode;
        this.content = content;
        this.result = result;
    }

    public List<NqsDashboardVo> getResult() {
        return result;
    }

    public void setResult(List<NqsDashboardVo> result) {
        this.result = result;
    }

    public NqsDashboardVo(String rootNode, String content, int count, int totalCount, BigDecimal percent) {
        super();
        this.rootNode = rootNode;
        this.content = content;
        this.count = count;
        this.totalCount = totalCount;
        this.percent = percent;
    }

    public NqsDashboardVo() {

    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getRootNode() {
        return rootNode;
    }

    public void setRootNode(String rootNode) {
        this.rootNode = rootNode;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public BigDecimal getPercent() {
        return percent;
    }

    public void setPercent(BigDecimal percent) {
        this.percent = percent;
    }

}
