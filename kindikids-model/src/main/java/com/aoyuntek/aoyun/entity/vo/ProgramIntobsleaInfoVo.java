package com.aoyuntek.aoyun.entity.vo;

import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.aoyuntek.aoyun.entity.po.program.ProgramIntobsleaInfo;
import com.aoyuntek.framework.deserializer.S3JsonSerializer;
import com.aoyuntek.framework.model.BaseModel;

public class ProgramIntobsleaInfoVo extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * ProgramIntobslea图片
     */
    private List<FileVo> images;

    /**
     * ProgramIntobslea信息
     */
    private ProgramIntobsleaInfo programIntobsleaInfo;

    /**
     * 小孩头像
     */
    private String avatar;

    /**
     * 小孩颜色
     */
    private String personColor;

    /**
     * 
     */
    private boolean selected = true;
    
    /**
     * 小孩全名
     */
    private String text;
    
    public ProgramIntobsleaInfo getProgramIntobsleaInfo() {
        return programIntobsleaInfo;
    }

    public void setProgramIntobsleaInfo(ProgramIntobsleaInfo programIntobsleaInfo) {
        this.programIntobsleaInfo = programIntobsleaInfo;
    }

    public List<FileVo> getImages() {
        return images;
    }

    public void setImages(List<FileVo> images) {
        this.images = images;
    }

    @JsonSerialize(using = S3JsonSerializer.class)
    public String getChildAvatar() {
        return avatar;
    }

    public void setChildAvatar(String childAvatar) {
        this.avatar = childAvatar;
    }

    public String getPersonColor() {
        return personColor;
    }

    public void setPersonColor(String personColor) {
        this.personColor = personColor;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
    
    

}
