package com.aoyuntek.aoyun.entity.vo.todaynote;

import java.util.List;

import com.aoyuntek.aoyun.enums.TodayNoteType;

public class TodayNoteVo2 {
    private String type;
    private List<NoteKeyValueVo> list;

    public List<NoteKeyValueVo> getList() {
        return list;
    }

    public TodayNoteVo2(String type, List<NoteKeyValueVo> list) {
        super();
        this.type = type;
        this.list = list;
    }

    public void setList(List<NoteKeyValueVo> list) {
        this.list = list;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public TodayNoteVo2(String type) {
        super();
        this.type = type;
    }

    public TodayNoteVo2() {
    }

}
