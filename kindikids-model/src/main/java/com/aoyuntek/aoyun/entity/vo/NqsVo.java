package com.aoyuntek.aoyun.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aoyuntek.aoyun.entity.po.NqsInfo;

public class NqsVo extends NqsInfo {
    /**
     * 序列化
     */
    private static final long serialVersionUID = 1L;
    private List<NqsVo> nqsChild = new ArrayList<NqsVo>();

    public List<NqsVo> getNqsChild() {
        return nqsChild;
    }

    public void setNqsChild(List<NqsVo> nqsChild) {
        this.nqsChild = nqsChild;
    }

}
