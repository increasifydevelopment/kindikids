package com.aoyuntek.aoyun.entity.po;

import java.util.UUID;

import com.aoyuntek.framework.model.BaseModel;

public class OutRelevantInfo extends BaseModel {

    private static final long serialVersionUID = 1L;

    private String id;

    private String childUserId;

    private String relationship1;

    private String relationship2;

    private String nationality;

    private String language;

    private String religion;

    private Short hasImmunised;

    private Short hasAnaphylaxis;

    private String learningDifficulties;

    private String medicalConditions;

    private String dietaryRequipments;

    private String allergies;

    private String proposal;

    private Short channel;
    
    private String channelOther;

    public OutRelevantInfo() {

    }

    public OutRelevantInfo(String childUserId, String relationship1, String relationship2, String nationality, String language, String religion,
            Short hasImmunised, Short hasAnaphylaxis, String learningDifficulties, String medicalConditions, String dietaryRequipments,
            String allergies, String proposal, Short channel) {
        this.id = UUID.randomUUID().toString();
        this.childUserId = childUserId;
        this.relationship1 = relationship1;
        this.relationship2 = relationship2;
        this.nationality = nationality;
        this.language = language;
        this.religion = religion;
        this.hasImmunised = hasImmunised;
        this.hasAnaphylaxis = hasAnaphylaxis;
        this.learningDifficulties = learningDifficulties;
        this.medicalConditions = medicalConditions;
        this.dietaryRequipments = dietaryRequipments;
        this.allergies = allergies;
        this.proposal = proposal;
        this.channel = channel;
    }

    public String getChannelOther() {
        return channelOther;
    }

    public void setChannelOther(String channelOther) {
        this.channelOther = channelOther;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getChildUserId() {
        return childUserId;
    }

    public void setChildUserId(String childUserId) {
        this.childUserId = childUserId == null ? null : childUserId.trim();
    }

    public String getRelationship1() {
        return relationship1;
    }

    public String getRelationship2() {
        return relationship2;
    }

    public void setRelationship1(String relationship1) {
        this.relationship1 = relationship1;
    }

    public void setRelationship2(String relationship2) {
        this.relationship2 = relationship2;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality == null ? null : nationality.trim();
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language == null ? null : language.trim();
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion == null ? null : religion.trim();
    }

    public Short getHasImmunised() {
        return hasImmunised;
    }

    public void setHasImmunised(Short hasImmunised) {
        this.hasImmunised = hasImmunised;
    }

    public Short getHasAnaphylaxis() {
        return hasAnaphylaxis;
    }

    public void setHasAnaphylaxis(Short hasAnaphylaxis) {
        this.hasAnaphylaxis = hasAnaphylaxis;
    }

    public String getLearningDifficulties() {
        return learningDifficulties;
    }

    public void setLearningDifficulties(String learningDifficulties) {
        this.learningDifficulties = learningDifficulties == null ? null : learningDifficulties.trim();
    }

    public String getMedicalConditions() {
        return medicalConditions;
    }

    public void setMedicalConditions(String medicalConditions) {
        this.medicalConditions = medicalConditions == null ? null : medicalConditions.trim();
    }

    public String getDietaryRequipments() {
        return dietaryRequipments;
    }

    public void setDietaryRequipments(String dietaryRequipments) {
        this.dietaryRequipments = dietaryRequipments == null ? null : dietaryRequipments.trim();
    }

    public String getAllergies() {
        return allergies;
    }

    public void setAllergies(String allergies) {
        this.allergies = allergies == null ? null : allergies.trim();
    }

    public String getProposal() {
        return proposal;
    }

    public void setProposal(String proposal) {
        this.proposal = proposal == null ? null : proposal.trim();
    }

    public Short getChannel() {
        return channel;
    }

    public void setChannel(Short channel) {
        this.channel = channel;
    }
}