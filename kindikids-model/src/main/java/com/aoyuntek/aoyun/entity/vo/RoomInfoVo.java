package com.aoyuntek.aoyun.entity.vo;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.RoomGroupInfo;
import com.aoyuntek.aoyun.entity.po.RoomInfo;

/**
 * 
 * @description roomVo
 * @author gfwang
 * @create 2016年8月20日上午11:44:28
 * @version 1.0
 */
public class RoomInfoVo extends RoomInfo {

    private static final long serialVersionUID = 1L;

    private List<RoomGroupInfo> groupList;

    public List<RoomGroupInfo> getGroupList() {
        return groupList;
    }

    public void setGroupList(List<RoomGroupInfo> groupList) {
        this.groupList = groupList;
    }

}
