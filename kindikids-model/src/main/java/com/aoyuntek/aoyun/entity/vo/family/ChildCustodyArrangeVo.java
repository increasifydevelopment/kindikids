package com.aoyuntek.aoyun.entity.vo.family;

import com.aoyuntek.aoyun.entity.po.ChildCustodyArrangeInfo;

@SuppressWarnings("serial")
public class ChildCustodyArrangeVo extends ChildCustodyArrangeInfo {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
