package com.aoyuntek.aoyun.entity.vo.dashboard;

import java.util.List;

import com.aoyuntek.aoyun.entity.vo.task.PersonSelectVo;
import com.aoyuntek.framework.model.BaseModel;

public class DashboardGroup extends BaseModel {
    private String text;

    private List<PersonSelectVo> children;

    public DashboardGroup(String text, List<PersonSelectVo> children) {
        super();
        this.text = text;
        this.children = children;
    }

    public DashboardGroup() {

    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<PersonSelectVo> getChildren() {
        return children;
    }

    public void setChildren(List<PersonSelectVo> children) {
        this.children = children;
    }

}
