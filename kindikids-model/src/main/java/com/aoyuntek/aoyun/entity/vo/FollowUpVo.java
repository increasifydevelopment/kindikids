package com.aoyuntek.aoyun.entity.vo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramIntobsleaInfo;
import com.aoyuntek.aoyun.entity.po.program.TaskProgramFollowUpInfo;
import com.aoyuntek.framework.model.BaseModel;

public class FollowUpVo extends BaseModel {

	private static final long serialVersionUID = 1L;

	private ProgramIntobsleaInfo intobsleaInfo = new ProgramIntobsleaInfo();

	private UserInfo child = new UserInfo();

	private TaskProgramFollowUpInfo followUpInfo = new TaskProgramFollowUpInfo();

	private List<FileVo> fileList = new ArrayList<FileVo>();;

	private List<EylfInfoVo> eylfList = new ArrayList<EylfInfoVo>();

	private boolean flag = false;

	private Date classDate;

	public UserInfo getChild() {
		return child;
	}

	public void setChild(UserInfo child) {
		this.child = child;
	}

	public ProgramIntobsleaInfo getIntobsleaInfo() {
		return intobsleaInfo;
	}

	public void setIntobsleaInfo(ProgramIntobsleaInfo intobsleaInfo) {
		this.intobsleaInfo = intobsleaInfo;
	}

	public TaskProgramFollowUpInfo getFollowUpInfo() {
		return followUpInfo;
	}

	public void setFollowUpInfo(TaskProgramFollowUpInfo followUpInfo) {
		this.followUpInfo = followUpInfo;
	}

	public List<FileVo> getFileList() {
		return fileList;
	}

	public void setFileList(List<FileVo> fileList) {
		this.fileList = fileList;
	}

	public List<EylfInfoVo> getEylfList() {
		return eylfList;
	}

	public void setEylfList(List<EylfInfoVo> eylfList) {
		this.eylfList = eylfList;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public Date getClassDate() {
		return classDate;
	}

	public void setClassDate(Date classDate) {
		this.classDate = classDate;
	}

}
