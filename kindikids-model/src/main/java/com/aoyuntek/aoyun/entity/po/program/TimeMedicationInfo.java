package com.aoyuntek.aoyun.entity.po.program;

import java.util.Date;

import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.framework.model.BaseModel;

public class TimeMedicationInfo extends BaseModel {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String id;

    private String programMedicationId;

    private Date day;

    private Boolean chooseFlag;

    private String createAccountId;

    private Date createTime;

    private Date updateTime;

    private String updateAccountId;

    private Short deleteFlag;
    
    private SelecterPo administeringValue=new SelecterPo();
    private SelecterPo checkingValue=new SelecterPo();
    
    private String staffAdministeringSignature;

    private String staffChecking;
    

    public SelecterPo getAdministeringValue() {
        return administeringValue;
    }

    public void setAdministeringValue(SelecterPo administeringValue) {
        this.administeringValue = administeringValue;
    }

    public SelecterPo getCheckingValue() {
        return checkingValue;
    }

    public void setCheckingValue(SelecterPo checkingValue) {
        this.checkingValue = checkingValue;
    }

    public String getStaffAdministeringSignature() {
        return staffAdministeringSignature;
    }

    public void setStaffAdministeringSignature(String staffAdministeringSignature) {
        this.staffAdministeringSignature = staffAdministeringSignature;
    }

    public String getStaffChecking() {
        return staffChecking;
    }

    public void setStaffChecking(String staffChecking) {
        this.staffChecking = staffChecking;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getProgramMedicationId() {
        return programMedicationId;
    }

    public void setProgramMedicationId(String programMedicationId) {
        this.programMedicationId = programMedicationId == null ? null : programMedicationId.trim();
    }

    public Date getDay() {
        return day;
    }

    public void setDay(Date day) {
        this.day = day;
    }

    public Boolean getChooseFlag() {
        return chooseFlag;
    }

    public void setChooseFlag(Boolean chooseFlag) {
        this.chooseFlag = chooseFlag;
    }

    public String getCreateAccountId() {
        return createAccountId;
    }

    public void setCreateAccountId(String createAccountId) {
        this.createAccountId = createAccountId == null ? null : createAccountId.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateAccountId() {
        return updateAccountId;
    }

    public void setUpdateAccountId(String updateAccountId) {
        this.updateAccountId = updateAccountId == null ? null : updateAccountId.trim();
    }

    public Short getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Short deleteFlag) {
        this.deleteFlag = deleteFlag;
    }
}