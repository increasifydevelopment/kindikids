package com.aoyuntek.aoyun.entity.vo.meeting;

import java.util.List;

import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.framework.model.BaseModel;

public class TableColRowVo extends BaseModel {
    /**
     * 序列化
     */
    private static final long serialVersionUID = 1L;
    private String colName;
    private String colType;
    private String rowValue;
    private List<SelecterPo> responsibility;

    public List<SelecterPo> getResponsibility() {
        return responsibility;
    }

    public void setResponsibility(List<SelecterPo> responsibility) {
        this.responsibility = responsibility;
    }

    public String getColName() {
        return colName;
    }

    public void setColName(String colName) {
        this.colName = colName;
    }

    public String getColType() {
        return colType;
    }

    public void setColType(String colType) {
        this.colType = colType;
    }

    public String getRowValue() {
        return rowValue;
    }

    public void setRowValue(String rowValue) {
        this.rowValue = rowValue;
    }

}
