package com.aoyuntek.aoyun.entity.vo;

import com.aoyuntek.aoyun.entity.po.program.EylfCheckInfo;

/**
 * @description 小孩完成Eylf进度
 * @author Hxzhang 2016年9月22日上午9:24:29
 * @version 1.0
 */
public class ChildEylfVo extends EylfCheckInfo {

    private static final long serialVersionUID = 1L;

    private Short value;

    private String reason;

    public Short getValue() {
        return value;
    }

    public void setValue(Short value) {
        this.value = value;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
