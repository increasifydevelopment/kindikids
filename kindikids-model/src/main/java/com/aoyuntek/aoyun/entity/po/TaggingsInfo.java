package com.aoyuntek.aoyun.entity.po;

public class TaggingsInfo {
    private String employmentTagsId;

    private String tagId;

    public String getEmploymentTagsId() {
        return employmentTagsId;
    }

    public void setEmploymentTagsId(String employmentTagsId) {
        this.employmentTagsId = employmentTagsId == null ? null : employmentTagsId.trim();
    }

    public String getTagId() {
        return tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId == null ? null : tagId.trim();
    }
}