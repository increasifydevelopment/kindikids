package com.aoyuntek.aoyun.entity.po;

import java.util.Date;

import com.aoyuntek.framework.model.BaseModel;

public class AuthorityGroupInfo extends BaseModel {
   /**
     * 
     */
    private static final long serialVersionUID = 1L;

 private String id;

    private String groupName;

    private Short status;

 

    private Date createTime;

    private Short deleteFlag;

    private String sqlTag;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName == null ? null : groupName.trim();
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Short getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Short deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getSqlTag() {
        return sqlTag;
    }

    public void setSqlTag(String sqlTag) {
        this.sqlTag = sqlTag == null ? null : sqlTag.trim();
    }
}