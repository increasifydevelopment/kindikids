package com.aoyuntek.aoyun.entity.vo.roster;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author dlli5 at 2016年10月14日上午8:48:21
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public class RosterStaffShiftTimeVo extends RosterShiftTimeVo {

	private static final long serialVersionUID = 1L;

	private Integer row;

	private String title;

	private List<RosterStaffShiftVo> rosterStaffShiftVos = new ArrayList<RosterStaffShiftVo>();

	private List<RosterStaffVo> rosterStaffVos = new ArrayList<RosterStaffVo>();

	public RosterStaffShiftTimeVo() {
		super();
	}

	public RosterStaffShiftTimeVo(String title, List<RosterStaffShiftVo> rosterStaffShiftVos, Integer row) {
		super();
		this.title = title;
		this.rosterStaffShiftVos = rosterStaffShiftVos;
		this.row = row;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<RosterStaffShiftVo> getRosterStaffShiftVos() {
		return rosterStaffShiftVos;
	}

	public void setRosterStaffShiftVos(List<RosterStaffShiftVo> rosterStaffShiftVos) {
		this.rosterStaffShiftVos = rosterStaffShiftVos;
	}

	public Integer getRow() {
		return row;
	}

	public void setRow(Integer row) {
		this.row = row;
	}

	public List<RosterStaffVo> getRosterStaffVos() {
		return rosterStaffVos;
	}

	public void setRosterStaffVos(List<RosterStaffVo> rosterStaffVos) {
		this.rosterStaffVos = rosterStaffVos;
	}
}
