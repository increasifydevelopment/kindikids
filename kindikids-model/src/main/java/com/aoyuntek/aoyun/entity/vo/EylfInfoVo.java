package com.aoyuntek.aoyun.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aoyuntek.aoyun.entity.po.YelfInfo;

public class EylfInfoVo extends YelfInfo {

    private static final long serialVersionUID = 1L;

    private List<FollowupEylfVo> eylfCilckList = new ArrayList<FollowupEylfVo>();

    private List<ChildEylfVo> eylfCheckList = new ArrayList<ChildEylfVo>();

    public List<FollowupEylfVo> getEylfCilckList() {
        return eylfCilckList;
    }

    public void setEylfCilckList(List<FollowupEylfVo> eylfCilckList) {
        this.eylfCilckList = eylfCilckList;
    }

    public List<ChildEylfVo> getEylfCheckList() {
        return eylfCheckList;
    }

    public void setEylfCheckList(List<ChildEylfVo> eylfCheckList) {
        this.eylfCheckList = eylfCheckList;
    }
}
