package com.aoyuntek.aoyun.entity.vo.event;

import java.util.ArrayList;
import java.util.List;

import com.aoyuntek.aoyun.entity.po.event.EventInfo;
import com.aoyuntek.aoyun.entity.vo.NqsVo;

/**
 * event Info vo
 * 
 * @author bing
 *
 */
public class EventInfoVo extends EventInfo {
	// visibility 0:public 1:staff 2:parents

	private static final long serialVersionUID = 1L;

	private List<NqsVo> nqsInfo = new ArrayList<NqsVo>();

	private List<EventDocumentVo> eventDocumentVoList = new ArrayList<EventDocumentVo>();

	private boolean confirm;

	public boolean isConfirm() {
		return confirm;
	}

	public void setConfirm(boolean confirm) {
		this.confirm = confirm;
	}

	public List<NqsVo> getNqsInfo() {
		return nqsInfo;
	}

	public void setNqsInfo(List<NqsVo> nqsInfo) {
		this.nqsInfo = nqsInfo;
	}

	public List<EventDocumentVo> getEventDocumentVoList() {
		return eventDocumentVoList;
	}

	public void setEventDocumentVoList(List<EventDocumentVo> eventDocumentVoList) {
		this.eventDocumentVoList = eventDocumentVoList;
	}
}
