package com.aoyuntek.aoyun.entity.vo;

import java.util.Collection;

import com.aoyuntek.framework.model.BaseModel;

/**
 * @description 发送邮件实体
 * @author Hxzhang 2016年9月2日下午5:11:08
 * @version 1.0
 */
public class EmailVo extends BaseModel {

    /**
     * 序列化
     */
    private static final long serialVersionUID = 1L;
    /**
     * 发送者地址
     */
    private String sendAddress;
    /**
     * 发送者名称
     */
    private String sendName;
    /**
     * 接收者地址
     */
    private String receiverAddress;
    /**
     * 接受者姓名
     */
    private String receiverName;
    /**
     * 邮件标题
     */
    private String sub;
    /**
     * 邮件内容
     */
    private String msg;
    /**
     * 是否是html
     */
    private boolean isHtml;
    /**
     * replyTos
     */
    private String replyTos;
    /**
     * 附件
     */
    private Collection attachments;
    /**
     * email_key
     */
    private String email_key;
    /**
     * 秘密抄送人
     */
    private String[] bccs;

    public String[] getBccs() {
        return bccs;
    }

    public void setBccs(String[] bccs) {
        this.bccs = bccs;
    }

    public String getSendAddress() {
        return sendAddress;
    }

    public void setSendAddress(String sendAddress) {
        this.sendAddress = sendAddress;
    }

    public String getSendName() {
        return sendName;
    }

    public void setSendName(String sendName) {
        this.sendName = sendName;
    }

    public String getReceiverAddress() {
        return receiverAddress;
    }

    public void setReceiverAddress(String receiverAddress) {
        this.receiverAddress = receiverAddress;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean isHtml() {
        return isHtml;
    }

    public void setHtml(boolean isHtml) {
        this.isHtml = isHtml;
    }

    public String getReplyTos() {
        return replyTos;
    }

    public void setReplyTos(String replyTos) {
        this.replyTos = replyTos;
    }

    public Collection getAttachments() {
        return attachments;
    }

    public void setAttachments(Collection attachments) {
        this.attachments = attachments;
    }

    public String getEmail_key() {
        return email_key;
    }

    public void setEmail_key(String email_key) {
        this.email_key = email_key;
    }

    public EmailVo() {
        super();
    }

    public EmailVo(String receiverAddress, String receiverName, String sub, String msg, boolean isHtml) {
        super();
        this.receiverAddress = receiverAddress;
        this.receiverName = receiverName;
        this.sub = sub;
        this.msg = msg;
        this.isHtml = isHtml;
    }

    public EmailVo(String receiverAddress, String receiverName, String sub, String msg, boolean isHtml, String[] bccs) {
        super();
        this.receiverAddress = receiverAddress;
        this.receiverName = receiverName;
        this.sub = sub;
        this.msg = msg;
        this.isHtml = isHtml;
        this.bccs = bccs;
    }

}
