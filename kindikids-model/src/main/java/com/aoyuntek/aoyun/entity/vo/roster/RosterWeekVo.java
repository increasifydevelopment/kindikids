package com.aoyuntek.aoyun.entity.vo.roster;

import java.util.ArrayList;
import java.util.List;

import com.aoyuntek.aoyun.entity.po.roster.RosterInfo;
import com.aoyuntek.aoyun.entity.vo.ClosurePeriodsVo;
import com.aoyuntek.framework.model.BaseModel;

/**
 * 
 * @author dlli5 at 2016年10月17日上午10:29:34
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public class RosterWeekVo extends BaseModel {

	private static final long serialVersionUID = 1L;

	private List<ClosurePeriodsVo> closurePeriodsVos = new ArrayList<ClosurePeriodsVo>();

	private RosterInfo rosterInfo;

	private List<RosterRoleDayVo> roleDayVos = new ArrayList<RosterRoleDayVo>();

	private List<RosterStaffShiftTimeVo> rosterStaffShiftTimeVos = new ArrayList<RosterStaffShiftTimeVo>();

	public List<ClosurePeriodsVo> getClosurePeriodsVos() {
		return closurePeriodsVos;
	}

	public void setClosurePeriodsVos(List<ClosurePeriodsVo> closurePeriodsVos) {
		this.closurePeriodsVos = closurePeriodsVos;
	}

	public RosterInfo getRosterInfo() {
		return rosterInfo;
	}

	public void setRosterInfo(RosterInfo rosterInfo) {
		this.rosterInfo = rosterInfo;
	}

	public List<RosterRoleDayVo> getRoleDayVos() {
		return roleDayVos;
	}

	public void setRoleDayVos(List<RosterRoleDayVo> roleDayVos) {
		this.roleDayVos = roleDayVos;
	}

	public List<RosterStaffShiftTimeVo> getRosterStaffShiftTimeVos() {
		return rosterStaffShiftTimeVos;
	}

	public void setRosterStaffShiftTimeVos(List<RosterStaffShiftTimeVo> rosterStaffShiftTimeVos) {
		this.rosterStaffShiftTimeVos = rosterStaffShiftTimeVos;
	}
}
