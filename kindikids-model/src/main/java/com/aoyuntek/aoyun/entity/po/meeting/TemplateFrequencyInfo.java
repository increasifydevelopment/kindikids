package com.aoyuntek.aoyun.entity.po.meeting;

import java.util.Date;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.aoyuntek.framework.deserializer.DateJsonSerializer2;
import com.aoyuntek.framework.model.BaseModel;

public class TemplateFrequencyInfo extends BaseModel {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String id;

    private String templateId;

    private Short repeats;

    private Date startsOnDate;

    private Date endOnDate;

    private String weekdayStart;

    private Short haveEndDate;

    private String weekdayDue;

    private Date createTime;

    private String createAccountId;

    private Date updateTime;

    private String updateAccountId;

    private Short deleteFlag;

    public Short getHaveEndDate() {
        return haveEndDate;
    }

    public void setHaveEndDate(Short haveEndDate) {
        this.haveEndDate = haveEndDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId == null ? null : templateId.trim();
    }

    public Short getRepeats() {
        return repeats;
    }

    public void setRepeats(Short repeats) {
        this.repeats = repeats;
    }

    @JsonSerialize(using = DateJsonSerializer2.class, include = JsonSerialize.Inclusion.ALWAYS)
    public Date getStartsOnDate() {
        return startsOnDate;
    }

    public void setStartsOnDate(Date startsOnDate) {
        this.startsOnDate = startsOnDate;
    }
    @JsonSerialize(using = DateJsonSerializer2.class, include = JsonSerialize.Inclusion.ALWAYS)
    public Date getEndOnDate() {
        return endOnDate;
    }

    public void setEndOnDate(Date endOnDate) {
        this.endOnDate = endOnDate;
    }

    public String getWeekdayStart() {
        return weekdayStart;
    }

    public void setWeekdayStart(String weekdayStart) {
        this.weekdayStart = weekdayStart == null ? null : weekdayStart.trim();
    }

    public String getWeekdayDue() {
        return weekdayDue;
    }

    public void setWeekdayDue(String weekdayDue) {
        this.weekdayDue = weekdayDue == null ? null : weekdayDue.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateAccountId() {
        return createAccountId;
    }

    public void setCreateAccountId(String createAccountId) {
        this.createAccountId = createAccountId == null ? null : createAccountId.trim();
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateAccountId() {
        return updateAccountId;
    }

    public void setUpdateAccountId(String updateAccountId) {
        this.updateAccountId = updateAccountId == null ? null : updateAccountId.trim();
    }

    public Short getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Short deleteFlag) {
        this.deleteFlag = deleteFlag;
    }
}