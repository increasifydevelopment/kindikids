package com.aoyuntek.aoyun.entity.po;

import java.util.Date;

import com.aoyuntek.framework.model.BaseModel;

public class ChildHealthMedicalDetailInfo extends BaseModel {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbl_child_health_medical_detail.id
     *
     * @mbggenerated Mon Aug 08 20:02:37 CST 2016
     */
    private String id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbl_child_health_medical_detail.user_id
     *
     * @mbggenerated Mon Aug 08 20:02:37 CST 2016
     */
    private String userId;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbl_child_health_medical_detail.name_index
     *
     * @mbggenerated Mon Aug 08 20:02:37 CST 2016
     */
    private Integer nameIndex;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbl_child_health_medical_detail.birth_check
     *
     * @mbggenerated Mon Aug 08 20:02:37 CST 2016
     */
    private Boolean birthCheck;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbl_child_health_medical_detail.mo2_check
     *
     * @mbggenerated Mon Aug 08 20:02:37 CST 2016
     */
    private Boolean mo2Check;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbl_child_health_medical_detail.mo4_check
     *
     * @mbggenerated Mon Aug 08 20:02:37 CST 2016
     */
    private Boolean mo4Check;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbl_child_health_medical_detail.mo6_check
     *
     * @mbggenerated Mon Aug 08 20:02:37 CST 2016
     */
    private Boolean mo6Check;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbl_child_health_medical_detail.mo12_check
     *
     * @mbggenerated Mon Aug 08 20:02:37 CST 2016
     */
    private Boolean mo12Check;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbl_child_health_medical_detail.mo18_check
     *
     * @mbggenerated Mon Aug 08 20:02:37 CST 2016
     */
    private Boolean mo18Check;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbl_child_health_medical_detail.yrs4_check
     *
     * @mbggenerated Mon Aug 08 20:02:37 CST 2016
     */
    private Boolean yrs4Check;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbl_child_health_medical_detail.create_time
     *
     * @mbggenerated Mon Aug 08 20:02:37 CST 2016
     */
    private Date createTime;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbl_child_health_medical_detail.delete_flag
     *
     * @mbggenerated Mon Aug 08 20:02:37 CST 2016
     */
    private Short deleteFlag;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbl_child_health_medical_detail.id
     *
     * @return the value of tbl_child_health_medical_detail.id
     *
     * @mbggenerated Mon Aug 08 20:02:37 CST 2016
     */
    public String getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbl_child_health_medical_detail.id
     *
     * @param id the value for tbl_child_health_medical_detail.id
     *
     * @mbggenerated Mon Aug 08 20:02:37 CST 2016
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbl_child_health_medical_detail.user_id
     *
     * @return the value of tbl_child_health_medical_detail.user_id
     *
     * @mbggenerated Mon Aug 08 20:02:37 CST 2016
     */
    public String getUserId() {
        return userId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbl_child_health_medical_detail.user_id
     *
     * @param userId the value for tbl_child_health_medical_detail.user_id
     *
     * @mbggenerated Mon Aug 08 20:02:37 CST 2016
     */
    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbl_child_health_medical_detail.name_index
     *
     * @return the value of tbl_child_health_medical_detail.name_index
     *
     * @mbggenerated Mon Aug 08 20:02:37 CST 2016
     */
    public Integer getNameIndex() {
        return nameIndex;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbl_child_health_medical_detail.name_index
     *
     * @param nameIndex the value for tbl_child_health_medical_detail.name_index
     *
     * @mbggenerated Mon Aug 08 20:02:37 CST 2016
     */
    public void setNameIndex(Integer nameIndex) {
        this.nameIndex = nameIndex;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbl_child_health_medical_detail.birth_check
     *
     * @return the value of tbl_child_health_medical_detail.birth_check
     *
     * @mbggenerated Mon Aug 08 20:02:37 CST 2016
     */
    public Boolean getBirthCheck() {
        return birthCheck;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbl_child_health_medical_detail.birth_check
     *
     * @param birthCheck the value for tbl_child_health_medical_detail.birth_check
     *
     * @mbggenerated Mon Aug 08 20:02:37 CST 2016
     */
    public void setBirthCheck(Boolean birthCheck) {
        this.birthCheck = birthCheck;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbl_child_health_medical_detail.mo2_check
     *
     * @return the value of tbl_child_health_medical_detail.mo2_check
     *
     * @mbggenerated Mon Aug 08 20:02:37 CST 2016
     */
    public Boolean getMo2Check() {
        return mo2Check;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbl_child_health_medical_detail.mo2_check
     *
     * @param mo2Check the value for tbl_child_health_medical_detail.mo2_check
     *
     * @mbggenerated Mon Aug 08 20:02:37 CST 2016
     */
    public void setMo2Check(Boolean mo2Check) {
        this.mo2Check = mo2Check;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbl_child_health_medical_detail.mo4_check
     *
     * @return the value of tbl_child_health_medical_detail.mo4_check
     *
     * @mbggenerated Mon Aug 08 20:02:37 CST 2016
     */
    public Boolean getMo4Check() {
        return mo4Check;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbl_child_health_medical_detail.mo4_check
     *
     * @param mo4Check the value for tbl_child_health_medical_detail.mo4_check
     *
     * @mbggenerated Mon Aug 08 20:02:37 CST 2016
     */
    public void setMo4Check(Boolean mo4Check) {
        this.mo4Check = mo4Check;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbl_child_health_medical_detail.mo6_check
     *
     * @return the value of tbl_child_health_medical_detail.mo6_check
     *
     * @mbggenerated Mon Aug 08 20:02:37 CST 2016
     */
    public Boolean getMo6Check() {
        return mo6Check;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbl_child_health_medical_detail.mo6_check
     *
     * @param mo6Check the value for tbl_child_health_medical_detail.mo6_check
     *
     * @mbggenerated Mon Aug 08 20:02:37 CST 2016
     */
    public void setMo6Check(Boolean mo6Check) {
        this.mo6Check = mo6Check;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbl_child_health_medical_detail.mo12_check
     *
     * @return the value of tbl_child_health_medical_detail.mo12_check
     *
     * @mbggenerated Mon Aug 08 20:02:37 CST 2016
     */
    public Boolean getMo12Check() {
        return mo12Check;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbl_child_health_medical_detail.mo12_check
     *
     * @param mo12Check the value for tbl_child_health_medical_detail.mo12_check
     *
     * @mbggenerated Mon Aug 08 20:02:37 CST 2016
     */
    public void setMo12Check(Boolean mo12Check) {
        this.mo12Check = mo12Check;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbl_child_health_medical_detail.mo18_check
     *
     * @return the value of tbl_child_health_medical_detail.mo18_check
     *
     * @mbggenerated Mon Aug 08 20:02:37 CST 2016
     */
    public Boolean getMo18Check() {
        return mo18Check;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbl_child_health_medical_detail.mo18_check
     *
     * @param mo18Check the value for tbl_child_health_medical_detail.mo18_check
     *
     * @mbggenerated Mon Aug 08 20:02:37 CST 2016
     */
    public void setMo18Check(Boolean mo18Check) {
        this.mo18Check = mo18Check;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbl_child_health_medical_detail.yrs4_check
     *
     * @return the value of tbl_child_health_medical_detail.yrs4_check
     *
     * @mbggenerated Mon Aug 08 20:02:37 CST 2016
     */
    public Boolean getYrs4Check() {
        return yrs4Check;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbl_child_health_medical_detail.yrs4_check
     *
     * @param yrs4Check the value for tbl_child_health_medical_detail.yrs4_check
     *
     * @mbggenerated Mon Aug 08 20:02:37 CST 2016
     */
    public void setYrs4Check(Boolean yrs4Check) {
        this.yrs4Check = yrs4Check;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbl_child_health_medical_detail.create_time
     *
     * @return the value of tbl_child_health_medical_detail.create_time
     *
     * @mbggenerated Mon Aug 08 20:02:37 CST 2016
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbl_child_health_medical_detail.create_time
     *
     * @param createTime the value for tbl_child_health_medical_detail.create_time
     *
     * @mbggenerated Mon Aug 08 20:02:37 CST 2016
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbl_child_health_medical_detail.delete_flag
     *
     * @return the value of tbl_child_health_medical_detail.delete_flag
     *
     * @mbggenerated Mon Aug 08 20:02:37 CST 2016
     */
    public Short getDeleteFlag() {
        return deleteFlag;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbl_child_health_medical_detail.delete_flag
     *
     * @param deleteFlag the value for tbl_child_health_medical_detail.delete_flag
     *
     * @mbggenerated Mon Aug 08 20:02:37 CST 2016
     */
    public void setDeleteFlag(Short deleteFlag) {
        this.deleteFlag = deleteFlag;
    }
}