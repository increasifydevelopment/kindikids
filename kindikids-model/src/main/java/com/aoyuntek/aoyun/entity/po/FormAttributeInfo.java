package com.aoyuntek.aoyun.entity.po;

import com.aoyuntek.aoyun.entity.vo.FormAttributeInfoVO;
import com.aoyuntek.framework.model.BaseModel;
import com.theone.json.util.JsonUtil;

@SuppressWarnings("serial")
public class FormAttributeInfo extends BaseModel{
	
	public FormAttributeInfo(){
		
	}
	
	public FormAttributeInfo(FormAttributeInfoVO vo){
		this.attrId = vo.getAttrId();
		this.formId = vo.getFormId();
		this.name = vo.getName();
		this.labelText = vo.getLabelText();
		this.type = vo.getType();
		this.json = JsonUtil.objectToJson( vo.getJson());
		this.sort = vo.getSort();
	}

    private String attrId;

    private String formId;

    private String name;

    private String labelText;

    private Short type;

    private String json;

    private Integer sort;

    private Short deleteFlag;

    public String getAttrId() {
        return attrId;
    }

    public void setAttrId(String attrId) {
        this.attrId = attrId == null ? null : attrId.trim();
    }

    public String getFormId() {
        return formId;
    }

    public void setFormId(String formId) {
        this.formId = formId == null ? null : formId.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getLabelText() {
        return labelText;
    }

    public void setLabelText(String labelText) {
        this.labelText = labelText == null ? null : labelText.trim();
    }

    public Short getType() {
        return type;
    }

    public void setType(Short type) {
        this.type = type;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json == null ? null : json.trim();
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Short getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Short deleteFlag) {
        this.deleteFlag = deleteFlag;
    }
}