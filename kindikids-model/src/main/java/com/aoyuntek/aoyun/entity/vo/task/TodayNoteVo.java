package com.aoyuntek.aoyun.entity.vo.task;

import java.util.List;

import com.aoyuntek.aoyun.entity.vo.ProgramMedicationInfoVo;
import com.aoyuntek.framework.model.BaseModel;

public class TodayNoteVo extends BaseModel{

    private static final long serialVersionUID = 1L;

    private List<String> notes;
    
    private List<ProgramMedicationInfoVo> medicationInfos;

    public List<String> getNotes() {
        return notes;
    }

    public void setNotes(List<String> notes) {
        this.notes = notes;
    }

    public List<ProgramMedicationInfoVo> getMedicationInfos() {
        return medicationInfos;
    }

    public void setMedicationInfos(List<ProgramMedicationInfoVo> medicationInfos) {
        this.medicationInfos = medicationInfos;
    }

}
