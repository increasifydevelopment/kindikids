package com.aoyuntek.aoyun.entity.vo.waiting;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.aoyuntek.aoyun.entity.po.CentersInfo;
import com.aoyuntek.aoyun.entity.vo.ChildInfoVo;
import com.theone.string.util.StringUtil;

public class WaitingListVo {
	private String id;
	private String childName;
	private Integer[] liveAge;
	private Date birthday;
	private String requestDay;
	private Date applicationDate;
	private Date applicationDatePosition;
	private Date requestedStartDate;
	private Integer queue;
	private Short archivedFrom;
	private String centreStr;
	private String personColor;
	private String familyId;
	private ChildInfoVo childVo;
	private Short listType;
	private boolean monday;
	private boolean tuesday;
	private boolean wednesday;
	private boolean thursday;
	private boolean friday;
	private List<CentersInfo> centers;
	private boolean enrolledFlag;

	public boolean isEnrolledFlag() {
		return enrolledFlag;
	}

	public void setEnrolledFlag(boolean enrolledFlag) {
		this.enrolledFlag = enrolledFlag;
	}

	public List<CentersInfo> getCenters() {
		return centers;
	}

	public void setCenters(List<CentersInfo> centers) {
		sort(centers);
		this.centers = centers;
	}

	public boolean isMonday() {
		return monday;
	}

	public void setMonday(boolean monday) {
		this.monday = monday;
	}

	public boolean isTuesday() {
		return tuesday;
	}

	public void setTuesday(boolean tuesday) {
		this.tuesday = tuesday;
	}

	public boolean isWednesday() {
		return wednesday;
	}

	public void setWednesday(boolean wednesday) {
		this.wednesday = wednesday;
	}

	public boolean isThursday() {
		return thursday;
	}

	public void setThursday(boolean thursday) {
		this.thursday = thursday;
	}

	public boolean isFriday() {
		return friday;
	}

	public void setFriday(boolean friday) {
		this.friday = friday;
	}

	public Short getListType() {
		return listType;
	}

	public void setListType(Short listType) {
		this.listType = listType;
	}

	public ChildInfoVo getChildVo() {
		return childVo;
	}

	public void setChildVo(ChildInfoVo childVo) {
		this.childVo = childVo;
	}

	public String getFamilyId() {
		return familyId;
	}

	public void setFamilyId(String familyId) {
		this.familyId = familyId;
	}

	public String getPersonColor() {
		return personColor;
	}

	public void setPersonColor(String personColor) {
		this.personColor = personColor;
	}

	public String getCentreStr() {
		return centreStr;
	}

	public void setCentreStr(String centreStr) {
		this.centreStr = centreStr;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getChildName() {
		return childName;
	}

	public void setChildName(String childName) {
		this.childName = childName;
	}

	public Integer[] getLiveAge() {
		return liveAge;
	}

	public void setLiveAge(Integer[] liveAge) {
		this.liveAge = liveAge;
	}

	public String getRequestDay() {
		return requestDay;
	}

	public void setRequestDay(String requestDay) {
		this.requestDay = StringUtil.isEmpty(requestDay) ? requestDay : requestDay.substring(0, requestDay.length() - 1);
	}

	public Date getApplicationDate() {
		return applicationDate;
	}

	public void setApplicationDate(Date applicationDate) {
		this.applicationDate = applicationDate;
	}

	public Date getApplicationDatePosition() {
		return applicationDatePosition;
	}

	public void setApplicationDatePosition(Date applicationDatePosition) {
		this.applicationDatePosition = applicationDatePosition;
	}

	public Date getRequestedStartDate() {
		return requestedStartDate;
	}

	public void setRequestedStartDate(Date requestedStartDate) {
		this.requestedStartDate = requestedStartDate;
	}

	public Integer getQueue() {
		return queue;
	}

	public void setQueue(Integer queue) {
		this.queue = queue;
	}

	public Short getArchivedFrom() {
		return archivedFrom;
	}

	public void setArchivedFrom(Short archivedFrom) {
		this.archivedFrom = archivedFrom;
	}

	public void sort(List<CentersInfo> centers) {
		Collections.sort(centers, new Comparator<CentersInfo>() {
			@Override
			public int compare(CentersInfo o1, CentersInfo o2) {
				if (o1.getCreateTime().before(o2.getCreateTime())) {
					return -1;
				} else {
					return 1;
				}

			}

		});
	}
}
