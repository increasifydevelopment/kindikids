package com.aoyuntek.aoyun.entity.po.profile;

import com.aoyuntek.framework.model.BaseModel;

public class ChildFormRelationInfo extends BaseModel{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String userId;

    private String valueId;

    private String templateId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getValueId() {
        return valueId;
    }

    public void setValueId(String valueId) {
        this.valueId = valueId == null ? null : valueId.trim();
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId == null ? null : templateId.trim();
    }
}