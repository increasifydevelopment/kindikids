package com.aoyuntek.aoyun.entity.vo.roster;

import com.aoyuntek.aoyun.entity.po.roster.RosterShiftTaskInfo;

/**
 * 
 * @author dlli5 at 2016年10月14日上午8:51:04
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public class RosterShiftTaskVo extends RosterShiftTaskInfo {

    private static final long serialVersionUID = 1L;

}
