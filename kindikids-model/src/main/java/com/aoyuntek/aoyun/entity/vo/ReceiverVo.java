package com.aoyuntek.aoyun.entity.vo;

import com.aoyuntek.framework.model.BaseModel;

public class ReceiverVo extends BaseModel {

    /**
     * 序列化
     */
    private static final long serialVersionUID = 1L;
    /**
     * 账号id
     */
    private String id;
    /**
     * 人名
     */
    private String name;
    /**
     * 是否属于分组
     */
    private short groupFlag;
    /**
     * 园区
     */
    private String centerId;
    /**
     * 角色
     */
    private Short roleValue;
    /**
     * 是否已读，用于标识收件人是否已经读取Msg
     */
    private short readFlag;

    /**
     * 分组名称
     */
    private String groupName;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public ReceiverVo() {
        super();
    }

    public ReceiverVo(String id, String name, String groupName) {
        super();
        this.id = id;
        this.name = name;
        this.groupName = groupName;
    }

    public ReceiverVo(String id, String name) {
        super();
        this.id = id;
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        ReceiverVo r = (ReceiverVo) obj;
        return id.equals(r.getId());
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    public short getReadFlag() {
        return readFlag;
    }

    public void setReadFlag(short readFlag) {
        this.readFlag = readFlag;
    }

    public String getCenterId() {
        return centerId;
    }

    public void setCenterId(String centerId) {
        this.centerId = centerId;
    }

    public Short getRoleValue() {
        return roleValue;
    }

    public void setRoleValue(Short roleValue) {
        this.roleValue = roleValue;
    }

    public short getGroupFlag() {
        return groupFlag;
    }

    public void setGroupFlag(short groupFlag) {
        this.groupFlag = groupFlag;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
