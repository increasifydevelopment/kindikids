package com.aoyuntek.aoyun.entity.vo;

import java.io.Serializable;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.aoyuntek.framework.deserializer.S3JsonSerializer;

/**
 * 
 * @author rick
 *
 */
public class StaffListVo implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	private String id;

	/**
	 * 
	 */
	private String firstName;

	/**
	 * 
	 */
	private String middleName;

	/**
	 * 
	 */
	private String lastName;

	/**
	 * 头像
	 */
	private String avatar;

	/**
	 * 组名
	 */
	private String roleName;
	
	/**
	 * 全部角色 逗号分隔
	 */
	private String roleNames;

	/**
	 * 标签
	 */
	private String roleTag;
	
	/**
	 * personColor
	 */
	private String personColor;
	
	

	public String getPersonColor() {
        return personColor;
    }

    public void setPersonColor(String personColor) {
        this.personColor = personColor;
    }

    public String getRoleNames() {
		return roleNames;
	}

	public void setRoleNames(String roleNames) {
		this.roleNames = roleNames;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@JsonSerialize(using = S3JsonSerializer.class)
	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleTag() {
		return roleTag;
	}

	public void setRoleTag(String roleTag) {
		this.roleTag = roleTag;
	}

	@Override
	public String toString() {
		return "StaffListVo [id=" + id + ", firstName=" + firstName + ", middleName=" + middleName + ", lastName=" + lastName + ", avatar=" + avatar
				+ ", roleName=" + roleName + ", roleTag=" + roleTag + "]";
	}

}
