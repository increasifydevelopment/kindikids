package com.aoyuntek.aoyun.entity.po;

import java.util.Date;

import com.aoyuntek.framework.model.BaseModel;

public class GivingNoticeInfo extends BaseModel {
	private static final long serialVersionUID = 1L;

	private boolean isPlan = false;

	public boolean isPlan() {
		return isPlan;
	}

	public void setPlan(boolean isPlan) {
		this.isPlan = isPlan;
	}

	private String centerId;

	public String getCenterId() {
		return centerId;
	}

	public void setCenterId(String centerId) {
		this.centerId = centerId;
	}

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_giving_notice.id
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	private String id;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_giving_notice.child_id
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	private String childId;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_giving_notice.child_name
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	private String childName;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_giving_notice.parent_name
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	private String parentName;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_giving_notice.reson
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	private String reson;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_giving_notice.lodged_date
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	private Date lodgedDate;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_giving_notice.last_date
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	private Date lastDate;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_giving_notice.attending_last_weeks
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	private Short attendingLastWeeks;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_giving_notice.covering
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	private Short covering;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_giving_notice.checked_parent_signature
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	private Boolean checkedParentSignature;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_giving_notice.account_name
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	private String accountName;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_giving_notice.bsb
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	private String bsb;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_giving_notice.account_number
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	private String accountNumber;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_giving_notice.Immediately_01
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	private Boolean immediately01;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_giving_notice.Immediately_02
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	private Boolean immediately02;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_giving_notice.Immediately_03
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	private Boolean immediately03;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_giving_notice.Immediately_04
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	private Boolean immediately04;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_giving_notice.Immediately_05
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	private Boolean immediately05;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_giving_notice.Immediately_06
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	private Boolean immediately06;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_giving_notice.Immediately_07
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	private Boolean immediately07;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_giving_notice.Immediately_08
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	private Date immediately08;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_giving_notice.Immediately_09
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	private String immediately09;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_giving_notice.period_01
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	private Boolean period01;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_giving_notice.period_02
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	private Boolean period02;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_giving_notice.period_03
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	private Boolean period03;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_giving_notice.period_04
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	private String period04;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_giving_notice.period_05
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	private Boolean period05;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_giving_notice.period_06
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	private Boolean period06;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_giving_notice.approval_account_id
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	private String approvalAccountId;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_giving_notice.state
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	private Short state;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_giving_notice.create_time
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	private Date createTime;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_giving_notice.create_account_id
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	private String createAccountId;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_giving_notice.update_time
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	private Date updateTime;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_giving_notice.update_account_id
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	private String updateAccountId;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_giving_notice.delete_flag
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	private Short deleteFlag;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_giving_notice.monday
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	private Boolean monday;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_giving_notice.tuesday
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	private Boolean tuesday;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_giving_notice.wednesday
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	private Boolean wednesday;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_giving_notice.thursday
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	private Boolean thursday;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_giving_notice.friday
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	private Boolean friday;

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_giving_notice.id
	 * 
	 * @return the value of tbl_giving_notice.id
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public String getId() {
		return id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_giving_notice.id
	 * 
	 * @param id
	 *            the value for tbl_giving_notice.id
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public void setId(String id) {
		this.id = id == null ? null : id.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_giving_notice.child_id
	 * 
	 * @return the value of tbl_giving_notice.child_id
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public String getChildId() {
		return childId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_giving_notice.child_id
	 * 
	 * @param childId
	 *            the value for tbl_giving_notice.child_id
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public void setChildId(String childId) {
		this.childId = childId == null ? null : childId.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_giving_notice.child_name
	 * 
	 * @return the value of tbl_giving_notice.child_name
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public String getChildName() {
		return childName;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_giving_notice.child_name
	 * 
	 * @param childName
	 *            the value for tbl_giving_notice.child_name
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public void setChildName(String childName) {
		this.childName = childName == null ? null : childName.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_giving_notice.parent_name
	 * 
	 * @return the value of tbl_giving_notice.parent_name
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public String getParentName() {
		return parentName;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_giving_notice.parent_name
	 * 
	 * @param parentName
	 *            the value for tbl_giving_notice.parent_name
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public void setParentName(String parentName) {
		this.parentName = parentName == null ? null : parentName.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_giving_notice.reson
	 * 
	 * @return the value of tbl_giving_notice.reson
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public String getReson() {
		return reson;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_giving_notice.reson
	 * 
	 * @param reson
	 *            the value for tbl_giving_notice.reson
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public void setReson(String reson) {
		this.reson = reson == null ? null : reson.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_giving_notice.lodged_date
	 * 
	 * @return the value of tbl_giving_notice.lodged_date
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public Date getLodgedDate() {
		return lodgedDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_giving_notice.lodged_date
	 * 
	 * @param lodgedDate
	 *            the value for tbl_giving_notice.lodged_date
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public void setLodgedDate(Date lodgedDate) {
		this.lodgedDate = lodgedDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_giving_notice.last_date
	 * 
	 * @return the value of tbl_giving_notice.last_date
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public Date getLastDate() {
		return lastDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_giving_notice.last_date
	 * 
	 * @param lastDate
	 *            the value for tbl_giving_notice.last_date
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public void setLastDate(Date lastDate) {
		this.lastDate = lastDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_giving_notice.attending_last_weeks
	 * 
	 * @return the value of tbl_giving_notice.attending_last_weeks
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public Short getAttendingLastWeeks() {
		return attendingLastWeeks;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_giving_notice.attending_last_weeks
	 * 
	 * @param attendingLastWeeks
	 *            the value for tbl_giving_notice.attending_last_weeks
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public void setAttendingLastWeeks(Short attendingLastWeeks) {
		this.attendingLastWeeks = attendingLastWeeks;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_giving_notice.covering
	 * 
	 * @return the value of tbl_giving_notice.covering
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public Short getCovering() {
		return covering;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_giving_notice.covering
	 * 
	 * @param covering
	 *            the value for tbl_giving_notice.covering
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public void setCovering(Short covering) {
		this.covering = covering;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_giving_notice.checked_parent_signature
	 * 
	 * @return the value of tbl_giving_notice.checked_parent_signature
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public Boolean getCheckedParentSignature() {
		return checkedParentSignature;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_giving_notice.checked_parent_signature
	 * 
	 * @param checkedParentSignature
	 *            the value for tbl_giving_notice.checked_parent_signature
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public void setCheckedParentSignature(Boolean checkedParentSignature) {
		this.checkedParentSignature = checkedParentSignature;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_giving_notice.account_name
	 * 
	 * @return the value of tbl_giving_notice.account_name
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public String getAccountName() {
		return accountName;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_giving_notice.account_name
	 * 
	 * @param accountName
	 *            the value for tbl_giving_notice.account_name
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public void setAccountName(String accountName) {
		this.accountName = accountName == null ? null : accountName.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_giving_notice.bsb
	 * 
	 * @return the value of tbl_giving_notice.bsb
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public String getBsb() {
		return bsb;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_giving_notice.bsb
	 * 
	 * @param bsb
	 *            the value for tbl_giving_notice.bsb
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public void setBsb(String bsb) {
		this.bsb = bsb == null ? null : bsb.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_giving_notice.account_number
	 * 
	 * @return the value of tbl_giving_notice.account_number
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public String getAccountNumber() {
		return accountNumber;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_giving_notice.account_number
	 * 
	 * @param accountNumber
	 *            the value for tbl_giving_notice.account_number
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber == null ? null : accountNumber.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_giving_notice.Immediately_01
	 * 
	 * @return the value of tbl_giving_notice.Immediately_01
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public Boolean getImmediately01() {
		return immediately01;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_giving_notice.Immediately_01
	 * 
	 * @param immediately01
	 *            the value for tbl_giving_notice.Immediately_01
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public void setImmediately01(Boolean immediately01) {
		this.immediately01 = immediately01;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_giving_notice.Immediately_02
	 * 
	 * @return the value of tbl_giving_notice.Immediately_02
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public Boolean getImmediately02() {
		return immediately02;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_giving_notice.Immediately_02
	 * 
	 * @param immediately02
	 *            the value for tbl_giving_notice.Immediately_02
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public void setImmediately02(Boolean immediately02) {
		this.immediately02 = immediately02;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_giving_notice.Immediately_03
	 * 
	 * @return the value of tbl_giving_notice.Immediately_03
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public Boolean getImmediately03() {
		return immediately03;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_giving_notice.Immediately_03
	 * 
	 * @param immediately03
	 *            the value for tbl_giving_notice.Immediately_03
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public void setImmediately03(Boolean immediately03) {
		this.immediately03 = immediately03;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_giving_notice.Immediately_04
	 * 
	 * @return the value of tbl_giving_notice.Immediately_04
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public Boolean getImmediately04() {
		return immediately04;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_giving_notice.Immediately_04
	 * 
	 * @param immediately04
	 *            the value for tbl_giving_notice.Immediately_04
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public void setImmediately04(Boolean immediately04) {
		this.immediately04 = immediately04;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_giving_notice.Immediately_05
	 * 
	 * @return the value of tbl_giving_notice.Immediately_05
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public Boolean getImmediately05() {
		return immediately05;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_giving_notice.Immediately_05
	 * 
	 * @param immediately05
	 *            the value for tbl_giving_notice.Immediately_05
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public void setImmediately05(Boolean immediately05) {
		this.immediately05 = immediately05;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_giving_notice.Immediately_06
	 * 
	 * @return the value of tbl_giving_notice.Immediately_06
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public Boolean getImmediately06() {
		return immediately06;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_giving_notice.Immediately_06
	 * 
	 * @param immediately06
	 *            the value for tbl_giving_notice.Immediately_06
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public void setImmediately06(Boolean immediately06) {
		this.immediately06 = immediately06;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_giving_notice.Immediately_07
	 * 
	 * @return the value of tbl_giving_notice.Immediately_07
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public Boolean getImmediately07() {
		return immediately07;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_giving_notice.Immediately_07
	 * 
	 * @param immediately07
	 *            the value for tbl_giving_notice.Immediately_07
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public void setImmediately07(Boolean immediately07) {
		this.immediately07 = immediately07;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_giving_notice.Immediately_08
	 * 
	 * @return the value of tbl_giving_notice.Immediately_08
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public Date getImmediately08() {
		return immediately08;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_giving_notice.Immediately_08
	 * 
	 * @param immediately08
	 *            the value for tbl_giving_notice.Immediately_08
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public void setImmediately08(Date immediately08) {
		this.immediately08 = immediately08;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_giving_notice.Immediately_09
	 * 
	 * @return the value of tbl_giving_notice.Immediately_09
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public String getImmediately09() {
		return immediately09;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_giving_notice.Immediately_09
	 * 
	 * @param immediately09
	 *            the value for tbl_giving_notice.Immediately_09
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public void setImmediately09(String immediately09) {
		this.immediately09 = immediately09 == null ? null : immediately09.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_giving_notice.period_01
	 * 
	 * @return the value of tbl_giving_notice.period_01
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public Boolean getPeriod01() {
		return period01;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_giving_notice.period_01
	 * 
	 * @param period01
	 *            the value for tbl_giving_notice.period_01
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public void setPeriod01(Boolean period01) {
		this.period01 = period01;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_giving_notice.period_02
	 * 
	 * @return the value of tbl_giving_notice.period_02
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public Boolean getPeriod02() {
		return period02;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_giving_notice.period_02
	 * 
	 * @param period02
	 *            the value for tbl_giving_notice.period_02
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public void setPeriod02(Boolean period02) {
		this.period02 = period02;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_giving_notice.period_03
	 * 
	 * @return the value of tbl_giving_notice.period_03
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public Boolean getPeriod03() {
		return period03;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_giving_notice.period_03
	 * 
	 * @param period03
	 *            the value for tbl_giving_notice.period_03
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public void setPeriod03(Boolean period03) {
		this.period03 = period03;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_giving_notice.period_04
	 * 
	 * @return the value of tbl_giving_notice.period_04
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public String getPeriod04() {
		return period04;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_giving_notice.period_04
	 * 
	 * @param period04
	 *            the value for tbl_giving_notice.period_04
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public void setPeriod04(String period04) {
		this.period04 = period04 == null ? null : period04.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_giving_notice.period_05
	 * 
	 * @return the value of tbl_giving_notice.period_05
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public Boolean getPeriod05() {
		return period05;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_giving_notice.period_05
	 * 
	 * @param period05
	 *            the value for tbl_giving_notice.period_05
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public void setPeriod05(Boolean period05) {
		this.period05 = period05;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_giving_notice.period_06
	 * 
	 * @return the value of tbl_giving_notice.period_06
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public Boolean getPeriod06() {
		return period06;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_giving_notice.period_06
	 * 
	 * @param period06
	 *            the value for tbl_giving_notice.period_06
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public void setPeriod06(Boolean period06) {
		this.period06 = period06;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_giving_notice.approval_account_id
	 * 
	 * @return the value of tbl_giving_notice.approval_account_id
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public String getApprovalAccountId() {
		return approvalAccountId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_giving_notice.approval_account_id
	 * 
	 * @param approvalAccountId
	 *            the value for tbl_giving_notice.approval_account_id
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public void setApprovalAccountId(String approvalAccountId) {
		this.approvalAccountId = approvalAccountId == null ? null : approvalAccountId.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_giving_notice.state
	 * 
	 * @return the value of tbl_giving_notice.state
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public Short getState() {
		return state;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_giving_notice.state
	 * 
	 * @param state
	 *            the value for tbl_giving_notice.state
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public void setState(Short state) {
		this.state = state;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_giving_notice.create_time
	 * 
	 * @return the value of tbl_giving_notice.create_time
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_giving_notice.create_time
	 * 
	 * @param createTime
	 *            the value for tbl_giving_notice.create_time
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_giving_notice.create_account_id
	 * 
	 * @return the value of tbl_giving_notice.create_account_id
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public String getCreateAccountId() {
		return createAccountId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_giving_notice.create_account_id
	 * 
	 * @param createAccountId
	 *            the value for tbl_giving_notice.create_account_id
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public void setCreateAccountId(String createAccountId) {
		this.createAccountId = createAccountId == null ? null : createAccountId.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_giving_notice.update_time
	 * 
	 * @return the value of tbl_giving_notice.update_time
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public Date getUpdateTime() {
		return updateTime;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_giving_notice.update_time
	 * 
	 * @param updateTime
	 *            the value for tbl_giving_notice.update_time
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_giving_notice.update_account_id
	 * 
	 * @return the value of tbl_giving_notice.update_account_id
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public String getUpdateAccountId() {
		return updateAccountId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_giving_notice.update_account_id
	 * 
	 * @param updateAccountId
	 *            the value for tbl_giving_notice.update_account_id
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public void setUpdateAccountId(String updateAccountId) {
		this.updateAccountId = updateAccountId == null ? null : updateAccountId.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_giving_notice.delete_flag
	 * 
	 * @return the value of tbl_giving_notice.delete_flag
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public Short getDeleteFlag() {
		return deleteFlag;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_giving_notice.delete_flag
	 * 
	 * @param deleteFlag
	 *            the value for tbl_giving_notice.delete_flag
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public void setDeleteFlag(Short deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_giving_notice.monday
	 * 
	 * @return the value of tbl_giving_notice.monday
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public Boolean getMonday() {
		return monday;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_giving_notice.monday
	 * 
	 * @param monday
	 *            the value for tbl_giving_notice.monday
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public void setMonday(Boolean monday) {
		this.monday = monday;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_giving_notice.tuesday
	 * 
	 * @return the value of tbl_giving_notice.tuesday
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public Boolean getTuesday() {
		return tuesday;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_giving_notice.tuesday
	 * 
	 * @param tuesday
	 *            the value for tbl_giving_notice.tuesday
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public void setTuesday(Boolean tuesday) {
		this.tuesday = tuesday;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_giving_notice.wednesday
	 * 
	 * @return the value of tbl_giving_notice.wednesday
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public Boolean getWednesday() {
		return wednesday;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_giving_notice.wednesday
	 * 
	 * @param wednesday
	 *            the value for tbl_giving_notice.wednesday
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public void setWednesday(Boolean wednesday) {
		this.wednesday = wednesday;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_giving_notice.thursday
	 * 
	 * @return the value of tbl_giving_notice.thursday
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public Boolean getThursday() {
		return thursday;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_giving_notice.thursday
	 * 
	 * @param thursday
	 *            the value for tbl_giving_notice.thursday
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public void setThursday(Boolean thursday) {
		this.thursday = thursday;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_giving_notice.friday
	 * 
	 * @return the value of tbl_giving_notice.friday
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public Boolean getFriday() {
		return friday;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_giving_notice.friday
	 * 
	 * @param friday
	 *            the value for tbl_giving_notice.friday
	 * 
	 * @mbggenerated Thu Sep 01 14:44:23 CST 2016
	 */
	public void setFriday(Boolean friday) {
		this.friday = friday;
	}
}