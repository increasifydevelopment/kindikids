package com.aoyuntek.aoyun.entity.vo;

import com.aoyuntek.framework.model.BaseModel;

public class ProgramRegisterItemUser  extends BaseModel{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    /**
     * 小孩头像
     */
    private String avatar;

    /**
     * 小孩颜色
     */
    private String personColor;

    /**
     * 小孩Id
     */
    private String id;
    /**
     * 
     */
    private boolean selected = true;
    
    /**
     * 小孩全名
     */
    private String text;
    
    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getPersonColor() {
        return personColor;
    }

    public void setPersonColor(String personColor) {
        this.personColor = personColor;
    }

    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }



}
