package com.aoyuntek.aoyun.entity.po.meeting;

import java.util.Date;

import com.aoyuntek.framework.model.BaseModel;

public class MeetingAgendaInfo extends BaseModel {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String id;

    private String agendaName;

    private String agendaTypeId;

    private String paragraph;

    private Short state;

    private Date createTime;

    private String createAccountId;

    private Date updateTime;

    private String updateAccountId;

    private Short deleteFlag;

    private Short currtenFlag;

    private String agendaCode;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getAgendaName() {
        return agendaName;
    }

    public void setAgendaName(String agendaName) {
        this.agendaName = agendaName == null ? null : agendaName.trim();
    }

    public String getAgendaTypeId() {
        return agendaTypeId;
    }

    public void setAgendaTypeId(String agendaTypeId) {
        this.agendaTypeId = agendaTypeId == null ? null : agendaTypeId.trim();
    }

    public String getParagraph() {
        return paragraph;
    }

    public void setParagraph(String paragraph) {
        this.paragraph = paragraph == null ? null : paragraph.trim();
    }

    public Short getState() {
        return state;
    }

    public void setState(Short state) {
        this.state = state;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateAccountId() {
        return createAccountId;
    }

    public void setCreateAccountId(String createAccountId) {
        this.createAccountId = createAccountId == null ? null : createAccountId.trim();
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateAccountId() {
        return updateAccountId;
    }

    public void setUpdateAccountId(String updateAccountId) {
        this.updateAccountId = updateAccountId == null ? null : updateAccountId.trim();
    }

    public Short getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Short deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public Short getCurrtenFlag() {
        return currtenFlag;
    }

    public void setCurrtenFlag(Short currtenFlag) {
        this.currtenFlag = currtenFlag;
    }

    public String getAgendaCode() {
        return agendaCode;
    }

    public void setAgendaCode(String agendaCode) {
        this.agendaCode = agendaCode == null ? null : agendaCode.trim();
    }
}