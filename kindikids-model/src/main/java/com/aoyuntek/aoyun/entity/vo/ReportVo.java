package com.aoyuntek.aoyun.entity.vo;

import java.util.List;

public class ReportVo {
    private List<EylfInfoVo> list;

    private String title;

    private String userId;

    private String name;

    private String avatar;

    public ReportVo() {

    }

    public ReportVo(List<EylfInfoVo> list) {
        this.list = list;
    }

    public List<EylfInfoVo> getList() {
        return list;
    }

    public void setList(List<EylfInfoVo> list) {
        this.list = list;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

}
