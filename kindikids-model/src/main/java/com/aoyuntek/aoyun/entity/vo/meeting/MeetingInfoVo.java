package com.aoyuntek.aoyun.entity.vo.meeting;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.meeting.MeetingColumnsInfo;
import com.aoyuntek.aoyun.entity.po.meeting.MeetingInfo;
import com.aoyuntek.aoyun.entity.vo.NqsVo;
import com.aoyuntek.framework.model.BaseModel;

public class MeetingInfoVo extends BaseModel {

    /**
     * 序列化
     */
    private static final long serialVersionUID = 1L;

    private String tempId;

    private String meetingId;

    private MeetingInfo meetingInfo;

    private List<NqsVo> nqsList;

    private List<MeetingColumnsInfo> colList;

    private List<TableVo> table;
    
    private boolean whenRequireFlag;
    

    public boolean isWhenRequireFlag() {
        return whenRequireFlag;
    }

    public void setWhenRequireFlag(boolean whenRequireFlag) {
        this.whenRequireFlag = whenRequireFlag;
    }

    public String getMeetingId() {
        return meetingId;
    }

    public void setMeetingId(String meetingId) {
        this.meetingId = meetingId;
    }

    public List<TableVo> getTable() {
        return table;
    }

    public MeetingInfo getMeetingInfo() {
        return meetingInfo;
    }

    public void setMeetingInfo(MeetingInfo meetingInfo) {
        this.meetingInfo = meetingInfo;
    }

    public void setTable(List<TableVo> table) {
        this.table = table;
    }

    public List<NqsVo> getNqsList() {
        return nqsList;
    }

    public void setNqsList(List<NqsVo> nqsList) {
        this.nqsList = nqsList;
    }

    public List<MeetingColumnsInfo> getColList() {
        return colList;
    }

    public void setColList(List<MeetingColumnsInfo> colList) {
        this.colList = colList;
    }

    public String getTempId() {
        return tempId;
    }

    public void setTempId(String tempId) {
        this.tempId = tempId;
    }

}
