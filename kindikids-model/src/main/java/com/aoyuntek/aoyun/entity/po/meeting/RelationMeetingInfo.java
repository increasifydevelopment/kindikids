package com.aoyuntek.aoyun.entity.po.meeting;

import com.aoyuntek.framework.model.BaseModel;

public class RelationMeetingInfo extends BaseModel {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String templateId;

    private String meetingId;

    private String valueId;

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId == null ? null : templateId.trim();
    }

    public String getMeetingId() {
        return meetingId;
    }

    public void setMeetingId(String meetingId) {
        this.meetingId = meetingId == null ? null : meetingId.trim();
    }

    public String getValueId() {
        return valueId;
    }

    public void setValueId(String valueId) {
        this.valueId = valueId == null ? null : valueId.trim();
    }
}