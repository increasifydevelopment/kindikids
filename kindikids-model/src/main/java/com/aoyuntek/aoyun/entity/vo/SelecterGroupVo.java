package com.aoyuntek.aoyun.entity.vo;

import java.util.List;

import com.aoyuntek.framework.model.BaseModel;

public class SelecterGroupVo extends BaseModel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String text;

    private List children;

    public SelecterGroupVo() {

    }

    public SelecterGroupVo(String text, List<SelecterPo> children) {
        super();
        this.text = text;
        this.children = children;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List getChildren() {
        return children;
    }

    public void setChildren(List children) {
        this.children = children;
    }

}
