package com.aoyuntek.aoyun.entity.vo.meeting;

import java.util.List;

import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.framework.model.BaseModel;

public class TableRowVo extends BaseModel {
    /**
     * 序列化
     */
    private static final long serialVersionUID = 1L;

    private String columnId;

    private Short columnType;

    private String columnName;

    private String value;

    private List<SelecterPo> responsibility;

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public List<SelecterPo> getResponsibility() {
        return responsibility;
    }

    public void setResponsibility(List<SelecterPo> responsibility) {
        this.responsibility = responsibility;
    }

    public String getColumnId() {
        return columnId;
    }

    public void setColumnId(String columnId) {
        this.columnId = columnId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Short getColumnType() {
        return columnType;
    }

    public void setColumnType(Short columnType) {
        this.columnType = columnType;
    }

}
