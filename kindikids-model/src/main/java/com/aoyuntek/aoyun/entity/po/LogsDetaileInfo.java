package com.aoyuntek.aoyun.entity.po;

import com.aoyuntek.framework.model.BaseModel;

public class LogsDetaileInfo extends BaseModel {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String id;

    private String changeFieldName;

    private String oldValue;

    private String newValue;

    private String tags;

    private String sourceLogsId;
    
    public LogsDetaileInfo() {
    }

    public LogsDetaileInfo(String id, String changeFieldName, String oldValue, String newValue, String tags,String sourceLogsId) {
        super();
        this.id = id;
        this.changeFieldName = changeFieldName;
        this.oldValue = oldValue;
        this.newValue = newValue;
        this.tags = tags;
        this.sourceLogsId=sourceLogsId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getChangeFieldName() {
        return changeFieldName;
    }

    public void setChangeFieldName(String changeFieldName) {
        this.changeFieldName = changeFieldName == null ? null : changeFieldName.trim();
    }

    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue == null ? null : oldValue.trim();
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue == null ? null : newValue.trim();
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags == null ? null : tags.trim();
    }

    public String getSourceLogsId() {
        return sourceLogsId;
    }

    public void setSourceLogsId(String sourceLogsId) {
        this.sourceLogsId = sourceLogsId == null ? null : sourceLogsId.trim();
    }
}