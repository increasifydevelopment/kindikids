package com.aoyuntek.aoyun.entity.vo.meeting;

import java.util.List;

import com.aoyuntek.framework.model.BaseModel;

public class TableVo extends BaseModel {

    /**
     * 序列化
     */
    private static final long serialVersionUID = 1L;

    private String tempId;

    private String rowId;

    private List<TableRowVo> rowList;

    public List<TableRowVo> getRowList() {
        return rowList;
    }

    public void setRowList(List<TableRowVo> rowList) {
        this.rowList = rowList;
    }

    public String getRowId() {
        return rowId;
    }

    public void setRowId(String rowId) {
        this.rowId = rowId;
    }

    public String getTempId() {
        return tempId;
    }

    public void setTempId(String tempId) {
        this.tempId = tempId;
    }

}
