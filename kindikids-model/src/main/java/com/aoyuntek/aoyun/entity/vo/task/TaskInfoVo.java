package com.aoyuntek.aoyun.entity.vo.task;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.task.TaskFollowUpInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskFrequencyInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskVisibleInfo;
import com.aoyuntek.aoyun.entity.vo.FormInfoVO;
import com.aoyuntek.aoyun.entity.vo.NqsVo;

/**
 * 
 * @author dlli5 at 2016年9月20日下午1:43:19
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public class TaskInfoVo extends TaskInfo {

    private static final long serialVersionUID = 1L;

    private boolean overWrite = false;

    private String formId;

    private FormInfoVO taskFormObj;

    private List<TaskFollowUpInfo> followUpInfos;

    // 下面三个分别对应了三个选项的人 枚举参照
    private List<TaskVisibleInfo> selectTaskVisibles;
    private List<TaskVisibleInfo> responsibleTaskVisibles;
    private List<TaskVisibleInfo> taskImplementersVisibles;

    // 在获取的时候 给的下拉列表
    private Object selectTaskVisiblesSelect;
    private Object responsibleTaskVisiblesSelect;
    private Object taskImplementersVisiblesSelect;

    private TaskFrequencyInfo taskFrequencyInfo;

    /**
     * 需求变更，新增NQS
     */
    private List<NqsVo> nqsList;

    public List<NqsVo> getNqsList() {
        return nqsList;
    }

    public void setNqsList(List<NqsVo> nqsList) {
        this.nqsList = nqsList;
    }

    public String getFormId() {
        return formId;
    }

    public void setFormId(String formId) {
        this.formId = formId;
    }

    public boolean getOverWrite() {
        return overWrite;
    }

    public void setOverWrite(boolean overWrite) {
        this.overWrite = overWrite;
    }

    public Object getSelectTaskVisiblesSelect() {
        return selectTaskVisiblesSelect;
    }

    public void setSelectTaskVisiblesSelect(Object selectTaskVisiblesSelect) {
        this.selectTaskVisiblesSelect = selectTaskVisiblesSelect;
    }

    public Object getResponsibleTaskVisiblesSelect() {
        return responsibleTaskVisiblesSelect;
    }

    public void setResponsibleTaskVisiblesSelect(Object responsibleTaskVisiblesSelect) {
        this.responsibleTaskVisiblesSelect = responsibleTaskVisiblesSelect;
    }

    public Object getTaskImplementersVisiblesSelect() {
        return taskImplementersVisiblesSelect;
    }

    public void setTaskImplementersVisiblesSelect(Object taskImplementersVisiblesSelect) {
        this.taskImplementersVisiblesSelect = taskImplementersVisiblesSelect;
    }

    public List<TaskVisibleInfo> getSelectTaskVisibles() {
        return selectTaskVisibles;
    }

    public void setSelectTaskVisibles(List<TaskVisibleInfo> selectTaskVisibles) {
        this.selectTaskVisibles = selectTaskVisibles;
    }

    public List<TaskVisibleInfo> getResponsibleTaskVisibles() {
        return responsibleTaskVisibles;
    }

    public void setResponsibleTaskVisibles(List<TaskVisibleInfo> responsibleTaskVisibles) {
        this.responsibleTaskVisibles = responsibleTaskVisibles;
    }

    public List<TaskVisibleInfo> getTaskImplementersVisibles() {
        return taskImplementersVisibles;
    }

    public void setTaskImplementersVisibles(List<TaskVisibleInfo> taskImplementersVisibles) {
        this.taskImplementersVisibles = taskImplementersVisibles;
    }

    public TaskFrequencyInfo getTaskFrequencyInfo() {
        return taskFrequencyInfo;
    }

    public void setTaskFrequencyInfo(TaskFrequencyInfo taskFrequencyInfo) {
        this.taskFrequencyInfo = taskFrequencyInfo;
    }

    public List<TaskFollowUpInfo> getFollowUpInfos() {
        return followUpInfos;
    }

    public void setFollowUpInfos(List<TaskFollowUpInfo> followUpInfos) {
        this.followUpInfos = followUpInfos;
    }

    public FormInfoVO getTaskFormObj() {
        return taskFormObj;
    }

    public void setTaskFormObj(FormInfoVO taskFormObj) {
        this.taskFormObj = taskFormObj;
    }
}
