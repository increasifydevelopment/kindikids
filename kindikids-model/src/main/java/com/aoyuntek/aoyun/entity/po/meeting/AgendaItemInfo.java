package com.aoyuntek.aoyun.entity.po.meeting;

import java.util.Date;

import com.aoyuntek.framework.model.BaseModel;

public class AgendaItemInfo extends BaseModel {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String id;

    private String agendaId;

    private String itemName;

    private String presentedAccountId;

    private Integer itemTime;

    private String itemMinutes;

    private Date createTime;

    private String createAccountId;

    private Date updateTime;

    private String updateAccountId;

    private Short deleteFlag;

    public Integer getItemTime() {
        return itemTime;
    }

    public void setItemTime(Integer itemTime) {
        this.itemTime = itemTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getAgendaId() {
        return agendaId;
    }

    public void setAgendaId(String agendaId) {
        this.agendaId = agendaId == null ? null : agendaId.trim();
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName == null ? null : itemName.trim();
    }

    public String getPresentedAccountId() {
        return presentedAccountId;
    }

    public void setPresentedAccountId(String presentedAccountId) {
        this.presentedAccountId = presentedAccountId == "" ? null : presentedAccountId.trim();
    }

    public String getItemMinutes() {
        return itemMinutes;
    }

    public void setItemMinutes(String itemMinutes) {
        this.itemMinutes = itemMinutes;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateAccountId() {
        return createAccountId;
    }

    public void setCreateAccountId(String createAccountId) {
        this.createAccountId = createAccountId == null ? null : createAccountId.trim();
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateAccountId() {
        return updateAccountId;
    }

    public void setUpdateAccountId(String updateAccountId) {
        this.updateAccountId = updateAccountId == null ? null : updateAccountId.trim();
    }

    public Short getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Short deleteFlag) {
        this.deleteFlag = deleteFlag;
    }
}