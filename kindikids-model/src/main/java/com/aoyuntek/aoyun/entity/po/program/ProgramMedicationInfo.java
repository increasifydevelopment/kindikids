package com.aoyuntek.aoyun.entity.po.program;

import java.util.Date;

import com.aoyuntek.framework.model.BaseModel;

public class ProgramMedicationInfo extends BaseModel {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String id;

    private Date day;

    private String roomId;

    private String childAccountId;

    private String medicationName;

    private Date expiryDate;

    private String reasonMedication;

    private String dosageMedication;

    private String timeMedicationId;

    private String medicationAdminitered;

    private String dateMedicationAdminitered;

    private String staffReceivingAccountId;

    private String mustAssignAccountId1;

    private String mustAssignAccountId2;

    private String createAccountId;

    private Date createTime;

    private Date updateTime;

    private String updateAccountId;

    private Short deleteFlag;

    private Short statu;

    public String getDosageMedication() {
        return dosageMedication;
    }

    public void setDosageMedication(String dosageMedication) {
        this.dosageMedication = dosageMedication == null ? null : dosageMedication.trim();
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public Short getStatu() {
        return statu;
    }

    public void setStatu(Short statu) {
        this.statu = statu;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Date getDay() {
        return day;
    }

    public void setDay(Date day) {
        this.day = day;
    }

    public String getChildAccountId() {
        return childAccountId;
    }

    public void setChildAccountId(String childAccountId) {
        this.childAccountId = childAccountId == null ? null : childAccountId.trim();
    }

    public String getMedicationName() {
        return medicationName;
    }

    public void setMedicationName(String medicationName) {
        this.medicationName = medicationName == null ? null : medicationName.trim();
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getReasonMedication() {
        return reasonMedication;
    }

    public void setReasonMedication(String reasonMedication) {
        this.reasonMedication = reasonMedication == null ? null : reasonMedication.trim();
    }

    public String getTimeMedicationId() {
        return timeMedicationId;
    }

    public void setTimeMedicationId(String timeMedicationId) {
        this.timeMedicationId = timeMedicationId == null ? null : timeMedicationId.trim();
    }

    public String getMedicationAdminitered() {
        return medicationAdminitered;
    }

    public void setMedicationAdminitered(String medicationAdminitered) {
        this.medicationAdminitered = medicationAdminitered == null ? null : medicationAdminitered.trim();
    }

    public String getDateMedicationAdminitered() {
        return dateMedicationAdminitered;
    }

    public void setDateMedicationAdminitered(String dateMedicationAdminitered) {
        this.dateMedicationAdminitered = dateMedicationAdminitered == null ? null : dateMedicationAdminitered.trim();
    }

    public String getStaffReceivingAccountId() {
        return staffReceivingAccountId;
    }

    public void setStaffReceivingAccountId(String staffReceivingAccountId) {
        this.staffReceivingAccountId = staffReceivingAccountId == null ? null : staffReceivingAccountId.trim();
    }

    public String getMustAssignAccountId1() {
        return mustAssignAccountId1;
    }

    public void setMustAssignAccountId1(String mustAssignAccountId1) {
        this.mustAssignAccountId1 = mustAssignAccountId1;
    }

    public String getMustAssignAccountId2() {
        return mustAssignAccountId2;
    }

    public void setMustAssignAccountId2(String mustAssignAccountId2) {
        this.mustAssignAccountId2 = mustAssignAccountId2;
    }

    public String getCreateAccountId() {
        return createAccountId;
    }

    public void setCreateAccountId(String createAccountId) {
        this.createAccountId = createAccountId == null ? null : createAccountId.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateAccountId() {
        return updateAccountId;
    }

    public void setUpdateAccountId(String updateAccountId) {
        this.updateAccountId = updateAccountId == null ? null : updateAccountId.trim();
    }

    public Short getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Short deleteFlag) {
        this.deleteFlag = deleteFlag;
    }
}