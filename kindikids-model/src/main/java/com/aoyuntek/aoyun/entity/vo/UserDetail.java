package com.aoyuntek.aoyun.entity.vo;

import com.aoyuntek.aoyun.entity.po.UserInfo;

/**
 * @description 用户详细信息
 * @author Hxzhang 2016年9月19日下午5:24:57
 * @version 1.0
 */
public class UserDetail extends UserInfo {
    /**
     * 序列化
     */
    private static final long serialVersionUID = 1L;
    /**
     * 园区名称
     */
    private String centreName;
    /**
     * 房间名称
     */
    private String roomName;
    /**
     * 分组名称
     */
    private String groupName;

    public String getCentreName() {
        return centreName;
    }

    public void setCentreName(String centreName) {
        this.centreName = centreName;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

}
