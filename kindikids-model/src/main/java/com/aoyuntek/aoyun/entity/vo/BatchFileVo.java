package com.aoyuntek.aoyun.entity.vo;

import java.util.List;

import com.aoyuntek.framework.model.BaseModel;

public class BatchFileVo extends BaseModel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String sourceId;

    private List<FileVo> files;

    public BatchFileVo() {
    }

    public BatchFileVo(String sourceId, List<FileVo> files) {
        super();
        this.sourceId = sourceId;
        this.files = files;
    }

    public List<FileVo> getFiles() {
        return files;
    }

    public void setFiles(List<FileVo> files) {
        this.files = files;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

}
