package com.aoyuntek.aoyun.entity.vo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.aoyuntek.aoyun.entity.po.AccountInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.framework.deserializer.DateJsonSerializer2;
import com.aoyuntek.framework.model.BaseModel;

/**
 * @description userInfoVO实体
 * @author bqbao
 * @creat 2016-6-20 10:31
 * @version 1.0
 */
public class UserInfoVo extends BaseModel {

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 1L;

	private boolean FutureEnrolled;

	/**
	 * 用户信息
	 */
	private UserInfo userInfo;

	/**
	 * 账号信息
	 */
	private AccountInfo accountInfo;

	/**
	 * 角色信息
	 */
	private List<RoleInfoVo> roleInfoVoList = new ArrayList<RoleInfoVo>();
	/**
	 * 兼职历史角色信息
	 */
	private List<RoleInfoVo> historyRoles = new ArrayList<RoleInfoVo>();

	/**
	 * 如果家长登录，家长的小孩所在的所有园区集合
	 */
	private List<String> parentCenterIds = new ArrayList<String>();

	private List<String> parentRoomsIds = new ArrayList<String>();

	private Date currtenDate;

	private List<String> rosterCentreIds;

	public List<RoleInfoVo> getHistoryRoles() {
		return historyRoles;
	}

	public void setHistoryRoles(List<RoleInfoVo> historyRoles) {
		this.historyRoles = historyRoles;
	}

	public List<String> getRosterCentreIds() {
		return rosterCentreIds;
	}

	public void setRosterCentreIds(List<String> rosterCentreIds) {
		this.rosterCentreIds = rosterCentreIds;
	}

	@JsonSerialize(using = DateJsonSerializer2.class, include = JsonSerialize.Inclusion.ALWAYS)
	public Date getCurrtenDate() {
		return currtenDate;
	}

	public void setCurrtenDate(Date currtenDate) {
		this.currtenDate = currtenDate;
	}

	public List<String> getParentRoomsIds() {
		return parentRoomsIds;
	}

	public void setParentRoomsIds(List<String> parentRoomsIds) {
		this.parentRoomsIds = parentRoomsIds;
	}

	public List<String> getParentCenterIds() {
		return parentCenterIds;
	}

	public void setParentCenterIds(List<String> parentCenterIds) {
		this.parentCenterIds = parentCenterIds;
	}

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	public AccountInfo getAccountInfo() {
		return accountInfo;
	}

	public void setAccountInfo(AccountInfo accountInfo) {
		this.accountInfo = accountInfo;
	}

	public List<RoleInfoVo> getRoleInfoVoList() {
		return roleInfoVoList;
	}

	public void setRoleInfoVoList(List<RoleInfoVo> roleInfoVoList) {
		this.roleInfoVoList = roleInfoVoList;
	}

	public boolean isFutureEnrolled() {
		return FutureEnrolled;
	}

	public void setFutureEnrolled(boolean futureEnrolled) {
		FutureEnrolled = futureEnrolled;
	}

}
