package com.aoyuntek.aoyun.entity.po;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.theone.date.util.DateUtil;

public class ClosurePeriod {
	private String id;

	private String chooseCentre;

	private Date dateFrom;

	private Date dateTo;

	private String name;

	private Short holiday;

	private Boolean deleteFlag;

	private List<String> centres = new ArrayList<String>();

	private boolean confirmRoster;

	private boolean confirmEvent;

	private boolean disabled;

	private boolean edit = true;

	public boolean isEdit() {
		return edit;
	}

	public void setEdit(boolean edit) {
		this.edit = edit;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public boolean isConfirmRoster() {
		return confirmRoster;
	}

	public void setConfirmRoster(boolean confirmRoster) {
		this.confirmRoster = confirmRoster;
	}

	public boolean isConfirmEvent() {
		return confirmEvent;
	}

	public void setConfirmEvent(boolean confirmEvent) {
		this.confirmEvent = confirmEvent;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id == null ? null : id.trim();
	}

	public String getChooseCentre() {
		return chooseCentre;
	}

	public void setChooseCentre(String chooseCentre) {
		this.chooseCentre = chooseCentre == null ? null : chooseCentre.trim();
	}

	public Date getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(Date dateFrom) {
		if (!dateFrom.after(currentDate())) {
			setDisabled(true);
		}
		this.dateFrom = dateFrom;
	}

	public Date getDateTo() {
		return dateTo;
	}

	public void setDateTo(Date dateTo) {
		if (!dateTo.after(currentDate())) {
			setEdit(false);
		}
		this.dateTo = dateTo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name == null ? null : name.trim();
	}

	public Short getHoliday() {
		return holiday;
	}

	public void setHoliday(Short holiday) {
		this.holiday = holiday;
	}

	public Boolean getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(Boolean deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public List<String> getCentres() {
		return centres;
	}

	public void setCentres(List<String> centres) {
		this.centres = centres;
	}

	private Date currentDate() {
		return DateUtil.parse(DateUtil.format(new Date(), "dd/MM/yyyy"), "dd/MM/yyyy");
	}
}