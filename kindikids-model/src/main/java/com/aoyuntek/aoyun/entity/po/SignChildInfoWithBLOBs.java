package com.aoyuntek.aoyun.entity.po;

import java.util.Date;
import java.util.UUID;

import com.aoyuntek.aoyun.enums.DeleteFlag;

public class SignChildInfoWithBLOBs extends SignChildInfo {
    private String signInName;

    private String signOutName;
    
    public SignChildInfoWithBLOBs() {

    }

    public SignChildInfoWithBLOBs(Boolean sunscreenApp, Date createTime, String createAccountId, String signInName, String signOutName) {
        super.setId(UUID.randomUUID().toString());
        super.setSunscreenApp(sunscreenApp);
        super.setCreateTime(createTime);
        super.setCreateAccountId(createAccountId);
        super.setUpdateTime(createTime);
        super.setUpdateAccountId(createAccountId);
        super.setDeleteFlag(DeleteFlag.Default.getValue());
        this.signInName = signInName;
        this.signOutName = signOutName;
    }

    public String getSignInName() {
        return signInName;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_sign_child.sign_in_name
     * 
     * @param signInName
     *            the value for tbl_sign_child.sign_in_name
     * 
     * @mbggenerated Mon Sep 12 14:35:05 CST 2016
     */
    public void setSignInName(String signInName) {
        this.signInName = signInName == null ? null : signInName.trim();
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_sign_child.sign_out_name
     * 
     * @return the value of tbl_sign_child.sign_out_name
     * 
     * @mbggenerated Mon Sep 12 14:35:05 CST 2016
     */
    public String getSignOutName() {
        return signOutName;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_sign_child.sign_out_name
     * 
     * @param signOutName
     *            the value for tbl_sign_child.sign_out_name
     * 
     * @mbggenerated Mon Sep 12 14:35:05 CST 2016
     */
    public void setSignOutName(String signOutName) {
        this.signOutName = signOutName == null ? null : signOutName.trim();
    }
}