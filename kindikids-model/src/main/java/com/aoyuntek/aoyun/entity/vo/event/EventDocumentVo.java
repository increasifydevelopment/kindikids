package com.aoyuntek.aoyun.entity.vo.event;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.aoyuntek.aoyun.entity.po.event.EventDocumentsInfo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.framework.deserializer.S3JsonSerializer;

public class EventDocumentVo extends EventDocumentsInfo {
	//tag小孩Everyone 数据为0
	private static final long serialVersionUID = 1L;

	private String fileName;
	
	private String attachId;
	private String visitUrl;
	private List<SelecterPo> childSelectList=new ArrayList<SelecterPo>();
	
	public List<SelecterPo> getChildSelectList() {
		return childSelectList;
	}

	public void setChildSelectList(List<SelecterPo> childSelectList) {
		this.childSelectList = childSelectList;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getAttachId() {
		return attachId;
	}

	public void setAttachId(String attachId) {
		this.attachId = attachId;
	}
	
	@JsonSerialize(using = S3JsonSerializer.class)
	public String getVisitUrl() {
		return getAttachId();
	}

	public void setVisitUrl(String visitUrl) {
		this.visitUrl = visitUrl;
	}
	
	 

	
}
