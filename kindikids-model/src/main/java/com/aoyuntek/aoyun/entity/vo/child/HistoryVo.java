package com.aoyuntek.aoyun.entity.vo.child;

import java.util.List;

import com.aoyuntek.aoyun.entity.vo.AttendanceHistoryVo;
import com.aoyuntek.framework.model.BaseModel;

public class HistoryVo extends BaseModel {

    private static final long serialVersionUID = 1L;

    private List<AttendanceHistoryGroupVo> attendanceHistoryGroupVos;

    private List<AttendanceHistoryVo> futureAttendance;
    
    public HistoryVo() {
        super();
    }

    public HistoryVo(List<AttendanceHistoryGroupVo> attendanceHistoryGroupVos, List<AttendanceHistoryVo> futureAttendance) {
        super();
        this.attendanceHistoryGroupVos = attendanceHistoryGroupVos;
        this.futureAttendance = futureAttendance;
    }

    public List<AttendanceHistoryGroupVo> getAttendanceHistoryGroupVos() {
        return attendanceHistoryGroupVos;
    }

    public void setAttendanceHistoryGroupVos(List<AttendanceHistoryGroupVo> attendanceHistoryGroupVos) {
        this.attendanceHistoryGroupVos = attendanceHistoryGroupVos;
    }

    public List<AttendanceHistoryVo> getFutureAttendance() {
        return futureAttendance;
    }

    public void setFutureAttendance(List<AttendanceHistoryVo> futureAttendance) {
        this.futureAttendance = futureAttendance;
    }
}
