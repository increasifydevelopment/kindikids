package com.aoyuntek.aoyun.entity.po;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author dlli5 at 2016年8月25日下午4:18:40
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public class ReplaceChildAttendanceVo  implements Serializable {

    private static final long serialVersionUID = 1L;

    private List<ReplaceChildVo> internal = new ArrayList<ReplaceChildVo>();

    private List<ReplaceChildVo> siblings = new ArrayList<ReplaceChildVo>();

    private List<ReplaceChildVo> external = new ArrayList<ReplaceChildVo>();
    
    private List<ReplaceChildVo> allInternal = new ArrayList<ReplaceChildVo>();

    private ReplaceChildVo absenteeChild;

    private AbsenteeRequestInfo absenteeRequestInfo;

    private int weekInstances = 0;

    public int getWeekInstances() {
        return weekInstances;
    }

    public void setWeekInstances(int weekInstances) {
        this.weekInstances = weekInstances;
    }

    public List<ReplaceChildVo> getInternal() {
        return internal;
    }

    public void setInternal(List<ReplaceChildVo> internal) {
        this.internal = internal;
    }

    public List<ReplaceChildVo> getSiblings() {
        return siblings;
    }

    public void setSiblings(List<ReplaceChildVo> siblings) {
        this.siblings = siblings;
    }

    public List<ReplaceChildVo> getExternal() {
        return external;
    }

    public void setExternal(List<ReplaceChildVo> external) {
        this.external = external;
    }

    public ReplaceChildVo getAbsenteeChild() {
        return absenteeChild;
    }

    public void setAbsenteeChild(ReplaceChildVo absenteeChild) {
        this.absenteeChild = absenteeChild;
    }

    public AbsenteeRequestInfo getAbsenteeRequestInfo() {
        return absenteeRequestInfo;
    }

    public void setAbsenteeRequestInfo(AbsenteeRequestInfo absenteeRequestInfo) {
        this.absenteeRequestInfo = absenteeRequestInfo;
    }

	public List<ReplaceChildVo> getAllInternal() {
		return allInternal;
	}

	public void setAllInternal(List<ReplaceChildVo> allInternal) {
		this.allInternal = allInternal;
	}

}
