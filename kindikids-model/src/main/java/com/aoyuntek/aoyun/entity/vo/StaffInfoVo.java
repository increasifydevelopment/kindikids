package com.aoyuntek.aoyun.entity.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.aoyuntek.aoyun.entity.po.AccountInfo;
import com.aoyuntek.aoyun.entity.po.AccountRoleInfo;
import com.aoyuntek.aoyun.entity.po.CredentialsInfo;
import com.aoyuntek.aoyun.entity.po.EmergencyContactInfo;
import com.aoyuntek.aoyun.entity.po.MedicalImmunisationInfo;
import com.aoyuntek.aoyun.entity.po.MedicalInfo;
import com.aoyuntek.aoyun.entity.po.StaffEmploymentInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;

/**
 * 
 * @author rick
 * 
 */
public class StaffInfoVo implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<StaffEmploymentInfo> employInfoList = new ArrayList<StaffEmploymentInfo>();

	private StaffEmploymentInfo employInfo;

	/**
	 * 用户信息
	 */
	private UserInfo userInfo;

	/**
	 * 账户
	 */
	private AccountInfo accountInfo;

	/**
	 * 角色
	 */
	private List<AccountRoleInfo> roleInfo;

	/**
	 * 紧急联系人
	 */
	private List<EmergencyContactInfo> contactInfo;

	/**
	 * 医疗信息
	 */
	private MedicalInfo medicalInfo;

	/**
	 * 疫苗信息
	 */
	private List<MedicalImmunisationInfo> medicalImmunisationInfo;

	/**
	 * 资格证书
	 */
	private List<CredentialsInfo> credentialsInfo;

	private String json;

	private String profileId;

	public String getProfileId() {
		return profileId;
	}

	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	public String getJson() {
		return json;
	}

	public void setJson(String json) {
		this.json = json;
	}

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	public List<EmergencyContactInfo> getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(List<EmergencyContactInfo> contactInfo) {
		this.contactInfo = contactInfo;
	}

	public MedicalInfo getMedicalInfo() {
		return medicalInfo;
	}

	public void setMedicalInfo(MedicalInfo medicalInfo) {
		this.medicalInfo = medicalInfo;
	}

	public List<MedicalImmunisationInfo> getMedicalImmunisationInfo() {
		return medicalImmunisationInfo;
	}

	public void setMedicalImmunisationInfo(List<MedicalImmunisationInfo> medicalImmunisationInfo) {
		this.medicalImmunisationInfo = medicalImmunisationInfo;
	}

	public List<CredentialsInfo> getCredentialsInfo() {
		return credentialsInfo;
	}

	public void setCredentialsInfo(List<CredentialsInfo> credentialsInfo) {
		this.credentialsInfo = credentialsInfo;
	}

	public AccountInfo getAccountInfo() {
		return accountInfo;
	}

	public void setAccountInfo(AccountInfo accountInfo) {
		this.accountInfo = accountInfo;
	}

	public List<AccountRoleInfo> getRoleInfo() {
		return roleInfo;
	}

	public void setRoleInfo(List<AccountRoleInfo> roleInfo) {
		this.roleInfo = roleInfo;
	}

	public StaffEmploymentInfo getEmployInfo() {
		return employInfo;
	}

	public void setEmployInfo(StaffEmploymentInfo employInfo) {
		this.employInfo = employInfo;
	}

	public List<StaffEmploymentInfo> getEmployInfoList() {
		return employInfoList;
	}

	public void setEmployInfoList(List<StaffEmploymentInfo> employInfoList) {
		this.employInfoList = employInfoList;
	}

}
