package com.aoyuntek.aoyun.entity.po;

public class GroupManagerRelationInfo {
	private String groupId;

	private String userId;

	private Boolean deleteFlag;

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId == null ? null : groupId.trim();
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId == null ? null : userId.trim();
	}

	public Boolean getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(Boolean deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public GroupManagerRelationInfo(String groupId, String userId, Boolean deleteFlag) {
		super();
		this.groupId = groupId;
		this.userId = userId;
		this.deleteFlag = deleteFlag;
	}
}