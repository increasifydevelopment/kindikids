package com.aoyuntek.aoyun.entity.po;

import java.util.Date;

import com.aoyuntek.aoyun.entity.vo.SelecterPo;

/**
 * 
 * @author dlli5 at 2017年1月3日上午9:53:53
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public class ReplaceChildVo extends SelecterPo {

	private static final long serialVersionUID = 1L;
	private String fristName;
	private String name;
	private String parentName;
	private String parentPhone;
	private String parentMobile;
	private Date date;
	private boolean interim = false;
	// 替换周期
	private Integer weekInstances;

	public Integer getWeekInstances() {
		return weekInstances;
	}

	public void setWeekInstances(Integer weekInstances) {
		this.weekInstances = weekInstances;
	}

	public String getFristName() {
		return fristName;
	}

	public void setFristName(String fristName) {
		this.fristName = fristName;
	}

	public String getParentMobile() {
		return parentMobile;
	}

	public void setParentMobile(String parentMobile) {
		this.parentMobile = parentMobile;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public String getParentPhone() {
		return parentPhone;
	}

	public void setParentPhone(String parentPhone) {
		this.parentPhone = parentPhone;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public boolean isInterim() {
		return interim;
	}

	public void setInterim(boolean interim) {
		this.interim = interim;
	}

}
