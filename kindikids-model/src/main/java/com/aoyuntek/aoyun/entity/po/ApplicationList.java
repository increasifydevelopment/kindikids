package com.aoyuntek.aoyun.entity.po;

import java.util.Date;
import java.util.List;

import com.theone.date.util.DateUtil;

public class ApplicationList {
	private String id;

	private String firstName;

	private String lastName;

	private Short gender;

	private Date birthday;

	private String birthdayStr;

	private Date requestedStartDate;

	private String requestedStartDateStr;

	private Boolean monday;

	private Boolean tuesday;

	private Boolean wednesday;

	private Boolean thursday;

	private Boolean friday;

	private Date applicationDate;

	private Date applicationDatePosition;

	private Short status;

	private String notes;

	private Date updateTime;

	private String updateAccountId;

	private Boolean deleteFlag;

	private String fullName;

	private List<String> centreIds;

	private Short archivedFrom;

	private String userId;

	public String getRequestedStartDateStr() {
		return requestedStartDateStr;
	}

	public void setRequestedStartDateStr(String requestedStartDateStr) {
		this.requestedStartDateStr = requestedStartDateStr;
	}

	public String getBirthdayStr() {
		return birthdayStr;
	}

	public void setBirthdayStr(String birthdayStr) {
		this.birthdayStr = birthdayStr;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Short getArchivedFrom() {
		return archivedFrom;
	}

	public void setArchivedFrom(Short archivedFrom) {
		this.archivedFrom = archivedFrom;
	}

	public List<String> getCentreIds() {
		return centreIds;
	}

	public void setCentreIds(List<String> centreIds) {
		this.centreIds = centreIds;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id == null ? null : id.trim();
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName == null ? null : firstName.trim();
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName == null ? null : lastName.trim();
	}

	public Short getGender() {
		return gender;
	}

	public void setGender(Short gender) {
		this.gender = gender;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
		setBirthdayStr(DateUtil.format(birthday, "dd/MM/yyyy"));
	}

	public Date getRequestedStartDate() {
		return requestedStartDate;
	}

	public void setRequestedStartDate(Date requestedStartDate) {
		this.requestedStartDate = requestedStartDate;
		setRequestedStartDateStr(DateUtil.format(requestedStartDate, "dd/MM/yyyy"));
	}

	public Boolean getMonday() {
		return monday;
	}

	public void setMonday(Boolean monday) {
		this.monday = monday;
	}

	public Boolean getTuesday() {
		return tuesday;
	}

	public void setTuesday(Boolean tuesday) {
		this.tuesday = tuesday;
	}

	public Boolean getWednesday() {
		return wednesday;
	}

	public void setWednesday(Boolean wednesday) {
		this.wednesday = wednesday;
	}

	public Boolean getThursday() {
		return thursday;
	}

	public void setThursday(Boolean thursday) {
		this.thursday = thursday;
	}

	public Boolean getFriday() {
		return friday;
	}

	public void setFriday(Boolean friday) {
		this.friday = friday;
	}

	public Date getApplicationDate() {
		return applicationDate;
	}

	public void setApplicationDate(Date applicationDate) {
		this.applicationDate = applicationDate;
	}

	public Date getApplicationDatePosition() {
		return applicationDatePosition;
	}

	public void setApplicationDatePosition(Date applicationDatePosition) {
		this.applicationDatePosition = applicationDatePosition;
	}

	public Short getStatus() {
		return status;
	}

	public void setStatus(Short status) {
		this.status = status;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes == null ? null : notes.trim();
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getUpdateAccountId() {
		return updateAccountId;
	}

	public void setUpdateAccountId(String updateAccountId) {
		this.updateAccountId = updateAccountId == null ? null : updateAccountId.trim();
	}

	public Boolean getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(Boolean deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public ApplicationList() {

	}

	public ApplicationList(String id, Short status, Short archivedFrom, String accountId) {
		this.id = id;
		this.status = status;
		this.archivedFrom = archivedFrom;
		this.updateTime = new Date();
		this.updateAccountId = accountId;
	}
}