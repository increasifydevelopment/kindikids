package com.aoyuntek.aoyun.entity.vo;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.aoyuntek.aoyun.entity.po.AttachmentInfo;
import com.aoyuntek.aoyun.entity.po.NqsInfo;
import com.aoyuntek.framework.deserializer.DateJsonSerializer4;
import com.aoyuntek.framework.deserializer.S3JsonSerializer;
import com.aoyuntek.framework.model.BaseModel;

/**
 * 
 * @description MGS详细信息VO
 * @author bbq
 * @create 2016年7月12日上午11:42:05
 * @version 1.0
 */
public class MessageInfoVo extends BaseModel {

    /**
     * 序列化
     */
    private static final long serialVersionUID = 1L;
    /**
     * msgId
     */
    private String msgId;
    /**
     * 发送人账号ID
     */
    private String sendAccountId;
    /**
     * 发送人
     */
    private String sendAccountName;
    /**
     * 主题
     */
    private String subject;
    /**
     * 分組+人
     */
    private List<ReceiverVo> receiverVo;

    /**
     * 人
     */
    private List<ReceiverVo> userList;

    /**
     * 头像
     */
    private String avatar;

    private String personColor;

    public List<ReceiverVo> getUserList() {
        return userList;
    }

    public void setUserList(List<ReceiverVo> userList) {
        this.userList = userList;
    }

    public String getPersonColor() {
        return personColor;
    }

    public void setPersonColor(String personColor) {
        this.personColor = personColor;
    }

    /**
     * NQS集合
     */
    private List<NqsInfo> nqsList;
    /**
     * 发送时间
     */
    private Date time;
    /**
     * ago
     */
    private String timeAgo;
    /**
     * 内容
     */
    private String content;
    /**
     * MSG类型
     */
    private short messageType;
    /**
     * 附件集合
     */
    private List<AttachmentInfo> AttachmentList;
    /**
     * email
     */
    private String email;

    private boolean showTimeAgoFlag = true;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public short getMessageType() {
        return messageType;
    }

    public void setMessageType(short messageType) {
        this.messageType = messageType;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public List<ReceiverVo> getReceiverVo() {
        return receiverVo;
    }

    public void setReceiverVo(List<ReceiverVo> receiverVo) {
        this.receiverVo = receiverVo;
    }

    @JsonSerialize(using = S3JsonSerializer.class)
    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public List<NqsInfo> getNqsList() {
        return nqsList;
    }

    public void setNqsList(List<NqsInfo> nqsList) {
        this.nqsList = nqsList;
    }

    @JsonSerialize(using = DateJsonSerializer4.class, include = JsonSerialize.Inclusion.ALWAYS)
    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getTimeAgo() {
        return timeAgo;
    }

    public void setTimeAgo(String timeAgo) {
        String str = getTimeAgoStr(this.time);
        this.timeAgo = str;
        if (StringUtils.isEmpty(str)) {
            this.showTimeAgoFlag = false;
        }
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = dealContent(content);
    }

    public String getSendAccountId() {
        return sendAccountId;
    }

    public void setSendAccountId(String sendAccountId) {
        this.sendAccountId = sendAccountId;
    }

    public String getSendAccountName() {
        return sendAccountName;
    }

    public void setSendAccountName(String sendAccountName) {
        this.sendAccountName = sendAccountName;
    }

    public List<AttachmentInfo> getAttachmentList() {
        return AttachmentList;
    }

    public void setAttachmentList(List<AttachmentInfo> attachmentList) {
        AttachmentList = attachmentList;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public boolean isShowTimeAgoFlag() {
        return showTimeAgoFlag;
    }

    public void setShowTimeAgoFlag(boolean showTimeAgoFlag) {
        this.showTimeAgoFlag = showTimeAgoFlag;
    }

    /**
     * 
     * @description 处理内容换行，空格
     * @author gfwang
     * @create 2016年7月19日下午7:45:02
     * @version 1.0
     * @param content
     *            内容
     * @return content
     */
    private String dealContent(String content) {
        if (StringUtils.isEmpty(content)) {
            return content;
        }
        // content = content.replaceAll(" ", "&nbsp;");
        content = content.replaceAll("[\\n]", "</br>");
        return content;
    }
}
