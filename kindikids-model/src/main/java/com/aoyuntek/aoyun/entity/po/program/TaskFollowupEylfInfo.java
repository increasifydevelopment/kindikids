package com.aoyuntek.aoyun.entity.po.program;

import com.aoyuntek.framework.model.BaseModel;

public class TaskFollowupEylfInfo extends BaseModel{
    private String id;

    private String followId;

    private String eylfVersion;

    private Boolean checkFlag;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getFollowId() {
        return followId;
    }

    public void setFollowId(String followId) {
        this.followId = followId == null ? null : followId.trim();
    }

    public String getEylfVersion() {
        return eylfVersion;
    }

    public void setEylfVersion(String eylfVersion) {
        this.eylfVersion = eylfVersion == null ? null : eylfVersion.trim();
    }

    public Boolean getCheckFlag() {
        return checkFlag;
    }

    public void setCheckFlag(Boolean checkFlag) {
        this.checkFlag = checkFlag;
    }
}