package com.aoyuntek.aoyun.entity.po;

@SuppressWarnings("serial")
public class ValueTableInfo extends ValueInfo {

    private String value;

    private Integer col;

    private Integer row;

    private Boolean inputFlag;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value == null ? null : value.trim();
    }

    public Integer getCol() {
        return col;
    }

    public void setCol(Integer col) {
        this.col = col;
    }

    public Integer getRow() {
        return row;
    }

    public void setRow(Integer row) {
        this.row = row;
    }

    public Boolean getInputFlag() {
        return inputFlag;
    }

    public void setInputFlag(Boolean inputFlag) {
        this.inputFlag = inputFlag;
    }
}