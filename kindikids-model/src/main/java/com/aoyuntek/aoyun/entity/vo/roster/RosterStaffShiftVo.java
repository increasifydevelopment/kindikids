package com.aoyuntek.aoyun.entity.vo.roster;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RosterStaffShiftVo {
	private Date day;
	private List<RosterStaffVo> rosterStaffVos = new ArrayList<RosterStaffVo>();

	public Date getDay() {
		return day;
	}

	public void setDay(Date day) {
		this.day = day;
	}

	public List<RosterStaffVo> getRosterStaffVos() {
		return rosterStaffVos;
	}

	public void setRosterStaffVos(List<RosterStaffVo> rosterStaffVos) {
		this.rosterStaffVos = rosterStaffVos;
	}

	public RosterStaffShiftVo() {
		super();
	}

	public RosterStaffShiftVo(Date day, List<RosterStaffVo> rosterStaffVos) {
		super();
		this.day = day;
		this.rosterStaffVos = rosterStaffVos;
	}
}
