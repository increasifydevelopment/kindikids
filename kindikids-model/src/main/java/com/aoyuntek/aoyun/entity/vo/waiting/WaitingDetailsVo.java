package com.aoyuntek.aoyun.entity.vo.waiting;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.ApplicationList;
import com.aoyuntek.aoyun.entity.po.ApplicationParent;
import com.aoyuntek.aoyun.entity.po.CentersInfo;
import com.aoyuntek.aoyun.entity.po.OutRelevantInfo;

public class WaitingDetailsVo {
	private ApplicationList child;
	private List<ApplicationParent> parents;
	private List<CentersInfo> centersInfos;
	private OutRelevantInfo outRelevantInfo;

	public OutRelevantInfo getOutRelevantInfo() {
		return outRelevantInfo;
	}

	public void setOutRelevantInfo(OutRelevantInfo outRelevantInfo) {
		this.outRelevantInfo = outRelevantInfo;
	}

	public List<CentersInfo> getCentersInfos() {
		return centersInfos;
	}

	public void setCentersInfos(List<CentersInfo> centersInfos) {
		this.centersInfos = centersInfos;
	}

	public ApplicationList getChild() {
		return child;
	}

	public void setChild(ApplicationList child) {
		this.child = child;
	}

	public List<ApplicationParent> getParents() {
		return parents;
	}

	public void setParents(List<ApplicationParent> parents) {
		this.parents = parents;
	}

	public WaitingDetailsVo() {

	}

	public WaitingDetailsVo(ApplicationList child, List<ApplicationParent> parents, List<CentersInfo> centersInfos,
			OutRelevantInfo outRelevantInfo) {
		this.child = child;
		this.parents = parents;
		this.centersInfos = centersInfos;
		this.outRelevantInfo = outRelevantInfo;
	}
}
