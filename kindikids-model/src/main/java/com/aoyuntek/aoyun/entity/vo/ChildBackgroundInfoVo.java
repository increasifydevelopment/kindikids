package com.aoyuntek.aoyun.entity.vo;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.BackgroundPersonInfo;
import com.aoyuntek.aoyun.entity.po.BackgroundPetsInfo;
import com.aoyuntek.aoyun.entity.po.ChildBackgroundInfo;
import com.aoyuntek.framework.model.BaseModel;

public class ChildBackgroundInfoVo extends BaseModel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    /**
     * ChildBackgroundInfo
     */
    private ChildBackgroundInfo childBackgroundInfo = new ChildBackgroundInfo();

    /**
     * 宠物
     */
    private List<BackgroundPetsInfo> petList;

    /**
     * 人
     */
    private List<BackgroundPersonInfo> personList;

    public List<BackgroundPetsInfo> getPetList() {
        return petList;
    }

    public void setPetList(List<BackgroundPetsInfo> petList) {
        this.petList = petList;
    }

    public List<BackgroundPersonInfo> getPersonList() {
        return personList;
    }

    public void setPersonList(List<BackgroundPersonInfo> personList) {
        this.personList = personList;
    }

    public ChildBackgroundInfo getChildBackgroundInfo() {
        return childBackgroundInfo;
    }

    public void setChildBackgroundInfo(ChildBackgroundInfo childBackgroundInfo) {
        this.childBackgroundInfo = childBackgroundInfo;
    }

}
