package com.aoyuntek.aoyun.entity.vo.old;

import java.util.ArrayList;
import java.util.List;

import com.aoyuntek.aoyun.entity.po.AccountInfo;
import com.aoyuntek.aoyun.entity.po.AccountRoleInfo;
import com.aoyuntek.aoyun.entity.po.AttachmentInfo;
import com.aoyuntek.aoyun.entity.po.AttendanceHistoryInfo;
import com.aoyuntek.aoyun.entity.po.BackgroundPersonInfo;
import com.aoyuntek.aoyun.entity.po.BackgroundPetsInfo;
import com.aoyuntek.aoyun.entity.po.ChildAttendanceInfo;
import com.aoyuntek.aoyun.entity.po.ChildBackgroundInfo;
import com.aoyuntek.aoyun.entity.po.ChildCustodyArrangeInfo;
import com.aoyuntek.aoyun.entity.po.ChildDietaryRequireInfo;
import com.aoyuntek.aoyun.entity.po.ChildHealthMedicalDetailInfo;
import com.aoyuntek.aoyun.entity.po.ChildHealthMedicalInfo;
import com.aoyuntek.aoyun.entity.po.ChildMedicalInfo;
import com.aoyuntek.aoyun.entity.po.EmergencyContactInfo;
import com.aoyuntek.aoyun.entity.po.FamilyInfo;
import com.aoyuntek.aoyun.entity.po.RelationKidParentInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.po.program.ChildEylfInfo;

public class OldFamilyInfoVo {
    private List<UserInfo> userList = new ArrayList<UserInfo>();
    private List<AccountInfo> accountList = new ArrayList<AccountInfo>();
    private List<AccountRoleInfo> roleRelationList = new ArrayList<AccountRoleInfo>();
    private List<RelationKidParentInfo> parentChildRelationList = new ArrayList<RelationKidParentInfo>();
    private List<AttachmentInfo> fileList = new ArrayList<AttachmentInfo>();
    private List<ChildBackgroundInfo> backgroundList = new ArrayList<ChildBackgroundInfo>();
    private List<ChildMedicalInfo> medicalList = new ArrayList<ChildMedicalInfo>();
    private List<ChildDietaryRequireInfo> dietaryList = new ArrayList<ChildDietaryRequireInfo>();
    private List<ChildAttendanceInfo> childAttendList = new ArrayList<ChildAttendanceInfo>();
    private List<EmergencyContactInfo> emergencyContactList = new ArrayList<EmergencyContactInfo>();
    private List<BackgroundPetsInfo> petList = new ArrayList<BackgroundPetsInfo>();
    private List<BackgroundPersonInfo> personList = new ArrayList<BackgroundPersonInfo>();
    private List<ChildCustodyArrangeInfo> custodyArrangeList = new ArrayList<ChildCustodyArrangeInfo>();
    private List<FamilyInfo> familyList = new ArrayList<FamilyInfo>();
    private List<ChildHealthMedicalInfo> healthMedicalList = new ArrayList<ChildHealthMedicalInfo>();
    private List<ChildHealthMedicalDetailInfo> healthMedicalDetailList = new ArrayList<ChildHealthMedicalDetailInfo>();
    private List<AttendanceHistoryInfo> childAttendHistoryList=new ArrayList<AttendanceHistoryInfo>();
    private List<ChildEylfInfo> eylfList=new ArrayList<ChildEylfInfo>();
    
    
    public List<ChildEylfInfo> getEylfList() {
        return eylfList;
    }

    public void setEylfList(List<ChildEylfInfo> eylfList) {
        this.eylfList = eylfList;
    }

    public List<AttendanceHistoryInfo> getChildAttendHistoryList() {
        return childAttendHistoryList;
    }

    public void setChildAttendHistoryList(List<AttendanceHistoryInfo> childAttendHistoryList) {
        this.childAttendHistoryList = childAttendHistoryList;
    }

    public List<ChildHealthMedicalInfo> getHealthMedicalList() {
        return healthMedicalList;
    }

    public void setHealthMedicalList(List<ChildHealthMedicalInfo> healthMedicalList) {
        this.healthMedicalList = healthMedicalList;
    }

    public List<ChildHealthMedicalDetailInfo> getHealthMedicalDetailList() {
        return healthMedicalDetailList;
    }

    public void setHealthMedicalDetailList(List<ChildHealthMedicalDetailInfo> healthMedicalDetailList) {
        this.healthMedicalDetailList = healthMedicalDetailList;
    }

    public List<UserInfo> getUserList() {
        return userList;
    }

    public void setUserList(List<UserInfo> userList) {
        this.userList = userList;
    }

    public List<AccountInfo> getAccountList() {
        return accountList;
    }

    public void setAccountList(List<AccountInfo> accountList) {
        this.accountList = accountList;
    }

    public List<AccountRoleInfo> getRoleRelationList() {
        return roleRelationList;
    }

    public void setRoleRelationList(List<AccountRoleInfo> roleRelationList) {
        this.roleRelationList = roleRelationList;
    }

    public List<RelationKidParentInfo> getParentChildRelationList() {
        return parentChildRelationList;
    }

    public void setParentChildRelationList(List<RelationKidParentInfo> parentChildRelationList) {
        this.parentChildRelationList = parentChildRelationList;
    }

    public List<AttachmentInfo> getFileList() {
        return fileList;
    }

    public void setFileList(List<AttachmentInfo> fileList) {
        this.fileList = fileList;
    }

    public List<ChildBackgroundInfo> getBackgroundList() {
        return backgroundList;
    }

    public void setBackgroundList(List<ChildBackgroundInfo> backgroundList) {
        this.backgroundList = backgroundList;
    }

    public List<ChildMedicalInfo> getMedicalList() {
        return medicalList;
    }

    public void setMedicalList(List<ChildMedicalInfo> medicalList) {
        this.medicalList = medicalList;
    }

    public List<ChildDietaryRequireInfo> getDietaryList() {
        return dietaryList;
    }

    public void setDietaryList(List<ChildDietaryRequireInfo> dietaryList) {
        this.dietaryList = dietaryList;
    }

    public List<ChildAttendanceInfo> getChildAttendList() {
        return childAttendList;
    }

    public void setChildAttendList(List<ChildAttendanceInfo> childAttendList) {
        this.childAttendList = childAttendList;
    }

    public List<EmergencyContactInfo> getEmergencyContactList() {
        return emergencyContactList;
    }

    public void setEmergencyContactList(List<EmergencyContactInfo> emergencyContactList) {
        this.emergencyContactList = emergencyContactList;
    }

    public List<BackgroundPetsInfo> getPetList() {
        return petList;
    }

    public void setPetList(List<BackgroundPetsInfo> petList) {
        this.petList = petList;
    }

    public List<BackgroundPersonInfo> getPersonList() {
        return personList;
    }

    public void setPersonList(List<BackgroundPersonInfo> personList) {
        this.personList = personList;
    }

    public List<ChildCustodyArrangeInfo> getCustodyArrangeList() {
        return custodyArrangeList;
    }

    public void setCustodyArrangeList(List<ChildCustodyArrangeInfo> custodyArrangeList) {
        this.custodyArrangeList = custodyArrangeList;
    }

    public List<FamilyInfo> getFamilyList() {
        return familyList;
    }

    public void setFamilyList(List<FamilyInfo> familyList) {
        this.familyList = familyList;
    }

}
