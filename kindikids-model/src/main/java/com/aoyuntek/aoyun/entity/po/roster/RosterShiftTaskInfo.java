package com.aoyuntek.aoyun.entity.po.roster;

import java.util.Date;

import com.aoyuntek.framework.model.BaseModel;

public class RosterShiftTaskInfo extends BaseModel {
    private static final long serialVersionUID = 1L;

    private String shiftTimeId;

    private String taskId;

    private Date createTime;

    private String createAccountId;

    private Short deleteFlag;

    public String getShiftTimeId() {
        return shiftTimeId;
    }

    public void setShiftTimeId(String shiftTimeId) {
        this.shiftTimeId = shiftTimeId == null ? null : shiftTimeId.trim();
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId == null ? null : taskId.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateAccountId() {
        return createAccountId;
    }

    public void setCreateAccountId(String createAccountId) {
        this.createAccountId = createAccountId == null ? null : createAccountId.trim();
    }

    public Short getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Short deleteFlag) {
        this.deleteFlag = deleteFlag;
    }
}