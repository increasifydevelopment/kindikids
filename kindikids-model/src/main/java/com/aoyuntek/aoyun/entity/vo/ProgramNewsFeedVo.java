package com.aoyuntek.aoyun.entity.vo;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.YelfInfo;
import com.aoyuntek.aoyun.enums.NewsType;
import com.aoyuntek.framework.model.BaseModel;

/**
 * 
 * @description 从program过来的newsfeed信息
 * @author gfwang
 * @create 2016年9月20日下午5:06:13
 * @version 1.0
 */
public class ProgramNewsFeedVo extends BaseModel {
    /**
     * 序列化
     */
    private static final long serialVersionUID = 1L;

    private String taskId;

    private String newsId;

    private String roomId;

    /**
     * 内容
     */
    private String content;
    /**
     * tag的小孩
     */
    private List<String> accounts;
    /**
     * news 类型
     */
    private NewsType newsType;
    /**
     * nqs信息
     */
    private List<NqsVo> nqsVoList;
    /**
     * eylf信息
     */
    private List<YelfInfo> eylfList;
    /**
     * 当前登录用户
     */
    private UserInfoVo currtenUser;

    /**
     * add by 2016/12/06
     */
    private String contentTip0;

    /**
     * Observation newsfeed内容 1
     */
    private String contentTip1;

    /**
     * Observation newsfeed内容2
     */
    private String contentTip2;
    /**
     * Observation newsfeed内容3
     */
    private String contentTip3;

    /**
     * 表示分享主体
     */
    private Short taskType;

    private short tagType = 0;

    /**
     * 表示是否為school newsfeed
     */
    private Short schoolFlag;

    /**
     * 
     */
    private List<ActivitiesVo> actList;

    public String getContentTip0() {
        return contentTip0;
    }

    public void setContentTip0(String contentTip0) {
        this.contentTip0 = contentTip0;
    }

    public List<ActivitiesVo> getActList() {
        return actList;
    }

    public void setActList(List<ActivitiesVo> actList) {
        this.actList = actList;
    }

    public Short getSchoolFlag() {
        return schoolFlag;
    }

    public void setSchoolFlag(Short schoolFlag) {
        this.schoolFlag = schoolFlag;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public short getTagType() {
        return tagType;
    }

    public void setTagType(short tagType) {
        this.tagType = tagType;
    }

    public Short getTaskType() {
        return taskType;
    }

    public void setTaskType(Short taskType) {
        this.taskType = taskType;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public List<YelfInfo> getEylfList() {
        return eylfList;
    }

    public void setEylfList(List<YelfInfo> eylfList) {
        this.eylfList = eylfList;
    }

    public UserInfoVo getCurrtenUser() {
        return currtenUser;
    }

    public void setCurrtenUser(UserInfoVo currtenUser) {
        this.currtenUser = currtenUser;
    }

    public String getContentTip1() {
        return contentTip1;
    }

    public void setContentTip1(String contentTip1) {
        this.contentTip1 = contentTip1;
    }

    public String getContentTip2() {
        return contentTip2;
    }

    public void setContentTip2(String contentTip2) {
        this.contentTip2 = contentTip2;
    }

    public String getContentTip3() {
        return contentTip3;
    }

    public void setContentTip3(String contentTip3) {
        this.contentTip3 = contentTip3;
    }

    public String getNewsId() {
        return newsId;
    }

    public void setNewsId(String newsId) {
        this.newsId = newsId;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<String> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<String> accounts) {
        this.accounts = accounts;
    }

    public NewsType getNewsType() {
        return newsType;
    }

    public void setNewsType(NewsType newsType) {
        this.newsType = newsType;
    }

    public List<NqsVo> getNqsVoList() {
        return nqsVoList;
    }

    public void setNqsVoList(List<NqsVo> nqsVoList) {
        this.nqsVoList = nqsVoList;
    }

}
