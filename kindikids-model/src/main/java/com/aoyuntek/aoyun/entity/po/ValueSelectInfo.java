package com.aoyuntek.aoyun.entity.po;

public class ValueSelectInfo {
    private String instanceId;

    private String formAttrId;

    private String value;

    private String name;

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId == null ? null : instanceId.trim();
    }

    public String getFormAttrId() {
        return formAttrId;
    }

    public void setFormAttrId(String formAttrId) {
        this.formAttrId = formAttrId == null ? null : formAttrId.trim();
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value == null ? null : value.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }
}