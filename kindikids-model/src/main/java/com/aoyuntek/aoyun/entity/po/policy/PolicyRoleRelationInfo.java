package com.aoyuntek.aoyun.entity.po.policy;

import com.aoyuntek.framework.model.BaseModel;

public class PolicyRoleRelationInfo extends BaseModel {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String roleId;

    private String policyId;

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId == null ? null : roleId.trim();
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId == null ? null : policyId.trim();
    }
}