package com.aoyuntek.aoyun.entity.po.program;

import java.util.Date;
import java.util.UUID;

import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.framework.model.BaseModel;

public class ProgramIntobsleaInfo extends BaseModel {
	private static final long serialVersionUID = 1L;

	private String id;

	private Short type;

	private String centerId;

	private String roomId;

	private String childId;

	private Short category;

	private String observation;

	private Date evaluationDate;

	private String learning;

	private String objective;

	private String educatorExperience;

	private Date day;

	private String newsfeedId;

	private Short state;

	private String createAccountId;

	private Date updateTime;

	private String updateAccountId;

	private Short deleteFlag;

	private String oldId;

	private Date classDate;

	public Date getClassDate() {
		return classDate;
	}

	public void setClassDate(Date classDate) {
		this.classDate = classDate;
	}

	public ProgramIntobsleaInfo() {

	}

	public ProgramIntobsleaInfo(Short type, String centerId, String roomId, String childId, Short category, String observation, Date evaluationDate, String learning,
			String objective, String educatorExperience, Date day, String newsfeedId, Short state, String createAccountId, Date updateTime, String updateAccountId,
			String oldId) {
		this.id = UUID.randomUUID().toString();
		this.type = type;
		this.centerId = centerId;
		this.roomId = roomId;
		this.childId = childId;
		this.category = category;
		this.observation = observation;
		this.evaluationDate = evaluationDate;
		this.learning = learning;
		this.objective = objective;
		this.educatorExperience = educatorExperience;
		this.day = day;
		this.newsfeedId = newsfeedId;
		this.state = state;
		this.createAccountId = createAccountId;
		this.updateTime = updateTime;
		this.updateAccountId = updateAccountId;
		this.deleteFlag = DeleteFlag.Default.getValue();
		this.oldId = oldId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id == null ? null : id.trim();
	}

	public Short getType() {
		return type;
	}

	public void setType(Short type) {
		this.type = type;
	}

	public String getCenterId() {
		return centerId;
	}

	public void setCenterId(String centerId) {
		this.centerId = centerId == null ? null : centerId.trim();
	}

	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId == null ? null : roomId.trim();
	}

	public String getChildId() {
		return childId;
	}

	public void setChildId(String childId) {
		this.childId = childId == null ? null : childId.trim();
	}

	public Short getCategory() {
		return category;
	}

	public void setCategory(Short category) {
		this.category = category;
	}

	public String getObservation() {
		return observation;
	}

	public void setObservation(String observation) {
		this.observation = observation == null ? null : observation.trim();
	}

	public Date getEvaluationDate() {
		return evaluationDate;
	}

	public void setEvaluationDate(Date evaluationDate) {
		this.evaluationDate = evaluationDate;
	}

	public String getLearning() {
		return learning;
	}

	public void setLearning(String learning) {
		this.learning = learning == null ? null : learning.trim();
	}

	public String getObjective() {
		return objective;
	}

	public void setObjective(String objective) {
		this.objective = objective == null ? null : objective.trim();
	}

	public String getEducatorExperience() {
		return educatorExperience;
	}

	public void setEducatorExperience(String educatorExperience) {
		this.educatorExperience = educatorExperience == null ? null : educatorExperience.trim();
	}

	public Date getDay() {
		return day;
	}

	public void setDay(Date day) {
		this.day = day;
	}

	public String getNewsfeedId() {
		return newsfeedId;
	}

	public void setNewsfeedId(String newsfeedId) {
		this.newsfeedId = newsfeedId == null ? null : newsfeedId.trim();
	}

	public Short getState() {
		return state;
	}

	public void setState(Short state) {
		this.state = state;
	}

	public String getCreateAccountId() {
		return createAccountId;
	}

	public void setCreateAccountId(String createAccountId) {
		this.createAccountId = createAccountId == null ? null : createAccountId.trim();
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getUpdateAccountId() {
		return updateAccountId;
	}

	public void setUpdateAccountId(String updateAccountId) {
		this.updateAccountId = updateAccountId == null ? null : updateAccountId.trim();
	}

	public Short getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(Short deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public String getOldId() {
		return oldId;
	}

	public void setOldId(String oldId) {
		this.oldId = oldId == null ? null : oldId.trim();
	}
}