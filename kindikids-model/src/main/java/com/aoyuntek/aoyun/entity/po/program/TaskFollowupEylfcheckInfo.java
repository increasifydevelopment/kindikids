package com.aoyuntek.aoyun.entity.po.program;

import com.aoyuntek.framework.model.BaseModel;

public class TaskFollowupEylfcheckInfo extends BaseModel{
    private String id;

    private String followId;

    private String eylfCheckId;

    private Short value;

    private String reason;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getFollowId() {
        return followId;
    }

    public void setFollowId(String followId) {
        this.followId = followId == null ? null : followId.trim();
    }

    public String getEylfCheckId() {
        return eylfCheckId;
    }

    public void setEylfCheckId(String eylfCheckId) {
        this.eylfCheckId = eylfCheckId == null ? null : eylfCheckId.trim();
    }

    public Short getValue() {
        return value;
    }

    public void setValue(Short value) {
        this.value = value;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason == null ? null : reason.trim();
    }
}