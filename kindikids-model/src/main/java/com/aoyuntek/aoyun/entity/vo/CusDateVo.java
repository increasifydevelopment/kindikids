package com.aoyuntek.aoyun.entity.vo;

import com.aoyuntek.framework.model.BaseModel;

public class CusDateVo extends BaseModel {

    String key;
    String value;

    public CusDateVo(String key, String value) {
        super();
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "CusDate [key=" + key + ", value=" + value + "]";
    }

}
