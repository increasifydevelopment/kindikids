package com.aoyuntek.aoyun.entity.vo.waiting;

public class CountResultVo {
	private boolean isParent;
	private Integer[] counts;

	public boolean isParent() {
		return isParent;
	}

	public void setParent(boolean isParent) {
		this.isParent = isParent;
	}

	public Integer[] getCounts() {
		return counts;
	}

	public void setCounts(Integer[] counts) {
		this.counts = counts;
	}

}
