package com.aoyuntek.aoyun.entity.vo;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.FormAttributeInfo;
import com.aoyuntek.aoyun.entity.po.InstanceAttributeInfo;

@SuppressWarnings("serial")
public class FormAttributeInfoDataVO extends FormAttributeInfo{

	private List<InstanceAttributeInfo> instanceAttributes;

	public List<InstanceAttributeInfo> getInstanceAttributes() {
		return instanceAttributes;
	}

	public void setInstanceAttributes(List<InstanceAttributeInfo> instanceAttributes) {
		this.instanceAttributes = instanceAttributes;
	}
}
