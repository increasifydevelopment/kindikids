package com.aoyuntek.aoyun.entity.vo;

public class MoveFamilyVo {
	private String userId;
	private String familyId;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getFamilyId() {
		return familyId;
	}

	public void setFamilyId(String familyId) {
		this.familyId = familyId;
	}
}
