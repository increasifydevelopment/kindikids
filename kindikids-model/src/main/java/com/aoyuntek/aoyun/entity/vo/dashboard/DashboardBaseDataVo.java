package com.aoyuntek.aoyun.entity.vo.dashboard;

import java.math.BigDecimal;

import com.aoyuntek.framework.model.BaseModel;

public class DashboardBaseDataVo extends BaseModel {
    private Object nqsList;
    private int nqsMaxCount;
    private Object taskCategories;
    private BigDecimal tcMaxCount;
    private Object condition;
    private Object activityList;
    private BigDecimal acMaxCount;
    private Object qipList;
    private BigDecimal qipMaxCount;
    private Object taskProgress;
    private BigDecimal tpMaxCount;

    public Object getActivityList() {
        return activityList;
    }

    public void setActivityList(Object activityList) {
        this.activityList = activityList;
    }

    public BigDecimal getAcMaxCount() {
        return acMaxCount;
    }

    public void setAcMaxCount(BigDecimal acMaxCount) {
        this.acMaxCount = acMaxCount;
    }

    public Object getQipList() {
        return qipList;
    }

    public void setQipList(Object qipList) {
        this.qipList = qipList;
    }

    public BigDecimal getQipMaxCount() {
        return qipMaxCount;
    }

    public void setQipMaxCount(BigDecimal qipMaxCount) {
        this.qipMaxCount = qipMaxCount;
    }

    public DashboardBaseDataVo(Object nqsList, int nqsMaxCount, Object condition) {
        super();
        this.nqsList = nqsList;
        this.nqsMaxCount = nqsMaxCount;
        this.condition = condition;
    }

    public DashboardBaseDataVo(Object nqsList, int nqsMaxCount) {
        super();
        this.nqsList = nqsList;
        this.nqsMaxCount = nqsMaxCount;
    }

    public DashboardBaseDataVo(Object taskCategories, BigDecimal tcMaxCount, Object condition) {
        super();
        this.taskCategories = taskCategories;
        this.tcMaxCount = tcMaxCount;
        this.condition = condition;
    }

    public DashboardBaseDataVo() {
    }

    public Object getCondition() {
        return condition;
    }

    public void setCondition(Object condition) {
        this.condition = condition;
    }

    public Object getNqsList() {
        return nqsList;
    }

    public void setNqsList(Object nqsList) {
        this.nqsList = nqsList;
    }

    public int getNqsMaxCount() {
        return nqsMaxCount;
    }

    public void setNqsMaxCount(int nqsMaxCount) {
        this.nqsMaxCount = nqsMaxCount;
    }

    public Object getTaskCategories() {
        return taskCategories;
    }

    public void setTaskCategories(Object taskCategories) {
        this.taskCategories = taskCategories;
    }

    public BigDecimal getTcMaxCount() {
        return tcMaxCount;
    }

    public void setTcMaxCount(BigDecimal tcMaxCount) {
        this.tcMaxCount = tcMaxCount;
    }

    public Object getTaskProgress() {
        return taskProgress;
    }

    public void setTaskProgress(Object taskProgress) {
        this.taskProgress = taskProgress;
    }

    public BigDecimal getTpMaxCount() {
        return tpMaxCount;
    }

    public void setTpMaxCount(BigDecimal tpMaxCount) {
        this.tpMaxCount = tpMaxCount;
    }

}
