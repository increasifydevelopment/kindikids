package com.aoyuntek.aoyun.entity.po.roster;

import java.util.Date;

import com.aoyuntek.framework.model.BaseModel;

public class TempRosterCenterInfo extends BaseModel {
    private static final long serialVersionUID = 1L;

    private String rosterStaffId;

    private String centreId;

    private String roleId;

    private Date startTime;

    private Date endTime;

    private Boolean deleteFalg;

    private Short state;
    
    public Short getState() {
        return state;
    }

    public void setState(Short state) {
        this.state = state;
    }

    public String getRosterStaffId() {
        return rosterStaffId;
    }

    public void setRosterStaffId(String rosterStaffId) {
        this.rosterStaffId = rosterStaffId == null ? null : rosterStaffId.trim();
    }

    public String getCentreId() {
        return centreId;
    }

    public void setCentreId(String centreId) {
        this.centreId = centreId == null ? null : centreId.trim();
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId == null ? null : roleId.trim();
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Boolean getDeleteFalg() {
        return deleteFalg;
    }

    public void setDeleteFalg(Boolean deleteFalg) {
        this.deleteFalg = deleteFalg;
    }
}