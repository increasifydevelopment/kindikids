package com.aoyuntek.aoyun.entity.vo.task;


/**
 * 
 * @author dlli5 at 2016年10月12日下午1:45:50
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public class TaskFollowUpInfoVo extends TaskInfoVo {

    private static final long serialVersionUID = 1L;
    
    private String followUpId;

    public String getFollowUpId() {
        return followUpId;
    }

    public void setFollowUpId(String followUpId) {
        this.followUpId = followUpId;
    }
}
