package com.aoyuntek.aoyun.entity.po;

public class ClosurePeriodCentres {
    private String closurePeriodId;

    private String centreId;

    private Boolean deleteFlag;

    public String getClosurePeriodId() {
        return closurePeriodId;
    }

    public void setClosurePeriodId(String closurePeriodId) {
        this.closurePeriodId = closurePeriodId == null ? null : closurePeriodId.trim();
    }

    public String getCentreId() {
        return centreId;
    }

    public void setCentreId(String centreId) {
        this.centreId = centreId == null ? null : centreId.trim();
    }

    public Boolean getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Boolean deleteFlag) {
        this.deleteFlag = deleteFlag;
    }
}