package com.aoyuntek.aoyun.entity.vo.task;

import com.aoyuntek.aoyun.entity.vo.SelecterPo;

/**
 * 
 * @author dlli5 at 2016年9月21日上午8:32:47
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public class PersonSelectVo extends SelecterPo {

    private static final long serialVersionUID = 1L;

    public PersonSelectVo(short type, String id, String text) {
        super(id + "," + type, text);
        this.type = type;
    }

    public short type;

    public short getType() {
        return type;
    }

    public void setType(short type) {
        this.type = type;
    }

    public PersonSelectVo() {

    }
}
