/**
 * 
 */
package com.aoyuntek.aoyun.entity.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.aoyuntek.aoyun.condtion.AttendanceCondtion;
import com.aoyuntek.framework.model.BaseModel;

/**
 * @author Administrator
 *
 */
public class CalendarListVo  implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<CalendarDayVo> weekDays;
	private AttendanceCondtion condition;
	private List<LeaveListVo> leave=new  ArrayList<LeaveListVo>();
	
	public List<LeaveListVo> getLeave() {
		return leave;
	}

	public void setLeave(List<LeaveListVo> leave) {
		this.leave = leave;
	}

	public CalendarListVo() {
		super();
	}

	public CalendarListVo(List<CalendarDayVo> weekDays, AttendanceCondtion condition,List<LeaveListVo> leave) {
		super();
		this.weekDays = weekDays;
		this.condition = condition;
		this.leave = leave;
	}
	
	public List<CalendarDayVo> getWeekDays() {
		return weekDays;
	}
	public void setWeekDays(List<CalendarDayVo> weekDays) {
		this.weekDays = weekDays;
	}
	public AttendanceCondtion getCondition() {
		return condition;
	}
	public void setCondition(AttendanceCondtion condition) {
		this.condition = condition;
	}
	
}
