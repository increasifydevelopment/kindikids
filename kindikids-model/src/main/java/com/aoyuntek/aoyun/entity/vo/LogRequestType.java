package com.aoyuntek.aoyun.entity.vo;

public enum LogRequestType {
    GiveNoteRequest((short) 0, "离园申请"),
    AbsenteeRequest((short) 1, "请假申请"),
    ChangeRoom((short) 2, "转Room"),
    ChangeCentre((short) 3, "转Center"),
    ChangeAttendance((short)4,"ChangeAttendance");

    private short value;
    private String name;

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    private LogRequestType(short value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
