package com.aoyuntek.aoyun.entity.vo;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.aoyuntek.aoyun.entity.po.CentersInfo;
import com.aoyuntek.aoyun.entity.po.WeekNutrionalMenuInfo;
import com.aoyuntek.framework.deserializer.S3JsonSerializer;
import com.aoyuntek.framework.model.BaseModel;

/**
 * 
 * @description 园区信息
 * @author gfwang
 * @create 2016年8月19日上午11:07:27
 * @version 1.0
 */
public class CenterInfoVo extends BaseModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 园区信息
	 */
	private CentersInfo center;

	/**
	 * 园区图片
	 */
	private FileVo image;

	/**
	 * 园长名称
	 */
	private String centerManagerName;

	/**
	 * 园长头像
	 */
	private String centerManegerAvater;

	/**
	 * 园长颜色
	 */
	private String personColor;

	private List<UserAvatarVo> list = new ArrayList<UserAvatarVo>();

	public List<UserAvatarVo> getList() {
		return list;
	}

	public void setList(List<UserAvatarVo> list) {
		this.list = list;
	}

	public String getPersonColor() {
		return personColor;
	}

	public void setPersonColor(String personColor) {
		this.personColor = personColor;
	}

	/**
	 * roomList
	 */
	private List<RoomInfoVo> roomList;

	/**
	 * weekMenuList
	 */
	private List<WeekNutrionalMenuInfo> weekMenuList;

	public CenterInfoVo() {

	}

	public CenterInfoVo(CentersInfo center, List<RoomInfoVo> roomList) {
		super();
		this.center = center;
		this.roomList = roomList;
	}

	public CenterInfoVo(CentersInfo center, List<RoomInfoVo> roomList, List<WeekNutrionalMenuInfo> weekMenuList) {
		super();
		this.center = center;
		this.roomList = roomList;
		this.weekMenuList = weekMenuList;
	}

	public List<WeekNutrionalMenuInfo> getWeekMenuList() {
		return weekMenuList;
	}

	public void setWeekMenuList(List<WeekNutrionalMenuInfo> weekMenuList) {
		this.weekMenuList = weekMenuList;
	}

	public CentersInfo getCenter() {
		return center;
	}

	public void setCenter(CentersInfo center) {
		this.center = center;
	}

	public List<RoomInfoVo> getRoomList() {
		return roomList;
	}

	public void setRoomList(List<RoomInfoVo> roomList) {
		this.roomList = roomList;
	}

	public String getCenterManagerName() {
		return centerManagerName;
	}

	public void setCenterManagerName(String centerManagerName) {
		this.centerManagerName = centerManagerName;
	}

	@JsonSerialize(using = S3JsonSerializer.class)
	public String getCenterManegerAvater() {
		return centerManegerAvater;
	}

	public void setCenterManegerAvater(String centerManegerAvater) {
		this.centerManegerAvater = centerManegerAvater;
	}

	public FileVo getImage() {
		return image;
	}

	public void setImage(FileVo image) {
		this.image = image;
	}

}
