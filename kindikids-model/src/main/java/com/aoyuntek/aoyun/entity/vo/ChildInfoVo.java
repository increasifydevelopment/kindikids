package com.aoyuntek.aoyun.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aoyuntek.aoyun.entity.po.AccountInfo;
import com.aoyuntek.aoyun.entity.po.ChildAttendanceInfo;
import com.aoyuntek.aoyun.entity.po.ChildCustodyArrangeInfo;
import com.aoyuntek.aoyun.entity.po.ChildDietaryRequireInfo;
import com.aoyuntek.aoyun.entity.po.ChildHealthMedicalDetailInfo;
import com.aoyuntek.aoyun.entity.po.ChildHealthMedicalInfo;
import com.aoyuntek.aoyun.entity.po.ChildMedicalInfo;
import com.aoyuntek.aoyun.entity.po.EmailMsg;
import com.aoyuntek.aoyun.entity.po.EmergencyContactInfo;
import com.aoyuntek.aoyun.entity.po.EmergencySignatureInfo;
import com.aoyuntek.aoyun.entity.po.PreviousEnrolment;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.aoyun.entity.vo.child.HistoryVo;
import com.aoyuntek.framework.model.BaseModel;

/**
 * 
 * @author dlli5 at 2016年8月18日下午4:27:12
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public class ChildInfoVo extends BaseModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 紧急联系人签名
	 */
	private EmergencySignatureInfo emergencySignatureInfo;

	/**
	 * UserInfo
	 */
	private UserInfo userInfo = new UserInfo();

	/**
	 * AccountInfo
	 */
	private AccountInfo accountInfo = new AccountInfo();

	/**
	 * 监护安排
	 */
	private ChildCustodyArrangeInfo custodyArrange = new ChildCustodyArrangeInfo();

	/**
	 * 紧急联系人
	 */
	private List<EmergencyContactInfo> emergencyContactList = new ArrayList<EmergencyContactInfo>();

	/**
	 * 健康和医疗信息
	 */
	private ChildHealthMedicalInfo healthMedical = new ChildHealthMedicalInfo();

	/**
	 * 健康和医疗信息
	 */
	private List<ChildHealthMedicalDetailInfo> healthMedicalDetailList;
	/**
	 * 背景信息
	 */
	private ChildBackgroundInfoVo background;

	/**
	 * 饮食要求
	 */
	private ChildDietaryRequireInfo dietaryRequire = new ChildDietaryRequireInfo();

	/**
	 * 药物
	 */
	private ChildMedicalInfo medical = new ChildMedicalInfo();

	/**
	 * 小孩attendance 上课信息
	 */
	private ChildAttendanceInfo attendance = new ChildAttendanceInfo();

	/**
	 * 请求历史记录VO
	 */
	private List<RequestLogVo> requestLogVos = new ArrayList<RequestLogVo>();

	/**
	 * Attendance 变更记录
	 */
	private HistoryVo attendanceHistoryInfos = new HistoryVo();

	private Boolean coverOld;

	private Boolean overFull;

	private String profileId;

	private String json;

	private List<EmailMsg> emailMsgList;

	private short plan;

	List<PreviousEnrolment> enrolments;

	public List<PreviousEnrolment> getEnrolments() {
		return enrolments;
	}

	public void setEnrolments(List<PreviousEnrolment> enrolments) {
		this.enrolments = enrolments;
	}

	public short getPlan() {
		return plan;
	}

	public void setPlan(short plan) {
		this.plan = plan;
	}

	public List<EmailMsg> getEmailMsgList() {
		return emailMsgList;
	}

	public void setEmailMsgList(List<EmailMsg> emailMsgList) {
		this.emailMsgList = emailMsgList;
	}

	public String getJson() {
		return json;
	}

	public void setJson(String json) {
		this.json = json;
	}

	public String getProfileId() {
		return profileId;
	}

	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	public Boolean getCoverOld() {
		return coverOld;
	}

	public void setCoverOld(Boolean coverOld) {
		this.coverOld = coverOld;
	}

	public Boolean getOverFull() {
		return overFull;
	}

	public void setOverFull(Boolean overFull) {
		this.overFull = overFull;
	}

	public List<ChildHealthMedicalDetailInfo> getHealthMedicalDetailList() {
		return healthMedicalDetailList;
	}

	public void setHealthMedicalDetailList(List<ChildHealthMedicalDetailInfo> healthMedicalDetailList) {
		this.healthMedicalDetailList = healthMedicalDetailList;
	}

	public ChildCustodyArrangeInfo getCustodyArrange() {
		return custodyArrange;
	}

	public void setCustodyArrange(ChildCustodyArrangeInfo custodyArrange) {
		this.custodyArrange = custodyArrange == null ? new ChildCustodyArrangeInfo() : custodyArrange;
	}

	public List<EmergencyContactInfo> getEmergencyContactList() {
		return emergencyContactList;
	}

	public void setEmergencyContactList(List<EmergencyContactInfo> emergencyContactList) {
		this.emergencyContactList = emergencyContactList;
	}

	public ChildHealthMedicalInfo getHealthMedical() {
		return healthMedical;
	}

	public void setHealthMedical(ChildHealthMedicalInfo healthMedical) {
		this.healthMedical = healthMedical == null ? new ChildHealthMedicalInfo() : healthMedical;
	}

	public ChildBackgroundInfoVo getBackground() {
		return background;
	}

	public void setBackground(ChildBackgroundInfoVo background) {
		this.background = background;
	}

	public ChildDietaryRequireInfo getDietaryRequire() {
		return dietaryRequire;
	}

	public void setDietaryRequire(ChildDietaryRequireInfo dietaryRequire) {
		this.dietaryRequire = dietaryRequire;
	}

	public ChildMedicalInfo getMedical() {
		return medical;
	}

	public void setMedical(ChildMedicalInfo medical) {
		this.medical = medical;
	}

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	public AccountInfo getAccountInfo() {
		return accountInfo;
	}

	public void setAccountInfo(AccountInfo accountInfo) {
		this.accountInfo = accountInfo;
	}

	public ChildAttendanceInfo getAttendance() {
		return attendance;
	}

	public void setAttendance(ChildAttendanceInfo attendance) {
		this.attendance = attendance;
	}

	public List<RequestLogVo> getRequestLogVos() {
		return requestLogVos;
	}

	public void setRequestLogVos(List<RequestLogVo> requestLogVos) {
		this.requestLogVos = requestLogVos;
	}

	public HistoryVo getAttendanceHistoryInfos() {
		return attendanceHistoryInfos;
	}

	public void setAttendanceHistoryInfos(HistoryVo attendanceHistoryInfos) {
		this.attendanceHistoryInfos = attendanceHistoryInfos;
	}

	public EmergencySignatureInfo getEmergencySignatureInfo() {
		return emergencySignatureInfo;
	}

	public void setEmergencySignatureInfo(EmergencySignatureInfo emergencySignatureInfo) {
		this.emergencySignatureInfo = emergencySignatureInfo;
	}

}
