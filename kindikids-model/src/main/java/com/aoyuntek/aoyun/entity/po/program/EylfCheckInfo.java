package com.aoyuntek.aoyun.entity.po.program;

import com.aoyuntek.framework.model.BaseModel;

public class EylfCheckInfo extends BaseModel {

    private static final long serialVersionUID = 1L;

    private String id;

    private String content;

    private String parentNode;

    private Integer ageScope;

    private Short deleteFlag;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public String getParentNode() {
        return parentNode;
    }

    public void setParentNode(String parentNode) {
        this.parentNode = parentNode;
    }

    public Integer getAgeScope() {
        return ageScope;
    }

    public void setAgeScope(Integer ageScope) {
        this.ageScope = ageScope;
    }

    public Short getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Short deleteFlag) {
        this.deleteFlag = deleteFlag;
    }
}