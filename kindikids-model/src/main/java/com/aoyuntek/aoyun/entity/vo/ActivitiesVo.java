package com.aoyuntek.aoyun.entity.vo;

import com.aoyuntek.framework.model.BaseModel;

public class ActivitiesVo extends BaseModel{
    /**
     * 活动名称
     */
    private String name;
    /**
     * 可操作标识
     */
    private boolean operation;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isOperation() {
        return operation;
    }

    public void setOperation(boolean operation) {
        this.operation = operation;
    }

}
