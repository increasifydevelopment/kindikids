package com.aoyuntek.aoyun.entity.vo.policy;

import java.util.ArrayList;
import java.util.List;

import com.aoyuntek.aoyun.entity.po.policy.PolicyCategoriesInfo;
import com.aoyuntek.aoyun.entity.po.policy.PolicyInfo;
import com.aoyuntek.aoyun.entity.po.policy.PolicyRoleRelationInfo;
import com.aoyuntek.aoyun.entity.vo.NqsVo;

/**
 * @description 附件信息
 * @author abliu 2016年10月12日
 * @version 1.0
 */
public class PolicyInfoVo extends PolicyInfo {

	private static final long serialVersionUID = 1L;

	private List<PolicyDocumentVo> policyDocList=new ArrayList<PolicyDocumentVo>();
	
	private List<PolicyRoleRelationInfo> policyVisibility=new ArrayList<PolicyRoleRelationInfo>();
	
	private List<PolicyCategoriesInfo> policyCategoriesSrcList=new ArrayList<PolicyCategoriesInfo>();
	
	private List<NqsVo> nqsInfo=new ArrayList<NqsVo>(); 
	
	private String updateName;	
	
	public List<PolicyCategoriesInfo> getPolicyCategoriesSrcList() {
		return policyCategoriesSrcList;
	}
	
	public String getUpdateName() {
		return updateName;
	}
	
	public void setUpdateName(String updateName) {
		this.updateName = updateName;
	}

	public void setPolicyCategoriesSrcList(List<PolicyCategoriesInfo> policyCategoriesSrcList) {
		this.policyCategoriesSrcList = policyCategoriesSrcList;
	}	
	
	public List<PolicyDocumentVo> getPolicyDocList() {
		return policyDocList;
	}

	public void setPolicyDocList(List<PolicyDocumentVo> policyDocList) {
		this.policyDocList = policyDocList;
	}

	public List<PolicyRoleRelationInfo> getPolicyVisibility() {
		return policyVisibility;
	}

	public void setPolicyVisibility(List<PolicyRoleRelationInfo> policyVisibility) {
		this.policyVisibility = policyVisibility;
	}

	public List<NqsVo> getNqsInfo() {
		return nqsInfo;
	}

	public void setNqsInfo(List<NqsVo> nqsInfo) {
		this.nqsInfo = nqsInfo;
	}	
}
