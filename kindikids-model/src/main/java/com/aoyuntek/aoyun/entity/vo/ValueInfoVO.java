package com.aoyuntek.aoyun.entity.vo;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.InstanceAttributeInfo;
import com.aoyuntek.aoyun.entity.po.InstanceInfo;
import com.aoyuntek.aoyun.entity.po.ValueSelectInfo;
import com.aoyuntek.aoyun.entity.po.ValueStringInfo;
import com.aoyuntek.aoyun.entity.po.ValueTableInfo;
import com.aoyuntek.aoyun.entity.po.ValueTextInfo;

@SuppressWarnings("serial")
public class ValueInfoVO extends InstanceInfo{
	private List<ValueStringInfo> valueStringInfo;
	private List<ValueTableInfo> valueTableInfo;
	private List<ValueTextInfo> valueTextInfo;
	private List<ValueSelectInfo> valueSelectInfo;
	private List<InstanceAttributeInfo> instanceAttrInfo;
	private List<String> addFiles;
	private List<String> removeFiles;

	public List<ValueStringInfo> getValueStringInfo() {
		return valueStringInfo;
	}
	public void setValueStringInfo(List<ValueStringInfo> valueStringInfo) {
		this.valueStringInfo = valueStringInfo;
	}
	public List<ValueTableInfo> getValueTableInfo() {
		return valueTableInfo;
	}
	public void setValueTableInfo(List<ValueTableInfo> valueTableInfo) {
		this.valueTableInfo = valueTableInfo;
	}
	public List<ValueTextInfo> getValueTextInfo() {
		return valueTextInfo;
	}
	public void setValueTextInfo(List<ValueTextInfo> valueTextInfo) {
		this.valueTextInfo = valueTextInfo;
	}
	public List<ValueSelectInfo> getValueSelectInfo() {
		return valueSelectInfo;
	}
	public void setValueSelectInfo(List<ValueSelectInfo> valueSelectInfo) {
		this.valueSelectInfo = valueSelectInfo;
	}
	public List<InstanceAttributeInfo> getInstanceAttrInfo() {
		return instanceAttrInfo;
	}
	public void setInstanceAttrInfo(List<InstanceAttributeInfo> instanceAttrInfo) {
		this.instanceAttrInfo = instanceAttrInfo;
	}
	public List<String> getAddFiles() {
		return addFiles;
	}
	public void setAddFiles(List<String> addFiles) {
		this.addFiles = addFiles;
	}
	public List<String> getRemoveFiles() {
		return removeFiles;
	}
	public void setRemoveFiles(List<String> removeFiles) {
		this.removeFiles = removeFiles;
	}
}
