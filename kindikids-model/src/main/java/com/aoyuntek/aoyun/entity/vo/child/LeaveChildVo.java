package com.aoyuntek.aoyun.entity.vo.child;

import com.aoyuntek.framework.model.BaseModel;

public class LeaveChildVo extends BaseModel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String centerId;

    private String centreName;

    private String childNames;

    public String getCenterId() {
        return centerId;
    }

    public void setCenterId(String centerId) {
        this.centerId = centerId;
    }

    public String getCentreName() {
        return centreName;
    }

    public void setCentreName(String centreName) {
        this.centreName = centreName;
    }

    public String getChildNames() {
        return childNames;
    }

    public void setChildNames(String childNames) {
        this.childNames = childNames;
    }

}
