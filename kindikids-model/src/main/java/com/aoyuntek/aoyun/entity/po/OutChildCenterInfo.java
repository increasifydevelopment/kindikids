package com.aoyuntek.aoyun.entity.po;

import java.util.UUID;

public class OutChildCenterInfo {
    private String id;

    private String childUserId;

    private String centerId;
    
    public OutChildCenterInfo() {

    }

    public OutChildCenterInfo(String childUserId, String centerId) {
        this.id = UUID.randomUUID().toString();
        this.childUserId = childUserId;
        this.centerId = centerId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getChildUserId() {
        return childUserId;
    }

    public void setChildUserId(String childUserId) {
        this.childUserId = childUserId == null ? null : childUserId.trim();
    }

    public String getCenterId() {
        return centerId;
    }

    public void setCenterId(String centerId) {
        this.centerId = centerId == null ? null : centerId.trim();
    }
}