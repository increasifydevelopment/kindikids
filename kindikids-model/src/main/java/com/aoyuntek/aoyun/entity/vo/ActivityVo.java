package com.aoyuntek.aoyun.entity.vo;

import com.aoyuntek.framework.model.BaseModel;

/**
 * @description 活动管理VO实体
 * @author Hxzhang 2016年9月20日下午8:48:47
 * @version 1.0
 */
public class ActivityVo extends BaseModel {
    /**
     * 序列化
     */
    private static final long serialVersionUID = 1L;
    /**
     * 活动ID
     */
    private String activityId;
    /**
     * 小孩的全名
     */
    private String fullName;
    /**
     * 带入到Task活动
     */
    private String educatorExperience;
    /**
     * 是否是followup
     */
    private boolean isFollowup;

    public boolean isFollowup() {
        return isFollowup;
    }

    public void setFollowup(boolean isFollowup) {
        this.isFollowup = isFollowup;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEducatorExperience() {
        return educatorExperience;
    }

    public void setEducatorExperience(String educatorExperience) {
        this.educatorExperience = educatorExperience;
    }

}
