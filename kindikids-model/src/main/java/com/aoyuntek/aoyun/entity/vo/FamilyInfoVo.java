package com.aoyuntek.aoyun.entity.vo;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.FamilyInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;

public class FamilyInfoVo extends FamilyInfo {
    /**
     * 序列化
     */
    private static final long serialVersionUID = 1L;
    /**
     * ChildInfoVo
     */
    private List<ChildInfoVo> childList;
    /**
     * UserInfoVo
     */
    private List<UserInfo> parentList;
    /**
     * 家庭信息
     */
    private String familyName;

    public List<ChildInfoVo> getChildList() {
        return childList;
    }

    public void setChildList(List<ChildInfoVo> childList) {
        this.childList = childList;
    }

    public List<UserInfo> getParentList() {
        return parentList;
    }

    public void setParentList(List<UserInfo> parentList) {
        this.parentList = parentList;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

}
