package com.aoyuntek.aoyun.entity.po.old;

public class ChildSignin {
    private String sign_date;
    private Short type;
    private Short state;
    private String sign_in_name;
    private Boolean sunscreen_app;
    private String room_oldid;
    private String child_oldId;
    private String centre_oldid;
    private String sign_out_name;
    private String create_time;
    private String create_account_id;
    private String logChildOldId;

    public String getLogChildOldId() {
        return logChildOldId;
    }

    public void setLogChildOldId(String logChildOldId) {
        this.logChildOldId = logChildOldId;
    }

    public String getSign_date() {
        return sign_date;
    }

    public void setSign_date(String sign_date) {
        this.sign_date = sign_date;
    }

    public Short getType() {
        return type;
    }

    public void setType(Short type) {
        this.type = type;
    }

    public Short getState() {
        return state;
    }

    public void setState(Short state) {
        this.state = state;
    }

    public String getSign_in_name() {
        return sign_in_name;
    }

    public void setSign_in_name(String sign_in_name) {
        this.sign_in_name = sign_in_name;
    }

    public Boolean getSunscreen_app() {
        return sunscreen_app;
    }

    public void setSunscreen_app(Boolean sunscreen_app) {
        this.sunscreen_app = sunscreen_app;
    }

    public String getRoom_oldid() {
        return room_oldid;
    }

    public void setRoom_oldid(String room_oldid) {
        this.room_oldid = room_oldid;
    }

    public String getChild_oldId() {
        return child_oldId;
    }

    public void setChild_oldId(String child_oldId) {
        this.child_oldId = child_oldId;
    }

    public String getCentre_oldid() {
        return centre_oldid;
    }

    public void setCentre_oldid(String centre_oldid) {
        this.centre_oldid = centre_oldid;
    }

    public String getSign_out_name() {
        return sign_out_name;
    }

    public void setSign_out_name(String sign_out_name) {
        this.sign_out_name = sign_out_name;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getCreate_account_id() {
        return create_account_id;
    }

    public void setCreate_account_id(String create_account_id) {
        this.create_account_id = create_account_id;
    }
}
