package com.aoyuntek.aoyun.entity.vo;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.NewsCommentInfo;
import com.aoyuntek.aoyun.entity.po.NewsInfo;
import com.aoyuntek.framework.model.BaseModel;

public class NewsInfoVo extends BaseModel {

    /**
     * 序列化
     */
    private static final long serialVersionUID = 1L;

    /**
     * 微博信息
     */
    private NewsInfo newsInfo;

    /**
     * 评论信息
     */
    private List<NewsCommentInfo> newsCommentInfoList;

    public NewsInfo getNewsInfo() {
        return newsInfo;
    }

    public void setNewsInfo(NewsInfo newsInfo) {
        this.newsInfo = newsInfo;
    }

    public List<NewsCommentInfo> getNewsCommentInfoList() {
        return newsCommentInfoList;
    }

    public void setNewsCommentInfoList(List<NewsCommentInfo> newsCommentInfoList) {
        this.newsCommentInfoList = newsCommentInfoList;
    }

}
