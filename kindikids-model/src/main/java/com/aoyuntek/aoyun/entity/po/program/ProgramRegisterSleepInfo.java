package com.aoyuntek.aoyun.entity.po.program;

import java.util.Date;

import com.aoyuntek.framework.model.BaseModel;


public class ProgramRegisterSleepInfo extends BaseModel{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String registerId;

    private Date sleepTime;

    private Date wokeupTime;

    public String getRegisterId() {
        return registerId;
    }

    public void setRegisterId(String registerId) {
        this.registerId = registerId == null ? null : registerId.trim();
    }

    public Date getSleepTime() {
        return sleepTime;
    }

    public void setSleepTime(Date sleepTime) {
        this.sleepTime = sleepTime;
    }

    public Date getWokeupTime() {
        return wokeupTime;
    }

    public void setWokeupTime(Date wokeupTime) {
        this.wokeupTime = wokeupTime;
    }
}