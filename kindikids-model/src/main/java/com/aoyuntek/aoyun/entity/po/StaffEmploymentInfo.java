package com.aoyuntek.aoyun.entity.po;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aoyuntek.aoyun.entity.po.roster.RosterStaffInfo;

public class StaffEmploymentInfo {

	private Integer confirm = 0;

	private Integer confirm2 = 0;

	private String id;

	private String accountId;

	private Boolean monday = false;

	private String monStartTime;

	private String monEndTime;

	private BigDecimal monHours;

	private Boolean tuesday = false;

	private String tueStartTime;

	private String tueEndTime;

	private BigDecimal tueHours;

	private Boolean wednesday = false;

	private String wedStartTime;

	private String wedEndTime;

	private BigDecimal wedHours;

	private Boolean thursday = false;

	private String thuStartTime;

	private String thuEndTime;

	private BigDecimal thuHours;

	private Boolean friday = false;

	private String friStartTime;

	private String friEndTime;

	private BigDecimal friHours;

	private Date lastDay;

	private String primaryRole;

	private Short employmentType;

	private Short region;

	private String centreId;

	private String roomId;

	private String groupId;

	private Date effectiveDate;

	private Short staffType;

	private Short status;

	private Date createTime;

	private String createAccountId;

	private Boolean current;

	private Boolean deleteFlag;

	private List<String> tags = new ArrayList<String>();

	private List<RosterStaffInfo> rosterStaffInfos = new ArrayList<RosterStaffInfo>();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id == null ? null : id.trim();
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId == null ? null : accountId.trim();
	}

	public Boolean getMonday() {
		return monday;
	}

	public void setMonday(Boolean monday) {
		this.monday = monday == null ? false : monday;
	}

	public String getMonStartTime() {
		return monStartTime;
	}

	public void setMonStartTime(String monStartTime) {
		this.monStartTime = monStartTime == null ? null : monStartTime.trim();
	}

	public String getMonEndTime() {
		return monEndTime;
	}

	public void setMonEndTime(String monEndTime) {
		this.monEndTime = monEndTime == null ? null : monEndTime.trim();
	}

	public BigDecimal getMonHours() {
		return monHours;
	}

	public void setMonHours(BigDecimal monHours) {
		this.monHours = monHours;
	}

	public Boolean getTuesday() {
		return tuesday;
	}

	public void setTuesday(Boolean tuesday) {
		this.tuesday = tuesday == null ? false : tuesday;
	}

	public String getTueStartTime() {
		return tueStartTime;
	}

	public void setTueStartTime(String tueStartTime) {
		this.tueStartTime = tueStartTime == null ? null : tueStartTime.trim();
	}

	public String getTueEndTime() {
		return tueEndTime;
	}

	public void setTueEndTime(String tueEndTime) {
		this.tueEndTime = tueEndTime == null ? null : tueEndTime.trim();
	}

	public BigDecimal getTueHours() {
		return tueHours;
	}

	public void setTueHours(BigDecimal tueHours) {
		this.tueHours = tueHours;
	}

	public Boolean getWednesday() {
		return wednesday;
	}

	public void setWednesday(Boolean wednesday) {
		this.wednesday = wednesday == null ? false : wednesday;
	}

	public String getWedStartTime() {
		return wedStartTime;
	}

	public void setWedStartTime(String wedStartTime) {
		this.wedStartTime = wedStartTime == null ? null : wedStartTime.trim();
	}

	public String getWedEndTime() {
		return wedEndTime;
	}

	public void setWedEndTime(String wedEndTime) {
		this.wedEndTime = wedEndTime == null ? null : wedEndTime.trim();
	}

	public BigDecimal getWedHours() {
		return wedHours;
	}

	public void setWedHours(BigDecimal wedHours) {
		this.wedHours = wedHours;
	}

	public Boolean getThursday() {
		return thursday;
	}

	public void setThursday(Boolean thursday) {
		this.thursday = thursday == null ? false : thursday;
	}

	public String getThuStartTime() {
		return thuStartTime;
	}

	public void setThuStartTime(String thuStartTime) {
		this.thuStartTime = thuStartTime == null ? null : thuStartTime.trim();
	}

	public String getThuEndTime() {
		return thuEndTime;
	}

	public void setThuEndTime(String thuEndTime) {
		this.thuEndTime = thuEndTime == null ? null : thuEndTime.trim();
	}

	public BigDecimal getThuHours() {
		return thuHours;
	}

	public void setThuHours(BigDecimal thuHours) {
		this.thuHours = thuHours;
	}

	public Boolean getFriday() {
		return friday;
	}

	public void setFriday(Boolean friday) {
		this.friday = friday == null ? false : friday;
	}

	public String getFriStartTime() {
		return friStartTime;
	}

	public void setFriStartTime(String friStartTime) {
		this.friStartTime = friStartTime == null ? null : friStartTime.trim();
	}

	public String getFriEndTime() {
		return friEndTime;
	}

	public void setFriEndTime(String friEndTime) {
		this.friEndTime = friEndTime == null ? null : friEndTime.trim();
	}

	public BigDecimal getFriHours() {
		return friHours;
	}

	public void setFriHours(BigDecimal friHours) {
		this.friHours = friHours;
	}

	public Date getLastDay() {
		return lastDay;
	}

	public void setLastDay(Date lastDay) {
		this.lastDay = lastDay;
	}

	public String getPrimaryRole() {
		return primaryRole;
	}

	public void setPrimaryRole(String primaryRole) {
		this.primaryRole = primaryRole == null ? null : primaryRole.trim();
	}

	public Short getEmploymentType() {
		return employmentType;
	}

	public void setEmploymentType(Short employmentType) {
		this.employmentType = employmentType;
	}

	public Short getRegion() {
		return region;
	}

	public void setRegion(Short region) {
		this.region = region;
	}

	public String getCentreId() {
		return centreId;
	}

	public void setCentreId(String centreId) {
		this.centreId = centreId == null ? null : centreId.trim();
	}

	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId == null ? null : roomId.trim();
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId == null ? null : groupId.trim();
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Short getStaffType() {
		return staffType;
	}

	public void setStaffType(Short staffType) {
		this.staffType = staffType;
	}

	public Short getStatus() {
		return status;
	}

	public void setStatus(Short status) {
		this.status = status;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCreateAccountId() {
		return createAccountId;
	}

	public void setCreateAccountId(String createAccountId) {
		this.createAccountId = createAccountId == null ? null : createAccountId.trim();
	}

	public Boolean getCurrent() {
		return current;
	}

	public void setCurrent(Boolean current) {
		this.current = current;
	}

	public Boolean getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(Boolean deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public Integer getConfirm() {
		return confirm;
	}

	public void setConfirm(Integer confirm) {
		this.confirm = confirm;
	}

	public List<RosterStaffInfo> getRosterStaffInfos() {
		return rosterStaffInfos;
	}

	public void setRosterStaffInfos(List<RosterStaffInfo> rosterStaffInfos) {
		this.rosterStaffInfos = rosterStaffInfos;
	}

	public Integer getConfirm2() {
		return confirm2;
	}

	public void setConfirm2(Integer confirm2) {
		this.confirm2 = confirm2;
	}
}