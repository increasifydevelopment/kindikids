package com.aoyuntek.aoyun.entity.po.task;

import java.util.Date;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.aoyuntek.framework.deserializer.DateJsonSerializer2;
import com.aoyuntek.framework.model.BaseModel;

public class TaskFrequencyInfo extends BaseModel {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String id;

    private String taskId;

    private Short repeatType;

    private Date startsDate;

    private Short haveEndDate;

    private Date endsDate;

    private String dayToStart;

    private String dayToEnd;

    private Date createTime;

    private String createAccountId;

    private Date updateTime;

    private Date updateAccountId;

    private Short deleteFlag;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId == null ? null : taskId.trim();
    }

    public Short getRepeatType() {
        return repeatType;
    }

    public void setRepeatType(Short repeatType) {
        this.repeatType = repeatType;
    }

    @JsonSerialize(using = DateJsonSerializer2.class, include = JsonSerialize.Inclusion.ALWAYS)
    public Date getStartsDate() {
        return startsDate;
    }

    public void setStartsDate(Date startsDate) {
        this.startsDate = startsDate;
    }

    public Short getHaveEndDate() {
        return haveEndDate;
    }

    public void setHaveEndDate(Short haveEndDate) {
        this.haveEndDate = haveEndDate;
    }

    @JsonSerialize(using = DateJsonSerializer2.class, include = JsonSerialize.Inclusion.ALWAYS)
    public Date getEndsDate() {
        return endsDate;
    }

    public void setEndsDate(Date endsDate) {
        this.endsDate = endsDate;
    }

    public String getDayToStart() {
        return dayToStart;
    }

    public void setDayToStart(String dayToStart) {
        this.dayToStart = dayToStart == null ? null : dayToStart.trim();
    }

    public String getDayToEnd() {
        return dayToEnd;
    }

    public void setDayToEnd(String dayToEnd) {
        this.dayToEnd = dayToEnd == null ? null : dayToEnd.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateAccountId() {
        return createAccountId;
    }

    public void setCreateAccountId(String createAccountId) {
        this.createAccountId = createAccountId == null ? null : createAccountId.trim();
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getUpdateAccountId() {
        return updateAccountId;
    }

    public void setUpdateAccountId(Date updateAccountId) {
        this.updateAccountId = updateAccountId;
    }

    public Short getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Short deleteFlag) {
        this.deleteFlag = deleteFlag;
    }
}