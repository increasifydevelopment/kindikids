package com.aoyuntek.aoyun.entity.po;

@SuppressWarnings("serial")
public class InstanceAttributeInfo extends ValueInfo {

    private Short type;

    private Integer value;

    public Short getType() {
        return type;
    }

    public void setType(Short type) {
        this.type = type;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}