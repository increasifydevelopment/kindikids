package com.aoyuntek.aoyun.entity.vo.dashboard;

import java.math.BigDecimal;

import com.aoyuntek.framework.model.BaseModel;

public class DashboardActivityVo extends BaseModel{
    
    private Object activityList;
    private BigDecimal maxCount;

    public Object getActivityList() {
        return activityList;
    }

    public void setActivityList(Object activityList) {
        this.activityList = activityList;
    }

    public BigDecimal getMaxCount() {
        return maxCount;
    }

    public void setMaxCount(BigDecimal maxCount) {
        this.maxCount = maxCount;
    }

}
