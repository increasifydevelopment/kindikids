package com.aoyuntek.aoyun.entity.po.roster;

import java.util.Date;

import com.aoyuntek.framework.model.BaseModel;

public class RosterLeaveItemInfo extends BaseModel {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String id;

    private String rosterLeaveId;

    private Date beginAbsenceDate;

    private Date endAbsenceDate;

    private String absenceReason;

    private Date createTime;

    private Date updateTime;

    private String createAccountId;

    private String updateAccountId;

    private Short deleteFlag;

    private Short type;

    public Short getType() {
        return type;
    }

    public void setType(Short type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getRosterLeaveId() {
        return rosterLeaveId;
    }

    public void setRosterLeaveId(String rosterLeaveId) {
        this.rosterLeaveId = rosterLeaveId == null ? null : rosterLeaveId.trim();
    }

    public Date getBeginAbsenceDate() {
        return beginAbsenceDate;
    }

    public void setBeginAbsenceDate(Date beginAbsenceDate) {
        this.beginAbsenceDate = beginAbsenceDate;
    }

    public Date getEndAbsenceDate() {
        return endAbsenceDate;
    }

    public void setEndAbsenceDate(Date endAbsenceDate) {
        this.endAbsenceDate = endAbsenceDate;
    }

    public String getAbsenceReason() {
        return absenceReason;
    }

    public void setAbsenceReason(String absenceReason) {
        this.absenceReason = absenceReason == null ? null : absenceReason.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getCreateAccountId() {
        return createAccountId;
    }

    public void setCreateAccountId(String createAccountId) {
        this.createAccountId = createAccountId == null ? null : createAccountId.trim();
    }

    public String getUpdateAccountId() {
        return updateAccountId;
    }

    public void setUpdateAccountId(String updateAccountId) {
        this.updateAccountId = updateAccountId == null ? null : updateAccountId.trim();
    }

    public Short getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Short deleteFlag) {
        this.deleteFlag = deleteFlag;
    }
}