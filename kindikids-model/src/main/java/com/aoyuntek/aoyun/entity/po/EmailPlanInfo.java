package com.aoyuntek.aoyun.entity.po;

import java.util.Date;
import java.util.UUID;

public class EmailPlanInfo {
	private String id;

	private String senderaddress;

	private String sendername;

	private String receiveraddress;

	private String receivername;

	private String sub;

	private Date createTime;

	private String objId;

	private Short type;

	private Boolean deleteFlag;

	private String msg;

	private String userId;

	private Short status;

	public Short getStatus() {
		return status;
	}

	public void setStatus(Short status) {
		this.status = status;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id == null ? null : id.trim();
	}

	public String getSenderaddress() {
		return senderaddress;
	}

	public void setSenderaddress(String senderaddress) {
		this.senderaddress = senderaddress == null ? null : senderaddress.trim();
	}

	public String getSendername() {
		return sendername;
	}

	public void setSendername(String sendername) {
		this.sendername = sendername == null ? null : sendername.trim();
	}

	public String getReceiveraddress() {
		return receiveraddress;
	}

	public void setReceiveraddress(String receiveraddress) {
		this.receiveraddress = receiveraddress == null ? null : receiveraddress.trim();
	}

	public String getReceivername() {
		return receivername;
	}

	public void setReceivername(String receivername) {
		this.receivername = receivername == null ? null : receivername.trim();
	}

	public String getSub() {
		return sub;
	}

	public void setSub(String sub) {
		this.sub = sub == null ? null : sub.trim();
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getObjId() {
		return objId;
	}

	public void setObjId(String objId) {
		this.objId = objId == null ? null : objId.trim();
	}

	public Short getType() {
		return type;
	}

	public void setType(Short type) {
		this.type = type;
	}

	public Boolean getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(Boolean deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg == null ? null : msg.trim();
	}

	public EmailPlanInfo() {
		this.id = UUID.randomUUID().toString();
		this.createTime = new Date();
		this.deleteFlag = false;
	}
}