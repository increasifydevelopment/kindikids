package com.aoyuntek.aoyun.entity.vo;

import java.util.Date;
import java.util.List;

import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.framework.model.BaseModel;

/**
 * @description 未激活账户信息的家长Vo
 * @author Hxzhang 2016年8月30日下午2:35:50
 * @version 1.0
 */
public class UnActiveParentVo extends BaseModel {
    /**
     * FamilyId
     */
    private String familyId;
    /**
     * 小孩入园时间
     */
    private Date enrolmentDate;
    /**
     * 未激活的家长
     */
    private List<UserInfo> userList;

    public String getFamilyId() {
        return familyId;
    }

    public void setFamilyId(String familyId) {
        this.familyId = familyId;
    }

    public Date getEnrolmentDate() {
        return enrolmentDate;
    }

    public void setEnrolmentDate(Date enrolmentDate) {
        this.enrolmentDate = enrolmentDate;
    }

    public List<UserInfo> getUserList() {
        return userList;
    }

    public void setUserList(List<UserInfo> userList) {
        this.userList = userList;
    }

}
