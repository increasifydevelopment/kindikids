package com.aoyuntek.aoyun.entity.po;

import java.util.Date;

public class CalendarInfo {
	private Integer id;

	private Date date;

	private Short weekDay;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Short getWeekDay() {
		return weekDay;
	}

	public void setWeekDay(Short weekDay) {
		this.weekDay = weekDay;
	}
}