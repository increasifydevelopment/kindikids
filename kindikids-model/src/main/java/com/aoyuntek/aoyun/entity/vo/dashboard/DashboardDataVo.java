package com.aoyuntek.aoyun.entity.vo.dashboard;

import java.math.BigDecimal;
import java.math.RoundingMode;

import com.aoyuntek.framework.model.BaseModel;

public class DashboardDataVo extends BaseModel {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private BigDecimal percent;

    private BigDecimal totalCount;

    private int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public BigDecimal getPercent() {
        return percent;
    }

    public void setPercent(BigDecimal percent) {
        this.percent = percent.setScale(0, BigDecimal.ROUND_HALF_UP);
    }

    public BigDecimal getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(BigDecimal totalCount) {
        this.totalCount = totalCount;
    }

    public DashboardDataVo(BigDecimal percent, BigDecimal totalCount) {
        super();
        this.percent = percent;
        this.totalCount = totalCount;
    }

    public DashboardDataVo(BigDecimal percent, BigDecimal totalCount, int count) {
        super();
        this.percent = percent;
        this.totalCount = totalCount;
        this.count = count;
    }

    public DashboardDataVo() {
        super();
    }
}
