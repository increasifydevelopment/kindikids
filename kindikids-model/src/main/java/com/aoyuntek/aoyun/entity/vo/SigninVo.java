package com.aoyuntek.aoyun.entity.vo;

import java.util.Date;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.aoyuntek.aoyun.entity.po.SigninInfo;
import com.aoyuntek.framework.deserializer.S3JsonSerializer;

public class SigninVo extends SigninInfo {

	private static final long serialVersionUID = 1L;

	private String name;
	private String lastName;
	private String img;
	private String personColor;
	private boolean leave = false;
	private String replaceId;
	private String absenteeId;
	private boolean sigin;
	private boolean sigout;
	private Date birthday;
	private boolean interim = false;

	public boolean isInterim() {
		return interim;
	}

	public void setInterim(boolean interim) {
		this.interim = interim;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public boolean isSigin() {
		return sigin;
	}

	public void setSigin(boolean sigin) {
		this.sigin = sigin;
	}

	public boolean isSigout() {
		return sigout;
	}

	public void setSigout(boolean sigout) {
		this.sigout = sigout;
	}

	private Short signOutState;

	public Short getSignOutState() {
		return signOutState;
	}

	public void setSignOutState(Short signOutState) {
		this.signOutState = signOutState;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAbsenteeId() {
		return absenteeId;
	}

	public void setAbsenteeId(String absenteeId) {
		this.absenteeId = absenteeId;
	}

	public String getReplaceId() {
		return replaceId;
	}

	public void setReplaceId(String replaceId) {
		this.replaceId = replaceId;
	}

	public boolean isLeave() {
		return leave;
	}

	public void setLeave(boolean leave) {
		this.leave = leave;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonSerialize(using = S3JsonSerializer.class)
	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getPersonColor() {
		return personColor;
	}

	public void setPersonColor(String personColor) {
		this.personColor = personColor;
	}
}
