package com.aoyuntek.aoyun.entity.po;

import java.util.Date;

import com.aoyuntek.framework.model.BaseModel;

public class LeaveInfo extends BaseModel {
	private static final long serialVersionUID = 1L;
	private String leaveTypeStr;
	private String leaveReason;
	private String leaveColor;
	private boolean disabled = true;

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public String getLeaveColor() {
		return leaveColor;
	}

	public void setLeaveColor(String leaveColor) {
		this.leaveColor = leaveColor;
	}

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_leave.id
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	private String id;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_leave.centre_id
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	private String centreId;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_leave.room_id
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	private String roomId;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_leave.account_id
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	private String accountId;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_leave.role_type
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	private String roleType;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_leave.leave_type
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	private Short leaveType;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_leave.start_date
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	private Date startDate;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_leave.end_date
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	private Date endDate;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_leave.status
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	private Short status;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_leave.reason
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	private String reason;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_leave.opera_account_id
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	private String operaAccountId;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_leave.update_time
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	private Date updateTime;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_leave.create_time
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	private Date createTime;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_leave.create_account_id
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	private String createAccountId;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_leave.delete_flag
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	private Short deleteFlag;

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_leave.id
	 * 
	 * @return the value of tbl_leave.id
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	public String getId() {
		return id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_leave.id
	 * 
	 * @param id
	 *            the value for tbl_leave.id
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	public void setId(String id) {
		this.id = id == null ? null : id.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_leave.centre_id
	 * 
	 * @return the value of tbl_leave.centre_id
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	public String getCentreId() {
		return centreId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_leave.centre_id
	 * 
	 * @param centreId
	 *            the value for tbl_leave.centre_id
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	public void setCentreId(String centreId) {
		this.centreId = centreId == null ? null : centreId.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_leave.room_id
	 * 
	 * @return the value of tbl_leave.room_id
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	public String getRoomId() {
		return roomId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_leave.room_id
	 * 
	 * @param roomId
	 *            the value for tbl_leave.room_id
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	public void setRoomId(String roomId) {
		this.roomId = roomId == null ? null : roomId.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_leave.account_id
	 * 
	 * @return the value of tbl_leave.account_id
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	public String getAccountId() {
		return accountId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_leave.account_id
	 * 
	 * @param accountId
	 *            the value for tbl_leave.account_id
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	public void setAccountId(String accountId) {
		this.accountId = accountId == null ? null : accountId.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_leave.role_type
	 * 
	 * @return the value of tbl_leave.role_type
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	public String getRoleType() {
		return roleType;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_leave.role_type
	 * 
	 * @param roleType
	 *            the value for tbl_leave.role_type
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	public void setRoleType(String roleType) {
		this.roleType = roleType == null ? null : roleType.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_leave.leave_type
	 * 
	 * @return the value of tbl_leave.leave_type
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	public Short getLeaveType() {
		return leaveType;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_leave.leave_type
	 * 
	 * @param leaveType
	 *            the value for tbl_leave.leave_type
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	public void setLeaveType(Short leaveType) {
		this.leaveType = leaveType;
		setLeaveTypeStr(String.valueOf(leaveType));
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_leave.start_date
	 * 
	 * @return the value of tbl_leave.start_date
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_leave.start_date
	 * 
	 * @param startDate
	 *            the value for tbl_leave.start_date
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_leave.end_date
	 * 
	 * @return the value of tbl_leave.end_date
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_leave.end_date
	 * 
	 * @param endDate
	 *            the value for tbl_leave.end_date
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_leave.status
	 * 
	 * @return the value of tbl_leave.status
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	public Short getStatus() {
		return status;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_leave.status
	 * 
	 * @param status
	 *            the value for tbl_leave.status
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	public void setStatus(Short status) {
		this.status = status;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_leave.reason
	 * 
	 * @return the value of tbl_leave.reason
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_leave.reason
	 * 
	 * @param reason
	 *            the value for tbl_leave.reason
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	public void setReason(String reason) {
		this.reason = reason == null ? null : reason.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_leave.opera_account_id
	 * 
	 * @return the value of tbl_leave.opera_account_id
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	public String getOperaAccountId() {
		return operaAccountId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_leave.opera_account_id
	 * 
	 * @param operaAccountId
	 *            the value for tbl_leave.opera_account_id
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	public void setOperaAccountId(String operaAccountId) {
		this.operaAccountId = operaAccountId == null ? null : operaAccountId.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_leave.update_time
	 * 
	 * @return the value of tbl_leave.update_time
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	public Date getUpdateTime() {
		return updateTime;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_leave.update_time
	 * 
	 * @param updateTime
	 *            the value for tbl_leave.update_time
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_leave.create_time
	 * 
	 * @return the value of tbl_leave.create_time
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_leave.create_time
	 * 
	 * @param createTime
	 *            the value for tbl_leave.create_time
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_leave.create_account_id
	 * 
	 * @return the value of tbl_leave.create_account_id
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	public String getCreateAccountId() {
		return createAccountId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_leave.create_account_id
	 * 
	 * @param createAccountId
	 *            the value for tbl_leave.create_account_id
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	public void setCreateAccountId(String createAccountId) {
		this.createAccountId = createAccountId == null ? null : createAccountId.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_leave.delete_flag
	 * 
	 * @return the value of tbl_leave.delete_flag
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	public Short getDeleteFlag() {
		return deleteFlag;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_leave.delete_flag
	 * 
	 * @param deleteFlag
	 *            the value for tbl_leave.delete_flag
	 * 
	 * @mbggenerated Mon Oct 10 11:07:05 CST 2016
	 */
	public void setDeleteFlag(Short deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public String getLeaveReason() {
		return leaveReason;
	}

	public void setLeaveReason(String leaveReason) {
		this.leaveReason = leaveReason;
	}

	public String getLeaveTypeStr() {
		return leaveTypeStr;
	}

	public void setLeaveTypeStr(String leaveTypeStr) {
		this.leaveTypeStr = leaveTypeStr;
	}

}