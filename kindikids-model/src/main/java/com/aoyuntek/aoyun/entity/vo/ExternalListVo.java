package com.aoyuntek.aoyun.entity.vo;

import java.util.Date;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.aoyuntek.framework.deserializer.S3JsonSerializer;
import com.aoyuntek.framework.model.BaseModel;

public class ExternalListVo extends BaseModel {
    private static final long serialVersionUID = 1L;
    private String id;
    private String childName;
    private Integer[] liveAge;
    private Date birthday;
    private Short type;
    private String reqDate;
    private Date appDate;
    private Date reqStartDate;
    private String p1Name;
    private String p1Mobile;
    private String p2Name;
    private String p2Mobile;
    private String personColor;
    private String avatar;
    private String familyId;
    private ChildInfoVo childVo;

    public ChildInfoVo getChildVo() {
        return childVo;
    }

    public void setChildVo(ChildInfoVo childVo) {
        this.childVo = childVo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFamilyId() {
        return familyId;
    }

    public void setFamilyId(String familyId) {
        this.familyId = familyId;
    }

    public String getPersonColor() {
        return personColor;
    }

    public void setPersonColor(String personColor) {
        this.personColor = personColor;
    }

    @JsonSerialize(using = S3JsonSerializer.class)
    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getChildName() {
        return childName;
    }

    public void setChildName(String childName) {
        this.childName = childName;
    }

    public Integer[] getLiveAge() {
        return liveAge;
    }

    public void setLiveAge(Integer[] liveAge) {
        this.liveAge = liveAge;
    }

    public Short getType() {
        return type;
    }

    public void setType(Short type) {
        this.type = type;
    }

    public String getReqDate() {
        return reqDate;
    }

    public void setReqDate(String reqDate) {
        this.reqDate = reqDate;
    }

    public Date getAppDate() {
        return appDate;
    }

    public void setAppDate(Date appDate) {
        this.appDate = appDate;
    }

    public Date getReqStartDate() {
        return reqStartDate;
    }

    public void setReqStartDate(Date reqStartDate) {
        this.reqStartDate = reqStartDate;
    }

    public String getP1Name() {
        return p1Name;
    }

    public void setP1Name(String p1Name) {
        this.p1Name = p1Name;
    }

    public String getP1Mobile() {
        return p1Mobile;
    }

    public void setP1Mobile(String p1Mobile) {
        this.p1Mobile = p1Mobile;
    }

    public String getP2Name() {
        return p2Name;
    }

    public void setP2Name(String p2Name) {
        this.p2Name = p2Name;
    }

    public String getP2Mobile() {
        return p2Mobile;
    }

    public void setP2Mobile(String p2Mobile) {
        this.p2Mobile = p2Mobile;
    }
}
