package com.aoyuntek.aoyun.entity.vo;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.aoyuntek.framework.deserializer.S3JsonSerializer;
import com.aoyuntek.framework.model.BaseModel;

public class FileVo extends BaseModel {
    /**
     * 序列化
     */
    private static final long serialVersionUID = 1L;

    /**
     * source id
     */
    private String id;

    /**
     * uuid.*
     */
    private String fileIdName;
    /**
     * 文件名
     */
    private String fileName;
    /**
     * 文件名相对路径temp/uuid.*
     */
    private String relativePathName;
    /**
     * 图片访问路径
     */
    private String visitedUrl;

    public FileVo() {

    }

    public FileVo(String fileName) {
        this.fileName = fileName;
    }

    public FileVo(String id, String fileIdName, String fileName, String relativePathName, String visitedUrl) {
        super();
        this.id = id;
        this.fileIdName = fileIdName;
        this.fileName = fileName;
        this.relativePathName = relativePathName;
        this.visitedUrl = visitedUrl;
    }

    public FileVo(String fileIdName, String fileName, String relativePathName, String visitedUrl) {
        super();
        this.fileIdName = fileIdName;
        this.fileName = fileName;
        this.relativePathName = relativePathName;
        this.visitedUrl = visitedUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFileIdName() {
        return fileIdName;
    }

    public void setFileIdName(String fileIdName) {
        this.fileIdName = fileIdName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getRelativePathName() {
        return relativePathName;
    }

    public void setRelativePathName(String relativePathName) {
        this.relativePathName = relativePathName;
    }

    @JsonSerialize(using = S3JsonSerializer.class)
    public String getVisitedUrl() {
        return getRelativePathName();
    }

    public void setVisitedUrl(String visitedUrl) {
        this.visitedUrl = visitedUrl;
    }

}
