package com.aoyuntek.aoyun.entity.po.program;

import com.aoyuntek.framework.model.BaseModel;

public class ChildEylfInfo extends BaseModel {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String id;

    private String childId;

    private String eylfCheckId;

    private Short value;

    private Integer ageScope;

    private Short deleteFlag;

    private String reason;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getChildId() {
        return childId;
    }

    public void setChildId(String childId) {
        this.childId = childId == null ? null : childId.trim();
    }

    public String getEylfCheckId() {
        return eylfCheckId;
    }

    public void setEylfCheckId(String eylfCheckId) {
        this.eylfCheckId = eylfCheckId == null ? null : eylfCheckId.trim();
    }

    public Short getValue() {
        return value;
    }

    public void setValue(Short value) {
        this.value = value;
    }

    public Integer getAgeScope() {
        return ageScope;
    }

    public void setAgeScope(Integer ageScope) {
        this.ageScope = ageScope;
    }

    public Short getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Short deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason == null ? null : reason.trim();
    }
}