package com.aoyuntek.aoyun.entity.vo;

public class QueuevVo {
	private String id;
	private Integer queue;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getQueue() {
		return queue;
	}

	public void setQueue(Integer queue) {
		this.queue = queue;
	}
}
