package com.aoyuntek.aoyun.entity.po.roster;

import com.aoyuntek.framework.model.BaseModel;

public class ShiftWeekInfo extends BaseModel {
    
    private static final long serialVersionUID = 1L;

    private String id;

    private String shiftTimeId;

    private Short weekNum;

    private String tags;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getShiftTimeId() {
        return shiftTimeId;
    }

    public void setShiftTimeId(String shiftTimeId) {
        this.shiftTimeId = shiftTimeId == null ? null : shiftTimeId.trim();
    }

    public Short getWeekNum() {
        return weekNum;
    }

    public void setWeekNum(Short weekNum) {
        this.weekNum = weekNum;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags == null ? null : tags.trim();
    }
}