package com.aoyuntek.aoyun.entity.po;

import java.util.ArrayList;
import java.util.List;

import com.aoyuntek.aoyun.entity.vo.FileVo;
import com.aoyuntek.framework.model.BaseModel;

public class FileBaseVo extends BaseModel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private List<FileVo> fileList=new ArrayList<FileVo>();

    public List<FileVo> getFileList() {
        return fileList;
    }

    public void setFileList(List<FileVo> fileList) {
        this.fileList = fileList;
    }
}
