package com.aoyuntek.aoyun.entity.vo;

import com.aoyuntek.aoyun.entity.po.ChildLogsInfo;

public class ChildLogsVo extends ChildLogsInfo {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    
    private String createName;
    
    private FileVo file;

    public FileVo getFile() {
        return file;
    }

    public void setFile(FileVo file) {
        this.file = file;
    }

    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }
}
