package com.aoyuntek.aoyun.entity.po;

import com.aoyuntek.framework.model.BaseModel;

@SuppressWarnings("serial")
public class ValueInfo extends BaseModel{
	private String instanceId;

    private String formAttrId;

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public String getFormAttrId() {
		return formAttrId;
	}

	public void setFormAttrId(String formAttrId) {
		this.formAttrId = formAttrId;
	}
}
