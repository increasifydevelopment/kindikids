package com.aoyuntek.aoyun.entity.vo;

import java.util.Date;

import com.aoyuntek.framework.model.BaseModel;

public class LogsDetaileVo extends BaseModel {
    private static final long serialVersionUID = 1L;

    private String id;
    private Date updateTime;
    private String userName;
    private String ipAddress;
    private boolean isAdd;
    private boolean isSuccess;

    private String changeFieldName;
    private String oldValue;
    private String newValue;
    private String tags;

    public LogsDetaileVo() {
    }

    public LogsDetaileVo(String id, String changeFieldName, String oldValue, String newValue, String tags) {
        super();
        this.id = id;
        this.changeFieldName = changeFieldName;
        this.oldValue = oldValue;
        this.newValue = newValue;
        this.tags = tags;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public boolean isAdd() {
        return isAdd;
    }

    public void setAdd(boolean isAdd) {
        this.isAdd = isAdd;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean isSuccess) {
        this.isSuccess = isSuccess;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getChangeFieldName() {
        return changeFieldName;
    }

    public void setChangeFieldName(String changeFieldName) {
        this.changeFieldName = changeFieldName;
    }

    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

}
