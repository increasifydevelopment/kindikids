package com.aoyuntek.aoyun.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aoyuntek.aoyun.entity.po.program.ProgramTaskExgrscInfo;
import com.aoyuntek.framework.model.BaseModel;

public class ProgramTaskExgrscInfoVo extends BaseModel {

	private static final long serialVersionUID = 1L;

	private ProgramTaskExgrscInfo programTaskExgrscInfo;

	private List<FileVo> images;

	private List<ActivitiesVo> actList;

	private List<ActivityVo> intList;

	private List<ActivityVo> obsList;

	private List<ActivityVo> leaList;

	private List<String> tagAccountIds = new ArrayList<String>();

	private List<NqsVo> nqsVoList = new ArrayList<NqsVo>();

	private List<SelecterPo> selecterPos;

	public ProgramTaskExgrscInfo getProgramTaskExgrscInfo() {
		return programTaskExgrscInfo;
	}

	public void setProgramTaskExgrscInfo(ProgramTaskExgrscInfo programTaskExgrscInfo) {
		this.programTaskExgrscInfo = programTaskExgrscInfo;
	}

	public List<FileVo> getImages() {
		return images;
	}

	public void setImages(List<FileVo> images) {
		this.images = images;
	}

	public List<ActivitiesVo> getActList() {
		return actList;
	}

	public void setActList(List<ActivitiesVo> actList) {
		this.actList = actList;
	}

	public List<ActivityVo> getIntList() {
		return intList;
	}

	public void setIntList(List<ActivityVo> intList) {
		this.intList = intList;
	}

	public List<ActivityVo> getObsList() {
		return obsList;
	}

	public void setObsList(List<ActivityVo> obsList) {
		this.obsList = obsList;
	}

	public List<ActivityVo> getLeaList() {
		return leaList;
	}

	public void setLeaList(List<ActivityVo> leaList) {
		this.leaList = leaList;
	}

	public List<String> getTagAccountIds() {
		return tagAccountIds;
	}

	public void setTagAccountIds(List<String> tagAccountIds) {
		this.tagAccountIds = tagAccountIds;
	}

	public List<NqsVo> getNqsVoList() {
		return nqsVoList;
	}

	public void setNqsVoList(List<NqsVo> nqsVoList) {
		this.nqsVoList = nqsVoList;
	}

	public List<SelecterPo> getSelecterPos() {
		return selecterPos;
	}

	public void setSelecterPos(List<SelecterPo> selecterPos) {
		this.selecterPos = selecterPos;
	}

}
