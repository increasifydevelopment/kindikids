package com.aoyuntek.aoyun.entity.po;

import com.aoyuntek.framework.model.BaseModel;

public class NewsGroupInfo extends BaseModel {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_news_group.id
     * 
     * @mbggenerated Fri Jul 08 11:55:56 CST 2016
     */
    private String id;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_news_group.group_name
     * 
     * @mbggenerated Fri Jul 08 11:55:56 CST 2016
     */
    private String groupName;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_news_group.news_id
     * 
     * @mbggenerated Fri Jul 08 11:55:56 CST 2016
     */
    private String newsId;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_news_group.delete_flag
     * 
     * @mbggenerated Fri Jul 08 11:55:56 CST 2016
     */
    private Short deleteFlag;
    
    private String groupCenterId;
    

    public String getGroupCenterId() {
        return groupCenterId;
    }

    public void setGroupCenterId(String groupCenterId) {
        this.groupCenterId = groupCenterId;
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_news_group.id
     * 
     * @return the value of tbl_news_group.id
     * 
     * @mbggenerated Fri Jul 08 11:55:56 CST 2016
     */
    public String getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_news_group.id
     * 
     * @param id
     *            the value for tbl_news_group.id
     * 
     * @mbggenerated Fri Jul 08 11:55:56 CST 2016
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_news_group.group_name
     * 
     * @return the value of tbl_news_group.group_name
     * 
     * @mbggenerated Fri Jul 08 11:55:56 CST 2016
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_news_group.group_name
     * 
     * @param groupName
     *            the value for tbl_news_group.group_name
     * 
     * @mbggenerated Fri Jul 08 11:55:56 CST 2016
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName == null ? null : groupName.trim();
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_news_group.news_id
     * 
     * @return the value of tbl_news_group.news_id
     * 
     * @mbggenerated Fri Jul 08 11:55:56 CST 2016
     */
    public String getNewsId() {
        return newsId;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_news_group.news_id
     * 
     * @param newsId
     *            the value for tbl_news_group.news_id
     * 
     * @mbggenerated Fri Jul 08 11:55:56 CST 2016
     */
    public void setNewsId(String newsId) {
        this.newsId = newsId == null ? null : newsId.trim();
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_news_group.delete_flag
     * 
     * @return the value of tbl_news_group.delete_flag
     * 
     * @mbggenerated Fri Jul 08 11:55:56 CST 2016
     */
    public Short getDeleteFlag() {
        return deleteFlag;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_news_group.delete_flag
     * 
     * @param deleteFlag
     *            the value for tbl_news_group.delete_flag
     * 
     * @mbggenerated Fri Jul 08 11:55:56 CST 2016
     */
    public void setDeleteFlag(Short deleteFlag) {
        this.deleteFlag = deleteFlag;
    }
}