package com.aoyuntek.aoyun.entity.vo;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.aoyuntek.framework.deserializer.S3JsonSerializer;
import com.aoyuntek.framework.model.BaseModel;

public class SelecterPo extends BaseModel {

	private static final long serialVersionUID = 1L;

	/**
	 * account_id or group name
	 */
	private String id;

	/**
	 * user name or group name
	 */
	private String text;
	/**
	 * selected
	 */
	private Boolean selected = true;
	/**
	 * user avatar
	 */
	private String avatar;

	private String personColor;

	private Boolean disabled;
	// 06:00 AM - 07:00 PM
	private String time;
	// FullTime 0, PartTime 1, Casual 2.
	private Short staffType;

	private boolean isWorkDay;

	private List<RoleInfoVo> roleInfoVoList = new ArrayList<RoleInfoVo>();

	public SelecterPo() {

	}

	public SelecterPo(String id, String text, String avatar, String personColor, String time, Short staffType, boolean isWorkDay) {
		super();
		this.id = id;
		this.text = text;
		this.avatar = avatar;
		this.personColor = personColor;
		this.time = time;
		this.staffType = staffType;
		this.isWorkDay = isWorkDay;
	}

	public SelecterPo(String id, String text, String avatar, String personColor) {
		super();
		this.id = id;
		this.text = text;
		this.avatar = avatar;
		this.personColor = personColor;
	}

	public SelecterPo(String id, String text) {
		super();
		this.id = id;
		this.text = text;
	}

	public SelecterPo(String id, String text, Boolean selected) {
		super();
		this.id = id;
		this.text = text;
		this.selected = selected;
	}

	public List<RoleInfoVo> getRoleInfoVoList() {
		return roleInfoVoList;
	}

	public void setRoleInfoVoList(List<RoleInfoVo> roleInfoVoList) {
		this.roleInfoVoList = roleInfoVoList;
	}

	public Boolean getDisabled() {
		return disabled;
	}

	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}

	public String getPersonColor() {
		return personColor;
	}

	public void setPersonColor(String personColor) {
		this.personColor = personColor;
	}

	@JsonSerialize(using = S3JsonSerializer.class)
	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public Boolean getSelected() {
		return selected;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getId() {
		return id == null ? "" : id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public int hashCode() {
		String key = this.id + this.text;
		return key.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		SelecterPo s = (SelecterPo) obj;
		return id.equals(s.id) && text.equals(s.text);
	}

	@Override
	public String toString() {
		return "SelecterPo [id=" + id + ", text=" + text + ", selected=" + selected + ", avatar=" + avatar + ", personColor=" + personColor + "]";
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Short getStaffType() {
		return staffType;
	}

	public void setStaffType(Short staffType) {
		this.staffType = staffType;
	}

	public boolean isWorkDay() {
		return isWorkDay;
	}

	public void setWorkDay(boolean isWorkDay) {
		this.isWorkDay = isWorkDay;
	}

}
