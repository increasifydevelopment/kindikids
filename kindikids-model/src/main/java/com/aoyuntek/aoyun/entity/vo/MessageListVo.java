package com.aoyuntek.aoyun.entity.vo;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.aoyuntek.framework.deserializer.DateJsonSerializer4;
import com.aoyuntek.framework.deserializer.S3JsonSerializer;
import com.aoyuntek.framework.model.BaseModel;

public class MessageListVo extends BaseModel {
    /**
     * 序列化
     */
    private static final long serialVersionUID = 1L;
    /**
     * 信息ID
     */
    private String msgId;
    /**
     * 头像
     */
    private String avatar;
    /**
     * 主题
     */
    private String subject;
    /**
     * 发送时间
     */
    private Date time;
    /**
     * 小时数（ago）
     */
    private String timeAgo;
    /**
     * 读取标识
     */
    private short readFlag;
    /**
     * 发送人ID
     */
    private String sendAccountId;
    /**
     * 发送人
     */
    private String sendAccountName;
    /**
     * 信息类型
     */
    private String messageType;

    private boolean showTimeAgoFlag = true;

    private String personColor;

    public String getPersonColor() {
        return personColor;
    }

    public void setPersonColor(String personColor) {
        this.personColor = personColor;
    }

    public String getTimeAgo() {
        return timeAgo;
    }

    public boolean isShowTimeAgoFlag() {
        return showTimeAgoFlag;
    }

    public void setShowTimeAgoFlag(boolean showTimeAgoFlag) {
        this.showTimeAgoFlag = showTimeAgoFlag;
    }

    public void setTimeAgo(String timeAgo) {
        try {
            this.timeAgo = getTimeAgoStr(this.time);
            if (StringUtils.isEmpty(getTimeAgoStr(this.time))) {
                this.showTimeAgoFlag = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    @JsonSerialize(using = S3JsonSerializer.class)
    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public short getReadFlag() {
        return readFlag;
    }

    public void setReadFlag(short readFlag) {
        this.readFlag = readFlag;
    }

    public String getSendAccountId() {
        return sendAccountId;
    }

    public void setSendAccountId(String sendAccountId) {
        this.sendAccountId = sendAccountId;
    }

    public String getSendAccountName() {
        return sendAccountName;
    }

    public void setSendAccountName(String sendAccountName) {
        this.sendAccountName = sendAccountName;
    }

    @JsonSerialize(using = DateJsonSerializer4.class, include = JsonSerialize.Inclusion.ALWAYS)
    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}
