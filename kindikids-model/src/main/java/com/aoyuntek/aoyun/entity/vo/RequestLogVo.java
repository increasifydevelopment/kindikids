package com.aoyuntek.aoyun.entity.vo;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.aoyuntek.aoyun.entity.po.ChangeAttendanceRequestInfo;
import com.aoyuntek.aoyun.entity.po.RequestLogInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;
import com.aoyuntek.framework.deserializer.S3JsonSerializer;
import com.theone.date.util.DateUtil;

/**
 * 请求日志VO
 * 
 * @author dlli5 at 2016年8月19日上午9:41:51
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public class RequestLogVo extends RequestLogInfo {

	private static final long serialVersionUID = 1L;

	private String aboutUserId;

	private boolean operaFlag;

	private boolean cancelFlag;

	private String name;

	private String familyId;

	private String avatar;

	private String personColor;

	private String centerName;

	private ChildInfoVo childVo;

	private String msgStr;

	public String getMsgStr() {
		return msgStr;
	}

	public void setMsgStr(String msgStr) {
		this.msgStr = msgStr;
	}

	private ChangeAttendanceRequestInfo requestInfo;

	public ChangeAttendanceRequestInfo getRequestInfo() {
		return requestInfo;
	}

	public void setRequestInfo(ChangeAttendanceRequestInfo requestInfo) {
		this.requestInfo = requestInfo;
	}

	public ChildInfoVo getChildVo() {
		return childVo;
	}

	public void setChildVo(ChildInfoVo childVo) {
		this.childVo = childVo;
	}

	public String getCenterName() {
		return centerName;
	}

	public void setCenterName(String centerName) {
		this.centerName = centerName;
	}

	public String getPersonColor() {
		return personColor;
	}

	public void setPersonColor(String personColor) {
		this.personColor = personColor;
	}

	@JsonSerialize(using = S3JsonSerializer.class)
	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFamilyId() {
		return familyId;
	}

	public void setFamilyId(String familyId) {
		this.familyId = familyId;
	}

	public boolean isCancelFlag() {
		return cancelFlag;
	}

	public void setCancelFlag(boolean cancelFlag) {
		this.cancelFlag = cancelFlag;
	}

	public boolean isOperaFlag() {
		return operaFlag;
	}

	public void setOperaFlag(boolean operaFlag) {
		this.operaFlag = operaFlag;
	}

	public String getAboutUserId() {
		return aboutUserId;
	}

	public void setAboutUserId(String aboutUserId) {
		this.aboutUserId = aboutUserId;
	}

	public String toString() {
		msgStr = msgStr.replace("&nbsp;", " ");
		return name + "," + "\"" + msgStr + "\"" + "," + centerName + "," + getType(getLogType()) + "," + DateUtil.format(getCreateTime(), "dd/MM/yyyy") + ","
				+ DateUtil.format(getUpdateTime(), "dd/MM/yyyy") + "," + getStatus(getState());
	}

	private String getType(Short logType) {
		switch (getLogType()) {
		case 0:
			return "Giving Notice";
		case 1:
			return "Absence Request";
		case 2:
			return "Change of Room";
		case 3:
			return "Change of Centre";
		case 4:
			return "Change of Attendance";
		}
		return null;
	}

	private String getStatus(Short status) {
		switch (status) {
		case 0:
			return "Pending";
		case 1:
			return "Cancelled";
		case 2:
			return "Partial Approved";
		case 3:
		case 5:
			return "Approved";
		case 4:
			return "Declined";
		case 6:
			return "Lapsed";
		}
		return null;
	}
}
