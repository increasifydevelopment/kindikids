package com.aoyuntek.aoyun.entity.vo.roster;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.aoyuntek.aoyun.entity.po.roster.RosterInfo;

/**
 * 
 * @author dlli5 at 2016年10月17日上午10:07:15
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public class RosterRoleDayVo extends RosterInfo {

    private static final long serialVersionUID = 1L;

    private Map<Short, List<RosterStaffVo>> roleRosterStaffMap = new HashMap<Short, List<RosterStaffVo>>();
    private Date day;

    public RosterRoleDayVo() {
        super();
    }

    public RosterRoleDayVo(Map<Short, List<RosterStaffVo>> roleRosterStaffMap, Date day) {
        super();
        this.roleRosterStaffMap = roleRosterStaffMap;
        this.day = day;
    }

    public Date getDay() {
        return day;
    }

    public void setDay(Date day) {
        this.day = day;
    }

    public Map<Short, List<RosterStaffVo>> getRoleRosterStaffMap() {
        return roleRosterStaffMap;
    }

    public void setRoleRosterStaffMap(Map<Short, List<RosterStaffVo>> roleRosterStaffMap) {
        this.roleRosterStaffMap = roleRosterStaffMap;
    }

}
