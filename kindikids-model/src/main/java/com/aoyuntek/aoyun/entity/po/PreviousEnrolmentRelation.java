package com.aoyuntek.aoyun.entity.po;

public class PreviousEnrolmentRelation {
    private String objId;

    private String previousEnrolmentId;

    public String getObjId() {
        return objId;
    }

    public void setObjId(String objId) {
        this.objId = objId == null ? null : objId.trim();
    }

    public String getPreviousEnrolmentId() {
        return previousEnrolmentId;
    }

    public void setPreviousEnrolmentId(String previousEnrolmentId) {
        this.previousEnrolmentId = previousEnrolmentId == null ? null : previousEnrolmentId.trim();
    }
}