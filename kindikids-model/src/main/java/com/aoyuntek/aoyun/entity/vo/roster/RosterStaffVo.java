package com.aoyuntek.aoyun.entity.vo.roster;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.aoyuntek.aoyun.entity.po.LeaveInfo;
import com.aoyuntek.aoyun.entity.po.roster.RosterStaffInfo;
import com.aoyuntek.aoyun.entity.vo.RoleInfoVo;
import com.aoyuntek.framework.deserializer.S3JsonSerializer;

/**
 * 
 * @author dlli5 at 2016年10月17日上午10:07:15
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public class RosterStaffVo extends RosterStaffInfo {

	private static final long serialVersionUID = 1L;

	private String name;

	private String personColor;

	private String avatar;

	private Short sinState;

	private Date signInDate;

	private Date signOutDate;

	private Short soutState;

	private String userId;

	private String workTime;

	private List<RoleInfoVo> roleInfos;

	private List<LeaveInfo> leaves = new ArrayList<LeaveInfo>();

	private List<String> leaveTimes = new ArrayList<String>();

	public List<String> getLeaveTimes() {
		return leaveTimes;
	}

	public void setLeaveTimes(List<String> leaveTimes) {
		this.leaveTimes = leaveTimes;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public RosterStaffVo() {
		super();
	}

	public RosterStaffVo(Date day) {
		super();
		setDay(day);
	}

	public List<RoleInfoVo> getRoleInfos() {
		return roleInfos;
	}

	public void setRoleInfos(List<RoleInfoVo> roleInfos) {
		this.roleInfos = roleInfos;
	}

	public Date getSignInDate() {
		return signInDate;
	}

	public void setSignInDate(Date signInDate) {
		this.signInDate = signInDate;
	}

	public Date getSignOutDate() {
		return signOutDate;
	}

	public void setSignOutDate(Date signOutDate) {
		this.signOutDate = signOutDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPersonColor() {
		return personColor;
	}

	public void setPersonColor(String personColor) {
		this.personColor = personColor;
	}

	@JsonSerialize(using = S3JsonSerializer.class)
	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public Short getSinState() {
		return sinState;
	}

	public void setSinState(Short sinState) {
		this.sinState = sinState;
	}

	public Short getSoutState() {
		return soutState;
	}

	public void setSoutState(Short soutState) {
		this.soutState = soutState;
	}

	public List<LeaveInfo> getLeaves() {
		return leaves;
	}

	public void setLeaves(List<LeaveInfo> leaves) {
		this.leaves = leaves;
	}

	public String getWorkTime() {
		return workTime;
	}

	public void setWorkTime(String workTime) {
		this.workTime = workTime;
	}

}
