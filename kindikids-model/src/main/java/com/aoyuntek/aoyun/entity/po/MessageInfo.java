package com.aoyuntek.aoyun.entity.po;

import java.util.Date;

import com.aoyuntek.framework.model.BaseModel;

public class MessageInfo extends BaseModel {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    /**
     * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_message.id
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    private String id;
    /**
     * 老id
     */
    private String oldId;
    
    public String getOldId() {
        return oldId;
    }

    public void setOldId(String oldId) {
        this.oldId = oldId;
    }

    /**
     * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_message.send_account_id
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    private String sendAccountId;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_message.send_account_name
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    private String sendAccountName;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_message.sender_time
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    private Date senderTime;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_message.create_time
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    private Date createTime;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_message.subject
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    private String subject;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_message.centers_id
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    private String centersId;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_message.delete_flag
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    private Short deleteFlag;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_message.message_type
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    private Short messageType;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_message.parent_message_id
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    private String parentMessageId;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_message.read_flag
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    private Short readFlag;

    private String fileId;
    
   

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_message.id
     * 
     * @return the value of tbl_message.id
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    public String getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_message.id
     * 
     * @param id
     *            the value for tbl_message.id
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_message.send_account_id
     * 
     * @return the value of tbl_message.send_account_id
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    public String getSendAccountId() {
        return sendAccountId;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_message.send_account_id
     * 
     * @param sendAccountId
     *            the value for tbl_message.send_account_id
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    public void setSendAccountId(String sendAccountId) {
        this.sendAccountId = sendAccountId == null ? null : sendAccountId.trim();
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_message.send_account_name
     * 
     * @return the value of tbl_message.send_account_name
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    public String getSendAccountName() {
        return sendAccountName;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_message.send_account_name
     * 
     * @param sendAccountName
     *            the value for tbl_message.send_account_name
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    public void setSendAccountName(String sendAccountName) {
        this.sendAccountName = sendAccountName == null ? null : sendAccountName.trim();
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_message.sender_time
     * 
     * @return the value of tbl_message.sender_time
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    public Date getSenderTime() {
        return senderTime;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_message.sender_time
     * 
     * @param senderTime
     *            the value for tbl_message.sender_time
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    public void setSenderTime(Date senderTime) {
        this.senderTime = senderTime;
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_message.create_time
     * 
     * @return the value of tbl_message.create_time
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_message.create_time
     * 
     * @param createTime
     *            the value for tbl_message.create_time
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_message.subject
     * 
     * @return the value of tbl_message.subject
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    public String getSubject() {
        return subject;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_message.subject
     * 
     * @param subject
     *            the value for tbl_message.subject
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    public void setSubject(String subject) {
        this.subject = subject == null ? null : subject.trim();
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_message.centers_id
     * 
     * @return the value of tbl_message.centers_id
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    public String getCentersId() {
        return centersId;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_message.centers_id
     * 
     * @param centersId
     *            the value for tbl_message.centers_id
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    public void setCentersId(String centersId) {
        this.centersId = centersId == null ? null : centersId.trim();
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_message.delete_flag
     * 
     * @return the value of tbl_message.delete_flag
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    public Short getDeleteFlag() {
        return deleteFlag;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_message.delete_flag
     * 
     * @param deleteFlag
     *            the value for tbl_message.delete_flag
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    public void setDeleteFlag(Short deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_message.message_type
     * 
     * @return the value of tbl_message.message_type
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    public Short getMessageType() {
        return messageType;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_message.message_type
     * 
     * @param messageType
     *            the value for tbl_message.message_type
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    public void setMessageType(Short messageType) {
        this.messageType = messageType;
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_message.parent_message_id
     * 
     * @return the value of tbl_message.parent_message_id
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    public String getParentMessageId() {
        return parentMessageId;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_message.parent_message_id
     * 
     * @param parentMessageId
     *            the value for tbl_message.parent_message_id
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    public void setParentMessageId(String parentMessageId) {
        this.parentMessageId = parentMessageId == null ? null : parentMessageId.trim();
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_message.read_flag
     * 
     * @return the value of tbl_message.read_flag
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    public Short getReadFlag() {
        return readFlag;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_message.read_flag
     * 
     * @param readFlag
     *            the value for tbl_message.read_flag
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    public void setReadFlag(Short readFlag) {
        this.readFlag = readFlag;
    }
}