package com.aoyuntek.aoyun.entity.vo;

import java.util.List;

import com.aoyuntek.framework.model.BaseModel;

public class OutModleVo extends BaseModel {
    private static final long serialVersionUID = 1L;
    private List<String> centreList;
    private String firstname1;
    private String lastname1;
    private String relationship1;
    private String address1;
    private String suburb1;
    private String postcode1;
    private String homephone1;
    private String workphone1;
    private String mobile1;
    private String email1;
    private String firstname2;
    private String lastname2;
    private String relationship2;
    private String address2;
    private String suburb2;
    private String postcode2;
    private String homephone2;
    private String workphone2;
    private String mobile2;
    private String email2;
    private String childFirstname;
    private String childLastname;
    private String childSex;
    private String dobChild;
    private String nationality;
    private String languages;
    private String religion;
    private String hasImmunised;
    private String hasAnaphylaxis;
    private String learningDifficulties;
    private String medicalConditions;
    private String dietaryRequipments;
    private String allergies;
    private String proposal;
    private String channel;
    private String channelOther;
    private String changeDate;
    private Boolean monday;
    private Boolean tuesday;
    private Boolean wednesday;
    private Boolean thursday;
    private Boolean friday;

    
    public String getChannelOther() {
        return channelOther;
    }

    public void setChannelOther(String channelOther) {
        this.channelOther = channelOther;
    }

    public String getChangeDate() {
        return changeDate;
    }

    public Boolean getMonday() {
        return monday;
    }

    public Boolean getTuesday() {
        return tuesday;
    }

    public Boolean getWednesday() {
        return wednesday;
    }

    public Boolean getThursday() {
        return thursday;
    }

    public Boolean getFriday() {
        return friday;
    }

    public void setChangeDate(String changeDate) {
        this.changeDate = changeDate;
    }

    public void setMonday(Boolean monday) {
        this.monday = monday;
    }

    public void setTuesday(Boolean tuesday) {
        this.tuesday = tuesday;
    }

    public void setWednesday(Boolean wednesday) {
        this.wednesday = wednesday;
    }

    public void setThursday(Boolean thursday) {
        this.thursday = thursday;
    }

    public void setFriday(Boolean friday) {
        this.friday = friday;
    }

    public List<String> getCentreList() {
        return centreList;
    }

    public void setCentreList(List<String> centreList) {
        this.centreList = centreList;
    }

    public String getFirstname1() {
        return firstname1;
    }

    public String getLastname1() {
        return lastname1;
    }

    public String getRelationship1() {
        return relationship1;
    }

    public String getAddress1() {
        return address1;
    }

    public String getSuburb1() {
        return suburb1;
    }

    public String getPostcode1() {
        return postcode1;
    }

    public String getHomephone1() {
        return homephone1;
    }

    public String getWorkphone1() {
        return workphone1;
    }

    public String getMobile1() {
        return mobile1;
    }

    public String getEmail1() {
        return email1;
    }

    public String getFirstname2() {
        return firstname2;
    }

    public String getLastname2() {
        return lastname2;
    }

    public String getRelationship2() {
        return relationship2;
    }

    public String getAddress2() {
        return address2;
    }

    public String getSuburb2() {
        return suburb2;
    }

    public String getPostcode2() {
        return postcode2;
    }

    public String getHomephone2() {
        return homephone2;
    }

    public String getWorkphone2() {
        return workphone2;
    }

    public String getMobile2() {
        return mobile2;
    }

    public String getEmail2() {
        return email2;
    }

    public String getChildFirstname() {
        return childFirstname;
    }

    public String getChildLastname() {
        return childLastname;
    }

    public String getChildSex() {
        return childSex;
    }

    public String getDobChild() {
        return dobChild;
    }

    public String getNationality() {
        return nationality;
    }

    public String getLanguages() {
        return languages;
    }

    public String getReligion() {
        return religion;
    }

    public String getHasImmunised() {
        return hasImmunised;
    }

    public String getHasAnaphylaxis() {
        return hasAnaphylaxis;
    }

    public String getLearningDifficulties() {
        return learningDifficulties;
    }

    public String getMedicalConditions() {
        return medicalConditions;
    }

    public String getDietaryRequipments() {
        return dietaryRequipments;
    }

    public String getAllergies() {
        return allergies;
    }

    public String getProposal() {
        return proposal;
    }

    public String getChannel() {
        return channel;
    }

    public void setFirstname1(String firstname1) {
        this.firstname1 = firstname1;
    }

    public void setLastname1(String lastname1) {
        this.lastname1 = lastname1;
    }

    public void setRelationship1(String relationship1) {
        this.relationship1 = relationship1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public void setSuburb1(String suburb1) {
        this.suburb1 = suburb1;
    }

    public void setPostcode1(String postcode1) {
        this.postcode1 = postcode1;
    }

    public void setHomephone1(String homephone1) {
        this.homephone1 = homephone1;
    }

    public void setWorkphone1(String workphone1) {
        this.workphone1 = workphone1;
    }

    public void setMobile1(String mobile1) {
        this.mobile1 = mobile1;
    }

    public void setEmail1(String email1) {
        this.email1 = email1;
    }

    public void setFirstname2(String firstname2) {
        this.firstname2 = firstname2;
    }

    public void setLastname2(String lastname2) {
        this.lastname2 = lastname2;
    }

    public void setRelationship2(String relationship2) {
        this.relationship2 = relationship2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public void setSuburb2(String suburb2) {
        this.suburb2 = suburb2;
    }

    public void setPostcode2(String postcode2) {
        this.postcode2 = postcode2;
    }

    public void setHomephone2(String homephone2) {
        this.homephone2 = homephone2;
    }

    public void setWorkphone2(String workphone2) {
        this.workphone2 = workphone2;
    }

    public void setMobile2(String mobile2) {
        this.mobile2 = mobile2;
    }

    public void setEmail2(String email2) {
        this.email2 = email2;
    }

    public void setChildFirstname(String childFirstname) {
        this.childFirstname = childFirstname;
    }

    public void setChildLastname(String childLastname) {
        this.childLastname = childLastname;
    }

    public void setChildSex(String childSex) {
        this.childSex = childSex;
    }

    public void setDobChild(String dobChild) {
        this.dobChild = dobChild;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public void setLanguages(String languages) {
        this.languages = languages;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public void setHasImmunised(String hasImmunised) {
        this.hasImmunised = hasImmunised;
    }

    public void setHasAnaphylaxis(String hasAnaphylaxis) {
        this.hasAnaphylaxis = hasAnaphylaxis;
    }

    public void setLearningDifficulties(String learningDifficulties) {
        this.learningDifficulties = learningDifficulties;
    }

    public void setMedicalConditions(String medicalConditions) {
        this.medicalConditions = medicalConditions;
    }

    public void setDietaryRequipments(String dietaryRequipments) {
        this.dietaryRequipments = dietaryRequipments;
    }

    public void setAllergies(String allergies) {
        this.allergies = allergies;
    }

    public void setProposal(String proposal) {
        this.proposal = proposal;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

}
