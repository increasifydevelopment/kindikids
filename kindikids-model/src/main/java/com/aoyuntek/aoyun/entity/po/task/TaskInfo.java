package com.aoyuntek.aoyun.entity.po.task;

import java.util.Date;

import com.aoyuntek.framework.model.BaseModel;

public class TaskInfo extends BaseModel {

    private static final long serialVersionUID = 1L;

    private String id;

    private String taskName;

    private Short taskCategory;

    private Boolean shiftFlag;

    private String selectObjId;

    private String responsiblePeopleId;

    private String taskImplementersId;

    private Boolean followFlag;

    private Short statu;

    private Date createTime;

    private String createAccountId;

    private Date updateTime;

    private String updateAccountId;

    private Short deleteFlag;

    private String taskCode;

    private Boolean currentFlag;

    private Boolean immediateEffect;

    public Boolean getImmediateEffect() {
        return immediateEffect;
    }

    public void setImmediateEffect(Boolean immediateEffect) {
        this.immediateEffect = immediateEffect == null ? false : immediateEffect;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName == null ? null : taskName.trim();
    }

    public Short getTaskCategory() {
        return taskCategory;
    }

    public void setTaskCategory(Short taskCategory) {
        this.taskCategory = taskCategory;
    }

    public Boolean getShiftFlag() {
        return shiftFlag;
    }

    public void setShiftFlag(Boolean shiftFlag) {
        this.shiftFlag = shiftFlag;
    }

    public String getSelectObjId() {
        return selectObjId;
    }

    public void setSelectObjId(String selectObjId) {
        this.selectObjId = selectObjId == null ? null : selectObjId.trim();
    }

    public String getResponsiblePeopleId() {
        return responsiblePeopleId;
    }

    public void setResponsiblePeopleId(String responsiblePeopleId) {
        this.responsiblePeopleId = responsiblePeopleId == null ? null : responsiblePeopleId.trim();
    }

    public String getTaskImplementersId() {
        return taskImplementersId;
    }

    public void setTaskImplementersId(String taskImplementersId) {
        this.taskImplementersId = taskImplementersId == null ? null : taskImplementersId.trim();
    }

    public Boolean getFollowFlag() {
        return followFlag;
    }

    public void setFollowFlag(Boolean followFlag) {
        this.followFlag = followFlag;
    }

    public Short getStatu() {
        return statu;
    }

    public void setStatu(Short statu) {
        this.statu = statu;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateAccountId() {
        return createAccountId;
    }

    public void setCreateAccountId(String createAccountId) {
        this.createAccountId = createAccountId == null ? null : createAccountId.trim();
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateAccountId() {
        return updateAccountId;
    }

    public void setUpdateAccountId(String updateAccountId) {
        this.updateAccountId = updateAccountId;
    }

    public Short getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Short deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getTaskCode() {
        return taskCode;
    }

    public void setTaskCode(String taskCode) {
        this.taskCode = taskCode == null ? null : taskCode.trim();
    }

    public Boolean getCurrentFlag() {
        return currentFlag;
    }

    public void setCurrentFlag(Boolean currentFlag) {
        this.currentFlag = currentFlag;
    }
}