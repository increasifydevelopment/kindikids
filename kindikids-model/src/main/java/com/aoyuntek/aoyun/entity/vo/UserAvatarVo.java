package com.aoyuntek.aoyun.entity.vo;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.aoyuntek.framework.deserializer.S3JsonSerializer;
import com.aoyuntek.framework.model.BaseModel;

/**
 * 
 * @author dlli5 at 2016年9月22日下午7:20:58
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public class UserAvatarVo extends BaseModel {

    private static final long serialVersionUID = 1L;

    private String avatar;
    
    private String personColor;
    
    private String fullName;
    
    public UserAvatarVo(String avatar, String personColor, String fullName) {
        super();
        this.avatar = avatar;
        this.personColor = personColor;
        this.fullName = fullName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public UserAvatarVo() {
        super();
    }

    public String getPersonColor() {
        return personColor;
    }

    public void setPersonColor(String personColor) {
        this.personColor = personColor;
    }

    @JsonSerialize(using = S3JsonSerializer.class)
    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

}
