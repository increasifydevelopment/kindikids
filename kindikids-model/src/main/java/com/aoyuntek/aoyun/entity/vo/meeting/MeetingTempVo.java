package com.aoyuntek.aoyun.entity.vo.meeting;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.meeting.MeetingColumnsInfo;
import com.aoyuntek.aoyun.entity.po.meeting.MeetingTemplateInfo;
import com.aoyuntek.aoyun.entity.po.meeting.TemplateFrequencyInfo;
import com.aoyuntek.aoyun.entity.vo.NqsVo;
import com.aoyuntek.framework.model.BaseModel;

public class MeetingTempVo extends BaseModel {
    /**
     * 序列化
     */
    private static final long serialVersionUID = 1L;
    /**
     * meetingTemplateInfo 信息
     */
    private MeetingTemplateInfo meetingTemplateInfo;
    /**
     * nqs 信息
     */
    private List<NqsVo> nqsList;
    /**
     * frequency 信息
     */
    private TemplateFrequencyInfo frequency;

    private List<MeetingColumnsInfo> columnList;

    public MeetingTemplateInfo getMeetingTemplateInfo() {
        return meetingTemplateInfo;
    }

    public void setMeetingTemplateInfo(MeetingTemplateInfo meetingTemplateInfo) {
        this.meetingTemplateInfo = meetingTemplateInfo;
    }

    public List<NqsVo> getNqsList() {
        return nqsList;
    }

    public void setNqsList(List<NqsVo> nqsList) {
        this.nqsList = nqsList;
    }

    public TemplateFrequencyInfo getFrequency() {
        return frequency;
    }

    public void setFrequency(TemplateFrequencyInfo frequency) {
        this.frequency = frequency;
    }

    public List<MeetingColumnsInfo> getColumnList() {
        return columnList;
    }

    public void setColumnList(List<MeetingColumnsInfo> columnList) {
        this.columnList = columnList;
    }

}
