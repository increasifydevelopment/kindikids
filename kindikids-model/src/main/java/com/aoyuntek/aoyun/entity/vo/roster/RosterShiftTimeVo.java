package com.aoyuntek.aoyun.entity.vo.roster;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.roster.RosterShiftTimeInfo;
import com.aoyuntek.aoyun.entity.po.task.TaskInfo;

/**
 * 
 * @author dlli5 at 2016年10月14日上午8:48:21
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public class RosterShiftTimeVo extends RosterShiftTimeInfo {

    private static final long serialVersionUID = 1L;

    private List<ShiftWeekVo> shiftWeekVos;

    private List<TaskInfo> rosterShiftTaskVos;

    public List<ShiftWeekVo> getShiftWeekVos() {
        return shiftWeekVos;
    }

    public void setShiftWeekVos(List<ShiftWeekVo> shiftWeekVos) {
        this.shiftWeekVos = shiftWeekVos;
    }

    public List<TaskInfo> getRosterShiftTaskVos() {
        return rosterShiftTaskVos;
    }

    public void setRosterShiftTaskVos(List<TaskInfo> rosterShiftTaskVos) {
        this.rosterShiftTaskVos = rosterShiftTaskVos;
    }
}
