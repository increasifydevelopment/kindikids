package com.aoyuntek.aoyun.entity.po;

import java.util.Date;
import java.util.UUID;

import com.aoyuntek.aoyun.enums.DeleteFlag;
import com.aoyuntek.aoyun.enums.jobs.MsgEmailCategory;
import com.aoyuntek.framework.model.BaseModel;

public class MsgEmailInfo extends BaseModel {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String id;

    /**
     * 发送地址
     */
    private String senderaddress;

    /**
     * 发送账户名
     */
    private String sendername;

    /**
     * 接受地址|手机号码
     */
    private String receiveraddress;

    /**
     * 接受人名称
     */
    private String receivername;

    /**
     * 主题
     */
    private String sub;

    /**
     * 回复人
     */
    private String replyto;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 列表 0邮件,1短信
     */
    private Short category;

    /**
     * 标示发出功能点
     */
    private Short type;

    private Short deleteFlag;
    /**
     * 内容
     */
    private String msg;

    public MsgEmailInfo() {

    }

    public MsgEmailInfo(String id, String senderaddress, String sendername, String receiveraddress, String receivername, String sub, String replyto,
            Date createTime, Short category, Short type, Short deleteFlag, String msg) {
        super();
        this.id = id;
        this.senderaddress = senderaddress;
        this.sendername = sendername;
        this.receiveraddress = receiveraddress;
        this.receivername = receivername;
        this.sub = sub;
        this.replyto = replyto;
        this.createTime = createTime;
        this.category = category;
        this.type = type;
        this.deleteFlag = deleteFlag;
        this.msg = msg;
    }

    public MsgEmailInfo(String id, String receiveraddress, Date createTime, Short category, Short type, String msg, Short deleteFlag) {
        super();
        this.id = id;
        this.createTime = createTime;
        this.category = category;
        this.type = type;
        this.msg = msg;
        this.deleteFlag = deleteFlag;
    }

    /**
     * 
     * @description 获取邮件实例
     * @author gfwang
     * @create 2016年10月10日上午9:45:36
     * @version 1.0
     * @param senderaddress
     *            发送地址
     * @param sendername
     *            发送人名
     * @param receiveraddress
     *            接受邮件地址
     * @param receivername
     *            接受人名
     * @param sub
     *            主题
     * @param replyto
     *            回复人
     * @param createTime
     *            创建时间
     * @param type
     *            类型
     * @param msg
     *            内容
     * @return
     */
    public static MsgEmailInfo getEmailMsgInstance(String senderaddress, String sendername, String receiveraddress, String receivername, String sub,
            String replyto, Date createTime, Short type, String msg) {
        if (createTime == null) {
            createTime = new Date();
        }
        return new MsgEmailInfo(UUID.randomUUID().toString(), senderaddress, sendername, receiveraddress, receivername, sub, replyto, createTime,
                MsgEmailCategory.EmailMsg.getValue(), type, DeleteFlag.Default.getValue(), msg);
    }

    /**
     * 
     * @description 获取短信实例
     * @author gfwang
     * @create 2016年10月10日上午9:37:26
     * @version 1.0
     * @param receiveraddress
     *            电话号码
     * @param createTime
     *            创建时间
     * @param type
     *            类型
     * @param msg
     *            消息
     * @return
     */
    public static MsgEmailInfo getPhoneMsgInstance(String receiveraddress, Date createTime, Short type, String msg) {
        if (createTime == null) {
            createTime = new Date();
        }
        return new MsgEmailInfo(UUID.randomUUID().toString(), receiveraddress, createTime, MsgEmailCategory.PhoneMsg.getValue(), type, msg,
                DeleteFlag.Default.getValue());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getSenderaddress() {
        return senderaddress;
    }

    public void setSenderaddress(String senderaddress) {
        this.senderaddress = senderaddress == null ? null : senderaddress.trim();
    }

    public String getSendername() {
        return sendername;
    }

    public void setSendername(String sendername) {
        this.sendername = sendername == null ? null : sendername.trim();
    }

    public String getReceiveraddress() {
        return receiveraddress;
    }

    public void setReceiveraddress(String receiveraddress) {
        this.receiveraddress = receiveraddress == null ? null : receiveraddress.trim();
    }

    public String getReceivername() {
        return receivername;
    }

    public void setReceivername(String receivername) {
        this.receivername = receivername == null ? null : receivername.trim();
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub == null ? null : sub.trim();
    }

    public String getReplyto() {
        return replyto;
    }

    public void setReplyto(String replyto) {
        this.replyto = replyto == null ? null : replyto.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Short getCategory() {
        return category;
    }

    public void setCategory(Short category) {
        this.category = category;
    }

    public Short getType() {
        return type;
    }

    public void setType(Short type) {
        this.type = type;
    }

    public Short getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Short deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg == null ? null : msg.trim();
    }
}