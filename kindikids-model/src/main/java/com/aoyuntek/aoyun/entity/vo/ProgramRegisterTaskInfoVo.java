package com.aoyuntek.aoyun.entity.vo;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.program.ProgramRegisterTaskInfo;
import com.aoyuntek.framework.model.BaseModel;

public class ProgramRegisterTaskInfoVo extends BaseModel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private ProgramRegisterTaskInfo programRegisterTaskInfo;

    private List<ProgramRegisterItemInfoVo> registerItemList;
    
    private Boolean isSubmit;

    
    public Boolean getIsSubmit() {
        return isSubmit;
    }

    public void setIsSubmit(Boolean isSubmit) {
        this.isSubmit = isSubmit;
    }

    public ProgramRegisterTaskInfo getProgramRegisterTaskInfo() {
        return programRegisterTaskInfo;
    }

    public void setProgramRegisterTaskInfo(ProgramRegisterTaskInfo programRegisterTaskInfo) {
        this.programRegisterTaskInfo = programRegisterTaskInfo;
    }

    public List<ProgramRegisterItemInfoVo> getRegisterItemList() {
        return registerItemList;
    }

    public void setRegisterItemList(List<ProgramRegisterItemInfoVo> registerItemList) {
        this.registerItemList = registerItemList;
    }

}
