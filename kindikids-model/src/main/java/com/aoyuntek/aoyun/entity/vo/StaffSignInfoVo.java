package com.aoyuntek.aoyun.entity.vo;

import java.util.Date;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.aoyuntek.framework.deserializer.DateJsonSerializer3;
import com.aoyuntek.framework.deserializer.S3JsonSerializer;
import com.aoyuntek.framework.model.BaseModel;

/**
 * @description 员工签入签出信息VO实体
 * @author Hxzhang 2016年9月20日上午11:32:29
 * @version 1.0
 */
public class StaffSignInfoVo extends BaseModel {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private String signinId;
    private String signoutId;
    private String centreId;
    private String centreName;
    private String personColor;
    private Date inTime;
    private Date outTime;
    private String firstName;
    private String middleName;
    private String lastName;
    private String accountId;
    private String avatar;
    private Short signInState;
    private String location;
    private Date startTime;
    private Date endTime;
    private Short type;

    public Short getType() {
        return type;
    }

    public void setType(Short type) {
        this.type = type;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @JsonSerialize(using = S3JsonSerializer.class)
    public String getAvatar() {
        return avatar;
    }

    public Short getSignInState() {
        return signInState;
    }

    public void setSignInState(Short signInState) {
        this.signInState = signInState;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCentreName() {
        return centreName;
    }

    public void setCentreName(String centreName) {
        this.centreName = centreName;
    }

    public String getSigninId() {
        return signinId;
    }

    public void setSigninId(String signinId) {
        this.signinId = signinId;
    }

    public String getSignoutId() {
        return signoutId;
    }

    public void setSignoutId(String signoutId) {
        this.signoutId = signoutId;
    }

    public String getCentreId() {
        return centreId;
    }

    public void setCentreId(String centreId) {
        this.centreId = centreId;
    }

    public String getPersonColor() {
        return personColor;
    }

    public void setPersonColor(String personColor) {
        this.personColor = personColor;
    }

    @JsonSerialize(using = DateJsonSerializer3.class, include = JsonSerialize.Inclusion.ALWAYS)
    public Date getInTime() {
        return inTime;
    }

    public void setInTime(Date inTime) {
        this.inTime = inTime;
    }

    @JsonSerialize(using = DateJsonSerializer3.class, include = JsonSerialize.Inclusion.ALWAYS)
    public Date getOutTime() {
        return outTime;
    }

    public void setOutTime(Date outTime) {
        this.outTime = outTime;
    }

}
