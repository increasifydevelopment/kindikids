package com.aoyuntek.aoyun.entity.vo.meeting;

import java.util.ArrayList;
import java.util.List;

import com.aoyuntek.aoyun.entity.po.meeting.AgendaItemInfo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;

public class AgendaItemInfoVo extends AgendaItemInfo {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private List<SelecterPo> presenter = new ArrayList<SelecterPo>();

    public List<SelecterPo> getPresenter() {
        return presenter;
    }

    public void setPresenter(List<SelecterPo> presenter) {
        this.presenter = presenter;
    }

}
