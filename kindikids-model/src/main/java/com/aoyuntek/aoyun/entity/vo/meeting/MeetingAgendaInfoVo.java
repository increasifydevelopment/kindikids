package com.aoyuntek.aoyun.entity.vo.meeting;

import java.util.ArrayList;
import java.util.List;

import com.aoyuntek.aoyun.entity.po.meeting.AgendaTypeInfo;
import com.aoyuntek.aoyun.entity.po.meeting.MeetingAgendaInfo;
import com.aoyuntek.aoyun.entity.vo.NqsVo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;

public class MeetingAgendaInfoVo extends MeetingAgendaInfo {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private List<NqsVo> nqsList;

    private AgendaTypeInfo typeInfo;
    
    private List<SelecterPo> selecterPo=new ArrayList<SelecterPo>();

    private List<AgendaItemInfoVo> agendaItems;

    public List<SelecterPo> getSelecterPo() {
        return selecterPo;
    }

    public AgendaTypeInfo getTypeInfo() {
        return typeInfo;
    }

    public void setTypeInfo(AgendaTypeInfo typeInfo) {
        this.typeInfo = typeInfo;
    }

    public void setSelecterPo(List<SelecterPo> selecterPo) {
        this.selecterPo = selecterPo;
    }

    public List<NqsVo> getNqsList() {
        return nqsList;
    }

    public void setNqsList(List<NqsVo> nqsList) {
        this.nqsList = nqsList;
    }

    public List<AgendaItemInfoVo> getAgendaItems() {
        return agendaItems;
    }

    public void setAgendaItems(List<AgendaItemInfoVo> agendaItems) {
        this.agendaItems = agendaItems;
    }

}
