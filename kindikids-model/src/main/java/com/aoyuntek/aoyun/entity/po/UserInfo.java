package com.aoyuntek.aoyun.entity.po;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.aoyuntek.aoyun.entity.vo.RoleInfoVo;
import com.aoyuntek.framework.deserializer.DateJsonSerializer2;
import com.aoyuntek.framework.deserializer.S3JsonSerializer;
import com.theone.string.util.StringUtil;

public class UserInfo extends FileBaseVo {
	private static final long serialVersionUID = 1L;

	private String crn;

	private String hubworkId;

	public String getCrn() {
		return crn;
	}

	public void setCrn(String crn) {
		this.crn = crn;
	}

	public String getHubworkId() {
		return hubworkId;
	}

	public void setHubworkId(String hubworkId) {
		this.hubworkId = hubworkId;
	}

	private Short enrolled;

	public Short getEnrolled() {
		return enrolled;
	}

	public void setEnrolled(Short enrolled) {
		this.enrolled = enrolled;
	}

	private String oldId;

	private List<String> outCenterIds;

	private List<RoleInfoVo> roleInfoVoList = new ArrayList<RoleInfoVo>();

	public List<RoleInfoVo> getRoleInfoVoList() {
		return roleInfoVoList;
	}

	public void setRoleInfoVoList(List<RoleInfoVo> roleInfoVoList) {
		this.roleInfoVoList = roleInfoVoList;
	}

	public List<String> getOutCenterIds() {
		return outCenterIds;
	}

	public void setOutCenterIds(List<String> outCenterIds) {
		this.outCenterIds = outCenterIds;
	}

	public String getOldId() {
		return oldId;
	}

	public void setOldId(String oldId) {
		this.oldId = oldId;
	}

	private String id;

	private String personColor;

	private String centersId;

	private String roomId;

	private String groupId;

	private String firstName;

	private String middleName;

	private String lastName;

	private String avatar;

	private String email;

	private Short gender;

	private Date birthday;

	private Short staffType;

	public String getPersonColor() {
		return personColor;
	}

	public void setPersonColor(String personColor) {
		this.personColor = personColor;
	}

	public Short getStaffType() {
		return staffType;
	}

	public void setStaffType(Short staffType) {
		this.staffType = staffType;
	}

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_user.birth_cert
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	private String birthCert;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_user.address
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	private String address;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_user.suburb
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	private String suburb;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_user.postcode
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	private String postcode;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_user.state
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	private String state;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_user.family_status
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	private Short familyStatus;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_user.family_status_other
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	private String familyStatusOther;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_user.phone_number
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	private String phoneNumber;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_user.mobile_number
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	private String mobileNumber;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_user.marital_status
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	private Short maritalStatus;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_user.ethnicity
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	private String ethnicity;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_user.children_num
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	private String childrenNum;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_user.children_age
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	private String childrenAge;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_user.contact_email
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	private Boolean contactEmail;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_user.contact_letter
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	private Boolean contactLetter;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_user.contact_person
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	private Boolean contactPerson;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_user.contact_phone
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	private Boolean contactPhone;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_user.work_phone_number
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	private String workPhoneNumber;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_user.occupation
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	private String occupation;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_user.place_of_work
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	private String placeOfWork;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_user.languages_at_home
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	private String languagesAtHome;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_user.user_type
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	private Short userType;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_user.create_time
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	private Date createTime;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_user.create_account_id
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	private String createAccountId;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_user.update_time
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	private Date updateTime;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_user.update_account_id
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	private String updateAccountId;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_account.family_id
	 * 
	 * @mbggenerated Fri Jul 29 15:19:33 CST 2016
	 */
	private String familyId;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column tbl_user.delete_flag
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	private Short deleteFlag;

	private Date staffInDate;

	private String fullName;

	private String accountId;

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	@JsonSerialize(using = DateJsonSerializer2.class, include = JsonSerialize.Inclusion.ALWAYS)
	public Date getStaffInDate() {
		return staffInDate;
	}

	public void setStaffInDate(Date staffInDate) {
		this.staffInDate = staffInDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_user.id
	 * 
	 * @return the value of tbl_user.id
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public String getId() {
		return id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_user.id
	 * 
	 * @param id
	 *            the value for tbl_user.id
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public void setId(String id) {
		this.id = id == null ? null : id.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_user.centers_id
	 * 
	 * @return the value of tbl_user.centers_id
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public String getCentersId() {
		return centersId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_user.centers_id
	 * 
	 * @param centersId
	 *            the value for tbl_user.centers_id
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public void setCentersId(String centersId) {
		this.centersId = centersId == null ? null : centersId.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_user.room_id
	 * 
	 * @return the value of tbl_user.room_id
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public String getRoomId() {
		return roomId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_user.room_id
	 * 
	 * @param roomId
	 *            the value for tbl_user.room_id
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public void setRoomId(String roomId) {
		this.roomId = roomId == null ? null : roomId.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_user.group_id
	 * 
	 * @return the value of tbl_user.group_id
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public String getGroupId() {
		return groupId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_user.group_id
	 * 
	 * @param groupId
	 *            the value for tbl_user.group_id
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public void setGroupId(String groupId) {
		this.groupId = groupId == null ? null : groupId.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_user.first_name
	 * 
	 * @return the value of tbl_user.first_name
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_user.first_name
	 * 
	 * @param firstName
	 *            the value for tbl_user.first_name
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName == null ? null : firstName.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_user.middle_name
	 * 
	 * @return the value of tbl_user.middle_name
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public String getMiddleName() {
		return middleName;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_user.middle_name
	 * 
	 * @param middleName
	 *            the value for tbl_user.middle_name
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName == null ? null : middleName.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_user.last_name
	 * 
	 * @return the value of tbl_user.last_name
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_user.last_name
	 * 
	 * @param lastName
	 *            the value for tbl_user.last_name
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName == null ? null : lastName.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_user.avatar
	 * 
	 * @return the value of tbl_user.avatar
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	@JsonSerialize(using = S3JsonSerializer.class)
	public String getAvatar() {
		return avatar;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_user.avatar
	 * 
	 * @param avatar
	 *            the value for tbl_user.avatar
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public void setAvatar(String avatar) {
		this.avatar = avatar == null ? null : avatar.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_user.email
	 * 
	 * @return the value of tbl_user.email
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_user.email
	 * 
	 * @param email
	 *            the value for tbl_user.email
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public void setEmail(String email) {
		this.email = email == null ? null : email.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_user.gender
	 * 
	 * @return the value of tbl_user.gender
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public Short getGender() {
		return gender;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_user.gender
	 * 
	 * @param gender
	 *            the value for tbl_user.gender
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public void setGender(Short gender) {
		this.gender = gender;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_user.birthday
	 * 
	 * @return the value of tbl_user.birthday
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	@JsonSerialize(using = DateJsonSerializer2.class, include = JsonSerialize.Inclusion.ALWAYS)
	public Date getBirthday() {
		return birthday;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_user.birthday
	 * 
	 * @param birthday
	 *            the value for tbl_user.birthday
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_user.birth_cert
	 * 
	 * @return the value of tbl_user.birth_cert
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public String getBirthCert() {
		return birthCert;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_user.birth_cert
	 * 
	 * @param birthCert
	 *            the value for tbl_user.birth_cert
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public void setBirthCert(String birthCert) {
		this.birthCert = birthCert == null ? null : birthCert.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_user.address
	 * 
	 * @return the value of tbl_user.address
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_user.address
	 * 
	 * @param address
	 *            the value for tbl_user.address
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public void setAddress(String address) {
		this.address = address == null ? null : address.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_user.suburb
	 * 
	 * @return the value of tbl_user.suburb
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public String getSuburb() {
		return suburb;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_user.suburb
	 * 
	 * @param suburb
	 *            the value for tbl_user.suburb
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public void setSuburb(String suburb) {
		this.suburb = suburb == null ? null : suburb.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_user.postcode
	 * 
	 * @return the value of tbl_user.postcode
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public String getPostcode() {
		return postcode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_user.postcode
	 * 
	 * @param postcode
	 *            the value for tbl_user.postcode
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public void setPostcode(String postcode) {
		this.postcode = postcode == null ? null : postcode.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_user.state
	 * 
	 * @return the value of tbl_user.state
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public String getState() {
		return state;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_user.state
	 * 
	 * @param state
	 *            the value for tbl_user.state
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public void setState(String state) {
		this.state = state == null ? null : state.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_user.family_status
	 * 
	 * @return the value of tbl_user.family_status
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public Short getFamilyStatus() {
		return familyStatus;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_user.family_status
	 * 
	 * @param familyStatus
	 *            the value for tbl_user.family_status
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public void setFamilyStatus(Short familyStatus) {
		this.familyStatus = familyStatus;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_user.family_status_other
	 * 
	 * @return the value of tbl_user.family_status_other
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public String getFamilyStatusOther() {
		return familyStatusOther;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_user.family_status_other
	 * 
	 * @param familyStatusOther
	 *            the value for tbl_user.family_status_other
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public void setFamilyStatusOther(String familyStatusOther) {
		this.familyStatusOther = familyStatusOther == null ? null : familyStatusOther.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_user.phone_number
	 * 
	 * @return the value of tbl_user.phone_number
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_user.phone_number
	 * 
	 * @param phoneNumber
	 *            the value for tbl_user.phone_number
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber == null ? null : phoneNumber.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_user.mobile_number
	 * 
	 * @return the value of tbl_user.mobile_number
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_user.mobile_number
	 * 
	 * @param mobileNumber
	 *            the value for tbl_user.mobile_number
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber == null ? null : mobileNumber.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_user.marital_status
	 * 
	 * @return the value of tbl_user.marital_status
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public Short getMaritalStatus() {
		return maritalStatus;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_user.marital_status
	 * 
	 * @param maritalStatus
	 *            the value for tbl_user.marital_status
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public void setMaritalStatus(Short maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_user.ethnicity
	 * 
	 * @return the value of tbl_user.ethnicity
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public String getEthnicity() {
		return ethnicity;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_user.ethnicity
	 * 
	 * @param ethnicity
	 *            the value for tbl_user.ethnicity
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public void setEthnicity(String ethnicity) {
		this.ethnicity = ethnicity == null ? null : ethnicity.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_user.children_num
	 * 
	 * @return the value of tbl_user.children_num
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public String getChildrenNum() {
		return childrenNum;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_user.children_num
	 * 
	 * @param childrenNum
	 *            the value for tbl_user.children_num
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public void setChildrenNum(String childrenNum) {
		this.childrenNum = childrenNum == null ? null : childrenNum.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_user.children_age
	 * 
	 * @return the value of tbl_user.children_age
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public String getChildrenAge() {
		return childrenAge;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_user.children_age
	 * 
	 * @param childrenAge
	 *            the value for tbl_user.children_age
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public void setChildrenAge(String childrenAge) {
		this.childrenAge = childrenAge == null ? null : childrenAge.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_user.contact_email
	 * 
	 * @return the value of tbl_user.contact_email
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public Boolean getContactEmail() {
		return contactEmail;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_user.contact_email
	 * 
	 * @param contactEmail
	 *            the value for tbl_user.contact_email
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public void setContactEmail(Boolean contactEmail) {
		this.contactEmail = contactEmail;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_user.contact_letter
	 * 
	 * @return the value of tbl_user.contact_letter
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public Boolean getContactLetter() {
		return contactLetter;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_user.contact_letter
	 * 
	 * @param contactLetter
	 *            the value for tbl_user.contact_letter
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public void setContactLetter(Boolean contactLetter) {
		this.contactLetter = contactLetter;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_user.contact_person
	 * 
	 * @return the value of tbl_user.contact_person
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public Boolean getContactPerson() {
		return contactPerson;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_user.contact_person
	 * 
	 * @param contactPerson
	 *            the value for tbl_user.contact_person
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public void setContactPerson(Boolean contactPerson) {
		this.contactPerson = contactPerson;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_user.contact_phone
	 * 
	 * @return the value of tbl_user.contact_phone
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public Boolean getContactPhone() {
		return contactPhone;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_user.contact_phone
	 * 
	 * @param contactPhone
	 *            the value for tbl_user.contact_phone
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public void setContactPhone(Boolean contactPhone) {
		this.contactPhone = contactPhone;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_user.work_phone_number
	 * 
	 * @return the value of tbl_user.work_phone_number
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public String getWorkPhoneNumber() {
		return workPhoneNumber;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_user.work_phone_number
	 * 
	 * @param workPhoneNumber
	 *            the value for tbl_user.work_phone_number
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public void setWorkPhoneNumber(String workPhoneNumber) {
		this.workPhoneNumber = workPhoneNumber == null ? null : workPhoneNumber.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_user.occupation
	 * 
	 * @return the value of tbl_user.occupation
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public String getOccupation() {
		return occupation;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_user.occupation
	 * 
	 * @param occupation
	 *            the value for tbl_user.occupation
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public void setOccupation(String occupation) {
		this.occupation = occupation == null ? null : occupation.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_user.place_of_work
	 * 
	 * @return the value of tbl_user.place_of_work
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public String getPlaceOfWork() {
		return placeOfWork;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_user.place_of_work
	 * 
	 * @param placeOfWork
	 *            the value for tbl_user.place_of_work
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public void setPlaceOfWork(String placeOfWork) {
		this.placeOfWork = placeOfWork == null ? null : placeOfWork.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_user.languages_at_home
	 * 
	 * @return the value of tbl_user.languages_at_home
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public String getLanguagesAtHome() {
		return languagesAtHome;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_user.languages_at_home
	 * 
	 * @param languagesAtHome
	 *            the value for tbl_user.languages_at_home
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public void setLanguagesAtHome(String languagesAtHome) {
		this.languagesAtHome = languagesAtHome == null ? null : languagesAtHome.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_user.user_type
	 * 
	 * @return the value of tbl_user.user_type
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public Short getUserType() {
		return userType;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_user.user_type
	 * 
	 * @param userType
	 *            the value for tbl_user.user_type
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public void setUserType(Short userType) {
		this.userType = userType;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_user.create_time
	 * 
	 * @return the value of tbl_user.create_time
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_user.create_time
	 * 
	 * @param createTime
	 *            the value for tbl_user.create_time
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_user.create_account_id
	 * 
	 * @return the value of tbl_user.create_account_id
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public String getCreateAccountId() {
		return createAccountId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_user.create_account_id
	 * 
	 * @param createAccountId
	 *            the value for tbl_user.create_account_id
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public void setCreateAccountId(String createAccountId) {
		this.createAccountId = createAccountId == null ? null : createAccountId.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_user.update_time
	 * 
	 * @return the value of tbl_user.update_time
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public Date getUpdateTime() {
		return updateTime;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_user.update_time
	 * 
	 * @param updateTime
	 *            the value for tbl_user.update_time
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_user.update_account_id
	 * 
	 * @return the value of tbl_user.update_account_id
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public String getUpdateAccountId() {
		return updateAccountId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_user.update_account_id
	 * 
	 * @param updateAccountId
	 *            the value for tbl_user.update_account_id
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public void setUpdateAccountId(String updateAccountId) {
		this.updateAccountId = updateAccountId == null ? null : updateAccountId.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column tbl_user.delete_flag
	 * 
	 * @return the value of tbl_user.delete_flag
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public Short getDeleteFlag() {
		return deleteFlag;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column tbl_user.delete_flag
	 * 
	 * @param deleteFlag
	 *            the value for tbl_user.delete_flag
	 * 
	 * @mbggenerated Thu Jul 28 18:37:22 CST 2016
	 */
	public void setDeleteFlag(Short deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public String getFamilyId() {
		return familyId;
	}

	public void setFamilyId(String familyId) {
		this.familyId = familyId;
	}

	public String getFullName() {
		if (StringUtil.isNotEmpty(fullName)) {
			return fullName;
		}
		return getFirstName() + " " + ((null == getMiddleName() || "".equals(getMiddleName().trim())) ? getLastName() : getMiddleName() + " " + getLastName());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserInfo other = (UserInfo) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}