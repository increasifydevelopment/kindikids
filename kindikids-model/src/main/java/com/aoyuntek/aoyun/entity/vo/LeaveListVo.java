package com.aoyuntek.aoyun.entity.vo;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.aoyuntek.aoyun.entity.po.LeaveInfo;
import com.aoyuntek.framework.deserializer.S3JsonSerializer;

/**
 * @description Leave列表返回实体
 * @author hxzhang
 * @create 2016年8月16日下午3:06:42
 * @version 1.0
 */
public class LeaveListVo extends LeaveInfo {
    /**
     * 序列化
     */
    private static final long serialVersionUID = 1L;
    private String userId;
    /**
     * 请假人姓名
     */
    private String userName;
    /**
     * 请假人所在园区ID
     */
    private String centreId;
    /**
     * 请假人所在园区
     */
    private String centreName;
    /**
     * 请假人角色
     */
    private String roleName;
    /**
     * 请假人头像
     */
    private String avatar;

    private String personColor;

    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LeaveListVo() {

    }

    public LeaveListVo(String userName, String avatar, String personColor) {
        this.userName = userName;
        this.avatar = avatar;
        this.personColor = personColor;
    }

    @JsonSerialize(using = S3JsonSerializer.class)
    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCentreName() {
        return centreName;
    }

    public void setCentreName(String centreName) {
        this.centreName = centreName;
    }

    public String getCentreId() {
        return centreId;
    }

    public void setCentreId(String centreId) {
        this.centreId = centreId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getPersonColor() {
        return personColor;
    }

    public void setPersonColor(String personColor) {
        this.personColor = personColor;
    }

}
