package com.aoyuntek.aoyun.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aoyuntek.aoyun.entity.po.program.ProgramLessonInfo;
import com.aoyuntek.framework.model.BaseModel;

public class ProgramLessonInfoVo extends BaseModel {

	private static final long serialVersionUID = 1L;

	private ProgramLessonInfo programLessonInfo;

	private List<FileVo> images;

	private List<String> tagAccountIds = new ArrayList<String>();

	private List<NqsVo> nqsVoList;

	private List<SelecterPo> selecterPos;

	public ProgramLessonInfo getProgramLessonInfo() {
		return programLessonInfo;
	}

	public void setProgramLessonInfo(ProgramLessonInfo programLessonInfo) {
		this.programLessonInfo = programLessonInfo;
	}

	public List<FileVo> getImages() {
		return images;
	}

	public void setImages(List<FileVo> images) {
		this.images = images;
	}

	public List<String> getTagAccountIds() {
		return tagAccountIds;
	}

	public void setTagAccountIds(List<String> tagAccountIds) {
		this.tagAccountIds = tagAccountIds;
	}

	public List<NqsVo> getNqsVoList() {
		return nqsVoList;
	}

	public void setNqsVoList(List<NqsVo> nqsVoList) {
		this.nqsVoList = nqsVoList;
	}

	public List<SelecterPo> getSelecterPos() {
		return selecterPos;
	}

	public void setSelecterPos(List<SelecterPo> selecterPos) {
		this.selecterPos = selecterPos;
	}

}
