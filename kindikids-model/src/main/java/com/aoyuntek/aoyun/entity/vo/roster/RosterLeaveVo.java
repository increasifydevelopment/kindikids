package com.aoyuntek.aoyun.entity.vo.roster;

import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.aoyuntek.aoyun.entity.po.roster.RosterLeaveItemInfo;
import com.aoyuntek.framework.deserializer.S3JsonSerializer;
import com.aoyuntek.framework.model.BaseModel;

public class RosterLeaveVo extends BaseModel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String userName;

    private String avatar;

    private String personColor;

    private String accountId;

    private List<RosterLeaveItemInfo> itemList;

    public List<RosterLeaveItemInfo> getItemList() {
        return itemList;
    }

    public void setItemList(List<RosterLeaveItemInfo> itemList) {
        this.itemList = itemList;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @JsonSerialize(using = S3JsonSerializer.class)
    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getPersonColor() {
        return personColor;
    }

    public void setPersonColor(String personColor) {
        this.personColor = personColor;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

}
