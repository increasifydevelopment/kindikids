package com.aoyuntek.aoyun.entity.vo.task;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.task.TaskFollowInstanceInfo;
import com.aoyuntek.aoyun.entity.vo.NqsVo;
import com.aoyuntek.framework.model.BaseModel;

public class TaskInstanceVo extends BaseModel {

	private static final long serialVersionUID = 1L;

	private String id;

	private String formId;

	private String valueId;

	private Short statu;

	private String taskName;

	private List<TaskFollowInstanceInfo> followInstanceInfos;

	private Boolean operationFlag = false;

	private List<NqsVo> nqsVos;

	public List<NqsVo> getNqsVos() {
		return nqsVos;
	}

	public void setNqsVos(List<NqsVo> nqsVos) {
		this.nqsVos = nqsVos;
	}

	public Boolean getOperationFlag() {
		return operationFlag;
	}

	public void setOperationFlag(Boolean operationFlag) {
		this.operationFlag = operationFlag;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public Short getStatu() {
		return statu;
	}

	public void setStatu(Short statu) {
		this.statu = statu;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFormId() {
		return formId;
	}

	public void setFormId(String formId) {
		this.formId = formId;
	}

	public String getValueId() {
		return valueId;
	}

	public void setValueId(String valueId) {
		this.valueId = valueId;
	}

	public List<TaskFollowInstanceInfo> getFollowInstanceInfos() {
		return followInstanceInfos;
	}

	public void setFollowInstanceInfos(List<TaskFollowInstanceInfo> followInstanceInfos) {
		this.followInstanceInfos = followInstanceInfos;
	}
}
