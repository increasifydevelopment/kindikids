package com.aoyuntek.aoyun.entity.po;

import java.util.Date;
import java.util.List;

import com.aoyuntek.aoyun.entity.vo.RequestLogVo;
import com.aoyuntek.aoyun.entity.vo.child.HistoryVo;

public class PreviousEnrolment {
	private String id;

	private Date applicationDate;

	private Date requestedDate;

	private Date orientationDate;

	private Date firstDay;

	private Date requestedLastDay;

	private Date lastDay;

	private String userId;

	private Short status;

	private Short enrolled;

	private Short type;

	private Date createTime;

	private Boolean deleteFlag;

	private List<RequestLogVo> requestLogVos;

	private HistoryVo historyVo;

	private List<EmailMsg> emailMsgList;

	public Short getStatus() {
		return status;
	}

	public void setStatus(Short status) {
		this.status = status;
	}

	public Short getEnrolled() {
		return enrolled;
	}

	public void setEnrolled(Short enrolled) {
		this.enrolled = enrolled;
	}

	public Short getType() {
		return type;
	}

	public void setType(Short type) {
		this.type = type;
	}

	public List<RequestLogVo> getRequestLogVos() {
		return requestLogVos;
	}

	public void setRequestLogVos(List<RequestLogVo> requestLogVos) {
		this.requestLogVos = requestLogVos;
	}

	public HistoryVo getHistoryVo() {
		return historyVo;
	}

	public void setHistoryVo(HistoryVo historyVo) {
		this.historyVo = historyVo;
	}

	public List<EmailMsg> getEmailMsgList() {
		return emailMsgList;
	}

	public void setEmailMsgList(List<EmailMsg> emailMsgList) {
		this.emailMsgList = emailMsgList;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id == null ? null : id.trim();
	}

	public Date getApplicationDate() {
		return applicationDate;
	}

	public void setApplicationDate(Date applicationDate) {
		this.applicationDate = applicationDate;
	}

	public Date getRequestedDate() {
		return requestedDate;
	}

	public void setRequestedDate(Date requestedDate) {
		this.requestedDate = requestedDate;
	}

	public Date getOrientationDate() {
		return orientationDate;
	}

	public void setOrientationDate(Date orientationDate) {
		this.orientationDate = orientationDate;
	}

	public Date getFirstDay() {
		return firstDay;
	}

	public void setFirstDay(Date firstDay) {
		this.firstDay = firstDay;
	}

	public Date getRequestedLastDay() {
		return requestedLastDay;
	}

	public void setRequestedLastDay(Date requestedLastDay) {
		this.requestedLastDay = requestedLastDay;
	}

	public Date getLastDay() {
		return lastDay;
	}

	public void setLastDay(Date lastDay) {
		this.lastDay = lastDay;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId == null ? null : userId.trim();
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Boolean getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(Boolean deleteFlag) {
		this.deleteFlag = deleteFlag;
	}
}