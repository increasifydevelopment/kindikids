package com.aoyuntek.aoyun.entity.vo;

import java.util.ArrayList;
import java.util.List;

import com.aoyuntek.aoyun.entity.po.FamilyInfo;
import com.aoyuntek.aoyun.entity.po.UserInfo;

public class FileListVo extends FamilyInfo {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private List<UserInfo> users = new ArrayList<UserInfo>();

    public List<UserInfo> getUsers() {
        return users;
    }

    public void setUsers(List<UserInfo> users) {
        this.users = users;
    }

}
