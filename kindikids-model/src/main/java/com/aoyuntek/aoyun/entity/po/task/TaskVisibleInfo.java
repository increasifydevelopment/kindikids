package com.aoyuntek.aoyun.entity.po.task;

import com.aoyuntek.framework.model.BaseModel;

public class TaskVisibleInfo extends BaseModel {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String objId;

    private String sourceId;

    private Short type;

    public String getObjId() {
        return objId;
    }

    public void setObjId(String objId) {
        this.objId = objId == null ? null : objId.trim();
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId == null ? null : sourceId.trim();
    }

    public Short getType() {
        return type;
    }

    public void setType(Short type) {
        this.type = type;
    }
}