package com.aoyuntek.aoyun.entity.vo;

import java.util.Date;
import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.aoyuntek.framework.deserializer.DateJsonSerializer3;
import com.aoyuntek.framework.deserializer.S3JsonSerializer;
import com.aoyuntek.framework.model.BaseModel;

public class SigninInfoVo extends BaseModel {
	private String id;
	private String firstName;
	private String middleName;
	private String lastName;
	private String reason;
	private String centreId;
	private String organisation;
	private String contactNum;
	private Date inTime;
	private Date outTime;
	private String signInName;
	private String signOutName;
	private Date updateTime;
	private String updateAccountId;
	private String signinId;
	private String signoutId;
	private String personColor;
	private String accountId;
	private String centreName;
	private String roomName;
	private String groupName;
	private Boolean sunscreenApp;
	private String avatar;
	private Boolean generationSign;
	private String generationSignAccountId;
	private Boolean generationSignOut;
	private String generationSignOutAccountId;
	private List<SelecterPo> selecterPos;
	private List<SelecterPo> selecterPosOut;
	private boolean interim = false;

	public boolean isInterim() {
		return interim;
	}

	public void setInterim(boolean interim) {
		this.interim = interim;
	}

	public Boolean getGenerationSignOut() {
		return generationSignOut;
	}

	public void setGenerationSignOut(Boolean generationSignOut) {
		this.generationSignOut = generationSignOut;
	}

	public String getGenerationSignOutAccountId() {
		return generationSignOutAccountId;
	}

	public void setGenerationSignOutAccountId(String generationSignOutAccountId) {
		this.generationSignOutAccountId = generationSignOutAccountId;
	}

	public List<SelecterPo> getSelecterPosOut() {
		return selecterPosOut;
	}

	public void setSelecterPosOut(List<SelecterPo> selecterPosOut) {
		this.selecterPosOut = selecterPosOut;
	}

	public String getGenerationSignAccountId() {
		return generationSignAccountId;
	}

	public void setGenerationSignAccountId(String generationSignAccountId) {
		this.generationSignAccountId = generationSignAccountId;
	}

	public Boolean getGenerationSign() {
		return generationSign;
	}

	public void setGenerationSign(Boolean generationSign) {
		this.generationSign = generationSign;
	}

	public List<SelecterPo> getSelecterPos() {
		return selecterPos;
	}

	public void setSelecterPos(List<SelecterPo> selecterPos) {
		this.selecterPos = selecterPos;
	}

	@JsonSerialize(using = S3JsonSerializer.class)
	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public Boolean getSunscreenApp() {
		return sunscreenApp;
	}

	public void setSunscreenApp(Boolean sunscreenApp) {
		this.sunscreenApp = sunscreenApp;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getCentreName() {
		return centreName;
	}

	public void setCentreName(String centreName) {
		this.centreName = centreName;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getPersonColor() {
		return personColor;
	}

	public void setPersonColor(String personColor) {
		this.personColor = personColor;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getCentreId() {
		return centreId;
	}

	public void setCentreId(String centreId) {
		this.centreId = centreId;
	}

	public String getOrganisation() {
		return organisation;
	}

	public void setOrganisation(String organisation) {
		this.organisation = organisation;
	}

	public String getContactNum() {
		return contactNum;
	}

	public void setContactNum(String contactNum) {
		this.contactNum = contactNum;
	}

	@JsonSerialize(using = DateJsonSerializer3.class, include = JsonSerialize.Inclusion.ALWAYS)
	public Date getInTime() {
		return inTime;
	}

	public void setInTime(Date inTime) {
		this.inTime = inTime;
	}

	@JsonSerialize(using = DateJsonSerializer3.class, include = JsonSerialize.Inclusion.ALWAYS)
	public Date getOutTime() {
		return outTime;
	}

	public void setOutTime(Date outTime) {
		this.outTime = outTime;
	}

	public String getSignInName() {
		return signInName;
	}

	public void setSignInName(String signInName) {
		this.signInName = signInName;
	}

	public String getSignOutName() {
		return signOutName;
	}

	public void setSignOutName(String signOutName) {
		this.signOutName = signOutName;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getUpdateAccountId() {
		return updateAccountId;
	}

	public void setUpdateAccountId(String updateAccountId) {
		this.updateAccountId = updateAccountId;
	}

	public String getSigninId() {
		return signinId;
	}

	public void setSigninId(String signinId) {
		this.signinId = signinId;
	}

	public String getSignoutId() {
		return signoutId;
	}

	public void setSignoutId(String signoutId) {
		this.signoutId = signoutId;
	}

}
