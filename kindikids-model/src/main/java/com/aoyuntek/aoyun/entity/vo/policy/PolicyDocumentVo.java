package com.aoyuntek.aoyun.entity.vo.policy;

import com.aoyuntek.aoyun.entity.po.policy.PolicyDocumentsInfo;
import com.aoyuntek.aoyun.entity.vo.FileVo;

public class PolicyDocumentVo extends PolicyDocumentsInfo {
	
	private static final long serialVersionUID = 1L;
			
	private String fileName;
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getAttachId() {
		return attachId;
	}
	public void setAttachId(String attachId) {
		this.attachId = attachId;
	}
	public String getVisitUrl() {
		return visitUrl;
	}
	public void setVisitUrl(String visitUrl) {
		this.visitUrl = visitUrl;
	}
	private String attachId;
	private String visitUrl;
}
