package com.aoyuntek.aoyun.entity.vo;

import java.util.Date;
import java.util.List;

import com.aoyuntek.aoyun.entity.po.program.ProgramRegisterItemInfo;
import com.aoyuntek.aoyun.entity.po.program.ProgramRegisterSleepInfo;
import com.aoyuntek.framework.model.BaseModel;

public class ProgramRegisterItemInfoVo extends BaseModel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private ProgramRegisterItemInfo programRegisterItemInfo;

    private ProgramRegisterSleepInfo programRegisterSleepInfo;

    private List<SelecterPo> programRegisterItemUsers;
    
    private Date changeTime;
    
    private String content;

    public Date getChangeTime() {
        return changeTime;
    }

    public void setChangeTime(Date changeTime) {
        this.changeTime = changeTime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public ProgramRegisterItemInfo getProgramRegisterItemInfo() {
        return programRegisterItemInfo;
    }

    public void setProgramRegisterItemInfo(ProgramRegisterItemInfo programRegisterItemInfo) {
        this.programRegisterItemInfo = programRegisterItemInfo;
    }

    public ProgramRegisterSleepInfo getProgramRegisterSleepInfo() {
        return programRegisterSleepInfo;
    }

    public void setProgramRegisterSleepInfo(ProgramRegisterSleepInfo programRegisterSleepInfo) {
        this.programRegisterSleepInfo = programRegisterSleepInfo;
    }

    public List<SelecterPo> getProgramRegisterItemUsers() {
        return programRegisterItemUsers;
    }

    public void setProgramRegisterItemUsers(List<SelecterPo> programRegisterItemUsers) {
        this.programRegisterItemUsers = programRegisterItemUsers;
    }


}
