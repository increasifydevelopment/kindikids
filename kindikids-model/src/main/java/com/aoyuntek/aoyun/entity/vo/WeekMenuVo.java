package com.aoyuntek.aoyun.entity.vo;

import java.util.List;

import com.aoyuntek.framework.model.BaseModel;

public class WeekMenuVo extends BaseModel{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private List<WeekNutrionalMenuVo> list;
    
    private String menuName;
    
    private String menuId;

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public List<WeekNutrionalMenuVo> getList() {
        return list;
    }

    public void setList(List<WeekNutrionalMenuVo> list) {
        this.list = list;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }
    
}
