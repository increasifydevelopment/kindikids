package com.aoyuntek.aoyun.entity.vo.waiting;

public class SubmitModel {
	private String objId;
	private short fromType;
	private short moveToType;
	private String familyId;
	private short familyToType;
	private boolean isPlan = false;

	public short getFamilyToType() {
		return familyToType;
	}

	public void setFamilyToType(short familyToType) {
		this.familyToType = familyToType;
	}

	public boolean isPlan() {
		return isPlan;
	}

	public void setPlan(boolean isPlan) {
		this.isPlan = isPlan;
	}

	public String getFamilyId() {
		return familyId;
	}

	public void setFamilyId(String familyId) {
		this.familyId = familyId;
	}

	public String getObjId() {
		return objId;
	}

	public void setObjId(String objId) {
		this.objId = objId;
	}

	public short getFromType() {
		return fromType;
	}

	public void setFromType(short fromType) {
		this.fromType = fromType;
	}

	public short getMoveToType() {
		return moveToType;
	}

	public void setMoveToType(short moveToType) {
		this.moveToType = moveToType;
	}

}
