package com.aoyuntek.aoyun.entity.vo.waiting;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.aoyuntek.aoyun.condtion.waitingList.WaitingListCondition;

public class WaitingResultVo {
	private WaitingListCondition condition;
	private List<WaitingListVo> applicationList;
	private List<WaitingListVo> externalList;
	private List<WaitingListVo> siblingList;
	private List<WaitingListVo> archivedList;

	public WaitingListCondition getCondition() {
		return condition;
	}

	public void setCondition(WaitingListCondition condition) {
		this.condition = condition;
	}

	public List<WaitingListVo> getApplicationList() {
		return applicationList;
	}

	public void setApplicationList(List<WaitingListVo> applicationList) {
		this.applicationList = applicationList;
	}

	public List<WaitingListVo> getExternalList() {
		return externalList;
	}

	public void setExternalList(List<WaitingListVo> externalList) {
		this.externalList = externalList;
	}

	public List<WaitingListVo> getSiblingList() {
		return siblingList;
	}

	public void setSiblingList(List<WaitingListVo> siblingList) {
		this.siblingList = siblingList;
	}

	public List<WaitingListVo> getArchivedList() {
		return archivedList;
	}

	public void setArchivedList(List<WaitingListVo> archivedList) {
		this.archivedList = archivedList;
	}

	public WaitingResultVo() {

	}

	public WaitingResultVo(WaitingListCondition c, List<WaitingListVo> list1, List<WaitingListVo> list2, List<WaitingListVo> list3,
			List<WaitingListVo> list4) {
		this.condition = c;
		this.applicationList = list1;
		this.externalList = list2;
		this.siblingList = list3;
		this.archivedList = list4;
	}

	private void dealSort(List<WaitingListVo> list) {
		Collections.sort(list, new Comparator<WaitingListVo>() {
			@Override
			public int compare(WaitingListVo o1, WaitingListVo o2) {
				if (o1.getApplicationDatePosition().before(o2.getApplicationDatePosition())) {
					return -1;
				}
				return 1;
			}
		});
	}

}
