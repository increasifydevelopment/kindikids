package com.aoyuntek.aoyun.entity.po.roster;

import java.util.Date;

import com.aoyuntek.framework.model.BaseModel;

public class RosterStaffInfo extends BaseModel {

	private static final long serialVersionUID = 1L;

	private String id;

	private String shiftTimeCode;

	private String rosterId;

	private Date day;

	private String staffAccountId;

	private Short staffType;

	private Date createTime;

	private String createAccountId;

	private Date updateTime;

	private String updateAccountId;

	private Short deleteFlag;

	private Date startTime;

	private Date endTime;

	private String shiftId;

	private boolean confirm = false;

	public boolean isConfirm() {
		return confirm;
	}

	public void setConfirm(boolean confirm) {
		this.confirm = confirm;
	}

	public String getShiftId() {
		return shiftId;
	}

	public void setShiftId(String shiftId) {
		this.shiftId = shiftId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id == null ? null : id.trim();
	}

	public String getShiftTimeCode() {
		return shiftTimeCode;
	}

	public void setShiftTimeCode(String shiftTimeCode) {
		this.shiftTimeCode = shiftTimeCode == null ? null : shiftTimeCode.trim();
	}

	public String getRosterId() {
		return rosterId;
	}

	public void setRosterId(String rosterId) {
		this.rosterId = rosterId == null ? null : rosterId.trim();
	}

	public Date getDay() {
		return day;
	}

	public void setDay(Date day) {
		this.day = day;
	}

	public String getStaffAccountId() {
		return staffAccountId;
	}

	public void setStaffAccountId(String staffAccountId) {
		this.staffAccountId = staffAccountId == null ? null : staffAccountId.trim();
	}

	public Short getStaffType() {
		return staffType;
	}

	public void setStaffType(Short staffType) {
		this.staffType = staffType;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCreateAccountId() {
		return createAccountId;
	}

	public void setCreateAccountId(String createAccountId) {
		this.createAccountId = createAccountId == null ? null : createAccountId.trim();
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getUpdateAccountId() {
		return updateAccountId;
	}

	public void setUpdateAccountId(String updateAccountId) {
		this.updateAccountId = updateAccountId == null ? null : updateAccountId.trim();
	}

	public Short getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(Short deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
}