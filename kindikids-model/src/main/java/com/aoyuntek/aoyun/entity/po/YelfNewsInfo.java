package com.aoyuntek.aoyun.entity.po;

import com.aoyuntek.framework.model.BaseModel;

public class YelfNewsInfo extends BaseModel {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    /**
     * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_yelf_news.id
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    private String id;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_yelf_news.news_id
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    private String newsId;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_yelf_news.yelf_version
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    private String yelfVersion;

    /**
     * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_yelf_news.delete_flag
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    private Short deleteFlag;

    /**
     * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_yelf_news.id
     * 
     * @return the value of tbl_yelf_news.id
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    public String getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_yelf_news.id
     * 
     * @param id
     *            the value for tbl_yelf_news.id
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_yelf_news.news_id
     * 
     * @return the value of tbl_yelf_news.news_id
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    public String getNewsId() {
        return newsId;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_yelf_news.news_id
     * 
     * @param newsId
     *            the value for tbl_yelf_news.news_id
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    public void setNewsId(String newsId) {
        this.newsId = newsId == null ? null : newsId.trim();
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_yelf_news.yelf_version
     * 
     * @return the value of tbl_yelf_news.yelf_version
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    public String getYelfVersion() {
        return yelfVersion;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_yelf_news.yelf_version
     * 
     * @param yelfVersion
     *            the value for tbl_yelf_news.yelf_version
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    public void setYelfVersion(String yelfVersion) {
        this.yelfVersion = yelfVersion == null ? null : yelfVersion.trim();
    }

    /**
     * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_yelf_news.delete_flag
     * 
     * @return the value of tbl_yelf_news.delete_flag
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    public Short getDeleteFlag() {
        return deleteFlag;
    }

    /**
     * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_yelf_news.delete_flag
     * 
     * @param deleteFlag
     *            the value for tbl_yelf_news.delete_flag
     * 
     * @mbggenerated Thu Jul 07 19:41:59 CST 2016
     */
    public void setDeleteFlag(Short deleteFlag) {
        this.deleteFlag = deleteFlag;
    }
}