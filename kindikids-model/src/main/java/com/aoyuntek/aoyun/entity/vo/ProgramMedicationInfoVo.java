package com.aoyuntek.aoyun.entity.vo;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.program.ProgramMedicationInfoWithBLOBs;
import com.aoyuntek.aoyun.entity.po.program.TimeMedicationInfo;
import com.aoyuntek.framework.model.BaseModel;

public class ProgramMedicationInfoVo extends BaseModel{

    private static final long serialVersionUID = 1L;
    
    private String userName;
    
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    private ProgramMedicationInfoWithBLOBs programMedicationInfoWithBLOBs;
 
    private List<TimeMedicationInfo> timeMedicationInfos;
    
    private List<SelecterPo> child;
    
    private List<SelecterPo> staffReceiving;
    
    private List<SelecterPo> assignToStaff;
    
    public List<SelecterPo> getChild() {
        return child;
    }

    public void setChild(List<SelecterPo> child) {
        this.child = child;
    }

    public List<SelecterPo> getStaffReceiving() {
        return staffReceiving;
    }

    public void setStaffReceiving(List<SelecterPo> staffReceiving) {
        this.staffReceiving = staffReceiving;
    }

    public List<SelecterPo> getAssignToStaff() {
        return assignToStaff;
    }

    public void setAssignToStaff(List<SelecterPo> assignToStaff) {
        this.assignToStaff = assignToStaff;
    }

    public ProgramMedicationInfoWithBLOBs getProgramMedicationInfoWithBLOBs() {
        return programMedicationInfoWithBLOBs;
    }

    public void setProgramMedicationInfoWithBLOBs(ProgramMedicationInfoWithBLOBs programMedicationInfoWithBLOBs) {
        this.programMedicationInfoWithBLOBs = programMedicationInfoWithBLOBs;
    }

    public List<TimeMedicationInfo> getTimeMedicationInfos() {
        return timeMedicationInfos;
    }

    public void setTimeMedicationInfos(List<TimeMedicationInfo> timeMedicationInfos) {
        this.timeMedicationInfos = timeMedicationInfos;
    }

}
