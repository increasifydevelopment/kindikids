package com.aoyuntek.aoyun.entity.po;

import java.util.Date;
import java.util.List;

import com.aoyuntek.framework.model.BaseModel;

/**
 * 
 * @description 对外新增family
 * @author gfwang
 * @create 2016年8月29日下午8:37:37
 * @version 1.0
 */
public class OutFamilyPo extends BaseModel {

	private static final long serialVersionUID = 1L;
	/**
	 * 小孩信息
	 */
	private UserInfo child;
	/**
	 * 家长信息
	 */
	private List<UserInfo> parentList;
	/**
	 * familyId
	 */
	private String familyId;
	/**
	 * 家庭名称
	 */
	private String familyName;
	/**
	 * date of change
	 */
	private Date changeDate;
	/**
	 * monday
	 */
	private Boolean monday;
	/**
	 * tuesday
	 */
	private Boolean tuesday;
	/**
	 * wednesday
	 */
	private Boolean wednesday;
	/**
	 * thursday
	 */
	private Boolean thursday;
	/**
	 * friday
	 */
	private Boolean friday;

	private Date time;

	private String oldId;

	private List<String> centreList;

	private Date applicationDate;

	private Date applicationDatePosition;

	private boolean siblingFlag;

	public boolean isSiblingFlag() {
		return siblingFlag;
	}

	public void setSiblingFlag(boolean siblingFlag) {
		this.siblingFlag = siblingFlag;
	}

	public Date getApplicationDate() {
		return applicationDate;
	}

	public void setApplicationDate(Date applicationDate) {
		this.applicationDate = applicationDate;
	}

	public Date getApplicationDatePosition() {
		return applicationDatePosition;
	}

	public void setApplicationDatePosition(Date applicationDatePosition) {
		this.applicationDatePosition = applicationDatePosition;
	}

	public String getFamilyId() {
		return familyId;
	}

	public void setFamilyId(String familyId) {
		this.familyId = familyId;
	}

	public List<String> getCentreList() {
		return centreList;
	}

	public void setCentreList(List<String> centreList) {
		this.centreList = centreList;
	}

	public String getOldId() {
		return oldId;
	}

	public void setOldId(String oldId) {
		this.oldId = oldId;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public Boolean getMonday() {
		return monday;
	}

	public void setMonday(Boolean monday) {
		this.monday = monday;
	}

	public Boolean getTuesday() {
		return tuesday;
	}

	public void setTuesday(Boolean tuesday) {
		this.tuesday = tuesday;
	}

	public Boolean getWednesday() {
		return wednesday;
	}

	public void setWednesday(Boolean wednesday) {
		this.wednesday = wednesday;
	}

	public Boolean getThursday() {
		return thursday;
	}

	public void setThursday(Boolean thursday) {
		this.thursday = thursday;
	}

	public Boolean getFriday() {
		return friday;
	}

	public void setFriday(Boolean friday) {
		this.friday = friday;
	}

	public Date getChangeDate() {
		return changeDate;
	}

	public void setChangeDate(Date changeDate) {
		this.changeDate = changeDate;
	}

	public UserInfo getChild() {
		return child;
	}

	public void setChild(UserInfo child) {
		this.child = child;
	}

	public List<UserInfo> getParentList() {
		return parentList;
	}

	public void setParentList(List<UserInfo> parentList) {
		this.parentList = parentList;
	}

	public String getFamilyName() {
		return familyName;
	}

	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
}
