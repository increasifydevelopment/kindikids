package com.aoyuntek.aoyun.entity.vo;

import java.math.BigDecimal;
import java.util.Date;

public class PayrollVo {
	private String id;
	private String firstName;
	private String lastName;
	private String accountId;
	private Date day;
	private Short staffType;
	private String centreName;
	private String centreId;
	private Date startDate;
	private Date endDate;
	private String leaveReason;
	private String approvalName;
	private Short leaveType;
	private BigDecimal hours;
	private Short type;
	private Date sInDate;
	private Date sOutDate;
	private BigDecimal units;
	private String cardId;
	private Short region;

	public Short getRegion() {
		return region;
	}

	public void setRegion(Short region) {
		this.region = region;
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public BigDecimal getUnits() {
		return units;
	}

	public void setUnits(BigDecimal units) {
		this.units = units;
	}

	public Date getsInDate() {
		return sInDate;
	}

	public void setsInDate(Date sInDate) {
		this.sInDate = sInDate;
	}

	public Date getsOutDate() {
		return sOutDate;
	}

	public void setsOutDate(Date sOutDate) {
		this.sOutDate = sOutDate;
	}

	public BigDecimal getHours() {
		return hours;
	}

	public void setHours(BigDecimal hours) {
		this.hours = hours;
	}

	public String getCentreId() {
		return centreId;
	}

	public void setCentreId(String centreId) {
		this.centreId = centreId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public Short getLeaveType() {
		return leaveType;
	}

	public void setLeaveType(Short leaveType) {
		this.leaveType = leaveType;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getDay() {
		return day;
	}

	public void setDay(Date day) {
		this.day = day;
	}

	public Short getStaffType() {
		return staffType;
	}

	public void setStaffType(Short staffType) {
		this.staffType = staffType;
	}

	public String getCentreName() {
		return centreName;
	}

	public void setCentreName(String centreName) {
		this.centreName = centreName;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Short getType() {
		return type;
	}

	public void setType(Short type) {
		this.type = type;
	}

	public String getLeaveReason() {
		return leaveReason;
	}

	public void setLeaveReason(String leaveReason) {
		this.leaveReason = leaveReason;
	}

	public String getApprovalName() {
		return approvalName;
	}

	public void setApprovalName(String approvalName) {
		this.approvalName = approvalName;
	}
}
