package com.aoyuntek.aoyun.entity.vo.dashboard;

import java.math.BigDecimal;
import java.util.List;

import com.aoyuntek.framework.model.BaseModel;

public class DashboardVo extends BaseModel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private BigDecimal bigCount;

    private short type;

    private List<DashboardDataVo> dataList;

    public DashboardVo() {

    }

    public DashboardVo(short type, List<DashboardDataVo> dataList) {
        super();
        this.type = type;
        this.dataList = dataList;
    }

    public BigDecimal getBigCount() {
        return bigCount;
    }

    public void setBigCount(BigDecimal bigCount) {
        this.bigCount = bigCount;
    }

    public short getType() {
        return type;
    }

    public void setType(short type) {
        this.type = type;
    }

    public List<DashboardDataVo> getDataList() {
        return dataList;
    }

    public void setDataList(List<DashboardDataVo> dataList) {
        this.dataList = dataList;
    }

}
