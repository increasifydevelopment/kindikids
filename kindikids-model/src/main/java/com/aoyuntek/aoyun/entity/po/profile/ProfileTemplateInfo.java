package com.aoyuntek.aoyun.entity.po.profile;

import java.util.Date;

import com.aoyuntek.framework.model.BaseModel;

public class ProfileTemplateInfo extends BaseModel {
    /**
     * 序列化
     */
    private static final long serialVersionUID = 1L;

    private String id;

    private String instanceId;

    private Short currtenFlag;

    private String templateName;

    private Short state;

    private Short profileTempType;

    private Short deleteFlag;

    private Date createTime;

    private String createAccountId;

    private Date updateTime;

    private String updateAccountId;

    private String valueId;

    public String getValueId() {
        return valueId;
    }

    public void setValueId(String valueId) {
        this.valueId = valueId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId == null ? null : instanceId.trim();
    }

    public Short getCurrtenFlag() {
        return currtenFlag;
    }

    public void setCurrtenFlag(Short currtenFlag) {
        this.currtenFlag = currtenFlag;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName == null ? null : templateName.trim();
    }

    public Short getState() {
        return state;
    }

    public void setState(Short state) {
        this.state = state;
    }

    public Short getProfileTempType() {
        return profileTempType;
    }

    public void setProfileTempType(Short profileTempType) {
        this.profileTempType = profileTempType;
    }

    public Short getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Short deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateAccountId() {
        return createAccountId;
    }

    public void setCreateAccountId(String createAccountId) {
        this.createAccountId = createAccountId == null ? null : createAccountId.trim();
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateAccountId() {
        return updateAccountId;
    }

    public void setUpdateAccountId(String updateAccountId) {
        this.updateAccountId = updateAccountId == null ? null : updateAccountId.trim();
    }
}