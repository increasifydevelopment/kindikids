package com.aoyuntek.aoyun.entity.po.task;

import com.aoyuntek.framework.model.BaseModel;

public class TaskResponsibleLogInfo extends BaseModel {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String taskInstanceId;

    private String acountId;

    public String getTaskInstanceId() {
        return taskInstanceId;
    }

    public void setTaskInstanceId(String taskInstanceId) {
        this.taskInstanceId = taskInstanceId == null ? null : taskInstanceId.trim();
    }

    public String getAcountId() {
        return acountId;
    }

    public void setAcountId(String acountId) {
        this.acountId = acountId == null ? null : acountId.trim();
    }

    public TaskResponsibleLogInfo() {

    }

    public TaskResponsibleLogInfo(String taskInstanceId, String acountId) {
        this.taskInstanceId = taskInstanceId;
        this.acountId = acountId;
    }
}