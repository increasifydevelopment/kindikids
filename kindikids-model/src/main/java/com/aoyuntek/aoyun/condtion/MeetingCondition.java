package com.aoyuntek.aoyun.condtion;

import java.util.Date;

import com.aoyuntek.framework.condtion.BaseCondition;

public class MeetingCondition extends BaseCondition {
    /**
     * 会议模版名称
     */
    private String name;
    /**
     * 日期
     */
    private Date day;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDay() {
        return day;
    }

    public void setDay(Date day) {
        this.day = day;
    }

}
