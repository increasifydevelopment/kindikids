package com.aoyuntek.aoyun.condtion;

import java.util.Date;
import java.util.List;

import com.aoyuntek.framework.condtion.BaseCondition;

/**
 * event
 * 
 * @author bing
 * 
 */
public class EventCondition extends BaseCondition {
    private String keyword;
    private Date startTime;
    private Date endTime;
    private Short visibility;
    private String createAccountId;
    private List<String> centerIds;
    private Boolean isCeo;
    private String nqsVersion;
    private String selectNqs;
    
    public String getSelectNqs() {
		return selectNqs;
	}

	public void setSelectNqs(String selectNqs) {
		this.selectNqs = selectNqs;
	}

	public String getNqsVersion() {
		return nqsVersion;
	}

	public void setNqsVersion(String nqsVersion) {
		this.nqsVersion = nqsVersion;
	}

	public Boolean getIsCeo() {
        return isCeo;
    }

    public void setIsCeo(Boolean isCeo) {
        this.isCeo = isCeo;
    }

    public List<String> getCenterIds() {
        return centerIds;
    }

    public void setCenterIds(List<String> centerIds) {
        this.centerIds = centerIds;
    }

    public String getCreateAccountId() {
        return createAccountId;
    }

    public void setCreateAccountId(String createAccountId) {
        this.createAccountId = createAccountId;
    }

    public Short getVisibility() {
        return visibility;
    }

    public void setVisibility(Short visibility) {
        this.visibility = visibility;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
}
