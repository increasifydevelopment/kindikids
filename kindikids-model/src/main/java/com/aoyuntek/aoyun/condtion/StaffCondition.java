package com.aoyuntek.aoyun.condtion;

import java.util.ArrayList;
import java.util.List;

import com.aoyuntek.framework.condtion.BaseCondition;

/**
 * @description 员工查询条件实体
 */
public class StaffCondition extends BaseCondition {

	/**
	 * 用户id
	 */
	private String userId;

	/**
	 * 角色id
	 */
	private String roleId;

	/**
	 * 园区id
	 */
	private String centerId;

	/**
	 * 房间id
	 */
	private String roomId;

	/**
	 * 模糊搜索
	 */
	private String params;

	/**
	 * 是否显示casual
	 */
	private Integer casual;

	private Short myStatus = -1;

	private List<String> centreIds = new ArrayList<String>();

	public List<String> getCentreIds() {
		return centreIds;
	}

	public void setCentreIds(List<String> centreIds) {
		this.centreIds = centreIds;
	}

	public Short getMyStatus() {
		return myStatus;
	}

	public void setMyStatus(Short myStatus) {
		this.myStatus = myStatus;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getCenterId() {
		return centerId;
	}

	public void setCenterId(String centerId) {
		this.centerId = centerId;
	}

	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}

	public String getParams() {
		return params;
	}

	public void setParams(String params) {
		this.params = params;
	}

	public Integer getCasual() {
		return casual;
	}

	public void setCasual(Integer casual) {
		this.casual = casual;
	}

	@Override
	public String toString() {
		return "StaffCondition [userId=" + userId + ", roleId=" + roleId + ", centerId=" + centerId + ", roomId=" + roomId + ", params=" + params + ", casual=" + casual
				+ "]";
	}

}