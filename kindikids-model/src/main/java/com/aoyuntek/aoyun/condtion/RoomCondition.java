package com.aoyuntek.aoyun.condtion;

import java.util.List;

import com.aoyuntek.framework.condtion.BaseCondition;

/**
 * 
 * @author dlli5 at 2016年9月20日下午6:54:17
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public class RoomCondition extends BaseCondition {

    private Short status;

    private List<String> centreIds;

    private List<String> roomIds;

    public List<String> getCentreIds() {
        return centreIds;
    }

    public void setCentreIds(List<String> centreIds) {
        this.centreIds = centreIds;
    }

    public List<String> getRoomIds() {
        return roomIds;
    }

    public void setRoomIds(List<String> roomIds) {
        this.roomIds = roomIds;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }
}
