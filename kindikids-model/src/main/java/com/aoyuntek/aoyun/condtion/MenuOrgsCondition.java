package com.aoyuntek.aoyun.condtion;

import java.util.Date;
import java.util.List;

/**
 * 
 * @description 左侧菜单组织信息condition
 * @author gfwang
 * @create 2016年9月8日下午1:42:33
 * @version 1.0
 */
public class MenuOrgsCondition {

    public List<String> centerIds;

    public List<String> roomIds;

    private int openCondtion = -1;

    private Date weekStart;

    public Date getWeekStart() {
        return weekStart;
    }

    public void setWeekStart(Date weekStart) {
        this.weekStart = weekStart;
    }

    public int getOpenCondtion() {
        return openCondtion;
    }

    public void setOpenCondtion(int openCondtion) {
        this.openCondtion = openCondtion;
    }

    public MenuOrgsCondition(List<String> centerIds, List<String> roomIds) {
        super();
        this.centerIds = centerIds;
        this.roomIds = roomIds;
    }

    public MenuOrgsCondition() {
    }

    public List<String> getCenterIds() {
        return centerIds;
    }

    public void setCenterIds(List<String> centerIds) {
        this.centerIds = centerIds;
    }

    public List<String> getRoomIds() {
        return roomIds;
    }

    public void setRoomIds(List<String> roomIds) {
        this.roomIds = roomIds;
    }

}
