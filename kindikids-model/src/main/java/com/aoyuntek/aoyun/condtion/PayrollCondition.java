package com.aoyuntek.aoyun.condtion;

import java.util.Date;
import java.util.List;

public class PayrollCondition {
	private List<String> centreIds;
	private List<String> staffIds;
	private Date startDate;
	private Date endDate;

	public List<String> getCentreIds() {
		return centreIds;
	}

	public void setCentreIds(List<String> centreIds) {
		this.centreIds = centreIds;
	}

	public List<String> getStaffIds() {
		return staffIds;
	}

	public void setStaffIds(List<String> staffIds) {
		this.staffIds = staffIds;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
