package com.aoyuntek.aoyun.condtion;

import com.aoyuntek.framework.condtion.BaseCondition;

/**
 * 
 * @author dlli5 at 2016年10月17日上午11:11:35
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public class RosterShiftCondition extends BaseCondition {

    private String centerId;
    private Boolean currentFlag;
    
    public Boolean getCurrentFlag() {
        return currentFlag;
    }

    public void setCurrentFlag(Boolean currentFlag) {
        this.currentFlag = currentFlag;
    }

    public String getCenterId() {
        return centerId;
    }

    public void setCenterId(String centerId) {
        this.centerId = centerId;
    }
}