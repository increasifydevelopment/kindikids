package com.aoyuntek.aoyun.condtion;

import com.aoyuntek.framework.condtion.BaseCondition;

/**
 * 
 * @author dlli5 at 2016年9月21日下午5:15:07
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public class FollowUpCondtion extends BaseCondition {

    private String taskId;

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }
}
