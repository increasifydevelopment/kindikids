package com.aoyuntek.aoyun.condtion;

import java.util.List;

import com.aoyuntek.aoyun.entity.vo.RoleInfoVo;
import com.aoyuntek.framework.condtion.BaseCondition;

/**
 * 
 * @description 权限组查询条件
 * @author gfwang
 * @create 2016年8月29日下午3:02:34
 * @version 1.0
 */
public class AuthGroupCondition extends BaseCondition {

    private List<RoleInfoVo> roleInfoVoList;

    private String centerId;
    private String keyWord;

    private Short type;

    private List<String> chooseGroups;

    private boolean messageCaualCenterManager;

    public boolean isMessageCaualCenterManager() {
        return messageCaualCenterManager;
    }

    public void setMessageCaualCenterManager(boolean messageCaualCenterManager) {
        this.messageCaualCenterManager = messageCaualCenterManager;
    }

    public AuthGroupCondition() {

    }

    public AuthGroupCondition(List<RoleInfoVo> roleInfoVoList, String centerId, String keyWord, Short type, boolean messageCaualCenterManager) {
        super();
        this.roleInfoVoList = roleInfoVoList;
        this.centerId = centerId;
        this.keyWord = keyWord;
        this.type = type;
        this.messageCaualCenterManager = messageCaualCenterManager;
    }

    public List<String> getChooseGroups() {
        return chooseGroups;
    }

    public void setChooseGroups(List<String> chooseGroups) {
        this.chooseGroups = chooseGroups;
    }

    public Short getType() {
        return type;
    }

    public void setType(Short type) {
        this.type = type;
    }

    public List<RoleInfoVo> getRoleInfoVoList() {
        return roleInfoVoList;
    }

    public void setRoleInfoVoList(List<RoleInfoVo> roleInfoVoList) {
        this.roleInfoVoList = roleInfoVoList;
    }

    public String getCenterId() {
        return centerId;
    }

    public void setCenterId(String centerId) {
        this.centerId = centerId;
    }

    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }

}
