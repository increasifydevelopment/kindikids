package com.aoyuntek.aoyun.condtion;

import java.util.List;

import com.aoyuntek.aoyun.entity.po.RoleInfo;

public class UserBySelectCondition {

    /**
     * 姓名
     */
    private String name;

    /**
     * 角色s
     */
    private List<RoleInfo> roles;

    /**
     * 园区
     */
    private String centerId;

    /**
     * 开启不等于功能
     */
    private int openNotIn = -1;

    /**
     * 查询类型
     */
    private int type=-1;
    
    /**
     * 是否兼职
     */
    private boolean orCauals;
    
    
    
    public boolean isOrCauals() {
        return orCauals;
    }

    public void setOrCauals(boolean orCauals) {
        this.orCauals = orCauals;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getOpenNotIn() {
        return openNotIn;
    }

    public void setOpenNotIn(int openNotIn) {
        this.openNotIn = openNotIn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<RoleInfo> getRoles() {
        return roles;
    }

    public void setRoles(List<RoleInfo> roles) {
        this.roles = roles;
    }

    public String getCenterId() {
        return centerId;
    }

    public void setCenterId(String centerId) {
        this.centerId = centerId;
    }

}
