package com.aoyuntek.aoyun.condtion;

import java.util.Date;

import com.aoyuntek.framework.condtion.BaseCondition;

public class ApproveAddCondition extends BaseCondition {
	private String roomId;
	private String childId;
	private Date day;
	private boolean addForMax = false;
	private boolean alertOther = false;
	private boolean dealAll = false;
	private boolean addForMaxAll = false;
	private boolean coverOld = false;
	private boolean interim = false;
	private boolean confirm = false;

	public boolean isConfirm() {
		return confirm;
	}

	public void setConfirm(boolean confirm) {
		this.confirm = confirm;
	}

	public boolean isInterim() {
		return interim;
	}

	public void setInterim(boolean interim) {
		this.interim = interim;
	}

	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}

	public String getChildId() {
		return childId;
	}

	public void setChildId(String childId) {
		this.childId = childId;
	}

	public Date getDay() {
		return day;
	}

	public void setDay(Date day) {
		this.day = day;
	}

	public boolean isAddForMax() {
		return addForMax;
	}

	public void setAddForMax(boolean addForMax) {
		this.addForMax = addForMax;
	}

	public boolean isAlertOther() {
		return alertOther;
	}

	public void setAlertOther(boolean alertOther) {
		this.alertOther = alertOther;
	}

	public boolean isDealAll() {
		return dealAll;
	}

	public void setDealAll(boolean dealAll) {
		this.dealAll = dealAll;
	}

	public boolean isAddForMaxAll() {
		return addForMaxAll;
	}

	public void setAddForMaxAll(boolean addForMaxAll) {
		this.addForMaxAll = addForMaxAll;
	}

	public boolean isCoverOld() {
		return coverOld;
	}

	public void setCoverOld(boolean coverOld) {
		this.coverOld = coverOld;
	}
}
