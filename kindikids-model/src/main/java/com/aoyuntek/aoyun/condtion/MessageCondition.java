package com.aoyuntek.aoyun.condtion;

import java.util.ArrayList;
import java.util.List;

import com.aoyuntek.aoyun.entity.vo.NqsVo;
import com.aoyuntek.framework.condtion.BaseCondition;

/**
 * @description 站内信查询条件
 * @author bbq
 * @create 2016年6月30日下午2:21:01
 * @version 1.0
 * 
 */
public class MessageCondition extends BaseCondition {
    /**
     * 当前用户accountID
     */
    private String accountId;
    /**
     * -1:全部、0：未读、1：已读
     */
    private int readFlag;
    /**
     * 模糊查询参数
     */
    private String params;
    /**
     * 角色
     */
    private short role;
    /**
     * 园区
     */
    private String centersId;
    /**
     * NQS集合
     */
    private List<NqsVo> chooseNqs = new ArrayList<NqsVo>();
    /**
     * 内容
     */
    private String content;
    /**
     * 接收人
     */
    private String receiverIds;
    /**
     * 主题
     */
    private String subject;
    /**
     * 文件名，逗号分割
     */
    private String fileNames;
    /**
     * 文件ID，逗号分割
     */
    private String fileIds;
    /**
     * 访问url
     */
    private String visitUrl;
    /**
     * 相对路径
     */
    private String relativePathNames;
    /**
     * 邮件类型
     */
    private short messageType;
    /**
     * 邮件id，转发回复使用
     */
    private String msgId;
    /**
     * NQS版本查询
     */
    private String nqsVersion;
    /**
     * NQS查询
     */
    private String selectNqs;

    public String getSelectNqs() {
        return selectNqs;
    }

    public void setSelectNqs(String selectNqs) {
        this.selectNqs = selectNqs;
    }

    public String getMsgId() {
        return msgId;
    }

    public List<NqsVo> getChooseNqs() {
        return chooseNqs;
    }

    public void setChooseNqs(List<NqsVo> chooseNqs) {
        this.chooseNqs = chooseNqs;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    public short getMessageType() {
        return messageType;
    }

    public void setMessageType(short messageType) {
        this.messageType = messageType;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getReceiverIds() {
        return receiverIds;
    }

    public void setReceiverIds(String receiverIds) {
        this.receiverIds = receiverIds;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getCentersId() {
        return centersId;
    }

    public void setCentersId(String centersId) {
        this.centersId = centersId;
    }

    public short getRole() {
        return role;
    }

    public void setRole(short role) {
        this.role = role;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public int getReadFlag() {
        return readFlag;
    }

    public void setReadFlag(int readFlag) {
        this.readFlag = readFlag;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getFileNames() {
        return fileNames;
    }

    public void setFileNames(String fileNames) {
        this.fileNames = fileNames;
    }

    public String getFileIds() {
        return fileIds;
    }

    public void setFileIds(String fileIds) {
        this.fileIds = fileIds;
    }

    public String getRelativePathNames() {
        return relativePathNames;
    }

    public void setRelativePathNames(String relativePathNames) {
        this.relativePathNames = relativePathNames;
    }

    public String getVisitUrl() {
        return visitUrl;
    }

    public void setVisitUrl(String visitUrl) {
        this.visitUrl = visitUrl;
    }

    public String getNqsVersion() {
        return nqsVersion;
    }

    public void setNqsVersion(String nqsVersion) {
        this.nqsVersion = nqsVersion;
    }

}
