package com.aoyuntek.aoyun.condtion;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aoyuntek.aoyun.entity.po.CentersInfo;
import com.aoyuntek.aoyun.entity.po.RoomInfo;
import com.aoyuntek.aoyun.entity.vo.CusDateVo;
import com.aoyuntek.aoyun.entity.vo.SelecterPo;
import com.aoyuntek.framework.condtion.BaseCondition;

/**
 * 
 * @description 微博查询条件
 * @author bbq
 * @create 2016年6月30日下午2:21:19
 * @version 1.0
 * 
 */
public class NewsCondition extends BaseCondition {

	private boolean feedbackSearch;

	private List<String> feedbacks;
	/**
	 * 园区ID
	 */
	private String centersId;
	/**
	 * room ID
	 */
	private String roomId;
	/**
	 * 人员ID
	 */
	private String accountId;
	/**
	 * 评分
	 */
	private Integer score;
	/**
	 * nqs版本
	 */
	private String nqsVersion;
	/**
	 * 查询nqs版本
	 */
	private String selectNqs;
	/**
	 * 起始时间
	 */
	private Date startDate;
	/**
	 * 结束时间
	 */
	private Date endDate;
	/**
	 * type
	 */
	private String type;
	/**
	 * status
	 */
	private short status = -1;
	/**
	 * visibility
	 */
	private Integer visibility;
	/**
	 * selectAccounts
	 */
	private List<SelecterPo> selectAccounts;
	/**
	 * time
	 */
	private String time;
	/**
	 * timeSource
	 */
	private List<CusDateVo> timeSource;
	/**
	 * 当前登陆人角色
	 */
	private short role;
	/**
	 * 微博ID
	 */
	private String id;
	/**
	 * tags
	 */
	private List<String> selectType;

	/**
	 * newsIds 可查看的 news
	 * 
	 */
	private List<String> newsIds = new ArrayList<String>();
	/**
	 * 关键字搜索
	 */
	private String keyword;
	/**
	 * 查询center
	 */
	private List<CentersInfo> selectCenters;
	/**
	 * 查询room
	 */
	private List<RoomInfo> selectRoom;

	private NewsFeedAuthorityCondition authorityCondition;

	private String str;

	public String getStr() {
		return str;
	}

	public void setStr(String str) {
		this.str = str;
	}

	public boolean isCeoFlag() {
		return ceoFlag;
	}

	public void setCeoFlag(boolean ceoFlag) {
		this.ceoFlag = ceoFlag;
	}

	private boolean ceoFlag = false;

	// 1，5
	private boolean centerManagerFlag = false;
	// 3
	private boolean ectFlag = false;

	private boolean educatorFlag = false;

	private boolean cookFlag = false;

	private boolean parentFlag = false;

	private boolean casualFlag = false;

	public boolean isCenterManagerFlag() {
		return centerManagerFlag;
	}

	public void setCenterManagerFlag(boolean centerManagerFlag) {
		this.centerManagerFlag = centerManagerFlag;
	}

	public boolean isEctFlag() {
		return ectFlag;
	}

	public void setEctFlag(boolean ectFlag) {
		this.ectFlag = ectFlag;
	}

	public boolean isEducatorFlag() {
		return educatorFlag;
	}

	public void setEducatorFlag(boolean educatorFlag) {
		this.educatorFlag = educatorFlag;
	}

	public boolean isCookFlag() {
		return cookFlag;
	}

	public void setCookFlag(boolean cookFlag) {
		this.cookFlag = cookFlag;
	}

	public boolean isParentFlag() {
		return parentFlag;
	}

	public void setParentFlag(boolean parentFlag) {
		this.parentFlag = parentFlag;
	}

	public boolean isCasualFlag() {
		return casualFlag;
	}

	public void setCasualFlag(boolean casualFlag) {
		this.casualFlag = casualFlag;
	}

	public NewsFeedAuthorityCondition getAuthorityCondition() {
		return authorityCondition;
	}

	public void setAuthorityCondition(NewsFeedAuthorityCondition authorityCondition) {
		this.authorityCondition = authorityCondition;
	}

	public List<CentersInfo> getSelectCenters() {
		return selectCenters;
	}

	public void setSelectCenters(List<CentersInfo> selectCenters) {
		this.selectCenters = selectCenters;
	}

	public List<RoomInfo> getSelectRoom() {
		return selectRoom;
	}

	public void setSelectRoom(List<RoomInfo> selectRoom) {
		this.selectRoom = selectRoom;
	}

	public List<String> getNewsIds() {
		return newsIds;
	}

	public void setNewsIds(List<String> newsIds) {
		this.newsIds = newsIds;
	}

	public List<String> getSelectType() {
		return selectType;
	}

	public void setSelectType(List<String> selectType) {
		this.selectType = selectType;
	}

	public Integer getVisibility() {
		return visibility;
	}

	public void setVisibility(Integer visibility) {
		this.visibility = visibility;
	}

	public List<SelecterPo> getSelectAccounts() {
		return selectAccounts;
	}

	public void setSelectAccounts(List<SelecterPo> selectAccounts) {
		this.selectAccounts = selectAccounts;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public List<CusDateVo> getTimeSource() {
		return timeSource;
	}

	public void setTimeSource(List<CusDateVo> timeSource) {
		this.timeSource = timeSource;
	}

	public void setStatus(short status) {
		this.status = status;
	}

	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public String getNqsVersion() {
		return nqsVersion;
	}

	public void setNqsVersion(String nqsVersion) {
		this.nqsVersion = nqsVersion;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public short getStatus() {
		return status;
	}

	public void setStatus(Short status) {
		this.status = status == null ? -1 : status;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public short getRole() {
		return role;
	}

	public void setRole(short role) {
		this.role = role;
	}

	public String getCentersId() {
		return centersId;
	}

	public void setCentersId(String centersId) {
		this.centersId = centersId;
	}

	public String getSelectNqs() {
		return selectNqs;
	}

	public void setSelectNqs(String selectNqs) {
		this.selectNqs = selectNqs;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public List<String> getFeedbacks() {
		return feedbacks;
	}

	public void setFeedbacks(List<String> feedbacks) {
		this.feedbacks = feedbacks;
	}

	public boolean isFeedbackSearch() {
		return feedbackSearch;
	}

	public void setFeedbackSearch(boolean feedbackSearch) {
		this.feedbackSearch = feedbackSearch;
	}
}
