package com.aoyuntek.aoyun.condtion;

import java.util.Date;

import com.aoyuntek.framework.condtion.BaseCondition;

public class MeetingAgendaCondition extends BaseCondition {

    private String meetingAgendaName;
    
    private String typeId;
    
    private Date startTime;
    
    private Date endTime;

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getMeetingAgendaName() {
        return meetingAgendaName;
    }

    public void setMeetingAgendaName(String meetingAgendaName) {
        this.meetingAgendaName = meetingAgendaName;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }
    
}
