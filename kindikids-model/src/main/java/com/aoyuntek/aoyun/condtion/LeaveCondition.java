package com.aoyuntek.aoyun.condtion;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aoyuntek.framework.condtion.BaseCondition;

/**
 * @description Leave查询实体
 * @author hxzhang
 * @create 2016年8月16日下午2:43:15
 * @version 1.0
 */
public class LeaveCondition extends BaseCondition {
	/**
	 * 请假人accountId
	 */
	private String userName;
	/**
	 * 请假人园区
	 */
	private String centreName;
	/**
	 * 请假人角色
	 */
	private String roleName;
	/**
	 * 请假类别
	 */
	private String leaveType;
	/**
	 * 开始时间
	 */
	private Date startDate;
	/**
	 * 结束时间
	 */
	private Date endDate;
	/**
	 * 申请时间
	 */
	private Date appDate;

	/**
	 * 请假状态
	 */
	private String status;
	/**
	 * 查询status
	 */
	private List<Short> selectStatus;
	/**
	 * 查询LeaveType
	 */
	private List<Short> selectLeaveType;
	/**
	 * 根据用户accountId过滤
	 */
	private String selectAccountId;
	/**
	 * 根据园区ID过滤
	 */
	private String selectCenterId;
	/**
	 * 请假条ID
	 */
	private String leaveId;
	/**
	 * 请假列表类型(0 自己的请假列表,1 员工的请假列表)
	 */
	private Short listType;

	private String reason;

	private List<String> centreIds = new ArrayList<String>();

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCentreName() {
		return centreName;
	}

	public void setCentreName(String centreName) {
		this.centreName = centreName;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getLeaveType() {
		return leaveType;
	}

	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getAppDate() {
		return appDate;
	}

	public void setAppDate(Date appDate) {
		this.appDate = appDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Short getListType() {
		return listType;
	}

	public void setListType(Short listType) {
		this.listType = listType;
	}

	public String getSelectAccountId() {
		return selectAccountId;
	}

	public void setSelectAccountId(String selectAccountId) {
		this.selectAccountId = selectAccountId;
	}

	public String getSelectCenterId() {
		return selectCenterId;
	}

	public void setSelectCenterId(String selectCenterId) {
		this.selectCenterId = selectCenterId;
	}

	public List<Short> getSelectStatus() {
		return selectStatus;
	}

	public void setSelectStatus(List<Short> selectStatus) {
		this.selectStatus = selectStatus;
	}

	public List<Short> getSelectLeaveType() {
		return selectLeaveType;
	}

	public void setSelectLeaveType(List<Short> selectLeaveType) {
		this.selectLeaveType = selectLeaveType;
	}

	public String getLeaveId() {
		return leaveId;
	}

	public void setLeaveId(String leaveId) {
		this.leaveId = leaveId;
	}

	public List<String> getCentreIds() {
		return centreIds;
	}

	public void setCentreIds(List<String> centreIds) {
		this.centreIds = centreIds;
	}

}
