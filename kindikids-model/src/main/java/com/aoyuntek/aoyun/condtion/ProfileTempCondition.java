package com.aoyuntek.aoyun.condtion;

import com.aoyuntek.framework.condtion.BaseCondition;

public class ProfileTempCondition extends BaseCondition {
    private String keyWord;
    private Short type;

    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }

    public Short getType() {
        return type;
    }

    public void setType(Short type) {
        this.type = type;
    }

}
