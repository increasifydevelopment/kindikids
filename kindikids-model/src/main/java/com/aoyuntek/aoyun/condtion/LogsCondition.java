package com.aoyuntek.aoyun.condtion;

import com.aoyuntek.framework.condtion.BaseCondition;

public class LogsCondition extends BaseCondition {
	/**
	 * log的源ID
	 */
	private String sourceId;

	public String getSourceId() {
		return sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}
	
}
