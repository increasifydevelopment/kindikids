package com.aoyuntek.aoyun.condtion.dashboard;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.aoyuntek.aoyun.entity.vo.task.PersonSelectVo;
import com.aoyuntek.framework.condtion.BaseCondition;

/**
 * 
 * @description 报表查询条件
 * @author gfwang
 * @create 2016年11月9日下午2:12:59
 * @version 1.0
 */
public class DashboardCondition extends BaseCondition {
    private Date startDate;

    private Date endDate;

    private List<PersonSelectVo> chooseOrgs;

    private short type;

    private String chooseId;

    private List<String> newsIdList = new ArrayList<String>();

    public List<String> getNewsIdList() {
        return newsIdList;
    }

    public void setNewsIdList(List<String> newsIdList) {
        this.newsIdList = newsIdList;
    }

    public short getType() {
        return type;
    }

    public void setType(short type) {
        this.type = type;
    }

    public String getChooseId() {
        return chooseId;
    }

    public void setChooseId(String chooseId) {
        this.chooseId = chooseId;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public List<PersonSelectVo> getChooseOrgs() {
        return chooseOrgs;
    }

    public void setChooseOrgs(List<PersonSelectVo> chooseOrgs) {
        this.chooseOrgs = chooseOrgs;
    }

    public DashboardCondition(short type, String chooseId) {
        super();
        this.type = type;
        this.chooseId = chooseId;
    }

    public Date getStartDate() {
        return startDate;
    }

}
