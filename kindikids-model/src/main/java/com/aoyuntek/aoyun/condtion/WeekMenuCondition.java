package com.aoyuntek.aoyun.condtion;

/**
 * 
 * @description 每周菜单
 * @author gfwang
 * @create 2016年8月20日下午1:46:47
 * @version 1.0
 */
public class WeekMenuCondition {
    /**
     * 园区Id
     */
    private String centerId;
    
    
    private String roomId;
    
    /**
     * 第几周
     */
    private Short weekNum;
    /**
     * 周几
     */
    private Short weekToday;

    public WeekMenuCondition() {

    }

    public WeekMenuCondition(String centerId, Short weekNum, Short weekToday) {
        super();
        this.centerId = centerId;
        this.weekNum = weekNum;
        this.weekToday = weekToday;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getCenterId() {
        return centerId;
    }

    public void setCenterId(String centerId) {
        this.centerId = centerId;
    }

    public Short getWeekNum() {
        return weekNum;
    }

    public void setWeekNum(Short weekNum) {
        this.weekNum = weekNum;
    }

    public Short getWeekToday() {
        return weekToday;
    }

    public void setWeekToday(Short weekToday) {
        this.weekToday = weekToday;
    }

}