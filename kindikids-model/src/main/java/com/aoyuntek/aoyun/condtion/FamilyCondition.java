package com.aoyuntek.aoyun.condtion;

import java.util.ArrayList;
import java.util.List;

import com.aoyuntek.framework.condtion.BaseCondition;

/**
 * 
 * @description family列表查询条件
 * @author gfwang
 * @create 2016年8月4日上午9:00:32
 * @version 1.0
 */
public class FamilyCondition extends BaseCondition {

	private List<String> centreIds = new ArrayList<String>();

	/**
	 * 园
	 */
	private String centerId;

	/**
	 * 房间
	 */
	private String roomId;
	/**
	 * 家庭
	 */
	private String familyId;

	/**
	 * other
	 */
	private Short myStatus = -1;

	/**
	 * 关键字（name,和email 关键字）
	 */
	private String keyWords;

	public Short getMyStatus() {
		return myStatus;
	}

	public void setMyStatus(Short myStatus) {
		this.myStatus = myStatus;
	}

	public String getKeyWords() {
		return keyWords;
	}

	public void setKeyWords(String keyWords) {
		this.keyWords = keyWords;
	}

	public String getCenterId() {
		return centerId;
	}

	public void setCenterId(String centerId) {
		this.centerId = centerId;
	}

	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}

	public String getFamilyId() {
		return familyId;
	}

	public void setFamilyId(String familyId) {
		this.familyId = familyId;
	}

	public List<String> getCentreIds() {
		return centreIds;
	}

	public void setCentreIds(List<String> centreIds) {
		this.centreIds = centreIds;
	}

}
