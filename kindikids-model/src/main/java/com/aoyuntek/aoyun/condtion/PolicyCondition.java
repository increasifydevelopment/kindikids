package com.aoyuntek.aoyun.condtion;

import java.util.ArrayList;
import java.util.List;

import com.aoyuntek.aoyun.entity.po.policy.PolicyCategoriesInfo;
import com.aoyuntek.framework.condtion.BaseCondition;

/**
 * policy列表参数
 * @author bing
 *
 */
public class PolicyCondition extends BaseCondition {
	
	private String nqsVersion;
    private String selectNqs;
	
	private String keyword;
	private String categoryId;
	private List<String> currentRoles;
	private List<PolicyCategoriesInfo> policyCategoriesSrcList=new ArrayList<PolicyCategoriesInfo>();
	public List<PolicyCategoriesInfo> getPolicyCategoriesSrcList() {
		return policyCategoriesSrcList;
	}
	public void setPolicyCategoriesSrcList(List<PolicyCategoriesInfo> policyCategoriesSrcList) {
		this.policyCategoriesSrcList = policyCategoriesSrcList;
	}
	public List<String> getCurrentRoles() {
		return currentRoles;
	}
	public void setCurrentRoles(List<String> currentRoles) {
		this.currentRoles = currentRoles;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public String getNqsVersion() {
		return nqsVersion;
	}
	public void setNqsVersion(String nqsVersion) {
		this.nqsVersion = nqsVersion;
	}
	public String getSelectNqs() {
		return selectNqs;
	}
	public void setSelectNqs(String selectNqs) {
		this.selectNqs = selectNqs;
	}
	
}
