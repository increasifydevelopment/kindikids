package com.aoyuntek.aoyun.condtion;

import java.util.Date;

import com.aoyuntek.framework.condtion.BaseCondition;

public class ExternalListCondition extends BaseCondition {
    private String childName;
    private String liveAge;
    private Date birthday;
    private Short type;
    private String reqDate;
    private Date appStartDate;
    private Date appEndDate;
    private Date reqStartDate;
    private Date reqEndDate;
    private String p1Name;
    private String p1Mobile;
    private String p2Name;
    private String p2Mobile;
    private String personColor;
    private String avatar;
    private String[] reqDates;
    private Date age;

    public Date getAge() {
        return age;
    }

    public void setAge(Date age) {
        this.age = age;
    }

    public Date getReqEndDate() {
        return reqEndDate;
    }

    public void setReqEndDate(Date reqEndDate) {
        this.reqEndDate = reqEndDate;
    }

    public Date getAppStartDate() {
        return appStartDate;
    }

    public void setAppStartDate(Date appStartDate) {
        this.appStartDate = appStartDate;
    }

    public Date getAppEndDate() {
        return appEndDate;
    }

    public void setAppEndDate(Date appEndDate) {
        this.appEndDate = appEndDate;
    }

    public String[] getReqDates() {
        return reqDates;
    }

    public void setReqDates(String[] reqDates) {
        this.reqDates = reqDates;
    }

    public String getChildName() {
        return childName;
    }

    public void setChildName(String childName) {
        this.childName = childName;
    }

    public String getLiveAge() {
        return liveAge;
    }

    public void setLiveAge(String liveAge) {
        this.liveAge = liveAge;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Short getType() {
        return type;
    }

    public void setType(Short type) {
        this.type = type;
    }

    public String getReqDate() {
        return reqDate;
    }

    public void setReqDate(String reqDate) {
        this.reqDate = reqDate;
    }

    public Date getReqStartDate() {
        return reqStartDate;
    }

    public void setReqStartDate(Date reqStartDate) {
        this.reqStartDate = reqStartDate;
    }

    public String getP1Name() {
        return p1Name;
    }

    public void setP1Name(String p1Name) {
        this.p1Name = p1Name;
    }

    public String getP1Mobile() {
        return p1Mobile;
    }

    public void setP1Mobile(String p1Mobile) {
        this.p1Mobile = p1Mobile;
    }

    public String getP2Name() {
        return p2Name;
    }

    public void setP2Name(String p2Name) {
        this.p2Name = p2Name;
    }

    public String getP2Mobile() {
        return p2Mobile;
    }

    public void setP2Mobile(String p2Mobile) {
        this.p2Mobile = p2Mobile;
    }

    public String getPersonColor() {
        return personColor;
    }

    public void setPersonColor(String personColor) {
        this.personColor = personColor;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

}
