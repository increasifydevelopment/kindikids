package com.aoyuntek.aoyun.condtion;

import com.aoyuntek.framework.condtion.BaseCondition;

public class GalleryCondition extends BaseCondition {
	
	private String childId;
	private String avatar;
	private String personColor;
	private String name;
	public String getChildId() {
		return childId;
	}
	public void setChildId(String childId) {
		this.childId = childId;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public String getPersonColor() {
		return personColor;
	}
	public void setPersonColor(String personColor) {
		this.personColor = personColor;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
