package com.aoyuntek.aoyun.condtion;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.aoyuntek.aoyun.entity.po.CentersInfo;
import com.aoyuntek.framework.condtion.BaseCondition;

public class AttendanceCondtion extends BaseCondition {
	private String roomId;
	private String centersId;
	private String accountId;
	private String keyword;
	private int type = 0;
	private String time;
	private Date weekDate;
	private Date day;
	private boolean isCeo = false;
	@JsonIgnore
	private List<String> selectCentreIds = new ArrayList<String>();
	// 0 child
	// 1 staff
	// 2 vistor
	private int listType;
	private boolean isCurrentCentreManager = false;

	/**
	 * 用来 存放 家长所属所有园区
	 */
	@JsonIgnore
	private List<String> centreIds = new ArrayList<String>();

	public List<String> getCentreIds() {
		return centreIds;
	}

	public void setCentreIds(List<String> centreIds) {
		this.centreIds = centreIds;
	}

	public boolean isCeo() {
		return isCeo;
	}

	public void setCeo(boolean isCeo) {
		this.isCeo = isCeo;
	}

	public String getAccountId() {
		return accountId;
	}

	public Date getDay() {
		return day;
	}

	public void setDay(Date day) {
		this.day = day;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public int getListType() {
		return listType;
	}

	public void setListType(int listType) {
		this.listType = listType;
	}

	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}

	public String getCentersId() {
		return centersId;
	}

	public void setCentersId(String centersId) {
		this.centersId = centersId;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Date getWeekDate() {
		return weekDate;
	}

	public void setWeekDate(Date weekDate) {
		this.weekDate = weekDate;
	}

	public boolean isCurrentCentreManager() {
		return isCurrentCentreManager;
	}

	public void setCurrentCentreManager(boolean isCurrentCentreManager) {
		this.isCurrentCentreManager = isCurrentCentreManager;
	}

	public List<String> getSelectCentreIds() {
		return selectCentreIds;
	}

	public void setSelectCentreIds(List<String> selectCentreIds) {
		this.selectCentreIds = selectCentreIds;
	}
}
