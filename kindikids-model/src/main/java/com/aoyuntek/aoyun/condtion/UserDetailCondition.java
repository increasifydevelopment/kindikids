package com.aoyuntek.aoyun.condtion;

import java.util.List;

import com.aoyuntek.framework.condtion.BaseCondition;

/**
 * 
 * @description
 * @author gfwang
 * @create 2016年8月1日下午2:51:44
 * @version 1.0
 */
public class UserDetailCondition extends BaseCondition {

    private String email;

    private String userId;

    private Short enrolled;

    private Short status;

    private Short userType;

    private List<String> centreIds;

    private List<String> roomIds;

    private List<String> accountIds;

    private String roomId;

    private String accountId;

    public List<String> getAccountIds() {
        return accountIds;
    }

    public void setAccountIds(List<String> accountIds) {
        this.accountIds = accountIds;
    }

    public List<String> getRoomIds() {
        return roomIds;
    }

    public void setRoomIds(List<String> roomIds) {
        this.roomIds = roomIds;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Short getEnrolled() {
        return enrolled;
    }

    public void setEnrolled(Short enrolled) {
        this.enrolled = enrolled;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    public Short getUserType() {
        return userType;
    }

    public void setUserType(Short userType) {
        this.userType = userType;
    }

    public List<String> getCentreIds() {
        return centreIds;
    }

    public void setCentreIds(List<String> centreIds) {
        this.centreIds = centreIds;
    }
}
