package com.aoyuntek.aoyun.condtion;

import java.util.Date;

import com.aoyuntek.framework.condtion.BaseCondition;

public class SubtractedAttendanceCondition extends BaseCondition {
    private String childId;
    private String roomId;
    private Date beginTime;
    private Date endTime;
    private short dayOfWeek;

    public String getChildId() {
        return childId;
    }

    public void setChildId(String childId) {
        this.childId = childId;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public short getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(short dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }
}
