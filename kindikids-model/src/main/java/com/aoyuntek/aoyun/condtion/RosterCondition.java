package com.aoyuntek.aoyun.condtion;

import java.util.Date;

import com.aoyuntek.framework.condtion.BaseCondition;

/**
 * 
 * @author dlli5 at 2016年10月17日上午11:11:35
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public class RosterCondition extends BaseCondition {

    private Date startDate;
    private String centerId;
    private Boolean currentFlag;
    private Short state;

    public Short getState() {
        return state;
    }

    public void setState(Short state) {
        this.state = state;
    }

    public Boolean getCurrentFlag() {
        return currentFlag;
    }

    public void setCurrentFlag(Boolean currentFlag) {
        this.currentFlag = currentFlag;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getCenterId() {
        return centerId;
    }

    public void setCenterId(String centerId) {
        this.centerId = centerId;
    }
}