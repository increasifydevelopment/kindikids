package com.aoyuntek.aoyun.condtion.waitingList;

import java.util.Date;
import java.util.List;

import com.aoyuntek.aoyun.entity.po.CentersInfo;
import com.aoyuntek.framework.condtion.BaseCondition;

public class WaitingListCondition extends BaseCondition {
	private List<Short> listType;
	private Date liveAge;
	private String childName;
	private String[] reqDates;
	private Date applicationDate;
	private Date applicationDatePosition;
	private boolean monday;
	private boolean tuesday;
	private boolean wednesday;
	private boolean thursday;
	private boolean friday;
	private String param;
	private List<String> selectPeople;
	private List<String> centerTypes;
	private List<String> selectCenterIds;
	private List<String> centerIds;
	private List<CentersInfo> allCenters;
	private Date appStartDate;
	private Date appEndDate;
	private Date requestStartDate;
	private Date requestEndDate;
	private String familyId;
	private Integer[] counts;

	public Integer[] getCounts() {
		return counts;
	}

	public void setCounts(Integer[] counts) {
		this.counts = counts;
	}

	public String getFamilyId() {
		return familyId;
	}

	public void setFamilyId(String familyId) {
		this.familyId = familyId;
	}

	public Date getAppStartDate() {
		return appStartDate;
	}

	public void setAppStartDate(Date appStartDate) {
		this.appStartDate = appStartDate;
	}

	public Date getAppEndDate() {
		return appEndDate;
	}

	public void setAppEndDate(Date appEndDate) {
		this.appEndDate = appEndDate;
	}

	public Date getRequestEndDate() {
		return requestEndDate;
	}

	public void setRequestEndDate(Date requestEndDate) {
		this.requestEndDate = requestEndDate;
	}

	public List<CentersInfo> getAllCenters() {
		return allCenters;
	}

	public void setAllCenters(List<CentersInfo> allCenters) {
		this.allCenters = allCenters;
	}

	public List<String> getSelectCenterIds() {
		return selectCenterIds;
	}

	public void setSelectCenterIds(List<String> selectCenterIds) {
		this.selectCenterIds = selectCenterIds;
	}

	public List<String> getCenterIds() {
		return centerIds;
	}

	public void setCenterIds(List<String> centerIds) {
		this.centerIds = centerIds;
	}

	public List<String> getCenterTypes() {
		return centerTypes;
	}

	public void setCenterTypes(List<String> centerTypes) {
		this.centerTypes = centerTypes;
	}

	public List<String> getSelectPeople() {
		return selectPeople;
	}

	public void setSelectPeople(List<String> selectPeople) {
		this.selectPeople = selectPeople;
	}

	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	public boolean isMonday() {
		return monday;
	}

	public void setMonday(boolean monday) {
		this.monday = monday;
	}

	public boolean isTuesday() {
		return tuesday;
	}

	public void setTuesday(boolean tuesday) {
		this.tuesday = tuesday;
	}

	public boolean isWednesday() {
		return wednesday;
	}

	public void setWednesday(boolean wednesday) {
		this.wednesday = wednesday;
	}

	public boolean isThursday() {
		return thursday;
	}

	public void setThursday(boolean thursday) {
		this.thursday = thursday;
	}

	public boolean isFriday() {
		return friday;
	}

	public void setFriday(boolean friday) {
		this.friday = friday;
	}

	public List<Short> getListType() {
		return listType;
	}

	public void setListType(List<Short> listType) {
		this.listType = listType;
	}

	public Date getLiveAge() {
		return liveAge;
	}

	public void setLiveAge(Date liveAge) {
		this.liveAge = liveAge;
	}

	public String getChildName() {
		return childName;
	}

	public void setChildName(String childName) {
		this.childName = childName;
	}

	public String[] getReqDates() {
		return reqDates;
	}

	public void setReqDates(String[] reqDates) {
		this.reqDates = reqDates;
	}

	public Date getApplicationDate() {
		return applicationDate;
	}

	public void setApplicationDate(Date applicationDate) {
		this.applicationDate = applicationDate;
	}

	public Date getApplicationDatePosition() {
		return applicationDatePosition;
	}

	public void setApplicationDatePosition(Date applicationDatePosition) {
		this.applicationDatePosition = applicationDatePosition;
	}

	public Date getRequestStartDate() {
		return requestStartDate;
	}

	public void setRequestStartDate(Date requestStartDate) {
		this.requestStartDate = requestStartDate;
	}
}
