package com.aoyuntek.aoyun.condtion;

import java.util.List;

import com.aoyuntek.framework.condtion.BaseCondition;

public class CenterCondition extends BaseCondition {
    private String centerId;
    private List<String> centerIds;
    private Short status;

    public List<String> getCenterIds() {
        return centerIds;
    }

    public void setCenterIds(List<String> centerIds) {
        this.centerIds = centerIds;
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    public String getCenterId() {
        return centerId;
    }

    public void setCenterId(String centerId) {
        this.centerId = centerId;
    }

}
