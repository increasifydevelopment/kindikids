package com.aoyuntek.aoyun.condtion;

import java.util.Date;
import java.util.List;

import com.aoyuntek.framework.condtion.BaseCondition;

public class AttendanceManagementCondition extends BaseCondition {

    private String id;
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String userName;

    private String description;

    private String centerName;

    private String typeStr;

    private Date appDate;

    private Date opeDate;

    private String status;

    private Short type;

    private List<Short> selectType;

    private List<Short> selectStatus;

    private String centerId;

    public String getCenterId() {
        return centerId;
    }

    public void setCenterId(String centerId) {
        this.centerId = centerId;
    }

    public Short getType() {
        return type;
    }

    public void setType(Short type) {
        this.type = type;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCenterName() {
        return centerName;
    }

    public void setCenterName(String centerName) {
        this.centerName = centerName;
    }

    public String getTypeStr() {
        return typeStr;
    }

    public void setTypeStr(String typeStr) {
        this.typeStr = typeStr;
    }

    public Date getAppDate() {
        return appDate;
    }

    public void setAppDate(Date appDate) {
        this.appDate = appDate;
    }

    public Date getOpeDate() {
        return opeDate;
    }

    public void setOpeDate(Date opeDate) {
        this.opeDate = opeDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Short> getSelectType() {
        return selectType;
    }

    public void setSelectType(List<Short> selectType) {
        this.selectType = selectType;
    }

    public List<Short> getSelectStatus() {
        return selectStatus;
    }

    public void setSelectStatus(List<Short> selectStatus) {
        this.selectStatus = selectStatus;
    }

}
