package com.aoyuntek.aoyun.condtion.dashboard;

import java.util.List;

import com.aoyuntek.aoyun.entity.vo.RoleInfoVo;

public class SelecterCondition {
    private String p;

    private String centerId;

    private String roomId;
 
    private int roleType=0;
    
    /**
     * 当前人的角色
     */
    private List<RoleInfoVo> currtenRoleList;

    public List<RoleInfoVo> getCurrtenRoleList() {
        return currtenRoleList;
    }

    public void setCurrtenRoleList(List<RoleInfoVo> currtenRoleList) {
        this.currtenRoleList = currtenRoleList;
    }

    public SelecterCondition() {

    }

    public int getRoleType() {
        return roleType;
    }

    public void setRoleType(int roleType) {
        this.roleType = roleType;
    }

    public SelecterCondition(String p, String centerId, String roomId) {
        super();
        this.p = p;
        this.centerId = centerId;
        this.roomId = roomId;
    }

    public SelecterCondition(String p, List<RoleInfoVo> currtenRoleList) {
        super();
        this.p = p;
        this.currtenRoleList = currtenRoleList;
    }

    public SelecterCondition(String p, String centerId) {
        super();
        this.p = p;
        this.centerId = centerId;
    }

    public String getP() {
        return p;
    }

    public void setP(String p) {
        this.p = p;
    }

    public String getCenterId() {
        return centerId;
    }

    public void setCenterId(String centerId) {
        this.centerId = centerId;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

}
