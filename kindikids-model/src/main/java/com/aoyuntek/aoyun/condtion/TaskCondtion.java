package com.aoyuntek.aoyun.condtion;

import java.util.Date;
import java.util.List;

import com.aoyuntek.framework.condtion.BaseCondition;

/**
 * 
 * @author dlli5 at 2016年9月21日下午5:15:07
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public class TaskCondtion extends BaseCondition {

	private int maxSize;

	private Short statu;

	private List<String> taskIds;

	private List<String> taskFollowUpIds;

	private String keyWord;

	private String centreId;

	private String userCentreId;

	private String roomId;

	private Boolean currentFlag;

	private Boolean shiftFlag;

	private Short type;

	private Short frequenceRepeatType;

	private Date day;

	private Date endDate;

	private Date startDate;

	private String accountId;

	private String centreIdOfManager;

	private String nqsVersion;

	private String selectNqs;

	private List<String> selectNqsIds;

	private List<String> centreIds;

	private List<String> roomIds;

	private String taskName;

	private List<String> responsibleIds;

	private List<String> implementersIds;

	private List<String> subjectIds;

	private List<String> nqs;

	private List<String> nqsList;

	private List<String> taskStatus;

	private List<Short> statusList;

	private List<String> responsibleTaskIds;

	private List<String> implementersTaskIds;

	private List<String> taskInstanceIds;

	private boolean selectName = false;

	public boolean isSelectName() {
		return selectName;
	}

	public void setSelectName(boolean selectName) {
		this.selectName = selectName;
	}

	public List<String> getTaskInstanceIds() {
		return taskInstanceIds;
	}

	public void setTaskInstanceIds(List<String> taskInstanceIds) {
		this.taskInstanceIds = taskInstanceIds;
	}

	public List<Short> getStatusList() {
		return statusList;
	}

	public void setStatusList(List<Short> statusList) {
		this.statusList = statusList;
	}

	public List<String> getNqsList() {
		return nqsList;
	}

	public void setNqsList(List<String> nqsList) {
		this.nqsList = nqsList;
	}

	public List<String> getResponsibleTaskIds() {
		return responsibleTaskIds;
	}

	public void setResponsibleTaskIds(List<String> responsibleTaskIds) {
		this.responsibleTaskIds = responsibleTaskIds;
	}

	public List<String> getImplementersTaskIds() {
		return implementersTaskIds;
	}

	public void setImplementersTaskIds(List<String> implementersTaskIds) {
		this.implementersTaskIds = implementersTaskIds;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public List<String> getResponsibleIds() {
		return responsibleIds;
	}

	public void setResponsibleIds(List<String> responsibleIds) {
		this.responsibleIds = responsibleIds;
	}

	public List<String> getImplementersIds() {
		return implementersIds;
	}

	public void setImplementersIds(List<String> implementersIds) {
		this.implementersIds = implementersIds;
	}

	public List<String> getSubjectIds() {
		return subjectIds;
	}

	public void setSubjectIds(List<String> subjectIds) {
		this.subjectIds = subjectIds;
	}

	public List<String> getNqs() {
		return nqs;
	}

	public void setNqs(List<String> nqs) {
		this.nqs = nqs;
	}

	public List<String> getTaskStatus() {
		return taskStatus;
	}

	public void setTaskStatus(List<String> taskStatus) {
		this.taskStatus = taskStatus;
	}

	public List<String> getCentreIds() {
		return centreIds;
	}

	public void setCentreIds(List<String> centreIds) {
		this.centreIds = centreIds;
	}

	public List<String> getRoomIds() {
		return roomIds;
	}

	public void setRoomIds(List<String> roomIds) {
		this.roomIds = roomIds;
	}

	private boolean isAdmin = false;

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public List<String> getSelectNqsIds() {
		return selectNqsIds;
	}

	public void setSelectNqsIds(List<String> selectNqsIds) {
		this.selectNqsIds = selectNqsIds;
	}

	public String getNqsVersion() {
		return nqsVersion;
	}

	public void setNqsVersion(String nqsVersion) {
		this.nqsVersion = nqsVersion;
	}

	public String getSelectNqs() {
		return selectNqs;
	}

	public void setSelectNqs(String selectNqs) {
		this.selectNqs = selectNqs;
	}

	public String getUserCentreId() {
		return userCentreId;
	}

	public void setUserCentreId(String userCentreId) {
		this.userCentreId = userCentreId;
	}

	public String getCentreIdOfManager() {
		return centreIdOfManager;
	}

	public void setCentreIdOfManager(String centreIdOfManager) {
		this.centreIdOfManager = centreIdOfManager;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public Boolean getShiftFlag() {
		return shiftFlag;
	}

	public void setShiftFlag(Boolean shiftFlag) {
		this.shiftFlag = shiftFlag;
	}

	public List<String> getTaskFollowUpIds() {
		return taskFollowUpIds;
	}

	public void setTaskFollowUpIds(List<String> taskFollowUpIds) {
		this.taskFollowUpIds = taskFollowUpIds;
	}

	public Short getFrequenceRepeatType() {
		return frequenceRepeatType;
	}

	public void setFrequenceRepeatType(Short frequenceRepeatType) {
		this.frequenceRepeatType = frequenceRepeatType;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Short getType() {
		return type;
	}

	public void setType(Short type) {
		this.type = type;
	}

	public Date getDay() {
		return day;
	}

	public void setDay(Date day) {
		this.day = day;
	}

	public Boolean getCurrentFlag() {
		return currentFlag;
	}

	public void setCurrentFlag(Boolean currentFlag) {
		this.currentFlag = currentFlag;
	}

	public String getCentreId() {
		return centreId;
	}

	public void setCentreId(String centreId) {
		this.centreId = centreId;
	}

	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}

	public String getKeyWord() {
		return keyWord;
	}

	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}

	public List<String> getTaskIds() {
		return taskIds;
	}

	public void setTaskIds(List<String> taskIds) {
		this.taskIds = taskIds;
	}

	public Short getStatu() {
		return statu;
	}

	public void setStatu(Short statu) {
		this.statu = statu;
	}

	public int getMaxSize() {
		return maxSize;
	}

	public void setMaxSize(int maxSize) {
		this.maxSize = maxSize;
	}
}
