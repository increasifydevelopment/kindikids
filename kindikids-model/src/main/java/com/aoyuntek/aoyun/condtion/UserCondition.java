package com.aoyuntek.aoyun.condtion;

import java.util.Date;
import java.util.List;

import com.aoyuntek.framework.condtion.BaseCondition;

/**
 * 
 * @author dlli5 at 2016年9月23日下午2:33:33
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public class UserCondition extends BaseCondition {
	/**
	 * 密码
	 */
	private String psw;

	/**
	 * 邮箱地址
	 */
	private String account;

	/**
	 * 修改密码token
	 */
	private String token;

	private String centreId;

	private List<String> centreIds;

	private String roomId;

	private List<String> roomIds;

	private String groupId;

	private Short userType;

	private Short roleType;

	private List<Short> roleTypes;

	private Short status;

	private String accountId;

	private List<String> accountIds;

	private Boolean notLeaveFlag;

	private String familyId;

	private Short region;

	public Short getRegion() {
		return region;
	}

	public void setRegion(Short region) {
		this.region = region;
	}

	public String getFamilyId() {
		return familyId;
	}

	public void setFamilyId(String familyId) {
		this.familyId = familyId;
	}

	private Date day;

	// 关键字搜索 gfwang add
	private String keyWord;

	public String getKeyWord() {
		return keyWord;
	}

	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}

	public Date getDay() {
		return day;
	}

	public void setDay(Date day) {
		this.day = day;
	}

	public Boolean getNotLeaveFlag() {
		return notLeaveFlag;
	}

	public void setNotLeaveFlag(Boolean notLeaveFlag) {
		this.notLeaveFlag = notLeaveFlag;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public List<String> getAccountIds() {
		return accountIds;
	}

	public void setAccountIds(List<String> accountIds) {
		this.accountIds = accountIds;
	}

	public List<String> getRoomIds() {
		return roomIds;
	}

	public void setRoomIds(List<String> roomIds) {
		this.roomIds = roomIds;
	}

	public List<Short> getRoleTypes() {
		return roleTypes;
	}

	public void setRoleTypes(List<Short> roleTypes) {
		this.roleTypes = roleTypes;
	}

	public List<String> getCentreIds() {
		return centreIds;
	}

	public void setCentreIds(List<String> centreIds) {
		this.centreIds = centreIds;
	}

	public String getPsw() {
		return psw;
	}

	public void setPsw(String psw) {
		this.psw = psw;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getCentreId() {
		return centreId;
	}

	public void setCentreId(String centreId) {
		this.centreId = centreId;
	}

	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public Short getUserType() {
		return userType;
	}

	public void setUserType(Short userType) {
		this.userType = userType;
	}

	public Short getRoleType() {
		return roleType;
	}

	public void setRoleType(Short roleType) {
		this.roleType = roleType;
	}

	public Short getStatus() {
		return status;
	}

	public void setStatus(Short status) {
		this.status = status;
	}

	public UserCondition() {

	}

	public UserCondition(String accountId) {
		this.accountId = accountId;
	}
}