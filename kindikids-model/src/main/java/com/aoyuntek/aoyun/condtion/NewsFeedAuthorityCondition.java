package com.aoyuntek.aoyun.condtion;

import java.util.List;

import com.aoyuntek.aoyun.entity.vo.RoleInfoVo;
import com.aoyuntek.framework.condtion.BaseCondition;

public class NewsFeedAuthorityCondition extends BaseCondition {
    public NewsFeedAuthorityCondition() {

    }

    /**
     * 当前人所属分组
     */
    private List<String> groupList;

    /**
     * 当前人accountId
     */
    private String currtenAccountId;

    /**
     * 当前人的centerId
     */
    private String currtenCenterId;

    /**
     * 当前人的角色
     */
    private List<RoleInfoVo> currtenRoleList;

    /**
     * 当前人的room
     */
    private String currtenRoomId;

    /**
     * 家长的小孩所在的所有园区集合
     */
    private List<String> parentCenterIds;

    /**
     * 家长的小孩所在的所有room集合
     */
    private List<String> parentRoomIds;
    
    
    public List<String> getParentRoomIds() {
        return parentRoomIds;
    }

    public void setParentRoomIds(List<String> parentRoomIds) {
        this.parentRoomIds = parentRoomIds;
    }

    /**
     * 是否有ceo角色
     */
    private short ceoFlag = -1;

    public NewsFeedAuthorityCondition(List<String> groupList, String currtenAccountId, String currtenCenterId, List<RoleInfoVo> currtenRoleList,
            String currtenRoomId, List<String> parentCenterIds, short ceoFlag) {
        super();
        this.groupList = groupList;
        this.currtenAccountId = currtenAccountId;
        this.currtenCenterId = currtenCenterId;
        this.currtenRoleList = currtenRoleList;
        this.currtenRoomId = currtenRoomId;
        this.parentCenterIds = parentCenterIds;
        this.ceoFlag = ceoFlag;
    }

    public short getCeoFlag() {
        return ceoFlag;
    }

    public void setCeoFlag(short ceoFlag) {
        this.ceoFlag = ceoFlag;
    }

    public List<String> getParentCenterIds() {
        return parentCenterIds;
    }

    public void setParentCenterIds(List<String> parentCenterIds) {
        this.parentCenterIds = parentCenterIds;
    }

    public String getCurrtenRoomId() {
        return currtenRoomId;
    }

    public void setCurrtenRoomId(String currtenRoomId) {
        this.currtenRoomId = currtenRoomId;
    }

    public List<RoleInfoVo> getCurrtenRoleList() {
        return currtenRoleList;
    }

    public void setCurrtenRoleList(List<RoleInfoVo> currtenRoleList) {
        this.currtenRoleList = currtenRoleList;
    }

    public String getCurrtenCenterId() {
        return currtenCenterId;
    }

    public void setCurrtenCenterId(String currtenCenterId) {
        this.currtenCenterId = currtenCenterId;
    }

    public String getCurrtenAccountId() {
        return currtenAccountId;
    }

    public void setCurrtenAccountId(String currtenAccountId) {
        this.currtenAccountId = currtenAccountId;
    }

    public List<String> getGroupList() {
        return groupList;
    }

    public void setGroupList(List<String> groupList) {
        this.groupList = groupList;
    }
}
