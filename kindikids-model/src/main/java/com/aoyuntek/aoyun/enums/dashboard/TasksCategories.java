package com.aoyuntek.aoyun.enums.dashboard;

public enum TasksCategories {
    CentreTasks((short) 0, "Centre Tasks"), RoomTasks((short) 1, "Room Tasks"), ChildrenTasks((short) 2, "Children Tasks"), OtherTasks((short) 3,
            "Other Tasks");

    private short value;
    private String desc;

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private TasksCategories(short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

}
