package com.aoyuntek.aoyun.enums;

/**
 * @description Eylf选择状态
 * @author Hxzhang 2016年9月22日上午9:54:16
 * @version 1.0
 */
public enum EylfCheckValue {
    Achieved((short) 0, "实现"), NotAchieved((short) 1, "没有实现"), NotAssessed((short) 2, "没有评估");

    private short value;

    private String desc;

    private EylfCheckValue(short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
