package com.aoyuntek.aoyun.enums.dashboard;


public enum SelectType {

    child((short) 0, "child"), staff((short) 1, "staff"), center((short) 2, "center"), room((short) 3, "room"), organisation((short) 4,
            "organisation");

    private short value;
    private String desc;

    public static SelectType getType(short value) {
        SelectType[] items = SelectType.values();
        for (SelectType item : items) {
            if (item.getValue() == value) {
                return item;
            }
        }
        return null;
    }

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private SelectType(short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

}
