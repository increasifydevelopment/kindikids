package com.aoyuntek.aoyun.enums;

/**
 * 
 * @author dlli5 at 2016年9月21日下午7:20:16
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public enum TaskFollowUpStatu {
    Default((short) 0, "Default"), COMPLETE((short) 1, "COMPLETE"), OVERDUE((short) 2, "OVERDUE");

    private short value;
    private String name;

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private TaskFollowUpStatu(short value, String name) {
        this.value = value;
        this.name = name;
    }
}
