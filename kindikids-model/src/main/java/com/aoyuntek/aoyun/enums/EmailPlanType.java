package com.aoyuntek.aoyun.enums;

public enum EmailPlanType {
	A((short) 1, "外部欢迎邮件"), B((short) 2, "内部未来入园"), C((short) 3, "入园");

	private short value;
	private String desc;

	public short getValue() {
		return value;
	}

	public void setValue(short value) {
		this.value = value;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	private EmailPlanType(short value, String desc) {
		this.value = value;
		this.desc = desc;
	}
}
