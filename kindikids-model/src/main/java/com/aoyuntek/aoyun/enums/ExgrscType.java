package com.aoyuntek.aoyun.enums;

/**
 * @description Porgram类型
 * @author Hxzhang 2016年9月20日下午6:27:35
 * @version 1.0
 */
public enum ExgrscType {
    /**
     * Explore Curriculum
     */
    ExploreCurriculum((short) 0, "Explore Curriculum"),
    /**
     * Grow Curriculum
     */
    GrowCurriculum((short) 1, "Grow Curriculum"),
    /**
     * School Readiness
     */
    SchoolReadiness((short) 2, "School Readiness");

    private short value;
    private String desc;

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private ExgrscType(short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

}
