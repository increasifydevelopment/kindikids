package com.aoyuntek.aoyun.enums;

public enum EmailType {
    inside((short) 0, "inside"), 
    intoCenter((short) 1, "intoCenter"), 
    leaveCenter((short) 2, "leaveCenter"), 
    roster_approved((short) 3,"roster_approved"),
    givenoticeApproved((short) 4, "givenoticeApproved"),
    archiveChild((short) 5, "archiveChild"), 
    changeCentre((short) 6, "changeCentre"), 
    changeRoom((short) 7, "changeRoom"), 
    staff_leave_to_admin((short) 8, "staff_leave_to_admin"),
    staff_leave_for_admin_apply((short)9, "staff_leave_for_admin_apply"), 
    absentee_approved_email((short) 10, "absentee_approved_email"),
    absent_over_42days((short) 11, "absent_over_42days"),
    requestChangeCenter((short) 12, "requestChangeCenter"),
    staff_leave_ceo_approve((short) 13, "staff_leave_ceo_approve"), 
    staff_leave_ceo_cancle((short) 14,"staff_leave_ceo_cancle"), 
    staff_leave_ceo_rejected((short) 15, "staff_leave_ceo_rejected"), 
    staff_leave_ceo_lapsed((short)16,"staff_leave_lapsed"),
    child_not_sign_out((short) 17, "child_not_sign_out"),
    admin_apply_staff_send_admin((short)18,"admin_apply_staff_send_admin"),
    out_add_child((short)19,"out_add_child"),
    request_attend((short) 20,"parnet_request_attend"),
    request_givenotice((short)21,"request_givenotice"),
    cancel_givenotice((short)22, "cancel_givenotice"),
    request_absentee((short)23,"request_absentee"),
    cancel_absentee((short)24,"cancel_absentee"),
    approve_attendance((short)25,"approve_attendance"),
    cancel_attendance((short)26,"cancel_attendance"),
    lapsed_absentee((short)24,"lapsed_absentee"),
    request_changCentre((short)25,"request_changCentre"),
    approve_changCentre((short)26,"approve_changCentre"),
    cancel_changCentre((short)27,"cancel_changCentre");

    private short value;
    private String desc;

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private EmailType(short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public static EmailType getType(short value) {
        EmailType[] items = EmailType.values();
        for (EmailType item : items) {
            if (item.getValue() == value) {
                return item;
            }
        }
        return null;
    }
}
