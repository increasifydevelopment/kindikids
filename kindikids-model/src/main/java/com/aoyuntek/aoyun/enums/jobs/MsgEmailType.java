package com.aoyuntek.aoyun.enums.jobs;

public enum MsgEmailType {
    /**
     * 家长欢迎邮件
     */
    parent_welcome((short) 0, "parent_welcome"),
    /**
     * 员工欢迎邮件
     */
    staff_welcome((short) 1, "staff_welcome"),
    /**
     * 小孩未签到给管理员发送短信
     */
    manager_msg((short) 2, "manager_msg"),
    /**
     *  员工请假lapsed
     */
    staff_leave_lapsed((short)3,"staff_leave_lapsed");

    private short value;
    private String desc;

    private MsgEmailType(short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
