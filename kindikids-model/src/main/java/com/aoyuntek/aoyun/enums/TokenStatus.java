package com.aoyuntek.aoyun.enums;

public enum TokenStatus {
	Used((short) 1, "已使用"), Unused((short) 0, "未使用"), ;

	private Short value;

	private String desc;

	/**
	 * 
	 * @param value
	 * @param desc
	 */
	private TokenStatus(Short value, String desc) {
		this.value = value;
		this.desc = desc;
	}

	public Short getValue() {
		return value;
	}

	public void setValue(Short value) {
		this.value = value;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
}
