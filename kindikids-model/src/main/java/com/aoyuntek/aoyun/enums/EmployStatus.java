package com.aoyuntek.aoyun.enums;

public enum EmployStatus {
	Request((short) 0, "申请"), Executed((short) 1, "已执行"), Cancelled((short) 2, "已取消");

	private short value;
	private String desc;

	public short getValue() {
		return value;
	}

	public void setValue(short value) {
		this.value = value;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	private EmployStatus(short value, String desc) {
		this.value = value;
		this.desc = desc;
	}
}
