package com.aoyuntek.aoyun.enums;

public enum Holiday {
	YES((short) 0), NO((short) 1);

	private short value;

	public short getValue() {
		return value;
	}

	public void setValue(short value) {
		this.value = value;
	}

	private Holiday(short value) {
		this.value = value;
	}

}
