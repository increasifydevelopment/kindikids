package com.aoyuntek.aoyun.enums;

/**
 * 
 * @author dlli5 at 2016年8月22日上午10:42:28
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public enum EnrolledState {

	Default((short)0, "默认未入院"), Enrolled((short)1, "已入院"), ;

	private Short value;

	private String desc;

	/**
	 * 
	 * @param value
	 * @param desc
	 */
	private EnrolledState(Short value, String desc) {
		this.value = value;
		this.desc = desc;
	}

	public Short getValue() {
		return value;
	}

	public void setValue(Short value) {
		this.value = value;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
}