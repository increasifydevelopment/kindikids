package com.aoyuntek.aoyun.enums;

/**
 * 
 * @description kk角色
 * @author gfwang
 * @create 2016年6月21日上午8:39:41
 * @version 1.0
 */
public enum Role {

    /**
     * CEO
     */
    CEO((short) 0, "CEO"),

    /**
     * 园长
     */
    CentreManager((short) 1, "Centre Manager"),

    /**
     * 老师
     */
    Educator((short) 2, "Educator"),

    /**
     * 上过大学教师
     */
    EducatorECT((short) 3, "ECT"),
    /**
     * 临时教师
     */
    Casual((short) 4, "Casual"),
    /**
     * 二级园长
     */
    EducatorSecondInCharge((short) 5, "Second In Charge"),
    /**
     * 
     */
    EducatorCertifiedSupersor((short) 6, "Certified Supervisor"),
    /**
     * 
     */
    EducatorFirstAidOfficer((short) 7, "First Aid Officer"),
    /**
     * 
     */
    EducatorNominatedSupervisor((short) 8, "Nominated Supervisor"),
    /**
     * 厨师
     */
    Cook((short) 9, "Cook"),

    /**
     * 家长
     */
    Parent((short) 10, "Parent"),

    /**
     * 小孩
     */
    Children((short) 11, "Children"),

    Diploma((short)12,"Diploma"),
    
    Educational_Leader((short)13,"Educational Leader");
    
    private short value;

    private String name;

    public static Role getType(short value) {
        Role[] items = Role.values();
        for (Role item : items) {
            if (item.getValue() == value) {
                return item;
            }
        }
        return null;
    }

    public static String getNameByValue(short value) {
        for (Role role : Role.values()) {
            if (value == role.getValue()) {
                return role.getName();
            }
        }
        return "";
    }

    public static Short getValueByName(String name) {
        for (Role role : Role.values()) {
            if (role.getName().equals(name)) {
                return role.getValue();
            }
        }
        return null;
    }

    private Role(short value, String name) {
        this.value = value;
        this.name = name;
    }

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
