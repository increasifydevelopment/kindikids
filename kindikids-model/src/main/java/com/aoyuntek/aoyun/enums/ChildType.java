package com.aoyuntek.aoyun.enums;

/**
 * 
 * @author dlli5 at 2016年8月29日下午6:48:44
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public enum ChildType {
    /*
     * 内部
     */
    Inside((short) 0, "Inside"),/*
     * 外部
     */
    External((short) 1, "External");

    private short value;
    private String desc;

    private ChildType(short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
