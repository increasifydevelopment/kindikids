package com.aoyuntek.aoyun.enums;

/**
 * 
 * @description 第几周
 * @author gfwang
 * @create 2016年8月20日下午2:01:15
 * @version 1.0
 */
public enum WeekNum {
    /**
     * 第一周
     */
    ONE((short) 1, "WEEK 1"),
    /**
     * 第二周
     */
    TWO((short) 2, "WEEK 2"),
    /**
     * 第三周
     */
    THREE((short) 3, "WEEK 3"),
    /**
     * 第四周
     */
    FOUR((short) 4, "WEEK 4");

    private short value;
    private String name;

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    private WeekNum(short value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
