package com.aoyuntek.aoyun.enums.common;

/**
 * 
 * @description 分组标识
 * @author bbq
 * @create 2016年7月9日下午3:32:13
 * @version 1.0
 */
public enum AuthFunctionType {
     BeforLogin((short) 0, "登陆前"),AfterLogin((short) 1, "登陆后"),Others((short)2,"其它");

    private short value;
    private String desc;

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private AuthFunctionType(short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

}
