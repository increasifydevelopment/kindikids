package com.aoyuntek.aoyun.enums;

public enum ProfileTempType {
    child((short) 0, "child"), staff((short) 1, "staff");

    private short value;
    private String desc;

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private ProfileTempType(short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

}
