package com.aoyuntek.aoyun.enums;

import java.util.Arrays;
import java.util.List;

/**
 * @description 请假类别枚举
 * @author hxzhang
 * @create 2016年8月16日上午9:38:59
 * @version 1.0
 */
public enum LeaveType {
	/**
	 * Personal Leave
	 */
	PersonalLeave((short) 0, "Personal Leave"),
	/**
	 * Annual Leave
	 */
	AnnualLeave((short) 1, "Annual Leave"),
	/**
	 * Long Service Leave
	 */
	LongServiceLeave((short) 2, "Long Service Leave"),
	/**
	 * Maternity Leave
	 */
	ParentLeave((short) 3, "Parental Leave"),
	/**
	 * Professional Development Leave
	 */
	ProfessionalDevelopmentLeave((short) 4, "Professional Development Leave"),
	/**
	 * Compassionate Leave
	 */
	CompassionateLeave((short) 5, "Compassionate Leave"),
	/**
	 * Unpaid Leave
	 */
	OtherLeave((short) 6, "Other Leave"),
	/**
	 * RDO
	 */
	RDO((short) 7, "RDO"),
	/**
	 * Change Of Shift
	 */
	ChangeOfShift((short) 8, "Change of Shift"),
	/**
	 * Change of RDO
	 */
	ChangeOfRDO((short) 9, "Change of RDO"),
	/**
	 * Casual Unavailability
	 */
	CasualUnavailability((short) 10, "Casual Unavailability"),
	/**
	 * Practicum (Uni/TAFE leave)
	 */
	Practicum((short) 11, "Practicum (Uni/TAFE leave)"),
	/**
	 * Jury Duty
	 */
	JuryDuty((short) 12, "Jury Duty"),
	/**
	 * Workers Compensation
	 */
	WorkersCompensation((short) 13, "Workers Compensation"),
	/**
	 * Time in Lieu
	 */
	TimeinLieu((short) 14, "Time in Lieu");

	private short value;
	private String desc;

	public static List<LeaveType> getList() {
		LeaveType[] array = new LeaveType[] { LeaveType.PersonalLeave, LeaveType.AnnualLeave, LeaveType.LongServiceLeave, LeaveType.ParentLeave,
				LeaveType.ProfessionalDevelopmentLeave, LeaveType.CompassionateLeave, LeaveType.OtherLeave, LeaveType.RDO };
		return Arrays.asList(array);
	}

	public static List<LeaveType> getMyLeaveList() {
		LeaveType[] array = new LeaveType[] { LeaveType.ChangeOfRDO, LeaveType.ChangeOfShift, LeaveType.PersonalLeave, LeaveType.AnnualLeave, LeaveType.LongServiceLeave,
				LeaveType.ParentLeave, LeaveType.ProfessionalDevelopmentLeave, LeaveType.CompassionateLeave, LeaveType.OtherLeave, LeaveType.CasualUnavailability,
				LeaveType.Practicum, LeaveType.TimeinLieu, LeaveType.JuryDuty, LeaveType.WorkersCompensation };
		return Arrays.asList(array);
	}

	public static List<LeaveType> getRosterLeaveList() {
		LeaveType[] array = new LeaveType[] { LeaveType.RDO, LeaveType.AnnualLeave, LeaveType.ProfessionalDevelopmentLeave, LeaveType.LongServiceLeave,
				LeaveType.OtherLeave, LeaveType.CasualUnavailability, LeaveType.Practicum, LeaveType.JuryDuty, LeaveType.WorkersCompensation, LeaveType.TimeinLieu,
				LeaveType.ParentLeave };
		return Arrays.asList(array);
	}

	public static List<LeaveType> getCeoLeaveList() {
		LeaveType[] array = new LeaveType[] { LeaveType.RDO, LeaveType.AnnualLeave, LeaveType.PersonalLeave, LeaveType.LongServiceLeave,
				LeaveType.ProfessionalDevelopmentLeave, LeaveType.CompassionateLeave, LeaveType.OtherLeave, LeaveType.CasualUnavailability, LeaveType.Practicum,
				LeaveType.JuryDuty, LeaveType.WorkersCompensation, LeaveType.TimeinLieu, LeaveType.ParentLeave };
		return Arrays.asList(array);
	}

	private LeaveType(short value, String desc) {
		this.value = value;
		this.desc = desc;
	}

	public short getValue() {
		return value;
	}

	public void setValue(short value) {
		this.value = value;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public static String getDescByValue(short value) {
		for (LeaveType type : LeaveType.values()) {
			if (value == type.getValue()) {
				return type.getDesc();
			}
		}
		return "";
	}

	public static List<Short> getLeaveForDay() {
		Short[] array = new Short[] { LeaveType.RDO.getValue(), LeaveType.LongServiceLeave.getValue(), LeaveType.ParentLeave.getValue() };
		return Arrays.asList(array);
	}

	public static List<Short> getLeaveForTime() {
		Short[] array = new Short[] { LeaveType.AnnualLeave.getValue(), LeaveType.PersonalLeave.getValue(), LeaveType.ProfessionalDevelopmentLeave.getValue(),
				LeaveType.CompassionateLeave.getValue(), LeaveType.OtherLeave.getValue(), LeaveType.CasualUnavailability.getValue(), LeaveType.Practicum.getValue(),
				LeaveType.JuryDuty.getValue(), LeaveType.WorkersCompensation.getValue(), LeaveType.TimeinLieu.getValue() };
		return Arrays.asList(array);
	}
	
	
}
