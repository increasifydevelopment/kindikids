package com.aoyuntek.aoyun.enums;

public enum EmailMsgType {
    msg_out_add((short) 1,"外部创建孩子"),
    msg_out_inside((short) 2,"孩子从外部到内部"),
    msg_into_center((short) 3,"孩子入园"),
    msg_more_42((short) 4,"请假超过42天"),
    msg_approve_give((short) 5,"离园申请被批准"),
    msg_leave_center((short) 6,"孩子离园"),
    msg_welcome((short) 7,"欢迎邮件");;
    
    private short value;
    private String desc;
    public short getValue() {
        return value;
    }
    public void setValue(short value) {
        this.value = value;
    }
    public String getDesc() {
        return desc;
    }
    public void setDesc(String desc) {
        this.desc = desc;
    }
    private EmailMsgType(short value, String desc) {
        this.value = value;
        this.desc = desc;
    }
}
