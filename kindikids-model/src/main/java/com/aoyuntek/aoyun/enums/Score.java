package com.aoyuntek.aoyun.enums;

public enum Score {
    /**
     * Very high quality
     */
    VeryHighQuality((short) 5, "Very high quality"),
    /**
     * High quality
     */
    HighQuality((short) 4, "High quality"),
    /**
     * Neither high nor low quality
     */
    NeitherHighNorLowQuality((short) 3, "Neither high nor low quality"),
    /**
     * Low quality
     */
    LowQuality((short) 2, " Low quality"),
    /**
     * Very low quality
     */
    VeryLowQuality((short) 1, "Very low quality");

    private short value;

    private String name;

    public static String getNameByValue(short value) {
        for (Role role : Role.values()) {
            if (value == role.getValue()) {
                return role.getName();
            }
        }
        return "";
    }

    private Score(short value, String name) {
        this.value = value;
        this.name = name;
    }

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
