package com.aoyuntek.aoyun.enums;

public enum UserType {
    Staff((short) 0, "员工"), Parent((short) 1, "家长"), Child((short) 2, "小孩");
    private Short value;

    private String desc;

    /**
     * 
     * @param value
     * @param desc
     */
    private UserType(Short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public Short getValue() {
        return value;
    }

    public void setValue(Short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
