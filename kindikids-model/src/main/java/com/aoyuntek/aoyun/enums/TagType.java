package com.aoyuntek.aoyun.enums;

public enum TagType {
    UnTagChild((short) 0, "非关联小孩"), TagChild((short) 1, "关联小孩");

    private short value;
    private String Desc;

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public String getDesc() {
        return Desc;
    }

    public void setDesc(String desc) {
        Desc = desc;
    }

    private TagType(short value, String desc) {
        this.value = value;
        Desc = desc;
    }

}
