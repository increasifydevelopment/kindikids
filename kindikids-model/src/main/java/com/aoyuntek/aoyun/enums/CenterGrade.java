package com.aoyuntek.aoyun.enums;

/**
 * 
 * @description 园区
 * @author bbq
 * @create 2016年7月9日下午3:32:13
 * @version 1.0
 */
public enum CenterGrade {
    /**
     * 园1
     */
    One((short) 0, "One"),
    /**
     * 园1
     */
    Two((short) 1, "Two"), 
    /**
     * 园3
     */
    Three((short) 1, "Three");

    private short value;
    private String desc;

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private CenterGrade(short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

}
