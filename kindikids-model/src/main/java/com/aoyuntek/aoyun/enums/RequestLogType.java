package com.aoyuntek.aoyun.enums;

/**
 * 
 * @author dlli5 at 2016年8月23日上午10:50:31
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public enum RequestLogType {
    ParentRequest((short) 0, "家長申請"),
    CentreManagerRequest((short) 1, "园长申请转园"),
    CeoChangeCentre((short) 2, "CEO直接转园"),
    CentreManagerSure((short) 3, "园长確認转园申请"),
    ChangeRoom((short) 4, "Transfer this child to another room "),
    ChangeAttendance((short) 5, "CEO 园长直接更改课程Change attendance"),
    CEOExternal2Inside((short) 6, "CEOExternal2Inside"),
    InitAttendance((short) 7, "InitAttendance"),
    AddAttendance((short) 8, "AddAttendance"),
    TemporaryAttendance((short) 10, "AddAttendance"),
    SubtractedAttendance((short) 11, "SubtractedAttendance"),
    GiveNoteRequest((short) 12, "离园申请"),
    AbsenteeRequest((short) 13, "请假申请");
	
    private short value;
    private String name;

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    private RequestLogType(short value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
