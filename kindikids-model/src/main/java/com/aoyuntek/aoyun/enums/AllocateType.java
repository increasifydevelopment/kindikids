package com.aoyuntek.aoyun.enums;

public enum AllocateType {
	// 1
	ApplicationToExternal((short) 0, (short) 1, "ApplicationToExternal"),
	// 2
	ApplicationToSibling((short) 0, (short) 2, "ApplicationToSibling"),
	// 3
	ApplicationToArchived((short) 0, (short) 3, "ApplicationToArchived"),
	// 4
	ExternalToApplication((short) 1, (short) 0, "ExternalToApplication"),
	// 5
	ExternalToSibling((short) 1, (short) 2, "ExternalToSibling"),
	// 6
	ExternalToArchived((short) 1, (short) 3, "ExternalToArchived"),
	// 7
	SiblingToApplication((short) 2, (short) 0, "SiblingToApplication"),
	// 8
	SiblingToExternal((short) 2, (short) 1, "SiblingToExternal"),
	// 9
	SiblingToArchived((short) 2, (short) 3, "SiblingToArchived"),
	// 10
	ArchivedToApplication((short) 3, (short) 0, "ArchivedToApplication"),
	// 11
	ArchivedToExternal((short) 3, (short) 1, "ArchivedToExternal"),
	// 12
	ArchivedToSibling((short) 3, (short) 2, "ArchivedToSibling");

	private short fromType;
	private short moveToType;
	private String desc;

	public short getFromType() {
		return fromType;
	}

	public void setFromType(short fromType) {
		this.fromType = fromType;
	}

	public short getMoveToType() {
		return moveToType;
	}

	public void setMoveToType(short moveToType) {
		this.moveToType = moveToType;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	private AllocateType(short fromType, short moveToType, String desc) {
		this.fromType = fromType;
		this.moveToType = moveToType;
		this.desc = desc;
	}

	public static AllocateType getType(short fromType, short moveToType) {
		AllocateType[] allocateTypes = AllocateType.values();
		for (AllocateType type : allocateTypes) {
			if (type.getFromType() == fromType && type.getMoveToType() == moveToType) {
				return type;
			}
		}
		return null;
	}
}
