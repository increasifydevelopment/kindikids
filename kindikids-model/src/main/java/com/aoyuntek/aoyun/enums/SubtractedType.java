package com.aoyuntek.aoyun.enums;

public enum SubtractedType {
    /*
     * 今天
     */
    DAY((short) 1, "ONE"),ALL((short) 0, "ALL");

    private short value;
    private String desc;

    private SubtractedType(short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
