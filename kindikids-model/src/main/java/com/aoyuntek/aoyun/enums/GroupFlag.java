package com.aoyuntek.aoyun.enums;

/**
 * 
 * @description 分组标识
 * @author bbq
 * @create 2016年7月9日下午3:32:13
 * @version 1.0
 */
public enum GroupFlag {
    Out((short) 0, "非分组内"), In((short) 1, "分组内");

    private short value;
    private String desc;

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private GroupFlag(short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

}
