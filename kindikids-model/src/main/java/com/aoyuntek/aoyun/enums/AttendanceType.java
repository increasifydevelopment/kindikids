package com.aoyuntek.aoyun.enums;

/**
 * 
 * @author dlli5 at 2016年8月22日上午10:42:28
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public enum AttendanceType {

    Child((short) 0, "小孩"), Staff((short) 1, "员工"), Vistor((short) 2, "访客");

    private Short value;

    private String desc;

    /**
     * 
     * @param value
     * @param desc
     */
    private AttendanceType(Short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public Short getValue() {
        return value;
    }

    public void setValue(Short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}