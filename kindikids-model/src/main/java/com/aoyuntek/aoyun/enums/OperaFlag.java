package com.aoyuntek.aoyun.enums;

/**
 * 刪除標示
 * 
 * @author dlli5 at 2016年4月26日下午4:05:00
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public enum OperaFlag {

    Yes(true, "可操作"), No(false, "不可操作"), ;

    private boolean value;

    private String desc;

    /**
     * 
     * @param value
     * @param desc
     */
    private OperaFlag(boolean value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public boolean getValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}