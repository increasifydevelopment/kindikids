package com.aoyuntek.aoyun.enums.meeting;

public enum MeetingMinutes {
    /**
     * ActionItem
     */
    ActionItem((short) 0, "ActionItem"),
    /**
     * Responsibility
     */
    Responsibility((short) 1, "Responsibility"),
    /**
     * Timeframe
     */
    Timeframe((short) 2, "Timeframe"),
    /**
     * Status
     */
    Status((short) 3, "Status");

    private short values;
    private String desc;

    private MeetingMinutes(short values, String desc) {
        this.values = values;
        this.desc = desc;
    }

    public short getValues() {
        return values;
    }

    public void setValues(short values) {
        this.values = values;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
