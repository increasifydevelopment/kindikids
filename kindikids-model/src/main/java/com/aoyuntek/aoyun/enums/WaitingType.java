package com.aoyuntek.aoyun.enums;

public enum WaitingType {
	ApplicationList((short) 0, "Application List"), ExternalWaitList((short) 1, "External Wait List"), SiblingWaitList((short) 2,
			"Sibling Wait List"), ArchivedApplicationList((short) 3, "Archived Application List");
	private short value;
	private String desc;

	public short getValue() {
		return value;
	}

	public void setValue(short value) {
		this.value = value;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	private WaitingType(short value, String desc) {
		this.value = value;
		this.desc = desc;
	}

	public static WaitingType getType(short value) {
		WaitingType[] types = WaitingType.values();
		for (WaitingType type : types) {
			if (type.getValue() == value) {
				return type;
			}
		}
		return null;
	}
}
