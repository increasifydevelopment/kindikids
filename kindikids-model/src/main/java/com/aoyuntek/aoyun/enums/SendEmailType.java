package com.aoyuntek.aoyun.enums;

public enum SendEmailType {
    isManual((short) 0, "isManual"), haveCancel((short) 1, "haveCancel"), timedTask((short) 2, "timedTask"), welTimedTask((short) 3, "welTimedTask");
    private short value;
    private String desc;

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private SendEmailType(short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

}
