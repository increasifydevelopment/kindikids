package com.aoyuntek.aoyun.enums;

public enum CheckFlag {
    check((short) 1, "选择"), unCheck((short) 0, "未选择");

    private short value;
    private String desc;

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private CheckFlag(short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

}
