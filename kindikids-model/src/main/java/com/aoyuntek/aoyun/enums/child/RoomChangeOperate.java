package com.aoyuntek.aoyun.enums.child;


/**
 * 
 * 
 * @author dlli5 at 2016年12月13日上午9:53:10
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public enum RoomChangeOperate {
    ADDALL((short) 0, "添加"), REMOVEALL((short) 1, "减少"),ADDDAY((short) 2, "添加"), REMOVEDAY((short) 3, "减少");

    private Short value;

    private String desc;

    public static RoomChangeOperate getType(short value) {
        RoomChangeOperate[] items = RoomChangeOperate.values();
        for (RoomChangeOperate item : items) {
            if (item.getValue() == value) {
                return item;
            }
        }
        return null;
    }

    /**
     * 
     * @param value
     * @param desc
     */
    private RoomChangeOperate(Short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public Short getValue() {
        return value;
    }

    public void setValue(Short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
