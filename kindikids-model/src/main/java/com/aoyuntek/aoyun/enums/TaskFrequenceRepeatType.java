package com.aoyuntek.aoyun.enums;

/**
 * 
 * @author dlli5 at 2016年8月22日上午10:42:28
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public enum TaskFrequenceRepeatType {

    Daily((short) 0, "Daily"), Weekly((short) 1, "Weekly"), Monthly((short) 2, "Monthly"), Quarterly((short) 3, "Quarterly"), Yearly((short) 4,
            "Yearly"), OnceOff((short) 5, "OnceOff"), WhenRequired((short) 6, "WhenRequired");

    private Short value;

    private String desc;
    
    public static TaskFrequenceRepeatType getType(short value){
        TaskFrequenceRepeatType[] items = TaskFrequenceRepeatType.values();
        for (TaskFrequenceRepeatType item : items) {
            if(item.getValue()==value){
                return  item;
            }
        }
        return null;
    }

    /**
     * 
     * @param value
     * @param desc
     */
    private TaskFrequenceRepeatType(Short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public Short getValue() {
        return value;
    }

    public void setValue(Short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}