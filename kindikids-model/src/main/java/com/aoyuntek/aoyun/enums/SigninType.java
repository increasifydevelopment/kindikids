package com.aoyuntek.aoyun.enums;

/**
 * 签到类型
 * @author dlli5 at 2016年8月31日下午9:29:03
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public enum SigninType {
	IN((short) 0, "签入"),OUT((short) 1, "签出");

    private short value;
    private String desc;

    private SigninType(short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
