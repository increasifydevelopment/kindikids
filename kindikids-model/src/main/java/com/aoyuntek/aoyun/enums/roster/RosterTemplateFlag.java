package com.aoyuntek.aoyun.enums.roster;

/**
 * 
 * @author dlli5 at 2016年10月14日下午1:42:39
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public enum RosterTemplateFlag {

    NO((short) 0, "NO"), Yes((short) 1, "Yes");

    /**
     * 值
     */
    private Short value;
    /**
     * 描述
     */
    private String desc;

    private RosterTemplateFlag(Short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public Short getValue() {
        return value;
    }

    public void setValue(Short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
