package com.aoyuntek.aoyun.enums;

public enum SigninState {
	NoSignin((short) 0, "未打卡"),Signin((short) 1, "已打卡");

    private short value;
    private String desc;

    private SigninState(short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
