package com.aoyuntek.aoyun.enums;

public enum AttendanceManagementType {
    // type : null All Type
    // type : 0 Giving Notice
    // type : 1 Absence Request
    // type : 2 Change of Room
    // type : 3 Change of Centre
    // type : 4 Change of Attendance
    Default((short) -1, "default"), GivingNotice((short) 0, "Giving Notice"), AbsenceRequest((short) 1, "Absence Request"), ChangeofRoom((short) 2,
            "Change of Room"), ChangeofCentre((short) 3, "Change of Centre"), ChangeofAttendance((short) 4, "Change of Attendance");

    private short value;
    private String desc;

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private AttendanceManagementType(short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

}
