package com.aoyuntek.aoyun.enums;

public enum TaskType {
    /**
     * Explore Curriculum
     */
    ExploreCurriculum((short) 0, "Explore Curriculum"),
    /**
     * Grow Curriculum
     */
    GrowCurriculum((short) 1, "Grow Curriculum"),
    /**
     * Lesson
     */
    Lesson((short) 2, "Lesson"),
    /**
     * School Readiness
     */
    SchoolReadiness((short) 3, "School Readiness"),
    /**
     * Interest
     */
    Interest((short) 4, "Interest"),
    /**
     * Interest Follow up
     */
    InterestFollowup((short) 5, "Interest Follow up"),
    /**
     * Observation
     */
    Observation((short) 6, "Observation"),
    /**
     * Observation Follow up
     */
    ObservationFollowup((short) 7, "Observation Follow up"),
    /**
     * Learning Story
     */
    LearningStory((short) 8, "Learning Story"),
    /**
     * Learning Story Follow up
     */
    LearningStoryFollowup((short) 9, "Learning Story Follow up"),
    /**
     * Weekly Evalution
     */
    WeeklyEvalution((short) 10, "Weekly Evalution"),
    /**
     * Meal Register
     */
    MealRegister((short) 11, "Meal Register"),
    /**
     * Sleep Register
     */
    SleepRegister((short) 12, "Sleep Register"),
    /**
     * Nappy Register
     */
    NappyRegister((short) 13, "Nappy Register"),
    /**
     * Toilet Register
     */
    ToiletRegister((short) 14, "Toilet Register"),
    /**
     * Sunscreen Application Register
     */
    SunscreenRegister((short) 15, "Sunscreen Application Register"),
    /**
     * Other Task
     */
    OtherTask((short) 16, "Other Task"),

    CustomTask((short) 17, "CustomTask"),

    AdministrationOfMedication((short) 18, "Administration of Medication"),

    CustomTaskFollowUp((short) 19, "CustomTaskFollowUp"),

    MeetingTask((short) 20, "MeetingTask"),

    DepositTask((short) 21, "DepositTask"),

    HatTask((short) 22, "HatTask");

    public static TaskType getType(short value) {
        TaskType[] items = TaskType.values();
        for (TaskType item : items) {
            if (item.getValue() == value) {
                return item;
            }
        }
        return null;
    }

    private short value;
    private String desc;

    private TaskType(short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
