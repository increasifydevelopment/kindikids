package com.aoyuntek.aoyun.enums;

/**
 * 资质证书
 * 
 * @author rick
 *
 */
public enum Credential {

	Qualification((short) 1, "Qualification"),

	FirstAids((short) 2, "First Aids"),

	ChildrenCheck((short) 3, "Working with Children's Check"),

	Trainings((short) 4, "Trainings"),

	Supervisors((short) 5, "Certified Supervisors");

	private short value;

	private String name;

	private Credential(short value, String name) {
		this.value = value;
		this.name = name;
	}

	public static String getNameByValue(short value) {
		for (Role role : Role.values()) {
			if (value == role.getValue()) {
				return role.getName();
			}
		}
		return "";
	}

	public short getValue() {
		return value;
	}

	public void setValue(short value) {
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
