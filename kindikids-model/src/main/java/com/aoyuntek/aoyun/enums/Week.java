package com.aoyuntek.aoyun.enums;

/**
 * 
 * @description 周1-周7
 * @author gfwang
 * @create 2016年8月20日下午2:00:59
 * @version 1.0
 */
public enum Week {
    /**
     * 周1
     */
    Monday((short) 1, "Monday"),
    /**
     * 周2
     */
    Tuesday((short) 2, "Tuesday"),
    /**
     * 周3
     */
    Wednesday((short) 3, "Wednesday"),
    /**
     * 周4
     */
    Thursday((short) 4, "Thursday"),
    /**
     * 周5
     */
    Friday((short) 5, "Friday"),
    /**
     * 周6
     */
    Saturday((short) 6, "Saturday"),
    /**
     * 周7
     */
    Sunday((short) 7, "Sunday");

    private short value;
    private String name;

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    private Week(short value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
