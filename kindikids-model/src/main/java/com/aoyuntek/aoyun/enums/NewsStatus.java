package com.aoyuntek.aoyun.enums;

/**
 * @description News状态
 * @author hxzhang
 * @create 2016年6月28日上午8:58:21
 * @version 1.0
 */
public enum NewsStatus {
    /**
     * Delete
     */
    Delete((short) 2, "Delete"),
    /**
     * Appreoved
     */
    Appreoved((short) 1, "Appreoved"),
    /**
     * Draft
     */
    Draft((short) 0, "Draft"), ;

    /**
     * 值
     */
    private Short value;
    /**
     * 描述
     */
    private String desc;

    private NewsStatus(Short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public Short getValue() {
        return value;
    }

    public void setValue(Short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
