package com.aoyuntek.aoyun.enums;

/**
 * 
 * @author dlli5 at 2016年8月22日上午10:42:28
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public enum ChangeAttendanceRequestState {

    Default((short) 0, "申请状态"), Cancel((short) 1, "取消"), PartialAproved((short) 2, "处理部分"), Over((short) 3, "已处理"), Discard((short) 4, "丢弃"), OnJob(
            (short) 5, "等待定時處理"), lapsed((short) 6, "失效"), Termination((short) 7, "Termination");

    private Short value;

    private String desc;

    /**
     * 
     * @param value
     * @param desc
     */
    private ChangeAttendanceRequestState(Short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public Short getValue() {
        return value;
    }

    public void setValue(Short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}