package com.aoyuntek.aoyun.enums;

public enum PayrollCategory {
	BaseHourly((short) 0, "Base Hourly"), HolidayPay((short) 1, "Holiday Pay"), Leave((short) 2, "Leave");

	private short value;
	private String desc;

	public short getValue() {
		return value;
	}

	public void setValue(short value) {
		this.value = value;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	private PayrollCategory(short value, String desc) {
		this.value = value;
		this.desc = desc;
	}
}
