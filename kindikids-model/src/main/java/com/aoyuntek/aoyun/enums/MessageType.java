package com.aoyuntek.aoyun.enums;

/**
 * 
 * @description 站内信信息类型
 * @author bbq
 * @create 2016年7月9日下午2:34:22
 * @version 1.0
 */
public enum MessageType {
    Send((short) 0, "正常发送"), Reply((short) 1, "回复发送"), ReplyAll((short) 2, "回复所有"), Forward((short) 3, "转发");
    private short value;
    private String desc;

    private MessageType(short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
