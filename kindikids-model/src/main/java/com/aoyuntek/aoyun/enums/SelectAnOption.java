package com.aoyuntek.aoyun.enums;

public enum SelectAnOption {
    Google((short) 0, "Google"), 
    Website((short) 1, "Website"), 
    Friend((short) 2, "Friend"), 
    Other((short) 3, "Other"), 
    Facebook((short) 4, "Facebook"),
    Instagram((short) 5, "Instagram"),
    DrovePast((short) 6, "DrovePast");

    private short value;
    private String desc;

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private SelectAnOption(short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

}
