package com.aoyuntek.aoyun.enums.dashboard;

public enum TasksProgress {
    OnTime((short) 0, "On Time"), LateCompletion((short) 1, "Late Completion"), Overdue((short) 2, "Overdue");

    private short value;
    private String desc;

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private TasksProgress(short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

}
