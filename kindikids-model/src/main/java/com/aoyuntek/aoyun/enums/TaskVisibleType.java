package com.aoyuntek.aoyun.enums;

/**
 * 
 * @author dlli5 at 2016年9月20日下午1:53:41
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public enum TaskVisibleType {

    CENTER((short) 0, "CENTER"), ROOM((short) 1, "ROOM"), USER((short) 2, "USER"), ROLE((short) 3, "ROLE"), ROLE_GROUP((short) 4, "ROLE_GROUP");

    private Short value;

    private String desc;

    /**
     * 
     * @param value
     * @param desc
     */
    private TaskVisibleType(Short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public Short getValue() {
        return value;
    }

    public void setValue(Short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
