package com.aoyuntek.aoyun.enums.jobs;

public enum MsgEmailCategory {
    EmailMsg((short) 0, "邮件"), PhoneMsg((short) 1, "短信");

    private short value;
    private String desc;

    private MsgEmailCategory(short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
