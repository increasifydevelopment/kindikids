package com.aoyuntek.aoyun.enums;

/**
 * 
 * @author dlli5 at 2016年8月29日下午6:48:44
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public enum AttendanceRequestType {

    K((short) 0, "Attendance"), R((short) 1, "Room"), C((short) 2, "Centre");

    private short value;
    private String desc;

    public static AttendanceRequestType getType(short value) {
        AttendanceRequestType[] items = AttendanceRequestType.values();
        for (AttendanceRequestType item : items) {
            if (item.getValue() == value) {
                return item;
            }
        }
        return null;
    }

    private AttendanceRequestType(short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
