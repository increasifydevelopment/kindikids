package com.aoyuntek.aoyun.enums;

/**
 * @description News归属类别
 * @author gfwang
 * @create 2016年6月28日上午8:58:21
 * @version 1.0
 */
public enum NewsType {

    /**
     * 手动创建
     */
    ManualPost((short) 0, "Manual Post"),
    /**
     * 户外活动创建
     */
    ExploreCurriculum((short) 1, "Explore Curriculum"),
    /**
     * 室内活动创建
     */
    GrowCurriculum((short) 2, "Grow Curriculum"),
    /**
     * 课程创建
     */
    Lessons((short) 3, "Lessons"),
    /**
     * 发现、观察模块填写后创建
     */
    Observation((short) 4, "Observation"),
    /**
     * 发现、观察模块跟踪时创建
     */
    ObservationFollowUp((short) 5, "Observation Follow Up"),
    /**
     * 兴趣模块填写后创建
     */
    InterestFollowUp((short) 6, "Interest Follow Up"),
    /**
     * 读书识字填写后创建
     */
    SchoolReadiness((short) 7, "School Readiness"),
    /**
     * 读书识字发现
     */
    SchoolReadinessObservation((short) 8, "School Readiness Observation"),
    /**
     * 读书识字发现跟踪
     */
    SchoolReadinessObservationFollowUp((short) 9, "School Readiness Observation Follow Up"),
    /**
     * 
     */
    LearningStory((short) 10, "Learning Story"),
    /**
     * 
     */
    LearningStoryFollowUp((short) 11, "Learning Story Follow up"),
    /**
     * 菜单
     */
    TodayMenus((short) 12, "Today’s Menus"),
    /**
     * Child Meal
     */
    ChildMeal((short) 13, "Meal"),
    /**
     * Sleep
     */
    Sleep((short) 14, "Sleep"),
    /**
     * Nappy
     */
    Nappy((short) 15, "Nappy"),
    /**
     * Toilet Registers
     */
    ToiletRegisters((short) 16, "Toilet Registers"),
    /**
     * Weekly Evalution
     */
    WeeklyEvalutionExplore((short) 17, "Weekly Evaluation Explore Curriculum"),

    /**
     * Birthday
     */
    Birthday((short) 18, "Birthday"),
    /**
     * Weekly Evalution
     */
    WeeklyEvalutionGrow((short) 19, "Weekly Evaluation Grow Curriculum"),
    /**
     * 发现、观察模块填写后创建
     */
    Interest((short) 20, "Interest");
    /**
     * 值
     */
    private Short value;
    /**
     * 描述
     */
    private String desc;

    private NewsType(Short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public Short getValue() {
        return value;
    }

    public void setValue(Short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
