package com.aoyuntek.aoyun.enums;

public enum ResultStatus {
    Fail(1, "失败"),

    Success(0, "成功"), ;

    private int value;

    private String name;

    /**
     * 
     * @param value
     * @param desc
     */
    private ResultStatus(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
