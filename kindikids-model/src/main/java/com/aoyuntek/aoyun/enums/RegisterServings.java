package com.aoyuntek.aoyun.enums;

public enum RegisterServings {

    /**
     * A tasing
     * Meal Register
     */
    ATasing((short) 0, "ATasing"),
    /**
     * A half serving
     * Meal Register
     */
    AHalfServing((short) 1, "AHalfServing"),
    /**
     * One serving
     * Meal Register
     */
    OneServing((short) 2, "OneServing"),
    /**
     * More than two meals
     * Meal Register
     */
    MoreThanTwoMeals((short) 3, "MoreThanTwoMeals"),
    /**
     * Absent
     * Meal Register
     */
    AbsentMeal((short) 4, "AbsentMeal"),
    /**
     * Sleep
     * Sleep Register 
     */
    Sleep((short) 5, "Sleep"),
    /**
     * Rest
     * Sleep Register 
     */
    Rest((short) 6, "Rest"),
    /**
     * Absent
     * Sleep Register 
     */
    AbsentSleep((short) 7, "AbsentSleep"),
    /**
     * Nappy wet
     * Nappy Register 
     */
    NappyWet((short) 8, "NappyWet"),
    /**
     * Nappy soiled
     * Nappy Register 
     */
    NappySoiled((short) 9, "NappySoiled"),
    /**
     * Nappy dry
     * Nappy Register 
     */
    NappyDry((short) 10, "NappyDry"),
    /**
     * Absent
     * Nappy Register 
     */
    AbsentNappy((short) 11, "AbsentNappy"),
    /**
     * Toilet wet
     * Toilet Register 
     */
    ToiletWet((short) 12, "ToiletWet"),
    /**
     * Toilet soiled
     * Toilet Register 
     */
    ToiletSoiled((short) 13, "ToiletSoiled"),
    /**
     * Toilet dry
     * Toilet Register 
     */
    ToiletDry((short) 14, "ToiletDry"),
    /**
     * Absent
     * Toilet Register 
     */
    AbsentToilet((short) 15, "AbsentToilet"),
    /**
     * Yes
     * Sunscreen Application Register 
     */
    Yes((short) 16, "Yes"),
    /**
     *  No, due to wet weather
     * Sunscreen Application Register 
     */
    NoDueToWetWeather((short) 17, "NoDueToWetWeather"),
    /**
     * Absent
     * Sunscreen Application Register 
     */
    AbsentSunscreen((short) 18, "AbsentSunscreen");
    
    private short value;
    private String name;

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    private RegisterServings(short value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
}
