package com.aoyuntek.aoyun.enums;

public enum ProgramWeeklyType {
    /**
     * Explore Curriculum
     */
    ExploreCurriculum((short) 0, "Explore Curriculum"),
    /**
     * Grow Curriculum
     */
    GrowCurriculum((short) 1, "Grow Curriculum");

    private short value;
    private String desc;

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private ProgramWeeklyType(short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

}
