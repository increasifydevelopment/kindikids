package com.aoyuntek.aoyun.enums;

/**
 * 
 * @description 园区
 * @author bbq
 * @create 2016年7月9日下午3:32:13
 * @version 1.0
 */
public enum FunctionType {
    /**
     * Fmily
     */
    Fmily((short) 0, "Fmily"),
    /**
     * NewsFeed
     */
    NewsFeed((short) 1, "NewsFeed"),
    /**
     * VisitorList
     */
    VisitorList((short) 2, "VisitorList"),
    /**
     * StaffList
     */
    StaffList((short) 3, "StaffList"),
    /**
     * WatingList
     */
    WatingList((short) 4, "WatingList"),
    /**
     * ProgramList
     */
    ProgramList((short) 5, "ProgramList"),
    /**
     * ProgramList
     */
    RosteringList((short) 6, "RosteringList");
    
    private short value;
    private String name;

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private FunctionType(short value, String desc) {
        this.value = value;
        this.name = desc;
    }
}
