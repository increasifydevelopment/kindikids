package com.aoyuntek.aoyun.enums;

/**
 * 
 * @description 分组标识
 * @author bbq
 * @create 2016年7月9日下午3:32:13
 * @version 1.0
 */
public enum AuthGroupType {
    Message((short) 0, "message"), NewsFeed((short) 1, "newsfeed");

    private short value;
    private String desc;

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private AuthGroupType(short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

}
