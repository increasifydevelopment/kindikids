package com.aoyuntek.aoyun.enums;

/**
 * @description Leave操作枚举
 * @author hxzhang
 * @create 2016年8月16日下午5:32:07
 * @version 1.0
 */
public enum OperaType {
    /**
     * 撤回
     */
    withdrawn((short) 0, "withdrawn"),
    /**
     * 批准
     */
    approve((short) 1, "approve"),
    /**
     * 拒绝
     */
    reject((short) 2, "reject"),
    /**
     * 取消
     */
    cancel((short) 3, "cancel");
    private short value;
    private String desc;

    private OperaType(short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
