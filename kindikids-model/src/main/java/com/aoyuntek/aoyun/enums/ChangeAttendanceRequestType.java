package com.aoyuntek.aoyun.enums;

/**
 * 
 * @author dlli5 at 2016年8月29日下午6:48:44
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public enum ChangeAttendanceRequestType {
    
    ParentRequest((short) 0, "家長申請"),
    CentreManagerRequest((short) 1, "园长申请转园"),
    CeoChangeCentre((short) 2, "CEO直接转园"),
    CentreManagerSure((short) 3, "园长確認转园申请"),
    ChangeRoom((short) 4, "Transfer this child to another room "),
    ChangeAttendance((short) 5, "CEO 园长直接更改课程Change attendance"),
    CEOExternal2Inside((short) 6, "CEOExternal2Inside"),
    InitAttendance((short) 7, "InitAttendance"),
    AddAttendance((short) 8, "AddAttendance");
    
    private short value;
    private String desc;
    
    public static ChangeAttendanceRequestType getType(short value){
        ChangeAttendanceRequestType[] items = ChangeAttendanceRequestType.values();
        for (ChangeAttendanceRequestType item : items) {
            if(item.getValue()==value){
                return  item;
            }
        }
        return null;
    }

    private ChangeAttendanceRequestType(short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
