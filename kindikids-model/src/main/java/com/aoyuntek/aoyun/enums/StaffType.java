package com.aoyuntek.aoyun.enums;

public enum StaffType {
	FullTime((short) 0, "Full Time"), PartTime((short) 1, "Part Time"), Casual((short) 2, "Casual");

	private short value;
	private String Desc;

	public short getValue() {
		return value;
	}

	public void setValue(short value) {
		this.value = value;
	}

	public String getDesc() {
		return Desc;
	}

	public void setDesc(String desc) {
		Desc = desc;
	}

	private StaffType(short value, String desc) {
		this.value = value;
		Desc = desc;
	}
}
