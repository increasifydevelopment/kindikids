package com.aoyuntek.aoyun.enums;


/**
 * 
 * @description 表示分组权限是newsfeed还是message
 * @author gfwang
 * @create 2016年7月9日下午3:32:13
 * @version 1.0
 */
public enum GroupType {
    /**
     * message分组权限
     */
    MessageGroup((short) 0, "message."),
    /**
     * newsfeed分组权限
     */
    newsFeedGroup((short) 1, "newsfeed."),
    /**
     * formBUild分组权限
     */
    FormGroup((short) 2, "form");

    private short value;
    private String name;

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    private GroupType(short value, String name) {
        this.value = value;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
}
