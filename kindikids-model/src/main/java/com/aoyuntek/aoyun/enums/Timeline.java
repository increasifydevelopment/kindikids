package com.aoyuntek.aoyun.enums;

public enum Timeline {
	
	Waitlist((short) 1), Orientation((short) 2), Enrolled((short) 3), GivingNotice((short) 4);

	private short value;

	public short getValue() {
		return value;
	}

	public void setValue(short value) {
		this.value = value;
	}

	Timeline(short value) {
		this.value = value;
	}
}
