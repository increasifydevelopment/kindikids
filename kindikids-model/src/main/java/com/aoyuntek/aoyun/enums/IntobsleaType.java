package com.aoyuntek.aoyun.enums;

/**
 * @description Intobslea类别
 * @author Hxzhang 2016年9月20日下午9:20:23
 * @version 1.0
 */
public enum IntobsleaType {
    Interest((short) 0, "Interest"), Observation((short) 1, "Observation"), LearningStory((short) 2, "Learning Story");

    private short Value;
    private String Desc;

    public short getValue() {
        return Value;
    }

    public void setValue(short value) {
        Value = value;
    }

    public String getDesc() {
        return Desc;
    }

    public void setDesc(String desc) {
        Desc = desc;
    }

    private IntobsleaType(short value, String desc) {
        Value = value;
        Desc = desc;
    }

}
