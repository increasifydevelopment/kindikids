package com.aoyuntek.aoyun.enums;

public enum ProfileTempState {
    Disable((short) 0, "Disable"), Enable((short) 1, "Enable");

    private Short valuse;
    private String desc;

    public Short getValuse() {
        return valuse;
    }

    public void setValuse(Short valuse) {
        this.valuse = valuse;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private ProfileTempState(Short valuse, String desc) {
        this.valuse = valuse;
        this.desc = desc;
    }

}
