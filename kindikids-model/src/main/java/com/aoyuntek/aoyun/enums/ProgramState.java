package com.aoyuntek.aoyun.enums;

/**
 * 
 * @description program状态
 * @author mingwang
 * @create 2016年9月20日上午10:05:16
 * @version 1.0
 */
public enum ProgramState {
    Default((short) -2, "Default"),

    Discard((short) -1, "Discard"),
    /**
     * UnComplete 未完成
     */
    Pending((short) 0, "Pending"),
    /**
     * Complete 完成
     */
    Complete((short) 1, "Complete"),
    /**
     * OverDue 过期
     */
    OverDue((short) 2, "OverDue");

    private short value;
    private String name;

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private ProgramState(short value, String name) {
        this.value = value;
        this.name = name;
    }
}
