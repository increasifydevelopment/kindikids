package com.aoyuntek.aoyun.enums;

/**
 * @description 请假条状态
 * @author hxzhang
 * @create 2016年8月16日上午11:55:04
 * @version 1.0
 */
public enum LeaveStatus {
    /**
     * 申请
     */
    requested((short) 0, "requested"),
    /**
     * 批准
     */
    approved((short) 1, "approved"),
    /**
     * 拒绝
     */
    rejected((short) 2, "rejected"),
    /**
     * 取消
     */
    cancelled((short) 3, "cancelled"),
    /**
     * 撤回
     */
    withdrawn((short) 4, "withdrawn"),
    /**
     * 失效
     */
    lapsed((short) 5, "lapsed");

    private short value;
    private String desc;

    private LeaveStatus(short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
