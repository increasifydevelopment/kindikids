package com.aoyuntek.aoyun.enums;

/**
 * 
 * @description 邮件发出的功能
 * @author gfwang
 * @create 2016年10月9日上午10:48:31
 * @version 1.0
 */
public enum JobsType {
    StaffActiveAccount((short) 1, "员工7天激活邮件");

    private Short value;

    private String desc;

    /**
     * 
     * @param value
     * @param desc
     */
    private JobsType(Short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public Short getValue() {
        return value;
    }

    public void setValue(Short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
