package com.aoyuntek.aoyun.enums.roster;


/**
 * 
 * @author dlli5 at 2016年10月14日下午1:42:39
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public enum RosterStaffType {

    CenterManager((short) 0, "CenterManager"), Cook((short) 1, "cook"), AdditionalStaff((short) 2, "Additional Staff"), Sign((short) 3, "Sign");

    private Short value;

    private String desc;

    public static RosterStaffType getType(short value) {
        RosterStaffType[] items = RosterStaffType.values();
        for (RosterStaffType item : items) {
            if (item.getValue() == value) {
                return item;
            }
        }
        return null;
    }

    private RosterStaffType(Short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public Short getValue() {
        return value;
    }

    public void setValue(Short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
