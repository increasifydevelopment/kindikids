package com.aoyuntek.aoyun.enums;

/**
 * 
 * @description 读取标识
 * @author bbq
 * @create 2016年7月9日下午2:39:44
 * @version 1.0
 */
public enum ReadFlag {
    Default((short) 0, "默认未读"), Read((short) 1, "已读"), ;

    private Short value;

    private String desc;

    /**
     * 
     * @param value
     * @param desc
     */
    private ReadFlag(Short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public Short getValue() {
        return value;
    }

    public void setValue(Short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
