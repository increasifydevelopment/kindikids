package com.aoyuntek.aoyun.enums;

public enum TemporaryType {
    Temporary((short) 0, "Temporary"),All((short) 1, "ALL");

    private short value;
    private String desc;

    private TemporaryType(short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
