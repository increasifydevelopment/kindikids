package com.aoyuntek.aoyun.enums.roster;

/**
 * 
 * @author dlli5 at 2017年1月11日下午2:06:52
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public enum TempRosterCenterState {

    BC((short) -2, "B=>C"), CA((short) -1, "C=A"), Default((short) 0, "default"), Start((short) 1, "Start"), End((short) 2, "End");

    /**
     * 值
     */
    private Short value;
    /**
     * 描述
     */
    private String desc;

    private TempRosterCenterState(Short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public Short getValue() {
        return value;
    }

    public void setValue(Short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
