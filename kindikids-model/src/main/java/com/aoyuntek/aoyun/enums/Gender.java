package com.aoyuntek.aoyun.enums;

public enum Gender {
    Female((short) 0, "女性"), Male((short) 1, "男性");

    private short values;
    private String desc;

    private Gender(short values, String desc) {
        this.values = values;
        this.desc = desc;
    }

    public short getValues() {
        return values;
    }

    public void setValues(short values) {
        this.values = values;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
