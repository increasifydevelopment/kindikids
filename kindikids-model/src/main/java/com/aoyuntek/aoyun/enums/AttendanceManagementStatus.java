package com.aoyuntek.aoyun.enums;

public enum AttendanceManagementStatus {
    Default((short) -1, "default"), Pending((short) 0, "Pending"), Cancelled((short) 1, "Cancelled"), PartialApproved((short) 2, "Partial Approved"), Approved(
            (short) 3, "Approved"), Declined((short) 4, "Declined"), Approved2((short) 5, "Approved"), Lapsed((short) 6, "Lapsed");

    private short value;
    private String desc;

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private AttendanceManagementStatus(short value, String desc) {
        this.value = value;
        this.desc = desc;
    }
}
