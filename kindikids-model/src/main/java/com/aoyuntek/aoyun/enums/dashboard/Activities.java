package com.aoyuntek.aoyun.enums.dashboard;

public enum Activities {
    Observation((short) 6, "Observation"), ObeservationFollowUp((short) 7, "Obeservation Follow Up"), Interests((short) 4, "Interests"), InterestsFollowUp(
            (short) 5, "Interests Follow Up"), LearningStory((short) 8, "Learning Story"), LearningStoryFollowUp((short) 9,
            "Learning Story Follow Up");
    
    private short value;
    private String desc;

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private Activities(short value, String desc) {
        this.value = value;
        this.desc = desc;
    }
}
