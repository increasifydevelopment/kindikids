package com.aoyuntek.aoyun.enums;

public enum Region {
	EdensorPark((short) 1, "Edensor Park"), Ryde((short) 2, "Ryde"), HeadOffice((short) 3, "Head Office");

	private short value;
	private String desc;

	public short getValue() {
		return value;
	}

	public void setValue(short value) {
		this.value = value;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	private Region(short value, String desc) {
		this.value = value;
		this.desc = desc;
	}
}
