package com.aoyuntek.aoyun.enums.meeting;

public enum CurrtenFlag {
    OldVersion((short) 0, "OldVersion"), NewVersion((short) 1, "NewVersion");

    private Short value;
    private String desc;

    public Short getValue() {
        return value;
    }

    public void setValue(Short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private CurrtenFlag(Short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

}
