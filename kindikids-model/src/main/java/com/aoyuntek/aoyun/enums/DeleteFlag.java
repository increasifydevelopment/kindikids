package com.aoyuntek.aoyun.enums;

/**
 * 刪除標示
 * 
 * @author dlli5 at 2016年4月26日下午4:05:00
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public enum DeleteFlag {

	Default((short)0, "默认未删除"), Delete((short)1, "已删除"), ;

	private Short value;

	private String desc;

	/**
	 * 
	 * @param value
	 * @param desc
	 */
	private DeleteFlag(Short value, String desc) {
		this.value = value;
		this.desc = desc;
	}

	public Short getValue() {
		return value;
	}

	public void setValue(Short value) {
		this.value = value;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
}