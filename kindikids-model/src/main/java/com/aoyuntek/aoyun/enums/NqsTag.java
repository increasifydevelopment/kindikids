package com.aoyuntek.aoyun.enums;

/**
 * @description nqs 版本控制
 * @author gfwang
 * @create 2018年3月13日上午11:40:22
 * @version 1.0
 */
public enum NqsTag {
    /**
     * 第一版本
     */
    V1((short) 1, "V1"),
    /**
     * 第二版本
     */
    V2((short) 2, "V2");

    private short value;
    private String desc;

    private NqsTag(short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
