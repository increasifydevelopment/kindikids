package com.aoyuntek.aoyun.enums.roster;

/**
 * 
 * @author dlli5 at 2016年10月14日下午1:42:39
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public enum RosterStatus {

    Draft((short) 0, "Draft"), Submitted((short) 1, "Submitted"), Approved((short) 2, "Approved");

    /**
     * 值
     */
    private Short value;
    /**
     * 描述
     */
    private String desc;

    private RosterStatus(Short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public Short getValue() {
        return value;
    }

    public void setValue(Short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
