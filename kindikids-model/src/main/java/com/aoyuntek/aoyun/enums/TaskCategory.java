package com.aoyuntek.aoyun.enums;

/**
 * 
 * @author dlli5 at 2016年9月20日下午1:53:41
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public enum TaskCategory {

    CENTER((short) 0, "CENTER"), ROOM((short) 1, "ROOM"), CHILDRRENN((short) 2, "CHILDRRENN"), STAFF((short) 3, "STAFF"), Individual((short) 4, "Individual");

    private Short value;

    private String desc;

    public static TaskCategory getType(short value) {
        TaskCategory[] items = TaskCategory.values();
        for (TaskCategory item : items) {
            if (item.getValue() == value) {
                return item;
            }
        }
        return null;
    }

    /**
     * 
     * @param value
     * @param desc
     */
    private TaskCategory(Short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public Short getValue() {
        return value;
    }

    public void setValue(Short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
