package com.aoyuntek.aoyun.enums;

/**
 * 园区是否归档
 * 
 * @author dlli5 at 2016年8月29日上午9:58:08
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public enum ArchivedStatus {

    UnArchived((short) 1, "未归档"), Archived((short) 0, "归档"), Default((short) -1, "默认值");

    private Short value;

    private String desc;

    /**
     * 
     * @param value
     * @param desc
     */
    private ArchivedStatus(Short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public Short getValue() {
        return value;
    }

    public void setValue(Short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
