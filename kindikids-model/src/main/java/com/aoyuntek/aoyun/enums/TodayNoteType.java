package com.aoyuntek.aoyun.enums;

/**
 * 
 * @description
 * @author gfwang
 * @create 2017年4月18日上午10:21:08
 * @version 1.0
 */
public enum TodayNoteType {

    Dietary_Requirement(0, "ALLERGIES / INTOLERANCES / RESTRICTIONS"), Medical(1, "MEDICAL"), Custody_Arrangements(2, "CUSTODY ARRANGEMENTS"), Event(
            3, "EVENTS"), Change_Attend(4, "ATTENDANCE CHANGES"), Medical_Task(5, "ADMINISTRATION OF MEDICATION"), OverDue_Task(6, "OverDue Task");

    private int index;

    private String value;

    /**
     * 
     * @param value
     * @param desc
     */
    private TodayNoteType(int index, String value) {
        this.index = index;
        this.value = value;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
