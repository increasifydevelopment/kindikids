package com.aoyuntek.aoyun.enums;

public enum ApplicationStatus {
	PendingForAction((short) 0, "Pending for Action"), MovedToExternal((short) 1, "Moved to External"),

	MovedToSibling((short) 2, "Moved to Sibling"), MovedToArchivedApplication((short) 3, "Moved to Archived Application");

	private short value;
	private String desc;

	public short getValue() {
		return value;
	}

	public void setValue(short value) {
		this.value = value;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	private ApplicationStatus(short value, String desc) {
		this.value = value;
		this.desc = desc;
	}

	public static ApplicationStatus getType(short value) {
		ApplicationStatus[] applicationStatus = ApplicationStatus.values();
		for (ApplicationStatus status : applicationStatus) {
			if (status.getValue() == value) {
				return status;
			}
		}
		return null;
	}
}
