package com.aoyuntek.aoyun.enums;

/**
 * @author zcfu at 2016年4月29日下午3:20:01
 * @Email zcfu@aoyuntek.cn
 * @QQ 1373527604
 */
public enum AccountActiveStatus {
    Enabled((short) 1, "启用"), Disabled((short) 0, "禁用");

    private Short value;

    private String desc;

    /**
     * 
     * @param value
     * @param desc
     */
    private AccountActiveStatus(Short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public Short getValue() {
        return value;
    }

    public void setValue(Short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
