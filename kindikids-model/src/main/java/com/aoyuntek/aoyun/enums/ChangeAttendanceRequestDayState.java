package com.aoyuntek.aoyun.enums;

/**
 * 
 * @author dlli5 at 2016年8月22日上午10:42:28
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public enum ChangeAttendanceRequestDayState {

    UnCheck((short) 0, "未勾选"), Check((short) 1, "钩选"), Deal((short) 2, "已安排"), Cancel((short) 3, "取消");

    private Short value;

    private String desc;

    /**
     * 
     * @param value
     * @param desc
     */
    private ChangeAttendanceRequestDayState(Short value, String desc) {
        this.value = value;
        this.desc = desc;
    }

    public Short getValue() {
        return value;
    }

    public void setValue(Short value) {
        this.value = value;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}