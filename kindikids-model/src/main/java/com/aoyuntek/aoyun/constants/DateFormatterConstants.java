package com.aoyuntek.aoyun.constants;

/**
 * 
 * @author dlli5 at 2016年10月23日上午11:27:11
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public class DateFormatterConstants {
    //*************************切勿动*************************//
    public static final String SYS_FORMAT = "dd/MM/yyyy HH:mm:ss";
    public static final String SYS_FORMAT2 = "dd/MM/yyyy";
    public static final String SYS_T_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS";
    public static final String SYS_T_FORMAT2 = "dd/MM/yyyy'T'HH:mm:ss.SSS";
}
