package com.aoyuntek.aoyun.constants;

import java.io.File;

public class SystemConstants {

    public final static String JobCreateAccountId = "job";

    public final static String startHour = "21:00:00";
    public final static String endHour = "05:00:00";

    public final static String defaultFile = "temp/ce5fe702-91de-4edd-bb95-2381a576a1c9.png";

    public final static String SPLITTAG = "&&&";

    public final static String EMAIL_WEB_TEMPLET_PATH = "WEB-INF" + File.separator + "classes" + File.separator + "conf" + File.separator + "templet"
            + File.separator ;
    
}
