package com.aoyuntek.aoyun.constants;

public class ChildDietaryRequirConstants {

    public static final String[] ChildDietaryRequirs = { "Food containing dairy, eg. yoghurt, custard, cream, cheese, etc", "Milk",
            "Food containing dairy powder eg. packet cakes", "All foods that may contain traces of nuts", "Peanuts", "Cashews", "Almonds",
            " Whole eggs", "Egg yolks", "Food containing eggs, eg. cakes, quiche", "Lamb", "Beef", "Chicken", "Pork", "Fish" };
}
