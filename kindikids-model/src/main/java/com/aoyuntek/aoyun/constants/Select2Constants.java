package com.aoyuntek.aoyun.constants;

public class Select2Constants {

    public final static String CentreManagers="Centre Managers";
    
    public final static String AllCentres="All Centres";
    
    public final static String AllCentreRooms="All Centre Rooms";
    
    public final static String  AllChildren="All Children";
    
    public final static String  AllCentresStaff="All Centres Staffs";
    
    public final static String  AllEducatorsInThRoom="All Educators In The Room";
    
    public final static String  Directors="Directors";
    
    public final static String  AllStaffInTheCentre="All Staff In The Centre";
    
    public final static String  AllXRooms="All {0} Rooms";
    
    public final static String  AllXChildren="All {0} Children";
    
    public final static String  AllXStaffs="All {0} Staffs";
    
    
    
    
    
}
