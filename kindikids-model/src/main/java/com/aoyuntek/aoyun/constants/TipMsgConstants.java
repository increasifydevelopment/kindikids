/**
 * <b>项目名：</b>kindkids<br/>
 * <b>包   名：</b>com.aoyuntek.aoyun.constants<br/>
 * <b>文件名：</b>TipMsgConstants.java<br/>
 * <b>版本信息：</b>1.1<br/>
 * <b>日期：</b>2016年4月28日-下午4:41:17<br/>
 * 
 */
package com.aoyuntek.aoyun.constants;

/**
 * <b>类 名：</b>TipMsgConstants<br/>
 * <b>类描述：</b>提示语对应的常量类<br/>
 * <b>创建人：</b>weiyang<br/>
 * <b>创建时间：</b>2016年4月28日 下午4:41:17<br/>
 * <b>修改人：</b>weiyang<br/>
 * <b>修改时间：</b>2016年4月28日 下午4:41:17<br/>
 * <b>修改备注：</b><br/>
 * 
 * @version 1.1<br/>
 * 
 */
public class TipMsgConstants {

    /**
     * 提示：查询数据失败
     */
    public static String FIND_DATA_FALIED = "find_data_fail";

    public static String[] WEEKITEMS = { "Monday's", "Tuesday's", "Wednesday's", "Thursday's", "Friday's" };
    
    public static String[] WEEKNAMES = { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday" };
}
