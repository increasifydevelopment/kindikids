package com.aoyuntek.aoyun.constants;

import com.aoyuntek.framework.constants.PropertieNameConts;
import com.theone.resource.util.PropertiesCacheUtil;

/**
 * 
 * @author dlli5 at 2016年9月19日上午9:52:25
 * @Email dlli5@iflytek.com
 * @QQ 386115312
 */
public class S3Constants {

    /**
     * S3链接失效时间
     */
    public static final int URL_TIMEOUT_HOUR = PropertiesCacheUtil.getIntValue("URL_TIMEOUT_HOUR", PropertieNameConts.S3);

    public static final String BUCKET_NAME = PropertiesCacheUtil.getValue("BucketName", PropertieNameConts.S3);
}
